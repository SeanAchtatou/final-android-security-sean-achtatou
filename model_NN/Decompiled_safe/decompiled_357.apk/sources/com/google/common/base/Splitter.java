package com.google.common.base;

import com.google.common.annotations.GwtCompatible;
import com.google.common.annotations.GwtIncompatible;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@GwtCompatible(emulated = true)
public final class Splitter {
    /* access modifiers changed from: private */
    public final int limit;
    /* access modifiers changed from: private */
    public final boolean omitEmptyStrings;
    /* access modifiers changed from: private */
    public final Strategy strategy;
    /* access modifiers changed from: private */
    public final CharMatcher trimmer;

    private interface Strategy {
        Iterator<String> iterator(Splitter splitter, CharSequence charSequence);
    }

    private Splitter(Strategy strategy2) {
        this(strategy2, false, CharMatcher.NONE, Integer.MAX_VALUE);
    }

    private Splitter(Strategy strategy2, boolean omitEmptyStrings2, CharMatcher trimmer2, int limit2) {
        this.strategy = strategy2;
        this.omitEmptyStrings = omitEmptyStrings2;
        this.trimmer = trimmer2;
        this.limit = limit2;
    }

    public static Splitter on(char separator) {
        return on(CharMatcher.is(separator));
    }

    public static Splitter on(final CharMatcher separatorMatcher) {
        Preconditions.checkNotNull(separatorMatcher);
        return new Splitter(new Strategy() {
            public SplittingIterator iterator(Splitter splitter, CharSequence toSplit) {
                return new SplittingIterator(splitter, toSplit) {
                    /* access modifiers changed from: package-private */
                    public int separatorStart(int start) {
                        return separatorMatcher.indexIn(this.toSplit, start);
                    }

                    /* access modifiers changed from: package-private */
                    public int separatorEnd(int separatorPosition) {
                        return separatorPosition + 1;
                    }
                };
            }
        });
    }

    public static Splitter on(final String separator) {
        Preconditions.checkArgument(separator.length() != 0, "The separator may not be the empty string.");
        return new Splitter(new Strategy() {
            public SplittingIterator iterator(Splitter splitter, CharSequence toSplit) {
                return new SplittingIterator(splitter, toSplit) {
                    public int separatorStart(int start) {
                        int delimeterLength = separator.length();
                        int p = start;
                        int last = this.toSplit.length() - delimeterLength;
                        while (p <= last) {
                            int i = 0;
                            while (i < delimeterLength) {
                                if (this.toSplit.charAt(i + p) != separator.charAt(i)) {
                                    p++;
                                } else {
                                    i++;
                                }
                            }
                            return p;
                        }
                        return -1;
                    }

                    public int separatorEnd(int separatorPosition) {
                        return separator.length() + separatorPosition;
                    }
                };
            }
        });
    }

    @GwtIncompatible("java.util.regex")
    public static Splitter on(final Pattern separatorPattern) {
        boolean z;
        Preconditions.checkNotNull(separatorPattern);
        if (!separatorPattern.matcher("").matches()) {
            z = true;
        } else {
            z = false;
        }
        Preconditions.checkArgument(z, "The pattern may not match the empty string: %s", separatorPattern);
        return new Splitter(new Strategy() {
            public SplittingIterator iterator(Splitter splitter, CharSequence toSplit) {
                final Matcher matcher = separatorPattern.matcher(toSplit);
                return new SplittingIterator(splitter, toSplit) {
                    public int separatorStart(int start) {
                        if (matcher.find(start)) {
                            return matcher.start();
                        }
                        return -1;
                    }

                    public int separatorEnd(int separatorPosition) {
                        return matcher.end();
                    }
                };
            }
        });
    }

    @GwtIncompatible("java.util.regex")
    public static Splitter onPattern(String separatorPattern) {
        return on(Pattern.compile(separatorPattern));
    }

    public static Splitter fixedLength(final int length) {
        Preconditions.checkArgument(length > 0, "The length may not be less than 1");
        return new Splitter(new Strategy() {
            public SplittingIterator iterator(Splitter splitter, CharSequence toSplit) {
                return new SplittingIterator(splitter, toSplit) {
                    public int separatorStart(int start) {
                        int nextChunkStart = start + length;
                        if (nextChunkStart < this.toSplit.length()) {
                            return nextChunkStart;
                        }
                        return -1;
                    }

                    public int separatorEnd(int separatorPosition) {
                        return separatorPosition;
                    }
                };
            }
        });
    }

    public Splitter omitEmptyStrings() {
        return new Splitter(this.strategy, true, this.trimmer, this.limit);
    }

    public Splitter trimResults() {
        return trimResults(CharMatcher.WHITESPACE);
    }

    public Splitter trimResults(CharMatcher trimmer2) {
        Preconditions.checkNotNull(trimmer2);
        return new Splitter(this.strategy, this.omitEmptyStrings, trimmer2, this.limit);
    }

    public Iterable<String> split(final CharSequence sequence) {
        Preconditions.checkNotNull(sequence);
        return new Iterable<String>() {
            public Iterator<String> iterator() {
                return Splitter.this.strategy.iterator(Splitter.this, sequence);
            }
        };
    }

    private static abstract class SplittingIterator extends AbstractIterator<String> {
        int limit;
        int offset = 0;
        final boolean omitEmptyStrings;
        final CharSequence toSplit;
        final CharMatcher trimmer;

        /* access modifiers changed from: package-private */
        public abstract int separatorEnd(int i);

        /* access modifiers changed from: package-private */
        public abstract int separatorStart(int i);

        protected SplittingIterator(Splitter splitter, CharSequence toSplit2) {
            super();
            this.trimmer = splitter.trimmer;
            this.omitEmptyStrings = splitter.omitEmptyStrings;
            this.limit = splitter.limit;
            this.toSplit = toSplit2;
        }

        /* Debug info: failed to restart local var, previous not found, register: 8 */
        /* access modifiers changed from: protected */
        public String computeNext() {
            int end;
            while (this.offset != -1) {
                int start = this.offset;
                int separatorPosition = separatorStart(this.offset);
                if (separatorPosition == -1) {
                    end = this.toSplit.length();
                    this.offset = -1;
                } else {
                    end = separatorPosition;
                    this.offset = separatorEnd(separatorPosition);
                }
                while (start < end && this.trimmer.matches(this.toSplit.charAt(start))) {
                    start++;
                }
                while (end > start && this.trimmer.matches(this.toSplit.charAt(end - 1))) {
                    end--;
                }
                if (this.omitEmptyStrings) {
                    if (start != end) {
                    }
                }
                if (this.limit == 1) {
                    int end2 = this.toSplit.length();
                    this.offset = -1;
                    while (end > start && this.trimmer.matches(this.toSplit.charAt(end - 1))) {
                        end2 = end - 1;
                    }
                } else {
                    this.limit--;
                }
                return this.toSplit.subSequence(start, end).toString();
            }
            return (String) endOfData();
        }
    }

    private static abstract class AbstractIterator<T> implements Iterator<T> {
        T next;
        State state;

        enum State {
            READY,
            NOT_READY,
            DONE,
            FAILED
        }

        /* access modifiers changed from: protected */
        public abstract T computeNext();

        private AbstractIterator() {
            this.state = State.NOT_READY;
        }

        /* access modifiers changed from: protected */
        public final T endOfData() {
            this.state = State.DONE;
            return null;
        }

        public final boolean hasNext() {
            Preconditions.checkState(this.state != State.FAILED);
            switch (this.state) {
                case DONE:
                    return false;
                case READY:
                    return true;
                default:
                    return tryToComputeNext();
            }
        }

        /* access modifiers changed from: package-private */
        public boolean tryToComputeNext() {
            this.state = State.FAILED;
            this.next = computeNext();
            if (this.state == State.DONE) {
                return false;
            }
            this.state = State.READY;
            return true;
        }

        public final T next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            this.state = State.NOT_READY;
            return this.next;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
