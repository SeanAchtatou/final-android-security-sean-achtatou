package com.google.common.base;

import com.google.common.annotations.Beta;
import java.util.concurrent.Future;

@Beta
public interface Service {

    @Beta
    public enum State {
        NEW,
        STARTING,
        RUNNING,
        STOPPING,
        TERMINATED,
        FAILED
    }

    boolean isRunning();

    Future<State> start();

    State startAndWait();

    State state();

    Future<State> stop();

    State stopAndWait();
}
