package com.google.common.base;

import com.google.common.annotations.GwtCompatible;
import org.codehaus.jackson.org.objectweb.asm.signature.SignatureVisitor;

@GwtCompatible
public enum CaseFormat {
    LOWER_HYPHEN(CharMatcher.is(SignatureVisitor.SUPER), "-"),
    LOWER_UNDERSCORE(CharMatcher.is('_'), "_"),
    LOWER_CAMEL(CharMatcher.inRange('A', 'Z'), ""),
    UPPER_CAMEL(CharMatcher.inRange('A', 'Z'), ""),
    UPPER_UNDERSCORE(CharMatcher.is('_'), "_");
    
    private final CharMatcher wordBoundary;
    private final String wordSeparator;

    private CaseFormat(CharMatcher wordBoundary2, String wordSeparator2) {
        this.wordBoundary = wordBoundary2;
        this.wordSeparator = wordSeparator2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [?, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, ?]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public String to(CaseFormat format, String s) {
        if (format == null) {
            throw new NullPointerException();
        } else if (s == null) {
            throw new NullPointerException();
        } else if (format == this) {
            return s;
        } else {
            switch (this) {
                case LOWER_UNDERSCORE:
                    switch (format) {
                        case UPPER_UNDERSCORE:
                            return Ascii.toUpperCase(s);
                        case LOWER_HYPHEN:
                            return s.replace('_', (char) SignatureVisitor.SUPER);
                    }
                case UPPER_UNDERSCORE:
                    switch (format) {
                        case LOWER_UNDERSCORE:
                            return Ascii.toLowerCase(s);
                        case LOWER_HYPHEN:
                            return Ascii.toLowerCase(s.replace('_', (char) SignatureVisitor.SUPER));
                    }
                case LOWER_HYPHEN:
                    switch (format) {
                        case LOWER_UNDERSCORE:
                            return s.replace((char) SignatureVisitor.SUPER, '_');
                        case UPPER_UNDERSCORE:
                            return Ascii.toUpperCase(s.replace((char) SignatureVisitor.SUPER, '_'));
                    }
            }
            StringBuilder out = null;
            int i = 0;
            int j = -1;
            while (true) {
                j = this.wordBoundary.indexIn(s, j + 1);
                if (j != -1) {
                    if (i == 0) {
                        out = new StringBuilder(s.length() + (this.wordSeparator.length() * 4));
                        out.append(format.normalizeFirstWord(s.substring(i, j)));
                    } else {
                        out.append(format.normalizeWord(s.substring(i, j)));
                    }
                    out.append(format.wordSeparator);
                    i = j + this.wordSeparator.length();
                } else if (i == 0) {
                    return format.normalizeFirstWord(s);
                } else {
                    out.append(format.normalizeWord(s.substring(i)));
                    return out.toString();
                }
            }
        }
    }

    private String normalizeFirstWord(String word) {
        switch (this) {
            case LOWER_CAMEL:
                return Ascii.toLowerCase(word);
            default:
                return normalizeWord(word);
        }
    }

    private String normalizeWord(String word) {
        switch (this) {
            case LOWER_UNDERSCORE:
                return Ascii.toLowerCase(word);
            case UPPER_UNDERSCORE:
                return Ascii.toUpperCase(word);
            case LOWER_HYPHEN:
                return Ascii.toLowerCase(word);
            case LOWER_CAMEL:
                return firstCharOnlyToUpper(word);
            case UPPER_CAMEL:
                return firstCharOnlyToUpper(word);
            default:
                throw new RuntimeException("unknown case: " + this);
        }
    }

    private static String firstCharOnlyToUpper(String word) {
        int length = word.length();
        if (length == 0) {
            return word;
        }
        return new StringBuilder(length).append(Ascii.toUpperCase(word.charAt(0))).append(Ascii.toLowerCase(word.substring(1))).toString();
    }
}
