package com.google.common.base;

import com.google.common.annotations.Beta;
import com.google.common.annotations.GwtCompatible;
import javax.annotation.Nullable;

@GwtCompatible
@Beta
public final class Equivalences {

    private enum Impl implements Equivalence<Object> {
        EQUALS {
            public boolean equivalent(@Nullable Object a, @Nullable Object b) {
                if (a == null) {
                    return b == null;
                }
                return a.equals(b);
            }

            public int hash(@Nullable Object o) {
                if (o == null) {
                    return 0;
                }
                return o.hashCode();
            }
        },
        IDENTITY {
            public boolean equivalent(@Nullable Object a, @Nullable Object b) {
                return a == b;
            }

            public int hash(@Nullable Object o) {
                return System.identityHashCode(o);
            }
        }
    }

    private Equivalences() {
    }

    public static Equivalence<Object> equals() {
        return Impl.EQUALS;
    }

    @Deprecated
    public static Equivalence<Object> nullAwareEquals() {
        return Impl.EQUALS;
    }

    public static Equivalence<Object> identity() {
        return Impl.IDENTITY;
    }
}
