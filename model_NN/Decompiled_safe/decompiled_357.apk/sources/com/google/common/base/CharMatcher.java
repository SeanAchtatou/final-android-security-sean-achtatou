package com.google.common.base;

import com.google.common.annotations.Beta;
import com.google.common.annotations.GwtCompatible;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.codehaus.jackson.org.objectweb.asm.Opcodes;

@GwtCompatible
@Beta
public abstract class CharMatcher implements Predicate<Character> {
    public static final CharMatcher ANY = new CharMatcher() {
        public /* bridge */ /* synthetic */ boolean apply(Object x0) {
            return CharMatcher.super.apply((Character) x0);
        }

        public boolean matches(char c) {
            return true;
        }

        public int indexIn(CharSequence sequence) {
            return sequence.length() == 0 ? -1 : 0;
        }

        public int indexIn(CharSequence sequence, int start) {
            int length = sequence.length();
            Preconditions.checkPositionIndex(start, length);
            if (start == length) {
                return -1;
            }
            return start;
        }

        public int lastIndexIn(CharSequence sequence) {
            return sequence.length() - 1;
        }

        public boolean matchesAllOf(CharSequence sequence) {
            Preconditions.checkNotNull(sequence);
            return true;
        }

        public boolean matchesNoneOf(CharSequence sequence) {
            return sequence.length() == 0;
        }

        public String removeFrom(CharSequence sequence) {
            Preconditions.checkNotNull(sequence);
            return "";
        }

        public String replaceFrom(CharSequence sequence, char replacement) {
            char[] array = new char[sequence.length()];
            Arrays.fill(array, replacement);
            return new String(array);
        }

        public String replaceFrom(CharSequence sequence, CharSequence replacement) {
            StringBuilder retval = new StringBuilder(sequence.length() * replacement.length());
            for (int i = 0; i < sequence.length(); i++) {
                retval.append(replacement);
            }
            return retval.toString();
        }

        public String collapseFrom(CharSequence sequence, char replacement) {
            return sequence.length() == 0 ? "" : String.valueOf(replacement);
        }

        public String trimFrom(CharSequence sequence) {
            Preconditions.checkNotNull(sequence);
            return "";
        }

        public int countIn(CharSequence sequence) {
            return sequence.length();
        }

        public CharMatcher and(CharMatcher other) {
            return (CharMatcher) Preconditions.checkNotNull(other);
        }

        public CharMatcher or(CharMatcher other) {
            Preconditions.checkNotNull(other);
            return this;
        }

        public CharMatcher negate() {
            return NONE;
        }

        public CharMatcher precomputed() {
            return this;
        }
    };
    public static final CharMatcher ASCII = inRange(0, 127);
    public static final CharMatcher BREAKING_WHITESPACE = anyOf(BREAKING_WHITESPACE_CHARS).or(inRange(8192, 8198)).or(inRange(8200, 8202)).precomputed();
    private static final String BREAKING_WHITESPACE_CHARS = "\t\n\u000b\f\r     　";
    public static final CharMatcher DIGIT;
    public static final CharMatcher INVISIBLE = inRange(0, ' ').or(inRange(127, 160)).or(is(173)).or(inRange(1536, 1539)).or(anyOf("۝܏ ឴឵᠎")).or(inRange(8192, 8207)).or(inRange(8232, 8239)).or(inRange(8287, 8292)).or(inRange(8298, 8303)).or(is(12288)).or(inRange(55296, 63743)).or(anyOf("﻿￹￺￻")).precomputed();
    public static final CharMatcher JAVA_DIGIT = new CharMatcher() {
        public /* bridge */ /* synthetic */ boolean apply(Object x0) {
            return CharMatcher.super.apply((Character) x0);
        }

        public boolean matches(char c) {
            return Character.isDigit(c);
        }
    };
    public static final CharMatcher JAVA_ISO_CONTROL = inRange(0, 31).or(inRange(127, 159));
    public static final CharMatcher JAVA_LETTER = new CharMatcher() {
        public /* bridge */ /* synthetic */ boolean apply(Object x0) {
            return CharMatcher.super.apply((Character) x0);
        }

        public boolean matches(char c) {
            return Character.isLetter(c);
        }
    };
    public static final CharMatcher JAVA_LETTER_OR_DIGIT = new CharMatcher() {
        public /* bridge */ /* synthetic */ boolean apply(Object x0) {
            return CharMatcher.super.apply((Character) x0);
        }

        public boolean matches(char c) {
            return Character.isLetterOrDigit(c);
        }
    };
    public static final CharMatcher JAVA_LOWER_CASE = new CharMatcher() {
        public /* bridge */ /* synthetic */ boolean apply(Object x0) {
            return CharMatcher.super.apply((Character) x0);
        }

        public boolean matches(char c) {
            return Character.isLowerCase(c);
        }
    };
    public static final CharMatcher JAVA_UPPER_CASE = new CharMatcher() {
        public /* bridge */ /* synthetic */ boolean apply(Object x0) {
            return CharMatcher.super.apply((Character) x0);
        }

        public boolean matches(char c) {
            return Character.isUpperCase(c);
        }
    };
    public static final CharMatcher JAVA_WHITESPACE = inRange(9, 13).or(inRange(28, ' ')).or(is(5760)).or(is(6158)).or(inRange(8192, 8198)).or(inRange(8200, 8203)).or(inRange(8232, 8233)).or(is(8287)).or(is(12288)).precomputed();
    public static final CharMatcher NONE = new CharMatcher() {
        public /* bridge */ /* synthetic */ boolean apply(Object x0) {
            return CharMatcher.super.apply((Character) x0);
        }

        public boolean matches(char c) {
            return false;
        }

        public int indexIn(CharSequence sequence) {
            Preconditions.checkNotNull(sequence);
            return -1;
        }

        public int indexIn(CharSequence sequence, int start) {
            Preconditions.checkPositionIndex(start, sequence.length());
            return -1;
        }

        public int lastIndexIn(CharSequence sequence) {
            Preconditions.checkNotNull(sequence);
            return -1;
        }

        public boolean matchesAllOf(CharSequence sequence) {
            return sequence.length() == 0;
        }

        public boolean matchesNoneOf(CharSequence sequence) {
            Preconditions.checkNotNull(sequence);
            return true;
        }

        public String removeFrom(CharSequence sequence) {
            return sequence.toString();
        }

        public String replaceFrom(CharSequence sequence, char replacement) {
            return sequence.toString();
        }

        public String replaceFrom(CharSequence sequence, CharSequence replacement) {
            Preconditions.checkNotNull(replacement);
            return sequence.toString();
        }

        public String collapseFrom(CharSequence sequence, char replacement) {
            return sequence.toString();
        }

        public String trimFrom(CharSequence sequence) {
            return sequence.toString();
        }

        public int countIn(CharSequence sequence) {
            Preconditions.checkNotNull(sequence);
            return 0;
        }

        public CharMatcher and(CharMatcher other) {
            Preconditions.checkNotNull(other);
            return this;
        }

        public CharMatcher or(CharMatcher other) {
            return (CharMatcher) Preconditions.checkNotNull(other);
        }

        public CharMatcher negate() {
            return ANY;
        }

        /* access modifiers changed from: package-private */
        public void setBits(LookupTable table) {
        }

        public CharMatcher precomputed() {
            return this;
        }
    };
    private static final String NON_BREAKING_WHITESPACE_CHARS = " ᠎ ";
    public static final CharMatcher SINGLE_WIDTH = inRange(0, 1273).or(is(1470)).or(inRange(1488, 1514)).or(is(1523)).or(is(1524)).or(inRange(1536, 1791)).or(inRange(1872, 1919)).or(inRange(3584, 3711)).or(inRange(7680, 8367)).or(inRange(8448, 8506)).or(inRange(64336, 65023)).or(inRange(65136, 65279)).or(inRange(65377, 65500)).precomputed();
    public static final CharMatcher WHITESPACE = anyOf("\t\n\u000b\f\r     　 ᠎ ").or(inRange(8192, 8202)).precomputed();

    public abstract boolean matches(char c);

    static {
        CharMatcher digit = inRange('0', '9');
        for (char base : "٠۰߀०০੦૦୦௦౦೦൦๐໐༠၀႐០᠐᥆᧐᭐᮰᱀᱐꘠꣐꤀꩐０".toCharArray()) {
            digit = digit.or(inRange(base, (char) (base + 9)));
        }
        DIGIT = digit.precomputed();
    }

    public static CharMatcher is(final char match) {
        return new CharMatcher() {
            public /* bridge */ /* synthetic */ boolean apply(Object x0) {
                return CharMatcher.super.apply((Character) x0);
            }

            public boolean matches(char c) {
                return c == match;
            }

            public String replaceFrom(CharSequence sequence, char replacement) {
                return sequence.toString().replace(match, replacement);
            }

            public CharMatcher and(CharMatcher other) {
                return other.matches(match) ? this : NONE;
            }

            public CharMatcher or(CharMatcher other) {
                return other.matches(match) ? other : CharMatcher.super.or(other);
            }

            public CharMatcher negate() {
                return isNot(match);
            }

            /* access modifiers changed from: package-private */
            public void setBits(LookupTable table) {
                table.set(match);
            }

            public CharMatcher precomputed() {
                return this;
            }
        };
    }

    public static CharMatcher isNot(final char match) {
        return new CharMatcher() {
            public /* bridge */ /* synthetic */ boolean apply(Object x0) {
                return CharMatcher.super.apply((Character) x0);
            }

            public boolean matches(char c) {
                return c != match;
            }

            public CharMatcher and(CharMatcher other) {
                return other.matches(match) ? CharMatcher.super.and(other) : other;
            }

            public CharMatcher or(CharMatcher other) {
                return other.matches(match) ? ANY : this;
            }

            public CharMatcher negate() {
                return is(match);
            }
        };
    }

    public static CharMatcher anyOf(CharSequence sequence) {
        switch (sequence.length()) {
            case 0:
                return NONE;
            case 1:
                return is(sequence.charAt(0));
            case 2:
                final char match1 = sequence.charAt(0);
                final char match2 = sequence.charAt(1);
                return new CharMatcher() {
                    public /* bridge */ /* synthetic */ boolean apply(Object x0) {
                        return CharMatcher.super.apply((Character) x0);
                    }

                    public boolean matches(char c) {
                        return c == match1 || c == match2;
                    }

                    /* access modifiers changed from: package-private */
                    public void setBits(LookupTable table) {
                        table.set(match1);
                        table.set(match2);
                    }

                    public CharMatcher precomputed() {
                        return this;
                    }
                };
            default:
                final char[] chars = sequence.toString().toCharArray();
                Arrays.sort(chars);
                return new CharMatcher() {
                    public /* bridge */ /* synthetic */ boolean apply(Object x0) {
                        return CharMatcher.super.apply((Character) x0);
                    }

                    public boolean matches(char c) {
                        return Arrays.binarySearch(chars, c) >= 0;
                    }

                    /* access modifiers changed from: package-private */
                    public void setBits(LookupTable table) {
                        for (char c : chars) {
                            table.set(c);
                        }
                    }
                };
        }
    }

    public static CharMatcher noneOf(CharSequence sequence) {
        return anyOf(sequence).negate();
    }

    public static CharMatcher inRange(final char startInclusive, final char endInclusive) {
        Preconditions.checkArgument(endInclusive >= startInclusive);
        return new CharMatcher() {
            public /* bridge */ /* synthetic */ boolean apply(Object x0) {
                return CharMatcher.super.apply((Character) x0);
            }

            public boolean matches(char c) {
                return startInclusive <= c && c <= endInclusive;
            }

            /* access modifiers changed from: package-private */
            public void setBits(LookupTable table) {
                char c = startInclusive;
                while (true) {
                    table.set(c);
                    char c2 = (char) (c + 1);
                    if (c != endInclusive) {
                        c = c2;
                    } else {
                        return;
                    }
                }
            }

            public CharMatcher precomputed() {
                return this;
            }
        };
    }

    public static CharMatcher forPredicate(final Predicate<? super Character> predicate) {
        Preconditions.checkNotNull(predicate);
        if (predicate instanceof CharMatcher) {
            return (CharMatcher) predicate;
        }
        return new CharMatcher() {
            public boolean matches(char c) {
                return predicate.apply(Character.valueOf(c));
            }

            public boolean apply(Character character) {
                return predicate.apply(Preconditions.checkNotNull(character));
            }
        };
    }

    public CharMatcher negate() {
        return new CharMatcher() {
            public /* bridge */ /* synthetic */ boolean apply(Object x0) {
                return CharMatcher.super.apply((Character) x0);
            }

            public boolean matches(char c) {
                return !this.matches(c);
            }

            public boolean matchesAllOf(CharSequence sequence) {
                return this.matchesNoneOf(sequence);
            }

            public boolean matchesNoneOf(CharSequence sequence) {
                return this.matchesAllOf(sequence);
            }

            public int countIn(CharSequence sequence) {
                return sequence.length() - this.countIn(sequence);
            }

            public CharMatcher negate() {
                return this;
            }
        };
    }

    public CharMatcher and(CharMatcher other) {
        return new And(Arrays.asList(this, (CharMatcher) Preconditions.checkNotNull(other)));
    }

    private static class And extends CharMatcher {
        List<CharMatcher> components;

        public /* bridge */ /* synthetic */ boolean apply(Object x0) {
            return CharMatcher.super.apply((Character) x0);
        }

        And(List<CharMatcher> components2) {
            this.components = components2;
        }

        public boolean matches(char c) {
            for (CharMatcher matcher : this.components) {
                if (!matcher.matches(c)) {
                    return false;
                }
            }
            return true;
        }

        public CharMatcher and(CharMatcher other) {
            List<CharMatcher> newComponents = new ArrayList<>(this.components);
            newComponents.add(Preconditions.checkNotNull(other));
            return new And(newComponents);
        }
    }

    public CharMatcher or(CharMatcher other) {
        return new Or(Arrays.asList(this, (CharMatcher) Preconditions.checkNotNull(other)));
    }

    private static class Or extends CharMatcher {
        List<CharMatcher> components;

        public /* bridge */ /* synthetic */ boolean apply(Object x0) {
            return CharMatcher.super.apply((Character) x0);
        }

        Or(List<CharMatcher> components2) {
            this.components = components2;
        }

        public boolean matches(char c) {
            for (CharMatcher matcher : this.components) {
                if (matcher.matches(c)) {
                    return true;
                }
            }
            return false;
        }

        public CharMatcher or(CharMatcher other) {
            List<CharMatcher> newComponents = new ArrayList<>(this.components);
            newComponents.add(Preconditions.checkNotNull(other));
            return new Or(newComponents);
        }

        /* access modifiers changed from: package-private */
        public void setBits(LookupTable table) {
            for (CharMatcher matcher : this.components) {
                matcher.setBits(table);
            }
        }
    }

    public CharMatcher precomputed() {
        return Platform.precomputeCharMatcher(this);
    }

    /* access modifiers changed from: package-private */
    public CharMatcher precomputedInternal() {
        final LookupTable table = new LookupTable();
        setBits(table);
        return new CharMatcher() {
            public /* bridge */ /* synthetic */ boolean apply(Object x0) {
                return CharMatcher.super.apply((Character) x0);
            }

            public boolean matches(char c) {
                return table.get(c);
            }

            public CharMatcher precomputed() {
                return this;
            }
        };
    }

    /* access modifiers changed from: package-private */
    public void setBits(LookupTable table) {
        char c = 0;
        while (true) {
            if (matches(c)) {
                table.set(c);
            }
            char c2 = (char) (c + 1);
            if (c != 65535) {
                c = c2;
            } else {
                return;
            }
        }
    }

    private static final class LookupTable {
        int[] data;

        private LookupTable() {
            this.data = new int[Opcodes.ACC_STRICT];
        }

        /* access modifiers changed from: package-private */
        public void set(char index) {
            int[] iArr = this.data;
            int i = index >> 5;
            iArr[i] = iArr[i] | (1 << index);
        }

        /* access modifiers changed from: package-private */
        public boolean get(char index) {
            return (this.data[index >> 5] & (1 << index)) != 0;
        }
    }

    public boolean matchesAnyOf(CharSequence sequence) {
        return !matchesNoneOf(sequence);
    }

    public boolean matchesAllOf(CharSequence sequence) {
        for (int i = sequence.length() - 1; i >= 0; i--) {
            if (!matches(sequence.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public boolean matchesNoneOf(CharSequence sequence) {
        return indexIn(sequence) == -1;
    }

    public int indexIn(CharSequence sequence) {
        int length = sequence.length();
        for (int i = 0; i < length; i++) {
            if (matches(sequence.charAt(i))) {
                return i;
            }
        }
        return -1;
    }

    public int indexIn(CharSequence sequence, int start) {
        int length = sequence.length();
        Preconditions.checkPositionIndex(start, length);
        for (int i = start; i < length; i++) {
            if (matches(sequence.charAt(i))) {
                return i;
            }
        }
        return -1;
    }

    public int lastIndexIn(CharSequence sequence) {
        for (int i = sequence.length() - 1; i >= 0; i--) {
            if (matches(sequence.charAt(i))) {
                return i;
            }
        }
        return -1;
    }

    public int countIn(CharSequence sequence) {
        int count = 0;
        for (int i = 0; i < sequence.length(); i++) {
            if (matches(sequence.charAt(i))) {
                count++;
            }
        }
        return count;
    }

    public String removeFrom(CharSequence sequence) {
        String string = sequence.toString();
        int pos = indexIn(string);
        if (pos == -1) {
            return string;
        }
        char[] chars = string.toCharArray();
        int spread = 1;
        while (true) {
            while (true) {
                pos++;
                if (pos == chars.length) {
                    return new String(chars, 0, pos - spread);
                }
                if (matches(chars[pos])) {
                    break;
                }
                chars[pos - spread] = chars[pos];
            }
            spread++;
        }
    }

    public String retainFrom(CharSequence sequence) {
        return negate().removeFrom(sequence);
    }

    public String replaceFrom(CharSequence sequence, char replacement) {
        String string = sequence.toString();
        int pos = indexIn(string);
        if (pos == -1) {
            return string;
        }
        char[] chars = string.toCharArray();
        chars[pos] = replacement;
        for (int i = pos + 1; i < chars.length; i++) {
            if (matches(chars[i])) {
                chars[i] = replacement;
            }
        }
        return new String(chars);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.StringBuilder}
     arg types: [java.lang.String, int, int]
     candidates:
      ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.Appendable throws java.io.IOException}
      ClspMth{java.lang.StringBuilder.append(char[], int, int):java.lang.StringBuilder}
      ClspMth{java.lang.Appendable.append(java.lang.CharSequence, int, int):java.lang.Appendable throws java.io.IOException}
      ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.StringBuilder} */
    public String replaceFrom(CharSequence sequence, CharSequence replacement) {
        int replacementLen = replacement.length();
        if (replacementLen == 0) {
            return removeFrom(sequence);
        }
        if (replacementLen == 1) {
            return replaceFrom(sequence, replacement.charAt(0));
        }
        String string = sequence.toString();
        int pos = indexIn(string);
        if (pos == -1) {
            return string;
        }
        int len = string.length();
        StringBuilder buf = new StringBuilder(((len * 3) / 2) + 16);
        int oldpos = 0;
        do {
            buf.append((CharSequence) string, oldpos, pos);
            buf.append(replacement);
            oldpos = pos + 1;
            pos = indexIn(string, oldpos);
        } while (pos != -1);
        buf.append((CharSequence) string, oldpos, len);
        return buf.toString();
    }

    public String trimFrom(CharSequence sequence) {
        int len = sequence.length();
        int first = 0;
        while (first < len && matches(sequence.charAt(first))) {
            first++;
        }
        int last = len - 1;
        while (last > first && matches(sequence.charAt(last))) {
            last--;
        }
        return sequence.subSequence(first, last + 1).toString();
    }

    public String trimLeadingFrom(CharSequence sequence) {
        int len = sequence.length();
        int first = 0;
        while (first < len && matches(sequence.charAt(first))) {
            first++;
        }
        return sequence.subSequence(first, len).toString();
    }

    public String trimTrailingFrom(CharSequence sequence) {
        int last = sequence.length() - 1;
        while (last >= 0 && matches(sequence.charAt(last))) {
            last--;
        }
        return sequence.subSequence(0, last + 1).toString();
    }

    public String collapseFrom(CharSequence sequence, char replacement) {
        int first = indexIn(sequence);
        if (first == -1) {
            return sequence.toString();
        }
        StringBuilder builder = new StringBuilder(sequence.length()).append(sequence.subSequence(0, first)).append(replacement);
        boolean in = true;
        for (int i = first + 1; i < sequence.length(); i++) {
            char c = sequence.charAt(i);
            if (!apply(Character.valueOf(c))) {
                builder.append(c);
                in = false;
            } else if (!in) {
                builder.append(replacement);
                in = true;
            }
        }
        return builder.toString();
    }

    public String trimAndCollapseFrom(CharSequence sequence, char replacement) {
        int first = negate().indexIn(sequence);
        if (first == -1) {
            return "";
        }
        StringBuilder builder = new StringBuilder(sequence.length());
        boolean inMatchingGroup = false;
        for (int i = first; i < sequence.length(); i++) {
            char c = sequence.charAt(i);
            if (apply(Character.valueOf(c))) {
                inMatchingGroup = true;
            } else {
                if (inMatchingGroup) {
                    builder.append(replacement);
                    inMatchingGroup = false;
                }
                builder.append(c);
            }
        }
        return builder.toString();
    }

    public boolean apply(Character character) {
        return matches(character.charValue());
    }
}
