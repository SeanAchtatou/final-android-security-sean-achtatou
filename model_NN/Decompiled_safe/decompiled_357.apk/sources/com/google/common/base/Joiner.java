package com.google.common.base;

import com.google.common.annotations.GwtCompatible;
import java.io.IOException;
import java.util.AbstractList;
import java.util.Arrays;
import java.util.Map;
import javax.annotation.Nullable;

@GwtCompatible
public class Joiner {
    /* access modifiers changed from: private */
    public final String separator;

    public static Joiner on(String separator2) {
        return new Joiner(separator2);
    }

    public static Joiner on(char separator2) {
        return new Joiner(String.valueOf(separator2));
    }

    private Joiner(String separator2) {
        this.separator = (String) Preconditions.checkNotNull(separator2);
    }

    private Joiner(Joiner prototype) {
        this.separator = prototype.separator;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public <A extends java.lang.Appendable> A appendTo(A r3, java.lang.Iterable<?> r4) throws java.io.IOException {
        /*
            r2 = this;
            com.google.common.base.Preconditions.checkNotNull(r3)
            java.util.Iterator r0 = r4.iterator()
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x002f
            java.lang.Object r1 = r0.next()
            java.lang.CharSequence r1 = r2.toString(r1)
            r3.append(r1)
        L_0x0018:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x002f
            java.lang.String r1 = r2.separator
            r3.append(r1)
            java.lang.Object r1 = r0.next()
            java.lang.CharSequence r1 = r2.toString(r1)
            r3.append(r1)
            goto L_0x0018
        L_0x002f:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.common.base.Joiner.appendTo(java.lang.Appendable, java.lang.Iterable):java.lang.Appendable");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.base.Joiner.appendTo(java.lang.Appendable, java.lang.Iterable<?>):A
     arg types: [A, java.util.List]
     candidates:
      com.google.common.base.Joiner.appendTo(java.lang.Appendable, java.lang.Object[]):A
      com.google.common.base.Joiner.appendTo(java.lang.StringBuilder, java.lang.Iterable<?>):java.lang.StringBuilder
      com.google.common.base.Joiner.appendTo(java.lang.StringBuilder, java.lang.Object[]):java.lang.StringBuilder
      com.google.common.base.Joiner.appendTo(java.lang.Appendable, java.lang.Iterable<?>):A */
    public final <A extends Appendable> A appendTo(A appendable, Object[] parts) throws IOException {
        return appendTo((Appendable) appendable, (Iterable<?>) Arrays.asList(parts));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.base.Joiner.appendTo(java.lang.Appendable, java.lang.Iterable<?>):A
     arg types: [A, java.lang.Iterable<java.lang.Object>]
     candidates:
      com.google.common.base.Joiner.appendTo(java.lang.Appendable, java.lang.Object[]):A
      com.google.common.base.Joiner.appendTo(java.lang.StringBuilder, java.lang.Iterable<?>):java.lang.StringBuilder
      com.google.common.base.Joiner.appendTo(java.lang.StringBuilder, java.lang.Object[]):java.lang.StringBuilder
      com.google.common.base.Joiner.appendTo(java.lang.Appendable, java.lang.Iterable<?>):A */
    public final <A extends Appendable> A appendTo(A appendable, @Nullable Object first, @Nullable Object second, Object... rest) throws IOException {
        return appendTo((Appendable) appendable, (Iterable<?>) iterable(first, second, rest));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.base.Joiner.appendTo(java.lang.Appendable, java.lang.Iterable<?>):A
     arg types: [java.lang.StringBuilder, java.lang.Iterable<?>]
     candidates:
      com.google.common.base.Joiner.appendTo(java.lang.Appendable, java.lang.Object[]):A
      com.google.common.base.Joiner.appendTo(java.lang.StringBuilder, java.lang.Iterable<?>):java.lang.StringBuilder
      com.google.common.base.Joiner.appendTo(java.lang.StringBuilder, java.lang.Object[]):java.lang.StringBuilder
      com.google.common.base.Joiner.appendTo(java.lang.Appendable, java.lang.Iterable<?>):A */
    public final StringBuilder appendTo(StringBuilder builder, Iterable<?> parts) {
        try {
            appendTo((Appendable) builder, parts);
            return builder;
        } catch (IOException impossible) {
            throw new AssertionError(impossible);
        }
    }

    public final StringBuilder appendTo(StringBuilder builder, Object[] parts) {
        return appendTo(builder, (Iterable<?>) Arrays.asList(parts));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.common.base.Joiner.appendTo(java.lang.StringBuilder, java.lang.Iterable<?>):java.lang.StringBuilder
     arg types: [java.lang.StringBuilder, java.lang.Iterable<java.lang.Object>]
     candidates:
      com.google.common.base.Joiner.appendTo(java.lang.Appendable, java.lang.Iterable<?>):A
      com.google.common.base.Joiner.appendTo(java.lang.Appendable, java.lang.Object[]):A
      com.google.common.base.Joiner.appendTo(java.lang.StringBuilder, java.lang.Object[]):java.lang.StringBuilder
      com.google.common.base.Joiner.appendTo(java.lang.StringBuilder, java.lang.Iterable<?>):java.lang.StringBuilder */
    public final StringBuilder appendTo(StringBuilder builder, @Nullable Object first, @Nullable Object second, Object... rest) {
        return appendTo(builder, (Iterable<?>) iterable(first, second, rest));
    }

    public final String join(Iterable<?> parts) {
        return appendTo(new StringBuilder(), parts).toString();
    }

    public final String join(Object[] parts) {
        return join(Arrays.asList(parts));
    }

    public final String join(@Nullable Object first, @Nullable Object second, Object... rest) {
        return join(iterable(first, second, rest));
    }

    public Joiner useForNull(final String nullText) {
        Preconditions.checkNotNull(nullText);
        return new Joiner(this) {
            /* access modifiers changed from: package-private */
            public CharSequence toString(Object part) {
                return part == null ? nullText : Joiner.this.toString(part);
            }

            public Joiner useForNull(String nullText) {
                Preconditions.checkNotNull(nullText);
                throw new UnsupportedOperationException("already specified useForNull");
            }

            public Joiner skipNulls() {
                throw new UnsupportedOperationException("already specified useForNull");
            }
        };
    }

    public Joiner skipNulls() {
        return new Joiner(this) {
            /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
                jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
                	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
                	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
                	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
                	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
                	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
                	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
                */
            public <A extends java.lang.Appendable> A appendTo(A r4, java.lang.Iterable<?> r5) throws java.io.IOException {
                /*
                    r3 = this;
                    java.lang.String r2 = "appendable"
                    com.google.common.base.Preconditions.checkNotNull(r4, r2)
                    java.lang.String r2 = "parts"
                    com.google.common.base.Preconditions.checkNotNull(r5, r2)
                    java.util.Iterator r0 = r5.iterator()
                L_0x000e:
                    boolean r2 = r0.hasNext()
                    if (r2 == 0) goto L_0x0023
                    java.lang.Object r1 = r0.next()
                    if (r1 == 0) goto L_0x000e
                    com.google.common.base.Joiner r2 = com.google.common.base.Joiner.this
                    java.lang.CharSequence r2 = r2.toString(r1)
                    r4.append(r2)
                L_0x0023:
                    boolean r2 = r0.hasNext()
                    if (r2 == 0) goto L_0x0042
                    java.lang.Object r1 = r0.next()
                    if (r1 == 0) goto L_0x0023
                    com.google.common.base.Joiner r2 = com.google.common.base.Joiner.this
                    java.lang.String r2 = r2.separator
                    r4.append(r2)
                    com.google.common.base.Joiner r2 = com.google.common.base.Joiner.this
                    java.lang.CharSequence r2 = r2.toString(r1)
                    r4.append(r2)
                    goto L_0x0023
                L_0x0042:
                    return r4
                */
                throw new UnsupportedOperationException("Method not decompiled: com.google.common.base.Joiner.AnonymousClass2.appendTo(java.lang.Appendable, java.lang.Iterable):java.lang.Appendable");
            }

            public Joiner useForNull(String nullText) {
                Preconditions.checkNotNull(nullText);
                throw new UnsupportedOperationException("already specified skipNulls");
            }

            public MapJoiner withKeyValueSeparator(String kvs) {
                Preconditions.checkNotNull(kvs);
                throw new UnsupportedOperationException("can't use .skipNulls() with maps");
            }
        };
    }

    public MapJoiner withKeyValueSeparator(String keyValueSeparator) {
        return new MapJoiner(keyValueSeparator);
    }

    public static final class MapJoiner {
        private final Joiner joiner;
        private final String keyValueSeparator;

        private MapJoiner(Joiner joiner2, String keyValueSeparator2) {
            this.joiner = joiner2;
            this.keyValueSeparator = (String) Preconditions.checkNotNull(keyValueSeparator2);
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public <A extends java.lang.Appendable> A appendTo(A r6, java.util.Map<?, ?> r7) throws java.io.IOException {
            /*
                r5 = this;
                com.google.common.base.Preconditions.checkNotNull(r6)
                java.util.Set r3 = r7.entrySet()
                java.util.Iterator r2 = r3.iterator()
                boolean r3 = r2.hasNext()
                if (r3 == 0) goto L_0x006b
                java.lang.Object r1 = r2.next()
                java.util.Map$Entry r1 = (java.util.Map.Entry) r1
                com.google.common.base.Joiner r3 = r5.joiner
                java.lang.Object r4 = r1.getKey()
                java.lang.CharSequence r3 = r3.toString(r4)
                r6.append(r3)
                java.lang.String r3 = r5.keyValueSeparator
                r6.append(r3)
                com.google.common.base.Joiner r3 = r5.joiner
                java.lang.Object r4 = r1.getValue()
                java.lang.CharSequence r3 = r3.toString(r4)
                r6.append(r3)
            L_0x0036:
                boolean r3 = r2.hasNext()
                if (r3 == 0) goto L_0x006b
                com.google.common.base.Joiner r3 = r5.joiner
                java.lang.String r3 = r3.separator
                r6.append(r3)
                java.lang.Object r0 = r2.next()
                java.util.Map$Entry r0 = (java.util.Map.Entry) r0
                com.google.common.base.Joiner r3 = r5.joiner
                java.lang.Object r4 = r0.getKey()
                java.lang.CharSequence r3 = r3.toString(r4)
                r6.append(r3)
                java.lang.String r3 = r5.keyValueSeparator
                r6.append(r3)
                com.google.common.base.Joiner r3 = r5.joiner
                java.lang.Object r4 = r0.getValue()
                java.lang.CharSequence r3 = r3.toString(r4)
                r6.append(r3)
                goto L_0x0036
            L_0x006b:
                return r6
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.common.base.Joiner.MapJoiner.appendTo(java.lang.Appendable, java.util.Map):java.lang.Appendable");
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.common.base.Joiner.MapJoiner.appendTo(java.lang.Appendable, java.util.Map<?, ?>):A
         arg types: [java.lang.StringBuilder, java.util.Map<?, ?>]
         candidates:
          com.google.common.base.Joiner.MapJoiner.appendTo(java.lang.StringBuilder, java.util.Map<?, ?>):java.lang.StringBuilder
          com.google.common.base.Joiner.MapJoiner.appendTo(java.lang.Appendable, java.util.Map<?, ?>):A */
        public StringBuilder appendTo(StringBuilder builder, Map<?, ?> map) {
            try {
                appendTo((Appendable) builder, map);
                return builder;
            } catch (IOException impossible) {
                throw new AssertionError(impossible);
            }
        }

        public String join(Map<?, ?> map) {
            return appendTo(new StringBuilder(), map).toString();
        }

        public MapJoiner useForNull(String nullText) {
            return new MapJoiner(this.joiner.useForNull(nullText), this.keyValueSeparator);
        }
    }

    /* access modifiers changed from: package-private */
    public CharSequence toString(Object part) {
        return part instanceof CharSequence ? (CharSequence) part : part.toString();
    }

    private static Iterable<Object> iterable(final Object first, final Object second, final Object[] rest) {
        Preconditions.checkNotNull(rest);
        return new AbstractList<Object>() {
            public int size() {
                return rest.length + 2;
            }

            public Object get(int index) {
                switch (index) {
                    case 0:
                        return first;
                    case 1:
                        return second;
                    default:
                        return rest[index - 2];
                }
            }
        };
    }
}
