package com.google.common.base;

import com.google.common.annotations.Beta;
import com.google.common.annotations.GwtCompatible;
import javax.annotation.Nullable;

@GwtCompatible
@Beta
public interface Equivalence<T> {
    boolean equivalent(@Nullable T t, @Nullable T t2);

    int hash(@Nullable T t);
}
