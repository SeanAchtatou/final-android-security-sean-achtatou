package com.google.common.base;

import com.google.common.annotations.GwtCompatible;
import java.util.Arrays;
import javax.annotation.Nullable;
import org.codehaus.jackson.org.objectweb.asm.signature.SignatureVisitor;

@GwtCompatible
public final class Objects {
    private Objects() {
    }

    public static boolean equal(@Nullable Object a, @Nullable Object b) {
        return a == b || (a != null && a.equals(b));
    }

    public static int hashCode(Object... objects) {
        return Arrays.hashCode(objects);
    }

    public static ToStringHelper toStringHelper(Object self) {
        return new ToStringHelper(simpleName(self.getClass()));
    }

    public static ToStringHelper toStringHelper(Class<?> clazz) {
        return new ToStringHelper(simpleName(clazz));
    }

    public static ToStringHelper toStringHelper(String className) {
        return new ToStringHelper(className);
    }

    private static String simpleName(Class<?> clazz) {
        String name = clazz.getName();
        int start = name.lastIndexOf(36);
        if (start == -1) {
            start = name.lastIndexOf(46);
        }
        return name.substring(start + 1);
    }

    public static <T> T firstNonNull(@Nullable T first, @Nullable T second) {
        return first != null ? first : Preconditions.checkNotNull(second);
    }

    public static final class ToStringHelper {
        private final StringBuilder builder;
        private String separator;

        private ToStringHelper(String className) {
            this.separator = "";
            this.builder = new StringBuilder(32).append((String) Preconditions.checkNotNull(className)).append('{');
        }

        public ToStringHelper add(String name, @Nullable Object value) {
            this.builder.append(this.separator).append((String) Preconditions.checkNotNull(name)).append((char) SignatureVisitor.INSTANCEOF).append(value);
            this.separator = ", ";
            return this;
        }

        public ToStringHelper addValue(@Nullable Object value) {
            this.builder.append(this.separator).append(value);
            this.separator = ", ";
            return this;
        }

        public String toString() {
            return this.builder.append('}').toString();
        }
    }
}
