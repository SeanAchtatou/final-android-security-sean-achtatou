package com.google.iap;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.google.iap.Consts;
import com.urbanairship.Logger;

public class BillingReceiver extends BroadcastReceiver {
    private void checkResponseCode(Context context, long j, int i) {
        Intent intent = new Intent(Consts.ACTION_RESPONSE_CODE);
        intent.setClass(context, BillingService.class);
        intent.putExtra(Consts.INAPP_REQUEST_ID, j);
        intent.putExtra(Consts.INAPP_RESPONSE_CODE, i);
        context.startService(intent);
    }

    private void notify(Context context, String str) {
        Intent intent = new Intent(Consts.ACTION_GET_PURCHASE_INFORMATION);
        intent.setClass(context, BillingService.class);
        intent.putExtra(Consts.NOTIFICATION_ID, str);
        context.startService(intent);
    }

    private void purchaseStateChanged(Context context, String str, String str2) {
        Intent intent = new Intent(Consts.ACTION_PURCHASE_STATE_CHANGED);
        intent.setClass(context, BillingService.class);
        intent.putExtra(Consts.INAPP_SIGNED_DATA, str);
        intent.putExtra(Consts.INAPP_SIGNATURE, str2);
        context.startService(intent);
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Logger.debug("Recieved: " + action);
        if (Consts.ACTION_PURCHASE_STATE_CHANGED.equals(action)) {
            String stringExtra = intent.getStringExtra(Consts.INAPP_SIGNED_DATA);
            String stringExtra2 = intent.getStringExtra(Consts.INAPP_SIGNATURE);
            if (stringExtra != null && stringExtra2 != null) {
                purchaseStateChanged(context, stringExtra, stringExtra2);
            }
        } else if (Consts.ACTION_NOTIFY.equals(action)) {
            notify(context, intent.getStringExtra(Consts.NOTIFICATION_ID));
        } else if (Consts.ACTION_RESPONSE_CODE.equals(action)) {
            checkResponseCode(context, intent.getLongExtra(Consts.INAPP_REQUEST_ID, -1), intent.getIntExtra(Consts.INAPP_RESPONSE_CODE, Consts.ResponseCode.RESULT_ERROR.ordinal()));
        } else {
            Logger.warn("unexpected action: " + action);
        }
    }
}
