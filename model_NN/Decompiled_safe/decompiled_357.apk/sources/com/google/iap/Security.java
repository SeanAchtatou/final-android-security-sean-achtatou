package com.google.iap;

import java.security.SecureRandom;
import java.util.HashSet;

public class Security {
    private static final SecureRandom RANDOM = new SecureRandom();
    private static HashSet<Long> sKnownNonces = new HashSet<>();

    public static long generateNonce() {
        long nextLong = RANDOM.nextLong();
        sKnownNonces.add(Long.valueOf(nextLong));
        return nextLong;
    }

    public static boolean isNonceKnown(long j) {
        return sKnownNonces.contains(Long.valueOf(j));
    }

    public static void removeNonce(long j) {
        sKnownNonces.remove(Long.valueOf(j));
    }
}
