package com.google.iap;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import com.android.vending.billing.IMarketBillingService;
import com.google.iap.Consts;
import com.urbanairship.Logger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BillingService extends Service implements ServiceConnection {
    /* access modifiers changed from: private */
    public static LinkedList<BillingRequest> mPendingRequests = new LinkedList<>();
    /* access modifiers changed from: private */
    public static HashMap<Long, BillingRequest> mSentRequests = new HashMap<>();
    /* access modifiers changed from: private */
    public static IMarketBillingService mService;

    abstract class BillingRequest {
        protected long mRequestId;
        private final int mStartId;

        public BillingRequest(int i) {
            this.mStartId = i;
        }

        public int getStartId() {
            return this.mStartId;
        }

        /* access modifiers changed from: protected */
        public void logResponseCode(String str, Bundle bundle) {
            Consts.ResponseCode.valueOf(bundle.getInt(Consts.BILLING_RESPONSE_RESPONSE_CODE));
        }

        /* access modifiers changed from: protected */
        public Bundle makeRequestBundle(String str) {
            Bundle bundle = new Bundle();
            bundle.putString(Consts.BILLING_REQUEST_METHOD, str);
            bundle.putInt(Consts.BILLING_REQUEST_API_VERSION, 1);
            bundle.putString(Consts.BILLING_REQUEST_PACKAGE_NAME, BillingService.this.getPackageName());
            return bundle;
        }

        /* access modifiers changed from: protected */
        public void onRemoteException(RemoteException remoteException) {
            Logger.warn("remote billing service crashed");
            IMarketBillingService unused = BillingService.mService = null;
        }

        /* access modifiers changed from: protected */
        public void responseCodeReceived(Consts.ResponseCode responseCode) {
        }

        /* access modifiers changed from: protected */
        public abstract long run() throws RemoteException;

        public boolean runIfConnected() {
            if (BillingService.mService != null) {
                try {
                    this.mRequestId = run();
                    if (this.mRequestId >= 0) {
                        BillingService.mSentRequests.put(Long.valueOf(this.mRequestId), this);
                    }
                    return true;
                } catch (RemoteException e) {
                    onRemoteException(e);
                }
            }
            return false;
        }

        public boolean runRequest() {
            if (runIfConnected()) {
                return true;
            }
            if (!BillingService.this.bindToMarketBillingService()) {
                return false;
            }
            BillingService.mPendingRequests.add(this);
            return true;
        }
    }

    class CheckBillingSupported extends BillingRequest {
        public CheckBillingSupported() {
            super(-1);
        }

        /* access modifiers changed from: protected */
        public long run() throws RemoteException {
            ResponseHandler.checkBillingSupportedResponse(BillingService.mService.sendBillingRequest(makeRequestBundle("CHECK_BILLING_SUPPORTED")).getInt(Consts.BILLING_RESPONSE_RESPONSE_CODE) == Consts.ResponseCode.RESULT_OK.ordinal());
            return Consts.BILLING_RESPONSE_INVALID_REQUEST_ID;
        }
    }

    class ConfirmNotifications extends BillingRequest {
        final String[] mNotifyIds;

        public ConfirmNotifications(int i, String[] strArr) {
            super(i);
            this.mNotifyIds = strArr;
        }

        /* access modifiers changed from: protected */
        public long run() throws RemoteException {
            Bundle makeRequestBundle = makeRequestBundle("CONFIRM_NOTIFICATIONS");
            makeRequestBundle.putStringArray(Consts.BILLING_REQUEST_NOTIFY_IDS, this.mNotifyIds);
            Bundle sendBillingRequest = BillingService.mService.sendBillingRequest(makeRequestBundle);
            logResponseCode("confirmNotifications", sendBillingRequest);
            return sendBillingRequest.getLong(Consts.BILLING_RESPONSE_REQUEST_ID, Consts.BILLING_RESPONSE_INVALID_REQUEST_ID);
        }
    }

    class GetPurchaseInformation extends BillingRequest {
        long mNonce;
        final String[] mNotifyIds;

        public GetPurchaseInformation(int i, String[] strArr) {
            super(i);
            this.mNotifyIds = strArr;
        }

        /* access modifiers changed from: protected */
        public void onRemoteException(RemoteException remoteException) {
            super.onRemoteException(remoteException);
            Security.removeNonce(this.mNonce);
        }

        /* access modifiers changed from: protected */
        public long run() throws RemoteException {
            this.mNonce = Security.generateNonce();
            Bundle makeRequestBundle = makeRequestBundle("GET_PURCHASE_INFORMATION");
            makeRequestBundle.putLong(Consts.BILLING_REQUEST_NONCE, this.mNonce);
            makeRequestBundle.putStringArray(Consts.BILLING_REQUEST_NOTIFY_IDS, this.mNotifyIds);
            Bundle sendBillingRequest = BillingService.mService.sendBillingRequest(makeRequestBundle);
            logResponseCode("getPurchaseInformation", sendBillingRequest);
            return sendBillingRequest.getLong(Consts.BILLING_RESPONSE_REQUEST_ID, Consts.BILLING_RESPONSE_INVALID_REQUEST_ID);
        }
    }

    public class Purchase {
        public String developerPayload;
        public String notificationId;
        public String orderId;
        public String productId;
        public Consts.PurchaseState purchaseState;
        public long purchaseTime;

        public Purchase(Consts.PurchaseState purchaseState2, String str, String str2, String str3, long j, String str4) {
            this.purchaseState = purchaseState2;
            this.notificationId = str;
            this.productId = str2;
            this.orderId = str3;
            this.purchaseTime = j;
            this.developerPayload = str4;
        }
    }

    public class RequestPurchase extends BillingRequest {
        public final Activity activity;
        public final String mDeveloperPayload;
        public final String mProductId;

        public RequestPurchase(BillingService billingService, Activity activity2, String str) {
            this(activity2, str, null);
        }

        public RequestPurchase(Activity activity2, String str, String str2) {
            super(-1);
            this.mProductId = str;
            this.mDeveloperPayload = str2;
            this.activity = activity2;
            Logger.debug("Request Purchase for: " + str);
        }

        public /* bridge */ /* synthetic */ int getStartId() {
            return super.getStartId();
        }

        /* access modifiers changed from: protected */
        public void responseCodeReceived(Consts.ResponseCode responseCode) {
            ResponseHandler.responseCodeReceived(BillingService.this, this, responseCode);
        }

        /* access modifiers changed from: protected */
        public long run() throws RemoteException {
            Bundle makeRequestBundle = makeRequestBundle("REQUEST_PURCHASE");
            makeRequestBundle.putString(Consts.BILLING_REQUEST_ITEM_ID, this.mProductId);
            if (this.mDeveloperPayload != null) {
                makeRequestBundle.putString(Consts.BILLING_REQUEST_DEVELOPER_PAYLOAD, this.mDeveloperPayload);
            }
            Bundle sendBillingRequest = BillingService.mService.sendBillingRequest(makeRequestBundle);
            PendingIntent pendingIntent = (PendingIntent) sendBillingRequest.getParcelable(Consts.BILLING_RESPONSE_PURCHASE_INTENT);
            if (pendingIntent == null) {
                Logger.error("Error with requestPurchase");
                return Consts.BILLING_RESPONSE_INVALID_REQUEST_ID;
            }
            ResponseHandler.buyPageIntentResponse(this.activity, pendingIntent, new Intent());
            return sendBillingRequest.getLong(Consts.BILLING_RESPONSE_REQUEST_ID, Consts.BILLING_RESPONSE_INVALID_REQUEST_ID);
        }

        public /* bridge */ /* synthetic */ boolean runIfConnected() {
            return super.runIfConnected();
        }

        public /* bridge */ /* synthetic */ boolean runRequest() {
            return super.runRequest();
        }
    }

    public class RestoreTransactions extends BillingRequest {
        long mNonce;

        public RestoreTransactions() {
            super(-1);
        }

        public /* bridge */ /* synthetic */ int getStartId() {
            return super.getStartId();
        }

        /* access modifiers changed from: protected */
        public void onRemoteException(RemoteException remoteException) {
            super.onRemoteException(remoteException);
            Security.removeNonce(this.mNonce);
        }

        /* access modifiers changed from: protected */
        public void responseCodeReceived(Consts.ResponseCode responseCode) {
            ResponseHandler.responseCodeReceived(BillingService.this, this, responseCode);
        }

        /* access modifiers changed from: protected */
        public long run() throws RemoteException {
            this.mNonce = Security.generateNonce();
            Bundle makeRequestBundle = makeRequestBundle("RESTORE_TRANSACTIONS");
            makeRequestBundle.putLong(Consts.BILLING_REQUEST_NONCE, this.mNonce);
            Bundle sendBillingRequest = BillingService.mService.sendBillingRequest(makeRequestBundle);
            logResponseCode("restoreTransactions", sendBillingRequest);
            return sendBillingRequest.getLong(Consts.BILLING_RESPONSE_REQUEST_ID, Consts.BILLING_RESPONSE_INVALID_REQUEST_ID);
        }

        public /* bridge */ /* synthetic */ boolean runIfConnected() {
            return super.runIfConnected();
        }

        public /* bridge */ /* synthetic */ boolean runRequest() {
            return super.runRequest();
        }
    }

    /* access modifiers changed from: private */
    public boolean bindToMarketBillingService() {
        try {
            if (bindService(new Intent(Consts.MARKET_BILLING_SERVICE_ACTION), this, 1)) {
                return true;
            }
            Logger.error("Could not bind to service.");
            return false;
        } catch (SecurityException e) {
            Logger.error("Security exception: " + e);
        }
    }

    private void checkResponseCode(long j, Consts.ResponseCode responseCode) {
        BillingRequest billingRequest = mSentRequests.get(Long.valueOf(j));
        if (billingRequest != null) {
            billingRequest.responseCodeReceived(responseCode);
        }
        mSentRequests.remove(Long.valueOf(j));
    }

    private boolean confirmNotifications(int i, String[] strArr) {
        return new ConfirmNotifications(i, strArr).runRequest();
    }

    private boolean getPurchaseInformation(int i, String[] strArr) {
        return new GetPurchaseInformation(i, strArr).runRequest();
    }

    private ArrayList<Purchase> getPurchases(String str, String str2) {
        ArrayList<Purchase> arrayList = new ArrayList<>();
        try {
            JSONArray optJSONArray = new JSONObject(str).optJSONArray("orders");
            int length = optJSONArray != null ? optJSONArray.length() : 0;
            int i = 0;
            while (i < length) {
                try {
                    JSONObject jSONObject = optJSONArray.getJSONObject(i);
                    arrayList.add(new Purchase(Consts.PurchaseState.valueOf(jSONObject.getInt("purchaseState")), jSONObject.has("notificationId") ? jSONObject.getString("notificationId") : null, jSONObject.getString("productId"), jSONObject.optString("orderId", ""), jSONObject.getLong("purchaseTime"), jSONObject.optString("developerPayload", null)));
                    i++;
                } catch (JSONException e) {
                    Logger.error("JSON exception: ", e);
                    return null;
                }
            }
            return arrayList;
        } catch (JSONException e2) {
            return null;
        }
    }

    private void purchaseStateChanged(int i, String str, String str2) {
        ArrayList<Purchase> purchases = getPurchases(str, str2);
        if (purchases != null) {
            ArrayList arrayList = new ArrayList();
            Iterator<Purchase> it = purchases.iterator();
            while (it.hasNext()) {
                Purchase next = it.next();
                if (next.notificationId != null) {
                    arrayList.add(next.notificationId);
                }
                ResponseHandler.purchaseResponse(next.purchaseState, next.productId, next.orderId, next.purchaseTime, str, str2);
            }
            if (!arrayList.isEmpty()) {
                confirmNotifications(i, (String[]) arrayList.toArray(new String[arrayList.size()]));
            }
        }
    }

    private void runPendingRequests() {
        int i = -1;
        while (true) {
            BillingRequest peek = mPendingRequests.peek();
            if (peek != null) {
                Logger.verbose("Billing Service - running request: " + peek.getStartId());
                if (peek.runIfConnected()) {
                    Logger.verbose("Billing Service - request ran: " + peek.getStartId());
                    mPendingRequests.remove();
                    if (i < peek.getStartId()) {
                        i = peek.getStartId();
                    }
                } else {
                    Logger.debug("Billing Service - bind to market service");
                    bindToMarketBillingService();
                    return;
                }
            } else if (i >= 0) {
                stopSelf(i);
                return;
            } else {
                return;
            }
        }
    }

    public boolean checkBillingSupported() {
        return new CheckBillingSupported().runRequest();
    }

    public void handleCommand(Intent intent, int i) {
        String action = intent.getAction();
        if (Consts.ACTION_CONFIRM_NOTIFICATION.equals(action)) {
            confirmNotifications(i, intent.getStringArrayExtra(Consts.NOTIFICATION_ID));
        } else if (Consts.ACTION_GET_PURCHASE_INFORMATION.equals(action)) {
            getPurchaseInformation(i, new String[]{intent.getStringExtra(Consts.NOTIFICATION_ID)});
        } else if (Consts.ACTION_PURCHASE_STATE_CHANGED.equals(action)) {
            purchaseStateChanged(i, intent.getStringExtra(Consts.INAPP_SIGNED_DATA), intent.getStringExtra(Consts.INAPP_SIGNATURE));
        } else if (Consts.ACTION_RESPONSE_CODE.equals(action)) {
            checkResponseCode(intent.getLongExtra(Consts.INAPP_REQUEST_ID, -1), Consts.ResponseCode.valueOf(intent.getIntExtra(Consts.INAPP_RESPONSE_CODE, Consts.ResponseCode.RESULT_ERROR.ordinal())));
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        mService = IMarketBillingService.Stub.asInterface(iBinder);
        runPendingRequests();
    }

    public void onServiceDisconnected(ComponentName componentName) {
        Logger.warn("Billing service disconnected");
        mService = null;
    }

    public void onStart(Intent intent, int i) {
        handleCommand(intent, i);
    }

    public boolean requestPurchase(Activity activity, String str) {
        return new RequestPurchase(this, activity, str).runRequest();
    }

    public boolean requestPurchase(Activity activity, String str, String str2) {
        return new RequestPurchase(activity, str, str2).runRequest();
    }

    public boolean restoreTransactions() {
        return new RestoreTransactions().runRequest();
    }

    public void setContext(Context context) {
        attachBaseContext(context);
    }

    public void unbind() {
        try {
            unbindService(this);
        } catch (IllegalArgumentException e) {
        }
    }
}
