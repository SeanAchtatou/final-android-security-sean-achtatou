package com.google.iap;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import com.google.iap.BillingService;
import com.google.iap.Consts;
import com.urbanairship.Logger;
import com.urbanairship.iap.MarketListener;

public class ResponseHandler {
    private static MarketListener marketListener;

    public static void buyPageIntentResponse(Activity activity, PendingIntent pendingIntent, Intent intent) {
        if (marketListener != null) {
            marketListener.startBuyPageActivity(activity, pendingIntent, intent);
        }
    }

    public static void checkBillingSupportedResponse(boolean z) {
        if (marketListener != null) {
            marketListener.onBillingSupported(z);
        }
    }

    public static void purchaseResponse(Consts.PurchaseState purchaseState, String str, String str2, long j, String str3, String str4) {
        Logger.verbose("purchaseResponse");
        if (marketListener != null) {
            marketListener.onPurchaseStateChange(purchaseState, str, str2, j, str3, str4);
        }
    }

    public static synchronized void register(MarketListener marketListener2) {
        synchronized (ResponseHandler.class) {
            marketListener = marketListener2;
        }
    }

    public static void responseCodeReceived(Context context, BillingService.RequestPurchase requestPurchase, Consts.ResponseCode responseCode) {
        if (marketListener != null) {
            marketListener.onRequestPurchaseResponse(requestPurchase, responseCode);
        }
    }

    public static void responseCodeReceived(Context context, BillingService.RestoreTransactions restoreTransactions, Consts.ResponseCode responseCode) {
        if (marketListener != null) {
            marketListener.onRestoreTransactionsResponse(restoreTransactions, responseCode);
        }
    }

    public static synchronized void unregister(MarketListener marketListener2) {
        synchronized (ResponseHandler.class) {
            marketListener = null;
        }
    }
}
