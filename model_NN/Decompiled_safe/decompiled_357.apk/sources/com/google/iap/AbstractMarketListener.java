package com.google.iap;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Handler;
import com.google.iap.BillingService;
import com.google.iap.Consts;
import com.urbanairship.Logger;
import java.lang.reflect.Method;

public abstract class AbstractMarketListener {
    private static final Class<?>[] START_INTENT_SENDER_SIG = {IntentSender.class, Intent.class, Integer.TYPE, Integer.TYPE, Integer.TYPE};
    private final Handler mHandler;
    private Method mStartIntentSender;
    private Object[] mStartIntentSenderArgs = new Object[5];

    public AbstractMarketListener(Handler handler) {
        this.mHandler = handler;
    }

    private void initCompatibilityLayer(Activity activity) {
        try {
            this.mStartIntentSender = activity.getClass().getMethod("startIntentSender", START_INTENT_SENDER_SIG);
        } catch (SecurityException e) {
            this.mStartIntentSender = null;
        } catch (NoSuchMethodException e2) {
            this.mStartIntentSender = null;
        }
    }

    public abstract void onBillingSupported(boolean z);

    public abstract void onPurchaseStateChange(Consts.PurchaseState purchaseState, String str, int i, long j, String str2);

    public abstract void onRequestPurchaseResponse(BillingService.RequestPurchase requestPurchase, Consts.ResponseCode responseCode);

    public abstract void onRestoreTransactionsResponse(BillingService.RestoreTransactions restoreTransactions, Consts.ResponseCode responseCode);

    public void postPurchaseStateChange(Consts.PurchaseState purchaseState, String str, int i, long j, String str2) {
        final Consts.PurchaseState purchaseState2 = purchaseState;
        final String str3 = str;
        final int i2 = i;
        final long j2 = j;
        final String str4 = str2;
        this.mHandler.post(new Runnable() {
            public void run() {
                AbstractMarketListener.this.onPurchaseStateChange(purchaseState2, str3, i2, j2, str4);
            }
        });
    }

    public void startBuyPageActivity(Activity activity, PendingIntent pendingIntent, Intent intent) {
        initCompatibilityLayer(activity);
        if (this.mStartIntentSender != null) {
            try {
                this.mStartIntentSenderArgs[0] = pendingIntent.getIntentSender();
                this.mStartIntentSenderArgs[1] = intent;
                this.mStartIntentSenderArgs[2] = 0;
                this.mStartIntentSenderArgs[3] = 0;
                this.mStartIntentSenderArgs[4] = 0;
                this.mStartIntentSender.invoke(activity, this.mStartIntentSenderArgs);
            } catch (Exception e) {
                Logger.error("error starting buy page activity", e);
            }
        } else {
            try {
                pendingIntent.send(activity, 0, intent);
            } catch (PendingIntent.CanceledException e2) {
                Logger.error("error starting buy page activity", e2);
            }
        }
    }
}
