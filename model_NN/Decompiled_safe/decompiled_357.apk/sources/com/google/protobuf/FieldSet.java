package com.google.protobuf;

import com.google.protobuf.FieldSet.FieldDescriptorLite;
import com.google.protobuf.Internal;
import com.google.protobuf.MessageLite;
import com.google.protobuf.WireFormat;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.codehaus.jackson.org.objectweb.asm.Opcodes;

final class FieldSet<FieldDescriptorType extends FieldDescriptorLite<FieldDescriptorType>> {
    private static final FieldSet DEFAULT_INSTANCE = new FieldSet(true);
    private Map<FieldDescriptorType, Object> fields;

    public interface FieldDescriptorLite<T extends FieldDescriptorLite<T>> extends Comparable<T> {
        Internal.EnumLiteMap<?> getEnumType();

        WireFormat.JavaType getLiteJavaType();

        WireFormat.FieldType getLiteType();

        int getNumber();

        MessageLite.Builder internalMergeFrom(MessageLite.Builder builder, MessageLite messageLite);

        boolean isPacked();

        boolean isRepeated();
    }

    private FieldSet() {
        this.fields = new TreeMap();
    }

    private FieldSet(boolean dummy) {
        this.fields = Collections.emptyMap();
    }

    public static <T extends FieldDescriptorLite<T>> FieldSet<T> newFieldSet() {
        return new FieldSet<>();
    }

    public static <T extends FieldDescriptorLite<T>> FieldSet<T> emptySet() {
        return DEFAULT_INSTANCE;
    }

    public void makeImmutable() {
        for (Map.Entry<FieldDescriptorType, Object> entry : this.fields.entrySet()) {
            if (((FieldDescriptorLite) entry.getKey()).isRepeated()) {
                this.fields.put(entry.getKey(), Collections.unmodifiableList((List) entry.getValue()));
            }
        }
        this.fields = Collections.unmodifiableMap(this.fields);
    }

    public void clear() {
        this.fields.clear();
    }

    public Map<FieldDescriptorType, Object> getAllFields() {
        return Collections.unmodifiableMap(this.fields);
    }

    public Iterator<Map.Entry<FieldDescriptorType, Object>> iterator() {
        return this.fields.entrySet().iterator();
    }

    public boolean hasField(FieldDescriptorType descriptor) {
        if (!descriptor.isRepeated()) {
            return this.fields.get(descriptor) != null;
        }
        throw new IllegalArgumentException("hasField() can only be called on non-repeated fields.");
    }

    public Object getField(FieldDescriptorType descriptor) {
        return this.fields.get(descriptor);
    }

    public void setField(FieldDescriptorType descriptor, Object value) {
        if (!descriptor.isRepeated()) {
            verifyType(descriptor.getLiteType(), value);
        } else if (!(value instanceof List)) {
            throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
        } else {
            List<Object> newList = new ArrayList<>();
            newList.addAll((List) value);
            for (Object element : newList) {
                verifyType(descriptor.getLiteType(), element);
            }
            value = newList;
        }
        this.fields.put(descriptor, value);
    }

    public void clearField(FieldDescriptorType descriptor) {
        this.fields.remove(descriptor);
    }

    public int getRepeatedFieldCount(FieldDescriptorType descriptor) {
        if (!descriptor.isRepeated()) {
            throw new IllegalArgumentException("getRepeatedField() can only be called on repeated fields.");
        }
        Object value = this.fields.get(descriptor);
        if (value == null) {
            return 0;
        }
        return ((List) value).size();
    }

    public Object getRepeatedField(FieldDescriptorType descriptor, int index) {
        if (!descriptor.isRepeated()) {
            throw new IllegalArgumentException("getRepeatedField() can only be called on repeated fields.");
        }
        Object value = this.fields.get(descriptor);
        if (value != null) {
            return ((List) value).get(index);
        }
        throw new IndexOutOfBoundsException();
    }

    public void setRepeatedField(FieldDescriptorType descriptor, int index, Object value) {
        if (!descriptor.isRepeated()) {
            throw new IllegalArgumentException("getRepeatedField() can only be called on repeated fields.");
        }
        Object list = this.fields.get(descriptor);
        if (list == null) {
            throw new IndexOutOfBoundsException();
        }
        verifyType(descriptor.getLiteType(), value);
        ((List) list).set(index, value);
    }

    public void addRepeatedField(FieldDescriptorType descriptor, Object value) {
        List list;
        if (!descriptor.isRepeated()) {
            throw new IllegalArgumentException("addRepeatedField() can only be called on repeated fields.");
        }
        verifyType(descriptor.getLiteType(), value);
        Object existingValue = this.fields.get(descriptor);
        if (existingValue == null) {
            list = new ArrayList();
            this.fields.put(descriptor, list);
        } else {
            list = (List) existingValue;
        }
        list.add(value);
    }

    private static void verifyType(WireFormat.FieldType type, Object value) {
        if (value == null) {
            throw new NullPointerException();
        }
        boolean isValid = false;
        switch (type.getJavaType()) {
            case INT:
                isValid = value instanceof Integer;
                break;
            case LONG:
                isValid = value instanceof Long;
                break;
            case FLOAT:
                isValid = value instanceof Float;
                break;
            case DOUBLE:
                isValid = value instanceof Double;
                break;
            case BOOLEAN:
                isValid = value instanceof Boolean;
                break;
            case STRING:
                isValid = value instanceof String;
                break;
            case BYTE_STRING:
                isValid = value instanceof ByteString;
                break;
            case ENUM:
                isValid = value instanceof Internal.EnumLite;
                break;
            case MESSAGE:
                isValid = value instanceof MessageLite;
                break;
        }
        if (!isValid) {
            throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
        }
    }

    public boolean isInitialized() {
        for (Map.Entry<FieldDescriptorType, Object> entry : this.fields.entrySet()) {
            FieldDescriptorType descriptor = (FieldDescriptorLite) entry.getKey();
            if (descriptor.getLiteJavaType() == WireFormat.JavaType.MESSAGE) {
                if (descriptor.isRepeated()) {
                    for (MessageLite element : (List) entry.getValue()) {
                        if (!element.isInitialized()) {
                            return false;
                        }
                    }
                    continue;
                } else if (!((MessageLite) entry.getValue()).isInitialized()) {
                    return false;
                }
            }
        }
        return true;
    }

    static int getWireFormatForFieldType(WireFormat.FieldType type, boolean isPacked) {
        if (isPacked) {
            return 2;
        }
        return type.getWireType();
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: FieldDescriptorType
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void mergeFrom(com.google.protobuf.FieldSet<FieldDescriptorType> r8) {
        /*
            r7 = this;
            java.util.Map<FieldDescriptorType, java.lang.Object> r5 = r8.fields
            java.util.Set r5 = r5.entrySet()
            java.util.Iterator r2 = r5.iterator()
        L_0x000a:
            boolean r5 = r2.hasNext()
            if (r5 == 0) goto L_0x0075
            java.lang.Object r1 = r2.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            java.lang.Object r0 = r1.getKey()
            com.google.protobuf.FieldSet$FieldDescriptorLite r0 = (com.google.protobuf.FieldSet.FieldDescriptorLite) r0
            java.lang.Object r3 = r1.getValue()
            boolean r5 = r0.isRepeated()
            if (r5 == 0) goto L_0x0043
            java.util.Map<FieldDescriptorType, java.lang.Object> r5 = r7.fields
            java.lang.Object r4 = r5.get(r0)
            if (r4 != 0) goto L_0x003b
            java.util.Map<FieldDescriptorType, java.lang.Object> r5 = r7.fields
            java.util.ArrayList r6 = new java.util.ArrayList
            java.util.List r3 = (java.util.List) r3
            r6.<init>(r3)
            r5.put(r0, r6)
            goto L_0x000a
        L_0x003b:
            java.util.List r4 = (java.util.List) r4
            java.util.List r3 = (java.util.List) r3
            r4.addAll(r3)
            goto L_0x000a
        L_0x0043:
            com.google.protobuf.WireFormat$JavaType r5 = r0.getLiteJavaType()
            com.google.protobuf.WireFormat$JavaType r6 = com.google.protobuf.WireFormat.JavaType.MESSAGE
            if (r5 != r6) goto L_0x006f
            java.util.Map<FieldDescriptorType, java.lang.Object> r5 = r7.fields
            java.lang.Object r4 = r5.get(r0)
            if (r4 != 0) goto L_0x0059
            java.util.Map<FieldDescriptorType, java.lang.Object> r5 = r7.fields
            r5.put(r0, r3)
            goto L_0x000a
        L_0x0059:
            java.util.Map<FieldDescriptorType, java.lang.Object> r5 = r7.fields
            com.google.protobuf.MessageLite r4 = (com.google.protobuf.MessageLite) r4
            com.google.protobuf.MessageLite$Builder r6 = r4.toBuilder()
            com.google.protobuf.MessageLite r3 = (com.google.protobuf.MessageLite) r3
            com.google.protobuf.MessageLite$Builder r6 = r0.internalMergeFrom(r6, r3)
            com.google.protobuf.MessageLite r6 = r6.build()
            r5.put(r0, r6)
            goto L_0x000a
        L_0x006f:
            java.util.Map<FieldDescriptorType, java.lang.Object> r5 = r7.fields
            r5.put(r0, r3)
            goto L_0x000a
        L_0x0075:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.FieldSet.mergeFrom(com.google.protobuf.FieldSet):void");
    }

    /* renamed from: com.google.protobuf.FieldSet$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$WireFormat$FieldType = new int[WireFormat.FieldType.values().length];

        static {
            try {
                $SwitchMap$com$google$protobuf$WireFormat$FieldType[WireFormat.FieldType.DOUBLE.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$google$protobuf$WireFormat$FieldType[WireFormat.FieldType.FLOAT.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$google$protobuf$WireFormat$FieldType[WireFormat.FieldType.INT64.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$google$protobuf$WireFormat$FieldType[WireFormat.FieldType.UINT64.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$google$protobuf$WireFormat$FieldType[WireFormat.FieldType.INT32.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$google$protobuf$WireFormat$FieldType[WireFormat.FieldType.FIXED64.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$com$google$protobuf$WireFormat$FieldType[WireFormat.FieldType.FIXED32.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$com$google$protobuf$WireFormat$FieldType[WireFormat.FieldType.BOOL.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                $SwitchMap$com$google$protobuf$WireFormat$FieldType[WireFormat.FieldType.STRING.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
            try {
                $SwitchMap$com$google$protobuf$WireFormat$FieldType[WireFormat.FieldType.BYTES.ordinal()] = 10;
            } catch (NoSuchFieldError e10) {
            }
            try {
                $SwitchMap$com$google$protobuf$WireFormat$FieldType[WireFormat.FieldType.UINT32.ordinal()] = 11;
            } catch (NoSuchFieldError e11) {
            }
            try {
                $SwitchMap$com$google$protobuf$WireFormat$FieldType[WireFormat.FieldType.SFIXED32.ordinal()] = 12;
            } catch (NoSuchFieldError e12) {
            }
            try {
                $SwitchMap$com$google$protobuf$WireFormat$FieldType[WireFormat.FieldType.SFIXED64.ordinal()] = 13;
            } catch (NoSuchFieldError e13) {
            }
            try {
                $SwitchMap$com$google$protobuf$WireFormat$FieldType[WireFormat.FieldType.SINT32.ordinal()] = 14;
            } catch (NoSuchFieldError e14) {
            }
            try {
                $SwitchMap$com$google$protobuf$WireFormat$FieldType[WireFormat.FieldType.SINT64.ordinal()] = 15;
            } catch (NoSuchFieldError e15) {
            }
            try {
                $SwitchMap$com$google$protobuf$WireFormat$FieldType[WireFormat.FieldType.GROUP.ordinal()] = 16;
            } catch (NoSuchFieldError e16) {
            }
            try {
                $SwitchMap$com$google$protobuf$WireFormat$FieldType[WireFormat.FieldType.MESSAGE.ordinal()] = 17;
            } catch (NoSuchFieldError e17) {
            }
            try {
                $SwitchMap$com$google$protobuf$WireFormat$FieldType[WireFormat.FieldType.ENUM.ordinal()] = 18;
            } catch (NoSuchFieldError e18) {
            }
            $SwitchMap$com$google$protobuf$WireFormat$JavaType = new int[WireFormat.JavaType.values().length];
            try {
                $SwitchMap$com$google$protobuf$WireFormat$JavaType[WireFormat.JavaType.INT.ordinal()] = 1;
            } catch (NoSuchFieldError e19) {
            }
            try {
                $SwitchMap$com$google$protobuf$WireFormat$JavaType[WireFormat.JavaType.LONG.ordinal()] = 2;
            } catch (NoSuchFieldError e20) {
            }
            try {
                $SwitchMap$com$google$protobuf$WireFormat$JavaType[WireFormat.JavaType.FLOAT.ordinal()] = 3;
            } catch (NoSuchFieldError e21) {
            }
            try {
                $SwitchMap$com$google$protobuf$WireFormat$JavaType[WireFormat.JavaType.DOUBLE.ordinal()] = 4;
            } catch (NoSuchFieldError e22) {
            }
            try {
                $SwitchMap$com$google$protobuf$WireFormat$JavaType[WireFormat.JavaType.BOOLEAN.ordinal()] = 5;
            } catch (NoSuchFieldError e23) {
            }
            try {
                $SwitchMap$com$google$protobuf$WireFormat$JavaType[WireFormat.JavaType.STRING.ordinal()] = 6;
            } catch (NoSuchFieldError e24) {
            }
            try {
                $SwitchMap$com$google$protobuf$WireFormat$JavaType[WireFormat.JavaType.BYTE_STRING.ordinal()] = 7;
            } catch (NoSuchFieldError e25) {
            }
            try {
                $SwitchMap$com$google$protobuf$WireFormat$JavaType[WireFormat.JavaType.ENUM.ordinal()] = 8;
            } catch (NoSuchFieldError e26) {
            }
            try {
                $SwitchMap$com$google$protobuf$WireFormat$JavaType[WireFormat.JavaType.MESSAGE.ordinal()] = 9;
            } catch (NoSuchFieldError e27) {
            }
        }
    }

    public static Object readPrimitiveField(CodedInputStream input, WireFormat.FieldType type) throws IOException {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$WireFormat$FieldType[type.ordinal()]) {
            case 1:
                return Double.valueOf(input.readDouble());
            case 2:
                return Float.valueOf(input.readFloat());
            case 3:
                return Long.valueOf(input.readInt64());
            case 4:
                return Long.valueOf(input.readUInt64());
            case 5:
                return Integer.valueOf(input.readInt32());
            case 6:
                return Long.valueOf(input.readFixed64());
            case 7:
                return Integer.valueOf(input.readFixed32());
            case 8:
                return Boolean.valueOf(input.readBool());
            case 9:
                return input.readString();
            case 10:
                return input.readBytes();
            case 11:
                return Integer.valueOf(input.readUInt32());
            case Opcodes.FCONST_1 /*12*/:
                return Integer.valueOf(input.readSFixed32());
            case Opcodes.FCONST_2 /*13*/:
                return Long.valueOf(input.readSFixed64());
            case Opcodes.DCONST_0 /*14*/:
                return Integer.valueOf(input.readSInt32());
            case Opcodes.DCONST_1 /*15*/:
                return Long.valueOf(input.readSInt64());
            case 16:
                throw new IllegalArgumentException("readPrimitiveField() cannot handle nested groups.");
            case Opcodes.SIPUSH /*17*/:
                throw new IllegalArgumentException("readPrimitiveField() cannot handle embedded messages.");
            case Opcodes.LDC /*18*/:
                throw new IllegalArgumentException("readPrimitiveField() cannot handle enums.");
            default:
                throw new RuntimeException("There is no way to get here, but the compiler thinks otherwise.");
        }
    }

    public void writeTo(CodedOutputStream output) throws IOException {
        for (Map.Entry<FieldDescriptorType, Object> entry : this.fields.entrySet()) {
            writeField((FieldDescriptorLite) entry.getKey(), entry.getValue(), output);
        }
    }

    public void writeMessageSetTo(CodedOutputStream output) throws IOException {
        for (Map.Entry<FieldDescriptorType, Object> entry : this.fields.entrySet()) {
            FieldDescriptorType descriptor = (FieldDescriptorLite) entry.getKey();
            if (descriptor.getLiteJavaType() != WireFormat.JavaType.MESSAGE || descriptor.isRepeated() || descriptor.isPacked()) {
                writeField(descriptor, entry.getValue(), output);
            } else {
                output.writeMessageSetExtension(((FieldDescriptorLite) entry.getKey()).getNumber(), (MessageLite) entry.getValue());
            }
        }
    }

    private static void writeElement(CodedOutputStream output, WireFormat.FieldType type, int number, Object value) throws IOException {
        if (type == WireFormat.FieldType.GROUP) {
            output.writeGroup(number, (MessageLite) value);
            return;
        }
        output.writeTag(number, getWireFormatForFieldType(type, false));
        writeElementNoTag(output, type, value);
    }

    private static void writeElementNoTag(CodedOutputStream output, WireFormat.FieldType type, Object value) throws IOException {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$WireFormat$FieldType[type.ordinal()]) {
            case 1:
                output.writeDoubleNoTag(((Double) value).doubleValue());
                return;
            case 2:
                output.writeFloatNoTag(((Float) value).floatValue());
                return;
            case 3:
                output.writeInt64NoTag(((Long) value).longValue());
                return;
            case 4:
                output.writeUInt64NoTag(((Long) value).longValue());
                return;
            case 5:
                output.writeInt32NoTag(((Integer) value).intValue());
                return;
            case 6:
                output.writeFixed64NoTag(((Long) value).longValue());
                return;
            case 7:
                output.writeFixed32NoTag(((Integer) value).intValue());
                return;
            case 8:
                output.writeBoolNoTag(((Boolean) value).booleanValue());
                return;
            case 9:
                output.writeStringNoTag((String) value);
                return;
            case 10:
                output.writeBytesNoTag((ByteString) value);
                return;
            case 11:
                output.writeUInt32NoTag(((Integer) value).intValue());
                return;
            case Opcodes.FCONST_1 /*12*/:
                output.writeSFixed32NoTag(((Integer) value).intValue());
                return;
            case Opcodes.FCONST_2 /*13*/:
                output.writeSFixed64NoTag(((Long) value).longValue());
                return;
            case Opcodes.DCONST_0 /*14*/:
                output.writeSInt32NoTag(((Integer) value).intValue());
                return;
            case Opcodes.DCONST_1 /*15*/:
                output.writeSInt64NoTag(((Long) value).longValue());
                return;
            case 16:
                output.writeGroupNoTag((MessageLite) value);
                return;
            case Opcodes.SIPUSH /*17*/:
                output.writeMessageNoTag((MessageLite) value);
                return;
            case Opcodes.LDC /*18*/:
                output.writeEnumNoTag(((Internal.EnumLite) value).getNumber());
                return;
            default:
                return;
        }
    }

    public static void writeField(FieldDescriptorLite<?> descriptor, Object value, CodedOutputStream output) throws IOException {
        WireFormat.FieldType type = descriptor.getLiteType();
        int number = descriptor.getNumber();
        if (descriptor.isRepeated()) {
            List<Object> valueList = (List) value;
            if (descriptor.isPacked()) {
                output.writeTag(number, 2);
                int dataSize = 0;
                for (Object element : valueList) {
                    dataSize += computeElementSizeNoTag(type, element);
                }
                output.writeRawVarint32(dataSize);
                for (Object element2 : valueList) {
                    writeElementNoTag(output, type, element2);
                }
                return;
            }
            for (Object element3 : valueList) {
                writeElement(output, type, number, element3);
            }
            return;
        }
        writeElement(output, type, number, value);
    }

    public int getSerializedSize() {
        int size = 0;
        for (Map.Entry<FieldDescriptorType, Object> entry : this.fields.entrySet()) {
            size += computeFieldSize((FieldDescriptorLite) entry.getKey(), entry.getValue());
        }
        return size;
    }

    public int getMessageSetSerializedSize() {
        int size = 0;
        for (Map.Entry<FieldDescriptorType, Object> entry : this.fields.entrySet()) {
            FieldDescriptorType descriptor = (FieldDescriptorLite) entry.getKey();
            if (descriptor.getLiteJavaType() != WireFormat.JavaType.MESSAGE || descriptor.isRepeated() || descriptor.isPacked()) {
                size += computeFieldSize(descriptor, entry.getValue());
            } else {
                size += CodedOutputStream.computeMessageSetExtensionSize(((FieldDescriptorLite) entry.getKey()).getNumber(), (MessageLite) entry.getValue());
            }
        }
        return size;
    }

    private static int computeElementSize(WireFormat.FieldType type, int number, Object value) {
        int tagSize = CodedOutputStream.computeTagSize(number);
        if (type == WireFormat.FieldType.GROUP) {
            tagSize *= 2;
        }
        return computeElementSizeNoTag(type, value) + tagSize;
    }

    private static int computeElementSizeNoTag(WireFormat.FieldType type, Object value) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$WireFormat$FieldType[type.ordinal()]) {
            case 1:
                return CodedOutputStream.computeDoubleSizeNoTag(((Double) value).doubleValue());
            case 2:
                return CodedOutputStream.computeFloatSizeNoTag(((Float) value).floatValue());
            case 3:
                return CodedOutputStream.computeInt64SizeNoTag(((Long) value).longValue());
            case 4:
                return CodedOutputStream.computeUInt64SizeNoTag(((Long) value).longValue());
            case 5:
                return CodedOutputStream.computeInt32SizeNoTag(((Integer) value).intValue());
            case 6:
                return CodedOutputStream.computeFixed64SizeNoTag(((Long) value).longValue());
            case 7:
                return CodedOutputStream.computeFixed32SizeNoTag(((Integer) value).intValue());
            case 8:
                return CodedOutputStream.computeBoolSizeNoTag(((Boolean) value).booleanValue());
            case 9:
                return CodedOutputStream.computeStringSizeNoTag((String) value);
            case 10:
                return CodedOutputStream.computeBytesSizeNoTag((ByteString) value);
            case 11:
                return CodedOutputStream.computeUInt32SizeNoTag(((Integer) value).intValue());
            case Opcodes.FCONST_1 /*12*/:
                return CodedOutputStream.computeSFixed32SizeNoTag(((Integer) value).intValue());
            case Opcodes.FCONST_2 /*13*/:
                return CodedOutputStream.computeSFixed64SizeNoTag(((Long) value).longValue());
            case Opcodes.DCONST_0 /*14*/:
                return CodedOutputStream.computeSInt32SizeNoTag(((Integer) value).intValue());
            case Opcodes.DCONST_1 /*15*/:
                return CodedOutputStream.computeSInt64SizeNoTag(((Long) value).longValue());
            case 16:
                return CodedOutputStream.computeGroupSizeNoTag((MessageLite) value);
            case Opcodes.SIPUSH /*17*/:
                return CodedOutputStream.computeMessageSizeNoTag((MessageLite) value);
            case Opcodes.LDC /*18*/:
                return CodedOutputStream.computeEnumSizeNoTag(((Internal.EnumLite) value).getNumber());
            default:
                throw new RuntimeException("There is no way to get here, but the compiler thinks otherwise.");
        }
    }

    public static int computeFieldSize(FieldDescriptorLite<?> descriptor, Object value) {
        WireFormat.FieldType type = descriptor.getLiteType();
        int number = descriptor.getNumber();
        if (!descriptor.isRepeated()) {
            return computeElementSize(type, number, value);
        }
        if (descriptor.isPacked()) {
            int dataSize = 0;
            for (Object element : (List) value) {
                dataSize += computeElementSizeNoTag(type, element);
            }
            return CodedOutputStream.computeTagSize(number) + dataSize + CodedOutputStream.computeRawVarint32Size(dataSize);
        }
        int size = 0;
        for (Object element2 : (List) value) {
            size += computeElementSize(type, number, element2);
        }
        return size;
    }
}
