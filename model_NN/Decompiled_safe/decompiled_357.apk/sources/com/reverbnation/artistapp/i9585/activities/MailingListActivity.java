package com.reverbnation.artistapp.i9585.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import com.google.common.base.Strings;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.api.tasks.PostFanReachApiTask;
import com.reverbnation.artistapp.i9585.utils.AnalyticsHelper;
import java.util.regex.Pattern;
import org.codehaus.jackson.util.MinimalPrettyPrinter;

public class MailingListActivity extends BaseTabChildActivity {
    private static final int ALREADY_REGISTERED_DIALOG = 3;
    private static final int INVALID_EMAIL_DIALOG = 0;
    private static final int REGISTER_DIALOG = 1;
    private static final int REGISTRATION_FAILED_DIALOG = 4;
    private static final int SUCCESS_DIALOG = 2;
    private static final String TAG = "MailingListActivity";
    /* access modifiers changed from: private */
    public EditText emailEditText;
    private CheckBox streetTeamCheckbox;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.mailing_list_activity);
        setupViews();
    }

    private void setupViews() {
        this.emailEditText = (EditText) findViewById(R.id.email_edit_text);
        this.streetTeamCheckbox = (CheckBox) findViewById(R.id.street_team_checkbox);
    }

    public void onSubmitButtonClicked(View v) {
        String email = this.emailEditText.getEditableText().toString();
        boolean isChecked = this.streetTeamCheckbox.isChecked();
        Log.v(TAG, "Street team is" + (isChecked ? MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR : " not ") + "checked");
        if (validateEmail(email)) {
            showDialog(1);
            new PostFanReachApiTask(new PostFanReachApiTask.PostFanReachApiDelegate() {
                public void fanReachFinished(String status) {
                    MailingListActivity.this.getActivityHelper().dismissDialog(1);
                    Log.v(MailingListActivity.TAG, "Insertion status: " + status);
                    if (status == null) {
                        MailingListActivity.this.showDialog(4);
                    } else if (status.equals("new")) {
                        AnalyticsHelper.getInstance(MailingListActivity.this.getBaseContext()).trackEvent("social", "mailinglist", "signup_new");
                        MailingListActivity.this.showDialog(2);
                    } else {
                        AnalyticsHelper.getInstance(MailingListActivity.this.getBaseContext()).trackEvent("social", "mailinglist", "signup_old");
                        MailingListActivity.this.showDialog(3);
                    }
                }

                public void fanReachStarted() {
                }
            }).execute(getActivityHelper().getArtistId(), null, email, Boolean.valueOf(isChecked));
            return;
        }
        showDialog(0);
    }

    private boolean validateEmail(String email) {
        return !Strings.isNullOrEmpty(email) && Pattern.compile(".+@.+").matcher(email).matches();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        AnalyticsHelper.getInstance(this).trackPageView("homescreen/social/mailing_list");
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        Dialog dialog = null;
        switch (id) {
            case 0:
                dialog = getActivityHelper().createDialog((int) R.string.oops_bang, (int) R.string.enter_an_email_address);
                break;
            case 1:
                dialog = getActivityHelper().createProgressDialog(R.string.registering_for_mailing_list, R.string.please_wait);
                break;
            case 2:
                dialog = createSuccessDialog();
                break;
            case 3:
                dialog = getActivityHelper().createDialog((int) R.string.you_are_already_a_fan, (int) R.string.thanks_for_being_a_fan);
                break;
            case 4:
                dialog = getActivityHelper().createDialog((int) R.string.error, (int) R.string.enter_valid_email_address);
                break;
        }
        if (dialog != null) {
            return dialog;
        }
        return super.onCreateDialog(id);
    }

    private Dialog createSuccessDialog() {
        return new AlertDialog.Builder(this).setTitle((int) R.string.success_bang).setMessage((int) R.string.please_check_your_email).setNeutralButton(17039370, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                MailingListActivity.this.emailEditText.getEditableText().clear();
            }
        }).create();
    }
}
