package com.reverbnation.artistapp.i9585.models;

import org.codehaus.jackson.annotate.JsonProperty;

public class Server {
    @JsonProperty("content")
    private Content content;
    @JsonProperty("features")
    public Features features;
    @JsonProperty("status")
    private Status status;
    @JsonProperty("urls")
    public Urls urls;

    public static class Status {
        @JsonProperty("availability")
        private String availability;
        @JsonProperty("message")
        private String message;

        public String getAvailability() {
            return this.availability;
        }

        public String getMessage() {
            return this.message;
        }
    }

    public static class Content {
        @JsonProperty("blogs")
        private int blogs;
        @JsonProperty("links")
        private int links;
        @JsonProperty("photos")
        private int photos;
        @JsonProperty("shows_past")
        private int showsPast;
        @JsonProperty("shows_upcoming")
        private int showsUpcoming;
        @JsonProperty("songs")
        private int songs;
        @JsonProperty("twitter")
        private int twitter;
        @JsonProperty("videos")
        private int videos;

        public int getLinks() {
            return this.links;
        }

        public int getShowsUpcoming() {
            return this.showsUpcoming;
        }

        public int getShowsPast() {
            return this.showsPast;
        }

        public int getPhotos() {
            return this.photos;
        }

        public int getBlogs() {
            return this.blogs;
        }

        public int getVideos() {
            return this.videos;
        }

        public int getSongs() {
            return this.songs;
        }

        public int getTwitter() {
            return this.twitter;
        }
    }

    public static class Urls {
        @JsonProperty("image")
        private String image;
        @JsonProperty("web")
        private String web;
        @JsonProperty("widget")
        private String widget;

        public String getWidget() {
            return this.widget;
        }

        public String getWeb() {
            return this.web;
        }

        public String getImage() {
            return this.image;
        }
    }

    public static class Features {
        @JsonProperty("cache_img")
        private boolean isCached;

        public boolean isCached() {
            return this.isCached;
        }
    }

    public Status getStatus() {
        return this.status;
    }

    public Content getContent() {
        return this.content;
    }

    public Urls getUrls() {
        return this.urls;
    }

    public Features getFeatures() {
        return this.features;
    }

    public boolean isUpdateRequired() {
        return "Update".equals(getStatus().getAvailability());
    }
}
