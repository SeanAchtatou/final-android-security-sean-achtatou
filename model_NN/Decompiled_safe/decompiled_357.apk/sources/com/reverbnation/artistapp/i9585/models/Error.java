package com.reverbnation.artistapp.i9585.models;

import com.urbanairship.analytics.EventDataManager;
import org.codehaus.jackson.annotate.JsonProperty;

public class Error {
    @JsonProperty("message")
    private String message;
    @JsonProperty(EventDataManager.Events.COLUMN_NAME_TYPE)
    private String type;

    public String getType() {
        return this.type;
    }

    public String getMessage() {
        return this.message;
    }
}
