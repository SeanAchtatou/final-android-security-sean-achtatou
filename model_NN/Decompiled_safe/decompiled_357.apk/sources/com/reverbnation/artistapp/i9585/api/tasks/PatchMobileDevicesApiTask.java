package com.reverbnation.artistapp.i9585.api.tasks;

import android.os.AsyncTask;
import com.reverbnation.artistapp.i9585.ReverbNationApplication;
import com.reverbnation.artistapp.i9585.api.ReverbNationApi;
import com.roobit.restkit.RestClient;
import java.util.Properties;

public class PatchMobileDevicesApiTask extends AsyncTask<Object, Void, Void> {
    /* access modifiers changed from: protected */
    public Void doInBackground(Object... args) {
        int mobileDeviceId = ((Integer) args[1]).intValue();
        ReverbNationApi api = ReverbNationApi.getInstance();
        RestClient.getClientWithBaseUrl(ReverbNationApi.getBaseUrl()).patch(getDeviceParameters((String) args[0], String.valueOf(api.getMobileApplication().getId())), api.getRequiredHeaderParameters()).setResource("mobile_devices/" + mobileDeviceId + ".json").synchronousExecute();
        RestClient.getClientWithBaseUrl(ReverbNationApi.getBaseUrl()).patch(getApplicationParameters(), api.getRequiredHeaderParameters()).setResource("mobile_application_links/" + String.valueOf(ReverbNationApplication.getInstance().getMobileApplicationLink()) + ".json").synchronousExecute();
        return null;
    }

    private Properties getApplicationParameters() {
        Properties props = new Properties();
        props.put("push_enabled", "1");
        return props;
    }

    private Properties getDeviceParameters(String pushKey, String applicationId) {
        Properties props = new Properties();
        props.put("push_key", pushKey);
        props.put("mobile_application_platform_id", applicationId);
        return props;
    }
}
