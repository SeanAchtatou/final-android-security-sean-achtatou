package com.reverbnation.artistapp.i9585.utils;

import android.util.Log;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import java.util.Iterator;
import org.codehaus.jackson.util.MinimalPrettyPrinter;

public class VersionString {
    private static final String TAG = "VersionString";
    String version;

    public VersionString(String version2) {
        Preconditions.checkArgument(!Strings.isNullOrEmpty(version2));
        this.version = version2;
    }

    public int compareTo(String rhs) {
        boolean z;
        if (!Strings.isNullOrEmpty(rhs)) {
            z = true;
        } else {
            z = false;
        }
        Preconditions.checkArgument(z);
        Iterator<String> rhsIter = Splitter.on('.').split(rhs).iterator();
        for (String parseInt : Splitter.on('.').split(this.version)) {
            if (!rhsIter.hasNext()) {
                explain("greater than", rhs);
                return 1;
            }
            int myVersion = Integer.parseInt(parseInt);
            int rhsVersion = Integer.parseInt(rhsIter.next());
            if (myVersion < rhsVersion) {
                explain("smaller than", rhs);
                return -1;
            } else if (myVersion > rhsVersion) {
                explain("greater than", rhs);
                return 1;
            }
        }
        Log.v(TAG, "Either less or equal depending on rhs length");
        boolean isSmaller = rhsIter.hasNext();
        explain(isSmaller ? "smaller than" : "equal to", rhs);
        if (isSmaller) {
            return -1;
        }
        return 0;
    }

    private void explain(String comparedTo, String rhs) {
        Log.v(TAG, "Version " + this.version + " is " + comparedTo + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + rhs);
    }
}
