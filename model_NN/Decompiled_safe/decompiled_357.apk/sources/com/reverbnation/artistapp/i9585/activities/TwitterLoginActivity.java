package com.reverbnation.artistapp.i9585.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.google.common.base.Strings;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.utils.AnalyticsHelper;
import java.util.HashMap;
import java.util.Map;
import twitter4j.TwitterException;
import twitter4j.auth.RequestToken;
import twitter4j.internal.http.HttpClientImpl;
import twitter4j.internal.http.HttpParameter;
import twitter4j.internal.http.HttpRequest;
import twitter4j.internal.http.HttpResponse;
import twitter4j.internal.http.RequestMethod;

public class TwitterLoginActivity extends BaseActivity {
    private static final int PROGRESS_DIALOG = 0;
    public static final String TAG = "TwitterLoginActivity";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(7);
        setContentView((int) R.layout.twitter_login_activity);
        getActivityHelper().setArtistTitlebar((int) R.string.twitter_login);
        setUpViews();
    }

    private void setUpViews() {
        ((TextView) findViewById(R.id.twitterTOSwarning2)).setMovementMethod(LinkMovementMethod.getInstance());
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        AnalyticsHelper.getInstance(this).trackPageView("social_twitter_login");
        super.onResume();
    }

    public void onClickLogin(View v) {
        showDialog(0);
        new AsyncTask<Void, Void, String>() {
            /* access modifiers changed from: protected */
            public String doInBackground(Void... params) {
                try {
                    TwitterLoginActivity.this.authorizeTwitter(TwitterLoginActivity.this.getUsername(), TwitterLoginActivity.this.getPassword());
                    return null;
                } catch (TwitterAuthorizationException e) {
                    return e.getMessage();
                }
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(String errorString) {
                TwitterLoginActivity.this.getActivityHelper().dismissDialog(0);
                boolean success = Strings.isNullOrEmpty(errorString);
                Intent intent = new Intent();
                intent.putExtra("errorString", errorString);
                TwitterLoginActivity.this.setResult(success ? -1 : 1, intent);
                TwitterLoginActivity.this.finish();
            }
        }.execute(new Void[0]);
    }

    public String getUsername() {
        return ((EditText) findViewById(R.id.username_edit)).getText().toString();
    }

    public String getPassword() {
        return ((EditText) findViewById(R.id.password_edit)).getText().toString();
    }

    /* access modifiers changed from: protected */
    public void authorizeTwitter(String username, String password) throws TwitterAuthorizationException {
        RequestToken currentRequestToken = getActivityHelper().getApplication().getRequestToken();
        if (currentRequestToken == null) {
            throw new TwitterAuthorizationException("The twitter request token is missing from this application. Not your fault.");
        }
        String authorizationUrl = currentRequestToken.getAuthorizationURL();
        try {
            HttpClientImpl http = new HttpClientImpl();
            HttpResponse response = http.get(authorizationUrl);
            if (response.getStatusCode() > 299) {
                throw new TwitterAuthorizationException("Authorization url get failed: " + response.getStatusCode());
            }
            Map<String, String> props = new HashMap<>();
            props.put("Cookie", response.getResponseHeader("Set-Cookie"));
            try {
                String responseString = response.asString();
                try {
                    try {
                        try {
                            try {
                                try {
                                    getActivityHelper().getApplication().setAccessToken(getActivityHelper().getTwitter().getOAuthAccessToken(currentRequestToken, catchPattern(http.request(new HttpRequest(RequestMethod.POST, catchPattern(responseString, "<form action=\"", "\" id=\"oauth_form\""), new HttpParameter[]{new HttpParameter("authenticity_token", catchPattern(responseString, "\"authenticity_token\" type=\"hidden\" value=\"", "\" />")), new HttpParameter("oauth_token", catchPattern(responseString, "name=\"oauth_token\" type=\"hidden\" value=\"", "\" />")), new HttpParameter("session[username_or_email]", username), new HttpParameter("session[password]", password)}, null, props)).asString(), "oauth_verifier=", "\">")));
                                } catch (TwitterException e) {
                                    throw new TwitterAuthorizationException("We failed to get your authorization token from twitter. " + e.getMessage());
                                }
                            } catch (StringIndexOutOfBoundsException e2) {
                                throw new TwitterAuthorizationException("Twitter's response doesn't have your authorization token in it");
                            }
                        } catch (TwitterException e3) {
                            throw new TwitterAuthorizationException("Can't figure out if twitter actually authorized you: " + e3.getMessage());
                        }
                    } catch (TwitterException e4) {
                        throw new TwitterAuthorizationException("Twitter authorization failed. " + e4.getMessage());
                    }
                } catch (StringIndexOutOfBoundsException e5) {
                    throw new TwitterAuthorizationException("Twitter's authorization page is missing the required form. Not good.");
                }
            } catch (TwitterException e6) {
                throw new TwitterAuthorizationException("Can't figure out the response from twitter: " + e6.getMessage());
            }
        } catch (TwitterException e7) {
            throw new TwitterAuthorizationException("Authorization url get failed: " + e7.getMessage());
        }
    }

    private String catchPattern(String body, String before, String after) {
        int beforeIndex = body.indexOf(before);
        return body.substring(before.length() + beforeIndex, body.indexOf(after, beforeIndex));
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        if (id == 0) {
            return getActivityHelper().createProgressDialog(0, R.string.authenticating);
        }
        return super.onCreateDialog(id);
    }

    public class TwitterAuthorizationException extends Exception {
        private static final long serialVersionUID = -4815349846871197620L;

        public TwitterAuthorizationException(String detailMessage) {
            super(detailMessage);
        }
    }
}
