package com.reverbnation.artistapp.i9585.models;

import android.net.Uri;
import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.google.common.collect.Lists;
import com.reverbnation.artistapp.i9585.models.interfaces.FacebookShareable;
import com.reverbnation.artistapp.i9585.utils.FacebookHelper;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.codehaus.jackson.annotate.JsonProperty;

public class Photoset implements Serializable {
    private static final long serialVersionUID = -2142324074236181078L;
    @JsonProperty("results")
    private ArrayList<Photo> photos;

    public static class Photo implements Serializable, FacebookShareable {
        private static final long serialVersionUID = -3156086038698730500L;
        @JsonProperty("image_height")
        private int height;
        @JsonProperty("id")
        private int id;
        @JsonProperty("image")
        private String imageUrl;
        @JsonProperty("name")
        private String name;
        @JsonProperty("image_thumb")
        private String thumbnailUrl;
        @JsonProperty("image_width")
        private int width;

        public int getId() {
            return this.id;
        }

        public int getWidth() {
            return this.width;
        }

        public int getHeight() {
            return this.height;
        }

        public String getThumbnailUrl() {
            return this.thumbnailUrl;
        }

        public void setThumbnailUrl(String thumbnailUrl2) {
            this.thumbnailUrl = thumbnailUrl2;
        }

        public String getImageUrl() {
            return this.imageUrl;
        }

        public void setImageUrl(String imageUrl2) {
            this.imageUrl = imageUrl2;
        }

        public String getName() {
            return this.name;
        }

        public String toString() {
            return Objects.toStringHelper(this).add("image", getImageUrl()).add("width", Integer.valueOf(getWidth())).add("height", Integer.valueOf(getHeight())).toString();
        }

        public String getPhotoLink(String artistId) {
            return new Uri.Builder().scheme("http").path("//www.reverbnation.com").appendPath(artistId).appendEncodedPath("#!/page_object/page_object_photos/artist_" + artistId).build().toString();
        }

        public String getFacebookName(FacebookHelper helper) {
            return helper.getFacebookName(this);
        }

        public String getFacebookLink(FacebookHelper helper) {
            return helper.getFacebookLink(this);
        }

        public String getFacebookSource(FacebookHelper helper) {
            return helper.getFacebookSource(this);
        }

        public String getFacebookPicture(FacebookHelper helper) {
            return helper.getFacebookPicture();
        }

        public String getFacebookCaption(FacebookHelper helper) {
            return helper.getFacebookCaption(this);
        }
    }

    public List<Photo> getPhotos() {
        return this.photos;
    }

    public Photo getPhotoAt(int index) {
        return this.photos.get(index);
    }

    public List<String> getPhotoUrls() {
        return Lists.transform(getPhotos(), new Function<Photo, String>() {
            public String apply(Photo photo) {
                return photo.getImageUrl();
            }
        });
    }

    public List<String> getThumbnailUrls() {
        return Lists.transform(getPhotos(), new Function<Photo, String>() {
            public String apply(Photo photo) {
                return photo.getThumbnailUrl();
            }
        });
    }

    public String toString() {
        return Objects.toStringHelper(this).add("photos", this.photos).toString();
    }
}
