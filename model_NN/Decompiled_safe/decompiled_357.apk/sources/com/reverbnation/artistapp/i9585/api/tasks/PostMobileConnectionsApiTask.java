package com.reverbnation.artistapp.i9585.api.tasks;

import android.content.Context;
import android.os.AsyncTask;
import com.reverbnation.artistapp.i9585.api.ReverbNationApi;
import com.reverbnation.artistapp.i9585.models.MobileConnection;
import com.roobit.restkit.RestClient;
import com.roobit.restkit.Result;
import java.io.IOException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

public class PostMobileConnectionsApiTask extends AsyncTask<Object, Void, MobileConnection> {
    private PostMobileConnectionsApiDelegate delegate;

    public interface PostMobileConnectionsApiDelegate {
        void mobileConnectionsPostCancelled();

        void mobileConnectionsPostFinished(MobileConnection mobileConnection);

        void mobileConnectionsPostStarted();
    }

    public PostMobileConnectionsApiTask(PostMobileConnectionsApiDelegate delegate2) {
        this.delegate = delegate2;
    }

    /* access modifiers changed from: protected */
    public MobileConnection doInBackground(Object... args) {
        Result result = RestClient.getClientWithBaseUrl(ReverbNationApi.getBaseUrl((String) args[0])).postHeaders(ReverbNationApi.getInstance().getRequiredHeaderParameters((Context) args[1])).setResource("mobile_connections.json").synchronousExecute();
        if (!result.isSuccess()) {
            return null;
        }
        try {
            return (MobileConnection) RestClient.getObjectMapper().readValue(result.getResponse(), MobileConnection.class);
        } catch (JsonParseException e) {
            e.printStackTrace();
            return null;
        } catch (JsonMappingException e2) {
            e2.printStackTrace();
            return null;
        } catch (IOException e3) {
            e3.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        this.delegate.mobileConnectionsPostCancelled();
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(MobileConnection result) {
        this.delegate.mobileConnectionsPostFinished(result);
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        this.delegate.mobileConnectionsPostStarted();
    }
}
