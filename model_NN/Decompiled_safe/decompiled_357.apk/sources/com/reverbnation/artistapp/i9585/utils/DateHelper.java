package com.reverbnation.artistapp.i9585.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateHelper {
    public static Date parse(String date) {
        try {
            return new Date(date);
        } catch (IllegalArgumentException e) {
            try {
                return parseDate(date);
            } catch (ParseException e2) {
                return new Date();
            }
        }
    }

    private static Date parseDate(String date) throws ParseException {
        return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").parse(date);
    }
}
