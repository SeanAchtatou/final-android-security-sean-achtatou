package com.reverbnation.artistapp.i9585.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.models.ShowList;
import java.util.List;

public class ShowsAdapter extends ArrayAdapter<ShowList.Show> {
    private LayoutInflater inflater;
    private int resource;
    private List<ShowList.Show> shows;

    public ShowsAdapter(Context context, int resource2, List<ShowList.Show> shows2) {
        super(context, 0, shows2);
        this.resource = resource2;
        this.shows = shows2;
        this.inflater = (LayoutInflater) context.getSystemService("layout_inflater");
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            v = this.inflater.inflate(this.resource, (ViewGroup) null);
        }
        boolean isEven = position % 2 == 0;
        ShowList.Show show = this.shows.get(position);
        TextView showDateText = (TextView) v.findViewById(R.id.show_date);
        showDateText.setText(ShowList.getFormattedShowTime(show.getShowTime()));
        showDateText.setTextColor(isEven ? getColor(R.color.CellEvenTextPrimary) : getColor(R.color.CellOddTextPrimary));
        TextView showVenueText = (TextView) v.findViewById(R.id.show_venue);
        showVenueText.setText(ShowList.getFormattedVenue(show.getCity(), show.getState(), show.getCountry()));
        showVenueText.setTextColor(isEven ? getColor(R.color.CellEvenTextSecondary) : getColor(R.color.CellOddTextSecondary));
        ((ImageView) v.findViewById(R.id.list_item_background)).setImageResource(isEven ? R.drawable.background_row_even : R.drawable.background_row_odd);
        return v;
    }

    private int getColor(int colorResId) {
        return getContext().getResources().getColor(colorResId);
    }
}
