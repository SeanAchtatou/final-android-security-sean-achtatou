package com.reverbnation.artistapp.i9585.activities;

import android.os.Bundle;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.ReverbNationApplication;
import com.reverbnation.artistapp.i9585.models.ShowList;
import com.reverbnation.artistapp.i9585.utils.AnalyticsHelper;
import com.reverbnation.artistapp.i9585.views.ShowOverlayItem;
import com.reverbnation.artistapp.i9585.views.UpcomingShowsOverlay;

public class MapShowsActivity extends MapActivity {
    private MapView mapView;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        MapShowsActivity.super.onCreate(savedInstanceState);
        setContentView(R.layout.map_shows_activity);
        setupViews();
        addOverlays();
    }

    private void setupViews() {
        this.mapView = findViewById(R.id.map_view);
        this.mapView.setBuiltInZoomControls(true);
    }

    private void addOverlays() {
        ReverbNationApplication app = ReverbNationApplication.getInstance();
        UpcomingShowsOverlay showsOverlay = new UpcomingShowsOverlay(getResources().getDrawable(R.drawable.icon_pushpin), this.mapView);
        for (ShowList.Show show : ShowList.getUpcomingShows(app.getUpcomingShowList().getShows())) {
            ShowList.Show.Venue.Location location = show.getVenue().getLocation();
            showsOverlay.addOverlay(new ShowOverlayItem(new GeoPoint((int) (location.getLatitude() * 1000000.0f), (int) (location.getLongitude() * 1000000.0f)), show));
        }
        this.mapView.getOverlays().add(showsOverlay);
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [android.content.Context, com.reverbnation.artistapp.i9585.activities.MapShowsActivity, com.google.android.maps.MapActivity] */
    /* access modifiers changed from: protected */
    public void onResume() {
        AnalyticsHelper.getInstance(this).trackPageView("homescreen/shows/map");
        MapShowsActivity.super.onResume();
    }

    /* access modifiers changed from: protected */
    public boolean isRouteDisplayed() {
        return false;
    }
}
