package com.reverbnation.artistapp.i9585.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Spanned;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.google.common.base.Strings;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.adapters.GalleryAdapter;
import com.reverbnation.artistapp.i9585.api.FacebookApi;
import com.reverbnation.artistapp.i9585.api.tasks.FacebookWallPostApiTask;
import com.reverbnation.artistapp.i9585.models.FacebookResponse;
import com.reverbnation.artistapp.i9585.models.Photoset;
import com.reverbnation.artistapp.i9585.models.interfaces.FacebookShareable;
import com.reverbnation.artistapp.i9585.utils.ActivityHelper;
import com.reverbnation.artistapp.i9585.utils.AnalyticsHelper;
import com.reverbnation.artistapp.i9585.utils.DrawableManager;
import com.reverbnation.artistapp.i9585.utils.LinkShortener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import org.codehaus.jackson.util.MinimalPrettyPrinter;
import twitter4j.TwitterException;

public class FilmstripActivity extends BaseActivity {
    private static final int EMAIL_PHOTO_ITEM = 3;
    private static final int SAVE_TO_DEVICE_ITEM = 0;
    private static final int SHARE_FB_FAILED_DIALOG = 5;
    private static final int SHARE_FB_PROGRESS_DIALOG = 3;
    private static final int SHARE_FB_SUCCESS_DIALOG = 4;
    private static final int SHARE_ON_FACEBOOK_ITEM = 2;
    private static final int SHARE_ON_TWITTER_ITEM = 1;
    private static final int SHARE_TWITTER_FAILED_DIALOG = 2;
    private static final int SHARE_TWITTER_PROGRESS_DIALOG = 0;
    private static final int SHARE_TWITTER_SUCCESS_DIALOG = 1;
    protected static final String TAG = "FilmstripActivity";
    /* access modifiers changed from: private */
    public TextView captionText;
    private ViewGroup frameFooter;
    private ViewGroup frameHeader;
    /* access modifiers changed from: private */
    public Gallery gallery;
    /* access modifiers changed from: private */
    public boolean isFirstFrame = true;
    private boolean isFrameOn = false;
    /* access modifiers changed from: private */
    public TextView navigationText;

    /* access modifiers changed from: protected */
    public void onMusicServiceConnected() {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.filmstrip_activity);
        setupViews();
        setupGallery();
    }

    private void setupViews() {
        this.frameHeader = (ViewGroup) findViewById(R.id.filmstrip_header_layout);
        this.frameFooter = (ViewGroup) findViewById(R.id.filmstrip_footer_layout);
        this.captionText = (TextView) findViewById(R.id.caption_text);
        this.navigationText = (TextView) findViewById(R.id.navigation_text);
        this.gallery = (Gallery) findViewById(R.id.photo_gallery);
        this.gallery.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                Photoset.Photo photo = (Photoset.Photo) FilmstripActivity.this.gallery.getItemAtPosition(position);
                FilmstripActivity.this.navigationText.setText((position + 1) + " of " + FilmstripActivity.this.gallery.getCount());
                FilmstripActivity.this.captionText.setText(Strings.isNullOrEmpty(photo.getName()) ? "" : photo.getName());
                FilmstripActivity.this.captionText.startAnimation(AnimationUtils.loadAnimation(FilmstripActivity.this.getBaseContext(), 17432576));
                if (FilmstripActivity.this.isFirstFrame) {
                    FilmstripActivity.this.toggleFrame();
                    boolean unused = FilmstripActivity.this.isFirstFrame = false;
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
                FilmstripActivity.this.navigationText.setText("");
                FilmstripActivity.this.captionText.setText("");
            }
        });
        this.gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                FilmstripActivity.this.toggleFrame();
            }
        });
        registerForContextMenu(this.gallery);
    }

    /* access modifiers changed from: protected */
    public void toggleFrame() {
        setFrame(!this.isFrameOn);
    }

    private void setFrame(boolean b) {
        int animationId = b ? 17432576 : 17432577;
        int visibility = b ? 0 : 4;
        this.frameHeader.startAnimation(AnimationUtils.loadAnimation(this, animationId));
        this.frameFooter.startAnimation(AnimationUtils.loadAnimation(this, animationId));
        this.frameHeader.setVisibility(visibility);
        this.frameFooter.setVisibility(visibility);
        this.isFrameOn = b;
    }

    private void setupGallery() {
        int startPhotoIndex = getIntent().getIntExtra("startPhotoIndex", 0);
        this.gallery.setAdapter((SpinnerAdapter) new GalleryAdapter(this, ((Photoset) getIntent().getSerializableExtra("photoset")).getPhotos(), ImageView.ScaleType.FIT_CENTER));
        this.gallery.setSelection(startPhotoIndex);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        AnalyticsHelper.getInstance(this).trackPageView("homescreen/photos_and_videos/photos/photo_details");
        super.onResume();
    }

    public void onShareItemClicked(View v) {
        registerForContextMenu(v);
        openContextMenu(v);
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.setHeaderTitle(getString(R.string.share_photo));
        menu.add(0, 0, 0, getString(R.string.save_to_device));
        menu.add(0, 1, 0, getString(R.string.share_on_twitter));
        menu.add(0, 2, 0, getString(R.string.share_on_facebook));
        menu.add(0, 3, 0, getString(R.string.email_photo));
    }

    public boolean onContextItemSelected(MenuItem item) {
        Photoset.Photo photo = (Photoset.Photo) this.gallery.getSelectedItem();
        switch (item.getItemId()) {
            case 0:
                AnalyticsHelper.getInstance(this).trackEvent("device", "save", "photo");
                try {
                    MediaStore.Images.Media.insertImage(getContentResolver(), locateCachedFile(photo).getAbsolutePath(), (String) null, (String) null);
                    break;
                } catch (FileNotFoundException e) {
                    Toast.makeText(this, (int) R.string.photo_not_found, 1).show();
                    break;
                }
            case 1:
                AnalyticsHelper.getInstance(this).trackEvent("sharing", "twitter", "photo");
                onShareByTwitter(photo);
                break;
            case 2:
                AnalyticsHelper.getInstance(this).trackEvent("sharing", "facebook", "photo");
                onShareByFacebook(photo);
                break;
            case 3:
                AnalyticsHelper.getInstance(this).trackEvent("sharing", "email", "photo");
                onShareByEmail(photo);
                break;
            default:
                return false;
        }
        return true;
    }

    /* access modifiers changed from: private */
    public File locateCachedFile(Photoset.Photo photo) throws FileNotFoundException {
        return DrawableManager.getInstance(this).openImageFileByUrl(photo.getImageUrl());
    }

    private void onShareByTwitter(Photoset.Photo photo) {
        if (!getActivityHelper().isTwitterAuthorized()) {
            getActivityHelper().getTwitterHelper().beginTwitterAuthentication();
        } else {
            shareOnTwitter(photo);
        }
    }

    private void shareOnTwitter(Photoset.Photo photo) {
        showDialog(0);
        final ActivityHelper helper = getActivityHelper();
        helper.getApplication().shorten(new String[]{photo.getPhotoLink(helper.getArtistId()), helper.getApplication().getMarketplaceLink()}, new LinkShortener.ShortenListener() {
            public void onLinkReady(List<String> shortened) {
                final String tweet = helper.getTwitterHelper().getPhotoShareText(shortened.get(0), shortened.get(1));
                new AsyncTask<Void, Void, Boolean>() {
                    /* access modifiers changed from: protected */
                    public Boolean doInBackground(Void... args) {
                        try {
                            helper.getTwitter().updateStatus(tweet);
                            return true;
                        } catch (TwitterException e) {
                            Log.v(FilmstripActivity.TAG, "Failed to share video on twitter" + e);
                            return false;
                        }
                    }

                    /* access modifiers changed from: protected */
                    public void onPostExecute(Boolean result) {
                        FilmstripActivity.this.getActivityHelper().dismissDialog(0);
                        FilmstripActivity.this.showDialog(result.booleanValue() ? 1 : 2);
                    }
                }.execute(new Void[0]);
            }
        });
    }

    private void onShareByFacebook(final Photoset.Photo photo) {
        Facebook facebook = FacebookApi.getInstance(this);
        if (!Strings.isNullOrEmpty(facebook.getAccessToken())) {
            shareOnFacebook(photo);
        } else {
            facebook.authorize(this, FacebookApi.getRequiredPermissions(), new Facebook.DialogListener() {
                public void onComplete(Bundle values) {
                    FacebookApi.setAccessToken(values.getString(Facebook.TOKEN));
                    FilmstripActivity.this.shareOnFacebook(photo);
                }

                public void onFacebookError(FacebookError e) {
                }

                public void onError(DialogError e) {
                }

                public void onCancel() {
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void shareOnFacebook(Photoset.Photo photo) {
        AnalyticsHelper.getInstance(this).trackEvent("sharing", "facebook", "photo");
        showDialog(3);
        try {
            new FacebookWallPostApiTask(this) {
                /* access modifiers changed from: protected */
                public void onPostExecute(FacebookResponse response) {
                    FilmstripActivity.this.getActivityHelper().dismissDialog(3);
                    if (response.isSuccessful()) {
                        FilmstripActivity.this.showDialog(4);
                    } else {
                        FilmstripActivity.this.showDialog(5);
                    }
                }
            }.execute(new FacebookShareable[]{photo});
        } catch (IllegalStateException e) {
            Log.e(TAG, e.toString());
            showDialog(5);
        }
    }

    private void onShareByEmail(final Photoset.Photo photo) {
        final String subject = getEmailSubject(photo);
        getActivityHelper().getApplication().shorten(new String[]{photo.getPhotoLink(getActivityHelper().getArtistId())}, new LinkShortener.ShortenListener() {
            public void onLinkReady(List<String> links) {
                try {
                    FilmstripActivity.this.startEmailActivity(subject, FilmstripActivity.this.getEmailBody(links.get(0)), FilmstripActivity.this.locateCachedFile(photo));
                } catch (FileNotFoundException e) {
                    Log.e(FilmstripActivity.TAG, "Can't find file to share");
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public Spanned getEmailBody(String link) {
        return getActivityHelper().getEmailHelper().getShareBody(this, link, link);
    }

    private String getEmailSubject(Photoset.Photo photo) {
        return getString(R.string.check_out_photos_from) + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + getString(R.string.artist_name);
    }

    /* access modifiers changed from: protected */
    public void startEmailActivity(String subject, Spanned body, File imageFile) {
        getActivityHelper().getEmailHelper().startShareActivity(this, subject, body, imageFile);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        Dialog dialog = null;
        switch (id) {
            case 0:
                dialog = getActivityHelper().createProgressDialog(R.string.share_photo, R.string.sharing_photo_to_twitter);
                break;
            case 1:
            case 4:
                dialog = getActivityHelper().createDialog((int) R.string.share_photo, (int) R.string.photo_was_shared);
                break;
            case 2:
                dialog = getActivityHelper().createDialog((int) R.string.share_photo, (int) R.string.sharing_photo_to_twitter_failed);
                break;
            case 3:
                dialog = getActivityHelper().createProgressDialog(R.string.share_photo, R.string.sharing_photo_to_facebook);
                break;
            case 5:
                dialog = getActivityHelper().createDialog((int) R.string.share_photo, (int) R.string.sharing_photo_to_facebook_failed);
                break;
        }
        if (dialog == null) {
            return super.onCreateDialog(id);
        }
        return dialog;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        FacebookApi.getInstance(this).authorizeCallback(requestCode, resultCode, data);
        if (requestCode != 100) {
            return;
        }
        if (resultCode == -1) {
            shareOnTwitter((Photoset.Photo) this.gallery.getSelectedItem());
        } else {
            getActivityHelper().getTwitterHelper().showTwitterAuthenticationError(data);
        }
    }

    public void onNextItemClicked(View v) {
        int position = this.gallery.getSelectedItemPosition() + 1;
        if (position < this.gallery.getCount()) {
            this.gallery.setSelection(position);
        }
    }

    public void onPreviousItemClicked(View v) {
        int position = this.gallery.getSelectedItemPosition() - 1;
        if (this.gallery.getCount() > 0 && position >= 0) {
            this.gallery.setSelection(position);
        }
    }
}
