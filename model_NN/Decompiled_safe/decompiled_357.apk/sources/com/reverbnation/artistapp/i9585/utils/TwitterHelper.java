package com.reverbnation.artistapp.i9585.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;
import com.google.common.base.Strings;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.ReverbNationApplication;
import com.reverbnation.artistapp.i9585.activities.TwitterLoginActivity;
import com.reverbnation.artistapp.i9585.models.BlogPost;
import com.reverbnation.artistapp.i9585.models.ShowList;
import com.reverbnation.artistapp.i9585.models.SongList;
import com.reverbnation.artistapp.i9585.models.TwitterInfo;
import java.text.SimpleDateFormat;
import org.codehaus.jackson.org.objectweb.asm.Opcodes;
import org.codehaus.jackson.util.MinimalPrettyPrinter;

public class TwitterHelper {
    public static final int AUTHENTICATE_REQUEST = 100;
    private static final String SCREEN_NAME_PREF = "TwitterScreenName";
    private Activity activity;

    public TwitterHelper(Activity activity2) {
        this.activity = activity2;
    }

    public void beginTwitterAuthentication() {
        this.activity.startActivityForResult(new Intent(this.activity, TwitterLoginActivity.class), 100);
    }

    public String getShareText(SongList.Song song, String url, String appUrl) {
        return validTweet(this.activity.getString(R.string.listen_to) + " '" + song.getName() + "' " + this.activity.getString(R.string.from) + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + getTwitterHandleOrArtistName() + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + url + ". " + getFreeAppText(appUrl));
    }

    public String getShareText(BlogPost post, String url, String appUrl) {
        return validTweet(this.activity.getString(R.string.check_out_blog_posts_from) + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + getTwitterHandleOrArtistName() + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + url + ". " + getFreeAppText(appUrl));
    }

    public String getLinkShareText(String linkName, String linkUrl, String appUrl) {
        return validTweet(this.activity.getString(R.string.check_out) + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + getTwitterHandleOrArtistName() + " on " + linkName + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + linkUrl + ". " + getFreeAppText(appUrl));
    }

    public String getPhotoShareText(String photosUrl, String appUrl) {
        return validTweet(this.activity.getString(R.string.check_out_photos_from) + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + getTwitterHandleOrArtistName() + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + photosUrl + ". " + getFreeAppText(appUrl));
    }

    public String getShowShareText(ShowList.Show show, String showUrl, String appUrl) {
        StringBuilder sb = new StringBuilder(this.activity.getString(R.string.check_out)).append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR).append(getTwitterHandleOrArtistName()).append(" live ");
        if (show.hasShowTime()) {
            sb.append("on ").append(show.getShowTime(new SimpleDateFormat("MMM dd"))).append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
        }
        return validTweet(sb.append(showUrl).append(". ").append(getFreeAppText(appUrl)).toString());
    }

    public String getTwitterHandleOrArtistName() {
        TwitterInfo twitter = ((ReverbNationApplication) this.activity.getApplication()).getMobileApplication().getPageObject().getTwitterInfo();
        if (!hasTwitterHandle(twitter)) {
            return this.activity.getString(R.string.artist_name);
        }
        return String.format("@%1$s", twitter.getUsername());
    }

    public String getScreenName() {
        return ((ReverbNationApplication) this.activity.getApplication()).getApplicationPreferences().getString(SCREEN_NAME_PREF, "");
    }

    public void cacheScreenName(String screenName) {
        SharedPreferences.Editor edit = ((ReverbNationApplication) this.activity.getApplication()).getApplicationPreferences().edit();
        edit.putString(SCREEN_NAME_PREF, screenName);
        edit.commit();
    }

    private boolean hasTwitterHandle(TwitterInfo twitter) {
        return twitter != null && !Strings.isNullOrEmpty(twitter.getUsername());
    }

    private String getFreeAppText(String appUrl) {
        return this.activity.getString(R.string.free_app_powered_by) + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + appUrl;
    }

    public void toastResultNotOK(int resultCode) {
        Toast.makeText(this.activity, resultCode == 0 ? R.string.twitter_authentication_cancelled : R.string.twitter_authentication_failed, 1).show();
    }

    private String validTweet(String tweet) {
        return tweet.length() > 140 ? tweet.substring(0, Opcodes.L2F) + "..." : tweet;
    }

    public void showTwitterAuthenticationError(Intent data) {
        String errorString = data.getStringExtra("errorString");
        if (!Strings.isNullOrEmpty(errorString)) {
            new AlertDialog.Builder(this.activity).setTitle((int) R.string.twitter_authentication_failed).setMessage(errorString).setNeutralButton(17039370, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).show();
        }
    }
}
