package com.reverbnation.artistapp.i9585.models;

import org.codehaus.jackson.annotate.JsonProperty;

public class MobileApplicationPlatform {
    @JsonProperty("mobile_application")
    private MobileApplication mobileApplication;
    @JsonProperty("store_url")
    private String storeUrl;
    @JsonProperty("version_minimum")
    private String versionMinimum;

    public MobileApplication getMobileApplication() {
        return this.mobileApplication;
    }

    public String getVersionMinimum() {
        return this.versionMinimum;
    }

    public void setVersionMinimum(String versionMinimum2) {
        this.versionMinimum = versionMinimum2;
    }

    public String getStoreUrl() {
        return this.storeUrl;
    }
}
