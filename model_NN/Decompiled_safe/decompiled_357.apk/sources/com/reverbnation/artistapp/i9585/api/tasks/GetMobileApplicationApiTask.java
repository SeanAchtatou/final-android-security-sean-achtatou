package com.reverbnation.artistapp.i9585.api.tasks;

import android.content.Context;
import android.os.AsyncTask;
import com.reverbnation.artistapp.i9585.api.ReverbNationApi;
import com.reverbnation.artistapp.i9585.models.MobileApplication;
import com.roobit.restkit.RestClient;
import com.roobit.restkit.Result;
import java.io.IOException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

public class GetMobileApplicationApiTask extends AsyncTask<Object, Void, MobileApplication> {
    private GetMobileApplicationApiDelegate delegate;

    public interface GetMobileApplicationApiDelegate {
        void getMobileApplicationCancelled();

        void getMobileApplicationFinished(MobileApplication mobileApplication);

        void getMobileApplicationStarted();
    }

    public GetMobileApplicationApiTask(GetMobileApplicationApiDelegate delegate2) {
        this.delegate = delegate2;
    }

    /* access modifiers changed from: protected */
    public MobileApplication doInBackground(Object... args) {
        Result result = RestClient.getClientWithBaseUrl(ReverbNationApi.getBaseUrl()).get(ReverbNationApi.getInstance().getRequiredHeaderParameters((Context) args[1])).setResource("mobile_application_platform/" + ((String) args[0]) + ".json").synchronousExecute();
        if (!result.isSuccess()) {
            return null;
        }
        try {
            return (MobileApplication) RestClient.getObjectMapper().readValue(result.getResponse(), MobileApplication.class);
        } catch (JsonParseException e) {
            e.printStackTrace();
            return null;
        } catch (JsonMappingException e2) {
            e2.printStackTrace();
            return null;
        } catch (IOException e3) {
            e3.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        this.delegate.getMobileApplicationStarted();
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        this.delegate.getMobileApplicationCancelled();
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(MobileApplication result) {
        this.delegate.getMobileApplicationFinished(result);
    }
}
