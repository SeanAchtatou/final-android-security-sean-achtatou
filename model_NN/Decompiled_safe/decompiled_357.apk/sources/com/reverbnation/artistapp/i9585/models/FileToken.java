package com.reverbnation.artistapp.i9585.models;

import org.codehaus.jackson.annotate.JsonProperty;

public class FileToken {
    @JsonProperty("rk")
    public String randomToken;
    @JsonProperty("tk")
    public String timeToken;
}
