package com.reverbnation.artistapp.i9585.api.tasks;

import android.content.Context;
import android.os.AsyncTask;
import com.reverbnation.artistapp.i9585.api.ReverbNationApi;
import com.reverbnation.artistapp.i9585.models.FileToken;
import com.roobit.restkit.RestClient;
import com.roobit.restkit.Result;
import java.io.IOException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

public class GetFileTokensApiTask extends AsyncTask<Object, Void, FileToken> {
    private GetFileTokensApiDelegate delegate;

    public interface GetFileTokensApiDelegate {
        void getFileTokensCancelled();

        void getFileTokensFinished(FileToken fileToken);

        void getFileTokensStarted();
    }

    public GetFileTokensApiTask(GetFileTokensApiDelegate delegate2) {
        this.delegate = delegate2;
    }

    /* access modifiers changed from: protected */
    public FileToken doInBackground(Object... args) {
        Result result = RestClient.getClientWithBaseUrl(ReverbNationApi.getBaseUrl((String) args[0])).get(ReverbNationApi.getInstance().getRequiredHeaderParameters((Context) args[1])).setResource("songs/file_tokens.json").synchronousExecute();
        if (!result.isSuccess()) {
            return null;
        }
        try {
            return (FileToken) RestClient.getObjectMapper().readValue(result.getResponse(), FileToken.class);
        } catch (JsonParseException e) {
            e.printStackTrace();
            return null;
        } catch (JsonMappingException e2) {
            e2.printStackTrace();
            return null;
        } catch (IOException e3) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        this.delegate.getFileTokensCancelled();
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(FileToken token) {
        this.delegate.getFileTokensFinished(token);
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        this.delegate.getFileTokensStarted();
    }
}
