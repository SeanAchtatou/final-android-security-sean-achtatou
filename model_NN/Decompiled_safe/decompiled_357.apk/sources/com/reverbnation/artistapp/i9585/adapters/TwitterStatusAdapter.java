package com.reverbnation.artistapp.i9585.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.Html;
import android.text.format.DateUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.reverbnation.artistapp.i9585.R;
import java.util.List;
import twitter4j.Status;
import twitter4j.User;

public class TwitterStatusAdapter extends ArrayAdapter<Status> {
    private int resource;
    private Bitmap thumbnail;

    public TwitterStatusAdapter(Context context, int resource2, List<Status> statuses, Bitmap thumbnail2) {
        super(context, resource2, statuses);
        this.resource = resource2;
        this.thumbnail = thumbnail2;
    }

    public View getView(int position, View convert, ViewGroup parent) {
        boolean isEven;
        View v = convert;
        if (v == null) {
            v = View.inflate(getContext(), this.resource, null);
        }
        if (position % 2 == 0) {
            isEven = true;
        } else {
            isEven = false;
        }
        Status status = (Status) getItem(position);
        User user = status.getUser();
        ImageView profileImage = (ImageView) v.findViewById(R.id.profile_image);
        if (this.thumbnail != null) {
            profileImage.setImageBitmap(this.thumbnail);
        }
        TextView screenNameText = (TextView) v.findViewById(R.id.screen_name_text);
        screenNameText.setText(user.getName());
        screenNameText.setTextColor(isEven ? getColor(R.color.CellEvenTextPrimary) : getColor(R.color.CellOddTextPrimary));
        TextView dateText = (TextView) v.findViewById(R.id.date_text);
        dateText.setText(DateUtils.getRelativeTimeSpanString(status.getCreatedAt().getTime(), System.currentTimeMillis(), 60000));
        dateText.setTextColor(isEven ? getColor(R.color.CellEvenTextPrimary) : getColor(R.color.CellOddTextPrimary));
        TextView tweetText = (TextView) v.findViewById(R.id.tweet_text);
        tweetText.setText(Html.fromHtml(status.getText()).toString());
        tweetText.setTextColor(isEven ? getColor(R.color.CellEvenTextPrimary) : getColor(R.color.CellOddTextPrimary));
        ((ImageView) v.findViewById(R.id.list_item_background)).setImageResource(isEven ? R.drawable.background_row_even : R.drawable.background_row_odd);
        return v;
    }

    private int getColor(int colorResId) {
        return getContext().getResources().getColor(colorResId);
    }
}
