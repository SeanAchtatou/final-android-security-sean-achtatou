package com.reverbnation.artistapp.i9585.utils;

import android.content.Context;
import android.os.StrictMode;

public class StrictModeWrapper {
    public static void init(Context context) {
        if ((context.getApplicationInfo().flags & 2) != 0) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectDiskReads().detectDiskWrites().detectNetwork().penaltyLog().build());
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectLeakedSqlLiteObjects().penaltyLog().penaltyDeath().build());
        }
    }
}
