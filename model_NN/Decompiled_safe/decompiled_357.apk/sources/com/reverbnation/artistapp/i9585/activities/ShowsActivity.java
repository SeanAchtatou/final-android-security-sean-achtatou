package com.reverbnation.artistapp.i9585.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;
import com.reverbnation.artistapp.i9585.R;

public class ShowsActivity extends BaseTabActivity {
    private static final String MapTab = "map_tab";
    private static final String PastTab = "past_tab";
    private static final String UpcomingTab = "upcoming_tab";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(7);
        setContentView((int) R.layout.shows_activity);
        getActivityHelper().setArtistTitlebar(getString(R.string.shows));
        setupTabs();
    }

    private void setupTabs() {
        TabHost tabHost = getTabHost();
        boolean hasUpcomingShows = getActivityHelper().getApplication().artistHasUpcomingShows();
        tabHost.addTab(tabHost.newTabSpec(UpcomingTab).setIndicator(createTabView(R.string.upcoming)).setContent(new Intent(this, UpcomingShowsActivity.class)));
        if (hasUpcomingShows) {
            tabHost.addTab(tabHost.newTabSpec(MapTab).setIndicator(createTabView(R.string.map)).setContent(new Intent(this, MapShowsActivity.class)));
        }
        tabHost.addTab(tabHost.newTabSpec(PastTab).setIndicator(createTabView(R.string.past)).setContent(new Intent(this, PastShowsActivity.class)));
    }
}
