package com.reverbnation.artistapp.i9585.models;

import com.urbanairship.analytics.EventDataManager;
import org.codehaus.jackson.annotate.JsonProperty;

public class FacebookResponseError {
    @JsonProperty("message")
    private String message;
    @JsonProperty(EventDataManager.Events.COLUMN_NAME_TYPE)
    private String type;

    public void setType(String type2) {
        this.type = type2;
    }

    public String getType() {
        return this.type;
    }

    public void setMessage(String message2) {
        this.message = message2;
    }

    public String getMessage() {
        return this.message;
    }

    public boolean equals(Object o) {
        boolean typesEqual;
        boolean messagesEqual;
        if (o == null) {
            return false;
        }
        if (!(o instanceof FacebookResponseError)) {
            return super.equals(o);
        }
        FacebookResponseError other = (FacebookResponseError) o;
        if (this.type != null) {
            typesEqual = getType().equals(other.getType());
        } else if (other.type == null) {
            typesEqual = true;
        } else {
            typesEqual = false;
        }
        if (this.message != null) {
            messagesEqual = getMessage().equals(other.getMessage());
        } else if (other.message == null) {
            messagesEqual = true;
        } else {
            messagesEqual = false;
        }
        if (!typesEqual || !messagesEqual) {
            return false;
        }
        return true;
    }
}
