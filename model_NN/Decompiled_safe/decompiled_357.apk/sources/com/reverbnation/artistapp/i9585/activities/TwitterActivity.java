package com.reverbnation.artistapp.i9585.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.ClipboardManager;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.adapters.TwitterStatusAdapter;
import com.reverbnation.artistapp.i9585.utils.AnalyticsHelper;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.codehaus.jackson.util.MinimalPrettyPrinter;
import twitter4j.IDs;
import twitter4j.ProfileImage;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;

public class TwitterActivity extends BaseListTabChildActivity {
    private static final int CONTEXT_GROUP = 1;
    private static final int COPY_CLIPBOARD_ITEM = 1;
    private static final int HASH_TAG_GROUP = 4;
    private static final int LINK_GROUP = 2;
    private static final int OPEN_ITEM = 3;
    protected static final int PROGRESS_DIALOG = 0;
    private static final int RETWEET_ITEM = 2;
    private static final String TAG = "TwitterActivity";
    private static final int TWITTER_FOLLOW_REQUEST = 0;
    private static final int TWITTER_RETWEET_REQUEST = 1;
    private static final String TWITTER_SEARCH_PATH_FORMAT = "https://api.twitter.com/#!/search/%s";
    private static final String TWITTER_USER_PATH_FORMAT = "http://twitter.com/%s";
    private static final int USER_GROUP = 3;
    private String artistTwitterName;
    private ImageView followButton;
    private long retweetId;
    /* access modifiers changed from: private */
    public Bitmap thumbnail;
    private List<String> tweetHashTags = new ArrayList();
    private List<String> tweetLinks = new ArrayList();
    private List<String> tweetUsers = new ArrayList();
    /* access modifiers changed from: private */
    public ResponseList<Status> tweets;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.twitter_activity);
        setupViews();
        loadTweets();
        registerForContextMenu(getListView());
    }

    private void setupViews() {
        this.followButton = (ImageView) findViewById(R.id.follow_button);
    }

    private void loadTweets() {
        if (artistHasTwitterAccount()) {
            loadArtistTweets(getArtistTwitterName());
        }
    }

    /* access modifiers changed from: private */
    public boolean artistHasTwitterAccount() {
        return !Strings.isNullOrEmpty(getArtistTwitterName());
    }

    public String getArtistTwitterName() {
        if (this.artistTwitterName == null) {
            this.artistTwitterName = getActivityHelper().getApplication().getArtistTwitterName();
            Log.v(TAG, "Artist twitter name: " + this.artistTwitterName);
        }
        return this.artistTwitterName;
    }

    public void loadArtistTweets(String twitterName) {
        showDialog(0);
        new AsyncTask<String, Void, ResponseList<Status>>() {
            /* access modifiers changed from: protected */
            public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
                onPostExecute((ResponseList<Status>) ((ResponseList) x0));
            }

            /* access modifiers changed from: protected */
            public ResponseList<Status> doInBackground(String... args) {
                IOException e;
                MalformedURLException e2;
                TwitterException e3;
                String screenName = args[0];
                Twitter twitter = TwitterActivity.this.getActivityHelper().getTwitter();
                URL url = null;
                try {
                    Log.v(TwitterActivity.TAG, "Loading timeline for " + screenName);
                    ResponseList unused = TwitterActivity.this.tweets = twitter.getUserTimeline(screenName);
                    URL url2 = new URL(twitter.getProfileImage(screenName, ProfileImage.NORMAL).getURL());
                    try {
                        Log.v(TwitterActivity.TAG, "Getting profile image from " + url2.toString());
                        Bitmap unused2 = TwitterActivity.this.thumbnail = BitmapFactory.decodeStream((InputStream) url2.getContent());
                        Log.v(TwitterActivity.TAG, "Finished getting user timeline");
                    } catch (TwitterException e4) {
                        e3 = e4;
                        Log.e(TwitterActivity.TAG, "Failed to get tweets for " + screenName);
                        e3.printStackTrace();
                        return TwitterActivity.this.tweets;
                    } catch (MalformedURLException e5) {
                        e2 = e5;
                        url = url2;
                        Log.e(TwitterActivity.TAG, "Failed to find profile image at " + url.toString());
                        e2.printStackTrace();
                        return TwitterActivity.this.tweets;
                    } catch (IOException e6) {
                        e = e6;
                        Log.e(TwitterActivity.TAG, "Failed to create profile image");
                        e.printStackTrace();
                        return TwitterActivity.this.tweets;
                    }
                } catch (TwitterException e7) {
                    e3 = e7;
                    Log.e(TwitterActivity.TAG, "Failed to get tweets for " + screenName);
                    e3.printStackTrace();
                    return TwitterActivity.this.tweets;
                } catch (MalformedURLException e8) {
                    e2 = e8;
                    Log.e(TwitterActivity.TAG, "Failed to find profile image at " + url.toString());
                    e2.printStackTrace();
                    return TwitterActivity.this.tweets;
                } catch (IOException e9) {
                    e = e9;
                    Log.e(TwitterActivity.TAG, "Failed to create profile image");
                    e.printStackTrace();
                    return TwitterActivity.this.tweets;
                }
                return TwitterActivity.this.tweets;
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(ResponseList<Status> responseList) {
                TwitterActivity.this.getActivityHelper().dismissDialog(0);
                TwitterActivity.this.setupListAdapter();
                TwitterActivity.this.setupFollowButton();
            }
        }.execute(twitterName);
    }

    /* access modifiers changed from: protected */
    public void setupListAdapter() {
        if (this.tweets != null) {
            setListAdapter(new TwitterStatusAdapter(this, R.layout.twitter_status, this.tweets, this.thumbnail));
        }
    }

    /* access modifiers changed from: protected */
    public void setupFollowButton() {
        new AsyncTask<Void, Void, Boolean>() {
            /* access modifiers changed from: protected */
            public Boolean doInBackground(Void... noargs) {
                if (!TwitterActivity.this.getActivityHelper().isTwitterAuthorized()) {
                    return false;
                }
                Twitter twitter = TwitterActivity.this.getActivityHelper().getTwitter();
                IDs ids = null;
                long cursor = -1;
                do {
                    try {
                        ids = twitter.getFriendsIDs(cursor);
                        for (long id : ids.getIDs()) {
                            if (id == twitter.getId()) {
                                return true;
                            }
                        }
                    } catch (TwitterException e) {
                        Log.e(TwitterActivity.TAG, "Failed checking following status: " + e);
                    }
                    cursor = ids.getNextCursor();
                } while (cursor != 0);
                return false;
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(Boolean isFollowing) {
                TwitterActivity.this.setFollowVisibility(isFollowing.booleanValue());
            }
        }.execute(new Void[0]);
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        Status selected = (Status) getListAdapter().getItem(position);
        if (selected != null) {
            Iterable<String> iter = tokenize(selected);
            this.tweetLinks = extractLinks(iter.iterator());
            this.tweetHashTags = extractHashTags(iter.iterator());
            this.tweetUsers = extractUsers(iter.iterator());
            v.showContextMenu();
        }
    }

    private Iterable<String> tokenize(Status status) {
        return Splitter.on(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR).split(Uri.decode(status.getText()).toString());
    }

    private List<String> extractLinks(Iterator<String> iter) {
        List<String> urls = new ArrayList<>();
        while (iter.hasNext()) {
            String candidate = iter.next();
            if (URLUtil.isValidUrl(candidate)) {
                urls.add(candidate);
            }
        }
        return urls;
    }

    private List<String> extractHashTags(Iterator<String> iter) {
        List<String> tags = new ArrayList<>();
        while (iter.hasNext()) {
            String candidate = iter.next();
            if (isValidTag(candidate)) {
                tags.add(candidate);
            }
        }
        return tags;
    }

    private boolean isValidTag(String tag) {
        return tag.startsWith("#") && tag.length() > 2;
    }

    private List<String> extractUsers(Iterator<String> iter) {
        List<String> users = new ArrayList<>();
        while (iter.hasNext()) {
            String candidate = iter.next();
            if (isValidUser(candidate)) {
                users.add(candidate);
            }
        }
        return users;
    }

    private boolean isValidUser(String user) {
        return user.startsWith("@") && user.length() > 2;
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        AnalyticsHelper.getInstance(this).trackPageView("homescreen/news/twitter/twitter_details");
        menu.setHeaderTitle((int) R.string.tweet_items);
        menu.add(1, 1, 0, (int) R.string.copy_to_clipboard);
        menu.add(1, 2, 0, (int) R.string.retweet);
        menu.add(1, 3, 0, (int) R.string.open);
        addItemsToContextMenu(menu, 2, this.tweetLinks);
        addItemsToContextMenu(menu, 3, this.tweetUsers);
        addItemsToContextMenu(menu, 4, this.tweetHashTags);
    }

    private void addItemsToContextMenu(ContextMenu menu, int groupId, List<String> values) {
        int itemId = 0;
        for (String add : values) {
            menu.add(groupId, itemId, 0, add);
            itemId++;
        }
        menu.setGroupVisible(groupId, true);
    }

    public boolean onContextItemSelected(MenuItem item) {
        AnalyticsHelper.getInstance(this).trackPageView("homescreen/news/twitter/twitter_details/embedded_item");
        switch (item.getGroupId()) {
            case 1:
                onContextGroupSelected(item);
                break;
            case 2:
                onLinkGroupSelected(item);
                break;
            case 3:
                onUserGroupSelected(item);
                break;
            case 4:
                onHashGroupSelected(item);
                break;
            default:
                return false;
        }
        return true;
    }

    private void onContextGroupSelected(MenuItem item) {
        Status tweet = (Status) getListAdapter().getItem(((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).position);
        switch (item.getItemId()) {
            case 1:
                ((ClipboardManager) getSystemService("clipboard")).setText(tweet.getText());
                Toast.makeText(getApplicationContext(), "Copied item to clipboard", 0);
                return;
            case 2:
                retweet(tweet);
                return;
            case 3:
                openTweet(tweet);
                return;
            default:
                return;
        }
    }

    private void onLinkGroupSelected(MenuItem item) {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(this.tweetLinks.get(item.getItemId()))));
    }

    private void onUserGroupSelected(MenuItem item) {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(String.format(TWITTER_USER_PATH_FORMAT, this.tweetUsers.get(item.getItemId()).substring(1)))));
    }

    private void onHashGroupSelected(MenuItem item) {
        String search = String.format(TWITTER_SEARCH_PATH_FORMAT, Uri.encode(this.tweetHashTags.get(item.getItemId())));
        Log.v(TAG, "Searching twitter: " + search);
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(search)));
    }

    private void openTweet(Status tweet) {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(String.format("http://twitter.com/#!/%s/status/%d", tweet.getUser().getScreenName(), Long.valueOf(tweet.getId())))));
    }

    private void retweet(Status tweet) {
        long statusId = tweet.getId();
        if (getActivityHelper().isTwitterAuthorized()) {
            retweet(statusId);
            return;
        }
        this.retweetId = statusId;
        startActivityForResult(new Intent(this, TwitterLoginActivity.class), 1);
    }

    private void retweet(final long statusId) {
        new Thread(new Runnable() {
            public void run() {
                try {
                    TwitterActivity.this.getActivityHelper().getTwitter().retweetStatus(statusId);
                    TwitterActivity.this.explain(R.string.retweeted);
                    Toast.makeText(TwitterActivity.this.getApplicationContext(), (int) R.string.retweeted, 1).show();
                } catch (TwitterException e) {
                    TwitterActivity.this.explain(R.string.retweet_failed);
                }
            }
        }).start();
    }

    /* access modifiers changed from: protected */
    public void explain(final int resId) {
        runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(TwitterActivity.this.getApplicationContext(), resId, 1).show();
            }
        });
    }

    public void onFollowButtonClicked(View v) {
        if (getActivityHelper().isTwitterAuthorized()) {
            followArtist();
        } else {
            startActivityForResult(new Intent(this, TwitterLoginActivity.class), 0);
        }
    }

    /* access modifiers changed from: protected */
    public void followArtist() {
        new AsyncTask<Void, Void, Boolean>() {
            /* access modifiers changed from: protected */
            public Boolean doInBackground(Void... noargs) {
                try {
                    if (TwitterActivity.this.artistHasTwitterAccount()) {
                        TwitterActivity.this.getActivityHelper().getTwitter().createFriendship(TwitterActivity.this.getArtistTwitterName());
                        return true;
                    }
                } catch (TwitterException e) {
                    Log.v(TwitterActivity.TAG, "Failed to follow: " + e);
                }
                return false;
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(Boolean isFollowing) {
                TwitterActivity.this.setFollowVisibility(isFollowing.booleanValue());
                if (isFollowing.booleanValue()) {
                    Toast.makeText(TwitterActivity.this.getBaseContext(), TwitterActivity.this.getString(R.string.you_are_following) + " @" + TwitterActivity.this.getArtistTwitterName(), 0).show();
                }
            }
        }.execute(new Void[0]);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1) {
            switch (requestCode) {
                case 0:
                    AnalyticsHelper.getInstance(this).trackEvent("social", "twitter", "follow");
                    followArtist();
                    return;
                case 1:
                    AnalyticsHelper.getInstance(this).trackEvent("social", "twitter", "retweet");
                    retweet(this.retweetId);
                    return;
                default:
                    return;
            }
        } else {
            explain(R.string.twitter_authentication_failed);
        }
    }

    /* access modifiers changed from: protected */
    public void setFollowVisibility(boolean isFollowing) {
        this.followButton.setVisibility(isFollowing ? 8 : 0);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        Dialog dialog = null;
        switch (id) {
            case 0:
                dialog = getActivityHelper().createProgressDialog(0, R.string.loading);
                break;
        }
        return dialog != null ? dialog : super.onCreateDialog(id);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        AnalyticsHelper.getInstance(this).trackPageView("homescreen/news/twitter");
        super.onResume();
    }
}
