package com.reverbnation.artistapp.i9585.utils;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

public class LastLocationFinder {
    protected static final String TAG = "LastLocationFinder";
    private Context context;
    private Criteria criteria;
    /* access modifiers changed from: private */
    public LocationListener locationListener;
    /* access modifiers changed from: private */
    public LocationManager locationManager;
    protected LocationListener singleUpdateListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            Log.d(LastLocationFinder.TAG, "Single location update received: " + location.getLatitude() + ", " + location.getLongitude());
            if (!(LastLocationFinder.this.locationListener == null || location == null)) {
                LastLocationFinder.this.locationListener.onLocationChanged(location);
            }
            LastLocationFinder.this.locationManager.removeUpdates(LastLocationFinder.this.singleUpdateListener);
        }

        public void onProviderDisabled(String provider) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };

    public LastLocationFinder(Context context2) {
        this.context = context2;
        this.locationManager = (LocationManager) context2.getSystemService("location");
        this.criteria = new Criteria();
        this.criteria.setAccuracy(2);
    }

    public Location getLastBestLocation(int minDistance, long minTime) {
        String provider;
        Location bestResult = null;
        float bestAccuracy = Float.MAX_VALUE;
        long bestTime = Long.MAX_VALUE;
        for (String provider2 : this.locationManager.getAllProviders()) {
            Location location = this.locationManager.getLastKnownLocation(provider2);
            if (location != null) {
                float accuracy = location.getAccuracy();
                long time = location.getTime();
                if (time < minTime && accuracy < bestAccuracy) {
                    bestResult = location;
                    bestAccuracy = accuracy;
                    bestTime = time;
                } else if (time > minTime && bestAccuracy == Float.MAX_VALUE && time < bestTime) {
                    bestResult = location;
                    bestTime = time;
                }
            }
        }
        if (this.locationListener != null && ((bestTime > minTime || bestAccuracy > ((float) minDistance)) && (provider = this.locationManager.getBestProvider(this.criteria, true)) != null)) {
            this.locationManager.requestLocationUpdates(provider, 0, 0.0f, this.singleUpdateListener, this.context.getMainLooper());
        }
        return bestResult;
    }

    public void setChangedLocationListener(LocationListener listener) {
        this.locationListener = listener;
    }

    public void cancel() {
        this.locationManager.removeUpdates(this.singleUpdateListener);
    }
}
