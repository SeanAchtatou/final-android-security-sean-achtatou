package com.reverbnation.artistapp.i9585.models;

import com.reverbnation.artistapp.i9585.models.ShowList;
import java.util.List;
import org.codehaus.jackson.annotate.JsonProperty;

public class PageObject {
    @JsonProperty("deleted")
    private boolean deleted;
    @JsonProperty("email")
    private String email;
    @JsonProperty("genres")
    private List<String> genres;
    @JsonProperty("id")
    private int id;
    @JsonProperty("image")
    private String image;
    @JsonProperty("image_small")
    private String imageSmall;
    @JsonProperty("image_thumb")
    private String imageThumb;
    @JsonProperty("location")
    private ShowList.Show.Venue.Location location;
    @JsonProperty("name")
    private String name;
    @JsonProperty("twitter")
    private TwitterInfo twitterInfo;
    @JsonProperty("under_construction")
    private boolean underConstruction;

    public TwitterInfo getTwitterInfo() {
        if (this.twitterInfo == null) {
            this.twitterInfo = new TwitterInfo();
        }
        return this.twitterInfo;
    }

    public void setId(int id2) {
        this.id = id2;
    }

    public int getId() {
        return this.id;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getName() {
        return this.name;
    }

    public void setEmail(String email2) {
        this.email = email2;
    }

    public String getEmail() {
        return this.email;
    }

    public void setGenres(List<String> genres2) {
        this.genres = genres2;
    }

    public List<String> getGenres() {
        return this.genres;
    }

    public void setLocation(ShowList.Show.Venue.Location location2) {
        this.location = location2;
    }

    public ShowList.Show.Venue.Location getLocation() {
        return this.location;
    }

    public void setImage(String image2) {
        this.image = image2;
    }

    public String getImage() {
        return this.image;
    }

    public void setImageSmall(String imageSmall2) {
        this.imageSmall = imageSmall2;
    }

    public String getImageSmall() {
        return this.imageSmall;
    }

    public void setImageThumb(String imageThumb2) {
        this.imageThumb = imageThumb2;
    }

    public String getImageThumb() {
        return this.imageThumb;
    }

    public void setUnderConstruction(boolean underConstruction2) {
        this.underConstruction = underConstruction2;
    }

    public boolean isUnderConstruction() {
        return this.underConstruction;
    }

    public void setDeleted(boolean deleted2) {
        this.deleted = deleted2;
    }

    public boolean isDeleted() {
        return this.deleted;
    }
}
