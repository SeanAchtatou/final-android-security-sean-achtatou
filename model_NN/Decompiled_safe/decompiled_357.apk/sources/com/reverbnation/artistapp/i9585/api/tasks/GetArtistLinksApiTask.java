package com.reverbnation.artistapp.i9585.api.tasks;

import android.content.Context;
import android.os.AsyncTask;
import com.reverbnation.artistapp.i9585.api.ReverbNationApi;
import com.reverbnation.artistapp.i9585.models.Links;
import com.roobit.restkit.RestClient;
import com.roobit.restkit.Result;
import java.io.IOException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

public class GetArtistLinksApiTask extends AsyncTask<Object, Void, Links> {
    private GetArtistLinksApiDelegate delegate;

    public interface GetArtistLinksApiDelegate {
        void getArtistLinksCancelled();

        void getArtistLinksFinished(Links links);

        void getArtistLinksStarted();
    }

    public GetArtistLinksApiTask(GetArtistLinksApiDelegate delegate2) {
        this.delegate = delegate2;
    }

    /* access modifiers changed from: protected */
    public Links doInBackground(Object... args) {
        Result result = RestClient.getClientWithBaseUrl(ReverbNationApi.getBaseUrl((String) args[1])).get(ReverbNationApi.getInstance().getRequiredHeaderParameters((Context) args[2])).setResource("artists/" + ((String) args[0]) + "/links.json").synchronousExecute();
        if (!result.isSuccess()) {
            return null;
        }
        try {
            return (Links) RestClient.getObjectMapper().readValue(result.getResponse(), Links.class);
        } catch (JsonParseException e) {
            e.printStackTrace();
            return null;
        } catch (JsonMappingException e2) {
            e2.printStackTrace();
            return null;
        } catch (IOException e3) {
            e3.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        this.delegate.getArtistLinksCancelled();
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Links result) {
        this.delegate.getArtistLinksFinished(result);
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        this.delegate.getArtistLinksStarted();
    }
}
