package com.reverbnation.artistapp.i9585.activities;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.google.common.base.Strings;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.api.FacebookApi;
import com.reverbnation.artistapp.i9585.api.tasks.FacebookWallPostApiTask;
import com.reverbnation.artistapp.i9585.models.FacebookResponse;
import com.reverbnation.artistapp.i9585.models.Videoset;
import com.reverbnation.artistapp.i9585.models.interfaces.FacebookShareable;
import com.reverbnation.artistapp.i9585.utils.AnalyticsHelper;
import com.reverbnation.artistapp.i9585.utils.DrawableManager;
import com.reverbnation.artistapp.i9585.utils.LinkShortener;
import java.util.List;
import org.codehaus.jackson.util.MinimalPrettyPrinter;
import twitter4j.TwitterException;

public class VideoDetailsActivity extends BaseActivity {
    private static final int SHARE_FB_FAILED_DIALOG = 2;
    private static final int SHARE_FB_PROGRESS_DIALOG = 0;
    private static final int SHARE_FB_SUCCESS_DIALOG = 1;
    private static final int SHARE_TWITTER_FAILED_DIALOG = 5;
    private static final int SHARE_TWITTER_PROGRESS_DIALOG = 3;
    private static final int SHARE_TWITTER_SUCCESS_DIALOG = 4;
    private static final String TAG = "VideoDetailsActivity";
    private Videoset.Video video;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(7);
        setContentView((int) R.layout.video_details_activity);
        setupViews();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        Dialog dialog = null;
        switch (id) {
            case 0:
                dialog = getActivityHelper().createProgressDialog(R.string.share_video, R.string.sharing_video_on_facebook);
                break;
            case 1:
            case 4:
                dialog = getActivityHelper().createDialog((int) R.string.share_video, (int) R.string.video_was_shared);
                break;
            case 2:
                dialog = getActivityHelper().createDialog((int) R.string.share_video, (int) R.string.sharing_video_to_facebook_failed);
                break;
            case 3:
                dialog = getActivityHelper().createProgressDialog(R.string.share_video, R.string.sharing_video_on_twitter);
                break;
            case 5:
                dialog = getActivityHelper().createDialog((int) R.string.share_video, (int) R.string.sharing_video_to_twitter_failed);
                break;
        }
        if (dialog != null) {
            return dialog;
        }
        return super.onCreateDialog(id);
    }

    private void setupViews() {
        getActivityHelper().setArtistTitlebar((int) R.string.video_details);
        this.video = (Videoset.Video) getIntent().getSerializableExtra("video");
        ((TextView) findViewById(R.id.artist_text)).setText((int) R.string.artist_name);
        ((TextView) findViewById(R.id.title_text)).setText(this.video.getName());
        DrawableManager.getInstance(this).bindDrawable(this.video.getThumbnailUrl(), (ImageView) findViewById(R.id.video_thumbnail_image));
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        AnalyticsHelper.getInstance(this).trackPageView("homescreen/photos_and_videos/videos/video_details");
    }

    public void onShareFacebookClick(View v) {
        Facebook facebook = FacebookApi.getInstance(this);
        if (!Strings.isNullOrEmpty(facebook.getAccessToken())) {
            shareOnFacebook();
            return;
        }
        AnalyticsHelper.getInstance(this).trackPageView("social_facebook_login");
        facebook.authorize(this, FacebookApi.getRequiredPermissions(), new Facebook.DialogListener() {
            public void onComplete(Bundle values) {
                FacebookApi.setAccessToken(values.getString(Facebook.TOKEN));
                VideoDetailsActivity.this.shareOnFacebook();
            }

            public void onFacebookError(FacebookError e) {
                Log.e(VideoDetailsActivity.TAG, "Facebook error " + e);
                throw new RuntimeException(e);
            }

            public void onError(DialogError e) {
                Log.e(VideoDetailsActivity.TAG, "Dialog error " + e);
                throw new RuntimeException(e);
            }

            public void onCancel() {
                Log.v(VideoDetailsActivity.TAG, "Facebook authorization cancelled by user");
            }
        });
    }

    /* access modifiers changed from: private */
    public void shareOnFacebook() {
        AnalyticsHelper.getInstance(this).trackEvent("sharing", "facebook", "video");
        showDialog(0);
        try {
            new FacebookWallPostApiTask(this) {
                /* access modifiers changed from: protected */
                public void onPostExecute(FacebookResponse response) {
                    VideoDetailsActivity.this.getActivityHelper().dismissDialog(0);
                    if (response.isSuccessful()) {
                        VideoDetailsActivity.this.showDialog(1);
                    } else {
                        VideoDetailsActivity.this.showDialog(2);
                    }
                }
            }.execute(new FacebookShareable[]{this.video});
        } catch (IllegalStateException e) {
            Log.e(TAG, e.toString());
            showDialog(2);
        }
    }

    public void onShareEmailClick(View v) {
        AnalyticsHelper.getInstance(this).trackEvent("sharing", "email", "video");
        getActivityHelper().getEmailHelper().startShareActivity(this, getEmailSubject(), getEmailBody());
    }

    private String getEmailSubject() {
        return getString(R.string.check_out) + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + getString(R.string.artist_name) + " - " + this.video.getName();
    }

    private Spanned getEmailBody() {
        return getActivityHelper().getEmailHelper().getShareBody(this, this.video.getVideoUrl(), getString(R.string.artist_name) + " - " + this.video.getName());
    }

    public void onShareTwitterClick(View v) {
        AnalyticsHelper.getInstance(this).trackEvent("sharing", "twitter", "video");
        if (!getActivityHelper().isTwitterAuthorized()) {
            getActivityHelper().getTwitterHelper().beginTwitterAuthentication();
        } else {
            tweetVideo();
        }
    }

    private void tweetVideo() {
        showDialog(3);
        getActivityHelper().getApplication().shorten(new String[]{this.video.getVideoUrl(), getActivityHelper().getApplication().getMarketplaceLink()}, new LinkShortener.ShortenListener() {
            public void onLinkReady(List<String> links) {
                final String tweet = VideoDetailsActivity.this.createTweet(links.get(0), links.get(1));
                new AsyncTask<Void, Void, Boolean>() {
                    /* access modifiers changed from: protected */
                    public Boolean doInBackground(Void... arg0) {
                        try {
                            VideoDetailsActivity.this.getActivityHelper().getTwitter().updateStatus(tweet);
                            return true;
                        } catch (TwitterException e) {
                            Log.v(VideoDetailsActivity.TAG, "Failed to share video on twitter" + e);
                            return false;
                        }
                    }

                    /* access modifiers changed from: protected */
                    public void onPostExecute(Boolean result) {
                        VideoDetailsActivity.this.getActivityHelper().dismissDialog(3);
                        VideoDetailsActivity.this.showDialog(result.booleanValue() ? 4 : 5);
                    }
                }.execute(new Void[0]);
            }
        });
    }

    /* access modifiers changed from: protected */
    public String createTweet(String videoUrl, String appUrl) {
        return getString(R.string.check_out) + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + "'" + this.video.getName() + "'" + " from " + getActivityHelper().getTwitterHelper().getTwitterHandleOrArtistName() + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + videoUrl + ". " + getString(R.string.free_app_powered_by) + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + appUrl;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        FacebookApi.getInstance(this).authorizeCallback(requestCode, resultCode, data);
        if (requestCode != 100) {
            return;
        }
        if (resultCode == -1) {
            tweetVideo();
        } else {
            getActivityHelper().getTwitterHelper().showTwitterAuthenticationError(data);
        }
    }

    public void onVideoClick(View v) {
        this.musicService.pause();
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(this.video.getVideoUrl())));
    }
}
