package com.reverbnation.artistapp.i9585.models;

import android.util.Log;
import com.reverbnation.artistapp.i9585.models.Photoset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.codehaus.jackson.annotate.JsonProperty;

public class MobileApplication {
    private static final String TAG = "MobileApplication";
    @JsonProperty("created_at")
    private String createdAt;
    @JsonProperty("id")
    private int id;
    @JsonProperty("mobile_application_images")
    private List<ApplicationImage> images;
    @JsonProperty("page_object")
    private PageObject pageObject;
    @JsonProperty("page_object_id")
    private int pageObjectId;
    @JsonProperty("page_object_type")
    private String pageObjectType;
    @JsonProperty("status")
    private String status;
    @JsonProperty("status_update")
    private String statusUpdate;
    @JsonProperty("updated_at")
    private String updatedAt;
    @JsonProperty("url")
    private String url;
    @JsonProperty("url_page")
    private String urlPage;
    @JsonProperty("url_store")
    private String urlStore;
    @JsonProperty("version")
    private String version;

    public int getId() {
        return this.id;
    }

    public void setId(int id2) {
        this.id = id2;
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(String version2) {
        this.version = version2;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status2) {
        this.status = status2;
    }

    public String getStatusUpdate() {
        return this.statusUpdate;
    }

    public void setStatusUpdate(String statusUpdate2) {
        this.statusUpdate = statusUpdate2;
    }

    public String getCreatedAt() {
        return this.createdAt;
    }

    public void setCreatedAt(String createdAt2) {
        this.createdAt = createdAt2;
    }

    public String getUpdatedAt() {
        return this.updatedAt;
    }

    public void setUpdatedAt(String updatedAt2) {
        this.updatedAt = updatedAt2;
    }

    public String getUrl() {
        return this.url;
    }

    public String getUrlPage() {
        return this.urlPage;
    }

    public void setUrlPage(String urlPage2) {
        this.urlPage = urlPage2;
    }

    public String getUrlStore() {
        return this.urlStore;
    }

    public void setUrlStore(String urlStore2) {
        this.urlStore = urlStore2;
    }

    public int getPageObjectId() {
        return this.pageObjectId;
    }

    public void setPageObjectId(int pageObjectId2) {
        this.pageObjectId = pageObjectId2;
    }

    public String getPageObjectType() {
        return this.pageObjectType;
    }

    public void setPageObjectType(String pageObjectType2) {
        this.pageObjectType = pageObjectType2;
    }

    public void setPageObject(PageObject pageObject2) {
        this.pageObject = pageObject2;
    }

    public PageObject getPageObject() {
        if (this.pageObject == null) {
            this.pageObject = new PageObject();
        }
        return this.pageObject;
    }

    public List<ApplicationImage> getImages() {
        if (this.images == null) {
            this.images = new ArrayList();
        }
        return this.images;
    }

    public String getTwitterUsername() {
        return getPageObject().getTwitterInfo().getUsername();
    }

    public String getMobileApplicationIconImage() {
        String url2 = null;
        Iterator<ApplicationImage> iter = getImages().iterator();
        while (true) {
            if (!iter.hasNext()) {
                break;
            }
            ApplicationImage image = iter.next();
            if (ApplicationImage.Icon.equals(image.getType())) {
                url2 = image.getUrl();
                break;
            }
        }
        Log.v(TAG, "Icon image: " + url2);
        return url2;
    }

    public List<ApplicationImage> getSlideImages() {
        List<ApplicationImage> slides = new ArrayList<>();
        for (ApplicationImage image : getImages()) {
            if ("MobileApplicationImageSlide".equals(image.getType())) {
                slides.add(image);
            }
        }
        return slides;
    }

    public List<Photoset.Photo> getSlidePhotos() {
        List<Photoset.Photo> photos = new ArrayList<>();
        for (ApplicationImage image : getSlideImages()) {
            Photoset.Photo photo = new Photoset.Photo();
            photo.setImageUrl(image.getUrl());
            photo.setThumbnailUrl(image.getUrl());
            photos.add(photo);
        }
        return photos;
    }
}
