package com.reverbnation.artistapp.i9585.models;

import org.codehaus.jackson.annotate.JsonProperty;

public class MobileApplicationPlatformLink {
    @JsonProperty("id")
    private int id;
    @JsonProperty("mobile_application_platform")
    private MobileApplicationPlatform mobileApplicationPlatform;
    @JsonProperty("mobile_device")
    private MobileDevice mobileDevice;

    public static class MobileDevice {
        @JsonProperty("id")
        private int id;

        public int getId() {
            return this.id;
        }
    }

    public int getId() {
        return this.id;
    }

    public MobileApplicationPlatform getMobileApplicationPlatform() {
        return this.mobileApplicationPlatform;
    }

    public MobileDevice getMobileDevice() {
        return this.mobileDevice;
    }
}
