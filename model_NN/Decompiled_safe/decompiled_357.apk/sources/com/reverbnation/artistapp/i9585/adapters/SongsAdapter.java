package com.reverbnation.artistapp.i9585.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.models.SongList;
import java.util.List;

public class SongsAdapter extends ArrayAdapter<SongList.Song> {
    public SongsAdapter(Context context, int textViewResourceId, List<SongList.Song> objects) {
        super(context, textViewResourceId, objects);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            v = View.inflate(getContext(), R.layout.song_list_item, null);
        }
        boolean isEven = position % 2 == 0;
        TextView title = (TextView) v.findViewById(R.id.text_song_title);
        title.setText(((SongList.Song) getItem(position)).getName());
        title.setTextColor(isEven ? getColor(R.color.CellEvenTextPrimary) : getColor(R.color.CellOddTextPrimary));
        ((ImageView) v.findViewById(R.id.list_item_background)).setImageResource(position % 2 == 0 ? R.drawable.background_row_even : R.drawable.background_row_odd);
        return v;
    }

    private int getColor(int colorResId) {
        return getContext().getResources().getColor(colorResId);
    }
}
