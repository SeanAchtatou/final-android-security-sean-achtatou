package com.reverbnation.artistapp.i9585.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.adapters.SongsAdapter;
import com.reverbnation.artistapp.i9585.utils.AnalyticsHelper;
import java.io.Serializable;

public class SongsActivity extends BaseListActivity {
    private static final int LOADING_DIALOG = 0;
    SongsAdapter songsAdapter;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(7);
        setContentView((int) R.layout.songs_activity);
        getActivityHelper().setArtistTitlebar(getString(R.string.songs_title));
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        AnalyticsHelper.getInstance(this).trackPageView("homescreen/songs/songs");
        if (isPlaylistReady()) {
            setupSongsAdapter();
        } else {
            showDialog(0);
        }
    }

    private boolean isPlaylistReady() {
        return getActivityHelper().getApplication().getPlaylist() != null;
    }

    /* access modifiers changed from: private */
    public void setupSongsAdapter() {
        this.songsAdapter = new SongsAdapter(this, R.layout.song_list_item, getActivityHelper().getApplication().getPlaylist());
        setListAdapter(this.songsAdapter);
    }

    /* access modifiers changed from: protected */
    public void onMusicReady() {
        runOnUiThread(new Runnable() {
            public void run() {
                SongsActivity.this.setupSongsAdapter();
                SongsActivity.this.getActivityHelper().dismissDialog(0);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        Intent intent = new Intent(this, SongDetailsActivity.class);
        intent.putExtra("song", (Serializable) this.songsAdapter.getItem(position));
        startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        Dialog dialog = null;
        switch (id) {
            case 0:
                dialog = getActivityHelper().createProgressDialog(0, R.string.loading);
                break;
        }
        return dialog != null ? dialog : super.onCreateDialog(id);
    }
}
