package com.reverbnation.artistapp.i9585.authorization;

import android.content.Context;
import android.content.SharedPreferences;
import com.reverbnation.artistapp.i9585.R;
import java.util.Properties;
import twitter4j.Twitter;
import twitter4j.auth.AccessToken;

public class OAuthHelper {
    private static final String AUTH_KEY = "auth_key";
    private static final String AUTH_SECRET_KEY = "auth_secret_key";
    private static final String OAUTH_PREFS = "oauth_prefs";
    private AccessToken accessToken = loadAccessToken();
    private String consumerKey;
    private String consumerSecretKey;
    private Context context;
    private SharedPreferences prefs;

    public OAuthHelper(Context context2) {
        this.context = context2;
        this.prefs = context2.getSharedPreferences(OAUTH_PREFS, 0);
        loadConsumerKeys();
    }

    public boolean hasAccessToken() {
        return this.accessToken != null;
    }

    public void setAccessToken(AccessToken accessToken2) {
        this.accessToken = accessToken2;
    }

    private void loadConsumerKeys() {
        try {
            Properties props = new Properties();
            props.load(this.context.getResources().openRawResource(R.raw.oath));
            this.consumerKey = props.getProperty("consumer_key");
            this.consumerSecretKey = props.getProperty("consumer_secret_key");
        } catch (Exception e) {
            throw new RuntimeException("Failed to load consumer keys from oauth.properties");
        }
    }

    public void configureOAuth(Twitter twitter) {
        twitter.setOAuthConsumer(this.consumerKey, this.consumerSecretKey);
        if (hasAccessToken()) {
            twitter.setOAuthAccessToken(this.accessToken);
        }
    }

    public AccessToken loadAccessToken() {
        String token = this.prefs.getString(AUTH_KEY, null);
        String tokenSecret = this.prefs.getString(AUTH_SECRET_KEY, null);
        if (token == null || tokenSecret == null) {
            return null;
        }
        return new AccessToken(token, tokenSecret);
    }

    public void storeAccessToken(AccessToken accessToken2) {
        SharedPreferences.Editor editor = this.prefs.edit();
        editor.putString(AUTH_KEY, accessToken2.getToken());
        editor.putString(AUTH_SECRET_KEY, accessToken2.getTokenSecret());
        editor.commit();
        this.accessToken = accessToken2;
    }

    public void deleteAccessToken() {
        this.accessToken = null;
        SharedPreferences.Editor editor = this.prefs.edit();
        editor.remove(AUTH_KEY);
        editor.remove(AUTH_SECRET_KEY);
        editor.commit();
    }
}
