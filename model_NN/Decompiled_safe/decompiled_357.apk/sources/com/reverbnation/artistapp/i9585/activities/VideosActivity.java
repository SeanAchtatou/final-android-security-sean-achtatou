package com.reverbnation.artistapp.i9585.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.adapters.VideoAdapter;
import com.reverbnation.artistapp.i9585.api.tasks.GetArtistVideosApiTask;
import com.reverbnation.artistapp.i9585.models.Videoset;
import com.reverbnation.artistapp.i9585.utils.AnalyticsHelper;

public class VideosActivity extends BaseListTabChildActivity {
    private static final int PROGRESS_DIALOG = 0;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.video_activity);
        if (shouldGetVideos()) {
            getVideos(getActivityHelper().getArtistId(), null);
        }
    }

    private boolean shouldGetVideos() {
        return getIntent().getBooleanExtra("shouldGetVideos", true);
    }

    /* access modifiers changed from: protected */
    public void getVideos(String artist_id, String baseUrlOrNull) {
        showDialog(0);
        new GetArtistVideosApiTask(new GetArtistVideosApiTask.GetArtistVideosApiDelegate() {
            public void getArtistVideosFinished(Videoset videos) {
                VideosActivity.this.getActivityHelper().dismissDialog(0);
                VideosActivity.this.setListUsingVideos(videos);
            }

            public void getArtistVideosStarted() {
            }

            public void getArtistVideosCancelled() {
            }
        }).execute(artist_id, baseUrlOrNull, this);
    }

    /* access modifiers changed from: private */
    public void setListUsingVideos(Videoset videos) {
        if (videos != null) {
            setListAdapter(new VideoAdapter(this, R.layout.video_list_item, videos.getVideos()));
        }
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Intent intent = new Intent(this, VideoDetailsActivity.class);
        intent.putExtra("video", (Videoset.Video) getListAdapter().getItem(position));
        startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        AnalyticsHelper.getInstance(this).trackPageView("homescreen/photos_and_videos/videos");
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        return id == 0 ? getActivityHelper().createProgressDialog(0, R.string.loading) : super.onCreateDialog(id);
    }
}
