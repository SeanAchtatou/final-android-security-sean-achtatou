package com.reverbnation.artistapp.i9585.services;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.ReverbNationApplication;
import com.reverbnation.artistapp.i9585.activities.HomeActivity;
import com.reverbnation.artistapp.i9585.api.ReverbNationApi;
import com.reverbnation.artistapp.i9585.api.tasks.PostSongProgressApiTask;
import com.reverbnation.artistapp.i9585.models.FileToken;
import com.reverbnation.artistapp.i9585.models.SongList;
import com.reverbnation.artistapp.i9585.utils.AudioFocusWrapper;
import com.reverbnation.artistapp.i9585.utils.SongHelper;
import com.roobit.restkit.RestClient;
import com.roobit.restkit.Result;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.codehaus.jackson.Base64Variant;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

public class AudioPlayer {
    private static final int NOTIFICATION_ID = 1;
    private static final int NO_SONGS_YET = -1;
    protected static final long PROGRESS_TICK_MS = 150;
    public static final String REVERB_MEDIA_ACTION = "com.reverbnation.artistapp.REVERB_MEDIA_ACTION";
    private static final String TAG = "AudioPlayer";
    private static final int UPDATE_PLAYER_PROGRESS_MSG = 1;
    static AudioPlayer instance;
    private static boolean isAudioFocusAvailable;
    private AudioManager.OnAudioFocusChangeListener afChangeListener;
    private AudioFocusWrapper audioFocusWrapper;
    private AudioManager audioManager;
    /* access modifiers changed from: private */
    public int currentSong = -1;
    /* access modifiers changed from: private */
    public long durationSeconds = 0;
    private Handler handler;
    /* access modifiers changed from: private */
    public boolean hasMediaPlayerErrors;
    private boolean isDucking;
    /* access modifiers changed from: private */
    public MediaPlayer mediaPlayer;
    private Notification notification;
    private int oldVolume = -1;
    private List<SongList.Song> playlist = new ArrayList();
    private boolean postedProgress10;
    private boolean postedProgress50;
    /* access modifiers changed from: private */
    public long progressSeconds = 0;
    /* access modifiers changed from: private */
    public int requestedSong = 0;
    /* access modifiers changed from: private */
    public Service service;
    /* access modifiers changed from: private */
    public final SongHelper songHelper = new SongHelper();
    /* access modifiers changed from: private */
    public State state = State.STOPPED;
    /* access modifiers changed from: private */
    public FileToken token;
    /* access modifiers changed from: private */
    public AsyncTask<Void, Void, FileToken> tokenFetchTask;

    public enum MusicActionEvent {
        MUSIC_READY_EVENT,
        SONG_READY_EVENT,
        SONG_PREPARING_EVENT,
        SONG_PLAYING_EVENT,
        SONG_PAUSED_EVENT,
        SONG_ENDED_EVENT,
        UPDATE_PROGRESS_EVENT,
        UNDEFINED_EVENT
    }

    private enum State {
        STOPPED,
        READY,
        PREPARING,
        PLAYING,
        PAUSED
    }

    static {
        try {
            AudioFocusWrapper.isAvailable();
            isAudioFocusAvailable = true;
        } catch (Throwable th) {
            isAudioFocusAvailable = false;
        }
    }

    public AudioPlayer(Service service2) {
        this.service = service2;
        setupTimer();
    }

    /* access modifiers changed from: private */
    public void startMediaPlayer(String url) {
        releaseMediaPlayer();
        this.mediaPlayer = new MediaPlayer();
        this.mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                Log.v(AudioPlayer.TAG, "Media Player Prepared");
                long unused = AudioPlayer.this.progressSeconds = 0;
                long unused2 = AudioPlayer.this.durationSeconds = (long) (AudioPlayer.this.mediaPlayer.getDuration() / 1000);
                mp.start();
                State unused3 = AudioPlayer.this.state = State.PLAYING;
                AudioPlayer.this.broadcast(MusicActionEvent.SONG_PLAYING_EVENT);
                AudioPlayer.this.notifyPlaying();
                AudioPlayer.this.updateProgressApiNow();
            }
        });
        this.mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Log.e(AudioPlayer.TAG, "MediaPlayer Error: " + what + " : " + extra);
                boolean unused = AudioPlayer.this.hasMediaPlayerErrors = true;
                mp.reset();
                State unused2 = AudioPlayer.this.state = State.READY;
                AudioPlayer.this.broadcastSongReady(AudioPlayer.this.currentSong);
                AudioPlayer.this.broadcast(MusicActionEvent.SONG_ENDED_EVENT);
                Toast.makeText(AudioPlayer.this.service.getApplicationContext(), "Music connection problem.", 0);
                AudioPlayer.this.cancelNotify();
                return true;
            }
        });
        this.mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                Log.v(AudioPlayer.TAG, "Media Player Completion");
                State unused = AudioPlayer.this.state = State.READY;
                AudioPlayer.this.broadcast(MusicActionEvent.SONG_ENDED_EVENT);
                if (!AudioPlayer.this.hasMediaPlayerErrors) {
                    if (AudioPlayer.this.isAutoplayEnabled()) {
                        AudioPlayer.this.startPlaying(AudioPlayer.this.getNextSongIndex(AudioPlayer.this.currentSong));
                    } else {
                        AudioPlayer.this.cancelNotify();
                    }
                }
                boolean unused2 = AudioPlayer.this.hasMediaPlayerErrors = false;
            }
        });
        this.mediaPlayer.setOnInfoListener(new MediaPlayer.OnInfoListener() {
            public boolean onInfo(MediaPlayer mp, int what, int extra) {
                Log.e(AudioPlayer.TAG, "MediaPlayer Info: " + what + " : " + extra);
                return true;
            }
        });
        this.mediaPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
            public void onBufferingUpdate(MediaPlayer mp, int percent) {
                Log.v(AudioPlayer.TAG, "Buffered " + percent + " %");
            }
        });
        try {
            this.mediaPlayer.setWakeMode(this.service.getApplicationContext(), 1);
            this.mediaPlayer.setAudioStreamType(3);
            this.mediaPlayer.setDataSource(this.service.getApplicationContext(), Uri.parse(url));
            this.state = State.PREPARING;
            broadcastPreparing();
            this.mediaPlayer.prepareAsync();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e2) {
            e2.printStackTrace();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
    }

    private void releaseMediaPlayer() {
        resetMediaPlayer();
        if (this.mediaPlayer != null) {
            this.mediaPlayer.release();
        }
    }

    /* access modifiers changed from: private */
    public void resetMediaPlayer() {
        if (this.mediaPlayer != null) {
            try {
                this.mediaPlayer.reset();
            } catch (IllegalStateException e) {
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean isAutoplayEnabled() {
        return PreferenceManager.getDefaultSharedPreferences(this.service).getBoolean("autoplay", true);
    }

    /* access modifiers changed from: protected */
    public void notifyPlaying() {
        String artistName = this.service.getString(R.string.artist_name);
        if (this.notification == null) {
            this.notification = new Notification(R.drawable.ic_launcher, artistName, System.currentTimeMillis());
        }
        Intent intent = new Intent(this.service.getApplicationContext(), HomeActivity.class);
        intent.addFlags(67108864);
        this.notification.setLatestEventInfo(this.service, "Playing " + getCurrentSong().getName(), "Music by " + artistName, PendingIntent.getActivity(this.service.getApplicationContext(), 0, intent, 134217728));
        this.notification.flags |= 2;
        this.service.startForeground(1, this.notification);
    }

    /* access modifiers changed from: protected */
    public void cancelNotify() {
        this.service.stopForeground(true);
    }

    private void setupTimer() {
        this.handler = new Handler() {
            public void handleMessage(Message msg) {
                if (msg.what == 1) {
                    AudioPlayer.this.updateProgress();
                }
            }
        };
        this.handler.sendEmptyMessageDelayed(1, PROGRESS_TICK_MS);
    }

    /* access modifiers changed from: protected */
    public void updateProgress() {
        if (isPlaying()) {
            long progress = (long) (this.mediaPlayer.getCurrentPosition() / 1000);
            long duration = (long) (this.mediaPlayer.getDuration() / 1000);
            if (progress > this.progressSeconds) {
                this.progressSeconds = progress;
                if (duration != this.durationSeconds) {
                    this.durationSeconds = duration;
                    broadcastProgress(progress, duration);
                } else {
                    broadcastProgress(progress);
                }
                updateProgressApi();
            }
        }
        this.handler.sendEmptyMessageDelayed(1, PROGRESS_TICK_MS);
    }

    public boolean isPlaying() {
        try {
            if (this.state != State.PLAYING || this.mediaPlayer == null || !this.mediaPlayer.isPlaying()) {
                return false;
            }
            return true;
        } catch (IllegalStateException e) {
            return false;
        }
    }

    /* access modifiers changed from: private */
    public void updateProgressApiNow() {
        updateProgressApi(getCompletionProgress());
    }

    private void updateProgressApi() {
        if (isPlaying()) {
            long complete = (long) getCompletionProgress();
            if (complete > 10 && !this.postedProgress10) {
                updateProgressApi(10);
                this.postedProgress10 = true;
            }
            if (complete > 50 && !this.postedProgress50) {
                updateProgressApi(50);
                this.postedProgress50 = true;
            }
        }
    }

    private void updateProgressApi(int progress) {
        new PostSongProgressApiTask().execute(Integer.valueOf(getCurrentSongId()), Integer.valueOf(progress));
    }

    private int getCompletionProgress() {
        return (int) (this.durationSeconds > 0 ? (100 * this.progressSeconds) / this.durationSeconds : 0);
    }

    public long getProgressSeconds() {
        return this.progressSeconds;
    }

    public long getDurationSeconds() {
        return this.durationSeconds;
    }

    public void setAutoplayEnabled(boolean isAutoplayEnabled) {
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this.service).edit();
        edit.putBoolean("autoplay", isAutoplayEnabled);
        edit.commit();
    }

    /* access modifiers changed from: private */
    public int getNextSongIndex(int index) {
        int next = index + 1;
        if (next >= this.playlist.size()) {
            return 0;
        }
        return next;
    }

    private int getPreviousSongIndex(int index) {
        int previous = index - 1;
        if (previous < 0) {
            return this.playlist.size() - 1;
        }
        return previous;
    }

    public void startPlaying(SongList.Song song) {
        Log.v(TAG, "Start playing " + song.getName());
        startPlaying(this.playlist.indexOf(song));
    }

    /* access modifiers changed from: private */
    public void startPlaying(int songIndex) {
        this.requestedSong = songIndex;
        resetMediaPlayer();
        broadcastPreparing(songIndex);
        fetchFreshToken();
    }

    private void fetchFreshToken() {
        if (this.tokenFetchTask == null || this.tokenFetchTask.getStatus() != AsyncTask.Status.RUNNING) {
            this.tokenFetchTask = new AsyncTask<Void, Void, FileToken>() {
                /* Debug info: failed to restart local var, previous not found, register: 7 */
                /* access modifiers changed from: protected */
                public FileToken doInBackground(Void... arg0) {
                    if (isCancelled()) {
                        return null;
                    }
                    Result result = RestClient.getClientWithBaseUrl(ReverbNationApi.getBaseUrl(ReverbNationApi.getBaseUrl())).get(ReverbNationApi.getInstance().getRequiredHeaderParameters(AudioPlayer.this.service)).setResource("songs/file_tokens.json").synchronousExecute();
                    if (isCancelled()) {
                        return null;
                    }
                    try {
                        return (FileToken) RestClient.getObjectMapper().readValue(result.getResponse(), FileToken.class);
                    } catch (JsonParseException e) {
                        e.printStackTrace();
                        return null;
                    } catch (JsonMappingException e2) {
                        e2.printStackTrace();
                        return null;
                    } catch (IOException e3) {
                        e3.printStackTrace();
                        return null;
                    }
                }

                /* access modifiers changed from: protected */
                public void onPostExecute(FileToken token) {
                    Log.v(AudioPlayer.TAG, "Finished getting token");
                    AsyncTask unused = AudioPlayer.this.tokenFetchTask = null;
                    if (token != null) {
                        AudioPlayer.this.startPlaying(AudioPlayer.this.requestedSong, token);
                    }
                }

                /* access modifiers changed from: protected */
                public void onCancelled() {
                    Log.v(AudioPlayer.TAG, "Cancelled token fetch");
                    AudioPlayer.this.resetMediaPlayer();
                    AsyncTask unused = AudioPlayer.this.tokenFetchTask = null;
                }
            }.execute(new Void[0]);
        }
    }

    /* access modifiers changed from: private */
    public void startPlaying(int songIndex, FileToken token2) {
        Log.v(TAG, "startPlaying index " + songIndex + " for token " + token2.toString());
        SongList.Song song = this.playlist.get(songIndex);
        setCurrentSong(songIndex);
        setToken(token2);
        if (requestAudioFocus()) {
            startMediaPlayer(this.songHelper.createSongUrl(song, token2));
        }
    }

    private void setToken(FileToken token2) {
        this.token = token2;
    }

    private void initializePlayer() {
        broadcast(MusicActionEvent.MUSIC_READY_EVENT);
        setCurrentSong(0);
        if (this.state == State.STOPPED) {
            this.state = State.READY;
        }
    }

    public SongList.Song getCurrentSong() {
        return this.playlist.get(this.currentSong);
    }

    private int getCurrentSongId() {
        if (getCurrentSong() != null) {
            return getCurrentSong().getId();
        }
        return 0;
    }

    public void playOrPause() {
        Log.v(TAG, "playOrPause:" + this.state.toString());
        switch (this.state) {
            case STOPPED:
                return;
            case PLAYING:
                pause();
                return;
            case PREPARING:
                play();
                return;
            default:
                play();
                return;
        }
    }

    public void play() {
        Log.v(TAG, "play:" + this.state.toString());
        switch (this.state) {
            case PREPARING:
            case READY:
                startPlaying(this.requestedSong);
                return;
            case PAUSED:
                Log.v(TAG, "play:paused");
                if (requestAudioFocus()) {
                    this.state = State.PLAYING;
                    this.mediaPlayer.start();
                    broadcast(MusicActionEvent.SONG_PLAYING_EVENT);
                    notifyPlaying();
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void pause() {
        Log.v(TAG, "pause:" + this.state.toString());
        this.mediaPlayer.pause();
        this.state = State.PAUSED;
        cancelNotify();
        updateProgressApiNow();
        abandonAudioFocus();
        broadcast(MusicActionEvent.SONG_PAUSED_EVENT);
    }

    public void playRequest() {
        Log.d(TAG, "playRequest");
        switch (this.state) {
            case PLAYING:
            case PREPARING:
                startPlaying(this.requestedSong);
                return;
            case READY:
                setCurrentSong(this.requestedSong);
                return;
            case PAUSED:
                resetMediaPlayer();
                setCurrentSong(this.requestedSong);
                return;
            default:
                return;
        }
    }

    private void setCurrentSong(int index) {
        this.currentSong = index;
        this.requestedSong = this.currentSong;
        resetProgress();
        broadcastSongReady();
    }

    private void resetProgress() {
        this.progressSeconds = 0;
        this.durationSeconds = getCurrentSong().getLength();
    }

    public void setPlaylist(List<SongList.Song> songs) {
        this.playlist = songs;
        initializePlayer();
        if (isAutoplayEnabled()) {
            play();
        }
        ReverbNationApplication.getInstance().setPlaylist(this.playlist);
    }

    public List<SongList.Song> getPlaylist() {
        return this.playlist;
    }

    public void release() {
        this.handler.removeMessages(1);
        cancelNotify();
        releaseMediaPlayer();
    }

    /* access modifiers changed from: private */
    public void broadcast(MusicActionEvent event) {
        Intent intent = new Intent(REVERB_MEDIA_ACTION);
        intent.putExtra("event", event.ordinal());
        this.service.sendBroadcast(intent);
    }

    private void broadcastPreparing() {
        broadcastPreparing(this.currentSong);
    }

    private void broadcastPreparing(int songIndex) {
        broadcastEverything(songIndex, MusicActionEvent.SONG_PREPARING_EVENT.ordinal());
    }

    private void broadcastSongReady() {
        broadcastSongReady(this.currentSong);
    }

    /* access modifiers changed from: private */
    public void broadcastSongReady(int songIndex) {
        broadcastEverything(songIndex, MusicActionEvent.SONG_READY_EVENT.ordinal());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent} */
    private void broadcastEverything(int songIndex, int event) {
        Intent intent = new Intent(REVERB_MEDIA_ACTION);
        intent.putExtra("event", event);
        intent.putExtra("title", this.playlist.get(songIndex).getName());
        intent.putExtra("progress", 0L);
        intent.putExtra("duration", this.durationSeconds);
        this.service.sendBroadcast(intent);
    }

    private void broadcastProgress(long progress) {
        Intent intent = new Intent(REVERB_MEDIA_ACTION);
        intent.putExtra("event", MusicActionEvent.UPDATE_PROGRESS_EVENT.ordinal());
        intent.putExtra("progress", progress);
        this.service.sendBroadcast(intent);
    }

    private void broadcastProgress(long progress, long duration) {
        Intent intent = new Intent(REVERB_MEDIA_ACTION);
        intent.putExtra("event", MusicActionEvent.UPDATE_PROGRESS_EVENT.ordinal());
        intent.putExtra("progress", progress);
        intent.putExtra("duration", duration);
        this.service.sendBroadcast(intent);
    }

    public boolean isReady() {
        return this.state != State.STOPPED;
    }

    public boolean hasSongs() {
        return this.currentSong != -1;
    }

    public void hintPlayOrPause() {
        broadcast(isPlaying() ? MusicActionEvent.SONG_PAUSED_EVENT : MusicActionEvent.SONG_PLAYING_EVENT);
    }

    public void hintNextSong() {
        cancelTokenFetch();
        if (this.state == State.READY || this.state == State.PAUSED) {
            broadcastSongReady(getNextRequestedSong());
        } else {
            broadcastPreparing(getNextRequestedSong());
        }
    }

    public void hintPrevSong() {
        cancelTokenFetch();
        if (this.state == State.READY || this.state == State.PAUSED) {
            broadcastSongReady(getPreviousRequestedSong());
        } else {
            broadcastPreparing(getPreviousRequestedSong());
        }
    }

    private void cancelTokenFetch() {
        if (this.tokenFetchTask != null && this.tokenFetchTask.getStatus() != AsyncTask.Status.FINISHED) {
            Log.v(TAG, "Cancelling token fetch");
            this.tokenFetchTask.cancel(true);
        }
    }

    private int getNextRequestedSong() {
        this.requestedSong = getNextSongIndex(this.requestedSong);
        return this.requestedSong;
    }

    private int getPreviousRequestedSong() {
        this.requestedSong = getPreviousSongIndex(this.requestedSong);
        return this.requestedSong;
    }

    private boolean requestAudioFocus() {
        if (!isAudioFocusAvailable) {
            Log.v(TAG, "Requesting non-existant audio focus");
            return true;
        }
        if (this.afChangeListener == null) {
            this.afChangeListener = new AudioManager.OnAudioFocusChangeListener() {
                public void onAudioFocusChange(int focusChange) {
                    switch (focusChange) {
                        case -3:
                            Log.v(AudioPlayer.TAG, "Audio focus transient loss can duck");
                            return;
                        case Base64Variant.BASE64_VALUE_PADDING /*-2*/:
                        case 0:
                        default:
                            Log.v(AudioPlayer.TAG, "Received unhandled focus change: " + focusChange);
                            return;
                        case -1:
                            Log.v(AudioPlayer.TAG, "Audio focus loss");
                            return;
                        case 1:
                            Log.v(AudioPlayer.TAG, "Audio focus gain");
                            AudioPlayer.this.startMediaPlayer(AudioPlayer.this.songHelper.createSongUrl(AudioPlayer.this.getCurrentSong(), AudioPlayer.this.token));
                            return;
                    }
                }
            };
        }
        Log.v(TAG, "Requesting audio focus");
        int result = getAudioFocusWrapper().requestAudioFocus(getAudioManager(), this.afChangeListener, 3, 1);
        Log.v(TAG, "Audio focus request result: " + result);
        return result == 1;
    }

    private AudioFocusWrapper getAudioFocusWrapper() {
        if (this.audioFocusWrapper == null) {
            this.audioFocusWrapper = new AudioFocusWrapper();
        }
        return this.audioFocusWrapper;
    }

    /* access modifiers changed from: protected */
    public void duckVolume() {
        if (this.isDucking) {
            Log.v(TAG, "Refusing to duck volume because we're already ducking");
            return;
        }
        Log.v(TAG, "Ducking volume");
        this.oldVolume = getAudioManager().getStreamVolume(3);
        Log.v(TAG, "Current volume is" + this.oldVolume);
        if (this.oldVolume > 0) {
            int maxVolume = getAudioManager().getStreamMaxVolume(3);
            Log.v(TAG, "Max volume is" + maxVolume);
            if (maxVolume > 0) {
                Log.v(TAG, "Setting volume to" + (maxVolume / 10));
                getAudioManager().setStreamVolume(3, maxVolume / 10, 0);
                this.isDucking = true;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void unduckVolume() {
        if (!this.isDucking) {
            Log.v(TAG, "Refusing to unduck volume because we're not ducking");
            return;
        }
        Log.v(TAG, "Unducking volume");
        if (this.oldVolume > 0) {
            Log.v(TAG, "Returning volume to" + this.oldVolume);
            getAudioManager().setStreamVolume(3, this.oldVolume, 0);
            this.isDucking = false;
        }
    }

    private boolean abandonAudioFocus() {
        if (!isAudioFocusAvailable) {
            Log.v(TAG, "Abandoning non-existent audio focus");
            return true;
        } else if (this.afChangeListener == null) {
            Log.v(TAG, "Abandoning null audio focus listener");
            return true;
        } else {
            Log.v(TAG, "Abandoning audio focus");
            return getAudioFocusWrapper().abandonAudioFocus(getAudioManager(), this.afChangeListener) == 1;
        }
    }

    private AudioManager getAudioManager() {
        if (this.audioManager == null) {
            this.audioManager = (AudioManager) this.service.getSystemService("audio");
        }
        return this.audioManager;
    }
}
