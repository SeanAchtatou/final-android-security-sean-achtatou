package com.reverbnation.artistapp.i9585.api.tasks;

import android.content.Context;
import android.os.AsyncTask;
import com.reverbnation.artistapp.i9585.api.ReverbNationApi;
import com.reverbnation.artistapp.i9585.models.Videoset;
import com.roobit.restkit.RestClient;
import com.roobit.restkit.Result;
import java.io.IOException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

public class GetArtistVideosApiTask extends AsyncTask<Object, Void, Videoset> {
    private GetArtistVideosApiDelegate delegate;

    public interface GetArtistVideosApiDelegate {
        void getArtistVideosCancelled();

        void getArtistVideosFinished(Videoset videoset);

        void getArtistVideosStarted();
    }

    public GetArtistVideosApiTask(GetArtistVideosApiDelegate delegate2) {
        this.delegate = delegate2;
    }

    /* access modifiers changed from: protected */
    public Videoset doInBackground(Object... args) {
        Result result = RestClient.getClientWithBaseUrl(ReverbNationApi.getBaseUrl((String) args[1])).get(ReverbNationApi.getInstance().getRequiredHeaderParameters((Context) args[2])).setResource("artists/" + ((String) args[0]) + "/videos.json").synchronousExecute();
        if (!result.isSuccess()) {
            return null;
        }
        try {
            return (Videoset) RestClient.getObjectMapper().readValue(result.getResponse(), Videoset.class);
        } catch (JsonParseException e) {
            e.printStackTrace();
            return null;
        } catch (JsonMappingException e2) {
            e2.printStackTrace();
            return null;
        } catch (IOException e3) {
            e3.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        this.delegate.getArtistVideosCancelled();
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Videoset result) {
        this.delegate.getArtistVideosFinished(result);
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        this.delegate.getArtistVideosStarted();
    }
}
