package com.reverbnation.artistapp.i9585.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.adapters.ShowsAdapter;
import com.reverbnation.artistapp.i9585.api.tasks.GetShowsApiTask;
import com.reverbnation.artistapp.i9585.models.ShowList;
import com.reverbnation.artistapp.i9585.utils.AnalyticsHelper;

public class UpcomingShowsActivity extends BaseListTabChildActivity {
    private static final int PROGRESS_DIALOG = 0;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.shows_list_activity);
        loadShows();
    }

    private void loadShows() {
        if (areShowsLoaded()) {
            setListUsingShows(getShows());
            return;
        }
        showDialog(0);
        getShows(getActivityHelper().getArtistId(), null, new GetShowsApiTask.GetShowsApiDelegate() {
            public void getShowsFinished(ShowList showList) {
                UpcomingShowsActivity.this.getActivityHelper().dismissDialog(0);
                if (showList != null) {
                    UpcomingShowsActivity.this.getActivityHelper().getApplication().setUpcomingShowList(showList);
                    UpcomingShowsActivity.this.setListUsingShows(showList);
                }
            }

            public void getShowsStarted() {
            }

            public void getShowsCancelled() {
            }
        });
    }

    private boolean areShowsLoaded() {
        return getShows() != null;
    }

    private ShowList getShows() {
        return getActivityHelper().getApplication().getUpcomingShowList();
    }

    /* access modifiers changed from: private */
    public void setListUsingShows(ShowList showList) {
        setListAdapter(showList != null ? createShowsAdapter(showList) : null);
    }

    private ShowsAdapter createShowsAdapter(ShowList showList) {
        return new ShowsAdapter(this, R.layout.show_list_item, ShowList.sortEntriesByAscendingDate(ShowList.getUpcomingShows(showList.getShows())));
    }

    /* access modifiers changed from: protected */
    public void getShows(String artistId, String baseUrlOrNull, GetShowsApiTask.GetShowsApiDelegate delegate) {
        new GetShowsApiTask(delegate).execute(this, artistId, baseUrlOrNull, "upcoming");
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        AnalyticsHelper.getInstance(this).trackPageView("homescreen/shows/upcoming");
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        AnalyticsHelper.getInstance(this).trackPageView("homescreen/shows/upcoming/show_details");
        ListAdapter showListAdapter = getListAdapter();
        Intent intent = new Intent(this, ShowDetailsActivity.class);
        intent.putExtra("show", (ShowList.Show) showListAdapter.getItem(position));
        startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        return id == 0 ? getActivityHelper().createProgressDialog(0, R.string.loading) : super.onCreateDialog(id);
    }
}
