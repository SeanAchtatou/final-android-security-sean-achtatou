package com.reverbnation.artistapp.i9585.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.utils.TitlebarHelper;

public class SocialActivity extends BaseTabActivity {
    public static final String MailingListTab = "mailing_list_tab";
    private static final String SharingTab = "sharing_tab";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(7);
        setContentView((int) R.layout.social_activity);
        getActivityHelper().setArtistTitlebar(TitlebarHelper.capitalize(getString(R.string.social)));
        setupTabs();
    }

    private void setupTabs() {
        TabHost tabHost = getTabHost();
        tabHost.addTab(tabHost.newTabSpec(SharingTab).setIndicator(createTabView(R.string.sharing)).setContent(new Intent(this, SharingActivity.class)));
        tabHost.addTab(tabHost.newTabSpec(MailingListTab).setIndicator(createTabView(R.string.mailing_list)).setContent(new Intent(this, MailingListActivity.class)));
        String showTag = getIntent().getStringExtra("show_tag");
        if (showTag != null) {
            tabHost.setCurrentTabByTag(showTag);
        }
    }
}
