package com.reverbnation.artistapp.i9585.utils;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.views.MusicPlayer;
import org.codehaus.jackson.util.MinimalPrettyPrinter;

public class TitlebarHelper {
    protected static final String TAG = "TitlebarHelper";
    private Activity activity;

    public TitlebarHelper(Activity activity2) {
        this.activity = activity2;
    }

    public void setArtistTitlebar(int resId) {
        setArtistTitlebar(this.activity.getString(resId));
    }

    public void setArtistTitlebar(String title) {
        this.activity.getWindow().setFeatureInt(7, R.layout.titlebar);
        ((TextView) this.activity.findViewById(R.id.titlebar_text)).setText(capitalize(title));
        final MusicPlayer player = getMusicPlayer();
        ViewGroup playerButton = (ViewGroup) this.activity.findViewById(R.id.titlebar_musicnote);
        playerButton.setVisibility(8);
        playerButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                player.animateOpen();
            }
        });
        player.setButton(playerButton);
    }

    public MusicPlayer getMusicPlayer() {
        return (MusicPlayer) this.activity.findViewById(R.id.audio_player);
    }

    public static String capitalize(String phrase) {
        return Joiner.on(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR).join(Iterables.transform(Splitter.on(' ').split(phrase), new Function<String, String>() {
            public String apply(String word) {
                char[] chars = word.toCharArray();
                chars[0] = Character.toUpperCase(chars[0]);
                return String.valueOf(chars);
            }
        }));
    }
}
