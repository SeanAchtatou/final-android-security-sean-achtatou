package com.reverbnation.artistapp.i9585.views;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;
import com.readystatesoftware.mapviewballoons.BalloonItemizedOverlay;
import com.reverbnation.artistapp.i9585.activities.ShowDetailsActivity;
import com.reverbnation.artistapp.i9585.models.ShowList;
import java.util.ArrayList;

public class UpcomingShowsOverlay extends BalloonItemizedOverlay<ShowOverlayItem> {
    private Context context;
    private ArrayList<ShowOverlayItem> overlays = new ArrayList<>();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.reverbnation.artistapp.i9585.views.UpcomingShowsOverlay.onBalloonTap(int, com.reverbnation.artistapp.i9585.views.ShowOverlayItem):boolean
     arg types: [int, com.google.android.maps.OverlayItem]
     candidates:
      com.reverbnation.artistapp.i9585.views.UpcomingShowsOverlay.onBalloonTap(int, com.google.android.maps.OverlayItem):boolean
      com.readystatesoftware.mapviewballoons.BalloonItemizedOverlay.onBalloonTap(int, com.google.android.maps.OverlayItem):boolean
      com.reverbnation.artistapp.i9585.views.UpcomingShowsOverlay.onBalloonTap(int, com.reverbnation.artistapp.i9585.views.ShowOverlayItem):boolean */
    /* access modifiers changed from: protected */
    public /* bridge */ /* synthetic */ boolean onBalloonTap(int x0, OverlayItem x1) {
        return onBalloonTap(x0, (ShowOverlayItem) ((ShowOverlayItem) x1));
    }

    public UpcomingShowsOverlay(Drawable defaultMarker, MapView mapView) {
        super(boundCenterBottom(defaultMarker), mapView);
        this.context = mapView.getContext();
    }

    public void addOverlay(ShowOverlayItem overlay) {
        this.overlays.add(overlay);
        populate();
    }

    /* access modifiers changed from: protected */
    public ShowOverlayItem createItem(int i) {
        return this.overlays.get(i);
    }

    public int size() {
        return this.overlays.size();
    }

    /* access modifiers changed from: protected */
    public boolean onBalloonTap(int index, ShowOverlayItem item) {
        startDetailsActivity(item.getShow());
        return true;
    }

    private void startDetailsActivity(ShowList.Show show) {
        Intent intent = new Intent(this.context, ShowDetailsActivity.class);
        intent.putExtra("show", show);
        this.context.startActivity(intent);
    }
}
