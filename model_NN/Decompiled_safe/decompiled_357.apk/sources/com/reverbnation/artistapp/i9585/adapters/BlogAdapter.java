package com.reverbnation.artistapp.i9585.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.models.BlogPost;
import java.text.SimpleDateFormat;
import java.util.List;

public class BlogAdapter extends ArrayAdapter<BlogPost> {
    private List<BlogPost> blogPosts;
    private LayoutInflater inflater;
    private int resource;

    public BlogAdapter(Context context, int resource2, List<BlogPost> blogPosts2) {
        super(context, 0, blogPosts2);
        this.resource = resource2;
        this.blogPosts = blogPosts2;
        this.inflater = (LayoutInflater) context.getSystemService("layout_inflater");
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            v = this.inflater.inflate(this.resource, (ViewGroup) null);
        }
        boolean isEven = position % 2 == 0;
        BlogPost blog = this.blogPosts.get(position);
        TextView blogTitleText = (TextView) v.findViewById(R.id.news_title);
        blogTitleText.setText(blog.getTitle());
        blogTitleText.setTextColor(isEven ? getColor(R.color.CellEvenTextPrimary) : getColor(R.color.CellOddTextPrimary));
        TextView blogDateText = (TextView) v.findViewById(R.id.news_date);
        blogDateText.setText(new SimpleDateFormat("EEEE, MMMM d").format(blog.getCreatedAtDate()));
        blogDateText.setTextColor(isEven ? getColor(R.color.CellEvenTextSecondary) : getColor(R.color.CellOddTextSecondary));
        ((ImageView) v.findViewById(R.id.list_item_background)).setImageResource(isEven ? R.drawable.background_row_even : R.drawable.background_row_odd);
        return v;
    }

    private int getColor(int colorResId) {
        return getContext().getResources().getColor(colorResId);
    }

    public List<BlogPost> getItems() {
        return this.blogPosts;
    }

    public BlogPost getItem(int position) {
        return this.blogPosts.get(position);
    }
}
