package com.reverbnation.artistapp.i9585.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.TextView;
import com.google.common.base.Strings;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.ReverbNationApplication;
import com.reverbnation.artistapp.i9585.api.tasks.PostMobileConnectionsApiTask;
import com.reverbnation.artistapp.i9585.models.MobileConnection;
import com.reverbnation.artistapp.i9585.models.Photoset;
import com.reverbnation.artistapp.i9585.utils.DrawableManager;
import com.reverbnation.artistapp.i9585.utils.VersionString;
import java.util.List;

public class SplashActivity extends Activity {
    private static final int CONNECTION_ERROR_DIALOG = 5;
    private static final int CONNECTION_FAILED_DIALOG = 1;
    private static final int FORCE_UPGRADE_DIALOG = 4;
    private static final String LAST_KNOWN_VERSION_PREF = "last_known_version";
    private static final int PROGRESS_DIALOG = 0;
    private static final int SERVER_UNAVAILABLE_DIALOG = 2;
    private static final int SYSTEM_MESSAGE_DIALOG = 3;
    private static final String TAG = "Splash Activity";
    /* access modifiers changed from: private */
    public MobileConnection mobileConnection;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PreferenceManager.setDefaultValues(this, R.xml.user_preferences, false);
        setContentView((int) R.layout.splash_activity);
        if (needsMobileConnect()) {
            startMobileConnect();
        } else {
            startupApplication();
        }
    }

    private boolean needsMobileConnect() {
        return !getReverbApplication().hasMobileConnection();
    }

    /* access modifiers changed from: private */
    public ReverbNationApplication getReverbApplication() {
        return (ReverbNationApplication) getApplication();
    }

    private void startMobileConnect() {
        showDialog(0);
        new PostMobileConnectionsApiTask(new PostMobileConnectionsApiTask.PostMobileConnectionsApiDelegate() {
            public void mobileConnectionsPostFinished(MobileConnection result) {
                MobileConnection unused = SplashActivity.this.mobileConnection = result;
                if (SplashActivity.this.hasMobileConnectionErrors()) {
                    try {
                        SplashActivity.this.dismissDialog(0);
                    } catch (IllegalArgumentException e) {
                    }
                    SplashActivity.this.dialogMobileConnectionErrors();
                } else if (SplashActivity.this.mobileConnection.hasSystemMessage()) {
                    try {
                        SplashActivity.this.dismissDialog(0);
                    } catch (IllegalArgumentException e2) {
                    }
                    SplashActivity.this.showDialog(3);
                } else {
                    SplashActivity.this.getReverbApplication().setMobileConnection(SplashActivity.this.mobileConnection);
                    SplashActivity.this.loadSlides();
                    SplashActivity.this.finishMobileConnect();
                }
            }

            public void mobileConnectionsPostStarted() {
            }

            public void mobileConnectionsPostCancelled() {
            }
        }).execute(null, this);
    }

    /* access modifiers changed from: protected */
    public boolean hasMobileConnectionErrors() {
        return this.mobileConnection == null || this.mobileConnection.getError() != null || this.mobileConnection.isServerDown();
    }

    public void dialogMobileConnectionErrors() {
        if (this.mobileConnection == null) {
            showDialog(1);
        } else if (this.mobileConnection.getError() != null) {
            showDialog(5);
        } else if (this.mobileConnection.isUpdateRequired()) {
            showDialog(4);
        } else if (this.mobileConnection.isServerDown()) {
            showDialog(2);
        }
    }

    /* access modifiers changed from: protected */
    public void loadSlides() {
        List<Photoset.Photo> slides = this.mobileConnection.getMobileApplication().getSlidePhotos();
        if (!slides.isEmpty()) {
            DrawableManager.getInstance(this).pleaseCacheDrawable(slides.get(0).getImageUrl());
        }
    }

    /* access modifiers changed from: protected */
    public void finishMobileConnect() {
        checkStatusAndStartupActivity();
    }

    private void checkStatusAndStartupActivity() {
        clearCacheIfRequested();
        if (isUpgradeRequired()) {
            showDialog(4);
        } else {
            startupApplication();
        }
    }

    private void clearCacheIfRequested() {
        if (this.mobileConnection.shouldClearCache() || isAppUpdated()) {
            DrawableManager.getInstance(this).clearCacheAndDisableCaching();
        }
    }

    private boolean isAppUpdated() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String lastKnownVersion = prefs.getString(LAST_KNOWN_VERSION_PREF, "");
        try {
            String currentVersion = getCurrentVersion();
            if (Strings.isNullOrEmpty(lastKnownVersion)) {
                Log.v(TAG, "Looks like a new installation; saving the current version string");
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString(LAST_KNOWN_VERSION_PREF, currentVersion);
                editor.commit();
            } else if (!lastKnownVersion.equals(currentVersion)) {
                Log.v(TAG, "The app was updated.");
                return true;
            } else {
                Log.v(TAG, "Same app we ran last time.");
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.wtf(TAG, "Current version not found; don't know if it was updated or not. Just say no.");
        }
        return false;
    }

    private boolean isUpgradeRequired() {
        try {
            if (new VersionString(getCurrentVersion()).compareTo(getMinimumRequiredVersion()) < 0 || this.mobileConnection.getServer().isUpdateRequired()) {
                return true;
            }
            return false;
        } catch (Exception e) {
            Log.v(TAG, "Failed getting version somehow, skipping upgrade check");
            return false;
        }
    }

    private String getCurrentVersion() throws PackageManager.NameNotFoundException {
        return getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
    }

    private String getMinimumRequiredVersion() {
        return this.mobileConnection.getVersionMinimum();
    }

    private void startupApplication() {
        startActivity(new Intent(this, HomeActivity.class));
        finish();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        Dialog dialog = null;
        switch (id) {
            case 0:
                dialog = createProgressDialog();
                break;
            case 1:
                dialog = createConnectionFailedDialog();
                break;
            case 2:
                dialog = createServerUnavailableDialog();
                break;
            case 3:
                dialog = createSystemMessageDialog();
                break;
            case 4:
                dialog = createForceUpgradeDialog();
                break;
            case 5:
                dialog = createConnectionErrorDialog();
                break;
        }
        if (dialog != null) {
            return dialog;
        }
        return super.onCreateDialog(id);
    }

    private Dialog createProgressDialog() {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setIndeterminate(true);
        dialog.setMessage(getString(R.string.loading));
        return dialog;
    }

    private Dialog createConnectionErrorDialog() {
        TextView message = new TextView(this);
        message.setText(this.mobileConnection.getError().getMessage());
        message.setGravity(1);
        return new AlertDialog.Builder(this).setTitle(this.mobileConnection.getError().getType()).setView(message).setPositiveButton((int) R.string.exit, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                SplashActivity.this.finish();
            }
        }).create();
    }

    private Dialog createConnectionFailedDialog() {
        TextView message = new TextView(this);
        message.setText((int) R.string.unable_to_connect);
        message.setGravity(1);
        return new AlertDialog.Builder(this).setTitle((int) R.string.connection_failed).setView(message).setPositiveButton((int) R.string.exit, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                SplashActivity.this.finish();
            }
        }).create();
    }

    private Dialog createServerUnavailableDialog() {
        return new AlertDialog.Builder(this).setTitle((int) R.string.server_unavailable).setMessage(this.mobileConnection.getServerMessage()).setPositiveButton((int) R.string.exit, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                SplashActivity.this.finish();
            }
        }).create();
    }

    private Dialog createSystemMessageDialog() {
        return new AlertDialog.Builder(this).setTitle((int) R.string.system_message).setMessage(this.mobileConnection.getServerMessage()).setPositiveButton(17039370, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                SplashActivity.this.finishMobileConnect();
            }
        }).create();
    }

    private Dialog createForceUpgradeDialog() {
        return new AlertDialog.Builder(this).setTitle((int) R.string.update_required).setMessage((int) R.string.update_message).setNegativeButton((int) R.string.exit, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                SplashActivity.this.finish();
            }
        }).setPositiveButton((int) R.string.update, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                SplashActivity.this.startUpgradeActivity();
                SplashActivity.this.finish();
            }
        }).create();
    }

    /* access modifiers changed from: protected */
    public void startUpgradeActivity() {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(getReverbApplication().getAppStoreUrl())));
    }
}
