package com.reverbnation.artistapp.i9585.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.models.Videoset;
import com.reverbnation.artistapp.i9585.utils.DrawableManager;
import java.util.List;

public class VideoAdapter extends ArrayAdapter<Videoset.Video> {
    private DrawableManager drawableManager;
    private LayoutInflater inflater;
    private int resource;

    public VideoAdapter(Context context, int resource2, List<Videoset.Video> videos) {
        super(context, 0, videos);
        this.resource = resource2;
        this.drawableManager = DrawableManager.getInstance(context);
        this.inflater = (LayoutInflater) context.getSystemService("layout_inflater");
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            v = this.inflater.inflate(this.resource, (ViewGroup) null);
        }
        boolean isEven = position % 2 == 0;
        Videoset.Video video = (Videoset.Video) getItem(position);
        ImageView thumbnail = (ImageView) v.findViewById(R.id.thumbnail_image);
        thumbnail.setImageResource(17170445);
        TextView videoNameText = (TextView) v.findViewById(R.id.video_name);
        videoNameText.setText(video.getName());
        videoNameText.setTextColor(isEven ? getColor(R.color.CellEvenTextPrimary) : getColor(R.color.CellOddTextPrimary));
        TextView videoDurationText = (TextView) v.findViewById(R.id.video_duration);
        videoDurationText.setText(video.getDuration());
        videoDurationText.setTextColor(isEven ? getColor(R.color.CellEvenTextSecondary) : getColor(R.color.CellOddTextSecondary));
        this.drawableManager.bindDrawable(video.getThumbnailUrl(), thumbnail);
        ((ImageView) v.findViewById(R.id.list_item_background)).setImageResource(position % 2 == 0 ? R.drawable.background_row_even : R.drawable.background_row_odd);
        return v;
    }

    private int getColor(int colorResId) {
        return getContext().getResources().getColor(colorResId);
    }
}
