package com.reverbnation.artistapp.i9585.api.tasks;

import android.content.Context;
import android.os.AsyncTask;
import com.reverbnation.artistapp.i9585.api.ReverbNationApi;
import com.reverbnation.artistapp.i9585.models.ShowList;
import com.roobit.restkit.RestClient;
import com.roobit.restkit.Result;
import java.io.IOException;
import java.util.Properties;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

public class GetShowsApiTask extends AsyncTask<Object, Void, ShowList> {
    private GetShowsApiDelegate delegate;

    public interface GetShowsApiDelegate {
        void getShowsCancelled();

        void getShowsFinished(ShowList showList);

        void getShowsStarted();
    }

    public GetShowsApiTask(GetShowsApiDelegate delegate2) {
        this.delegate = delegate2;
    }

    /* access modifiers changed from: protected */
    public ShowList doInBackground(Object... args) {
        String baseUrl = ReverbNationApi.getBaseUrl((String) args[2]);
        Properties queryParams = new Properties();
        queryParams.put("mode", (String) args[3]);
        Result result = RestClient.getClientWithBaseUrl(baseUrl).get(queryParams, ReverbNationApi.getInstance().getRequiredHeaderParameters((Context) args[0])).setResource("artists/" + ((String) args[1]) + "/shows.json").synchronousExecute();
        if (!result.isSuccess()) {
            return null;
        }
        try {
            return (ShowList) RestClient.getObjectMapper().readValue(result.getResponse(), ShowList.class);
        } catch (JsonParseException e) {
            e.printStackTrace();
            return null;
        } catch (JsonMappingException e2) {
            e2.printStackTrace();
            return null;
        } catch (IOException e3) {
            e3.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        this.delegate.getShowsCancelled();
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(ShowList result) {
        this.delegate.getShowsFinished(result);
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        this.delegate.getShowsStarted();
    }
}
