package com.reverbnation.artistapp.i9585.api.tasks;

import android.content.Context;
import android.os.AsyncTask;
import com.reverbnation.artistapp.i9585.api.ReverbNationApi;
import com.reverbnation.artistapp.i9585.models.NewsItem;
import com.roobit.restkit.RestClient;
import com.roobit.restkit.Result;
import java.io.IOException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

public class GetBlogEntriesApiTask extends AsyncTask<Object, Void, NewsItem> {
    private GetBlogEntriesApiDelegate delegate;

    public interface GetBlogEntriesApiDelegate {
        void getEntriesCancelled();

        void getEntriesFinished(NewsItem newsItem);

        void getEntriesStarted();
    }

    public GetBlogEntriesApiTask(GetBlogEntriesApiDelegate delegate2) {
        this.delegate = delegate2;
    }

    /* access modifiers changed from: protected */
    public NewsItem doInBackground(Object... args) {
        Result result = RestClient.getClientWithBaseUrl(ReverbNationApi.getBaseUrl((String) args[1])).get(ReverbNationApi.getInstance().getRequiredHeaderParameters((Context) args[2])).setResource("artists/" + ((String) args[0]) + "/blogs.json").synchronousExecute();
        if (!result.isSuccess()) {
            return null;
        }
        try {
            return (NewsItem) RestClient.getObjectMapper().readValue(result.getResponse(), NewsItem.class);
        } catch (JsonParseException e) {
            e.printStackTrace();
            return null;
        } catch (JsonMappingException e2) {
            e2.printStackTrace();
            return null;
        } catch (IOException e3) {
            e3.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        this.delegate.getEntriesCancelled();
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(NewsItem entries) {
        this.delegate.getEntriesFinished(entries);
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        this.delegate.getEntriesStarted();
    }
}
