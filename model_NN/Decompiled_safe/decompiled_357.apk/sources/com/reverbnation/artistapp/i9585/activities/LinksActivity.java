package com.reverbnation.artistapp.i9585.activities;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.ClipboardManager;
import android.text.Spanned;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.google.common.base.Strings;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.adapters.LinksAdapter;
import com.reverbnation.artistapp.i9585.api.FacebookApi;
import com.reverbnation.artistapp.i9585.api.tasks.FacebookWallPostApiTask;
import com.reverbnation.artistapp.i9585.api.tasks.GetArtistLinksApiTask;
import com.reverbnation.artistapp.i9585.models.FacebookResponse;
import com.reverbnation.artistapp.i9585.models.Links;
import com.reverbnation.artistapp.i9585.models.interfaces.FacebookShareable;
import com.reverbnation.artistapp.i9585.utils.AnalyticsHelper;
import com.reverbnation.artistapp.i9585.utils.LinkShortener;
import java.util.List;
import org.codehaus.jackson.util.MinimalPrettyPrinter;
import twitter4j.TwitterException;

public class LinksActivity extends BaseListTabChildActivity {
    private static final int COPY_LINK_ITEM = 2;
    private static final int EMAIL_LINK_ITEM = 5;
    private static final int OPEN_IN_BROWSER_ITEM = 1;
    private static final int PROGRESS_DIALOG = 6;
    private static final int SHARE_FB_FAILED_DIALOG = 2;
    private static final int SHARE_FB_PROGRESS_DIALOG = 0;
    private static final int SHARE_FB_SUCCESS_DIALOG = 1;
    private static final int SHARE_ON_FACEBOOK_ITEM = 4;
    private static final int SHARE_ON_TWITTER_ITEM = 3;
    private static final int SHARE_TWITTER_FAILED_DIALOG = 5;
    private static final int SHARE_TWITTER_PROGRESS_DIALOG = 3;
    private static final int SHARE_TWITTER_SUCCESS_DIALOG = 4;
    protected static final String TAG = "LinksActivity";
    private Links.Link selectedLink;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.links_activity);
        getLinks();
        registerForContextMenu(getListView());
    }

    private void getLinks() {
        showDialog(6);
        getLinks(getActivityHelper().getArtistId(), null, new GetArtistLinksApiTask.GetArtistLinksApiDelegate() {
            public void getArtistLinksFinished(Links links) {
                LinksActivity.this.getActivityHelper().dismissDialog(6);
                LinksActivity.this.setListUsingLinks(links);
            }

            public void getArtistLinksStarted() {
            }

            public void getArtistLinksCancelled() {
            }
        });
    }

    private void getLinks(String artist_id, String baseUrlOrNull, GetArtistLinksApiTask.GetArtistLinksApiDelegate delegate) {
        new GetArtistLinksApiTask(delegate).execute(artist_id, baseUrlOrNull, this);
    }

    /* access modifiers changed from: private */
    public void setListUsingLinks(Links links) {
        setListAdapter(links == null ? null : new LinksAdapter(this, R.layout.links_list_item, links.getLinks()));
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        AnalyticsHelper.getInstance(this).trackPageView("homescreen/band_info/links");
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        AnalyticsHelper.getInstance(this).trackPageView("homescreen/band_info/links/link_details");
        openContextMenu(v);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        Dialog dialog = null;
        switch (id) {
            case 0:
                dialog = getActivityHelper().createProgressDialog(R.string.share_link, R.string.sharing_link_to_facebook);
                break;
            case 1:
            case 4:
                dialog = getActivityHelper().createDialog((int) R.string.share_link, (int) R.string.link_was_shared);
                break;
            case 2:
                dialog = getActivityHelper().createDialog((int) R.string.share_link, (int) R.string.sharing_link_to_facebook_failed);
                break;
            case 3:
                dialog = getActivityHelper().createProgressDialog(R.string.share_link, R.string.sharing_link_to_twitter);
                break;
            case 5:
                dialog = getActivityHelper().createDialog((int) R.string.share_link, (int) R.string.sharing_link_to_twitter_failed);
                break;
            case 6:
                dialog = getActivityHelper().createProgressDialog(0, R.string.loading);
                break;
        }
        if (dialog != null) {
            return dialog;
        }
        return super.onCreateDialog(id);
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.setHeaderTitle(getString(R.string.link_action));
        menu.add(0, 1, 0, getString(R.string.openBrowser));
        menu.add(0, 2, 0, getString(R.string.copyLink));
        menu.add(0, 3, 0, getString(R.string.share_on_twitter));
        menu.add(0, 4, 0, getString(R.string.share_on_facebook));
        menu.add(0, 5, 0, getString(R.string.email_link));
    }

    public boolean onContextItemSelected(MenuItem item) {
        Links.Link link = (Links.Link) getListAdapter().getItem(((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).position);
        switch (item.getItemId()) {
            case 1:
                AnalyticsHelper.getInstance(this).trackEvent("device", "open", "link");
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse(link.getUrl())));
                break;
            case 2:
                AnalyticsHelper.getInstance(this).trackEvent("device", "copy", "link");
                ((ClipboardManager) getSystemService("clipboard")).setText(link.getUrl());
                Toast.makeText(this, (int) R.string.link_was_copied, 1).show();
                break;
            case 3:
                AnalyticsHelper.getInstance(this).trackEvent("sharing", "twitter", "link");
                onShareTwitter(link);
                break;
            case 4:
                AnalyticsHelper.getInstance(this).trackEvent("sharing", "facebook", "link");
                onShareFacebook(link);
                break;
            case 5:
                AnalyticsHelper.getInstance(this).trackEvent("sharing", "email", "link");
                getActivityHelper().getEmailHelper().startShareActivity(this, getMailSubject(link), getMailBody(link));
                break;
            default:
                return false;
        }
        return true;
    }

    private String getMailSubject(Links.Link link) {
        return getString(R.string.check_out) + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + getString(R.string.artist_name) + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + getString(R.string.on) + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + link.getWebsite();
    }

    private Spanned getMailBody(Links.Link link) {
        return getActivityHelper().getEmailHelper().getShareBody(this, link.getUrl(), getString(R.string.artist_name) + " - " + link.getWebsite());
    }

    /* access modifiers changed from: protected */
    public void onShareFacebook(final Links.Link link) {
        Facebook facebook = FacebookApi.getInstance(this);
        if (!Strings.isNullOrEmpty(facebook.getAccessToken())) {
            shareOnFacebook(link);
            return;
        }
        AnalyticsHelper.getInstance(this).trackPageView("social_facebook_login");
        facebook.authorize(this, FacebookApi.getRequiredPermissions(), new Facebook.DialogListener() {
            public void onComplete(Bundle values) {
                FacebookApi.setAccessToken(values.getString(Facebook.TOKEN));
                LinksActivity.this.shareOnFacebook(link);
            }

            public void onFacebookError(FacebookError e) {
            }

            public void onError(DialogError e) {
            }

            public void onCancel() {
            }
        });
    }

    /* access modifiers changed from: private */
    public void shareOnFacebook(Links.Link link) {
        AnalyticsHelper.getInstance(this).trackEvent("sharing", "facebook", "link");
        showDialog(0);
        try {
            new FacebookWallPostApiTask(this) {
                /* access modifiers changed from: protected */
                public void onPostExecute(FacebookResponse response) {
                    LinksActivity.this.getActivityHelper().dismissDialog(0);
                    if (response.isSuccessful()) {
                        LinksActivity.this.showDialog(1);
                    } else {
                        LinksActivity.this.showDialog(2);
                    }
                }
            }.execute(new FacebookShareable[]{link});
        } catch (IllegalStateException e) {
            Log.e(TAG, e.toString());
            showDialog(2);
        }
    }

    /* access modifiers changed from: protected */
    public void onShareTwitter(Links.Link link) {
        if (!getActivityHelper().isTwitterAuthorized()) {
            getActivityHelper().getTwitterHelper().beginTwitterAuthentication();
        } else {
            shareOnTwitter(link);
        }
    }

    private void shareOnTwitter(final Links.Link link) {
        showDialog(3);
        getActivityHelper().getApplication().shorten(new String[]{link.getUrl(), getActivityHelper().getApplication().getMarketplaceLink()}, new LinkShortener.ShortenListener() {
            public void onLinkReady(List<String> shortened) {
                try {
                    LinksActivity.this.getActivityHelper().getTwitter().updateStatus(LinksActivity.this.getActivityHelper().getTwitterHelper().getLinkShareText(link.getWebsite(), shortened.get(0), shortened.get(1)));
                    LinksActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            LinksActivity.this.showDialog(4);
                        }
                    });
                } catch (TwitterException e) {
                    TwitterException e2 = e;
                    LinksActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            LinksActivity.this.showDialog(5);
                        }
                    });
                    e2.printStackTrace();
                } finally {
                    LinksActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            LinksActivity.this.getActivityHelper().dismissDialog(3);
                        }
                    });
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        FacebookApi.getInstance(this).authorizeCallback(requestCode, resultCode, data);
        if (requestCode != 100) {
            return;
        }
        if (resultCode == -1) {
            shareOnTwitter(this.selectedLink);
        } else {
            getActivityHelper().getTwitterHelper().showTwitterAuthenticationError(data);
        }
    }
}
