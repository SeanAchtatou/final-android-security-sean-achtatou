package com.reverbnation.artistapp.i9585.api;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import com.facebook.android.Facebook;
import com.google.common.base.Strings;
import com.reverbnation.artistapp.i9585.models.FacebookResponse;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

public class FacebookApi {
    private static final String PRODUCTION_APP_ID = "143806795669845";
    private static final String[] REQUIRED_PERMISSIONS = {"read_stream", "publish_stream", "offline_access"};
    private static final String ROOBIT_TEST_APP_ID = "234519803245896";
    private static final String TAG = "FacebookApi";
    private static Facebook facebook;
    private static SharedPreferences facebookSettings;
    private static String widgetUrl;

    public static Facebook getInstance(Context context) {
        if (facebook == null) {
            facebook = new Facebook(isDebugBuild(context) ? ROOBIT_TEST_APP_ID : PRODUCTION_APP_ID);
            facebookSettings = context.getSharedPreferences("FacebookPrefs", 0);
            facebook.setAccessToken(facebookSettings.getString(Facebook.TOKEN, ""));
        }
        return facebook;
    }

    private static boolean isDebugBuild(Context context) {
        return (context.getApplicationInfo().flags & 2) != 0;
    }

    public static boolean hasAccessToken() {
        return !Strings.isNullOrEmpty(facebook.getAccessToken());
    }

    public static void setAccessToken(String token) {
        facebook.setAccessToken(token);
        SharedPreferences.Editor editor = facebookSettings.edit();
        editor.putString(Facebook.TOKEN, token);
        editor.commit();
    }

    public static String[] getRequiredPermissions() {
        return REQUIRED_PERMISSIONS;
    }

    public static Bundle getParametersForWallPost(String name, String description, String link, String sourceURL) {
        Bundle parameters = new Bundle();
        if (!Strings.isNullOrEmpty(link)) {
            parameters.putString("link", link);
        }
        if (!Strings.isNullOrEmpty(name)) {
            parameters.putString("name", name);
        }
        if (!Strings.isNullOrEmpty(description)) {
            parameters.putString("description", description);
        }
        if (!Strings.isNullOrEmpty(sourceURL)) {
            parameters.putString("source", sourceURL);
        }
        return parameters;
    }

    public static FacebookResponse postToWall(Bundle parameters) {
        FacebookResponse response = new FacebookResponse();
        try {
            return new FacebookResponse(facebook.request("me/feed", parameters, "POST"));
        } catch (FileNotFoundException e) {
            FileNotFoundException e2 = e;
            Log.e(TAG, "Error posting to wall", e2);
            response.setRequestError(e2);
            return response;
        } catch (MalformedURLException e3) {
            MalformedURLException e4 = e3;
            Log.e(TAG, "Error posting to wall", e4);
            response.setRequestError(e4);
            return response;
        } catch (IOException e5) {
            IOException e6 = e5;
            Log.e(TAG, "Error posting to wall", e6);
            response.setRequestError(e6);
            return response;
        }
    }

    public static FacebookResponse get(String graphPath) {
        FacebookResponse response = new FacebookResponse();
        try {
            return new FacebookResponse(facebook.request(graphPath));
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return response;
        } catch (IOException e2) {
            e2.printStackTrace();
            return response;
        }
    }

    public static String getWidgetUrl() {
        return widgetUrl;
    }

    public static void setWidgetUrl(String widgetUrl2) {
        widgetUrl = widgetUrl2;
    }
}
