package com.reverbnation.artistapp.i9585.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.Gallery;

public class SingleAdvanceGallery extends Gallery {
    public SingleAdvanceGallery(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return false;
    }
}
