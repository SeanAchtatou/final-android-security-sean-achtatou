package com.reverbnation.artistapp.i9585.activities;

import android.app.Activity;
import android.os.Bundle;
import com.reverbnation.artistapp.i9585.R;

public class AboutActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.about_activity);
    }
}
