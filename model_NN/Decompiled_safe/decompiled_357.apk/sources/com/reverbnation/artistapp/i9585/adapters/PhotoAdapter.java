package com.reverbnation.artistapp.i9585.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.utils.DrawableManager;
import java.util.List;

public class PhotoAdapter extends ArrayAdapter<String> {
    private DrawableManager drawableManager;

    public PhotoAdapter(Context context, int textViewResourceId, List<String> imageUrls) {
        super(context, textViewResourceId, imageUrls);
        this.drawableManager = DrawableManager.getInstance(context);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView v = (ImageView) convertView;
        if (v == null) {
            v = new ImageView(getContext());
            v.setScaleType(ImageView.ScaleType.CENTER_CROP);
            v.setBackgroundColor(R.color.photo_background);
            v.setLayoutParams(new AbsListView.LayoutParams(-1, getContext().getResources().getDimensionPixelSize(R.dimen.photo_thumbnail_height)));
        }
        v.setImageResource(17170445);
        this.drawableManager.bindDrawable((String) getItem(position), v);
        return v;
    }
}
