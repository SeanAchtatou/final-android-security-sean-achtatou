package com.reverbnation.artistapp.i9585.views;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.OverlayItem;
import com.reverbnation.artistapp.i9585.models.ShowList;
import java.text.SimpleDateFormat;

public class ShowOverlayItem extends OverlayItem {
    private ShowList.Show show;

    public ShowOverlayItem(GeoPoint point, ShowList.Show show2) {
        super(point, getVenueName(show2), getSnippet(show2));
        this.show = show2;
    }

    public ShowList.Show getShow() {
        return this.show;
    }

    private static String getSnippet(ShowList.Show show2) {
        return show2.getShowTime(new SimpleDateFormat("MMM ")) + getOrdinalDay(show2.getShowTime(new SimpleDateFormat("d"))) + show2.getShowTime(new SimpleDateFormat(" h:mm aa")) + " - " + getVenueLocation(show2);
    }

    private static String getOrdinalDay(String day) {
        return day + getOrdinalFor(Integer.parseInt(day));
    }

    public static String getOrdinalFor(int value) {
        int hundredRemainder = value % 100;
        if (hundredRemainder >= 10 && hundredRemainder <= 20) {
            return "th";
        }
        switch (value % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }

    private static String getVenueLocation(ShowList.Show show2) {
        if (!show2.hasCity()) {
            return "" + show2.getState();
        }
        return ("" + show2.getCity()) + ", " + show2.getState();
    }

    private static String getVenueName(ShowList.Show show2) {
        return show2.getVenue().getName();
    }
}
