package com.reverbnation.artistapp.i9585.views;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.models.SongList;
import com.reverbnation.artistapp.i9585.utils.TimeFormatter;

public class MusicPlayer extends RelativeLayout {
    private static final long ANIMATION_DURATION_MILLIS = 825;
    private static boolean FirstEnable = true;
    static boolean IsButtonEnabled = false;
    private static final String TAG = "MusicPlayer";
    private ViewGroup button;
    private TextView currentTimemarkText;
    private TextView durationTimemarkText;
    private ImageView playControlImage;
    private ProgressBar preparingProgress;
    private ProgressBar songProgressBar;
    private TextView songTitleText;

    public MusicPlayer(Context context, AttributeSet attrs) {
        super(context, attrs);
        ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.music_player, this);
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        setupViews();
        setVisibility(8);
    }

    private void setupViews() {
        this.songTitleText = (TextView) findViewById(R.id.song_title_text);
        this.currentTimemarkText = (TextView) findViewById(R.id.current_time_mark_text);
        this.durationTimemarkText = (TextView) findViewById(R.id.end_time_mark_text);
        this.songProgressBar = (ProgressBar) findViewById(R.id.song_progress);
        this.playControlImage = (ImageView) findViewById(R.id.play_control);
        this.preparingProgress = (ProgressBar) findViewById(R.id.progressbar_music);
        findViewById(R.id.close_text).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MusicPlayer.this.animateClose();
            }
        });
    }

    public void animateClose() {
        startInFromTopAnimation(this.button, new Animation.AnimationListener() {
            public void onAnimationEnd(Animation arg0) {
                MusicPlayer.this.startButtonAnimation();
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });
        startOutToTopAnimation(this);
        setVisibility(8);
    }

    public void setSong(SongList.Song currentSong) {
        if (currentSong != null) {
            setSongTitle(currentSong.getName());
        }
    }

    public void animateOpen() {
        startOutToTopAnimation(this.button);
        startInFromTopAnimation(this);
        setVisibility(0);
        this.button.setVisibility(4);
    }

    private void startInFromTopAnimation(View v) {
        v.startAnimation(getInFromTopAnimation());
    }

    private void startInFromTopAnimation(View v, Animation.AnimationListener listener) {
        Animation anim = getInFromTopAnimation();
        anim.setAnimationListener(listener);
        v.startAnimation(anim);
    }

    private Animation getInFromTopAnimation() {
        Animation anim = new TranslateAnimation(2, 0.0f, 2, 0.0f, 2, -1.0f, 2, 0.0f);
        anim.setDuration(ANIMATION_DURATION_MILLIS);
        anim.setInterpolator(new AccelerateInterpolator());
        return anim;
    }

    private void startOutToTopAnimation(View v) {
        v.startAnimation(getOutToTopAnimation());
    }

    private Animation getOutToTopAnimation() {
        Animation anim = new TranslateAnimation(2, 0.0f, 2, 0.0f, 2, 0.0f, 2, -1.0f);
        anim.setDuration(ANIMATION_DURATION_MILLIS);
        anim.setInterpolator(new AccelerateInterpolator());
        return anim;
    }

    public void setCurrentTimemark(String time) {
        this.currentTimemarkText.setText(time);
    }

    public void setDurationTimemark(String time) {
        this.durationTimemarkText.setText(time);
    }

    public void showProgress(boolean progressing) {
        this.preparingProgress.setVisibility(progressing ? 0 : 4);
        this.preparingProgress.invalidate();
    }

    public void setPlaying(boolean isPlaying) {
        if (isPlaying) {
            setPauseControlImage();
        } else {
            setPlayControlImage();
        }
    }

    private void setPauseControlImage() {
        this.playControlImage.setImageResource(R.drawable.icon_music_pause);
        this.playControlImage.invalidate();
    }

    private void setPlayControlImage() {
        this.playControlImage.setImageResource(R.drawable.icon_music_play);
        this.playControlImage.invalidate();
    }

    public void updateProgress(long progressSeconds) {
        setCurrentTimemark(TimeFormatter.getFormattedTime(progressSeconds));
        this.songProgressBar.setProgress((int) progressSeconds);
        this.currentTimemarkText.invalidate();
        this.songProgressBar.invalidate();
    }

    public void updateProgress(long progressSeconds, long durationSeconds) {
        setCurrentTimemark(TimeFormatter.getFormattedTime(progressSeconds));
        setDurationTimemark(TimeFormatter.getFormattedTime(durationSeconds));
        this.songProgressBar.setMax((int) durationSeconds);
        this.songProgressBar.setProgress((int) progressSeconds);
        this.currentTimemarkText.invalidate();
        this.durationTimemarkText.invalidate();
        this.songProgressBar.invalidate();
    }

    public void setButton(ViewGroup button2) {
        this.button = button2;
    }

    /* access modifiers changed from: private */
    public void startButtonAnimation() {
        if (this.button != null && IsButtonEnabled) {
            this.button.setVisibility(0);
            Animation anim = new AlphaAnimation(0.8f, 0.35f);
            anim.setDuration(1500);
            anim.setRepeatCount(-1);
            anim.setRepeatMode(2);
            this.button.startAnimation(anim);
        }
    }

    public void setSongTitle(String title) {
        this.songTitleText.setText(title);
        this.songTitleText.invalidate();
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int visibility) {
        super.onWindowVisibilityChanged(visibility);
        Log.v(TAG, "Window visibility changed: " + visibility);
        if (visibility == 0) {
            setVisibility(8);
        }
    }

    public void setButtonEnabled() {
        Log.v(TAG, "Enabling button");
        if (!IsButtonEnabled) {
            IsButtonEnabled = true;
            this.button.setVisibility(0);
            if (FirstEnable) {
                FirstEnable = false;
                Log.v(TAG, "Starting button in from top animation");
                startInFromTopAnimation(this.button, new Animation.AnimationListener() {
                    public void onAnimationEnd(Animation args) {
                        Log.v(MusicPlayer.TAG, "Animation ended");
                        MusicPlayer.this.startButtonAnimation();
                    }

                    public void onAnimationRepeat(Animation animation) {
                    }

                    public void onAnimationStart(Animation animation) {
                    }
                });
                return;
            }
            return;
        }
        startButtonAnimation();
    }

    public void reset() {
        setSongTitle("");
        updateProgress(0, 0);
    }
}
