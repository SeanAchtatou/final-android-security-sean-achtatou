package com.reverbnation.artistapp.i9585.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.Gallery;

public class ViewOnlyGallery extends Gallery {
    public ViewOnlyGallery(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }
}
