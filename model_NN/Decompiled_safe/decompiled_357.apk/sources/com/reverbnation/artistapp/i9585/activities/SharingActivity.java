package com.reverbnation.artistapp.i9585.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.google.common.base.Strings;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.api.FacebookApi;
import com.reverbnation.artistapp.i9585.models.FacebookResponse;
import com.reverbnation.artistapp.i9585.utils.AnalyticsHelper;
import com.reverbnation.artistapp.i9585.utils.FacebookHelper;
import java.io.IOException;
import java.net.MalformedURLException;
import org.codehaus.jackson.util.MinimalPrettyPrinter;
import twitter4j.TwitterException;

public class SharingActivity extends BaseTabChildActivity {
    protected static final int SHOW_PROGRESS_MSG = 2;
    protected static final int UPDATE_FACEBOOK_MSG = 1;
    protected static final int UPDATE_TWITTER_MSG = 0;
    private ImageView facebookButton;
    private FacebookHelper facebookHelper;
    /* access modifiers changed from: private */
    public String facebookName;
    private TextView facebookNameText;
    /* access modifiers changed from: private */
    public Handler handler = null;
    /* access modifiers changed from: private */
    public boolean hasFacebookSession = false;
    /* access modifiers changed from: private */
    public boolean hasTwitterSession = false;
    private View progressBackground;
    private View progressBar;
    private int progressRefCount = 0;
    private ImageView twitterButton;
    /* access modifiers changed from: private */
    public String twitterName;
    private TextView twitterNameText;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.sharing_activity);
        setUpViews();
        initHandler();
    }

    private void setUpViews() {
        findViewById(R.id.background_pulse).setTag(Integer.valueOf((int) R.drawable.background_pulse));
        this.progressBar = findViewById(R.id.progress_bar);
        this.progressBar.setVisibility(4);
        this.progressBackground = findViewById(R.id.progress_background);
        this.progressBackground.setVisibility(4);
        this.twitterNameText = (TextView) findViewById(R.id.screen_name_text);
        this.twitterButton = (ImageView) findViewById(R.id.twitter_connect_image);
        this.facebookNameText = (TextView) findViewById(R.id.facebook_name_text);
        this.facebookButton = (ImageView) findViewById(R.id.facebook_connect_image);
    }

    private void initHandler() {
        this.handler = new Handler(new Handler.Callback() {
            public boolean handleMessage(Message msg) {
                switch (msg.what) {
                    case 0:
                        SharingActivity.this.hideProgress();
                        SharingActivity.this.updateTwitterViews();
                        return false;
                    case 1:
                        SharingActivity.this.hideProgress();
                        SharingActivity.this.updateFacebookViews();
                        return false;
                    case 2:
                        SharingActivity.this.showProgress();
                        return false;
                    default:
                        return false;
                }
            }
        });
        updateTwitterStateAsync();
        updateFacebookStateAsync();
    }

    /* access modifiers changed from: private */
    public void hideProgress() {
        if (this.progressRefCount > 0) {
            this.progressRefCount--;
            if (this.progressRefCount == 0) {
                this.progressBackground.setVisibility(4);
                this.progressBar.setVisibility(4);
            }
        }
    }

    /* access modifiers changed from: private */
    public void showProgress() {
        if (this.progressRefCount == 0) {
            this.progressBackground.setVisibility(0);
            this.progressBar.setVisibility(0);
        }
        this.progressRefCount++;
    }

    /* access modifiers changed from: private */
    public void updateTwitterViews() {
        this.twitterButton.setImageResource(this.hasTwitterSession ? R.drawable.socialmedia_twitter_logout_button : R.drawable.socialmedia_twitter_login_button);
        this.twitterNameText.setText("");
        if (this.hasTwitterSession) {
            displayLoggedInAs(this.twitterNameText, this.twitterName);
        }
    }

    private void displayLoggedInAs(TextView tv, String userName) {
        tv.setText(getString(R.string.logged_in) + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + userName);
    }

    /* access modifiers changed from: protected */
    public void updateFacebookViews() {
        this.facebookButton.setImageResource(this.hasFacebookSession ? R.drawable.socialmedia_facebook_logout_button : R.drawable.socialmedia_facebook_login_button);
        this.facebookNameText.setText("");
        if (this.hasFacebookSession) {
            displayLoggedInAs(this.facebookNameText, this.facebookName);
        }
    }

    public void updateTwitterStateAsync() {
        this.handler.sendEmptyMessage(2);
        new Thread(new Runnable() {
            public void run() {
                String unused = SharingActivity.this.twitterName = "";
                boolean unused2 = SharingActivity.this.hasTwitterSession = SharingActivity.this.getActivityHelper().isTwitterAuthorized();
                if (SharingActivity.this.hasTwitterSession) {
                    try {
                        String unused3 = SharingActivity.this.twitterName = SharingActivity.this.getActivityHelper().getTwitterHelper().getScreenName();
                        if (Strings.isNullOrEmpty(SharingActivity.this.twitterName)) {
                            String unused4 = SharingActivity.this.twitterName = SharingActivity.this.getActivityHelper().getTwitter().getScreenName();
                            SharingActivity.this.getActivityHelper().getTwitterHelper().cacheScreenName(SharingActivity.this.twitterName);
                        }
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    } catch (TwitterException e2) {
                        e2.printStackTrace();
                    }
                }
                SharingActivity.this.handler.sendEmptyMessage(0);
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public void updateFacebookStateAsync() {
        this.handler.sendEmptyMessage(2);
        new Thread(new Runnable() {
            public void run() {
                String unused = SharingActivity.this.facebookName = "";
                boolean unused2 = SharingActivity.this.hasFacebookSession = FacebookApi.getInstance(SharingActivity.this.getBaseContext()).isSessionValid();
                if (SharingActivity.this.hasFacebookSession) {
                    String unused3 = SharingActivity.this.facebookName = SharingActivity.this.getFacebookHelper().getUserName();
                    if (Strings.isNullOrEmpty(SharingActivity.this.facebookName)) {
                        FacebookResponse response = FacebookApi.get("me");
                        boolean unused4 = SharingActivity.this.hasFacebookSession = response.isSuccessful();
                        if (SharingActivity.this.hasFacebookSession) {
                            String unused5 = SharingActivity.this.facebookName = response.getName();
                            SharingActivity.this.getFacebookHelper().cacheUserName(SharingActivity.this.facebookName);
                        }
                    }
                }
                SharingActivity.this.handler.sendEmptyMessage(1);
            }
        }).start();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        AnalyticsHelper.getInstance(this).trackPageView("homescreen/social/sharing");
        super.onResume();
    }

    public void onTwitterConnectClick(View v) {
        if (!getActivityHelper().isTwitterAuthorized()) {
            AnalyticsHelper.getInstance(this).trackEvent("social", "twitter", "login");
            getActivityHelper().getTwitterHelper().beginTwitterAuthentication();
            return;
        }
        AnalyticsHelper.getInstance(this).trackEvent("social", "twitter", "logout");
        getActivityHelper().getApplication().twitterLogout();
        getActivityHelper().getTwitterHelper().cacheScreenName("");
        updateTwitterStateAsync();
    }

    public void onFacebookConnectClick(View v) {
        if (!this.hasFacebookSession) {
            AnalyticsHelper.getInstance(this).trackPageView("social_facebook_login");
            AnalyticsHelper.getInstance(this).trackEvent("social", "facebook", "login");
            FacebookApi.getInstance(this).authorize(this, FacebookApi.getRequiredPermissions(), new Facebook.DialogListener() {
                public void onComplete(Bundle values) {
                    FacebookApi.setAccessToken(values.getString(Facebook.TOKEN));
                    SharingActivity.this.updateFacebookStateAsync();
                }

                public void onFacebookError(FacebookError e) {
                }

                public void onError(DialogError e) {
                }

                public void onCancel() {
                }
            });
            return;
        }
        logoutFacebook();
    }

    private void logoutFacebook() {
        AnalyticsHelper.getInstance(this).trackEvent("social", "facebook", "logout");
        new Thread(new Runnable() {
            public void run() {
                try {
                    FacebookApi.getInstance(SharingActivity.this.getBaseContext()).logout(SharingActivity.this.getBaseContext());
                    SharingActivity.this.getFacebookHelper().cacheUserName("");
                    SharingActivity.this.updateFacebookStateAsync();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
        }).start();
    }

    /* access modifiers changed from: protected */
    public FacebookHelper getFacebookHelper() {
        if (this.facebookHelper == null) {
            this.facebookHelper = new FacebookHelper(this);
        }
        return this.facebookHelper;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        FacebookApi.getInstance(this).authorizeCallback(requestCode, resultCode, data);
        if (requestCode != 100) {
            return;
        }
        if (resultCode == -1) {
            updateTwitterStateAsync();
        } else {
            getActivityHelper().getTwitterHelper().showTwitterAuthenticationError(data);
        }
    }
}
