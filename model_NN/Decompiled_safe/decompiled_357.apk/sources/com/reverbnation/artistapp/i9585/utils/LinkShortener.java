package com.reverbnation.artistapp.i9585.utils;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.reverbnation.artistapp.i9585.R;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class LinkShortener {
    private static final String DOMAIN_PREFIX = "http://rvb.fm/";
    private static final int MAX_ATTEMPTS = 5;
    private static final String TAG = "LinkShortener";
    private String apiKey;
    private Context context;
    private String login;
    private String url;

    public interface ShortenListener {
        void onLinkReady(List<String> list);
    }

    public LinkShortener(Context context2) {
        this.context = context2;
        loadBitlyProperties();
    }

    public void shorten(final String[] links, final ShortenListener listener) {
        new Thread() {
            public void run() {
                listener.onLinkReady(Lists.transform(Arrays.asList(links), new Function<String, String>() {
                    public String apply(String linkToShorten) {
                        return LinkShortener.this.shorten(linkToShorten);
                    }
                }));
            }
        }.start();
    }

    /* JADX INFO: finally extract failed */
    public String shorten(String link) {
        String result;
        int attempt = 5;
        while (attempt > 0) {
            try {
                HttpResponse response = new DefaultHttpClient().execute(new HttpGet(getUrl(link)));
                if (response.getStatusLine().getStatusCode() != 200 || (result = getResponseBody(response)) == null || result.length() <= DOMAIN_PREFIX.length()) {
                    attempt--;
                } else {
                    int attempt2 = attempt - 1;
                    return result.trim();
                }
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                attempt--;
            } catch (IOException e2) {
                e2.printStackTrace();
                attempt--;
            } catch (Throwable th) {
                int attempt3 = attempt - 1;
                throw th;
            }
        }
        Log.v(TAG, "Shorten failed");
        return null;
    }

    private String getUrl(String link) {
        return Uri.parse(this.url).buildUpon().appendQueryParameter("login", this.login).appendQueryParameter("apiKey", this.apiKey).appendQueryParameter("longUrl", link).appendQueryParameter("format", "txt").build().toString();
    }

    private String getResponseBody(HttpResponse response) {
        try {
            return convertStreamToString(response.getEntity().getContent());
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        return null;
    }

    private static String convertStreamToString(InputStream is) throws IOException {
        int read;
        char[] buffer = new char[65536];
        StringBuilder sb = new StringBuilder();
        Reader in = new InputStreamReader(is, "UTF-8");
        do {
            read = in.read(buffer, 0, buffer.length);
            if (read > 0) {
                sb.append(buffer, 0, read);
                continue;
            }
        } while (read >= 0);
        return sb.toString();
    }

    private void loadBitlyProperties() {
        Properties props = new Properties();
        try {
            props.load(this.context.getResources().openRawResource(R.raw.bitly));
            this.url = props.getProperty("url");
            this.login = props.getProperty("login");
            this.apiKey = props.getProperty("api_key");
        } catch (IOException e) {
            Log.d(TAG, "Failed to load bitly api keys from bitly.properties");
        }
    }
}
