package com.reverbnation.artistapp.i9585.models;

import com.google.common.base.Function;
import com.google.common.collect.Ordering;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.codehaus.jackson.annotate.JsonProperty;

public class NewsItem {
    @JsonProperty("results")
    private ArrayList<BlogPost> entries;

    public List<BlogPost> getEntries() {
        return this.entries;
    }

    public void addEntries(BlogPost blog) {
        if (this.entries == null) {
            this.entries = new ArrayList<>();
        }
        this.entries.add(blog);
    }

    public BlogPost getBlogPostAt(int index) {
        return this.entries.get(index);
    }

    public static List<BlogPost> sortEntriesByDate(List<BlogPost> entries2) {
        return Ordering.natural().reverse().onResultOf(new Function<BlogPost, Date>() {
            public Date apply(BlogPost blog) {
                return blog.getCreatedAtDate();
            }
        }).sortedCopy(entries2);
    }
}
