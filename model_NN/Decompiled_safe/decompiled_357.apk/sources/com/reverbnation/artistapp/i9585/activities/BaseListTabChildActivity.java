package com.reverbnation.artistapp.i9585.activities;

import android.app.ListActivity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.utils.ActivityHelper;
import com.reverbnation.artistapp.i9585.utils.SharedMenu;

public class BaseListTabChildActivity extends ListActivity {
    final ActivityHelper activityHelper = ActivityHelper.getInstance(this);

    public ActivityHelper getActivityHelper() {
        return this.activityHelper;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        SharedMenu.onCreateOptionsMenu(this, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() != R.id.settings) {
            return super.onOptionsItemSelected(item);
        }
        startActivity(new Intent(this, SettingsActivity.class));
        return true;
    }
}
