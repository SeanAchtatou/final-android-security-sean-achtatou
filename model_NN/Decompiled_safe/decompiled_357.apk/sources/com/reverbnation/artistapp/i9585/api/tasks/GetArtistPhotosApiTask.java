package com.reverbnation.artistapp.i9585.api.tasks;

import android.content.Context;
import android.os.AsyncTask;
import com.reverbnation.artistapp.i9585.api.ReverbNationApi;
import com.reverbnation.artistapp.i9585.models.Photoset;
import com.roobit.restkit.RestClient;
import com.roobit.restkit.Result;
import java.io.IOException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

public class GetArtistPhotosApiTask extends AsyncTask<Object, Void, Photoset> {
    private GetArtistPhotosApiDelegate delegate;

    public interface GetArtistPhotosApiDelegate {
        void getArtistPhotosCancelled();

        void getArtistPhotosFinished(Photoset photoset);

        void getArtistPhotosStarted();
    }

    public GetArtistPhotosApiTask(GetArtistPhotosApiDelegate delegate2) {
        this.delegate = delegate2;
    }

    /* access modifiers changed from: protected */
    public Photoset doInBackground(Object... args) {
        Result result = RestClient.getClientWithBaseUrl(ReverbNationApi.getBaseUrl((String) args[1])).get(ReverbNationApi.getInstance().getRequiredHeaderParameters((Context) args[2])).setResource("artists/" + ((String) args[0]) + "/photos.json").synchronousExecute();
        if (!result.isSuccess()) {
            return null;
        }
        try {
            return (Photoset) RestClient.getObjectMapper().readValue(result.getResponse(), Photoset.class);
        } catch (JsonParseException e) {
            e.printStackTrace();
            return null;
        } catch (JsonMappingException e2) {
            e2.printStackTrace();
            return null;
        } catch (IOException e3) {
            e3.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        this.delegate.getArtistPhotosCancelled();
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Photoset result) {
        this.delegate.getArtistPhotosFinished(result);
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        this.delegate.getArtistPhotosStarted();
    }
}
