package com.reverbnation.artistapp.i9585.activities;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.adapters.BlogAdapter;
import com.reverbnation.artistapp.i9585.api.tasks.GetBlogEntriesApiTask;
import com.reverbnation.artistapp.i9585.models.BlogPost;
import com.reverbnation.artistapp.i9585.models.NewsItem;
import com.reverbnation.artistapp.i9585.utils.AnalyticsHelper;

public class BlogActivity extends BaseListTabChildActivity {
    protected static final int PROGRESS_DIALOG = 0;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.blog_activity);
        getBlogEntries();
    }

    private void getBlogEntries() {
        showDialog(0);
        getBlogEntries(getActivityHelper().getArtistId(), null, new GetBlogEntriesApiTask.GetBlogEntriesApiDelegate() {
            public void getEntriesFinished(NewsItem entries) {
                BlogActivity.this.getActivityHelper().dismissDialog(0);
                if (entries != null) {
                    BlogActivity.this.setListAdapter(BlogActivity.this.createBlogAdapter(entries));
                }
            }

            public void getEntriesStarted() {
            }

            public void getEntriesCancelled() {
            }
        });
    }

    /* access modifiers changed from: protected */
    public void getBlogEntries(String artist_id, String baseUrlOrNull, GetBlogEntriesApiTask.GetBlogEntriesApiDelegate delegate) {
        new GetBlogEntriesApiTask(delegate).execute(artist_id, baseUrlOrNull, this);
    }

    /* access modifiers changed from: private */
    public BlogAdapter createBlogAdapter(NewsItem news) {
        return new BlogAdapter(this, R.layout.blog_list_item, NewsItem.sortEntriesByDate(news.getEntries()));
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        AnalyticsHelper.getInstance(this).trackPageView("homescreen/news/blog");
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        BlogPost post = (BlogPost) getListAdapter().getItem(position);
        if (post != null) {
            startActivity(post);
        }
    }

    private void startActivity(BlogPost post) {
        Intent intent = null;
        if (BlogPost.REVERBNATION_TYPE.equals(post.getType())) {
            intent = new Intent(this, BlogPostActivity.class);
            intent.putExtra("blog_post", post);
        } else if (BlogPost.RSS_TYPE.equals(post.getType())) {
            intent = new Intent("android.intent.action.VIEW", Uri.parse(post.getLink()));
        }
        if (intent != null) {
            startActivity(intent);
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        if (id == 0) {
            return getActivityHelper().createProgressDialog(0, R.string.loading);
        }
        return super.onCreateDialog(id);
    }
}
