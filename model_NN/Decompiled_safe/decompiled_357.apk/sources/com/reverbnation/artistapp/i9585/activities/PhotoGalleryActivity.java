package com.reverbnation.artistapp.i9585.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListAdapter;
import com.google.common.base.Preconditions;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.adapters.PhotoAdapter;
import com.reverbnation.artistapp.i9585.api.tasks.GetArtistPhotosApiTask;
import com.reverbnation.artistapp.i9585.models.Photoset;
import com.reverbnation.artistapp.i9585.utils.AnalyticsHelper;
import java.util.ArrayList;
import java.util.List;

public class PhotoGalleryActivity extends BaseTabChildActivity {
    private static final int PROGRESS_DIALOG = 0;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.photo_gallery_activity);
        setupViews();
    }

    private void setupViews() {
        List<String> thumbnails = getThumbnails();
        if (thumbnails != null) {
            setupAdapter(thumbnails);
        } else {
            getPhotos();
        }
    }

    /* access modifiers changed from: private */
    public void setupAdapter(List<String> thumbnails) {
        boolean z;
        if (thumbnails != null) {
            z = true;
        } else {
            z = false;
        }
        Preconditions.checkState(z);
        GridView gridView = (GridView) findViewById(R.id.photo_grid);
        gridView.setAdapter((ListAdapter) new PhotoAdapter(this, 0, thumbnails));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long id) {
                PhotoGalleryActivity.this.startFilmstripActivity(position);
            }
        });
    }

    private List<String> getThumbnails() {
        return getIntent().getStringArrayListExtra("thumbnailUrls");
    }

    private void getPhotos() {
        if (getIntent().getBooleanExtra("shouldGetPhotos", true)) {
            showDialog(0);
            new GetArtistPhotosApiTask(new GetArtistPhotosApiTask.GetArtistPhotosApiDelegate() {
                public void getArtistPhotosFinished(Photoset photoset) {
                    PhotoGalleryActivity.this.getActivityHelper().dismissDialog(0);
                    PhotoGalleryActivity.this.setupAdapter(photoset != null ? photoset.getThumbnailUrls() : new ArrayList());
                    PhotoGalleryActivity.this.getActivityHelper().getApplication().setPhotoset(photoset);
                }

                public void getArtistPhotosStarted() {
                }

                public void getArtistPhotosCancelled() {
                }
            }).execute(getActivityHelper().getArtistId(), null, this);
        }
    }

    /* access modifiers changed from: protected */
    public void startFilmstripActivity(int position) {
        Intent intent = new Intent(this, FilmstripActivity.class);
        intent.putExtra("startPhotoIndex", position);
        intent.putExtra("photoset", getActivityHelper().getApplication().getPhotoset());
        startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        AnalyticsHelper.getInstance(this).trackPageView("homescreen/photos_and_videos/photos");
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        if (id == 0) {
            return getActivityHelper().createProgressDialog(0, R.string.loading);
        }
        return super.onCreateDialog(id);
    }
}
