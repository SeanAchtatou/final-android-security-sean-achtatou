package com.reverbnation.artistapp.i9585.utils;

import java.util.concurrent.TimeUnit;

public class TimeFormatter {
    public static String getFormattedTime(long seconds) {
        return String.format("%d:%02d", Long.valueOf(seconds / 60), Long.valueOf(seconds % 60));
    }

    public static String getFormattedTimeMs(long millis) {
        return String.format("%d:%02d", Long.valueOf(TimeUnit.MILLISECONDS.toSeconds(millis) / 60), Long.valueOf(TimeUnit.MILLISECONDS.toSeconds(millis) % 60));
    }
}
