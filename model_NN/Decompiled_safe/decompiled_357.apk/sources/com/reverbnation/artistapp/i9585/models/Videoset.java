package com.reverbnation.artistapp.i9585.models;

import com.reverbnation.artistapp.i9585.models.interfaces.FacebookShareable;
import com.reverbnation.artistapp.i9585.utils.FacebookHelper;
import com.reverbnation.artistapp.i9585.utils.TimeFormatter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.codehaus.jackson.annotate.JsonProperty;

public class Videoset {
    @JsonProperty("results")
    private ArrayList<Video> videos;

    public static class Video implements Serializable, FacebookShareable {
        private static final long serialVersionUID = -5648949429646258046L;
        @JsonProperty("id")
        private int id;
        @JsonProperty("length")
        private String length;
        @JsonProperty("name")
        private String name;
        @JsonProperty("thumb_url")
        private String thumb_url;
        @JsonProperty("video_url")
        private String video_url;

        public int getId() {
            return this.id;
        }

        public String getLength() {
            return this.length;
        }

        public String getVideoUrl() {
            return this.video_url;
        }

        public String getName() {
            return this.name;
        }

        public String getThumbnailUrl() {
            return this.thumb_url;
        }

        public String getDuration() {
            return this.length == null ? "0:00" : TimeFormatter.getFormattedTime(Long.valueOf(this.length).longValue());
        }

        public String getFacebookCaption(FacebookHelper helper) {
            return helper.getFacebookCaption(this);
        }

        public String getFacebookLink(FacebookHelper helper) {
            return helper.getFacebookLink(this);
        }

        public String getFacebookSource(FacebookHelper helper) {
            return helper.getFacebookSource(this);
        }

        public String getFacebookName(FacebookHelper helper) {
            return helper.getFacebookName(this);
        }

        public String getFacebookPicture(FacebookHelper helper) {
            return helper.getFacebookPicture();
        }
    }

    public List<Video> getVideos() {
        return this.videos;
    }

    public Video getVideoAt(int index) {
        return this.videos.get(index);
    }

    public static String getFormattedDuration(String seconds) {
        return seconds == null ? "0:00" : TimeFormatter.getFormattedTime(Long.valueOf(seconds).longValue());
    }
}
