package com.reverbnation.artistapp.i9585.models;

import android.util.Log;
import com.google.common.base.Strings;
import java.io.IOException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

public class FacebookResponse {
    private final String TAG = "FacebookResponse";
    @JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String name;
    private Exception requestError;
    @JsonProperty("error")
    private FacebookResponseError responseError;

    public void setId(String id2) {
        this.id = id2;
    }

    public String getId() {
        return this.id;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getName() {
        return this.name;
    }

    public void setResponseError(FacebookResponseError error) {
        this.responseError = error;
    }

    public FacebookResponseError getResponseError() {
        return this.responseError;
    }

    public void setRequestError(Exception error) {
        this.requestError = error;
    }

    public Exception getRequestError() {
        return this.requestError;
    }

    public FacebookResponse() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.codehaus.jackson.map.ObjectMapper.configure(org.codehaus.jackson.map.DeserializationConfig$Feature, boolean):org.codehaus.jackson.map.ObjectMapper
     arg types: [org.codehaus.jackson.map.DeserializationConfig$Feature, int]
     candidates:
      org.codehaus.jackson.map.ObjectMapper.configure(org.codehaus.jackson.JsonGenerator$Feature, boolean):org.codehaus.jackson.map.ObjectMapper
      org.codehaus.jackson.map.ObjectMapper.configure(org.codehaus.jackson.JsonParser$Feature, boolean):org.codehaus.jackson.map.ObjectMapper
      org.codehaus.jackson.map.ObjectMapper.configure(org.codehaus.jackson.map.SerializationConfig$Feature, boolean):org.codehaus.jackson.map.ObjectMapper
      org.codehaus.jackson.map.ObjectMapper.configure(org.codehaus.jackson.map.DeserializationConfig$Feature, boolean):org.codehaus.jackson.map.ObjectMapper */
    public FacebookResponse(String json) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        try {
            Log.v("FacebookResponse", json);
            copy((FacebookResponse) mapper.readValue(json, FacebookResponse.class));
        } catch (JsonParseException e) {
            Log.e("FacebookResponse", "Error parsing the JSON", e);
        } catch (JsonMappingException e2) {
            Log.e("FacebookResponse", "Error mapping the JSON", e2);
        } catch (IOException e3) {
            Log.e("FacebookResponse", "Error with the JSON file (not possible)", e3);
        } catch (Exception e4) {
            Log.e("FacebookResponse", "Unforseen Error", e4);
        }
    }

    public void copy(FacebookResponse response) {
        this.id = response.id;
        this.name = response.name;
        this.responseError = response.responseError;
    }

    public boolean isSuccessful() {
        return !Strings.isNullOrEmpty(this.id) && this.responseError == null && this.requestError == null;
    }
}
