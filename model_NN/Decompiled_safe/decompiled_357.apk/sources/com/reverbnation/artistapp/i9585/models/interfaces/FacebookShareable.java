package com.reverbnation.artistapp.i9585.models.interfaces;

import com.reverbnation.artistapp.i9585.utils.FacebookHelper;

public interface FacebookShareable {
    String getFacebookCaption(FacebookHelper facebookHelper);

    String getFacebookLink(FacebookHelper facebookHelper);

    String getFacebookName(FacebookHelper facebookHelper);

    String getFacebookPicture(FacebookHelper facebookHelper);

    String getFacebookSource(FacebookHelper facebookHelper);
}
