package com.reverbnation.artistapp.i9585.activities;

import android.app.Dialog;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.api.tasks.GetArtistDataApiTask;
import com.reverbnation.artistapp.i9585.models.ArtistData;
import com.reverbnation.artistapp.i9585.utils.AnalyticsHelper;
import com.reverbnation.artistapp.i9585.utils.DrawableManager;

public class BiographyActivity extends BaseTabChildActivity {
    private static final int PROGRESS_DIALOG = 0;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.biography_activity);
        loadArtistInfo();
    }

    private void loadArtistInfo() {
        if (isArtistInfoLoaded()) {
            setUpViews(getActivityHelper().getApplication().getArtistData());
            return;
        }
        showDialog(0);
        getArtistInfo(getActivityHelper().getArtistId(), null, new GetArtistDataApiTask.GetArtistDataApiDelegate() {
            public void getArtistDataFinished(ArtistData artistData) {
                BiographyActivity.this.getActivityHelper().dismissDialog(0);
                if (artistData != null) {
                    BiographyActivity.this.getActivityHelper().getApplication().setArtistData(artistData);
                    BiographyActivity.this.setUpViews(artistData);
                }
            }

            public void getArtistDataStarted() {
            }

            public void getArtistDataCancelled() {
            }
        });
    }

    private boolean isArtistInfoLoaded() {
        return getActivityHelper().getApplication().getArtistData() != null;
    }

    /* access modifiers changed from: private */
    public void setUpViews(ArtistData artistData) {
        ((TextView) findViewById(R.id.band_bio)).setText(artistData.getBio().getBio());
        DrawableManager.getInstance(this).bindDrawable(artistData.getImageURL(), (ImageView) findViewById(R.id.band_pic));
    }

    /* access modifiers changed from: protected */
    public void getArtistInfo(String artist_id, String baseUrlOrNull, GetArtistDataApiTask.GetArtistDataApiDelegate delegate) {
        new GetArtistDataApiTask(delegate).execute(artist_id, baseUrlOrNull, this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        AnalyticsHelper.getInstance(this).trackPageView("homescreen/band_info/biography");
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        if (id == 0) {
            return getActivityHelper().createProgressDialog(0, R.string.loading);
        }
        return super.onCreateDialog(id);
    }
}
