package com.reverbnation.artistapp.i9585.activities;

import android.app.TabActivity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.services.AudioPlayer;
import com.reverbnation.artistapp.i9585.services.ReverbMusicService;
import com.reverbnation.artistapp.i9585.utils.ActivityHelper;
import com.reverbnation.artistapp.i9585.utils.MusicPlayerRemoteControl;

public class BaseTabActivity extends TabActivity implements MusicPlayerRemoteControl {
    private static final String TAG = "BaseTabActivity";
    final ActivityHelper activityHelper = ActivityHelper.getInstance(this);
    private ServiceConnection connection = new ServiceConnection() {
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.v(BaseTabActivity.TAG, "Connected to music service");
            BaseTabActivity.this.musicService = ((ReverbMusicService.LocalBinder) service).getService();
            BaseTabActivity.this.onMusicServiceConnected();
        }

        public void onServiceDisconnected(ComponentName name) {
            Log.v(BaseTabActivity.TAG, "Disconnected from music service");
            BaseTabActivity.this.musicService = null;
        }
    };
    private BroadcastReceiver musicPlayerReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(AudioPlayer.REVERB_MEDIA_ACTION)) {
                BaseTabActivity.this.handlePlayerEvent(BaseTabActivity.this.musicService, intent);
            }
        }
    };
    protected ReverbMusicService musicService;

    /* access modifiers changed from: protected */
    public void handlePlayerEvent(ReverbMusicService musicService2, Intent intent) {
        AudioPlayer.MusicActionEvent event = AudioPlayer.MusicActionEvent.values()[intent.getIntExtra("event", AudioPlayer.MusicActionEvent.UNDEFINED_EVENT.ordinal())];
        Log.v(TAG, "Received REVERB_MEDIA_ACTION: " + event.toString());
        switch (event) {
            case MUSIC_READY_EVENT:
                this.activityHelper.enableAudioActivator();
                this.activityHelper.syncMusicPlayerView(musicService2);
                break;
        }
        this.activityHelper.handlePlayerEvent(musicService2, intent);
    }

    /* access modifiers changed from: protected */
    public void onMusicServiceConnected() {
        if (this.musicService.isReady()) {
            this.activityHelper.enableAudioActivator();
        }
        this.activityHelper.syncMusicPlayerView(this.musicService);
    }

    public ActivityHelper getActivityHelper() {
        return this.activityHelper;
    }

    /* access modifiers changed from: protected */
    public View createTabView(int textResourceId) {
        View v = (FrameLayout) getLayoutInflater().inflate((int) R.layout.tab_layout, (ViewGroup) null);
        ((TextView) v.findViewById(R.id.text_tabs)).setText(getString(textResourceId).toUpperCase());
        return v;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        registerReceiver(this.musicPlayerReceiver, new IntentFilter(AudioPlayer.REVERB_MEDIA_ACTION));
        bindService(new Intent(this, ReverbMusicService.class), this.connection, 1);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        unregisterReceiver(this.musicPlayerReceiver);
        unbindService(this.connection);
    }

    public void onPlayControlClicked(View v) {
        getActivityHelper().broadcastKeyCode(85);
    }

    public void onNextControlClicked(View v) {
        getActivityHelper().getMusicPlayer().reset();
        getActivityHelper().broadcastKeyCode(87);
    }

    public void onPreviousControlClicked(View v) {
        getActivityHelper().getMusicPlayer().reset();
        getActivityHelper().broadcastKeyCode(88);
    }
}
