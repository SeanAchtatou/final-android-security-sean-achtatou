package com.reverbnation.artistapp.i9585.utils;

import android.app.Activity;
import android.view.Menu;
import com.reverbnation.artistapp.i9585.R;

public class SharedMenu {
    public static void onCreateOptionsMenu(Activity activity, Menu menu) {
        activity.getMenuInflater().inflate(R.menu.options_menu, menu);
    }
}
