package com.reverbnation.artistapp.i9585.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.services.AudioPlayer;
import com.reverbnation.artistapp.i9585.services.ReverbMusicService;
import com.reverbnation.artistapp.i9585.utils.ActivityHelper;
import com.reverbnation.artistapp.i9585.utils.MusicPlayerRemoteControl;
import com.reverbnation.artistapp.i9585.utils.SharedMenu;

public class BaseActivity extends Activity implements MusicPlayerRemoteControl {
    protected static final String TAG = "BaseActivity";
    final ActivityHelper activityHelper = ActivityHelper.getInstance(this);
    private ServiceConnection connection = new ServiceConnection() {
        public void onServiceConnected(ComponentName name, IBinder service) {
            BaseActivity.this.musicService = ((ReverbMusicService.LocalBinder) service).getService();
            BaseActivity.this.onMusicServiceConnected();
        }

        public void onServiceDisconnected(ComponentName name) {
            Log.v(BaseActivity.TAG, "Disconnected from music service");
            BaseActivity.this.musicService = null;
        }
    };
    private BroadcastReceiver musicPlayerReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(AudioPlayer.REVERB_MEDIA_ACTION)) {
                BaseActivity.this.handlePlayerEvent(BaseActivity.this.musicService, intent);
            }
        }
    };
    protected ReverbMusicService musicService;

    /* access modifiers changed from: protected */
    public void handlePlayerEvent(ReverbMusicService musicService2, Intent intent) {
        switch (AudioPlayer.MusicActionEvent.values()[intent.getIntExtra("event", AudioPlayer.MusicActionEvent.UNDEFINED_EVENT.ordinal())]) {
            case MUSIC_READY_EVENT:
                this.activityHelper.enableAudioActivator();
                this.activityHelper.syncMusicPlayerView(musicService2);
                break;
        }
        this.activityHelper.handlePlayerEvent(musicService2, intent);
    }

    /* access modifiers changed from: protected */
    public void onMusicServiceConnected() {
        if (this.musicService.isReady()) {
            this.activityHelper.enableAudioActivator();
        }
        this.activityHelper.syncMusicPlayerView(this.musicService);
    }

    public ActivityHelper getActivityHelper() {
        return this.activityHelper;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        SharedMenu.onCreateOptionsMenu(this, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() != R.id.settings) {
            return super.onOptionsItemSelected(item);
        }
        startActivity(new Intent(this, SettingsActivity.class));
        return true;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        registerReceiver(this.musicPlayerReceiver, new IntentFilter(AudioPlayer.REVERB_MEDIA_ACTION));
        bindService(new Intent(this, ReverbMusicService.class), this.connection, 1);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        unregisterReceiver(this.musicPlayerReceiver);
        unbindService(this.connection);
    }

    public void onPlayControlClicked(View v) {
        getActivityHelper().broadcastKeyCode(85);
    }

    public void onNextControlClicked(View v) {
        getActivityHelper().broadcastKeyCode(87);
    }

    public void onPreviousControlClicked(View v) {
        getActivityHelper().broadcastKeyCode(88);
    }
}
