package com.reverbnation.artistapp.i9585.api.tasks;

import android.os.AsyncTask;
import android.util.Log;
import com.reverbnation.artistapp.i9585.api.ReverbNationApi;
import com.reverbnation.artistapp.i9585.models.FanReach;
import com.roobit.restkit.RestClient;
import com.roobit.restkit.Result;
import java.io.IOException;
import java.util.Properties;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.util.MinimalPrettyPrinter;

public class PostFanReachApiTask extends AsyncTask<Object, Void, String> {
    private static final String TAG = "PostFanReachApiTask";
    private PostFanReachApiDelegate delegate;

    public interface PostFanReachApiDelegate {
        void fanReachFinished(String str);

        void fanReachStarted();
    }

    public PostFanReachApiTask(PostFanReachApiDelegate delegate2) {
        this.delegate = delegate2;
    }

    /* access modifiers changed from: protected */
    public String doInBackground(Object... args) {
        String artistId = (String) args[0];
        String baseUrl = ReverbNationApi.getBaseUrl((String) args[1]);
        String email = (String) args[2];
        Boolean isStreetTeam = (Boolean) args[3];
        Log.v(TAG, "Street team is" + (isStreetTeam.booleanValue() ? MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR : " not ") + "requested");
        Result result = RestClient.getClientWithBaseUrl(baseUrl).postQuery(buildQueryParameters(email, isStreetTeam.booleanValue()), ReverbNationApi.getInstance().getRequiredHeaderParameters()).setResource("artists/" + artistId + "/fan_reaches.json").synchronousExecute();
        if (!result.isSuccess()) {
            return null;
        }
        try {
            return ((FanReach) RestClient.getObjectMapper().readValue(result.getResponse(), FanReach.class)).insertion_status;
        } catch (JsonParseException e) {
            e.printStackTrace();
            return null;
        } catch (JsonMappingException e2) {
            e2.printStackTrace();
            return null;
        } catch (IOException e3) {
            e3.printStackTrace();
            return null;
        }
    }

    private Properties buildQueryParameters(String email, boolean isStreetTeam) {
        Properties props = new Properties();
        props.put("email", email);
        props.put("street_team", String.valueOf(isStreetTeam ? 1 : 0));
        return props;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(String status) {
        this.delegate.fanReachFinished(status);
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        this.delegate.fanReachStarted();
    }
}
