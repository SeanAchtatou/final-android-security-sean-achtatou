package com.reverbnation.artistapp.i9585.models;

import org.codehaus.jackson.annotate.JsonProperty;

public class MobileSession {
    @JsonProperty("id")
    private int id;
    @JsonProperty("mobile_application_platform_link")
    private MobileApplicationPlatformLink mobileApplicationPlatformLink;

    public int getId() {
        return this.id;
    }

    public MobileApplicationPlatformLink getMobileApplicationPlatformLink() {
        return this.mobileApplicationPlatformLink;
    }
}
