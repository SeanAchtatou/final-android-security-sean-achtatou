package com.reverbnation.artistapp.i9585.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;
import com.reverbnation.artistapp.i9585.R;

public class PhotosAndVideosActivity extends BaseTabActivity {
    private static final String PhotosTab = "photos_tab";
    private static final String VideosTab = "videos_tab";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(7);
        setContentView((int) R.layout.photos_and_videos_activity);
        getActivityHelper().setArtistTitlebar(getString(R.string.photos_and_videos));
        setupTabs();
    }

    private void setupTabs() {
        TabHost tabHost = getTabHost();
        tabHost.addTab(tabHost.newTabSpec(PhotosTab).setIndicator(createTabView(R.string.photos)).setContent(setupPhotoGalleryIntent()));
        tabHost.addTab(tabHost.newTabSpec(VideosTab).setIndicator(createTabView(R.string.videos)).setContent(new Intent(this, VideosActivity.class)));
    }

    private Intent setupPhotoGalleryIntent() {
        Intent intent = new Intent(this, PhotoGalleryActivity.class);
        intent.putExtra("shouldGetPhotos", getIntent().getBooleanExtra("shouldGetPhotos", true));
        return intent;
    }
}
