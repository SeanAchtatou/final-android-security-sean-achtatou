package com.reverbnation.artistapp.i9585.activities;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.view.View;
import com.reverbnation.artistapp.i9585.R;

public class SettingsActivity extends PreferenceActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.user_preferences);
        setContentView((int) R.layout.settings_activity);
    }

    public void onAboutClick(View v) {
        startActivity(new Intent(this, AboutActivity.class));
    }
}
