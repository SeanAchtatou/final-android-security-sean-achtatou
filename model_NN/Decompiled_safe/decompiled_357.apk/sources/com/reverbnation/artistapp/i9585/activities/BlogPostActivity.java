package com.reverbnation.artistapp.i9585.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.google.common.base.Strings;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.api.FacebookApi;
import com.reverbnation.artistapp.i9585.api.tasks.FacebookWallPostApiTask;
import com.reverbnation.artistapp.i9585.models.BlogPost;
import com.reverbnation.artistapp.i9585.models.FacebookResponse;
import com.reverbnation.artistapp.i9585.models.interfaces.FacebookShareable;
import com.reverbnation.artistapp.i9585.utils.ActivityHelper;
import com.reverbnation.artistapp.i9585.utils.AnalyticsHelper;
import com.reverbnation.artistapp.i9585.utils.LinkShortener;
import java.util.List;
import org.codehaus.jackson.util.MinimalPrettyPrinter;
import twitter4j.TwitterException;

public class BlogPostActivity extends BaseActivity {
    protected static final int NO_EMAIL_DIALOG = 6;
    protected static final int SHARE_FB_FAILED_DIALOG = 5;
    protected static final int SHARE_FB_PROGRESS_DIALOG = 3;
    protected static final int SHARE_FB_SUCCESS_DIALOG = 4;
    protected static final int SHARE_TWITTER_FAILED_DIALOG = 2;
    protected static final int SHARE_TWITTER_PROGRESS_DIALOG = 0;
    protected static final int SHARE_TWITTER_SUCCESS_DIALOG = 1;
    private static final String TAG = "BlogPostActivity";
    private TextView bodyText;
    private BlogPost post;
    private TextView titleText;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(7);
        setContentView((int) R.layout.blog_post_activity);
        getActivityHelper().setArtistTitlebar(getString(R.string.blog_post));
        setupViews();
        loadBlogPost();
    }

    private void setupViews() {
        this.titleText = (TextView) findViewById(R.id.blog_post_title_text);
        this.bodyText = (TextView) findViewById(R.id.blog_post_body_text);
    }

    private void loadBlogPost() {
        BlogPost post2 = (BlogPost) getIntent().getSerializableExtra("blog_post");
        if (post2 != null) {
            setPost(post2);
        }
    }

    private void setPost(BlogPost post2) {
        this.post = post2;
        if (post2 != null) {
            this.titleText.setText(post2.getTitle());
            this.bodyText.setText(post2.getBody());
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        AnalyticsHelper.getInstance(this).trackPageView("homescreen/news/blog/blog_details");
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        Dialog dialog = null;
        switch (id) {
            case 0:
                dialog = getActivityHelper().createProgressDialog(R.string.share_news, R.string.sharing_blog_on_twitter);
                break;
            case 1:
            case 4:
                dialog = getActivityHelper().createDialog((int) R.string.share_news, (int) R.string.blog_post_was_shared);
                break;
            case 2:
                dialog = getActivityHelper().createDialog((int) R.string.share_news, (int) R.string.sharing_news_to_twitter_failed);
                break;
            case 3:
                dialog = getActivityHelper().createProgressDialog(R.string.share_news, R.string.sharing_blog_on_facebook);
                break;
            case 5:
                dialog = getActivityHelper().createDialog((int) R.string.share_news, (int) R.string.sharing_news_to_facebook_failed);
                break;
            case 6:
                dialog = getActivityHelper().createDialog((int) R.string.share_news, (int) R.string.no_email_client);
                break;
        }
        if (dialog != null) {
            return dialog;
        }
        return super.onCreateDialog(id);
    }

    public void onShareFacebookClick(View v) {
        AnalyticsHelper.getInstance(this).trackEvent("sharing", "facebook", "blog");
        Facebook facebook = FacebookApi.getInstance(this);
        if (!Strings.isNullOrEmpty(facebook.getAccessToken())) {
            shareOnFacebook();
            return;
        }
        AnalyticsHelper.getInstance(this).trackPageView("social_facebook_login");
        facebook.authorize(this, FacebookApi.getRequiredPermissions(), new Facebook.DialogListener() {
            public void onComplete(Bundle values) {
                FacebookApi.setAccessToken(values.getString(Facebook.TOKEN));
                BlogPostActivity.this.shareOnFacebook();
            }

            public void onFacebookError(FacebookError e) {
            }

            public void onError(DialogError e) {
            }

            public void onCancel() {
            }
        });
    }

    /* access modifiers changed from: protected */
    public void shareOnFacebook() {
        AnalyticsHelper.getInstance(this).trackEvent("sharing", "facebook", "blog");
        showDialog(3);
        try {
            new FacebookWallPostApiTask(this) {
                /* access modifiers changed from: protected */
                public void onPostExecute(FacebookResponse response) {
                    BlogPostActivity.this.getActivityHelper().dismissDialog(3);
                    if (response.isSuccessful()) {
                        BlogPostActivity.this.showDialog(4);
                    } else {
                        BlogPostActivity.this.showDialog(5);
                    }
                }
            }.execute(new FacebookShareable[]{this.post});
        } catch (IllegalStateException e) {
            Log.e(TAG, e.toString());
            showDialog(5);
        }
    }

    public void onShareEmailClick(View v) {
        AnalyticsHelper.getInstance(this).trackEvent("sharing", "email", "blog");
        getActivityHelper().getEmailHelper().startShareActivity(this, getMailSubject(), getMailBody());
    }

    private String getMailSubject() {
        return getString(R.string.check_out_blog_posts_from) + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + getString(R.string.artist_name);
    }

    private Spanned getMailBody() {
        return getActivityHelper().getEmailHelper().getShareBody(this, this.post.getBlogLink(getActivityHelper().getArtistId()), this.post.getTitle());
    }

    public void onShareTwitterClick(View v) {
        AnalyticsHelper.getInstance(this).trackEvent("sharing", "twitter", "blog");
        if (!getActivityHelper().isTwitterAuthorized()) {
            getActivityHelper().getTwitterHelper().beginTwitterAuthentication();
        } else {
            shareOnTwitter(this.post);
        }
    }

    private void shareOnTwitter(final BlogPost post2) {
        showDialog(0);
        final ActivityHelper helper = getActivityHelper();
        helper.getApplication().shorten(new String[]{post2.getBlogLink(helper.getArtistId()), helper.getApplication().getMarketplaceLink()}, new LinkShortener.ShortenListener() {
            public void onLinkReady(List<String> shortened) {
                final String tweet = helper.getTwitterHelper().getShareText(post2, shortened.get(0), shortened.get(1));
                new AsyncTask<Void, Void, Boolean>() {
                    /* access modifiers changed from: protected */
                    public Boolean doInBackground(Void... arg0) {
                        try {
                            helper.getTwitter().updateStatus(tweet);
                            return true;
                        } catch (TwitterException e) {
                            Log.v(BlogPostActivity.TAG, "Failed to share video on twitter" + e);
                            return false;
                        }
                    }

                    /* access modifiers changed from: protected */
                    public void onPostExecute(Boolean result) {
                        BlogPostActivity.this.getActivityHelper().dismissDialog(0);
                        BlogPostActivity.this.showDialog(result.booleanValue() ? 1 : 2);
                    }
                }.execute(new Void[0]);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        FacebookApi.getInstance(this).authorizeCallback(requestCode, resultCode, data);
        if (requestCode != 100) {
            return;
        }
        if (resultCode == -1) {
            shareOnTwitter(this.post);
        } else {
            getActivityHelper().getTwitterHelper().showTwitterAuthenticationError(data);
        }
    }
}
