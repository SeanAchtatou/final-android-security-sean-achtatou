package com.reverbnation.artistapp.i9585.api.tasks;

import android.content.Context;
import android.os.AsyncTask;
import com.reverbnation.artistapp.i9585.api.ReverbNationApi;
import com.reverbnation.artistapp.i9585.models.SongList;
import com.roobit.restkit.RestClient;
import com.roobit.restkit.Result;
import java.io.IOException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

public class GetSongsApiTask extends AsyncTask<Object, Void, SongList> {
    private GetSongsApiDelegate delegate;

    public interface GetSongsApiDelegate {
        void getSongsCancelled();

        void getSongsFinished(SongList songList);

        void getSongsStarted();
    }

    public GetSongsApiTask(GetSongsApiDelegate delegate2) {
        this.delegate = delegate2;
    }

    /* access modifiers changed from: protected */
    public SongList doInBackground(Object... args) {
        Result result = RestClient.getClientWithBaseUrl(ReverbNationApi.getBaseUrl((String) args[1])).get(ReverbNationApi.getInstance().getRequiredHeaderParameters((Context) args[2])).setResource("artists/" + ((String) args[0]) + "/songs.json").synchronousExecute();
        if (!result.isSuccess()) {
            return null;
        }
        try {
            return (SongList) RestClient.getObjectMapper().readValue(result.getResponse(), SongList.class);
        } catch (JsonParseException e) {
            e.printStackTrace();
            return null;
        } catch (JsonMappingException e2) {
            e2.printStackTrace();
            return null;
        } catch (IOException e3) {
            e3.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        this.delegate.getSongsCancelled();
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(SongList result) {
        this.delegate.getSongsFinished(result);
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        this.delegate.getSongsStarted();
    }
}
