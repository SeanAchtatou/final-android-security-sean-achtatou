package com.reverbnation.artistapp.i9585.utils;

import android.media.AudioManager;
import android.os.Build;

public class AudioFocusWrapper {
    public static boolean isAvailable() {
        if (Build.VERSION.SDK_INT >= 8) {
            return true;
        }
        throw new RuntimeException("No audio focus available");
    }

    public int requestAudioFocus(AudioManager audioManager, AudioManager.OnAudioFocusChangeListener changeListener, int streamType, int durationHint) {
        return audioManager.requestAudioFocus(changeListener, streamType, durationHint);
    }

    public int abandonAudioFocus(AudioManager audioManager, AudioManager.OnAudioFocusChangeListener changeListener) {
        return audioManager.abandonAudioFocus(changeListener);
    }
}
