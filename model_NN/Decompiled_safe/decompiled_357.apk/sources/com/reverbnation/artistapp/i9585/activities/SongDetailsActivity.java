package com.reverbnation.artistapp.i9585.activities;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.api.FacebookApi;
import com.reverbnation.artistapp.i9585.api.tasks.FacebookWallPostApiTask;
import com.reverbnation.artistapp.i9585.models.FacebookResponse;
import com.reverbnation.artistapp.i9585.models.SongList;
import com.reverbnation.artistapp.i9585.models.interfaces.FacebookShareable;
import com.reverbnation.artistapp.i9585.services.AudioPlayer;
import com.reverbnation.artistapp.i9585.services.ReverbMusicService;
import com.reverbnation.artistapp.i9585.utils.AnalyticsHelper;
import com.reverbnation.artistapp.i9585.utils.DrawableManager;
import com.reverbnation.artistapp.i9585.utils.LinkShortener;
import java.util.List;
import twitter4j.TwitterException;

public class SongDetailsActivity extends BaseActivity {
    private static final int SHARE_FB_FAILED_DIALOG = 2;
    private static final int SHARE_FB_PROGRESS_DIALOG = 0;
    private static final int SHARE_FB_SUCCESS_DIALOG = 1;
    private static final int SHARE_TWITTER_FAILED_DIALOG = 4;
    private static final int SHARE_TWITTER_PROGRESS_DIALOG = 5;
    private static final int SHARE_TWITTER_SUCCESS_DIALOG = 3;
    private static final String TAG = "SongDetails";
    private Button playButton;
    /* access modifiers changed from: private */
    public SongList.Song song;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(7);
        setContentView((int) R.layout.song_details_activity);
        getActivityHelper().setArtistTitlebar((int) R.string.song_details);
        setupViews();
        setSongFromIntent();
    }

    private void setupViews() {
        this.playButton = (Button) findViewById(R.id.play_button);
    }

    private void setSongFromIntent() {
        SongList.Song song2 = (SongList.Song) getIntent().getSerializableExtra("song");
        Preconditions.checkState(song2 != null);
        setSong(song2);
    }

    private void setSong(SongList.Song song2) {
        this.song = song2;
        setAlbumArt(song2);
        ((TextView) findViewById(R.id.song_name)).setText(song2.getName());
        ((Button) findViewById(R.id.buy_button)).setEnabled(song2.isPurchasable());
        ((TextView) findViewById(R.id.song_lyrics)).setText(song2.getLyrics());
    }

    private void setAlbumArt(SongList.Song song2) {
        ImageView albumImage = (ImageView) findViewById(R.id.album_art);
        SongList.SongPhoto photo = song2.getPhoto();
        DrawableManager.getInstance(this).bindDrawable(photo != null ? photo.getImageThumbURL() : getActivityHelper().getSmallArtistImage(), albumImage);
    }

    private void updatePlayButton() {
        Preconditions.checkState(this.playButton != null);
        this.playButton.setText(isPlaying() ? R.string.pause : R.string.play);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        AnalyticsHelper.getInstance(this).trackPageView("homescreen/songs/songs/song_details");
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        Dialog dialog = null;
        switch (id) {
            case 0:
                dialog = getActivityHelper().createProgressDialog(R.string.share_song, R.string.sharing_song_on_facebook);
                break;
            case 1:
            case 3:
                dialog = getActivityHelper().createDialog((int) R.string.share_song, (int) R.string.song_was_shared);
                break;
            case 2:
                dialog = getActivityHelper().createDialog((int) R.string.share_song, (int) R.string.sharing_song_to_facebook_failed);
                break;
            case 4:
                dialog = getActivityHelper().createDialog((int) R.string.share_song, (int) R.string.sharing_song_to_twitter_failed);
                break;
            case 5:
                dialog = getActivityHelper().createProgressDialog(R.string.share_song, R.string.sharing_song_on_twitter);
                break;
        }
        if (dialog != null) {
            return dialog;
        }
        return super.onCreateDialog(id);
    }

    public void onPlayButtonClicked(View v) {
        if (this.song != null) {
            if (isPlaying()) {
                this.musicService.pause();
            } else if (isPaused()) {
                this.musicService.play();
            } else {
                this.musicService.startPlaying(this.song);
            }
        }
    }

    private boolean isPlaying() {
        return this.musicService.isPlaying() && this.musicService.getCurrentSong().equals(this.song);
    }

    private boolean isPaused() {
        return this.musicService.isPaused() && this.musicService.getCurrentSong().equals(this.song);
    }

    public void onBuyButtonClicked() {
        if (this.song.isPurchasable()) {
            String buyUrl = this.song.getBuyURL().trim();
            Log.d(TAG, "Buying from " + buyUrl);
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse(buyUrl)));
        }
    }

    public void onShareTwitterClick(View v) {
        AnalyticsHelper.getInstance(this).trackEvent("sharing", "twitter", "song");
        if (!getActivityHelper().getApplication().isTwitterAuthorized()) {
            getActivityHelper().getTwitterHelper().beginTwitterAuthentication();
        } else {
            shareOnTwitter();
        }
    }

    private void shareOnTwitter() {
        showDialog(5);
        getActivityHelper().getApplication().shorten(new String[]{this.song.getShareLink(), getActivityHelper().getApplication().getMarketplaceLink()}, new LinkShortener.ShortenListener() {
            public void onLinkReady(List<String> shortened) {
                final String tweet = SongDetailsActivity.this.getActivityHelper().getTwitterHelper().getShareText(SongDetailsActivity.this.song, shortened.get(0), shortened.get(1));
                new AsyncTask<Void, Void, Boolean>() {
                    /* access modifiers changed from: protected */
                    public Boolean doInBackground(Void... arg0) {
                        try {
                            SongDetailsActivity.this.getActivityHelper().getApplication().getTwitter().updateStatus(tweet);
                            return true;
                        } catch (TwitterException e) {
                            Log.v(SongDetailsActivity.TAG, "Failed to share video on twitter" + e);
                            return false;
                        }
                    }

                    /* access modifiers changed from: protected */
                    public void onPostExecute(Boolean result) {
                        SongDetailsActivity.this.getActivityHelper().dismissDialog(5);
                        SongDetailsActivity.this.showDialog(result.booleanValue() ? 3 : 4);
                    }
                }.execute(new Void[0]);
            }
        });
    }

    public void onShareEmailClick(View v) {
        AnalyticsHelper.getInstance(this).trackEvent("sharing", "email", "song");
        getActivityHelper().getEmailHelper().startShareActivity(this, getMailSubject(), getMailBody());
    }

    private String getMailSubject() {
        return String.format(getString(R.string.song_share_email_subject, new Object[]{this.song.getName(), getString(R.string.artist_name)}), new Object[0]);
    }

    private Spanned getMailBody() {
        return getActivityHelper().getEmailHelper().getShareBody(this, this.song.getShareLink(), getMailSongTitle());
    }

    private String getMailSongTitle() {
        return String.format("%1$s - %2$s", getString(R.string.artist_name), this.song.getName());
    }

    public void onShareFacebookClick(View v) {
        AnalyticsHelper.getInstance(this).trackEvent("sharing", "facebook", "song");
        Facebook facebook = FacebookApi.getInstance(this);
        if (!Strings.isNullOrEmpty(facebook.getAccessToken())) {
            shareOnFacebook();
            return;
        }
        AnalyticsHelper.getInstance(this).trackPageView("social_facebook_login");
        facebook.authorize(this, FacebookApi.getRequiredPermissions(), new Facebook.DialogListener() {
            public void onComplete(Bundle values) {
                FacebookApi.setAccessToken(values.getString(Facebook.TOKEN));
                SongDetailsActivity.this.shareOnFacebook();
            }

            public void onFacebookError(FacebookError e) {
                e.printStackTrace();
            }

            public void onError(DialogError e) {
                e.printStackTrace();
            }

            public void onCancel() {
            }
        });
    }

    /* access modifiers changed from: private */
    public void shareOnFacebook() {
        showDialog(0);
        try {
            new FacebookWallPostApiTask(this) {
                /* access modifiers changed from: protected */
                public void onPostExecute(FacebookResponse response) {
                    SongDetailsActivity.this.getActivityHelper().dismissDialog(0);
                    if (response.isSuccessful()) {
                        SongDetailsActivity.this.showDialog(1);
                    } else {
                        SongDetailsActivity.this.showDialog(2);
                    }
                }
            }.execute(new FacebookShareable[]{this.song});
        } catch (IllegalStateException e) {
            e.printStackTrace();
            showDialog(2);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        FacebookApi.getInstance(this).authorizeCallback(requestCode, resultCode, data);
        if (requestCode != 100) {
            return;
        }
        if (resultCode == -1) {
            shareOnTwitter();
        } else {
            getActivityHelper().getTwitterHelper().showTwitterAuthenticationError(data);
        }
    }

    public void handlePlayerEvent(ReverbMusicService musicService, Intent intent) {
        super.handlePlayerEvent(musicService, intent);
        Log.v(TAG, "Received REVERB_MEDIA_ACTION: " + AudioPlayer.MusicActionEvent.values()[intent.getIntExtra("event", AudioPlayer.MusicActionEvent.UNDEFINED_EVENT.ordinal())].toString());
        updatePlayButton();
    }

    /* access modifiers changed from: protected */
    public void onMusicServiceConnected() {
        super.onMusicServiceConnected();
        updatePlayButton();
    }
}
