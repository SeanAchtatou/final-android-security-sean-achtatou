package com.reverbnation.artistapp.i9585.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.reverbnation.artistapp.i9585.ReverbNationApplication;
import com.reverbnation.artistapp.i9585.activities.SplashActivity;
import com.urbanairship.UAirship;
import com.urbanairship.push.PushManager;

public class IntentReceiver extends BroadcastReceiver {
    private static final String Tag = UAirship.getAppName();

    public void onReceive(Context context, Intent intent) {
        Log.i(Tag, "Received intent: " + intent.toString());
        String action = intent.getAction();
        if (action.equals(PushManager.ACTION_PUSH_RECEIVED)) {
            Log.i(Tag, "Received push notification. Alert: " + intent.getStringExtra(PushManager.EXTRA_ALERT) + ". Payload: " + intent.getStringExtra(PushManager.EXTRA_STRING_EXTRA) + ". NotificationID=" + intent.getIntExtra(PushManager.EXTRA_NOTIFICATION_ID, 0));
        } else if (action.equals(PushManager.ACTION_NOTIFICATION_OPENED)) {
            Log.i(Tag, "User clicked notification. Message: " + intent.getStringExtra(PushManager.EXTRA_ALERT) + ". Payload: " + intent.getStringExtra(PushManager.EXTRA_STRING_EXTRA));
            Intent launch = new Intent("android.intent.action.MAIN");
            launch.setClass(UAirship.shared().getApplicationContext(), SplashActivity.class);
            launch.setFlags(268435456);
            UAirship.shared().getApplicationContext().startActivity(launch);
        } else if (action.equals(PushManager.ACTION_REGISTRATION_FINISHED)) {
            String apid = intent.getStringExtra(PushManager.EXTRA_APID);
            Log.d(Tag, "Registration complete. APID:" + apid + ". Valid: " + intent.getBooleanExtra(PushManager.EXTRA_REGISTRATION_VALID, false));
            ReverbNationApplication.getInstance().onReceivedPushKey(apid);
        }
    }
}
