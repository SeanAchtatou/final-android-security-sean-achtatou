package com.reverbnation.artistapp.i9585.api;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.ReverbNationApplication;
import com.reverbnation.artistapp.i9585.models.MobileApplication;
import com.reverbnation.artistapp.i9585.utils.Installation;
import java.util.Properties;

public class ReverbNationApi {
    private static final String DEVELOPMENT_API_URL = "http://www.tunehive.com:83/c/api/v3";
    private static final String PRODUCTION_API_URL = "https://www.reverbnation.com/c/api/v3";
    private static final String ReverbNationHttpsUrl = "https://www.reverbnation.com";
    public static final String ReverbNationTwitterUrl = "http://www.twitter.com/reverbnation";
    public static final String ReverbNationUrl = "http://www.reverbnation.com";
    private static final String TunehiveUrl = "http://www.tunehive.com:83";
    private static ReverbNationApi instance;
    private static int sessionId = -1;
    private String apiUrl;
    private Context context;
    private MobileApplication mobileApplication;
    private Properties requiredHeaderParameters;

    public static ReverbNationApi getInstance() {
        if (instance == null) {
            instance = new ReverbNationApi(ReverbNationApplication.getInstance().getApplicationContext());
        }
        return instance;
    }

    protected ReverbNationApi(Context context2) {
        this.context = context2;
    }

    public MobileApplication getMobileApplication() {
        return this.mobileApplication;
    }

    public void setMobileApplication(MobileApplication mobileApplication2) {
        this.mobileApplication = mobileApplication2;
    }

    public Properties getRequiredHeaderParameters() {
        if (this.requiredHeaderParameters == null) {
            this.requiredHeaderParameters = new Properties();
        }
        return this.requiredHeaderParameters;
    }

    public Properties getRequiredHeaderParameters(Context context2) {
        if (this.requiredHeaderParameters == null || this.requiredHeaderParameters.isEmpty()) {
            this.requiredHeaderParameters = createRequiredHeaderParameters(context2);
        }
        return this.requiredHeaderParameters;
    }

    private Properties createRequiredHeaderParameters(Context context2) {
        Properties props = new Properties();
        props.put("X-ReverbNation-Mobile-Device-Software", Build.VERSION.RELEASE);
        props.put("X-ReverbNation-Mobile-Device-Key", Installation.id(context2));
        props.put("X-ReverbNation-Mobile-Application-Platform-ID", context2.getString(R.string.mobile_application_platform_id));
        props.put("X-ReverbNation-Mobile-Device-Hardware", Build.MODEL);
        try {
            props.put("X-ReverbNation-Mobile-Application-Version", context2.getPackageManager().getPackageInfo(context2.getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            props.put("X-ReverbNation-Mobile-Application-Version", "Unknown");
        }
        return props;
    }

    public String getApiUrl() {
        if (this.apiUrl == null) {
            this.apiUrl = (this.context.getApplicationInfo().flags & 2) != 0 ? DEVELOPMENT_API_URL : PRODUCTION_API_URL;
        }
        return this.apiUrl;
    }

    public static String getBaseUrl() {
        return getInstance().getApiUrl();
    }

    public static String getBaseUrl(String alternateUrl) {
        return alternateUrl != null ? alternateUrl : getBaseUrl();
    }

    public static String getPublicKey() {
        return "reverbmobileapp";
    }

    public static String getPrivateKey() {
        return "musicpimp321";
    }

    public static void setSessionId(int id) {
        sessionId = id;
    }

    public static int getSessionId() {
        return sessionId;
    }
}
