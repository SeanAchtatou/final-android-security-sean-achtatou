package com.reverbnation.artistapp.i9585.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.utils.TitlebarHelper;

public class BandInfoActivity extends BaseTabActivity {
    public static final String BiographyTab = "biography_tab";
    public static final String LinksTab = "links_tab";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(7);
        setContentView((int) R.layout.band_info_activity);
        getActivityHelper().setArtistTitlebar(TitlebarHelper.capitalize(getString(R.string.band_information)));
        setupTabs();
    }

    private void setupTabs() {
        TabHost tabHost = getTabHost();
        tabHost.addTab(tabHost.newTabSpec(BiographyTab).setIndicator(createTabView(R.string.biography)).setContent(new Intent(this, BiographyActivity.class)));
        tabHost.addTab(tabHost.newTabSpec(LinksTab).setIndicator(createTabView(R.string.links)).setContent(new Intent(this, LinksActivity.class)));
    }
}
