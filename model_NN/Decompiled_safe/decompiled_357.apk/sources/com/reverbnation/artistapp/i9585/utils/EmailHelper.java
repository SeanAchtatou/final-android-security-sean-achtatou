package com.reverbnation.artistapp.i9585.utils;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.Html;
import android.text.Spanned;
import android.widget.Toast;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.ReverbNationApplication;
import com.reverbnation.artistapp.i9585.api.ReverbNationApi;
import java.io.File;
import java.io.FileNotFoundException;

public class EmailHelper {
    public Spanned getShareBody(Activity activity, String shareLink, String shareTitle) {
        return Html.fromHtml(String.format(activity.getString(R.string.share_email_template), shareLink, shareTitle, ((ReverbNationApplication) activity.getApplication()).getMarketingUrl(), ReverbNationApi.ReverbNationUrl, ReverbNationApi.ReverbNationTwitterUrl));
    }

    public void startShareActivity(Activity activity, String subject, Spanned body) {
        startActivity(activity, getEmailIntent(subject, body));
    }

    public void startShareActivity(Activity activity, String subject, Spanned body, File imageFile) {
        try {
            startActivity(activity, getEmailIntent(activity, subject, body, imageFile));
        } catch (FileNotFoundException e) {
            Toast.makeText(activity, (int) R.string.photo_not_found, 1).show();
        }
    }

    public void startActivity(final Activity activity, Intent intent) {
        try {
            activity.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            activity.runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(activity, (int) R.string.no_email_client, 1).show();
                }
            });
        }
    }

    private Intent getEmailIntent(String subject, Spanned body) {
        Intent intent = new Intent("android.intent.action.SEND");
        intent.setType("text/html");
        intent.putExtra("android.intent.extra.SUBJECT", subject);
        intent.putExtra("android.intent.extra.TEXT", body);
        return intent;
    }

    private Intent getEmailIntent(Activity activity, String subject, Spanned body, File imageFile) throws FileNotFoundException {
        Intent intent = getEmailIntent(subject, body);
        intent.putExtra("android.intent.extra.STREAM", Uri.parse(MediaStore.Images.Media.insertImage(activity.getContentResolver(), imageFile.getAbsolutePath(), (String) null, (String) null)));
        return intent;
    }
}
