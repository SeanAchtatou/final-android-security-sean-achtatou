package com.reverbnation.artistapp.i9585.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.InvalidParameterException;

public class RemoteBitmapLoader extends AsyncTask<String, Integer, Bitmap> {
    private ImageView view;

    /* access modifiers changed from: protected */
    public Bitmap doInBackground(String... urls) {
        if (urls.length == 0) {
            throw new InvalidParameterException("A url must be specified");
        } else if (urls.length > 1) {
            throw new InvalidParameterException("Only one url can be specified");
        } else {
            try {
                return BitmapFactory.decodeStream((InputStream) new URL(urls[0]).getContent());
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return null;
            } catch (IOException e2) {
                e2.printStackTrace();
                return null;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Bitmap result) {
        super.onPostExecute((Object) result);
        if (this.view != null) {
            this.view.setImageBitmap(result);
        }
    }

    public void setView(ImageView view2) {
        this.view = view2;
    }

    public ImageView getView() {
        return this.view;
    }
}
