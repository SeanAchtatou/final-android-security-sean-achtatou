package com.reverbnation.artistapp.i9585.services;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import com.reverbnation.artistapp.i9585.models.SongList;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import org.codehaus.jackson.org.objectweb.asm.Opcodes;

public class ReverbMusicService extends Service {
    public static final String MEDIA_BUTTON = "com.reverbnation.artistapp.i9585.intent.action.MEDIA_BUTTON";
    protected static final int NEXT_MSG = 1;
    protected static final int PLAY_PAUSE_MSG = 0;
    protected static final int PREVIOUS_MSG = 2;
    private static final String TAG = "ReverbMusicService";
    /* access modifiers changed from: private */
    public static AudioPlayer audioPlayer;
    /* access modifiers changed from: private */
    public static Handler handler;
    private static Method registerMediaButtonEventReceiver = null;
    private static Method unregisterMediaButtonEventReceiver = null;
    private AudioManager audioManager;
    private final IBinder binder = new LocalBinder();
    private ComponentName mediaButtonResponder;

    static {
        initializeRemoteControlRegistrationMethods();
    }

    public class LocalBinder extends Binder {
        public LocalBinder() {
        }

        public ReverbMusicService getService() {
            return ReverbMusicService.this;
        }
    }

    public void onCreate() {
        Log.v(TAG, "Creating service");
        super.onCreate();
        audioPlayer = new AudioPlayer(this);
        registerRemoteControl();
        handler = new Handler(new Handler.Callback() {
            public boolean handleMessage(Message msg) {
                switch (msg.what) {
                    case 0:
                        ReverbMusicService.audioPlayer.playOrPause();
                        break;
                    case 1:
                        ReverbMusicService.handler.removeMessages(1);
                        ReverbMusicService.audioPlayer.playRequest();
                        break;
                    case 2:
                        ReverbMusicService.handler.removeMessages(2);
                        ReverbMusicService.audioPlayer.playRequest();
                        break;
                    default:
                        return false;
                }
                return true;
            }
        });
    }

    public void onDestroy() {
        Log.v(TAG, "Destroying service");
        unregisterRemoteControl();
        if (audioPlayer != null) {
            audioPlayer.release();
        }
        super.onDestroy();
    }

    public IBinder onBind(Intent intent) {
        Log.v(TAG, "Binding service");
        return this.binder;
    }

    /* JADX WARN: Type inference failed for: r2v5, types: [java.io.Serializable] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int onStartCommand(android.content.Intent r6, int r7, int r8) {
        /*
            r5 = this;
            java.lang.String r2 = "ReverbMusicService"
            java.lang.String r3 = "Starting service"
            android.util.Log.v(r2, r3)
            r1 = 0
            java.lang.String r2 = "playlist"
            java.io.Serializable r2 = r6.getSerializableExtra(r2)     // Catch:{ Throwable -> 0x004b }
            r0 = r2
            com.reverbnation.artistapp.i9585.models.SongList r0 = (com.reverbnation.artistapp.i9585.models.SongList) r0     // Catch:{ Throwable -> 0x004b }
            r1 = r0
            if (r1 == 0) goto L_0x0024
            java.lang.String r2 = "ReverbMusicService"
            java.lang.String r3 = "Setting playlist"
            android.util.Log.v(r2, r3)     // Catch:{ Throwable -> 0x004b }
            com.reverbnation.artistapp.i9585.services.AudioPlayer r2 = com.reverbnation.artistapp.i9585.services.ReverbMusicService.audioPlayer     // Catch:{ Throwable -> 0x004b }
            java.util.List r3 = r1.getSongs()     // Catch:{ Throwable -> 0x004b }
            r2.setPlaylist(r3)     // Catch:{ Throwable -> 0x004b }
        L_0x0024:
            java.lang.String r2 = "ReverbMusicService"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Service started "
            java.lang.StringBuilder r3 = r3.append(r4)
            if (r1 == 0) goto L_0x0048
            java.lang.String r4 = "with"
        L_0x0035:
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = " playlist"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            android.util.Log.v(r2, r3)
            r2 = 1
            return r2
        L_0x0048:
            java.lang.String r4 = "without"
            goto L_0x0035
        L_0x004b:
            r2 = move-exception
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: com.reverbnation.artistapp.i9585.services.ReverbMusicService.onStartCommand(android.content.Intent, int, int):int");
    }

    public static class MediaButtonReceiver extends BroadcastReceiver {
        public void onReceive(Context context, Intent intent) {
            Log.v(ReverbMusicService.TAG, "onReceive");
            if (ReverbMusicService.isActionMediaButtonIntent(intent)) {
                switch (((KeyEvent) intent.getParcelableExtra("android.intent.extra.KEY_EVENT")).getKeyCode()) {
                    case Opcodes.CASTORE /*85*/:
                        Log.v(ReverbMusicService.TAG, "Play/Pause key event");
                        ReverbMusicService.playOrPause();
                        return;
                    case Opcodes.SASTORE /*86*/:
                    default:
                        Log.v(ReverbMusicService.TAG, "Unknown key event");
                        return;
                    case Opcodes.POP /*87*/:
                        Log.v(ReverbMusicService.TAG, "Next key event");
                        ReverbMusicService.nextSong();
                        return;
                    case Opcodes.POP2 /*88*/:
                        Log.v(ReverbMusicService.TAG, "Previous key event");
                        ReverbMusicService.previousSong();
                        return;
                }
            }
        }
    }

    public static boolean isActionMediaButtonIntent(Intent intent) {
        return MEDIA_BUTTON.equals(intent.getAction()) || "android.intent.action.MEDIA_BUTTON".equals(intent.getAction());
    }

    public static void playOrPause() {
        audioPlayer.hintPlayOrPause();
        handler.sendEmptyMessage(0);
    }

    public static void nextSong() {
        audioPlayer.hintNextSong();
        handler.sendEmptyMessage(1);
    }

    public static void previousSong() {
        audioPlayer.hintPrevSong();
        handler.sendEmptyMessage(2);
    }

    private static void initializeRemoteControlRegistrationMethods() {
        Log.v(TAG, "Initializing remote control methods");
        try {
            if (registerMediaButtonEventReceiver == null) {
                registerMediaButtonEventReceiver = AudioManager.class.getMethod("registerMediaButtonEventReceiver", ComponentName.class);
            }
            if (unregisterMediaButtonEventReceiver == null) {
                unregisterMediaButtonEventReceiver = AudioManager.class.getMethod("unregisterMediaButtonEventReceiver", ComponentName.class);
            }
        } catch (NoSuchMethodException e) {
            Log.i(TAG, "Media button receiver methods don't exist");
        }
    }

    private void registerRemoteControl() {
        if (registerMediaButtonEventReceiver != null) {
            try {
                registerMediaButtonEventReceiver.invoke(getAudioManager(), getMediaButtonResponder());
                Log.v(TAG, "Registered button receiver with audio manager");
            } catch (InvocationTargetException e) {
                InvocationTargetException ite = e;
                Throwable cause = ite.getCause();
                if (cause instanceof RuntimeException) {
                    throw ((RuntimeException) cause);
                } else if (cause instanceof Error) {
                    throw ((Error) cause);
                } else {
                    throw new RuntimeException(ite);
                }
            } catch (IllegalAccessException e2) {
                Log.e(TAG, "unexpected " + e2);
            }
        }
    }

    private void unregisterRemoteControl() {
        if (unregisterMediaButtonEventReceiver != null) {
            try {
                unregisterMediaButtonEventReceiver.invoke(getAudioManager(), getMediaButtonResponder());
                Log.v(TAG, "Unregistered button receiver with audio manager");
            } catch (InvocationTargetException e) {
                InvocationTargetException ite = e;
                Throwable cause = ite.getCause();
                if (cause instanceof RuntimeException) {
                    throw ((RuntimeException) cause);
                } else if (cause instanceof Error) {
                    throw ((Error) cause);
                } else {
                    throw new RuntimeException(ite);
                }
            } catch (IllegalAccessException e2) {
                Log.e(TAG, "unexpected " + e2);
            }
        }
    }

    private AudioManager getAudioManager() {
        if (this.audioManager == null) {
            this.audioManager = (AudioManager) getSystemService("audio");
        }
        return this.audioManager;
    }

    private ComponentName getMediaButtonResponder() {
        if (this.mediaButtonResponder == null) {
            this.mediaButtonResponder = new ComponentName(getPackageName(), MediaButtonReceiver.class.getName());
        }
        return this.mediaButtonResponder;
    }

    public boolean isReady() {
        return audioPlayer != null && audioPlayer.isReady();
    }

    public boolean isPlaying() {
        return audioPlayer != null && audioPlayer.isPlaying();
    }

    public boolean isPaused() {
        return !isPlaying();
    }

    public SongList.Song getCurrentSong() {
        if (audioPlayer != null) {
            return audioPlayer.getCurrentSong();
        }
        throw new IllegalStateException();
    }

    public String getCurrentSongTitle() {
        return getCurrentSong().getName();
    }

    public boolean hasSongs() {
        return audioPlayer != null && audioPlayer.hasSongs();
    }

    public List<SongList.Song> getPlaylist() {
        return audioPlayer.getPlaylist();
    }

    public long getCurrentSongProgress() {
        return audioPlayer.getProgressSeconds();
    }

    public long getCurrentSongDuration() {
        return audioPlayer.getDurationSeconds();
    }

    public void setAutoplayEnabled(boolean isEnabled) {
        audioPlayer.setAutoplayEnabled(isEnabled);
    }

    public void startPlaying(SongList.Song song) {
        audioPlayer.startPlaying(song);
    }

    public void pause() {
        if (isPlaying()) {
            audioPlayer.pause();
        }
    }

    public void play() {
        audioPlayer.play();
    }
}
