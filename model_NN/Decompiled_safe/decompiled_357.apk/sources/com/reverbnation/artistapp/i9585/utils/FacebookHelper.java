package com.reverbnation.artistapp.i9585.utils;

import android.app.Activity;
import android.content.SharedPreferences;
import android.util.Log;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.ReverbNationApplication;
import com.reverbnation.artistapp.i9585.api.FacebookApi;
import com.reverbnation.artistapp.i9585.api.ReverbNationApi;
import com.reverbnation.artistapp.i9585.models.BlogPost;
import com.reverbnation.artistapp.i9585.models.Links;
import com.reverbnation.artistapp.i9585.models.Photoset;
import com.reverbnation.artistapp.i9585.models.ShowList;
import com.reverbnation.artistapp.i9585.models.SongList;
import com.reverbnation.artistapp.i9585.models.Videoset;
import java.text.SimpleDateFormat;

public class FacebookHelper {
    private static final String TAG = "FacebookHelper";
    private static final String USER_NAME_PREF = "FacebookUserName";
    private Activity activity;
    private ReverbNationApplication app = ReverbNationApplication.getInstance();

    public FacebookHelper(Activity activity2) {
        this.activity = activity2;
    }

    public String getUserName() {
        return this.app.getApplicationPreferences().getString(USER_NAME_PREF, "");
    }

    public void cacheUserName(String userName) {
        SharedPreferences.Editor edit = this.app.getApplicationPreferences().edit();
        edit.putString(USER_NAME_PREF, userName);
        edit.commit();
    }

    public String getFacebookLink(Videoset.Video video) {
        return video.getVideoUrl();
    }

    public String getFacebookLink(ShowList.Show show) {
        return getFacebookLink("show", show.getId());
    }

    public String getFacebookLink(SongList.Song song) {
        return String.format("%1$s/artist/song_details/%2$s", ReverbNationApi.ReverbNationUrl, Integer.valueOf(song.getId()));
    }

    public String getFacebookLink(BlogPost post) {
        return getPageObjectFacebookLink("blogs");
    }

    public String getFacebookLink(Photoset.Photo photo) {
        return getPageObjectFacebookLink("photos");
    }

    private String getPageObjectFacebookLink(String pageObjectName) {
        return String.format("%1$s/%2$s/#!/page_object/page_object_%3$s/artist_%4$s", ReverbNationApi.ReverbNationUrl, this.app.getArtistId(), pageObjectName, this.app.getArtistId());
    }

    private String getFacebookLink(String type, int id) {
        return String.format("%1$s/%2$s/#!/%3$s/%4$s", ReverbNationApi.ReverbNationUrl, this.app.getArtistId(), type, Integer.valueOf(id));
    }

    public String getFacebookSource(SongList.Song song) {
        return getFacebookWidgetURL("audio", song.getId());
    }

    public String getFacebookSource(ShowList.Show show) {
        return getFacebookWidgetURL("show", show.getId());
    }

    public String getFacebookSource(Videoset.Video video) {
        return getFacebookWidgetURL("video", video.getId());
    }

    public String getFacebookSource(Photoset.Photo photo) {
        return getFacebookWidgetURL("photo", photo.getId());
    }

    public String getFacebookSource() {
        return getFacebookWidgetURL();
    }

    public String getFacebookWidgetURL() {
        return String.format("%1$s/widgets/swf/18/facebook_widget.swf?id=artist_%2$s", FacebookApi.getWidgetUrl(), this.app.getArtistId());
    }

    public String getFacebookWidgetURL(String type, int id) {
        String url = getFacebookWidgetURL() + String.format("&deep_link=%1$s.%2$d", type, Integer.valueOf(id));
        Log.v(TAG, "Sourcing: " + url);
        return url;
    }

    public String getFacebookPicture() {
        return ReverbNationApi.getInstance().getMobileApplication().getMobileApplicationIconImage();
    }

    public String getFacebookName(SongList.Song song) {
        return getFacebookName(song.getName());
    }

    public String getFacebookName(Videoset.Video video) {
        return getFacebookName(video.getName());
    }

    public String getFacebookName(BlogPost post) {
        return getFacebookName(post.getTitle());
    }

    public String getFacebookName(Links.Link link) {
        return getFacebookName(link.getWebsite());
    }

    public String getFacebookName(ShowList.Show show) {
        return String.format("%1$s - %2$s (%3$s)", this.activity.getString(R.string.artist_name), show.getVenue().getName(), new SimpleDateFormat("MMM d").format(show.getShowTimeDate()));
    }

    public String getFacebookName(Photoset.Photo photo) {
        return getFacebookName(this.activity.getString(R.string.mobile_app_photos));
    }

    private String getFacebookName(String name) {
        return String.format("%1$s - %2$s", getArtistName(), name);
    }

    public String getFacebookCaption(SongList.Song song) {
        return String.format("%1$s '%2$s' %3$s %4$s. %5$s", this.activity.getString(R.string.listen_to), song.getName(), this.activity.getString(R.string.from), getArtistName(), getTheFreeMobileApps());
    }

    public String getFacebookCaption(Videoset.Video video) {
        return getCheckOutCaption("'" + video.getName() + "'");
    }

    public String getFacebookCaption(Photoset.Photo photo) {
        return getCheckOutCaption(this.activity.getString(R.string.photos));
    }

    public String getFacebookCaption(BlogPost post) {
        return getCheckOutCaption(this.activity.getString(R.string.blog_posts));
    }

    public String getFacebookCaption(Links.Link link) {
        return String.format("%1$s %2$s %3$s %4$s. %5$s", this.activity.getString(R.string.check_out), getArtistName(), this.activity.getString(R.string.on), link.getWebsite(), getTheFreeMobileApps());
    }

    private String getCheckOutCaption(String what) {
        return String.format("%1$s %2$s from %3$s. %4$s", this.activity.getString(R.string.check_out), what, getArtistName(), getTheFreeMobileApps());
    }

    public String getFacebookCaption(ShowList.Show show) {
        return String.format("Check out %1$s live on %2$s at %3$s. %4$s", this.activity.getString(R.string.artist_name), new SimpleDateFormat("MMM d").format(show.getShowTimeDate()), show.getVenue().getName(), getTheFreeMobileApps());
    }

    private String getTheFreeMobileApps() {
        return this.activity.getString(R.string.get_the_free_mobile_apps);
    }

    private String getArtistName() {
        return this.activity.getString(R.string.artist_name);
    }
}
