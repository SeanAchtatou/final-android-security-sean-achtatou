package com.reverbnation.artistapp.i9585.api.tasks;

import android.os.AsyncTask;
import android.util.Log;
import com.reverbnation.artistapp.i9585.api.ReverbNationApi;
import com.roobit.restkit.RestClient;
import java.util.Properties;

public class PostSongProgressApiTask extends AsyncTask<Object, Void, Void> {
    private static final String TAG = "ReverbNation";

    public interface PostSongProgressApiDelegate {
        void postProgressCancelled();

        void postProgressFinished();

        void postProgressStarted();
    }

    /* access modifiers changed from: protected */
    public Void doInBackground(Object... args) {
        int songId = ((Integer) args[0]).intValue();
        int progress = ((Integer) args[1]).intValue();
        Log.d("ReverbNation", "Posting progress " + progress + " for song " + songId);
        RestClient.getClientWithBaseUrl(ReverbNationApi.getBaseUrl()).postQuery(getQueryParameters(songId, progress), ReverbNationApi.getInstance().getRequiredHeaderParameters()).setResource("songs/progress.json").synchronousExecute();
        return null;
    }

    private Properties getQueryParameters(int songId, int progress) {
        Properties props = new Properties();
        props.put("mobile_session_id", String.valueOf(ReverbNationApi.getSessionId()));
        props.put("song_id", String.valueOf(songId));
        props.put("progress", String.valueOf(progress));
        return props;
    }
}
