package com.reverbnation.artistapp.i9585.models;

import android.webkit.URLUtil;
import com.google.common.base.Strings;
import com.reverbnation.artistapp.i9585.models.interfaces.FacebookShareable;
import com.reverbnation.artistapp.i9585.utils.FacebookHelper;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.codehaus.jackson.annotate.JsonProperty;

public class SongList implements Serializable {
    private static final long serialVersionUID = -6886324482095525972L;
    @JsonProperty("limit")
    private long limit;
    @JsonProperty("offset")
    private long offset;
    @JsonProperty("results")
    private ArrayList<Song> songs;
    @JsonProperty("sort")
    private String sort;
    @JsonProperty("total")
    private long total;

    public static class Song implements Serializable, FacebookShareable {
        private static final long serialVersionUID = 4593508262743242153L;
        @JsonProperty("buy_url")
        private String buyURL;
        @JsonProperty("id")
        private int id;
        @JsonProperty("is_exclusive")
        private boolean isExclusive;
        @JsonProperty("length")
        private long length;
        @JsonProperty("lyrics")
        private String lyrics;
        @JsonProperty("name")
        private String name;
        @JsonProperty("photo")
        private SongPhoto photo;
        @JsonProperty("size")
        private long size;
        @JsonProperty("updated_at")
        private String updatedAt;
        @JsonProperty("url")
        private String url;

        public void setId(int id2) {
            this.id = id2;
        }

        public int getId() {
            return this.id;
        }

        public void setBuyURL(String buyURL2) {
            this.buyURL = buyURL2;
        }

        public String getBuyURL() {
            return this.buyURL;
        }

        public void setExclusive(boolean isExclusive2) {
            this.isExclusive = isExclusive2;
        }

        public boolean isExclusive() {
            return this.isExclusive;
        }

        public void setLength(long length2) {
            this.length = length2;
        }

        public long getLength() {
            return this.length;
        }

        public void setPhoto(SongPhoto photo2) {
            this.photo = photo2;
        }

        public SongPhoto getPhoto() {
            return this.photo;
        }

        public void setUpdatedAt(String updatedAt2) {
            this.updatedAt = updatedAt2;
        }

        public String getUpdatedAt() {
            return this.updatedAt;
        }

        public void setLyrics(String lyrics2) {
            this.lyrics = lyrics2;
        }

        public String getLyrics() {
            return this.lyrics;
        }

        public void setSize(long size2) {
            this.size = size2;
        }

        public long getSize() {
            return this.size;
        }

        public void setUrl(String url2) {
            this.url = url2;
        }

        public String getUrl() {
            return this.url;
        }

        public void setName(String name2) {
            this.name = name2;
        }

        public String getName() {
            return this.name;
        }

        public Song() {
        }

        public Song(String name2, String url2) {
            this.name = name2;
            this.url = url2;
        }

        public boolean equals(Object o) {
            if (o.getClass() != Song.class) {
                return super.equals(o);
            }
            Song song = (Song) o;
            return this.name.equals(song.getName()) && this.url.equals(song.getUrl());
        }

        public boolean isPurchasable() {
            return !Strings.isNullOrEmpty(getBuyURL()) && URLUtil.isValidUrl(getBuyURL().trim());
        }

        public String getShareLink() {
            return "http://www.reverbnation.com/artist/song_details/" + this.id;
        }

        public String toString() {
            return this.name;
        }

        public String getFacebookCaption(FacebookHelper helper) {
            return helper.getFacebookCaption(this);
        }

        public String getFacebookLink(FacebookHelper helper) {
            return helper.getFacebookLink(this);
        }

        public String getFacebookSource(FacebookHelper helper) {
            return helper.getFacebookSource(this);
        }

        public String getFacebookName(FacebookHelper helper) {
            return helper.getFacebookName(this);
        }

        public String getFacebookPicture(FacebookHelper helper) {
            return helper.getFacebookPicture();
        }
    }

    public static class SongPhoto implements Serializable {
        private static final long serialVersionUID = -2570371879963187647L;
        @JsonProperty("created_at")
        private String createdAt;
        @JsonProperty("id")
        private long id;
        @JsonProperty("image_height")
        private long imageHeight;
        @JsonProperty("image_thumb")
        private String imageThumbURL;
        @JsonProperty("image")
        private String imageURL;
        @JsonProperty("image_width")
        private long imageWidth;
        @JsonProperty("name")
        private String name;

        public void setId(long id2) {
            this.id = id2;
        }

        public long getId() {
            return this.id;
        }

        public void setCreatedAt(String createdAt2) {
            this.createdAt = createdAt2;
        }

        public String getCreatedAt() {
            return this.createdAt;
        }

        public void setImageWidth(long imageWidth2) {
            this.imageWidth = imageWidth2;
        }

        public long getImageWidth() {
            return this.imageWidth;
        }

        public void setImageHeight(long imageHeight2) {
            this.imageHeight = imageHeight2;
        }

        public long getImageHeight() {
            return this.imageHeight;
        }

        public void setImageURL(String imageURL2) {
            this.imageURL = imageURL2;
        }

        public String getImageURL() {
            return this.imageURL;
        }

        public void setImageThumbURL(String imageThumbURL2) {
            this.imageThumbURL = imageThumbURL2;
        }

        public String getImageThumbURL() {
            return this.imageThumbURL;
        }

        public void setName(String name2) {
            this.name = name2;
        }

        public String getName() {
            return this.name;
        }
    }

    public void setOffset(long offset2) {
        this.offset = offset2;
    }

    public long getOffset() {
        return this.offset;
    }

    public void setTotal(long total2) {
        this.total = total2;
    }

    public long getTotal() {
        return this.total;
    }

    public void setSort(String sort2) {
        this.sort = sort2;
    }

    public String getSort() {
        return this.sort;
    }

    public void setLimit(long limit2) {
        this.limit = limit2;
    }

    public long getLimit() {
        return this.limit;
    }

    public List<Song> getSongs() {
        if (this.songs == null) {
            this.songs = new ArrayList<>();
        }
        return this.songs;
    }

    public Song getSongAt(int position) {
        return getSongs().get(position);
    }

    public void addSong(Song song) {
        getSongs().add(song);
    }

    public String getFirstSongUrl() {
        return getSongAt(0).getUrl();
    }
}
