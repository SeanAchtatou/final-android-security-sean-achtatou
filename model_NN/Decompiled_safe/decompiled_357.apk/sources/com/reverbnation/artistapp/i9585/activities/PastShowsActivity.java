package com.reverbnation.artistapp.i9585.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.adapters.ShowsAdapter;
import com.reverbnation.artistapp.i9585.api.tasks.GetShowsApiTask;
import com.reverbnation.artistapp.i9585.models.ShowList;
import com.reverbnation.artistapp.i9585.utils.AnalyticsHelper;

public class PastShowsActivity extends BaseListTabChildActivity {
    private static final int LOADING_DIALOG_ID = 0;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.shows_list_activity);
        loadShows();
    }

    private void loadShows() {
        if (areShowsLoaded()) {
            setListUsingShows(getShows());
            return;
        }
        showDialog(0);
        new GetShowsApiTask(new GetShowsApiTask.GetShowsApiDelegate() {
            public void getShowsFinished(ShowList showList) {
                PastShowsActivity.this.getActivityHelper().dismissDialog(0);
                if (showList != null) {
                    PastShowsActivity.this.getActivityHelper().getApplication().setPastShowList(showList);
                    PastShowsActivity.this.setListUsingShows(showList);
                }
            }

            public void getShowsStarted() {
            }

            public void getShowsCancelled() {
            }
        }).execute(this, getActivityHelper().getArtistId(), null, "past");
    }

    private boolean areShowsLoaded() {
        return getShows() != null;
    }

    private ShowList getShows() {
        return getActivityHelper().getApplication().getPastShowList();
    }

    /* access modifiers changed from: private */
    public void setListUsingShows(ShowList showList) {
        setListAdapter(showList != null ? createShowsAdapter(showList) : null);
    }

    private ShowsAdapter createShowsAdapter(ShowList showList) {
        return new ShowsAdapter(this, R.layout.show_list_item, ShowList.sortEntriesByDescendingDate(ShowList.getPastShows(showList.getShows())));
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        AnalyticsHelper.getInstance(this).trackPageView("homescreen/shows/past");
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        AnalyticsHelper.getInstance(this).trackPageView("homescreen/shows/past/show_details");
        Intent intent = new Intent(this, ShowDetailsActivity.class);
        intent.putExtra("show", (ShowList.Show) getListAdapter().getItem(position));
        startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        if (id == 0) {
            return getActivityHelper().createProgressDialog(0, R.string.loading);
        }
        return super.onCreateDialog(id);
    }
}
