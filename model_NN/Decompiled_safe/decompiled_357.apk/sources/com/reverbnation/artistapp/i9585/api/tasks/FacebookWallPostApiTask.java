package com.reverbnation.artistapp.i9585.api.tasks;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import com.google.common.base.Preconditions;
import com.reverbnation.artistapp.i9585.api.FacebookApi;
import com.reverbnation.artistapp.i9585.models.FacebookResponse;
import com.reverbnation.artistapp.i9585.models.interfaces.FacebookShareable;
import com.reverbnation.artistapp.i9585.utils.FacebookHelper;
import com.reverbnation.artistapp.i9585.utils.LinkShortener;

public class FacebookWallPostApiTask extends AsyncTask<FacebookShareable, Void, FacebookResponse> {
    private Activity activity;

    public FacebookWallPostApiTask(Activity activity2) {
        this.activity = activity2;
    }

    /* access modifiers changed from: protected */
    public FacebookResponse doInBackground(FacebookShareable... items) {
        boolean z;
        if (items.length == 1) {
            z = true;
        } else {
            z = false;
        }
        Preconditions.checkState(z, "Only one sharable item at a time");
        FacebookShareable item = items[0];
        LinkShortener bitly = new LinkShortener(this.activity);
        FacebookHelper helper = new FacebookHelper(this.activity);
        Bundle parameters = new Bundle();
        parameters.putString("link", bitly.shorten(item.getFacebookLink(helper)));
        parameters.putString("source", item.getFacebookSource(helper));
        parameters.putString("picture", item.getFacebookPicture(helper));
        parameters.putString("name", item.getFacebookName(helper));
        parameters.putString("caption", item.getFacebookCaption(helper));
        return FacebookApi.postToWall(parameters);
    }
}
