package com.reverbnation.artistapp.i9585.exceptions;

import com.reverbnation.artistapp.i9585.models.SongList;

public class SongNotFoundException extends Exception {
    private static final long serialVersionUID = 193395133136330795L;
    private String message;

    public SongNotFoundException(SongList.Song song) {
        this.message = "The song '" + song.getName() + "' could not be found.";
    }

    public String getMessage() {
        return this.message;
    }
}
