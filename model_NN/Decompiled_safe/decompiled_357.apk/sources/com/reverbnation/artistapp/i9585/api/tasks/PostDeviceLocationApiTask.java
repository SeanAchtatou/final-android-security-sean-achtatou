package com.reverbnation.artistapp.i9585.api.tasks;

import android.location.Location;
import android.os.AsyncTask;
import com.google.common.base.Preconditions;
import com.reverbnation.artistapp.i9585.api.ReverbNationApi;
import com.roobit.restkit.RestClient;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

public class PostDeviceLocationApiTask extends AsyncTask<Location, Void, Void> {
    /* access modifiers changed from: protected */
    public Void doInBackground(Location... locations) {
        boolean z;
        if (locations.length == 1) {
            z = true;
        } else {
            z = false;
        }
        Preconditions.checkState(z);
        try {
            RestClient.getClientWithBaseUrl(ReverbNationApi.getBaseUrl()).postQuery(getQueryParameters(locations[0]), ReverbNationApi.getInstance().getRequiredHeaderParameters()).setResource("mobile_device_locations.json").synchronousExecute();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private Properties getQueryParameters(Location location) {
        Properties props = new Properties();
        props.put("mobile_session_id", String.valueOf(ReverbNationApi.getSessionId()));
        props.put("latitude", String.valueOf(location.getLatitude()));
        props.put("longitude", String.valueOf(location.getLongitude()));
        props.put("accuracy", String.valueOf(location.getAccuracy()));
        props.put("recorded_at", getRecordedAt());
        props.put("mobile_application_link_id", String.valueOf(ReverbNationApi.getInstance().getMobileApplication().getId()));
        return props;
    }

    private String getRecordedAt() {
        return new SimpleDateFormat("EEEE, MMMM dd, yyyy hh:mm:ss aa zzzz").format(new Date());
    }
}
