package com.reverbnation.artistapp.i9585.models;

import android.net.Uri;
import com.google.common.base.Function;
import com.google.common.base.Strings;
import com.google.common.collect.Ordering;
import com.reverbnation.artistapp.i9585.models.interfaces.FacebookShareable;
import com.reverbnation.artistapp.i9585.utils.FacebookHelper;
import com.reverbnation.artistapp.i9585.utils.MapHelper;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.util.MinimalPrettyPrinter;
import twitter4j.GeoQuery;

public class ShowList {
    @JsonProperty("results")
    private ArrayList<Show> shows;

    public static class Show implements Serializable, FacebookShareable {
        private static final long serialVersionUID = 5279632991763060912L;
        @JsonProperty("age_limit")
        private long ageLimit;
        @JsonProperty("attendance")
        private long attendance;
        @JsonProperty("details")
        private String details;
        @JsonProperty("id")
        private int id;
        @JsonProperty("showtime")
        private String showTime;
        @JsonProperty("ticket_url")
        private String ticketURL;
        @JsonProperty("venue")
        private Venue venue;

        public static class Venue implements Serializable {
            private static final long serialVersionUID = 7043813244519364871L;
            @JsonProperty("id")
            private long id;
            @JsonProperty("location")
            private Location location;
            @JsonProperty("name")
            private String name;

            public static class Location implements Serializable {
                private static final long serialVersionUID = -5753265968608431753L;
                @JsonProperty("address")
                private String address;
                @JsonProperty(GeoQuery.CITY)
                private String city;
                @JsonProperty("country")
                private String country;
                @JsonProperty("latitude")
                private float latitude;
                @JsonProperty("longitude")
                private float longitude;
                @JsonProperty("postal_code")
                private String postalCode;
                @JsonProperty("state")
                private String state;

                public void setAddress(String address2) {
                    this.address = address2;
                }

                public String getAddress() {
                    return this.address;
                }

                public boolean hasAddress() {
                    return this.address != null && !this.address.equals("");
                }

                public void setCity(String city2) {
                    this.city = city2;
                }

                public String getCity() {
                    return this.city;
                }

                public boolean hasCity() {
                    return !Strings.isNullOrEmpty(this.city);
                }

                public void setState(String state2) {
                    this.state = state2;
                }

                public boolean hasState() {
                    return !Strings.isNullOrEmpty(this.state);
                }

                public String getState() {
                    return this.state;
                }

                public void setCountry(String country2) {
                    this.country = country2;
                }

                public String getCountry() {
                    return this.country;
                }

                public boolean hasCountry() {
                    return !Strings.isNullOrEmpty(this.country);
                }

                public void setPostalCode(String postalCode2) {
                    this.postalCode = postalCode2;
                }

                public String getPostalCode() {
                    return this.postalCode;
                }

                public boolean hasPostalCode() {
                    return !Strings.isNullOrEmpty(this.postalCode);
                }

                public void setLongitude(float longitude2) {
                    this.longitude = longitude2;
                }

                public float getLongitude() {
                    return this.longitude;
                }

                public boolean hasLongitude() {
                    return this.longitude != 0.0f;
                }

                public void setLatitude(float latitude2) {
                    this.latitude = latitude2;
                }

                public float getLatitude() {
                    return this.latitude;
                }

                public boolean hasLatitude() {
                    return this.latitude != 0.0f;
                }

                public Uri generateUri(String locationName) {
                    try {
                        return MapHelper.generateUri(this, locationName);
                    } catch (Exception e) {
                        return null;
                    }
                }

                public String getMapQuery() {
                    StringBuilder s = new StringBuilder();
                    if (hasAddress()) {
                        s.append(getAddress());
                        s.append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
                    }
                    if (hasCity()) {
                        s.append(getCity());
                        s.append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
                    }
                    if (hasState()) {
                        s.append(getState());
                        s.append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
                    }
                    if (hasPostalCode()) {
                        s.append(getPostalCode());
                        s.append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
                    }
                    if (hasCountry()) {
                        s.append(getCountry());
                        s.append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
                    }
                    if (hasLatitude() && hasLongitude()) {
                        s.append(getLatitude());
                        s.append(",");
                        s.append(getLongitude());
                    }
                    return s.toString();
                }
            }

            public void setId(long id2) {
                this.id = id2;
            }

            public long getId() {
                return this.id;
            }

            public void setName(String name2) {
                this.name = name2;
            }

            public String getName() {
                return this.name;
            }

            public boolean hasName() {
                return this.name != null && !this.name.equals("");
            }

            public void setLocation(Location location2) {
                this.location = location2;
            }

            public Location getLocation() {
                return this.location;
            }

            public boolean hasLocation() {
                return this.location != null;
            }
        }

        public String getShowTime() {
            return this.showTime;
        }

        public String getShowTime(DateFormat df) {
            Date d = getShowTimeDate();
            return d != null ? df.format(d) : "";
        }

        public void setShowTime(String showtime) {
            this.showTime = showtime;
        }

        public boolean hasShowTime() {
            return !Strings.isNullOrEmpty(this.showTime);
        }

        public Date getShowTimeDate() {
            try {
                return new Date(this.showTime);
            } catch (Exception e) {
                return null;
            }
        }

        public boolean hasShowTimeDate() {
            return getShowTimeDate() != null;
        }

        public boolean isThisYear() {
            int showYear;
            int thisYear = new Date().getYear();
            if (hasShowTimeDate()) {
                showYear = getShowTimeDate().getYear();
            } else {
                showYear = 0;
            }
            if (thisYear == showYear) {
                return true;
            }
            return false;
        }

        public Venue getVenue() {
            return this.venue;
        }

        public void setVenue(Venue venue2) {
            this.venue = venue2;
        }

        public boolean hasVenue() {
            return this.venue != null;
        }

        public String getCity() {
            if (hasCity()) {
                return getVenue().getLocation().getCity();
            }
            return null;
        }

        public boolean hasCity() {
            return hasVenue() && getVenue().hasLocation() && getVenue().getLocation().hasCity();
        }

        public String getState() {
            if (hasState()) {
                return getVenue().getLocation().getState();
            }
            return null;
        }

        public boolean hasState() {
            return hasVenue() && getVenue().hasLocation() && getVenue().getLocation().hasState();
        }

        public String getCountry() {
            if (hasCountry()) {
                return getVenue().getLocation().getCountry();
            }
            return null;
        }

        public boolean hasCountry() {
            return hasVenue() && getVenue().hasLocation() && getVenue().getLocation().hasCountry();
        }

        public void setId(int id2) {
            this.id = id2;
        }

        public int getId() {
            return this.id;
        }

        public void setAgeLimit(long ageLimit2) {
            this.ageLimit = ageLimit2;
        }

        public long getAgeLimit() {
            return this.ageLimit;
        }

        public void setDetails(String details2) {
            this.details = details2;
        }

        public String getDetails() {
            return this.details;
        }

        public boolean hasDetails() {
            return this.details != null && !this.details.equals("");
        }

        public void setTicketURL(String ticketURL2) {
            this.ticketURL = ticketURL2;
        }

        public String getTicketURL() {
            return this.ticketURL;
        }

        public boolean hasTicketURL() {
            return this.ticketURL != null && !this.ticketURL.equals("");
        }

        public void setAttendance(long attendance2) {
            this.attendance = attendance2;
        }

        public long getAttendance() {
            return this.attendance;
        }

        public String getFacebookLink(FacebookHelper helper) {
            return helper.getFacebookLink(this);
        }

        public String getFacebookSource(FacebookHelper helper) {
            return helper.getFacebookSource(this);
        }

        public String getFacebookName(FacebookHelper helper) {
            return helper.getFacebookName(this);
        }

        public String getShowLink(String artistId) {
            return new Uri.Builder().scheme("http").path("//www.reverbnation.com").appendPath(artistId).appendEncodedPath("#!/show/" + this.id).build().toString();
        }

        public String getFacebookCaption(FacebookHelper helper) {
            return helper.getFacebookCaption(this);
        }

        public String getFacebookPicture(FacebookHelper helper) {
            return helper.getFacebookPicture();
        }
    }

    public List<Show> getShows() {
        if (this.shows == null) {
            this.shows = new ArrayList<>();
        }
        return this.shows;
    }

    public Show getShowAt(int index) {
        return this.shows.get(index);
    }

    public void addShow(Show show) {
        if (this.shows == null) {
            this.shows = new ArrayList<>();
        }
        this.shows.add(show);
    }

    public static String getFormattedShowTime(String unformattedTime) {
        Date date = null;
        try {
            date = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z").parse(unformattedTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat postFormat = new SimpleDateFormat("MMM dd");
        postFormat.setTimeZone(TimeZone.getTimeZone("EST"));
        try {
            return postFormat.format(date);
        } catch (NullPointerException e2) {
            return "";
        }
    }

    public static String getFormattedVenue(String city, String state, String country) {
        StringBuilder sb = new StringBuilder();
        if (!Strings.isNullOrEmpty(city)) {
            sb.append(city);
        }
        if (!Strings.isNullOrEmpty(state)) {
            sb.append(", ");
            sb.append(state);
        } else if (!Strings.isNullOrEmpty(country)) {
            sb.append(", ");
            sb.append(country);
        }
        return sb.toString();
    }

    public static List<Show> sortEntriesByAscendingDate(List<Show> showList) {
        return Ordering.natural().onResultOf(new Function<Show, Date>() {
            public Date apply(Show show) {
                return show.getShowTimeDate();
            }
        }).sortedCopy(showList);
    }

    public static List<Show> sortEntriesByDescendingDate(List<Show> showList) {
        return Ordering.natural().onResultOf(new Function<Show, Date>() {
            public Date apply(Show show) {
                return show.getShowTimeDate();
            }
        }).reverse().sortedCopy(showList);
    }

    public static List<Show> getUpcomingShows(List<Show> showList) {
        Date today = new Date(System.currentTimeMillis());
        List<Show> upcomingShowList = new ArrayList<>();
        for (Show s : showList) {
            if (s.getShowTimeDate().after(today)) {
                upcomingShowList.add(s);
            }
        }
        return upcomingShowList;
    }

    public static List<Show> getPastShows(List<Show> showList) {
        Date today = new Date(System.currentTimeMillis());
        List<Show> pastShowList = new ArrayList<>();
        for (Show s : showList) {
            if (s.getShowTimeDate().before(today)) {
                pastShowList.add(s);
            }
        }
        return pastShowList;
    }
}
