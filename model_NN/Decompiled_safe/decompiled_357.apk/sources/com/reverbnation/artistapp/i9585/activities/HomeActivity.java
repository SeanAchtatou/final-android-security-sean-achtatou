package com.reverbnation.artistapp.i9585.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.TextView;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.api.tasks.GetSongsApiTask;
import com.reverbnation.artistapp.i9585.api.tasks.PostDeviceLocationApiTask;
import com.reverbnation.artistapp.i9585.models.Photoset;
import com.reverbnation.artistapp.i9585.models.SongList;
import com.reverbnation.artistapp.i9585.services.ReverbMusicService;
import com.reverbnation.artistapp.i9585.utils.AnalyticsHelper;
import com.reverbnation.artistapp.i9585.utils.LastLocationFinder;
import com.reverbnation.artistapp.i9585.utils.SlideSwitcher;
import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends BaseActivity {
    private static final int ARTIST_ID_FAILED_DIALOG = 1;
    private static final int LOCATION_MAX_AGE_MINUTES = 2;
    private static final int LOCATION_MIN_ACCURACY_METERS = 50;
    private static final int MAIL_LIST_DIALOG = 0;
    private static final long MAIL_LIST_TIMEOUT_MILLIS = 75000;
    protected static final int MAIL_LIST_TIMEOUT_MSG = 0;
    private static final int MINUTE_TO_MILLIS = 60000;
    private static final long NEXT_SLIDE_DELAY_MILLIS = 5000;
    protected static final int NEXT_SLIDE_MSG = 1;
    private static final int SLIDE_ANIMATION_DURATION_MILLIS = 2000;
    private static final String TAG = "ReverbNation";
    Handler handler;
    LastLocationFinder locationFinder;
    SlideSwitcher slideSwitcher;
    boolean timersArePaused = false;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(7);
        setContentView((int) R.layout.home_activity);
        getActivityHelper().setArtistTitlebar((int) R.string.artist_name);
        setupViews();
        setupPhotos();
        setupLocationFinder();
        setupTimers();
        startMusicService();
    }

    private void setupViews() {
        setupHomeButton(R.id.songs_button, R.string.songs, R.drawable.home_songs, SongsActivity.class);
        setupHomeButton(R.id.photos_button, R.string.photos_and_videos, R.drawable.home_multimedia, PhotosAndVideosActivity.class);
        setupHomeButton(R.id.news_button, R.string.news, R.drawable.home_news, NewsActivity.class);
        setupHomeButton(R.id.shows_button, R.string.shows, R.drawable.home_shows, ShowsActivity.class, hasShows() ? 0 : 8);
        setupHomeButton(R.id.band_button, R.string.band_info, R.drawable.home_band_info, BandInfoActivity.class);
        setupHomeButton(R.id.pulse_button, R.string.social, R.drawable.home_pulse, SocialActivity.class);
    }

    private void setupHomeButton(int viewId, int textId, int imageId, Class<?> activityClass) {
        setupHomeButton(viewId, textId, imageId, activityClass, 0);
    }

    private void setupHomeButton(int viewId, int textId, int imageId, final Class<?> activityClass, int visibility) {
        View v = findViewById(viewId);
        v.setVisibility(visibility);
        ImageView iv = (ImageView) v.findViewById(R.id.button_image);
        ((TextView) v.findViewById(R.id.button_text)).setText(textId);
        iv.setImageResource(imageId);
        iv.setTag(Integer.valueOf(imageId));
        iv.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HomeActivity.this.startActivity(new Intent(HomeActivity.this.getBaseContext(), activityClass));
            }
        });
    }

    private boolean hasShows() {
        int showCount = getIntent().getIntExtra("showCount", -1);
        if (showCount == -1) {
            return getActivityHelper().getApplication().artistHasShows();
        }
        return showCount > 0;
    }

    private void setupPhotos() {
        List<Photoset.Photo> photos;
        try {
            photos = getActivityHelper().getApplication().getMobileApplication().getSlidePhotos();
        } catch (NullPointerException e) {
            photos = new ArrayList<>();
        }
        this.slideSwitcher = new SlideSwitcher(this, (ImageSwitcher) findViewById(R.id.switcher_slides), photos, 2000);
    }

    private void setupLocationFinder() {
        this.locationFinder = new LastLocationFinder(this);
        this.locationFinder.setChangedLocationListener(new LocationListener() {
            public void onLocationChanged(Location location) {
                HomeActivity.this.postLocation(location);
            }

            public void onProviderDisabled(String provider) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }
        });
    }

    private void startMusicService() {
        try {
            String artistId = getActivityHelper().getArtistId();
            new GetSongsApiTask(new GetSongsApiTask.GetSongsApiDelegate() {
                public void getSongsFinished(SongList songList) {
                    if (songList != null) {
                        Intent intent = new Intent(HomeActivity.this.getBaseContext(), ReverbMusicService.class);
                        intent.putExtra("playlist", songList);
                        HomeActivity.this.startService(intent);
                    }
                }

                public void getSongsStarted() {
                }

                public void getSongsCancelled() {
                }
            }).execute(artistId, null, this);
        } catch (Exception e) {
            showDialog(1);
            pauseTimers();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        stopService(new Intent(this, ReverbMusicService.class));
        super.onDestroy();
    }

    private void setupTimers() {
        this.handler = new Handler() {
            public void handleMessage(Message msg) {
                if (msg.what == 0) {
                    HomeActivity.this.showDialog(0);
                } else if (msg.what == 1) {
                    HomeActivity.this.advanceSlideshow();
                }
            }
        };
        if (!hasMailListPreference()) {
            this.handler.sendEmptyMessageDelayed(0, MAIL_LIST_TIMEOUT_MILLIS);
        }
        this.handler.sendEmptyMessageDelayed(1, NEXT_SLIDE_DELAY_MILLIS);
    }

    /* access modifiers changed from: protected */
    public void advanceSlideshow() {
        this.slideSwitcher.nextSlide();
        if (!this.timersArePaused) {
            this.handler.sendEmptyMessageDelayed(1, NEXT_SLIDE_DELAY_MILLIS);
        }
    }

    private boolean hasMailListPreference() {
        return getActivityHelper().getApplicationPreferences().getString("mail_list", null) != null;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        pauseTimers();
        this.locationFinder.cancel();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        AnalyticsHelper.getInstance(this).trackPageView("homescreen");
        super.onResume();
        if (this.timersArePaused) {
            resumeTimers();
        }
        updateLocation();
    }

    private void updateLocation() {
        Location location = this.locationFinder.getLastBestLocation(50, 120000);
        if (location != null && locationIsFreshAndAccurate(location)) {
            postLocation(location);
        }
    }

    private boolean locationIsFreshAndAccurate(Location location) {
        if (!location.hasAccuracy() || location.getAccuracy() > 50.0f || System.currentTimeMillis() - location.getTime() <= 120000) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void postLocation(Location location) {
        Log.d("ReverbNation", "Posting device location: " + location.getLatitude() + ", " + location.getLongitude());
        new PostDeviceLocationApiTask().execute(location);
    }

    public void onJoinButtonClicked(View v) {
        AnalyticsHelper.getInstance(this).trackEvent("prompts", "mailinglist", "join_now");
        getActivityHelper().dismissDialog(0);
        Intent intent = new Intent(this, SocialActivity.class);
        intent.putExtra("show_tag", SocialActivity.MailingListTab);
        startActivity(intent);
    }

    public void onRemindMeButtonClicked(View v) {
        AnalyticsHelper.getInstance(this).trackEvent("prompts", "mailinglist", "remind_me");
        getActivityHelper().dismissDialog(0);
    }

    public void onNoThanksButtonClicked(View v) {
        AnalyticsHelper.getInstance(this).trackEvent("prompts", "mailinglist", "no_thanks");
        getActivityHelper().dismissDialog(0);
        setMailListPreference(false);
    }

    private void setMailListPreference(boolean preference) {
        SharedPreferences.Editor editor = getActivityHelper().getApplicationPreferences().edit();
        editor.putString("mail_list", preference ? "yes" : "no");
        editor.commit();
    }

    private void resumeTimers() {
        if (this.timersArePaused) {
            this.timersArePaused = false;
            this.handler.sendEmptyMessageDelayed(1, NEXT_SLIDE_DELAY_MILLIS);
            if (!hasMailListPreference()) {
                this.handler.sendEmptyMessageDelayed(0, MAIL_LIST_TIMEOUT_MILLIS);
            }
        }
    }

    private void pauseTimers() {
        this.timersArePaused = true;
        this.handler.removeMessages(0);
        this.handler.removeMessages(1);
    }

    /* access modifiers changed from: protected */
    public void setMailListTimer(int millis) {
        this.handler.removeMessages(0);
        this.handler.sendEmptyMessageDelayed(0, (long) millis);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        if (id == 0) {
            return createMailListDialog();
        }
        if (id != 1) {
            return super.onCreateDialog(id);
        }
        Log.v("ReverbNation", "onCreateDialog - artist id failed");
        return createArtistIdFailedDialog();
    }

    private Dialog createMailListDialog() {
        AnalyticsHelper.getInstance(this).trackPageView("mailinglist");
        AnalyticsHelper.getInstance(this).trackEvent("prompts", "mailinglist", "displayed");
        return new AlertDialog.Builder(this).setTitle((int) R.string.mailing_list).setMessage((int) R.string.join_the_mailing_list).setView(getLayoutInflater().inflate((int) R.layout.mail_list_dialog, (ViewGroup) null)).create();
    }

    private Dialog createArtistIdFailedDialog() {
        Log.v("ReverbNation", "Creating artist id failed dialog");
        return new AlertDialog.Builder(this).setTitle((int) R.string.connection_failed).setMessage((int) R.string.artist_id_error).setNeutralButton(17039370, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                HomeActivity.this.finish();
            }
        }).create();
    }
}
