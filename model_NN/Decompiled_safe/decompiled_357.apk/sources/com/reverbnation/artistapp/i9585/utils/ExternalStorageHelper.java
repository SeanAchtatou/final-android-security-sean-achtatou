package com.reverbnation.artistapp.i9585.utils;

import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import org.codehaus.jackson.org.objectweb.asm.Opcodes;

public class ExternalStorageHelper {
    private static int BufferSize = Opcodes.ACC_ENUM;
    private static final byte MAGIC_NUMBER = 13;
    private static final String TAG = "ExternalStorageHelper";

    /* access modifiers changed from: private */
    public static void translateBuffer(byte[] buffer) {
        for (int i = 0; i < buffer.length; i++) {
            buffer[i] = (byte) (buffer[i] ^ 13);
        }
    }

    private static class TranslateOutputStream extends FileOutputStream {
        public TranslateOutputStream(File file) throws FileNotFoundException {
            super(file);
        }

        public void write(int oneByte) throws IOException {
            super.write(oneByte ^ 13);
        }

        public void write(byte[] buffer, int offset, int count) throws IOException {
            ExternalStorageHelper.translateBuffer(buffer);
            super.write(buffer, offset, count);
        }
    }

    private static class TranslateInputStream extends FileInputStream {
        public TranslateInputStream(File file) throws FileNotFoundException {
            super(file);
        }

        public int read() throws IOException {
            return super.read() ^ 13;
        }

        public int read(byte[] buffer, int offset, int count) throws IOException {
            int bytesRead = super.read(buffer, offset, count);
            ExternalStorageHelper.translateBuffer(buffer);
            return bytesRead;
        }
    }

    private static class PassThroughInputStream extends FileInputStream {
        public PassThroughInputStream(File file) throws FileNotFoundException {
            super(file);
        }

        public int read() throws IOException {
            return super.read();
        }

        public int read(byte[] buffer, int offset, int count) throws IOException {
            return super.read(buffer, offset, count);
        }
    }

    public static File openDirectory(String dirname) {
        File f = null;
        if (Environment.getExternalStorageState().equals("mounted") && (f = new File(Environment.getExternalStorageDirectory(), dirname)) != null && !f.exists()) {
            Log.d(TAG, "Creating directory " + f.toURI());
            f.mkdirs();
        }
        return f;
    }

    public static File openFile(String dirname, String filename) {
        File dir = openDirectory(dirname);
        if (dir != null) {
            return new File(dir, filename);
        }
        return null;
    }

    public static String getFilenameFromUrl(String urlString) {
        if (urlString == null) {
            return null;
        }
        int index = urlString.lastIndexOf(47);
        return index == -1 ? urlString : urlString.substring(index + 1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static String UrlToFilename(String urlString) {
        if (urlString == null) {
            return null;
        }
        return urlString.replace('/', '_').replace(':', '_');
    }

    public static void writeStreamToFile(InputStream is, String dirname, String outputFilename, boolean shouldEncrypt) throws IOException {
        File f = openFile(dirname, outputFilename);
        FileOutputStream fos = shouldEncrypt ? new TranslateOutputStream(f) : new FileOutputStream(f);
        byte[] buffer = new byte[BufferSize];
        while (true) {
            int count = is.read(buffer, 0, BufferSize);
            if (count > 0) {
                fos.write(buffer, 0, count);
            } else {
                is.close();
                fos.close();
                return;
            }
        }
    }

    public static FileInputStream translateInputStream(File f) throws FileNotFoundException {
        return new TranslateInputStream(f);
    }

    public static FileInputStream passThroughInputStream(File f) throws FileNotFoundException {
        return new PassThroughInputStream(f);
    }

    public static File getCacheDir(Context context) {
        return Environment.getExternalStorageState().equals("mounted") ? getExternalCacheDir(context) : context.getCacheDir();
    }

    private static File getExternalCacheDir(Context context) {
        return Build.VERSION.SDK_INT >= 8 ? context.getExternalCacheDir() : context.getCacheDir();
    }
}
