package com.reverbnation.artistapp.i9585.models;

import android.net.Uri;
import com.reverbnation.artistapp.i9585.models.interfaces.FacebookShareable;
import com.reverbnation.artistapp.i9585.utils.DateHelper;
import com.reverbnation.artistapp.i9585.utils.FacebookHelper;
import com.urbanairship.analytics.EventDataManager;
import java.io.Serializable;
import java.util.Date;
import org.codehaus.jackson.annotate.JsonProperty;

public class BlogPost implements Serializable, FacebookShareable {
    public static final String REVERBNATION_TYPE = "ReverbNation";
    public static final String RSS_TYPE = "RSS";
    private static final long serialVersionUID = 1122773582483844173L;
    @JsonProperty("contents")
    private String body;
    @JsonProperty("created_at")
    private String createdAt;
    private Date createdAtDate;
    @JsonProperty("id")
    private String id;
    @JsonProperty("link")
    private String link;
    @JsonProperty("title")
    private String title;
    @JsonProperty(EventDataManager.Events.COLUMN_NAME_TYPE)
    private String type;

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getCreatedAt() {
        return this.createdAt;
    }

    public void setCreatedAt(String createdAt2) {
        this.createdAt = createdAt2;
    }

    public String getBody() {
        return this.body;
    }

    public void setBody(String body2) {
        this.body = body2;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public String getType() {
        return this.type;
    }

    public String getLink() {
        return this.link;
    }

    public Date getCreatedAtDate() {
        if (this.createdAtDate == null) {
            this.createdAtDate = DateHelper.parse(getCreatedAt());
        }
        return this.createdAtDate;
    }

    public String getBlogLink(String artistId) {
        return new Uri.Builder().scheme("http").path("//www.reverbnation.com").appendPath(artistId).appendEncodedPath("#!/page_object/page_object_blogs/artist_" + artistId).build().toString();
    }

    public String getFacebookName(FacebookHelper helper) {
        return helper.getFacebookName(this);
    }

    public String getFacebookLink(FacebookHelper helper) {
        return helper.getFacebookLink(this);
    }

    public String getFacebookSource(FacebookHelper helper) {
        return helper.getFacebookSource();
    }

    public String getFacebookPicture(FacebookHelper helper) {
        return helper.getFacebookPicture();
    }

    public String getFacebookCaption(FacebookHelper helper) {
        return helper.getFacebookCaption(this);
    }
}
