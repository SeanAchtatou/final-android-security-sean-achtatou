package com.reverbnation.artistapp.i9585.utils;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.reverbnation.artistapp.i9585.R;
import java.io.IOException;
import java.util.Properties;
import org.codehaus.jackson.util.MinimalPrettyPrinter;

public class AnalyticsHelper {
    private static final boolean ANALYTICS_ENABLED = true;
    private static final String TAG = "AnalyticsHelper";
    private static boolean analyticsEnabled;
    private static AnalyticsHelper instance;
    private Context context;
    /* access modifiers changed from: private */
    public GoogleAnalyticsTracker tracker = GoogleAnalyticsTracker.getInstance();
    private String uaCode;

    public static AnalyticsHelper getInstance(Context context2) {
        if (instance == null) {
            analyticsEnabled = ANALYTICS_ENABLED;
            instance = new AnalyticsHelper(context2);
        }
        return instance;
    }

    private AnalyticsHelper(Context context2) {
        this.context = context2.getApplicationContext();
        if (analyticsEnabled) {
            loadAnalyticsProperties();
            this.tracker.start(this.uaCode, 30, this.context);
        }
    }

    public void trackPageView(final String path) {
        Log.v(TAG, "Tracking page view: " + path);
        if (analyticsEnabled) {
            new AsyncTask<Void, Void, Void>() {
                /* access modifiers changed from: protected */
                public Void doInBackground(Void... voids) {
                    try {
                        AnalyticsHelper.this.tracker.trackPageView(path);
                        return null;
                    } catch (Exception e) {
                        Log.w(AnalyticsHelper.TAG, "trackPageView error: " + path, e);
                        return null;
                    }
                }
            }.execute(new Void[0]);
        }
    }

    public void trackEvent(final String event, final String action, final String label) {
        Log.v(TAG, "Tracking event: " + event + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + action + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + label);
        if (analyticsEnabled) {
            new AsyncTask<Void, Void, Void>() {
                /* access modifiers changed from: protected */
                public Void doInBackground(Void... voids) {
                    try {
                        AnalyticsHelper.this.tracker.trackEvent(event, action, label, -1);
                        return null;
                    } catch (Exception e) {
                        Log.w(AnalyticsHelper.TAG, "trackError error: " + event, e);
                        return null;
                    }
                }
            }.execute(new Void[0]);
        }
    }

    private void loadAnalyticsProperties() {
        Properties props = new Properties();
        try {
            props.load(this.context.getResources().openRawResource(R.raw.analytics));
            this.uaCode = props.getProperty("ua_code");
        } catch (IOException e) {
            Log.d(TAG, "Failed to load anaytics.properties");
            analyticsEnabled = false;
        }
    }

    public void stopTracker() {
        if (analyticsEnabled) {
            this.tracker.stop();
        }
    }
}
