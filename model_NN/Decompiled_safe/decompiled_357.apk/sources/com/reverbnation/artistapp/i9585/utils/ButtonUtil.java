package com.reverbnation.artistapp.i9585.utils;

import android.view.MotionEvent;
import android.widget.ImageView;

public class ButtonUtil {
    public static boolean buttonTouched(ImageView button, MotionEvent event, int up_resid, int down_resid) {
        switch (event.getAction()) {
            case 0:
                button.setImageResource(down_resid);
                return false;
            case 1:
                button.setImageResource(up_resid);
                return false;
            default:
                return false;
        }
    }
}
