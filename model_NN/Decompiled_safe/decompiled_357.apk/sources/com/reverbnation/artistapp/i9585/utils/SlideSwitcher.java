package com.reverbnation.artistapp.i9585.utils;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.ViewSwitcher;
import com.reverbnation.artistapp.i9585.models.Photoset;
import java.util.List;

public class SlideSwitcher implements ViewSwitcher.ViewFactory {
    int animationDurationMs;
    Context context;
    int currentSlideIndex = 0;
    DrawableManager drawableManager;
    List<Photoset.Photo> photos;
    ImageSwitcher switcher;

    public SlideSwitcher(Context context2, ImageSwitcher switcher2, List<Photoset.Photo> photos2, int animationDurationMs2) {
        this.context = context2;
        this.switcher = switcher2;
        this.photos = photos2;
        this.animationDurationMs = animationDurationMs2;
        this.currentSlideIndex = 0;
        this.drawableManager = DrawableManager.getInstance(context2);
        setupSwitcher();
        preloadImages();
    }

    private void setupSwitcher() {
        this.switcher.setFactory(this);
        Animation fadeIn = AnimationUtils.loadAnimation(this.context, 17432576);
        Animation fadeOut = AnimationUtils.loadAnimation(this.context, 17432577);
        fadeIn.setDuration((long) this.animationDurationMs);
        fadeOut.setDuration((long) this.animationDurationMs);
        this.switcher.setInAnimation(fadeIn);
        this.switcher.setOutAnimation(fadeOut);
        if (this.photos.size() > 0) {
            this.drawableManager.bindDrawable(this.photos.get(0).getImageUrl(), (ImageView) this.switcher.getCurrentView());
        }
    }

    private void preloadImages() {
        new AsyncTask<Void, Void, Void>() {
            /* access modifiers changed from: protected */
            public Void doInBackground(Void... noargs) {
                for (Photoset.Photo imageUrl : SlideSwitcher.this.photos) {
                    SlideSwitcher.this.drawableManager.pleaseCacheDrawable(imageUrl.getImageUrl());
                }
                return null;
            }
        }.execute(new Void[0]);
    }

    public void nextSlide() {
        int next = getNextSlideIndex();
        prepareNextSlide(next);
        this.switcher.showNext();
        this.currentSlideIndex = next;
    }

    private int getNextSlideIndex() {
        int index = this.currentSlideIndex + 1;
        if (index >= this.photos.size()) {
            return 0;
        }
        return index;
    }

    private void prepareNextSlide(int index) {
        if (index < this.photos.size()) {
            this.drawableManager.bindDrawable(this.photos.get(index).getImageUrl(), (ImageView) this.switcher.getNextView());
        }
    }

    public View makeView() {
        ImageView v = new ImageView(this.context);
        v.setScaleType(ImageView.ScaleType.CENTER_CROP);
        v.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        return v;
    }
}
