package com.reverbnation.artistapp.i9585.models;

import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.models.interfaces.FacebookShareable;
import com.reverbnation.artistapp.i9585.utils.FacebookHelper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.codehaus.jackson.annotate.JsonProperty;

public class Links {
    private static Map<String, Integer> map = new HashMap();
    @JsonProperty("results")
    private ArrayList<Link> links;

    public static class Link implements FacebookShareable {
        @JsonProperty("id")
        private int id;
        @JsonProperty("url")
        private String url;
        @JsonProperty("website")
        private String website;

        public int getId() {
            return this.id;
        }

        public void setId(int id2) {
            this.id = id2;
        }

        public String getWebsite() {
            return this.website;
        }

        public void setWebsite(String website2) {
            this.website = website2;
        }

        public String getUrl() {
            return this.url;
        }

        public void setUrl(String url2) {
            this.url = url2;
        }

        public String getFacebookName(FacebookHelper helper) {
            return helper.getFacebookName(this);
        }

        public String getFacebookLink(FacebookHelper helper) {
            return getUrl();
        }

        public String getFacebookSource(FacebookHelper helper) {
            return helper.getFacebookSource();
        }

        public String getFacebookPicture(FacebookHelper helper) {
            return helper.getFacebookPicture();
        }

        public String getFacebookCaption(FacebookHelper helper) {
            return helper.getFacebookCaption(this);
        }
    }

    public List<Link> getLinks() {
        return this.links;
    }

    public Link getLinkAt(int position) {
        return this.links.get(position);
    }

    static {
        map.put("amazon", Integer.valueOf((int) R.drawable.link_amazon));
        map.put("artist website", Integer.valueOf((int) R.drawable.link_artistwebsite));
        map.put("emusic", Integer.valueOf((int) R.drawable.link_emusic));
        map.put("facebook", Integer.valueOf((int) R.drawable.link_facebook));
        map.put("flickr", Integer.valueOf((int) R.drawable.link_flickr));
        map.put("foursquare", Integer.valueOf((int) R.drawable.link_foursquare));
        map.put("gowalla", Integer.valueOf((int) R.drawable.link_gowalla));
        map.put("homepage", Integer.valueOf((int) R.drawable.link_artistwebsite));
        map.put("last.fm", Integer.valueOf((int) R.drawable.link_lastfm));
        map.put("mog", Integer.valueOf((int) R.drawable.link_mog));
        map.put("myspace", Integer.valueOf((int) R.drawable.link_myspace));
        map.put("picasa", Integer.valueOf((int) R.drawable.link_picasa));
        map.put("posterous", Integer.valueOf((int) R.drawable.link_posterous));
        map.put("purevolume", Integer.valueOf((int) R.drawable.link_purevolume));
        map.put("rdio", Integer.valueOf((int) R.drawable.link_rdio));
        map.put("rhapsody", Integer.valueOf((int) R.drawable.link_rhapsody));
        map.put("soundcloud", Integer.valueOf((int) R.drawable.link_soundcloud));
        map.put("spotify", Integer.valueOf((int) R.drawable.link_spotify));
        map.put("tumblr", Integer.valueOf((int) R.drawable.link_tumblr));
        map.put("twitter", Integer.valueOf((int) R.drawable.link_twitter));
        map.put("vimeo", Integer.valueOf((int) R.drawable.link_vimeo));
        map.put("wordpress", Integer.valueOf((int) R.drawable.link_wordpress));
        map.put("youtube", Integer.valueOf((int) R.drawable.link_youtube));
    }

    public static int getLinkImageResource(String websiteName) {
        Integer resId = map.get(websiteName.toLowerCase());
        return resId != null ? resId.intValue() : R.drawable.link_other;
    }
}
