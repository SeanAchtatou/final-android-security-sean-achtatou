package com.reverbnation.artistapp.i9585.models;

import org.codehaus.jackson.annotate.JsonProperty;

public class TwitterInfo {
    @JsonProperty("username")
    private String username;

    public String getUsername() {
        return this.username;
    }
}
