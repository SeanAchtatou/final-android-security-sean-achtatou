package com.reverbnation.artistapp.i9585.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Toast;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.ReverbNationApplication;
import com.reverbnation.artistapp.i9585.services.AudioPlayer;
import com.reverbnation.artistapp.i9585.services.ReverbMusicService;
import com.reverbnation.artistapp.i9585.views.MusicPlayer;
import twitter4j.Twitter;

public class ActivityHelper {
    private static final String TAG = "ActivityHelper";
    /* access modifiers changed from: private */
    public Activity activity;
    private EmailHelper emailHelper;
    private AlertDialog pauseDialog;
    private TitlebarHelper titlebarHelper;
    private TwitterHelper twitterHelper;

    public static ActivityHelper getInstance(Activity activity2) {
        return new ActivityHelper(activity2);
    }

    protected ActivityHelper(Activity activity2) {
        this.activity = activity2;
    }

    public ReverbNationApplication getApplication() {
        return (ReverbNationApplication) this.activity.getApplication();
    }

    public SharedPreferences getApplicationPreferences() {
        return getApplication().getApplicationPreferences();
    }

    public Dialog createProgressDialog(int titleResourceId, int msgResourceId) {
        ProgressDialog dialog = new ProgressDialog(this.activity);
        dialog.setIndeterminate(true);
        if (titleResourceId != 0) {
            dialog.setTitle(titleResourceId);
        }
        dialog.setMessage(this.activity.getString(msgResourceId));
        return dialog;
    }

    public Dialog createDialog(int titleResourceId, int msgResourceId) {
        return createDialog(titleResourceId, this.activity.getString(msgResourceId));
    }

    public Dialog createDialog(int titleResourceId, String message) {
        return new AlertDialog.Builder(this.activity).setTitle(titleResourceId).setMessage(message).setNeutralButton(17039370, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).create();
    }

    public void wtf(String message) {
        Log.i(this.activity.getLocalClassName(), message);
        Toast.makeText(this.activity, (int) R.string.totally_unexpected, 1).show();
        try {
            Log.wtf(this.activity.getLocalClassName(), message);
        } catch (NoSuchMethodError e) {
        }
    }

    public void setArtistTitlebar(int titleRes) {
        getTitlebarHelper().setArtistTitlebar(titleRes);
    }

    public void setArtistTitlebar(String title) {
        getTitlebarHelper().setArtistTitlebar(title);
    }

    public MusicPlayer getMusicPlayer() {
        return getTitlebarHelper().getMusicPlayer();
    }

    public TitlebarHelper getTitlebarHelper() {
        if (this.titlebarHelper == null) {
            this.titlebarHelper = new TitlebarHelper(this.activity);
        }
        return this.titlebarHelper;
    }

    public TwitterHelper getTwitterHelper() {
        if (this.twitterHelper == null) {
            this.twitterHelper = new TwitterHelper(this.activity);
        }
        return this.twitterHelper;
    }

    public Twitter getTwitter() {
        return getApplication().getTwitter();
    }

    public boolean isTwitterAuthorized() {
        return getApplication().isTwitterAuthorized();
    }

    public EmailHelper getEmailHelper() {
        if (this.emailHelper == null) {
            this.emailHelper = new EmailHelper();
        }
        return this.emailHelper;
    }

    public String getSmallArtistImage() {
        return getApplication().getMobileApplication().getPageObject().getImageSmall();
    }

    public void handlePlayerEvent(ReverbMusicService musicService, Intent intent) {
        AudioPlayer.MusicActionEvent event = AudioPlayer.MusicActionEvent.values()[intent.getIntExtra("event", AudioPlayer.MusicActionEvent.UNDEFINED_EVENT.ordinal())];
        Log.v(tag(), "Received REVERB_MEDIA_ACTION: " + event.toString());
        MusicPlayer player = getMusicPlayer();
        if (player == null) {
            Log.v(tag(), "No music player found");
            return;
        }
        switch (event) {
            case SONG_READY_EVENT:
                player.showProgress(false);
                player.setPlaying(false);
                player.setSongTitle(intent.getStringExtra("title"));
                player.updateProgress(intent.getLongExtra("progress", 0), intent.getLongExtra("duration", 0));
                return;
            case SONG_PREPARING_EVENT:
                player.showProgress(true);
                player.setPlaying(true);
                player.setSongTitle(intent.getStringExtra("title"));
                player.updateProgress(intent.getLongExtra("progress", 0), intent.getLongExtra("duration", 0));
                return;
            case SONG_PLAYING_EVENT:
                player.showProgress(false);
                player.setPlaying(true);
                return;
            case SONG_PAUSED_EVENT:
                player.showProgress(false);
                player.setPlaying(false);
                if (isFirstPause()) {
                    showPauseDialog(musicService);
                    return;
                }
                return;
            case SONG_ENDED_EVENT:
                player.showProgress(false);
                player.setPlaying(false);
                return;
            case UPDATE_PROGRESS_EVENT:
                player.showProgress(false);
                long progress = intent.getLongExtra("progress", -1);
                long duration = intent.getLongExtra("duration", -1);
                if (progress >= 0 && duration >= 0) {
                    player.updateProgress(progress, duration);
                    return;
                } else if (progress >= 0) {
                    player.updateProgress(progress);
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }

    private boolean isFirstPause() {
        return PreferenceManager.getDefaultSharedPreferences(this.activity).getBoolean("first_pause", true);
    }

    private void setFirstPause() {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this.activity).edit();
        editor.putBoolean("first_pause", false);
        editor.commit();
    }

    private void showPauseDialog(final ReverbMusicService musicService) {
        AnalyticsHelper.getInstance(this.activity).trackEvent("prompts", "autoplay", "displayed");
        if (this.pauseDialog == null) {
            setFirstPause();
            this.pauseDialog = new AlertDialog.Builder(this.activity).setTitle((int) R.string.music_player).setMessage((int) R.string.autoplay_is_on).setNegativeButton((int) R.string.turn_off, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    AnalyticsHelper.getInstance(ActivityHelper.this.activity).trackEvent("prompts", "autoplay", "turn_off");
                    musicService.setAutoplayEnabled(false);
                }
            }).setPositiveButton((int) R.string.leave_on, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    AnalyticsHelper.getInstance(ActivityHelper.this.activity).trackEvent("prompts", "autoplay", "keep_on");
                    musicService.setAutoplayEnabled(true);
                }
            }).create();
        }
        this.pauseDialog.show();
    }

    private void dismissPauseDialog() {
        if (this.pauseDialog != null) {
            this.pauseDialog.dismiss();
            this.pauseDialog = null;
        }
    }

    public void syncMusicPlayerView(ReverbMusicService musicService) {
        Log.v(tag(), "Synching music player view");
        MusicPlayer player = getMusicPlayer();
        if (player == null) {
            Log.i(tag(), "No music player found");
        } else if (!musicService.isReady()) {
            Log.i(tag(), "Music service not ready");
        } else {
            try {
                Log.v(tag(), "Setting playing attribute");
                player.setPlaying(musicService.isPlaying());
                Log.v(tag(), "Setting title attribute");
                player.setSongTitle(musicService.getCurrentSongTitle());
                Log.v(tag(), "Setting progress attributes");
                player.updateProgress(musicService.getCurrentSongProgress(), musicService.getCurrentSongDuration());
            } catch (IllegalStateException e) {
                Log.v(tag(), "Music service not ready yet");
            }
        }
    }

    public void broadcastKeyCode(int keyCode) {
        Intent intent = new Intent(ReverbMusicService.MEDIA_BUTTON);
        intent.putExtra("android.intent.extra.KEY_EVENT", new KeyEvent(0, keyCode));
        this.activity.sendBroadcast(intent);
    }

    private String tag() {
        return "ActivityHelper:" + this.activity.getClass().getSimpleName();
    }

    public String getArtistId() {
        return getApplication().getArtistId();
    }

    public void enableAudioActivator() {
        this.activity.runOnUiThread(new Runnable() {
            public void run() {
                ActivityHelper.this.getMusicPlayer().setButtonEnabled();
            }
        });
    }

    public void dismissDialog(int id) {
        try {
            this.activity.dismissDialog(id);
        } catch (IllegalArgumentException e) {
        }
    }
}
