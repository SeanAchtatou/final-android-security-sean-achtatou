package com.reverbnation.artistapp.i9585.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.models.Links;
import java.util.List;

public class LinksAdapter extends ArrayAdapter<Links.Link> {
    private Context context;
    private int layoutId;
    private List<Links.Link> links;

    public LinksAdapter(Context context2, int layoutId2, List<Links.Link> links2) {
        super(context2, 0, links2);
        this.context = context2;
        this.layoutId = layoutId2;
        this.links = links2;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            v = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(this.layoutId, (ViewGroup) null);
        }
        boolean isEven = position % 2 == 0;
        String websiteName = this.links.get(position).getWebsite();
        ImageView icon = (ImageView) v.findViewById(R.id.link_icon);
        icon.setImageResource(Links.getLinkImageResource(websiteName));
        icon.setTag(websiteName);
        TextView title = (TextView) v.findViewById(R.id.link_text);
        title.setText(websiteName);
        title.setTextColor(isEven ? getColor(R.color.CellEvenTextPrimary) : getColor(R.color.CellOddTextPrimary));
        ((ImageView) v.findViewById(R.id.list_item_background)).setImageResource(isEven ? R.drawable.background_row_even : R.drawable.background_row_odd);
        return v;
    }

    private int getColor(int colorResId) {
        return getContext().getResources().getColor(colorResId);
    }
}
