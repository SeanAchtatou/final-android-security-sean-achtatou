package com.reverbnation.artistapp.i9585.utils;

import android.net.Uri;
import com.reverbnation.artistapp.i9585.models.ShowList;

public abstract class MapHelper {
    private static final String GEO_URI_FORMAT = "geo:0,0?q=%1$s,%2$s";
    private static final String GEO_URI_QUERY_FORMAT = "geo:0,0?q=%1$s";
    private static final String GEO_URI_QUERY_WITH_LABEL_FORMAT = "geo:0,0?q=%1$s (%2$s)";
    private static final String GEO_URI_WITH_LABEL_FORMAT = "geo:0,0?q=%1$s,%2$s (%3$s)";

    public static Uri generateUri(float latitude, float longitude) throws NullPointerException {
        return Uri.parse(String.format(GEO_URI_FORMAT, Float.valueOf(latitude), Float.valueOf(longitude)));
    }

    public static Uri generateUri(float latitude, float longitude, String label) throws NullPointerException {
        if (label == null || label.equals("")) {
            return generateUri(latitude, longitude);
        }
        return Uri.parse(String.format(GEO_URI_WITH_LABEL_FORMAT, Float.valueOf(latitude), Float.valueOf(longitude), label));
    }

    public static Uri generateUri(ShowList.Show.Venue.Location location) {
        return Uri.parse(String.format(GEO_URI_QUERY_FORMAT, location.getMapQuery()));
    }

    public static Uri generateUri(ShowList.Show.Venue.Location location, String label) throws NullPointerException {
        if (label == null || label.equals("")) {
            return generateUri(location);
        }
        return Uri.parse(String.format(GEO_URI_QUERY_WITH_LABEL_FORMAT, location.getMapQuery(), label));
    }
}
