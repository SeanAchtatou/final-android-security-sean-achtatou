package com.reverbnation.artistapp.i9585.api.tasks;

import android.content.Context;
import android.os.AsyncTask;
import com.reverbnation.artistapp.i9585.api.ReverbNationApi;
import com.reverbnation.artistapp.i9585.models.ArtistData;
import com.roobit.restkit.RestClient;
import com.roobit.restkit.Result;
import java.io.IOException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

public class GetArtistDataApiTask extends AsyncTask<Object, Void, ArtistData> {
    GetArtistDataApiDelegate delegate;

    public interface GetArtistDataApiDelegate {
        void getArtistDataCancelled();

        void getArtistDataFinished(ArtistData artistData);

        void getArtistDataStarted();
    }

    public GetArtistDataApiTask(GetArtistDataApiDelegate delegate2) {
        this.delegate = delegate2;
    }

    /* access modifiers changed from: protected */
    public ArtistData doInBackground(Object... args) {
        Result result = RestClient.getClientWithBaseUrl(ReverbNationApi.getBaseUrl((String) args[1])).get(ReverbNationApi.getInstance().getRequiredHeaderParameters((Context) args[2])).setResource("artists/" + ((String) args[0]) + ".json?include=bio").synchronousExecute();
        if (!result.isSuccess()) {
            return null;
        }
        try {
            return (ArtistData) RestClient.getObjectMapper().readValue(result.getResponse(), ArtistData.class);
        } catch (JsonParseException e) {
            e.printStackTrace();
            return null;
        } catch (JsonMappingException e2) {
            e2.printStackTrace();
            return null;
        } catch (IOException e3) {
            e3.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        this.delegate.getArtistDataCancelled();
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(ArtistData result) {
        this.delegate.getArtistDataFinished(result);
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        this.delegate.getArtistDataStarted();
    }
}
