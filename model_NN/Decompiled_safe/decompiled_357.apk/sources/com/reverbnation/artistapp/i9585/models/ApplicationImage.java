package com.reverbnation.artistapp.i9585.models;

import com.urbanairship.analytics.EventDataManager;
import org.codehaus.jackson.annotate.JsonProperty;

public class ApplicationImage {
    public static final String Icon = "MobileApplicationImageIcon";
    @JsonProperty("id")
    private int id;
    @JsonProperty(EventDataManager.Events.COLUMN_NAME_TYPE)
    private String type;
    @JsonProperty("image")
    private String url;

    public int getId() {
        return this.id;
    }

    public String getType() {
        return this.type;
    }

    public String getUrl() {
        return this.url;
    }
}
