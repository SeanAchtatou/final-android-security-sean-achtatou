package com.reverbnation.artistapp.i9585.models;

import java.util.List;
import org.codehaus.jackson.annotate.JsonProperty;

public class ArtistData {
    private Bio bio;
    @JsonProperty("email")
    private String email;
    @JsonProperty("genres")
    private List<String> genres;
    @JsonProperty("id")
    private String id;
    @JsonProperty("image")
    private String imageURL;
    @JsonProperty("name")
    private String name;
    @JsonProperty("networks")
    private List<String> networks;

    public static class Bio {
        @JsonProperty("bio")
        private String bio;

        public void setBio(String bio2) {
            this.bio = bio2;
        }

        public String getBio() {
            return this.bio;
        }
    }

    public Bio getBio() {
        return this.bio;
    }

    public void setBio(Bio bio2) {
        this.bio = bio2;
    }

    public String getBandBio() {
        return getBio().getBio();
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getId() {
        return this.id;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getName() {
        return this.name;
    }

    public void setEmail(String email2) {
        this.email = email2;
    }

    public String getEmail() {
        return this.email;
    }

    public void setGenres(List<String> genres2) {
        this.genres = genres2;
    }

    public List<String> getGenres() {
        return this.genres;
    }

    public void setNetworks(List<String> networks2) {
        this.networks = networks2;
    }

    public List<String> getNetworks() {
        return this.networks;
    }

    public void setImageURL(String imageURL2) {
        this.imageURL = imageURL2;
    }

    public String getImageURL() {
        return this.imageURL;
    }
}
