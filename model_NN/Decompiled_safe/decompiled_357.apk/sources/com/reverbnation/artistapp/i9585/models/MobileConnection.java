package com.reverbnation.artistapp.i9585.models;

import com.google.common.base.Strings;
import com.reverbnation.artistapp.i9585.models.Server;
import org.codehaus.jackson.annotate.JsonProperty;

public class MobileConnection {
    @JsonProperty("error")
    private Error error;
    @JsonProperty("mobile_session")
    private MobileSession mobileSession;
    @JsonProperty("server")
    private Server server;

    public Error getError() {
        return this.error;
    }

    public Server getServer() {
        return this.server;
    }

    public MobileSession getMobileSession() {
        return this.mobileSession;
    }

    public MobileApplication getMobileApplication() {
        return getMobileSession().getMobileApplicationPlatformLink().getMobileApplicationPlatform().getMobileApplication();
    }

    public String getServerAvailability() {
        return getServer().getStatus().getAvailability();
    }

    public String getServerMessage() {
        return getServer().getStatus().getMessage();
    }

    public boolean hasShows() {
        if (getServer() == null || getServer().getContent() == null) {
            return false;
        }
        Server.Content content = getServer().getContent();
        return content.getShowsUpcoming() + content.getShowsPast() > 0;
    }

    public boolean hasUpcomingShows() {
        if (getServer() == null || getServer().getContent() == null) {
            return false;
        }
        return getServer().getContent().getShowsUpcoming() > 0;
    }

    public boolean isServerDown() {
        boolean isAvailable = false;
        if (hasServerStatus()) {
            isAvailable = "Up".equals(getServer().getStatus().getAvailability());
        }
        return !isAvailable;
    }

    public boolean isUpdateRequired() {
        if (hasServerStatus()) {
            return getServer().isUpdateRequired();
        }
        return false;
    }

    public int getSessionId() {
        return getMobileSession().getId();
    }

    public String getWidgetUrl() {
        return getServer().getUrls().getWidget();
    }

    private boolean hasServerStatus() {
        return (getServer() == null || getServer().getStatus() == null) ? false : true;
    }

    public boolean hasSystemMessage() {
        return !Strings.isNullOrEmpty(getServerMessage());
    }

    public boolean shouldClearCache() {
        return !getServer().getFeatures().isCached();
    }

    public String getVersionMinimum() {
        return getMobileSession().getMobileApplicationPlatformLink().getMobileApplicationPlatform().getVersionMinimum();
    }

    public int getMobileApplicationLink() {
        return getMobileSession().getMobileApplicationPlatformLink().getId();
    }

    public int getMobileDeviceId() {
        return getMobileSession().getMobileApplicationPlatformLink().getMobileDevice().getId();
    }
}
