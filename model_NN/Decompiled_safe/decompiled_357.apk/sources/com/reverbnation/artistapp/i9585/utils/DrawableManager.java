package com.reverbnation.artistapp.i9585.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.ImageView;
import com.google.common.base.Preconditions;
import com.google.common.collect.MapMaker;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.util.concurrent.ConcurrentMap;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class DrawableManager {
    protected static final int BIND_DRAWABLE_MSG = 1;
    protected static final int BIND_FROM_CACHE_MSG = 0;
    private static final String TAG = "DrawableManager";
    private static DrawableManager instance;
    private Context context;
    /* access modifiers changed from: private */
    public boolean isCachingAllowed = true;
    /* access modifiers changed from: private */
    public final ConcurrentMap<String, Drawable> map = new MapMaker().softValues().maximumSize(75).makeMap();

    public static DrawableManager getInstance(Context context2) {
        if (instance == null) {
            instance = new DrawableManager(context2.getApplicationContext());
        }
        return instance;
    }

    protected DrawableManager(Context context2) {
        this.context = context2;
    }

    public void clearCacheAndDisableCaching() {
        setCachingAllowed(false);
        clearMemoryCache();
        clearFileCache();
    }

    public void setCachingAllowed(boolean isCachingAllowed2) {
        this.isCachingAllowed = isCachingAllowed2;
    }

    public void clearMemoryCache() {
        this.map.clear();
    }

    public void clearFileCache() {
        for (File f : ExternalStorageHelper.getCacheDir(this.context).listFiles()) {
            f.delete();
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public Drawable getCachedDrawable(String key) throws FileNotFoundException {
        if (isMapped(key)) {
            return this.map.get(key);
        }
        if (isCached(key)) {
            return getDrawableFromCache(key);
        }
        throw new FileNotFoundException();
    }

    private Drawable getDrawableFromCache(String key) throws FileNotFoundException {
        return decodeBitmap(openImageFileByUrl(key).getAbsolutePath());
    }

    public void bindDrawable(String imageUrl, ImageView imageView) {
        if (isMapped(imageUrl)) {
            bindFromMap(imageUrl, imageView);
        } else if (isCached(imageUrl)) {
            bindFromCache(imageUrl, imageView);
        } else {
            fetchAndBind(imageUrl, imageView);
        }
    }

    public boolean isMapped(String url) {
        return this.map.containsKey(url);
    }

    private void bindFromMap(String imageUrl, ImageView imageView) {
        Log.v(TAG, "Binding image from map");
        setImage(imageView, getMappedDrawable(imageUrl));
    }

    private void setImage(ImageView imageView, Drawable drawable) {
        imageView.setImageDrawable(drawable);
    }

    public Drawable getMappedDrawable(String url) {
        return this.map.get(url);
    }

    private boolean isCached(String imageUrl) {
        try {
            return openImageFileByUrl(imageUrl).exists();
        } catch (FileNotFoundException e) {
            return false;
        }
    }

    public File openImageFileByUrl(String url) throws FileNotFoundException {
        return openImageFile(ExternalStorageHelper.UrlToFilename(url));
    }

    private File openImageFile(String filename) throws FileNotFoundException {
        Preconditions.checkState(filename != null);
        File dir = ExternalStorageHelper.getCacheDir(this.context);
        if (dir != null) {
            return new File(dir, filename);
        }
        throw new FileNotFoundException();
    }

    /* access modifiers changed from: private */
    public void bindFromCache(String imageUrl, ImageView imageView) {
        try {
            Drawable drawable = getDrawableFromCache(imageUrl);
            if (drawable != null) {
                Log.v(TAG, "Binding drawable from cache");
                imageView.setImageDrawable(drawable);
                if (this.isCachingAllowed) {
                    this.map.putIfAbsent(imageUrl, drawable);
                }
            }
        } catch (OutOfMemoryError e) {
            Log.e(TAG, "Failed to bind drawable from cache; out of memory error.");
        } catch (FileNotFoundException e2) {
            Log.e(TAG, "Failed to bind drawable from cache; file note found.");
        }
    }

    private Drawable decodeBitmap(String filePath) {
        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeFile(filePath);
        } catch (OutOfMemoryError e) {
            Log.w(TAG, "Out of memory while decoding bitmap file");
        }
        if (bitmap != null) {
            return new BitmapDrawable(this.context.getResources(), bitmap);
        }
        return null;
    }

    private void fetchAndBind(final String imageUrl, final ImageView imageView) {
        final Handler handler = new Handler() {
            public void handleMessage(Message message) {
                Drawable drawable;
                if (message.what == 0) {
                    Log.v(DrawableManager.TAG, "Handler is binding from cache: " + imageUrl);
                    DrawableManager.this.bindFromCache(imageUrl, imageView);
                } else if (message.what == 1 && (drawable = (Drawable) message.obj) != null) {
                    Log.v(DrawableManager.TAG, "Handler is binding drawable");
                    imageView.setImageDrawable(drawable);
                    if (DrawableManager.this.isCachingAllowed) {
                        DrawableManager.this.map.putIfAbsent(imageUrl, drawable);
                    }
                }
            }
        };
        new Thread() {
            public void run() {
                if (DrawableManager.this.isCachingAllowed) {
                    DrawableManager.this.fetchAndCache(imageUrl);
                    handler.sendEmptyMessage(0);
                    return;
                }
                try {
                    InputStream is = DrawableManager.this.fetch(imageUrl);
                    handler.sendMessage(handler.obtainMessage(1, DrawableManager.this.decodeBitmap(is)));
                    is.close();
                } catch (MalformedURLException e) {
                    Log.e(DrawableManager.TAG, "Fetch and bind failed because of malformed url: " + imageUrl);
                } catch (IOException e2) {
                    Log.e(DrawableManager.TAG, "Fetch and bind failed: " + e2);
                }
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public Drawable decodeBitmap(InputStream is) {
        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeStream(is);
        } catch (OutOfMemoryError e) {
            Log.w(TAG, "Out of memory while decoding bitmap stream");
        }
        if (bitmap != null) {
            return new BitmapDrawable(this.context.getResources(), bitmap);
        }
        return null;
    }

    public void pleaseCacheDrawable(String urlString) {
        if (!isCached(urlString)) {
            fetchAndCache(urlString);
        }
        if (!isMapped(urlString)) {
            try {
                this.map.putIfAbsent(urlString, getCachedDrawable(urlString));
            } catch (FileNotFoundException e) {
                Log.e(TAG, "Courtesy cache failed; file not found");
            } catch (NullPointerException e2) {
                Log.e(TAG, "Courtesy cache failed; null pointer");
            }
        }
    }

    /* access modifiers changed from: protected */
    public void fetchAndCache(String urlString) {
        try {
            writeToExternalStorage(fetch(urlString), ExternalStorageHelper.UrlToFilename(urlString));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e2) {
        }
    }

    /* access modifiers changed from: protected */
    public InputStream fetch(String urlString) throws MalformedURLException, IOException {
        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpGet request = new HttpGet(urlString);
        Log.v(TAG, "Fetching drawable: " + urlString);
        HttpResponse response = httpClient.execute(request);
        int statusCode = response.getStatusLine().getStatusCode();
        String reason = response.getStatusLine().getReasonPhrase();
        if (statusCode <= 299) {
            return response.getEntity().getContent();
        }
        Log.e(TAG, "Fetch failed with " + statusCode + " : " + reason);
        throw new HttpResponseException(statusCode, reason);
    }

    private void writeToExternalStorage(InputStream is, String filename) throws IOException {
        createFileIfNonexistent(filename);
        try {
            OutputStream os = new FileOutputStream(openImageFile(filename));
            try {
                byte[] buffer = new byte[4096];
                while (true) {
                    int bytesRead = is.read(buffer, 0, buffer.length);
                    if (bytesRead < 0) {
                        break;
                    }
                    os.write(buffer, 0, bytesRead);
                }
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
                os.close();
            } catch (Throwable th) {
                os.close();
                throw th;
            }
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
        } finally {
            is.close();
        }
    }

    private void createFileIfNonexistent(String filename) {
        try {
            if (openImageFile(filename).exists()) {
                return;
            }
        } catch (FileNotFoundException e) {
        }
        createFile(filename);
    }

    private void createFile(String filename) {
        try {
            openImageFile(filename).createNewFile();
        } catch (IOException e) {
            Log.e(TAG, "File creation failed: " + e);
        }
    }
}
