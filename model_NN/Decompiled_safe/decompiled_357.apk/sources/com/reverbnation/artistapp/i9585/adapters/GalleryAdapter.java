package com.reverbnation.artistapp.i9585.adapters;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.models.Photoset;
import com.reverbnation.artistapp.i9585.utils.DrawableManager;
import java.io.FileNotFoundException;
import java.util.List;

public class GalleryAdapter extends ArrayAdapter<Photoset.Photo> {
    private static final String TAG = "GalleryAdapter";
    /* access modifiers changed from: private */
    public DrawableManager drawableManager;
    private ImageView.ScaleType scaleType;

    public GalleryAdapter(Context context, List<Photoset.Photo> photos, ImageView.ScaleType scaleType2) {
        super(context, 0, photos);
        this.scaleType = scaleType2;
        this.drawableManager = DrawableManager.getInstance(context);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView v = (ImageView) convertView;
        if (v == null) {
            v = new ImageView(getContext());
            v.setLayoutParams(new Gallery.LayoutParams(-1, -1));
            v.setBackgroundResource(R.color.photo_gallery_background);
        }
        Photoset.Photo photo = (Photoset.Photo) getItem(position);
        v.setScaleType(this.scaleType);
        v.setImageResource(R.color.photo_gallery_background);
        try {
            v.setImageDrawable(this.drawableManager.getCachedDrawable(photo.getImageUrl()));
            Log.v(TAG, "Gallery found a cached full size photo");
        } catch (FileNotFoundException e) {
            try {
                v.setImageDrawable(this.drawableManager.getCachedDrawable(photo.getThumbnailUrl()));
                Log.v(TAG, "Gallery found a cached thumbnail");
                requestFullsizePhotoCache(photo);
            } catch (FileNotFoundException e2) {
                this.drawableManager.bindDrawable(photo.getImageUrl(), v);
            }
        }
        return v;
    }

    private void requestFullsizePhotoCache(final Photoset.Photo photo) {
        new Thread(new Runnable() {
            public void run() {
                GalleryAdapter.this.drawableManager.pleaseCacheDrawable(photo.getImageUrl());
            }
        }).start();
    }
}
