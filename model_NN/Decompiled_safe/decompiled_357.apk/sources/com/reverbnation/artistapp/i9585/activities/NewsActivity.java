package com.reverbnation.artistapp.i9585.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;
import com.reverbnation.artistapp.i9585.R;

public class NewsActivity extends BaseTabActivity {
    private static final String BlogTab = "blog_tab";
    private static final String TwitterTab = "twitter_tab";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(7);
        setContentView((int) R.layout.news_activity);
        getActivityHelper().setArtistTitlebar(getString(R.string.news));
        setupTabs();
    }

    private void setupTabs() {
        TabHost tabHost = getTabHost();
        tabHost.addTab(tabHost.newTabSpec(BlogTab).setIndicator(createTabView(R.string.blog)).setContent(setupBlogActivityIntent()));
        tabHost.addTab(tabHost.newTabSpec(TwitterTab).setIndicator(createTabView(R.string.twitter)).setContent(new Intent(this, TwitterActivity.class)));
    }

    private Intent setupBlogActivityIntent() {
        Intent intent = new Intent(this, BlogActivity.class);
        intent.putExtra("shouldGetBlogs", getIntent().getBooleanExtra("shouldGetBlogs", true));
        intent.putExtra("shouldGetTweets", getIntent().getBooleanExtra("shouldGetTweets", true));
        return intent;
    }
}
