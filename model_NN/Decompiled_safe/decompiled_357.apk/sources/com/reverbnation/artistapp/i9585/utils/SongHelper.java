package com.reverbnation.artistapp.i9585.utils;

import android.net.Uri;
import android.util.Log;
import com.google.common.base.Ascii;
import com.reverbnation.artistapp.i9585.api.ReverbNationApi;
import com.reverbnation.artistapp.i9585.models.FileToken;
import com.reverbnation.artistapp.i9585.models.SongList;
import java.io.FileDescriptor;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;

public class SongHelper {
    private static final String HEX = "0123456789abcdef";
    private static final String TAG = "SongHelper";
    private String seed = "yoov5id9lyg6jish7git";

    public interface FetchCompleteListener {
        void onFetchComplete();
    }

    public interface OnSongReadyListener {
        void onSongReady(FileDescriptor fileDescriptor);
    }

    public String createSongUrl(SongList.Song song, FileToken token) {
        Log.v(TAG, "Creating song url wth token");
        return Uri.parse(song.getUrl()).buildUpon().appendQueryParameter("tk", token.timeToken).appendQueryParameter("rk", token.randomToken).appendQueryParameter("pk", ReverbNationApi.getPublicKey()).appendQueryParameter("bpm", hash(song.getId(), token.timeToken, token.randomToken, ReverbNationApi.getPrivateKey())).build().toString();
    }

    private String hash(int songId, String timeToken, String randomToken, String privateKey) {
        return md5(songId + '.' + timeToken + '.' + randomToken + '.' + privateKey);
    }

    private String md5(String s) {
        String md5String = "";
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            md5String = toHex(digest.digest());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "Md5[" + md5String.length() + "] " + md5String);
        return md5String;
    }

    /* access modifiers changed from: protected */
    public String encrypt(String plainText) {
        try {
            return xxcrypt(plainText, true);
        } catch (Exception e) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public String decrypt(String encryptedText) {
        try {
            return xxcrypt(encryptedText, false);
        } catch (Exception e) {
            return null;
        }
    }

    private String xxcrypt(String text, boolean encrypt) throws Exception {
        KeyGenerator keygen = KeyGenerator.getInstance("AES");
        SecureRandom secrand = SecureRandom.getInstance("SHA1PRNG");
        secrand.setSeed(this.seed.getBytes());
        keygen.init(128, secrand);
        SecretKeySpec skeySpec = new SecretKeySpec(keygen.generateKey().getEncoded(), "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(encrypt ? 1 : 2, skeySpec);
        byte[] xxcrypted = cipher.doFinal(encrypt ? text.getBytes() : toByte(text));
        if (encrypt) {
            return toHex(xxcrypted);
        }
        return new String(xxcrypted);
    }

    private static byte[] toByte(String hexString) {
        int len = hexString.length() / 2;
        byte[] result = new byte[len];
        for (int i = 0; i < len; i++) {
            result[i] = Integer.valueOf(hexString.substring(i * 2, (i * 2) + 2), 16).byteValue();
        }
        return result;
    }

    private String toHex(byte[] buf) {
        if (buf == null) {
            return "";
        }
        StringBuffer result = new StringBuffer(buf.length * 2);
        for (byte appendHex : buf) {
            appendHex(result, appendHex);
        }
        return result.toString();
    }

    private void appendHex(StringBuffer sb, byte b) {
        sb.append(HEX.charAt((b >> 4) & 15)).append(HEX.charAt(b & Ascii.SI));
    }
}
