package com.reverbnation.artistapp.i9585;

import android.app.AlertDialog;
import android.app.Application;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Log;
import com.google.common.base.Strings;
import com.reverbnation.artistapp.i9585.api.FacebookApi;
import com.reverbnation.artistapp.i9585.api.ReverbNationApi;
import com.reverbnation.artistapp.i9585.api.tasks.PatchMobileDevicesApiTask;
import com.reverbnation.artistapp.i9585.authorization.OAuthHelper;
import com.reverbnation.artistapp.i9585.models.ArtistData;
import com.reverbnation.artistapp.i9585.models.MobileApplication;
import com.reverbnation.artistapp.i9585.models.MobileConnection;
import com.reverbnation.artistapp.i9585.models.Photoset;
import com.reverbnation.artistapp.i9585.models.ShowList;
import com.reverbnation.artistapp.i9585.models.SongList;
import com.reverbnation.artistapp.i9585.receivers.IntentReceiver;
import com.reverbnation.artistapp.i9585.utils.LinkShortener;
import com.reverbnation.artistapp.i9585.utils.StrictModeWrapper;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.push.PushManager;
import java.util.List;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

public class ReverbNationApplication extends Application {
    private static final String APP_PREFS_NAME = "ReverbNationPrefs";
    private static final int DEBUG_SIGNATURE_HASH = -1212404872;
    private static final String TAG = "ReverbNationApp";
    private static ReverbNationApplication instance;
    private ArtistData artistData;
    private RequestToken currentRequestToken;
    private String deferredPushKey;
    private boolean hasMobileApplicationBeenSet = false;
    private Boolean isDebugBuild;
    private LinkShortener linkShortener;
    private MobileConnection mobileConnection;
    private OAuthHelper oAuthHelper;
    private ShowList pastShowList;
    private Photoset photoset;
    private List<SongList.Song> playlist;
    private Twitter twitter;
    private ShowList upcomingShowList;

    public static ReverbNationApplication getInstance() {
        return instance;
    }

    public void onCreate() {
        super.onCreate();
        instance = this;
        initializeInstance();
    }

    private void initializeInstance() {
        this.twitter = new TwitterFactory().getInstance();
        this.oAuthHelper = new OAuthHelper(this);
        this.oAuthHelper.configureOAuth(this.twitter);
        this.linkShortener = new LinkShortener(this);
        UAirship.takeOff(this);
        Logger.logLevel = 2;
        PushManager.enablePush();
        PushManager.shared().setIntentReceiver(IntentReceiver.class);
        try {
            StrictModeWrapper.init(this);
        } catch (Throwable th) {
            Log.v("StrictMode", "... is not available. Punting...");
        }
    }

    public SharedPreferences getApplicationPreferences() {
        return getSharedPreferences(APP_PREFS_NAME, 0);
    }

    public void setMobileConnection(MobileConnection mobileConnection2) {
        this.mobileConnection = mobileConnection2;
        ReverbNationApi.setSessionId(mobileConnection2.getSessionId());
        FacebookApi.setWidgetUrl(mobileConnection2.getWidgetUrl());
        setMobileApplication(mobileConnection2.getMobileApplication());
        if (!Strings.isNullOrEmpty(this.deferredPushKey)) {
            onReceivedPushKey(this.deferredPushKey);
        }
    }

    public boolean hasMobileConnection() {
        return this.mobileConnection != null;
    }

    public void setMobileApplication(MobileApplication mobileApplication) {
        ReverbNationApi.getInstance().setMobileApplication(mobileApplication);
        this.hasMobileApplicationBeenSet = true;
    }

    public MobileApplication getMobileApplication() {
        return ReverbNationApi.getInstance().getMobileApplication();
    }

    public boolean hasMobileApplication() {
        return this.hasMobileApplicationBeenSet;
    }

    public boolean artistHasShows() {
        if (this.mobileConnection == null) {
            return false;
        }
        return this.mobileConnection.hasShows();
    }

    public boolean artistHasUpcomingShows() {
        if (this.mobileConnection == null) {
            return false;
        }
        return this.mobileConnection.hasUpcomingShows();
    }

    public void setUpcomingShowList(ShowList upcomingShowList2) {
        this.upcomingShowList = upcomingShowList2;
    }

    public ShowList getUpcomingShowList() {
        return this.upcomingShowList;
    }

    public void setPastShowList(ShowList pastShowList2) {
        this.pastShowList = pastShowList2;
    }

    public ShowList getPastShowList() {
        return this.pastShowList;
    }

    public void setArtistData(ArtistData artistData2) {
        this.artistData = artistData2;
    }

    public ArtistData getArtistData() {
        return this.artistData;
    }

    public Twitter getTwitter() {
        return this.twitter;
    }

    public boolean isTwitterAuthorized() {
        return this.oAuthHelper.hasAccessToken();
    }

    public void twitterLogout() {
        this.oAuthHelper.deleteAccessToken();
        this.oAuthHelper.setAccessToken(null);
        this.twitter.setOAuthAccessToken(null);
    }

    public String getArtistTwitterName() {
        return getMobileApplication().getTwitterUsername();
    }

    public RequestToken getRequestToken() {
        if (this.currentRequestToken == null) {
            try {
                this.currentRequestToken = this.twitter.getOAuthRequestToken();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return this.currentRequestToken;
    }

    public void clearAccessToken() {
        this.oAuthHelper.setAccessToken(null);
    }

    public void setAccessToken(AccessToken accessToken) {
        this.oAuthHelper.storeAccessToken(accessToken);
    }

    public AccessToken getAccessToken() {
        return this.oAuthHelper.loadAccessToken();
    }

    public void shorten(String[] links, LinkShortener.ShortenListener shortenListener) {
        this.linkShortener.shorten(links, shortenListener);
    }

    public String getMarketplaceLink() {
        return "http://market.android.com/details?id=" + getPackageName();
    }

    public Boolean isDebugBuild(Context context) {
        if (this.isDebugBuild == null) {
            try {
                this.isDebugBuild = false;
                Signature[] sigs = context.getPackageManager().getPackageInfo(context.getPackageName(), 64).signatures;
                int i = 0;
                while (true) {
                    if (i >= sigs.length) {
                        break;
                    } else if (sigs[i].hashCode() == DEBUG_SIGNATURE_HASH) {
                        Log.d(TAG, "This is a debug build!");
                        this.isDebugBuild = true;
                        break;
                    } else {
                        i++;
                    }
                }
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
        return this.isDebugBuild;
    }

    public void setPhotoset(Photoset photoset2) {
        this.photoset = photoset2;
    }

    public Photoset getPhotoset() {
        return this.photoset;
    }

    public Dialog createProgressDialog(Context context, int titleResourceId, int msgResourceId) {
        ProgressDialog dialog = new ProgressDialog(context);
        dialog.setIndeterminate(true);
        if (titleResourceId != 0) {
            dialog.setTitle(titleResourceId);
        }
        dialog.setMessage(context.getString(msgResourceId));
        return dialog;
    }

    public Dialog createDialog(Context context, int titleResourceId, int msgResourceId) {
        return new AlertDialog.Builder(context).setTitle(titleResourceId).setMessage(msgResourceId).setNeutralButton(17039370, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).create();
    }

    public void onReceivedPushKey(String pushKey) {
        if (hasMobileApplication()) {
            this.deferredPushKey = "";
            new PatchMobileDevicesApiTask().execute(pushKey, Integer.valueOf(this.mobileConnection.getMobileDeviceId()));
            return;
        }
        this.deferredPushKey = pushKey;
    }

    public int getMobileApplicationLink() {
        return this.mobileConnection.getMobileApplicationLink();
    }

    public String getArtistId() {
        return String.valueOf(getMobileApplication().getPageObject().getId());
    }

    public String getAppStoreUrl() {
        return this.mobileConnection.getMobileSession().getMobileApplicationPlatformLink().getMobileApplicationPlatform().getStoreUrl();
    }

    public String getMarketingUrl() {
        return getMobileApplication().getUrl();
    }

    public List<SongList.Song> getPlaylist() {
        return this.playlist;
    }

    public void setPlaylist(List<SongList.Song> playlist2) {
        this.playlist = playlist2;
    }
}
