package com.reverbnation.artistapp.i9585.utils;

import android.view.View;

public interface MusicPlayerRemoteControl {
    void onNextControlClicked(View view);

    void onPlayControlClicked(View view);

    void onPreviousControlClicked(View view);
}
