package com.reverbnation.artistapp.i9585.activities;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.reverbnation.artistapp.i9585.R;
import com.reverbnation.artistapp.i9585.api.FacebookApi;
import com.reverbnation.artistapp.i9585.api.tasks.FacebookWallPostApiTask;
import com.reverbnation.artistapp.i9585.models.FacebookResponse;
import com.reverbnation.artistapp.i9585.models.ShowList;
import com.reverbnation.artistapp.i9585.models.interfaces.FacebookShareable;
import com.reverbnation.artistapp.i9585.utils.ActivityHelper;
import com.reverbnation.artistapp.i9585.utils.AnalyticsHelper;
import com.reverbnation.artistapp.i9585.utils.LinkShortener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import twitter4j.TwitterException;

public class ShowDetailsActivity extends BaseActivity {
    private static final int SHARE_FB_FAILURE_DIALOG = 2;
    private static final int SHARE_FB_PROGRESS_DIALOG = 0;
    private static final int SHARE_FB_SUCCESS_DIALOG = 1;
    private static final int SHARE_TWITTER_FAILED_DIALOG = 5;
    private static final int SHARE_TWITTER_PROGRESS_DIALOG = 3;
    private static final int SHARE_TWITTER_SUCCESS_DIALOG = 4;
    private final String TAG = "ShowDetailsActivity";
    private TextView addressField;
    private TextView addressLabel;
    private Button directionsButton;
    private TextView locationField;
    private TextView miscShowDetailsField;
    /* access modifiers changed from: private */
    public ShowList.Show show;
    private TextView showtimeField;
    private TextView showtimeLabel;
    private Button ticketsButton;
    private TextView venueNameField;
    private TextView venueNameLabel;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(7);
        setContentView((int) R.layout.show_details_activity);
        getActivityHelper().setArtistTitlebar((int) R.string.show_details);
        this.show = (ShowList.Show) getIntent().getExtras().get("show");
        setupViews();
    }

    private void setupViews() {
        this.locationField = (TextView) findViewById(R.id.show_details_location);
        this.locationField.setText(getShowLocation());
        this.directionsButton = (Button) findViewById(R.id.show_details_directions_button);
        this.directionsButton.setEnabled(hasVenueAndLocation(this.show));
        this.directionsButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShowDetailsActivity.this.directionsButtonClicked();
            }
        });
        this.ticketsButton = (Button) findViewById(R.id.show_details_tickets_button);
        this.ticketsButton.setEnabled(this.show.hasTicketURL());
        this.ticketsButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShowDetailsActivity.this.ticketsButtonClicked();
            }
        });
        this.venueNameLabel = (TextView) findViewById(R.id.show_details_venue_field_label);
        this.venueNameLabel.setText(getVenueNameLabel());
        this.venueNameField = (TextView) findViewById(R.id.show_details_venue_field_text);
        this.venueNameField.setText(getVenueName());
        this.showtimeLabel = (TextView) findViewById(R.id.show_details_time_field_label);
        this.showtimeLabel.setText(getShowtimeLabel());
        this.showtimeField = (TextView) findViewById(R.id.show_details_time_field_text);
        this.showtimeField.setText(getShowtimeField());
        this.addressLabel = (TextView) findViewById(R.id.show_details_address_field_label);
        this.addressLabel.setText(getVenueAddressLabel());
        this.addressField = (TextView) findViewById(R.id.show_details_address_field_text);
        this.addressField.setText(getVenueAddress());
        this.miscShowDetailsField = (TextView) findViewById(R.id.show_details_misc_text);
        this.miscShowDetailsField.setText(getMiscShowDetails());
    }

    private boolean hasVenueAndLocation(ShowList.Show show2) {
        return show2.hasVenue() && show2.getVenue().hasLocation();
    }

    private CharSequence getShowLocation() {
        if (this.show.hasVenue()) {
            if (this.show.hasCity() && this.show.hasState()) {
                return String.format(getString(R.string.show_location_format), this.show.getCity(), this.show.getState());
            } else if (this.show.hasCity()) {
                return this.show.getCity();
            } else {
                if (this.show.hasState()) {
                    return this.show.getState();
                }
            }
        }
        return "";
    }

    private CharSequence getVenueNameLabel() {
        if (!this.show.hasVenue() || !this.show.getVenue().hasName()) {
            return "";
        }
        return getString(R.string.venue_field_label);
    }

    private CharSequence getVenueName() {
        if (!this.show.hasVenue() || !this.show.getVenue().hasName()) {
            return "";
        }
        return this.show.getVenue().getName();
    }

    private CharSequence getShowtimeLabel() {
        if (this.show.hasShowTime()) {
            return getString(R.string.time_field_label);
        }
        return "";
    }

    private CharSequence getShowtimeField() {
        SimpleDateFormat showTimeFormat;
        if (!this.show.hasShowTimeDate()) {
            return "";
        }
        Date showTime = this.show.getShowTimeDate();
        if (this.show.isThisYear()) {
            showTimeFormat = new SimpleDateFormat(getString(R.string.showtime_format));
        } else {
            showTimeFormat = new SimpleDateFormat(getString(R.string.showtime_with_year_format));
        }
        return showTimeFormat.format(showTime);
    }

    private CharSequence getVenueAddressLabel() {
        if (!this.show.hasVenue() || !this.show.getVenue().hasLocation() || !this.show.getVenue().getLocation().hasAddress()) {
            return "";
        }
        return getString(R.string.address_field_label);
    }

    private CharSequence getVenueAddress() {
        if (!this.show.hasVenue() || !this.show.getVenue().hasLocation() || !this.show.getVenue().getLocation().hasAddress()) {
            return "";
        }
        return this.show.getVenue().getLocation().getAddress();
    }

    private CharSequence getMiscShowDetails() {
        if (this.show.hasDetails()) {
            return this.show.getDetails();
        }
        return "";
    }

    /* access modifiers changed from: protected */
    public void directionsButtonClicked() {
        AnalyticsHelper.getInstance(this).trackPageView("homescreen/shows/show_details/directions");
        startActivity(new Intent("android.intent.action.VIEW", this.show.getVenue().getLocation().generateUri(this.show.getVenue().getName())));
    }

    /* access modifiers changed from: protected */
    public void ticketsButtonClicked() {
        AnalyticsHelper.getInstance(this).trackPageView("homescreen/shows/show_details/tickets");
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(this.show.getTicketURL().trim()));
        intent.setComponent(new ComponentName("com.android.browser", "com.android.browser.BrowserActivity"));
        startActivity(intent);
    }

    public void onShareTwitterClick(View v) {
        AnalyticsHelper.getInstance(this).trackEvent("sharing", "twitter", "show");
        if (!getActivityHelper().isTwitterAuthorized()) {
            getActivityHelper().getTwitterHelper().beginTwitterAuthentication();
        } else {
            shareOnTwitter();
        }
    }

    private void shareOnTwitter() {
        showDialog(3);
        final ActivityHelper helper = getActivityHelper();
        helper.getApplication().shorten(new String[]{this.show.getShowLink(helper.getArtistId()), helper.getApplication().getMarketplaceLink()}, new LinkShortener.ShortenListener() {
            public void onLinkReady(List<String> shortened) {
                final String tweet = helper.getTwitterHelper().getShowShareText(ShowDetailsActivity.this.show, shortened.get(0), shortened.get(1));
                new AsyncTask<Void, Void, Boolean>() {
                    /* access modifiers changed from: protected */
                    public Boolean doInBackground(Void... arg0) {
                        try {
                            helper.getTwitter().updateStatus(tweet);
                            return true;
                        } catch (TwitterException e) {
                            Log.v("ShowDetailsActivity", "Failed to share video on twitter" + e);
                            return false;
                        }
                    }

                    /* access modifiers changed from: protected */
                    public void onPostExecute(Boolean result) {
                        ShowDetailsActivity.this.getActivityHelper().dismissDialog(3);
                        ShowDetailsActivity.this.showDialog(result.booleanValue() ? 4 : 5);
                    }
                }.execute(new Void[0]);
            }
        });
    }

    public void onShareEmailClick(View v) {
        AnalyticsHelper.getInstance(this).trackEvent("sharing", "email", "show");
        getActivityHelper().getEmailHelper().startShareActivity(this, getMailSubject(), getMailBody());
    }

    private Spanned getMailBody() {
        return getActivityHelper().getEmailHelper().getShareBody(this, this.show.getShowLink(getActivityHelper().getArtistId()), getString(R.string.show_details));
    }

    private String getMailSubject() {
        SimpleDateFormat showDateFormat = new SimpleDateFormat("MMM d");
        String dateVerb = "";
        String dateString = "";
        if (this.show.hasShowTimeDate()) {
            dateVerb = "on ";
            dateString = showDateFormat.format(this.show.getShowTimeDate());
        }
        String venueVerb = "";
        String venueName = "";
        if (this.show.getVenue() != null) {
            venueVerb = "at ";
            venueName = this.show.getVenue().getName();
        }
        return String.format(getString(R.string.show_share_email_subject, new Object[]{getString(R.string.artist_name), dateVerb, dateString, venueVerb, venueName}), new Object[0]);
    }

    public void onShareFacebookClick(View v) {
        Facebook facebook = FacebookApi.getInstance(this);
        if (FacebookApi.hasAccessToken()) {
            shareOnFacebook();
            return;
        }
        AnalyticsHelper.getInstance(this).trackPageView("social_facebook_login");
        facebook.authorize(this, FacebookApi.getRequiredPermissions(), new Facebook.DialogListener() {
            public void onComplete(Bundle values) {
                FacebookApi.setAccessToken(values.getString(Facebook.TOKEN));
                ShowDetailsActivity.this.shareOnFacebook();
            }

            public void onFacebookError(FacebookError e) {
            }

            public void onError(DialogError e) {
            }

            public void onCancel() {
            }
        });
    }

    /* access modifiers changed from: private */
    public void shareOnFacebook() {
        showDialog(0);
        AnalyticsHelper.getInstance(this).trackEvent("sharing", "facebook", "show");
        try {
            new FacebookWallPostApiTask(this) {
                /* access modifiers changed from: protected */
                public void onPostExecute(FacebookResponse response) {
                    ShowDetailsActivity.this.getActivityHelper().dismissDialog(0);
                    if (response.isSuccessful()) {
                        ShowDetailsActivity.this.showDialog(1);
                    } else {
                        ShowDetailsActivity.this.showDialog(2);
                    }
                }
            }.execute(new FacebookShareable[]{this.show});
        } catch (IllegalStateException e) {
            Log.e("ShowDetailsActivity", e.toString());
            showDialog(2);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        FacebookApi.getInstance(this).authorizeCallback(requestCode, resultCode, data);
        if (requestCode != 100) {
            return;
        }
        if (resultCode == -1) {
            shareOnTwitter();
        } else {
            getActivityHelper().getTwitterHelper().showTwitterAuthenticationError(data);
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        Dialog dialog = null;
        switch (id) {
            case 0:
                dialog = getActivityHelper().createProgressDialog(R.string.share_show, R.string.sharing_show_on_facebook);
                break;
            case 1:
            case 4:
                dialog = getActivityHelper().createDialog((int) R.string.share_show, (int) R.string.show_was_shared);
                break;
            case 2:
                dialog = getActivityHelper().createDialog((int) R.string.share_show, (int) R.string.sharing_show_to_facebook_failed);
                break;
            case 3:
                dialog = getActivityHelper().createProgressDialog(R.string.share_show, R.string.sharing_show_on_twitter);
                break;
            case 5:
                dialog = getActivityHelper().createDialog((int) R.string.share_show, (int) R.string.sharing_show_to_twitter_failed);
                break;
        }
        if (dialog != null) {
            return dialog;
        }
        return super.onCreateDialog(id);
    }
}
