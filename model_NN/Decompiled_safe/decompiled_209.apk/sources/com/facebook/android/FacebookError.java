package com.facebook.android;

public class FacebookError extends Throwable {
    private int a = 0;

    /* renamed from: a  reason: collision with other field name */
    private String f6a;

    public FacebookError(String str) {
        super(str);
    }

    public FacebookError(String str, String str2, int i) {
        super(str);
        this.f6a = str2;
        this.a = i;
    }

    public int getErrorCode() {
        return this.a;
    }

    public String getErrorType() {
        return this.f6a;
    }
}
