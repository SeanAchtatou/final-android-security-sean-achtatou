package com.facebook.android;

import android.os.Bundle;
import android.widget.Toast;
import com.facebook.android.Facebook;

class g implements Facebook.DialogListener {
    final /* synthetic */ ShareOnFacebook a;

    private g(ShareOnFacebook shareOnFacebook) {
        this.a = shareOnFacebook;
    }

    public void onCancel() {
        Toast.makeText(ShareOnFacebook.a(this.a), "network error", 1).show();
    }

    public void onComplete(Bundle bundle) {
        Bundle bundle2 = new Bundle();
        bundle2.putString("message", "I just got " + ShareOnFacebook.a(this.a) + " in Torus 3D. It's an amazing game. Come on and challenge my best record. https://market.android.com/details?id=com.reactor.game.torus");
        ShareOnFacebook.a(this.a).dialog(ShareOnFacebook.a(this.a), "feed", bundle2, new h(this.a));
    }

    public void onError(DialogError dialogError) {
    }

    public void onFacebookError(FacebookError facebookError) {
    }
}
