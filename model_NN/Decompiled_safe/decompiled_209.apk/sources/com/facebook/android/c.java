package com.facebook.android;

import android.os.Bundle;
import android.webkit.CookieSyncManager;
import com.facebook.android.Facebook;

class c implements Facebook.DialogListener {
    final /* synthetic */ Facebook a;

    c(Facebook facebook) {
        this.a = facebook;
    }

    public void onCancel() {
        this.a.f4a.onCancel();
    }

    public void onComplete(Bundle bundle) {
        CookieSyncManager.getInstance().sync();
        this.a.setAccessToken(bundle.getString(Facebook.TOKEN));
        this.a.setAccessExpiresIn(bundle.getString(Facebook.EXPIRES));
        if (this.a.isSessionValid()) {
            this.a.f4a.onComplete(bundle);
        } else {
            this.a.f4a.onFacebookError(new FacebookError("Failed to receive access token."));
        }
    }

    public void onError(DialogError dialogError) {
        this.a.f4a.onError(dialogError);
    }

    public void onFacebookError(FacebookError facebookError) {
        this.a.f4a.onFacebookError(facebookError);
    }
}
