package com.facebook.android;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.Signature;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.CookieSyncManager;

public class Facebook {
    public static final String CANCEL_URI = "fbconnect://cancel";
    public static final String EXPIRES = "expires_in";
    public static final String FB_APP_SIGNATURE = "30820268308201d102044a9c4610300d06092a864886f70d0101040500307a310b3009060355040613025553310b3009060355040813024341311230100603550407130950616c6f20416c746f31183016060355040a130f46616365626f6f6b204d6f62696c653111300f060355040b130846616365626f6f6b311d301b0603550403131446616365626f6f6b20436f72706f726174696f6e3020170d3039303833313231353231365a180f32303530303932353231353231365a307a310b3009060355040613025553310b3009060355040813024341311230100603550407130950616c6f20416c746f31183016060355040a130f46616365626f6f6b204d6f62696c653111300f060355040b130846616365626f6f6b311d301b0603550403131446616365626f6f6b20436f72706f726174696f6e30819f300d06092a864886f70d010101050003818d0030818902818100c207d51df8eb8c97d93ba0c8c1002c928fab00dc1b42fca5e66e99cc3023ed2d214d822bc59e8e35ddcf5f44c7ae8ade50d7e0c434f500e6c131f4a2834f987fc46406115de2018ebbb0d5a3c261bd97581ccfef76afc7135a6d59e8855ecd7eacc8f8737e794c60a761c536b72b11fac8e603f5da1a2d54aa103b8a13c0dbc10203010001300d06092a864886f70d0101040500038181005ee9be8bcbb250648d3b741290a82a1c9dc2e76a0af2f2228f1d9f9c4007529c446a70175c5a900d5141812866db46be6559e2141616483998211f4a673149fb2232a10d247663b26a9031e15f84bc1c74d141ff98a02d76f85b2c8ab2571b6469b232d8e768a7f7ca04f7abe4a775615916c07940656b58717457b42bd928a2";
    public static final int FORCE_DIALOG_AUTH = -1;
    public static final String REDIRECT_URI = "fbconnect://success";
    public static final String SINGLE_SIGN_ON_DISABLED = "service_disabled";
    public static final String TOKEN = "access_token";
    protected static String a = "https://m.facebook.com/dialog/";
    protected static String b = "https://graph.facebook.com/";
    protected static String c = "https://api.facebook.com/restserver.php";

    /* renamed from: a  reason: collision with other field name */
    private int f1a;

    /* renamed from: a  reason: collision with other field name */
    private long f2a = 0;

    /* renamed from: a  reason: collision with other field name */
    private Activity f3a;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with other field name */
    public DialogListener f4a;

    /* renamed from: a  reason: collision with other field name */
    private String[] f5a;
    private String d = null;
    private String e;

    public interface DialogListener {
        void onCancel();

        void onComplete(Bundle bundle);

        void onError(DialogError dialogError);

        void onFacebookError(FacebookError facebookError);
    }

    public Facebook(String str) {
        if (str == null) {
            throw new IllegalArgumentException("You must specify your application ID when instantiating a Facebook object. See README for details.");
        }
        this.e = str;
    }

    private void a(Activity activity, String[] strArr) {
        Bundle bundle = new Bundle();
        if (strArr.length > 0) {
            bundle.putString("scope", TextUtils.join(",", strArr));
        }
        CookieSyncManager.createInstance(activity);
        dialog(activity, "oauth", bundle, new c(this));
    }

    private boolean a(Activity activity, Intent intent) {
        ResolveInfo resolveActivity = activity.getPackageManager().resolveActivity(intent, 0);
        if (resolveActivity == null) {
            return false;
        }
        try {
            for (Signature charsString : activity.getPackageManager().getPackageInfo(resolveActivity.activityInfo.packageName, 64).signatures) {
                if (charsString.toCharsString().equals(FB_APP_SIGNATURE)) {
                    return true;
                }
            }
            return false;
        } catch (PackageManager.NameNotFoundException e2) {
            return false;
        }
    }

    private boolean a(Activity activity, String str, String[] strArr, int i) {
        Intent intent = new Intent();
        intent.setClassName("com.facebook.katana", "com.facebook.katana.ProxyAuth");
        intent.putExtra("client_id", str);
        if (strArr.length > 0) {
            intent.putExtra("scope", TextUtils.join(",", strArr));
        }
        if (!a(activity, intent)) {
            return false;
        }
        this.f3a = activity;
        this.f5a = strArr;
        this.f1a = i;
        try {
            activity.startActivityForResult(intent, i);
            return true;
        } catch (ActivityNotFoundException e2) {
            return false;
        }
    }

    public void authorize(Activity activity, DialogListener dialogListener) {
        authorize(activity, new String[0], 32665, dialogListener);
    }

    public void authorize(Activity activity, String[] strArr, int i, DialogListener dialogListener) {
        boolean z = false;
        this.f4a = dialogListener;
        if (i >= 0) {
            z = a(activity, this.e, strArr, i);
        }
        if (!z) {
            a(activity, strArr);
        }
    }

    public void authorize(Activity activity, String[] strArr, DialogListener dialogListener) {
        authorize(activity, strArr, 32665, dialogListener);
    }

    public void authorizeCallback(int i, int i2, Intent intent) {
        if (i != this.f1a) {
            return;
        }
        if (i2 == -1) {
            String stringExtra = intent.getStringExtra("error");
            if (stringExtra == null) {
                stringExtra = intent.getStringExtra("error_type");
            }
            if (stringExtra == null) {
                setAccessToken(intent.getStringExtra(TOKEN));
                setAccessExpiresIn(intent.getStringExtra(EXPIRES));
                if (isSessionValid()) {
                    this.f4a.onComplete(intent.getExtras());
                } else {
                    this.f4a.onFacebookError(new FacebookError("Failed to receive access token."));
                }
            } else if (stringExtra.equals(SINGLE_SIGN_ON_DISABLED) || stringExtra.equals("AndroidAuthKillSwitchException")) {
                a(this.f3a, this.f5a);
            } else if (stringExtra.equals("access_denied") || stringExtra.equals("OAuthAccessDeniedException")) {
                this.f4a.onCancel();
            } else {
                this.f4a.onFacebookError(new FacebookError(stringExtra));
            }
        } else if (i2 != 0) {
        } else {
            if (intent != null) {
                this.f4a.onError(new DialogError(intent.getStringExtra("error"), intent.getIntExtra("error_code", -1), intent.getStringExtra("failing_url")));
            } else {
                this.f4a.onCancel();
            }
        }
    }

    public void dialog(Context context, String str, Bundle bundle, DialogListener dialogListener) {
        String str2 = a + str;
        bundle.putString("display", "touch");
        bundle.putString("redirect_uri", REDIRECT_URI);
        if (str.equals("oauth")) {
            bundle.putString("type", "user_agent");
            bundle.putString("client_id", this.e);
        } else {
            bundle.putString("app_id", this.e);
        }
        if (isSessionValid()) {
            bundle.putString(TOKEN, getAccessToken());
        }
        String str3 = str2 + "?" + Util.encodeUrl(bundle);
        if (context.checkCallingOrSelfPermission("android.permission.INTERNET") != 0) {
            Util.showAlert(context, "Error", "Application requires permission to access the Internet");
        } else {
            new FbDialog(context, str3, dialogListener).show();
        }
    }

    public void dialog(Context context, String str, DialogListener dialogListener) {
        dialog(context, str, new Bundle(), dialogListener);
    }

    public long getAccessExpires() {
        return this.f2a;
    }

    public String getAccessToken() {
        return this.d;
    }

    public String getAppId() {
        return this.e;
    }

    public boolean isSessionValid() {
        return getAccessToken() != null && (getAccessExpires() == 0 || System.currentTimeMillis() < getAccessExpires());
    }

    public String logout(Context context) {
        Util.clearCookies(context);
        Bundle bundle = new Bundle();
        bundle.putString("method", "auth.expireSession");
        String request = request(bundle);
        setAccessToken(null);
        setAccessExpires(0);
        return request;
    }

    public String request(Bundle bundle) {
        if (bundle.containsKey("method")) {
            return request(null, bundle, "GET");
        }
        throw new IllegalArgumentException("API method must be specified. (parameters must contain key \"method\" and value). See http://developers.facebook.com/docs/reference/rest/");
    }

    public String request(String str) {
        return request(str, new Bundle(), "GET");
    }

    public String request(String str, Bundle bundle) {
        return request(str, bundle, "GET");
    }

    public String request(String str, Bundle bundle, String str2) {
        bundle.putString("format", "json");
        if (isSessionValid()) {
            bundle.putString(TOKEN, getAccessToken());
        }
        return Util.openUrl(str != null ? b + str : c, str2, bundle);
    }

    public void setAccessExpires(long j) {
        this.f2a = j;
    }

    public void setAccessExpiresIn(String str) {
        if (str != null && !str.equals("0")) {
            setAccessExpires(System.currentTimeMillis() + ((long) (Integer.parseInt(str) * 1000)));
        }
    }

    public void setAccessToken(String str) {
        this.d = str;
    }

    public void setAppId(String str) {
        this.e = str;
    }
}
