package com.facebook.android;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Display;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.facebook.android.Facebook;
import com.reactor.game.torus.R;

public class FbDialog extends Dialog {
    static final FrameLayout.LayoutParams a = new FrameLayout.LayoutParams(-1, -1);

    /* renamed from: a  reason: collision with other field name */
    static final float[] f7a = {460.0f, 260.0f};
    static final float[] b = {280.0f, 420.0f};

    /* renamed from: a  reason: collision with other field name */
    private ProgressDialog f8a;

    /* renamed from: a  reason: collision with other field name */
    private WebView f9a;

    /* renamed from: a  reason: collision with other field name */
    private LinearLayout f10a;

    /* renamed from: a  reason: collision with other field name */
    private TextView f11a;

    /* renamed from: a  reason: collision with other field name */
    private Facebook.DialogListener f12a;

    /* renamed from: a  reason: collision with other field name */
    private String f13a;

    public FbDialog(Context context, String str, Facebook.DialogListener dialogListener) {
        super(context);
        this.f13a = str;
        this.f12a = dialogListener;
    }

    private void a() {
        requestWindowFeature(1);
        Drawable drawable = getContext().getResources().getDrawable(R.drawable.facebook_icon);
        this.f11a = new TextView(getContext());
        this.f11a.setText("Facebook");
        this.f11a.setTextColor(-1);
        this.f11a.setTypeface(Typeface.DEFAULT_BOLD);
        this.f11a.setBackgroundColor(-9599820);
        this.f11a.setPadding(6, 4, 4, 4);
        this.f11a.setCompoundDrawablePadding(6);
        this.f11a.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
        this.f10a.addView(this.f11a);
    }

    private void b() {
        this.f9a = new WebView(getContext());
        this.f9a.setVerticalScrollBarEnabled(false);
        this.f9a.setHorizontalScrollBarEnabled(false);
        this.f9a.setWebViewClient(new e(this));
        this.f9a.getSettings().setJavaScriptEnabled(true);
        this.f9a.loadUrl(this.f13a);
        this.f9a.setLayoutParams(a);
        this.f10a.addView(this.f9a);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f8a = new ProgressDialog(getContext());
        this.f8a.requestWindowFeature(1);
        this.f8a.setMessage("Loading...");
        this.f10a = new LinearLayout(getContext());
        this.f10a.setOrientation(1);
        a();
        b();
        Display defaultDisplay = getWindow().getWindowManager().getDefaultDisplay();
        float f = getContext().getResources().getDisplayMetrics().density;
        float[] fArr = defaultDisplay.getWidth() < defaultDisplay.getHeight() ? b : f7a;
        addContentView(this.f10a, new FrameLayout.LayoutParams((int) ((fArr[0] * f) + 0.5f), (int) ((fArr[1] * f) + 0.5f)));
    }
}
