package com.facebook.android;

import android.util.Log;
import com.facebook.android.AsyncFacebookRunner;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import org.json.JSONException;

class i implements AsyncFacebookRunner.RequestListener {
    final /* synthetic */ ShareOnFacebook a;

    private i(ShareOnFacebook shareOnFacebook) {
        this.a = shareOnFacebook;
    }

    public void onComplete(String str, Object obj) {
        try {
            Util.parseJson(str).getString("message");
        } catch (JSONException e) {
            Log.w("ShareOnFacebook", "JSON Error in response");
        } catch (FacebookError e2) {
            Log.w("ShareOnFacebook", "Facebook Error: " + e2.getMessage());
        }
    }

    public void onFacebookError(FacebookError facebookError, Object obj) {
    }

    public void onFileNotFoundException(FileNotFoundException fileNotFoundException, Object obj) {
    }

    public void onIOException(IOException iOException, Object obj) {
    }

    public void onMalformedURLException(MalformedURLException malformedURLException, Object obj) {
    }
}
