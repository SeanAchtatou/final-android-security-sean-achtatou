package com.facebook.android;

import android.app.Activity;

public class ShareOnFacebook {
    public static final String APP_ID = "197604240277340";
    private Activity a;

    /* renamed from: a  reason: collision with other field name */
    private AsyncFacebookRunner f14a = new AsyncFacebookRunner(this.f15a);

    /* renamed from: a  reason: collision with other field name */
    private Facebook f15a = new Facebook(APP_ID);

    /* renamed from: a  reason: collision with other field name */
    private String f16a;

    public ShareOnFacebook(Activity activity) {
        this.a = activity;
    }

    public void authorize() {
        this.f15a.authorize(this.a, new String[]{"publish_stream"}, new g(this));
    }

    public Facebook getFb() {
        return this.f15a;
    }

    public void setBestRecord(String str) {
        this.f16a = str;
    }
}
