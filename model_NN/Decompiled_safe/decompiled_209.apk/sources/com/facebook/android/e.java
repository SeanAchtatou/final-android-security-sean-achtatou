package com.facebook.android;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

class e extends WebViewClient {
    final /* synthetic */ FbDialog a;

    private e(FbDialog fbDialog) {
        this.a = fbDialog;
    }

    public void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        String title = FbDialog.a(this.a).getTitle();
        if (title != null && title.length() > 0) {
            FbDialog.a(this.a).setText(title);
        }
        FbDialog.a(this.a).dismiss();
    }

    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        super.onPageStarted(webView, str, bitmap);
        FbDialog.a(this.a).show();
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
        super.onReceivedError(webView, i, str, str2);
        FbDialog.a(this.a).onError(new DialogError(str, i, str2));
        this.a.dismiss();
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (str.startsWith(Facebook.REDIRECT_URI)) {
            Bundle parseUrl = Util.parseUrl(str);
            String string = parseUrl.getString("error");
            if (string == null) {
                string = parseUrl.getString("error_type");
            }
            if (string == null) {
                FbDialog.a(this.a).onComplete(parseUrl);
            } else if (string.equals("access_denied") || string.equals("OAuthAccessDeniedException")) {
                FbDialog.a(this.a).onCancel();
            } else {
                FbDialog.a(this.a).onFacebookError(new FacebookError(string));
            }
            this.a.dismiss();
            return true;
        } else if (str.startsWith(Facebook.CANCEL_URI)) {
            FbDialog.a(this.a).onCancel();
            this.a.dismiss();
            return true;
        } else if (str.contains("touch")) {
            return false;
        } else {
            this.a.getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            return true;
        }
    }
}
