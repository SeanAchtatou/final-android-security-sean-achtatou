package com.facebook.android;

import android.content.Context;
import com.facebook.android.AsyncFacebookRunner;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

class a extends Thread {
    final /* synthetic */ Context a;

    /* renamed from: a  reason: collision with other field name */
    final /* synthetic */ AsyncFacebookRunner.RequestListener f17a;

    /* renamed from: a  reason: collision with other field name */
    final /* synthetic */ AsyncFacebookRunner f18a;

    /* renamed from: a  reason: collision with other field name */
    final /* synthetic */ Object f19a;

    a(AsyncFacebookRunner asyncFacebookRunner, Context context, AsyncFacebookRunner.RequestListener requestListener, Object obj) {
        this.f18a = asyncFacebookRunner;
        this.a = context;
        this.f17a = requestListener;
        this.f19a = obj;
    }

    public void run() {
        try {
            String logout = this.f18a.a.logout(this.a);
            if (logout.length() == 0 || logout.equals("false")) {
                this.f17a.onFacebookError(new FacebookError("auth.expireSession failed"), this.f19a);
            } else {
                this.f17a.onComplete(logout, this.f19a);
            }
        } catch (FileNotFoundException e) {
            this.f17a.onFileNotFoundException(e, this.f19a);
        } catch (MalformedURLException e2) {
            this.f17a.onMalformedURLException(e2, this.f19a);
        } catch (IOException e3) {
            this.f17a.onIOException(e3, this.f19a);
        }
    }
}
