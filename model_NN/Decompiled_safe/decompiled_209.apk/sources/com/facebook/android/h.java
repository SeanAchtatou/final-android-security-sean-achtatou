package com.facebook.android;

import android.os.Bundle;
import com.facebook.android.Facebook;

class h implements Facebook.DialogListener {
    final /* synthetic */ ShareOnFacebook a;

    private h(ShareOnFacebook shareOnFacebook) {
        this.a = shareOnFacebook;
    }

    public void onCancel() {
    }

    public void onComplete(Bundle bundle) {
        String string = bundle.getString("post_id");
        if (string != null) {
            ShareOnFacebook.a(this.a).request(string, new i(this.a));
        }
    }

    public void onError(DialogError dialogError) {
    }

    public void onFacebookError(FacebookError facebookError) {
    }
}
