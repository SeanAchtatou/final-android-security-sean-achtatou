package com.facebook.android;

public class DialogError extends Throwable {
    private int a;

    /* renamed from: a  reason: collision with other field name */
    private String f0a;

    public DialogError(String str, int i, String str2) {
        super(str);
        this.a = i;
        this.f0a = str2;
    }
}
