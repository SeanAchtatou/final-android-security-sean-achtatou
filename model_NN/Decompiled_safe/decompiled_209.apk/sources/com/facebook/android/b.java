package com.facebook.android;

import android.os.Bundle;
import com.facebook.android.AsyncFacebookRunner;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

class b extends Thread {
    final /* synthetic */ Bundle a;

    /* renamed from: a  reason: collision with other field name */
    final /* synthetic */ AsyncFacebookRunner.RequestListener f20a;

    /* renamed from: a  reason: collision with other field name */
    final /* synthetic */ AsyncFacebookRunner f21a;

    /* renamed from: a  reason: collision with other field name */
    final /* synthetic */ Object f22a;

    /* renamed from: a  reason: collision with other field name */
    final /* synthetic */ String f23a;
    final /* synthetic */ String b;

    b(AsyncFacebookRunner asyncFacebookRunner, String str, Bundle bundle, String str2, AsyncFacebookRunner.RequestListener requestListener, Object obj) {
        this.f21a = asyncFacebookRunner;
        this.f23a = str;
        this.a = bundle;
        this.b = str2;
        this.f20a = requestListener;
        this.f22a = obj;
    }

    public void run() {
        try {
            this.f20a.onComplete(this.f21a.a.request(this.f23a, this.a, this.b), this.f22a);
        } catch (FileNotFoundException e) {
            this.f20a.onFileNotFoundException(e, this.f22a);
        } catch (MalformedURLException e2) {
            this.f20a.onMalformedURLException(e2, this.f22a);
        } catch (IOException e3) {
            this.f20a.onIOException(e3, this.f22a);
        }
    }
}
