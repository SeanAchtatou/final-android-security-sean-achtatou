package com.reactor.game.torus;

public class CubicMotion {
    int a;

    /* renamed from: a  reason: collision with other field name */
    long f25a;

    /* renamed from: a  reason: collision with other field name */
    float[] f26a;
    public float mTarget;

    CubicMotion(int i) {
        this.a = i > 0 ? i : 1000;
        this.f25a = System.currentTimeMillis();
        this.f26a = new float[4];
        this.mTarget = 0.0f;
    }

    /* access modifiers changed from: package-private */
    public float a() {
        long currentTimeMillis = (System.currentTimeMillis() - this.f25a) / ((long) this.a);
        if (currentTimeMillis < 0 || currentTimeMillis >= 1) {
            return this.mTarget;
        }
        return (((float) currentTimeMillis) * ((((this.f26a[0] * ((float) currentTimeMillis)) + this.f26a[1]) * ((float) currentTimeMillis)) + this.f26a[2])) + this.f26a[3];
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a  reason: collision with other method in class */
    public void m0a() {
        for (int i = 0; i < 4; i++) {
            this.f26a[i] = 0.0f;
        }
        this.mTarget = 0.0f;
    }

    public void setTarget(float f) {
        float f2;
        float a2 = a();
        long currentTimeMillis = System.currentTimeMillis() - this.f25a;
        this.f25a += currentTimeMillis;
        long j = currentTimeMillis / ((long) this.a);
        if (j < 0 || j >= 1) {
            f2 = 0.0f;
        } else {
            f2 = (((float) j) * ((this.f26a[0] * 3.0f * ((float) j)) + (this.f26a[1] * 2.0f))) + this.f26a[2];
        }
        this.mTarget = f;
        float f3 = f - a2;
        this.f26a[0] = f2 - (2.0f * f3);
        this.f26a[1] = (f3 * 3.0f) - (2.0f * f2);
        this.f26a[2] = f2;
        this.f26a[3] = a2;
    }
}
