package com.reactor.game.torus;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.EditText;
import com.facebook.android.ShareOnFacebook;
import com.reactor.game.torus.Game;
import java.util.ArrayList;

public class TorusView extends SurfaceView implements GestureDetector.OnGestureListener, SurfaceHolder.Callback {
    private float a = 0.0f;

    /* renamed from: a  reason: collision with other field name */
    private int f65a;

    /* renamed from: a  reason: collision with other field name */
    private long f66a = 0;

    /* renamed from: a  reason: collision with other field name */
    private Bitmap f67a;

    /* renamed from: a  reason: collision with other field name */
    private Canvas f68a;

    /* renamed from: a  reason: collision with other field name */
    private Paint f69a;

    /* renamed from: a  reason: collision with other field name */
    private Typeface f70a;

    /* renamed from: a  reason: collision with other field name */
    private final Handler f71a = new Handler();

    /* renamed from: a  reason: collision with other field name */
    private GestureDetector f72a;

    /* renamed from: a  reason: collision with other field name */
    private SurfaceHolder f73a;

    /* renamed from: a  reason: collision with other field name */
    private Game f74a;

    /* renamed from: a  reason: collision with other field name */
    private TorusAct f75a;

    /* renamed from: a  reason: collision with other field name */
    private f f76a;

    /* renamed from: a  reason: collision with other field name */
    private g f77a;

    /* renamed from: a  reason: collision with other field name */
    private h f78a;

    /* renamed from: a  reason: collision with other field name */
    private final Runnable f79a = new c(this);
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with other field name */
    public String f80a = new String();

    /* renamed from: a  reason: collision with other field name */
    private ArrayList f81a;

    /* renamed from: a  reason: collision with other field name */
    private boolean f82a = false;
    private float b = 0.0f;

    /* renamed from: b  reason: collision with other field name */
    private int f83b;

    /* renamed from: b  reason: collision with other field name */
    private Bitmap f84b;

    /* renamed from: b  reason: collision with other field name */
    private String f85b = "0:00";
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with other field name */
    public boolean f86b = false;
    private float c;

    /* renamed from: c  reason: collision with other field name */
    private int f87c;

    /* renamed from: c  reason: collision with other field name */
    private Bitmap f88c;

    /* renamed from: c  reason: collision with other field name */
    private String f89c;

    /* renamed from: c  reason: collision with other field name */
    private boolean f90c = false;
    private int d;

    /* renamed from: d  reason: collision with other field name */
    private Bitmap f91d;

    /* renamed from: d  reason: collision with other field name */
    private boolean f92d = false;
    private int e = 480;

    /* renamed from: e  reason: collision with other field name */
    private Bitmap f93e;

    /* renamed from: e  reason: collision with other field name */
    private boolean f94e = false;
    private int f = 800;

    /* renamed from: f  reason: collision with other field name */
    private Bitmap f95f;
    private int g = -159;

    /* renamed from: g  reason: collision with other field name */
    private Bitmap f96g;
    private int h = -183;

    /* renamed from: h  reason: collision with other field name */
    private Bitmap f97h;
    private int i;

    /* renamed from: i  reason: collision with other field name */
    private Bitmap f98i;
    private int j = 1;

    /* renamed from: j  reason: collision with other field name */
    private Bitmap f99j;
    private int k = 40;

    /* renamed from: k  reason: collision with other field name */
    private Bitmap f100k;
    private Bitmap l;
    private Bitmap m;
    private Bitmap n;
    private Bitmap o;
    private Bitmap p;
    private Bitmap q;
    private Bitmap r;
    private Bitmap s;
    private Bitmap t;
    private Bitmap u;
    private Bitmap v;
    private Bitmap w;
    private Bitmap x;
    private Bitmap y;

    public TorusView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f75a = (TorusAct) context;
        this.f73a = getHolder();
        this.f73a.addCallback(this);
        this.f69a = new Paint();
        this.f77a = new g(this);
        this.f74a = new Game();
        this.f76a = new f(this);
        this.f78a = new h(this);
        setFocusable(true);
        setLongClickable(true);
        this.f72a = new GestureDetector(this);
        this.f100k = BitmapFactory.decodeResource(context.getResources(), R.drawable.blocks);
        this.f91d = BitmapFactory.decodeResource(context.getResources(), R.drawable.next);
        this.m = BitmapFactory.decodeResource(context.getResources(), R.drawable.score);
        this.f84b = BitmapFactory.decodeResource(context.getResources(), R.drawable.best);
        this.f88c = GlobalVars.scaleBitmap(context, R.drawable.smallbox, ((float) this.e) * 0.19f, ((float) this.f) * 0.08f);
        this.f98i = BitmapFactory.decodeResource(context.getResources(), R.drawable.paused);
        this.f95f = GlobalVars.scaleBitmap(context, R.drawable.pause, ((float) this.e) * 0.2f, ((float) this.f) * 0.05f);
        this.f99j = GlobalVars.scaleBitmap(context, R.drawable.gameover, ((float) this.e) * 0.25f, ((float) this.f) * 0.04f);
        this.f67a = BitmapFactory.decodeResource(this.f75a.getResources(), R.drawable.base0);
        this.p = GlobalVars.scaleBitmap(context, R.drawable.left, ((float) this.e) * 0.15f, ((float) this.f) * 0.09f);
        this.q = GlobalVars.scaleBitmap(context, R.drawable.right, ((float) this.e) * 0.15f, ((float) this.f) * 0.09f);
        this.r = GlobalVars.scaleBitmap(context, R.drawable.down, ((float) this.e) * 0.15f, ((float) this.f) * 0.09f);
        this.s = GlobalVars.scaleBitmap(context, R.drawable.rotate, ((float) this.e) * 0.15f, ((float) this.f) * 0.09f);
        this.v = GlobalVars.scaleBitmap(context, R.drawable.smallplay, ((float) this.e) * 0.2f, ((float) this.f) * 0.05f);
        this.t = BitmapFactory.decodeResource(context.getResources(), R.drawable.time);
        this.u = GlobalVars.scaleBitmap(context, R.drawable.panel, ((float) this.e) * 0.165f, ((float) this.f) * 0.046f);
        new DisplayMetrics();
        DisplayMetrics displayMetrics = this.f75a.getApplicationContext().getResources().getDisplayMetrics();
        this.e = displayMetrics.widthPixels;
        this.f = displayMetrics.heightPixels;
        this.h = this.f == 480 ? -138 : this.f == 533 ? -183 : -213;
        this.i = this.f == 480 ? 301 : this.f == 533 ? 346 : 376;
        this.x = GlobalVars.scaleBitmap(context, R.drawable.facebook, 32.0f, 32.0f);
        this.y = GlobalVars.scaleBitmap(context, R.drawable.tip, 64.0f, 64.0f);
        this.l = GlobalVars.scaleBitmap(context, R.drawable.mainmenu, 0.65f * ((float) this.e), ((float) this.f) * 0.14f);
        this.f96g = GlobalVars.scaleBitmap(context, R.drawable.restart, 0.65f * ((float) this.e), ((float) this.f) * 0.14f);
        this.f97h = GlobalVars.scaleBitmap(context, R.drawable.quit, 0.65f * ((float) this.e), ((float) this.f) * 0.14f);
        this.o = GlobalVars.scaleBitmap(context, R.drawable.bigbox, ((float) this.e) * 0.85f, ((float) this.f) * 0.23f);
        this.w = Bitmap.createBitmap(this.e, this.f, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(this.w);
        this.f69a.setColor(-1291845632);
        canvas.drawRect(0.0f, 0.0f, (float) this.e, (float) this.f, this.f69a);
        this.f70a = Typeface.createFromAsset(context.getAssets(), "222-CAI978.ttf");
        this.c = 0.0f;
    }

    public static String niceTime(long j2) {
        long j3 = j2 % 60;
        return (j2 / 60) + ":" + (j3 < 10 ? "0" + j3 : String.valueOf(j3));
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.f68a = this.f73a.lockCanvas();
        long currentTimeMillis = System.currentTimeMillis();
        if (this.f68a != null) {
            this.f68a.translate((float) (-this.g), (float) (-this.h));
            this.f69a.setAntiAlias(true);
            this.f69a.setStyle(Paint.Style.FILL);
            this.f69a.setColor(-16777216);
            this.f68a.drawBitmap(this.n, (Rect) null, new RectF((float) this.g, (float) this.h, (float) (this.e + this.g), (float) (this.f + this.h)), this.f69a);
            this.f68a.drawBitmap(this.f67a, (float) (this.g + ((this.e - this.f67a.getWidth()) / 2)), (float) (this.h + this.i), this.f69a);
            float f2 = (float) this.g;
            if (!this.f92d) {
                this.f68a.drawBitmap(this.f91d, (Rect) null, new RectF((float) (this.g + 18), ((float) (this.h + 10)) + this.c, (float) (this.g + 71), ((float) (this.h + 27)) + this.c), this.f69a);
                float width = (float) (((this.g + this.e) - this.f84b.getWidth()) - 2);
                this.f68a.drawBitmap(this.f84b, (Rect) null, new RectF(2.0f + width, ((float) (this.h + 10)) + this.c, 55.0f + width, ((float) (this.h + 27)) + this.c), this.f69a);
                this.f68a.drawBitmap(this.f88c, (float) this.g, ((float) (this.h + 27)) + this.c, this.f69a);
                this.f68a.drawBitmap(this.f88c, width - 14.0f, ((float) (this.h + 27)) + this.c, this.f69a);
                if (this.f74a.mode != 3 && this.f74a.score > Integer.parseInt(this.f89c)) {
                    this.f89c = String.valueOf(this.f74a.score);
                }
                this.f69a.setTypeface(this.f70a);
                this.f69a.setTextSize(17.0f);
                this.f69a.setColor(-1);
                Paint.FontMetrics fontMetrics = this.f69a.getFontMetrics();
                float ceil = (float) Math.ceil((double) (fontMetrics.descent - fontMetrics.ascent));
                this.f68a.drawText(this.f89c, ((((float) this.f88c.getWidth()) - this.f69a.measureText(this.f89c)) / 2.0f) + (width - 14.0f), ((float) (this.h + 27)) + this.c + (((float) this.f88c.getHeight()) / 2.0f), this.f69a);
                this.f68a.save();
                this.f68a.clipRect(((float) this.g) + (((float) (this.f88c.getWidth() - 78)) / 2.0f), ((float) (this.h + 27)) + this.c + (((float) (this.f88c.getHeight() - 46)) / 2.0f), ((float) this.g) + (((float) (this.f88c.getWidth() - 78)) / 2.0f) + 78.0f, ((float) (this.h + 27)) + this.c + (((float) (this.f88c.getHeight() - 46)) / 2.0f) + 46.0f);
                this.f68a.drawBitmap(this.f100k, ((((float) this.g) + (((float) (this.f88c.getWidth() - 78)) / 2.0f)) - ((float) (this.d * 80))) - 2.0f, ((((float) (this.h + 27)) + this.c) + (((float) (this.f88c.getHeight() - 46)) / 2.0f)) - 3.0f, this.f69a);
                this.f68a.restore();
                float height = ((float) (this.h + 27)) + this.c + ((float) this.f88c.getHeight()) + 5.0f;
                this.f68a.drawBitmap(this.m, (Rect) null, new RectF((float) (this.g + 18), height, (float) (this.g + 71), 17.0f + height), this.f69a);
                this.f68a.drawBitmap(this.t, (Rect) null, new RectF(2.0f + width, height, 53.0f + width, 17.0f + height), this.f69a);
                this.f68a.drawBitmap(this.u, ((float) this.g) + (((float) (this.f88c.getWidth() - this.u.getWidth())) * 0.5f), 17.0f + height, this.f69a);
                this.f68a.drawBitmap(this.u, (width - 14.0f) + (((float) (this.f88c.getWidth() - this.u.getWidth())) * 0.5f), 17.0f + height, this.f69a);
                this.f69a.setTypeface(this.f70a);
                this.f69a.setTextSize(15.0f);
                this.f69a.setColor(-1);
                Paint.FontMetrics fontMetrics2 = this.f69a.getFontMetrics();
                float measureText = this.f69a.measureText(this.f80a);
                float ceil2 = (float) Math.ceil((double) (fontMetrics2.descent - fontMetrics2.ascent));
                this.f68a.drawText(this.f80a, ((((float) this.u.getWidth()) - measureText) * 0.5f) + ((float) this.g) + (((float) (this.f88c.getWidth() - this.u.getWidth())) * 0.5f), 17.0f + height + ((((float) this.u.getHeight()) + ceil2) * 0.5f), this.f69a);
                this.f68a.drawText(this.f85b, (width - 14.0f) + (((float) (this.f88c.getWidth() - this.u.getWidth())) * 0.5f) + ((((float) this.u.getWidth()) - this.f69a.measureText(this.f85b)) * 0.5f), height + 17.0f + ((ceil2 + ((float) this.u.getHeight())) * 0.5f), this.f69a);
                if (!this.f90c) {
                    float height2 = (float) ((this.h + this.f) - this.f95f.getHeight());
                    this.f68a.drawBitmap(this.f95f, ((float) this.g) + (((float) (this.e - this.f95f.getWidth())) * 0.5f), height2, this.f69a);
                    this.f68a.drawBitmap(this.f93e, ((float) this.g) + (((float) (this.e - this.f93e.getWidth())) * 0.5f), height2 - ((float) this.f93e.getHeight()), this.f69a);
                    if (GlobalVars.KEYS) {
                        float height3 = (float) ((this.h + this.f) - this.p.getHeight());
                        this.f68a.drawBitmap(this.p, (float) (this.g + 4), height3, this.f69a);
                        this.f68a.drawBitmap(this.q, (float) ((this.g + this.e) - this.q.getWidth()), height3, this.f69a);
                        float height4 = height3 - ((float) this.s.getHeight());
                        this.f68a.drawBitmap(this.r, (float) (this.g + 4), height4, this.f69a);
                        this.f68a.drawBitmap(this.s, (float) ((this.g + this.e) - this.s.getWidth()), height4, this.f69a);
                    }
                    if (this.f82a) {
                        this.f74a.drawCylinder(this.f68a, this.f69a, false, false, 0.0f, null);
                        this.f82a = false;
                    }
                    if (!GlobalVars.HELP || this.f74a.dropPositions[3] <= ((float) (this.h + 101))) {
                        a((String) null);
                    } else {
                        this.f74a.drawCylinder(this.f68a, this.f69a, true, false, 0.0f, null);
                        this.f69a.setTypeface(this.f70a);
                        this.f69a.setTextSize(17.0f);
                        this.f69a.setColor(-1);
                        Paint.FontMetrics fontMetrics3 = this.f69a.getFontMetrics();
                        float ceil3 = (float) Math.ceil((double) (fontMetrics3.descent - fontMetrics3.ascent));
                        float width2 = ((((float) this.g) + (((float) this.e) * 0.5f)) - (0.45f * ((float) this.o.getWidth()))) + (((float) this.y.getWidth()) * 0.6f) + 5.0f;
                        float f3 = 0.0125f * ceil3;
                        float width3 = (((float) (this.e - this.o.getWidth())) * 0.5f) + ((float) this.g);
                        float height5 = (((float) (this.f - this.o.getHeight())) * 0.5f) + ((float) this.h);
                        this.f68a.drawBitmap(this.o, width3, height5, this.f69a);
                        this.f68a.drawBitmap(this.y, width3 + 5.0f, 5.0f + height5, this.f69a);
                        if (this.j < 4) {
                            GlobalVars.drawContent(this.j == 1 ? "Click the falling piece to rotate." : this.j == 2 ? "Click the ghost piece to accelerate the falling block." : "Turn the torus to left or right with your gesture.", ceil3, f3, ((((float) this.o.getWidth()) * 0.9f) - 5.0f) - (((float) this.y.getWidth()) * 0.6f), width2, 10.0f + height5, this.f68a, this.f69a);
                            if (this.j == 1 || this.j == 2) {
                                this.f69a.setColor(-256);
                                this.f68a.drawText("Next tip", ((((0.5f * ((float) this.e)) + (0.45f * ((float) this.o.getWidth()))) + ((float) this.g)) - this.f69a.measureText("Next tip")) - 5.0f, ((((float) (this.f + this.o.getHeight())) * 0.5f) + ((float) this.h)) - 20.0f, this.f69a);
                            }
                        }
                    }
                } else {
                    this.f68a.drawBitmap(this.f98i, (float) (this.g + ((this.e - this.f67a.getWidth()) / 2) + 37), (float) (this.h + this.i + 30), this.f69a);
                    this.f68a.drawBitmap(this.w, (float) this.g, (float) this.h, this.f69a);
                    this.f68a.drawBitmap(this.v, ((float) this.g) + (((float) (this.e - this.f95f.getWidth())) * 0.5f), (float) ((this.h + this.f) - this.f95f.getHeight()), this.f69a);
                    this.f68a.drawBitmap(this.f96g, (float) (this.g + ((this.e - this.f96g.getWidth()) / 2)), (((float) this.h) + (0.5f * ((float) this.f))) - (1.04f * ((float) this.f97h.getHeight())), this.f69a);
                    this.f68a.drawBitmap(this.f97h, ((float) this.g) + (((float) (this.e - this.f97h.getWidth())) * 0.5f), ((float) this.h) + (0.5f * ((float) this.f)), this.f69a);
                    this.f68a.drawBitmap(this.x, ((float) this.g) + (((float) (this.e - this.f97h.getWidth())) * 0.5f), ((float) this.h) + (0.5f * ((float) this.f)) + (1.04f * ((float) this.f97h.getHeight())), this.f69a);
                    this.f69a.setTypeface(this.f70a);
                    this.f69a.setTextSize(17.0f);
                    this.f69a.setColor(-1);
                    Paint.FontMetrics fontMetrics4 = this.f69a.getFontMetrics();
                    this.f69a.measureText("Share to Facebook");
                    this.f68a.drawText("Share to Facebook", ((float) this.g) + (((float) (this.e - this.f97h.getWidth())) * 0.5f) + ((float) this.x.getWidth()) + 10.0f, ((((float) Math.ceil((double) (fontMetrics4.descent - fontMetrics4.ascent))) + ((float) this.x.getHeight())) * 0.5f) + ((float) this.h) + (0.5f * ((float) this.f)) + (1.04f * ((float) this.f97h.getHeight())), this.f69a);
                }
                this.f68a.drawBitmap(this.f93e, ((float) this.g) + (((float) (this.e - this.f93e.getWidth())) * 0.5f), (float) (((this.h + this.f) - this.f95f.getHeight()) - this.f93e.getHeight()), this.f69a);
            } else {
                this.f68a.drawBitmap(this.m, ((float) this.g) + (((float) (this.e - this.m.getWidth())) / 2.0f), (float) (this.h + 55), this.f69a);
                this.f69a.setTypeface(this.f70a);
                this.f69a.setTextSize(17.0f);
                this.f69a.setColor(-1);
                Paint.FontMetrics fontMetrics5 = this.f69a.getFontMetrics();
                float ceil4 = (float) Math.ceil((double) (fontMetrics5.descent - fontMetrics5.ascent));
                this.f68a.drawText(this.f80a, ((((float) this.e) - this.f69a.measureText(this.f80a)) / 2.0f) + ((float) this.g), ((float) (this.h + 55 + this.m.getHeight())) + ceil4, this.f69a);
                this.f68a.drawBitmap(this.f99j, (float) (this.g + ((this.e - this.f99j.getWidth()) / 2)), (float) (this.h + 126), this.f69a);
                float height6 = ((float) (this.h + 126)) + ((float) this.f99j.getHeight()) + (((float) this.f96g.getHeight()) * 0.08f);
                this.f68a.drawBitmap(this.f96g, ((float) this.g) + (((float) (this.e - this.f96g.getWidth())) * 0.5f), height6, this.f69a);
                float height7 = height6 + (((float) this.f96g.getHeight()) * 1.04f);
                this.f68a.drawBitmap(this.l, ((float) this.g) + (((float) (this.e - this.f96g.getWidth())) * 0.5f), height7, this.f69a);
                float height8 = height7 + (((float) this.f96g.getHeight()) * 1.04f);
                this.f68a.drawBitmap(this.x, ((float) this.g) + (((float) (this.e - this.f96g.getWidth())) * 0.5f), height8, this.f69a);
                this.f68a.drawText("Share to Facebook", ((float) this.g) + (((float) (this.e - this.f97h.getWidth())) * 0.5f) + ((float) this.x.getWidth()) + 10.0f, ((ceil4 + ((float) this.x.getHeight())) * 0.5f) + height8, this.f69a);
            }
        }
        if (this.f68a != null) {
            this.f73a.unlockCanvasAndPost(this.f68a);
        }
        this.f71a.removeCallbacks(this.f79a);
        long currentTimeMillis2 = System.currentTimeMillis() - currentTimeMillis;
        if (this.f65a != 0) {
            return;
        }
        if (currentTimeMillis2 >= ((long) this.k)) {
            this.f71a.post(this.f79a);
        } else {
            this.f71a.postDelayed(this.f79a, ((long) this.k) - currentTimeMillis2);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        this.f77a.f116a = false;
        for (int size = this.f74a.block.size() - 1; size >= 0; size--) {
            if (((Game.Coord) this.f74a.block.get(size)).y + ((float) i2) > 14.0f) {
                this.f74a.drawCylinder(this.f68a, this.f69a, false, true, 0.0f, null);
                this.f76a.a(0);
                return;
            }
        }
        for (int size2 = this.f74a.block.size() - 1; size2 >= 0; size2--) {
            int i3 = (int) ((((((Game.Coord) this.f74a.block.get(size2)).x + this.f74a.x) % 15.0f) + 15.0f) % 15.0f);
            int i4 = (int) (((Game.Coord) this.f74a.block.get(size2)).y + ((float) i2));
            if (this.f74a.field[i3][i4] == null) {
                this.f74a.field[i3][i4] = new int[2];
            }
            this.f74a.field[i3][i4][0] = this.f74a.type + 1;
            this.f74a.field[i3][i4][1] = this.f74a.pieceCount;
        }
        this.f74a.pieceCount++;
        ArrayList arrayList = new ArrayList();
        int i5 = 0;
        while (i5 < 15) {
            int i6 = 0;
            while (i6 < 15 && this.f74a.field[i6][i5] != null) {
                i6++;
            }
            if (i6 == 15) {
                for (int i7 = 0; i7 < 15; i7++) {
                    if (this.f74a.field[i7][i5] == null) {
                        this.f74a.field[i7][i5] = new int[2];
                    }
                    this.f74a.field[i7][i5][0] = this.f74a.field[i7][i5][0];
                    this.f74a.field[i7][i5][1] = 0;
                }
                arrayList.add(Integer.valueOf(i5));
            }
            i5++;
        }
        if (arrayList.size() != 0) {
            this.f94e = true;
            this.b = (float) this.f74a.time;
            this.f81a = arrayList;
            return;
        }
        b();
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.f75a);
        EditText editText = new EditText(this.f75a);
        builder.setView(editText);
        builder.setCancelable(false);
        builder.setTitle("Please fill your name");
        builder.setPositiveButton("Yes", new d(this, editText, i3, i2));
        builder.setNegativeButton("No", new e(this));
        builder.create();
        builder.show();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* access modifiers changed from: package-private */
    public void a(String str) {
        long currentTimeMillis = System.currentTimeMillis();
        float max = Math.max(0.0f, Math.min(1000.0f, (float) (currentTimeMillis - this.f66a)));
        Game game = this.f74a;
        game.time = (long) (((float) game.time) + max);
        if (this.f74a.mode != 2 || this.f74a.time <= 301000) {
            this.f85b = niceTime((long) ((int) ((this.f74a.mode == 2 ? 301000 - this.f74a.time : this.f74a.time) / 1000)));
        } else {
            this.f76a.a(0);
        }
        float exp = (float) (this.f74a.mode == 3 ? 2000.0d : 20.0d + (2980.0d * Math.exp((double) ((-this.f74a.lines) / 35))));
        if (this.f77a.f116a) {
            Game game2 = this.f74a;
            game2.score = (int) (((float) game2.score) + (max / 100.0f));
            if (this.f74a.mode != 3) {
                this.f80a = String.valueOf(this.f74a.score);
            }
            exp = Math.min(exp, 30.0f);
        }
        this.f74a.posY -= Math.max(0.0f, Math.min(1.0f, max / exp));
        this.f66a = currentTimeMillis;
        if (this.f94e) {
            float sqrt = (float) (((double) (((float) this.f74a.time) - this.b)) / Math.sqrt((double) this.f81a.size()));
            if (sqrt > 300.0f || sqrt < 0.0f) {
                this.f94e = false;
                b();
                this.f74a.drawCylinder(this.f68a, this.f69a, true, false, 0.0f, null);
                if (GlobalVars.SOUND) {
                    Log.d("TEST", "explode");
                    try {
                        MediaPlayer.create(getContext(), (int) R.raw.explode).start();
                    } catch (Exception e2) {
                    }
                }
            } else {
                this.f74a.drawCylinder(this.f68a, this.f69a, false, false, sqrt / 300.0f, this.f81a);
            }
        } else {
            if (str != null || ((this.f77a.b ^ this.f77a.c) && ((float) currentTimeMillis) - this.a > 150.0f)) {
                this.a = (float) currentTimeMillis;
                a(str == "left" ? 1.0f : str == "right" ? -1.0f : this.f77a.b ? 1.0f : this.f77a.c ? -1.0f : 0.0f);
                if (GlobalVars.SOUND) {
                    Log.d("TEST", "clank");
                    try {
                        MediaPlayer.create(getContext(), (int) R.raw.rotate).start();
                    } catch (Exception e3) {
                    }
                }
            }
            int floor = (int) Math.floor((double) this.f74a.posY);
            if (this.f74a.block.size() == 1) {
                int i2 = floor;
                while (i2 >= 0) {
                    if (this.f74a.field[(int) ((((((Game.Coord) this.f74a.block.get(0)).x + this.f74a.x) % 15.0f) + 15.0f) % 15.0f)][i2] == null) {
                        break;
                    }
                    i2--;
                }
                if (i2 < 0) {
                    a(floor + 1);
                    this.f74a.drawCylinder(this.f68a, this.f69a, true, false, 0.0f, null);
                    return;
                }
            } else {
                int size = this.f74a.block.size() - 1;
                while (size >= 0) {
                    int i3 = (int) (((Game.Coord) this.f74a.block.get(size)).y + ((float) floor));
                    if (i3 >= 0) {
                        if (i3 >= 0 && i3 < 15) {
                            if (this.f74a.field[(int) ((((((Game.Coord) this.f74a.block.get(size)).x + this.f74a.x) % 15.0f) + 15.0f) % 15.0f)][i3] != null) {
                            }
                        }
                        size--;
                    }
                    a(floor + 1);
                    this.f74a.drawCylinder(this.f68a, this.f69a, true, false, 0.0f, null);
                    if (GlobalVars.SOUND) {
                        try {
                            MediaPlayer.create(getContext(), (int) R.raw.clank).start();
                            return;
                        } catch (Exception e4) {
                            return;
                        }
                    } else {
                        return;
                    }
                }
            }
            this.f74a.drawCylinder(this.f68a, this.f69a, true, false, 0.0f, null);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a  reason: collision with other method in class */
    public boolean m2a() {
        int floor = (int) Math.floor((double) this.f74a.posY);
        if (this.f74a.block.size() == 1) {
            while (floor >= 0) {
                if (this.f74a.field[(int) ((((((Game.Coord) this.f74a.block.get(0)).x + this.f74a.x) % 15.0f) + 15.0f) % 15.0f)][floor] == null) {
                    return false;
                }
                floor--;
            }
            return true;
        }
        for (int size = this.f74a.block.size() - 1; size >= 0; size--) {
            int i2 = (int) ((((((Game.Coord) this.f74a.block.get(size)).x + this.f74a.x) % 15.0f) + 15.0f) % 15.0f);
            int i3 = (int) (((Game.Coord) this.f74a.block.get(size)).y + ((float) floor));
            if (i3 < 0 || (i2 > 0 && i2 < 15 && i3 > 0 && i3 < 15 && this.f74a.field[i2][i3] != null)) {
                return true;
            }
        }
        if (this.f74a.posY % 1.0f > 0.0f) {
            int ceil = (int) Math.ceil((double) this.f74a.posY);
            for (int size2 = this.f74a.block.size() - 1; size2 >= 0; size2--) {
                int i4 = (int) ((((((Game.Coord) this.f74a.block.get(size2)).x + this.f74a.x) % 15.0f) + 15.0f) % 15.0f);
                int i5 = (int) (((Game.Coord) this.f74a.block.get(size2)).y + ((float) ceil));
                if (i5 < 0 || (i4 >= 0 && i4 < 15 && i5 >= 0 && i5 < 15 && this.f74a.field[i4][i5] != null)) {
                    return true;
                }
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean a(float f2) {
        this.f74a.x += f2;
        if (m2a()) {
            this.f74a.x -= f2;
            return false;
        }
        this.f74a.viewPort.setTarget(this.f74a.viewPort.mTarget + f2);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.reactor.game.torus.TorusView.a(int, boolean):boolean
     arg types: [int, int]
     candidates:
      com.reactor.game.torus.TorusView.a(com.reactor.game.torus.TorusView, java.lang.String):java.lang.String
      com.reactor.game.torus.TorusView.a(com.reactor.game.torus.TorusView, boolean):boolean
      com.reactor.game.torus.TorusView.a(int, int):void
      com.reactor.game.torus.TorusView.a(int, boolean):boolean */
    /* access modifiers changed from: package-private */
    public boolean a(int i2, boolean z) {
        if (this.f74a.type == 0) {
            return false;
        }
        this.f87c = ((this.f87c + i2) + 4) % ((Game.BlockData) this.f74a.blockData.get(this.f74a.type)).coords.size();
        this.f74a.block = ((Game.Coords) ((Game.BlockData) this.f74a.blockData.get(this.f74a.type)).coords.get(this.f87c)).block;
        if (z) {
            return false;
        }
        if (!m2a() || a(1.0f) || a(-1.0f)) {
            if (GlobalVars.SOUND) {
                try {
                    MediaPlayer.create(getContext(), (int) R.raw.bounce).start();
                } catch (Exception e2) {
                }
            }
            return true;
        }
        a(-i2, true);
        return false;
    }

    /* access modifiers changed from: package-private */
    public void b() {
        int i2 = 0;
        int i3 = 0;
        while (i2 < 15) {
            int i4 = 0;
            while (i4 < 15 && this.f74a.field[i4][i2] != null) {
                i4++;
            }
            if (i4 == 15) {
                for (int i5 = 0; i5 < 15; i5++) {
                    this.f74a.field[i5][i2] = null;
                    for (int i6 = i2 + 1; i6 < 15; i6++) {
                        this.f74a.field[i5][i6 - 1] = this.f74a.field[i5][i6];
                    }
                    this.f74a.field[i5][14] = null;
                }
                i2--;
                this.f74a.lines++;
                i3++;
            }
            i2++;
        }
        Game game = this.f74a;
        game.score = new int[]{0, 10, 25, 40, 60, 100}[i3] + game.score;
        if (this.f74a.mode == 3) {
            int i7 = 14;
            while (i7 > 2) {
                int i8 = 0;
                while (i8 < 15 && this.f74a.field[i8][i7] == null) {
                    i8++;
                }
                if (i8 < 15) {
                    break;
                }
                i7--;
            }
            this.f80a = "+" + (i7 - 2);
            if (i7 == 2) {
                f();
                this.f74a.drawCylinder(this.f68a, this.f69a, false, false, 0.0f, null);
                this.f76a.a(1);
                this.f74a.viewPort.setTarget(this.f74a.viewPort.mTarget + ((Game.BlockData) this.f74a.blockData.get(this.f74a.type)).view);
                return;
            }
        } else {
            this.f80a = String.valueOf((int) Math.floor((double) this.f74a.score));
        }
        this.f74a.viewPort.setTarget(((Game.BlockData) this.f74a.blockData.get(this.f74a.type)).view + this.f74a.viewPort.mTarget);
        f();
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (!this.f90c) {
            this.f90c = true;
        }
    }

    /* access modifiers changed from: package-private */
    public void d() {
        c();
        this.f92d = true;
    }

    /* access modifiers changed from: package-private */
    public void e() {
        this.f74a.x = -2.0f;
        f();
    }

    /* access modifiers changed from: package-private */
    public void f() {
        this.f74a.type = this.f74a.nextType;
        this.f74a.nextType = this.f74a.getRandomPieceType();
        this.d = ((Game.BlockData) this.f74a.blockData.get(this.f74a.nextType)).theme;
        this.f87c = 0;
        this.f74a.posY = 16.0f;
        this.f74a.viewPort.setTarget(this.f74a.viewPort.mTarget - ((Game.BlockData) this.f74a.blockData.get(this.f74a.type)).view);
        this.f74a.block = ((Game.Coords) ((Game.BlockData) this.f74a.blockData.get(this.f74a.type)).coords.get(0)).block;
        this.f74a.pieceSpawnTime = (float) this.f74a.time;
    }

    /* access modifiers changed from: package-private */
    public void g() {
        this.f75a.startActivity(new Intent(this.f75a, WelcomeAct.class));
        this.f75a.finish();
    }

    public String getBestScore() {
        return this.f89c;
    }

    public f getControl() {
        return this.f76a;
    }

    public Game getGame() {
        return this.f74a;
    }

    /* access modifiers changed from: package-private */
    public void h() {
        GlobalVars.HELP = false;
        SharedPreferences.Editor edit = this.f75a.getSharedPreferences("com.rector.game.torus_preferences", 0).edit();
        edit.putBoolean("help", false);
        edit.commit();
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.reactor.game.torus.TorusView.a(int, boolean):boolean
     arg types: [int, int]
     candidates:
      com.reactor.game.torus.TorusView.a(com.reactor.game.torus.TorusView, java.lang.String):java.lang.String
      com.reactor.game.torus.TorusView.a(com.reactor.game.torus.TorusView, boolean):boolean
      com.reactor.game.torus.TorusView.a(int, int):void
      com.reactor.game.torus.TorusView.a(int, boolean):boolean */
    public boolean onDown(MotionEvent motionEvent) {
        float x2 = motionEvent.getX() + ((float) this.g);
        float y2 = motionEvent.getY() + ((float) this.h);
        if (!this.f92d) {
            if (!this.f90c) {
                if (GlobalVars.HELP && x2 >= ((float) this.g) + (((float) (this.e - this.o.getWidth())) * 0.5f) && x2 <= ((float) this.g) + (((float) (this.e + this.o.getWidth())) * 0.5f) && y2 >= ((float) this.h) + (((float) (this.f - this.o.getHeight())) * 0.5f) && y2 <= ((float) this.h) + (((float) (this.f + this.o.getHeight())) * 0.5f)) {
                    this.j++;
                    if (this.j > 3) {
                        GlobalVars.HELP = false;
                        h();
                    }
                    return false;
                } else if (!GlobalVars.HELP && !this.f77a.f116a) {
                    if (GlobalVars.KEYS) {
                        if (y2 >= ((float) ((this.h + this.f) - this.p.getHeight())) && y2 <= ((float) (this.h + this.f))) {
                            if (x2 < ((float) (this.g + 4)) || x2 > ((float) (this.g + 4 + this.p.getWidth()))) {
                                if (x2 >= ((float) ((this.g + this.e) - this.q.getWidth())) && x2 <= ((float) (this.g + this.e)) && !this.f77a.c) {
                                    this.f77a.c = true;
                                    a("right");
                                    this.f83b = 2;
                                }
                            } else if (!this.f77a.b) {
                                this.f77a.b = true;
                                a("left");
                                this.f83b = 1;
                            }
                            if (this.f83b == 1) {
                                this.f77a.b = false;
                            }
                            if (this.f83b == 2) {
                                this.f77a.c = false;
                            }
                            this.f83b = 0;
                        }
                        if (y2 >= ((float) (((this.h + this.f) - this.p.getHeight()) - this.s.getHeight())) && y2 <= ((float) ((this.h + this.f) - this.p.getHeight()))) {
                            if (x2 >= ((float) (this.g + 4)) && x2 <= ((float) (this.g + 4 + this.p.getWidth()))) {
                                this.f77a.f116a = true;
                                a((String) null);
                                return false;
                            } else if (x2 >= ((float) ((this.g + this.e) - this.q.getWidth())) && x2 <= ((float) (this.g + this.e))) {
                                a(1, false);
                            }
                        }
                    }
                    if (this.f74a.dropPositions[3] < this.f74a.f30a[2] && x2 >= this.f74a.f30a[0] && x2 <= this.f74a.f30a[1] && y2 >= this.f74a.f30a[2] && y2 <= this.f74a.f30a[3]) {
                        this.f77a.f116a = true;
                        a((String) null);
                        return false;
                    }
                }
            }
            if (x2 >= ((float) this.g) + (((float) (this.e - this.f95f.getWidth())) * 0.5f) && x2 <= ((float) this.g) + (((float) (this.e + this.f95f.getWidth())) * 0.5f) && y2 >= ((float) ((this.h + this.f) - this.f95f.getHeight())) && y2 <= ((float) (this.h + this.f))) {
                a();
                if (this.f90c) {
                    this.f76a.c();
                } else {
                    this.f76a.b();
                }
                return false;
            } else if (this.f90c && x2 >= ((float) (this.g + ((this.e - this.f96g.getWidth()) / 2))) && x2 <= ((float) this.g) + (((float) (this.e + this.f96g.getWidth())) * 0.5f)) {
                if (y2 >= (((float) this.h) + (((float) this.f) * 0.5f)) - (((float) this.f97h.getHeight()) * 1.04f) && y2 <= (((float) this.h) + (((float) this.f) * 0.5f)) - (0.04f * ((float) this.f97h.getHeight()))) {
                    this.j = 1;
                    this.f76a.a();
                } else if (y2 >= ((float) this.h) + (((float) this.f) * 0.5f) && y2 <= ((float) this.h) + (((float) this.f) * 0.5f) + ((float) this.f97h.getHeight())) {
                    this.f65a = 4;
                    a();
                    c();
                    this.f76a.a(2);
                    this.f78a.a();
                    g();
                } else if (y2 >= ((float) this.h) + (((float) this.f) * 0.5f) + (((float) this.f97h.getHeight()) * 1.04f) && y2 <= ((float) this.h) + (((float) this.f) * 0.5f) + (((float) this.f97h.getHeight()) * 1.04f) + ((float) this.x.getHeight())) {
                    this.f75a.shareOnFacebook = new ShareOnFacebook(this.f75a);
                    this.f75a.shareOnFacebook.authorize();
                }
            }
        } else if (x2 >= ((float) this.g) + (((float) (this.e - this.f96g.getWidth())) * 0.5f) && x2 <= ((float) this.g) + (((float) (this.e + this.f96g.getWidth())) * 0.5f)) {
            if (y2 >= ((float) (this.h + 126 + this.f99j.getHeight())) + (1.12f * ((float) this.f96g.getHeight())) && y2 <= ((float) (this.h + 126 + this.f99j.getHeight())) + (2.12f * ((float) this.f96g.getHeight()))) {
                a();
                this.f76a.a(2);
                this.f78a.a();
                g();
            } else if (y2 >= ((float) (this.h + 126 + this.f99j.getHeight())) + (((float) this.f96g.getHeight()) * 0.08f) && y2 <= ((float) (this.h + 126 + this.f99j.getHeight())) + (1.08f * ((float) this.f96g.getHeight()))) {
                a();
                this.f76a.a();
            } else if (y2 >= ((float) (this.h + 126 + this.f99j.getHeight())) + (2.16f * ((float) this.f96g.getHeight())) && y2 <= ((float) (this.h + 126 + this.f99j.getHeight())) + (2.16f * ((float) this.f96g.getHeight())) + ((float) this.x.getHeight())) {
                this.f75a.shareOnFacebook = new ShareOnFacebook(this.f75a);
                this.f75a.shareOnFacebook.authorize();
            }
        }
        return false;
    }

    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        if (GlobalVars.HELP || this.f77a.f116a) {
            return false;
        }
        float x2 = motionEvent.getX() - motionEvent2.getX();
        if (!this.f92d && !this.f90c) {
            if (x2 > 10.0f) {
                if (!this.f77a.b) {
                    this.f77a.b = true;
                    a("left");
                    this.f83b = 1;
                }
            } else if ((-x2) > 10.0f && !this.f77a.c) {
                this.f77a.c = true;
                a("right");
                this.f83b = 2;
            }
            if (this.f83b == 1) {
                this.f77a.b = false;
            }
            if (this.f83b == 2) {
                this.f77a.c = false;
            }
            this.f83b = 0;
        }
        return false;
    }

    public void onLongPress(MotionEvent motionEvent) {
    }

    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        return false;
    }

    public void onShowPress(MotionEvent motionEvent) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.reactor.game.torus.TorusView.a(int, boolean):boolean
     arg types: [int, int]
     candidates:
      com.reactor.game.torus.TorusView.a(com.reactor.game.torus.TorusView, java.lang.String):java.lang.String
      com.reactor.game.torus.TorusView.a(com.reactor.game.torus.TorusView, boolean):boolean
      com.reactor.game.torus.TorusView.a(int, int):void
      com.reactor.game.torus.TorusView.a(int, boolean):boolean */
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        float x2 = motionEvent.getX() + ((float) this.g);
        float y2 = motionEvent.getY() + ((float) this.h);
        if (!GlobalVars.HELP && !this.f92d && !this.f90c && !this.f77a.f116a && x2 >= this.f74a.dropPositions[0] && x2 <= this.f74a.dropPositions[1] && y2 >= this.f74a.dropPositions[2] && y2 <= this.f74a.dropPositions[3]) {
            a(1, false);
        }
        if (this.f77a.f116a) {
            this.f77a.f116a = false;
        }
        return false;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        this.f72a.onTouchEvent(motionEvent);
        return super.onTouchEvent(motionEvent);
    }

    public void resume() {
        if (this.f90c) {
            this.f90c = false;
            this.f77a.b = false;
            this.f77a.c = false;
            this.f77a.f116a = false;
            this.f66a = System.currentTimeMillis();
            this.f65a = 0;
        }
    }

    public void setOffsetForAd(float f2) {
        this.c = f2;
    }

    public void start() {
        this.f74a.nextType = this.f74a.getRandomPieceType();
        e();
        c();
        resume();
        this.f74a.time = 0;
        this.f92d = false;
    }

    public void startGame(int i2) {
        this.f82a = true;
        switch (i2) {
            case 1:
                this.f93e = GlobalVars.scaleBitmap(this.f75a, R.drawable.traditional, ((float) this.e) * 0.32f, ((float) this.f) * 0.03f);
                this.n = GlobalVars.scaleBitmap(this.f75a, R.drawable.traditionalbackground, (float) this.e, (float) this.f);
                break;
            case 2:
                this.f93e = GlobalVars.scaleBitmap(this.f75a, R.drawable.timeattack, ((float) this.e) * 0.32f, ((float) this.f) * 0.03f);
                this.n = GlobalVars.scaleBitmap(this.f75a, R.drawable.timerbackground, (float) this.e, (float) this.f);
                break;
            case 3:
                this.f93e = GlobalVars.scaleBitmap(this.f75a, R.drawable.garbage, ((float) this.e) * 0.32f, ((float) this.f) * 0.03f);
                this.n = GlobalVars.scaleBitmap(this.f75a, R.drawable.garbagebackground, (float) this.e, (float) this.f);
                break;
        }
        this.f76a.b(i2);
        int i3 = ((a) this.f76a.a.a(this.f74a.mode - 1).get(0)).a;
        this.f89c = i2 == 3 ? niceTime((long) i3) : String.valueOf(i3);
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i2, int i3, int i4) {
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        a();
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        this.f65a = 4;
        this.f71a.removeCallbacks(this.f79a);
    }
}
