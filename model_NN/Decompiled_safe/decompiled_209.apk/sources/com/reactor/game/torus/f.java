package com.reactor.game.torus;

class f {
    Best a = new Best();

    /* renamed from: a  reason: collision with other field name */
    GlobalVars f114a = new GlobalVars();

    /* renamed from: a  reason: collision with other field name */
    final /* synthetic */ TorusView f115a;

    f(TorusView torusView) {
        this.f115a = torusView;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        a(2);
        b(TorusView.a(this.f115a).mode);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.reactor.game.torus.TorusView.a(com.reactor.game.torus.TorusView, boolean):boolean
     arg types: [com.reactor.game.torus.TorusView, int]
     candidates:
      com.reactor.game.torus.TorusView.a(com.reactor.game.torus.TorusView, java.lang.String):java.lang.String
      com.reactor.game.torus.TorusView.a(int, int):void
      com.reactor.game.torus.TorusView.a(int, boolean):boolean
      com.reactor.game.torus.TorusView.a(com.reactor.game.torus.TorusView, boolean):boolean */
    /* access modifiers changed from: package-private */
    public void a(int i) {
        this.f115a.d();
        int floor = (int) Math.floor((double) TorusView.a(this.f115a).score);
        long j = TorusView.a(this.f115a).time;
        if (i != 2) {
            boolean unused = this.f115a.f86b = false;
            if (TorusView.a(this.f115a).mode == 1 && floor > ((a) this.a.a(0).get(2)).a) {
                boolean unused2 = this.f115a.f86b = true;
                this.f115a.a(floor, 1);
            }
            if (TorusView.a(this.f115a).mode == 2 && j > 301000 && floor > ((a) this.a.a(1).get(2)).a) {
                boolean unused3 = this.f115a.f86b = true;
                this.f115a.a(floor, 2);
            }
            if (TorusView.a(this.f115a).mode == 3 && i != 0 && j < ((long) (((a) this.a.a(2).get(2)).a * 1000))) {
                boolean unused4 = this.f115a.f86b = true;
                this.f115a.a((int) (j / 1000), 3);
            }
            TorusView.a(this.f115a).d();
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.f115a.c();
        TorusView.a(this.f115a).b();
    }

    /* access modifiers changed from: package-private */
    public void b(int i) {
        TorusView.a(this.f115a).setMode(i);
        String unused = this.f115a.f80a = i == 3 ? "+6" : String.valueOf(TorusView.a(this.f115a).score);
        this.f115a.start();
    }

    /* access modifiers changed from: package-private */
    public void c() {
        this.f115a.resume();
        TorusView.a(this.f115a).c();
    }
}
