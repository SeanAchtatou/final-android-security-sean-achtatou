package com.reactor.game.torus;

public final class R {

    public final class attr {
    }

    public final class drawable {
        public static final int base0 = 2130837504;
        public static final int best = 2130837505;
        public static final int bigbox = 2130837506;
        public static final int blocks = 2130837507;
        public static final int deselection = 2130837508;
        public static final int down = 2130837509;
        public static final int facebook = 2130837510;
        public static final int facebook_icon = 2130837511;
        public static final int gameover = 2130837512;
        public static final int garbage = 2130837513;
        public static final int garbagebackground = 2130837514;
        public static final int gogarbage = 2130837515;
        public static final int gotimer = 2130837516;
        public static final int gotraditional = 2130837517;
        public static final int help = 2130837518;
        public static final int highscores = 2130837519;
        public static final int icon = 2130837520;
        public static final int largebox = 2130837521;
        public static final int left = 2130837522;
        public static final int logo = 2130837523;
        public static final int mainmenu = 2130837524;
        public static final int next = 2130837525;
        public static final int panel = 2130837526;
        public static final int pause = 2130837527;
        public static final int paused = 2130837528;
        public static final int play = 2130837529;
        public static final int quit = 2130837530;
        public static final int restart = 2130837531;
        public static final int right = 2130837532;
        public static final int rotate = 2130837533;
        public static final int score = 2130837534;
        public static final int selection = 2130837535;
        public static final int settings = 2130837536;
        public static final int smallbox = 2130837537;
        public static final int smallplay = 2130837538;
        public static final int time = 2130837539;
        public static final int timeattack = 2130837540;
        public static final int timerbackground = 2130837541;
        public static final int tinybox = 2130837542;
        public static final int tip = 2130837543;
        public static final int traditional = 2130837544;
        public static final int traditionalbackground = 2130837545;
    }

    public final class id {
        public static final int aboutContent = 2131165186;
        public static final int aboutIcon = 2131165184;
        public static final int versionnum = 2131165185;
    }

    public final class layout {
        public static final int about_dlg = 2130903040;
        public static final int copyright = 2130903041;
    }

    public final class raw {
        public static final int bounce = 2131034112;
        public static final int clank = 2131034113;
        public static final int explode = 2131034114;
        public static final int rotate = 2131034115;
    }

    public final class string {
        public static final int Torus = 2131099649;
        public static final int app_name = 2131099648;
        public static final int link_text = 2131099653;
        public static final int livewallpaper_name = 2131099655;
        public static final int menu_about = 2131099652;
        public static final int menu_copyright = 2131099651;
        public static final int tetris_copy = 2131099654;
        public static final int torus_welcome = 2131099650;
        public static final int wallpaper_author = 2131099656;
        public static final int wallpaper_desc = 2131099657;
    }

    public final class xml {
        public static final int livewallpaper = 2130968576;
    }
}
