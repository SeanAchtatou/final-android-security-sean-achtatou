package com.reactor.game.torus;

import android.os.Environment;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Best {
    String a;

    /* renamed from: a  reason: collision with other field name */
    ArrayList f24a;

    Best() {
        a();
    }

    /* access modifiers changed from: package-private */
    public ArrayList a(int i) {
        return ((b) this.f24a.get(i)).f111a;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        ArrayList arrayList;
        IOException e;
        this.f24a = new ArrayList(3);
        if (new File("/sdcard/.torus").exists()) {
            this.a = "/sdcard/.torus";
        } else if (!new File("../data/data/com.reactor.game.torus/.torus").exists()) {
            if (Environment.getExternalStorageState().equals("mounted")) {
                this.a = GlobalVars.SDCARD;
            } else {
                this.a = GlobalVars.DATADIR;
            }
            this.a += GlobalVars.TORUSDIR;
            new File(this.a).mkdir();
        } else {
            this.a = "../data/data/com.reactor.game.torus/.torus";
        }
        ArrayList arrayList2 = null;
        for (int i = 0; i < 3; i++) {
            File file = new File(this.a + "/" + GlobalVars.highScoreFiles[i]);
            if (!file.exists()) {
                try {
                    if (file.createNewFile()) {
                    }
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
                switch (i) {
                    case 0:
                    case 1:
                        arrayList2 = new ArrayList(3);
                        for (int i2 = 0; i2 < 3; i2++) {
                            arrayList2.add(new a(this));
                        }
                        this.f24a.add(new b(this, arrayList2));
                        break;
                    case 2:
                        arrayList2 = new ArrayList(3);
                        for (int i3 = 0; i3 < 3; i3++) {
                            a aVar = new a(this);
                            aVar.a = 3599;
                            arrayList2.add(aVar);
                        }
                        this.f24a.add(new b(this, arrayList2));
                        break;
                }
                a(i, arrayList2);
            } else {
                try {
                    FileReader fileReader = new FileReader(file);
                    BufferedReader bufferedReader = new BufferedReader(fileReader);
                    ArrayList arrayList3 = new ArrayList();
                    try {
                        for (String readLine = bufferedReader.readLine(); readLine != null; readLine = bufferedReader.readLine()) {
                            a aVar2 = new a(this);
                            aVar2.a = Integer.parseInt(readLine);
                            aVar2.f110a = bufferedReader.readLine();
                            arrayList3.add(aVar2);
                        }
                        if (arrayList3.size() > 0) {
                            this.f24a.add(new b(this, arrayList3));
                        }
                        bufferedReader.close();
                        fileReader.close();
                        arrayList2 = arrayList3;
                    } catch (IOException e3) {
                        e = e3;
                        arrayList = arrayList3;
                        e.printStackTrace();
                        arrayList2 = arrayList;
                    }
                } catch (IOException e4) {
                    IOException iOException = e4;
                    arrayList = arrayList2;
                    e = iOException;
                    e.printStackTrace();
                    arrayList2 = arrayList;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i, ArrayList arrayList) {
        try {
            PrintWriter printWriter = new PrintWriter(new File(this.a + "/" + GlobalVars.highScoreFiles[i]));
            for (int i2 = 0; i2 < 3; i2++) {
                printWriter.println(String.valueOf(((a) arrayList.get(i2)).a));
                printWriter.println(((a) arrayList.get(i2)).f110a);
            }
            printWriter.flush();
            printWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
