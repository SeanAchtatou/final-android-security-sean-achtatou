package com.reactor.game.torus;

import android.content.DialogInterface;
import android.widget.EditText;
import java.util.ArrayList;

class d implements DialogInterface.OnClickListener {
    final /* synthetic */ int a;

    /* renamed from: a  reason: collision with other field name */
    final /* synthetic */ EditText f112a;

    /* renamed from: a  reason: collision with other field name */
    final /* synthetic */ TorusView f113a;
    final /* synthetic */ int b;

    d(TorusView torusView, EditText editText, int i, int i2) {
        this.f113a = torusView;
        this.f112a = editText;
        this.a = i;
        this.b = i2;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        String obj = this.f112a.getText().toString();
        ArrayList a2 = TorusView.a(this.f113a).a.a(this.a - 1);
        if (this.a != 3) {
            int i2 = 1;
            while (i2 >= 0 && ((a) a2.get(i2)).a < this.b) {
                ((a) a2.get(i2 + 1)).a = ((a) a2.get(i2)).a;
                ((a) a2.get(i2 + 1)).f110a = ((a) a2.get(i2)).f110a;
                i2--;
            }
            ((a) a2.get(i2 + 1)).a = this.b;
            ((a) a2.get(i2 + 1)).f110a = obj;
        } else {
            int i3 = 1;
            while (i3 >= 0 && ((a) a2.get(i3)).a > this.b) {
                ((a) a2.get(i3 + 1)).a = ((a) a2.get(i3)).a;
                ((a) a2.get(i3 + 1)).f110a = ((a) a2.get(i3)).f110a;
                i3--;
            }
            ((a) a2.get(i3 + 1)).a = this.b;
            ((a) a2.get(i3 + 1)).f110a = obj;
        }
        TorusView.a(this.f113a).a.a(this.a - 1, a2);
    }
}
