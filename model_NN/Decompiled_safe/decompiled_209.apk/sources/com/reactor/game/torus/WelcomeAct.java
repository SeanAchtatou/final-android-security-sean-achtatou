package com.reactor.game.torus;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;

public class WelcomeAct extends Activity {
    private WelcomeView a;

    /* renamed from: a  reason: collision with other field name */
    private boolean f101a = false;

    public class WelcomeView extends SurfaceView implements GestureDetector.OnGestureListener, SurfaceHolder.Callback {
        private int a = 480;

        /* renamed from: a  reason: collision with other field name */
        private Bitmap f102a;

        /* renamed from: a  reason: collision with other field name */
        private GestureDetector f103a = new GestureDetector(this);

        /* renamed from: a  reason: collision with other field name */
        private SurfaceHolder f104a;
        private int b = 854;

        /* renamed from: b  reason: collision with other field name */
        private Bitmap f106b;
        private int c = 0;

        /* renamed from: c  reason: collision with other field name */
        private Bitmap f107c;
        private int d = 0;

        /* renamed from: d  reason: collision with other field name */
        private Bitmap f108d;
        private Bitmap e;
        private Bitmap f;

        public WelcomeView(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            setFocusable(true);
            setLongClickable(true);
            this.f104a = getHolder();
            this.f104a.addCallback(this);
            new DisplayMetrics();
            DisplayMetrics displayMetrics = WelcomeAct.this.getApplicationContext().getResources().getDisplayMetrics();
            this.a = displayMetrics.widthPixels;
            this.b = displayMetrics.heightPixels;
            this.f102a = GlobalVars.scaleBitmap(context, R.drawable.traditionalbackground, (float) this.a, (float) this.b);
            this.f = GlobalVars.scaleBitmap(context, R.drawable.logo, 0.7f * ((float) this.a), 0.3f * ((float) this.b));
            this.f106b = GlobalVars.scaleBitmap(context, R.drawable.play, ((float) this.a) * 0.65f, ((float) this.b) * 0.14f);
            this.f107c = GlobalVars.scaleBitmap(context, R.drawable.settings, ((float) this.a) * 0.65f, ((float) this.b) * 0.14f);
            this.f108d = GlobalVars.scaleBitmap(context, R.drawable.highscores, ((float) this.a) * 0.65f, ((float) this.b) * 0.14f);
            this.e = GlobalVars.scaleBitmap(context, R.drawable.help, ((float) this.a) * 0.65f, ((float) this.b) * 0.14f);
            SharedPreferences sharedPreferences = WelcomeAct.this.getSharedPreferences("com.rector.game.torus_preferences", 0);
            GlobalVars.HELP = sharedPreferences.getBoolean("help", true);
            GlobalVars.KEYS = sharedPreferences.getBoolean("keys", true);
            GlobalVars.SOUND = sharedPreferences.getBoolean("sound", true);
        }

        /* access modifiers changed from: package-private */
        public void a() {
            Canvas lockCanvas = this.f104a.lockCanvas();
            if (lockCanvas != null) {
                lockCanvas.translate((float) (-this.c), (float) (-this.d));
                Paint paint = new Paint();
                paint.setStyle(Paint.Style.FILL);
                lockCanvas.drawBitmap(this.f102a, (Rect) null, new RectF((float) this.c, (float) this.d, (float) (this.a + this.c), (float) (this.b + this.d)), paint);
                float f2 = (float) (this.d + 41);
                lockCanvas.drawBitmap(this.f, (((float) (this.a - this.f.getWidth())) * 0.5f) + ((float) this.c), (float) (this.d + 41), paint);
                float width = (((float) (this.a - this.f106b.getWidth())) * 0.5f) + ((float) this.c);
                float height = ((float) this.f.getHeight()) + (((float) this.f106b.getHeight()) * 0.04f) + 41.0f + ((float) this.d);
                lockCanvas.drawBitmap(this.f106b, width, height, paint);
                float height2 = height + (((float) this.f107c.getHeight()) * 1.04f);
                lockCanvas.drawBitmap(this.f107c, width, height2, paint);
                float height3 = height2 + (((float) this.f107c.getHeight()) * 1.04f);
                lockCanvas.drawBitmap(this.f108d, width, height3, paint);
                lockCanvas.drawBitmap(this.e, width, height3 + (((float) this.f107c.getHeight()) * 1.04f), paint);
                this.f104a.unlockCanvasAndPost(lockCanvas);
            }
        }

        public boolean onDown(MotionEvent motionEvent) {
            float x = motionEvent.getX() + ((float) this.c);
            float y = motionEvent.getY() + ((float) this.d);
            if (x >= (((float) (this.a - this.f106b.getWidth())) * 0.5f) + ((float) this.c) && x <= (((float) (this.a + this.f106b.getWidth())) * 0.5f) + ((float) this.c)) {
                if (y >= ((float) this.f.getHeight()) + (((float) this.f106b.getHeight()) * 0.04f) + 41.0f + ((float) this.d) && y <= ((float) this.f.getHeight()) + (((float) this.f106b.getHeight()) * 1.04f) + 41.0f + ((float) this.d)) {
                    WelcomeAct.this.a();
                    return false;
                } else if (y >= ((float) this.f.getHeight()) + (((float) this.f106b.getHeight()) * 1.08f) + 41.0f + ((float) this.d) && y <= ((float) this.f.getHeight()) + (((float) this.f106b.getHeight()) * 2.08f) + 41.0f + ((float) this.d)) {
                    WelcomeAct.this.b();
                } else if (y >= ((float) this.f.getHeight()) + (((float) this.f106b.getHeight()) * 2.12f) + 41.0f + ((float) this.d) && y <= ((float) this.f.getHeight()) + (((float) this.f106b.getHeight()) * 3.12f) + 41.0f + ((float) this.d)) {
                    WelcomeAct.this.d();
                } else if (y >= ((float) this.f.getHeight()) + (((float) this.f106b.getHeight()) * 3.16f) + 41.0f + ((float) this.d) && y <= ((float) this.f.getHeight()) + (((float) this.f106b.getHeight()) * 4.16f) + 41.0f + ((float) this.d)) {
                    WelcomeAct.this.c();
                }
            }
            return false;
        }

        public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
            return false;
        }

        public void onLongPress(MotionEvent motionEvent) {
        }

        public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
            return false;
        }

        public void onShowPress(MotionEvent motionEvent) {
        }

        public boolean onSingleTapUp(MotionEvent motionEvent) {
            return false;
        }

        public boolean onTouchEvent(MotionEvent motionEvent) {
            this.f103a.onTouchEvent(motionEvent);
            return super.onTouchEvent(motionEvent);
        }

        public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        }

        public void surfaceCreated(SurfaceHolder surfaceHolder) {
            a();
        }

        public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        }
    }

    /* access modifiers changed from: private */
    public void a() {
        startActivity(new Intent(this, GoAct.class));
        this.f101a = false;
        finish();
    }

    /* access modifiers changed from: private */
    public void b() {
        startActivity(new Intent(this, SettingAct.class));
        this.f101a = false;
        finish();
    }

    /* access modifiers changed from: private */
    public void c() {
        startActivity(new Intent(this, HelpAct.class));
        this.f101a = false;
        finish();
    }

    /* access modifiers changed from: private */
    public void d() {
        startActivity(new Intent(this, HighScoresAct.class));
        this.f101a = false;
        finish();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        this.a = new WelcomeView(this, null);
        setContentView(this.a);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 1, 0, (int) R.string.menu_copyright);
        menu.add(0, 2, 0, (int) R.string.menu_about);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.f101a) {
            System.exit(0);
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (82 == i) {
            return false;
        }
        if (4 == i) {
            this.f101a = true;
            finish();
        }
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        LayoutInflater from = LayoutInflater.from(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        switch (menuItem.getItemId()) {
            case 1:
                View inflate = from.inflate((int) R.layout.copyright, (ViewGroup) null);
                builder.setTitle((int) R.string.menu_copyright);
                builder.setPositiveButton("OK", (DialogInterface.OnClickListener) null);
                builder.setView(inflate);
                builder.show();
                return true;
            case 2:
                View inflate2 = from.inflate((int) R.layout.about_dlg, (ViewGroup) null);
                builder.setTitle("About Tetris 3D");
                builder.setPositiveButton("OK", (DialogInterface.OnClickListener) null);
                builder.setView(inflate2);
                builder.show();
                return true;
            default:
                return false;
        }
    }
}
