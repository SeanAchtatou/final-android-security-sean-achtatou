package com.reactor.game.torus;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import java.util.HashMap;

public class SettingAct extends Activity {
    private AdView a;

    /* renamed from: a  reason: collision with other field name */
    private SettingView f55a;

    public class SettingView extends SurfaceView implements GestureDetector.OnGestureListener, SurfaceHolder.Callback {
        private int a = 480;

        /* renamed from: a  reason: collision with other field name */
        private Bitmap f56a;

        /* renamed from: a  reason: collision with other field name */
        private Typeface f57a;

        /* renamed from: a  reason: collision with other field name */
        private GestureDetector f58a = new GestureDetector(this);

        /* renamed from: a  reason: collision with other field name */
        private SurfaceHolder f59a;
        private int b = 800;

        /* renamed from: b  reason: collision with other field name */
        private Bitmap f61b;
        private int c = -159;

        /* renamed from: c  reason: collision with other field name */
        private Bitmap f62c;
        private int d = -183;

        /* renamed from: d  reason: collision with other field name */
        private Bitmap f63d;
        private Bitmap e;

        public SettingView(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            setFocusable(true);
            setLongClickable(true);
            this.f59a = getHolder();
            this.f59a.addCallback(this);
            this.f56a = BitmapFactory.decodeResource(context.getResources(), R.drawable.traditionalbackground);
            new DisplayMetrics();
            DisplayMetrics displayMetrics = SettingAct.this.getApplicationContext().getResources().getDisplayMetrics();
            this.a = displayMetrics.widthPixels;
            this.b = displayMetrics.heightPixels;
            this.f61b = GlobalVars.scaleBitmap(context, R.drawable.mainmenu, ((float) this.a) * 0.5f, ((float) this.b) * 0.1f);
            this.f62c = GlobalVars.scaleBitmap(context, R.drawable.largebox, ((float) this.a) * 0.82f, ((float) this.b) * 0.58f);
            this.f63d = GlobalVars.scaleBitmap(context, R.drawable.selection, ((float) this.a) * 0.09f, ((float) this.a) * 0.09f);
            this.e = GlobalVars.scaleBitmap(context, R.drawable.tinybox, ((float) this.a) * 0.1f, ((float) this.a) * 0.1f);
            this.f57a = Typeface.createFromAsset(context.getAssets(), "222-CAI978.ttf");
            this.d = this.b == 480 ? -138 : this.b == 533 ? -183 : -213;
        }

        /* access modifiers changed from: package-private */
        public void a() {
            Canvas lockCanvas = this.f59a.lockCanvas();
            if (lockCanvas != null) {
                lockCanvas.translate((float) (-this.c), (float) (-this.d));
                Paint paint = new Paint();
                paint.setAntiAlias(true);
                paint.setStyle(Paint.Style.FILL);
                paint.setColor(-16777216);
                lockCanvas.drawBitmap(this.f56a, (Rect) null, new RectF((float) this.c, (float) this.d, (float) (this.a + this.c), (float) (this.b + this.d)), paint);
                lockCanvas.drawBitmap(this.f62c, ((float) this.c) + (((float) (this.a - this.f62c.getWidth())) * 0.5f), ((float) this.d) + (((float) (this.b - this.f62c.getHeight())) * 0.5f), paint);
                lockCanvas.drawBitmap(this.f61b, ((float) this.c) + (((float) (this.a - this.f61b.getWidth())) * 0.5f), (float) (((this.d + this.b) - this.f61b.getHeight()) - 10), paint);
                paint.setTypeface(this.f57a);
                paint.setColor(-1);
                paint.setTextSize(22.0f);
                paint.setFakeBoldText(true);
                Paint.FontMetrics fontMetrics = paint.getFontMetrics();
                float width = ((float) this.c) + (((float) (this.a - this.f62c.getWidth())) * 0.5f);
                lockCanvas.drawText("Game Settings", width + 10.0f, ((float) Math.ceil((double) (fontMetrics.descent - fontMetrics.ascent))) + ((float) this.d) + (((float) (this.b - this.f62c.getHeight())) * 0.5f), paint);
                float height = (((float) this.d) + (((float) this.b) * 0.5f)) - (((float) this.f62c.getHeight()) * 0.35f);
                lockCanvas.drawBitmap(this.e, width + 15.0f, height, paint);
                lockCanvas.drawBitmap(this.e, width + 15.0f, (((float) this.a) * 0.15f) + height, paint);
                lockCanvas.drawBitmap(this.e, width + 15.0f, (0.3f * ((float) this.a)) + height, paint);
                paint.setAlpha(255);
                if (GlobalVars.HELP) {
                    lockCanvas.drawBitmap(this.f63d, width + 15.0f, height, paint);
                }
                if (GlobalVars.KEYS) {
                    lockCanvas.drawBitmap(this.f63d, width + 15.0f, (((float) this.a) * 0.15f) + height, paint);
                }
                if (GlobalVars.SOUND) {
                    lockCanvas.drawBitmap(this.f63d, width + 15.0f, (0.3f * ((float) this.a)) + height, paint);
                }
                paint.setColor(-1);
                paint.setTextSize(17.0f);
                paint.getFontMetrics(fontMetrics);
                paint.setFakeBoldText(false);
                paint.setStrokeWidth(0.0f);
                float ceil = (float) Math.ceil((double) (fontMetrics.descent - fontMetrics.ascent));
                lockCanvas.drawText("Show Tips", width + 15.0f + (((float) this.a) * 0.1f) + 10.0f, height + ceil, paint);
                lockCanvas.drawText("Show Keypad", width + 15.0f + (((float) this.a) * 0.1f) + 10.0f, (((float) this.a) * 0.15f) + height + ceil, paint);
                lockCanvas.drawText("Sound Effect", width + 15.0f + (((float) this.a) * 0.1f) + 10.0f, ceil + height + (0.3f * ((float) this.a)), paint);
                this.f59a.unlockCanvasAndPost(lockCanvas);
            }
        }

        public boolean onDown(MotionEvent motionEvent) {
            float x = motionEvent.getX() + ((float) this.c);
            float y = motionEvent.getY() + ((float) this.d);
            if (x >= ((float) this.c) + (((float) (this.a - this.f62c.getWidth())) * 0.5f) && x <= ((float) this.c) + (((float) this.a) * 0.5f) && y >= (((float) this.d) + (((float) this.b) * 0.5f)) - (((float) this.f62c.getHeight()) * 0.35f) && y <= ((((float) this.d) + (((float) this.b) * 0.5f)) - (((float) this.f62c.getHeight()) * 0.35f)) + (0.1f * ((float) this.a))) {
                GlobalVars.HELP = !GlobalVars.HELP;
                SharedPreferences.Editor edit = SettingAct.this.getSharedPreferences("com.rector.game.torus_preferences", 0).edit();
                edit.putBoolean("help", GlobalVars.HELP);
                edit.commit();
                a();
                return false;
            } else if (x >= ((float) this.c) + (((float) (this.a - this.f62c.getWidth())) * 0.5f) && x <= (((float) this.c) + (((float) this.a) * 0.5f)) - (((float) this.f62c.getWidth()) * 0.16f) && y >= ((((float) this.d) + (((float) this.b) * 0.5f)) - (((float) this.f62c.getHeight()) * 0.35f)) + (0.15f * ((float) this.a)) && y <= ((((float) this.d) + (((float) this.b) * 0.5f)) - (((float) this.f62c.getHeight()) * 0.35f)) + (0.25f * ((float) this.a))) {
                GlobalVars.KEYS = !GlobalVars.KEYS;
                SharedPreferences.Editor edit2 = SettingAct.this.getSharedPreferences("com.rector.game.torus_preferences", 0).edit();
                edit2.putBoolean("keys", GlobalVars.KEYS);
                edit2.commit();
                a();
                return false;
            } else if (x >= ((float) this.c) + (((float) (this.a - this.f62c.getWidth())) * 0.5f) && x <= (((float) this.c) + (((float) this.a) * 0.5f)) - (((float) this.f62c.getWidth()) * 0.16f) && y >= ((((float) this.d) + (((float) this.b) * 0.5f)) - (((float) this.f62c.getHeight()) * 0.35f)) + (0.3f * ((float) this.a)) && y <= ((((float) this.d) + (((float) this.b) * 0.5f)) - (((float) this.f62c.getHeight()) * 0.35f)) + (0.4f * ((float) this.a))) {
                GlobalVars.SOUND = !GlobalVars.SOUND;
                SharedPreferences.Editor edit3 = SettingAct.this.getSharedPreferences("com.rector.game.torus_preferences", 0).edit();
                edit3.putBoolean("sound", GlobalVars.SOUND);
                edit3.commit();
                a();
                return false;
            } else if (y < ((float) (((this.d + this.b) - this.f61b.getHeight()) - 10)) || y > ((float) ((this.d + this.b) - 10)) || x < ((float) this.c) + (((float) (this.a - this.f61b.getWidth())) * 0.5f) || x > ((float) this.c) + (((float) (this.a + this.f61b.getWidth())) * 0.5f)) {
                return false;
            } else {
                SettingAct.this.a();
                return false;
            }
        }

        public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
            return false;
        }

        public void onLongPress(MotionEvent motionEvent) {
        }

        public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
            return false;
        }

        public void onShowPress(MotionEvent motionEvent) {
        }

        public boolean onSingleTapUp(MotionEvent motionEvent) {
            return false;
        }

        public boolean onTouchEvent(MotionEvent motionEvent) {
            this.f58a.onTouchEvent(motionEvent);
            return super.onTouchEvent(motionEvent);
        }

        public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        }

        public void surfaceCreated(SurfaceHolder surfaceHolder) {
            a();
        }

        public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        }
    }

    /* access modifiers changed from: private */
    public void a() {
        startActivity(new Intent(this, WelcomeAct.class));
        finish();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        FrameLayout frameLayout = new FrameLayout(this);
        frameLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.f55a = new SettingView(this, null);
        frameLayout.addView(this.f55a);
        this.a = new AdView(this, AdSize.BANNER, "a14e16a52f18373");
        AdRequest adRequest = new AdRequest();
        HashMap hashMap = new HashMap();
        hashMap.put("color_bg", "1556a6");
        hashMap.put("color_bg_top", "1556a6");
        hashMap.put("color_border", "1556a6");
        hashMap.put("color_link", "FFFFFF");
        hashMap.put("color_text", "FFFFFF");
        hashMap.put("color_url", "FFFFFF");
        adRequest.setExtras(hashMap);
        this.a.loadAd(adRequest);
        frameLayout.addView(this.a);
        setContentView(frameLayout);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.a != null) {
            this.a.destroy();
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return true;
    }
}
