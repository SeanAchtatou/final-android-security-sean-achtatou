package com.reactor.game.torus;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class GoAct extends Activity {
    private GoView a;

    public class GoView extends SurfaceView implements GestureDetector.OnGestureListener, SurfaceHolder.Callback {
        private int a = 480;

        /* renamed from: a  reason: collision with other field name */
        private Bitmap f32a;

        /* renamed from: a  reason: collision with other field name */
        private Typeface f33a;

        /* renamed from: a  reason: collision with other field name */
        private GestureDetector f34a = new GestureDetector(this);

        /* renamed from: a  reason: collision with other field name */
        private SurfaceHolder f35a;
        private int b = 800;

        /* renamed from: b  reason: collision with other field name */
        private Bitmap f37b;
        private Bitmap c;
        private Bitmap d;
        private Bitmap e;
        private Bitmap f;
        private Bitmap g;
        private Bitmap h;
        private Bitmap i;

        public GoView(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            setFocusable(true);
            setLongClickable(true);
            this.f35a = getHolder();
            this.f35a.addCallback(this);
            new DisplayMetrics();
            DisplayMetrics displayMetrics = GoAct.this.getApplicationContext().getResources().getDisplayMetrics();
            this.a = displayMetrics.widthPixels;
            this.b = displayMetrics.heightPixels;
            this.f32a = BitmapFactory.decodeResource(context.getResources(), R.drawable.traditionalbackground);
            this.f37b = GlobalVars.scaleBitmap(context, R.drawable.logo, 0.7f * ((float) this.a), 0.3f * ((float) this.b));
            this.c = GlobalVars.scaleBitmap(context, R.drawable.bigbox, 0.71f * ((float) this.a), 0.2f * ((float) this.b));
            this.d = GlobalVars.scaleBitmap(context, R.drawable.traditional, ((float) this.a) * 0.32f, ((float) this.b) * 0.03f);
            this.e = GlobalVars.scaleBitmap(context, R.drawable.timeattack, ((float) this.a) * 0.32f, ((float) this.b) * 0.03f);
            this.f = GlobalVars.scaleBitmap(context, R.drawable.garbage, ((float) this.a) * 0.25f, ((float) this.b) * 0.03f);
            this.g = GlobalVars.scaleBitmap(context, R.drawable.gotraditional, ((float) this.a) * 0.17f, ((float) this.a) * 0.17f);
            this.i = GlobalVars.scaleBitmap(context, R.drawable.gogarbage, ((float) this.a) * 0.17f, ((float) this.a) * 0.17f);
            this.h = GlobalVars.scaleBitmap(context, R.drawable.gotimer, ((float) this.a) * 0.17f, ((float) this.a) * 0.17f);
            this.f33a = Typeface.createFromAsset(context.getAssets(), "222-CAI978.ttf");
        }

        /* access modifiers changed from: package-private */
        public void a() {
            Canvas lockCanvas = this.f35a.lockCanvas();
            if (lockCanvas != null) {
                Paint paint = new Paint();
                lockCanvas.drawBitmap(this.f32a, (Rect) null, new RectF(0.0f, 0.0f, (float) this.a, (float) this.b), paint);
                lockCanvas.drawBitmap(this.f37b, ((float) (this.a - this.f37b.getWidth())) / 2.0f, 41.0f, paint);
                float width = ((float) (this.a - this.c.getWidth())) / 2.0f;
                float height = 41.0f + ((float) this.f37b.getHeight());
                float height2 = (((float) (this.b - (this.c.getHeight() * 3))) - height) * 0.25f;
                float height3 = ((float) this.c.getHeight()) / 9.0f;
                float f2 = height + height2;
                lockCanvas.drawBitmap(this.c, width, f2, paint);
                lockCanvas.drawBitmap(this.g, width + height3, f2 + height3, paint);
                lockCanvas.drawBitmap(this.d, width + height3 + ((float) this.g.getWidth()), f2 + height3, paint);
                String helpContent = GlobalVars.getHelpContent("traditional.txt", GoAct.this);
                paint.setTextSize(12.0f);
                paint.setStyle(Paint.Style.STROKE);
                paint.setTypeface(this.f33a);
                paint.setColor(-1);
                Paint.FontMetrics fontMetrics = paint.getFontMetrics();
                float ceil = (float) Math.ceil((double) (fontMetrics.descent - fontMetrics.ascent));
                float f3 = ceil / 10.0f;
                float width2 = ((((float) this.a) - (2.0f * width)) - (2.5f * height3)) - ((float) this.g.getWidth());
                float width3 = width + height3 + ((float) this.g.getWidth());
                GlobalVars.drawContent(helpContent, ceil, f3, width2, width3, ((float) this.d.getHeight()) + f2 + height3, lockCanvas, paint);
                float height4 = f2 + ((float) this.c.getHeight()) + height2;
                lockCanvas.drawBitmap(this.c, width, height4, paint);
                lockCanvas.drawBitmap(this.h, width + height3, height4 + height3, paint);
                lockCanvas.drawBitmap(this.e, width + height3 + ((float) this.g.getWidth()), height4 + height3, paint);
                GlobalVars.drawContent(GlobalVars.getHelpContent("time.txt", GoAct.this), ceil, f3, width2, width3, height4 + height3 + ((float) this.e.getHeight()), lockCanvas, paint);
                float height5 = height4 + ((float) this.c.getHeight()) + height2;
                lockCanvas.drawBitmap(this.c, width, height5, paint);
                lockCanvas.drawBitmap(this.i, width + height3, height5 + height3, paint);
                lockCanvas.drawBitmap(this.f, width + height3 + ((float) this.g.getWidth()), height5 + height3, paint);
                GlobalVars.drawContent(GlobalVars.getHelpContent("garbage.txt", GoAct.this), ceil, f3, width2, width3, height5 + height3 + ((float) this.f.getHeight()), lockCanvas, paint);
                this.f35a.unlockCanvasAndPost(lockCanvas);
            }
        }

        public boolean onDown(MotionEvent motionEvent) {
            float x = motionEvent.getX();
            float y = motionEvent.getY();
            float height = 41.0f + ((float) this.f37b.getHeight());
            float height2 = (((float) (this.b - (this.c.getHeight() * 3))) - height) * 0.25f;
            if (x < ((float) (this.a - this.c.getWidth())) / 2.0f || x > ((float) this.a) + (((float) this.c.getWidth()) / 2.0f)) {
                return false;
            }
            if (y >= height + height2 && y <= height + height2 + ((float) this.c.getHeight())) {
                GoAct.this.a(1);
                return false;
            } else if (y >= ((float) this.c.getHeight()) + height + (2.0f * height2) && y <= ((((float) this.c.getHeight()) + height2) * 2.0f) + height) {
                GoAct.this.a(2);
                return false;
            } else if (y < (((float) this.c.getHeight()) * 2.0f) + height + (3.0f * height2) || y > (4.0f * ((float) this.c.getHeight())) + height + (3.0f * height2)) {
                return false;
            } else {
                GoAct.this.a(3);
                return false;
            }
        }

        public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
            return false;
        }

        public void onLongPress(MotionEvent motionEvent) {
        }

        public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
            return false;
        }

        public void onShowPress(MotionEvent motionEvent) {
        }

        public boolean onSingleTapUp(MotionEvent motionEvent) {
            return false;
        }

        public boolean onTouchEvent(MotionEvent motionEvent) {
            this.f34a.onTouchEvent(motionEvent);
            return super.onTouchEvent(motionEvent);
        }

        public void surfaceChanged(SurfaceHolder surfaceHolder, int i2, int i3, int i4) {
        }

        public void surfaceCreated(SurfaceHolder surfaceHolder) {
            a();
        }

        public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i) {
        Intent intent = new Intent(this, TorusAct.class);
        intent.putExtra("com.reactor.game.torus.Mode", i);
        startActivity(intent);
        finish();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        this.a = new GoView(this, null);
        setContentView(this.a);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (4 != i) {
            return true;
        }
        startActivity(new Intent(this, WelcomeAct.class));
        finish();
        return true;
    }
}
