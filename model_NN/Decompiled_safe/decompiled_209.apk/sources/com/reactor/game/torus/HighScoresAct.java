package com.reactor.game.torus;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import java.util.ArrayList;
import java.util.HashMap;

public class HighScoresAct extends Activity {
    private AdView a;

    /* renamed from: a  reason: collision with other field name */
    private HighScoresView f45a;

    public class HighScoresView extends SurfaceView implements GestureDetector.OnGestureListener, SurfaceHolder.Callback {
        private int a = 1;

        /* renamed from: a  reason: collision with other field name */
        private Bitmap f46a;

        /* renamed from: a  reason: collision with other field name */
        private Typeface f47a;

        /* renamed from: a  reason: collision with other field name */
        private GestureDetector f48a = new GestureDetector(this);

        /* renamed from: a  reason: collision with other field name */
        private SurfaceHolder f49a;

        /* renamed from: a  reason: collision with other field name */
        private Best f50a = new Best();

        /* renamed from: a  reason: collision with other field name */
        private final String[] f52a = {"Traditional", "Attack time", "Garbage"};
        private int b = 480;

        /* renamed from: b  reason: collision with other field name */
        private Bitmap f53b;
        private int c = 800;

        /* renamed from: c  reason: collision with other field name */
        private Bitmap f54c;
        private Bitmap d;
        private Bitmap e;
        private Bitmap f;

        public HighScoresView(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            setFocusable(true);
            setLongClickable(true);
            this.f49a = getHolder();
            this.f49a.addCallback(this);
            this.f46a = BitmapFactory.decodeResource(context.getResources(), R.drawable.traditionalbackground);
            new DisplayMetrics();
            DisplayMetrics displayMetrics = HighScoresAct.this.getApplicationContext().getResources().getDisplayMetrics();
            this.b = displayMetrics.widthPixels;
            this.c = displayMetrics.heightPixels;
            this.f54c = GlobalVars.scaleBitmap(context, R.drawable.bigbox, ((float) this.b) * 0.9f, ((float) this.c) * 0.2f);
            this.f53b = GlobalVars.scaleBitmap(context, R.drawable.mainmenu, ((float) this.b) * 0.35f, ((float) this.c) * 0.08f);
            this.d = BitmapFactory.decodeResource(context.getResources(), R.drawable.traditional);
            this.e = BitmapFactory.decodeResource(context.getResources(), R.drawable.timeattack);
            this.f = BitmapFactory.decodeResource(context.getResources(), R.drawable.garbage);
            this.f47a = Typeface.createFromAsset(context.getAssets(), "222-CAI978.ttf");
        }

        /* access modifiers changed from: package-private */
        public void a(ArrayList arrayList, String str) {
            Canvas lockCanvas = this.f49a.lockCanvas();
            if (lockCanvas != null) {
                Paint paint = new Paint();
                lockCanvas.drawBitmap(this.f46a, (Rect) null, new RectF(0.0f, 0.0f, (float) this.b, (float) this.c), paint);
                lockCanvas.drawBitmap(this.f53b, ((float) (this.b - this.f53b.getWidth())) * 0.5f, ((float) this.c) * 0.91f, paint);
                float f2 = 0.0f;
                float width = ((float) (this.b - this.f54c.getWidth())) * 0.5f;
                if (this.a == 1) {
                    f2 = (0.3f * ((float) this.c)) - (((float) this.f54c.getHeight()) * 0.5f);
                    lockCanvas.drawBitmap(this.f54c, ((float) (this.b - this.f54c.getWidth())) * 0.5f, f2, paint);
                    lockCanvas.drawBitmap(this.d, (Rect) null, new RectF(((0.13f * ((float) this.b)) + (((float) this.f54c.getWidth()) * 0.5f)) - 10.0f, 0.28f * ((float) this.c), (((float) (this.b + this.f54c.getWidth())) * 0.5f) - 10.0f, 0.32f * ((float) this.c)), paint);
                } else {
                    lockCanvas.drawBitmap(this.d, (Rect) null, new RectF(((0.18f * ((float) this.b)) + (0.5f * ((float) this.f54c.getWidth()))) - 10.0f, 0.285f * ((float) this.c), (((float) (this.b + this.f54c.getWidth())) * 0.5f) - 10.0f, 0.315f * ((float) this.c)), paint);
                }
                if (this.a == 2) {
                    f2 = (0.5f * ((float) this.c)) - (((float) this.f54c.getHeight()) * 0.5f);
                    lockCanvas.drawBitmap(this.f54c, ((float) (this.b - this.f54c.getWidth())) * 0.5f, f2, paint);
                    lockCanvas.drawBitmap(this.e, (Rect) null, new RectF(((0.13f * ((float) this.b)) + (((float) this.f54c.getWidth()) * 0.5f)) - 10.0f, 0.48f * ((float) this.c), (((float) (this.b + this.f54c.getWidth())) * 0.5f) - 10.0f, 0.52f * ((float) this.c)), paint);
                } else {
                    lockCanvas.drawBitmap(this.e, (Rect) null, new RectF(((0.18f * ((float) this.b)) + (0.5f * ((float) this.f54c.getWidth()))) - 10.0f, 0.485f * ((float) this.c), (((float) (this.b + this.f54c.getWidth())) * 0.5f) - 10.0f, 0.515f * ((float) this.c)), paint);
                }
                if (this.a == 3) {
                    f2 = (0.7f * ((float) this.c)) - (((float) this.f54c.getHeight()) * 0.5f);
                    lockCanvas.drawBitmap(this.f54c, ((float) (this.b - this.f54c.getWidth())) * 0.5f, f2, paint);
                    lockCanvas.drawBitmap(this.f, (Rect) null, new RectF(((0.2f * ((float) this.b)) + (((float) this.f54c.getWidth()) * 0.5f)) - 10.0f, 0.68f * ((float) this.c), (((float) (this.b + this.f54c.getWidth())) * 0.5f) - 10.0f, 0.72f * ((float) this.c)), paint);
                } else {
                    lockCanvas.drawBitmap(this.f, (Rect) null, new RectF(((0.25f * ((float) this.b)) + (((float) this.f54c.getWidth()) * 0.5f)) - 10.0f, 0.685f * ((float) this.c), (((float) (this.b + this.f54c.getWidth())) * 0.5f) - 10.0f, 0.715f * ((float) this.c)), paint);
                }
                paint.setTypeface(this.f47a);
                paint.setColor(-1);
                paint.setFakeBoldText(true);
                paint.setTextSize(20.0f);
                Paint.FontMetrics fontMetrics = paint.getFontMetrics();
                float ceil = f2 + ((float) Math.ceil((double) (fontMetrics.descent - fontMetrics.ascent)));
                lockCanvas.drawText(str, 10.0f + width, ceil, paint);
                paint.setTextSize(15.0f);
                Paint.FontMetrics fontMetrics2 = paint.getFontMetrics();
                float ceil2 = (float) Math.ceil((double) (fontMetrics2.descent - fontMetrics2.ascent));
                float f3 = ceil + ceil2;
                if (this.a != 3) {
                    for (int i = 0; i < 3; i++) {
                        lockCanvas.drawText(((i + 1) + ". ") + ((a) arrayList.get(i)).f110a + " ( " + String.valueOf(((a) arrayList.get(i)).a) + " )", 8.0f + width, (((float) i) * ceil2) + f3, paint);
                    }
                } else {
                    for (int i2 = 0; i2 < 3; i2++) {
                        int i3 = ((a) arrayList.get(i2)).a;
                        lockCanvas.drawText(((i2 + 1) + ". ") + ((a) arrayList.get(i2)).f110a + " ( " + String.valueOf(i3 / 60) + " : " + String.valueOf(i3 % 60) + ")", 8.0f + width, (((float) i2) * ceil2) + f3, paint);
                    }
                }
                this.f49a.unlockCanvasAndPost(lockCanvas);
            }
        }

        public boolean onDown(MotionEvent motionEvent) {
            float x = motionEvent.getX();
            float y = motionEvent.getY();
            if (x >= ((0.13f * ((float) this.b)) + (((float) this.f54c.getWidth()) * 0.5f)) - 10.0f && x <= (((float) (this.b + this.f54c.getWidth())) * 0.5f) - 10.0f) {
                if (y >= 0.22f * ((float) this.c) && y <= 0.38f * ((float) this.c)) {
                    this.a = 1;
                    a(this.f50a.a(0), this.f52a[this.a - 1]);
                    return false;
                } else if (y >= 0.42f * ((float) this.c) && y <= 0.58f * ((float) this.c)) {
                    this.a = 2;
                    a(this.f50a.a(1), this.f52a[this.a - 1]);
                    return false;
                }
            }
            if (x < ((0.2f * ((float) this.b)) + (((float) this.f54c.getWidth()) * 0.5f)) - 10.0f || x > (((float) (this.b + this.f54c.getWidth())) * 0.5f) - 10.0f || y < 0.62f * ((float) this.c) || y > 0.78f * ((float) this.c)) {
                if (x >= ((float) (this.b - this.f53b.getWidth())) * 0.5f && x <= ((float) (this.b + this.f53b.getWidth())) * 0.5f && y >= ((float) this.c) * 0.91f && y <= (((float) this.c) * 0.91f) + ((float) this.f53b.getHeight())) {
                    HighScoresAct.this.a();
                }
                return false;
            }
            this.a = 3;
            a(this.f50a.a(2), this.f52a[this.a - 1]);
            return false;
        }

        public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
            return false;
        }

        public void onLongPress(MotionEvent motionEvent) {
        }

        public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
            return false;
        }

        public void onShowPress(MotionEvent motionEvent) {
        }

        public boolean onSingleTapUp(MotionEvent motionEvent) {
            return false;
        }

        public boolean onTouchEvent(MotionEvent motionEvent) {
            this.f48a.onTouchEvent(motionEvent);
            return super.onTouchEvent(motionEvent);
        }

        public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        }

        public void surfaceCreated(SurfaceHolder surfaceHolder) {
            a(this.f50a.a(this.a - 1), this.f52a[this.a - 1]);
        }

        public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        }
    }

    /* access modifiers changed from: private */
    public void a() {
        startActivity(new Intent(this, WelcomeAct.class));
        finish();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        FrameLayout frameLayout = new FrameLayout(this);
        frameLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.f45a = new HighScoresView(this, null);
        frameLayout.addView(this.f45a);
        this.a = new AdView(this, AdSize.BANNER, "a14e16a52f18373");
        AdRequest adRequest = new AdRequest();
        HashMap hashMap = new HashMap();
        hashMap.put("color_bg", "1556a6");
        hashMap.put("color_bg_top", "1556a6");
        hashMap.put("color_border", "1556a6");
        hashMap.put("color_link", "FFFFFF");
        hashMap.put("color_text", "FFFFFF");
        hashMap.put("color_url", "FFFFFF");
        adRequest.setExtras(hashMap);
        this.a.loadAd(adRequest);
        frameLayout.addView(this.a);
        setContentView(frameLayout);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.a != null) {
            this.a.destroy();
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return true;
    }
}
