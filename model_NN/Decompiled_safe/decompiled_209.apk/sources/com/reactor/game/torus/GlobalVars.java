package com.reactor.game.torus;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import java.io.IOException;
import java.io.InputStream;

public class GlobalVars {
    public static final String DATADIR = "../data/data/com.reactor.game.torus";
    public static boolean HELP = true;
    public static boolean KEYS = true;
    public static boolean KEYSPOS = true;
    public static final String SDCARD = "/sdcard";
    public static boolean SOUND = true;
    public static final String TORUSDIR = "/.torus";
    public static final String[] highScoreFiles = {"traditional.txt", "time.txt", "garbage.txt"};

    public static float drawContent(String str, float f, float f2, float f3, float f4, float f5, Canvas canvas, Paint paint) {
        String substring;
        int length = str.length();
        float f6 = f5;
        int i = 0;
        int i2 = 0;
        float f7 = 0.0f;
        while (i < length) {
            f7 += paint.measureText(String.valueOf(str.charAt(i)));
            if (i + 1 < length && paint.measureText(String.valueOf(str.charAt(i + 1))) + f7 > f3) {
                if (str.charAt(i) == ' ' || str.charAt(i + 1) == ' ') {
                    f7 = 0.0f;
                    substring = str.substring(i2, i + 1);
                    i2 = i + 1;
                } else if (str.charAt(i - 1) == ' ') {
                    f7 = 0.0f;
                    substring = str.substring(i2, i);
                    i2 = i;
                    i--;
                } else {
                    f7 = 0.0f;
                    substring = str.substring(i2, i) + "-";
                    i2 = i;
                    i--;
                }
                f6 += f2 + f;
                canvas.drawText(substring, f4, f6, paint);
            }
            i++;
        }
        if (f7 == 0.0f) {
            return f6;
        }
        float f8 = f + f2 + f6;
        canvas.drawText(str.substring(i2, i), f4, f8, paint);
        return f8;
    }

    public static String getHelpContent(String str, Context context) {
        try {
            InputStream open = context.getAssets().open(str);
            int available = open.available();
            byte[] bArr = new byte[available];
            open.read(bArr, 0, available);
            open.close();
            return new String(bArr);
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap scaleBitmap(Context context, int i, float f, float f2) {
        Bitmap decodeResource = BitmapFactory.decodeResource(context.getResources(), i);
        int width = decodeResource.getWidth();
        int height = decodeResource.getHeight();
        Matrix matrix = new Matrix();
        matrix.postScale(f / ((float) width), f2 / ((float) height));
        return Bitmap.createBitmap(decodeResource, 0, 0, width, height, matrix, true);
    }
}
