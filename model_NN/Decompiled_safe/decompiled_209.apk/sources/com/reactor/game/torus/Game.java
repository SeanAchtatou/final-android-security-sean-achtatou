package com.reactor.game.torus;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import java.util.ArrayList;

public class Game {
    private float a = 40.0f;

    /* renamed from: a  reason: collision with other field name */
    private int f27a = 0;

    /* renamed from: a  reason: collision with other field name */
    private Paint f28a = new Paint();

    /* renamed from: a  reason: collision with other field name */
    private boolean f29a = false;

    /* renamed from: a  reason: collision with other field name */
    float[] f30a = new float[4];
    public ArrayList block = null;
    public ArrayList blockData = null;
    public float[] dropPositions = new float[4];
    public int[][][] field;
    public int lines = 0;
    public int mode = 1;
    public int nextType = 0;
    public int pieceCount;
    public float pieceSpawnTime = 0.0f;
    public float posY;
    public int score = 0;
    public long time = 0;
    public int type;
    public CubicMotion viewPort = new CubicMotion(250);
    public float x = -2.0f;

    public class BlockData {
        float a;
        public ArrayList coords;
        public int theme;
        public float view;

        BlockData(float f, int i, float f2, ArrayList arrayList) {
            this.view = f;
            this.theme = i;
            this.a = f2;
            this.coords = arrayList;
        }
    }

    public class Coord {
        public float x;
        public float y;

        Coord(float f, float f2) {
            this.x = f;
            this.y = f2;
        }
    }

    public class Coords {
        public ArrayList block;

        Coords(ArrayList arrayList) {
            this.block = arrayList;
        }
    }

    public Game() {
        this.f28a.setStrokeCap(Paint.Cap.ROUND);
        this.f28a.setStrokeJoin(Paint.Join.ROUND);
        this.f28a.setStrokeWidth(0.7f);
        this.f28a.setStyle(Paint.Style.STROKE);
        this.f28a.setColor(-502197999);
        this.f28a.setAntiAlias(true);
        b();
        this.dropPositions[3] = -138.0f;
    }

    private void b() {
        this.blockData = new ArrayList();
        ArrayList arrayList = new ArrayList();
        arrayList.add(new Coord(1.0f, 0.0f));
        arrayList.add(new Coord(2.0f, 0.0f));
        arrayList.add(new Coord(1.0f, 1.0f));
        arrayList.add(new Coord(2.0f, 1.0f));
        ArrayList arrayList2 = new ArrayList();
        arrayList2.add(new Coords(arrayList));
        this.blockData.add(new BlockData(0.08f, 0, 1.0f, arrayList2));
        ArrayList arrayList3 = new ArrayList();
        arrayList3.add(new Coord(0.0f, 0.0f));
        arrayList3.add(new Coord(1.0f, 0.0f));
        arrayList3.add(new Coord(2.0f, 0.0f));
        arrayList3.add(new Coord(3.0f, 0.0f));
        ArrayList arrayList4 = new ArrayList();
        arrayList4.add(new Coords(arrayList3));
        ArrayList arrayList5 = new ArrayList();
        arrayList5.add(new Coord(1.0f, -1.0f));
        arrayList5.add(new Coord(1.0f, 0.0f));
        arrayList5.add(new Coord(1.0f, 1.0f));
        arrayList5.add(new Coord(1.0f, 2.0f));
        arrayList4.add(new Coords(arrayList5));
        this.blockData.add(new BlockData(0.25f, 1, 1.0f, arrayList4));
        ArrayList arrayList6 = new ArrayList();
        arrayList6.add(new Coord(1.0f, 1.0f));
        arrayList6.add(new Coord(0.0f, 0.0f));
        arrayList6.add(new Coord(1.0f, 0.0f));
        arrayList6.add(new Coord(2.0f, 0.0f));
        ArrayList arrayList7 = new ArrayList();
        arrayList7.add(new Coords(arrayList6));
        ArrayList arrayList8 = new ArrayList();
        arrayList8.add(new Coord(1.0f, 1.0f));
        arrayList8.add(new Coord(2.0f, 0.0f));
        arrayList8.add(new Coord(1.0f, -1.0f));
        arrayList8.add(new Coord(1.0f, 0.0f));
        arrayList7.add(new Coords(arrayList8));
        ArrayList arrayList9 = new ArrayList();
        arrayList9.add(new Coord(2.0f, 0.0f));
        arrayList9.add(new Coord(1.0f, -1.0f));
        arrayList9.add(new Coord(0.0f, 0.0f));
        arrayList9.add(new Coord(1.0f, 0.0f));
        arrayList7.add(new Coords(arrayList9));
        ArrayList arrayList10 = new ArrayList();
        arrayList10.add(new Coord(1.0f, 1.0f));
        arrayList10.add(new Coord(0.0f, 0.0f));
        arrayList10.add(new Coord(1.0f, -1.0f));
        arrayList10.add(new Coord(1.0f, 0.0f));
        arrayList7.add(new Coords(arrayList10));
        this.blockData.add(new BlockData(0.5f, 2, 1.0f, arrayList7));
        ArrayList arrayList11 = new ArrayList();
        arrayList11.add(new Coord(0.0f, 0.0f));
        arrayList11.add(new Coord(1.0f, 0.0f));
        arrayList11.add(new Coord(2.0f, 0.0f));
        arrayList11.add(new Coord(0.0f, 1.0f));
        ArrayList arrayList12 = new ArrayList();
        arrayList12.add(new Coords(arrayList11));
        ArrayList arrayList13 = new ArrayList();
        arrayList13.add(new Coord(1.0f, -1.0f));
        arrayList13.add(new Coord(1.0f, 0.0f));
        arrayList13.add(new Coord(1.0f, 1.0f));
        arrayList13.add(new Coord(2.0f, 1.0f));
        arrayList12.add(new Coords(arrayList13));
        ArrayList arrayList14 = new ArrayList();
        arrayList14.add(new Coord(0.0f, 0.0f));
        arrayList14.add(new Coord(2.0f, 0.0f));
        arrayList14.add(new Coord(1.0f, 0.0f));
        arrayList14.add(new Coord(2.0f, -1.0f));
        arrayList12.add(new Coords(arrayList14));
        ArrayList arrayList15 = new ArrayList();
        arrayList15.add(new Coord(1.0f, -1.0f));
        arrayList15.add(new Coord(1.0f, 0.0f));
        arrayList15.add(new Coord(1.0f, 1.0f));
        arrayList15.add(new Coord(0.0f, -1.0f));
        arrayList12.add(new Coords(arrayList15));
        this.blockData.add(new BlockData(0.5f, 3, 1.0f, arrayList12));
        ArrayList arrayList16 = new ArrayList();
        arrayList16.add(new Coord(2.0f, 0.0f));
        arrayList16.add(new Coord(1.0f, 0.0f));
        arrayList16.add(new Coord(0.0f, 0.0f));
        arrayList16.add(new Coord(2.0f, 1.0f));
        ArrayList arrayList17 = new ArrayList();
        arrayList17.add(new Coords(arrayList16));
        ArrayList arrayList18 = new ArrayList();
        arrayList18.add(new Coord(1.0f, -1.0f));
        arrayList18.add(new Coord(1.0f, 0.0f));
        arrayList18.add(new Coord(1.0f, 1.0f));
        arrayList18.add(new Coord(2.0f, -1.0f));
        arrayList17.add(new Coords(arrayList18));
        ArrayList arrayList19 = new ArrayList();
        arrayList19.add(new Coord(0.0f, 0.0f));
        arrayList19.add(new Coord(1.0f, 0.0f));
        arrayList19.add(new Coord(2.0f, 0.0f));
        arrayList19.add(new Coord(0.0f, -1.0f));
        arrayList17.add(new Coords(arrayList19));
        ArrayList arrayList20 = new ArrayList();
        arrayList20.add(new Coord(1.0f, -1.0f));
        arrayList20.add(new Coord(1.0f, 0.0f));
        arrayList20.add(new Coord(1.0f, 1.0f));
        arrayList20.add(new Coord(0.0f, 1.0f));
        arrayList17.add(new Coords(arrayList20));
        this.blockData.add(new BlockData(0.5f, 4, 1.0f, arrayList17));
        ArrayList arrayList21 = new ArrayList();
        arrayList21.add(new Coord(2.0f, 1.0f));
        arrayList21.add(new Coord(1.0f, 1.0f));
        arrayList21.add(new Coord(1.0f, 0.0f));
        arrayList21.add(new Coord(0.0f, 0.0f));
        ArrayList arrayList22 = new ArrayList();
        arrayList22.add(new Coords(arrayList21));
        ArrayList arrayList23 = new ArrayList();
        arrayList23.add(new Coord(1.0f, 1.0f));
        arrayList23.add(new Coord(1.0f, 0.0f));
        arrayList23.add(new Coord(0.0f, 2.0f));
        arrayList23.add(new Coord(0.0f, 1.0f));
        arrayList22.add(new Coords(arrayList23));
        this.blockData.add(new BlockData(0.5f, 5, 1.0f, arrayList22));
        ArrayList arrayList24 = new ArrayList();
        arrayList24.add(new Coord(1.0f, 1.0f));
        arrayList24.add(new Coord(0.0f, 1.0f));
        arrayList24.add(new Coord(2.0f, 0.0f));
        arrayList24.add(new Coord(1.0f, 0.0f));
        ArrayList arrayList25 = new ArrayList();
        arrayList25.add(new Coords(arrayList24));
        ArrayList arrayList26 = new ArrayList();
        arrayList26.add(new Coord(1.0f, 1.0f));
        arrayList26.add(new Coord(1.0f, 0.0f));
        arrayList26.add(new Coord(2.0f, 2.0f));
        arrayList26.add(new Coord(2.0f, 1.0f));
        arrayList25.add(new Coords(arrayList26));
        this.blockData.add(new BlockData(0.5f, 6, 1.0f, arrayList25));
        ArrayList arrayList27 = new ArrayList();
        arrayList27.add(new Coord(1.0f, 1.0f));
        arrayList27.add(new Coord(2.0f, 1.0f));
        arrayList27.add(new Coord(1.0f, 0.0f));
        arrayList27.add(new Coord(2.0f, 0.0f));
        arrayList27.add(new Coord(3.0f, 0.0f));
        ArrayList arrayList28 = new ArrayList();
        arrayList28.add(new Coords(arrayList27));
        ArrayList arrayList29 = new ArrayList();
        arrayList29.add(new Coord(1.0f, 1.0f));
        arrayList29.add(new Coord(2.0f, 1.0f));
        arrayList29.add(new Coord(1.0f, 0.0f));
        arrayList29.add(new Coord(2.0f, 0.0f));
        arrayList29.add(new Coord(1.0f, -1.0f));
        arrayList28.add(new Coords(arrayList29));
        ArrayList arrayList30 = new ArrayList();
        arrayList30.add(new Coord(1.0f, 1.0f));
        arrayList30.add(new Coord(2.0f, 1.0f));
        arrayList30.add(new Coord(1.0f, 0.0f));
        arrayList30.add(new Coord(2.0f, 0.0f));
        arrayList30.add(new Coord(0.0f, 1.0f));
        arrayList28.add(new Coords(arrayList30));
        ArrayList arrayList31 = new ArrayList();
        arrayList31.add(new Coord(2.0f, 2.0f));
        arrayList31.add(new Coord(1.0f, 1.0f));
        arrayList31.add(new Coord(2.0f, 1.0f));
        arrayList31.add(new Coord(1.0f, 0.0f));
        arrayList31.add(new Coord(2.0f, 0.0f));
        arrayList28.add(new Coords(arrayList31));
        this.blockData.add(new BlockData(-0.15f, 0, 0.025f, arrayList28));
        ArrayList arrayList32 = new ArrayList();
        arrayList32.add(new Coord(1.0f, 1.0f));
        arrayList32.add(new Coord(2.0f, 1.0f));
        arrayList32.add(new Coord(1.0f, 0.0f));
        arrayList32.add(new Coord(2.0f, 0.0f));
        arrayList32.add(new Coord(0.0f, 0.0f));
        ArrayList arrayList33 = new ArrayList();
        arrayList33.add(new Coords(arrayList32));
        ArrayList arrayList34 = new ArrayList();
        arrayList34.add(new Coord(1.0f, 2.0f));
        arrayList34.add(new Coord(1.0f, 1.0f));
        arrayList34.add(new Coord(2.0f, 1.0f));
        arrayList34.add(new Coord(1.0f, 0.0f));
        arrayList34.add(new Coord(2.0f, 0.0f));
        arrayList33.add(new Coords(arrayList34));
        ArrayList arrayList35 = new ArrayList();
        arrayList35.add(new Coord(1.0f, 1.0f));
        arrayList35.add(new Coord(3.0f, 1.0f));
        arrayList35.add(new Coord(2.0f, 1.0f));
        arrayList35.add(new Coord(1.0f, 0.0f));
        arrayList35.add(new Coord(2.0f, 0.0f));
        arrayList33.add(new Coords(arrayList35));
        ArrayList arrayList36 = new ArrayList();
        arrayList36.add(new Coord(1.0f, 1.0f));
        arrayList36.add(new Coord(2.0f, 1.0f));
        arrayList36.add(new Coord(1.0f, 0.0f));
        arrayList36.add(new Coord(2.0f, 0.0f));
        arrayList36.add(new Coord(2.0f, -1.0f));
        arrayList33.add(new Coords(arrayList36));
        this.blockData.add(new BlockData(0.15f, 0, 0.025f, arrayList33));
        ArrayList arrayList37 = new ArrayList();
        arrayList37.add(new Coord(-1.0f, 0.0f));
        arrayList37.add(new Coord(0.0f, 0.0f));
        arrayList37.add(new Coord(1.0f, 0.0f));
        arrayList37.add(new Coord(2.0f, 0.0f));
        arrayList37.add(new Coord(3.0f, 0.0f));
        ArrayList arrayList38 = new ArrayList();
        arrayList38.add(new Coords(arrayList37));
        ArrayList arrayList39 = new ArrayList();
        arrayList39.add(new Coord(1.0f, -2.0f));
        arrayList39.add(new Coord(1.0f, -1.0f));
        arrayList39.add(new Coord(1.0f, 0.0f));
        arrayList39.add(new Coord(1.0f, 1.0f));
        arrayList39.add(new Coord(1.0f, 2.0f));
        arrayList38.add(new Coords(arrayList39));
        this.blockData.add(new BlockData(0.5f, 1, 0.025f, arrayList38));
        ArrayList arrayList40 = new ArrayList();
        arrayList40.add(new Coord(0.0f, 0.0f));
        arrayList40.add(new Coord(1.0f, 0.0f));
        arrayList40.add(new Coord(2.0f, 0.0f));
        ArrayList arrayList41 = new ArrayList();
        arrayList41.add(new Coords(arrayList40));
        ArrayList arrayList42 = new ArrayList();
        arrayList42.add(new Coord(1.0f, 1.0f));
        arrayList42.add(new Coord(1.0f, 0.0f));
        arrayList42.add(new Coord(1.0f, -1.0f));
        arrayList41.add(new Coords(arrayList42));
        this.blockData.add(new BlockData(0.5f, 1, 0.025f, arrayList41));
        ArrayList arrayList43 = new ArrayList();
        arrayList43.add(new Coord(1.0f, 2.0f));
        arrayList43.add(new Coord(0.0f, 1.0f));
        arrayList43.add(new Coord(1.0f, 1.0f));
        arrayList43.add(new Coord(2.0f, 1.0f));
        arrayList43.add(new Coord(1.0f, 0.0f));
        ArrayList arrayList44 = new ArrayList();
        arrayList44.add(new Coords(arrayList43));
        this.blockData.add(new BlockData(0.5f, 2, 0.05f, arrayList44));
        ArrayList arrayList45 = new ArrayList();
        arrayList45.add(new Coord(1.0f, 1.0f));
        arrayList45.add(new Coord(1.0f, 0.0f));
        arrayList45.add(new Coord(2.0f, 0.0f));
        ArrayList arrayList46 = new ArrayList();
        arrayList46.add(new Coords(arrayList45));
        ArrayList arrayList47 = new ArrayList();
        arrayList47.add(new Coord(1.0f, 1.0f));
        arrayList47.add(new Coord(2.0f, 1.0f));
        arrayList47.add(new Coord(1.0f, 0.0f));
        arrayList46.add(new Coords(arrayList47));
        ArrayList arrayList48 = new ArrayList();
        arrayList48.add(new Coord(1.0f, 1.0f));
        arrayList48.add(new Coord(2.0f, 1.0f));
        arrayList48.add(new Coord(2.0f, 0.0f));
        arrayList46.add(new Coords(arrayList48));
        ArrayList arrayList49 = new ArrayList();
        arrayList49.add(new Coord(2.0f, 1.0f));
        arrayList49.add(new Coord(1.0f, 0.0f));
        arrayList49.add(new Coord(2.0f, 0.0f));
        arrayList46.add(new Coords(arrayList49));
        this.blockData.add(new BlockData(0.25f, 3, 0.05f, arrayList46));
        ArrayList arrayList50 = new ArrayList();
        arrayList50.add(new Coord(2.0f, 1.0f));
        arrayList50.add(new Coord(1.0f, 0.0f));
        arrayList50.add(new Coord(2.0f, 0.0f));
        ArrayList arrayList51 = new ArrayList();
        arrayList51.add(new Coords(arrayList50));
        ArrayList arrayList52 = new ArrayList();
        arrayList52.add(new Coord(1.0f, 1.0f));
        arrayList52.add(new Coord(1.0f, 0.0f));
        arrayList52.add(new Coord(2.0f, 0.0f));
        arrayList51.add(new Coords(arrayList52));
        ArrayList arrayList53 = new ArrayList();
        arrayList53.add(new Coord(1.0f, 1.0f));
        arrayList53.add(new Coord(2.0f, 1.0f));
        arrayList53.add(new Coord(1.0f, 0.0f));
        arrayList51.add(new Coords(arrayList53));
        ArrayList arrayList54 = new ArrayList();
        arrayList54.add(new Coord(1.0f, 1.0f));
        arrayList54.add(new Coord(2.0f, 1.0f));
        arrayList54.add(new Coord(2.0f, 0.0f));
        arrayList51.add(new Coords(arrayList54));
        this.blockData.add(new BlockData(0.25f, 4, 0.05f, arrayList51));
        ArrayList arrayList55 = new ArrayList();
        arrayList55.add(new Coord(2.0f, 2.0f));
        arrayList55.add(new Coord(1.0f, 2.0f));
        arrayList55.add(new Coord(1.0f, 1.0f));
        arrayList55.add(new Coord(1.0f, 0.0f));
        arrayList55.add(new Coord(0.0f, 0.0f));
        ArrayList arrayList56 = new ArrayList();
        arrayList56.add(new Coords(arrayList55));
        ArrayList arrayList57 = new ArrayList();
        arrayList57.add(new Coord(0.0f, 2.0f));
        arrayList57.add(new Coord(0.0f, 1.0f));
        arrayList57.add(new Coord(1.0f, 1.0f));
        arrayList57.add(new Coord(2.0f, 1.0f));
        arrayList57.add(new Coord(2.0f, 0.0f));
        arrayList56.add(new Coords(arrayList57));
        this.blockData.add(new BlockData(0.5f, 5, 0.025f, arrayList56));
        ArrayList arrayList58 = new ArrayList();
        arrayList58.add(new Coord(0.0f, 2.0f));
        arrayList58.add(new Coord(1.0f, 2.0f));
        arrayList58.add(new Coord(1.0f, 1.0f));
        arrayList58.add(new Coord(1.0f, 0.0f));
        arrayList58.add(new Coord(2.0f, 0.0f));
        ArrayList arrayList59 = new ArrayList();
        arrayList59.add(new Coords(arrayList58));
        ArrayList arrayList60 = new ArrayList();
        arrayList60.add(new Coord(2.0f, 2.0f));
        arrayList60.add(new Coord(2.0f, 1.0f));
        arrayList60.add(new Coord(1.0f, 1.0f));
        arrayList60.add(new Coord(0.0f, 1.0f));
        arrayList60.add(new Coord(0.0f, 0.0f));
        arrayList59.add(new Coords(arrayList60));
        this.blockData.add(new BlockData(0.5f, 6, 0.025f, arrayList59));
        ArrayList arrayList61 = new ArrayList();
        arrayList61.add(new Coord(0.0f, 1.0f));
        arrayList61.add(new Coord(2.0f, 1.0f));
        arrayList61.add(new Coord(1.0f, 0.0f));
        arrayList61.add(new Coord(0.0f, 0.0f));
        arrayList61.add(new Coord(2.0f, 0.0f));
        ArrayList arrayList62 = new ArrayList();
        arrayList62.add(new Coords(arrayList61));
        ArrayList arrayList63 = new ArrayList();
        arrayList63.add(new Coord(1.0f, 1.0f));
        arrayList63.add(new Coord(2.0f, 1.0f));
        arrayList63.add(new Coord(1.0f, 0.0f));
        arrayList63.add(new Coord(1.0f, -1.0f));
        arrayList63.add(new Coord(2.0f, -1.0f));
        arrayList62.add(new Coords(arrayList63));
        ArrayList arrayList64 = new ArrayList();
        arrayList64.add(new Coord(1.0f, 0.0f));
        arrayList64.add(new Coord(0.0f, 0.0f));
        arrayList64.add(new Coord(2.0f, 0.0f));
        arrayList64.add(new Coord(0.0f, -1.0f));
        arrayList64.add(new Coord(2.0f, -1.0f));
        arrayList62.add(new Coords(arrayList64));
        ArrayList arrayList65 = new ArrayList();
        arrayList65.add(new Coord(1.0f, 1.0f));
        arrayList65.add(new Coord(0.0f, 1.0f));
        arrayList65.add(new Coord(1.0f, 0.0f));
        arrayList65.add(new Coord(1.0f, -1.0f));
        arrayList65.add(new Coord(0.0f, -1.0f));
        arrayList62.add(new Coords(arrayList65));
        this.blockData.add(new BlockData(0.5f, 7, 0.025f, arrayList62));
        ArrayList arrayList66 = new ArrayList();
        arrayList66.add(new Coord(1.0f, 2.0f));
        arrayList66.add(new Coord(0.0f, 2.0f));
        arrayList66.add(new Coord(2.0f, 2.0f));
        arrayList66.add(new Coord(1.0f, 1.0f));
        arrayList66.add(new Coord(1.0f, 0.0f));
        ArrayList arrayList67 = new ArrayList();
        arrayList67.add(new Coords(arrayList66));
        ArrayList arrayList68 = new ArrayList();
        arrayList68.add(new Coord(2.0f, 2.0f));
        arrayList68.add(new Coord(1.0f, 1.0f));
        arrayList68.add(new Coord(2.0f, 1.0f));
        arrayList68.add(new Coord(0.0f, 1.0f));
        arrayList68.add(new Coord(2.0f, 0.0f));
        arrayList67.add(new Coords(arrayList68));
        ArrayList arrayList69 = new ArrayList();
        arrayList69.add(new Coord(1.0f, 2.0f));
        arrayList69.add(new Coord(1.0f, 1.0f));
        arrayList69.add(new Coord(1.0f, 0.0f));
        arrayList69.add(new Coord(0.0f, 0.0f));
        arrayList69.add(new Coord(2.0f, 0.0f));
        arrayList67.add(new Coords(arrayList69));
        ArrayList arrayList70 = new ArrayList();
        arrayList70.add(new Coord(0.0f, 2.0f));
        arrayList70.add(new Coord(1.0f, 1.0f));
        arrayList70.add(new Coord(0.0f, 1.0f));
        arrayList70.add(new Coord(2.0f, 1.0f));
        arrayList70.add(new Coord(0.0f, 0.0f));
        arrayList67.add(new Coords(arrayList70));
        this.blockData.add(new BlockData(0.5f, 7, 0.025f, arrayList67));
        ArrayList arrayList71 = new ArrayList();
        arrayList71.add(new Coord(1.0f, 0.0f));
        ArrayList arrayList72 = new ArrayList();
        arrayList72.add(new Coords(arrayList71));
        this.blockData.add(new BlockData(0.5f, 7, 0.05f, arrayList72));
        ArrayList arrayList73 = new ArrayList();
        arrayList73.add(new Coord(1.0f, 0.0f));
        arrayList73.add(new Coord(0.0f, 0.0f));
        arrayList73.add(new Coord(14.0f, 0.0f));
        arrayList73.add(new Coord(2.0f, 0.0f));
        arrayList73.add(new Coord(13.0f, 0.0f));
        arrayList73.add(new Coord(3.0f, 0.0f));
        arrayList73.add(new Coord(12.0f, 0.0f));
        arrayList73.add(new Coord(4.0f, 0.0f));
        arrayList73.add(new Coord(11.0f, 0.0f));
        arrayList73.add(new Coord(5.0f, 0.0f));
        arrayList73.add(new Coord(10.0f, 0.0f));
        arrayList73.add(new Coord(6.0f, 0.0f));
        arrayList73.add(new Coord(9.0f, 0.0f));
        arrayList73.add(new Coord(7.0f, 0.0f));
        arrayList73.add(new Coord(8.0f, 0.0f));
        ArrayList arrayList74 = new ArrayList();
        arrayList74.add(new Coords(arrayList73));
        this.blockData.add(new BlockData(0.5f, 7, 0.01f, arrayList74));
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.blockData.size()) {
                this.f27a = (int) (((BlockData) this.blockData.get(i2)).a + ((float) this.f27a));
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int a() {
        float floor = (float) Math.floor((double) this.posY);
        if (this.block.size() == 1) {
            for (int i = 0; i < 15; i++) {
                if (this.field[(int) ((((((Coord) this.block.get(0)).x + this.x) % 15.0f) + 15.0f) % 15.0f)][i] == null) {
                    return i;
                }
            }
        }
        int i2 = 0;
        loop1:
        while (true) {
            for (int size = this.block.size() - 1; size >= 0; size--) {
                if ((((Coord) this.block.get(size)).y + floor) - ((float) i2) < 0.0f) {
                    break loop1;
                }
                if ((((Coord) this.block.get(size)).y + floor) - ((float) i2) < 15.0f) {
                    if (this.field[(int) ((((((Coord) this.block.get(size)).x + this.x) % 15.0f) + 15.0f) % 15.0f)][(int) ((((Coord) this.block.get(size)).y + floor) - ((float) i2))] != null) {
                        break loop1;
                    }
                }
            }
            i2++;
        }
        return (int) ((floor - ((float) i2)) + 1.0f);
    }

    /* access modifiers changed from: package-private */
    public int a(int i, float f, float f2) {
        int i2 = i < 20 ? ((BlockData) this.blockData.get(i)).theme : i;
        float f3 = f2 <= 0.0f ? 0.0f : f2;
        switch (i2) {
            case 0:
                return Color.rgb((int) (f * 255.0f), (int) (f * f3), (int) (f3 * f));
            case 1:
                return Color.rgb((int) (f * f3), (int) (255.0f * f), (int) (f3 * f));
            case 2:
                return Color.rgb((int) (f * f3), (int) (f3 * f), (int) (255.0f * f));
            case 3:
                return Color.rgb((int) (f * 255.0f), (int) (255.0f * f), (int) (f3 * f));
            case 4:
                return Color.rgb((int) (f * 255.0f), (int) (f3 * f), (int) (255.0f * f));
            case 5:
                return Color.rgb((int) (f * f3), (int) (f * 255.0f), (int) (255.0f * f));
            case 6:
                return Color.rgb((int) (f * 220.0f), (int) (f * 220.0f), (int) (f * 220.0f));
            case 20:
                return Color.rgb((int) (f * 100.0f), (int) (f * 100.0f), (int) (f * 100.0f));
            case 30:
                return Color.rgb((int) (f * 190.0f), (int) (f * 190.0f), 0);
            default:
                return Color.rgb((int) (f * 220.0f), (int) (f * 220.0f), (int) (f * 220.0f));
        }
    }

    /* access modifiers changed from: package-private */
    public Coord a(float f, float f2, boolean z, float f3) {
        float f4 = z ? this.a : 60.0f;
        return new Coord((float) (((((double) f4) * Math.cos(6.283185307179586d * ((double) (((f - f3) / 15.0f) - 0.25f)))) * ((double) (f2 + 30.0f))) / ((double) 30.0f)), (float) (((double) (200.0f - (((20.0f * f2) * ((f2 / 2.0f) + 60.0f)) / 60.0f))) - ((0.3d * ((double) f4)) * Math.sin(6.283185307179586d * ((double) (((f - f3) / 15.0f) - 0.25f))))));
    }

    /* access modifiers changed from: package-private */
    public ArrayList a(Canvas canvas, Paint paint, float f, float f2, boolean z, float f3, boolean z2, boolean z3, boolean z4, boolean z5) {
        paint.setAntiAlias(true);
        Path path = new Path();
        ArrayList arrayList = new ArrayList();
        arrayList.add(a((float) (((double) f) - 0.015d), (float) (((double) f2) + 0.015d), z, f3));
        arrayList.add(a((float) (((double) f) + 1.015d), (float) (((double) f2) + 0.015d), z, f3));
        arrayList.add(a((float) (((double) f) + 1.015d), (float) (((double) f2) - 1.015d), z, f3));
        arrayList.add(a((float) (((double) f) - 0.015d), (float) (((double) f2) - 1.015d), z, f3));
        path.moveTo(((Coord) arrayList.get(0)).x, ((Coord) arrayList.get(0)).y);
        path.lineTo(((Coord) arrayList.get(1)).x, ((Coord) arrayList.get(1)).y);
        path.lineTo(((Coord) arrayList.get(2)).x, ((Coord) arrayList.get(2)).y);
        path.lineTo(((Coord) arrayList.get(3)).x, ((Coord) arrayList.get(3)).y);
        path.close();
        paint.setStyle(Paint.Style.FILL);
        canvas.drawPath(path, paint);
        if (z4 || z5 || z2 || z3) {
            path.reset();
            if (z4) {
                path.moveTo(((Coord) arrayList.get(1)).x, ((Coord) arrayList.get(1)).y);
            } else {
                path.moveTo(((Coord) arrayList.get(0)).x, ((Coord) arrayList.get(0)).y);
                path.lineTo(((Coord) arrayList.get(1)).x, ((Coord) arrayList.get(1)).y);
            }
            if (z3) {
                path.moveTo(((Coord) arrayList.get(2)).x, ((Coord) arrayList.get(2)).y);
            } else {
                path.lineTo(((Coord) arrayList.get(2)).x, ((Coord) arrayList.get(2)).y);
            }
            if (z5) {
                path.moveTo(((Coord) arrayList.get(3)).x, ((Coord) arrayList.get(3)).y);
            } else {
                path.lineTo(((Coord) arrayList.get(3)).x, ((Coord) arrayList.get(3)).y);
            }
            if (!z2) {
                path.lineTo(((Coord) arrayList.get(0)).x, ((Coord) arrayList.get(0)).y);
            }
        }
        canvas.drawPath(path, this.f28a);
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a  reason: collision with other method in class */
    public void m1a() {
        this.x = 0.0f;
        this.field = new int[15][][];
        for (int i = 0; i < 15; i++) {
            this.field[i] = new int[15][];
        }
        this.viewPort.m0a();
        this.lines = 0;
        this.score = 0;
        if (this.mode == 1) {
            int[][][] iArr = this.field;
            iArr[10][0] = new int[2];
            iArr[10][0][0] = 1;
            iArr[10][0][1] = 1;
            iArr[10][1] = new int[2];
            iArr[10][1][0] = 1;
            iArr[10][1][1] = 1;
            iArr[11][0] = new int[2];
            iArr[11][0][0] = 1;
            iArr[11][0][1] = 1;
            iArr[11][1] = new int[2];
            iArr[11][1][0] = 1;
            iArr[11][1][1] = 1;
            for (int i2 = 0; i2 < 4; i2++) {
                iArr[9][i2] = new int[2];
                iArr[9][i2][0] = 2;
                iArr[9][i2][1] = 2;
            }
            iArr[8][0] = new int[2];
            iArr[8][0][0] = 3;
            iArr[8][0][1] = 3;
            for (int i3 = 0; i3 < 2; i3++) {
                iArr[7][i3] = new int[2];
                iArr[7][i3][0] = 3;
                iArr[7][i3][1] = 3;
            }
            iArr[6][0] = new int[2];
            iArr[6][0][0] = 3;
            iArr[6][0][1] = 3;
            iArr[12][0] = new int[2];
            iArr[12][0][0] = 4;
            iArr[12][0][1] = 4;
            for (int i4 = 0; i4 < 3; i4++) {
                iArr[13][i4] = new int[2];
                iArr[13][i4][0] = 4;
                iArr[13][i4][1] = 4;
            }
            for (int i5 = 3; i5 < 6; i5++) {
                iArr[i5][0] = new int[2];
                iArr[i5][0][0] = 5;
                iArr[i5][0][1] = 5;
            }
            iArr[5][1] = new int[2];
            iArr[5][1][0] = 5;
            iArr[5][1][1] = 5;
            iArr[1][0] = new int[2];
            iArr[1][0][0] = 6;
            iArr[1][0][1] = 6;
            for (int i6 = 0; i6 < 2; i6++) {
                iArr[2][i6] = new int[2];
                iArr[2][i6][0] = 6;
                iArr[2][i6][1] = 6;
            }
            iArr[3][1] = new int[2];
            iArr[3][1][0] = 6;
            iArr[3][1][1] = 6;
            for (int i7 = 2; i7 < 4; i7++) {
                iArr[7][i7] = new int[2];
                iArr[7][i7][0] = 7;
                iArr[7][i7][1] = 7;
            }
            for (int i8 = 1; i8 < 3; i8++) {
                iArr[6][i8] = new int[2];
                iArr[6][i8][0] = 7;
                iArr[6][i8][1] = 7;
            }
            this.pieceCount = 8;
        }
        if (this.mode == 2) {
            int[][][] iArr2 = this.field;
            for (int i9 = 0; i9 < 13; i9 += 3) {
                iArr2[i9][0] = new int[2];
                iArr2[i9][0][0] = (int) ((Math.random() * 6.0d) + 1.0d);
                iArr2[i9][0][1] = 0;
            }
            this.pieceCount = 1;
        }
        if (this.mode == 3) {
            for (int i10 = 0; i10 < 9; i10++) {
                int i11 = 0;
                while (i11 < 5) {
                    int random = (int) (Math.random() * 15.0d);
                    if (this.field[random][i10] != null) {
                        i11--;
                    } else {
                        this.field[random][i10] = new int[2];
                        this.field[random][i10][0] = 1;
                        this.field[random][i10][1] = 0;
                    }
                    i11++;
                }
            }
            this.pieceCount = 1;
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x04f3, code lost:
        if (r0.field[(int) (((((((com.reactor.game.torus.Game.Coord) r0.block.get(r25)).x + r0.x) + 1.0f) % 15.0f) + 15.0f) % 15.0f)][r5] == null) goto L_0x04f5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x05e5, code lost:
        if (r0.field[(int) (((((((com.reactor.game.torus.Game.Coord) r0.block.get(r25)).x + r0.x) - 1.0f) % 15.0f) + 15.0f) % 15.0f)][r5] == null) goto L_0x05e7;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(android.graphics.Canvas r27, android.graphics.Paint r28) {
        /*
            r26 = this;
            r4 = 1
            r0 = r28
            r1 = r4
            r0.setAntiAlias(r1)
            r0 = r26
            com.reactor.game.torus.CubicMotion r0 = r0.viewPort
            r4 = r0
            float r4 = r4.a()
            r5 = 1097859072(0x41700000, float:15.0)
            float r4 = r4 % r5
            r5 = 1097859072(0x41700000, float:15.0)
            float r4 = r4 + r5
            r5 = 1097859072(0x41700000, float:15.0)
            float r10 = r4 % r5
            r0 = r26
            com.reactor.game.torus.CubicMotion r0 = r0.viewPort
            r4 = r0
            float r4 = r4.mTarget
            r5 = 1097859072(0x41700000, float:15.0)
            float r4 = r4 % r5
            r5 = 1097859072(0x41700000, float:15.0)
            float r4 = r4 + r5
            r5 = 1097859072(0x41700000, float:15.0)
            float r20 = r4 % r5
            r0 = r26
            float r0 = r0.posY
            r4 = r0
            double r4 = (double) r4
            double r4 = java.lang.Math.floor(r4)
            r0 = r4
            float r0 = (float) r0
            r23 = r0
            r15 = 1061997773(0x3f4ccccd, float:0.8)
            android.graphics.Paint$Style r4 = android.graphics.Paint.Style.FILL
            r0 = r28
            r1 = r4
            r0.setStyle(r1)
            r0 = r26
            int r0 = r0.type
            r4 = r0
            r5 = 1124859904(0x430c0000, float:140.0)
            r0 = r26
            r1 = r4
            r2 = r15
            r3 = r5
            int r4 = r0.a(r1, r2, r3)
            r0 = r28
            r1 = r4
            r0.setColor(r1)
            r16 = 1065353216(0x3f800000, float:1.0)
            java.util.ArrayList r24 = new java.util.ArrayList
            r24.<init>()
            int r17 = r26.a()
            r0 = r26
            float r0 = r0.a
            r4 = r0
            r5 = 0
            int r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
            if (r4 != 0) goto L_0x0076
            r4 = 1109655552(0x42240000, float:41.0)
            r0 = r4
            r1 = r26
            r1.a = r0
        L_0x0076:
            r4 = 4600877379321698714(0x3fd999999999999a, double:0.4)
            r0 = r16
            double r0 = (double) r0
            r6 = r0
            double r4 = r4 * r6
            r6 = 4643176031446892544(0x406fe00000000000, double:255.0)
            double r4 = r4 * r6
            int r4 = (int) r4
            r0 = r28
            r1 = r4
            r0.setAlpha(r1)
            r4 = 0
            r18 = r4
        L_0x0090:
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            int r4 = r4.size()
            r0 = r18
            r1 = r4
            if (r0 >= r1) goto L_0x02c6
            r4 = 0
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r5 = r0
            int r5 = r5.size()
            r6 = 1
            int r5 = r5 - r6
            r19 = r4
            r21 = r4
        L_0x00ae:
            if (r5 < 0) goto L_0x014e
            r0 = r5
            r1 = r18
            if (r0 == r1) goto L_0x0730
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            java.lang.Object r4 = r4.get(r5)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r6 = r4.x
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r18
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.x
            int r4 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r4 != 0) goto L_0x0105
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            java.lang.Object r4 = r4.get(r5)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r6 = r4.y
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r18
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.y
            r7 = 1065353216(0x3f800000, float:1.0)
            float r4 = r4 - r7
            int r4 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r4 != 0) goto L_0x0105
            r4 = 1
            r6 = r4
            r4 = r19
        L_0x00fe:
            int r5 = r5 + -1
            r19 = r4
            r21 = r6
            goto L_0x00ae
        L_0x0105:
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            java.lang.Object r4 = r4.get(r5)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r6 = r4.y
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r18
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.y
            int r4 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r4 != 0) goto L_0x0730
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            java.lang.Object r4 = r4.get(r5)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r6 = r4.x
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r18
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.x
            r7 = 1065353216(0x3f800000, float:1.0)
            float r4 = r4 + r7
            int r4 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r4 != 0) goto L_0x0730
            r4 = 1
            r6 = r21
            goto L_0x00fe
        L_0x014e:
            com.reactor.game.torus.Game$Coords r22 = new com.reactor.game.torus.Game$Coords
            r0 = r26
            float r0 = r0.x
            r5 = r0
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r18
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.x
            float r7 = r5 + r4
            r0 = r17
            float r0 = (float) r0
            r5 = r0
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r18
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.y
            float r8 = r5 + r4
            r9 = 0
            r11 = 0
            r12 = 0
            r13 = 0
            r14 = 0
            r4 = r26
            r5 = r27
            r6 = r28
            java.util.ArrayList r4 = r4.a(r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)
            r0 = r22
            r1 = r26
            r2 = r4
            r0.<init>(r2)
            r0 = r24
            r1 = r22
            r0.add(r1)
            r0 = r26
            float r0 = r0.x
            r5 = r0
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r18
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.x
            float r8 = r5 + r4
            r0 = r17
            float r0 = (float) r0
            r5 = r0
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r18
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.y
            float r9 = r5 + r4
            r11 = 0
            r12 = 0
            r13 = 0
            r5 = r26
            r6 = r27
            r7 = r28
            r5.a(r6, r7, r8, r9, r10, r11, r12, r13)
            r0 = r26
            float r0 = r0.x
            r5 = r0
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r18
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.x
            float r8 = r5 + r4
            r0 = r17
            float r0 = (float) r0
            r5 = r0
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r18
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.y
            float r9 = r5 + r4
            r11 = 0
            r12 = 0
            r13 = 0
            r5 = r26
            r6 = r27
            r7 = r28
            r5.a(r6, r7, r8, r9, r10, r11, r12, r13)
            r0 = r26
            float r0 = r0.x
            r5 = r0
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r18
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.x
            float r8 = r5 + r4
            r0 = r17
            float r0 = (float) r0
            r5 = r0
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r18
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.y
            float r9 = r5 + r4
            r11 = 0
            r12 = 0
            r5 = r26
            r6 = r27
            r7 = r28
            r5.a(r6, r7, r8, r9, r10, r11, r12)
            if (r19 != 0) goto L_0x0282
            r0 = r26
            float r0 = r0.x
            r5 = r0
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r18
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.x
            float r4 = r4 + r5
            r5 = 1065353216(0x3f800000, float:1.0)
            float r8 = r4 + r5
            r0 = r17
            float r0 = (float) r0
            r5 = r0
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r18
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.y
            float r9 = r5 + r4
            r11 = 0
            r12 = 0
            r5 = r26
            r6 = r27
            r7 = r28
            r5.a(r6, r7, r8, r9, r10, r11, r12)
        L_0x0282:
            if (r21 != 0) goto L_0x02c0
            r0 = r26
            float r0 = r0.x
            r5 = r0
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r18
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.x
            float r8 = r5 + r4
            r0 = r17
            float r0 = (float) r0
            r5 = r0
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r18
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.y
            float r4 = r4 + r5
            r5 = 1065353216(0x3f800000, float:1.0)
            float r9 = r4 - r5
            r11 = 0
            r12 = 0
            r13 = 0
            r5 = r26
            r6 = r27
            r7 = r28
            r5.a(r6, r7, r8, r9, r10, r11, r12, r13)
        L_0x02c0:
            int r4 = r18 + 1
            r18 = r4
            goto L_0x0090
        L_0x02c6:
            r0 = r26
            float r0 = r0.a
            r4 = r0
            r5 = 1109655552(0x42240000, float:41.0)
            int r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
            if (r4 != 0) goto L_0x02d7
            r4 = 0
            r0 = r4
            r1 = r26
            r1.a = r0
        L_0x02d7:
            r0 = r26
            float[] r0 = r0.f30a
            r4 = r0
            r0 = r26
            r1 = r24
            r2 = r4
            r0.a(r1, r2)
            android.graphics.Paint$Style r4 = android.graphics.Paint.Style.FILL
            r0 = r28
            r1 = r4
            r0.setStyle(r1)
            r0 = r26
            int r0 = r0.type
            r4 = r0
            r5 = 0
            r0 = r26
            r1 = r4
            r2 = r15
            r3 = r5
            int r4 = r0.a(r1, r2, r3)
            r0 = r28
            r1 = r4
            r0.setColor(r1)
            r4 = 4606281698874543309(0x3feccccccccccccd, double:0.9)
            r0 = r16
            double r0 = (double) r0
            r6 = r0
            double r4 = r4 * r6
            r6 = 4643176031446892544(0x406fe00000000000, double:255.0)
            double r4 = r4 * r6
            int r4 = (int) r4
            r0 = r28
            r1 = r4
            r0.setAlpha(r1)
            r24.clear()
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            int r4 = r4.size()
            r5 = 1
            int r4 = r4 - r5
            r25 = r4
        L_0x0328:
            if (r25 < 0) goto L_0x071c
            r4 = 0
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r5 = r0
            int r5 = r5.size()
            r6 = 1
            int r5 = r5 - r6
            r12 = r4
            r11 = r4
            r14 = r4
            r13 = r4
        L_0x033a:
            if (r5 < 0) goto L_0x0437
            r0 = r5
            r1 = r25
            if (r0 == r1) goto L_0x072a
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            java.lang.Object r4 = r4.get(r5)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r6 = r4.x
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r25
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.x
            int r4 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r4 != 0) goto L_0x03bc
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            java.lang.Object r4 = r4.get(r5)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r6 = r4.y
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r25
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.y
            r7 = 1065353216(0x3f800000, float:1.0)
            float r4 = r4 + r7
            int r4 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r4 != 0) goto L_0x0392
            r4 = 1
            r6 = r11
            r7 = r14
            r8 = r4
            r4 = r12
        L_0x038b:
            int r5 = r5 + -1
            r12 = r4
            r11 = r6
            r14 = r7
            r13 = r8
            goto L_0x033a
        L_0x0392:
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            java.lang.Object r4 = r4.get(r5)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r6 = r4.y
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r25
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.y
            r7 = 1065353216(0x3f800000, float:1.0)
            float r4 = r4 - r7
            int r4 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r4 != 0) goto L_0x072a
            r4 = 1
            r6 = r11
            r7 = r4
            r8 = r13
            r4 = r12
            goto L_0x038b
        L_0x03bc:
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            java.lang.Object r4 = r4.get(r5)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r6 = r4.y
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r25
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.y
            int r4 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r4 != 0) goto L_0x072a
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            java.lang.Object r4 = r4.get(r5)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r6 = r4.x
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r25
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.x
            r7 = 1096810496(0x41600000, float:14.0)
            float r4 = r4 + r7
            r7 = 1097859072(0x41700000, float:15.0)
            float r4 = r4 % r7
            int r4 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r4 != 0) goto L_0x040a
            r4 = 1
            r6 = r4
            r7 = r14
            r8 = r13
            r4 = r12
            goto L_0x038b
        L_0x040a:
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            java.lang.Object r4 = r4.get(r5)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r6 = r4.x
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r25
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.x
            r7 = 1065353216(0x3f800000, float:1.0)
            float r4 = r4 + r7
            r7 = 1097859072(0x41700000, float:15.0)
            float r4 = r4 % r7
            int r4 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r4 != 0) goto L_0x072a
            r4 = 1
            r6 = r11
            r7 = r14
            r8 = r13
            goto L_0x038b
        L_0x0437:
            com.reactor.game.torus.Game$Coords r15 = new com.reactor.game.torus.Game$Coords
            r0 = r26
            float r0 = r0.x
            r5 = r0
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r25
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.x
            float r7 = r5 + r4
            r0 = r26
            float r0 = r0.posY
            r5 = r0
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r25
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.y
            float r8 = r5 + r4
            r9 = 0
            r4 = r26
            r5 = r27
            r6 = r28
            r10 = r20
            java.util.ArrayList r4 = r4.a(r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)
            r0 = r15
            r1 = r26
            r2 = r4
            r0.<init>(r2)
            r0 = r24
            r1 = r15
            r0.add(r1)
            r0 = r26
            float r0 = r0.posY
            r5 = r0
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r25
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.y
            float r4 = r4 + r5
            int r5 = (int) r4
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r25
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r6 = r4.x
            r0 = r26
            java.util.ArrayList r0 = r0.blockData
            r4 = r0
            r0 = r26
            int r0 = r0.type
            r7 = r0
            java.lang.Object r4 = r4.get(r7)
            com.reactor.game.torus.Game$BlockData r4 = (com.reactor.game.torus.Game.BlockData) r4
            float r4 = r4.view
            float r4 = r4 + r6
            r6 = 1065353216(0x3f800000, float:1.0)
            int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r4 >= 0) goto L_0x0577
            if (r5 < 0) goto L_0x04f5
            r4 = 15
            if (r5 >= r4) goto L_0x04f5
            r0 = r26
            int[][][] r0 = r0.field
            r6 = r0
            r0 = r26
            float r0 = r0.x
            r7 = r0
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r25
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.x
            float r4 = r4 + r7
            r7 = 1065353216(0x3f800000, float:1.0)
            float r4 = r4 + r7
            r7 = 1097859072(0x41700000, float:15.0)
            float r4 = r4 % r7
            r7 = 1097859072(0x41700000, float:15.0)
            float r4 = r4 + r7
            r7 = 1097859072(0x41700000, float:15.0)
            float r4 = r4 % r7
            int r4 = (int) r4
            r4 = r6[r4]
            r4 = r4[r5]
            if (r4 != 0) goto L_0x0577
        L_0x04f5:
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            int r4 = r4.size()
            r5 = 1
            int r4 = r4 - r5
            r5 = r4
        L_0x0501:
            if (r5 < 0) goto L_0x0537
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            java.lang.Object r4 = r4.get(r5)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.x
            r6 = 1065353216(0x3f800000, float:1.0)
            int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r4 != 0) goto L_0x070d
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            java.lang.Object r4 = r4.get(r5)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r6 = r4.y
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r25
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.y
            int r4 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r4 != 0) goto L_0x070d
        L_0x0537:
            if (r5 >= 0) goto L_0x0577
            r0 = r26
            float r0 = r0.x
            r5 = r0
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r25
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.x
            float r4 = r4 + r5
            r5 = 1065353216(0x3f800000, float:1.0)
            float r18 = r4 + r5
            r0 = r26
            float r0 = r0.posY
            r5 = r0
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r25
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.y
            float r19 = r5 + r4
            r15 = r26
            r16 = r27
            r17 = r28
            r21 = r13
            r22 = r14
            r15.a(r16, r17, r18, r19, r20, r21, r22)
        L_0x0577:
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r25
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.y
            float r4 = r4 + r23
            int r5 = (int) r4
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r25
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r6 = r4.x
            r0 = r26
            java.util.ArrayList r0 = r0.blockData
            r4 = r0
            r0 = r26
            int r0 = r0.type
            r7 = r0
            java.lang.Object r4 = r4.get(r7)
            com.reactor.game.torus.Game$BlockData r4 = (com.reactor.game.torus.Game.BlockData) r4
            float r4 = r4.view
            float r4 = r4 + r6
            r6 = 1073741824(0x40000000, float:2.0)
            int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r4 <= 0) goto L_0x0677
            if (r5 < 0) goto L_0x05e7
            r4 = 15
            if (r5 >= r4) goto L_0x05e7
            r0 = r26
            int[][][] r0 = r0.field
            r6 = r0
            r0 = r26
            float r0 = r0.x
            r7 = r0
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r25
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.x
            float r4 = r4 + r7
            r7 = 1065353216(0x3f800000, float:1.0)
            float r4 = r4 - r7
            r7 = 1097859072(0x41700000, float:15.0)
            float r4 = r4 % r7
            r7 = 1097859072(0x41700000, float:15.0)
            float r4 = r4 + r7
            r7 = 1097859072(0x41700000, float:15.0)
            float r4 = r4 % r7
            int r4 = (int) r4
            r4 = r6[r4]
            r4 = r4[r5]
            if (r4 != 0) goto L_0x0677
        L_0x05e7:
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            int r4 = r4.size()
            r5 = 1
            int r4 = r4 - r5
            r5 = r4
        L_0x05f3:
            if (r5 < 0) goto L_0x063a
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            java.lang.Object r4 = r4.get(r5)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r6 = r4.x
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r25
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.x
            r7 = 1065353216(0x3f800000, float:1.0)
            float r4 = r4 - r7
            int r4 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r4 != 0) goto L_0x0712
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            java.lang.Object r4 = r4.get(r5)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r6 = r4.y
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r25
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.y
            int r4 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r4 != 0) goto L_0x0712
        L_0x063a:
            if (r5 >= 0) goto L_0x0677
            r0 = r26
            float r0 = r0.x
            r5 = r0
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r25
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.x
            float r18 = r5 + r4
            r0 = r26
            float r0 = r0.posY
            r5 = r0
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r25
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.y
            float r19 = r5 + r4
            r15 = r26
            r16 = r27
            r17 = r28
            r21 = r13
            r22 = r14
            r15.a(r16, r17, r18, r19, r20, r21, r22)
        L_0x0677:
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            int r4 = r4.size()
            r5 = 1
            int r4 = r4 - r5
            r5 = r4
        L_0x0683:
            if (r5 < 0) goto L_0x06cb
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            java.lang.Object r4 = r4.get(r5)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r6 = r4.x
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r25
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.x
            int r4 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r4 != 0) goto L_0x0717
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            java.lang.Object r4 = r4.get(r5)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.y
            r6 = 1065353216(0x3f800000, float:1.0)
            float r6 = r4 - r6
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r25
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.y
            int r4 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r4 != 0) goto L_0x0717
        L_0x06cb:
            if (r5 >= 0) goto L_0x0707
            r0 = r26
            float r0 = r0.x
            r5 = r0
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r25
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.x
            float r8 = r5 + r4
            r0 = r26
            float r0 = r0.posY
            r5 = r0
            r0 = r26
            java.util.ArrayList r0 = r0.block
            r4 = r0
            r0 = r4
            r1 = r25
            java.lang.Object r4 = r0.get(r1)
            com.reactor.game.torus.Game$Coord r4 = (com.reactor.game.torus.Game.Coord) r4
            float r4 = r4.y
            float r9 = r5 + r4
            r13 = 0
            r5 = r26
            r6 = r27
            r7 = r28
            r10 = r20
            r5.a(r6, r7, r8, r9, r10, r11, r12, r13)
        L_0x0707:
            int r4 = r25 + -1
            r25 = r4
            goto L_0x0328
        L_0x070d:
            int r4 = r5 + -1
            r5 = r4
            goto L_0x0501
        L_0x0712:
            int r4 = r5 + -1
            r5 = r4
            goto L_0x05f3
        L_0x0717:
            int r4 = r5 + -1
            r5 = r4
            goto L_0x0683
        L_0x071c:
            r0 = r26
            float[] r0 = r0.dropPositions
            r4 = r0
            r0 = r26
            r1 = r24
            r2 = r4
            r0.a(r1, r2)
            return
        L_0x072a:
            r4 = r12
            r6 = r11
            r7 = r14
            r8 = r13
            goto L_0x038b
        L_0x0730:
            r4 = r19
            r6 = r21
            goto L_0x00fe
        */
        throw new UnsupportedOperationException("Method not decompiled: com.reactor.game.torus.Game.a(android.graphics.Canvas, android.graphics.Paint):void");
    }

    /* access modifiers changed from: package-private */
    public void a(Canvas canvas, Paint paint, float f, float f2, float f3, boolean z, boolean z2) {
        paint.setAntiAlias(true);
        ArrayList arrayList = new ArrayList();
        arrayList.add(a(f, (float) (((double) f2) - 1.015d), false, f3));
        arrayList.add(a(f, (float) (((double) f2) + 0.015d), false, f3));
        arrayList.add(a(f, (float) (((double) f2) + 0.015d), true, f3));
        arrayList.add(a(f, (float) (((double) f2) - 1.015d), true, f3));
        Path path = new Path();
        path.moveTo(((Coord) arrayList.get(0)).x, ((Coord) arrayList.get(0)).y);
        path.lineTo(((Coord) arrayList.get(1)).x, ((Coord) arrayList.get(1)).y);
        path.lineTo(((Coord) arrayList.get(2)).x, ((Coord) arrayList.get(2)).y);
        path.lineTo(((Coord) arrayList.get(3)).x, ((Coord) arrayList.get(3)).y);
        path.close();
        paint.setStyle(Paint.Style.FILL);
        canvas.drawPath(path, paint);
        if (z || z2) {
            path.reset();
            path.moveTo(((Coord) arrayList.get(0)).x, ((Coord) arrayList.get(0)).y);
            path.lineTo(((Coord) arrayList.get(1)).x, ((Coord) arrayList.get(1)).y);
            if (z) {
                path.moveTo(((Coord) arrayList.get(2)).x, ((Coord) arrayList.get(2)).y);
            } else {
                path.lineTo(((Coord) arrayList.get(2)).x, ((Coord) arrayList.get(2)).y);
            }
            path.lineTo(((Coord) arrayList.get(3)).x, ((Coord) arrayList.get(3)).y);
            if (!z2) {
                path.lineTo(((Coord) arrayList.get(0)).x, ((Coord) arrayList.get(0)).y);
            }
        }
        canvas.drawPath(path, this.f28a);
    }

    /* access modifiers changed from: package-private */
    public void a(Canvas canvas, Paint paint, float f, float f2, float f3, boolean z, boolean z2, boolean z3) {
        paint.setAntiAlias(true);
        ArrayList arrayList = new ArrayList();
        arrayList.add(a((float) (((double) f) - 0.015d), f2, false, f3));
        arrayList.add(a((float) (((double) f) - 0.015d), f2, true, f3));
        arrayList.add(a((float) (((double) f) + 1.015d), f2, true, f3));
        arrayList.add(a((float) (((double) f) + 1.015d), f2, false, f3));
        Path path = new Path();
        path.moveTo(((Coord) arrayList.get(0)).x, ((Coord) arrayList.get(0)).y);
        path.lineTo(((Coord) arrayList.get(1)).x, ((Coord) arrayList.get(1)).y);
        path.lineTo(((Coord) arrayList.get(2)).x, ((Coord) arrayList.get(2)).y);
        path.lineTo(((Coord) arrayList.get(3)).x, ((Coord) arrayList.get(3)).y);
        path.close();
        paint.setStyle(Paint.Style.FILL);
        canvas.drawPath(path, paint);
        if (!z3) {
            if (z || z2) {
                path.reset();
                if (z) {
                    path.moveTo(((Coord) arrayList.get(1)).x, ((Coord) arrayList.get(1)).y);
                } else {
                    path.moveTo(((Coord) arrayList.get(0)).x, ((Coord) arrayList.get(0)).y);
                    path.lineTo(((Coord) arrayList.get(1)).x, ((Coord) arrayList.get(1)).y);
                }
                path.lineTo(((Coord) arrayList.get(2)).x, ((Coord) arrayList.get(2)).y);
                if (z2) {
                    path.moveTo(((Coord) arrayList.get(3)).x, ((Coord) arrayList.get(3)).y);
                } else {
                    path.lineTo(((Coord) arrayList.get(3)).x, ((Coord) arrayList.get(3)).y);
                }
                path.lineTo(((Coord) arrayList.get(0)).x, ((Coord) arrayList.get(0)).y);
            }
            canvas.drawPath(path, this.f28a);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(ArrayList arrayList, float[] fArr) {
        int size = arrayList.size();
        fArr[0] = ((Coord) ((Coords) arrayList.get(0)).block.get(0)).x;
        fArr[1] = fArr[0];
        fArr[2] = ((Coord) ((Coords) arrayList.get(0)).block.get(0)).y;
        fArr[3] = fArr[2];
        for (int i = 0; i < size; i++) {
            ArrayList arrayList2 = ((Coords) arrayList.get(i)).block;
            int size2 = arrayList2.size();
            for (int i2 = 0; i2 < size2; i2++) {
                Coord coord = (Coord) arrayList2.get(i2);
                if (fArr[0] > coord.x) {
                    fArr[0] = coord.x;
                }
                if (fArr[1] < coord.x) {
                    fArr[1] = coord.x;
                }
                if (fArr[2] > coord.y) {
                    fArr[2] = coord.y;
                }
                if (fArr[3] < coord.y) {
                    fArr[3] = coord.y;
                }
            }
        }
        float f = fArr[1] - fArr[0];
        float f2 = fArr[3] - fArr[2];
        if (f < f2) {
            float f3 = (fArr[0] + fArr[1]) * 0.5f;
            fArr[0] = f3 - (0.5f * f2);
            fArr[1] = f3 + (f2 * 0.5f);
        } else if (f > f2) {
            float f4 = (fArr[2] + fArr[3]) * 0.5f;
            fArr[2] = f4 - (f * 0.5f);
            fArr[3] = (f * 0.5f) + f4;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    public void drawCylinder(Canvas canvas, Paint paint, boolean z, boolean z2, float f, ArrayList arrayList) {
        int i;
        float pow;
        float f2;
        int[] iArr;
        int[] iArr2;
        int[] iArr3;
        int[] iArr4;
        paint.setAntiAlias(true);
        float a2 = ((this.viewPort.a() % 15.0f) + 15.0f) % 15.0f;
        int i2 = ((int) ((a2 % 15.0f) + 15.0f)) % 15;
        int[] iArr5 = new int[15];
        int[] iArr6 = {7, 8, 6, 9, 5, 10, 4, 11, 3, 12, 2, 13, 1, 14, 0};
        int i3 = 0;
        float f3 = f;
        while (i3 < 15) {
            int i4 = iArr6[i3];
            int i5 = 0;
            float f4 = f3;
            int i6 = 0;
            while (i5 < 15) {
                int i7 = (i4 + i2) % 15;
                if (this.field[i7][i5] != null) {
                    iArr5[i7] = i5;
                    float cos = (float) (0.6000000238418579d + (0.4000000059604645d * Math.cos((6.283185307179586d * ((double) (((float) i7) - a2))) / 15.0d)));
                    int i8 = ((BlockData) this.blockData.get(this.field[i7][i5][0] - 1)).theme;
                    if (this.mode == 3 && i5 >= 3) {
                        i8 = 20;
                    }
                    if (this.f29a) {
                        i8 = 30;
                    }
                    if (f4 == 0.0f || !arrayList.contains(Integer.valueOf(i5))) {
                        if (this.mode == 3) {
                            paint.setAlpha((int) (Math.max((((double) cos) - 0.3d) / 0.7d, 0.0d) * 255.0d));
                            this.f28a.setAlpha((int) (Math.max((((double) cos) - 0.3d) / 0.7d, 0.0d) * 255.0d));
                        } else {
                            paint.setAlpha(226);
                            this.f28a.setAlpha(226);
                        }
                        if (f4 < 0.0f) {
                            f4 = 0.0f;
                        }
                        i = i6;
                        pow = (float) (((double) i5) - (((double) i6) * Math.pow((double) Math.max(0.0f, f4), 5.0d)));
                        f2 = f4;
                    } else {
                        paint.setAlpha((int) (Math.max(0.0d, Math.min(1.0d, ((double) cos) * (1.0d - (((double) f4) * 1.5d)))) * 255.0d));
                        this.f28a.setAlpha((int) (Math.max(0.0d, Math.min(1.0d, ((double) cos) * (1.0d - (((double) f4) * 1.5d)))) * 255.0d));
                        i = i6 + 1;
                        pow = (float) i5;
                        f2 = f4;
                    }
                    float f5 = 0.8f;
                    if (0.8f < 0.0f) {
                        f5 = 0.0f;
                    } else if (0.8f > 1.0f) {
                        f5 = 1.0f;
                    }
                    int alpha = paint.getAlpha();
                    switch (i8) {
                        case 0:
                            paint.setStyle(Paint.Style.FILL);
                            paint.setARGB(alpha, (int) (255.0f * f5), (int) (0.0f * f5), (int) (f5 * 0.0f));
                            break;
                        case 1:
                            paint.setStyle(Paint.Style.FILL);
                            paint.setARGB(alpha, (int) (0.0f * f5), (int) (255.0f * f5), (int) (f5 * 0.0f));
                            break;
                        case 2:
                            paint.setStyle(Paint.Style.FILL);
                            paint.setARGB(alpha, (int) (0.0f * f5), (int) (0.0f * f5), (int) (f5 * 255.0f));
                            break;
                        case 3:
                            paint.setStyle(Paint.Style.FILL);
                            paint.setARGB(alpha, (int) (255.0f * f5), (int) (255.0f * f5), (int) (f5 * 0.0f));
                            break;
                        case 4:
                            paint.setStyle(Paint.Style.FILL);
                            paint.setARGB(alpha, (int) (255.0f * f5), (int) (0.0f * f5), (int) (f5 * 255.0f));
                            break;
                        case 5:
                            paint.setStyle(Paint.Style.FILL);
                            paint.setARGB(alpha, (int) (0.0f * f5), (int) (255.0f * f5), (int) (f5 * 255.0f));
                            break;
                        case 6:
                            paint.setStyle(Paint.Style.FILL);
                            paint.setARGB(alpha, (int) (220.0f * f5), (int) (220.0f * f5), (int) (f5 * 220.0f));
                            break;
                        case 20:
                            paint.setStyle(Paint.Style.FILL);
                            paint.setARGB(alpha, (int) (100.0f * f5), (int) (100.0f * f5), (int) (f5 * 100.0f));
                            break;
                        case 30:
                            paint.setStyle(Paint.Style.FILL);
                            paint.setARGB(alpha, (int) (190.0f * f5), (int) (190.0f * f5), (int) (f5 * 0.0f));
                            break;
                        default:
                            paint.setStyle(Paint.Style.FILL);
                            paint.setARGB(alpha, (int) (220.0f * f5), (int) (220.0f * f5), (int) (f5 * 220.0f));
                            break;
                    }
                    float f6 = (((((float) i7) - a2) % 15.0f) + 15.0f) % 15.0f;
                    float f7 = (float) i7;
                    boolean z3 = ((double) f6) > 3.3d && ((double) f6) < 10.7d;
                    boolean z4 = (((double) f6) > 11.9d || ((double) f6) < 10.9d) && (iArr4 = this.field[(i7 + 14) % 15][i5]) != null && iArr4[1] == this.field[i7][i5][1];
                    boolean z5 = (((double) f6) < 2.1d || ((double) f6) > 3.1d) && (iArr3 = this.field[(i7 + 1) % 15][i5]) != null && iArr3[1] == this.field[i7][i5][1];
                    int[] iArr7 = i5 >= 14 ? null : this.field[i7][i5 + 1];
                    boolean z6 = iArr7 != null && iArr7[1] == this.field[i7][i5][1];
                    int[] iArr8 = i5 - 1 < 0 ? null : this.field[i7][i5 - 1];
                    a(canvas, paint, f7, pow, z3, a2, z4, z5, z6, iArr8 != null && iArr8[1] == this.field[i7][i5][1]);
                    if (arrayList != null) {
                        iArr = new int[arrayList.size()];
                        for (int i9 = 0; i9 < arrayList.size(); i9++) {
                            iArr[i9] = ((Integer) arrayList.get(i9)).intValue();
                        }
                    } else {
                        iArr = null;
                    }
                    if (i5 >= 14 || this.field[i7][i5 + 1] == null || (iArr != null && arrayList.contains(Integer.valueOf(i5 + 1)))) {
                        a(canvas, paint, (float) i7, pow, a2, (iArr != null || (i5 < 14 && this.field[(i7 + 14) % 15][i5 + 1] == null)) && (iArr = this.field[(i7 + 14) % 15][i5]) != null && iArr[1] == this.field[i7][i5][1], (iArr != null || (i5 < 14 && this.field[(i7 + 1) % 15][i5 + 1] == null)) && (iArr2 = this.field[(i7 + 1) % 15][i5]) != null && iArr2[1] == this.field[i7][i5][1], false);
                    }
                    if (((double) f6) > 6.5d && f6 < 14.0f && this.field[((((i4 + 1) + i2) % 15) + 15) % 15][i5] == null) {
                        float f8 = (float) (i7 + 1);
                        int[] iArr9 = i5 >= 14 ? null : this.field[i7][i5 + 1];
                        boolean z7 = iArr9 != null && iArr9[1] == this.field[i7][i5][1];
                        int[] iArr10 = i5 - 1 < 0 ? null : this.field[i7][i5 - 1];
                        a(canvas, paint, f8, pow, a2, z7, iArr10 != null && iArr10[1] == this.field[i7][i5][1]);
                    }
                    if (((double) f6) < 7.5d && this.field[((((i4 - 1) + i2) % 15) + 15) % 15][i5] == null) {
                        float f9 = (float) i7;
                        int[] iArr11 = i5 >= 14 ? null : this.field[i7][i5 + 1];
                        boolean z8 = iArr11 != null && iArr11[1] == this.field[i7][i5][1];
                        int[] iArr12 = i5 - 1 < 0 ? null : this.field[i7][i5 - 1];
                        a(canvas, paint, f9, pow, a2, z8, iArr12 != null && iArr12[1] == this.field[i7][i5][1]);
                    }
                    i6 = i;
                    f4 = f2;
                }
                i5++;
            }
            i3++;
            f3 = f4;
        }
        for (int i10 = 0; i10 < 15 && !z2; i10++) {
            float f10 = (float) iArr5[i10];
            for (int i11 = 1; i11 < 5; i11++) {
                f10 = Math.max(Math.max(f10, (float) (iArr5[(i10 + i11) % 15] - i11)), (float) (iArr5[((i10 - i11) + 15) % 15] - i11));
            }
            if (f10 > 9.0f) {
                paint.setStyle(Paint.Style.FILL);
                paint.setColor(Color.rgb((int) (255.0d * ((Math.sin((double) (System.currentTimeMillis() / 200)) / 2.0d) + 0.0d)), 30, 0));
                paint.setAlpha(((int) (f10 - 9.0f)) * 51);
                a(canvas, paint, (float) i10, 14.0f, a2, false, false, true);
            }
        }
        if (z) {
            a(canvas, paint);
        }
    }

    public int getRandomPieceType() {
        float random = ((float) this.f27a) * ((float) Math.random());
        float f = 0.0f;
        int i = 0;
        while (true) {
            float f2 = f;
            if (i >= this.blockData.size()) {
                return i - 1;
            }
            f = ((BlockData) this.blockData.get(i)).a + f2;
            if (random < f) {
                return i;
            }
            i++;
        }
    }

    public void setMode(int i) {
        this.mode = i;
        m1a();
    }
}
