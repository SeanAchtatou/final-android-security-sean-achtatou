package com.reactor.game.torus;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.facebook.android.ShareOnFacebook;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import java.util.HashMap;

public class TorusAct extends Activity implements AdListener {
    private AdView a;

    /* renamed from: a  reason: collision with other field name */
    private TorusView f64a;
    public ShareOnFacebook shareOnFacebook;

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (this.shareOnFacebook != null) {
            this.shareOnFacebook.setBestRecord(this.f64a.getBestScore());
            this.shareOnFacebook.getFb().authorizeCallback(i, i2, intent);
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        FrameLayout frameLayout = new FrameLayout(this);
        frameLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.f64a = new TorusView(this, null);
        frameLayout.addView(this.f64a);
        this.a = new AdView(this, AdSize.BANNER, "a14e16a52f18373");
        this.a.setAdListener(this);
        AdRequest adRequest = new AdRequest();
        HashMap hashMap = new HashMap();
        hashMap.put("color_bg", "1556a6");
        hashMap.put("color_bg_top", "1556a6");
        hashMap.put("color_border", "1556a6");
        hashMap.put("color_link", "FFFFFF");
        hashMap.put("color_text", "FFFFFF");
        hashMap.put("color_url", "FFFFFF");
        adRequest.setExtras(hashMap);
        this.a.loadAd(adRequest);
        frameLayout.addView(this.a);
        setContentView(frameLayout);
        this.f64a.startGame(getIntent().getIntExtra("com.reactor.game.torus.Mode", 1));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.a != null) {
            this.a.destroy();
        }
    }

    public void onDismissScreen(Ad ad) {
    }

    public void onFailedToReceiveAd(Ad ad, AdRequest.ErrorCode errorCode) {
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return true;
    }

    public void onLeaveApplication(Ad ad) {
    }

    public void onPause() {
        super.onPause();
        this.f64a.getControl().b();
    }

    public void onPresentScreen(Ad ad) {
    }

    public void onReceiveAd(Ad ad) {
        this.f64a.setOffsetForAd(45.0f);
    }

    public void onResume() {
        super.onResume();
        this.f64a.getControl().c();
    }
}
