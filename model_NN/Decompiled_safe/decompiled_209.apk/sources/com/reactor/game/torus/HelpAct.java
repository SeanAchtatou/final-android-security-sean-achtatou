package com.reactor.game.torus;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import java.util.HashMap;

public class HelpAct extends Activity {
    private AdView a;

    /* renamed from: a  reason: collision with other field name */
    private HelpView f38a;

    public class HelpView extends SurfaceView implements GestureDetector.OnGestureListener, SurfaceHolder.Callback {
        private int a = 480;

        /* renamed from: a  reason: collision with other field name */
        private Bitmap f39a;

        /* renamed from: a  reason: collision with other field name */
        private Typeface f40a;

        /* renamed from: a  reason: collision with other field name */
        private GestureDetector f41a = new GestureDetector(this);

        /* renamed from: a  reason: collision with other field name */
        private SurfaceHolder f42a;
        private int b = 800;

        /* renamed from: b  reason: collision with other field name */
        private Bitmap f44b;
        private Bitmap c;
        private Bitmap d;

        public HelpView(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            setFocusable(true);
            setLongClickable(true);
            this.f42a = getHolder();
            this.f42a.addCallback(this);
            this.f39a = BitmapFactory.decodeResource(context.getResources(), R.drawable.traditionalbackground);
            new DisplayMetrics();
            DisplayMetrics displayMetrics = HelpAct.this.getApplicationContext().getResources().getDisplayMetrics();
            this.a = displayMetrics.widthPixels;
            this.b = displayMetrics.heightPixels;
            this.f44b = GlobalVars.scaleBitmap(context, R.drawable.largebox, ((float) this.a) * 0.82f, ((float) this.b) * 0.85f);
            this.f40a = Typeface.createFromAsset(context.getAssets(), "222-CAI978.ttf");
            this.c = GlobalVars.scaleBitmap(context, R.drawable.mainmenu, ((float) this.a) * 0.35f, ((float) this.b) * 0.08f);
            this.d = Bitmap.createBitmap((int) (((float) this.f44b.getWidth()) * 0.8f), (int) (((float) this.b) * 0.8f), Bitmap.Config.ARGB_8888);
            String helpContent = GlobalVars.getHelpContent("help.txt", HelpAct.this);
            int indexOf = helpContent.indexOf("\n");
            String substring = helpContent.substring(0, indexOf - 1);
            Canvas canvas = new Canvas(this.d);
            Paint paint = new Paint();
            paint.setColor(-1);
            paint.setTypeface(this.f40a);
            paint.setTextSize(20.0f);
            paint.setFakeBoldText(true);
            Paint.FontMetrics fontMetrics = paint.getFontMetrics();
            float ceil = (float) Math.ceil((double) (fontMetrics.descent - fontMetrics.ascent));
            float f = 0.1f * ceil;
            float drawContent = GlobalVars.drawContent(substring, ceil, f, ((float) this.f44b.getWidth()) * 0.8f, 0.0f, -f, canvas, paint);
            paint.setTextSize(15.0f);
            paint.setFakeBoldText(false);
            Paint.FontMetrics fontMetrics2 = paint.getFontMetrics();
            float ceil2 = (float) Math.ceil((double) (fontMetrics2.descent - fontMetrics2.ascent));
            float f2 = 0.2f * ceil2;
            int i = indexOf + 1;
            int indexOf2 = helpContent.indexOf("\n", i);
            float drawContent2 = GlobalVars.drawContent(helpContent.substring(i, indexOf2 - 1), ceil2, f2, ((float) this.f44b.getWidth()) * 0.8f, 0.0f, drawContent, canvas, paint);
            int i2 = indexOf2 + 1;
            GlobalVars.drawContent(helpContent.substring(i2, helpContent.indexOf("\n", i2) - 1), ceil2, f2, ((float) this.f44b.getWidth()) * 0.8f, 0.0f, drawContent2, canvas, paint);
        }

        /* access modifiers changed from: package-private */
        public void a() {
            Canvas lockCanvas = this.f42a.lockCanvas();
            if (lockCanvas != null) {
                Paint paint = new Paint();
                paint.setAntiAlias(true);
                lockCanvas.drawBitmap(this.f39a, (Rect) null, new RectF(0.0f, 0.0f, (float) this.a, (float) this.b), paint);
                lockCanvas.drawBitmap(this.f44b, ((float) (this.a - this.f44b.getWidth())) / 2.0f, ((float) (this.b - this.f44b.getHeight())) / 2.0f, paint);
                lockCanvas.drawBitmap(this.c, ((float) (this.a - this.c.getWidth())) * 0.5f, (((3.0f * ((float) this.b)) + ((float) this.f44b.getHeight())) * 0.25f) - (((float) this.c.getHeight()) * 0.5f), paint);
                lockCanvas.drawBitmap(this.d, ((float) (this.a - this.d.getWidth())) * 0.5f, ((float) (this.b - this.d.getHeight())) * 0.5f, paint);
                this.f42a.unlockCanvasAndPost(lockCanvas);
            }
        }

        public boolean onDown(MotionEvent motionEvent) {
            float x = motionEvent.getX();
            float y = motionEvent.getY();
            float height = (((3.0f * ((float) this.b)) + ((float) this.f44b.getHeight())) * 0.25f) - (((float) this.c.getHeight()) * 0.5f);
            if (x < ((float) (this.a - this.c.getWidth())) * 0.5f || x > ((float) (this.a + this.c.getWidth())) * 0.5f || y < height || y > ((float) this.c.getHeight()) + height) {
                return false;
            }
            HelpAct.this.a();
            return false;
        }

        public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
            return false;
        }

        public void onLongPress(MotionEvent motionEvent) {
        }

        public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
            return false;
        }

        public void onShowPress(MotionEvent motionEvent) {
        }

        public boolean onSingleTapUp(MotionEvent motionEvent) {
            return false;
        }

        public boolean onTouchEvent(MotionEvent motionEvent) {
            this.f41a.onTouchEvent(motionEvent);
            return super.onTouchEvent(motionEvent);
        }

        public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        }

        public void surfaceCreated(SurfaceHolder surfaceHolder) {
            a();
        }

        public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        }
    }

    /* access modifiers changed from: private */
    public void a() {
        startActivity(new Intent(this, WelcomeAct.class));
        finish();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        FrameLayout frameLayout = new FrameLayout(this);
        frameLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.f38a = new HelpView(this, null);
        frameLayout.addView(this.f38a);
        this.a = new AdView(this, AdSize.BANNER, "a14e16a52f18373");
        AdRequest adRequest = new AdRequest();
        HashMap hashMap = new HashMap();
        hashMap.put("color_bg", "1556a6");
        hashMap.put("color_bg_top", "1556a6");
        hashMap.put("color_border", "1556a6");
        hashMap.put("color_link", "FFFFFF");
        hashMap.put("color_text", "FFFFFF");
        hashMap.put("color_url", "FFFFFF");
        adRequest.setExtras(hashMap);
        this.a.loadAd(adRequest);
        frameLayout.addView(this.a);
        setContentView(frameLayout);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.a != null) {
            this.a.destroy();
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return true;
    }
}
