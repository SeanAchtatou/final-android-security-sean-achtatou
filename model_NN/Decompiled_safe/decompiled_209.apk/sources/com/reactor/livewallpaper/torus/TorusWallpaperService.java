package com.reactor.livewallpaper.torus;

import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Handler;
import android.service.wallpaper.WallpaperService;
import android.util.DisplayMetrics;
import android.view.SurfaceHolder;
import android.view.ViewGroup;
import android.view.WindowManager;
import com.admob.android.ads.AdListener;
import com.admob.android.ads.AdView;
import com.reactor.game.torus.Game;
import com.reactor.game.torus.R;
import java.util.ArrayList;

public class TorusWallpaperService extends WallpaperService {
    /* access modifiers changed from: private */
    public static boolean a = false;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with other field name */
    public int f121a = 480;

    /* renamed from: a  reason: collision with other field name */
    private final Handler f122a = new Handler();
    /* access modifiers changed from: private */
    public int b = 800;

    public class TorusEngine extends WallpaperService.Engine implements AdListener {
        private float a = 0.0f;

        /* renamed from: a  reason: collision with other field name */
        private int f123a;

        /* renamed from: a  reason: collision with other field name */
        private long f124a = 0;

        /* renamed from: a  reason: collision with other field name */
        private Bitmap f125a;

        /* renamed from: a  reason: collision with other field name */
        private Canvas f126a;

        /* renamed from: a  reason: collision with other field name */
        private Paint f127a;

        /* renamed from: a  reason: collision with other field name */
        private WindowManager.LayoutParams f128a = null;

        /* renamed from: a  reason: collision with other field name */
        private WindowManager f129a = null;

        /* renamed from: a  reason: collision with other field name */
        private AdView f130a = null;

        /* renamed from: a  reason: collision with other field name */
        private Game f131a;

        /* renamed from: a  reason: collision with other field name */
        private b f133a;

        /* renamed from: a  reason: collision with other field name */
        private final Runnable f134a = new a(this);

        /* renamed from: a  reason: collision with other field name */
        private ArrayList f135a;

        /* renamed from: a  reason: collision with other field name */
        private boolean f136a = false;
        private float b = 0.0f;

        /* renamed from: b  reason: collision with other field name */
        private int f137b;

        /* renamed from: b  reason: collision with other field name */
        private Bitmap f138b;

        /* renamed from: b  reason: collision with other field name */
        private boolean f139b;
        private float c;

        /* renamed from: c  reason: collision with other field name */
        private boolean f140c = false;
        private float d;
        private float e;
        private float f;
        private float g;

        public TorusEngine() {
            super(TorusWallpaperService.this);
        }

        private void f() {
            this.f131a.setMode(1);
            h();
        }

        private void g() {
            this.f131a.x = -2.0f;
            d();
        }

        private void h() {
            this.f131a.nextType = this.f131a.getRandomPieceType();
            g();
            this.f133a.b = false;
            this.f133a.c = false;
            this.f133a.f141a = false;
            this.f124a = System.currentTimeMillis();
            this.f131a.time = 0;
        }

        /* access modifiers changed from: package-private */
        public void a() {
            if (TorusWallpaperService.a(TorusWallpaperService.this) == 480) {
                this.d = 0.49f * ((float) TorusWallpaperService.this.f121a);
                this.e = 0.285f * ((float) TorusWallpaperService.a(TorusWallpaperService.this));
                this.f = 0.63f * ((float) TorusWallpaperService.a(TorusWallpaperService.this));
                this.g = 1.0f;
            } else if (TorusWallpaperService.a(TorusWallpaperService.this) == 800) {
                this.d = ((float) TorusWallpaperService.this.f121a) * 0.335f;
                this.e = 0.229f * ((float) TorusWallpaperService.a(TorusWallpaperService.this));
                this.f = 0.649f * ((float) TorusWallpaperService.a(TorusWallpaperService.this));
                this.g = 1.5f;
            } else {
                this.d = ((float) TorusWallpaperService.this.f121a) * 0.335f;
                this.e = 0.245f * ((float) TorusWallpaperService.a(TorusWallpaperService.this));
                this.f = 0.656f * ((float) TorusWallpaperService.a(TorusWallpaperService.this));
                this.g = 1.5f;
            }
        }

        /* access modifiers changed from: package-private */
        public void a(int i) {
            this.f133a.f141a = false;
            for (int size = this.f131a.block.size() - 1; size >= 0; size--) {
                if (((Game.Coord) this.f131a.block.get(size)).y + ((float) i) > 14.0f) {
                    this.f131a.drawCylinder(this.f126a, this.f127a, false, true, 0.0f, null);
                    f();
                    return;
                }
            }
            for (int size2 = this.f131a.block.size() - 1; size2 >= 0; size2--) {
                int i2 = (int) ((((((Game.Coord) this.f131a.block.get(size2)).x + this.f131a.x) % 15.0f) + 15.0f) % 15.0f);
                int i3 = (int) (((Game.Coord) this.f131a.block.get(size2)).y + ((float) i));
                if (this.f131a.field[i2][i3] == null) {
                    this.f131a.field[i2][i3] = new int[2];
                }
                this.f131a.field[i2][i3][0] = this.f131a.type + 1;
                this.f131a.field[i2][i3][1] = this.f131a.pieceCount;
            }
            this.f131a.pieceCount++;
            ArrayList arrayList = new ArrayList();
            int i4 = 0;
            while (i4 < 15) {
                int i5 = 0;
                while (i5 < 15 && this.f131a.field[i5][i4] != null) {
                    i5++;
                }
                if (i5 == 15) {
                    for (int i6 = 0; i6 < 15; i6++) {
                        if (this.f131a.field[i6][i4] == null) {
                            this.f131a.field[i6][i4] = new int[2];
                        }
                        this.f131a.field[i6][i4][0] = this.f131a.field[i6][i4][0];
                        this.f131a.field[i6][i4][1] = 0;
                    }
                    arrayList.add(Integer.valueOf(i4));
                }
                i4++;
            }
            if (arrayList.size() != 0) {
                this.f140c = true;
                this.b = (float) this.f131a.time;
                this.f135a = arrayList;
                return;
            }
            c();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a  reason: collision with other method in class */
        public boolean m3a() {
            int floor = (int) Math.floor((double) this.f131a.posY);
            if (this.f131a.block.size() == 1) {
                while (floor >= 0) {
                    if (this.f131a.field[(int) ((((((Game.Coord) this.f131a.block.get(0)).x + this.f131a.x) % 15.0f) + 15.0f) % 15.0f)][floor] == null) {
                        return false;
                    }
                    floor--;
                }
                return true;
            }
            for (int size = this.f131a.block.size() - 1; size >= 0; size--) {
                int i = (int) ((((((Game.Coord) this.f131a.block.get(size)).x + this.f131a.x) % 15.0f) + 15.0f) % 15.0f);
                int i2 = (int) (((Game.Coord) this.f131a.block.get(size)).y + ((float) floor));
                if (i2 < 0 || (i > 0 && i < 15 && i2 > 0 && i2 < 15 && this.f131a.field[i][i2] != null)) {
                    return true;
                }
            }
            if (this.f131a.posY % 1.0f > 0.0f) {
                int ceil = (int) Math.ceil((double) this.f131a.posY);
                for (int size2 = this.f131a.block.size() - 1; size2 >= 0; size2--) {
                    int i3 = (int) ((((((Game.Coord) this.f131a.block.get(size2)).x + this.f131a.x) % 15.0f) + 15.0f) % 15.0f);
                    int i4 = (int) (((Game.Coord) this.f131a.block.get(size2)).y + ((float) ceil));
                    if (i4 < 0 || (i3 >= 0 && i3 < 15 && i4 >= 0 && i4 < 15 && this.f131a.field[i3][i4] != null)) {
                        return true;
                    }
                }
            }
            return false;
        }

        /* access modifiers changed from: package-private */
        public boolean a(float f2) {
            this.f131a.x += f2;
            if (m3a()) {
                this.f131a.x -= f2;
                return false;
            }
            this.f131a.viewPort.setTarget(this.f131a.viewPort.mTarget + f2);
            return true;
        }

        /* access modifiers changed from: package-private */
        public boolean a(int i, boolean z) {
            if (this.f131a.type == 0) {
                return false;
            }
            this.f137b = ((this.f137b + i) + 4) % ((Game.BlockData) this.f131a.blockData.get(this.f131a.type)).coords.size();
            this.f131a.block = ((Game.Coords) ((Game.BlockData) this.f131a.blockData.get(this.f131a.type)).coords.get(this.f137b)).block;
            if (z) {
                return false;
            }
            if (!m3a() || a(1.0f) || a(-1.0f)) {
                return true;
            }
            a(-i, true);
            return false;
        }

        /* access modifiers changed from: package-private */
        public void b() {
            SurfaceHolder surfaceHolder = getSurfaceHolder();
            this.f126a = surfaceHolder.lockCanvas();
            long currentTimeMillis = System.currentTimeMillis();
            if (this.f126a != null) {
                this.f127a.setAlpha(255);
                this.f126a.translate(this.c, 0.0f);
                this.f126a.drawBitmap(this.f125a, (Rect) null, new RectF(0.0f, 0.0f, (float) TorusWallpaperService.this.f121a, (float) TorusWallpaperService.a(TorusWallpaperService.this)), this.f127a);
                this.f126a.drawBitmap(this.f138b, ((float) (TorusWallpaperService.this.f121a - this.f138b.getWidth())) * 0.5f, this.f, this.f127a);
                this.f126a.save();
                this.f126a.scale(this.g, this.g);
                this.f126a.translate(this.d, this.e);
                this.f127a.setAntiAlias(true);
                float random = (float) Math.random();
                if (random > 0.69f && random < 0.7f) {
                    this.f133a.f141a = true;
                } else if (random > 0.79f && random < 0.8f) {
                    this.f133a.b = true;
                } else if (random > 0.89f && random < 0.9f) {
                    this.f133a.c = true;
                } else if (random > 0.95f && ((double) random) < 0.97d) {
                    a(1, false);
                }
                e();
                b bVar = this.f133a;
                b bVar2 = this.f133a;
                this.f133a.c = false;
                bVar2.b = false;
                bVar.f141a = false;
                this.f126a.restore();
            }
            if (this.f126a != null) {
                surfaceHolder.unlockCanvasAndPost(this.f126a);
            }
            TorusWallpaperService.a(TorusWallpaperService.this).removeCallbacks(this.f134a);
            long currentTimeMillis2 = System.currentTimeMillis() - currentTimeMillis;
            if (!this.f136a) {
                return;
            }
            if (currentTimeMillis2 >= ((long) this.f123a)) {
                TorusWallpaperService.a(TorusWallpaperService.this).post(this.f134a);
            } else {
                TorusWallpaperService.a(TorusWallpaperService.this).postDelayed(this.f134a, ((long) this.f123a) - currentTimeMillis2);
            }
        }

        /* access modifiers changed from: package-private */
        public void c() {
            int i = 0;
            int i2 = 0;
            while (i < 15) {
                int i3 = 0;
                while (i3 < 15 && this.f131a.field[i3][i] != null) {
                    i3++;
                }
                if (i3 == 15) {
                    for (int i4 = 0; i4 < 15; i4++) {
                        this.f131a.field[i4][i] = null;
                        for (int i5 = i + 1; i5 < 15; i5++) {
                            this.f131a.field[i4][i5 - 1] = this.f131a.field[i4][i5];
                        }
                        this.f131a.field[i4][14] = null;
                    }
                    i--;
                    this.f131a.lines++;
                    i2++;
                }
                i++;
            }
            this.f131a.viewPort.setTarget(((Game.BlockData) this.f131a.blockData.get(this.f131a.type)).view + this.f131a.viewPort.mTarget);
            d();
        }

        /* access modifiers changed from: package-private */
        public void d() {
            this.f131a.type = this.f131a.nextType;
            this.f131a.nextType = this.f131a.getRandomPieceType();
            this.f137b = 0;
            this.f131a.posY = 16.0f;
            this.f131a.viewPort.setTarget(this.f131a.viewPort.mTarget - ((Game.BlockData) this.f131a.blockData.get(this.f131a.type)).view);
            this.f131a.block = ((Game.Coords) ((Game.BlockData) this.f131a.blockData.get(this.f131a.type)).coords.get(0)).block;
            this.f131a.pieceSpawnTime = (float) this.f131a.time;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.max(float, float):float}
         arg types: [int, float]
         candidates:
          ClspMth{java.lang.Math.max(double, double):double}
          ClspMth{java.lang.Math.max(int, int):int}
          ClspMth{java.lang.Math.max(long, long):long}
          ClspMth{java.lang.Math.max(float, float):float} */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.min(float, float):float}
         arg types: [int, float]
         candidates:
          ClspMth{java.lang.Math.min(double, double):double}
          ClspMth{java.lang.Math.min(long, long):long}
          ClspMth{java.lang.Math.min(int, int):int}
          ClspMth{java.lang.Math.min(float, float):float} */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.min(float, float):float}
         arg types: [float, int]
         candidates:
          ClspMth{java.lang.Math.min(double, double):double}
          ClspMth{java.lang.Math.min(long, long):long}
          ClspMth{java.lang.Math.min(int, int):int}
          ClspMth{java.lang.Math.min(float, float):float} */
        /* access modifiers changed from: package-private */
        public void e() {
            long currentTimeMillis = System.currentTimeMillis();
            float max = Math.max(0.0f, Math.min(1000.0f, (float) (currentTimeMillis - this.f124a)));
            Game game = this.f131a;
            game.time = (long) (((float) game.time) + max);
            float exp = (float) (this.f131a.mode == 3 ? 2000.0d : 20.0d + (2980.0d * Math.exp((double) ((-this.f131a.lines) / 35))));
            if (this.f133a.f141a) {
                exp = Math.min(exp, 30.0f);
            }
            this.f131a.posY -= Math.max(0.0f, Math.min(1.0f, max / exp));
            this.f124a = currentTimeMillis;
            if (this.f140c) {
                float sqrt = (float) (((double) (((float) this.f131a.time) - this.b)) / Math.sqrt((double) this.f135a.size()));
                if (sqrt > 300.0f || sqrt < 0.0f) {
                    this.f140c = false;
                    c();
                    this.f131a.drawCylinder(this.f126a, this.f127a, true, false, 0.0f, null);
                    return;
                }
                this.f131a.drawCylinder(this.f126a, this.f127a, false, false, sqrt / 300.0f, this.f135a);
                return;
            }
            if ((this.f133a.b ^ this.f133a.c) && ((float) currentTimeMillis) - this.a > 150.0f) {
                a(this.f133a.b ? 1.0f : this.f133a.c ? -1.0f : 0.0f);
            }
            int floor = (int) Math.floor((double) this.f131a.posY);
            if (this.f131a.block.size() == 1) {
                int i = floor;
                while (i >= 0) {
                    if (this.f131a.field[(int) ((((((Game.Coord) this.f131a.block.get(0)).x + this.f131a.x) % 15.0f) + 15.0f) % 15.0f)][i] == null) {
                        break;
                    }
                    i--;
                }
                if (i < 0) {
                    a(floor + 1);
                    this.f131a.drawCylinder(this.f126a, this.f127a, true, false, 0.0f, null);
                    return;
                }
            } else {
                int size = this.f131a.block.size() - 1;
                while (size >= 0) {
                    int i2 = (int) (((Game.Coord) this.f131a.block.get(size)).y + ((float) floor));
                    if (i2 >= 0) {
                        if (i2 >= 0 && i2 < 15) {
                            if (this.f131a.field[(int) ((((((Game.Coord) this.f131a.block.get(size)).x + this.f131a.x) % 15.0f) + 15.0f) % 15.0f)][i2] != null) {
                            }
                        }
                        size--;
                    }
                    a(floor + 1);
                    this.f131a.drawCylinder(this.f126a, this.f127a, true, false, 0.0f, null);
                    return;
                }
            }
            this.f131a.drawCylinder(this.f126a, this.f127a, true, false, 0.0f, null);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
         arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
         candidates:
          ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
          ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
        public void onCreate(SurfaceHolder surfaceHolder) {
            setTouchEventsEnabled(true);
            this.f123a = 40;
            this.f139b = TorusWallpaperService.a;
            if (this.f139b) {
                this.c = ((float) (TorusWallpaperService.a(TorusWallpaperService.this) - TorusWallpaperService.this.f121a)) * 0.5f;
            } else {
                this.c = 0.0f;
            }
            this.f127a = new Paint();
            this.f125a = BitmapFactory.decodeResource(TorusWallpaperService.this.getResources(), R.drawable.traditionalbackground);
            this.f133a = new b(this);
            if (TorusWallpaperService.a(TorusWallpaperService.this) == 480) {
                this.g = 1.0f;
            } else if (TorusWallpaperService.a(TorusWallpaperService.this) == 533) {
                this.g = 1.5f;
            } else {
                this.g = 1.5f;
            }
            this.f138b = BitmapFactory.decodeResource(TorusWallpaperService.this.getResources(), R.drawable.base0);
            Matrix matrix = new Matrix();
            matrix.postScale(this.g, this.g);
            this.f138b = Bitmap.createBitmap(this.f138b, 0, 0, this.f138b.getWidth(), this.f138b.getHeight(), matrix, true);
            this.f131a = new Game();
            f();
            if (isPreview()) {
                this.f129a = (WindowManager) TorusWallpaperService.this.getSystemService("window");
                this.f128a = new WindowManager.LayoutParams(-1, -2);
                this.f128a.alpha = 0.0f;
                this.f128a.flags = 8;
                this.f128a.flags |= 262144;
                this.f128a.flags |= 512;
                this.f128a.type = 2003;
                DisplayMetrics displayMetrics = new DisplayMetrics();
                this.f129a.getDefaultDisplay().getMetrics(displayMetrics);
                this.f128a.height = (int) (displayMetrics.density * 50.0f);
                this.f128a.gravity = 48;
                this.f128a.format = -1;
                this.f128a.token = null;
                this.f128a.x = 0;
                this.f128a.y = 0;
                this.f130a = new AdView(TorusWallpaperService.this, null);
                this.f130a.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
                this.f130a.setBackgroundColor(-16777216);
                this.f130a.setPrimaryTextColor(-1);
                this.f130a.setSecondaryTextColor(-3355444);
                this.f130a.setKeywords("Android app game wallpaper tetris");
                this.f130a.setRequestInterval(20);
            }
        }

        public void onDestroy() {
            super.onDestroy();
            TorusWallpaperService.a(TorusWallpaperService.this).removeCallbacks(this.f134a);
            if (this.f130a != null) {
                this.f130a.setRequestInterval(0);
                this.f130a.setAdListener(null);
                this.f130a.cleanup();
                this.f130a = null;
                if (this.f129a != null) {
                    try {
                        this.f129a.removeView(this.f130a);
                        this.f129a = null;
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            }
        }

        public void onFailedToReceiveAd(AdView adView) {
        }

        public void onFailedToReceiveRefreshedAd(AdView adView) {
        }

        public void onReceiveAd(AdView adView) {
            if (isPreview()) {
                if (this.f128a != null) {
                    this.f128a.alpha = 0.5f;
                }
                if (this.f129a != null && this.f130a != null) {
                    try {
                        this.f129a.removeView(this.f130a);
                    } catch (Exception e2) {
                    } finally {
                        this.f129a.addView(this.f130a, this.f128a);
                    }
                }
            }
        }

        public void onReceiveRefreshedAd(AdView adView) {
            if (isPreview()) {
                if (this.f128a != null) {
                    this.f128a.alpha = 0.5f;
                }
                if (this.f129a != null && this.f130a != null) {
                    try {
                        this.f129a.removeView(this.f130a);
                    } catch (Exception e2) {
                    } finally {
                        this.f129a.addView(this.f130a, this.f128a);
                    }
                }
            }
        }

        public void onSurfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
            if (i2 > i3) {
                int unused = TorusWallpaperService.this.f121a = i3;
                int unused2 = TorusWallpaperService.this.b = i2;
                this.f139b = TorusWallpaperService.a = true;
                this.c = ((float) (TorusWallpaperService.a(TorusWallpaperService.this) - TorusWallpaperService.this.f121a)) * 0.5f;
            } else {
                int unused3 = TorusWallpaperService.this.f121a = i2;
                int unused4 = TorusWallpaperService.this.b = i3;
                this.f139b = TorusWallpaperService.a = false;
                this.c = 0.0f;
            }
            a();
        }

        public void onSurfaceCreated(SurfaceHolder surfaceHolder) {
            super.onSurfaceCreated(surfaceHolder);
            a();
            b();
        }

        public void onSurfaceDestroyed(SurfaceHolder surfaceHolder) {
            super.onSurfaceDestroyed(surfaceHolder);
            this.f136a = false;
            TorusWallpaperService.a(TorusWallpaperService.this).removeCallbacks(this.f134a);
        }

        public void onVisibilityChanged(boolean z) {
            this.f136a = z;
            if (z) {
                b();
                if (isPreview() && this.f129a != null && this.f130a != null) {
                    this.f130a.setAdListener(this);
                    this.f130a.requestFreshAd();
                    try {
                        this.f129a.removeView(this.f130a);
                    } catch (Exception e2) {
                    } finally {
                        this.f129a.addView(this.f130a, this.f128a);
                    }
                }
            } else {
                TorusWallpaperService.a(TorusWallpaperService.this).removeCallbacks(this.f134a);
                if (isPreview() && this.f129a != null && this.f130a != null) {
                    try {
                        this.f130a.setAdListener(null);
                        this.f129a.removeView(this.f130a);
                    } catch (Exception e3) {
                        e3.printStackTrace();
                    }
                }
            }
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (configuration.orientation == 1) {
            a = false;
        }
        if (configuration.orientation == 2) {
            a = true;
        }
    }

    public WallpaperService.Engine onCreateEngine() {
        new DisplayMetrics();
        DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
        this.f121a = displayMetrics.widthPixels;
        this.b = displayMetrics.heightPixels;
        if (this.f121a < this.b) {
            a = false;
        } else {
            int i = this.f121a;
            this.f121a = this.b;
            this.b = i;
            a = true;
        }
        return new TorusEngine();
    }
}
