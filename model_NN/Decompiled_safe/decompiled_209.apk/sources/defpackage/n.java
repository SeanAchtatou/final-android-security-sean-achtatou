package defpackage;

import android.webkit.WebView;
import com.google.ads.AdActivity;
import com.google.ads.util.a;
import java.util.HashMap;

/* renamed from: n  reason: default package */
public final class n implements i {
    public final void a(d dVar, HashMap<String, String> hashMap, WebView webView) {
        String str = hashMap.get(AdActivity.URL_PARAM);
        if (str == null) {
            a.e("Could not get URL from click gmsg.");
        } else {
            new Thread(new w(str, webView.getContext().getApplicationContext())).start();
        }
    }
}
