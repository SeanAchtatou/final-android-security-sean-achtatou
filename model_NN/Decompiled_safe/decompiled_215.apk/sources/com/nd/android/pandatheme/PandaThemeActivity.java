package com.nd.android.pandatheme;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import com.nd.android.pandatheme.Luffy_Theme.R;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class PandaThemeActivity extends Activity {
    private static String APK_NAME = "pandahome2";
    private static String Home2Launcher = "com.nd.android.launcher.Launcher";
    /* access modifiers changed from: private */
    public static String Home2Market = "com.nd.android.pandahome2.manage.shop.ThemeShopMainActivity";
    /* access modifiers changed from: private */
    public static String Home2Pkg = "com.nd.android.pandahome2";
    private static String HomeLauncher = "com.nd.android.pandahome.home.Launcher";
    private static String HomePkg = "com.nd.android.pandahome";
    private static String SAVE_PATH = "/sdcard/pandatheme/";
    /* access modifiers changed from: private */
    public String[] apkArray = {"PandaHome2.apk.1", "PandaHome2.apk.2", "PandaHome2.apk.3", "PandaHome2.apk.4", "PandaHome2.apk.5", "PandaHome2.apk.6"};
    /* access modifiers changed from: private */
    public String apkPath = (String.valueOf(SAVE_PATH) + APK_NAME);
    BroadcastReceiver mInstallReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if ("android.intent.action.PACKAGE_ADDED".equals(intent.getAction()) || "android.intent.action.PACKAGE_CHANGED".equals(intent.getAction())) {
                PandaThemeActivity.this.applyTheme(context);
            }
        }
    };
    /* access modifiers changed from: private */
    public int[] resIds;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.main_layout);
        setTitle((int) R.string.app_title);
        Util.checkMkdirs(SAVE_PATH);
        new loadPandaHome(this).start();
        ((TextView) findViewById(R.id.desc)).getPaint().setFakeBoldText(true);
        final Context ctx = getBaseContext();
        IntentFilter filter = new IntentFilter("android.intent.action.PACKAGE_ADDED");
        filter.addAction("android.intent.action.PACKAGE_CHANGED");
        filter.addDataScheme("package");
        registerReceiver(this.mInstallReceiver, filter);
        ((Button) findViewById(R.id.btn_apply)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PandaThemeActivity.this.applyTheme(ctx);
            }
        });
        ((Button) findViewById(R.id.btn_theme_market)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String uriString;
                Intent intent = new Intent("android.intent.action.VIEW");
                if (Util.isZh(ctx)) {
                    uriString = "http://mobile.91.com/theme/Android.html";
                } else {
                    uriString = "http://market.pandaapp.com/theme/";
                }
                intent.setData(Uri.parse(uriString));
                PandaThemeActivity.this.startActivity(intent);
            }
        });
        ((Button) findViewById(R.id.btn_down_more)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    PandaThemeActivity.this.startHomeActivity(ctx, PandaThemeActivity.Home2Pkg, PandaThemeActivity.Home2Market);
                } catch (Exception e) {
                    PandaThemeActivity.this.showDialog(ctx);
                }
            }
        });
        this.resIds = new int[]{R.drawable.preview0, R.drawable.preview1, R.drawable.preview2};
        ((Gallery) findViewById(R.id.gallery)).setAdapter((SpinnerAdapter) new ImageAdapter(this));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        unregisterReceiver(this.mInstallReceiver);
        super.onDestroy();
    }

    public void applyTheme(Context ctx) {
        try {
            startHomeActivity(ctx, Home2Pkg, Home2Launcher);
        } catch (Exception e) {
            try {
                startHomeActivity(ctx, HomePkg, HomeLauncher);
            } catch (Exception e2) {
                showDialog(ctx);
            }
        }
    }

    /* access modifiers changed from: private */
    public void showDialog(final Context ctx) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage((int) R.string.tip_no_panda);
        builder.setPositiveButton((int) R.string.btn_common_install, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                try {
                    PackageManager pm = ctx.getPackageManager();
                    if (pm.getPackageArchiveInfo(PandaThemeActivity.this.apkPath, 1).versionCode > pm.getPackageInfo(PandaThemeActivity.Home2Pkg, 1).versionCode) {
                        Util.installApplication(ctx, new File(PandaThemeActivity.this.apkPath));
                    }
                } catch (Exception e) {
                    Util.installApplication(ctx, new File(PandaThemeActivity.this.apkPath));
                    e.printStackTrace();
                }
            }
        });
        builder.setNegativeButton((int) R.string.btn_common_cancel, (DialogInterface.OnClickListener) null);
        builder.show();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void startHomeActivity(Context ctx, String packageName, String activityName) throws PackageManager.NameNotFoundException {
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.LAUNCHER");
        intent.setComponent(new ComponentName(packageName, activityName));
        intent.setFlags(270532608);
        intent.putExtra("from", "pandatheme:" + ctx.getPackageName());
        intent.putExtra("hide_icon", true);
        startActivity(intent);
        finish();
    }

    private class loadPandaHome extends Thread {
        private Context ctx;

        public loadPandaHome(Context ctx2) {
            this.ctx = ctx2;
        }

        public void run() {
            try {
                OutputStream Fos = new BufferedOutputStream(new FileOutputStream(PandaThemeActivity.this.apkPath));
                AssetManager am = this.ctx.getAssets();
                for (String apk : PandaThemeActivity.this.apkArray) {
                    InputStream is = am.open(apk);
                    int TotalLength = 0;
                    try {
                        TotalLength = is.available();
                    } catch (IOException e) {
                    }
                    byte[] buffer = new byte[TotalLength];
                    int len = 0;
                    try {
                        len = is.read(buffer);
                    } catch (IOException e2) {
                    }
                    Fos.write(buffer, 0, len);
                    is.close();
                }
                Fos.close();
            } catch (Exception e3) {
                Log.i("create apk  ", e3.getMessage());
            }
        }
    }

    public class ImageAdapter extends BaseAdapter {
        private Context mContext;
        int mGalleryItemBackground;

        public ImageAdapter(Context context) {
            this.mContext = context;
            this.mGalleryItemBackground = PandaThemeActivity.this.obtainStyledAttributes(R.styleable.Gallery).getResourceId(0, 0);
        }

        public int getCount() {
            return PandaThemeActivity.this.resIds.length;
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView imageView = new ImageView(this.mContext);
            imageView.setImageResource(PandaThemeActivity.this.resIds[position]);
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            imageView.setLayoutParams(new Gallery.LayoutParams(200, 268));
            imageView.setBackgroundResource(this.mGalleryItemBackground);
            return imageView;
        }
    }
}
