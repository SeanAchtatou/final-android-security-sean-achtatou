package com.waterbear.am;

public final class R {

    public static final class array {
        public static final int gametype_array = 2131099648;
        public static final int wordlen_array = 2131099649;
    }

    public static final class attr {
    }

    public static final class drawable {
        public static final int icon = 2130837504;
    }

    public static final class id {
        public static final int AMView = 2131230722;
        public static final int StartGame = 2131230731;
        public static final int about_button = 2131230726;
        public static final int gametype_spinner = 2131230728;
        public static final int internalLayout = 2131230721;
        public static final int mainLayout = 2131230720;
        public static final int newgame_layout = 2131230727;
        public static final int resume_button = 2131230725;
        public static final int start_button = 2131230724;
        public static final int startscreen_layout = 2131230723;
        public static final int widget32 = 2131230730;
        public static final int wordlen_spinner = 2131230729;
    }

    public static final class layout {
        public static final int main = 2130903040;
        public static final int menu = 2130903041;
    }

    public static final class raw {
        public static final int anagram_words10 = 2130968576;
        public static final int anagram_words11 = 2130968577;
        public static final int anagram_words12 = 2130968578;
        public static final int anagram_words13 = 2130968579;
        public static final int anagram_words14 = 2130968580;
        public static final int anagram_words15 = 2130968581;
        public static final int anagram_words16 = 2130968582;
        public static final int anagram_words4 = 2130968583;
        public static final int anagram_words5 = 2130968584;
        public static final int anagram_words6 = 2130968585;
        public static final int anagram_words7 = 2130968586;
        public static final int anagram_words8 = 2130968587;
        public static final int anagram_words9 = 2130968588;
    }

    public static final class string {
        public static final int app_name = 2131034112;
        public static final int gametype_prompt = 2131034113;
        public static final int wordlen_prompt = 2131034114;
    }

    public static final class style {
        public static final int normal_text = 2131165185;
        public static final int title_text = 2131165184;
    }
}
