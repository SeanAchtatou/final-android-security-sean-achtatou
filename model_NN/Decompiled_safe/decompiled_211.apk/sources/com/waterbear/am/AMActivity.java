package com.waterbear.am;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.waterbear.am.AMView;

public class AMActivity extends Activity {
    public static final int DIALOG_ABOUT = 0;
    public static final int DIALOG_CORRUPTWARN = 3;
    public static final int DIALOG_DELETESTATS = 2;
    public static final int DIALOG_NEWGAME = 1;
    private static String ICICLE_KEY = "anagramathica-view";
    static final int MENU_OTHER = 1;
    static final int MENU_RESET = 0;
    String aboutText;
    /* access modifiers changed from: private */
    public AMView amView;
    String[] dialogs = {"Anagram Mathica Version ", "\n\nTry to find 10 scrambled words or scrambled equations as quickly as possible. Hints cost a time penalty which is a minute multiplied by the length of the word. Letters can be deselected by touch as well as by pressing delete button.\n\nStored statistics can be cleared by pressing the menu button.\n\nPlease send any comments or queries to gordon.gm31@gmail.com\n\nThis game uses a subset of SCOWL wordlists (c) Kevin Atkinson. To see further copyright information visit http://wordlist.sourceforge.net/", "OK", "Are you sure you want to start a new game? Saved game will be lost.", "Yes", "No", "Do you really want to delete stored data?", "Yes", "No", "Anagram Mathica Upgraded \n\nSome stored statistics (such as success rate, average time, best time) may be inaccurate because of errors in previous version 0.22 released 25th July 2011. \n\nIf you would like to delete all saved data select Delete, or press the menu button and select Delete Saved Data in the future.\n\nSorry about the mistake.", "Delete Saved Data", "Maybe Later"};
    public Spinner gameTypeSpinner;
    public LinearLayout newGameLayout;
    public Spinner wordLenSpinner;

    public class MyOnItemSelectedListener implements AdapterView.OnItemSelectedListener {
        static final int GAMETYPE = 1;
        static final int WORDLEN = 0;
        boolean initialised = false;
        int type;

        public MyOnItemSelectedListener(int setType) {
            this.type = setType;
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
            if (this.type == 0) {
                AMActivity.this.amView.thread.wordLenOptionSelected(pos);
            } else {
                AMActivity.this.amView.thread.gameTypeOptionSelected(pos);
            }
            AMActivity.this.onResume();
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
            AMActivity.this.onResume();
        }
    }

    public void setAboutText(String version) {
        this.aboutText = String.valueOf(this.dialogs[0]) + version + this.dialogs[1];
    }

    public boolean dispatchTouchEvent(MotionEvent me) {
        return super.dispatchTouchEvent(me);
    }

    public Dialog onCreateDialog(int i) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        switch (i) {
            case 0:
                builder.setMessage(this.aboutText).setPositiveButton(this.dialogs[2], new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        AMActivity.this.onResume();
                    }
                });
                break;
            case 1:
                builder.setMessage(this.dialogs[3]).setPositiveButton(this.dialogs[4], new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        AMActivity.this.amView.thread.setState(2);
                        AMActivity.this.onResume();
                    }
                }).setNegativeButton(this.dialogs[5], new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        AMActivity.this.onResume();
                    }
                });
                break;
            case 2:
                builder.setMessage(this.dialogs[6]).setPositiveButton(this.dialogs[7], new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        AMActivity.this.amView.thread.resetBestTimes();
                        AMActivity.this.onResume();
                    }
                }).setNegativeButton(this.dialogs[8], new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        AMActivity.this.onResume();
                    }
                });
                break;
            case 3:
                builder.setMessage(this.dialogs[9]).setPositiveButton(this.dialogs[10], new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        AMActivity.this.amView.thread.resetBestTimes();
                        AMActivity.this.onResume();
                    }
                }).setNegativeButton(this.dialogs[11], new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        AMActivity.this.onResume();
                    }
                });
                break;
        }
        return builder.create();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 0, 0, "Delete Saved Data");
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                showDialog(2);
                return true;
            case 1:
                getSharedPreferences(AMView.AMThread.PREFS, 0).edit().remove("version").commit();
                return true;
            default:
                return false;
        }
    }

    public void onOptionsMenuClosed(Menu menu) {
        onResume();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.main);
        this.amView = (AMView) findViewById(R.id.AMView);
        this.amView.thread.setActivity(this);
        this.wordLenSpinner = (Spinner) findViewById(R.id.wordlen_spinner);
        ArrayAdapter<CharSequence> wlAdapter = ArrayAdapter.createFromResource(this, R.array.wordlen_array, 17367048);
        wlAdapter.setDropDownViewResource(17367049);
        this.wordLenSpinner.setAdapter((SpinnerAdapter) wlAdapter);
        this.wordLenSpinner.setOnItemSelectedListener(new MyOnItemSelectedListener(0));
        this.gameTypeSpinner = (Spinner) findViewById(R.id.gametype_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.gametype_array, 17367048);
        adapter.setDropDownViewResource(17367049);
        this.gameTypeSpinner.setAdapter((SpinnerAdapter) adapter);
        this.gameTypeSpinner.setOnItemSelectedListener(new MyOnItemSelectedListener(1));
        this.newGameLayout = (LinearLayout) findViewById(R.id.newgame_layout);
        int gts = this.gameTypeSpinner.getContext().getSharedPreferences(AMView.AMThread.PREFS, 0).getInt("gameTypeSpin", 0);
        int wls = this.wordLenSpinner.getContext().getSharedPreferences(AMView.AMThread.PREFS, 0).getInt("wordLenSpin", 0);
        this.gameTypeSpinner.setSelection(gts);
        this.wordLenSpinner.setSelection(wls);
        this.amView.thread.gameType = gts;
        this.amView.thread.wordLen = wls;
        AdView adView = new AdView(this, AdSize.BANNER, "a14dd7048676013");
        ((FrameLayout) findViewById(R.id.mainLayout)).addView(adView);
        AdRequest request = new AdRequest();
        request.addTestDevice(AdRequest.TEST_EMULATOR);
        request.addTestDevice("D7CDAAA43C86F60B32B4299ACC5E1954");
        adView.loadAd(request);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.amView.thread.unPause();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.amView.thread.pause();
    }
}
