package com.waterbear.am;

import java.util.HashSet;

public class AnagraWa {
    HashSet<String> words = new HashSet<>();

    public AnagraWa() {
    }

    public AnagraWa(String firstWord) {
        this.words.add(firstWord);
    }

    public void addWord(String word) {
        this.words.add(word);
    }
}
