package com.waterbear.am;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;

public abstract class AMButton {
    static Paint buttonPaint = new Paint();
    static Paint buttonSelectPaint = new Paint();
    static Paint buttonUnselectablePaint = new Paint();
    static Paint shadow = new Paint();
    static Paint textPaint = new Paint();
    RectF bounds = new RectF();
    PointF position = new PointF();
    boolean selectable;
    boolean selected;
    String text = new String();
    Rect textBounds = new Rect();

    /* access modifiers changed from: package-private */
    public abstract void buttonPush();

    static {
        buttonPaint.setColor(-1);
        buttonSelectPaint.setColor(-16711936);
        buttonUnselectablePaint.setColor(-7829368);
        textPaint.setColor(-16777216);
        shadow.setARGB(100, 0, 0, 0);
        textPaint.setTextSize(18.0f);
        buttonPaint.setAntiAlias(true);
        textPaint.setAntiAlias(true);
        buttonSelectPaint.setAntiAlias(true);
        buttonUnselectablePaint.setAntiAlias(true);
        shadow.setAntiAlias(true);
    }

    public AMButton(String text2, boolean selectable2) {
        this.text = text2;
        this.selectable = selectable2;
    }

    public void setTextBounds() {
        textPaint.getTextBounds(this.text, 0, this.text.length(), this.textBounds);
        this.bounds.set(this.textBounds);
        this.bounds.left -= (float) (this.textBounds.width() / 2);
        this.bounds.top -= (float) this.textBounds.height();
        this.bounds.right += (float) (this.textBounds.width() / 2);
        this.bounds.bottom += (float) this.textBounds.height();
    }

    public void setPositionXCentred(int halfW, int posY) {
        xCentreRectangle(this.textBounds, (float) halfW, posY);
        xCentreRectangle(this.bounds, (float) halfW, posY);
    }

    public static void xCentreRectangle(RectF rect, float halfW, int posY) {
        int centerX = (int) (halfW - rect.centerX());
        rect.left += (float) centerX;
        rect.right += (float) centerX;
        rect.top += (float) posY;
        rect.bottom += (float) posY;
    }

    public static void xCentreRectangle(Rect rect, float halfW, int posY) {
        int centerX = ((int) halfW) - rect.centerX();
        rect.left += centerX;
        rect.right += centerX;
        rect.top += posY;
        rect.bottom += posY;
    }

    public void paint(Canvas c) {
        this.bounds.left += 8.0f;
        this.bounds.top += 8.0f;
        this.bounds.right += 8.0f;
        this.bounds.bottom += 8.0f;
        c.drawRoundRect(this.bounds, 10.0f, 10.0f, shadow);
        this.bounds.left -= 8.0f;
        this.bounds.top -= 8.0f;
        this.bounds.right -= 8.0f;
        this.bounds.bottom -= 8.0f;
        if (this.selected) {
            c.drawRoundRect(this.bounds, 10.0f, 10.0f, buttonSelectPaint);
        } else if (this.selectable) {
            c.drawRoundRect(this.bounds, 10.0f, 10.0f, buttonPaint);
        } else {
            c.drawRoundRect(this.bounds, 10.0f, 10.0f, buttonUnselectablePaint);
        }
        c.drawText(this.text, (float) this.textBounds.left, (float) this.textBounds.bottom, textPaint);
    }

    public void actionDown(float x, float y) {
        if (this.selectable && this.bounds.contains(x, y)) {
            this.selected = true;
        }
    }

    public void actionUp(float x, float y) {
        if (this.selected && this.bounds.contains(x, y)) {
            buttonPush();
        }
        this.selected = false;
    }

    public void actionMove(float x, float y) {
        if (this.selected && !this.bounds.contains(x, y)) {
            this.selected = false;
        }
    }
}
