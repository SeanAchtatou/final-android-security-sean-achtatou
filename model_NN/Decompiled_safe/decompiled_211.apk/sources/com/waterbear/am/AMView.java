package com.waterbear.am;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;

public class AMView extends SurfaceView implements SurfaceHolder.Callback {
    public AMActivity activity;
    SharedPreferences sp;
    public AMThread thread;

    class AMThread extends Thread {
        static final int BUTTON_BACKSPACE = 0;
        static final int BUTTON_HINT = 1;
        static final int BUTTON_MIX = 2;
        static final int BUTTON_NEXT = 4;
        static final int BUTTON_PREV = 3;
        public static final String PREFS = "prefs";
        public static final int STATE_NEWGAME = 2;
        public static final int STATE_PLAYING = 4;
        public static final int STATE_RESUME = 3;
        public static final int STATE_STARTSCREEN = 1;
        public static final int STATE_UNINITIALISED = 0;
        public static final int STATE_WIN = 5;
        String[] _anagramString = {"mAnagram0", "mAnagram1", "mAnagram2", "mAnagram3", "mAnagram4", "mAnagram5", "mAnagram6", "mAnagram7", "mAnagram8", "mAnagram9"};
        Rect _newGameBounds = new Rect();
        String _newGameStr = "Start New Game";
        int _tempTimer = 0;
        int _timePenaltyCounter = 0;
        int _timePenaltyStrWidth = 0;
        Rect _titleBounds = new Rect();
        char[] _titleChar1 = {'A', 'N', 'A', 'G', 'R', 'A', 'M'};
        char[] _titleChar2 = {'M', 'A', 'T', 'H', 'I', 'C', 'A'};
        Point _titlePoint1 = new Point();
        Point _titlePoint2 = new Point();
        String _titleStr = "Anagram Mathica";
        String _wAvgTimeStr;
        String _wFastTimeStr;
        boolean _wNewRecord = false;
        String _wTimeStr;
        String[] _winStr = new String[12];
        AMButton aboutBut;
        public AMActivity activity;
        String anaNoText;
        Anagram[] anagram;
        float[][] avgTime = ((float[][]) Array.newInstance(Float.TYPE, 5, 3));
        int[][] bestTime = ((int[][]) Array.newInstance(Integer.TYPE, 5, 3));
        int buttonSelected = -1;
        String[] buttonText = {"Del", "Hint", "Mix", "Prev", "Next"};
        int canvasHeight;
        int canvasWidth;
        Context context;
        int curAnagram = 0;
        Dict[] dict;
        String emptyStr = "";
        public int gameType;
        Paint greenPaint = new Paint();
        long lastTime;
        String[] newGameInfoText = {"", "", ""};
        boolean nextToggle = false;
        boolean paused;
        boolean prevToggle = false;
        AMButton resumeBut;
        boolean running = true;
        Paint smallFont = new Paint();
        AMButton startBut;
        AMButton startGameBut;
        long startTime;
        int state = 0;
        boolean surfaceCreated = false;
        SurfaceHolder surfaceHolder;
        RectF tempBounds = new RectF();
        Rect textTempBounds = new Rect();
        int timePenalty = 0;
        int[] timePenaltyFade = new int[5];
        String[] timePenaltyStr = new String[5];
        int timer = 0;
        Rect timerRect = new Rect();
        String timerStr = "0";
        int[][] timesCompleted = ((int[][]) Array.newInstance(Integer.TYPE, 5, 3));
        int[][] timesPlayed = ((int[][]) Array.newInstance(Integer.TYPE, 5, 3));
        Paint titlePaint = new Paint();
        Paint tpPaint = new Paint();
        String version;
        public int wordLen;
        int xDiff = 0;

        public Bundle saveState() {
            if (this.state != 4) {
                return null;
            }
            Bundle map = new Bundle();
            for (int i = 0; i < this.anagram.length; i++) {
                map.putBundle(this._anagramString[i], this.anagram[i].saveState());
            }
            map.putInt("mCurAnagram", this.curAnagram);
            map.putInt("mTimer", this.timer);
            map.putInt("mTimePenalty", this.timePenalty);
            return map;
        }

        public void restoreState(Bundle map) {
            setState(1);
            if (this.anagram == null) {
                this.anagram = new Anagram[10];
            }
            for (int i = 0; i < this.anagram.length; i++) {
                this.anagram[i] = Anagram.loadAnagram(map, this);
            }
            this.curAnagram = map.getInt("mCurAnagram");
            this.timer = map.getInt("mTimer");
            this.timePenalty = map.getInt("mTimePenalty");
        }

        public AMThread(SurfaceHolder surfaceHolder2, Context context2) {
            this.surfaceHolder = surfaceHolder2;
            this.context = context2;
            Resources res = context2.getResources();
            this.dict = new Dict[]{new Dict(res, R.raw.anagram_words4, this, 4), new Dict(res, R.raw.anagram_words5, this, 5), new Dict(res, R.raw.anagram_words6, this, 6), new Dict(res, R.raw.anagram_words7, this, 7), new Dict(res, R.raw.anagram_words8, this, 8), new Dict(res, R.raw.anagram_words9, this, 9), new Dict(res, R.raw.anagram_words10, this, 10), new Dict(res, R.raw.anagram_words11, this, 11), new Dict(res, R.raw.anagram_words12, this, 12), new Dict(res, R.raw.anagram_words13, this, 13), new Dict(res, R.raw.anagram_words14, this, 14)};
            for (int i = 0; i < this.timePenaltyStr.length; i++) {
                this.timePenaltyStr[i] = "";
            }
            this.titlePaint.setAntiAlias(true);
            this.greenPaint.setAntiAlias(true);
            this.tpPaint.setAntiAlias(true);
            this.smallFont.setAntiAlias(true);
        }

        public void resetBestTimes() {
            SharedPreferences.Editor ed = this.context.getSharedPreferences(PREFS, 0).edit();
            for (int i = 0; i < this.timesPlayed.length; i++) {
                for (int j = 0; j < this.timesPlayed[0].length; j++) {
                    this.timesPlayed[i][j] = 0;
                    this.timesCompleted[i][j] = 0;
                    this.bestTime[i][j] = 0;
                    this.avgTime[i][j] = 0.0f;
                    ed.remove("timesPlayed" + i + j);
                    ed.remove("timesCompleted" + i + j);
                    ed.remove("bestTime" + i + j);
                    ed.remove("avgTime" + i + j);
                }
            }
            ed.commit();
            this.anagram = null;
            saveToFileMain(true);
            this.resumeBut.selectable = false;
        }

        public Mathagram createMathagram(int length) {
            int a;
            int b;
            int c;
            if (length < 5) {
                return null;
            }
            if (length == 5) {
                c = ((int) (Math.random() * 9.0d)) + 1;
                b = (int) (Math.random() * ((double) c));
                a = c - b;
            } else if (length == 6) {
                a = ((int) (Math.random() * 9.0d)) + 1;
                b = (((int) (Math.random() * ((double) a))) + 10) - a;
                c = a + b;
            } else if (length == 7) {
                a = (int) (Math.random() * 10.0d);
                b = (int) (Math.random() * 90.0d);
                c = a + b;
            } else if (length == 8) {
                if (((int) (Math.random() * 8.0d)) < 7) {
                    a = ((int) (Math.random() * 80.0d)) + 10;
                    b = ((int) (Math.random() * ((double) (90 - a)))) + 10;
                    c = a + b;
                } else {
                    a = ((int) (Math.random() * 10.0d)) + 91;
                    b = 9 - ((int) (Math.random() * ((double) (a - 90))));
                    c = a + b;
                }
            } else if (length == 9) {
                if (((int) (Math.random() * 8.0d)) < 7) {
                    a = ((int) (Math.random() * 50.0d)) + 50;
                    b = ((int) (Math.random() * 50.0d)) + 50;
                    c = a + b;
                } else {
                    a = ((int) (Math.random() * 880.0d)) + 100;
                    b = (int) (Math.random() * 10.0d);
                    c = a + b;
                }
            } else if (length == 10) {
                a = ((int) (Math.random() * 800.0d)) + 100;
                b = ((int) (Math.random() * 90.0d)) + 10;
                c = a + b;
            } else if (length == 11 || length == 12) {
                a = ((int) (Math.random() * 900.0d)) + 100;
                b = ((int) (Math.random() * 900.0d)) + 100;
                c = a + b;
            } else {
                a = ((int) (Math.random() * 900.0d)) + 100;
                b = ((int) (Math.random() * 9000.0d)) + 1000;
                c = a + b;
            }
            return new Mathagram(String.format("%s+%s=%s", String.valueOf(a), String.valueOf(b), String.valueOf(c)), this);
        }

        public void createNewGame() {
            boolean used;
            boolean used2;
            this.curAnagram = 0;
            this.timePenalty = 0;
            this.anaNoText = "1/10";
            for (int i = 0; i < this.timePenaltyFade.length; i++) {
                this.timePenaltyFade[i] = 0;
            }
            this.anagram = new Anagram[10];
            if (this.gameType == 0) {
                Anagram[] temp = this.dict[this.wordLen * 2].generateAnagrams(3);
                for (int i2 = 0; i2 < this.anagram.length; i2++) {
                    if (i2 % 2 == 0) {
                        if (i2 == 6) {
                            temp = this.dict[(this.wordLen * 2) + 1].generateAnagrams(2);
                        }
                        if (i2 < 5) {
                            this.anagram[i2] = temp[i2 / 2];
                        } else {
                            this.anagram[i2] = temp[(i2 / 2) - 3];
                        }
                    } else {
                        do {
                            this.anagram[i2] = createMathagram((this.wordLen * 2) + 5 + 0);
                            used2 = false;
                            for (int j = 0; j < i2 && !used2; j++) {
                                used2 = this.anagram[j].equals(this.anagram[i2]);
                            }
                        } while (used2);
                    }
                }
            }
            if (this.gameType == 1) {
                Anagram[] temp2 = this.dict[this.wordLen * 2].generateAnagrams(5);
                for (int i3 = 0; i3 < temp2.length; i3++) {
                    this.anagram[i3] = temp2[i3];
                }
                Anagram[] temp3 = this.dict[(this.wordLen * 2) + 1].generateAnagrams(5);
                for (int i4 = 0; i4 < temp3.length; i4++) {
                    this.anagram[i4 + 5] = temp3[i4];
                }
            }
            if (this.gameType == 2) {
                for (int i5 = 0; i5 < this.anagram.length; i5++) {
                    do {
                        this.anagram[i5] = createMathagram((this.wordLen * 2) + 5 + 0);
                        used = false;
                        for (int j2 = 0; j2 < i5 && !used; j2++) {
                            used = this.anagram[j2].equals(this.anagram[i5]);
                        }
                    } while (used);
                }
            }
            saveToFiles();
            setTimer(0);
            this.startTime = System.currentTimeMillis();
            incrementTimesPlayed();
        }

        public void incrementTimesPlayed() {
            int[] iArr = this.timesPlayed[this.wordLen];
            int i = this.gameType;
            iArr[i] = iArr[i] + 1;
            this.context.getSharedPreferences(PREFS, 0).edit().putInt("timesPlayed" + this.wordLen + this.gameType, this.timesPlayed[this.wordLen][this.gameType]).commit();
        }

        public void setState(int newState) {
            synchronized (this.surfaceHolder) {
                if (this.state != newState) {
                    switch (this.state) {
                        case 2:
                            this.activity.newGameLayout.setVisibility(8);
                            unPause();
                            break;
                    }
                    switch (newState) {
                        case 1:
                            this._titlePoint1.x = this.canvasWidth;
                            this._titlePoint1.y = (int) Anagram.tileSize.bottom;
                            this._titlePoint2.x = -this.canvasWidth;
                            this._titlePoint2.y = (int) (((double) Anagram.tileSize.bottom) * 1.75d);
                            if (this.startBut != null) {
                                this.startBut.selected = false;
                            }
                            this.resumeBut.selectable = this.anagram != null;
                            break;
                        case 2:
                            this.activity.newGameLayout.setVisibility(0);
                            setGameInfoText();
                            this.anagram = null;
                            saveToFileMain(true);
                            break;
                    }
                    this.state = newState;
                }
            }
        }

        public void timePenalty(int seconds) {
            this.timePenalty += seconds;
            this._timePenaltyCounter = (this._timePenaltyCounter + 1) % this.timePenaltyStr.length;
            this.timePenaltyStr[this._timePenaltyCounter] = "+" + setTimeString(seconds);
            this.timePenaltyFade[this._timePenaltyCounter] = 255;
            this._timePenaltyStrWidth = (int) this.tpPaint.measureText(this.timePenaltyStr[this._timePenaltyCounter]);
            setTimer(this.timer);
        }

        public void setTimer(int timer2) {
            this.timer = timer2;
            this.timerStr = setTimeString(timer2 + this.timePenalty);
            this.titlePaint.getTextBounds(this.timerStr, 0, this.timerStr.length(), this.timerRect);
        }

        public String setTimeString(int timer2) {
            if (timer2 > 3600) {
                return String.format("%d:%02d:%02d", Integer.valueOf(timer2 / 3600), Integer.valueOf((timer2 / 60) % 60), Integer.valueOf(timer2 % 60));
            }
            return String.format("%02d:%02d", Integer.valueOf((timer2 / 60) % 60), Integer.valueOf(timer2 % 60));
        }

        public int getTimer() {
            return this.timer;
        }

        public void unPause() {
            synchronized (this.surfaceHolder) {
                if (this.paused && this.running) {
                    this.paused = false;
                }
                if (!this.running) {
                    AMView.this.createThread();
                }
            }
        }

        public void pause() {
            synchronized (this.surfaceHolder) {
                if (this.state == 4) {
                    saveToFile(this.curAnagram);
                    saveToFileMain(false);
                    setState(1);
                }
                this.paused = true;
            }
        }

        /* JADX WARNING: CFG modification limit reached, blocks count: 158 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r5 = this;
                r2 = -8
                android.os.Process.setThreadPriority(r2)
            L_0x0004:
                boolean r2 = r5.surfaceCreated
                if (r2 != 0) goto L_0x000c
                boolean r2 = r5.running
                if (r2 != 0) goto L_0x0011
            L_0x000c:
                boolean r2 = r5.running
                if (r2 != 0) goto L_0x001e
                return
            L_0x0011:
                r2 = 50
                java.lang.Thread.sleep(r2)     // Catch:{ InterruptedException -> 0x0017 }
                goto L_0x0004
            L_0x0017:
                r2 = move-exception
                goto L_0x0004
            L_0x0019:
                r2 = 100
                java.lang.Thread.sleep(r2)     // Catch:{ InterruptedException -> 0x006c }
            L_0x001e:
                boolean r2 = r5.paused
                if (r2 == 0) goto L_0x0026
                boolean r2 = r5.running
                if (r2 != 0) goto L_0x0019
            L_0x0026:
                r0 = 0
                android.view.SurfaceHolder r2 = r5.surfaceHolder     // Catch:{ Exception -> 0x0056 }
                r3 = 0
                android.graphics.Canvas r0 = r2.lockCanvas(r3)     // Catch:{ Exception -> 0x0056 }
                android.view.SurfaceHolder r2 = r5.surfaceHolder     // Catch:{ Exception -> 0x0056 }
                monitor-enter(r2)     // Catch:{ Exception -> 0x0056 }
                int r3 = r5.state     // Catch:{ all -> 0x0053 }
                if (r3 != 0) goto L_0x0040
                int r3 = r0.getWidth()     // Catch:{ all -> 0x0053 }
                int r4 = r0.getHeight()     // Catch:{ all -> 0x0053 }
                r5.initPosCol(r3, r4)     // Catch:{ all -> 0x0053 }
            L_0x0040:
                boolean r3 = r5.paused     // Catch:{ all -> 0x0053 }
                if (r3 != 0) goto L_0x0047
                r5.update()     // Catch:{ all -> 0x0053 }
            L_0x0047:
                r5.doDraw(r0)     // Catch:{ all -> 0x0053 }
                monitor-exit(r2)     // Catch:{ all -> 0x0053 }
                if (r0 == 0) goto L_0x000c
                android.view.SurfaceHolder r2 = r5.surfaceHolder
                r2.unlockCanvasAndPost(r0)
                goto L_0x000c
            L_0x0053:
                r3 = move-exception
                monitor-exit(r2)     // Catch:{ all -> 0x0053 }
                throw r3     // Catch:{ Exception -> 0x0056 }
            L_0x0056:
                r2 = move-exception
                r1 = r2
                r1.printStackTrace()     // Catch:{ all -> 0x0063 }
                if (r0 == 0) goto L_0x000c
                android.view.SurfaceHolder r2 = r5.surfaceHolder
                r2.unlockCanvasAndPost(r0)
                goto L_0x000c
            L_0x0063:
                r2 = move-exception
                if (r0 == 0) goto L_0x006b
                android.view.SurfaceHolder r3 = r5.surfaceHolder
                r3.unlockCanvasAndPost(r0)
            L_0x006b:
                throw r2
            L_0x006c:
                r2 = move-exception
                goto L_0x001e
            */
            throw new UnsupportedOperationException("Method not decompiled: com.waterbear.am.AMView.AMThread.run():void");
        }

        private void update() {
            long now = System.currentTimeMillis();
            int elapsed = (int) (now - this.lastTime);
            if (elapsed >= 0) {
                switch (this.state) {
                    case 1:
                        if (((double) this._titlePoint1.x) > ((double) Anagram.tileSize.right) * 0.5d) {
                            this._titlePoint1.x -= elapsed;
                            if (((double) this._titlePoint1.x) < ((double) Anagram.tileSize.right) * 0.5d) {
                                this._titlePoint1.x = (int) (((double) Anagram.tileSize.right) * 0.5d);
                            }
                        }
                        if (((float) this._titlePoint2.x) < Anagram.tileSize.right) {
                            this._titlePoint2.x += elapsed;
                            if (((float) this._titlePoint2.x) > Anagram.tileSize.right) {
                                this._titlePoint2.x = (int) Anagram.tileSize.right;
                                break;
                            }
                        }
                        break;
                    case 4:
                        if (this.anagram != null) {
                            for (int i = 0; i < this.timePenaltyFade.length; i++) {
                                if (this.timePenaltyFade[i] != 0) {
                                    int[] iArr = this.timePenaltyFade;
                                    iArr[i] = iArr[i] - (elapsed / 2);
                                    if (this.timePenaltyFade[i] < 0) {
                                        this.timePenaltyFade[i] = 0;
                                    }
                                }
                            }
                            this._tempTimer = (int) (now - this.startTime);
                            if (this._tempTimer / 1000 > this.timer) {
                                setTimer(this.timer + 1);
                            }
                            if (this.nextToggle) {
                                this.xDiff = elapsed;
                                this.anagram[this.curAnagram].position.x -= (float) this.xDiff;
                                int nextAna = (this.curAnagram + 1) % this.anagram.length;
                                this.anagram[nextAna].position.x -= (float) this.xDiff;
                                if (this.anagram[nextAna].position.x < 0.0f) {
                                    this.anagram[nextAna].position.x = 0.0f;
                                    this.nextToggle = false;
                                    this.curAnagram = nextAna;
                                    this.anaNoText = String.format("%s/%s", Integer.valueOf(this.curAnagram + 1), Integer.valueOf(this.anagram.length));
                                    if (this.anagram[this.curAnagram].complete) {
                                        nextAnagram();
                                    }
                                }
                            }
                            if (this.prevToggle) {
                                this.xDiff = elapsed;
                                this.anagram[this.curAnagram].position.x += (float) this.xDiff;
                                int prevAna = ((this.curAnagram + this.anagram.length) - 1) % this.anagram.length;
                                this.anagram[prevAna].position.x += (float) this.xDiff;
                                if (this.anagram[prevAna].position.x > 0.0f) {
                                    this.anagram[prevAna].position.x = 0.0f;
                                    this.prevToggle = false;
                                    this.curAnagram = prevAna;
                                    this.anaNoText = String.format("%s/%s", Integer.valueOf(this.curAnagram + 1), Integer.valueOf(this.anagram.length));
                                    if (this.anagram[this.curAnagram].complete) {
                                        prevAnagram();
                                        break;
                                    }
                                }
                            }
                        }
                        break;
                }
            }
            this.lastTime = now;
        }

        private void doDraw(Canvas c) {
            switch (this.state) {
                case 1:
                    drawStartScreen(c);
                    return;
                case 2:
                    drawNewGame(c);
                    return;
                case 3:
                default:
                    return;
                case 4:
                    drawPlaying(c);
                    return;
                case STATE_WIN /*5*/:
                    drawWin(c);
                    return;
            }
        }

        private void drawStartScreen(Canvas c) {
            c.drawARGB(255, 100, 100, 100);
            int tilesPerLine = (int) (((float) this.canvasWidth) / Anagram.tileSize.right);
            for (int i = 0; i < this._titleChar1.length; i++) {
                this.tempBounds.left = ((float) (this._titlePoint1.x + 10)) + ((Anagram.tileSize.right / 2.0f) * ((float) (i % (tilesPerLine * 2))));
                this.tempBounds.top = ((float) (this._titlePoint1.y + 10)) + ((Anagram.tileSize.bottom / 2.0f) * ((float) ((i / tilesPerLine) / 2)));
                this.tempBounds.right = (this.tempBounds.left + (Anagram.tileSize.right / 2.0f)) - 5.0f;
                this.tempBounds.bottom = (this.tempBounds.top + (Anagram.tileSize.bottom / 2.0f)) - 5.0f;
                c.drawRoundRect(this.tempBounds, 5.0f, 5.0f, Anagram.shadow);
                this.tempBounds.left -= 4.0f;
                this.tempBounds.top -= 4.0f;
                this.tempBounds.right -= 4.0f;
                this.tempBounds.bottom -= 4.0f;
                c.drawRoundRect(this.tempBounds, 5.0f, 5.0f, Anagram.buttonPaint);
                this.smallFont.getTextBounds(this._titleChar1, i, 1, this.textTempBounds);
                c.drawText(this._titleChar1, i, 1, this.tempBounds.centerX() - ((float) this.textTempBounds.centerX()), this.tempBounds.centerY() - ((float) this.textTempBounds.centerY()), this.smallFont);
            }
            for (int i2 = 0; i2 < this._titleChar2.length; i2++) {
                this.tempBounds.left = ((float) (this._titlePoint2.x + 10)) + ((Anagram.tileSize.right / 2.0f) * ((float) (i2 % (tilesPerLine * 2))));
                this.tempBounds.top = ((float) (this._titlePoint2.y + 10)) + ((Anagram.tileSize.bottom / 2.0f) * ((float) ((i2 / tilesPerLine) / 2)));
                this.tempBounds.right = (this.tempBounds.left + (Anagram.tileSize.right / 2.0f)) - 5.0f;
                this.tempBounds.bottom = (this.tempBounds.top + (Anagram.tileSize.bottom / 2.0f)) - 5.0f;
                c.drawRoundRect(this.tempBounds, 5.0f, 5.0f, Anagram.shadow);
                this.tempBounds.left -= 4.0f;
                this.tempBounds.top -= 4.0f;
                this.tempBounds.right -= 4.0f;
                this.tempBounds.bottom -= 4.0f;
                c.drawRoundRect(this.tempBounds, 5.0f, 5.0f, Anagram.buttonPaint);
                this.smallFont.getTextBounds(this._titleChar2, i2, 1, this.textTempBounds);
                c.drawText(this._titleChar2, i2, 1, this.tempBounds.centerX() - ((float) this.textTempBounds.centerX()), this.tempBounds.centerY() - ((float) this.textTempBounds.centerY()), this.smallFont);
            }
            this.startBut.paint(c);
            this.resumeBut.paint(c);
            this.aboutBut.paint(c);
        }

        private void initWin() {
            this._winStr[0] = "Success";
            this._winStr[1] = "Time " + setTimeString(this.timer + this.timePenalty);
            if (this._wNewRecord) {
                this._winStr[3] = "New Record!";
            } else {
                this._winStr[3] = "";
            }
            this._winStr[5] = String.format("%s:%s", this.activity.gameTypeSpinner.getPrompt(), this.activity.gameTypeSpinner.getSelectedItem());
            this._winStr[6] = String.format("%s:%s", this.activity.wordLenSpinner.getPrompt(), this.activity.wordLenSpinner.getSelectedItem());
            this._winStr[9] = String.format("Success Rate %d%% (%d/%d)", Integer.valueOf(Math.round((((float) this.timesCompleted[this.wordLen][this.gameType]) / ((float) this.timesPlayed[this.wordLen][this.gameType])) * 100.0f)), Integer.valueOf(this.timesCompleted[this.wordLen][this.gameType]), Integer.valueOf(this.timesPlayed[this.wordLen][this.gameType]));
            this._winStr[10] = "Best Time " + setTimeString(this.bestTime[this.wordLen][this.gameType]);
            this._winStr[11] = "Average Time " + setTimeString((int) this.avgTime[this.wordLen][this.gameType]);
        }

        private void drawWin(Canvas c) {
            c.drawARGB(255, 0, 0, 0);
            for (int i = 0; i < this._winStr.length; i++) {
                if (this._winStr[i] != null) {
                    c.drawText(this._winStr[i], 10.0f, (float) (((this.canvasHeight * i) / this._winStr.length) + 30), this.titlePaint);
                }
            }
        }

        private void drawNewGame(Canvas c) {
            c.drawARGB(255, 100, 100, 100);
            c.drawText(this.newGameInfoText[0], 20.0f, (float) ((this.canvasHeight * 3) / 5), AMButton.textPaint);
            c.drawText(this.newGameInfoText[1], 20.0f, (float) (((this.canvasHeight * 3) / 5) + this.startGameBut.textBounds.height() + 20), AMButton.textPaint);
            c.drawText(this.newGameInfoText[2], 20.0f, (float) (((this.canvasHeight * 3) / 5) + ((this.startGameBut.textBounds.height() + 20) * 2)), AMButton.textPaint);
            this.startGameBut.paint(c);
        }

        private void drawPlaying(Canvas c) {
            c.drawARGB(255, 100, 100, 100);
            if (this.anagram != null) {
                this.anagram[this.curAnagram].paint(c);
                if (this.nextToggle) {
                    this.anagram[(this.curAnagram + 1) % this.anagram.length].paint(c);
                }
                if (this.prevToggle) {
                    this.anagram[((this.curAnagram + this.anagram.length) - 1) % this.anagram.length].paint(c);
                }
                Paint p = new Paint();
                p.setColor(-16777216);
                p.setAntiAlias(true);
                c.drawRect(0.0f, ((((float) this.canvasHeight) - (Anagram.tileSize.bottom / 2.0f)) - ((float) this.timerRect.height())) - 20.0f, (float) this.canvasWidth, (float) this.canvasHeight, p);
                for (int i = this._timePenaltyCounter; i < this.timePenaltyStr.length + this._timePenaltyCounter; i++) {
                    this.tpPaint.setARGB(this.timePenaltyFade[i % this.timePenaltyStr.length], 255, 0, 0);
                    c.drawText(this.timePenaltyStr[i % this.timePenaltyStr.length], (float) ((this.canvasWidth - this._timePenaltyStrWidth) - 10), ((((((float) this.canvasHeight) - (Anagram.tileSize.bottom / 2.0f)) - ((float) this.timerRect.height())) - 10.0f) - 50.0f) + ((float) (this.timePenaltyFade[i % this.timePenaltyStr.length] / 5)), this.tpPaint);
                }
                c.drawText(this.timerStr, (float) ((this.canvasWidth - this.timerRect.width()) - 10), (((float) this.canvasHeight) - (Anagram.tileSize.bottom / 2.0f)) - 10.0f, this.titlePaint);
                c.drawText(this.anaNoText, 10.0f, (((float) this.canvasHeight) - (Anagram.tileSize.bottom / 2.0f)) - 10.0f, this.titlePaint);
                this.tempBounds.bottom = (float) this.canvasHeight;
                this.tempBounds.top = ((float) this.canvasHeight) - (Anagram.tileSize.bottom / 2.0f);
                for (int i2 = 0; i2 < 5; i2++) {
                    this.tempBounds.left = (Anagram.tileSize.right * ((float) i2)) + 1.0f;
                    this.tempBounds.right = (this.tempBounds.left + Anagram.tileSize.right) - 1.0f;
                    if (i2 != 2) {
                        if (i2 == this.buttonSelected) {
                            c.drawRoundRect(this.tempBounds, 10.0f, 10.0f, Anagram.buttonSelectPaint);
                        } else {
                            c.drawRoundRect(this.tempBounds, 10.0f, 10.0f, Anagram.buttonPaint);
                        }
                        c.drawText(this.buttonText[i2], this.tempBounds.left + 10.0f, this.tempBounds.top + Anagram.textPaint.getTextSize(), Anagram.textPaint);
                    }
                }
            }
        }

        public void touchEvent(MotionEvent me) {
            switch (this.state) {
                case 1:
                    touchStartScreen(me);
                    return;
                case 2:
                    touchNewGame(me);
                    return;
                case 3:
                default:
                    return;
                case 4:
                    touchPlaying(me);
                    return;
            }
        }

        private void touchWin(MotionEvent me) {
            if (me.getAction() == 0) {
                setState(1);
            }
        }

        private void touchNewGame(MotionEvent me) {
            int action = me.getAction();
            float x = me.getX();
            float y = me.getY();
            if (action == 0) {
                this.startGameBut.actionDown(x, y);
            } else if (action == 1 || action == 3) {
                this.startGameBut.actionUp(x, y);
            } else if (action == 2) {
                this.startGameBut.actionMove(x, y);
            }
        }

        private void touchStartScreen(MotionEvent me) {
            int action = me.getAction();
            float x = me.getX();
            float y = me.getY();
            if (action == 0) {
                this.startBut.actionDown(x, y);
                this.resumeBut.actionDown(x, y);
                this.aboutBut.actionDown(x, y);
            } else if (action == 1 || action == 3) {
                this.startBut.actionUp(x, y);
                this.resumeBut.actionUp(x, y);
                this.aboutBut.actionUp(x, y);
            } else if (action == 2) {
                this.startBut.actionMove(x, y);
                this.resumeBut.actionMove(x, y);
                this.aboutBut.actionMove(x, y);
            }
        }

        private void touchPlaying(MotionEvent me) {
            int action = me.getAction();
            float x = me.getX();
            float y = me.getY();
            this.anagram[this.curAnagram].touch(action, x, y);
            if (y > ((float) this.canvasHeight) - (Anagram.tileSize.bottom / 2.0f)) {
                int tile = (int) (x / Anagram.tileSize.right);
                if (action == 0) {
                    if (tile == 0 || ((tile == 1 && !this.anagram[this.curAnagram].complete) || ((!this.nextToggle && tile == 4) || (!this.prevToggle && tile == 3)))) {
                        this.buttonSelected = tile;
                    }
                } else if (action == 1 || action == 3) {
                    if (tile == this.buttonSelected) {
                        switch (tile) {
                            case 0:
                                this.anagram[this.curAnagram].backspace();
                                break;
                            case 1:
                                this.anagram[this.curAnagram].hint();
                                break;
                            case 3:
                                prevAnagram();
                                break;
                            case 4:
                                nextAnagram();
                                break;
                        }
                    }
                    this.buttonSelected = -1;
                }
            } else {
                this.buttonSelected = -1;
            }
        }

        public void nextAnagram() {
            if (!this.nextToggle) {
                if (this.prevToggle) {
                    this.prevToggle = false;
                } else {
                    this.anagram[(this.curAnagram + 1) % this.anagram.length].position.x = (float) this.canvasWidth;
                    saveToFile(this.curAnagram);
                }
                this.nextToggle = true;
            }
        }

        public void prevAnagram() {
            if (!this.prevToggle) {
                if (this.nextToggle) {
                    this.nextToggle = false;
                } else {
                    this.anagram[((this.curAnagram + this.anagram.length) - 1) % this.anagram.length].position.x = (float) (-this.canvasWidth);
                    saveToFile(this.curAnagram);
                }
                this.prevToggle = true;
            }
        }

        public void anagramComplete() {
            int complete = 0;
            while (complete < this.anagram.length && this.anagram[complete].complete) {
                complete++;
            }
            if (complete == this.anagram.length) {
                if (this.bestTime[this.wordLen][this.gameType] == 0 || this.timer + this.timePenalty < this.bestTime[this.wordLen][this.gameType]) {
                    this.bestTime[this.wordLen][this.gameType] = this.timer + this.timePenalty;
                    this._wNewRecord = true;
                } else {
                    this._wNewRecord = false;
                }
                this.avgTime[this.wordLen][this.gameType] = (this.avgTime[this.wordLen][this.gameType] * ((float) this.timesCompleted[this.wordLen][this.gameType])) + ((float) this.timer) + ((float) this.timePenalty);
                int[] iArr = this.timesCompleted[this.wordLen];
                int i = this.gameType;
                iArr[i] = iArr[i] + 1;
                float[] fArr = this.avgTime[this.wordLen];
                int i2 = this.gameType;
                fArr[i2] = fArr[i2] / ((float) this.timesCompleted[this.wordLen][this.gameType]);
                SharedPreferences.Editor ed = this.context.getSharedPreferences(PREFS, 0).edit();
                ed.putInt("bestTime" + this.wordLen + this.gameType, this.bestTime[this.wordLen][this.gameType]);
                ed.putFloat("avgTime" + this.wordLen + this.gameType, this.avgTime[this.wordLen][this.gameType]);
                ed.putInt("timesCompleted" + this.wordLen + this.gameType, this.timesCompleted[this.wordLen][this.gameType]);
                ed.commit();
                this.anagram = null;
                saveToFileMain(true);
                initWin();
                setState(5);
                return;
            }
            nextAnagram();
        }

        public boolean menuButton() {
            if (this.state == 1) {
                return false;
            }
            return true;
        }

        public boolean backButton() {
            switch (this.state) {
                case 2:
                case STATE_WIN /*5*/:
                    setState(1);
                    return true;
                case 3:
                default:
                    return false;
                case 4:
                    saveToFileMain(false);
                    saveToFile(this.curAnagram);
                    setState(1);
                    return true;
            }
        }

        public void setActivity(AMActivity a) {
            this.activity = a;
            boolean storedInfoExists = false;
            for (int i = 0; i < this.timesPlayed.length; i++) {
                for (int j = 0; j < this.timesPlayed[0].length; j++) {
                    this.timesPlayed[i][j] = this.context.getSharedPreferences(PREFS, 0).getInt("timesPlayed" + i + j, 0);
                    this.timesCompleted[i][j] = this.context.getSharedPreferences(PREFS, 0).getInt("timesCompleted" + i + j, 0);
                    this.bestTime[i][j] = this.context.getSharedPreferences(PREFS, 0).getInt("bestTime" + i + j, 0);
                    this.avgTime[i][j] = this.context.getSharedPreferences(PREFS, 0).getFloat("avgTime" + i + j, 0.0f);
                    if (this.timesPlayed[i][j] != 0) {
                        storedInfoExists = true;
                    }
                }
            }
            try {
                String ourVersion = this.context.getPackageManager().getPackageInfo(this.context.getPackageName(), 0).versionName;
                String savedVersion = this.context.getSharedPreferences(PREFS, 0).getString("version", "");
                if (storedInfoExists && savedVersion.equals("")) {
                    this.activity.showDialog(3);
                }
                SharedPreferences.Editor ed = this.context.getSharedPreferences(PREFS, 0).edit();
                ed.putString("version", ourVersion);
                ed.commit();
                this.version = ourVersion;
                this.activity.setAboutText(this.version);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void loadFromFiles() {
            this.anagram = new Anagram[10];
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(this.activity.openFileInput("AMsaveMaster")));
                this.timePenalty = Integer.valueOf(br.readLine()).intValue();
                setTimer(Integer.valueOf(br.readLine()).intValue());
                this.curAnagram = Integer.valueOf(br.readLine()).intValue();
                br.close();
                for (int i = 0; i < this.anagram.length; i++) {
                    BufferedReader br2 = new BufferedReader(new InputStreamReader(this.activity.openFileInput("AMsave" + i)));
                    this.anagram[i] = Anagram.loadFromFile(br2, this);
                    br2.close();
                }
            } catch (Exception e) {
                this.anagram = null;
                e.printStackTrace();
            }
        }

        private void saveToFiles() {
            for (int i = 0; i < this.anagram.length; i++) {
                saveToFile(i);
            }
            saveToFileMain(false);
        }

        private void saveToFileMain(boolean wipe) {
            try {
                FileOutputStream fos = this.activity.openFileOutput("AMsaveMaster", 0);
                if (!wipe) {
                    fos.write(String.valueOf(this.timePenalty).getBytes());
                    fos.write(Anagram.newLineBytes);
                    fos.write(String.valueOf(this.timer).getBytes());
                    fos.write(Anagram.newLineBytes);
                    fos.write(String.valueOf(this.curAnagram).getBytes());
                    fos.write(Anagram.newLineBytes);
                }
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private void saveToFile(int i) {
            try {
                FileOutputStream fos = this.activity.openFileOutput("AMsave" + i, 0);
                this.anagram[i].saveToFile(fos);
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void setGameInfoText() {
            if (this.timesPlayed[this.wordLen][this.gameType] > 0) {
                this.newGameInfoText[0] = String.format("Best Time %s", setTimeString(this.bestTime[this.wordLen][this.gameType]));
                this.newGameInfoText[1] = String.format("Average Time %s", setTimeString((int) this.avgTime[this.wordLen][this.gameType]));
                this.newGameInfoText[2] = String.format("Success Rate %d%% (%d/%d)", Integer.valueOf(Math.round((((float) this.timesCompleted[this.wordLen][this.gameType]) / ((float) this.timesPlayed[this.wordLen][this.gameType])) * 100.0f)), Integer.valueOf(this.timesCompleted[this.wordLen][this.gameType]), Integer.valueOf(this.timesPlayed[this.wordLen][this.gameType]));
                return;
            }
            this.newGameInfoText[0] = "";
            this.newGameInfoText[1] = "";
            this.newGameInfoText[2] = "";
        }

        public void wordLenOptionSelected(int tile) {
            this.wordLen = tile;
            SharedPreferences.Editor ed = this.context.getSharedPreferences(PREFS, 0).edit();
            ed.putInt("wordLenSpin", tile);
            ed.commit();
            setGameInfoText();
        }

        public void gameTypeOptionSelected(int tile) {
            this.gameType = tile;
            SharedPreferences.Editor ed = this.context.getSharedPreferences(PREFS, 0).edit();
            ed.putInt("gameTypeSpin", tile);
            ed.commit();
            setGameInfoText();
        }

        private void initPosCol(int screenWidth, int screenHeight) {
            boolean z;
            this.canvasWidth = screenWidth;
            this.canvasHeight = screenHeight;
            this.greenPaint.setColor(-16711936);
            this.titlePaint.setColor(-1);
            this.titlePaint.setTextSize(24.0f);
            this.tpPaint.setTextSize(24.0f);
            Anagram.setTileSize((float) this.canvasWidth, (float) this.canvasHeight);
            loadFromFiles();
            int halfW = screenWidth / 2;
            float textSize = Anagram.bigFont.getTextSize() / 2.0f;
            this.smallFont.setTextSize(textSize);
            AMButton.textPaint.setTextSize((3.0f * textSize) / 4.0f);
            int posY = screenHeight / 2;
            this.startBut = new AMButton("Start", true) {
                /* access modifiers changed from: package-private */
                public void buttonPush() {
                    if (AMThread.this.anagram == null) {
                        AMThread.this.setState(2);
                    } else {
                        AMThread.this.activity.showDialog(1);
                    }
                }
            };
            if (this.anagram != null) {
                z = true;
            } else {
                z = false;
            }
            this.resumeBut = new AMButton("Resume", z) {
                /* access modifiers changed from: package-private */
                public void buttonPush() {
                    AMThread.this.lastTime = System.currentTimeMillis();
                    AMThread.this.startTime = AMThread.this.lastTime - ((long) (AMThread.this.timer * 1000));
                    AMThread.this.anaNoText = String.valueOf(AMThread.this.curAnagram + 1) + "/10";
                    AMThread.this.setState(4);
                }
            };
            this.aboutBut = new AMButton("About", true) {
                /* access modifiers changed from: package-private */
                public void buttonPush() {
                    AMThread.this.activity.showDialog(0);
                }
            };
            this.startBut.setTextBounds();
            this.startBut.setPositionXCentred(halfW, posY);
            int posY2 = (int) (((float) posY) + this.startBut.bounds.height() + 20.0f);
            this.resumeBut.setTextBounds();
            this.resumeBut.setPositionXCentred(halfW, posY2);
            int posY3 = (int) (((float) posY2) + this.resumeBut.bounds.height() + 20.0f);
            this.aboutBut.setTextBounds();
            this.aboutBut.setPositionXCentred(halfW, posY3);
            this.startBut.bounds.left = this.resumeBut.bounds.left;
            this.startBut.bounds.right = this.resumeBut.bounds.right;
            this.aboutBut.bounds.left = this.resumeBut.bounds.left;
            this.aboutBut.bounds.right = this.resumeBut.bounds.right;
            this.titlePaint.getTextBounds(this._newGameStr, 0, this._newGameStr.length(), this._newGameBounds);
            AMButton.xCentreRectangle(this._newGameBounds, (float) halfW, this.canvasHeight / 5);
            setGameInfoText();
            this.startGameBut = new AMButton("Start", true) {
                /* access modifiers changed from: package-private */
                public void buttonPush() {
                    AMThread.this.createNewGame();
                    AMThread.this.setState(4);
                }
            };
            this.startGameBut.setTextBounds();
            this.startGameBut.setPositionXCentred(halfW, ((this.canvasHeight / 5) * 4) + 30);
            this.startGameBut.bounds.left = this.resumeBut.bounds.left;
            this.startGameBut.bounds.right = this.resumeBut.bounds.right;
            setState(1);
        }
    }

    public AMView(Context context, AttributeSet attrs) {
        super(context, attrs);
        getHolder().addCallback(this);
        createThread();
        this.thread.start();
        requestFocus();
        setFocusableInTouchMode(true);
        setFocusable(true);
    }

    public void createThread() {
        this.thread = new AMThread(getHolder(), getContext());
    }

    public void onWindowFocusChanged(boolean hasWindowFocus) {
        if (!hasWindowFocus) {
            this.thread.pause();
        }
    }

    public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
    }

    public void surfaceCreated(SurfaceHolder holder) {
        this.thread.surfaceCreated = true;
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        this.thread.surfaceCreated = false;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4 && event.getRepeatCount() == 0) {
            return this.thread.backButton();
        }
        if (keyCode == 82 && event.getRepeatCount() == 0) {
            return this.thread.menuButton();
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        return super.onKeyUp(keyCode, event);
    }

    public boolean onTouchEvent(MotionEvent me) {
        this.thread.touchEvent(me);
        try {
            Thread.sleep(16);
            return true;
        } catch (InterruptedException e) {
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public void onFocusChanged(boolean gainFocus, int direction, Rect prev) {
    }
}
