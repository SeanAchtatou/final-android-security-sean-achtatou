package com.waterbear.am;

import android.content.res.Resources;
import com.waterbear.am.AMView;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Dict {
    int _loadCounter = 0;
    AMView.AMThread amViewThread;
    AnagraWa[] anagrams;
    int id;
    boolean loaded = false;
    Resources res;
    int wordLen;

    public Dict(Resources res2, int id2, AMView.AMThread amViewThread2, int wordLen2) {
        this.res = res2;
        this.id = id2;
        this.amViewThread = amViewThread2;
        this.wordLen = wordLen2;
    }

    public AnagraWa randomWord() {
        return this.anagrams[(int) (Math.random() * ((double) this.anagrams.length))];
    }

    public Anagram[] generateAnagrams(int howMany) {
        Anagram[] anagrams2 = null;
        BufferedReader br = new BufferedReader(new InputStreamReader(this.res.openRawResource(this.id)));
        try {
            int size = Integer.parseInt(br.readLine());
            br.mark(102400);
            anagrams2 = new Anagram[howMany];
            for (int i = 0; i < anagrams2.length; i++) {
                AnagraWa aw = findAWord(br, this.wordLen * size);
                while (true) {
                    if (aw != null && aw.words.size() != 0) {
                        break;
                    }
                    br.reset();
                    aw = findAWord(br, this.wordLen * size);
                }
                anagrams2[i] = createAnagram(aw);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return anagrams2;
    }

    private Anagram createAnagram(AnagraWa aw) {
        Object[] arr = aw.words.toArray();
        String[] wordArr = new String[arr.length];
        for (int i = 0; i < arr.length; i++) {
            wordArr[i] = (String) arr[i];
        }
        return new Anagram(wordArr, this.amViewThread);
    }

    private AnagraWa findAWord(BufferedReader br, int maxSkip) throws IOException {
        int skip = (int) br.skip((long) ((int) (Math.random() * ((double) maxSkip))));
        String str = br.readLine();
        if (str != null) {
            do {
                str = br.readLine();
                if (str == null) {
                    break;
                }
            } while (str.length() != 0);
        }
        if (str == null) {
            return null;
        }
        AnagraWa answer = new AnagraWa();
        String str2 = br.readLine();
        while (str2 != null && str2.length() != 0) {
            answer.addWord(str2);
            str2 = br.readLine();
        }
        return answer;
    }

    private void parseLine(String str) {
        if (!this.loaded) {
            this.anagrams = new AnagraWa[Integer.parseInt(str)];
            this.anagrams[this._loadCounter] = new AnagraWa();
            this.loaded = true;
        } else if (str.length() == 0) {
            this._loadCounter++;
            if (this._loadCounter < this.anagrams.length) {
                this.anagrams[this._loadCounter] = new AnagraWa();
            }
        } else {
            this.anagrams[this._loadCounter].addWord(str);
        }
    }
}
