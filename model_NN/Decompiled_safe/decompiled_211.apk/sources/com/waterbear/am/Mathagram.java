package com.waterbear.am;

import android.os.Bundle;
import com.waterbear.am.AMView;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;

public class Mathagram extends Anagram {
    int[] var = new int[10];

    public Mathagram(String text, AMView.AMThread amViewThread, Bundle map) {
        super(text, amViewThread, map);
    }

    public Mathagram(String text, AMView.AMThread amViewThread, BufferedReader br) throws IOException {
        super(text, amViewThread, br);
    }

    public Mathagram(String text, AMView.AMThread amViewThread) {
        super(text, amViewThread);
        while (checkWordFound()) {
            boolean finish = false;
            for (int i = 1; i < this.letter.length && !finish; i++) {
                if (this.letter[i] == '+') {
                    char swap = this.letter[0];
                    this.letter[0] = '+';
                    this.letter[i] = swap;
                    finish = true;
                } else {
                    int rando = (int) (Math.random() * ((double) i));
                    char swap2 = this.letter[rando];
                    this.letter[rando] = this.letter[i];
                    this.letter[i] = swap2;
                }
            }
        }
    }

    public Bundle saveState() {
        Bundle map = new Bundle();
        map.putBoolean("mMathematical", true);
        return saveCommonState(map);
    }

    public boolean equals(Mathagram x) {
        if (x.text.length() != this.text.length()) {
            return false;
        }
        char[] newLetter = (char[]) x.letter.clone();
        for (int i = 0; i < this.letter.length; i++) {
            boolean letterFound = false;
            for (int j = 0; j < newLetter.length && !letterFound; j++) {
                if (this.letter[i] == newLetter[j]) {
                    newLetter[j] = 0;
                    letterFound = true;
                }
            }
            if (!letterFound) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void saveToFile(FileOutputStream fos) throws IOException {
        fos.write("Math".getBytes());
        fos.write(newLineBytes);
        saveToFileCommon(fos);
    }

    /* access modifiers changed from: protected */
    public boolean checkWordFound() {
        if (this.selectedLetters[0] == '+' || this.selectedLetters[0] == '=') {
            return false;
        }
        if (this.selectedLetters[this.selectedLetters.length - 1] == '+' || this.selectedLetters[this.selectedLetters.length - 1] == '=') {
            return false;
        }
        int equals = -1;
        int add = -1;
        int curVar = 0;
        for (int i = 0; i < this.var.length; i++) {
            this.var[i] = 0;
        }
        this.var[0] = this.selectedLetters[0] - '0';
        for (int i2 = 1; i2 < this.selectedLetters.length; i2++) {
            if (this.selectedLetters[i2] == '+') {
                if (this.selectedLetters[i2 + 1] == '=') {
                    return false;
                }
                add = curVar;
                curVar++;
            } else if (this.selectedLetters[i2] != '=') {
                int[] iArr = this.var;
                iArr[curVar] = iArr[curVar] * 10;
                int[] iArr2 = this.var;
                iArr2[curVar] = iArr2[curVar] + (this.selectedLetters[i2] - '0');
            } else if (this.selectedLetters[i2 + 1] == '+') {
                return false;
            } else {
                equals = curVar;
                curVar++;
            }
        }
        if (add >= equals) {
            return this.var[0] == this.var[1] + this.var[2];
        }
        if (this.var[0] + this.var[1] == this.var[2]) {
            return true;
        }
        return false;
    }
}
