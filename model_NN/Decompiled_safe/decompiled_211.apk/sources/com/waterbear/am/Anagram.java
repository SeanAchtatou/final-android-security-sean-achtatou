package com.waterbear.am;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Bundle;
import com.waterbear.am.AMView;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;

public class Anagram extends AMButton {
    static Paint bigFont = new Paint();
    static byte[] newLineBytes = "\n".getBytes();
    static RectF tileSize = new RectF();
    String[] alternatives;
    AMView.AMThread amViewThread;
    Paint bluePaint;
    boolean complete;
    int hint;
    char[] letter;
    int letterCounter;
    int letterSelected;
    boolean[] letterUsed;
    int miniTileWidth;
    int scrHeight;
    int scrWidth;
    char[] selectedLetters;
    Paint smallFont;
    RectF tempBounds;
    Rect textTempBounds;

    public static void setTileSize(float scrWidth2, float scrHeight2) {
        if (scrWidth2 < scrHeight2) {
            tileSize.right = scrWidth2 / 5.0f;
        } else {
            tileSize.right = scrHeight2 / 5.0f;
        }
        tileSize.bottom = tileSize.right;
        int textSize = 18;
        while (((double) bigFont.measureText("W")) < ((double) tileSize.right) * 0.75d) {
            bigFont.setTextSize((float) textSize);
            textSize++;
        }
        bigFont.setAntiAlias(true);
    }

    public Anagram(String text, AMView.AMThread amViewThread2) {
        this(text, (String[]) null, amViewThread2);
    }

    public Anagram(String[] words, AMView.AMThread amViewThread2) {
        this(words[0], words, amViewThread2);
    }

    public Bundle saveState() {
        Bundle map = new Bundle();
        map.putBoolean("mMathematical", false);
        return saveCommonState(map);
    }

    /* access modifiers changed from: protected */
    public void saveToFile(FileOutputStream fos) throws IOException {
        fos.write("Alph".getBytes());
        fos.write(newLineBytes);
        saveToFileCommon(fos);
    }

    /* access modifiers changed from: protected */
    public void saveToFileCommon(FileOutputStream fos) throws IOException {
        int i;
        int i2;
        fos.write(this.text.getBytes());
        fos.write(newLineBytes);
        for (char write : this.letter) {
            fos.write(write);
        }
        fos.write(newLineBytes);
        if (this.alternatives != null) {
            for (String bytes : this.alternatives) {
                fos.write(bytes.getBytes());
                fos.write(58);
            }
        }
        fos.write(newLineBytes);
        for (char write2 : this.selectedLetters) {
            fos.write(write2);
        }
        fos.write(newLineBytes);
        for (boolean z : this.letterUsed) {
            if (z) {
                i2 = 1;
            } else {
                i2 = 0;
            }
            fos.write(i2);
        }
        fos.write(newLineBytes);
        fos.write(String.valueOf(this.letterCounter).getBytes());
        fos.write(newLineBytes);
        fos.write(String.valueOf(this.hint).getBytes());
        fos.write(newLineBytes);
        if (this.complete) {
            i = 1;
        } else {
            i = 0;
        }
        fos.write(i);
        fos.write(newLineBytes);
    }

    public Anagram(String text, AMView.AMThread amViewThread2, BufferedReader br) throws IOException {
        super(text, true);
        boolean z;
        boolean z2;
        this.letterSelected = -1;
        this.letterCounter = 0;
        this.hint = -1;
        this.complete = false;
        this.bluePaint = new Paint();
        this.smallFont = new Paint();
        this.amViewThread = null;
        this.tempBounds = new RectF();
        this.textTempBounds = new Rect();
        this.letter = br.readLine().toCharArray();
        this.alternatives = br.readLine().split(":");
        this.selectedLetters = br.readLine().toCharArray();
        String bools = br.readLine();
        if (bools != null) {
            this.letterUsed = new boolean[bools.length()];
            for (int i = 0; i < bools.length(); i++) {
                boolean[] zArr = this.letterUsed;
                if (bools.charAt(i) == 1) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                zArr[i] = z2;
            }
        }
        this.letterCounter = Integer.parseInt(br.readLine());
        this.hint = Integer.parseInt(br.readLine());
        if (br.readLine().charAt(0) == 1) {
            z = true;
        } else {
            z = false;
        }
        this.complete = z;
        initialiseBounds(amViewThread2);
    }

    public static Anagram loadFromFile(BufferedReader br, AMView.AMThread vt) throws IOException {
        String str = br.readLine();
        if (str == null) {
            return null;
        }
        if (str.equals("Math")) {
            return new Mathagram(br.readLine(), vt, br);
        }
        return new Anagram(br.readLine(), vt, br);
    }

    /* access modifiers changed from: protected */
    public Bundle saveCommonState(Bundle map) {
        map.putString("mText", this.text);
        map.putCharArray("mSelectedLetters", this.selectedLetters);
        map.putBooleanArray("mLetterUsed", this.letterUsed);
        map.putStringArray("mAlternatives", this.alternatives);
        map.putInt("mLetterCounter", this.letterCounter);
        map.putInt("mHint", this.hint);
        map.putBoolean("mComplete", this.complete);
        return map;
    }

    public Anagram(String text, AMView.AMThread amViewThread2, Bundle map) {
        super(text, true);
        this.letterSelected = -1;
        this.letterCounter = 0;
        this.hint = -1;
        this.complete = false;
        this.bluePaint = new Paint();
        this.smallFont = new Paint();
        this.amViewThread = null;
        this.tempBounds = new RectF();
        this.textTempBounds = new Rect();
        this.letter = text.toCharArray();
        this.selectedLetters = map.getCharArray("mSelectedLetters");
        this.letterUsed = map.getBooleanArray("mLetterUsed");
        this.alternatives = map.getStringArray("mAlternatives");
        this.letterCounter = map.getInt("mLetterCounter");
        this.hint = map.getInt("mHint");
        this.complete = map.getBoolean("mComplete");
        initialiseBounds(amViewThread2);
    }

    public static Anagram loadAnagram(Bundle map, AMView.AMThread vt) {
        if (map.getBoolean("mMathematical")) {
            return new Mathagram(map.getString("mText"), vt, map);
        }
        return new Anagram(map.getString("mText"), vt, map);
    }

    public boolean equals(Anagram x) {
        return x.text.equals(this.text);
    }

    public Anagram(String text, String[] alternates, AMView.AMThread amViewThread2) {
        super(text, true);
        this.letterSelected = -1;
        this.letterCounter = 0;
        this.hint = -1;
        this.complete = false;
        this.bluePaint = new Paint();
        this.smallFont = new Paint();
        this.amViewThread = null;
        this.tempBounds = new RectF();
        this.textTempBounds = new Rect();
        this.alternatives = alternates;
        this.letter = new char[text.length()];
        this.selectedLetters = new char[text.length()];
        this.letterUsed = new boolean[text.length()];
        this.letter = text.toCharArray();
        scramble();
        initialiseBounds(amViewThread2);
    }

    private void initialiseBounds(AMView.AMThread amViewThread2) {
        this.scrWidth = amViewThread2.canvasWidth;
        this.scrHeight = amViewThread2.canvasHeight;
        this.amViewThread = amViewThread2;
        this.bluePaint.setColor(-16711681);
        this.bluePaint.setAntiAlias(true);
        textPaint.getTextBounds("W", 0, 1, this.textBounds);
        this.bounds.set(tileSize);
        this.bounds.right = (this.bounds.right - this.bounds.left) + 20.0f;
        this.bounds.bottom = (this.bounds.bottom - this.bounds.top) + 20.0f;
        this.bounds.left = 0.0f;
        this.bounds.top = 0.0f;
        if (this.bounds.right > this.bounds.bottom) {
            this.bounds.bottom = this.bounds.right;
        } else {
            this.bounds.right = this.bounds.bottom;
        }
        if (this.letter.length > 9) {
            this.miniTileWidth = this.scrWidth / this.letter.length;
            float textSize = 1.0f;
            while (this.smallFont.measureText("W") < ((float) this.miniTileWidth)) {
                this.smallFont.setTextSize(textSize);
                textSize += 1.0f;
            }
        } else {
            this.miniTileWidth = (int) (tileSize.right / 2.0f);
            this.smallFont.setTextSize(bigFont.getTextSize() / 2.0f);
        }
        this.smallFont.setAntiAlias(true);
    }

    /* access modifiers changed from: package-private */
    public void scramble() {
        int rando;
        LinkedList<Character> ordering = new LinkedList<>();
        for (char valueOf : this.letter) {
            ordering.add(Character.valueOf(valueOf));
        }
        boolean fly = false;
        for (int i = 0; i < this.letter.length; i++) {
            while (true) {
                rando = (int) (Math.random() * ((double) ordering.size()));
                if (i == 0 && this.alternatives != null) {
                    fly = false;
                    for (int j = 0; j < this.alternatives.length && !fly; j++) {
                        if (this.letter[rando] == this.alternatives[j].charAt(i)) {
                            fly = true;
                        } else {
                            fly = false;
                        }
                    }
                }
                if (i != 0 || (this.letter[rando] != this.text.charAt(0) && !fly)) {
                    this.letter[i] = ((Character) ordering.remove(rando)).charValue();
                    this.letterUsed[i] = this.letterUsed[rando];
                }
            }
            this.letter[i] = ((Character) ordering.remove(rando)).charValue();
            this.letterUsed[i] = this.letterUsed[rando];
        }
    }

    /* access modifiers changed from: package-private */
    public void buttonPush() {
    }

    public void paint(Canvas c) {
        int tilesPerLine = (int) (((float) this.scrWidth) / tileSize.right);
        this.tempBounds.set(this.textBounds);
        for (int i = 0; i < this.selectedLetters.length; i++) {
            this.tempBounds.left = this.position.x + 6.0f + ((float) (this.miniTileWidth * i));
            this.tempBounds.top = this.position.y + (tileSize.bottom / 3.0f);
            this.tempBounds.right = (this.tempBounds.left + ((float) this.miniTileWidth)) - 2.0f;
            this.tempBounds.bottom = this.tempBounds.top + ((float) this.miniTileWidth);
            c.drawRoundRect(this.tempBounds, 5.0f, 5.0f, shadow);
            this.tempBounds.left -= 4.0f;
            this.tempBounds.top -= 4.0f;
            this.tempBounds.right -= 4.0f;
            this.tempBounds.bottom -= 4.0f;
            if (i <= this.hint || i == this.letterCounter) {
                c.drawRoundRect(this.tempBounds, 5.0f, 5.0f, this.bluePaint);
            } else {
                c.drawRoundRect(this.tempBounds, 5.0f, 5.0f, buttonPaint);
            }
            this.smallFont.getTextBounds(this.selectedLetters, i, 1, this.textTempBounds);
            c.drawText(this.selectedLetters, i, 1, this.tempBounds.centerX() - ((float) this.textTempBounds.centerX()), ((float) (this.textTempBounds.bottom / 4)) + (this.tempBounds.centerY() - ((float) this.textTempBounds.centerY())), this.smallFont);
        }
        for (int i2 = 0; i2 < this.letter.length; i2++) {
            if (!this.letterUsed[i2]) {
                this.tempBounds.left = this.position.x + 10.0f + (tileSize.right * ((float) (i2 % tilesPerLine)));
                this.tempBounds.top = this.position.y + 30.0f + (tileSize.bottom * ((float) ((i2 / tilesPerLine) + 1)));
                this.tempBounds.right = (this.tempBounds.left + tileSize.right) - 10.0f;
                this.tempBounds.bottom = (this.tempBounds.top + tileSize.bottom) - 7.0f;
                c.drawRoundRect(this.tempBounds, 10.0f, 10.0f, shadow);
                this.tempBounds.left -= 8.0f;
                this.tempBounds.top -= 8.0f;
                this.tempBounds.right -= 8.0f;
                this.tempBounds.bottom -= 8.0f;
                if (i2 == this.letterSelected) {
                    c.drawRoundRect(this.tempBounds, 10.0f, 10.0f, buttonSelectPaint);
                } else {
                    c.drawRoundRect(this.tempBounds, 10.0f, 10.0f, buttonPaint);
                }
                bigFont.getTextBounds(this.letter, i2, 1, this.textTempBounds);
                c.drawText(this.letter, i2, 1, this.tempBounds.centerX() - ((float) this.textTempBounds.centerX()), ((float) (this.textTempBounds.bottom / 4)) + (this.tempBounds.centerY() - ((float) this.textTempBounds.centerY())), bigFont);
            }
        }
    }

    public boolean touch(int action, float x, float y) {
        float x2 = x - this.position.x;
        float y2 = y - this.position.y;
        if (((double) y2) < ((double) tileSize.bottom) * 0.8d && action == 0) {
            int temp = (int) (x2 / ((float) this.miniTileWidth));
            if (temp < this.selectedLetters.length && temp > this.hint) {
                this.letterCounter = temp;
                deleteLetter(this.letterCounter);
            }
            return true;
        } else if (y2 - 20.0f < tileSize.bottom) {
            return true;
        } else {
            int tile = ((int) (x2 / tileSize.right)) + (((int) (((y2 - 20.0f) / tileSize.bottom) - 1.0f)) * ((int) (((float) this.scrWidth) / tileSize.right)));
            if (tile >= this.letter.length || tile < 0 || this.letterUsed[tile]) {
                this.letterSelected = -1;
                return false;
            }
            if (action == 0) {
                this.letterSelected = tile;
                this.selectedLetters[this.letterCounter] = this.letter[this.letterSelected];
                this.letterUsed[this.letterSelected] = true;
                this.letterCounter = findNextBlank(this.selectedLetters, this.letterCounter);
                if (this.letterCounter == -1) {
                    boolean checkWordFound = checkWordFound();
                    this.complete = checkWordFound;
                    if (checkWordFound) {
                        anagramCompleted();
                    } else {
                        clearLetters();
                    }
                }
                this.letterSelected = -1;
            } else if (action == 2 && this.letterSelected != tile) {
                this.letterSelected = -1;
            }
            return true;
        }
    }

    private int findNextBlank(char[] charArr, int from) {
        for (int i = 0; i < charArr.length; i++) {
            int temp = (i + from) % charArr.length;
            if (charArr[temp] == 0) {
                return temp;
            }
        }
        return -1;
    }

    public void anagramCompleted() {
        this.complete = true;
        this.hint = this.letter.length;
        this.letterSelected = this.letter.length;
        this.amViewThread.anagramComplete();
    }

    private void clearLetters() {
        for (int i = this.hint + 1; i < this.letter.length; i++) {
            int l = findUsedLetterTile(this.selectedLetters[i]);
            if (l != this.letter.length) {
                this.letterUsed[l] = false;
                this.selectedLetters[i] = 0;
            }
        }
        this.letterCounter = this.hint + 1;
    }

    private int findUsedLetterTile(char ch) {
        int i = 0;
        while (i < this.letter.length && (this.letter[i] != ch || !this.letterUsed[i])) {
            i++;
        }
        return i;
    }

    private int findUnusedLetterTile(char ch) {
        int i = 0;
        while (i < this.letter.length && (this.letter[i] != ch || this.letterUsed[i])) {
            i++;
        }
        return i;
    }

    public void backspace() {
        if (this.letterCounter > this.hint + 1) {
            this.letterCounter--;
            deleteLetter(this.letterCounter);
        } else {
            deleteLetter(this.letterCounter + 1);
        }
        for (int i = this.letterCounter + 1; i < this.selectedLetters.length - 1; i++) {
            this.selectedLetters[i] = this.selectedLetters[i + 1];
        }
        this.selectedLetters[this.selectedLetters.length - 1] = 0;
    }

    private void deleteLetter(int cursor) {
        char ch = this.selectedLetters[cursor];
        this.selectedLetters[cursor] = 0;
        if (ch != 0) {
            this.letterUsed[findUsedLetterTile(ch)] = false;
        }
    }

    public void hint() {
        clearLetters();
        if (this.hint + 1 < this.text.length()) {
            this.hint++;
            this.letterCounter = this.hint + 1;
            this.selectedLetters[this.hint] = this.text.charAt(this.hint);
            this.letterUsed[findUnusedLetterTile(this.selectedLetters[this.hint])] = true;
            if (this.hint == this.text.length() - 1) {
                anagramCompleted();
            }
            this.amViewThread.timePenalty(this.letter.length * 60);
        }
    }

    /* access modifiers changed from: protected */
    public boolean checkWordFound() {
        boolean wordFound = true;
        for (int i = 0; i < this.letter.length && wordFound; i++) {
            if (this.selectedLetters[i] != this.text.charAt(i)) {
                wordFound = false;
            }
        }
        if (!wordFound && this.alternatives != null) {
            for (int j = 0; j < this.alternatives.length && !wordFound; j++) {
                wordFound = true;
                for (int i2 = 0; i2 < this.letter.length && wordFound; i2++) {
                    if (this.selectedLetters[i2] != this.alternatives[j].charAt(i2)) {
                        wordFound = false;
                    }
                }
            }
        }
        return wordFound;
    }

    public void setPosition(float x, float y) {
        this.position.x = x;
        this.position.y = y;
    }
}
