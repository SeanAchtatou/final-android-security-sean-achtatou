package com.image.clmitones;

public class Sample {
    private int resId;
    private String text;

    public Sample(String text2, int resId2) {
        this.text = text2;
        this.resId = resId2;
    }

    public String getName() {
        return this.text;
    }

    public int getResId() {
        return this.resId;
    }
}
