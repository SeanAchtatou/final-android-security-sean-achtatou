package com.image.clmitones;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

public class StarterActivity extends Activity {
    /* access modifiers changed from: private */
    public static int MSG_PROGRESS = 1;
    private final Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (msg.what == StarterActivity.MSG_PROGRESS) {
                StarterActivity.this.startActivity(new Intent(StarterActivity.this, ImageSoundboard.class));
                StarterActivity.this.finish();
            }
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.splash);
        this.mHandler.sendEmptyMessageDelayed(MSG_PROGRESS, 2000);
    }
}
