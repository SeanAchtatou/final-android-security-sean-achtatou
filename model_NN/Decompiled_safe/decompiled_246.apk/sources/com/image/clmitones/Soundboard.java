package com.image.clmitones;

import java.util.ArrayList;
import java.util.List;

public abstract class Soundboard {
    private String name;
    private List<Sample> samples = new ArrayList();

    public Soundboard(String name2) {
        this.name = name2;
    }

    /* access modifiers changed from: protected */
    public Sample addSample(String text, int resId) {
        Sample sample = new Sample(text, resId);
        this.samples.add(sample);
        return sample;
    }

    public String getName() {
        return this.name;
    }

    public List<Sample> getSamples() {
        return this.samples;
    }
}
