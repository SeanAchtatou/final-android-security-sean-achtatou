package wall.bt.wp.P092;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.xl.util.Constant;
import com.xl.util.FileUtil;
import com.xl.util.Folder;
import com.xl.util.GenUtil;
import com.xl.util.MultiDownloadNew;
import java.io.File;
import java.net.URL;
import java.util.Date;

public class WebMoreImg extends Activity {
    private static final int DIALOG1 = 1;
    private static final int DIALOG2 = 2;
    private static final int DIALOG3 = 3;
    private static final int DIALOG5 = 5;
    private static final int DIALOG6 = 6;
    private static final String PATH = "/sdcard/download/";
    private static boolean contDownload = false;
    /* access modifiers changed from: private */
    public static boolean stopDownload = false;
    private File apk;
    private String bookFileName = "";
    /* access modifiers changed from: private */
    public String bookFolder = "";
    final Activity context = this;
    public URL gUrl;
    public final String gotourl = "http://store.apkshare.com/item_details.php?softid=3279&p=1&c=001002";
    Handler handler = new ApkWebViewtemp();
    ImageView mGoBack;
    private WebView mWebView;
    private MultiDownloadNew multiDownload;
    public ProgressDialog pd;
    private AlertDialog.Builder progressBuilder;
    private String strBookMark = "";
    private String strCurrDestFile = "";
    private String strURLFold = "";
    private String strUrl = "";
    private downTask task = null;
    private Thread tdDownload = null;
    LinearLayout webparent;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        getWindow().setFlags(1024, 1024);
        setRequestedOrientation(1);
        requestWindowFeature(DIALOG2);
        setContentView((int) R.layout.webmoreimg);
        this.mWebView = (WebView) findViewById(R.id.web);
        WebSettings localWebSettings = this.mWebView.getSettings();
        localWebSettings.setJavaScriptEnabled(true);
        localWebSettings.setSavePassword(false);
        localWebSettings.setSaveFormData(false);
        localWebSettings.setSupportZoom(false);
        this.mWebView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                WebMoreImg.this.context.setProgress(progress * 100);
            }
        });
        this.mWebView.setWebChromeClient(new MyWebChromeClient());
        this.mWebView.setWebViewClient(new MyWebClient());
        this.mWebView.addJavascriptInterface(new DemoJavaScriptInterface(), "zhuazhua");
        String strDownURL = getResources().getString(R.string.webimgrooturlHtml);
        this.mWebView.loadUrl(strDownURL);
        Log.v("startSocketMonitor", strDownURL);
    }

    public void setInfo(String msgTitle, String msgString) {
        AlertDialog.Builder myDialogBuilder = new AlertDialog.Builder(this);
        myDialogBuilder.setTitle(msgTitle);
        if (!msgString.equalsIgnoreCase("")) {
            myDialogBuilder.setMessage(msgString);
        }
        myDialogBuilder.create();
        myDialogBuilder.setPositiveButton(getResources().getString(R.string.btn_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        myDialogBuilder.setNegativeButton(getResources().getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
    }

    public void showInfo(String msgTitle, String msgString) {
        this.progressBuilder = new AlertDialog.Builder(this);
        this.progressBuilder.setTitle(msgTitle);
        this.progressBuilder.setMessage(msgString);
        this.progressBuilder.setCancelable(true);
        this.progressBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                boolean unused = WebMoreImg.stopDownload = true;
            }
        });
        this.progressBuilder.create();
        this.progressBuilder.show();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int paramInt) {
        switch (paramInt) {
            case 1:
                return buildDialog1(this);
            default:
                return null;
        }
    }

    private Dialog buildDialog1(Context paramContext) {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(paramContext);
        localBuilder.setTitle(getResources().getString(R.string.txt_download_msg_error)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        return localBuilder.create();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        GenUtil.systemPrintln(" ==========> onKeyDown  called KeyEvent.KEYCODE_BACK ");
        stopProgress();
        stopDownload = true;
        GenUtil.systemPrintln("  ==========>  onKeyDown  called KeyEvent.KEYCODE_BACK stopDownload = " + stopDownload);
        if (this.mWebView.canGoBack()) {
            this.mWebView.goBack();
            return true;
        }
        quitSystem();
        return true;
    }

    public void quitSystem() {
        new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.txt_quit_title)).setMessage(getResources().getString(R.string.txt_quit_body)).setIcon((int) R.drawable.icon).setPositiveButton(getResources().getString(R.string.btn_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                WebMoreImg.this.finish();
            }
        }).setNegativeButton(getResources().getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).show();
    }

    final class DemoJavaScriptInterface {
        DemoJavaScriptInterface() {
        }

        public void clickOnAndroid(String paramString) {
            Log.v("startSocketMonitor", " clickOnAndroid called " + paramString);
            if (Environment.getExternalStorageState().equals("mounted")) {
                WebMoreImg.this.downLoadFile(paramString);
            } else {
                WebMoreImg.this.showDialog(WebMoreImg.DIALOG3);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void downLoadFile(String httpUrl) {
        setProgress("");
        String[] strTmpArray = httpUrl.split("\\|\\|\\|");
        if (strTmpArray.length == DIALOG2) {
            String[] arrParam = new String[DIALOG3];
            String strURLBase = strTmpArray[0].substring(0, strTmpArray[0].length() - 1);
            String strCatNameString = strURLBase.substring(strURLBase.lastIndexOf("/") + 1);
            String strURLBase2 = strURLBase.substring(0, strURLBase.lastIndexOf("/"));
            String strWebImgRootURL = getResources().getString(R.string.webimgrooturl);
            String strRelativePath = strURLBase2.substring(strWebImgRootURL.length());
            GenUtil.systemPrintln("strURLBase: " + strURLBase2);
            GenUtil.systemPrintln("strWebImgRootURL: " + strWebImgRootURL);
            GenUtil.systemPrintln("strRelativePath: " + strRelativePath);
            String strLocalImg = Folder.MYCACHE + strRelativePath + "/" + strTmpArray[1];
            this.strBookMark = strCatNameString + "|||" + strTmpArray[0] + "|||" + strLocalImg;
            this.strUrl = strTmpArray[0];
            arrParam[0] = strURLBase2 + "/" + strTmpArray[1];
            arrParam[1] = strLocalImg;
            arrParam[DIALOG2] = "1";
            GenUtil.systemPrintln("doInBackground0: " + arrParam[0]);
            GenUtil.systemPrintln("doInBackground1: " + arrParam[1]);
            GenUtil.systemPrintln("doInBackground2: " + arrParam[DIALOG2]);
            GenUtil.systemPrintln("strBookMark: " + this.strBookMark);
            if (FileUtil.fileExist(strLocalImg)) {
                startGridActivity();
                stopProgress();
                return;
            }
            GenUtil.systemPrintln("task started");
            this.task = new downTask();
            this.task.execute(arrParam);
        }
    }

    final class MyWebChromeClient extends WebChromeClient {
        MyWebChromeClient() {
        }

        public boolean onJsAlert(WebView paramWebView, String paramString1, String paramString2, JsResult paramJsResult) {
            Log.v("startSocketMonitor", " onJsAlert called paramString1" + paramString1);
            Log.v("startSocketMonitor", " onJsAlert called paramString2" + paramString2);
            paramJsResult.confirm();
            return true;
        }

        public boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString) {
            paramWebView.loadUrl(paramString);
            return true;
        }
    }

    final class MyWebClient extends WebViewClient {
        MyWebClient() {
        }

        public boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString) {
            paramWebView.loadUrl(paramString);
            return true;
        }
    }

    class downTask extends AsyncTask {
        downTask() {
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            super.onCancelled();
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String strtxt) {
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(int iPercent) {
        }

        /* access modifiers changed from: protected */
        public Object doInBackground(Object[] arrParams) {
            GenUtil.systemPrintln("doInBackground: " + ((String) arrParams[0]));
            GenUtil.systemPrintln("doInBackground: " + ((String) arrParams[1]));
            GenUtil.systemPrintln("doInBackground: " + ((String) arrParams[WebMoreImg.DIALOG2]));
            String bookFolderTMP = (Environment.getExternalStorageState().equals("mounted") ? Folder.POPIMG : Folder.POPIMG1) + Constant.TMP;
            int startThread = Integer.parseInt((String) arrParams[WebMoreImg.DIALOG2]);
            GenUtil.systemPrintln("doInBackground: " + bookFolderTMP);
            final MultiDownloadNew multiDownload = new MultiDownloadNew(startThread, (String) arrParams[0], (String) arrParams[1], bookFolderTMP);
            multiDownload.start();
            new Thread(new Runnable() {
                public void run() {
                    long time = new Date().getTime();
                    long lastdata = 0;
                    GenUtil.systemPrintln("doInBackground: 3");
                    while (multiDownload.getPercntInt() < 100) {
                        GenUtil.systemPrintln("downloading..." + multiDownload.getPercntInt());
                        if (WebMoreImg.stopDownload) {
                        }
                        try {
                            Thread.sleep(200);
                            long currTime = new Date().getTime();
                            long currData = multiDownload.getFileDownloadTotal();
                            if (currData != lastdata) {
                                long lasttime = currTime;
                                lastdata = currData;
                            }
                            Message m = new Message();
                            m.obj = multiDownload.getPercntInt() + "";
                            m.what = WebMoreImg.DIALOG2;
                            WebMoreImg.this.handler.sendMessage(m);
                        } catch (InterruptedException e) {
                            Message m1 = new Message();
                            m1.what = -1;
                            WebMoreImg.this.handler.sendMessage(m1);
                        }
                    }
                    Message m2 = new Message();
                    System.out.println("multiDownload.getPercntInt() = " + multiDownload.getPercntInt());
                    System.out.println("stopDownload = " + WebMoreImg.stopDownload);
                    if (multiDownload.getPercntInt() == 100) {
                        m2.what = 1;
                    } else {
                        m2.what = WebMoreImg.DIALOG6;
                    }
                    WebMoreImg.this.handler.sendMessage(m2);
                }
            }).start();
            return true;
        }
    }

    public boolean onCreateOptionsMenu(Menu paramMenu) {
        getMenuInflater().inflate(R.menu.apk_menu, paramMenu);
        return super.onCreateOptionsMenu(paramMenu);
    }

    public boolean onOptionsItemSelected(MenuItem paramMenuItem) {
        super.onOptionsItemSelected(paramMenuItem);
        switch (paramMenuItem.getItemId()) {
            case R.id.refresh:
                this.mWebView.reload();
                return true;
            default:
                return true;
        }
    }

    /* access modifiers changed from: private */
    public void startGridActivity() {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putString("url", this.strUrl);
        bundle.putString("bookmark", this.strBookMark);
        intent.putExtras(bundle);
        intent.setClass(this, GridActivity.class);
        startActivity(intent);
    }

    class ApkWebViewtemp extends Handler {
        ApkWebViewtemp() {
        }

        public void handleMessage(Message paramMessage) {
            GenUtil.systemPrintln("handleMessage  paramMessage.what = " + paramMessage.what);
            switch (paramMessage.what) {
                case -1:
                    WebMoreImg.this.stopProgress();
                    WebMoreImg.this.showDialog(1);
                    return;
                case 0:
                case WebMoreImg.DIALOG3 /*3*/:
                case 4:
                default:
                    WebMoreImg.this.setProgress("0");
                    return;
                case 1:
                    WebMoreImg.this.startGridActivity();
                    WebMoreImg.this.stopProgress();
                    return;
                case WebMoreImg.DIALOG2 /*2*/:
                    WebMoreImg.this.setProgress((String) paramMessage.obj);
                    return;
                case WebMoreImg.DIALOG5 /*5*/:
                    WebMoreImg.this.stopProgress();
                    try {
                        FileUtil.delFolder(WebMoreImg.this.bookFolder);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    WebMoreImg.this.showDialog(WebMoreImg.DIALOG5);
                    return;
                case WebMoreImg.DIALOG6 /*6*/:
                    WebMoreImg.this.stopProgress();
                    try {
                        FileUtil.delFolder(WebMoreImg.this.bookFolder);
                        return;
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        return;
                    }
            }
        }
    }

    public void stopProgress() {
        if (this.pd != null) {
            this.pd.dismiss();
            this.pd = null;
        }
    }

    public void setProgress(String paramMessage) {
        System.out.println("----------> paramMessage = " + paramMessage);
        String msg = getResources().getString(R.string.txt_download_msg);
        int iPct = 0;
        if (!paramMessage.equals("")) {
            iPct = Integer.parseInt(paramMessage);
        }
        if (this.pd == null) {
            ProgressDialog localProgressDialog = new ProgressDialog(this);
            localProgressDialog.setProgressStyle(0);
            localProgressDialog.setMessage(msg);
            this.pd = localProgressDialog;
            this.pd.show();
        }
        this.pd.setProgress(iPct);
    }
}
