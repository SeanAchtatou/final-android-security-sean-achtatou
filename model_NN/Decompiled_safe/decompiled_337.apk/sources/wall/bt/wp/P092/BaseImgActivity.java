package wall.bt.wp.P092;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class BaseImgActivity extends Activity {
    static final String ALBUM_ID = "album_id";
    static final String ALBUM_NAME = "album_name";
    static final String ALBUM_NUM = "album_num";
    static final int DIALOG_LOADING = 1;
    static final int FALSE = 0;
    static final String IMAGE_POS = "image_pos";
    static final int MENU_ADD_FAVORITE = 9;
    static final int MENU_ITEM_REFRESH = 8;
    static final String PAGE_GOTO = "page_goto";
    static final String PAGE_INDEX = "page_index";
    static final String PAGE_TOTAL = "page_total";
    static final int RESULT_ALREADY_EXISTS = 3;
    static final int RQ_ADD_TO_FAVORITE = 1;
    static final int RQ_EXIT = 3;
    static final int RQ_GOTO = 2;
    static final int TRUE = 1;
    int iScreenHeight;
    int iScreenWidth;
    Toast mToast;

    /* access modifiers changed from: package-private */
    public void calScreen() {
        Display display = getWindowManager().getDefaultDisplay();
        this.iScreenWidth = display.getWidth();
        this.iScreenWidth = display.getHeight();
    }

    /* access modifiers changed from: package-private */
    public void fullScreen() {
        getWindow().addFlags(1024);
        requestWindowFeature(1);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int iparam1, int iparam2, Intent paramIntent) {
        if (iparam1 == 3 && iparam2 == -1) {
            System.exit(0);
        }
        super.onActivityResult(iparam1, iparam2, paramIntent);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        calScreen();
        this.mToast = new Toast(this);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int iParm) {
        if (iParm == 1) {
            ProgressDialog pd = new ProgressDialog(this);
            pd.setIndeterminate(false);
            pd.setCancelable(false);
            pd.setMessage("loading...");
        }
        return super.onCreateDialog(iParm);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: package-private */
    public void showToast(int iparam1, int iParam2) {
        if (this.mToast != null) {
            this.mToast.cancel();
            this.mToast.setDuration(0);
            this.mToast.setGravity(80, 0, 0);
            View localView = LayoutInflater.from(this).inflate((int) R.layout.toast_view, (ViewGroup) null);
            ((ImageView) localView.findViewById(R.id.toast_img)).setImageResource(iparam1);
            ((TextView) localView.findViewById(R.id.toast_txt)).setText(iParam2);
            this.mToast.setView(localView);
            this.mToast.show();
        }
    }
}
