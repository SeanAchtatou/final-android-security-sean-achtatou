package wall.bt.wp.P092;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.xl.util.FileUtil;
import com.xl.util.XMUtilityFoldonly;
import java.io.File;
import java.util.HashMap;

public class FileManagementSaveImage extends Activity {
    private ListView lvFile;
    /* access modifiers changed from: private */
    public String strDestImgPath;
    /* access modifiers changed from: private */
    public String strImgPath;
    /* access modifiers changed from: private */
    public TextView tvDir;
    /* access modifiers changed from: private */
    public XMUtilityFoldonly xmUtility = new XMUtilityFoldonly();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.file_list);
        this.strImgPath = getIntent().getStringExtra("imgpath");
        Log.v("startSocketMonitor", "______________ strImgPath=" + this.strImgPath);
        this.tvDir = (TextView) findViewById(R.id.tvPathText);
        this.tvDir.setText("/sdcard");
        this.lvFile = (ListView) findViewById(R.id.listview);
        this.lvFile.setAdapter((ListAdapter) new SimpleAdapter(this, this.xmUtility.getFileDir("/sdcard"), R.layout.list_row, new String[]{"icon", "fname"}, new int[]{R.id.img, R.id.filename}));
        this.lvFile.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                FileManagementSaveImage.this.getFoldList(FileManagementSaveImage.this.xmUtility.arr.get(arg2));
            }
        });
        this.lvFile.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView arg0, View arg1, int arg2, long arg3) {
                String strDir = (String) FileManagementSaveImage.this.xmUtility.arr.get(arg2).get("fdir");
                Log.v("startSocketMonitor", "______________ strDir=" + strDir);
                File file = new File(FileManagementSaveImage.this.strImgPath);
                Log.v("startSocketMonitor", "______________ file.getName()=" + file.getName());
                String unused = FileManagementSaveImage.this.strDestImgPath = strDir + "/" + file.getName();
                if (FileUtil.fileExist(FileManagementSaveImage.this.strDestImgPath)) {
                    FileManagementSaveImage.this.showInfoReplaceImage(FileManagementSaveImage.this.getResources().getString(R.string.txt_save_title), FileManagementSaveImage.this.getResources().getString(R.string.txt_save_body));
                    return true;
                }
                FileManagementSaveImage.this.SaveImage();
                return true;
            }
        });
        this.lvFile.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                HashMap hashMap = (HashMap) arg0.getSelectedItem();
                String strDir = (String) hashMap.get("fdir");
                boolean bFold = Boolean.parseBoolean(hashMap.get("bfold").toString());
                FileManagementSaveImage.this.tvDir.setText(strDir);
                if (!bFold) {
                    FileManagementSaveImage.this.onExit(strDir);
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    /* access modifiers changed from: private */
    public void SaveImage() {
        try {
            FileUtil.copyFile(this.strImgPath, this.strDestImgPath);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finish();
    }

    public void showInfoReplaceImage(String msgTitle, String msgString) {
        new AlertDialog.Builder(this).setTitle(msgTitle).setMessage(msgString).setPositiveButton(getResources().getString(R.string.btn_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                FileManagementSaveImage.this.SaveImage();
            }
        }).setNegativeButton(getResources().getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        }).show();
    }

    /* access modifiers changed from: protected */
    public void getFoldList(HashMap<String, Object> map) {
        String strDir = (String) map.get("fdir");
        if (lastFolder(strDir) || strDir.length() <= 0) {
            return;
        }
        if (Boolean.parseBoolean(map.get("bfold").toString())) {
            this.tvDir.setText(strDir);
            this.xmUtility.getFileDir(strDir);
            this.lvFile.invalidateViews();
            return;
        }
        onExit(strDir);
    }

    private boolean lastFolder(String strFoldPath) {
        File file = new File(strFoldPath);
        if (!file.isDirectory()) {
            return true;
        }
        File[] filelist = file.listFiles();
        for (File isDirectory : filelist) {
            if (isDirectory.isDirectory()) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void getFoldList(String strDir) {
        String strParent;
        if (strDir.length() > 0 && (strParent = new File(strDir).getParent()) != "/") {
            this.tvDir.setText(strParent);
            this.xmUtility.getFileDir(strParent);
            this.lvFile.invalidateViews();
        }
    }

    /* access modifiers changed from: protected */
    public void delList(HashMap<String, Object> map) {
        if (map != null) {
            String strDir = (String) map.get("fdir");
            if (strDir.length() > 1) {
                boolean bFold = Boolean.parseBoolean(map.get("bfold").toString());
                File file = new File(strDir);
                if (bFold) {
                    if (file.exists() && file.isDirectory()) {
                        file.delete();
                    }
                } else if (file.exists() && file.isFile()) {
                    file.delete();
                }
                String strCurDir = this.xmUtility.getCurDirectory();
                this.tvDir.setText(strCurDir);
                this.xmUtility.getFileDir(strCurDir);
                this.lvFile.invalidateViews();
            }
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            String strDir = this.xmUtility.getCurDirectory();
            if (!strDir.equalsIgnoreCase("/sdcard")) {
                getFoldList(strDir);
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    public void onExit(String strFile) {
        Intent intent = getIntent();
        Bundle bound = intent.getExtras();
        bound.putString("SelFile", strFile);
        intent.putExtras(bound);
        setResult(-1, intent);
        finish();
    }

    /* access modifiers changed from: protected */
    public void openFile(String strFile) {
    }
}
