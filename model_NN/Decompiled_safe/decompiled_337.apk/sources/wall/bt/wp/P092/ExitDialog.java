package wall.bt.wp.P092;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class ExitDialog extends BaseImgActivity {
    private ImageButton mCancel;
    private ImageButton mOk;
    private TextView mTextView;

    private void initView() {
        this.mTextView = (TextView) findViewById(R.id.goto_txt);
        this.mTextView.setText((int) R.string.exit_confirm);
        this.mOk = (ImageButton) findViewById(R.id.goto_ok);
        this.mCancel = (ImageButton) findViewById(R.id.goto_cancel);
        this.mOk.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ExitDialog.this.setResult(-1);
                ExitDialog.this.getParent().finish();
            }
        });
        this.mCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ExitDialog.this.setResult(0);
                ExitDialog.this.getParent().finish();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        fullScreen();
        setContentView((int) R.layout.goto_dialog);
        initView();
    }
}
