package com.xl.util;

import android.app.Activity;
import android.content.res.AssetManager;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

public class FileReader {
    private Activity activity;
    private AssetManager assetManager;
    private boolean bFirst = false;
    private boolean bLast = false;
    private boolean bValid = false;
    private char[] cPage = null;
    private String filePath = "";
    private long iCurrPosition = 0;
    private int iOnePage = 100;
    private InputStream is = null;
    private InputStreamReader isr = null;
    private long lFileLength = -1;
    private String strEncode = "";

    public FileReader(Activity activity2, String filePath2, int iOnePage2, String encode) {
        this.activity = activity2;
        this.assetManager = this.activity.getAssets();
        this.strEncode = encode;
        this.filePath = filePath2;
        this.iOnePage = iOnePage2;
        this.cPage = new char[iOnePage2];
        this.bValid = init();
        this.bFirst = true;
        this.iCurrPosition = 0;
    }

    private boolean init() {
        try {
            this.is = this.assetManager.open(this.filePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (this.is == null) {
            System.out.println("is is null");
            return false;
        }
        try {
            this.isr = new InputStreamReader(this.is, this.strEncode);
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        }
        if (this.isr != null) {
            return true;
        }
        System.out.println("isr is null");
        return false;
    }

    public long getSize() {
        if (this.lFileLength > 0) {
            return this.lFileLength;
        }
        long lSize = 0;
        try {
            lSize = this.isr.skip(1000000000000L);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.lFileLength = lSize;
        try {
            this.is = this.assetManager.open(this.filePath);
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        try {
            this.isr = new InputStreamReader(this.is, this.strEncode);
        } catch (UnsupportedEncodingException e3) {
            e3.printStackTrace();
        }
        return this.lFileLength;
    }

    public String getNextPage() {
        String strPageOut;
        try {
            int ipage = this.isr.read(this.cPage);
            if (this.iCurrPosition > 0) {
                this.bFirst = false;
            } else {
                this.bFirst = true;
            }
            this.iCurrPosition += (long) ipage;
            if (ipage < this.iOnePage) {
                this.bLast = true;
                char[] charTmp = new char[ipage];
                for (int i = 0; i < ipage; i++) {
                    charTmp[i] = this.cPage[i];
                }
                strPageOut = String.valueOf(charTmp);
            } else {
                strPageOut = String.valueOf(this.cPage);
            }
            return strPageOut;
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    public String getPrePage() {
        this.bLast = false;
        long lPosition = this.iCurrPosition - ((long) (this.iOnePage * 2));
        GenUtil.systemPrintln("iCurrPosition = " + this.iCurrPosition);
        GenUtil.systemPrintln("iOnePage = " + this.iOnePage);
        GenUtil.systemPrintln("lPosition = " + lPosition);
        return getPage(lPosition);
    }

    public String getPage(long lPosition) {
        if (this.iCurrPosition != 0) {
            try {
                this.is = this.assetManager.open(this.filePath);
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                this.isr = new InputStreamReader(this.is, this.strEncode);
            } catch (UnsupportedEncodingException e2) {
                e2.printStackTrace();
            }
        }
        long lSkip = lPosition;
        if (lPosition < 0) {
            lSkip = 0;
            this.bFirst = true;
        }
        this.iCurrPosition = lSkip;
        try {
            this.isr.skip(lSkip);
        } catch (IOException e3) {
            e3.printStackTrace();
        }
        return getNextPage();
    }

    public int getPageStatus() {
        if (this.bLast) {
            return 1;
        }
        if (this.bFirst) {
            return 2;
        }
        return 0;
    }

    public boolean getStatus() {
        return this.bValid;
    }

    public long getCurrPosition() {
        return this.iCurrPosition;
    }
}
