package yunfeng.puzzle.snowwhite;

public final class R {

    public static final class attr {
        public static final int adSize = 2130771968;
        public static final int adUnitId = 2130771969;
        public static final int backgroundColor = 2130771970;
        public static final int keywords = 2130771973;
        public static final int primaryTextColor = 2130771971;
        public static final int refreshInterval = 2130771974;
        public static final int secondaryTextColor = 2130771972;
    }

    public static final class drawable {
        public static final int bodybg = 2130837504;
        public static final int btgb = 2130837505;
        public static final int control_bg = 2130837506;
        public static final int icon = 2130837507;
        public static final int levelautop = 2130837508;
        public static final int levelnext = 2130837509;
        public static final int levelprev = 2130837510;
        public static final int levelrestart = 2130837511;
        public static final int levelviewpic = 2130837512;
        public static final int p_001 = 2130837513;
        public static final int p_002 = 2130837514;
        public static final int p_003 = 2130837515;
        public static final int p_004 = 2130837516;
        public static final int p_005 = 2130837517;
        public static final int p_006 = 2130837518;
        public static final int p_007 = 2130837519;
        public static final int p_008 = 2130837520;
        public static final int p_009 = 2130837521;
        public static final int p_010 = 2130837522;
        public static final int p_011 = 2130837523;
        public static final int p_012 = 2130837524;
        public static final int p_013 = 2130837525;
        public static final int p_014 = 2130837526;
        public static final int p_015 = 2130837527;
        public static final int p_016 = 2130837528;
        public static final int p_017 = 2130837529;
        public static final int p_018 = 2130837530;
        public static final int p_019 = 2130837531;
        public static final int p_020 = 2130837532;
        public static final int p_021 = 2130837533;
        public static final int p_022 = 2130837534;
        public static final int p_023 = 2130837535;
        public static final int p_024 = 2130837536;
        public static final int p_025 = 2130837537;
        public static final int p_026 = 2130837538;
        public static final int p_027 = 2130837539;
        public static final int p_028 = 2130837540;
        public static final int p_029 = 2130837541;
        public static final int p_030 = 2130837542;
        public static final int p_031 = 2130837543;
        public static final int p_032 = 2130837544;
        public static final int p_033 = 2130837545;
        public static final int p_034 = 2130837546;
        public static final int p_035 = 2130837547;
        public static final int p_036 = 2130837548;
        public static final int p_037 = 2130837549;
        public static final int p_038 = 2130837550;
        public static final int p_039 = 2130837551;
        public static final int p_040 = 2130837552;
        public static final int p_041 = 2130837553;
    }

    public static final class id {
        public static final int BANNER = 2130968576;
        public static final int IAB_BANNER = 2130968578;
        public static final int IAB_LEADERBOARD = 2130968579;
        public static final int IAB_MRECT = 2130968577;
        public static final int btnCurrentLevel = 2130968605;
        public static final int btnnext = 2130968590;
        public static final int btnsave = 2130968589;
        public static final int btnshare = 2130968587;
        public static final int btnwp = 2130968588;
        public static final int content_text = 2130968582;
        public static final int expandable_list_view = 2130968583;
        public static final int imageButton1 = 2130968601;
        public static final int imgBtnAutoPuzzle = 2130968595;
        public static final int imgBtnNextLevel = 2130968597;
        public static final int imgBtnPrevLevel = 2130968593;
        public static final int imgBtnRestarLevel = 2130968596;
        public static final int imgBtnViewPic = 2130968594;
        public static final int imgViewPic = 2130968614;
        public static final int layout_013 = 2130968581;
        public static final int layout_013_linear = 2130968580;
        public static final int linearLayout1 = 2130968603;
        public static final int llHeader = 2130968598;
        public static final int llMain = 2130968591;
        public static final int llViewCountInfo = 2130968612;
        public static final int llad = 2130968584;
        public static final int lladlayer = 2130968585;
        public static final int llcontrol = 2130968592;
        public static final int llfinish = 2130968586;
        public static final int relInfo = 2130968602;
        public static final int relTitle = 2130968599;
        public static final int relativeLayout2 = 2130968607;
        public static final int tvAppName = 2130968600;
        public static final int tvLevelCounts = 2130968606;
        public static final int tvLevelMoveInfo = 2130968609;
        public static final int tvLevelMoveName = 2130968608;
        public static final int tvLevelName = 2130968604;
        public static final int tvLevelTimeInfo = 2130968611;
        public static final int tvLevelTimeName = 2130968610;
        public static final int tvViewCount = 2130968613;
    }

    public static final class layout {
        public static final int group_bar = 2130903040;
        public static final int levelselect = 2130903041;
        public static final int main = 2130903042;
        public static final int viewpic = 2130903043;
    }

    public static final class string {
        public static final int alertexit = 2131034134;
        public static final int alertfinishallnotcontinue = 2131034124;
        public static final int alertfinishlevel = 2131034129;
        public static final int alertisfirstnotprev = 2131034127;
        public static final int alertnotfinishnotcontinue = 2131034126;
        public static final int alertnotselectnotfinishlevel = 2131034125;
        public static final int alertoverviewcount = 2131034128;
        public static final int alertsavefail = 2131034131;
        public static final int alertsavesecc = 2131034130;
        public static final int app_name = 2131034112;
        public static final int continuenext = 2131034114;
        public static final int countStep = 2131034118;
        public static final int countTimer = 2131034117;
        public static final int installinfo = 2131034135;
        public static final int level = 2131034119;
        public static final int levelmove = 2131034120;
        public static final int leveltime = 2131034121;
        public static final int savescard = 2131034116;
        public static final int selectlevel = 2131034122;
        public static final int selectlevelinfoformat = 2131034123;
        public static final int share = 2131034113;
        public static final int sharecontent = 2131034133;
        public static final int sharetitle = 2131034132;
        public static final int wallpaper = 2131034115;
    }

    public static final class style {
        public static final int dialog = 2131099648;
    }

    public static final class styleable {
        public static final int[] com_admob_android_ads_AdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.keywords, R.attr.refreshInterval};
        public static final int com_admob_android_ads_AdView_backgroundColor = 0;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_primaryTextColor = 1;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_secondaryTextColor = 2;
        public static final int[] com_google_ads_AdView = {R.attr.adSize, R.attr.adUnitId};
        public static final int com_google_ads_AdView_adSize = 0;
        public static final int com_google_ads_AdView_adUnitId = 1;
    }
}
