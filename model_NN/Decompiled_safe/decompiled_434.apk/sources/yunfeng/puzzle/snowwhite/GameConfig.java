package yunfeng.puzzle.snowwhite;

public class GameConfig {
    public static final int CountAllowViewCount = 3;
    public static final int CountLevel = 41;
    public static final int Difficulty = 4;
    public static final int[] DifficultyStartLevel = {1, 10, 25, 10000};
    public static final int DifficultyStartSize = 3;

    public static int getDifficultySize(int level) {
        for (int i = 0; i < DifficultyStartLevel.length - 1; i++) {
            if (level >= DifficultyStartLevel[i] && level < DifficultyStartLevel[i + 1]) {
                return i + 3;
            }
        }
        return DifficultyStartLevel[0];
    }
}
