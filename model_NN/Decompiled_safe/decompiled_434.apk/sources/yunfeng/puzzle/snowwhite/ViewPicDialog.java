package yunfeng.puzzle.snowwhite;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class ViewPicDialog extends Dialog implements View.OnClickListener {
    private Context context;
    private ImageView imgViewPic;
    private TextView tvViewCount;

    public void setImageResrources(int resID) {
        this.imgViewPic.setBackgroundResource(resID);
    }

    public void setTextInfo(String text) {
        this.tvViewCount.setText(text);
    }

    public ViewPicDialog(Context context2) {
        super(context2, R.style.dialog);
        this.context = context2;
        DisplayMetrics dm = new DisplayMetrics();
        getWindow().getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = (int) (((double) dm.widthPixels) * 0.8d);
        ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(width, width + 48);
        View rowView = getLayoutInflater().inflate((int) R.layout.viewpic, (ViewGroup) null);
        this.tvViewCount = (TextView) rowView.findViewById(R.id.tvViewCount);
        this.imgViewPic = (ImageView) rowView.findViewById(R.id.imgViewPic);
        rowView.setOnClickListener(this);
        setContentView(rowView, lp);
        setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface arg0) {
                ViewPicDialog.this.dismiss();
            }
        });
    }

    public void onClick(View v) {
        dismiss();
    }
}
