package yunfeng.puzzle.snowwhite;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class Utility {
    public static String padLeft(String s, int n, char c) {
        while (s.length() < n) {
            s = String.valueOf(c) + s;
        }
        return s;
    }

    public static String SaveSDCard(int resID, Context context) {
        Bitmap bmp = BitmapFactory.decodeResource(context.getResources(), resID);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imgData = baos.toByteArray();
        String fileName = String.valueOf(System.currentTimeMillis()) + ".jpg";
        if (Environment.getExternalStorageState().equals("mounted")) {
            File xmlCachePath = new File(String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath()) + File.separator + "puzzle_gw" + File.separator);
            try {
                if (!xmlCachePath.exists()) {
                    xmlCachePath.mkdirs();
                }
                File f = new File(String.valueOf(xmlCachePath.getAbsolutePath()) + File.separator + fileName);
                if (!f.exists()) {
                    try {
                        f.createNewFile();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
                try {
                    FileOutputStream fileOS = new FileOutputStream(f.getAbsoluteFile());
                    fileOS.write(imgData, 0, imgData.length);
                    fileOS.close();
                    return f.getAbsoluteFile().toString();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return null;
    }
}
