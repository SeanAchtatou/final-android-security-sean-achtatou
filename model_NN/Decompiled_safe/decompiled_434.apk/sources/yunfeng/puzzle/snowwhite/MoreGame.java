package yunfeng.puzzle.snowwhite;

import android.content.Context;
import java.util.List;
import java.util.Vector;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class MoreGame {
    public static List<MoreGameItem> getMoreGame(Context context) {
        Document doc = new GetRes(context).getAdXml();
        if (doc == null) {
            return null;
        }
        List<MoreGameItem> gameList = null;
        NodeList nl = doc.getElementsByTagName("item");
        if (nl.getLength() > 0) {
            gameList = new Vector<>(0);
            for (int i = 0; i < nl.getLength(); i++) {
                String pname = getStringNodeValue((Element) nl.item(i), "packageName");
                if (!pname.equals("") && !pname.equals(context.getPackageName())) {
                    MoreGameItem aitem = new MoreGameItem();
                    aitem.PackageName = pname;
                    gameList.add(aitem);
                }
            }
        }
        return gameList;
    }

    private static String getStringNodeValue(Element aElem, String nodeName) {
        NodeList subNode = aElem.getElementsByTagName(nodeName);
        if (subNode == null || subNode.getLength() <= 0 || subNode.item(0).getFirstChild() == null) {
            return "";
        }
        return subNode.item(0).getFirstChild().getNodeValue().trim();
    }

    private static int getIntNodeValue(Element aElem, String nodeName) {
        NodeList subNode = aElem.getElementsByTagName(nodeName);
        if (subNode == null || subNode.getLength() <= 0 || subNode.item(0).getFirstChild() == null) {
            return 0;
        }
        return Integer.parseInt(subNode.item(0).getFirstChild().getNodeValue().trim());
    }
}
