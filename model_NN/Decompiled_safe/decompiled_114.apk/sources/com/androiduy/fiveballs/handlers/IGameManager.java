package com.androiduy.fiveballs.handlers;

import com.androiduy.fiveballs.exceptions.AlreadySelectedBallExeption;
import com.androiduy.fiveballs.exceptions.EmptyCellExeption;
import com.androiduy.fiveballs.exceptions.NotEmptyCellExeption;
import com.androiduy.fiveballs.exceptions.NotExistEmptyCellException;
import com.androiduy.fiveballs.exceptions.UnSelectedBallExpetion;
import com.androiduy.fiveballs.model.BallColor;
import com.androiduy.fiveballs.model.Coord;
import java.util.HashMap;
import java.util.LinkedList;

public interface IGameManager {
    HashMap<Coord, BallColor> addBalls(LinkedList<BallColor> linkedList) throws NotEmptyCellExeption, EmptyCellExeption, NotExistEmptyCellException;

    boolean emptyGrid();

    LinkedList<Coord> freeCells();

    BallColor getBallColor(Coord coord) throws EmptyCellExeption;

    LinkedList<Coord> getCoordsInLine();

    LinkedList<BallColor> getCurrentNextBalls();

    LinkedList<BallColor> getNextColorBalls();

    String getPanelGrid();

    int getScore();

    Coord getSelected();

    BallColor getSelectedColor() throws UnSelectedBallExpetion, EmptyCellExeption;

    boolean hasBall(Coord coord);

    boolean hasBallsInLine();

    boolean hasSelected();

    LinkedList<Coord> moveTo(Coord coord) throws UnSelectedBallExpetion, EmptyCellExeption, NotEmptyCellExeption;

    void removeBalls() throws EmptyCellExeption;

    BallColor select(Coord coord) throws AlreadySelectedBallExeption, EmptyCellExeption;

    void unselect() throws UnSelectedBallExpetion;
}
