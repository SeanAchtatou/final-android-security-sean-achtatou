package com.androiduy.fiveballs.handlers;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) {
    }

    public static int leerInt() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            try {
                break;
            } catch (Exception e) {
                System.out.println("Error : Datos ingresados incorrectamente");
                System.out.println("Ingresar Nuevamente :");
            }
        }
        return Integer.parseInt(br.readLine());
    }

    public static String leerString() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            try {
                break;
            } catch (Exception e) {
                System.out.println("Error : Datos ingresados incorrectamente");
                System.out.println("Ingresar Nuevamente :");
            }
        }
        return br.readLine();
    }
}
