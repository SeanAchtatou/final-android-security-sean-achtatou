package com.androiduy.fiveballs.handlers;

import android.util.Log;
import com.androiduy.fiveballs.exceptions.AlreadySelectedBallExeption;
import com.androiduy.fiveballs.exceptions.EmptyCellExeption;
import com.androiduy.fiveballs.exceptions.NotEmptyCellExeption;
import com.androiduy.fiveballs.exceptions.NotExistEmptyCellException;
import com.androiduy.fiveballs.exceptions.UnSelectedBallExpetion;
import com.androiduy.fiveballs.model.BallColor;
import com.androiduy.fiveballs.model.Coord;
import com.androiduy.fiveballs.model.Grid;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

public class GameManager implements IGameManager {
    private static final String TAG = "GameManager";
    private LinkedList<Coord> coordsInLine = new LinkedList<>();
    private Grid grid = new Grid();
    private LinkedList<BallColor> nextColorBalls = new LinkedList<>();
    private int score = 0;

    public HashMap<Coord, BallColor> addBalls(LinkedList<BallColor> balls) throws NotEmptyCellExeption, EmptyCellExeption, NotExistEmptyCellException {
        HashMap<Coord, BallColor> newBalls = new HashMap<>();
        this.coordsInLine.clear();
        Iterator<BallColor> it = balls.iterator();
        while (it.hasNext()) {
            BallColor ballColor = it.next();
            if (this.grid.getCountFree() == 0) {
                break;
            }
            Coord coord = this.grid.getRandomCoord();
            this.grid.addBall(ballColor, coord);
            newBalls.put(coord, ballColor);
            Iterator<Coord> it2 = this.grid.getBallsInLine(coord).iterator();
            while (it2.hasNext()) {
                Coord coordInLine = it2.next();
                if (!this.coordsInLine.contains(coordInLine)) {
                    this.coordsInLine.add(coordInLine);
                }
            }
            if (this.coordsInLine.size() > 0) {
                this.score += getLinesScore(this.coordsInLine.size());
            }
        }
        return newBalls;
    }

    private int getLinesScore(int balls) {
        if (balls <= 6) {
            return balls * 2;
        }
        if (balls < 10) {
            return ((balls * balls) - (balls * 3)) - 10;
        }
        return balls * balls;
    }

    public LinkedList<Coord> getCoordsInLine() {
        Collections.sort(this.coordsInLine);
        return this.coordsInLine;
    }

    public LinkedList<BallColor> getNextColorBalls() {
        this.nextColorBalls.clear();
        for (int i = 0; i < 3; i++) {
            this.nextColorBalls.add(this.grid.getRandomBallColor());
        }
        return this.nextColorBalls;
    }

    public LinkedList<BallColor> getCurrentNextBalls() {
        return this.nextColorBalls;
    }

    public int getScore() {
        return this.score;
    }

    public Coord getSelected() {
        return this.grid.getSelected();
    }

    public BallColor getSelectedColor() throws UnSelectedBallExpetion, EmptyCellExeption {
        return this.grid.getSelectedColor();
    }

    public boolean hasBall(Coord coord) {
        return this.grid.hasBall(coord);
    }

    public BallColor getBallColor(Coord coord) throws EmptyCellExeption {
        return this.grid.getBall(coord).getBallColor();
    }

    public boolean hasBallsInLine() {
        return this.coordsInLine.size() >= 5;
    }

    public boolean hasSelected() {
        return this.grid.hasSelected();
    }

    public LinkedList<Coord> moveTo(Coord coord) throws UnSelectedBallExpetion, EmptyCellExeption, NotEmptyCellExeption {
        if (!this.grid.hasSelected()) {
            throw new UnSelectedBallExpetion();
        }
        if (this.grid.hasPathTo(coord)) {
            this.grid.moveTo(coord);
            this.coordsInLine.clear();
            this.coordsInLine = this.grid.getBallsInLine(coord);
            this.score += getLinesScore(this.coordsInLine.size());
        } else {
            this.grid.getPath().clear();
        }
        return this.grid.getPath();
    }

    public void removeBalls() throws EmptyCellExeption {
        this.grid.removeBalls(this.coordsInLine);
    }

    public BallColor select(Coord coord) throws AlreadySelectedBallExeption, EmptyCellExeption {
        if (!this.grid.hasSelected()) {
            return this.grid.selectBall(coord);
        }
        throw new AlreadySelectedBallExeption(this.grid.getSelected());
    }

    public void unselect() throws UnSelectedBallExpetion {
        this.grid.unselect();
    }

    public LinkedList<Coord> freeCells() {
        return this.grid.getFreeCells();
    }

    public String getPanelGrid() {
        StringBuilder builder = new StringBuilder();
        builder.append("__________________________________________________\n");
        builder.append("Next Balls: ");
        Iterator<BallColor> it = this.nextColorBalls.iterator();
        while (it.hasNext()) {
            builder.append(BallColor.getBallColorChar(it.next()));
            builder.append("  ");
        }
        builder.append("\n");
        builder.append(this.grid.toString());
        return builder.toString();
    }

    /* access modifiers changed from: package-private */
    public void LogPath() {
        LinkedList<Coord> path = this.grid.getPath();
        StringBuilder builder = new StringBuilder();
        Iterator<Coord> it = path.iterator();
        while (it.hasNext()) {
            builder.append(it.next().toString());
        }
        Log.i(TAG, builder.toString());
    }

    public boolean emptyGrid() {
        return this.grid.emptyGrid();
    }
}
