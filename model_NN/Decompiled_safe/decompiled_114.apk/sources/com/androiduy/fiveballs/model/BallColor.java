package com.androiduy.fiveballs.model;

import com.androiduy.fiveballs.view.GameActivity;
import com.androiduy.fiveballs.view.R;

public enum BallColor {
    RED,
    YELLOW,
    BLUE,
    ORANGE,
    CELESTE,
    GREEN,
    VIOLET;
    
    private static /* synthetic */ int[] $SWITCH_TABLE$com$androiduy$fiveballs$model$BallColor;

    public static char getBallColorChar(BallColor ballColor) {
        switch ($SWITCH_TABLE$com$androiduy$fiveballs$model$BallColor()[ballColor.ordinal()]) {
            case 1:
                return 'R';
            case GameActivity.DIALOG_LOCAL_SCORES /*2*/:
                return 'Y';
            case 3:
                return 'B';
            case GameActivity.DIALOG_ERROR /*4*/:
                return 'O';
            case 5:
                return 'C';
            case GameActivity.DIALOG_FILTER_BY_COUNTRY /*6*/:
                return 'G';
            case 7:
                return 'V';
            default:
                return 'X';
        }
    }

    public static BallColor getBallColor(int color) {
        switch (color) {
            case R.styleable.com_google_ads_AdView_adSize /*0*/:
                return RED;
            case 1:
                return YELLOW;
            case GameActivity.DIALOG_LOCAL_SCORES /*2*/:
                return BLUE;
            case 3:
                return ORANGE;
            case GameActivity.DIALOG_ERROR /*4*/:
                return CELESTE;
            case 5:
                return GREEN;
            case GameActivity.DIALOG_FILTER_BY_COUNTRY /*6*/:
                return VIOLET;
            default:
                throw new IllegalArgumentException("Invalid ballColor index");
        }
    }

    public static int getImageResource(BallColor ballColor, ImageType imageType) {
        if (imageType == ImageType.BALLS) {
            switch ($SWITCH_TABLE$com$androiduy$fiveballs$model$BallColor()[ballColor.ordinal()]) {
                case 1:
                    return R.drawable.red1;
                case GameActivity.DIALOG_LOCAL_SCORES /*2*/:
                    return R.drawable.yellow1;
                case 3:
                    return R.drawable.blue1;
                case GameActivity.DIALOG_ERROR /*4*/:
                    return R.drawable.orange1;
                case 5:
                    return R.drawable.celeste1;
                case GameActivity.DIALOG_FILTER_BY_COUNTRY /*6*/:
                    return R.drawable.green1;
                case 7:
                    return R.drawable.violet1;
            }
        } else if (imageType == ImageType.STARS) {
            switch ($SWITCH_TABLE$com$androiduy$fiveballs$model$BallColor()[ballColor.ordinal()]) {
                case 1:
                    return R.drawable.star_red;
                case GameActivity.DIALOG_LOCAL_SCORES /*2*/:
                    return R.drawable.star_yellow;
                case 3:
                    return R.drawable.star_blue;
                case GameActivity.DIALOG_ERROR /*4*/:
                    return R.drawable.star_orange;
                case 5:
                    return R.drawable.star_celeste;
                case GameActivity.DIALOG_FILTER_BY_COUNTRY /*6*/:
                    return R.drawable.star_green;
                case 7:
                    return R.drawable.star_violet;
            }
        } else {
            switch ($SWITCH_TABLE$com$androiduy$fiveballs$model$BallColor()[ballColor.ordinal()]) {
                case 1:
                    return R.drawable.shapes_red;
                case GameActivity.DIALOG_LOCAL_SCORES /*2*/:
                    return R.drawable.shapes_yellow;
                case 3:
                    return R.drawable.shapes_blue;
                case GameActivity.DIALOG_ERROR /*4*/:
                    return R.drawable.shapes_orange;
                case 5:
                    return R.drawable.shapes_celeste;
                case GameActivity.DIALOG_FILTER_BY_COUNTRY /*6*/:
                    return R.drawable.shapes_green;
                case 7:
                    return R.drawable.shapes_violet;
            }
        }
        return -1;
    }

    public static int getResourceAnimation(BallColor ballColor, ImageType imageType) {
        if (imageType != ImageType.BALLS) {
            return R.anim.rotate_animation;
        }
        switch ($SWITCH_TABLE$com$androiduy$fiveballs$model$BallColor()[ballColor.ordinal()]) {
            case 1:
                return R.anim.red_animation;
            case GameActivity.DIALOG_LOCAL_SCORES /*2*/:
            default:
                return R.anim.yellow_animation;
            case 3:
                return R.anim.blue_animation;
            case GameActivity.DIALOG_ERROR /*4*/:
                return R.anim.orange_animation;
            case 5:
                return R.anim.celeste_animation;
            case GameActivity.DIALOG_FILTER_BY_COUNTRY /*6*/:
                return R.anim.green_animation;
            case 7:
                return R.anim.violet_animation;
        }
    }
}
