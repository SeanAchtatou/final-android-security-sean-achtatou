package com.androiduy.fiveballs.model;

import com.androiduy.fiveballs.view.GameActivity;

public enum Direction {
    SOUTH,
    NORTH,
    EAST,
    WEST,
    SOUTHEAST,
    SOUTHWEST,
    NORTHWEST,
    NORTHESAT;
    
    private static /* synthetic */ int[] $SWITCH_TABLE$com$androiduy$fiveballs$model$Direction;

    public static Direction getOposite(Direction dir) {
        switch ($SWITCH_TABLE$com$androiduy$fiveballs$model$Direction()[dir.ordinal()]) {
            case 1:
                return NORTH;
            case GameActivity.DIALOG_LOCAL_SCORES /*2*/:
                return SOUTH;
            case 3:
                return WEST;
            case GameActivity.DIALOG_ERROR /*4*/:
                return EAST;
            case 5:
                return NORTHWEST;
            case GameActivity.DIALOG_FILTER_BY_COUNTRY /*6*/:
                return NORTHESAT;
            case 7:
            default:
                return SOUTHEAST;
            case 8:
                return SOUTHWEST;
        }
    }
}
