package com.androiduy.fiveballs.model;

import com.androiduy.fiveballs.view.GameActivity;
import java.io.Serializable;
import java.util.LinkedList;

public class Coord implements Serializable, Comparable<Coord> {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$androiduy$fiveballs$model$Direction = null;
    private static final long serialVersionUID = 5668437993441275584L;
    private int x;
    private int y;

    static /* synthetic */ int[] $SWITCH_TABLE$com$androiduy$fiveballs$model$Direction() {
        int[] iArr = $SWITCH_TABLE$com$androiduy$fiveballs$model$Direction;
        if (iArr == null) {
            iArr = new int[Direction.values().length];
            try {
                iArr[Direction.EAST.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[Direction.NORTH.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[Direction.NORTHESAT.ordinal()] = 8;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[Direction.NORTHWEST.ordinal()] = 7;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[Direction.SOUTH.ordinal()] = 1;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[Direction.SOUTHEAST.ordinal()] = 5;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[Direction.SOUTHWEST.ordinal()] = 6;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[Direction.WEST.ordinal()] = 4;
            } catch (NoSuchFieldError e8) {
            }
            $SWITCH_TABLE$com$androiduy$fiveballs$model$Direction = iArr;
        }
        return iArr;
    }

    public Coord(int x2, int y2) throws IllegalArgumentException {
        if (!assertCoord(x2, y2)) {
            throw new IllegalArgumentException("La corrdenada es incorrecta: (" + x2 + "," + y2 + ")");
        }
        setX(x2);
        setY(y2);
    }

    public Coord(Coord coord) {
        this.x = coord.getX();
        this.y = coord.getY();
    }

    public int getX() {
        return this.x;
    }

    private void setX(int x2) {
        this.x = x2;
    }

    public int getY() {
        return this.y;
    }

    private void setY(int y2) {
        this.y = y2;
    }

    public boolean equals(Object o) {
        Coord coord = (Coord) o;
        return this.x == coord.getX() && this.y == coord.getY();
    }

    public LinkedList<Coord> nearCoordsSimple() {
        LinkedList<Coord> coords = new LinkedList<>();
        addUpCoord(coords);
        addRightCoord(coords);
        addDownCoord(coords);
        addLeftCoord(coords);
        return coords;
    }

    public LinkedList<Coord> nearCoordsComplex(Coord end) {
        if (equals(end)) {
            return new LinkedList<>();
        }
        LinkedList<Coord> coords = new LinkedList<>();
        if (this.x == end.getX()) {
            if (this.y < end.getY()) {
                addRightCoord(coords);
                addUpCoord(coords);
                addDownCoord(coords);
                addLeftCoord(coords);
            } else {
                addLeftCoord(coords);
                addUpCoord(coords);
                addDownCoord(coords);
                addRightCoord(coords);
            }
        } else if (this.x > end.getX()) {
            addUpCoord(coords);
            if (this.y < end.getY()) {
                addRightCoord(coords);
                addLeftCoord(coords);
            } else {
                addLeftCoord(coords);
                addRightCoord(coords);
            }
            addDownCoord(coords);
        } else {
            addDownCoord(coords);
            if (this.y < end.getY()) {
                addRightCoord(coords);
                addLeftCoord(coords);
            } else {
                addLeftCoord(coords);
                addRightCoord(coords);
            }
            addUpCoord(coords);
        }
        return coords;
    }

    private void addDownCoord(LinkedList<Coord> coords) {
        if (assertCoord(this.x + 1, this.y)) {
            coords.add(new Coord(this.x + 1, this.y));
        }
    }

    private void addUpCoord(LinkedList<Coord> coords) {
        if (assertCoord(this.x - 1, this.y)) {
            coords.add(new Coord(this.x - 1, this.y));
        }
    }

    private void addLeftCoord(LinkedList<Coord> coords) {
        if (assertCoord(this.x, this.y - 1)) {
            coords.add(new Coord(this.x, this.y - 1));
        }
    }

    private void addRightCoord(LinkedList<Coord> coords) {
        if (assertCoord(this.x, this.y + 1)) {
            coords.add(new Coord(this.x, this.y + 1));
        }
    }

    private boolean assertCoord(int x2, int y2) {
        if (x2 < 0 || x2 >= 9 || y2 < 0 || y2 >= 9) {
            return false;
        }
        return true;
    }

    public Coord getNextCoord(Direction dir) {
        switch ($SWITCH_TABLE$com$androiduy$fiveballs$model$Direction()[dir.ordinal()]) {
            case 1:
                if (assertCoord(this.x + 1, this.y)) {
                    return new Coord(this.x + 1, this.y);
                }
                return null;
            case GameActivity.DIALOG_LOCAL_SCORES /*2*/:
                if (assertCoord(this.x - 1, this.y)) {
                    return new Coord(this.x - 1, this.y);
                }
                return null;
            case 3:
                if (assertCoord(this.x, this.y + 1)) {
                    return new Coord(this.x, this.y + 1);
                }
                return null;
            case GameActivity.DIALOG_ERROR /*4*/:
                if (assertCoord(this.x, this.y - 1)) {
                    return new Coord(this.x, this.y - 1);
                }
                return null;
            case 5:
                if (assertCoord(this.x + 1, this.y + 1)) {
                    return new Coord(this.x + 1, this.y + 1);
                }
                return null;
            case GameActivity.DIALOG_FILTER_BY_COUNTRY /*6*/:
                if (assertCoord(this.x + 1, this.y - 1)) {
                    return new Coord(this.x + 1, this.y - 1);
                }
                return null;
            case 7:
                if (assertCoord(this.x - 1, this.y - 1)) {
                    return new Coord(this.x - 1, this.y - 1);
                }
                return null;
            case 8:
                if (assertCoord(this.x - 1, this.y + 1)) {
                    return new Coord(this.x - 1, this.y + 1);
                }
                return null;
            default:
                return null;
        }
    }

    public String toString() {
        return "(" + this.x + "," + this.y + ")";
    }

    public int compareTo(Coord another) {
        if (this.x < another.getX()) {
            return -1;
        }
        if (this.x > another.getX()) {
            return 1;
        }
        if (this.y < another.getY()) {
            return -1;
        }
        if (this.y > another.getY()) {
            return 1;
        }
        return 0;
    }
}
