package com.androiduy.fiveballs.model;

public class Ball {
    private BallColor ballColor;

    public Ball(BallColor color) {
        setBallColor(color);
    }

    public BallColor getBallColor() {
        return this.ballColor;
    }

    private void setBallColor(BallColor ballColor2) {
        this.ballColor = ballColor2;
    }
}
