package com.androiduy.fiveballs.model;

import android.util.Log;
import com.androiduy.fiveballs.exceptions.EmptyCellExeption;
import com.androiduy.fiveballs.exceptions.NotEmptyCellExeption;
import com.androiduy.fiveballs.exceptions.NotExistEmptyCellException;
import com.androiduy.fiveballs.exceptions.UnSelectedBallExpetion;
import java.lang.reflect.Array;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

public class Grid {
    private static final String TAG = "grid";
    private Cell[][] cells = ((Cell[][]) Array.newInstance(Cell.class, 9, 9));
    private LinkedList<Coord> freeCells = new LinkedList<>();
    private LinkedList<Coord> path;
    private Random randomCell;
    private Random randomColor;
    private Coord selected;

    public Grid() {
        setPath(new LinkedList());
        this.selected = null;
        this.randomCell = new Random();
        this.randomCell.setSeed(Long.valueOf(new Date().getTime()).longValue());
        this.randomColor = new Random();
        this.randomColor.setSeed(Long.valueOf(new Date().getTime()).longValue());
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                Cell cell = new Cell(i, j);
                this.cells[i][j] = cell;
                this.freeCells.add(cell.getCoord());
            }
        }
    }

    public void addBall(BallColor ballColor, Coord coord) throws NotEmptyCellExeption {
        Cell cell = getCell(coord);
        if (!cell.isFree()) {
            throw new NotEmptyCellExeption(coord);
        }
        cell.setBall(new Ball(ballColor));
        this.freeCells.remove(coord);
    }

    private LinkedList<Coord> getAvialbleNearCoords(Coord coord, Coord end, int[][] visited) {
        LinkedList<Coord> nearCoords = coord.nearCoordsComplex(end);
        nearCoords.retainAll(this.freeCells);
        return nearCoords;
    }

    public Ball getBall(Coord coord) throws EmptyCellExeption {
        Cell cell = getCell(coord);
        if (!cell.isFree()) {
            return cell.getBall();
        }
        throw new EmptyCellExeption(coord);
    }

    public LinkedList<Coord> getBallsInLine(Coord moved) throws EmptyCellExeption {
        if (getCell(moved).isFree()) {
            throw new EmptyCellExeption(moved);
        }
        LinkedList<Coord> coordsInLine = new LinkedList<>();
        BallColor ballColor = getBall(moved).getBallColor();
        LinkedList<Coord> verticalInLine = getInLine(moved, Direction.SOUTH, ballColor);
        if (!verticalInLine.isEmpty()) {
            verticalInLine.removeAll(coordsInLine);
            coordsInLine.addAll(verticalInLine);
        }
        LinkedList<Coord> mainDiagonalInLine = getInLine(moved, Direction.SOUTHEAST, ballColor);
        if (!mainDiagonalInLine.isEmpty()) {
            mainDiagonalInLine.removeAll(coordsInLine);
            coordsInLine.addAll(mainDiagonalInLine);
        }
        LinkedList<Coord> secondaryDiagonalInLine = getInLine(moved, Direction.SOUTHWEST, ballColor);
        if (!secondaryDiagonalInLine.isEmpty()) {
            secondaryDiagonalInLine.removeAll(coordsInLine);
            coordsInLine.addAll(secondaryDiagonalInLine);
        }
        LinkedList<Coord> horizontalInLine = getInLine(moved, Direction.EAST, ballColor);
        if (!horizontalInLine.isEmpty()) {
            horizontalInLine.removeAll(coordsInLine);
            coordsInLine.addAll(horizontalInLine);
        }
        return coordsInLine;
    }

    private Cell getCell(Coord coord) {
        return getCell(coord.getX(), coord.getY());
    }

    private Cell getCell(int x, int y) {
        return this.cells[x][y];
    }

    public int getCountFree() {
        return this.freeCells.size();
    }

    private LinkedList<Coord> getInLine(Coord moved, Direction dir, BallColor ballColor) {
        LinkedList<Coord> inLine = new LinkedList<>();
        Coord dirNextCoord = moved.getNextCoord(dir);
        if (dirNextCoord != null) {
            inLine = inDir(dirNextCoord, dir, ballColor);
        }
        inLine.add(moved);
        Direction oposite = Direction.getOposite(dir);
        Coord opositeNextCoord = moved.getNextCoord(oposite);
        if (opositeNextCoord != null) {
            inLine.addAll(inDir(opositeNextCoord, oposite, ballColor));
        }
        if (inLine.size() < 5) {
            inLine.clear();
        }
        return inLine;
    }

    public LinkedList<Coord> getPath() {
        return this.path;
    }

    public BallColor getRandomBallColor() {
        return BallColor.getBallColor(nextColorVal());
    }

    public Coord getRandomCoord() throws NotExistEmptyCellException {
        int size = this.freeCells.size();
        if (size == 0) {
            throw new NotExistEmptyCellException();
        }
        return this.freeCells.get(nextVal(size));
    }

    public Coord getSelected() {
        return this.selected;
    }

    public BallColor getSelectedColor() throws UnSelectedBallExpetion, EmptyCellExeption {
        if (hasSelected()) {
            return getBall(this.selected).getBallColor();
        }
        throw new UnSelectedBallExpetion();
    }

    public boolean hasBall(Coord coord) {
        if (getCell(coord).getBall() != null) {
            return true;
        }
        return false;
    }

    private boolean hasPath(Coord init, Coord end, int[][] table) {
        LinkedList<Coord> coords = new LinkedList<>();
        coords.add(init);
        boolean found = false;
        int count = 0;
        while (!coords.isEmpty() && !found) {
            LinkedList<Coord> coordsAux = new LinkedList<>();
            coordsAux.addAll(coords);
            coords = new LinkedList<>();
            Iterator it = coordsAux.iterator();
            while (it.hasNext()) {
                Coord coord = (Coord) it.next();
                int x = coord.getX();
                int y = coord.getY();
                if (table[x][y] < 0 || table[x][y] > count) {
                    table[x][y] = count;
                    LinkedList<Coord> nearCoords = getAvialbleNearCoords(coord, end, table);
                    nearCoords.removeAll(coords);
                    coords.addAll(nearCoords);
                }
                if (coord.equals(end)) {
                    found = true;
                }
            }
            count++;
        }
        return found;
    }

    public boolean hasPathTo(Coord end) {
        this.path.clear();
        int[][] table = (int[][]) Array.newInstance(Integer.TYPE, 9, 9);
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                table[i][j] = 100;
            }
        }
        if (!hasPath(this.selected, end, table)) {
            return false;
        }
        LinkedList<Coord> visited = new LinkedList<>();
        visited.add(end);
        Coord aux = new Coord(end);
        while (!aux.equals(this.selected)) {
            this.path.addFirst(aux);
            LinkedList<Coord> nearCoords = aux.nearCoordsSimple();
            nearCoords.removeAll(visited);
            visited.addAll(nearCoords);
            int min = Integer.MAX_VALUE;
            Iterator<Coord> it = nearCoords.iterator();
            while (it.hasNext()) {
                Coord nearCoord = it.next();
                int x = nearCoord.getX();
                int y = nearCoord.getY();
                if (table[x][y] < min) {
                    min = table[x][y];
                    aux = nearCoord;
                }
            }
        }
        this.path.addFirst(this.selected);
        return true;
    }

    public boolean hasSelected() {
        if (this.selected != null) {
            return true;
        }
        return false;
    }

    private LinkedList<Coord> inDir(Coord coord, Direction dir, BallColor ballColor) {
        LinkedList<Coord> result = new LinkedList<>();
        Cell cell = getCell(coord);
        if (!cell.isFree() && cell.getBall().getBallColor() == ballColor) {
            result.add(coord);
            Coord nextCoord = coord.getNextCoord(dir);
            if (nextCoord != null) {
                result.addAll(inDir(nextCoord, dir, ballColor));
            }
        }
        return result;
    }

    public void moveTo(Coord coord) throws UnSelectedBallExpetion, EmptyCellExeption, NotEmptyCellExeption {
        if (!hasSelected()) {
            throw new UnSelectedBallExpetion();
        }
        Cell init = getCell(this.selected);
        if (init.isFree()) {
            throw new EmptyCellExeption(this.selected);
        }
        Cell end = getCell(coord);
        if (!end.isFree()) {
            throw new NotEmptyCellExeption(coord);
        }
        Ball ball = init.getBall();
        init.setBall(null);
        this.freeCells.add(this.selected);
        end.setBall(ball);
        this.freeCells.remove(coord);
        this.selected = null;
    }

    private int nextColorVal() {
        return this.randomColor.nextInt(7);
    }

    private int nextVal(int range) {
        return this.randomCell.nextInt(range);
    }

    public void removeBalls(LinkedList<Coord> toRemoveBalls) throws EmptyCellExeption {
        Iterator<Coord> it = toRemoveBalls.iterator();
        while (it.hasNext()) {
            Coord coord = it.next();
            Cell cell = getCell(coord);
            if (cell.isFree()) {
                throw new EmptyCellExeption(coord);
            }
            cell.setBall(null);
            this.freeCells.add(coord);
        }
    }

    public BallColor selectBall(Coord coord) throws EmptyCellExeption {
        Cell cell = getCell(coord);
        if (cell.isFree()) {
            throw new EmptyCellExeption(coord);
        }
        this.selected = new Coord(coord);
        return cell.getBall().getBallColor();
    }

    private void setPath(LinkedList<Coord> path2) {
        this.path = path2;
    }

    public void unselect() throws UnSelectedBallExpetion {
        if (!hasSelected()) {
            throw new UnSelectedBallExpetion();
        }
        this.selected = null;
    }

    public LinkedList<Coord> getFreeCells() {
        return this.freeCells;
    }

    public boolean emptyGrid() {
        return this.freeCells.size() == 81;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("    0     1     2     3     4     5     6     7     8   \n");
        builder.append("  _____________________________________________________\n");
        for (int i = 0; i < 9; i++) {
            builder.append(" |     |     |     |     |     |     |     |     |     |\n");
            builder.append(i);
            builder.append("|");
            for (int j = 0; j < 9; j++) {
                builder.append("  ");
                Cell cell = getCell(i, j);
                if (cell.getBall() == null) {
                    builder.append(" ");
                } else {
                    builder.append(BallColor.getBallColorChar(cell.getBall().getBallColor()));
                }
                builder.append("  |");
            }
            builder.append("\n");
            builder.append(" |_____|_____|_____|_____|_____|_____|_____|_____|_____|\n");
        }
        builder.append("\n");
        return builder.toString();
    }

    public void log() {
        Log.i(TAG, "  0 1 2 3 4 5 6 7 8");
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 9; i++) {
            builder.append(i);
            for (int j = 0; j < 9; j++) {
                Cell cell = getCell(i, j);
                if (cell.getBall() == null) {
                    builder.append("  ");
                } else {
                    builder.append(" " + BallColor.getBallColorChar(cell.getBall().getBallColor()));
                }
            }
            Log.i(TAG, builder.toString());
            builder = new StringBuilder();
        }
        Log.i(TAG, "");
    }
}
