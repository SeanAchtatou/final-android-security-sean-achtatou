package com.androiduy.fiveballs.model;

public enum ImageType {
    BALLS,
    SHAPES,
    STARS
}
