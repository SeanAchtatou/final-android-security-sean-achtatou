package com.androiduy.fiveballs.model;

public class Cell {
    private Ball ball;
    private Coord coord;

    public Cell(int x, int y) {
        setBall(null);
        setCoord(new Coord(x, y));
    }

    public Ball getBall() {
        return this.ball;
    }

    public void setBall(Ball ball2) {
        this.ball = ball2;
    }

    public Coord getCoord() {
        return this.coord;
    }

    public void setCoord(Coord coord2) {
        this.coord = coord2;
    }

    public boolean isFree() {
        if (this.ball == null) {
            return true;
        }
        return false;
    }
}
