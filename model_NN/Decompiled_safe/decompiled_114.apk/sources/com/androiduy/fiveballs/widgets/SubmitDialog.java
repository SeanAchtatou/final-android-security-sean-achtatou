package com.androiduy.fiveballs.widgets;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.androiduy.fiveballs.view.R;

public class SubmitDialog extends Dialog {
    public static final String TAG = "SubmitDialog";
    private int currentRanking = 0;
    private EditText nameEdit;

    public SubmitDialog(Context context) {
        super(context);
    }

    public SubmitDialog(Context context, int theme) {
        super(context, theme);
    }

    public void setCurrentRanking(int currentRanking2) {
        this.currentRanking = currentRanking2;
    }

    public int getCurrentRanking() {
        return this.currentRanking;
    }

    public static class SubitDialogBuilder {
        private boolean askForName = false;
        private String contentText;
        private Context context;
        private View layout;
        /* access modifiers changed from: private */
        public DialogInterface.OnClickListener okButtonListener;
        /* access modifiers changed from: private */
        public DialogInterface.OnClickListener shareButtonListener;
        private int theme;
        /* access modifiers changed from: private */
        public DialogInterface.OnClickListener uploadButtonListener;

        public SubitDialogBuilder(Context context2, int theme2) {
            this.context = context2;
            this.theme = theme2;
        }

        public SubitDialogBuilder(Context context2) {
            this.context = context2;
        }

        public SubmitDialog create() {
            final SubmitDialog dialog = new SubmitDialog(this.context, this.theme);
            this.layout = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate((int) R.layout.submitdialog, (ViewGroup) null);
            ((TextView) this.layout.findViewById(R.id.submitDialogContent)).setText(this.contentText);
            EditText nameEdit = (EditText) this.layout.findViewById(R.id.submitDialogNameEditText);
            dialog.setEditText(nameEdit);
            if (this.askForName) {
                nameEdit.setVisibility(0);
            } else {
                nameEdit.setVisibility(4);
            }
            Button okButton = (Button) this.layout.findViewById(R.id.submitDialogOkButton);
            Button shareButton = (Button) this.layout.findViewById(R.id.submitDialogShareButton);
            Button upButton = (Button) this.layout.findViewById(R.id.submitDialogUploadButton);
            if (this.okButtonListener != null) {
                okButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        SubitDialogBuilder.this.okButtonListener.onClick(dialog, -1);
                    }
                });
            }
            if (this.shareButtonListener != null) {
                shareButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        SubitDialogBuilder.this.shareButtonListener.onClick(dialog, -1);
                    }
                });
            }
            if (this.uploadButtonListener != null) {
                upButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        SubitDialogBuilder.this.uploadButtonListener.onClick(dialog, -1);
                    }
                });
            }
            dialog.setContentView(this.layout);
            return dialog;
        }

        public void setAskForName(boolean b) {
            this.askForName = b;
        }

        public void setOkButtonListener(DialogInterface.OnClickListener okButtonListener2) {
            this.okButtonListener = okButtonListener2;
        }

        public DialogInterface.OnClickListener getOkButtonListener() {
            return this.okButtonListener;
        }

        public void setShareButtonListener(DialogInterface.OnClickListener shareButtonListener2) {
            this.shareButtonListener = shareButtonListener2;
        }

        public DialogInterface.OnClickListener getShareButtonListener() {
            return this.shareButtonListener;
        }

        public void setUploadButtonListener(DialogInterface.OnClickListener uploadButtonListener2) {
            this.uploadButtonListener = uploadButtonListener2;
        }

        public DialogInterface.OnClickListener getUploadButtonListener() {
            return this.uploadButtonListener;
        }

        public void setContentText(String text) {
            this.contentText = text;
        }
    }

    public String getName() {
        if (this.nameEdit == null || this.nameEdit.getText() == null) {
            return null;
        }
        return this.nameEdit.getText().toString();
    }

    public void setEditText(EditText nameEdit2) {
        this.nameEdit = nameEdit2;
    }

    public void setOkButton(boolean visible) {
        Button b = (Button) findViewById(R.id.submitDialogOkButton);
        if (visible) {
            b.setVisibility(0);
        } else {
            b.setVisibility(8);
        }
    }

    public void setShareButton(boolean visible) {
        Button b = (Button) findViewById(R.id.submitDialogShareButton);
        if (visible) {
            b.setVisibility(0);
        } else {
            b.setVisibility(8);
        }
    }

    public void setUploadButton(boolean visible) {
        Button b = (Button) findViewById(R.id.submitDialogUploadButton);
        if (visible) {
            b.setVisibility(0);
        } else {
            b.setVisibility(8);
        }
    }

    public void setContentText(String contentText) {
        ((TextView) findViewById(R.id.submitDialogContent)).setText(contentText);
    }

    public void setNameVisible(boolean visible) {
        EditText nameEdit2 = (EditText) findViewById(R.id.submitDialogNameEditText);
        if (visible) {
            nameEdit2.setVisibility(0);
        } else {
            nameEdit2.setVisibility(8);
        }
    }
}
