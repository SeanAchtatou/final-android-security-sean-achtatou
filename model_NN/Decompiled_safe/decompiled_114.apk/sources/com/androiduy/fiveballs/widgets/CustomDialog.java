package com.androiduy.fiveballs.widgets;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import com.androiduy.fiveballs.model.Consts;
import com.androiduy.fiveballs.persistence.ScoreData;
import com.androiduy.fiveballs.view.GameActivity;
import com.androiduy.fiveballs.view.R;
import java.util.List;

public class CustomDialog extends Dialog {
    public static final String TAG = "CustomDialog";
    /* access modifiers changed from: private */
    public static int currentRanking = 0;
    private static String name = "";
    final int rankDateColor = getContext().getResources().getColor(R.color.rankDate);
    final int rankDateFocusColor = getContext().getResources().getColor(R.color.rankFocusDate);
    final int rankNickColor = getContext().getResources().getColor(R.color.rankNick);
    final int rankNickFocusColor = getContext().getResources().getColor(R.color.rankFocusNick);
    final int rankRankginColor = getContext().getResources().getColor(R.color.rankRanking);
    final int rankRankginFocusColor = getContext().getResources().getColor(R.color.rankFocusRanking);
    final int rankScoreColor = getContext().getResources().getColor(R.color.rankScore);
    final int rankScoreFocusColor = getContext().getResources().getColor(R.color.rankFocusScore);

    public CustomDialog(Context context) {
        super(context);
    }

    public CustomDialog(Context context, int theme) {
        super(context, theme);
    }

    public void setCurrentPosition(int position) {
        setCurrentRanking(position);
    }

    public void setCurrentRanking(int currentRanking2) {
        currentRanking = currentRanking2;
    }

    public int getCurrentRanking() {
        return currentRanking;
    }

    public static void setName(String name2) {
        name = name2;
    }

    public static String getName() {
        return name;
    }

    public static class CustomBuilder {
        /* access modifiers changed from: private */
        public DialogInterface.OnClickListener backButtonClickListener;
        private String backButtonText;
        private Context context;
        /* access modifiers changed from: private */
        public DialogInterface.OnClickListener goButtonClickListener;
        private String goButtonText;
        private boolean hasPaging;
        private View layout;
        /* access modifiers changed from: private */
        public DialogInterface.OnClickListener mePageButtonClickListener;
        private String mePageButtonText;
        /* access modifiers changed from: private */
        public DialogInterface.OnClickListener nextPageButtonClickListener;
        private String nextPageButtonText;
        /* access modifiers changed from: private */
        public DialogInterface.OnClickListener prevPageButtonClickListener;
        private String prevPageButtonText;
        private View.OnLongClickListener rowLongClickListener;
        private int theme;
        private String title;

        public CustomBuilder(Context context2, int theme2) {
            this.context = context2;
            this.theme = theme2;
            this.prevPageButtonText = context2.getString(R.string.dialog_btn_prev);
            this.mePageButtonText = context2.getString(R.string.dialog_btn_me);
            this.nextPageButtonText = context2.getString(R.string.dialog_btn_next);
            this.backButtonText = context2.getString(R.string.dialog_btn_back);
            this.goButtonText = context2.getString(R.string.dialog_btn_go);
        }

        public CustomBuilder(Context context2) {
            this.context = context2;
            this.prevPageButtonText = context2.getString(R.string.dialog_btn_prev);
            this.mePageButtonText = context2.getString(R.string.dialog_btn_me);
            this.nextPageButtonText = context2.getString(R.string.dialog_btn_next);
            this.backButtonText = context2.getString(R.string.dialog_btn_back);
            this.goButtonText = context2.getString(R.string.dialog_btn_go);
        }

        public CustomBuilder setTitle(int title2) {
            this.title = (String) this.context.getText(title2);
            return this;
        }

        public CustomBuilder setTitle(String title2) {
            this.title = title2;
            return this;
        }

        public CustomBuilder setHasPaging(boolean hasPaging2) {
            this.hasPaging = hasPaging2;
            return this;
        }

        public CustomBuilder setPrevPageButton(DialogInterface.OnClickListener listener) {
            this.prevPageButtonClickListener = listener;
            return this;
        }

        public CustomBuilder setPrevPageButton(int prevPageButtonText2, DialogInterface.OnClickListener listener) {
            this.prevPageButtonText = (String) this.context.getText(prevPageButtonText2);
            this.prevPageButtonClickListener = listener;
            return this;
        }

        public CustomBuilder setPrevPageButton(String prevPageButtonText2, DialogInterface.OnClickListener listener) {
            this.prevPageButtonText = prevPageButtonText2;
            this.prevPageButtonClickListener = listener;
            return this;
        }

        public CustomBuilder setMePageButton(DialogInterface.OnClickListener listener) {
            this.mePageButtonClickListener = listener;
            return this;
        }

        public CustomBuilder setMePageButton(int mePageButtonText2, DialogInterface.OnClickListener listener) {
            this.mePageButtonText = (String) this.context.getText(mePageButtonText2);
            this.mePageButtonClickListener = listener;
            return this;
        }

        public CustomBuilder setMePageButton(String mePageButtonText2, DialogInterface.OnClickListener listener) {
            this.mePageButtonText = mePageButtonText2;
            this.mePageButtonClickListener = listener;
            return this;
        }

        public CustomBuilder setNextPageButton(DialogInterface.OnClickListener listener) {
            this.nextPageButtonClickListener = listener;
            return this;
        }

        public CustomBuilder setNextPageButton(int nextPageButtonText2, DialogInterface.OnClickListener listener) {
            this.nextPageButtonText = (String) this.context.getText(nextPageButtonText2);
            this.nextPageButtonClickListener = listener;
            return this;
        }

        public CustomBuilder setNextPageButton(String nextPageButtonText2, DialogInterface.OnClickListener listener) {
            this.nextPageButtonText = nextPageButtonText2;
            this.nextPageButtonClickListener = listener;
            return this;
        }

        public CustomBuilder setBackPageButton(DialogInterface.OnClickListener listener) {
            this.backButtonClickListener = listener;
            return this;
        }

        public CustomBuilder setGoButton(DialogInterface.OnClickListener listener) {
            this.goButtonClickListener = listener;
            return this;
        }

        public CustomBuilder setGoButtonText(int goButtonText2) {
            this.goButtonText = this.context.getResources().getString(goButtonText2);
            return this;
        }

        public CustomBuilder setOnRowLongClickListener(View.OnLongClickListener listener) {
            this.rowLongClickListener = listener;
            return this;
        }

        public CustomDialog create(int position, String name) {
            CustomDialog customDialog = new CustomDialog(this.context, this.theme);
            this.layout = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(R.layout.customdialog, (ViewGroup) null);
            TextView titleView = (TextView) this.layout.findViewById(R.id.customDialogTitle);
            if (this.title == null) {
                this.title = this.context.getResources().getString(R.id.customDialogTitle);
            }
            titleView.setText(this.title);
            CustomDialog.currentRanking = position;
            CustomDialog.setName(name);
            this.layout.findViewById(R.id.dialogSubTitile).setVisibility(8);
            if (this.hasPaging) {
                Button prevPageButton = (Button) this.layout.findViewById(R.id.prevPageButton);
                Button mePageButton = (Button) this.layout.findViewById(R.id.mePageButton);
                Button nextPageButton = (Button) this.layout.findViewById(R.id.nextPageButton);
                prevPageButton.setText(this.prevPageButtonText);
                mePageButton.setText(this.mePageButtonText);
                nextPageButton.setText(this.nextPageButtonText);
                if (this.prevPageButtonClickListener != null) {
                    final CustomDialog customDialog2 = customDialog;
                    prevPageButton.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            CustomBuilder.this.prevPageButtonClickListener.onClick(customDialog2, -1);
                        }
                    });
                }
                if (this.mePageButtonClickListener != null) {
                    final CustomDialog customDialog3 = customDialog;
                    mePageButton.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            CustomBuilder.this.mePageButtonClickListener.onClick(customDialog3, -1);
                        }
                    });
                }
                if (this.nextPageButtonClickListener != null) {
                    final CustomDialog customDialog4 = customDialog;
                    nextPageButton.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            CustomBuilder.this.nextPageButtonClickListener.onClick(customDialog4, -1);
                        }
                    });
                }
            } else {
                ((LinearLayout) this.layout.findViewById(R.id.dialogPagingPl)).setVisibility(8);
            }
            Button backButton = (Button) this.layout.findViewById(R.id.dialogBackButton);
            Button goButton = (Button) this.layout.findViewById(R.id.dialogGoButton);
            backButton.setText(this.backButtonText);
            goButton.setText(this.goButtonText);
            if (this.backButtonClickListener != null) {
                final CustomDialog customDialog5 = customDialog;
                backButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        CustomBuilder.this.backButtonClickListener.onClick(customDialog5, -1);
                    }
                });
            }
            if (this.goButtonClickListener != null) {
                final CustomDialog customDialog6 = customDialog;
                goButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        CustomBuilder.this.goButtonClickListener.onClick(customDialog6, -1);
                    }
                });
            }
            Typeface font = Typeface.createFromAsset(this.context.getAssets(), "circulat.ttf");
            TableLayout scoreTable = (TableLayout) this.layout.findViewById(R.id.scoreTable);
            for (int i = 1; i <= 10; i++) {
                TableRow row = null;
                switch (i) {
                    case 1:
                        row = (TableRow) scoreTable.findViewById(R.id.scoreRow1);
                        break;
                    case GameActivity.DIALOG_LOCAL_SCORES /*2*/:
                        row = (TableRow) scoreTable.findViewById(R.id.scoreRow2);
                        break;
                    case 3:
                        row = (TableRow) scoreTable.findViewById(R.id.scoreRow3);
                        break;
                    case GameActivity.DIALOG_ERROR /*4*/:
                        row = (TableRow) scoreTable.findViewById(R.id.scoreRow4);
                        break;
                    case 5:
                        row = (TableRow) scoreTable.findViewById(R.id.scoreRow5);
                        break;
                    case GameActivity.DIALOG_FILTER_BY_COUNTRY /*6*/:
                        row = (TableRow) scoreTable.findViewById(R.id.scoreRow6);
                        break;
                    case 7:
                        row = (TableRow) scoreTable.findViewById(R.id.scoreRow7);
                        break;
                    case 8:
                        row = (TableRow) scoreTable.findViewById(R.id.scoreRow8);
                        break;
                    case Consts.game.gridsize:
                        row = (TableRow) scoreTable.findViewById(R.id.scoreRow9);
                        break;
                    case 10:
                        row = (TableRow) scoreTable.findViewById(R.id.scoreRow10);
                        break;
                }
                row.setOnLongClickListener(this.rowLongClickListener);
                ((TextView) row.getChildAt(3)).setTypeface(font);
            }
            customDialog.setContentView(this.layout);
            return customDialog;
        }
    }

    public void setFilter(String countryName) {
        findViewById(R.id.dialogSubTitile).setVisibility(0);
        ((TextView) findViewById(R.id.countryName)).setText(countryName);
    }

    public void clearFilter() {
        findViewById(R.id.dialogSubTitile).setVisibility(4);
        ((TextView) findViewById(R.id.countryName)).setText("");
    }

    public void setData(List<ScoreData> data) {
        TableLayout scoreTable = (TableLayout) findViewById(R.id.scoreTable);
        TextView textView = (TextView) findViewById(R.id.dialogContentEmpty);
        int size = data.size();
        if (size == 0) {
            scoreTable.setVisibility(8);
            textView.setVisibility(0);
            return;
        }
        scoreTable.setVisibility(0);
        textView.setVisibility(8);
        for (int i = 1; i <= 10; i++) {
            TableRow row = null;
            switch (i) {
                case 1:
                    row = (TableRow) scoreTable.findViewById(R.id.scoreRow1);
                    break;
                case GameActivity.DIALOG_LOCAL_SCORES /*2*/:
                    row = (TableRow) scoreTable.findViewById(R.id.scoreRow2);
                    break;
                case 3:
                    row = (TableRow) scoreTable.findViewById(R.id.scoreRow3);
                    break;
                case GameActivity.DIALOG_ERROR /*4*/:
                    row = (TableRow) scoreTable.findViewById(R.id.scoreRow4);
                    break;
                case 5:
                    row = (TableRow) scoreTable.findViewById(R.id.scoreRow5);
                    break;
                case GameActivity.DIALOG_FILTER_BY_COUNTRY /*6*/:
                    row = (TableRow) scoreTable.findViewById(R.id.scoreRow6);
                    break;
                case 7:
                    row = (TableRow) scoreTable.findViewById(R.id.scoreRow7);
                    break;
                case 8:
                    row = (TableRow) scoreTable.findViewById(R.id.scoreRow8);
                    break;
                case Consts.game.gridsize:
                    row = (TableRow) scoreTable.findViewById(R.id.scoreRow9);
                    break;
                case 10:
                    row = (TableRow) scoreTable.findViewById(R.id.scoreRow10);
                    break;
            }
            if (size < i) {
                row.setVisibility(8);
            } else {
                row.setVisibility(0);
                ScoreData scoreData = data.get(i - 1);
                String rank = String.valueOf(scoreData.getRank());
                Drawable image = scoreData.getImage();
                String nick = scoreData.getName();
                String score = String.valueOf(scoreData.getScore());
                String date = scoreData.getDateString();
                TextView rankView = (TextView) row.getChildAt(0);
                LinearLayout flagContainer = (LinearLayout) row.getChildAt(1);
                ImageView flagView = (ImageView) flagContainer.getChildAt(0);
                TextView nickView = (TextView) row.getChildAt(2);
                TextView scoreView = (TextView) row.getChildAt(3);
                TextView dateView = (TextView) row.getChildAt(4);
                rankView.setText(rank);
                if (image != null) {
                    flagView.setImageDrawable(image);
                    flagView.setVisibility(0);
                    flagContainer.setVisibility(0);
                    row.setTag(scoreData.getCountryCode());
                } else {
                    flagView.setVisibility(8);
                    flagContainer.setVisibility(8);
                }
                nickView.setText(nick);
                scoreView.setText(score);
                if (date == null) {
                    dateView.setVisibility(8);
                } else {
                    dateView.setText(date);
                }
                int ranking = Integer.parseInt(rank);
                if (ranking == 0 || ranking != currentRanking) {
                    rankView.setTextColor(this.rankRankginColor);
                    nickView.setTextColor(this.rankNickColor);
                    scoreView.setTextColor(this.rankScoreColor);
                    dateView.setTextColor(this.rankDateColor);
                } else {
                    rankView.setTextColor(this.rankRankginFocusColor);
                    nickView.setTextColor(this.rankNickFocusColor);
                    scoreView.setTextColor(this.rankScoreFocusColor);
                    dateView.setTextColor(this.rankDateFocusColor);
                }
            }
        }
    }
}
