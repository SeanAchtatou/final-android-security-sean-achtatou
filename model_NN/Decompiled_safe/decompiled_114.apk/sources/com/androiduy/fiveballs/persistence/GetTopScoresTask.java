package com.androiduy.fiveballs.persistence;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import com.androiduy.fiveballs.view.GameActivity;
import com.androiduy.fiveballs.view.R;
import java.io.IOException;
import java.util.List;

public class GetTopScoresTask extends AsyncTask<Void, Void, List<ScoreData>> {
    private static final String TAG = "GetTopScoresTask";
    private GameActivity activity;
    private String countryCode;
    private ProgressDialog dialog;
    private boolean hayResultados = false;
    private int pageNumber;

    /* access modifiers changed from: protected */
    public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        onPostExecute((List<ScoreData>) ((List) obj));
    }

    public GetTopScoresTask(GameActivity activity2, int page) {
        this.activity = activity2;
        this.pageNumber = page;
        this.countryCode = null;
    }

    public GetTopScoresTask(GameActivity activity2, int page, String countryCode2) {
        this.activity = activity2;
        this.pageNumber = page;
        this.countryCode = countryCode2;
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        this.dialog = new ProgressDialog(this.activity);
        this.dialog.setMessage(this.activity.getString(R.string.app_loading_message));
        this.dialog.setIndeterminate(true);
        this.dialog.setCancelable(true);
        this.dialog.show();
    }

    /* access modifiers changed from: protected */
    public List<ScoreData> doInBackground(Void... unused) {
        try {
            List<ScoreData> resultados = getGlobalTopScores();
            this.activity.setResultados(resultados);
            this.hayResultados = true;
            for (ScoreData scoreData : resultados) {
                String countryCode2 = scoreData.getCountryCode();
                int identifier = 0;
                if (countryCode2 != null) {
                    countryCode2 = countryCode2.toLowerCase();
                    identifier = this.activity.getResources().getIdentifier(countryCode2, "drawable", "com.androiduy.fiveballs.view");
                }
                if (identifier == 0) {
                    Log.i(TAG, "Not found flag to " + countryCode2 + " flag");
                    identifier = this.activity.getResources().getIdentifier("noflag", "drawable", "com.androiduy.fiveballs.view");
                }
                scoreData.setImage(this.activity.getResources().getDrawable(identifier));
            }
            return resultados;
        } catch (IOException e) {
            this.hayResultados = false;
            return null;
        }
    }

    private List<ScoreData> getGlobalTopScores() throws IOException {
        ScoreSubmitter ss = new ScoreSubmitter();
        if (this.countryCode == null) {
            return ss.getTopScores(this.pageNumber);
        }
        return ss.getTopScores(this.pageNumber, this.countryCode);
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(List<ScoreData> list) {
        this.dialog.dismiss();
        if (!this.hayResultados) {
            Toast tostada = Toast.makeText(this.activity, this.activity.getString(R.string.app_score_get_error), 0);
            tostada.setGravity(80, 5, 2);
            tostada.show();
        } else if (this.countryCode == null) {
            this.activity.showDialog(3);
        } else {
            this.activity.showDialog(7);
        }
    }
}
