package com.androiduy.fiveballs.persistence;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import com.androiduy.fiveballs.view.GameActivity;
import com.androiduy.fiveballs.view.R;

public class SubmitScoreTask extends AsyncTask<Void, Void, String> {
    private static final String TAG = SubmitScoreTask.class.getSimpleName();
    private ProgressDialog dialog;
    private GameActivity gameActivity;
    private String name;
    private ScoreData score;
    private long scoreId;
    private boolean share = false;

    public SubmitScoreTask(GameActivity gameActivity2, ScoreData s, long scoreId2, boolean share2) {
        this.gameActivity = gameActivity2;
        this.score = s;
        this.name = s.getName();
        this.scoreId = scoreId2;
        this.share = share2;
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        this.dialog = new ProgressDialog(this.gameActivity);
        this.dialog.setMessage(this.gameActivity.getString(R.string.app_loading_message));
        this.dialog.setIndeterminate(true);
        this.dialog.setCancelable(true);
        this.dialog.show();
    }

    /* access modifiers changed from: protected */
    public String doInBackground(Void... unused) {
        SubmitData res = new ScoreSubmitter().submitScore(this.score.getName(), String.valueOf(this.score.getScore()));
        if (res == null) {
            return null;
        }
        Log.i(TAG, "Submiting score online ... El resultado es: " + res.getRes() + " idExterno = " + res.getRemoteScoreId());
        Log.i(TAG, "Updating local db with external Id");
        this.gameActivity.getPersistencia().updateSubmitId(this.scoreId, res.getRemoteScoreId());
        return res.getRes();
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(String res) {
        int page;
        this.dialog.dismiss();
        if (res != null) {
            int position = Integer.parseInt(res);
            if (position == 0) {
                page = 0;
            } else {
                page = (position - 1) / 10;
            }
            Log.i(TAG, "La pagina calculada es " + page);
            if (this.share) {
                this.gameActivity.shareScore(getName(), this.score.getScore());
            }
            this.gameActivity.setPosition(Integer.valueOf(res).intValue());
            this.gameActivity.setCurrentPage(page);
            this.gameActivity.getGlobalTopScores(position, getName());
            return;
        }
        this.gameActivity.showError(R.string.score_submit_error);
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getName() {
        return this.name;
    }
}
