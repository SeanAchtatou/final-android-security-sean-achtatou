package com.androiduy.fiveballs.persistence;

import android.util.Log;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

public class ScoreSubmitter {
    private static final String APP_NAME = "fivemore";
    private static final String TAG = "SS";
    private static final int timeoutMs = 15000;

    public SubmitData submitScore(String username, String score) {
        return postData(APP_NAME, username, score);
    }

    public List<ScoreData> getTopScores(int pageNum) throws IOException {
        return getTopScoresXml(APP_NAME, pageNum, null);
    }

    public List<ScoreData> getTopScores(int pageNum, String countryCode) throws IOException {
        return getTopScoresXml(APP_NAME, pageNum, countryCode);
    }

    private List<ScoreData> getTopScoresXml(String appName, int pageNum, String countryCode) throws IOException {
        try {
            String urlStr = String.valueOf("http://www.southdroid.com/score?action=getscores&app=fivemore") + "&page=" + String.valueOf(pageNum);
            if (countryCode != null) {
                urlStr = String.valueOf(urlStr) + "&countryCode=" + countryCode;
            }
            URL url = new URL(urlStr);
            XMLReader xr = SAXParserFactory.newInstance().newSAXParser().getXMLReader();
            ScoreXmlHandler xmlHanlder = new ScoreXmlHandler();
            xr.setContentHandler(xmlHanlder);
            URLConnection conn = url.openConnection();
            conn.setConnectTimeout(timeoutMs);
            conn.setReadTimeout(timeoutMs);
            xr.parse(new InputSource(conn.getInputStream()));
            ParsedScoreDataSet parsedScoreDataSet = xmlHanlder.getParsedData();
            Log.d(TAG, "Los datos parseados son: " + parsedScoreDataSet.getExtractedString() + " " + parsedScoreDataSet.getExtractedInt());
            return parsedScoreDataSet.getScores();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        } catch (SAXException e2) {
            e2.printStackTrace();
            return null;
        } catch (ParserConfigurationException e3) {
            e3.printStackTrace();
            return null;
        }
    }

    private SubmitData postData(String appName, String username, String score) {
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 10000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, timeoutMs);
        HttpClient httpclient = new DefaultHttpClient(basicHttpParams);
        HttpPost httpPost = new HttpPost("http://www.southdroid.com/score");
        try {
            ArrayList arrayList = new ArrayList(2);
            arrayList.add(new BasicNameValuePair("app", appName));
            arrayList.add(new BasicNameValuePair("action", "submit"));
            arrayList.add(new BasicNameValuePair("username", URLEncoder.encode(username)));
            arrayList.add(new BasicNameValuePair("score", score));
            try {
                String claveEncriptada = SHA1(String.valueOf("fivemore4567uy24") + score + appName);
                arrayList.add(new BasicNameValuePair("key", URLEncoder.encode(claveEncriptada)));
                Log.i(TAG, "La clave encriptada es: " + claveEncriptada);
            } catch (NoSuchAlgorithmException e) {
                Log.e(TAG, "NoSuchALgorithmExceptin " + e.getLocalizedMessage());
            }
            httpPost.setEntity(new UrlEncodedFormEntity(arrayList));
            HttpResponse response = httpclient.execute(httpPost);
            Header[] headerArr = null;
            if (!response.containsHeader("submit_result_ok")) {
                return null;
            }
            Header[] header = response.getHeaders("submit_result_ok");
            Log.i(TAG, String.valueOf(header[0].getName()) + header[0].getValue());
            Header[] headerArr2 = null;
            long extScoreId = 0;
            if (response.containsHeader("submit_result_id")) {
                Header[] extScoreIdHeader = response.getHeaders("submit_result_id");
                Log.i(TAG, String.valueOf(extScoreIdHeader[0].getName()) + extScoreIdHeader[0].getValue());
                extScoreId = Long.parseLong(extScoreIdHeader[0].getValue());
            }
            return new SubmitData(header[0].getValue(), extScoreId);
        } catch (IOException | ClientProtocolException e2) {
            return null;
        }
    }

    private String convertToHex(byte[] data) {
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < data.length; i++) {
            int halfbyte = (data[i] >>> 4) & 15;
            int two_halfs = 0;
            while (true) {
                if (halfbyte < 0 || halfbyte > 9) {
                    buf.append((char) ((halfbyte - 10) + 97));
                } else {
                    buf.append((char) (halfbyte + 48));
                }
                halfbyte = data[i] & 15;
                int two_halfs2 = two_halfs + 1;
                if (two_halfs >= 1) {
                    break;
                }
                two_halfs = two_halfs2;
            }
        }
        return buf.toString();
    }

    private String SHA1(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        byte[] bArr = new byte[40];
        md.update(text.getBytes("iso-8859-1"), 0, text.length());
        return convertToHex(md.digest());
    }

    public byte[] encrypt(String text, String password) {
        try {
            byte[] input = text.getBytes();
            byte[] key = password.getBytes();
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(1, new SecretKeySpec(key, "AES"));
            return cipher.doFinal(input);
        } catch (InvalidKeyException e) {
            throw new RuntimeException(e);
        } catch (NoSuchAlgorithmException e2) {
            throw new RuntimeException(e2);
        } catch (NoSuchPaddingException e3) {
            throw new RuntimeException(e3);
        } catch (IllegalBlockSizeException e4) {
            throw new RuntimeException(e4);
        } catch (BadPaddingException e5) {
            throw new RuntimeException(e5);
        }
    }
}
