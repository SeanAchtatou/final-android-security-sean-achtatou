package com.androiduy.fiveballs.persistence;

public class SubmitData {
    private String position;
    private long remoteScoreId;

    public String getRes() {
        return this.position;
    }

    public void setRes(String res) {
        this.position = res;
    }

    public long getRemoteScoreId() {
        return this.remoteScoreId;
    }

    public void setRemoteScoreId(long remoteScoreId2) {
        this.remoteScoreId = remoteScoreId2;
    }

    public SubmitData(String res, long remoteScoreId2) {
        this.position = res;
        this.remoteScoreId = remoteScoreId2;
    }
}
