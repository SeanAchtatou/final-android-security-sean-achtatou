package com.androiduy.fiveballs.persistence;

import android.graphics.drawable.Drawable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ScoreData {
    private String countryCode;
    private String countryName;
    private Date fecha;
    private Drawable image;
    private String name;
    private int rank;
    private int score;
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy");

    public ScoreData(String name2, int score2, Date fecha2) {
        this.name = name2;
        this.score = score2;
        this.fecha = fecha2;
    }

    public ScoreData(String name2, int score2, Date fecha2, String countryCode2, String countryName2) {
        this.name = name2;
        this.score = score2;
        this.fecha = fecha2;
        this.countryCode = countryCode2;
        this.countryName = countryName2;
    }

    public ScoreData(int rank2, String name2, int score2) {
        this.rank = rank2;
        this.name = name2;
        this.score = score2;
    }

    public ScoreData(int rank2, Drawable image2, String name2, int score2, Date fecha2) {
        this.rank = rank2;
        this.image = image2;
        this.name = name2;
        this.score = score2;
        this.fecha = fecha2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public int getScore() {
        return this.score;
    }

    public void setScore(int score2) {
        this.score = score2;
    }

    public void setFecha(Date fecha2) {
        this.fecha = fecha2;
    }

    public Date getFecha() {
        return this.fecha;
    }

    public String getDateString() {
        if (this.fecha != null) {
            return this.sdf.format(this.fecha);
        }
        return null;
    }

    public String getCountryCode() {
        return this.countryCode;
    }

    public void setCountryCode(String countryCode2) {
        this.countryCode = countryCode2;
    }

    public Drawable getImage() {
        return this.image;
    }

    public void setImage(Drawable image2) {
        this.image = image2;
    }

    public int getRank() {
        return this.rank;
    }

    public void setRank(int rank2) {
        this.rank = rank2;
    }

    public String getCountryName() {
        return this.countryName;
    }
}
