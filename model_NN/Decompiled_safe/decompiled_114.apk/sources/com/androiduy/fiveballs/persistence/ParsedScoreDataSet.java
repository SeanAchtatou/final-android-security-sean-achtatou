package com.androiduy.fiveballs.persistence;

import java.util.LinkedList;
import java.util.List;

public class ParsedScoreDataSet {
    private int extractedInt = 0;
    private String extractedString = null;
    private List<ScoreData> scores = new LinkedList();

    public String getExtractedString() {
        return this.extractedString;
    }

    public void setExtractedString(String extractedString2) {
        this.extractedString = extractedString2;
    }

    public int getExtractedInt() {
        return this.extractedInt;
    }

    public void setExtractedInt(int extractedInt2) {
        this.extractedInt = extractedInt2;
    }

    public String toString() {
        return "ExtractedString = " + this.extractedString + "nExtractedInt = " + this.extractedInt;
    }

    public void setScores(List<ScoreData> scores2) {
        this.scores = scores2;
    }

    public List<ScoreData> getScores() {
        return this.scores;
    }
}
