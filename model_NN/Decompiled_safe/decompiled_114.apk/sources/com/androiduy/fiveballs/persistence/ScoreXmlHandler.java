package com.androiduy.fiveballs.persistence;

import android.util.Log;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ScoreXmlHandler extends DefaultHandler {
    private static final String ATTR_COUNTRY_CODE = "countryCode";
    private static final String ATTR_COUNTRY_NAME = "countryName";
    private static final String ATTR_DATE = "fecha";
    private static final String ATTR_NAME = "name";
    private static final String ATTR_SCORE = "score";
    private static final String INNERTAG = "innertag";
    private static final String MYTAG = "mytag";
    private static final String OUTERTAG = "topScores";
    private static final String TAG = "ScoreXmlHanlder";
    private static final String TAGWITHNUMBER = "scoreEntry";
    private boolean in_innertag = false;
    private boolean in_mytag = false;
    private boolean in_outertag = false;
    private ParsedScoreDataSet myParsedExampleDataSet = new ParsedScoreDataSet();

    public ParsedScoreDataSet getParsedData() {
        return this.myParsedExampleDataSet;
    }

    public void startDocument() throws SAXException {
        this.myParsedExampleDataSet = new ParsedScoreDataSet();
    }

    public void endDocument() throws SAXException {
    }

    public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
        if (localName.equals(OUTERTAG)) {
            this.in_outertag = true;
        } else if (localName.equals(INNERTAG)) {
            this.in_innertag = true;
        } else if (localName.equals(MYTAG)) {
            this.in_mytag = true;
        } else if (localName.equals(TAGWITHNUMBER)) {
            int i = Integer.parseInt(atts.getValue(ATTR_SCORE));
            this.myParsedExampleDataSet.setExtractedInt(i);
            String attrNameValue = URLDecoder.decode(atts.getValue(ATTR_NAME));
            this.myParsedExampleDataSet.setExtractedString(attrNameValue);
            Date fecha = new Date();
            String stringFecha = atts.getValue(ATTR_DATE);
            if (stringFecha == null || stringFecha.length() < 3) {
                stringFecha = "28/08/2010";
            }
            Log.i("SS", "La fecha parseada es + " + stringFecha);
            try {
                fecha = new SimpleDateFormat("dd/MM/yyyy").parse(stringFecha);
            } catch (ParseException e) {
                Log.e(TAG, "Ocurrió un error al parsear la fecha obtenida: " + e.getMessage());
            }
            this.myParsedExampleDataSet.getScores().add(new ScoreData(attrNameValue, i, fecha, assertNull(atts.getValue(ATTR_COUNTRY_CODE)), assertNull(atts.getValue(ATTR_COUNTRY_NAME))));
        }
    }

    public void endElement(String namespaceURI, String localName, String qName) throws SAXException {
        if (localName.equals(OUTERTAG)) {
            this.in_outertag = false;
        } else if (localName.equals(INNERTAG)) {
            this.in_innertag = false;
        } else if (localName.equals(MYTAG)) {
            this.in_mytag = false;
        } else {
            localName.equals(TAGWITHNUMBER);
        }
    }

    public void characters(char[] ch, int start, int length) {
        if (this.in_mytag) {
            this.myParsedExampleDataSet.setExtractedString(new String(ch, start, length));
        }
    }

    private String assertNull(String attr) {
        if ("null".equals(attr)) {
            return null;
        }
        return attr;
    }
}
