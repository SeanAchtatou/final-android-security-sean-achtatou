package com.androiduy.fiveballs.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class PuntajeDB extends SQLiteOpenHelper {
    private static final String DATABASE_CREATE = "create table PUNTAJES(_id integer primary key autoincrement, name text not null,score text not null,idSubmit integer not null default 0);";
    private static final String DATABASE_NAME = "FIVEMORE_SCORESDB";
    private static final String DATABASE_TABLE = "PUNTAJES";
    private static final String DATABASE_UPGRADE = "alter table PUNTAJES add column idSubmit integer not null default 0;";
    private static final String TAG = "PuntajeDB";
    private static final int TOP_SCORES_MAX = 10;
    private static SQLiteDatabase.CursorFactory fabrica;
    private SQLiteDatabase db = getWritableDatabase();

    public class Row {
        public long _Id;
        public long idSubmit;
        public String name;
        public int score;

        public Row() {
        }
    }

    public class RowComparator implements Comparator {
        public RowComparator() {
        }

        public int compare(Object o1, Object o2) {
            Row r1 = (Row) o1;
            Row r2 = (Row) o2;
            if (r1.score > r2.score) {
                return -1;
            }
            if (r1.score == r2.score) {
                return 0;
            }
            return 1;
        }
    }

    public PuntajeDB(Context ctx) {
        super(ctx, DATABASE_NAME, fabrica, 2);
    }

    public void close() {
        this.db.close();
    }

    public boolean isTopScore(int score) {
        List<Row> rows = fetchAllRows();
        if (rows.size() < 10) {
            return true;
        }
        Collections.reverse(rows);
        Row menor = rows.iterator().next();
        int scoreMin = menor.score;
        Log.i(TAG, "El mínimo score de la DB es " + scoreMin);
        if (score <= scoreMin) {
            return false;
        }
        deleteRow(menor._Id);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public long createRow(String name, String score) {
        Log.i(TAG, "Insertando en db");
        ContentValues initialValues = new ContentValues();
        initialValues.put("score", score);
        initialValues.put("name", name);
        initialValues.put("idSubmit", (Integer) 0);
        return this.db.insert(DATABASE_TABLE, null, initialValues);
    }

    public void deleteRow(long rowId) {
        this.db.delete(DATABASE_TABLE, "_id=" + rowId, null);
    }

    public List<Row> fetchAllRows() {
        LinkedList<Row> ret = new LinkedList<>();
        try {
            Cursor c = this.db.query(DATABASE_TABLE, new String[]{"_id", "name", "score", "idSubmit"}, null, null, null, null, null);
            int numRows = c.getCount();
            c.moveToFirst();
            for (int i = 0; i < numRows; i++) {
                Row row = new Row();
                row._Id = c.getLong(0);
                row.score = Integer.valueOf(c.getString(2)).intValue();
                row.name = c.getString(1);
                row.idSubmit = c.getLong(3);
                ret.add(row);
                c.moveToNext();
            }
            c.close();
        } catch (SQLException e) {
            Log.e("Exception on query", e.toString());
        }
        Collections.sort(ret, new RowComparator());
        return ret;
    }

    public Row fetchRow(long rowId) {
        Row row = new Row();
        Cursor c = this.db.query(DATABASE_TABLE, new String[]{"_id", "name", "score"}, "_id=" + rowId, null, null, null, null);
        if (c.getCount() > 0) {
            c.isFirst();
            row._Id = c.getLong(0);
            row.score = Integer.valueOf(c.getString(2)).intValue();
            row.name = c.getString(1);
            row.idSubmit = c.getLong(3);
        } else {
            row._Id = -1;
            row.score = 0;
            row.name = null;
            row.idSubmit = -1;
        }
        return row;
    }

    public void updateRow(long rowId, String name, String score) {
        ContentValues args = new ContentValues();
        args.put("name", name);
        args.put("score", score);
        this.db.update(DATABASE_TABLE, args, "_id=" + rowId, null);
    }

    public void updateSubmitId(long rowId, long submitId) {
        ContentValues args = new ContentValues();
        args.put("idSubmit", Long.valueOf(submitId));
        this.db.update(DATABASE_TABLE, args, "_id=" + rowId, null);
    }

    public Cursor GetAllRows() {
        try {
            return this.db.query(DATABASE_TABLE, new String[]{"_id", "name", "score", "idSubmit"}, null, null, null, null, null);
        } catch (SQLException e) {
            Log.e("Exception on query", e.toString());
            return null;
        }
    }

    public void onCreate(SQLiteDatabase db2) {
        Log.i(TAG, "onCreate");
        db2.execSQL(DATABASE_CREATE);
    }

    public void onUpgrade(SQLiteDatabase db2, int oldVersion, int newVersion) {
        Log.i(TAG, "onUpgrade");
        if (!db2.isReadOnly() && oldVersion == 1 && newVersion == 2) {
            db2.execSQL(DATABASE_UPGRADE);
        }
    }
}
