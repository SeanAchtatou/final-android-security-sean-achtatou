package com.androiduy.fiveballs.view;

import android.content.res.Resources;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.Toast;
import com.androiduy.fiveballs.exceptions.AlreadySelectedBallExeption;
import com.androiduy.fiveballs.exceptions.EmptyCellExeption;
import com.androiduy.fiveballs.exceptions.NotEmptyCellExeption;
import com.androiduy.fiveballs.exceptions.UnSelectedBallExpetion;
import com.androiduy.fiveballs.handlers.IGameManager;
import com.androiduy.fiveballs.model.BallColor;
import com.androiduy.fiveballs.model.Coord;
import com.androiduy.fiveballs.model.ImageType;
import com.androiduy.fiveballs.utils.MoveAnimationListener;
import com.androiduy.fiveballs.utils.RemoveBallAnimationListener;
import com.androiduy.fiveballs.utils.SoundUtils;
import com.androiduy.fiveballs.utils.Utils;
import java.util.HashMap;
import java.util.LinkedList;

public class CellView implements View.OnClickListener {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$androiduy$fiveballs$view$ClickBallAction = null;
    private static final String TAG = "CellView";
    /* access modifiers changed from: private */
    public static boolean enableSelect = true;
    private IGameManager aGame = null;
    private GameActivity activity = null;
    private MediaPlayer audioPlayer = null;

    static /* synthetic */ int[] $SWITCH_TABLE$com$androiduy$fiveballs$view$ClickBallAction() {
        int[] iArr = $SWITCH_TABLE$com$androiduy$fiveballs$view$ClickBallAction;
        if (iArr == null) {
            iArr = new int[ClickBallAction.values().length];
            try {
                iArr[ClickBallAction.MOVE_BALL.ordinal()] = 4;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[ClickBallAction.NOTHING.ordinal()] = 5;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[ClickBallAction.SELECT_BALL.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[ClickBallAction.SELECT_OTHER_BALL.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[ClickBallAction.UNSELECT_BALL.ordinal()] = 3;
            } catch (NoSuchFieldError e5) {
            }
            $SWITCH_TABLE$com$androiduy$fiveballs$view$ClickBallAction = iArr;
        }
        return iArr;
    }

    public CellView(IGameManager aGame2, GameActivity activity2, MediaPlayer mp) {
        this.aGame = aGame2;
        this.activity = activity2;
        this.audioPlayer = mp;
    }

    public void onClick(View v) {
        if (enableSelect) {
            ImageView ballView = (ImageView) v;
            Coord coord = getCoord(ballView);
            try {
                switch ($SWITCH_TABLE$com$androiduy$fiveballs$view$ClickBallAction()[getClick(coord).ordinal()]) {
                    case 1:
                        selectBall(ballView, coord);
                        return;
                    case GameActivity.DIALOG_LOCAL_SCORES /*2*/:
                        unselect();
                        selectBall(ballView, coord);
                        return;
                    case 3:
                        unselect();
                        return;
                    case GameActivity.DIALOG_ERROR /*4*/:
                        moveBall(coord);
                        return;
                    default:
                        return;
                }
            } catch (EmptyCellExeption e) {
                Log.i(TAG, e.getMessage());
            } catch (NotEmptyCellExeption e2) {
                NotEmptyCellExeption e3 = e2;
                Log.e(TAG, e3.getMessage(), e3);
                Utils.displayError(this.activity, e3.getMessage());
            } catch (AlreadySelectedBallExeption e4) {
                AlreadySelectedBallExeption e5 = e4;
                Log.e(TAG, e5.getMessage(), e5);
                Utils.displayError(this.activity, e5.getMessage());
            } catch (UnSelectedBallExpetion e6) {
                UnSelectedBallExpetion e7 = e6;
                Log.e(TAG, e7.getMessage(), e7);
                Utils.displayError(this.activity, e7.getMessage());
            } catch (Exception e8) {
                Exception e9 = e8;
                Log.e(TAG, e9.getMessage(), e9);
                Utils.displayError(this.activity, e9.getMessage());
            }
        }
    }

    private void unselect() throws UnSelectedBallExpetion {
        Coord selected = this.aGame.getSelected();
        Log.d(TAG, "unselectBall " + selected);
        ImageView imageView = findByCoord(selected);
        if (GameActivity.imageType.equals(ImageType.BALLS)) {
            ((AnimationDrawable) imageView.getBackground()).stop();
        } else {
            imageView.clearAnimation();
        }
        this.aGame.unselect();
    }

    private void selectBall(ImageView ballView, Coord coord) throws AlreadySelectedBallExeption, EmptyCellExeption {
        Log.d(TAG, "selectBall " + coord);
        this.aGame.select(coord);
        if (GameActivity.imageType.equals(ImageType.BALLS)) {
            ((AnimationDrawable) ballView.getBackground()).start();
            return;
        }
        ballView.startAnimation(AnimationUtils.loadAnimation(this.activity, BallColor.getResourceAnimation(this.aGame.getBallColor(coord), GameActivity.imageType)));
    }

    private void moveBall(Coord endCoord) throws UnSelectedBallExpetion, EmptyCellExeption, NotEmptyCellExeption {
        int resourceAnimation;
        Log.d(TAG, "moveBall " + this.aGame.getSelected() + " to " + endCoord);
        AnimationSet animationSet = new AnimationSet(true);
        LinkedList<Coord> path = this.aGame.moveTo(endCoord);
        if (!path.isEmpty()) {
            int size = path.size();
            BallColor ballColor = this.aGame.getBallColor(endCoord);
            Coord init = path.get(0);
            Coord end = path.get(size - 1);
            enableSelect = false;
            ImageView ballView = findByCoord(init);
            if (GameActivity.imageType.equals(ImageType.BALLS)) {
                ((AnimationDrawable) ballView.getBackground()).stop();
            } else {
                ballView.clearAnimation();
            }
            for (int i = 0; i < size - 1; i++) {
                Coord coord1 = path.get(i);
                Coord coord2 = path.get(i + 1);
                float fromXValue = 0.0f;
                float fromYValue = 0.0f;
                float toXValue = ((float) coord2.getY()) - ((float) coord1.getY());
                float toYValue = ((float) coord2.getX()) - ((float) coord1.getX());
                if (i == 0) {
                    fromXValue = ((float) coord1.getY()) - ((float) endCoord.getY());
                    fromYValue = ((float) coord1.getX()) - ((float) endCoord.getX());
                    toXValue = ((float) coord2.getY()) - ((float) endCoord.getY());
                    toYValue = ((float) coord2.getX()) - ((float) endCoord.getX());
                }
                TranslateAnimation ta = new TranslateAnimation(1, fromXValue, 1, toXValue, 1, fromYValue, 1, toYValue);
                ta.setDuration(25);
                ta.setStartOffset(((long) i) * 25);
                if (i == size - 2) {
                    ta.setAnimationListener(new MoveAnimationListener(init, end) {
                        public void onMoveEnd(Coord init, Coord end) {
                            CellView.enableSelect = true;
                            CellView.this.updateGame();
                        }
                    });
                }
                animationSet.addAnimation(ta);
            }
            ImageView fromBallView = findByCoord(init);
            ImageView toBallView = findByCoord(end);
            if (GameActivity.imageType.equals(ImageType.BALLS)) {
                resourceAnimation = BallColor.getResourceAnimation(ballColor, GameActivity.imageType);
            } else {
                resourceAnimation = BallColor.getImageResource(ballColor, GameActivity.imageType);
            }
            toBallView.setBackgroundResource(resourceAnimation);
            fromBallView.setBackgroundResource(R.drawable.noball);
            findByCoord(endCoord).startAnimation(animationSet);
            return;
        }
        Toast tostada = Toast.makeText(this.activity, this.activity.getResources().getString(R.string.app_invalid_move), 0);
        tostada.setGravity(80, 5, 2);
        tostada.show();
    }

    public void updateGame() {
        try {
            if (this.aGame.hasBallsInLine()) {
                playAudio(SoundUtils.SoundType.BUBBLES);
                removeBalls();
            } else {
                showBalls(this.aGame.addBalls(this.aGame.getCurrentNextBalls()));
                if (this.aGame.hasBallsInLine()) {
                    removeBalls();
                }
                nextBalls();
            }
            while (this.aGame.emptyGrid()) {
                showBalls(this.aGame.addBalls(this.aGame.getCurrentNextBalls()));
                if (this.aGame.hasBallsInLine()) {
                    removeBalls();
                }
                nextBalls();
            }
            updateScore();
            if (this.aGame.freeCells().size() == 0) {
                this.activity.showDialog(5);
            }
        } catch (Exception e) {
            Exception e2 = e;
            Log.e(TAG, e2.getMessage(), e2);
            Utils.displayError(this.activity, e2.getMessage());
        }
    }

    private void updateScore() {
        this.activity.updateScore(this.aGame.getScore());
    }

    private void nextBalls() {
        Animation animation = new ScaleAnimation(0.3f, 1.0f, 0.3f, 1.0f, 1, 0.5f, 1, 0.5f);
        animation.setDuration(200);
        animation.setFillAfter(false);
        LinkedList<BallColor> nextBalls = this.aGame.getNextColorBalls();
        ImageView nextball1 = (ImageView) this.activity.findViewById(R.id.NextBall1);
        ImageView nextball2 = (ImageView) this.activity.findViewById(R.id.NextBall2);
        ImageView nextball3 = (ImageView) this.activity.findViewById(R.id.NextBall3);
        nextball1.setImageResource(BallColor.getImageResource(nextBalls.get(0), GameActivity.imageType));
        nextball2.setImageResource(BallColor.getImageResource(nextBalls.get(1), GameActivity.imageType));
        nextball3.setImageResource(BallColor.getImageResource(nextBalls.get(2), GameActivity.imageType));
        nextball1.startAnimation(animation);
        nextball2.startAnimation(animation);
        nextball3.startAnimation(animation);
    }

    private ClickBallAction getClick(Coord selected) {
        if (!this.aGame.hasSelected()) {
            if (this.aGame.hasBall(selected)) {
                return ClickBallAction.SELECT_BALL;
            }
            return ClickBallAction.NOTHING;
        } else if (this.aGame.getSelected().equals(selected)) {
            return ClickBallAction.UNSELECT_BALL;
        } else {
            if (this.aGame.hasBall(selected)) {
                return ClickBallAction.SELECT_OTHER_BALL;
            }
            playAudio(SoundUtils.SoundType.BUBBLE_SHORT);
            return ClickBallAction.MOVE_BALL;
        }
    }

    public void addBall(Coord coord, BallColor ballColor) {
        int resourceAnimation;
        Log.d(TAG, "addBall " + coord);
        Animation animation = new ScaleAnimation(0.3f, 1.0f, 0.3f, 1.0f, 1, 0.5f, 1, 0.5f);
        animation.setDuration(200);
        animation.setFillAfter(false);
        ImageView imageView = findByCoord(coord);
        if (GameActivity.imageType.equals(ImageType.BALLS)) {
            resourceAnimation = BallColor.getResourceAnimation(ballColor, GameActivity.imageType);
        } else {
            resourceAnimation = BallColor.getImageResource(ballColor, GameActivity.imageType);
        }
        imageView.setBackgroundResource(resourceAnimation);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        imageView.startAnimation(animation);
    }

    private Coord getCoord(ImageView ballImage) {
        String resourceName = this.activity.getResources().getResourceName(ballImage.getId());
        if (resourceName.startsWith("com.androiduy.fiveballs.view:id/ball")) {
            return new Coord(Integer.parseInt(String.valueOf(resourceName.charAt("com.androiduy.fiveballs.view:id/ball".length()))), Integer.parseInt(String.valueOf(resourceName.charAt("com.androiduy.fiveballs.view:id/ball".length() + 1))));
        }
        throw new Resources.NotFoundException(resourceName);
    }

    public ImageView findByCoord(int i, int j) {
        return (ImageView) this.activity.findViewById(this.activity.getResources().getIdentifier("ball" + i + j, "id", "com.androiduy.fiveballs.view"));
    }

    public ImageView findByCoord(Coord coord) {
        return findByCoord(coord.getX(), coord.getY());
    }

    private void removeBalls() throws EmptyCellExeption {
        LinkedList<Coord> coords = this.aGame.getCoordsInLine();
        this.aGame.removeBalls();
        enableSelect = false;
        int size = coords.size();
        int i = 0;
        while (i < size) {
            ImageView imageView = findByCoord(coords.get(i));
            ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 0.0f, 1.0f, 0.0f, 1, 0.5f, 1, 0.5f);
            scaleAnimation.setDuration(100);
            scaleAnimation.setStartOffset(100 * ((long) i));
            scaleAnimation.setAnimationListener(new RemoveBallAnimationListener(imageView, i == size - 1) {
                public void onRemoveEnd(ImageView imageView, boolean enabled) {
                    imageView.setBackgroundResource(R.drawable.noball);
                    if (enabled) {
                        CellView.enableSelect = true;
                    }
                }
            });
            imageView.startAnimation(scaleAnimation);
            i++;
        }
    }

    private void showBalls(HashMap<Coord, BallColor> newBalls) {
        for (Coord coord : newBalls.keySet()) {
            addBall(coord, newBalls.get(coord));
        }
    }

    private void playAudio(SoundUtils.SoundType soundType) {
        if (this.activity.isAudioEnabled()) {
            SoundUtils.play(this.activity, this.audioPlayer, soundType);
        }
    }
}
