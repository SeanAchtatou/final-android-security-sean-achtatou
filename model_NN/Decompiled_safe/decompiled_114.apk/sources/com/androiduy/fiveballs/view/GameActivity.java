package com.androiduy.fiveballs.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;
import com.androiduy.fiveballs.exceptions.EmptyCellExeption;
import com.androiduy.fiveballs.handlers.FactoryGame;
import com.androiduy.fiveballs.handlers.IGameManager;
import com.androiduy.fiveballs.model.BallColor;
import com.androiduy.fiveballs.model.Coord;
import com.androiduy.fiveballs.model.ImageType;
import com.androiduy.fiveballs.persistence.GetTopScoresTask;
import com.androiduy.fiveballs.persistence.PuntajeDB;
import com.androiduy.fiveballs.persistence.ScoreData;
import com.androiduy.fiveballs.persistence.SubmitScoreTask;
import com.androiduy.fiveballs.utils.SoundUtils;
import com.androiduy.fiveballs.utils.Utils;
import com.androiduy.fiveballs.widgets.CustomDialog;
import com.androiduy.fiveballs.widgets.SubmitDialog;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class GameActivity extends Activity implements ViewSwitcher.ViewFactory {
    private static int ABOUT = 0;
    public static final int DIALOG_ERROR = 4;
    public static final int DIALOG_FILTER_BY_COUNTRY = 6;
    public static final int DIALOG_GLOBAL_SCORES = 3;
    public static final int DIALOG_GLOBAL_SCORES_BY_COUNTRY = 7;
    public static final int DIALOG_LOCAL_SCORES = 2;
    public static final int DIALOG_NEW_SCORE = 5;
    public static final int DIALOG_RESTART = 1;
    private static int HELP = 0;
    private static int NEW_GAME = 0;
    private static int OPTIONS = 0;
    public static final String PREFEREBCE_IMAGES = "imageTypes";
    public static final String PREFERENCE_SOUND = "sound";
    private static final String TAG = "GameActivity";
    /* access modifiers changed from: private */
    public static IGameManager aGame = null;
    public static ImageType imageType = null;
    private static final int pageSize = 10;
    /* access modifiers changed from: private */
    public int currentPage = 0;
    /* access modifiers changed from: private */
    public String filterCountryCode;
    /* access modifiers changed from: private */
    public String filterCountryName;
    private AdView mAdView;
    private RelativeLayout mGameBoardView;
    private Button mGameButtonView;
    private Button mRankingButtonView;
    private TextSwitcher mScoreSwitcher;
    private Button mShareButtonView;
    private MediaPlayer mp = null;
    /* access modifiers changed from: private */
    public int msgErrorId;
    private String name;
    /* access modifiers changed from: private */
    public PuntajeDB persistencia;
    private int position;
    /* access modifiers changed from: private */
    public List<ScoreData> resultados;
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy");
    private boolean showAnimationsLayouts;

    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate Fivemore");
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        String type = PreferenceManager.getDefaultSharedPreferences(this).getString(PREFEREBCE_IMAGES, ImageType.BALLS.toString());
        if (ImageType.BALLS.toString().equals(type)) {
            imageType = ImageType.BALLS;
        } else if (ImageType.BALLS.toString().equals(type)) {
            imageType = ImageType.SHAPES;
        } else {
            imageType = ImageType.STARS;
        }
        aGame = FactoryGame.newGame();
        setPersistencia(new PuntajeDB(getApplicationContext()));
        this.mGameBoardView = (RelativeLayout) findViewById(R.id.gameBoard);
        this.mGameButtonView = (Button) findViewById(R.id.button_game);
        this.mRankingButtonView = (Button) findViewById(R.id.button_ranking);
        this.mShareButtonView = (Button) findViewById(R.id.ButtonPreferences);
        this.mScoreSwitcher = (TextSwitcher) findViewById(R.id.scoreValue);
        this.mGameButtonView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                GameActivity.this.showDialog(1);
                GameActivity.this.playAudio(SoundUtils.SoundType.CLICK);
            }
        });
        this.mRankingButtonView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                GameActivity.this.showDialog(2);
                GameActivity.this.playAudio(SoundUtils.SoundType.CLICK);
            }
        });
        this.mShareButtonView.setOnClickListener(shareActionListener());
        this.mScoreSwitcher.setFactory(this);
        Animation in = AnimationUtils.loadAnimation(this, 17432576);
        Animation out = AnimationUtils.loadAnimation(this, 17432577);
        this.mScoreSwitcher.setInAnimation(in);
        this.mScoreSwitcher.setOutAnimation(out);
        this.mScoreSwitcher.setText(String.valueOf(String.format("%04d", 0)));
        this.mAdView = (AdView) findViewById(R.id.adView);
        if (this.mAdView != null) {
            this.mAdView.loadAd(new AdRequest());
        }
        try {
            startGame();
        } catch (Exception e) {
            Log.e(TAG, "onStart: Fallo", e);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        Log.i(TAG, "onStart Fivemore");
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        Log.i(TAG, "onResume Fivemore");
        String type = PreferenceManager.getDefaultSharedPreferences(this).getString(PREFEREBCE_IMAGES, ImageType.BALLS.toString());
        if (ImageType.BALLS.toString().equals(type) && imageType != ImageType.BALLS) {
            imageType = ImageType.BALLS;
            changeImageTypes();
        } else if (ImageType.SHAPES.toString().equals(type) && imageType != ImageType.SHAPES) {
            imageType = ImageType.SHAPES;
            changeImageTypes();
        } else if (ImageType.STARS.toString().equals(type) && imageType != ImageType.STARS) {
            imageType = ImageType.STARS;
            changeImageTypes();
        }
        super.onResume();
        this.showAnimationsLayouts = true;
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        Log.i(TAG, "onWindowFocusChanged hasFocus = " + hasFocus);
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus && this.showAnimationsLayouts) {
            Animation gameBoardAnimation = AnimationUtils.loadAnimation(this, R.anim.to_up);
            Animation gameButtonAnimation = AnimationUtils.loadAnimation(this, R.anim.to_right);
            Animation rankingButtonAnimation = AnimationUtils.loadAnimation(this, R.anim.to_down);
            Animation shareButtonAnimation = AnimationUtils.loadAnimation(this, R.anim.to_left);
            this.mGameBoardView.startAnimation(gameBoardAnimation);
            this.mGameButtonView.startAnimation(gameButtonAnimation);
            this.mRankingButtonView.startAnimation(rankingButtonAnimation);
            this.mShareButtonView.startAnimation(shareButtonAnimation);
            this.showAnimationsLayouts = false;
        }
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        Log.i(TAG, "onRestart Fivemore");
        super.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        Log.i(TAG, "onPause Fivemore");
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        Log.i(TAG, "onStop Fivemore");
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        Log.i(TAG, "onDestroy Fivemore");
        super.onDestroy();
        if (this.mp != null) {
            this.mp.release();
        }
        if (this.persistencia != null) {
            this.persistencia.close();
        }
    }

    public boolean isAudioEnabled() {
        return PreferenceManager.getDefaultSharedPreferences(getBaseContext()).getBoolean("sound", true);
    }

    private View.OnClickListener shareActionListener() {
        return new View.OnClickListener() {
            public void onClick(View arg0) {
                GameActivity.this.share();
                GameActivity.this.playAudio(SoundUtils.SoundType.CLICK);
            }
        };
    }

    private void showPreferences() {
        startActivity(new Intent(getBaseContext(), OptionsActivity.class));
    }

    public void share() {
        Intent shareIntent = new Intent("android.intent.action.SEND");
        shareIntent.setType("text/plain");
        shareIntent.putExtra("android.intent.extra.SUBJECT", "I am playing FiveBalls on my android phone.");
        shareIntent.putExtra("android.intent.extra.TEXT", "http://fiveballs.androiduy.com/share.html");
        startActivity(Intent.createChooser(shareIntent, "http://fiveballs.androiduy.com/share.html I have been playing fiveballs"));
    }

    public void shareScore(String name2, int score) {
        Intent shareIntent = new Intent("android.intent.action.SEND");
        shareIntent.setType("text/plain");
        shareIntent.putExtra("android.intent.extra.SUBJECT", "I am playing FiveBalls on my android phone.");
        shareIntent.putExtra("android.intent.extra.TEXT", "http://fiveballs.androiduy.com/share?name=" + URLEncoder.encode(name2) + "&score=" + String.valueOf(score));
        startActivity(Intent.createChooser(shareIntent, "http://fiveballs.androiduy.com/share.html I have been playing fiveballs"));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options_menu, menu);
        NEW_GAME = menu.getItem(0).getItemId();
        OPTIONS = menu.getItem(1).getItemId();
        HELP = menu.getItem(2).getItemId();
        ABOUT = menu.getItem(3).getItemId();
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        playAudio(SoundUtils.SoundType.CLICK);
        if (item.getItemId() == NEW_GAME) {
            showDialog(1);
        } else if (item.getItemId() == OPTIONS) {
            showPreferences();
        } else if (item.getItemId() == HELP) {
            Utils.displayMessage(this, getString(R.string.app_help_title), getString(R.string.app_help_paragraph1) + "\n" + getString(R.string.app_help_paragraph2) + "\n" + getString(R.string.app_help_paragraph3) + "\n" + getString(R.string.app_help_paragraph4));
        } else if (item.getItemId() != ABOUT) {
            return super.onContextItemSelected(item);
        } else {
            Utils.displayAboutMessage(this, getString(R.string.app_about_title), getString(R.string.app_about_message));
        }
        return true;
    }

    /* Debug info: failed to restart local var, previous not found, register: 28 */
    public void startGame() throws Exception {
        HashMap<Coord, BallColor> newBalls = aGame.addBalls(aGame.getNextColorBalls());
        this.mp = MediaPlayer.create(getApplicationContext(), (int) R.raw.bubble2);
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                CellView cellView = new CellView(aGame, this, this.mp);
                Coord coord = new Coord(i, j);
                Iterator<Map.Entry<Coord, BallColor>> it = newBalls.entrySet().iterator();
                while (true) {
                    if (it.hasNext()) {
                        Map.Entry<Coord, BallColor> entry = it.next();
                        Coord coord2 = (Coord) entry.getKey();
                        if (coord2.equals(coord)) {
                            cellView.addBall(coord2, (BallColor) entry.getValue());
                            break;
                        }
                    } else {
                        break;
                    }
                }
                cellView.findByCoord(coord).setOnClickListener(cellView);
            }
        }
        ScaleAnimation scaleAnimation = new ScaleAnimation(0.3f, 1.0f, 0.3f, 1.0f, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setDuration(200);
        scaleAnimation.setFillAfter(false);
        LinkedList<BallColor> nextBalls = aGame.getNextColorBalls();
        ImageView nextball1 = (ImageView) findViewById(R.id.NextBall1);
        ImageView nextball2 = (ImageView) findViewById(R.id.NextBall2);
        ImageView nextball3 = (ImageView) findViewById(R.id.NextBall3);
        nextball1.setImageResource(BallColor.getImageResource(nextBalls.get(0), imageType));
        nextball2.setImageResource(BallColor.getImageResource(nextBalls.get(1), imageType));
        nextball3.setImageResource(BallColor.getImageResource(nextBalls.get(2), imageType));
        nextball1.startAnimation(scaleAnimation);
        nextball2.startAnimation(scaleAnimation);
        nextball3.startAnimation(scaleAnimation);
    }

    public void restartGame() {
        try {
            if (aGame.hasSelected()) {
                Coord selected = aGame.getSelected();
                findByCoord(selected.getX(), selected.getY()).clearAnimation();
            }
            this.mScoreSwitcher.setText(String.valueOf(String.format("%04d", 0)));
            for (int i = 0; i < 9; i++) {
                for (int j = 0; j < 9; j++) {
                    ImageView imageView = findByCoord(i, j);
                    imageView.setBackgroundResource(R.drawable.noball);
                    imageView.setOnClickListener(null);
                }
            }
            aGame = FactoryGame.restart();
            startGame();
        } catch (Exception e) {
            Log.e(TAG, "No deberia pasar nunca.", e);
        }
    }

    public ImageView findByCoord(int i, int j) {
        return (ImageView) findViewById(getResources().getIdentifier("ball" + i + j, "id", "com.androiduy.fiveballs.view"));
    }

    public void setPersistencia(PuntajeDB persistencia2) {
        this.persistencia = persistencia2;
    }

    public PuntajeDB getPersistencia() {
        return this.persistencia;
    }

    public void goToWebSite() {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(getString(R.string.app_website_url))));
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                Log.i(TAG, "create restartDialog");
                return createRestartDialog();
            case DIALOG_LOCAL_SCORES /*2*/:
                Log.i(TAG, "create localScoresDialog");
                return createLocalScoresDialog();
            case 3:
                Log.i(TAG, "create globalScoresDialog");
                return createGlobalScoresDialog();
            case DIALOG_ERROR /*4*/:
                Log.i(TAG, "create erroDialog");
                return createErrorDialog();
            case 5:
                Log.i(TAG, "create newScoreDialog");
                return createNewScoreDialog();
            case DIALOG_FILTER_BY_COUNTRY /*6*/:
                Log.i(TAG, "create filterByCountry dialog");
                return createFilterByCountryDialog();
            case 7:
                Log.i(TAG, "create globalScoresByCountryDialog");
                return createGlobalScoresByCountryDialog();
            default:
                return super.onCreateDialog(id);
        }
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int id, Dialog dialog) {
        super.onPrepareDialog(id, dialog);
        switch (id) {
            case DIALOG_LOCAL_SCORES /*2*/:
                Log.i(TAG, "prepare localscoresDialog");
                List<PuntajeDB.Row> lscores = this.persistencia.fetchAllRows();
                ArrayList arrayList = new ArrayList();
                int i = 0;
                for (PuntajeDB.Row row : lscores) {
                    i++;
                    arrayList.add(new ScoreData(i, row.name, row.score));
                }
                CustomDialog localScoresDialog = (CustomDialog) dialog;
                localScoresDialog.clearFilter();
                localScoresDialog.setData(arrayList);
                return;
            case 3:
                Log.i(TAG, "prepare globalscoresDialog");
                int j = (this.currentPage * 10) + 1;
                for (ScoreData sd : this.resultados) {
                    sd.setRank(j);
                    j++;
                }
                CustomDialog globalScoresDialog = (CustomDialog) dialog;
                globalScoresDialog.clearFilter();
                globalScoresDialog.setCurrentPosition(this.position);
                globalScoresDialog.setData(this.resultados);
                return;
            case DIALOG_ERROR /*4*/:
                Log.i(TAG, "prepare errorDialog");
                String msg = getString(this.msgErrorId);
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("Error: ");
                stringBuilder.append(msg);
                stringBuilder.append("\n");
                ((AlertDialog) dialog).setMessage(stringBuilder.toString());
                return;
            case 5:
                Log.i(TAG, "prepate newScoreDialog");
                String finishString = getString(R.string.app_finish_game);
                String scoreString = getString(R.string.app_score_game);
                StringBuilder stringBuilder2 = new StringBuilder();
                stringBuilder2.append(finishString);
                stringBuilder2.append("\n");
                stringBuilder2.append(scoreString);
                stringBuilder2.append("  ");
                int score = aGame.getScore();
                stringBuilder2.append(score);
                SubmitDialog submitDialog = (SubmitDialog) dialog;
                boolean topScore = this.persistencia.isTopScore(score);
                submitDialog.setContentText(stringBuilder2.toString());
                if (topScore) {
                    submitDialog.setNameVisible(true);
                    submitDialog.setOkButton(true);
                    submitDialog.setUploadButton(true);
                    submitDialog.setShareButton(true);
                    return;
                }
                submitDialog.setTitle(R.string.app_finish_game);
                submitDialog.setNameVisible(false);
                submitDialog.setOkButton(true);
                submitDialog.setUploadButton(false);
                submitDialog.setShareButton(false);
                return;
            case DIALOG_FILTER_BY_COUNTRY /*6*/:
                Log.i(TAG, "prepate filterByCountry dialog");
                ListView listView = ((AlertDialog) dialog).getListView();
                StringBuilder item = (StringBuilder) listView.getAdapter().getItem(0);
                item.delete(0, item.length());
                item.append(this.filterCountryName);
                listView.invalidateViews();
                return;
            case 7:
                Log.i(TAG, "prepare globalscoresByCountryDialog filter = " + this.filterCountryCode);
                int j2 = (this.currentPage * 10) + 1;
                for (ScoreData sd2 : this.resultados) {
                    sd2.setRank(j2);
                    j2++;
                }
                CustomDialog globalScoresByCountryDialog = (CustomDialog) dialog;
                globalScoresByCountryDialog.setFilter(this.filterCountryName);
                globalScoresByCountryDialog.setData(this.resultados);
                return;
            default:
                return;
        }
    }

    private Dialog createRestartDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.msg_nuevo_juego));
        builder.setNegativeButton(getString(R.string.app_button_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GameActivity.this.playAudio(SoundUtils.SoundType.CLICK);
            }
        });
        builder.setPositiveButton(getString(R.string.app_button_accept), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GameActivity.this.restartGame();
                GameActivity.this.playAudio(SoundUtils.SoundType.CLICK);
            }
        });
        return builder.create();
    }

    private Dialog createGlobalScoresDialog() {
        CustomDialog.CustomBuilder builder = new CustomDialog.CustomBuilder(this, R.style.AquaTheme_Dialog);
        builder.setHasPaging(true);
        builder.setBackPageButton(new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GameActivity.this.playAudio(SoundUtils.SoundType.CLICK);
                dialog.dismiss();
            }
        });
        builder.setPrevPageButton(new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GameActivity.this.playAudio(SoundUtils.SoundType.CLICK);
                Log.d(GameActivity.TAG, "prev page");
                if (GameActivity.this.previousPage()) {
                    dialog.dismiss();
                }
            }
        });
        builder.setNextPageButton(new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GameActivity.this.playAudio(SoundUtils.SoundType.CLICK);
                Log.d(GameActivity.TAG, "next page");
                dialog.dismiss();
                GameActivity.this.nextPage();
            }
        });
        builder.setGoButtonText(R.string.dialog_btn_go_local_scores);
        builder.setGoButton(new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GameActivity.this.playAudio(SoundUtils.SoundType.CLICK);
                dialog.dismiss();
                GameActivity.this.showDialog(2);
            }
        });
        builder.setTitle(getResources().getString(R.string.dialog_title_global_scores));
        builder.setOnRowLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                GameActivity.this.playAudio(SoundUtils.SoundType.CLICK);
                TableRow row = (TableRow) v;
                ScoreData scoreData = (ScoreData) GameActivity.this.resultados.get(((TableLayout) row.getParent()).indexOfChild(row));
                GameActivity.this.filterCountryName = scoreData.getCountryName();
                GameActivity.this.filterCountryCode = scoreData.getCountryCode();
                if (GameActivity.this.filterCountryCode == null) {
                    return true;
                }
                GameActivity.this.showDialog(6);
                return true;
            }
        });
        return builder.create(this.position, this.name);
    }

    private Dialog createGlobalScoresByCountryDialog() {
        CustomDialog.CustomBuilder builder = new CustomDialog.CustomBuilder(this, R.style.AquaTheme_Dialog);
        builder.setHasPaging(true);
        builder.setBackPageButton(new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GameActivity.this.playAudio(SoundUtils.SoundType.CLICK);
                GameActivity.this.filterCountryCode = null;
                GameActivity.this.filterCountryName = null;
                dialog.dismiss();
            }
        });
        builder.setPrevPageButton(new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GameActivity.this.playAudio(SoundUtils.SoundType.CLICK);
                Log.d(GameActivity.TAG, "prev page");
                if (GameActivity.this.previousPage()) {
                    dialog.dismiss();
                }
            }
        });
        builder.setNextPageButton(new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GameActivity.this.playAudio(SoundUtils.SoundType.CLICK);
                Log.d(GameActivity.TAG, "next page");
                dialog.dismiss();
                GameActivity.this.nextPage();
            }
        });
        builder.setGoButtonText(R.string.dialog_btn_go_global_scores);
        builder.setGoButton(new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GameActivity.this.playAudio(SoundUtils.SoundType.CLICK);
                GameActivity.this.filterCountryCode = null;
                GameActivity.this.filterCountryName = null;
                GameActivity.this.currentPage = 0;
                dialog.dismiss();
                GameActivity.this.getGlobalTopScores();
            }
        });
        builder.setTitle(getResources().getString(R.string.dialog_title_global_scores));
        return builder.create(0, this.name);
    }

    private Dialog createLocalScoresDialog() {
        CustomDialog.CustomBuilder builder = new CustomDialog.CustomBuilder(this, R.style.AquaTheme_Dialog);
        builder.setHasPaging(false);
        builder.setBackPageButton(new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                GameActivity.this.playAudio(SoundUtils.SoundType.CLICK);
            }
        });
        builder.setGoButtonText(R.string.dialog_btn_go_global_scores);
        builder.setGoButton(new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GameActivity.this.playAudio(SoundUtils.SoundType.CLICK);
                Log.i(GameActivity.TAG, "Obteniendo los puntajes globales");
                dialog.dismiss();
                GameActivity.this.filterCountryName = null;
                GameActivity.this.filterCountryCode = null;
                GameActivity.this.getGlobalTopScores();
            }
        });
        builder.setTitle(getString(R.string.dialog_title_local_scores));
        List<PuntajeDB.Row> resultados2 = this.persistencia.fetchAllRows();
        List<ScoreData> listScores = new ArrayList<>();
        int i = 0;
        for (PuntajeDB.Row row : resultados2) {
            i++;
            listScores.add(new ScoreData(i, row.name, row.score));
        }
        return builder.create(0, "");
    }

    private Dialog createNewScoreDialog() {
        SubmitDialog.SubitDialogBuilder builder = new SubmitDialog.SubitDialogBuilder(this, R.style.AquaTheme_Dialog);
        builder.setAskForName(true);
        builder.setOkButtonListener(new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GameActivity.this.playAudio(SoundUtils.SoundType.CLICK);
                SubmitDialog d = (SubmitDialog) dialog;
                int score = GameActivity.aGame.getScore();
                if (GameActivity.this.persistencia.isTopScore(score)) {
                    String name = d.getName();
                    if (name == null || name.equals("") || name.length() < 2) {
                        GameActivity.this.msgErrorId = R.string.app_error_empty_name;
                        dialog.dismiss();
                        GameActivity.this.showDialog(4);
                        return;
                    }
                    GameActivity.this.persistencia.createRow(name, String.valueOf(score));
                    GameActivity.this.showDialog(2);
                    dialog.dismiss();
                    return;
                }
                dialog.dismiss();
            }
        });
        builder.setUploadButtonListener(new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GameActivity.this.playAudio(SoundUtils.SoundType.CLICK);
                int score = GameActivity.aGame.getScore();
                if (GameActivity.this.persistencia.isTopScore(score)) {
                    String name = ((SubmitDialog) dialog).getName();
                    if (name == null || name.equals("") || name.length() < 2) {
                        GameActivity.this.msgErrorId = R.string.app_error_empty_name;
                        GameActivity.this.showDialog(4);
                        dialog.dismiss();
                        return;
                    }
                    dialog.dismiss();
                    new SubmitScoreTask(GameActivity.this, new ScoreData(name.replaceAll("\n", " ").trim(), score, (Date) null), GameActivity.this.persistencia.createRow(name, String.valueOf(score)), false).execute(new Void[0]);
                }
            }
        });
        builder.setShareButtonListener(new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GameActivity.this.playAudio(SoundUtils.SoundType.CLICK);
                int score = GameActivity.aGame.getScore();
                if (GameActivity.this.persistencia.isTopScore(score)) {
                    String name = ((SubmitDialog) dialog).getName();
                    if (name == null || name.equals("") || name.length() < 2) {
                        GameActivity.this.msgErrorId = R.string.app_error_empty_name;
                        GameActivity.this.showDialog(4);
                        dialog.dismiss();
                    } else {
                        dialog.dismiss();
                        new SubmitScoreTask(GameActivity.this, new ScoreData(name.replaceAll("\n", " ").trim(), score, (Date) null), GameActivity.this.persistencia.createRow(name, String.valueOf(score)), true).execute(new Void[0]);
                    }
                    GameActivity.this.shareScore(name, score);
                }
            }
        });
        return builder.create();
    }

    private Dialog createErrorDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Error");
        builder.setPositiveButton((int) R.string.app_ok_button_text, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (GameActivity.this.msgErrorId == R.string.app_error_empty_name) {
                    GameActivity.this.playAudio(SoundUtils.SoundType.CLICK);
                    GameActivity.this.showDialog(5);
                }
            }
        });
        return builder.create();
    }

    private Dialog createFilterByCountryDialog() {
        CharSequence[] items = {new StringBuilder()};
        AlertDialog.Builder b = new AlertDialog.Builder(this);
        b.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GameActivity.this.playAudio(SoundUtils.SoundType.CLICK);
                GameActivity.this.currentPage = 0;
                GameActivity.this.getGlobalTopScoresByCountry();
                GameActivity.this.dismissDialog(3);
            }
        });
        AlertDialog spinner = b.create();
        spinner.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                Log.d(GameActivity.TAG, "onCancel filterByCountryDialog");
                GameActivity.this.filterCountryCode = null;
                GameActivity.this.filterCountryName = null;
            }
        });
        return spinner;
    }

    /* access modifiers changed from: protected */
    public void nextPage() {
        this.currentPage++;
        if (this.filterCountryCode == null) {
            getGlobalTopScores();
        } else {
            getGlobalTopScoresByCountry();
        }
    }

    /* access modifiers changed from: private */
    public boolean previousPage() {
        if (this.currentPage > 0) {
            this.currentPage--;
            if (this.filterCountryCode == null) {
                getGlobalTopScores();
            } else {
                getGlobalTopScoresByCountry();
            }
            return true;
        }
        Toast tostada = Toast.makeText(this, getString(R.string.app_first_page_msg), 0);
        tostada.setGravity(80, 5, 2);
        tostada.show();
        return false;
    }

    public void getGlobalTopScores() {
        GetTopScoresTask task = new GetTopScoresTask(this, this.currentPage);
        Log.i(TAG, "get score page:" + this.currentPage);
        task.execute(new Void[0]);
    }

    /* access modifiers changed from: private */
    public void getGlobalTopScoresByCountry() {
        GetTopScoresTask task = new GetTopScoresTask(this, this.currentPage, this.filterCountryCode);
        Log.i(TAG, "get score page:" + this.currentPage + " by " + this.filterCountryCode);
        task.execute(new Void[0]);
    }

    public void setResultados(List<ScoreData> resultados2) {
        this.resultados = resultados2;
    }

    public void setCurrentPage(int currentPage2) {
        this.currentPage = currentPage2;
    }

    public void setPosition(int position2) {
        this.position = position2;
    }

    public void getGlobalTopScores(int position2, String name2) {
        this.position = position2;
        this.name = name2;
        getGlobalTopScores();
    }

    public void showError(int errorId) {
        this.msgErrorId = errorId;
        showDialog(4);
    }

    /* Debug info: failed to restart local var, previous not found, register: 14 */
    public void changeImageTypes() {
        int resourceAnimation;
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                Coord coord = new Coord(i, j);
                if (aGame.hasBall(coord)) {
                    try {
                        BallColor ballColor = aGame.getBallColor(coord);
                        ImageView imageView = findByCoord(i, j);
                        if (imageType.equals(ImageType.BALLS)) {
                            resourceAnimation = BallColor.getResourceAnimation(ballColor, imageType);
                        } else {
                            resourceAnimation = BallColor.getImageResource(ballColor, imageType);
                        }
                        imageView.clearAnimation();
                        imageView.setBackgroundResource(resourceAnimation);
                    } catch (EmptyCellExeption e) {
                        EmptyCellExeption e2 = e;
                        Log.e(TAG, "No deberia pasar", e2);
                        e2.printStackTrace();
                    }
                }
            }
        }
        LinkedList<BallColor> currentNextBalls = aGame.getCurrentNextBalls();
        ((ImageView) findViewById(R.id.NextBall1)).setImageResource(BallColor.getImageResource(currentNextBalls.get(0), imageType));
        ((ImageView) findViewById(R.id.NextBall2)).setImageResource(BallColor.getImageResource(currentNextBalls.get(1), imageType));
        ((ImageView) findViewById(R.id.NextBall3)).setImageResource(BallColor.getImageResource(currentNextBalls.get(2), imageType));
    }

    public View makeView() {
        TextView t = new TextView(this);
        t.setTextSize(55.0f);
        t.setTypeface(Typeface.createFromAsset(getAssets(), "digital-7.ttf"));
        return t;
    }

    public void updateScore(int score) {
        this.mScoreSwitcher.setText(String.valueOf(String.format("%04d", Integer.valueOf(score))));
    }

    public void playAudio(SoundUtils.SoundType soundType) {
        if (isAudioEnabled()) {
            SoundUtils.play(this, this.mp, soundType);
        }
    }
}
