package com.androiduy.fiveballs.view;

public enum ClickBallAction {
    SELECT_BALL,
    SELECT_OTHER_BALL,
    UNSELECT_BALL,
    MOVE_BALL,
    NOTHING
}
