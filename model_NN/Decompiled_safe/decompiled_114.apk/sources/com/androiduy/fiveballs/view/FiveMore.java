package com.androiduy.fiveballs.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class FiveMore extends Activity {
    private static final String TAG = "SplashScreenActivity";
    private ImageView logo;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.splash);
        this.logo = (ImageView) findViewById(R.id.splashLogo);
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.splash_screen);
        anim.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                ((LinearLayout) FiveMore.this.findViewById(R.id.splashLayout)).removeAllViews();
                FiveMore.this.startActivity(new Intent(FiveMore.this, GameActivity.class));
                FiveMore.this.setActivityAnimation(FiveMore.this, R.anim.fade, R.anim.hold);
                FiveMore.this.finish();
            }
        });
        this.logo.startAnimation(anim);
    }

    public void setActivityAnimation(Activity activity, int in, int out) {
        Log.i(TAG, "setActivityAnimation");
        Class<Activity> cls = Activity.class;
        try {
            cls.getMethod("overridePendingTransition", Integer.TYPE, Integer.TYPE).invoke(activity, Integer.valueOf(in), Integer.valueOf(out));
        } catch (Exception e) {
            Log.w(TAG, e);
            getWindow().setWindowAnimations(in);
        }
    }
}
