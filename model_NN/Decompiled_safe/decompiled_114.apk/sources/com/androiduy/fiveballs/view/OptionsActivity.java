package com.androiduy.fiveballs.view;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.util.Log;

public class OptionsActivity extends PreferenceActivity {
    private static final String SOUND_ENABLED_PREFERENCE_KEY = "sound";
    private static final String TAG = "OptionsActivity";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        findPreference("sound").setOnPreferenceClickListener(onPreferenceClickListener());
    }

    private Preference.OnPreferenceClickListener onPreferenceClickListener() {
        return new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                String prefKey = preference.getKey();
                Log.d(OptionsActivity.TAG, "Se ha modificado la preferencia " + prefKey);
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(OptionsActivity.this.getBaseContext());
                boolean valor = prefs.getBoolean(prefKey, true);
                prefs.edit().putBoolean(prefKey, valor);
                prefs.edit().commit();
                Log.d(OptionsActivity.TAG, "El resultado de la preferencia es: " + valor);
                return true;
            }
        };
    }
}
