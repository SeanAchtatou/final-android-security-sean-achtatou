package com.androiduy.fiveballs.utils;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.Log;
import com.androiduy.fiveballs.view.GameActivity;
import com.androiduy.fiveballs.view.R;

public class SoundUtils {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$androiduy$fiveballs$utils$SoundUtils$SoundType = null;
    private static final String TAG = "FiveBalls";

    public enum SoundType {
        CLICK,
        BUBBLES,
        BUBBLE_SHORT
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$androiduy$fiveballs$utils$SoundUtils$SoundType() {
        int[] iArr = $SWITCH_TABLE$com$androiduy$fiveballs$utils$SoundUtils$SoundType;
        if (iArr == null) {
            iArr = new int[SoundType.values().length];
            try {
                iArr[SoundType.BUBBLES.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[SoundType.BUBBLE_SHORT.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[SoundType.CLICK.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$androiduy$fiveballs$utils$SoundUtils$SoundType = iArr;
        }
        return iArr;
    }

    public static void play(Context context, MediaPlayer mp, SoundType sound) {
        int audioId = R.raw.click;
        switch ($SWITCH_TABLE$com$androiduy$fiveballs$utils$SoundUtils$SoundType()[sound.ordinal()]) {
            case 1:
                audioId = R.raw.click;
                break;
            case GameActivity.DIALOG_LOCAL_SCORES /*2*/:
                audioId = R.raw.bubbles2;
                break;
            case 3:
                audioId = R.raw.bubble2;
                break;
        }
        try {
            MediaPlayer mp2 = MediaPlayer.create(context, audioId);
            if (mp2 != null) {
                mp2.start();
            } else {
                Log.i(TAG, "The audioPlayer reference was lost, disabling audio");
            }
        } catch (IllegalStateException e) {
            Log.e(TAG, "Error en el start del audioPlayer. Illegal State");
            e.printStackTrace();
        } catch (Exception e2) {
            Log.e(TAG, "An error ocurred when play aduio");
            e2.printStackTrace();
        }
    }
}
