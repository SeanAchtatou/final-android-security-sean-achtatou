package com.androiduy.fiveballs.utils;

import android.app.AlertDialog;
import android.content.DialogInterface;
import com.androiduy.fiveballs.utils.SoundUtils;
import com.androiduy.fiveballs.view.GameActivity;
import com.androiduy.fiveballs.view.R;

public class Utils {
    public static void displayMessage(final GameActivity activity, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title != null ? title : "");
        builder.setIcon(activity.getResources().getDrawable(R.drawable.iconcolors));
        builder.setMessage(message);
        builder.setPositiveButton((int) R.string.app_button_accept, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GameActivity.this.playAudio(SoundUtils.SoundType.CLICK);
            }
        });
        builder.create().show();
    }

    public static void displayError(final GameActivity activity, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Se ha producido un error");
        builder.setIcon(activity.getResources().getDrawable(R.drawable.iconcolors));
        builder.setMessage(message);
        builder.setPositiveButton((int) R.string.app_button_accept, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GameActivity.this.playAudio(SoundUtils.SoundType.CLICK);
                GameActivity.this.finish();
            }
        });
        builder.create().show();
    }

    public static void displayAboutMessage(final GameActivity activity, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title != null ? title : "");
        builder.setIcon(activity.getResources().getDrawable(R.drawable.iconcolors));
        builder.setMessage(message);
        builder.setNeutralButton(activity.getString(R.string.app_about_button_moreapps), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GameActivity.this.playAudio(SoundUtils.SoundType.CLICK);
                GameActivity.this.goToWebSite();
            }
        });
        builder.setPositiveButton((int) R.string.app_share_button, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GameActivity.this.playAudio(SoundUtils.SoundType.CLICK);
                GameActivity.this.share();
            }
        });
        builder.setNegativeButton((int) R.string.app_button_accept, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                GameActivity.this.playAudio(SoundUtils.SoundType.CLICK);
            }
        });
        builder.create().show();
    }
}
