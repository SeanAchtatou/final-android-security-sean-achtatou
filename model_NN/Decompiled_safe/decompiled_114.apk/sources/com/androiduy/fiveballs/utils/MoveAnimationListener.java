package com.androiduy.fiveballs.utils;

import android.view.animation.Animation;
import com.androiduy.fiveballs.model.Coord;

public abstract class MoveAnimationListener implements Animation.AnimationListener {
    private Coord end;
    private Coord init;

    public abstract void onMoveEnd(Coord coord, Coord coord2);

    public MoveAnimationListener(Coord init2, Coord end2) {
        this.init = init2;
        this.end = end2;
    }

    public void onAnimationEnd(Animation animation) {
        onMoveEnd(this.init, this.end);
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }
}
