package com.androiduy.fiveballs.utils;

import android.view.animation.Animation;
import android.widget.ImageView;

public abstract class RemoveBallAnimationListener implements Animation.AnimationListener {
    private boolean enabled;
    private ImageView imageView;

    public abstract void onRemoveEnd(ImageView imageView2, boolean z);

    public RemoveBallAnimationListener(ImageView imageView2, boolean enabled2) {
        this.imageView = imageView2;
        this.enabled = enabled2;
    }

    public void onAnimationEnd(Animation animation) {
        onRemoveEnd(this.imageView, this.enabled);
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }
}
