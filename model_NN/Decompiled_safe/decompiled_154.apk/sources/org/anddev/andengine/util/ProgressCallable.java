package org.anddev.andengine.util;

import org.anddev.progressmonitor.IProgressListener;

public interface ProgressCallable<T> {
    T call(IProgressListener iProgressListener) throws Exception;
}
