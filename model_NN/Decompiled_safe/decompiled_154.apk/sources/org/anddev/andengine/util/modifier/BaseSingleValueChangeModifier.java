package org.anddev.andengine.util.modifier;

import org.anddev.andengine.util.modifier.IModifier;

public abstract class BaseSingleValueChangeModifier<T> extends BaseDurationModifier<T> {
    private final float mValueChangePerSecond;

    /* access modifiers changed from: protected */
    public abstract void onChangeValue(Object obj, float f);

    public BaseSingleValueChangeModifier(float pDuration, float pValueChange) {
        this(pDuration, pValueChange, null);
    }

    public BaseSingleValueChangeModifier(float pDuration, float pValueChange, IModifier.IModifierListener<T> pModiferListener) {
        super(pDuration, pModiferListener);
        this.mValueChangePerSecond = pValueChange / pDuration;
    }

    protected BaseSingleValueChangeModifier(BaseSingleValueChangeModifier<T> pBaseSingleValueChangeModifier) {
        super(pBaseSingleValueChangeModifier);
        this.mValueChangePerSecond = pBaseSingleValueChangeModifier.mValueChangePerSecond;
    }

    /* access modifiers changed from: protected */
    public void onManagedInitialize(T t) {
    }

    /* access modifiers changed from: protected */
    public void onManagedUpdate(float pSecondsElapsed, T pItem) {
        onChangeValue(pItem, this.mValueChangePerSecond * pSecondsElapsed);
    }
}
