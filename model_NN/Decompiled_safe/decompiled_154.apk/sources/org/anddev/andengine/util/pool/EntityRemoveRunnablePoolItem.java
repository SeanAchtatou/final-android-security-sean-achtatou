package org.anddev.andengine.util.pool;

import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.layer.ILayer;

public class EntityRemoveRunnablePoolItem extends RunnablePoolItem {
    protected IEntity mEntity;
    protected ILayer mLayer;

    public void setEntity(IEntity pEntity) {
        this.mEntity = pEntity;
    }

    public void setLayer(ILayer pLayer) {
        this.mLayer = pLayer;
    }

    public void set(IEntity pEntity, ILayer pLayer) {
        this.mEntity = pEntity;
        this.mLayer = pLayer;
    }

    public void run() {
        this.mLayer.removeEntity(this.mEntity);
    }
}
