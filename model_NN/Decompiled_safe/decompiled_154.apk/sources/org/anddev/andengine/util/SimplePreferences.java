package org.anddev.andengine.util;

import android.content.Context;
import android.content.SharedPreferences;
import org.anddev.andengine.util.constants.Constants;

public class SimplePreferences implements Constants {
    private static SharedPreferences.Editor EDITORINSTANCE;
    private static SharedPreferences INSTANCE;
    private static final String PREFERENCES_NAME = null;

    public static SharedPreferences getInstance(Context ctx) {
        if (INSTANCE == null) {
            INSTANCE = ctx.getSharedPreferences(PREFERENCES_NAME, 0);
        }
        return INSTANCE;
    }

    public static SharedPreferences.Editor getEditorInstance(Context ctx) {
        if (EDITORINSTANCE == null) {
            EDITORINSTANCE = getInstance(ctx).edit();
        }
        return EDITORINSTANCE;
    }

    public static boolean isFirstAccess(Context pCtx, String pKey) {
        return isFirstAccess(pCtx, pKey, true);
    }

    public static boolean isFirstAccess(Context pCtx, String pKey, boolean pIncrement) {
        return isXthAccess(pCtx, pKey, 0);
    }

    public static boolean isXthAccess(Context pCtx, String pKey, int pXthAccess) {
        return isXthAccess(pCtx, pKey, pXthAccess, true);
    }

    public static boolean isXthAccess(Context pCtx, String pKey, int pXthAccess, boolean pIncrement) {
        SharedPreferences prefs = getInstance(pCtx);
        int xthTime = prefs.getInt(pKey, 0);
        if (pIncrement) {
            prefs.edit().putInt(pKey, xthTime + 1).commit();
        }
        return xthTime == pXthAccess;
    }
}
