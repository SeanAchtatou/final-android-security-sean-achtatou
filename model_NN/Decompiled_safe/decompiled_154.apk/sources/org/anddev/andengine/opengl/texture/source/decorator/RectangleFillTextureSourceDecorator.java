package org.anddev.andengine.opengl.texture.source.decorator;

import android.graphics.Canvas;
import org.anddev.andengine.opengl.texture.source.ITextureSource;

public class RectangleFillTextureSourceDecorator extends FillTextureSourceDecorator {
    public RectangleFillTextureSourceDecorator(ITextureSource pTextureSource, int pFillColor) {
        super(pTextureSource, pFillColor);
    }

    public RectangleFillTextureSourceDecorator clone() {
        return new RectangleFillTextureSourceDecorator(this.mTextureSource, this.mFillColor);
    }

    /* access modifiers changed from: protected */
    public void onDecorateBitmap(Canvas pCanvas) {
        pCanvas.drawRect(0.0f, 0.0f, (float) (pCanvas.getWidth() - 1), (float) (pCanvas.getHeight() - 1), this.mPaint);
    }
}
