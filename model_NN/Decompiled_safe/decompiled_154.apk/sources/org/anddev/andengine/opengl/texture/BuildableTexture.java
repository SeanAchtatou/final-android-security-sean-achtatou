package org.anddev.andengine.opengl.texture;

import android.graphics.Bitmap;
import java.util.ArrayList;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.builder.ITextureBuilder;
import org.anddev.andengine.opengl.texture.source.ITextureSource;
import org.anddev.andengine.util.Callback;

public class BuildableTexture extends Texture {
    private final ArrayList<TextureSourceWithWithLocationCallback> mTextureSourcesToPlace = new ArrayList<>();

    public BuildableTexture(int pWidth, int pHeight) {
        super(pWidth, pHeight, TextureOptions.DEFAULT, null);
    }

    public BuildableTexture(int pWidth, int pHeight, Texture.ITextureStateListener pTextureStateListener) {
        super(pWidth, pHeight, TextureOptions.DEFAULT, pTextureStateListener);
    }

    public BuildableTexture(int pWidth, int pHeight, TextureOptions pTextureOptions) throws IllegalArgumentException {
        super(pWidth, pHeight, pTextureOptions, null);
    }

    public BuildableTexture(int pWidth, int pHeight, TextureOptions pTextureOptions, Texture.ITextureStateListener pTextureStateListener) throws IllegalArgumentException {
        super(pWidth, pHeight, pTextureOptions, pTextureStateListener);
    }

    @Deprecated
    public Texture.TextureSourceWithLocation addTextureSource(ITextureSource pTextureSource, int pTexturePositionX, int pTexturePositionY) {
        return super.addTextureSource(pTextureSource, pTexturePositionX, pTexturePositionY);
    }

    public void clearTextureSources() {
        super.clearTextureSources();
        this.mTextureSourcesToPlace.clear();
    }

    public void addTextureSource(ITextureSource pTextureSource, Callback<Texture.TextureSourceWithLocation> pCallback) {
        this.mTextureSourcesToPlace.add(new TextureSourceWithWithLocationCallback(pTextureSource, pCallback));
    }

    public void removeTextureSource(ITextureSource pTextureSource) {
        ArrayList<TextureSourceWithWithLocationCallback> textureSources = this.mTextureSourcesToPlace;
        for (int i = textureSources.size() - 1; i >= 0; i--) {
            if (textureSources.get(i).mTextureSource == pTextureSource) {
                textureSources.remove(i);
                this.mUpdateOnHardwareNeeded = true;
                return;
            }
        }
    }

    public void build(ITextureBuilder pTextureSourcePackingAlgorithm) throws ITextureBuilder.TextureSourcePackingException {
        pTextureSourcePackingAlgorithm.pack(this, this.mTextureSourcesToPlace);
        this.mTextureSourcesToPlace.clear();
        this.mUpdateOnHardwareNeeded = true;
    }

    public static class TextureSourceWithWithLocationCallback implements ITextureSource {
        private final Callback<Texture.TextureSourceWithLocation> mCallback;
        /* access modifiers changed from: private */
        public final ITextureSource mTextureSource;

        public TextureSourceWithWithLocationCallback(ITextureSource pTextureSource, Callback<Texture.TextureSourceWithLocation> pCallback) {
            this.mTextureSource = pTextureSource;
            this.mCallback = pCallback;
        }

        public TextureSourceWithWithLocationCallback clone() {
            return null;
        }

        public Callback<Texture.TextureSourceWithLocation> getCallback() {
            return this.mCallback;
        }

        public ITextureSource getTextureSource() {
            return this.mTextureSource;
        }

        public int getWidth() {
            return this.mTextureSource.getWidth();
        }

        public int getHeight() {
            return this.mTextureSource.getHeight();
        }

        public Bitmap onLoadBitmap() {
            return this.mTextureSource.onLoadBitmap();
        }

        public String toString() {
            return this.mTextureSource.toString();
        }
    }
}
