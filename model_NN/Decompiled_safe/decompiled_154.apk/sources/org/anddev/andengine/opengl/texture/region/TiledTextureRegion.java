package org.anddev.andengine.opengl.texture.region;

import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.region.buffer.TiledTextureRegionBuffer;

public class TiledTextureRegion extends BaseTextureRegion {
    private int mCurrentTileColumn = 0;
    private int mCurrentTileRow = 0;
    private final int mTileColumns;
    private final int mTileCount = (this.mTileColumns * this.mTileRows);
    private final int mTileRows;

    public TiledTextureRegion(Texture pTexture, int pTexturePositionX, int pTexturePositionY, int pWidth, int pHeight, int pTileColumns, int pTileRows) {
        super(pTexture, pTexturePositionX, pTexturePositionY, pWidth, pHeight);
        this.mTileColumns = pTileColumns;
        this.mTileRows = pTileRows;
        initTextureBuffer();
    }

    /* access modifiers changed from: protected */
    public void initTextureBuffer() {
        if (this.mTileRows != 0 && this.mTileColumns != 0) {
            super.initTextureBuffer();
        }
    }

    public TiledTextureRegionBuffer getTextureBuffer() {
        return (TiledTextureRegionBuffer) this.mTextureRegionBuffer;
    }

    public int getTileCount() {
        return this.mTileCount;
    }

    public int getTileWidth() {
        return super.getWidth() / this.mTileColumns;
    }

    public int getTileHeight() {
        return super.getHeight() / this.mTileRows;
    }

    public int getCurrentTileColumn() {
        return this.mCurrentTileColumn;
    }

    public int getCurrentTileRow() {
        return this.mCurrentTileRow;
    }

    public int getCurrentTileIndex() {
        return (this.mCurrentTileRow * this.mTileColumns) + this.mCurrentTileColumn;
    }

    public void setCurrentTileIndex(int pTileColumn, int pTileRow) {
        if (pTileColumn != this.mCurrentTileColumn || pTileRow != this.mCurrentTileRow) {
            this.mCurrentTileColumn = pTileColumn;
            this.mCurrentTileRow = pTileRow;
            super.updateTextureRegionBuffer();
        }
    }

    public void setCurrentTileIndex(int pTileIndex) {
        if (pTileIndex < this.mTileCount) {
            int tileColumns = this.mTileColumns;
            setCurrentTileIndex(pTileIndex % tileColumns, pTileIndex / tileColumns);
        }
    }

    public float getTexturePositionOfCurrentTileX() {
        return (float) (super.getTexturePositionX() + (this.mCurrentTileColumn * getTileWidth()));
    }

    public float getTexturePositionOfCurrentTileY() {
        return (float) (super.getTexturePositionY() + (this.mCurrentTileRow * getTileHeight()));
    }

    public TiledTextureRegion clone() {
        TiledTextureRegion clone = new TiledTextureRegion(this.mTexture, getTexturePositionX(), getTexturePositionY(), getWidth(), getHeight(), this.mTileColumns, this.mTileRows);
        clone.setCurrentTileIndex(this.mCurrentTileColumn, this.mCurrentTileRow);
        return clone;
    }

    /* access modifiers changed from: protected */
    public TiledTextureRegionBuffer onCreateTextureRegionBuffer() {
        return new TiledTextureRegionBuffer(this, 35044);
    }

    public void nextTile() {
        setCurrentTileIndex((getCurrentTileIndex() + 1) % getTileCount());
    }
}
