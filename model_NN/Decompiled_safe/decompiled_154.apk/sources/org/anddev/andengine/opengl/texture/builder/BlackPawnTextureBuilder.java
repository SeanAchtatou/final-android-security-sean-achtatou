package org.anddev.andengine.opengl.texture.builder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import org.anddev.andengine.entity.layer.tiled.tmx.util.constants.TMXConstants;
import org.anddev.andengine.opengl.texture.BuildableTexture;
import org.anddev.andengine.opengl.texture.source.ITextureSource;

public class BlackPawnTextureBuilder implements ITextureBuilder {
    private static final Comparator<ITextureSource> TEXTURESOURCE_COMPARATOR = new Comparator<ITextureSource>() {
        public int compare(ITextureSource pTextureSourceA, ITextureSource pTextureSourceB) {
            int deltaWidth = pTextureSourceB.getWidth() - pTextureSourceA.getWidth();
            if (deltaWidth != 0) {
                return deltaWidth;
            }
            return pTextureSourceB.getHeight() - pTextureSourceA.getHeight();
        }
    };
    private final int mTextureSourceSpacing;

    public BlackPawnTextureBuilder(int pTextureSourceSpacing) {
        this.mTextureSourceSpacing = pTextureSourceSpacing;
    }

    public void pack(BuildableTexture pBuildableTexture, ArrayList<BuildableTexture.TextureSourceWithWithLocationCallback> pTextureSourcesWithLocationCallback) throws IllegalArgumentException {
        Collections.sort(pTextureSourcesWithLocationCallback, TEXTURESOURCE_COMPARATOR);
        Node root = new Node(new Rect(0, 0, pBuildableTexture.getWidth(), pBuildableTexture.getHeight()));
        int textureSourceCount = pTextureSourcesWithLocationCallback.size();
        for (int i = 0; i < textureSourceCount; i++) {
            BuildableTexture.TextureSourceWithWithLocationCallback textureSourceWithLocationCallback = pTextureSourcesWithLocationCallback.get(i);
            ITextureSource textureSource = textureSourceWithLocationCallback.getTextureSource();
            Node inserted = root.insert(textureSource, pBuildableTexture.getWidth(), pBuildableTexture.getHeight(), this.mTextureSourceSpacing);
            if (inserted == null) {
                throw new IllegalArgumentException("Could not pack: " + textureSource.toString());
            }
            textureSourceWithLocationCallback.getCallback().onCallback(pBuildableTexture.addTextureSource(textureSource, inserted.mRect.mLeft, inserted.mRect.mTop));
        }
    }

    protected static class Rect {
        private final int mHeight;
        /* access modifiers changed from: private */
        public final int mLeft;
        /* access modifiers changed from: private */
        public final int mTop;
        private final int mWidth;

        public Rect(int pLeft, int pTop, int pWidth, int pHeight) {
            this.mLeft = pLeft;
            this.mTop = pTop;
            this.mWidth = pWidth;
            this.mHeight = pHeight;
        }

        public int getWidth() {
            return this.mWidth;
        }

        public int getHeight() {
            return this.mHeight;
        }

        public int getLeft() {
            return this.mLeft;
        }

        public int getTop() {
            return this.mTop;
        }

        public int getRight() {
            return this.mLeft + this.mWidth;
        }

        public int getBottom() {
            return this.mTop + this.mHeight;
        }

        public String toString() {
            return "@: " + this.mLeft + "/" + this.mTop + " * " + this.mWidth + TMXConstants.TAG_OBJECT_ATTRIBUTE_X + this.mHeight;
        }
    }

    protected static class Node {
        private Node mChildA;
        private Node mChildB;
        /* access modifiers changed from: private */
        public final Rect mRect;
        private ITextureSource mTextureSource;

        public Node(int pLeft, int pTop, int pWidth, int pHeight) {
            this(new Rect(pLeft, pTop, pWidth, pHeight));
        }

        public Node(Rect pRect) {
            this.mRect = pRect;
        }

        public Rect getRect() {
            return this.mRect;
        }

        public Node getChildA() {
            return this.mChildA;
        }

        public Node getChildB() {
            return this.mChildB;
        }

        public Node insert(ITextureSource pTextureSource, int pTextureWidth, int pTextureHeight, int pTextureSpacing) throws IllegalArgumentException {
            if (this.mChildA != null && this.mChildB != null) {
                Node newNode = this.mChildA.insert(pTextureSource, pTextureWidth, pTextureHeight, pTextureSpacing);
                if (newNode != null) {
                    return newNode;
                }
                return this.mChildB.insert(pTextureSource, pTextureWidth, pTextureHeight, pTextureSpacing);
            } else if (this.mTextureSource != null) {
                return null;
            } else {
                int textureSourceWidth = pTextureSource.getWidth();
                int textureSourceHeight = pTextureSource.getHeight();
                int rectWidth = this.mRect.getWidth();
                int rectHeight = this.mRect.getHeight();
                if (textureSourceWidth > rectWidth || textureSourceHeight > rectHeight) {
                    return null;
                }
                int textureSourceWidthWithSpacing = textureSourceWidth + pTextureSpacing;
                int textureSourceHeightWithSpacing = textureSourceHeight + pTextureSpacing;
                int rectLeft = this.mRect.getLeft();
                boolean fitToBottomWithoutSpacing = textureSourceHeight == rectHeight && this.mRect.getTop() + textureSourceHeight == pTextureHeight;
                boolean fitToRightWithoutSpacing = textureSourceWidth == rectWidth && rectLeft + textureSourceWidth == pTextureWidth;
                if (textureSourceWidthWithSpacing == rectWidth) {
                    if (textureSourceHeightWithSpacing == rectHeight) {
                        this.mTextureSource = pTextureSource;
                        return this;
                    } else if (fitToBottomWithoutSpacing) {
                        this.mTextureSource = pTextureSource;
                        return this;
                    }
                }
                if (fitToRightWithoutSpacing) {
                    if (textureSourceHeightWithSpacing == rectHeight) {
                        this.mTextureSource = pTextureSource;
                        return this;
                    } else if (fitToBottomWithoutSpacing) {
                        this.mTextureSource = pTextureSource;
                        return this;
                    } else if (textureSourceHeightWithSpacing > rectHeight) {
                        return null;
                    } else {
                        return createChildren(pTextureSource, pTextureWidth, pTextureHeight, pTextureSpacing, rectWidth - textureSourceWidth, rectHeight - textureSourceHeightWithSpacing);
                    }
                } else if (fitToBottomWithoutSpacing) {
                    if (textureSourceWidthWithSpacing == rectWidth) {
                        this.mTextureSource = pTextureSource;
                        return this;
                    } else if (textureSourceWidthWithSpacing > rectWidth) {
                        return null;
                    } else {
                        return createChildren(pTextureSource, pTextureWidth, pTextureHeight, pTextureSpacing, rectWidth - textureSourceWidthWithSpacing, rectHeight - textureSourceHeight);
                    }
                } else if (textureSourceWidthWithSpacing > rectWidth || textureSourceHeightWithSpacing > rectHeight) {
                    return null;
                } else {
                    return createChildren(pTextureSource, pTextureWidth, pTextureHeight, pTextureSpacing, rectWidth - textureSourceWidthWithSpacing, rectHeight - textureSourceHeightWithSpacing);
                }
            }
        }

        private Node createChildren(ITextureSource pTextureSource, int pTextureWidth, int pTextureHeight, int pTextureSpacing, int pDeltaWidth, int pDeltaHeight) {
            Rect rect = this.mRect;
            if (pDeltaWidth >= pDeltaHeight) {
                this.mChildA = new Node(rect.getLeft(), rect.getTop(), pTextureSource.getWidth() + pTextureSpacing, rect.getHeight());
                this.mChildB = new Node(rect.getLeft() + pTextureSource.getWidth() + pTextureSpacing, rect.getTop(), rect.getWidth() - (pTextureSource.getWidth() + pTextureSpacing), rect.getHeight());
            } else {
                this.mChildA = new Node(rect.getLeft(), rect.getTop(), rect.getWidth(), pTextureSource.getHeight() + pTextureSpacing);
                this.mChildB = new Node(rect.getLeft(), rect.getTop() + pTextureSource.getHeight() + pTextureSpacing, rect.getWidth(), rect.getHeight() - (pTextureSource.getHeight() + pTextureSpacing));
            }
            return this.mChildA.insert(pTextureSource, pTextureWidth, pTextureHeight, pTextureSpacing);
        }
    }
}
