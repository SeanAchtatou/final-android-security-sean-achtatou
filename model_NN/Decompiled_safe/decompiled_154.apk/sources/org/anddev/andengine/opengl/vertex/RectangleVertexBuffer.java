package org.anddev.andengine.opengl.vertex;

import org.anddev.andengine.opengl.util.FastFloatBuffer;

public class RectangleVertexBuffer extends VertexBuffer {
    public static final int VERTICES_PER_RECTANGLE = 4;

    public RectangleVertexBuffer(int pDrawType) {
        super(8, pDrawType);
    }

    public synchronized void update(float pX, float pY, float pWidth, float pHeight) {
        int x = Float.floatToRawIntBits(pX);
        int y = Float.floatToRawIntBits(pY);
        int x2 = Float.floatToRawIntBits(pX + pWidth);
        int y2 = Float.floatToRawIntBits(pY + pHeight);
        int[] bufferData = this.mBufferData;
        bufferData[0] = x;
        bufferData[1] = y;
        bufferData[2] = x;
        bufferData[3] = y2;
        bufferData[4] = x2;
        bufferData[5] = y;
        bufferData[6] = x2;
        bufferData[7] = y2;
        FastFloatBuffer buffer = getFloatBuffer();
        buffer.position(0);
        buffer.put(bufferData);
        buffer.position(0);
        super.setHardwareBufferNeedsUpdate();
    }
}
