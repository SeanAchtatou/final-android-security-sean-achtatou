package org.anddev.andengine.opengl.texture;

import android.graphics.Bitmap;
import android.opengl.GLUtils;
import java.util.ArrayList;
import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.opengl.texture.source.ITextureSource;
import org.anddev.andengine.opengl.util.GLHelper;
import org.anddev.andengine.util.Debug;
import org.anddev.andengine.util.MathUtils;

public class Texture {
    private static final int[] HARDWARETEXTUREID_FETCHER = new int[1];
    private int mHardwareTextureID;
    private final int mHeight;
    private boolean mLoadedToHardware;
    private final TextureOptions mTextureOptions;
    private final ArrayList<TextureSourceWithLocation> mTextureSources;
    private final ITextureStateListener mTextureStateListener;
    protected boolean mUpdateOnHardwareNeeded;
    private final int mWidth;

    public Texture(int pWidth, int pHeight) {
        this(pWidth, pHeight, TextureOptions.DEFAULT, null);
    }

    public Texture(int pWidth, int pHeight, ITextureStateListener pTextureStateListener) {
        this(pWidth, pHeight, TextureOptions.DEFAULT, pTextureStateListener);
    }

    public Texture(int pWidth, int pHeight, TextureOptions pTextureOptions) throws IllegalArgumentException {
        this(pWidth, pHeight, pTextureOptions, null);
    }

    public Texture(int pWidth, int pHeight, TextureOptions pTextureOptions, ITextureStateListener pTextureStateListener) throws IllegalArgumentException {
        this.mHardwareTextureID = -1;
        this.mTextureSources = new ArrayList<>();
        this.mUpdateOnHardwareNeeded = false;
        if (!MathUtils.isPowerOfTwo(pWidth) || !MathUtils.isPowerOfTwo(pHeight)) {
            throw new IllegalArgumentException("Width and Height of a Texture must be a power of 2!");
        }
        this.mWidth = pWidth;
        this.mHeight = pHeight;
        this.mTextureOptions = pTextureOptions;
        this.mTextureStateListener = pTextureStateListener;
    }

    public int getHardwareTextureID() {
        return this.mHardwareTextureID;
    }

    public boolean isLoadedToHardware() {
        return this.mLoadedToHardware;
    }

    public boolean isUpdateOnHardwareNeeded() {
        return this.mUpdateOnHardwareNeeded;
    }

    /* access modifiers changed from: package-private */
    public void setLoadedToHardware(boolean pLoadedToHardware) {
        this.mLoadedToHardware = pLoadedToHardware;
    }

    public int getWidth() {
        return this.mWidth;
    }

    public int getHeight() {
        return this.mHeight;
    }

    public TextureOptions getTextureOptions() {
        return this.mTextureOptions;
    }

    public TextureSourceWithLocation addTextureSource(ITextureSource pTextureSource, int pTexturePositionX, int pTexturePositionY) throws IllegalArgumentException {
        checkTextureSourcePosition(pTextureSource, pTexturePositionX, pTexturePositionY);
        TextureSourceWithLocation textureSourceWithLocation = new TextureSourceWithLocation(pTextureSource, pTexturePositionX, pTexturePositionY);
        this.mTextureSources.add(textureSourceWithLocation);
        this.mUpdateOnHardwareNeeded = true;
        return textureSourceWithLocation;
    }

    private void checkTextureSourcePosition(ITextureSource pTextureSource, int pTexturePositionX, int pTexturePositionY) throws IllegalArgumentException {
        if (pTexturePositionX < 0) {
            throw new IllegalArgumentException("Illegal negative pTexturePositionX supplied: '" + pTexturePositionX + "'");
        } else if (pTexturePositionY < 0) {
            throw new IllegalArgumentException("Illegal negative pTexturePositionY supplied: '" + pTexturePositionY + "'");
        } else if (pTextureSource.getWidth() + pTexturePositionX > this.mWidth || pTextureSource.getHeight() + pTexturePositionY > this.mHeight) {
            throw new IllegalArgumentException("Supplied TextureSource must not exceed bounds of Texture.");
        }
    }

    public void removeTextureSource(ITextureSource pTextureSource, int pTexturePositionX, int pTexturePositionY) {
        ArrayList<TextureSourceWithLocation> textureSources = this.mTextureSources;
        for (int i = textureSources.size() - 1; i >= 0; i--) {
            TextureSourceWithLocation textureSourceWithLocation = textureSources.get(i);
            if (textureSourceWithLocation.mTextureSource == pTextureSource && textureSourceWithLocation.mTexturePositionX == pTexturePositionX && textureSourceWithLocation.mTexturePositionY == pTexturePositionY) {
                textureSources.remove(i);
                this.mUpdateOnHardwareNeeded = true;
                return;
            }
        }
    }

    public void clearTextureSources() {
        this.mTextureSources.clear();
        this.mUpdateOnHardwareNeeded = true;
    }

    public void loadToHardware(GL10 pGL) {
        GLHelper.enableTextures(pGL);
        this.mHardwareTextureID = generateHardwareTextureID(pGL);
        allocateAndBindTextureOnHardware(pGL);
        applyTextureOptions(pGL);
        writeTextureToHardware(pGL);
        this.mUpdateOnHardwareNeeded = false;
        this.mLoadedToHardware = true;
        if (this.mTextureStateListener != null) {
            this.mTextureStateListener.onLoadedToHardware(this);
        }
    }

    public void unloadFromHardware(GL10 pGL) {
        GLHelper.enableTextures(pGL);
        deleteTextureOnHardware(pGL);
        this.mHardwareTextureID = -1;
        this.mLoadedToHardware = false;
        if (this.mTextureStateListener != null) {
            this.mTextureStateListener.onUnloadedFromHardware(this);
        }
    }

    private void writeTextureToHardware(GL10 pGL) {
        boolean preMultipyAlpha = this.mTextureOptions.mPreMultipyAlpha;
        ArrayList<TextureSourceWithLocation> textureSources = this.mTextureSources;
        int textureSourceCount = textureSources.size();
        for (int j = 0; j < textureSourceCount; j++) {
            TextureSourceWithLocation textureSourceWithLocation = textureSources.get(j);
            if (textureSourceWithLocation != null) {
                Bitmap bmp = textureSourceWithLocation.onLoadBitmap();
                if (bmp == null) {
                    try {
                        throw new IllegalArgumentException("TextureSource: " + textureSourceWithLocation.toString() + " returned a null Bitmap.");
                    } catch (IllegalArgumentException e) {
                        IllegalArgumentException iae = e;
                        Debug.e("Error loading: " + textureSourceWithLocation.toString(), iae);
                        if (this.mTextureStateListener != null) {
                            this.mTextureStateListener.onTextureSourceLoadExeption(this, textureSourceWithLocation.mTextureSource, iae);
                        } else {
                            throw iae;
                        }
                    }
                } else {
                    if (preMultipyAlpha) {
                        GLUtils.texSubImage2D(3553, 0, textureSourceWithLocation.getTexturePositionX(), textureSourceWithLocation.getTexturePositionY(), bmp, 6408, 5121);
                    } else {
                        GLHelper.glTexSubImage2D(pGL, 3553, 0, textureSourceWithLocation.getTexturePositionX(), textureSourceWithLocation.getTexturePositionY(), bmp, 6408, 5121);
                    }
                    bmp.recycle();
                }
            }
        }
    }

    private void applyTextureOptions(GL10 pGL) {
        TextureOptions textureOptions = this.mTextureOptions;
        pGL.glTexParameterf(3553, 10241, (float) textureOptions.mMinFilter);
        pGL.glTexParameterf(3553, 10240, (float) textureOptions.mMagFilter);
        pGL.glTexParameterf(3553, 10242, textureOptions.mWrapS);
        pGL.glTexParameterf(3553, 10243, textureOptions.mWrapT);
        pGL.glTexEnvf(8960, 8704, (float) textureOptions.mTextureEnvironment);
    }

    private void allocateAndBindTextureOnHardware(GL10 pGL) {
        GLHelper.bindTexture(pGL, this.mHardwareTextureID);
        sendPlaceholderBitmapToHardware(this.mWidth, this.mHeight);
    }

    private void deleteTextureOnHardware(GL10 pGL) {
        GLHelper.deleteTexture(pGL, this.mHardwareTextureID);
    }

    private static int generateHardwareTextureID(GL10 pGL) {
        pGL.glGenTextures(1, HARDWARETEXTUREID_FETCHER, 0);
        return HARDWARETEXTUREID_FETCHER[0];
    }

    private static void sendPlaceholderBitmapToHardware(int pWidth, int pHeight) {
        Bitmap textureBitmap = Bitmap.createBitmap(pWidth, pHeight, Bitmap.Config.ARGB_8888);
        GLUtils.texImage2D(3553, 0, textureBitmap, 0);
        textureBitmap.recycle();
    }

    public interface ITextureStateListener {
        void onLoadedToHardware(Texture texture);

        void onTextureSourceLoadExeption(Texture texture, ITextureSource iTextureSource, Throwable th);

        void onUnloadedFromHardware(Texture texture);

        public static class TextureStateAdapter implements ITextureStateListener {
            public void onLoadedToHardware(Texture pTexture) {
            }

            public void onTextureSourceLoadExeption(Texture pTexture, ITextureSource pTextureSource, Throwable pThrowable) {
            }

            public void onUnloadedFromHardware(Texture pTexture) {
            }
        }

        public static class DebugTextureStateListener implements ITextureStateListener {
            public void onLoadedToHardware(Texture pTexture) {
                Debug.d("Texture loaded: " + pTexture.toString());
            }

            public void onTextureSourceLoadExeption(Texture pTexture, ITextureSource pTextureSource, Throwable pThrowable) {
                Debug.e("Exception loading TextureSource. Texture: " + pTexture.toString() + " TextureSource: " + pTextureSource.toString(), pThrowable);
            }

            public void onUnloadedFromHardware(Texture pTexture) {
                Debug.d("Texture unloaded: " + pTexture.toString());
            }
        }
    }

    public static class TextureSourceWithLocation {
        /* access modifiers changed from: private */
        public final int mTexturePositionX;
        /* access modifiers changed from: private */
        public final int mTexturePositionY;
        /* access modifiers changed from: private */
        public final ITextureSource mTextureSource;

        public TextureSourceWithLocation(ITextureSource pTextureSource, int pTexturePositionX, int pTexturePositionY) {
            this.mTextureSource = pTextureSource;
            this.mTexturePositionX = pTexturePositionX;
            this.mTexturePositionY = pTexturePositionY;
        }

        public int getTexturePositionX() {
            return this.mTexturePositionX;
        }

        public int getTexturePositionY() {
            return this.mTexturePositionY;
        }

        public int getWidth() {
            return this.mTextureSource.getWidth();
        }

        public int getHeight() {
            return this.mTextureSource.getHeight();
        }

        public Bitmap onLoadBitmap() {
            return this.mTextureSource.onLoadBitmap();
        }

        public String toString() {
            return this.mTextureSource.toString();
        }
    }
}
