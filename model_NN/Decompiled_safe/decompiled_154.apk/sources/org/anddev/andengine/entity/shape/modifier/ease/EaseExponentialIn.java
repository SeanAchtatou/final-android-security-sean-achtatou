package org.anddev.andengine.entity.shape.modifier.ease;

public class EaseExponentialIn implements IEaseFunction {
    private static EaseExponentialIn INSTANCE;

    private EaseExponentialIn() {
    }

    public static EaseExponentialIn getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseExponentialIn();
        }
        return INSTANCE;
    }

    public float getPercentageDone(float pSecondsElapsed, float pDuration, float pMinValue, float pMaxValue) {
        return (float) (pSecondsElapsed == 0.0f ? (double) pMinValue : ((((double) pMaxValue) * Math.pow(2.0d, (double) (10.0f * ((pSecondsElapsed / pDuration) - 1.0f)))) + ((double) pMinValue)) - ((double) (0.001f * pMaxValue)));
    }
}
