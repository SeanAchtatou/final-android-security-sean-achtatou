package org.anddev.andengine.entity.shape.modifier;

import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.entity.shape.modifier.IShapeModifier;

public class DelayModifier extends DurationShapeModifier {
    public DelayModifier(float pDuration, IShapeModifier.IShapeModifierListener pShapeModiferListener) {
        super(pDuration, pShapeModiferListener);
    }

    public DelayModifier(float pDuration) {
        super(pDuration);
    }

    protected DelayModifier(DelayModifier pDelayModifier) {
        super(pDelayModifier);
    }

    public DelayModifier clone() {
        return new DelayModifier(this);
    }

    /* access modifiers changed from: protected */
    public void onManagedInitialize(IShape pShape) {
    }

    /* access modifiers changed from: protected */
    public void onManagedUpdate(float pSecondsElapsed, IShape pShape) {
    }
}
