package org.anddev.andengine.entity.shape;

import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.util.modifier.IModifier;

public interface IShape extends IEntity, Scene.ITouchArea {
    void accelerate(float f, float f2);

    void addShapeModifier(IModifier<IShape> iModifier);

    void clearShapeModifiers();

    boolean collidesWith(IShape iShape);

    float getAccelerationX();

    float getAccelerationY();

    float getAlpha();

    float getAngularVelocity();

    float getBaseHeight();

    float getBaseWidth();

    float getBaseX();

    float getBaseY();

    float getBlue();

    float getGreen();

    float getHeight();

    float getHeightScaled();

    float getRed();

    float getRotation();

    float getRotationCenterX();

    float getRotationCenterY();

    float getScaleCenterX();

    float getScaleCenterY();

    float getScaleX();

    float getScaleY();

    float[] getSceneCenterCoordinates();

    float getVelocityX();

    float getVelocityY();

    float getWidth();

    float getWidthScaled();

    float getX();

    float getY();

    boolean isCullingEnabled();

    boolean isScaled();

    boolean isUpdatePhysics();

    boolean removeShapeModifier(IModifier<IShape> iModifier);

    void setAcceleration(float f);

    void setAcceleration(float f, float f2);

    void setAccelerationX(float f);

    void setAccelerationY(float f);

    void setAlpha(float f);

    void setAngularVelocity(float f);

    void setBasePosition();

    void setBlendFunction(int i, int i2);

    void setColor(float f, float f2, float f3);

    void setColor(float f, float f2, float f3, float f4);

    void setCullingEnabled(boolean z);

    void setPosition(float f, float f2);

    void setPosition(IShape iShape);

    void setRotation(float f);

    void setRotationCenter(float f, float f2);

    void setRotationCenterX(float f);

    void setRotationCenterY(float f);

    void setScale(float f);

    void setScale(float f, float f2);

    void setScaleCenter(float f, float f2);

    void setScaleCenterX(float f);

    void setScaleCenterY(float f);

    void setScaleX(float f);

    void setScaleY(float f);

    void setUpdatePhysics(boolean z);

    void setVelocity(float f);

    void setVelocity(float f, float f2);

    void setVelocityX(float f);

    void setVelocityY(float f);
}
