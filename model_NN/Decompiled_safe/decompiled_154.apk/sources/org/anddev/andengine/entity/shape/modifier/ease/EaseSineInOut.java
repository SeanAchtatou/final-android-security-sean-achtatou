package org.anddev.andengine.entity.shape.modifier.ease;

import android.util.FloatMath;

public class EaseSineInOut implements IEaseFunction {
    private static EaseSineInOut INSTANCE;

    private EaseSineInOut() {
    }

    public static EaseSineInOut getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseSineInOut();
        }
        return INSTANCE;
    }

    public float getPercentageDone(float pSecondsElapsed, float pDuration, float pMinValue, float pMaxValue) {
        return ((-pMaxValue) * 0.5f * (FloatMath.cos((3.1415927f * pSecondsElapsed) / pDuration) - 1.0f)) + pMinValue;
    }
}
