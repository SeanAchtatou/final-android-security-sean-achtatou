package org.anddev.andengine.entity.shape.modifier.ease;

import android.util.FloatMath;

public class EaseCircularIn implements IEaseFunction {
    private static EaseCircularIn INSTANCE;

    private EaseCircularIn() {
    }

    public static EaseCircularIn getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseCircularIn();
        }
        return INSTANCE;
    }

    public float getPercentageDone(float pSecondsElapsed, float pDuration, float pMinValue, float pMaxValue) {
        float pSecondsElapsed2 = pSecondsElapsed / pDuration;
        return ((-pMaxValue) * (FloatMath.sqrt(1.0f - (pSecondsElapsed2 * pSecondsElapsed2)) - 1.0f)) + pMinValue;
    }
}
