package org.anddev.andengine.entity.shape.modifier;

import org.anddev.andengine.entity.shape.modifier.IShapeModifier;
import org.anddev.andengine.entity.shape.modifier.ease.IEaseFunction;

public class FadeInModifier extends AlphaModifier {
    public FadeInModifier(float pDuration) {
        super(pDuration, 0.0f, 1.0f, IEaseFunction.DEFAULT);
    }

    public FadeInModifier(float pDuration, IEaseFunction pEaseFunction) {
        super(pDuration, 0.0f, 1.0f, pEaseFunction);
    }

    public FadeInModifier(float pDuration, IShapeModifier.IShapeModifierListener pShapeModiferListener) {
        super(pDuration, 0.0f, 1.0f, pShapeModiferListener, IEaseFunction.DEFAULT);
    }

    public FadeInModifier(float pDuration, IShapeModifier.IShapeModifierListener pShapeModiferListener, IEaseFunction pEaseFunction) {
        super(pDuration, 0.0f, 1.0f, pShapeModiferListener, pEaseFunction);
    }

    protected FadeInModifier(FadeInModifier pFadeInModifier) {
        super(pFadeInModifier);
    }

    public FadeInModifier clone() {
        return new FadeInModifier(this);
    }
}
