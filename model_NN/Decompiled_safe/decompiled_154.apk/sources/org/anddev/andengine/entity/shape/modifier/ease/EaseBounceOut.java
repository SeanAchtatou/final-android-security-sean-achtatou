package org.anddev.andengine.entity.shape.modifier.ease;

public class EaseBounceOut implements IEaseFunction {
    private static EaseBounceOut INSTANCE;

    private EaseBounceOut() {
    }

    public static EaseBounceOut getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseBounceOut();
        }
        return INSTANCE;
    }

    public float getPercentageDone(float pSecondsElapsed, float pDuration, float pMinValue, float pMaxValue) {
        float pSecondsElapsed2 = pSecondsElapsed / pDuration;
        if (((double) pSecondsElapsed2) < 0.36363636363636365d) {
            return (7.5625f * pSecondsElapsed2 * pSecondsElapsed2 * pMaxValue) + pMinValue;
        }
        if (((double) pSecondsElapsed2) < 0.7272727272727273d) {
            float pSecondsElapsed3 = pSecondsElapsed2 - 0.54545456f;
            return (((7.5625f * pSecondsElapsed3 * pSecondsElapsed3) + 0.75f) * pMaxValue) + pMinValue;
        } else if (((double) pSecondsElapsed2) < 0.9090909090909091d) {
            float pSecondsElapsed4 = pSecondsElapsed2 - 0.8181818f;
            return (((7.5625f * pSecondsElapsed4 * pSecondsElapsed4) + 0.9375f) * pMaxValue) + pMinValue;
        } else {
            float pSecondsElapsed5 = pSecondsElapsed2 - 0.95454544f;
            return (((7.5625f * pSecondsElapsed5 * pSecondsElapsed5) + 0.984375f) * pMaxValue) + pMinValue;
        }
    }
}
