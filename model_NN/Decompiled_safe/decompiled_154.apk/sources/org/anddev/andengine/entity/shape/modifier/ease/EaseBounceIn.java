package org.anddev.andengine.entity.shape.modifier.ease;

public class EaseBounceIn implements IEaseFunction {
    private static EaseBounceIn INSTANCE;

    private EaseBounceIn() {
    }

    public static EaseBounceIn getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseBounceIn();
        }
        return INSTANCE;
    }

    public float getPercentageDone(float pSecondsElapsed, float pDuration, float pMinValue, float pMaxValue) {
        return (pMaxValue - EaseBounceOut.getInstance().getPercentageDone(pDuration - pSecondsElapsed, pDuration, 0.0f, pMaxValue)) + pMinValue;
    }
}
