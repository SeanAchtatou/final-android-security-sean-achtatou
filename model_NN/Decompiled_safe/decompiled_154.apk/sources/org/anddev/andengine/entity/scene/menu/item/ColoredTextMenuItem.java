package org.anddev.andengine.entity.scene.menu.item;

import org.anddev.andengine.opengl.font.Font;

public class ColoredTextMenuItem extends TextMenuItem {
    private final float mSelectedBlue;
    private final float mSelectedGreen;
    private final float mSelectedRed;
    private final float mUnselectedBlue;
    private final float mUnselectedGreen;
    private final float mUnselectedRed;

    public ColoredTextMenuItem(int pID, Font pFont, String pText, float pSelectedRed, float pSelectedGreen, float pSelectedBlue, float pUnselectedRed, float pUnselectedGreen, float pUnselectedBlue) {
        super(pID, pFont, pText);
        this.mSelectedRed = pSelectedRed;
        this.mSelectedGreen = pSelectedGreen;
        this.mSelectedBlue = pSelectedBlue;
        this.mUnselectedRed = pUnselectedRed;
        this.mUnselectedGreen = pUnselectedGreen;
        this.mUnselectedBlue = pUnselectedBlue;
        setColor(this.mUnselectedRed, this.mUnselectedGreen, this.mUnselectedBlue);
    }

    public void onSelected() {
        setColor(this.mSelectedRed, this.mSelectedGreen, this.mSelectedBlue);
    }

    public void onUnselected() {
        setColor(this.mUnselectedRed, this.mUnselectedGreen, this.mUnselectedBlue);
    }

    public void reset() {
        super.reset();
        setColor(this.mUnselectedRed, this.mUnselectedGreen, this.mUnselectedBlue);
    }
}
