package org.anddev.andengine.entity;

import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.opengl.IDrawable;

public interface IEntity extends IDrawable, IUpdateHandler {
    int getZIndex();

    void setZIndex(int i);
}
