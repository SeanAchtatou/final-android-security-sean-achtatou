package org.anddev.andengine.entity.primitive;

import org.anddev.andengine.entity.shape.RectangularShape;
import org.anddev.andengine.opengl.vertex.RectangleVertexBuffer;

public abstract class BaseRectangle extends RectangularShape {
    public BaseRectangle(float pX, float pY, float pWidth, float pHeight) {
        super(pX, pY, pWidth, pHeight, new RectangleVertexBuffer(35044));
        updateVertexBuffer();
    }

    public BaseRectangle(float pX, float pY, float pWidth, float pHeight, RectangleVertexBuffer pRectangleVertexBuffer) {
        super(pX, pY, pWidth, pHeight, pRectangleVertexBuffer);
    }

    public RectangleVertexBuffer getVertexBuffer() {
        return (RectangleVertexBuffer) super.getVertexBuffer();
    }

    /* access modifiers changed from: protected */
    public void onUpdateVertexBuffer() {
        getVertexBuffer().update(0.0f, 0.0f, this.mWidth, this.mHeight);
    }
}
