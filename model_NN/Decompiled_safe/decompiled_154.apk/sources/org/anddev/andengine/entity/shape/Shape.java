package org.anddev.andengine.entity.shape;

import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.Entity;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.util.GLHelper;
import org.anddev.andengine.util.modifier.IModifier;
import org.anddev.andengine.util.modifier.ModifierList;

public abstract class Shape extends Entity implements IShape {
    public static final int BLENDFUNCTION_DESTINATION_DEFAULT = 771;
    public static final int BLENDFUNCTION_DESTINATION_PREMULTIPLYALPHA_DEFAULT = 771;
    public static final int BLENDFUNCTION_SOURCE_DEFAULT = 770;
    public static final int BLENDFUNCTION_SOURCE_PREMULTIPLYALPHA_DEFAULT = 1;
    protected float mAccelerationX = 0.0f;
    protected float mAccelerationY = 0.0f;
    protected float mAlpha = 1.0f;
    protected float mAngularVelocity = 0.0f;
    private final float mBaseX;
    private final float mBaseY;
    protected float mBlue = 1.0f;
    private boolean mCullingEnabled = false;
    protected int mDestinationBlendFunction = 771;
    protected float mGreen = 1.0f;
    protected float mRed = 1.0f;
    protected float mRotation = 0.0f;
    protected float mRotationCenterX = 0.0f;
    protected float mRotationCenterY = 0.0f;
    protected float mScaleCenterX = 0.0f;
    protected float mScaleCenterY = 0.0f;
    protected float mScaleX = 1.0f;
    protected float mScaleY = 1.0f;
    private final ModifierList<IShape> mShapeModifiers = new ModifierList<>(this);
    protected int mSourceBlendFunction = BLENDFUNCTION_SOURCE_DEFAULT;
    private boolean mUpdatePhysics = true;
    protected float mVelocityX = 0.0f;
    protected float mVelocityY = 0.0f;
    protected float mX;
    protected float mY;

    /* access modifiers changed from: protected */
    public abstract void drawVertices(GL10 gl10, Camera camera);

    /* access modifiers changed from: protected */
    public abstract boolean isCulled(Camera camera);

    /* access modifiers changed from: protected */
    public abstract void onApplyVertices(GL10 gl10);

    public Shape(float pX, float pY) {
        this.mBaseX = pX;
        this.mBaseY = pY;
        this.mX = pX;
        this.mY = pY;
    }

    public float getRed() {
        return this.mRed;
    }

    public float getGreen() {
        return this.mGreen;
    }

    public float getBlue() {
        return this.mBlue;
    }

    public float getAlpha() {
        return this.mAlpha;
    }

    public void setAlpha(float pAlpha) {
        this.mAlpha = pAlpha;
    }

    public void setColor(float pRed, float pGreen, float pBlue) {
        this.mRed = pRed;
        this.mGreen = pGreen;
        this.mBlue = pBlue;
    }

    public void setColor(float pRed, float pGreen, float pBlue, float pAlpha) {
        this.mRed = pRed;
        this.mGreen = pGreen;
        this.mBlue = pBlue;
        this.mAlpha = pAlpha;
    }

    public float getX() {
        return this.mX;
    }

    public float getY() {
        return this.mY;
    }

    public float getBaseX() {
        return this.mBaseX;
    }

    public float getBaseY() {
        return this.mBaseY;
    }

    public void setPosition(IShape pOtherShape) {
        setPosition(pOtherShape.getX(), pOtherShape.getY());
    }

    public void setPosition(float pX, float pY) {
        this.mX = pX;
        this.mY = pY;
        onPositionChanged();
    }

    public void setBasePosition() {
        this.mX = this.mBaseX;
        this.mY = this.mBaseY;
        onPositionChanged();
    }

    public float getVelocityX() {
        return this.mVelocityX;
    }

    public float getVelocityY() {
        return this.mVelocityY;
    }

    public void setVelocityX(float pVelocityX) {
        this.mVelocityX = pVelocityX;
    }

    public void setVelocityY(float pVelocityY) {
        this.mVelocityY = pVelocityY;
    }

    public void setVelocity(float pVelocity) {
        this.mVelocityX = pVelocity;
        this.mVelocityY = pVelocity;
    }

    public void setVelocity(float pVelocityX, float pVelocityY) {
        this.mVelocityX = pVelocityX;
        this.mVelocityY = pVelocityY;
    }

    public float getAccelerationX() {
        return this.mAccelerationX;
    }

    public float getAccelerationY() {
        return this.mAccelerationY;
    }

    public void setAccelerationX(float pAccelerationX) {
        this.mAccelerationX = pAccelerationX;
    }

    public void setAccelerationY(float pAccelerationY) {
        this.mAccelerationY = pAccelerationY;
    }

    public void setAcceleration(float pAccelerationX, float pAccelerationY) {
        this.mAccelerationX = pAccelerationX;
        this.mAccelerationY = pAccelerationY;
    }

    public void setAcceleration(float pAcceleration) {
        this.mAccelerationX = pAcceleration;
        this.mAccelerationY = pAcceleration;
    }

    public void accelerate(float pAccelerationX, float pAccelerationY) {
        this.mAccelerationX += pAccelerationX;
        this.mAccelerationY += pAccelerationY;
    }

    public float getRotation() {
        return this.mRotation;
    }

    public void setRotation(float pRotation) {
        this.mRotation = pRotation;
    }

    public float getAngularVelocity() {
        return this.mAngularVelocity;
    }

    public void setAngularVelocity(float pAngularVelocity) {
        this.mAngularVelocity = pAngularVelocity;
    }

    public float getRotationCenterX() {
        return this.mRotationCenterX;
    }

    public float getRotationCenterY() {
        return this.mRotationCenterY;
    }

    public void setRotationCenterX(float pRotationCenterX) {
        this.mRotationCenterX = pRotationCenterX;
    }

    public void setRotationCenterY(float pRotationCenterY) {
        this.mRotationCenterY = pRotationCenterY;
    }

    public void setRotationCenter(float pRotationCenterX, float pRotationCenterY) {
        this.mRotationCenterX = pRotationCenterX;
        this.mRotationCenterY = pRotationCenterY;
    }

    public boolean isScaled() {
        return (this.mScaleX == 1.0f && this.mScaleY == 1.0f) ? false : true;
    }

    public float getScaleX() {
        return this.mScaleX;
    }

    public float getScaleY() {
        return this.mScaleY;
    }

    public void setScaleX(float pScaleX) {
        this.mScaleX = pScaleX;
    }

    public void setScaleY(float pScaleY) {
        this.mScaleY = pScaleY;
    }

    public void setScale(float pScale) {
        this.mScaleX = pScale;
        this.mScaleY = pScale;
    }

    public void setScale(float pScaleX, float pScaleY) {
        this.mScaleX = pScaleX;
        this.mScaleY = pScaleY;
    }

    public float getScaleCenterX() {
        return this.mScaleCenterX;
    }

    public float getScaleCenterY() {
        return this.mScaleCenterY;
    }

    public void setScaleCenterX(float pScaleCenterX) {
        this.mScaleCenterX = pScaleCenterX;
    }

    public void setScaleCenterY(float pScaleCenterY) {
        this.mScaleCenterY = pScaleCenterY;
    }

    public void setScaleCenter(float pScaleCenterX, float pScaleCenterY) {
        this.mScaleCenterX = pScaleCenterX;
        this.mScaleCenterY = pScaleCenterY;
    }

    public boolean isUpdatePhysics() {
        return this.mUpdatePhysics;
    }

    public void setUpdatePhysics(boolean pUpdatePhysics) {
        this.mUpdatePhysics = pUpdatePhysics;
    }

    public boolean isCullingEnabled() {
        return this.mCullingEnabled;
    }

    public void setCullingEnabled(boolean pCullingEnabled) {
        this.mCullingEnabled = pCullingEnabled;
    }

    public void setBlendFunction(int pSourceBlendFunction, int pDestinationBlendFunction) {
        this.mSourceBlendFunction = pSourceBlendFunction;
        this.mDestinationBlendFunction = pDestinationBlendFunction;
    }

    public float getWidthScaled() {
        return getWidth() * this.mScaleX;
    }

    public float getHeightScaled() {
        return getHeight() * this.mScaleY;
    }

    public void addShapeModifier(IModifier<IShape> pShapeModifier) {
        this.mShapeModifiers.add(pShapeModifier);
    }

    public boolean removeShapeModifier(IModifier<IShape> pShapeModifier) {
        return this.mShapeModifiers.remove(pShapeModifier);
    }

    public void clearShapeModifiers() {
        this.mShapeModifiers.clear();
    }

    public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onPositionChanged() {
    }

    /* access modifiers changed from: protected */
    public void onManagedUpdate(float pSecondsElapsed) {
        if (this.mUpdatePhysics) {
            float accelerationX = this.mAccelerationX;
            float accelerationY = this.mAccelerationY;
            if (!(accelerationX == 0.0f && accelerationY == 0.0f)) {
                this.mVelocityX += accelerationX * pSecondsElapsed;
                this.mVelocityY += accelerationY * pSecondsElapsed;
            }
            float angularVelocity = this.mAngularVelocity;
            if (angularVelocity != 0.0f) {
                this.mRotation += angularVelocity * pSecondsElapsed;
            }
            float velocityX = this.mVelocityX;
            float velocityY = this.mVelocityY;
            if (!(velocityX == 0.0f && velocityY == 0.0f)) {
                this.mX += velocityX * pSecondsElapsed;
                this.mY += velocityY * pSecondsElapsed;
                onPositionChanged();
            }
        }
        this.mShapeModifiers.onUpdate(pSecondsElapsed);
    }

    /* access modifiers changed from: protected */
    public void onManagedDraw(GL10 pGL, Camera pCamera) {
        if (!this.mCullingEnabled || !isCulled(pCamera)) {
            onInitDraw(pGL);
            pGL.glPushMatrix();
            onApplyVertices(pGL);
            onApplyTransformations(pGL);
            drawVertices(pGL, pCamera);
            pGL.glPopMatrix();
        }
    }

    /* access modifiers changed from: protected */
    public void onInitDraw(GL10 pGL) {
        GLHelper.setColor(pGL, this.mRed, this.mGreen, this.mBlue, this.mAlpha);
        GLHelper.enableVertexArray(pGL);
        GLHelper.blendFunction(pGL, this.mSourceBlendFunction, this.mDestinationBlendFunction);
    }

    /* access modifiers changed from: protected */
    public void onApplyTransformations(GL10 pGL) {
        applyTranslation(pGL);
        applyRotation(pGL);
        applyScale(pGL);
    }

    /* access modifiers changed from: protected */
    public void applyTranslation(GL10 pGL) {
        pGL.glTranslatef(this.mX, this.mY, 0.0f);
    }

    /* access modifiers changed from: protected */
    public void applyRotation(GL10 pGL) {
        float rotation = this.mRotation;
        if (rotation != 0.0f) {
            float rotationCenterX = this.mRotationCenterX;
            float rotationCenterY = this.mRotationCenterY;
            pGL.glTranslatef(rotationCenterX, rotationCenterY, 0.0f);
            pGL.glRotatef(rotation, 0.0f, 0.0f, 1.0f);
            pGL.glTranslatef(-rotationCenterX, -rotationCenterY, 0.0f);
        }
    }

    /* access modifiers changed from: protected */
    public void applyScale(GL10 pGL) {
        float scaleX = this.mScaleX;
        float scaleY = this.mScaleY;
        if (scaleX != 1.0f || scaleY != 1.0f) {
            float scaleCenterX = this.mScaleCenterX;
            float scaleCenterY = this.mScaleCenterY;
            pGL.glTranslatef(scaleCenterX, scaleCenterY, 0.0f);
            pGL.glScalef(scaleX, scaleY, 1.0f);
            pGL.glTranslatef(-scaleCenterX, -scaleCenterY, 0.0f);
        }
    }

    public void reset() {
        super.reset();
        this.mX = this.mBaseX;
        this.mY = this.mBaseY;
        this.mAccelerationX = 0.0f;
        this.mAccelerationY = 0.0f;
        this.mVelocityX = 0.0f;
        this.mVelocityY = 0.0f;
        this.mRotation = 0.0f;
        this.mAngularVelocity = 0.0f;
        this.mScaleX = 1.0f;
        this.mScaleY = 1.0f;
        onPositionChanged();
        this.mRed = 1.0f;
        this.mGreen = 1.0f;
        this.mBlue = 1.0f;
        this.mAlpha = 1.0f;
        this.mSourceBlendFunction = BLENDFUNCTION_SOURCE_DEFAULT;
        this.mDestinationBlendFunction = 771;
        this.mShapeModifiers.reset();
    }
}
