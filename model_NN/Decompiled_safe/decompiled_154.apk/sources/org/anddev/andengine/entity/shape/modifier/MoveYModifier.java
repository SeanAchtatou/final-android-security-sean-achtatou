package org.anddev.andengine.entity.shape.modifier;

import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.entity.shape.modifier.IShapeModifier;
import org.anddev.andengine.entity.shape.modifier.ease.IEaseFunction;

public class MoveYModifier extends SingleValueSpanShapeModifier {
    public MoveYModifier(float pDuration, float pFromY, float pToY) {
        this(pDuration, pFromY, pToY, null, IEaseFunction.DEFAULT);
    }

    public MoveYModifier(float pDuration, float pFromY, float pToY, IEaseFunction pEaseFunction) {
        this(pDuration, pFromY, pToY, null, pEaseFunction);
    }

    public MoveYModifier(float pDuration, float pFromY, float pToY, IShapeModifier.IShapeModifierListener pShapeModiferListener) {
        super(pDuration, pFromY, pToY, pShapeModiferListener, IEaseFunction.DEFAULT);
    }

    public MoveYModifier(float pDuration, float pFromY, float pToY, IShapeModifier.IShapeModifierListener pShapeModiferListener, IEaseFunction pEaseFunction) {
        super(pDuration, pFromY, pToY, pShapeModiferListener, pEaseFunction);
    }

    protected MoveYModifier(MoveYModifier pMoveYModifier) {
        super(pMoveYModifier);
    }

    public MoveYModifier clone() {
        return new MoveYModifier(this);
    }

    /* access modifiers changed from: protected */
    public void onSetInitialValue(IShape pShape, float pY) {
        pShape.setPosition(pShape.getX(), pY);
    }

    /* access modifiers changed from: protected */
    public void onSetValue(IShape pShape, float pPercentageDone, float pY) {
        pShape.setPosition(pShape.getX(), pY);
    }
}
