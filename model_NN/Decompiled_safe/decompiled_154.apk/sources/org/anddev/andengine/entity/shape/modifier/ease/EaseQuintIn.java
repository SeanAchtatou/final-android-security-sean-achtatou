package org.anddev.andengine.entity.shape.modifier.ease;

public class EaseQuintIn implements IEaseFunction {
    private static EaseQuintIn INSTANCE;

    private EaseQuintIn() {
    }

    public static EaseQuintIn getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseQuintIn();
        }
        return INSTANCE;
    }

    public float getPercentageDone(float pSecondsElapsed, float pDuration, float pMinValue, float pMaxValue) {
        float pSecondsElapsed2 = pSecondsElapsed / pDuration;
        return (pMaxValue * pSecondsElapsed2 * pSecondsElapsed2 * pSecondsElapsed2 * pSecondsElapsed2 * pSecondsElapsed2) + pMinValue;
    }
}
