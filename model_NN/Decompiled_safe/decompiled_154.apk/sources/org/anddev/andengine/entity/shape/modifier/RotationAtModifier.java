package org.anddev.andengine.entity.shape.modifier;

import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.entity.shape.modifier.IShapeModifier;
import org.anddev.andengine.entity.shape.modifier.ease.IEaseFunction;

public class RotationAtModifier extends RotationModifier {
    private final float mRotationCenterX;
    private final float mRotationCenterY;

    public RotationAtModifier(float pDuration, float pFromRotation, float pToRotation, float pRotationCenterX, float pRotationCenterY) {
        super(pDuration, pFromRotation, pToRotation, IEaseFunction.DEFAULT);
        this.mRotationCenterX = pRotationCenterX;
        this.mRotationCenterY = pRotationCenterY;
    }

    public RotationAtModifier(float pDuration, float pFromRotation, float pToRotation, float pRotationCenterX, float pRotationCenterY, IEaseFunction pEaseFunction) {
        super(pDuration, pFromRotation, pToRotation, pEaseFunction);
        this.mRotationCenterX = pRotationCenterX;
        this.mRotationCenterY = pRotationCenterY;
    }

    public RotationAtModifier(float pDuration, float pFromRotation, float pToRotation, float pRotationCenterX, float pRotationCenterY, IShapeModifier.IShapeModifierListener pShapeModiferListener) {
        super(pDuration, pFromRotation, pToRotation, pShapeModiferListener, IEaseFunction.DEFAULT);
        this.mRotationCenterX = pRotationCenterX;
        this.mRotationCenterY = pRotationCenterY;
    }

    public RotationAtModifier(float pDuration, float pFromRotation, float pToRotation, float pRotationCenterX, float pRotationCenterY, IShapeModifier.IShapeModifierListener pShapeModiferListener, IEaseFunction pEaseFunction) {
        super(pDuration, pFromRotation, pToRotation, pShapeModiferListener, pEaseFunction);
        this.mRotationCenterX = pRotationCenterX;
        this.mRotationCenterY = pRotationCenterY;
    }

    protected RotationAtModifier(RotationAtModifier pRotationAtModifier) {
        super(pRotationAtModifier);
        this.mRotationCenterX = pRotationAtModifier.mRotationCenterX;
        this.mRotationCenterY = pRotationAtModifier.mRotationCenterY;
    }

    public RotationAtModifier clone() {
        return new RotationAtModifier(this);
    }

    /* access modifiers changed from: protected */
    public void onManagedInitialize(IShape pShape) {
        super.onManagedInitialize((Object) pShape);
        pShape.setRotationCenter(this.mRotationCenterX, this.mRotationCenterY);
    }
}
