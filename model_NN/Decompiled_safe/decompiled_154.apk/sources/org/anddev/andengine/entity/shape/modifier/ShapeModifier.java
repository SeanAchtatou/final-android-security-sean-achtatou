package org.anddev.andengine.entity.shape.modifier;

import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.entity.shape.modifier.IShapeModifier;
import org.anddev.andengine.util.modifier.BaseModifier;

public abstract class ShapeModifier extends BaseModifier<IShape> implements IShapeModifier {
    public ShapeModifier() {
    }

    public ShapeModifier(IShapeModifier.IShapeModifierListener pShapeModiferListener) {
        super(pShapeModiferListener);
    }

    protected ShapeModifier(ShapeModifier pShapeModifier) {
        super(pShapeModifier);
    }
}
