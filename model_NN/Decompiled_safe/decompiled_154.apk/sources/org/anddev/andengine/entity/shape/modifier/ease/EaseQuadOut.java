package org.anddev.andengine.entity.shape.modifier.ease;

public class EaseQuadOut implements IEaseFunction {
    private static EaseQuadOut INSTANCE;

    private EaseQuadOut() {
    }

    public static EaseQuadOut getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseQuadOut();
        }
        return INSTANCE;
    }

    public float getPercentageDone(float pSecondsElapsed, float pDuration, float pMinValue, float pMaxValue) {
        float pSecondsElapsed2 = pSecondsElapsed / pDuration;
        return ((-pMaxValue) * pSecondsElapsed2 * (pSecondsElapsed2 - 2.0f)) + pMinValue;
    }
}
