package org.anddev.andengine.entity.shape.modifier.ease;

import android.util.FloatMath;

public class EaseCircularInOut implements IEaseFunction {
    private static EaseCircularInOut INSTANCE;

    private EaseCircularInOut() {
    }

    public static EaseCircularInOut getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseCircularInOut();
        }
        return INSTANCE;
    }

    public float getPercentageDone(float pSecondsElapsed, float pDuration, float pMinValue, float pMaxValue) {
        float pSecondsElapsed2 = (float) (((double) pSecondsElapsed) / (((double) pDuration) * 0.5d));
        if (pSecondsElapsed2 < 1.0f) {
            return (float) ((((double) (-pMaxValue)) * 0.5d * ((double) (FloatMath.sqrt(1.0f - (pSecondsElapsed2 * pSecondsElapsed2)) - 1.0f))) + ((double) pMinValue));
        }
        float pSecondsElapsed3 = pSecondsElapsed2 - 2.0f;
        return (float) ((((double) pMaxValue) * 0.5d * ((double) (FloatMath.sqrt(1.0f - (pSecondsElapsed3 * pSecondsElapsed3)) + 1.0f))) + ((double) pMinValue));
    }
}
