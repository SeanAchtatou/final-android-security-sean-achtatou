package org.anddev.andengine.entity.shape.modifier.ease;

public class EaseStrongOut implements IEaseFunction {
    private static EaseStrongOut INSTANCE;

    private EaseStrongOut() {
    }

    public static EaseStrongOut getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseStrongOut();
        }
        return INSTANCE;
    }

    public float getPercentageDone(float pSecondsElapsed, float pDuration, float pMinValue, float pMaxValue) {
        float pSecondsElapsed2 = (pSecondsElapsed / pDuration) - 1.0f;
        return (((pSecondsElapsed2 * pSecondsElapsed2 * pSecondsElapsed2 * pSecondsElapsed2 * pSecondsElapsed2) + 1.0f) * pMaxValue) + pMinValue;
    }
}
