package org.anddev.andengine.entity.shape.modifier.ease;

public class EaseBackInOut implements IEaseFunction {
    private static EaseBackInOut INSTANCE;

    private EaseBackInOut() {
    }

    public static EaseBackInOut getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseBackInOut();
        }
        return INSTANCE;
    }

    public float getPercentageDone(float pSecondsElapsed, float pDuration, float pMinValue, float pMaxValue) {
        float pSecondsElapsed2 = pSecondsElapsed / (pDuration * 0.5f);
        if (pSecondsElapsed2 < 1.0f) {
            float s = 1.70158f * 1.525f;
            return (pMaxValue * 0.5f * pSecondsElapsed2 * pSecondsElapsed2 * (((1.0f + s) * pSecondsElapsed2) - s)) + pMinValue;
        }
        float pSecondsElapsed3 = pSecondsElapsed2 - 2.0f;
        float s2 = 1.70158f * 1.525f;
        return ((pMaxValue / 2.0f) * ((pSecondsElapsed3 * pSecondsElapsed3 * (((1.0f + s2) * pSecondsElapsed3) + s2)) + 2.0f)) + pMinValue;
    }
}
