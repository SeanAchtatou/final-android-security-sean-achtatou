package org.anddev.andengine.entity.shape.modifier.ease;

public class EaseQuadInOut implements IEaseFunction {
    private static EaseQuadInOut INSTANCE;

    private EaseQuadInOut() {
    }

    public static EaseQuadInOut getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseQuadInOut();
        }
        return INSTANCE;
    }

    public float getPercentageDone(float pSecondsElapsed, float pDuration, float pMinValue, float pMaxValue) {
        float pSecondsElapsed2 = pSecondsElapsed / (pDuration * 0.5f);
        if (pSecondsElapsed2 < 1.0f) {
            return (pMaxValue * 0.5f * pSecondsElapsed2 * pSecondsElapsed2) + pMinValue;
        }
        float pSecondsElapsed3 = pSecondsElapsed2 - 1.0f;
        return ((-pMaxValue) * 0.5f * (((pSecondsElapsed3 - 2.0f) * pSecondsElapsed3) - 1.0f)) + pMinValue;
    }
}
