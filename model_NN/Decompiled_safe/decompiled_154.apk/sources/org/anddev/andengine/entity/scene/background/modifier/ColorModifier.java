package org.anddev.andengine.entity.scene.background.modifier;

import org.anddev.andengine.entity.scene.background.IBackground;
import org.anddev.andengine.entity.scene.background.modifier.IBackgroundModifier;
import org.anddev.andengine.entity.shape.modifier.ease.IEaseFunction;
import org.anddev.andengine.util.modifier.BaseTripleValueSpanModifier;

public class ColorModifier extends BaseTripleValueSpanModifier<IBackground> implements IBackgroundModifier {
    public ColorModifier(float pDuration, float pFromRed, float pToRed, float pFromGreen, float pToGreen, float pFromBlue, float pToBlue) {
        this(pDuration, pFromRed, pToRed, pFromGreen, pToGreen, pFromBlue, pToBlue, null, IEaseFunction.DEFAULT);
    }

    public ColorModifier(float pDuration, float pFromRed, float pToRed, float pFromGreen, float pToGreen, float pFromBlue, float pToBlue, IEaseFunction pEaseFunction) {
        this(pDuration, pFromRed, pToRed, pFromGreen, pToGreen, pFromBlue, pToBlue, null, pEaseFunction);
    }

    public ColorModifier(float pDuration, float pFromRed, float pToRed, float pFromGreen, float pToGreen, float pFromBlue, float pToBlue, IBackgroundModifier.IBackgroundModifierListener pBackgroundModiferListener) {
        super(pDuration, pFromRed, pToRed, pFromGreen, pToGreen, pFromBlue, pToBlue, pBackgroundModiferListener, IEaseFunction.DEFAULT);
    }

    public ColorModifier(float pDuration, float pFromRed, float pToRed, float pFromGreen, float pToGreen, float pFromBlue, float pToBlue, IBackgroundModifier.IBackgroundModifierListener pBackgroundModiferListener, IEaseFunction pEaseFunction) {
        super(pDuration, pFromRed, pToRed, pFromGreen, pToGreen, pFromBlue, pToBlue, pBackgroundModiferListener, pEaseFunction);
    }

    protected ColorModifier(ColorModifier pColorModifier) {
        super(pColorModifier);
    }

    public ColorModifier clone() {
        return new ColorModifier(this);
    }

    /* access modifiers changed from: protected */
    public void onSetInitialValues(IBackground pBackground, float pRed, float pGreen, float pBlue) {
        pBackground.setColor(pRed, pGreen, pBlue);
    }

    /* access modifiers changed from: protected */
    public void onSetValues(IBackground pBackground, float pPerctentageDone, float pRed, float pGreen, float pBlue) {
        pBackground.setColor(pRed, pGreen, pBlue);
    }
}
