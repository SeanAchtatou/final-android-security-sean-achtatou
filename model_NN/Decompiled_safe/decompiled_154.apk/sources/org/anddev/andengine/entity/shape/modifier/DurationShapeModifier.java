package org.anddev.andengine.entity.shape.modifier;

import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.entity.shape.modifier.IShapeModifier;
import org.anddev.andengine.util.modifier.BaseDurationModifier;

public abstract class DurationShapeModifier extends BaseDurationModifier<IShape> implements IShapeModifier {
    public DurationShapeModifier() {
    }

    public DurationShapeModifier(float pDuration) {
        super(pDuration);
    }

    public DurationShapeModifier(float pDuration, IShapeModifier.IShapeModifierListener pShapeModiferListener) {
        super(pDuration, pShapeModiferListener);
    }

    protected DurationShapeModifier(DurationShapeModifier pDurationShapeModifier) {
        super(pDurationShapeModifier);
    }
}
