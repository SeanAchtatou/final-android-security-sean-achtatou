package org.anddev.andengine.entity.shape.modifier.ease;

public class EaseQuartIn implements IEaseFunction {
    private static EaseQuartIn INSTANCE;

    private EaseQuartIn() {
    }

    public static EaseQuartIn getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseQuartIn();
        }
        return INSTANCE;
    }

    public float getPercentageDone(float pSecondsElapsed, float pDuration, float pMinValue, float pMaxValue) {
        float pSecondsElapsed2 = pSecondsElapsed / pDuration;
        return (pMaxValue * pSecondsElapsed2 * pSecondsElapsed2 * pSecondsElapsed2 * pSecondsElapsed2) + pMinValue;
    }
}
