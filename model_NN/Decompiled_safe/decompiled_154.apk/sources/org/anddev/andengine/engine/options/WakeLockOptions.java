package org.anddev.andengine.engine.options;

public enum WakeLockOptions {
    FULL(26),
    SCREEN_BRIGHT(10),
    SCREEN_DIM(6);
    
    private final int mFlag;

    private WakeLockOptions(int pFlag) {
        this.mFlag = pFlag;
    }

    public int getFlag() {
        return this.mFlag;
    }
}
