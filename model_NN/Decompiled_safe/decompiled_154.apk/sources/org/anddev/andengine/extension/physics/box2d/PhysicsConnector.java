package org.anddev.andengine.extension.physics.box2d;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.entity.shape.Shape;
import org.anddev.andengine.extension.physics.box2d.util.constants.PhysicsConstants;
import org.anddev.andengine.util.MathUtils;

public class PhysicsConnector implements IUpdateHandler, PhysicsConstants {
    protected final Body mBody;
    protected final float mPixelToMeterRatio;
    protected final Shape mShape;
    protected final float mShapeHalfBaseHeight;
    protected final float mShapeHalfBaseWidth;
    protected boolean mUpdateAngularVelocity;
    protected boolean mUpdateLinearVelocity;
    protected boolean mUpdatePosition;
    protected boolean mUpdateRotation;

    public PhysicsConnector(Shape pShape, Body pBody) {
        this(pShape, pBody, true, true, true, true);
    }

    public PhysicsConnector(Shape pShape, Body pBody, float pPixelToMeterRatio) {
        this(pShape, pBody, true, true, true, true, pPixelToMeterRatio);
    }

    public PhysicsConnector(Shape pShape, Body pBody, boolean pUdatePosition, boolean pUpdateRotation, boolean pUpdateLinearVelocity, boolean pUpdateAngularVelocity) {
        this(pShape, pBody, pUdatePosition, pUpdateRotation, pUpdateLinearVelocity, pUpdateAngularVelocity, 32.0f);
    }

    public PhysicsConnector(Shape pShape, Body pBody, boolean pUdatePosition, boolean pUpdateRotation, boolean pUpdateLinearVelocity, boolean pUpdateAngularVelocity, float pPixelToMeterRatio) {
        this.mShape = pShape;
        this.mBody = pBody;
        this.mUpdatePosition = pUdatePosition;
        this.mUpdateRotation = pUpdateRotation;
        this.mUpdateLinearVelocity = pUpdateLinearVelocity;
        this.mUpdateAngularVelocity = pUpdateAngularVelocity;
        this.mPixelToMeterRatio = pPixelToMeterRatio;
        this.mShapeHalfBaseWidth = pShape.getBaseWidth() * 0.5f;
        this.mShapeHalfBaseHeight = pShape.getBaseHeight() * 0.5f;
    }

    public Shape getShape() {
        return this.mShape;
    }

    public Body getBody() {
        return this.mBody;
    }

    public boolean isUpdatePosition() {
        return this.mUpdatePosition;
    }

    public boolean isUpdateRotation() {
        return this.mUpdateRotation;
    }

    public boolean isUpdateLinearVelocity() {
        return this.mUpdateLinearVelocity;
    }

    public boolean isUpdateAngularVelocity() {
        return this.mUpdateAngularVelocity;
    }

    public void setUpdatePosition(boolean pUpdatePosition) {
        this.mUpdatePosition = pUpdatePosition;
    }

    public void setUpdateRotation(boolean pUpdateRotation) {
        this.mUpdateRotation = pUpdateRotation;
    }

    public void setUpdateLinearVelocity(boolean pUpdateLinearVelocity) {
        this.mUpdateLinearVelocity = pUpdateLinearVelocity;
    }

    public void setUpdateAngularVelocity(boolean pUpdateAngularVelocity) {
        this.mUpdateAngularVelocity = pUpdateAngularVelocity;
    }

    public void onUpdate(float pSecondsElapsed) {
        Shape shape = this.mShape;
        Body body = this.mBody;
        if (this.mUpdatePosition) {
            Vector2 position = body.getPosition();
            float pixelToMeterRatio = this.mPixelToMeterRatio;
            shape.setPosition((position.x * pixelToMeterRatio) - this.mShapeHalfBaseWidth, (position.y * pixelToMeterRatio) - this.mShapeHalfBaseHeight);
        }
        if (this.mUpdateRotation) {
            shape.setRotation(MathUtils.radToDeg(body.getAngle()));
        }
        if (this.mUpdateLinearVelocity) {
            Vector2 linearVelocity = body.getLinearVelocity();
            shape.setVelocity(linearVelocity.x, linearVelocity.y);
        }
        if (this.mUpdateAngularVelocity) {
            shape.setAngularVelocity(body.getAngularVelocity());
        }
    }

    public void reset() {
    }
}
