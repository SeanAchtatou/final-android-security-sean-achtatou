package org.anddev.andengine.extension.input.touch.controller;

import android.content.Context;
import android.content.pm.PackageManager;
import java.lang.reflect.Method;
import org.anddev.andengine.util.SystemUtils;

public class MultiTouch {
    private static Boolean SUPPORTED = null;
    private static Boolean SUPPORTED_DISTINCT = null;

    public static boolean isSupported(Context pContext) {
        if (SUPPORTED == null) {
            SUPPORTED = Boolean.valueOf(SystemUtils.isAndroidVersionOrHigher(7) && hasFeature(pContext, "android.hardware.touchscreen.multitouch"));
        }
        return SUPPORTED.booleanValue();
    }

    public static boolean isSupportedDistinct(Context pContext) {
        if (SUPPORTED_DISTINCT == null) {
            SUPPORTED_DISTINCT = Boolean.valueOf(SystemUtils.isAndroidVersionOrHigher(7) && hasFeature(pContext, "android.hardware.touchscreen.multitouch.distinct"));
        }
        return SUPPORTED_DISTINCT.booleanValue();
    }

    private static boolean hasFeature(Context pContext, String pFeature) {
        try {
            Method PackageManager_hasSystemFeatures = PackageManager.class.getMethod("hasSystemFeature", String.class);
            if (PackageManager_hasSystemFeatures == null) {
                return false;
            }
            return ((Boolean) PackageManager_hasSystemFeatures.invoke(pContext.getPackageManager(), pFeature)).booleanValue();
        } catch (Throwable th) {
            return false;
        }
    }

    public static boolean isSupportedByAndroidVersion() {
        return SystemUtils.isAndroidVersionOrHigher(5);
    }
}
