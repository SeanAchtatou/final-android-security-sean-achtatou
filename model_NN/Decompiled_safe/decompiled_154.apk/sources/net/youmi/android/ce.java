package net.youmi.android;

import android.app.Activity;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.ImageView;

class ce extends FrameLayout implements ac {
    ImageView a;
    ImageView b;
    ImageView c;
    Animation d;
    Animation e;
    final /* synthetic */ bb f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ce(bb bbVar, Activity activity, ak akVar, boolean z) {
        super(activity);
        int i;
        this.f = bbVar;
        this.a = new ImageView(activity);
        this.b = new ImageView(activity);
        this.a.setVisibility(8);
        this.b.setVisibility(8);
        int d2 = akVar.d();
        if (z) {
            int e2 = akVar.e();
            i = e2;
            d2 = akVar.e();
        } else {
            i = -1;
            this.a.setScaleType(ImageView.ScaleType.FIT_XY);
            this.b.setScaleType(ImageView.ScaleType.FIT_XY);
        }
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(i, d2);
        addView(this.a, layoutParams);
        addView(this.b, layoutParams);
        if (z) {
            this.d = r.c(akVar);
            this.e = r.d(akVar);
            return;
        }
        this.d = r.a(akVar);
        this.e = r.b(akVar);
    }

    public void a() {
        setVisibility(8);
    }

    public void a(Animation animation) {
        try {
            startAnimation(animation);
        } catch (Exception e2) {
        }
    }

    public boolean a(ax axVar) {
        if (axVar == null) {
            return false;
        }
        try {
            if (axVar.d() == null) {
                return false;
            }
            ImageView b2 = b();
            if (b2 == null) {
                return false;
            }
            b2.setImageBitmap(axVar.d());
            return true;
        } catch (Exception e2) {
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public ImageView b() {
        return this.a == this.c ? this.b : this.a;
    }

    public void c() {
        ImageView b2 = b();
        if (b2 != null) {
            if (this.c != null) {
                this.c.setVisibility(8);
            }
            b2.setVisibility(0);
            this.c = b2;
        }
    }

    public void d() {
        setVisibility(0);
    }

    public void e() {
        ImageView b2 = b();
        if (b2 != null) {
            if (this.c != null) {
                this.c.startAnimation(this.e);
                this.c.setVisibility(8);
            }
            b2.setVisibility(0);
            b2.startAnimation(this.d);
            this.c = b2;
        }
    }

    public void f() {
        removeAllViews();
    }
}
