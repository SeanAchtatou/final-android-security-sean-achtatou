package net.youmi.android;

class bz {
    private final int[] a = {1732584193, -271733879, -1732584194, 271733878, -1009589776};
    private int[] b = new int[5];
    private int[] c = new int[80];

    bz() {
    }

    private int a(int i, int i2) {
        return (i << i2) | (i >>> (32 - i2));
    }

    private int a(int i, int i2, int i3) {
        return (i & i2) | ((i ^ -1) & i3);
    }

    private int a(byte[] bArr, int i) {
        return ((bArr[i] & 255) << 24) | ((bArr[i + 1] & 255) << 16) | ((bArr[i + 2] & 255) << 8) | (bArr[i + 3] & 255);
    }

    private void a() {
        for (int i = 16; i <= 79; i++) {
            this.c[i] = a(((this.c[i - 3] ^ this.c[i - 8]) ^ this.c[i - 14]) ^ this.c[i - 16], 1);
        }
        int[] iArr = new int[5];
        for (int i2 = 0; i2 < iArr.length; i2++) {
            iArr[i2] = this.b[i2];
        }
        for (int i3 = 0; i3 <= 19; i3++) {
            iArr[4] = iArr[3];
            iArr[3] = iArr[2];
            iArr[2] = a(iArr[1], 30);
            iArr[1] = iArr[0];
            iArr[0] = a(iArr[0], 5) + a(iArr[1], iArr[2], iArr[3]) + iArr[4] + this.c[i3] + 1518500249;
        }
        for (int i4 = 20; i4 <= 39; i4++) {
            iArr[4] = iArr[3];
            iArr[3] = iArr[2];
            iArr[2] = a(iArr[1], 30);
            iArr[1] = iArr[0];
            iArr[0] = a(iArr[0], 5) + b(iArr[1], iArr[2], iArr[3]) + iArr[4] + this.c[i4] + 1859775393;
        }
        for (int i5 = 40; i5 <= 59; i5++) {
            iArr[4] = iArr[3];
            iArr[3] = iArr[2];
            iArr[2] = a(iArr[1], 30);
            iArr[1] = iArr[0];
            iArr[0] = (((a(iArr[0], 5) + c(iArr[1], iArr[2], iArr[3])) + iArr[4]) + this.c[i5]) - 1894007588;
        }
        for (int i6 = 60; i6 <= 79; i6++) {
            iArr[4] = iArr[3];
            iArr[3] = iArr[2];
            iArr[2] = a(iArr[1], 30);
            iArr[1] = iArr[0];
            iArr[0] = (((a(iArr[0], 5) + b(iArr[1], iArr[2], iArr[3])) + iArr[4]) + this.c[i6]) - 899497514;
        }
        for (int i7 = 0; i7 < iArr.length; i7++) {
            this.b[i7] = this.b[i7] + iArr[i7];
        }
        for (int i8 = 0; i8 < this.c.length; i8++) {
            this.c[i8] = 0;
        }
    }

    private void a(int i, byte[] bArr, int i2) {
        bArr[i2] = (byte) (i >>> 24);
        bArr[i2 + 1] = (byte) (i >>> 16);
        bArr[i2 + 2] = (byte) (i >>> 8);
        bArr[i2 + 3] = (byte) i;
    }

    private int b(int i, int i2, int i3) {
        return (i ^ i2) ^ i3;
    }

    private int b(byte[] bArr) {
        System.arraycopy(this.a, 0, this.b, 0, this.a.length);
        byte[] c2 = c(bArr);
        int length = c2.length / 64;
        for (int i = 0; i < length; i++) {
            for (int i2 = 0; i2 < 16; i2++) {
                this.c[i2] = a(c2, (i * 64) + (i2 * 4));
            }
            a();
        }
        return 20;
    }

    private int c(int i, int i2, int i3) {
        return (i & i2) | (i & i3) | (i2 & i3);
    }

    private byte[] c(byte[] bArr) {
        int i;
        int i2;
        int length = bArr.length;
        int i3 = length % 64;
        if (i3 < 56) {
            i = 55 - i3;
            i2 = (length - i3) + 64;
        } else if (i3 == 56) {
            i = 63;
            i2 = length + 8 + 64;
        } else {
            i = (63 - i3) + 56;
            i2 = ((length + 64) - i3) + 64;
        }
        byte[] bArr2 = new byte[i2];
        System.arraycopy(bArr, 0, bArr2, 0, length);
        bArr2[length] = Byte.MIN_VALUE;
        int i4 = length + 1;
        int i5 = 0;
        while (i5 < i) {
            bArr2[i4] = 0;
            i5++;
            i4++;
        }
        long j = ((long) length) * 8;
        int i6 = i4 + 1;
        bArr2[i4] = (byte) ((int) (j >> 56));
        int i7 = i6 + 1;
        bArr2[i6] = (byte) ((int) ((j >> 48) & 255));
        int i8 = i7 + 1;
        bArr2[i7] = (byte) ((int) ((j >> 40) & 255));
        int i9 = i8 + 1;
        bArr2[i8] = (byte) ((int) ((j >> 32) & 255));
        int i10 = i9 + 1;
        bArr2[i9] = (byte) ((int) ((j >> 24) & 255));
        int i11 = i10 + 1;
        bArr2[i10] = (byte) ((int) ((j >> 16) & 255));
        int i12 = i11 + 1;
        bArr2[i11] = (byte) ((int) ((j >> 8) & 255));
        int i13 = i12 + 1;
        bArr2[i12] = (byte) ((int) (255 & j));
        return bArr2;
    }

    /* access modifiers changed from: package-private */
    public byte[] a(byte[] bArr) {
        b(bArr);
        byte[] bArr2 = new byte[20];
        for (int i = 0; i < this.b.length; i++) {
            a(this.b[i], bArr2, i * 4);
        }
        return bArr2;
    }
}
