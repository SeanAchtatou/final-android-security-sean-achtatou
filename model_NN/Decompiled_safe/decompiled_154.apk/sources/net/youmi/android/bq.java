package net.youmi.android;

import android.view.MotionEvent;
import android.view.View;

class bq implements View.OnTouchListener {
    final /* synthetic */ ci a;

    bq(ci ciVar) {
        this.a = ciVar;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
            case 2:
                try {
                    if (this.a.v) {
                        this.a.a.setImageBitmap(this.a.n());
                        return false;
                    }
                    this.a.a.setImageBitmap(this.a.i());
                    return false;
                } catch (Exception e) {
                    return false;
                }
            case 1:
            default:
                try {
                    if (this.a.v) {
                        this.a.a.setImageBitmap(this.a.m());
                        return false;
                    }
                    this.a.a.setImageBitmap(this.a.h());
                    return false;
                } catch (Exception e2) {
                    return false;
                }
        }
    }
}
