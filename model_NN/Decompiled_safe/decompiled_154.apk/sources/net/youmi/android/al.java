package net.youmi.android;

class al {
    String a;
    String b;
    bv c = new bv(this);
    cv d = new cv(this);

    al() {
    }

    /* access modifiers changed from: package-private */
    public String a() {
        StringBuilder sb = new StringBuilder(256);
        sb.append("style=\"");
        if (this.b != null) {
            sb.append("color:");
            sb.append(this.b);
            sb.append(";");
        }
        if (this.a != null) {
            sb.append("background-color:");
            sb.append(this.a);
            sb.append(";");
        }
        if (this.d != null) {
            if (this.d.a != null) {
                sb.append("font-size:");
                sb.append(this.d.a);
                sb.append(";");
            }
            if (this.d.b != null) {
                sb.append("font-weight:");
                sb.append(this.d.b);
                sb.append(";");
            }
        }
        if (this.c != null) {
            if (this.c.a != null) {
                sb.append("border:");
                sb.append(this.c.a);
                sb.append(";");
            }
            if (this.c.b != null) {
                sb.append("border-bottom-width:");
                sb.append(this.c.b);
                sb.append(";");
            }
            if (this.c.c != null) {
                sb.append("border-bottom-style:");
                sb.append(this.c.c);
                sb.append(";");
            }
            if (this.c.d != null) {
                sb.append("border-bottom-color:");
                sb.append(this.c.d);
                sb.append(";");
            }
        }
        sb.append("\"");
        return sb.toString();
    }
}
