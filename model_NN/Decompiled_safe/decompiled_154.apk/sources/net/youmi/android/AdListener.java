package net.youmi.android;

public interface AdListener {
    void onConnectFailed(AdView adView);

    void onReceiveAd(AdView adView);
}
