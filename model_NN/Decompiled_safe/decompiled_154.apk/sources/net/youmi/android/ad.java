package net.youmi.android;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import java.net.URLDecoder;

class ad {
    ad() {
    }

    static void a(Activity activity, String str) {
        if (str != null) {
            String trim = str.trim();
            if (trim.length() != 0) {
                try {
                    Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(trim));
                    intent.addFlags(268435456);
                    intent.setComponent(new ComponentName("com.android.browser", "com.android.browser.BrowserActivity"));
                    activity.startActivity(intent);
                } catch (Exception e) {
                    try {
                        Intent intent2 = new Intent("android.intent.action.VIEW", Uri.parse(trim));
                        intent2.addFlags(268435456);
                        activity.startActivity(intent2);
                    } catch (Exception e2) {
                    }
                }
            }
        }
    }

    static boolean a(Activity activity, String str, String str2) {
        if (str != null) {
            try {
                Intent intent = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:" + str));
                if (str2 != null) {
                    intent.putExtra("sms_body", URLDecoder.decode(str2));
                }
                activity.startActivity(intent);
                return true;
            } catch (Exception e) {
            }
        }
        return false;
    }

    static void b(Activity activity, String str) {
        try {
            Intent intent = new Intent(activity, AdActivity.class);
            AdActivity.a(intent, str);
            activity.startActivity(intent);
        } catch (Exception e) {
        }
    }

    static boolean c(Activity activity, String str) {
        if (str == null) {
            return false;
        }
        try {
            if (str.indexOf("geo:") == 0) {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
                intent.addFlags(268435456);
                activity.startActivity(intent);
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    static boolean d(Activity activity, String str) {
        String str2;
        if (str == null) {
            return false;
        }
        try {
            String str3 = new String(str);
            int indexOf = str3.indexOf("?body=");
            if (indexOf > -1) {
                str2 = str3.substring("?body=".length() + indexOf);
                str3 = str3.substring(0, indexOf);
            } else {
                str2 = null;
            }
            String substring = str3.indexOf("sms:") == 0 ? str3.substring("sms:".length()) : str3.indexOf("smsto:") == 0 ? str3.substring("smsto:".length()) : null;
            if (substring != null) {
                Intent intent = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:" + substring));
                if (str2 != null) {
                    intent.putExtra("sms_body", URLDecoder.decode(str2));
                }
                activity.startActivity(intent);
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    static boolean e(Activity activity, String str) {
        if (str != null) {
            try {
                Intent intent = new Intent("android.intent.action.DIAL", Uri.parse("tel:" + str));
                intent.addFlags(268435456);
                activity.startActivity(intent);
                return true;
            } catch (Exception e) {
            }
        }
        return false;
    }

    static boolean f(Activity activity, String str) {
        if (str == null) {
            return false;
        }
        String str2 = null;
        try {
            if (str.indexOf("wtai://wp/mc;") == 0) {
                str2 = str.substring("wtai://wp/mc;".length());
            } else if (str.indexOf("tel:") == 0) {
                str2 = str.substring("tel:".length());
            }
            if (str2 != null) {
                Intent intent = new Intent("android.intent.action.DIAL", Uri.parse("tel:" + str2));
                intent.addFlags(268435456);
                activity.startActivity(intent);
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }
}
