package net.youmi.android;

public class AdManager {
    static boolean a = false;

    public static void init(String str, String str2, int i, boolean z, double d) {
        if (!a) {
            a = true;
            n.a(z);
            n.a(str);
            n.b(str2);
            n.a(i);
            n.a(d);
            g.a("current sdk version is youmi android sdk " + h.c());
            g.a("App ID is set to " + str);
            g.a("App Sec is set to " + str2);
            g.a("App Version is set to " + d);
            g.a("Requesting fresh ads every " + i + " seconds.");
        }
    }
}
