package net.youmi.android;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Shader;
import com.adview.util.AdViewUtil;

class ah {
    static final int a = Color.argb(160, (int) AdViewUtil.VERSION, (int) AdViewUtil.VERSION, (int) AdViewUtil.VERSION);
    static final int b = Color.argb(50, (int) AdViewUtil.VERSION, (int) AdViewUtil.VERSION, (int) AdViewUtil.VERSION);
    static final int c = Color.argb(128, (int) AdViewUtil.VERSION, (int) AdViewUtil.VERSION, (int) AdViewUtil.VERSION);
    static final int d = Color.argb(128, 0, 0, 0);
    static final int e = Color.argb(80, 0, 0, 0);
    static final int f = Color.argb(150, 135, 206, 250);
    private static LinearGradient g;

    ah() {
    }

    static Bitmap a(int i, int i2, int i3, int i4) {
        try {
            Bitmap createBitmap = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(createBitmap);
            Paint paint = new Paint();
            Bitmap createBitmap2 = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
            paint.reset();
            Canvas canvas2 = new Canvas(createBitmap2);
            canvas2.drawColor(i3);
            paint.reset();
            paint.setShader(a(i2));
            Rect rect = new Rect();
            rect.top = 0;
            rect.left = 0;
            rect.bottom = i2 / 2;
            rect.right = i;
            canvas2.drawRect(rect, paint);
            paint.reset();
            paint.setColor(d);
            canvas2.drawLine(0.0f, 0.0f, (float) i, 0.0f, paint);
            paint.reset();
            paint.setColor(c);
            canvas2.drawLine(0.0f, 1.0f, (float) i, 1.0f, paint);
            paint.reset();
            paint.setColor(e);
            canvas2.drawLine(0.0f, (float) (i2 - 1), (float) i, (float) (i2 - 1), paint);
            paint.reset();
            paint.reset();
            paint.setAlpha(i4);
            canvas.drawBitmap(createBitmap2, 0.0f, 0.0f, paint);
            try {
                if (!createBitmap2.isRecycled()) {
                    createBitmap2.recycle();
                }
            } catch (Exception e2) {
            }
            return createBitmap;
        } catch (Exception e3) {
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
     arg types: [int, int, int, float, int, int, android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
    static LinearGradient a(int i) {
        if (g == null) {
            g = new LinearGradient(0.0f, 0.0f, 0.0f, 0.5f * ((float) i), a, b, Shader.TileMode.CLAMP);
        }
        return g;
    }
}
