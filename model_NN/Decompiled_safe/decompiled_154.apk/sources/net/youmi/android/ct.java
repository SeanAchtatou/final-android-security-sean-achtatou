package net.youmi.android;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;

class ct extends ImageView {
    /* access modifiers changed from: private */
    public au a;
    private cu b;
    /* access modifiers changed from: private */
    public Bitmap c;
    private boolean d = false;

    public ct(Context context) {
        super(context);
    }

    private void b() {
        try {
            if (this.b != null) {
                c();
            }
            this.b = new cu(new bf(this), -1);
        } catch (Exception e) {
        }
    }

    private void c() {
        try {
            if (this.b != null) {
                this.b.a();
            }
            this.b = null;
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.d = false;
        c();
    }

    /* access modifiers changed from: package-private */
    public void a(au auVar) {
        if (auVar != null) {
            try {
                if (auVar.c() > 0) {
                    this.a = auVar;
                    this.d = true;
                    b();
                }
            } catch (Exception e) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        try {
            this.a = null;
            c();
        } catch (Exception e) {
        }
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (!z) {
            c();
        } else if (this.d) {
            b();
        }
    }
}
