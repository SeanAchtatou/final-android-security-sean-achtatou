package net.youmi.android;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;

class m {
    m() {
    }

    static Location a(Context context) {
        LocationManager locationManager;
        Location location = null;
        try {
            if (!bx.a(context, "android.permission.ACCESS_FINE_LOCATION") || (locationManager = (LocationManager) context.getSystemService("location")) == null) {
                return null;
            }
            if (locationManager.isProviderEnabled("gps")) {
                location = locationManager.getLastKnownLocation("gps");
            }
            return (location != null || !locationManager.isProviderEnabled("network")) ? location : locationManager.getLastKnownLocation("network");
        } catch (Exception e) {
            return null;
        }
    }
}
