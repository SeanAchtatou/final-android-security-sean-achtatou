package net.youmi.android;

import android.app.Activity;
import android.graphics.Bitmap;
import android.widget.ImageView;
import android.widget.RelativeLayout;

class ba extends RelativeLayout implements cp {
    private ImageView a;
    private Bitmap b;
    private Activity c;
    private ak d;

    public ba(Activity activity, ak akVar, Bitmap bitmap) {
        super(activity);
        this.b = bitmap;
        this.c = activity;
        this.d = akVar;
        a(activity);
    }

    private void a(Activity activity) {
        this.a = new ImageView(activity);
        this.a.setImageBitmap(this.b);
        RelativeLayout.LayoutParams a2 = ap.a();
        a2.addRule(13);
        addView(this.a, a2);
    }

    public void a() {
        this.c.finish();
    }
}
