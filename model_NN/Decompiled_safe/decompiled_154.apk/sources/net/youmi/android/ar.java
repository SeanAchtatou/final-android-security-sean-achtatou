package net.youmi.android;

import java.io.UnsupportedEncodingException;

class ar {
    static final char[] a = "0123456789ABCDEF".toCharArray();
    static String b;
    private static final char[] c = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-".toCharArray();

    ar() {
    }

    static String a() {
        if (b == null) {
            b = v.a("59587142510671244a4410427c0f1132", "0359204502517f2316154f427a024269");
        }
        return b;
    }

    static final String a(String str, int i) {
        String a2 = as.a(String.valueOf(a()) + i);
        StringBuilder sb = new StringBuilder(1024);
        int length = a2.length();
        int length2 = str.length();
        int length3 = ((str.length() - 1) / length) + 1;
        int i2 = 0;
        for (int i3 = 0; i3 < length3; i3++) {
            int i4 = i2 + length;
            if (i4 > length2) {
                i4 = length2;
            }
            byte[] a3 = a(str.substring(i2, i4), a2);
            for (int i5 = 0; i5 < a3.length; i5++) {
                sb.append(a[(a3[i5] >>> 4) & 15]);
                sb.append(a[a3[i5] & 15]);
            }
            i2 += length;
        }
        return a(sb.toString(), sb);
    }

    static final String a(String str, StringBuilder sb) {
        sb.delete(0, sb.length());
        int length = str.length();
        sb.append(c[length % 3]);
        int i = 0;
        for (int i2 = 0; i2 < length; i2 += 3) {
            int parseInt = Integer.parseInt(str.substring(i2, i2 + 3 < length ? i2 + 3 : length), 16);
            sb.append(c[(((byte) (parseInt < 64 ? 0 : parseInt >>> 6)) + i) % 64]);
            int i3 = i + 1;
            sb.append(c[(parseInt + i3) % 64]);
            i = i3 + 1;
        }
        return sb.toString();
    }

    private static byte[] a(String str) {
        try {
            return str.getBytes("UTF-8");
        } catch (Exception e) {
            return null;
        }
    }

    private static final byte[] a(String str, String str2) {
        byte[] a2 = a(str2);
        if (a2 == null) {
            throw new UnsupportedEncodingException("key should be null");
        }
        byte[] bytes = str.getBytes("UTF-8");
        byte[] bArr = new byte[bytes.length];
        int length = a2.length;
        int length2 = ((bytes.length - 1) / a2.length) + 1;
        int i = 0;
        for (int i2 = 0; i2 < length2; i2++) {
            for (int i3 = 0; i3 < length; i3++) {
                int i4 = i + i3;
                if (i4 >= bArr.length) {
                    break;
                }
                bArr[i4] = (byte) (bytes[i4] ^ a2[i3]);
            }
            i += length;
        }
        return bArr;
    }
}
