package net.youmi.android;

import org.anddev.andengine.util.constants.TimeConstants;

class n {
    private static boolean a = false;
    private static String b = "";
    private static String c = "";
    private static long d = 30000;
    private static double e = 1.0d;

    n() {
    }

    static void a(double d2) {
        e = Math.abs(d2);
    }

    static void a(int i) {
        d = (long) (Math.abs(i) * TimeConstants.MILLISECONDSPERSECOND);
    }

    static void a(String str) {
        if (str != null) {
            b = str.trim();
        }
    }

    static void a(boolean z) {
        a = z;
    }

    static boolean a() {
        return a;
    }

    static String b() {
        return b;
    }

    static void b(String str) {
        c = str;
    }

    static String c() {
        return c;
    }

    static long d() {
        return d;
    }

    static String e() {
        return new StringBuilder(String.valueOf(e)).toString();
    }
}
