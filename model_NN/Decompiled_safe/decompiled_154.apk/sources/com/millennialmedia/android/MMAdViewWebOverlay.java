package com.millennialmedia.android;

import android.app.Activity;
import android.content.res.AssetManager;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.io.IOException;
import java.lang.ref.WeakReference;

public class MMAdViewWebOverlay extends FrameLayout {
    private static final String TAG = "MillennialMediaSDK";
    private static final int kPadding = 0;
    private static final int kTitleMarginX = 8;
    private static final int kTitleMarginY = 9;
    private static final int kTransitionDuration = 200;
    private Button backButton;
    private ConnectivityManager cm;
    private RelativeLayout content;
    private Button forwardButton;
    Handler hClose = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 2:
                    MMAdViewWebOverlay.this.dismiss(true);
                    return;
                default:
                    return;
            }
        }
    };
    public WeakReference<Activity> overlayActivityWeakReference;
    /* access modifiers changed from: private */
    public String overlayUrl;
    /* access modifiers changed from: private */
    public TextView title;
    protected WebView webView;
    private LinearLayout webViewLayout;

    public MMAdViewWebOverlay(Activity context, int padding, long time, String transition, boolean titlebar, String titleText, boolean navbar) {
        super(context);
        this.overlayActivityWeakReference = new WeakReference<>(context);
        setId(15062);
        Activity activity = this.overlayActivityWeakReference.get();
        if (activity != null) {
            activity.setTheme(16973840);
            this.cm = (ConnectivityManager) activity.getSystemService("connectivity");
            setWillNotDraw(false);
            Integer scaledPadding = Integer.valueOf((int) (0.0625f * activity.getResources().getDisplayMetrics().density * ((float) padding)));
            setPadding(scaledPadding.intValue(), scaledPadding.intValue(), scaledPadding.intValue(), scaledPadding.intValue());
            this.content = new RelativeLayout(activity);
            this.content.setBackgroundColor(0);
            this.content.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
            if (titlebar) {
                RelativeLayout relativeLayout = new RelativeLayout(activity);
                relativeLayout.setLayoutParams(new FrameLayout.LayoutParams(-1, -2));
                relativeLayout.setBackgroundColor(-16777216);
                this.title = new TextView(activity);
                this.title.setText(titleText);
                this.title.setTextColor(-1);
                this.title.setBackgroundColor(-16777216);
                this.title.setTypeface(Typeface.DEFAULT_BOLD);
                this.title.setPadding(8, 9, 8, 9);
                this.title.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
                relativeLayout.addView(this.title);
                Button closeButton = new Button(activity);
                closeButton.setBackgroundColor(-16777216);
                closeButton.setText("Close");
                closeButton.setTextColor(-1);
                closeButton.setOnTouchListener(new View.OnTouchListener() {
                    public boolean onTouch(View view, MotionEvent event) {
                        switch (event.getAction()) {
                            case 0:
                                Log.i(MMAdViewWebOverlay.TAG, "Close button down");
                                return true;
                            case 1:
                                MMAdViewWebOverlay.this.title.setBackgroundColor(-7829368);
                                Log.i(MMAdViewWebOverlay.TAG, "Close button up");
                                MMAdViewWebOverlay.this.dismiss(true);
                                return true;
                            default:
                                return false;
                        }
                    }
                });
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams.addRule(11);
                relativeLayout.addView(closeButton, layoutParams);
                this.content.addView(relativeLayout);
            }
            Activity activity2 = this.overlayActivityWeakReference.get();
            if (activity2 != null) {
                this.webViewLayout = new LinearLayout(activity2);
                this.webViewLayout.setOrientation(1);
                this.webViewLayout.setBackgroundColor(0);
                this.webViewLayout.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
                this.webView = new WebView(activity2);
                this.webView.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
                this.webView.setWebViewClient(new OverlayWebViewClient());
                this.webView.addJavascriptInterface(new OverlayJSInterface(), "interface");
                WebSettings webSettings = this.webView.getSettings();
                webSettings.setJavaScriptEnabled(true);
                webSettings.setDefaultTextEncodingName("UTF-8");
                this.content.addView(this.webView);
                if (navbar) {
                    RelativeLayout navBar = new RelativeLayout(activity2);
                    RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, 50);
                    layoutParams2.addRule(12);
                    navBar.setBackgroundColor(-3355444);
                    Button navCloseButton = new Button(activity2);
                    navCloseButton.setBackgroundColor(-16777216);
                    navCloseButton.setBackgroundResource(17301560);
                    navCloseButton.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View view) {
                            Log.i(MMAdViewWebOverlay.TAG, "Close button");
                            MMAdViewWebOverlay.this.dismiss(true);
                        }
                    });
                    RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
                    layoutParams3.addRule(11);
                    layoutParams3.addRule(15);
                    navBar.addView(navCloseButton, layoutParams3);
                    this.forwardButton = new Button(activity2);
                    AssetManager am = activity2.getAssets();
                    try {
                        this.forwardButton.setBackgroundDrawable(Drawable.createFromStream(am.open("right_arrow.png"), "right_arrow.png"));
                    } catch (IOException e) {
                        this.forwardButton.setBackgroundColor(-3355444);
                        this.forwardButton.setText(">>");
                        this.forwardButton.setTextColor(-16777216);
                        e.printStackTrace();
                    }
                    this.forwardButton.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View view) {
                            Log.i(MMAdViewWebOverlay.TAG, "Forward button up");
                            MMAdViewWebOverlay.this.webView.goForward();
                        }
                    });
                    this.backButton = new Button(activity2);
                    try {
                        this.backButton.setBackgroundDrawable(Drawable.createFromStream(am.open("left_arrow.png"), "left_arrow.png"));
                    } catch (IOException e2) {
                        this.backButton.setBackgroundColor(-3355444);
                        this.backButton.setText("<<");
                        this.backButton.setTextColor(-16777216);
                        e2.printStackTrace();
                    }
                    this.backButton.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View view) {
                            MMAdViewWebOverlay.this.webView.goBack();
                            Log.i(MMAdViewWebOverlay.TAG, "back button up");
                        }
                    });
                    this.backButton.setId(2401);
                    RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
                    layoutParams4.addRule(15);
                    navBar.addView(this.backButton, layoutParams4);
                    RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(-2, -2);
                    layoutParams5.addRule(1, this.backButton.getId());
                    layoutParams5.addRule(15);
                    navBar.addView(this.forwardButton, layoutParams5);
                    this.content.addView(navBar, layoutParams2);
                }
                addView(this.content);
                animateView(transition, time);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Rect grayRect = new Rect(new Rect(canvas.getClipBounds()));
        grayRect.inset(0, 0);
        drawRect(canvas, grayRect);
    }

    private void drawRect(Canvas canvas, Rect rect) {
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setAntiAlias(true);
    }

    private void animateView(String animation, long time) {
        if (animation.equals("toptobottom")) {
            TranslateAnimation translateDown = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, -1.0f, 1, 0.0f);
            translateDown.setDuration(time);
            translateDown.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationEnd(Animation arg0) {
                }

                public void onAnimationRepeat(Animation arg0) {
                }

                public void onAnimationStart(Animation arg0) {
                }
            });
            Log.d(TAG, "translate down");
            startAnimation(translateDown);
        } else if (animation.equals("explode")) {
            ScaleAnimation scale = new ScaleAnimation(1.1f, 0.9f, 0.1f, 0.9f, 1, 0.5f, 1, 0.5f);
            scale.setDuration(time);
            scale.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationEnd(Animation arg0) {
                }

                public void onAnimationRepeat(Animation arg0) {
                }

                public void onAnimationStart(Animation arg0) {
                }
            });
            startAnimation(scale);
        } else {
            TranslateAnimation translateUp = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
            translateUp.setDuration(time);
            translateUp.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationEnd(Animation arg0) {
                }

                public void onAnimationRepeat(Animation arg0) {
                }

                public void onAnimationStart(Animation arg0) {
                }
            });
            Log.d(TAG, "translate up");
            startAnimation(translateUp);
        }
    }

    public void loadWebContent(String url) {
        this.overlayUrl = url;
        if (this.cm.getActiveNetworkInfo() == null || !this.cm.getActiveNetworkInfo().isConnected()) {
            Log.i(TAG, "No network available, can't load overlay.");
        } else {
            new Thread() {
                public void run() {
                    MMAdViewWebOverlay.this.webView.loadUrl(MMAdViewWebOverlay.this.overlayUrl);
                }
            }.start();
        }
    }

    public void injectJS(String jsString) {
        this.webView.loadUrl(jsString);
    }

    /* access modifiers changed from: private */
    public void dismiss(boolean animated) {
        Log.i(TAG, "Ad overlay closed");
        Activity activity = this.overlayActivityWeakReference.get();
        if (activity != null) {
            if (animated) {
                AlphaAnimation animation = new AlphaAnimation(1.0f, 0.0f);
                animation.setDuration(200);
                activity.finish();
                startAnimation(animation);
                return;
            }
            activity.finish();
        }
    }

    public class OverlayJSInterface {
        public OverlayJSInterface() {
        }

        public void shouldCloseOverlay() {
            MMAdViewWebOverlay.this.hClose.sendEmptyMessage(2);
        }

        public void shouldVibrate(String milliseconds) {
        }
    }

    public Activity getOverlayActivity() {
        return this.overlayActivityWeakReference.get();
    }
}
