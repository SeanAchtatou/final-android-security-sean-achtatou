package com.millennialmedia.android;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import java.util.List;
import org.anddev.andengine.util.constants.TimeConstants;

public class AccelerometerManager {
    /* access modifiers changed from: private */
    public static int interval = TimeConstants.MILLISECONDSPERSECOND;
    /* access modifiers changed from: private */
    public static AccelerometerListener listener;
    private static boolean running = false;
    private static Sensor sensor;
    private static SensorEventListener sensorEventListener = new SensorEventListener() {
        private float force = 0.0f;
        private long lastShake = 0;
        private long lastUpdate = 0;
        private float lastX = 0.0f;
        private float lastY = 0.0f;
        private float lastZ = 0.0f;
        private long now = 0;
        private long timeDiff = 0;
        private float x = 0.0f;
        private float y = 0.0f;
        private float z = 0.0f;

        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }

        public void onSensorChanged(SensorEvent event) {
            this.now = event.timestamp;
            this.x = event.values[0];
            this.y = event.values[1];
            this.z = event.values[2];
            if (this.lastUpdate == 0) {
                this.lastUpdate = this.now;
                this.lastShake = this.now;
                this.lastX = this.x;
                this.lastY = this.y;
                this.lastZ = this.z;
                AccelerometerManager.listener.didAccelerate(this.x, this.y, this.z);
                return;
            }
            this.timeDiff = this.now - this.lastUpdate;
            if (this.timeDiff > 500) {
                this.force = Math.abs(((((this.x + this.y) + this.z) - this.lastX) - this.lastY) - this.lastZ) / ((float) this.timeDiff);
                AccelerometerManager.listener.didAccelerate(this.x, this.y, this.z);
                if (this.force > AccelerometerManager.threshold) {
                    if (this.now - this.lastShake >= ((long) AccelerometerManager.interval)) {
                        AccelerometerManager.listener.didShake(this.force);
                    }
                    this.lastShake = this.now;
                }
                this.lastX = this.x;
                this.lastY = this.y;
                this.lastZ = this.z;
                this.lastUpdate = this.now;
            }
        }
    };
    private static SensorManager sensorManager;
    private static Boolean supported;
    /* access modifiers changed from: private */
    public static float threshold = 0.2f;
    protected Activity amContext;

    public static boolean isListening() {
        return running;
    }

    public static void stopListening() {
        running = false;
        try {
            if (sensorManager != null && sensorEventListener != null) {
                sensorManager.unregisterListener(sensorEventListener);
            }
        } catch (Exception e) {
        }
    }

    public static boolean isSupported() {
        if (supported == null) {
            if (MMAdViewOverlayActivity.getContext() != null) {
                sensorManager = (SensorManager) MMAdViewOverlayActivity.getContext().getSystemService("sensor");
                supported = new Boolean(sensorManager.getSensorList(1).size() > 0);
            } else {
                supported = Boolean.FALSE;
            }
        }
        return supported.booleanValue();
    }

    public static void configure(int threshold2, int interval2) {
        threshold = (float) threshold2;
        interval = interval2;
    }

    public static void startListening(AccelerometerListener accelerometerListener) {
        sensorManager = (SensorManager) MMAdViewOverlayActivity.getContext().getSystemService("sensor");
        List<Sensor> sensors = sensorManager.getSensorList(1);
        if (sensors.size() > 0) {
            sensor = sensors.get(0);
            running = sensorManager.registerListener(sensorEventListener, sensor, 1);
            listener = accelerometerListener;
        }
    }

    public static void startListening(AccelerometerListener accelerometerListener, int threshold2, int interval2) {
        configure(threshold2, interval2);
        startListening(accelerometerListener);
    }
}
