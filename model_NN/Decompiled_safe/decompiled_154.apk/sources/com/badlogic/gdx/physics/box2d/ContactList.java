package com.badlogic.gdx.physics.box2d;

import java.util.ArrayList;

public class ContactList {
    private final ArrayList<Contact> contacts = new ArrayList<>();
    private final ArrayList<Contact> freeContacts = new ArrayList<>();
    private int freeIdx = 0;

    /* access modifiers changed from: protected */
    public void add(long addr) {
    }

    /* access modifiers changed from: protected */
    public void remove(long addr) {
    }

    public int size() {
        return this.contacts.size();
    }

    public Contact get(int index) {
        return this.contacts.get(index);
    }
}
