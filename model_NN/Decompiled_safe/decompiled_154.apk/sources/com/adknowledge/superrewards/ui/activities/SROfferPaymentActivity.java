package com.adknowledge.superrewards.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.adknowledge.superrewards.SRResources;
import com.adknowledge.superrewards.Utils;
import com.adknowledge.superrewards.model.SROffer;
import com.adknowledge.superrewards.model.SRPricePoint;
import com.adknowledge.superrewards.tracking.SRAppInstallTracker;
import com.adknowledge.superrewards.web.SRClient;
import com.adknowledge.superrewards.web.SRRequest;

public class SROfferPaymentActivity extends Activity {
    /* access modifiers changed from: private */
    public SROffer offer;
    private TextView textButton;
    private TextView textView;
    private TextView textView2;
    private TextView textView3;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        boolean customTitleSupported = requestWindowFeature(7);
        setContentView(SRResources.layout.sr_offer_payment_activity_layout);
        this.offer = (SROffer) getIntent().getSerializableExtra(SROffer.OFFER);
        ((ImageView) findViewById(SRResources.id.SROfferListItemImageView)).setImageDrawable(Utils.getImage(getApplicationContext(), this.offer.getImage()));
        this.textView = (TextView) findViewById(SRResources.id.SROfferName);
        this.textView.setText(this.offer.getName().trim());
        this.textView2 = (TextView) findViewById(SRResources.id.SROfferPaymentDescription);
        this.textView2.setText(this.offer.getDescription().replace("%points%", this.offer.getCurrency().toLowerCase()).replace("%POINTS%", this.offer.getCurrency().toLowerCase()));
        this.textView3 = (TextView) findViewById(SRResources.id.SROfferPaymentRequirements);
        this.textView3.setText(this.offer.getRequirements().replace("%points%", this.offer.getCurrency().toLowerCase()).replace("%POINTS%", this.offer.getCurrency().toLowerCase()));
        this.textButton = (TextView) findViewById(SRResources.id.SROfferPaymentBuyButton);
        this.textButton.setText(Html.fromHtml("<small>complete the offer now for<br/></small>"));
        this.textButton.append(Html.fromHtml("<big>" + this.offer.getPayout().toLowerCase().trim() + " " + this.offer.getCurrency().toLowerCase().trim() + "</big>"));
        this.textButton.setOnClickListener(getBuyButtonOnCLickListener());
        customTitleBar("earn more " + this.offer.getCurrency().trim().toLowerCase(), Boolean.valueOf(customTitleSupported));
    }

    private View.OnClickListener getBuyButtonOnCLickListener() {
        return new View.OnClickListener() {
            public void onClick(View arg0) {
                SRRequest request = SRClient.getInstance().createRequest();
                request.setCommand(SRRequest.Command.METHOD);
                request.setCall(SRRequest.Call.CLICK);
                request.addInnerParam(SRPricePoint.AMOUNT, SROfferPaymentActivity.this.offer.getPayout());
                request.addInnerParam(SROffer.CLICK_URL, String.valueOf(SROfferPaymentActivity.this.offer.getUrl()) + "&amount=" + SROfferPaymentActivity.this.offer.getPayout());
                Intent i = new Intent("android.intent.action.VIEW");
                i.setData(Uri.parse(request.getUrlWithJson(SROfferPaymentActivity.this.getApplicationContext(), SRAppInstallTracker.h)));
                SROfferPaymentActivity.this.startActivity(i);
            }
        };
    }

    public void customTitleBar(String blurb, Boolean customTitleSupported) {
        if (customTitleSupported.booleanValue()) {
            getWindow().setFeatureInt(7, SRResources.layout.sr_custom_title);
            ((TextView) findViewById(SRResources.id.SRCustomTitleLeft)).setText(blurb);
            ((ProgressBar) findViewById(SRResources.id.SRTitleProgressBar)).setVisibility(8);
        }
    }
}
