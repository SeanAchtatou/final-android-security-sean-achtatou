package com.adknowledge.superrewards.model;

import com.adknowledge.superrewards.SRResources;

public class SRImageByName {
    static int id;

    public static int getDirectPayImageByName(String name) {
        String name2 = name.toLowerCase();
        if (name2.equals("paypal")) {
            return SRResources.drawable.sr_paypal_directpay_logo;
        }
        if (name2.equals("credit card")) {
            return SRResources.drawable.sr_creditcard_directpay_logo;
        }
        if (name2.equals("google")) {
            return SRResources.drawable.sr_googlecheckout_directpay_logo;
        }
        if (name2.equals("zong")) {
            return SRResources.drawable.sr_zong_directpay_logo;
        }
        return 0;
    }
}
