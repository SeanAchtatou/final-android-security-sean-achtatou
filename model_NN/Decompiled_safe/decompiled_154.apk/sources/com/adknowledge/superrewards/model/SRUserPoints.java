package com.adknowledge.superrewards.model;

import android.content.Context;
import android.widget.Toast;
import com.adknowledge.superrewards.tracking.SRAppInstallTracker;
import com.adknowledge.superrewards.web.SRClient;
import com.adknowledge.superrewards.web.SRRequest;
import org.anddev.andengine.entity.layer.tiled.tmx.util.constants.TMXConstants;
import org.anddev.andengine.level.util.constants.LevelConstants;
import org.json.JSONException;
import org.json.JSONObject;

public class SRUserPoints {
    private Context ctx;
    private int newpoints = 0;
    private int totalpoints = 0;

    public SRUserPoints(Context ctx2) {
        this.ctx = ctx2;
    }

    public void updatePoints(String hparam, String uid) {
        SRAppInstallTracker instance = SRAppInstallTracker.getInstance(this.ctx, hparam);
        SRRequest request = SRClient.getInstance().createRequest();
        request.setCommand(SRRequest.Command.METHOD);
        request.setCall(SRRequest.Call.CHECK_POINTS);
        request.addInnerParam(LevelConstants.TAG_LEVEL_ATTRIBUTE_UID, uid);
        request.execute(this.ctx, hparam);
        try {
            JSONObject innerjson = new JSONObject(request.getResult()).getJSONObject(TMXConstants.TAG_DATA);
            this.newpoints = innerjson.getInt("new");
            this.totalpoints = innerjson.getInt("total");
        } catch (JSONException e) {
            Toast.makeText(this.ctx, "There was a communication problem. Please try again later.", 2000).show();
        }
    }

    public int getNewPoints() {
        return this.newpoints;
    }

    public int getTotalPoints() {
        return this.totalpoints;
    }
}
