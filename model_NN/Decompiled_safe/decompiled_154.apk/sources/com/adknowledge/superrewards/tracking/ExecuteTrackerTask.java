package com.adknowledge.superrewards.tracking;

import android.os.AsyncTask;
import com.adknowledge.superrewards.Utils;
import java.io.IOException;
import java.net.MalformedURLException;

public class ExecuteTrackerTask extends AsyncTask<String, Void, Void> {
    /* access modifiers changed from: protected */
    public Void doInBackground(String... url) {
        try {
            Utils.fetch(url[0]);
            return null;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
