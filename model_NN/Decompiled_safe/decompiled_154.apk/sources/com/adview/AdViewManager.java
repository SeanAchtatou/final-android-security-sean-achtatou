package com.adview;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.util.Log;
import com.adview.AdViewTargeting;
import com.adview.obj.Extra;
import com.adview.obj.Ration;
import com.adview.util.AdViewUtil;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import org.anddev.andengine.util.StreamUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AdViewManager {
    private static final String PREFS_STRING_CONFIG = "config";
    private static final String PREFS_STRING_TIMESTAMP = "timestamp";
    private static long configExpireTimeout = 1800000;
    private static boolean youmiInit = true;
    private WeakReference<Context> contextReference;
    private Extra extra;
    public String keyAdView;
    public String localeString;
    public Location location;
    private List<Ration> rationsList;
    private List<Ration> rationsList_pri;
    Iterator<Ration> rollover_pri;
    Iterator<Ration> rollovers;
    private double totalWeight = 0.0d;

    public AdViewManager(WeakReference<Context> contextReference2, String keyAdView2) {
        Log.i("Android", "Creating weivda reganam...");
        this.contextReference = contextReference2;
        this.keyAdView = keyAdView2;
        if (AdViewTargeting.getUpdateMode() == AdViewTargeting.UpdateMode.DEFAULT) {
            setConfigExpireTimeout(1800000);
        } else if (AdViewTargeting.getUpdateMode() == AdViewTargeting.UpdateMode.EVERYTIME) {
            setConfigExpireTimeout(-1);
        }
    }

    public static void setYoumiInit(boolean flag) {
        youmiInit = flag;
    }

    public static boolean getYoumiInit() {
        return youmiInit;
    }

    public static void setConfigExpireTimeout(long configExpireTimeout2) {
        configExpireTimeout = configExpireTimeout2;
    }

    public Extra getExtra() {
        if (this.totalWeight <= 0.0d) {
            return null;
        }
        return this.extra;
    }

    public Ration getRation() {
        double r = new Random().nextDouble() * this.totalWeight;
        double s = 0.0d;
        Iterator<Ration> it = this.rationsList.iterator();
        Ration ration = null;
        while (it.hasNext()) {
            ration = it.next();
            s += ration.weight;
            if (s >= r) {
                break;
            }
        }
        return ration;
    }

    public Ration getRollover() {
        if (this.rollovers == null) {
            return null;
        }
        Ration ration = null;
        if (this.rollovers.hasNext()) {
            ration = this.rollovers.next();
        }
        return ration;
    }

    public Ration getRollover_pri() {
        int max = 100000000;
        if (this.rollover_pri == null) {
            return null;
        }
        Ration ration_pri = null;
        while (this.rollover_pri.hasNext()) {
            Ration ration = this.rollover_pri.next();
            if (ration.priority < max) {
                ration_pri = ration;
                max = ration.priority;
            }
        }
        return ration_pri;
    }

    public void resetRollover_pri() {
        this.rollovers = this.rationsList.iterator();
        this.rollover_pri = this.rationsList_pri.iterator();
    }

    public void resetRollover() {
        this.rollovers = this.rationsList.iterator();
    }

    public void fetchConfig() {
        Context context = this.contextReference.get();
        if (context != null) {
            SharedPreferences adViewPrefs = context.getSharedPreferences(this.keyAdView, 0);
            String jsonString = adViewPrefs.getString(PREFS_STRING_CONFIG, null);
            long timestamp = adViewPrefs.getLong(PREFS_STRING_TIMESTAMP, -1);
            if (jsonString == null || configExpireTimeout == -1 || System.currentTimeMillis() >= configExpireTimeout + timestamp) {
                try {
                    HttpEntity entity = new DefaultHttpClient().execute(new HttpGet(String.format(AdViewUtil.urlConfig, this.keyAdView, Integer.valueOf((int) AdViewUtil.VERSION)))).getEntity();
                    if (entity != null) {
                        jsonString = convertStreamToString(entity.getContent());
                        SharedPreferences.Editor editor = adViewPrefs.edit();
                        editor.putString(PREFS_STRING_CONFIG, jsonString);
                        editor.putLong(PREFS_STRING_TIMESTAMP, System.currentTimeMillis());
                        editor.commit();
                    }
                } catch (IOException | ClientProtocolException e) {
                }
            }
            parseConfigurationString(jsonString);
        }
    }

    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is), StreamUtils.IO_BUFFER_SIZE);
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String line = reader.readLine();
                if (line == null) {
                    try {
                        is.close();
                        return sb.toString();
                    } catch (IOException e) {
                        return null;
                    }
                } else {
                    sb.append(String.valueOf(line) + "\n");
                }
            } catch (IOException e2) {
                try {
                    is.close();
                    return null;
                } catch (IOException e3) {
                    return null;
                }
            } catch (Throwable th) {
                try {
                    is.close();
                    throw th;
                } catch (IOException e4) {
                    return null;
                }
            }
        }
    }

    private void parseConfigurationString(String jsonString) {
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "Received jsonString: " + jsonString);
        }
        try {
            JSONObject json = new JSONObject(jsonString);
            parseExtraJson(json.getJSONObject("extra"));
            parseRationsJson(json.getJSONArray("rations"));
        } catch (JSONException e) {
            this.extra = new Extra();
        } catch (NullPointerException e2) {
            this.extra = new Extra();
        }
    }

    private void parseExtraJson(JSONObject json) {
        Extra extra2 = new Extra();
        try {
            extra2.cycleTime = json.getInt("cycle_time");
            extra2.transition = json.getInt("transition");
            extra2.report = json.getString("report");
            AdViewUtil.urlImpression = "http://" + extra2.report + "/agent/agent2.php?appid=%s&nid=%s&type=%d&uuid=%s&country_code=%s&appver=%d&client=0";
            AdViewUtil.urlClick = "http://" + extra2.report + "/agent/agent3.php?appid=%s&nid=%s&type=%d&uuid=%s&country_code=%s&appver=%d&client=0";
            JSONObject backgroundColor = json.getJSONObject("background_color_rgb");
            extra2.bgRed = backgroundColor.getInt("red");
            extra2.bgGreen = backgroundColor.getInt("green");
            extra2.bgBlue = backgroundColor.getInt("blue");
            extra2.bgAlpha = backgroundColor.getInt("alpha") * AdViewUtil.VERSION;
            JSONObject textColor = json.getJSONObject("text_color_rgb");
            extra2.fgRed = textColor.getInt("red");
            extra2.fgGreen = textColor.getInt("green");
            extra2.fgBlue = textColor.getInt("blue");
            extra2.fgAlpha = textColor.getInt("alpha") * AdViewUtil.VERSION;
        } catch (JSONException e) {
        }
        this.extra = extra2;
    }

    private void parseRationsJson(JSONArray json) {
        List<Ration> rationsList2 = new ArrayList<>();
        List<Ration> rationsList_pri2 = new ArrayList<>();
        this.totalWeight = 0.0d;
        int i = 0;
        while (i < json.length()) {
            try {
                JSONObject jsonRation = json.getJSONObject(i);
                if (jsonRation != null) {
                    Ration ration = new Ration();
                    ration.nid = jsonRation.getString("nid");
                    ration.type = jsonRation.getInt("type");
                    ration.name = jsonRation.getString("nname");
                    ration.weight = (double) jsonRation.getInt("weight");
                    ration.priority = jsonRation.getInt("priority");
                    switch (ration.type) {
                        case 22:
                        case AdViewUtil.NETWORK_TYPE_SMARTAD /*29*/:
                        case 35:
                            ration.key = jsonRation.getString("key");
                            ration.key2 = jsonRation.getString("key2");
                            break;
                        case AdViewUtil.NETWORK_TYPE_ADVIEWAD /*28*/:
                            ration.key = jsonRation.getString("key");
                            ration.key2 = jsonRation.getString("key2");
                            ration.type2 = jsonRation.getInt("type2");
                            ration.logo = jsonRation.getString("logo");
                            break;
                        default:
                            ration.key = jsonRation.getString("key");
                            break;
                    }
                    this.totalWeight += ration.weight;
                    rationsList2.add(ration);
                    if (ration.priority > 0) {
                        rationsList_pri2.add(ration);
                    }
                }
                i++;
            } catch (JSONException e) {
            }
        }
        Collections.sort(rationsList2);
        this.rationsList = rationsList2;
        this.rollovers = this.rationsList.iterator();
        Collections.sort(rationsList_pri2);
        this.rationsList_pri = rationsList_pri2;
        this.rollover_pri = rationsList_pri2.iterator();
    }
}
