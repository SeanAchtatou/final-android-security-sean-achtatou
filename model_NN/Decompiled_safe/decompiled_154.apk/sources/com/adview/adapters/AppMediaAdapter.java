package com.adview.adapters;

import android.app.Activity;
import android.util.Log;
import cn.appmedia.ad.AdManager;
import cn.appmedia.ad.AdViewListener;
import cn.appmedia.ad.BannerAdView;
import com.adview.AdViewLayout;
import com.adview.AdViewTargeting;
import com.adview.obj.Ration;
import com.adview.util.AdViewUtil;

public class AppMediaAdapter extends AdViewAdapter implements AdViewListener {
    public AppMediaAdapter(AdViewLayout adViewLayout, Ration ration) {
        super(adViewLayout, ration);
        AdManager.setAid(ration.key);
    }

    public void handle() {
        Activity activity;
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "Into AppMedia");
        }
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null && (activity = adViewLayout.activityReference.get()) != null) {
            BannerAdView adView = new BannerAdView(activity, 0);
            adView.requestAd();
            adView.setAdListener(this);
        }
    }

    public void onReceiveAdFailure(BannerAdView arg0) {
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "AppMedia failure");
        }
        arg0.setAdListener((AdViewListener) null);
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null) {
            adViewLayout.adViewManager.resetRollover_pri();
            adViewLayout.rollover_pri();
        }
    }

    public void onReceiveAdSuccess(BannerAdView arg0) {
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "AppMedia success");
        }
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null) {
            adViewLayout.adViewManager.resetRollover();
            adViewLayout.handler.post(new AdViewLayout.ViewAdRunnable(adViewLayout, arg0));
            adViewLayout.rotateThreadedDelayed();
        }
    }
}
