package com.adview.adapters;

import android.graphics.Color;
import android.util.AttributeSet;
import android.util.Log;
import com.adview.AdViewLayout;
import com.adview.AdViewTargeting;
import com.adview.obj.Extra;
import com.adview.obj.Ration;
import com.adview.util.AdViewUtil;
import com.casee.adsdk.CaseeAdView;
import org.anddev.andengine.util.constants.TimeConstants;

public class CaseeAdapter extends AdViewAdapter implements CaseeAdView.AdListener {
    public CaseeAdapter(AdViewLayout adViewLayout, Ration ration) {
        super(adViewLayout, ration);
    }

    public void handle() {
        CaseeAdView adView;
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "Into CASEE");
        }
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null) {
            Extra extra = adViewLayout.extra;
            int bgColor = Color.rgb(extra.bgRed, extra.bgGreen, extra.bgBlue);
            int fgColor = Color.rgb(extra.fgRed, extra.fgGreen, extra.fgBlue);
            if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
                adView = new CaseeAdView(adViewLayout.getContext(), (AttributeSet) null, 0, this.ration.key, true, extra.cycleTime * TimeConstants.MILLISECONDSPERSECOND, bgColor, fgColor, false);
            } else if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.NORMAL) {
                adView = new CaseeAdView(adViewLayout.getContext(), (AttributeSet) null, 0, this.ration.key, false, extra.cycleTime * TimeConstants.MILLISECONDSPERSECOND, bgColor, fgColor, false);
            } else {
                adView = new CaseeAdView(adViewLayout.getContext(), (AttributeSet) null, 0, this.ration.key, false, extra.cycleTime * TimeConstants.MILLISECONDSPERSECOND, bgColor, fgColor, false);
            }
            adView.setListener(this);
        }
    }

    public void onFailedToReceiveAd(CaseeAdView arg0) {
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "CASEE fail");
        }
        arg0.setListener((CaseeAdView.AdListener) null);
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null) {
            adViewLayout.adViewManager.resetRollover_pri();
            adViewLayout.rollover_pri();
        }
    }

    public void onFailedToReceiveRefreshAd(CaseeAdView arg0) {
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "success=>fail for fresh");
        }
        arg0.setListener((CaseeAdView.AdListener) null);
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null) {
            adViewLayout.adViewManager.resetRollover_pri();
            adViewLayout.rollover_pri();
        }
    }

    public void onReceiveAd(CaseeAdView arg0) {
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, " CASEE success");
        }
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null) {
            adViewLayout.adViewManager.resetRollover();
            adViewLayout.handler.post(new AdViewLayout.ViewAdRunnable(adViewLayout, arg0));
            adViewLayout.rotateThreadedDelayed();
        }
    }

    public void onReceiveRefreshAd(CaseeAdView arg0) {
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, " fail=>success for fresh");
        }
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null) {
            adViewLayout.adViewManager.resetRollover();
            adViewLayout.handler.post(new AdViewLayout.ViewAdRunnable(adViewLayout, arg0));
            adViewLayout.rotateThreadedDelayed();
        }
    }
}
