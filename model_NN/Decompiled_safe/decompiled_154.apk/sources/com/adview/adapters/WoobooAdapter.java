package com.adview.adapters;

import android.graphics.Color;
import android.util.Log;
import com.adview.AdViewLayout;
import com.adview.AdViewTargeting;
import com.adview.obj.Extra;
import com.adview.obj.Ration;
import com.adview.util.AdViewUtil;
import com.wooboo.adlib_android.AdListener;
import com.wooboo.adlib_android.WoobooAdView;

public class WoobooAdapter extends AdViewAdapter implements AdListener {
    public WoobooAdapter(AdViewLayout adViewLayout, Ration ration) {
        super(adViewLayout, ration);
    }

    public void handle() {
        WoobooAdView adView;
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "Into Wooboo");
        }
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null) {
            Extra extra = adViewLayout.extra;
            int bgColor = Color.rgb(extra.bgRed, extra.bgGreen, extra.bgBlue);
            int fgColor = Color.rgb(extra.fgRed, extra.fgGreen, extra.fgBlue);
            if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
                adView = new WoobooAdView(adViewLayout.getContext(), this.ration.key, bgColor, fgColor, true, 600);
            } else if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.NORMAL) {
                adView = new WoobooAdView(adViewLayout.getContext(), this.ration.key, bgColor, fgColor, false, 600);
            } else {
                adView = new WoobooAdView(adViewLayout.getContext(), this.ration.key, bgColor, fgColor, false, 600);
            }
            adView.setHorizontalScrollBarEnabled(false);
            adView.setVerticalScrollBarEnabled(false);
            adView.setAdListener(this);
        }
    }

    public void onFailedToReceiveAd(Object arg0) {
        WoobooAdView adView = (WoobooAdView) arg0;
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "Woboo failure");
        }
        adView.setAdListener((AdListener) null);
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null) {
            adViewLayout.adViewManager.resetRollover();
            adViewLayout.handler.post(new AdViewLayout.ViewAdRunnable(adViewLayout, adView));
            adViewLayout.rotateThreadedDelayed();
        }
    }

    public void onReceiveAd(Object arg0) {
        WoobooAdView adView = (WoobooAdView) arg0;
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "Wooboo success");
        }
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null) {
            adViewLayout.adViewManager.resetRollover();
            adViewLayout.handler.post(new AdViewLayout.ViewAdRunnable(adViewLayout, adView));
            adViewLayout.rotateThreadedDelayed();
        }
    }
}
