package com.adview.adapters;

import android.app.Activity;
import android.util.Log;
import com.adview.AdViewLayout;
import com.adview.AdViewTargeting;
import com.adview.obj.Ration;
import com.adview.util.AdViewUtil;
import com.mt.airad.AirAD;

public class AirAdAdapter extends AdViewAdapter {
    private int area;

    public AirAdAdapter(AdViewLayout adViewLayout, Ration ration) {
        super(adViewLayout, ration);
        AirAD.setGlobalParameter(ration.key, 30.0d);
    }

    public void handle() {
        Activity activity;
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "Into AirAD");
        }
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null && (activity = adViewLayout.activityReference.get()) != null) {
            if (AdViewTargeting.getAdArea() == AdViewTargeting.AdArea.BOTTOM) {
                this.area = 11;
            } else {
                this.area = 12;
            }
            new AirAD(activity, this.area, 1);
            adViewLayout.adViewManager.resetRollover();
            adViewLayout.rotateThreadedDelayed();
        }
    }
}
