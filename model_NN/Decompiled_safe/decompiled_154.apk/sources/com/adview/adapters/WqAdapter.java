package com.adview.adapters;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.adview.AdViewLayout;
import com.adview.AdViewTargeting;
import com.adview.obj.Ration;
import com.adview.util.AdViewUtil;
import com.wqmobile.sdk.widget.ADView;

public class WqAdapter extends AdViewAdapter {
    public WqAdapter(AdViewLayout adViewLayout, Ration ration) {
        super(adViewLayout, ration);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: SimpleMethodDetails{com.adview.AdViewLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void}
     arg types: [com.wqmobile.sdk.widget.ADView, android.widget.RelativeLayout$LayoutParams]
     candidates:
      ClspMth{android.view.ViewGroup.addView(android.view.View, int):void}
      SimpleMethodDetails{com.adview.AdViewLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void} */
    public void handle() {
        Activity activity;
        String mode;
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "Into WQ");
        }
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null && (activity = adViewLayout.activityReference.get()) != null) {
            String key = new String(this.ration.key);
            String key2 = new String(this.ration.key2);
            if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
                mode = new String("Y");
            } else {
                mode = new String("N");
            }
            ADView view = new ADView(activity);
            view.Init(String.valueOf(new String("<?xml version=\"1.0\" encoding=\"utf-8\"?>")) + "<AppSettings>" + "<AppID>" + key + "</AppID>" + "<PublisherID>" + key2 + "</PublisherID>" + "<URL></URL>" + "<AppStoreURL></AppStoreURL>" + "<BackgroundColor></BackgroundColor>" + "<TextColor></TextColor>" + "<UseLocationInfo></UseLocationInfo>" + "<RefreshRate>60</RefreshRate>" + "<TestMode>" + mode + "</TestMode>" + "<NextADCount>0</NextADCount>" + "<LoopTimes>2</LoopTimes>" + "<AcceptsOfflineAD>N</AcceptsOfflineAD>" + "<OfflineADCount>0</OfflineADCount>" + "<OfflineADSurvivalDays>0</OfflineADSurvivalDays>" + "<Width></Width>" + "<Height></Height>" + "<UseInternalBrowser>false</UseInternalBrowser>" + "<FittingStrategy>size</FittingStrategy>" + "</AppSettings>");
            adViewLayout.addView((View) view, (ViewGroup.LayoutParams) new RelativeLayout.LayoutParams(-1, 50));
            adViewLayout.adViewManager.resetRollover();
            adViewLayout.rotateThreadedDelayed();
        }
    }
}
