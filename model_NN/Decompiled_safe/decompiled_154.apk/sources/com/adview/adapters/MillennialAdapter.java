package com.adview.adapters;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;
import com.adview.AdViewLayout;
import com.adview.AdViewTargeting;
import com.adview.obj.Extra;
import com.adview.obj.Ration;
import com.adview.util.AdViewUtil;
import com.millennialmedia.android.MMAdView;
import java.util.Hashtable;

public class MillennialAdapter extends AdViewAdapter implements MMAdView.MMAdListener {
    public MillennialAdapter(AdViewLayout adViewLayout, Ration ration) {
        super(adViewLayout, ration);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.millennialmedia.android.MMAdView.<init>(android.app.Activity, java.lang.String, java.lang.String, int, boolean, java.util.Hashtable<java.lang.String, java.lang.String>):void
     arg types: [android.app.Activity, java.lang.String, java.lang.String, int, int, java.util.Hashtable<java.lang.String, java.lang.String>]
     candidates:
      com.millennialmedia.android.MMAdView.<init>(android.app.Activity, java.lang.String, java.lang.String, int, boolean, boolean):void
      com.millennialmedia.android.MMAdView.<init>(android.app.Activity, java.lang.String, java.lang.String, int, boolean, java.util.Hashtable<java.lang.String, java.lang.String>):void */
    public void handle() {
        String keywords;
        MMAdView adView;
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null) {
            Hashtable<String, String> map = new Hashtable<>();
            AdViewTargeting.Gender gender = AdViewTargeting.getGender();
            if (gender == AdViewTargeting.Gender.MALE) {
                map.put("gender", "male");
            } else if (gender == AdViewTargeting.Gender.FEMALE) {
                map.put("gender", "female");
            }
            int age = AdViewTargeting.getAge();
            if (age != -1) {
                map.put("age", String.valueOf(age));
            }
            String postalCode = AdViewTargeting.getPostalCode();
            if (!TextUtils.isEmpty(postalCode)) {
                map.put("zip", postalCode);
            }
            if (AdViewTargeting.getKeywordSet() != null) {
                keywords = TextUtils.join(",", AdViewTargeting.getKeywordSet());
            } else {
                keywords = AdViewTargeting.getKeywords();
            }
            if (!TextUtils.isEmpty(keywords)) {
                map.put("keywords", keywords);
            }
            map.put("vendor", "adview");
            Extra extra = adViewLayout.extra;
            if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
                adView = new MMAdView((Activity) adViewLayout.getContext(), this.ration.key, "MMBannerAdTop", extra.cycleTime, true, map);
            } else if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.NORMAL) {
                adView = new MMAdView((Activity) adViewLayout.getContext(), this.ration.key, "MMBannerAdTop", extra.cycleTime, false, map);
            } else {
                adView = new MMAdView((Activity) adViewLayout.getContext(), this.ration.key, "MMBannerAdTop", extra.cycleTime, false, map);
            }
            adView.setListener(this);
            adView.callForAd();
            adView.setHorizontalScrollBarEnabled(false);
            adView.setVerticalScrollBarEnabled(false);
        }
    }

    public void MMAdReturned(MMAdView adView) {
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "Millennial success");
        }
        adView.setListener(null);
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null) {
            adViewLayout.adViewManager.resetRollover();
            adViewLayout.handler.post(new AdViewLayout.ViewAdRunnable(adViewLayout, adView));
            adViewLayout.rotateThreadedDelayed();
        }
    }

    public void MMAdFailed(MMAdView adView) {
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "Millennial failure");
        }
        adView.setListener(null);
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null) {
            adViewLayout.adViewManager.resetRollover_pri();
            adViewLayout.rollover_pri();
        }
    }

    public void MMAdClickedToNewBrowser(MMAdView adview) {
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "Millennial Ad clicked, new browser launched");
        }
    }

    public void MMAdClickedToOverlay(MMAdView adview) {
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "Millennial Ad Clicked to overlay");
        }
    }

    public void MMAdOverlayLaunched(MMAdView adview) {
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "Millennial Ad Overlay Launched");
        }
    }
}
