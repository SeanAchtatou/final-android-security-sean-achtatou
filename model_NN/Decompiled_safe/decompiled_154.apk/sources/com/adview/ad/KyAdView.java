package com.adview.ad;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Xml;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.TextView;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlSerializer;

public class KyAdView extends ViewGroup {
    static final String TAG = "AdViewAd";
    ImageView adImage = null;
    TextView adText = null;
    private View.OnClickListener adView_listener = new View.OnClickListener() {
        public void onClick(View v) {
            int len;
            Context context = v.getContext();
            String url = KyAdView.this.retAdBean.getAdLink();
            if (url != null && url.length() >= (len = new String("http://").length())) {
                if (url.substring(0, len - 1).compareTo(new String("http://")) == 0) {
                }
                if (KyAdView.this.applyAdBean.getTestMode() == 0) {
                    KyAdView.this.clickServer(1, 0, 1, KyAdView.this.agent2);
                }
                Log.i(KyAdView.TAG, "Begin OnClick action.....");
                KyAdView.this.openWebBrowser(url, context);
            }
        }
    };
    private String address = null;
    /* access modifiers changed from: private */
    public String agent1 = null;
    /* access modifiers changed from: private */
    public String agent1test = null;
    /* access modifiers changed from: private */
    public String agent2 = null;
    private String appId = null;
    /* access modifiers changed from: private */
    public ApplyAdBean applyAdBean = null;
    private int backGroundColor = -16776961;
    /* access modifiers changed from: private */
    public Bitmap bitmap = null;
    /* access modifiers changed from: private */
    public String getString = null;
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    if (KyAdView.this.retAdBean.getAdShowText() == null) {
                        if (KyAdView.this.mOnAdListener != null) {
                            KyAdView.this.mOnAdListener.onReceivedAd(KyAdView.this);
                            break;
                        }
                    } else {
                        KyAdView.this.mOnAdListener.onReceivedAd(KyAdView.this);
                        KyAdView.this.adText.setText(KyAdView.this.retAdBean.getAdShowText());
                        if (KyAdView.this.bitmap != null) {
                            KyAdView.this.adImage.setImageBitmap(KyAdView.this.bitmap);
                        }
                        KyAdView.this.startLayoutAnimation();
                        break;
                    }
                    break;
                case 1:
                    if (KyAdView.this.mOnAdListener != null) {
                        KyAdView.this.mOnAdListener.onReceivedAd(KyAdView.this);
                        break;
                    }
                    break;
                case 2:
                    if (KyAdView.this.mOnAdListener != null) {
                        KyAdView.this.mOnAdListener.onReceivedAd(KyAdView.this);
                        break;
                    }
                    break;
            }
            super.handleMessage(msg);
        }
    };
    private int internal = 50;
    private boolean isTestMode = false;
    TextView logoText = null;
    private Client mClient = new Client();
    /* access modifiers changed from: private */
    public onAdListener mOnAdListener = null;
    private Thread mThread = null;
    /* access modifiers changed from: private */
    public RetAdBean retAdBean = null;
    private int screenHeight = 0;
    private int screenWidth = 0;
    private int textColor = -65536;
    private int topPadding = 0;
    /* access modifiers changed from: private */
    public String writeString = null;
    /* access modifiers changed from: private */
    public String xmlCp = null;

    public interface onAdListener {
        void onConnectFailed(KyAdView kyAdView);

        void onReceivedAd(KyAdView kyAdView);
    }

    public class Client implements Runnable {
        Client() {
        }

        public void run() {
            KyAdView.this.writeString = writeApplyAdXml();
            if (KyAdView.this.writeString != null) {
                if (KyAdView.this.applyAdBean.getTestMode() == 1) {
                    KyAdView.this.getString = getResponse(KyAdView.this.writeString, KyAdView.this.agent1test);
                } else {
                    KyAdView.this.getString = getResponse(KyAdView.this.writeString, KyAdView.this.agent1);
                }
            }
            if (KyAdView.this.getString != null) {
                KyAdView.this.xmlCp = KyAdView.this.getString.replaceAll("\\n", "").replaceAll("\\r", "");
                KyAdView.this.xmlCp = KyAdView.this.xmlCp.replace("&amp;", "#$amp;");
            } else {
                KyAdView.this.notifyConncetFail();
            }
            if (KyAdView.this.xmlCp != null) {
                KyAdView.this.retAdBean = readXML(KyAdView.this.xmlCp);
            } else {
                KyAdView.this.retAdBean = null;
            }
            if (KyAdView.this.retAdBean != null) {
                display();
                KyAdView.this.frushAd();
                if (KyAdView.this.applyAdBean.getTestMode() == 0) {
                    reportServer(0, 1, 0, KyAdView.this.agent2);
                }
            } else if (KyAdView.this.xmlCp != null) {
                if (KyAdView.this.mOnAdListener != null) {
                    KyAdView.this.mOnAdListener.onReceivedAd(KyAdView.this);
                }
                KyAdView.this.notifyReceiveAdError();
            }
        }

        private String writeApplyAdXml() {
            XmlSerializer serializer = Xml.newSerializer();
            StringWriter writer = new StringWriter();
            try {
                serializer.setOutput(writer);
                serializer.startDocument(null, true);
                serializer.startTag("", "application");
                serializer.startTag("", "idApp");
                serializer.text(KyAdView.this.applyAdBean.getAppId());
                serializer.endTag("", "idApp");
                serializer.startTag("", "system");
                serializer.text(Integer.toString(KyAdView.this.applyAdBean.getSystem()));
                serializer.endTag("", "system");
                serializer.endTag("", "application");
                serializer.endDocument();
                return writer.toString();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        private void reportServer(int type, int display, int click, String agent) {
            getResponse(displayReport(type, display, click), agent);
        }

        private String displayReport(int type, int display, int click) {
            XmlSerializer serializer = Xml.newSerializer();
            StringWriter writer = new StringWriter();
            try {
                serializer.setOutput(writer);
                serializer.startDocument(null, true);
                serializer.startTag("", "application");
                serializer.startTag("", "idApp");
                serializer.text(KyAdView.this.applyAdBean.getAppId());
                serializer.endTag("", "idApp");
                serializer.startTag("", "idAd");
                serializer.text(KyAdView.this.retAdBean.getIdAd());
                serializer.endTag("", "idAd");
                serializer.startTag("", "system");
                serializer.text(Integer.toString(KyAdView.this.applyAdBean.getSystem()));
                serializer.endTag("", "system");
                serializer.startTag("", "reportType");
                serializer.text(Integer.toString(type));
                serializer.endTag("", "reportType");
                serializer.startTag("", "display");
                serializer.text(Integer.toString(display));
                serializer.endTag("", "display");
                serializer.startTag("", "click");
                serializer.text(Integer.toString(click));
                serializer.endTag("", "click");
                serializer.endTag("", "application");
                serializer.endDocument();
                return writer.toString();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        private void display() {
            switch (KyAdView.this.retAdBean.getAdShowType()) {
                case 0:
                default:
                    return;
                case 1:
                case 2:
                    clientBitMap(KyAdView.this.retAdBean);
                    return;
            }
        }

        private void clientBitMap(RetAdBean retAdBean) {
            URL myFileUrl = null;
            try {
                myFileUrl = new URL(retAdBean.getAdShowPic());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            try {
                HttpURLConnection conn = (HttpURLConnection) myFileUrl.openConnection();
                conn.setDoInput(true);
                conn.connect();
                InputStream is = conn.getInputStream();
                KyAdView.this.bitmap = BitmapFactory.decodeStream(is);
                is.close();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }

        private RetAdBean readXML(String xmlStr) {
            Text t;
            Text t2;
            Text t3;
            Text t4;
            Text t5;
            RetAdBean retAdBean = new RetAdBean();
            if (xmlStr == null) {
                return null;
            }
            InputSource inputSource = new InputSource(new StringReader(xmlStr));
            DocumentBuilder db = null;
            try {
                db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            } catch (ParserConfigurationException pce) {
                System.err.println(pce);
                System.exit(1);
            }
            Document doc = null;
            try {
                doc = db.parse(inputSource);
            } catch (SAXException e) {
                try {
                    e.printStackTrace();
                } catch (DOMException e2) {
                    System.err.println(e2.getMessage());
                    System.exit(1);
                } catch (IOException e3) {
                    System.err.println(e3);
                    System.exit(1);
                }
            }
            NodeList applications = doc.getDocumentElement().getElementsByTagName("application");
            for (int i = 0; i < applications.getLength(); i++) {
                Element application = (Element) applications.item(i);
                retAdBean.setIdApp(application.getAttribute("idApp"));
                NodeList idAds = application.getElementsByTagName("idAd");
                if (idAds.getLength() == 1 && (t5 = (Text) ((Element) idAds.item(0)).getFirstChild()) != null) {
                    retAdBean.setIdAd(t5.getNodeValue());
                }
                NodeList adShowTypes = application.getElementsByTagName("adShowType");
                if (adShowTypes.getLength() == 1 && (t4 = (Text) ((Element) adShowTypes.item(0)).getFirstChild()) != null) {
                    retAdBean.setAdShowType(Integer.parseInt(t4.getNodeValue()));
                }
                NodeList adShowTexts = application.getElementsByTagName("adShowText");
                if (adShowTexts.getLength() == 1 && (t3 = (Text) ((Element) adShowTexts.item(0)).getFirstChild()) != null) {
                    retAdBean.setAdShowText(t3.getNodeValue());
                }
                NodeList adShowPics = application.getElementsByTagName("adShowPic");
                if (adShowPics.getLength() == 1 && (t2 = (Text) ((Element) adShowPics.item(0)).getFirstChild()) != null) {
                    retAdBean.setAdShowPic(t2.getNodeValue());
                }
                NodeList adLinkTypes = application.getElementsByTagName("adLinkType");
                if (adLinkTypes.getLength() == 1 && (t = (Text) ((Element) adLinkTypes.item(0)).getFirstChild()) != null) {
                    retAdBean.setAdLinkType(Integer.parseInt(t.getNodeValue()));
                }
                NodeList adLinks = application.getElementsByTagName("adLink");
                if (adLinks.getLength() == 1) {
                    Text t6 = (Text) ((Element) adLinks.item(0)).getFirstChild();
                    if (t6 != null) {
                        retAdBean.setAdLink(t6.getNodeValue());
                    }
                    String link = retAdBean.getAdLink();
                    if (link != null) {
                        link = link.replace("#$amp;", "&");
                    }
                    retAdBean.setAdLink(link);
                }
            }
            return retAdBean;
        }

        private String getResponse(String writeStr, String agent) {
            HttpPost httpRequest = new HttpPost(agent);
            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("name", writeStr));
            try {
                httpRequest.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
                HttpResponse httpResponse = new DefaultHttpClient().execute(httpRequest);
                if (httpResponse.getStatusLine().getStatusCode() == 200) {
                    return EntityUtils.toString(httpResponse.getEntity(), "UTF_8");
                }
                return null;
            } catch (ClientProtocolException e) {
                e.printStackTrace();
                return null;
            } catch (UnsupportedEncodingException e2) {
                e2.printStackTrace();
                return null;
            } catch (IOException e3) {
                e3.printStackTrace();
                return null;
            }
        }
    }

    public KyAdView(Context context, String kyAdID, String addr, String logo, int refreshInterval, boolean istestMode, int backgroundColorVal, int textColorVal) {
        super(context);
        this.appId = new String(kyAdID);
        this.address = new String(addr);
        this.internal = refreshInterval;
        if (this.internal < 30) {
            this.internal = 30;
        } else if (this.internal > 300) {
            this.internal = 300;
        }
        this.isTestMode = istestMode;
        this.backGroundColor = backgroundColorVal;
        this.textColor = textColorVal;
        initPanal(logo);
        initAd(context);
    }

    public void setTopPadding(int TopPadding) {
        this.topPadding = TopPadding;
        setPadding(0, this.topPadding, 0, 0);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean changed, int l, int t, int r, int b) {
        View child = getChildAt(0);
        child.measure(this.screenWidth, this.screenHeight);
        child.setVisibility(0);
        int picWidth = child.getMeasuredWidth();
        int picHeight = child.getMeasuredHeight();
        View child1 = getChildAt(1);
        child1.measure(this.screenWidth, this.screenHeight);
        child1.setVisibility(0);
        int textWidth = child1.getMeasuredWidth();
        int textHeight = child1.getMeasuredHeight();
        if (textWidth == 0) {
            textHeight = 0;
        }
        View child2 = getChildAt(2);
        child2.measure(this.screenWidth, this.screenHeight);
        child2.setVisibility(0);
        int logoWidth = child2.getMeasuredWidth();
        int logoHeight = child2.getMeasuredHeight();
        if (logoWidth == 0) {
            logoHeight = 0;
        }
        if (textWidth + picWidth == 0) {
            setVisibility(4);
        } else {
            setVisibility(0);
        }
        if (picHeight > 0) {
            int picLayoutWidth = Math.min(((getHeight() - 2) * picWidth) / picHeight, getWidth()) - 2;
            child.layout(1, 1, picLayoutWidth + 1, (getHeight() - 2) + 1);
            child1.layout(picLayoutWidth + 2, 0, getWidth(), getHeight());
        } else if (textHeight > 0) {
            child1.layout(0, 0, getWidth(), getHeight());
        }
        if (logoWidth > 0 && logoHeight > 0) {
            child2.layout(getWidth() - logoWidth, (getHeight() - logoHeight) - 1, getWidth(), getHeight());
        }
    }

    private void initPanal(String logo) {
        setBackgroundColor(this.backGroundColor);
        this.adImage = new ImageView(getContext());
        addView(this.adImage);
        this.adText = new TextView(getContext());
        this.adText.setTextColor(this.textColor);
        this.adText.setTextSize(16.0f);
        addView(this.adText);
        this.logoText = new TextView(getContext());
        this.logoText.setTextColor(this.textColor);
        this.logoText.setText(logo);
        this.logoText.setTextSize(10.0f);
        addView(this.logoText);
        AnimationSet set = new AnimationSet(true);
        Animation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(1000);
        set.addAnimation(animation);
        setLayoutAnimation(new LayoutAnimationController(set, 0.25f));
    }

    private void initAd(Context context) {
        new DisplayMetrics();
        DisplayMetrics dm = context.getApplicationContext().getResources().getDisplayMetrics();
        this.screenWidth = dm.widthPixels;
        this.screenHeight = dm.heightPixels;
        initApplyAdInfo(context);
        this.mThread = new Thread(this.mClient);
        this.mThread.start();
    }

    /* access modifiers changed from: private */
    public void frushAd() {
        new Message().what = 0;
        notifyReceiveAdOk();
    }

    /* access modifiers changed from: private */
    public void notifyConncetFail() {
        Message message = new Message();
        message.what = 2;
        this.handler.sendMessage(message);
    }

    /* access modifiers changed from: private */
    public void notifyReceiveAdError() {
        Message message = new Message();
        message.what = 1;
        this.handler.sendMessage(message);
    }

    private void notifyReceiveAdOk() {
        Message message = new Message();
        message.what = 0;
        this.handler.sendMessage(message);
    }

    private void initApplyAdInfo(Context context) {
        this.applyAdBean = new ApplyAdBean();
        this.applyAdBean.setAppId(getSysId(context));
        this.applyAdBean.setSystem(0);
        if (this.isTestMode) {
            this.applyAdBean.setTestMode(1);
        } else {
            this.applyAdBean.setTestMode(0);
        }
        this.agent1 = new String("http://" + this.address + "/nusoap/nusoap_agent1.php");
        this.agent2 = new String("http://" + this.address + "/nusoap/nusoap_agent2.php");
        this.agent1test = new String("http://" + this.address + "/nusoap/nusoap_agent1_test.php");
        setOnClickListener(this.adView_listener);
    }

    private String clickResponse(String writeStr, String agent) {
        HttpPost httpRequest = new HttpPost(agent);
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("name", writeStr));
        try {
            httpRequest.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
            HttpResponse httpResponse = new DefaultHttpClient().execute(httpRequest);
            if (httpResponse.getStatusLine().getStatusCode() == 200) {
                return EntityUtils.toString(httpResponse.getEntity(), "UTF_8");
            }
            return null;
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            return null;
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
            return null;
        } catch (IOException e3) {
            e3.printStackTrace();
            return null;
        }
    }

    private String clickReport(int type, int display, int click) {
        XmlSerializer serializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        try {
            serializer.setOutput(writer);
            serializer.startDocument(null, true);
            serializer.startTag("", "application");
            serializer.startTag("", "idApp");
            serializer.text(this.applyAdBean.getAppId());
            serializer.endTag("", "idApp");
            serializer.startTag("", "idAd");
            serializer.text(this.retAdBean.getIdAd());
            serializer.endTag("", "idAd");
            serializer.startTag("", "system");
            serializer.text(Integer.toString(this.applyAdBean.getSystem()));
            serializer.endTag("", "system");
            serializer.startTag("", "reportType");
            serializer.text(Integer.toString(type));
            serializer.endTag("", "reportType");
            serializer.startTag("", "display");
            serializer.text(Integer.toString(display));
            serializer.endTag("", "display");
            serializer.startTag("", "click");
            serializer.text(Integer.toString(click));
            serializer.endTag("", "click");
            serializer.endTag("", "application");
            serializer.endDocument();
            return writer.toString();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /* access modifiers changed from: private */
    public void clickServer(int type, int display, int click, String agent) {
        clickResponse(clickReport(type, display, click), agent);
    }

    /* access modifiers changed from: package-private */
    public void openWebBrowser(String url, Context context) {
        Intent i = new Intent("android.intent.action.VIEW");
        i.setData(Uri.parse(url));
        context.startActivity(i);
    }

    private String getSysId(Context context) {
        if (this.appId != null) {
            return this.appId;
        }
        String id = null;
        try {
            id = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.getString("APP_ID");
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return id;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(measureWidth(widthMeasureSpec), measureHeight(heightMeasureSpec));
    }

    private int measureWidth(int measureSpec) {
        View child = getChildAt(0);
        child.measure(this.screenWidth, this.screenHeight);
        View child1 = getChildAt(1);
        child1.measure(this.screenWidth, this.screenHeight);
        int specMode = View.MeasureSpec.getMode(measureSpec);
        int specSize = View.MeasureSpec.getSize(measureSpec);
        if (specMode == 1073741824) {
            return specSize;
        }
        int result = child.getMeasuredWidth() + child1.getMeasuredWidth() + getPaddingLeft() + getPaddingRight() + 2;
        if (result > this.screenWidth) {
            result = this.screenWidth;
        }
        if (specMode == Integer.MIN_VALUE) {
            return Math.min(result, specSize);
        }
        return result;
    }

    private int measureHeight(int measureSpec) {
        getChildAt(0).measure(this.screenWidth, this.screenHeight);
        int specMode = View.MeasureSpec.getMode(measureSpec);
        int specSize = View.MeasureSpec.getSize(measureSpec);
        if (specMode == 1073741824) {
            return specSize;
        }
        return 50;
    }

    public void setAdListener(onAdListener listener) {
        this.mOnAdListener = listener;
    }
}
