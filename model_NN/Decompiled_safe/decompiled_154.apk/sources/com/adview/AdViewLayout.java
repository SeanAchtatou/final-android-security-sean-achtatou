package com.adview;

import android.app.Activity;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.adview.AdViewTargeting;
import com.adview.adapters.AdViewAdapter;
import com.adview.obj.Extra;
import com.adview.obj.Ration;
import com.adview.util.AdViewUtil;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class AdViewLayout extends RelativeLayout {
    public Ration activeRation;
    public final WeakReference<Activity> activityReference;
    public AdViewInterface adViewInterface;
    public AdViewManager adViewManager;
    public Extra extra;
    public final Handler handler;
    /* access modifiers changed from: private */
    public boolean hasWindow;
    /* access modifiers changed from: private */
    public boolean isScheduled;
    private String keyAdView;
    private int maxHeight;
    private int maxWidth;
    public Ration nextRation;
    public final ScheduledExecutorService scheduler;
    public WeakReference<RelativeLayout> superViewReference = new WeakReference<>(this);

    public void setMaxWidth(int width) {
        this.maxWidth = width;
    }

    public void setMaxHeight(int height) {
        this.maxHeight = height;
    }

    public AdViewLayout(Activity context, String keyAdView2) {
        super(context);
        this.activityReference = new WeakReference<>(context);
        this.keyAdView = keyAdView2;
        this.hasWindow = true;
        this.handler = new Handler();
        this.scheduler = Executors.newScheduledThreadPool(1);
        this.isScheduled = true;
        this.scheduler.schedule(new InitRunnable(this, keyAdView2), 0, TimeUnit.SECONDS);
        setHorizontalScrollBarEnabled(false);
        setVerticalScrollBarEnabled(false);
        this.maxWidth = 0;
        this.maxHeight = 0;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthSize = View.MeasureSpec.getSize(widthMeasureSpec);
        int heightSize = View.MeasureSpec.getSize(heightMeasureSpec);
        if (this.maxWidth > 0 && widthSize > this.maxWidth) {
            widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(this.maxWidth, Integer.MIN_VALUE);
        }
        if (this.maxHeight > 0 && heightSize > this.maxHeight) {
            heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(this.maxHeight, Integer.MIN_VALUE);
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int visibility) {
        if (visibility == 0) {
            this.hasWindow = true;
            if (!this.isScheduled) {
                this.isScheduled = true;
                if (this.extra != null) {
                    rotateThreadedNow();
                } else {
                    this.scheduler.schedule(new InitRunnable(this, this.keyAdView), 0, TimeUnit.SECONDS);
                }
            }
        } else {
            this.hasWindow = false;
        }
    }

    /* access modifiers changed from: private */
    public void rotateAd() {
        if (!this.hasWindow) {
            this.isScheduled = false;
            return;
        }
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.i(AdViewUtil.ADVIEW, "Rotating Ad");
        }
        this.nextRation = this.adViewManager.getRation();
        this.handler.post(new HandleAdRunnable(this));
    }

    /* access modifiers changed from: private */
    public void handleAd() {
        if (this.nextRation == null) {
            if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
                Log.e(AdViewUtil.ADVIEW, "nextRation is null!");
            }
            rotateThreadedDelayed();
            return;
        }
        String rationInfo = String.format("Showing ad:\nname: %s", this.nextRation.name);
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, rationInfo);
        }
        try {
            AdViewAdapter.handle(this, this.nextRation);
        } catch (Throwable th) {
            Throwable t = th;
            if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
                Log.w(AdViewUtil.ADVIEW, "Caught an exception in adapter:", t);
            }
            rollover();
        }
    }

    public void rotateThreadedNow() {
        this.scheduler.schedule(new RotateAdRunnable(this), 0, TimeUnit.SECONDS);
    }

    public void rotateThreadedDelayed() {
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "Will call rotateAd() in " + this.extra.cycleTime + " seconds");
        }
        this.scheduler.schedule(new RotateAdRunnable(this), (long) this.extra.cycleTime, TimeUnit.SECONDS);
    }

    public void pushSubView(ViewGroup subView) {
        RelativeLayout superView = this.superViewReference.get();
        if (superView != null) {
            superView.removeAllViews();
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
            layoutParams.addRule(13);
            superView.addView(subView, layoutParams);
            if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
                Log.d(AdViewUtil.ADVIEW, "Added subview");
            }
            this.activeRation = this.nextRation;
            countImpression();
        }
    }

    public void rollover() {
        this.nextRation = this.adViewManager.getRollover();
        this.handler.post(new HandleAdRunnable(this));
    }

    public void rollover_pri() {
        this.nextRation = this.adViewManager.getRollover_pri();
        this.handler.post(new HandleAdRunnable(this));
    }

    private void countImpression() {
        if (this.activeRation != null) {
            this.scheduler.schedule(new PingUrlRunnable(String.format(AdViewUtil.urlImpression, this.adViewManager.keyAdView, this.activeRation.nid, Integer.valueOf(this.activeRation.type), 0, this.adViewManager.localeString, Integer.valueOf((int) AdViewUtil.VERSION))), 0, TimeUnit.SECONDS);
            if (this.adViewInterface != null) {
                this.adViewInterface.onDisplayAd();
            }
        }
    }

    private void countClick() {
        if (this.activeRation != null) {
            this.scheduler.schedule(new PingUrlRunnable(String.format(AdViewUtil.urlClick, this.adViewManager.keyAdView, this.activeRation.nid, Integer.valueOf(this.activeRation.type), 0, this.adViewManager.localeString, Integer.valueOf((int) AdViewUtil.VERSION))), 0, TimeUnit.SECONDS);
            if (this.adViewInterface != null) {
                this.adViewInterface.onClickAd();
            }
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case 0:
                if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
                    Log.d(AdViewUtil.ADVIEW, "Intercepted ACTION_DOWN event");
                }
                if (this.activeRation == null) {
                    return false;
                }
                countClick();
                return false;
            default:
                return false;
        }
    }

    public void setAdViewInterface(AdViewInterface adViewInterface2) {
        this.adViewInterface = adViewInterface2;
    }

    private static class InitRunnable implements Runnable {
        private WeakReference<AdViewLayout> adViewLayoutReference;
        private String keyAdView;

        public InitRunnable(AdViewLayout adViewLayout, String keyAdView2) {
            this.adViewLayoutReference = new WeakReference<>(adViewLayout);
            this.keyAdView = keyAdView2;
        }

        public void run() {
            Activity activity;
            AdViewLayout adViewLayout = this.adViewLayoutReference.get();
            if (adViewLayout != null && (activity = adViewLayout.activityReference.get()) != null) {
                if (adViewLayout.adViewManager == null) {
                    adViewLayout.adViewManager = new AdViewManager(new WeakReference(activity.getApplicationContext()), this.keyAdView);
                }
                if (!adViewLayout.hasWindow) {
                    adViewLayout.isScheduled = false;
                    return;
                }
                adViewLayout.adViewManager.fetchConfig();
                adViewLayout.extra = adViewLayout.adViewManager.getExtra();
                if (adViewLayout.extra == null) {
                    adViewLayout.scheduler.schedule(this, 30, TimeUnit.SECONDS);
                } else {
                    adViewLayout.rotateAd();
                }
            }
        }
    }

    private static class HandleAdRunnable implements Runnable {
        private WeakReference<AdViewLayout> adViewLayoutReference;

        public HandleAdRunnable(AdViewLayout adViewLayout) {
            this.adViewLayoutReference = new WeakReference<>(adViewLayout);
        }

        public void run() {
            AdViewLayout adViewLayout = this.adViewLayoutReference.get();
            if (adViewLayout != null) {
                adViewLayout.handleAd();
            }
        }
    }

    public static class ViewAdRunnable implements Runnable {
        private WeakReference<AdViewLayout> adViewLayoutReference;
        private ViewGroup nextView;

        public ViewAdRunnable(AdViewLayout adViewLayout, ViewGroup nextView2) {
            this.adViewLayoutReference = new WeakReference<>(adViewLayout);
            this.nextView = nextView2;
        }

        public void run() {
            AdViewLayout adViewLayout = this.adViewLayoutReference.get();
            if (adViewLayout != null) {
                adViewLayout.pushSubView(this.nextView);
            }
        }
    }

    private static class RotateAdRunnable implements Runnable {
        private WeakReference<AdViewLayout> adViewLayoutReference;

        public RotateAdRunnable(AdViewLayout adViewLayout) {
            this.adViewLayoutReference = new WeakReference<>(adViewLayout);
        }

        public void run() {
            AdViewLayout adViewLayout = this.adViewLayoutReference.get();
            if (adViewLayout != null) {
                adViewLayout.rotateAd();
            }
        }
    }

    private static class PingUrlRunnable implements Runnable {
        private String url;

        public PingUrlRunnable(String url2) {
            this.url = url2;
        }

        public void run() {
            try {
                new DefaultHttpClient().execute(new HttpGet(this.url));
            } catch (ClientProtocolException e) {
                if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
                    Log.e(AdViewUtil.ADVIEW, "Caught ClientProtocolException in PingUrlRunnable", e);
                }
            } catch (IOException e2) {
                if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
                    Log.e(AdViewUtil.ADVIEW, "Caught IOException in PingUrlRunnable", e2);
                }
            }
        }
    }
}
