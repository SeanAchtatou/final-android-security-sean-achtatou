package com.finger2finger.games.start;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.finger2finger.games.common.activity.F2FGameActivity;

public class StartActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startActivity(new Intent(this, F2FGameActivity.class));
        finish();
    }
}
