package com.finger2finger.games.common.scene.dialog;

import android.content.res.Resources;
import com.f2fgames.games.monkeybreak.lite.R;
import com.finger2finger.games.common.CommonConst;
import com.finger2finger.games.common.Utils;
import com.finger2finger.games.common.activity.F2FGameActivity;
import com.finger2finger.games.common.base.BaseFont;
import com.finger2finger.games.common.base.BaseSprite;
import com.finger2finger.games.common.base.BaseStrokeFont;
import com.finger2finger.games.common.res.CommonResource;
import com.finger2finger.games.res.Resource;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.primitive.Rectangle;
import org.anddev.andengine.entity.scene.CameraScene;
import org.anddev.andengine.entity.text.ChangeableText;
import org.anddev.andengine.input.touch.TouchEvent;

public class PortraitGameOverDialog extends CameraScene {
    private F2FGameActivity mContext;
    private String tryAgainText;

    private enum BtnType {
        MENU_BTN,
        REPLAY_BTN,
        NEXTLEVEL_BTN
    }

    public PortraitGameOverDialog(Camera camera, F2FGameActivity pContext, boolean pIsThreeBtn, boolean pIsSucceed, String plevelText, int pHightScore, int pScore, boolean pHaveShopBtn, String pMessage) {
        super(1, camera);
        this.mContext = pContext;
        this.tryAgainText = pMessage;
        loadScene(pIsThreeBtn, pHaveShopBtn, plevelText, pHightScore, pScore, pIsSucceed);
        setBackgroundEnabled(false);
    }

    public PortraitGameOverDialog(Camera camera, F2FGameActivity pContext, boolean pIsThreeBtn, boolean pIsSucceed, String plevelText, int pHightScore, int pScore, boolean pHaveShopBtn) {
        super(1, camera);
        this.mContext = pContext;
        loadScene(pIsThreeBtn, pHaveShopBtn, plevelText, pHightScore, pScore, pIsSucceed);
        setBackgroundEnabled(false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.finger2finger.games.common.base.BaseSprite.<init>(float, float, float, org.anddev.andengine.opengl.texture.region.TextureRegion, boolean):void
     arg types: [int, int, float, org.anddev.andengine.opengl.texture.region.TextureRegion, int]
     candidates:
      com.finger2finger.games.common.base.BaseSprite.<init>(float, float, float, float, org.anddev.andengine.opengl.texture.region.TextureRegion):void
      com.finger2finger.games.common.base.BaseSprite.<init>(float, float, float, org.anddev.andengine.opengl.texture.region.TextureRegion, boolean):void */
    public void loadScene(boolean pIsThreeBtn, boolean pHaveShopBtn, String plevelText, int pHightScore, int pScore, boolean pIsSucceed) {
        Rectangle rectangle = new Rectangle(0.0f, 0.0f, (float) CommonConst.CAMERA_WIDTH, (float) CommonConst.CAMERA_HEIGHT);
        rectangle.setColor(CommonConst.GrayColor.red, CommonConst.GrayColor.green, CommonConst.GrayColor.blue);
        rectangle.setAlpha(CommonConst.GrayColor.alpha);
        getTopLayer().addEntity(rectangle);
        BaseSprite backgroudSpr = new BaseSprite(0.0f, 0.0f, CommonConst.RALE_SAMALL_VALUE * 1.15f, this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.GAME_DIALOG_BG.mKey), false);
        float backgroudSprWidth = backgroudSpr.getWidth();
        float backgroudSprHeight = backgroudSpr.getHeight();
        float backgroudSprPX = (((float) CommonConst.CAMERA_WIDTH) - backgroudSprWidth) / 2.0f;
        float backgroudSprPY = (((float) CommonConst.CAMERA_HEIGHT) - backgroudSprHeight) / 2.0f;
        backgroudSpr.setPosition(backgroudSprPX, backgroudSprPY);
        getTopLayer().addEntity(backgroudSpr);
        float stepValue = 15.0f * CommonConst.RALE_SAMALL_VALUE;
        AnonymousClass1 r6 = new BaseSprite(0.0f, 0.0f, CommonConst.RALE_SAMALL_VALUE, this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.BUTTON_MENU.mKey), false) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() != 0) {
                    return true;
                }
                PortraitGameOverDialog.this.closeBtn(BtnType.MENU_BTN);
                return true;
            }
        };
        if (!CommonConst.ENABLE_SHOW_ADVIEW) {
            backgroudSprPX = (((float) CommonConst.CAMERA_WIDTH) - backgroudSprWidth) / 2.0f;
            backgroudSprPY = ((((float) CommonConst.CAMERA_HEIGHT) - backgroudSprHeight) - r6.getHeight()) - (5.0f * CommonConst.RALE_SAMALL_VALUE);
            backgroudSpr.setPosition(backgroudSprPX, backgroudSprPY);
        }
        if (pIsSucceed && pHightScore <= pScore) {
            BaseSprite mNewRecordSpr = new BaseSprite(0.0f, 0.0f, CommonConst.RALE_SAMALL_VALUE, this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.GAME_NEW_RECORD.mKey), false);
            mNewRecordSpr.setPosition((backgroudSprPX + backgroudSprWidth) - mNewRecordSpr.getWidth(), backgroudSprPY);
            getTopLayer().addEntity(mNewRecordSpr);
        }
        float height = r6.getHeight();
        r6.setPosition(backgroudSprPX + stepValue, backgroudSprPY + backgroudSprHeight);
        registerTouchArea(r6);
        getTopLayer().addEntity(r6);
        AnonymousClass2 r8 = new BaseSprite(0.0f, 0.0f, CommonConst.RALE_SAMALL_VALUE, this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.BUTTON_REPLAY.mKey), false) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() != 0) {
                    return true;
                }
                PortraitGameOverDialog.this.closeBtn(BtnType.REPLAY_BTN);
                return true;
            }
        };
        float spriteReplayWidth = r8.getWidth();
        float height2 = r8.getHeight();
        float spriteReplaySprPX = backgroudSprPX + ((backgroudSprWidth - spriteReplayWidth) / 2.0f);
        float spriteReplaySprPY = backgroudSprPY + backgroudSprHeight;
        if (!pIsThreeBtn) {
            spriteReplaySprPX = ((backgroudSprPX + backgroudSprWidth) - spriteReplayWidth) - stepValue;
            spriteReplaySprPY = backgroudSprPY + backgroudSprHeight;
        } else {
            AnonymousClass3 r9 = new BaseSprite(0.0f, 0.0f, CommonConst.RALE_SAMALL_VALUE, this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.BUTTON_NEXTLEVEL.mKey), false) {
                public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                    if (pSceneTouchEvent.getAction() != 0) {
                        return true;
                    }
                    PortraitGameOverDialog.this.closeBtn(BtnType.NEXTLEVEL_BTN);
                    return true;
                }
            };
            float nextLevelWidth = r9.getWidth();
            float height3 = r9.getHeight();
            r9.setPosition(((backgroudSprPX + backgroudSprWidth) - nextLevelWidth) - stepValue, backgroudSprPY + backgroudSprHeight);
            registerTouchArea(r9);
            getTopLayer().addEntity(r9);
        }
        r8.setPosition(spriteReplaySprPX, spriteReplaySprPY);
        registerTouchArea(r8);
        getTopLayer().addEntity(r8);
        BaseFont contentFont = this.mContext.mResource.getBaseFontByKey(Resource.FONT.DIALOG_CONTEXT.mKey);
        BaseStrokeFont mGameOverPointFont = this.mContext.mResource.getStrokeFontByKey(Resource.STROKEFONT.GAME_POINT_FONT.mKey);
        Resources res = this.mContext.getResources();
        String msgTitleStr = res.getString(R.string.game_title_result);
        float textTitlePX = backgroudSprPX + ((backgroudSprWidth - ((float) Utils.getMaxCharacterLens(contentFont, msgTitleStr))) / 2.0f);
        float textTitlePY = backgroudSprPY + (30.0f * CommonConst.RALE_SAMALL_VALUE);
        getTopLayer().addEntity(new ChangeableText(textTitlePX, textTitlePY, contentFont, msgTitleStr));
        String msgContent = res.getString(R.string.str_score);
        int textWidth = contentFont.getStringWidth(msgContent);
        float textPX = backgroudSprPX + (60.0f * CommonConst.RALE_SAMALL_VALUE);
        float textPY = ((float) contentFont.getLineHeight()) + textTitlePY + (35.0f * CommonConst.RALE_SAMALL_VALUE);
        ChangeableText mGameOverContent = new ChangeableText(textPX, textPY, contentFont, msgContent, textWidth);
        mGameOverContent.setScale(0.75f);
        getTopLayer().addEntity(mGameOverContent);
        String sore = String.valueOf(pScore);
        float textSorePX = ((float) textWidth) + textPX + (10.0f * CommonConst.RALE_SAMALL_VALUE);
        float textSorePY = textPY + (((float) (contentFont.getLineHeight() - mGameOverPointFont.getLineHeight())) / 2.0f);
        getTopLayer().addEntity(new ChangeableText(textSorePX, textSorePY, mGameOverPointFont, sore, mGameOverPointFont.getStringWidth(sore + "00000")));
        if (this.tryAgainText == null || this.tryAgainText.equals("")) {
            this.tryAgainText = res.getString(R.string.game_over_content_try_again);
        }
        String tryAgainMsg = Utils.getAutoLine(contentFont, this.tryAgainText, (int) (backgroudSprWidth - (100.0f * CommonConst.RALE_SAMALL_VALUE)), false, 0);
        ChangeableText mGameOverTryAgainContent = new ChangeableText(backgroudSprPX + ((backgroudSprWidth - ((float) Utils.getMaxCharacterLens(contentFont, tryAgainMsg))) / 2.0f), ((float) mGameOverPointFont.getLineHeight()) + textSorePY + (10.0f * CommonConst.RALE_SAMALL_VALUE), contentFont, tryAgainMsg, contentFont.getStringWidth(tryAgainMsg));
        mGameOverTryAgainContent.setScale(0.75f);
        getTopLayer().addEntity(mGameOverTryAgainContent);
        if (this.mContext.getmGameScene() == null) {
            return;
        }
        if (pIsSucceed) {
            Utils.setGoogleAnalytics(this.mContext, 2, this.mContext.getmGameScene().mScore, this.mContext.getmGameScene().mMeter);
        } else {
            Utils.setGoogleAnalytics(this.mContext, 3, this.mContext.getmGameScene().mScore, this.mContext.getmGameScene().mMeter);
        }
    }

    /* access modifiers changed from: private */
    public void closeBtn(BtnType pbtnType) {
        this.mContext.getEngine().getScene().clearChildScene();
        switch (pbtnType) {
            case MENU_BTN:
                this.mContext.getmGameScene().onMenuBtn();
                return;
            case REPLAY_BTN:
                this.mContext.getmGameScene().onReplayBtn();
                return;
            case NEXTLEVEL_BTN:
                this.mContext.getmGameScene().onNextLevelBtn();
                return;
            default:
                return;
        }
    }
}
