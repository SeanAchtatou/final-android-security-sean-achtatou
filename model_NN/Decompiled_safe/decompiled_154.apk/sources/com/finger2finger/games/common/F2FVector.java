package com.finger2finger.games.common;

public class F2FVector {
    float x;
    float y;

    public float getX() {
        return this.x;
    }

    public float getY() {
        return this.y;
    }

    public void setX(float x2) {
        this.x = x2;
    }

    public void setY(float y2) {
        this.y = y2;
    }

    public F2FVector(float pX, float pY) {
        this.x = pX;
        this.y = pY;
    }
}
