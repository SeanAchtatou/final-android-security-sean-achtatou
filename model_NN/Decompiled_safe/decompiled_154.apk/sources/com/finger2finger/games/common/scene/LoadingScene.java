package com.finger2finger.games.common.scene;

import com.finger2finger.games.common.CommonConst;
import com.finger2finger.games.common.GoogleAnalyticsUtils;
import com.finger2finger.games.common.Utils;
import com.finger2finger.games.common.activity.F2FGameActivity;
import com.finger2finger.games.common.res.CommonResource;
import com.finger2finger.games.res.Const;
import com.finger2finger.games.res.PortConst;
import org.anddev.andengine.entity.shape.modifier.LoopShapeModifier;
import org.anddev.andengine.entity.shape.modifier.ScaleModifier;
import org.anddev.andengine.entity.shape.modifier.SequenceShapeModifier;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.texture.region.TextureRegion;

public class LoadingScene extends F2FScene {
    public boolean enableTouch = true;

    public LoadingScene(F2FGameActivity pContext) {
        super(1, pContext);
        loadScene();
    }

    public void loadScene() {
        float cameraMinX = this.mContext.getEngine().getCamera().getMinX();
        float cameraMinY = this.mContext.getEngine().getCamera().getMinY();
        if (PortConst.enablePromotion) {
            getTopLayer().addEntity(new Sprite(cameraMinX, cameraMinY, (float) CommonConst.CAMERA_WIDTH, (float) CommonConst.CAMERA_HEIGHT, this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.PROMOTION_BG.mKey)));
            TextureRegion linkPic = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.PROMOTION_LINKPIC.mKey);
            float linkWidth = ((float) linkPic.getWidth()) * CommonConst.RALE_SAMALL_VALUE;
            float linkHeight = ((float) linkPic.getHeight()) * CommonConst.RALE_SAMALL_VALUE;
            int promotionIndex = this.mContext.promotionHandler.mGamePromotionIndex;
            Sprite loadLinkSprite = new Sprite((((this.mContext.promotionHandler.promationInfo.get(promotionIndex).promationActionPicPX * ((float) CommonConst.CAMERA_WIDTH)) / 100.0f) + cameraMinX) - (linkWidth / 2.0f), (((((float) CommonConst.CAMERA_HEIGHT) * this.mContext.promotionHandler.promationInfo.get(promotionIndex).promationActionPicPY) / 100.0f) + cameraMinY) - (linkHeight / 2.0f), linkWidth, linkHeight, linkPic) {
                public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                    if (LoadingScene.this.enableTouch) {
                        LoadingScene.this.enableTouch = false;
                        setVisible(false);
                        PortConst.enablePromotion = false;
                        GoogleAnalyticsUtils.setTracker("Go_To_Promotion/From/crabbreak/Url:" + PortConst.loadLinkUrl);
                        Utils.goPage(LoadingScene.this.mContext, PortConst.loadLinkUrl);
                    }
                    return false;
                }
            };
            if (this.mContext.promotionHandler.promationInfo.get(promotionIndex).promationType == CommonConst.PROMATION_TYPE.ZOOM) {
                String[] actionInfo = this.mContext.promotionHandler.promationInfo.get(promotionIndex).promationActionInfo.split(Const.SEPARATOR_ITEM_COMMA);
                float startScale = Float.parseFloat(actionInfo[0]);
                float endScale = Float.parseFloat(actionInfo[1]);
                loadLinkSprite.addShapeModifier(new LoopShapeModifier(new SequenceShapeModifier(new ScaleModifier(Const.BTN_SCALE_TIME, startScale, endScale), new ScaleModifier(Const.BTN_SCALE_TIME, endScale, startScale))));
            }
            getTopLayer().addEntity(loadLinkSprite);
            getTopLayer().registerTouchArea(loadLinkSprite);
            return;
        }
        getTopLayer().addEntity(new Sprite(cameraMinX, cameraMinY, (float) CommonConst.CAMERA_WIDTH, (float) CommonConst.CAMERA_HEIGHT, this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.LOADING_BG.mKey)));
        TextureRegion textTextureRegion = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.LOADING_TEXT.mKey);
        getTopLayer().addEntity(new Sprite(((((float) CommonConst.CAMERA_WIDTH) + cameraMinX) - (((float) textTextureRegion.getWidth()) * CommonConst.RALE_SAMALL_VALUE)) - (12.0f * CommonConst.RALE_SAMALL_VALUE), ((((float) CommonConst.CAMERA_HEIGHT) + cameraMinY) - (((float) textTextureRegion.getHeight()) * CommonConst.RALE_SAMALL_VALUE)) - (15.0f * CommonConst.RALE_SAMALL_VALUE), ((float) textTextureRegion.getWidth()) * CommonConst.RALE_SAMALL_VALUE, ((float) textTextureRegion.getHeight()) * CommonConst.RALE_SAMALL_VALUE, textTextureRegion));
    }
}
