package com.finger2finger.games.common.scene.dialog;

import android.content.Intent;
import com.f2fgames.games.monkeybreak.lite.R;
import com.finger2finger.games.common.CommonConst;
import com.finger2finger.games.common.Utils;
import com.finger2finger.games.common.activity.F2FGameActivity;
import com.finger2finger.games.common.base.BaseFont;
import com.finger2finger.games.common.base.BaseSprite;
import com.finger2finger.games.common.res.CommonResource;
import com.finger2finger.games.common.res.MoreGameConst;
import com.finger2finger.games.common.res.SMSConst;
import com.finger2finger.games.common.res.ShopConst;
import com.finger2finger.games.common.scene.F2FScene;
import com.finger2finger.games.common.store.activity.ShopActivity;
import com.finger2finger.games.res.PortConst;
import com.finger2finger.games.res.Resource;
import org.anddev.andengine.entity.primitive.Rectangle;
import org.anddev.andengine.entity.scene.CameraScene;
import org.anddev.andengine.entity.text.ChangeableText;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.font.Font;

public class F2FAlertDialog extends CameraScene {
    public String btnName = "";
    public String btnName2 = "";
    public String btnName3 = "";
    public int btnNum = 1;
    public float btnRale = (CommonConst.RALE_SAMALL_VALUE * 0.8f);
    public int btnTxtMaximum = 10;
    public float btnTxtRale = 0.85f;
    public int captionMaximum = 100;
    public F2FGameActivity mContext;
    public float mDistanceY = 50.0f;
    public F2FScene mScene;
    public int maxBtnTxt = 8;
    public String msgContent = "";
    public String titleName = "";
    public int typeID = 0;

    public F2FAlertDialog(F2FGameActivity pContext) {
        super(1, pContext.getEngine().getCamera());
        this.mContext = pContext;
    }

    public void showDialog(int dialogTypeID) {
        this.typeID = dialogTypeID;
        setDialoginfo(this.typeID);
        setBackgroundEnabled(false);
        setDialogElements(true);
    }

    public void showDialog(int dialogTypeID, String dialogContext) {
        this.typeID = dialogTypeID;
        this.msgContent = dialogContext;
        setDialoginfo(this.typeID);
        setBackgroundEnabled(false);
        setDialogElements(true);
    }

    public void showDialog(String dialogContext, F2FScene pScene, int btnCount, String btn1Txt, String btn2Txt) {
        this.msgContent = dialogContext;
        this.btnName = btn1Txt;
        this.btnName2 = btn2Txt;
        this.btnNum = btnCount;
        this.mScene = pScene;
        this.titleName = this.mContext.getResources().getString(R.string.game_title_tips);
        setBackgroundEnabled(false);
        setDialogElements(false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.finger2finger.games.common.base.BaseSprite.<init>(float, float, float, org.anddev.andengine.opengl.texture.region.TextureRegion, boolean):void
     arg types: [int, int, float, org.anddev.andengine.opengl.texture.region.TextureRegion, int]
     candidates:
      com.finger2finger.games.common.base.BaseSprite.<init>(float, float, float, float, org.anddev.andengine.opengl.texture.region.TextureRegion):void
      com.finger2finger.games.common.base.BaseSprite.<init>(float, float, float, org.anddev.andengine.opengl.texture.region.TextureRegion, boolean):void */
    public void setDialogElements(boolean pIsShowByType) {
        BaseSprite btn1Spr;
        ChangeableText btn1Txt = null;
        Font mFontTextDialogTitle = this.mContext.mResource.getBaseFontByKey(Resource.FONT.DIALOG_TITLE.mKey);
        BaseFont baseFontByKey = this.mContext.mResource.getBaseFontByKey(Resource.FONT.DIALOG_CONTEXT.mKey);
        Rectangle rectangle = new Rectangle(0.0f, 0.0f, (float) CommonConst.CAMERA_WIDTH, (float) CommonConst.CAMERA_HEIGHT);
        rectangle.setColor(CommonConst.GrayColor.red, CommonConst.GrayColor.green, CommonConst.GrayColor.blue, CommonConst.GrayColor.alpha);
        getTopLayer().addEntity(rectangle);
        BaseSprite backgroudSpr = new BaseSprite(0.0f, 0.0f, CommonConst.RALE_SAMALL_VALUE * 1.15f, this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.GAME_DIALOG_BG.mKey).clone(), false);
        float backgroudSprWidth = backgroudSpr.getWidth();
        float backgroudSprHeight = backgroudSpr.getHeight();
        float backgroudSprPX = (((float) CommonConst.CAMERA_WIDTH) - backgroudSprWidth) / 2.0f;
        float backgroudSprPY = (((float) CommonConst.CAMERA_HEIGHT) - backgroudSprHeight) / 2.0f;
        if (!CommonConst.ENABLE_SHOW_ADVIEW) {
            backgroudSprPY = (((float) CommonConst.CAMERA_HEIGHT) - backgroudSprHeight) - (70.0f * CommonConst.RALE_SAMALL_VALUE);
        }
        backgroudSpr.setPosition(backgroudSprPX, backgroudSprPY);
        getTopLayer().addEntity(backgroudSpr);
        ChangeableText changeableText = new ChangeableText(0.0f, 0.0f, mFontTextDialogTitle, this.titleName);
        float text_x = ((backgroudSprWidth / 2.0f) + backgroudSprPX) - (changeableText.getWidth() / 2.0f);
        float text_y = backgroudSprPY + (22.0f * CommonConst.RALE_SAMALL_VALUE);
        changeableText.setPosition(text_x, text_y);
        getTopLayer().addEntity(changeableText);
        String autoLineMsg = Utils.getAutoLine(baseFontByKey, Utils.getOmissionString(this.msgContent, this.captionMaximum), (int) (backgroudSprWidth - (100.0f * CommonConst.RALE_SAMALL_VALUE)), false, 0);
        getTopLayer().addEntity(new ChangeableText(backgroudSprPX + ((backgroudSprWidth - ((float) Utils.getMaxCharacterLens(baseFontByKey, autoLineMsg))) / 2.0f), text_y + (this.mDistanceY * CommonConst.RALE_SAMALL_VALUE) + changeableText.getHeight(), baseFontByKey, autoLineMsg, baseFontByKey.getStringWidth(autoLineMsg + "00000000")));
        if (this.btnNum > 0) {
            final boolean z = pIsShowByType;
            btn1Spr = new BaseSprite(0.0f, 0.0f, this.btnRale, this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.BUTTON_BLANK.mKey).clone(), false) {
                public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                    if (pSceneTouchEvent.getAction() != 0) {
                        return true;
                    }
                    F2FAlertDialog.this.onBtn1(F2FAlertDialog.this.typeID, z);
                    return true;
                }
            };
            float btn1PX = backgroudSprPX + (15.0f * CommonConst.RALE_SAMALL_VALUE);
            float btn1PY = backgroudSprPY + backgroudSprHeight;
            btn1Spr.setPosition(btn1PX, btn1PY);
            registerTouchArea(btn1Spr);
            getTopLayer().addEntity(btn1Spr);
            btn1Txt = new ChangeableText(0.0f, 0.0f, baseFontByKey, this.btnName);
            float btn1TxtPX = btn1PX + ((btn1Spr.getWidth() - btn1Txt.getWidth()) / 2.0f);
            float btn1TxtPY = btn1PY + ((btn1Spr.getHeight() - btn1Txt.getHeight()) / 2.0f);
            if (this.btnName.length() > this.maxBtnTxt || this.btnName2.length() > this.maxBtnTxt) {
                btn1Txt.setScale(this.btnTxtRale);
            }
            btn1Txt.setPosition(btn1TxtPX, btn1TxtPY);
            getTopLayer().addEntity(btn1Txt);
        } else {
            btn1Spr = null;
        }
        if (this.btnNum == 1) {
            float btn1PX2 = ((backgroudSprPX + backgroudSprWidth) - btn1Spr.getWidth()) - (15.0f * CommonConst.RALE_SAMALL_VALUE);
            float btn1PY2 = backgroudSprPY + backgroudSprHeight;
            btn1Spr.setPosition(btn1PX2, btn1PY2);
            btn1Txt.setPosition(btn1PX2 + ((btn1Spr.getWidth() - btn1Txt.getWidth()) / 2.0f), btn1PY2 + ((btn1Spr.getHeight() - btn1Txt.getHeight()) / 2.0f));
        } else if (this.btnNum == 2) {
            final boolean z2 = pIsShowByType;
            AnonymousClass2 r12 = new BaseSprite(0.0f, 0.0f, this.btnRale, this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.BUTTON_BLANK.mKey).clone(), false) {
                public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                    if (pSceneTouchEvent.getAction() != 0) {
                        return true;
                    }
                    F2FAlertDialog.this.onBtn2(F2FAlertDialog.this.typeID, z2);
                    return true;
                }
            };
            float btn2PX = ((backgroudSprPX + backgroudSprWidth) - r12.getWidth()) - (15.0f * CommonConst.RALE_SAMALL_VALUE);
            float btn2PY = backgroudSprPY + backgroudSprHeight;
            r12.setPosition(btn2PX, btn2PY);
            registerTouchArea(r12);
            getTopLayer().addEntity(r12);
            ChangeableText changeableText2 = new ChangeableText(0.0f, 0.0f, baseFontByKey, this.btnName2);
            changeableText2.setPosition(btn2PX + ((r12.getWidth() - changeableText2.getWidth()) / 2.0f), btn2PY + ((r12.getHeight() - changeableText2.getHeight()) / 2.0f));
            if (this.btnName2.length() > this.maxBtnTxt || this.btnName.length() > this.maxBtnTxt) {
                changeableText2.setScale(this.btnTxtRale);
            }
            getTopLayer().addEntity(changeableText2);
        }
    }

    public void setDialoginfo(int dialogTypeID) {
        this.titleName = this.mContext.getResources().getString(R.string.game_title_tips);
        switch (dialogTypeID) {
            case 0:
                this.btnNum = 1;
                this.btnName = this.mContext.getResources().getString(R.string.F2FAlertDialog_Btn_Back);
                this.btnName2 = "";
                this.btnName3 = "";
                this.msgContent = this.mContext.getResources().getString(R.string.F2FAlertDialog_FIRST_PLAY_MSG);
                this.msgContent += "  " + this.mContext.getResources().getString(R.string.F2FAlertDialog_FIRST_PLAY_MSG_F2F_GOLD).replace("%d", String.valueOf(10)).trim();
                this.msgContent += ",  " + this.mContext.getResources().getString(R.string.F2FAlertDialog_FIRST_PLAY_MSG_F2F_MONNEY).replace("%d", String.valueOf(5));
                this.msgContent += ",  " + this.mContext.getResources().getString(R.string.F2FAlertDialog_FIRST_PLAY_MSG_F2F_DELAYER).replace("%d", String.valueOf(1));
                this.msgContent += ",  " + this.mContext.getResources().getString(R.string.F2FAlertDialog_FIRST_PLAY_MSG_F2F_EXTRALIFE).replace("%d", String.valueOf(1));
                return;
            case 1:
                this.btnName = this.mContext.getResources().getString(R.string.F2FAlertDialog_Btn_Back);
                if (!PortConst.enableMoreGame || MoreGameConst.enableInstallGame.size() <= 0) {
                    this.btnName2 = "";
                    this.btnNum = 1;
                } else {
                    this.btnName2 = this.mContext.getResources().getString(R.string.F2FAlertDialog_Btn_MoreGame);
                    this.btnNum = 2;
                }
                this.btnName3 = "";
                this.msgContent = this.mContext.getResources().getString(R.string.F2FAlertDialog_EVERYDAY_START_MSG).replace("%d", String.valueOf(CommonConst.ADD_EVERYDAY_GOLDEN_MONEY));
                return;
            case 2:
                this.btnNum = 1;
                this.btnName = this.mContext.getResources().getString(R.string.F2FAlertDialog_Btn_Back);
                this.btnName2 = "";
                this.btnName3 = "";
                this.msgContent = this.mContext.getResources().getString(R.string.F2FAlertDialog_PASSLEVEL_MSG).replace("%d", String.valueOf(CommonConst.PASS_LEVEL_GOLD_NUMBER));
                return;
            case 3:
            default:
                return;
            case 4:
                this.btnNum = 2;
                this.btnName = this.mContext.getResources().getString(R.string.F2FAlertDialog_Btn_Back);
                this.btnName2 = this.mContext.getResources().getString(R.string.F2FAlertDialog_Btn_Share);
                this.btnName3 = "";
                this.msgContent = this.mContext.getResources().getString(R.string.F2FAlertDialog_INVITE_IN_GAME_MSG);
                return;
            case 5:
                this.btnNum = 1;
                this.btnName = this.mContext.getResources().getString(R.string.F2FAlertDialog_Btn_Back);
                this.btnName2 = "";
                this.btnName3 = "";
                this.msgContent = this.mContext.getResources().getString(R.string.F2FAlertDialog_INVITE_CALLBACK_MSG).replace("%d", String.valueOf(CommonConst.INVITE_FRIEND_GOLD_NUMBER));
                return;
            case 8:
                this.btnNum = 2;
                this.btnName = this.mContext.getResources().getString(R.string.str_button_use);
                this.btnName2 = this.mContext.getResources().getString(R.string.F2FAlertDialog_Btn_Back);
                this.btnName3 = "";
                String propPerPanId = ShopConst.PropInfo[0][0];
                String propId = ShopConst.PropInfo[0][1];
                F2FGameActivity f2FGameActivity = this.mContext;
                this.msgContent = String.format(this.mContext.getResources().getString(R.string.store_tip_use_delayer), Integer.valueOf(F2FGameActivity.getTableLoad().getPropEnable(propPerPanId, propId)));
                return;
            case 9:
                this.btnNum = 2;
                this.btnName = this.mContext.getResources().getString(R.string.str_button_use);
                this.btnName2 = this.mContext.getResources().getString(R.string.F2FAlertDialog_Btn_Back);
                this.btnName3 = "";
                String propPerPanId2 = ShopConst.PropInfo[1][0];
                String propId2 = ShopConst.PropInfo[1][1];
                F2FGameActivity f2FGameActivity2 = this.mContext;
                this.msgContent = String.format(this.mContext.getResources().getString(R.string.store_tip_use_extraLife), Integer.valueOf(F2FGameActivity.getTableLoad().getPropEnable(propPerPanId2, propId2)));
                return;
            case 10:
                this.btnNum = 2;
                this.btnName = this.mContext.getResources().getString(R.string.str_button_goshop);
                this.btnName2 = this.mContext.getResources().getString(R.string.F2FAlertDialog_Btn_Back);
                this.btnName3 = "";
                this.msgContent = this.mContext.getResources().getString(R.string.store_tip_goshop_getdelayer);
                return;
            case 11:
                this.btnNum = 2;
                this.btnName = this.mContext.getResources().getString(R.string.str_button_goshop);
                this.btnName2 = this.mContext.getResources().getString(R.string.F2FAlertDialog_Btn_Back);
                this.btnName3 = "";
                this.msgContent = this.mContext.getResources().getString(R.string.store_tip_goshop_getextraLifes);
                return;
            case 12:
                this.btnNum = 2;
                this.btnName = this.mContext.getResources().getString(R.string.F2FAlertDialog_Btn_MARKET);
                this.btnName2 = this.mContext.getResources().getString(R.string.F2FAlertDialog_Btn_Back);
                this.btnName3 = "";
                this.msgContent = this.mContext.getResources().getString(R.string.F2F_Score_Our_Game);
                return;
            case 13:
                this.btnNum = 1;
                this.btnName = this.mContext.getResources().getString(R.string.str_button_yes);
                this.btnName2 = "";
                this.btnName3 = "";
                this.msgContent = this.mContext.getResources().getString(R.string.error_sd_cards_message);
                return;
            case 14:
                this.btnNum = 2;
                this.btnName = this.mContext.getResources().getString(R.string.str_button_yes);
                this.btnName2 = this.mContext.getResources().getString(R.string.str_button_no);
                this.btnName3 = "";
                this.msgContent = this.mContext.getResources().getString(R.string.error_message);
                return;
            case 15:
                this.titleName = this.mContext.getResources().getString(R.string.str_eixt_title);
                this.btnNum = 2;
                this.btnName = this.mContext.getResources().getString(R.string.str_button_yes);
                this.btnName2 = this.mContext.getResources().getString(R.string.str_button_no);
                this.btnName3 = "";
                this.msgContent = this.mContext.getResources().getString(R.string.str_eixt_content);
                return;
            case 16:
                this.btnNum = 2;
                this.btnName = this.mContext.getResources().getString(R.string.str_button_yes);
                this.btnName2 = this.mContext.getResources().getString(R.string.str_button_no);
                this.btnName3 = "";
                this.msgContent = this.mContext.getResources().getString(R.string.F2FAlertDialog_WIRELESS_SET);
                return;
            case 17:
                this.btnNum = 2;
                this.btnName = this.mContext.getResources().getString(R.string.str_button_yes);
                this.btnName2 = this.mContext.getResources().getString(R.string.str_button_cancel);
                this.btnName3 = "";
                this.msgContent = this.mContext.getResources().getString(R.string.F2FAlertDialog_SMS_CONFIRM);
                return;
            case CommonConst.F2FALERT_MSGID.SMS_SEND_FAIL:
                this.btnNum = 2;
                this.btnName = this.mContext.getResources().getString(R.string.str_button_yes);
                this.btnName2 = this.mContext.getResources().getString(R.string.str_button_cancel);
                this.btnName3 = "";
                this.msgContent = this.mContext.getResources().getString(R.string.F2FAlertDialog_SMS_FAIL);
                return;
            case 19:
                this.btnNum = 0;
                this.btnName = "";
                this.btnName2 = "";
                this.btnName3 = "";
                this.msgContent = this.mContext.getResources().getString(R.string.F2FAlertDialog_SMS_WAITING);
                return;
            case 20:
                this.btnNum = 1;
                this.btnName = this.mContext.getResources().getString(R.string.str_button_ok);
                this.btnName2 = "";
                this.btnName3 = "";
                this.msgContent = this.mContext.getResources().getString(R.string.F2FAlertDialog_SMS_OK);
                return;
            case 100:
                this.btnNum = 1;
                this.btnName = this.mContext.getResources().getString(R.string.str_button_yes);
                this.btnName2 = "";
                this.btnName3 = "";
                return;
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    public void onBtn1(int dialogTypeID, boolean pIsShowByType) {
        this.mContext.getEngine().getScene().clearChildScene();
        ((F2FScene) this.mContext.getEngine().getScene()).isChildSceneClick = true;
        if (pIsShowByType || this.mScene == null) {
            if (this.mContext.mGameScene != null && this.mContext.getEngine().getScene().equals(this.mContext.mGameScene)) {
                this.mContext.mGameScene.enableHud();
            }
            switch (dialogTypeID) {
                case 0:
                case 1:
                case 3:
                case 100:
                default:
                    return;
                case 2:
                    CommonConst.IS_GAMEOVER = true;
                    if (this.mContext.getmGameScene() != null) {
                        this.mContext.getmGameScene().showOKDialog();
                        return;
                    }
                    return;
                case 4:
                    CommonConst.IS_GAMEOVER = true;
                    if (this.mContext.getmGameScene() != null) {
                        this.mContext.getmGameScene().showOKDialog();
                        return;
                    }
                    return;
                case 5:
                    CommonConst.IS_GAMEOVER = true;
                    if (this.mContext.getmGameScene() != null) {
                        this.mContext.getmGameScene().showOKDialog();
                        return;
                    }
                    return;
                case 8:
                    if (this.mContext.getmGameScene() != null) {
                        this.mContext.getmGameScene().useProp(0);
                        return;
                    }
                    return;
                case 9:
                    if (this.mContext.getmGameScene() != null) {
                        this.mContext.getmGameScene().useProp(1);
                        return;
                    }
                    return;
                case 10:
                    CommonConst.LAST_F2FDIALOG_TYPE = 10;
                    CommonConst.FROM_GAMESCENE_TO_SHOP = true;
                    this.mContext.startActivity(new Intent(this.mContext, ShopActivity.class));
                    return;
                case 11:
                    CommonConst.LAST_F2FDIALOG_TYPE = 11;
                    CommonConst.FROM_GAMESCENE_TO_SHOP = true;
                    this.mContext.startActivity(new Intent(this.mContext, ShopActivity.class));
                    return;
                case 12:
                    if (this.mContext.getmGameScene() != null) {
                        this.mContext.getmGameScene().showScoreGame();
                        return;
                    }
                    return;
                case 13:
                    this.mContext.operateErrorBtn();
                    return;
                case 14:
                    this.mContext.operateErrorBtn();
                    return;
                case 15:
                    this.mContext.exitGame(true);
                    return;
                case 16:
                    this.mContext.setWireless();
                    return;
                case 17:
                    SMSConst.sendMSM(this.mContext);
                    return;
                case CommonConst.F2FALERT_MSGID.SMS_SEND_FAIL:
                    SMSConst.sendMSM(this.mContext);
                    return;
                case 20:
                    ((F2FScene) this.mContext.getEngine().getScene()).setSMSNextProcess();
                    return;
            }
        } else {
            this.mScene.onTextDialogOKBtn();
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    public void onBtn2(int dialogTypeID, boolean pIsShowByType) {
        this.mContext.getEngine().getScene().clearChildScene();
        ((F2FScene) this.mContext.getEngine().getScene()).isChildSceneClick = true;
        if (pIsShowByType || this.mScene == null) {
            if (this.mContext.mGameScene != null && this.mContext.getEngine().getScene().equals(this.mContext.mGameScene)) {
                this.mContext.mGameScene.enableHud();
            }
            switch (dialogTypeID) {
                case 0:
                case 2:
                case 3:
                case 15:
                case 16:
                case 100:
                default:
                    return;
                case 1:
                    this.mContext.setStatus(F2FGameActivity.Status.MORE_GAME);
                    return;
                case 4:
                    this.mContext.sendMailInGame();
                    return;
                case 5:
                    if (this.mContext.getmGameScene() != null) {
                        this.mContext.getmGameScene().showOKDialog();
                        return;
                    }
                    return;
                case 8:
                    if (this.mContext.getmGameScene() != null) {
                        this.mContext.getmGameScene().loadGameOverTips();
                        return;
                    }
                    return;
                case 9:
                    if (this.mContext.getmGameScene() != null) {
                        this.mContext.getmGameScene().loadGameOverTips();
                        return;
                    }
                    return;
                case 10:
                    if (this.mContext.getmGameScene() != null) {
                        this.mContext.getmGameScene().loadGameOverTips();
                        return;
                    }
                    return;
                case 11:
                    if (this.mContext.getmGameScene() != null) {
                        this.mContext.getmGameScene().loadGameOverTips();
                        return;
                    }
                    return;
                case 12:
                    if (this.mContext.getmGameScene() != null) {
                        this.mContext.getmGameScene().showGameOverDialog();
                        return;
                    }
                    return;
                case 13:
                    this.mContext.exitGame(false);
                    return;
                case 14:
                    this.mContext.exitGame(false);
                    return;
                case 17:
                    ((F2FScene) this.mContext.getEngine().getScene()).isSMSSendCanle = true;
                    return;
                case CommonConst.F2FALERT_MSGID.SMS_SEND_FAIL:
                    ((F2FScene) this.mContext.getEngine().getScene()).isSMSSendCanle = true;
                    return;
            }
        } else {
            this.mScene.onTextDialogCancelBtn();
        }
    }

    public void onBtn3(int dialogTypeID) {
    }
}
