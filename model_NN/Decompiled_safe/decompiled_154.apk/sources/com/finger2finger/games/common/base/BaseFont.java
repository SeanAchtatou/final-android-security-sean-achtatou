package com.finger2finger.games.common.base;

import android.graphics.Typeface;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.texture.Texture;

public class BaseFont extends Font {
    public BaseFont(Texture texture, Typeface typeface, float size, boolean antiAlias, int color, float praleValue) {
        super(texture, typeface, size * praleValue, antiAlias, color);
    }
}
