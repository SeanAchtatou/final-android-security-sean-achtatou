package com.finger2finger.games.common.message;

import com.finger2finger.games.common.activity.F2FGameActivity;

public class F2FMessage {
    public int messageId = -1;
    public F2FGameActivity.Status showInSceneStatus = F2FGameActivity.Status.UNDEFINED;

    public F2FMessage(int pMessageId, F2FGameActivity.Status pStatus) {
        this.messageId = pMessageId;
        this.showInSceneStatus = pStatus;
    }
}
