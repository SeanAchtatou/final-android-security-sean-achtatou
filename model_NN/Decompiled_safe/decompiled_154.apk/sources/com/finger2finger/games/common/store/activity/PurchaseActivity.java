package com.finger2finger.games.common.store.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.adknowledge.superrewards.SuperRewardsImpl;
import com.f2fgames.games.monkeybreak.lite.R;
import com.finger2finger.games.common.CommonConst;
import com.finger2finger.games.common.Utils;
import com.finger2finger.games.common.activity.F2FGameActivity;
import com.finger2finger.games.common.res.ShopConst;
import com.finger2finger.games.common.store.data.PersonalAccount;
import com.finger2finger.games.common.store.data.PersonalAccountTable;
import com.finger2finger.games.common.store.data.PersonalPannier;
import com.finger2finger.games.common.store.data.PersonalPannierTable;
import com.finger2finger.games.common.store.data.Product;
import com.finger2finger.games.common.store.data.ProductTable;
import com.finger2finger.games.common.store.data.Store;
import com.finger2finger.games.common.store.data.StoreTable;
import com.finger2finger.games.common.store.io.TableLoad;
import com.finger2finger.games.res.PortConst;
import java.util.ArrayList;

public class PurchaseActivity extends Activity {
    private Product good = null;
    private PersonalAccount mPersonalAccount;
    private PersonalAccountTable mPersonalAccountTable;
    private PersonalPannierTable mPersonalPannierTable;
    private ProductTable mProductTable;
    private StoreTable mStoreTable;
    private TableLoad mTableLoad;
    private String productId;
    private TextView product_comment;
    private ArrayList<Product> produtList = new ArrayList<>();
    private Button purchase_button_getitem_finger;
    private Button purchase_button_getitem_gold;
    private ImageView purchase_image;
    private TextView purchase_txt_name;
    private Button shop_button_getmore;
    private TextView shop_finger_point;
    private TextView shop_gold_point;
    private Store store = null;
    private String storeId;
    private ArrayList<Store> storeList = new ArrayList<>();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.setFullScreen(this);
        setContentView((int) R.layout.product_details);
        this.mTableLoad = F2FGameActivity.getTableLoad();
        if (this.mTableLoad != null) {
            this.mStoreTable = this.mTableLoad.getmStoreTable();
            this.mProductTable = this.mTableLoad.getmProductTable();
            this.produtList = this.mProductTable.getProductList();
            this.storeList = this.mStoreTable.getStoreList();
            this.mPersonalPannierTable = this.mTableLoad.getmPersonalPannierTable();
            this.mPersonalAccountTable = this.mTableLoad.getmPersonalAccountTable();
            this.mPersonalAccount = this.mPersonalAccountTable.getPersonalAccountList().get(0);
            findView();
            getExtras();
            loadPurchaseInfo();
            if (this.good != null) {
                updateView();
            }
        }
    }

    private void updateView() {
        this.purchase_txt_name.setText(this.good.getName());
        Bitmap bitMap = null;
        try {
            bitMap = com.finger2finger.games.common.store.io.Utils.loadBitmapFromAssets(this, this.good.getIcon());
        } catch (Exception e) {
            Log.e("F2FError", e.getMessage());
        }
        this.purchase_image.setImageBitmap(bitMap);
        if (this.good.getFingerprice() > 0) {
            this.purchase_button_getitem_finger.setText(String.valueOf(this.good.getFingerprice()));
            this.purchase_button_getitem_finger.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    PurchaseActivity.this.buyGood(true);
                }
            });
        } else {
            this.purchase_button_getitem_finger.setVisibility(8);
        }
        if (this.good.getGoldprice() > 0) {
            this.purchase_button_getitem_gold.setText(String.valueOf(this.good.getGoldprice()));
            this.purchase_button_getitem_gold.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    PurchaseActivity.this.buyGood(false);
                }
            });
        } else {
            this.purchase_button_getitem_gold.setVisibility(8);
        }
        this.product_comment.setText(this.good.getComment());
        updateViewFoorter();
    }

    private void findView() {
        this.purchase_button_getitem_finger = (Button) findViewById(R.id.purchase_button_getitem_finger);
        this.purchase_button_getitem_gold = (Button) findViewById(R.id.purchase_button_getitem_gold);
        this.purchase_txt_name = (TextView) findViewById(R.id.purchase_txt_name);
        this.purchase_image = (ImageView) findViewById(R.id.purchase_image);
        this.product_comment = (TextView) findViewById(R.id.product_comment);
        this.shop_finger_point = (TextView) findViewById(R.id.shop_finger_point);
        this.shop_gold_point = (TextView) findViewById(R.id.shop_gold_point);
        this.shop_button_getmore = (Button) findViewById(R.id.shop_button_getmore);
        this.shop_button_getmore.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                PurchaseActivity.this.startSuperRewards();
            }
        });
    }

    private void updateViewFoorter() {
        this.shop_finger_point.setText(String.valueOf((this.mPersonalAccount.getFinger_money_count() + this.mPersonalAccount.getBonus_finger_money()) - this.mPersonalAccount.getComsume_finger_money()));
        this.shop_gold_point.setText(String.valueOf(this.mPersonalAccount.getGolden_count()));
    }

    /* access modifiers changed from: private */
    public void startSuperRewards() {
        if (Utils.checkNetWork(this)) {
            new SuperRewardsImpl(com.finger2finger.games.framework.R.class).showOffers(this, PortConst.AppSupserRewardsHParameter, this.mPersonalAccount.getId(), com.adknowledge.superrewards.Utils.getCountryCode());
        } else {
            showDialogOK(getString(R.string.store_tip_network_inavailable));
        }
    }

    public void onResume() {
        if (this.mTableLoad != null) {
            this.mTableLoad.updateUserPoints();
            updateViewFoorter();
        }
        super.onResume();
    }

    private void getExtras() {
        try {
            Bundle bundle = getIntent().getExtras();
            this.storeId = bundle.getString(CommonConst.EXTRA_STORE_ID);
            this.productId = bundle.getString(CommonConst.EXTRA_PRODUCT_ID);
        } catch (Exception e) {
            Log.v("PurchaseActivity_getExtras_error", e.getMessage());
        }
    }

    private void loadPurchaseInfo() {
        int i = 0;
        while (true) {
            if (i < this.storeList.size()) {
                if (this.storeId.equals(this.storeList.get(i).getId()) && this.productId.equals(this.storeList.get(i).getProduct_id())) {
                    this.store = this.storeList.get(i);
                    break;
                }
                i++;
            } else {
                break;
            }
        }
        for (int i2 = 0; i2 < this.produtList.size(); i2++) {
            if (this.productId.equals(this.produtList.get(i2).getId())) {
                this.good = this.produtList.get(i2);
                return;
            }
        }
    }

    private void showDialog(String pTxt) {
        new AlertDialog.Builder(this).setTitle(getString(R.string.game_title_tips)).setMessage(pTxt).setPositiveButton(getResources().getString(R.string.str_button_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).create().show();
    }

    private void showDialogOK(String pTxt) {
        new AlertDialog.Builder(this).setTitle(getString(R.string.game_title_tips)).setMessage(pTxt).setPositiveButton(getResources().getString(R.string.str_button_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).create().show();
    }

    private void showDialogMoneyNotEnough(String pTxt) {
        new AlertDialog.Builder(this).setTitle(getString(R.string.game_title_tips)).setMessage(pTxt).setPositiveButton(getResources().getString(R.string.store_purchase_getmore_gold), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                PurchaseActivity.this.startSuperRewards();
            }
        }).setNegativeButton(getResources().getString(R.string.str_button_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).create().show();
    }

    /* access modifiers changed from: private */
    public void buyGood(boolean isFinger) {
        if (isFinger) {
            if (this.good.getFingerprice() > (this.mPersonalAccount.getFinger_money_count() + this.mPersonalAccount.getBonus_finger_money()) - this.mPersonalAccount.getComsume_finger_money()) {
                showDialogMoneyNotEnough(getResources().getString(R.string.store_tip_finger_notenough_error));
                return;
            }
        } else if (this.good.getGoldprice() > this.mPersonalAccount.getGolden_count()) {
            showDialogMoneyNotEnough(getResources().getString(R.string.store_tip_gold_notenough_error));
            return;
        }
        if (this.mTableLoad.checkProductTerm(this.storeId, this.productId)) {
            showDialog(getResources().getString(R.string.store_tip_expire));
        } else if (1 > this.store.getTotal_count()) {
            showDialog(getResources().getString(R.string.store_tip_purchase_count_error));
        } else if (this.mTableLoad.checkProductDailyLimit(this.storeId, this.productId, 1)) {
            showDialog(getResources().getString(R.string.store_tip_purchase_limit_error));
        } else {
            this.store.setTotal_count(this.store.getTotal_count() - 1);
            this.store.setSaled_count(this.store.getSaled_count() + 1);
            boolean isFind = false;
            int i = 0;
            while (true) {
                if (i >= this.mPersonalPannierTable.getPersonalPannierList().size()) {
                    break;
                }
                PersonalPannier pa = this.mPersonalPannierTable.getPersonalPannierList().get(i);
                if (pa.getProduct_id().equals(this.productId)) {
                    isFind = true;
                    pa.setProduct_count(pa.getProduct_count() + 1);
                    break;
                }
                i++;
            }
            if (!isFind) {
                try {
                    this.mPersonalPannierTable.getPersonalPannierList().add(new PersonalPannier(new String[]{ShopConst.PERSONALPANNIER_ID, this.mPersonalAccount.getId(), this.productId, String.valueOf(1)}));
                } catch (Exception e) {
                    Log.e("F2FError", e.getMessage());
                }
            }
            if (isFinger) {
                this.mPersonalAccount.setComsume_finger_money(this.mPersonalAccount.getComsume_finger_money() + (this.good.getFingerprice() * 1));
            } else {
                this.mPersonalAccount.setGolden_count(this.mPersonalAccount.getGolden_count() - (this.good.getGoldprice() * 1));
            }
            try {
                this.mStoreTable.write();
                this.mPersonalPannierTable.write();
                this.mPersonalAccountTable.write();
            } catch (Exception e2) {
                Log.e("F2FError", e2.getMessage());
            }
            showDialogOK(getResources().getString(R.string.store_tip_purchase_successful));
            if (this.good.getId().equals(ShopConst.Product_ID.DAILY_REMOVE_ADVIEW)) {
                CommonConst.DIALY_CLEAR_ADS_DEFAUT = System.currentTimeMillis() + 86400000;
            } else if (this.good.getId().equals(ShopConst.Product_ID.FOREVER_REMOVE_ADVIEW)) {
                CommonConst.FOREVER_CLEAR_ADS_DEFAUT = -1;
            }
            updateViewFoorter();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        finish();
        return true;
    }
}
