package com.finger2finger.games.common.scene;

import com.f2fgames.games.monkeybreak.lite.R;
import com.finger2finger.games.common.CommonConst;
import com.finger2finger.games.common.Utils;
import com.finger2finger.games.common.activity.F2FGameActivity;
import com.finger2finger.games.common.base.BaseFont;
import com.finger2finger.games.common.base.ParallaxLayer;
import com.finger2finger.games.common.res.CommonResource;
import com.finger2finger.games.res.Const;
import com.finger2finger.games.res.Resource;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.entity.primitive.Rectangle;
import org.anddev.andengine.entity.scene.CameraScene;
import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.entity.shape.modifier.IShapeModifier;
import org.anddev.andengine.entity.shape.modifier.MoveModifier;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.Text;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.util.HorizontalAlign;
import org.anddev.andengine.util.modifier.IModifier;

public class AboutScene extends CameraScene {
    private static final int mLayerCount = 4;
    private float AboutFrameHeight = 0.0f;
    private float AboutFrameWidth = 0.0f;
    private float aboutTextTitle_original_x;
    private float aboutTextTitle_original_y;
    private float aboutTextTitle_target_x;
    private float aboutTextTitle_target_y;
    private float aboutText_original_x;
    private float aboutText_original_y;
    private float aboutText_target_x;
    private float aboutText_target_y;
    private float aboutpic_original_x;
    private float aboutpic_original_y;
    private float aboutpic_target_x;
    private float aboutpic_target_y;
    private float backpic_original_x;
    private float backpic_original_y;
    private float backpic_target_x;
    private float backpic_target_y;
    /* access modifiers changed from: private */
    public Text mAboutText;
    /* access modifiers changed from: private */
    public Text mAboutTitle;
    F2FGameActivity mContext;
    private BaseFont mFontAbout;
    private BaseFont mFontAbout_Title;
    /* access modifiers changed from: private */
    public float mLastY = 0.0f;
    private Rectangle mRect1;
    private Rectangle mRectBlank;
    private Rectangle mRectDarkGreen;
    /* access modifiers changed from: private */
    public boolean mStopAout = false;
    private TextureRegion mbackRegion;
    private Sprite mbackSprite;

    public AboutScene(Camera pCamera, F2FGameActivity pContext) {
        super(4, pCamera);
        this.mContext = pContext;
        loadScene();
        setBackgroundEnabled(false);
    }

    public void loadScene() {
        loadCommonInfo();
        registerUpdateHandler(new IUpdateHandler() {
            public void reset() {
            }

            public void onUpdate(float pSecondsElapsed) {
                if (!AboutScene.this.mStopAout) {
                    AboutScene.this.updateAbout();
                }
            }
        });
        this.mRect1 = new Rectangle(0.0f, 0.0f, (float) CommonConst.CAMERA_WIDTH, (float) CommonConst.CAMERA_HEIGHT) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() != 0 || pTouchAreaLocalX <= ((float) (CommonConst.CAMERA_WIDTH / 2))) {
                    return true;
                }
                AboutScene.this.showAboutSceneMoveAnimation(false);
                return true;
            }
        };
        this.mRect1.setColor(CommonConst.GrayColor.red, CommonConst.GrayColor.green, CommonConst.GrayColor.blue);
        this.mRect1.setAlpha(CommonConst.GrayColor.alpha);
        getLayer(0).addEntity(this.mRect1);
        registerTouchArea(this.mRect1);
        float aboutPicWidth = this.AboutFrameWidth;
        float aboutPicHeight = this.AboutFrameHeight;
        float aboutPX = this.aboutpic_original_x;
        float aboutPY = this.aboutpic_original_y;
        if (this.mRectBlank == null || this.mRectDarkGreen == null) {
            this.mRectBlank = new Rectangle(aboutPX, aboutPY, aboutPicWidth, aboutPicHeight);
            this.mRectBlank.setColor(Const.RectBlankColor.red, Const.RectBlankColor.green, Const.RectBlankColor.blue);
            this.mRectDarkGreen = new Rectangle(aboutPX + (3.0f * CommonConst.RALE_SAMALL_VALUE), aboutPY + (3.0f * CommonConst.RALE_SAMALL_VALUE), aboutPicWidth - (6.0f * CommonConst.RALE_SAMALL_VALUE), aboutPicHeight - (6.0f * CommonConst.RALE_SAMALL_VALUE)) {
                boolean mGrabbed = false;

                public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                    float touchY = pSceneTouchEvent.getY();
                    switch (pSceneTouchEvent.getAction()) {
                        case 0:
                            float unused = AboutScene.this.mLastY = touchY;
                            this.mGrabbed = true;
                            boolean unused2 = AboutScene.this.mStopAout = this.mGrabbed;
                            break;
                        case 1:
                            if (this.mGrabbed) {
                                this.mGrabbed = false;
                                boolean unused3 = AboutScene.this.mStopAout = this.mGrabbed;
                                break;
                            }
                            break;
                        case 2:
                            if (this.mGrabbed) {
                                float distanceY = touchY - AboutScene.this.mLastY;
                                if (Math.abs(distanceY) > 10.0f * CommonConst.RALE_SAMALL_VALUE) {
                                    ((ParallaxLayer) AboutScene.this.getLayer(3)).setParallaxValue(distanceY);
                                    break;
                                }
                            }
                            break;
                    }
                    return true;
                }
            };
            this.mRectDarkGreen.setColor(Const.RectDarkColor.red, Const.RectDarkColor.green, Const.RectDarkColor.blue);
        }
        getLayer(1).addEntity(this.mRectBlank);
        getLayer(1).registerTouchArea(this.mRectBlank);
        getLayer(1).addEntity(this.mRectDarkGreen);
        getLayer(1).registerTouchArea(this.mRectDarkGreen);
        this.mbackSprite = new Sprite(this.backpic_original_x, this.backpic_original_y, CommonConst.RALE_SAMALL_VALUE * ((float) this.mbackRegion.getWidth()), CommonConst.RALE_SAMALL_VALUE * ((float) this.mbackRegion.getHeight()), this.mbackRegion) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() != 0) {
                    return true;
                }
                AboutScene.this.showAboutSceneMoveAnimation(false);
                return true;
            }
        };
        getLayer(2).addEntity(this.mbackSprite);
        ParallaxLayer parallaxLayer = new ParallaxLayer(2);
        String gameAboutTilte = this.mContext.getResources().getString(R.string.str_gameabout_tilte);
        String gameAboutText = this.mContext.getResources().getString(R.string.str_gameabout_content) + this.mContext.getResources().getString(R.string.framework_version);
        this.aboutTextTitle_target_x = (float) ((CommonConst.CAMERA_WIDTH / 4) - (this.mFontAbout_Title.getStringWidth(gameAboutTilte) / 2));
        this.aboutTextTitle_original_x = (float) ((-this.mFontAbout_Title.getStringWidth(gameAboutTilte)) * 2);
        this.aboutTextTitle_target_y = 30.0f;
        this.aboutTextTitle_original_y = 30.0f;
        this.mAboutTitle = new Text(this.aboutTextTitle_target_x, this.aboutTextTitle_target_y, this.mFontAbout_Title, gameAboutTilte, HorizontalAlign.CENTER);
        this.aboutText_target_x = (float) ((CommonConst.CAMERA_WIDTH / 4) - (Utils.getMaxCharacterLens(this.mFontAbout, gameAboutText) / 2));
        this.aboutText_original_x = this.aboutText_target_x - ((float) Utils.getMaxCharacterLens(this.mFontAbout, gameAboutText));
        float heightScaled = this.aboutTextTitle_target_y + this.mAboutTitle.getHeightScaled();
        this.aboutText_target_y = heightScaled;
        this.aboutText_original_y = heightScaled;
        this.mAboutText = new Text(this.aboutText_target_y, this.aboutText_target_y, this.mFontAbout, gameAboutText, HorizontalAlign.CENTER);
        parallaxLayer.addEntity(this.mAboutTitle);
        parallaxLayer.addEntity(this.mAboutText);
        setLayer(3, parallaxLayer);
        registerTouchArea(this.mbackSprite);
    }

    public void showAboutSceneMoveAnimation(final boolean isOn) {
        float f;
        float f2;
        float f3;
        float f4;
        this.mAboutTitle.setVisible(false);
        this.mAboutText.setVisible(false);
        this.mbackSprite.addShapeModifier(new MoveModifier(0.5f, isOn ? this.backpic_original_x : this.backpic_target_x, isOn ? this.backpic_target_x : this.backpic_original_x, isOn ? this.backpic_original_y : this.backpic_target_y, isOn ? this.backpic_target_y : this.backpic_original_y));
        this.mRectBlank.addShapeModifier(new MoveModifier(0.5f, isOn ? this.aboutpic_original_x : this.aboutpic_target_x, isOn ? this.aboutpic_target_x : this.aboutpic_original_x, isOn ? this.aboutpic_original_y : this.aboutpic_target_y, isOn ? this.aboutpic_target_y : this.aboutpic_original_y));
        this.mRectDarkGreen.addShapeModifier(new MoveModifier(0.5f, isOn ? this.aboutpic_original_x + (CommonConst.RALE_SAMALL_VALUE * 3.0f) : this.aboutpic_target_x + (CommonConst.RALE_SAMALL_VALUE * 3.0f), isOn ? this.aboutpic_target_x + (CommonConst.RALE_SAMALL_VALUE * 3.0f) : this.aboutpic_original_x + (CommonConst.RALE_SAMALL_VALUE * 3.0f), isOn ? this.aboutpic_original_y + (CommonConst.RALE_SAMALL_VALUE * 3.0f) : this.aboutpic_target_y + (CommonConst.RALE_SAMALL_VALUE * 3.0f), isOn ? this.aboutpic_target_y + (CommonConst.RALE_SAMALL_VALUE * 3.0f) : this.aboutpic_original_y + (CommonConst.RALE_SAMALL_VALUE * 3.0f), new IShapeModifier.IShapeModifierListener() {
            public /* bridge */ /* synthetic */ void onModifierFinished(IModifier x0, Object x1) {
                onModifierFinished((IModifier<IShape>) x0, (IShape) x1);
            }

            public void onModifierFinished(IModifier<IShape> iModifier, IShape arg1) {
                if (isOn) {
                    AboutScene.this.mAboutTitle.setVisible(true);
                    AboutScene.this.mAboutText.setVisible(true);
                }
            }
        }));
        this.mAboutTitle.addShapeModifier(new MoveModifier(0.5f, isOn ? this.aboutTextTitle_original_x : this.aboutTextTitle_target_x, isOn ? this.aboutTextTitle_target_x : this.aboutTextTitle_original_x, isOn ? this.aboutTextTitle_original_y : this.aboutTextTitle_target_y, isOn ? this.aboutTextTitle_target_y : this.aboutTextTitle_original_y));
        Text text = this.mAboutText;
        if (isOn) {
            f = this.aboutText_original_x;
        } else {
            f = this.aboutText_target_x;
        }
        if (isOn) {
            f2 = this.aboutText_target_x;
        } else {
            f2 = this.aboutText_original_x;
        }
        if (isOn) {
            f3 = this.aboutText_original_y;
        } else {
            f3 = this.aboutText_target_y;
        }
        if (isOn) {
            f4 = this.aboutText_target_y;
        } else {
            f4 = this.aboutText_original_y;
        }
        text.addShapeModifier(new MoveModifier(0.5f, f, f2, f3, f4, new IShapeModifier.IShapeModifierListener() {
            public /* bridge */ /* synthetic */ void onModifierFinished(IModifier x0, Object x1) {
                onModifierFinished((IModifier<IShape>) x0, (IShape) x1);
            }

            public void onModifierFinished(IModifier<IShape> iModifier, IShape pItem) {
                if (!isOn) {
                    AboutScene.this.mContext.getEngine().getScene().clearChildScene();
                }
            }
        }));
    }

    /* access modifiers changed from: private */
    public void updateAbout() {
        ParallaxLayer layer = (ParallaxLayer) getLayer(3);
        layer.setParallaxValue(layer.getParallaxValue() - 8.0f);
    }

    private void loadCommonInfo() {
        this.mbackRegion = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.BUTTON_LEFT.mKey);
        loadFont();
        setSpritePosition();
    }

    private void loadFont() {
        this.mFontAbout_Title = this.mContext.mResource.getBaseFontByKey(Resource.FONT.MAINMENU_ABOUTTITLE.mKey);
        this.mFontAbout = this.mContext.mResource.getBaseFontByKey(Resource.FONT.MAINMENU_ABOUTCONTENT.mKey);
    }

    private void setSpritePosition() {
        this.AboutFrameWidth = (float) CommonConst.CAMERA_WIDTH;
        this.AboutFrameHeight = ((float) CommonConst.CAMERA_HEIGHT) * 0.54f;
        this.aboutpic_original_x = (float) ((-CommonConst.CAMERA_WIDTH) / 2);
        this.aboutpic_original_y = 0.0f;
        this.aboutText_original_x = this.aboutpic_original_x;
        this.aboutText_original_y = this.aboutpic_original_y;
        this.aboutpic_target_x = 0.0f;
        this.aboutpic_target_y = 0.0f;
        this.backpic_original_x = ((float) (-this.mbackRegion.getWidth())) * CommonConst.RALE_SAMALL_VALUE;
        this.backpic_original_y = (float) ((CommonConst.CAMERA_HEIGHT * 3) / 5);
        this.AboutFrameWidth = ((float) CommonConst.CAMERA_WIDTH) * 0.5f;
        this.AboutFrameHeight = (float) CommonConst.CAMERA_HEIGHT;
        this.backpic_target_x = this.AboutFrameWidth - ((((float) this.mbackRegion.getWidth()) * CommonConst.RALE_SAMALL_VALUE) / 2.0f);
        this.backpic_target_y = (float) ((CommonConst.CAMERA_HEIGHT * 3) / 5);
    }
}
