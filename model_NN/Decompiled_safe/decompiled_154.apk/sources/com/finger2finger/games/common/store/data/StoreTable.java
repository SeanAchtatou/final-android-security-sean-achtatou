package com.finger2finger.games.common.store.data;

import com.finger2finger.games.common.store.io.Utils;
import java.util.ArrayList;

public class StoreTable {
    private ArrayList<Store> storeList = new ArrayList<>();

    public ArrayList<Store> getStoreList() {
        return this.storeList;
    }

    public void setStoreList(ArrayList<Store> storeList2) {
        this.storeList = storeList2;
    }

    public void createFile() throws Exception {
        Utils.createFile(TablePath.T_STORE_PATH);
    }

    public String[] readFile() throws Exception {
        return Utils.readFile(TablePath.T_STORE_PATH);
    }

    public void serlize(String[] data) throws Exception {
        Exception e;
        Store store = null;
        if (data != null && data.length > 0) {
            int i = 0;
            while (i < data.length) {
                try {
                    store = new Store(Utils.getSplitData(data[i], 7));
                    try {
                        this.storeList.add(store);
                        i++;
                    } catch (Exception e2) {
                        e = e2;
                        throw e;
                    }
                } catch (Exception e3) {
                    e = e3;
                    throw e;
                }
            }
        }
    }

    public void write() throws Exception {
        String[] data = new String[this.storeList.size()];
        for (int i = 0; i < this.storeList.size(); i++) {
            data[i] = this.storeList.get(i).toString();
        }
        if (data != null && data.length > 0) {
            Utils.writeFile(TablePath.T_STORE_PATH, data);
        }
    }
}
