package com.finger2finger.games.common.store.data;

import com.finger2finger.games.common.store.io.Utils;
import java.util.ArrayList;

public class ProductTable {
    private ArrayList<Product> ProductList = new ArrayList<>();

    public ArrayList<Product> getProductList() {
        return this.ProductList;
    }

    public void setProductList(ArrayList<Product> productList) {
        this.ProductList = productList;
    }

    public void createFile() throws Exception {
        Utils.createFile(TablePath.T_PRODUCT_PATH);
    }

    public String[] readFile() throws Exception {
        return Utils.readFile(TablePath.T_PRODUCT_PATH);
    }

    public void serlize(String[] data) throws Exception {
        Exception e;
        Product commodity = null;
        if (data != null && data.length > 0) {
            int i = 0;
            while (i < data.length) {
                try {
                    commodity = new Product(Utils.getSplitData(data[i], 6));
                    try {
                        this.ProductList.add(commodity);
                        i++;
                    } catch (Exception e2) {
                        e = e2;
                        throw e;
                    }
                } catch (Exception e3) {
                    e = e3;
                    throw e;
                }
            }
        }
    }

    public void write() throws Exception {
        String[] data = new String[this.ProductList.size()];
        for (int i = 0; i < this.ProductList.size(); i++) {
            data[i] = this.ProductList.get(i).toString();
        }
        if (data != null && data.length > 0) {
            Utils.writeFile(TablePath.T_PRODUCT_PATH, data);
        }
    }
}
