package com.finger2finger.games.common.store.data;

import com.finger2finger.games.common.store.io.Utils;
import java.util.ArrayList;

public class PersonalAccountTable {
    private ArrayList<PersonalAccount> personalAccountList = new ArrayList<>();

    public ArrayList<PersonalAccount> getPersonalAccountList() {
        return this.personalAccountList;
    }

    public void setPersonalAccountList(ArrayList<PersonalAccount> PersonalAccountList) {
        this.personalAccountList = PersonalAccountList;
    }

    public void createFile() throws Exception {
        Utils.createFile(TablePath.T_PER_ACC_PATH);
    }

    public String[] readFile() throws Exception {
        return Utils.readFile(TablePath.T_PER_ACC_PATH);
    }

    public void serlize(String[] data) throws Exception {
        Exception e;
        PersonalAccount personalAccount = null;
        if (data != null && data.length > 0) {
            int i = 0;
            while (i < data.length) {
                try {
                    personalAccount = new PersonalAccount(Utils.getSplitData(data[i], 7));
                    try {
                        this.personalAccountList.add(personalAccount);
                        i++;
                    } catch (Exception e2) {
                        e = e2;
                        throw e;
                    }
                } catch (Exception e3) {
                    e = e3;
                    throw e;
                }
            }
        }
    }

    public void write() throws Exception {
        String[] data = new String[this.personalAccountList.size()];
        for (int i = 0; i < this.personalAccountList.size(); i++) {
            data[i] = this.personalAccountList.get(i).toString();
        }
        if (data != null && data.length > 0) {
            Utils.writeFile(TablePath.T_PER_ACC_PATH, data);
        }
    }
}
