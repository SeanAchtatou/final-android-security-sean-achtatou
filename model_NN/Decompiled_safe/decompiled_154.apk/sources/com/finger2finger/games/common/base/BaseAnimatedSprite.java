package com.finger2finger.games.common.base;

import org.anddev.andengine.entity.sprite.AnimatedSprite;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;

public class BaseAnimatedSprite extends AnimatedSprite {
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BaseAnimatedSprite(float px, float py, float praleValue, TiledTextureRegion textureRegion, boolean pPostionEnble) {
        super(pPostionEnble ? px * praleValue : px, pPostionEnble ? py * praleValue : py, ((float) textureRegion.getTileWidth()) * praleValue, ((float) textureRegion.getTileHeight()) * praleValue, textureRegion);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public BaseAnimatedSprite(float r7, float r8, float r9, float r10, float r11, org.anddev.andengine.opengl.texture.region.TiledTextureRegion r12, boolean r13) {
        /*
            r6 = this;
            r2 = 1
            if (r13 != r2) goto L_0x0015
            float r0 = r7 * r11
            r1 = r0
        L_0x0006:
            if (r13 != r2) goto L_0x0017
            float r0 = r8 * r11
            r2 = r0
        L_0x000b:
            float r3 = r9 * r11
            float r4 = r10 * r11
            r0 = r6
            r5 = r12
            r0.<init>(r1, r2, r3, r4, r5)
            return
        L_0x0015:
            r1 = r7
            goto L_0x0006
        L_0x0017:
            r2 = r8
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.finger2finger.games.common.base.BaseAnimatedSprite.<init>(float, float, float, float, float, org.anddev.andengine.opengl.texture.region.TiledTextureRegion, boolean):void");
    }
}
