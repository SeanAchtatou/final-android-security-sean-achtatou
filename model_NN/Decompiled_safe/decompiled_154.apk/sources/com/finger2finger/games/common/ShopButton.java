package com.finger2finger.games.common;

import android.content.Intent;
import android.widget.Toast;
import com.f2fgames.games.monkeybreak.lite.R;
import com.finger2finger.games.common.activity.F2FGameActivity;
import com.finger2finger.games.common.activity.RankingListActivity;
import com.finger2finger.games.common.base.BaseSprite;
import com.finger2finger.games.common.res.CommonResource;
import com.finger2finger.games.common.res.MoreGameConst;
import com.finger2finger.games.common.store.activity.ShopActivity;
import com.finger2finger.games.res.Const;
import com.finger2finger.games.res.PortConst;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.texture.region.TextureRegion;

public class ShopButton {
    public String GO_MOREGAME = "/GoMoreGame_From";
    public String GO_PROMOTION = "/GoPromotion_From/%d/";
    public String GO_RANKINGLIST = "/GoRankingList_From";
    public String GO_SHOP = "/GoShop_From";
    public boolean enableMoreGame = true;
    public boolean enableShowPromtion = false;
    public BaseSprite moregameSprite;
    public int picWidth = 64;
    public BaseSprite promotionIconSprite;
    public BaseSprite rankinglistSprite;
    public BaseSprite shopSprite;

    public void showShop(F2FGameActivity pContext, Scene pScene, int pLayoutIndex, String pGoogleAnalyStr) {
        float shopPX = 0.0f;
        float offset = 15.0f * CommonConst.RALE_SAMALL_VALUE;
        if (PortConst.enableStoreMode) {
            TextureRegion shop = pContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.SHOP_ICON.mKey);
            float shopPX2 = ((float) shop.getWidth()) * 0.2f * CommonConst.RALE_SAMALL_VALUE;
            final F2FGameActivity f2FGameActivity = pContext;
            final String str = pGoogleAnalyStr;
            this.shopSprite = new BaseSprite(shopPX2, ((float) shop.getHeight()) * 0.2f * CommonConst.RALE_SAMALL_VALUE, CommonConst.RALE_SAMALL_VALUE, shop, false) {
                public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                    if (pSceneTouchEvent.getAction() == 1) {
                        f2FGameActivity.startActivity(new Intent(f2FGameActivity, ShopActivity.class));
                        GoogleAnalyticsUtils.setTracker(ShopButton.this.GO_SHOP + str);
                    }
                    return true;
                }
            };
            shopPX = shopPX2 + (((float) shop.getWidth()) * CommonConst.RALE_SAMALL_VALUE) + offset;
            pScene.registerTouchArea(this.shopSprite);
            pScene.getLayer(pLayoutIndex).addEntity(this.shopSprite);
        }
        if (PortConst.enableMoreGame && this.enableMoreGame) {
            TextureRegion moregame = pContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.MOREGAME_ICON.mKey);
            final F2FGameActivity f2FGameActivity2 = pContext;
            final String str2 = pGoogleAnalyStr;
            this.moregameSprite = new BaseSprite(shopPX + (((float) moregame.getWidth()) * 0.2f * CommonConst.RALE_SAMALL_VALUE), ((float) moregame.getHeight()) * 0.2f * CommonConst.RALE_SAMALL_VALUE, CommonConst.RALE_SAMALL_VALUE, moregame, false) {
                public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                    if (pSceneTouchEvent.getAction() == 1) {
                        ShopButton.this.unloadResouce(f2FGameActivity2);
                        GoogleAnalyticsUtils.setTracker(ShopButton.this.GO_MOREGAME + str2);
                    }
                    return true;
                }
            };
            shopPX = shopPX + (((float) moregame.getWidth()) * CommonConst.RALE_SAMALL_VALUE) + offset;
            pScene.registerTouchArea(this.moregameSprite);
            pScene.getLayer(pLayoutIndex).addEntity(this.moregameSprite);
        }
        if (Const.enableRankingList) {
            TextureRegion rankinglist = pContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.BUTTON_RANKINGLIST.mKey);
            final F2FGameActivity f2FGameActivity3 = pContext;
            final String str3 = pGoogleAnalyStr;
            this.rankinglistSprite = new BaseSprite(shopPX + (((float) rankinglist.getWidth()) * 0.2f * CommonConst.RALE_SAMALL_VALUE), ((float) rankinglist.getHeight()) * 0.2f * CommonConst.RALE_SAMALL_VALUE, CommonConst.RALE_SAMALL_VALUE, rankinglist, false) {
                public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                    if (pSceneTouchEvent.getAction() == 1) {
                        f2FGameActivity3.startActivity(new Intent(f2FGameActivity3, RankingListActivity.class));
                        GoogleAnalyticsUtils.setTracker(ShopButton.this.GO_RANKINGLIST + str3);
                    }
                    return true;
                }
            };
            shopPX = shopPX + (((float) rankinglist.getWidth()) * CommonConst.RALE_SAMALL_VALUE) + offset;
            pScene.registerTouchArea(this.rankinglistSprite);
            pScene.getLayer(pLayoutIndex).addEntity(this.rankinglistSprite);
        }
        if (PortConst.showPromtionIcon && this.enableShowPromtion) {
            TextureRegion promotionIcon = pContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.PROMOTION_ICON.mKey);
            if (promotionIcon != null) {
                final F2FGameActivity f2FGameActivity4 = pContext;
                this.promotionIconSprite = new BaseSprite(shopPX + (((float) this.picWidth) * 0.2f * CommonConst.RALE_SAMALL_VALUE), ((float) this.picWidth) * 0.2f * CommonConst.RALE_SAMALL_VALUE, (float) this.picWidth, (float) this.picWidth, CommonConst.RALE_SAMALL_VALUE, promotionIcon, false) {
                    public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                        if (pSceneTouchEvent.getAction() == 1) {
                            Utils.goPage(f2FGameActivity4, PortConst.loadLinkUrl);
                            GoogleAnalyticsUtils.setTracker(ShopButton.this.GO_PROMOTION.replace("%d", Const.GAME_NAME) + "/Url:" + PortConst.loadLinkUrl);
                        }
                        return true;
                    }
                };
                shopPX = shopPX + (this.promotionIconSprite.getWidth() * CommonConst.RALE_SAMALL_VALUE) + offset;
                pScene.registerTouchArea(this.promotionIconSprite);
                pScene.getLayer(pLayoutIndex).addEntity(this.promotionIconSprite);
            }
            TextureRegion promotionNewGame = pContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.NEW_GAME.mKey);
            if (promotionNewGame != null) {
                final F2FGameActivity f2FGameActivity5 = pContext;
                AnonymousClass5 r28 = new BaseSprite(shopPX - offset, ((float) promotionNewGame.getHeight()) * 0.2f * CommonConst.RALE_SAMALL_VALUE, CommonConst.RALE_SAMALL_VALUE, promotionNewGame, false) {
                    public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                        if (pSceneTouchEvent.getAction() == 1) {
                            Utils.goPage(f2FGameActivity5, PortConst.loadLinkUrl);
                            GoogleAnalyticsUtils.setTracker(ShopButton.this.GO_PROMOTION.replace("%d", Const.GAME_NAME) + "/Url:" + PortConst.loadLinkUrl);
                        }
                        return true;
                    }
                };
                float shopPX3 = shopPX + (r28.getWidth() * CommonConst.RALE_SAMALL_VALUE) + offset;
                pScene.registerTouchArea(r28);
                pScene.getLayer(pLayoutIndex).addEntity(r28);
            }
        }
    }

    /* access modifiers changed from: private */
    public void unloadResouce(F2FGameActivity pContext) {
        if (!PortConst.enableMoreGame) {
            return;
        }
        if (MoreGameConst.enableInstallGame.size() > 0) {
            pContext.setStatus(F2FGameActivity.Status.MORE_GAME);
        } else {
            Toast.makeText(pContext, pContext.getResources().getString(R.string.F2F_All_Game_Install), 1).show();
        }
    }
}
