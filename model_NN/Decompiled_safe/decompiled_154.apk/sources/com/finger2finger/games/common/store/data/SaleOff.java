package com.finger2finger.games.common.store.data;

public class SaleOff {
    public static final int LIST_ITEM_COUNT = 7;
    private String comment = "";
    private int count = 0;
    private String id = "";
    private String picture = "";
    private int rate = 0;
    private String store_id = "";
    private int term = 0;

    public String getId() {
        return this.id;
    }

    public String getComment() {
        return this.comment;
    }

    public String getPicture() {
        return this.picture;
    }

    public String getStore_id() {
        return this.store_id;
    }

    public int getRate() {
        return this.rate;
    }

    public int getTerm() {
        return this.term;
    }

    public int getCount() {
        return this.count;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public void setComment(String comment2) {
        this.comment = comment2;
    }

    public void setPicture(String picture2) {
        this.picture = picture2;
    }

    public void setStore_id(String storeId) {
        this.store_id = storeId;
    }

    public void setRate(int rate2) {
        this.rate = rate2;
    }

    public void setTerm(int term2) {
        this.term = term2;
    }

    public void setCount(int count2) {
        this.count = count2;
    }

    public SaleOff(String[] data) throws Exception {
        if (data == null || data.length != 7) {
            throw new IllegalArgumentException();
        }
        this.id = data[0];
        this.comment = data[1];
        this.picture = data[2];
        this.store_id = data[3];
        this.rate = Integer.parseInt(data[4]);
        this.term = Integer.parseInt(data[5]);
        this.count = Integer.parseInt(data[6]);
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(this.id).append(TablePath.SEPARATOR_ITEM).append(this.comment).append(TablePath.SEPARATOR_ITEM).append(this.picture).append(TablePath.SEPARATOR_ITEM).append(this.store_id).append(TablePath.SEPARATOR_ITEM).append(this.rate).append(TablePath.SEPARATOR_ITEM).append(this.term).append(TablePath.SEPARATOR_ITEM).append(this.count);
        return sb.toString();
    }
}
