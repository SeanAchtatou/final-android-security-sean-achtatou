package com.finger2finger.games.common;

public class DownLoadFile {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0071 A[SYNTHETIC, Splitter:B:29:0x0071] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0077 A[Catch:{ IOException -> 0x0089 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean downLoadFile(java.lang.String r13, java.lang.String r14) throws java.lang.Exception {
        /*
            r6 = 0
            r3 = 0
            r8 = 0
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x0098 }
            java.io.File r11 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ Exception -> 0x0098 }
            r2.<init>(r11, r14)     // Catch:{ Exception -> 0x0098 }
            java.net.URL r10 = new java.net.URL     // Catch:{ Exception -> 0x0098 }
            r10.<init>(r13)     // Catch:{ Exception -> 0x0098 }
            java.net.URLConnection r5 = r10.openConnection()     // Catch:{ Exception -> 0x0098 }
            java.net.HttpURLConnection r5 = (java.net.HttpURLConnection) r5     // Catch:{ Exception -> 0x0098 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0098 }
            r11.<init>()     // Catch:{ Exception -> 0x0098 }
            java.lang.String r12 = "bytes="
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ Exception -> 0x0098 }
            r12 = 0
            java.lang.String r12 = java.lang.String.valueOf(r12)     // Catch:{ Exception -> 0x0098 }
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ Exception -> 0x0098 }
            java.lang.String r12 = "-"
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ Exception -> 0x0098 }
            java.lang.String r9 = r11.toString()     // Catch:{ Exception -> 0x0098 }
            java.lang.String r11 = "RANGE"
            r5.setRequestProperty(r11, r9)     // Catch:{ Exception -> 0x0098 }
            java.io.InputStream r6 = r5.getInputStream()     // Catch:{ Exception -> 0x0098 }
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0098 }
            r11 = 1
            r4.<init>(r2, r11)     // Catch:{ Exception -> 0x0098 }
            r11 = 1024(0x400, float:1.435E-42)
            byte[] r0 = new byte[r11]     // Catch:{ Exception -> 0x0061, all -> 0x0095 }
        L_0x0048:
            int r7 = r6.read(r0)     // Catch:{ Exception -> 0x0061, all -> 0x0095 }
            if (r7 > 0) goto L_0x005c
            r8 = 1
            if (r4 == 0) goto L_0x009e
            r4.close()     // Catch:{ IOException -> 0x007c }
            r3 = 0
        L_0x0055:
            if (r6 == 0) goto L_0x005b
            r6.close()     // Catch:{ IOException -> 0x009b }
            r6 = 0
        L_0x005b:
            return r8
        L_0x005c:
            r11 = 0
            r4.write(r0, r11, r7)     // Catch:{ Exception -> 0x0061, all -> 0x0095 }
            goto L_0x0048
        L_0x0061:
            r11 = move-exception
            r1 = r11
            r3 = r4
        L_0x0064:
            java.lang.String r11 = "f2f_DownLoadFile_downLoadFile_Exception"
            java.lang.String r12 = r1.toString()     // Catch:{ all -> 0x006e }
            android.util.Log.e(r11, r12)     // Catch:{ all -> 0x006e }
            throw r1     // Catch:{ all -> 0x006e }
        L_0x006e:
            r11 = move-exception
        L_0x006f:
            if (r3 == 0) goto L_0x0075
            r3.close()     // Catch:{ IOException -> 0x0089 }
            r3 = 0
        L_0x0075:
            if (r6 == 0) goto L_0x007b
            r6.close()     // Catch:{ IOException -> 0x0089 }
            r6 = 0
        L_0x007b:
            throw r11
        L_0x007c:
            r11 = move-exception
            r1 = r11
            r3 = r4
        L_0x007f:
            java.lang.String r11 = "f2f_DownLoadFile_downLoadFile_IOException"
            java.lang.String r12 = r1.toString()
            android.util.Log.e(r11, r12)
            throw r1
        L_0x0089:
            r11 = move-exception
            r1 = r11
            java.lang.String r11 = "f2f_DownLoadFile_downLoadFile_IOException"
            java.lang.String r12 = r1.toString()
            android.util.Log.e(r11, r12)
            throw r1
        L_0x0095:
            r11 = move-exception
            r3 = r4
            goto L_0x006f
        L_0x0098:
            r11 = move-exception
            r1 = r11
            goto L_0x0064
        L_0x009b:
            r11 = move-exception
            r1 = r11
            goto L_0x007f
        L_0x009e:
            r3 = r4
            goto L_0x0055
        */
        throw new UnsupportedOperationException("Method not decompiled: com.finger2finger.games.common.DownLoadFile.downLoadFile(java.lang.String, java.lang.String):boolean");
    }
}
