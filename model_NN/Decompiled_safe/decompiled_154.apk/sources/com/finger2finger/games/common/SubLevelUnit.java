package com.finger2finger.games.common;

import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.Text;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.texture.region.TextureRegion;

public class SubLevelUnit {
    public ExplainEntity mExplainEntity;
    public NumberEntity mNumberEntity;
    public StarEntity mStarEntity;

    public StarEntity getmStarEntity() {
        return this.mStarEntity;
    }

    public NumberEntity getmNumberEntity() {
        return this.mNumberEntity;
    }

    public ExplainEntity getmExplainEntity() {
        return this.mExplainEntity;
    }

    public void setmStarEntity(int pStarLightCount, Scene pScene, int pLayer, float pStartX, float pStartY, TextureRegion pTRStarLight, TextureRegion pTRStarGray) {
        this.mStarEntity = new StarEntity(pStarLightCount, pScene, pLayer, pStartX, pStartY, pTRStarLight, pTRStarGray);
    }

    public void setmNumberEntity(int pLevelIndex, Scene pScene, int pLayer, float pStartX, float pStartY, TextureRegion[] pTRNumbers) {
        this.mNumberEntity = new NumberEntity(pLevelIndex, pScene, pLayer, pStartX, pStartY, pTRNumbers);
    }

    public void setmExplainEntity(Scene pScene, int pLayer, float pStartX, float pStartY, String pStream, Font pFont, boolean isEnable) {
        this.mExplainEntity = new ExplainEntity(pScene, pLayer, pStartX, pStartY, pStream, pFont, isEnable);
    }

    public void disable() {
        if (this.mStarEntity != null) {
            this.mStarEntity.disable();
        }
        if (this.mNumberEntity != null) {
            this.mNumberEntity.disable();
        }
        if (this.mExplainEntity != null) {
            this.mExplainEntity.disable();
        }
    }

    public void enable() {
        if (this.mStarEntity != null) {
            this.mStarEntity.enable();
        }
        if (this.mNumberEntity != null) {
            this.mNumberEntity.enable();
        }
        if (this.mExplainEntity != null) {
            this.mExplainEntity.enable();
        }
    }

    public class StarEntity {
        private int mLayer;
        private Scene mScene;
        private Sprite[] mSpriteStars = new Sprite[CommonConst.STAR_NUM];
        private int mStarCount = CommonConst.STAR_NUM;
        private int mStarLightCount = 3;
        private float mStartX;
        private float mStartY;
        private TextureRegion mTRStarGray;
        private TextureRegion mTRStarLight;

        public StarEntity(int pStarLightCount, Scene pScene, int pLayer, float pStartX, float pStartY, TextureRegion pTRStarLight, TextureRegion pTRStarGray) {
            this.mStartX = pStartX;
            this.mStartY = pStartY;
            this.mStarLightCount = pStarLightCount;
            this.mTRStarLight = pTRStarLight;
            this.mTRStarGray = pTRStarGray;
            this.mLayer = pLayer;
            this.mScene = pScene;
        }

        public void addStars() {
            TextureRegion pTRStar;
            float pX = this.mStartX;
            float pY = this.mStartY;
            for (int i = 0; i < this.mStarCount; i++) {
                if (i < this.mStarLightCount) {
                    pTRStar = this.mTRStarLight;
                } else {
                    pTRStar = this.mTRStarGray;
                }
                this.mSpriteStars[i] = new Sprite(pX, pY, ((float) pTRStar.getWidth()) * CommonConst.RALE_SAMALL_VALUE * CommonConst.RALE_SMALL_STAR, ((float) pTRStar.getHeight()) * CommonConst.RALE_SAMALL_VALUE * CommonConst.RALE_SMALL_STAR, pTRStar.clone());
                this.mScene.getLayer(this.mLayer).addEntity(this.mSpriteStars[i]);
                pX += ((((float) pTRStar.getWidth()) * CommonConst.RALE_SMALL_STAR) + 2.0f) * CommonConst.RALE_SAMALL_VALUE;
            }
        }

        public void disable() {
            for (int i = 0; i < this.mStarCount; i++) {
                if (this.mSpriteStars[i] != null) {
                    this.mSpriteStars[i].setVisible(false);
                }
            }
        }

        public void enable() {
            for (int i = 0; i < this.mStarCount; i++) {
                if (this.mSpriteStars[i] != null) {
                    this.mSpriteStars[i].setVisible(true);
                }
            }
        }
    }

    public class NumberEntity {
        private int mLayer;
        private int mLevelIndex;
        private Sprite[] mLevelIndexUnit = new Sprite[2];
        private Scene mScene;
        private float mStartX;
        private float mStartY;
        private TextureRegion[] mTRLevelIndex;

        public NumberEntity(int pLevelIndex, Scene pScene, int pLayer, float pStartX, float pStartY, TextureRegion[] pTRNumbers) {
            this.mStartX = pStartX;
            this.mStartY = pStartY;
            this.mLevelIndex = pLevelIndex;
            this.mLayer = pLayer;
            this.mScene = pScene;
            this.mTRLevelIndex = pTRNumbers;
        }

        public void addLevelIndex() {
            int gewei = (this.mLevelIndex + 1) % 10;
            int shiwei = (this.mLevelIndex + 1) / 10;
            if (shiwei > 0) {
                float width = ((float) this.mTRLevelIndex[shiwei].getWidth()) * CommonConst.RALE_SAMALL_VALUE;
                float height = ((float) this.mTRLevelIndex[shiwei].getHeight()) * CommonConst.RALE_SAMALL_VALUE;
                this.mLevelIndexUnit[0] = new Sprite(this.mStartX - (((width + (5.0f * CommonConst.RALE_SAMALL_VALUE)) + (((float) this.mTRLevelIndex[gewei].getWidth()) * CommonConst.RALE_SAMALL_VALUE)) / 2.0f), this.mStartY - ((height * 3.0f) / 4.0f), ((float) this.mTRLevelIndex[shiwei].getWidth()) * CommonConst.RALE_SAMALL_VALUE, ((float) this.mTRLevelIndex[shiwei].getHeight()) * CommonConst.RALE_SAMALL_VALUE, this.mTRLevelIndex[shiwei]);
                this.mLevelIndexUnit[1] = new Sprite(this.mLevelIndexUnit[0].getX() + (((float) (this.mTRLevelIndex[shiwei].getWidth() + 5)) * CommonConst.RALE_SAMALL_VALUE), this.mStartY - ((height * 3.0f) / 4.0f), ((float) this.mTRLevelIndex[gewei].getWidth()) * CommonConst.RALE_SAMALL_VALUE, ((float) this.mTRLevelIndex[gewei].getHeight()) * CommonConst.RALE_SAMALL_VALUE, this.mTRLevelIndex[gewei]);
                this.mScene.getLayer(this.mLayer).addEntity(this.mLevelIndexUnit[0]);
                this.mScene.getLayer(this.mLayer).addEntity(this.mLevelIndexUnit[1]);
                return;
            }
            this.mLevelIndexUnit[0] = new Sprite(this.mStartX - ((((float) this.mTRLevelIndex[shiwei].getWidth()) * CommonConst.RALE_SAMALL_VALUE) / 2.0f), this.mStartY - (((((float) this.mTRLevelIndex[shiwei].getHeight()) * CommonConst.RALE_SAMALL_VALUE) * 3.0f) / 4.0f), ((float) this.mTRLevelIndex[gewei].getWidth()) * CommonConst.RALE_SAMALL_VALUE, ((float) this.mTRLevelIndex[gewei].getHeight()) * CommonConst.RALE_SAMALL_VALUE, this.mTRLevelIndex[gewei]);
            this.mScene.getLayer(this.mLayer).addEntity(this.mLevelIndexUnit[0]);
        }

        public void disable() {
            if (this.mLevelIndexUnit[0] != null) {
                this.mLevelIndexUnit[0].setVisible(false);
            }
            if (this.mLevelIndexUnit[1] != null) {
                this.mLevelIndexUnit[1].setVisible(false);
            }
        }

        public void enable() {
            if (this.mLevelIndexUnit[0] != null) {
                this.mLevelIndexUnit[0].setVisible(true);
            }
            if (this.mLevelIndexUnit[1] != null) {
                this.mLevelIndexUnit[1].setVisible(true);
            }
        }
    }

    public class ExplainEntity {
        private Font mFont;
        private boolean mIsEnable = false;
        private int mLayer;
        private Scene mScene;
        private float mStartX;
        private float mStartY;
        private String mStream;
        private Text mText;

        public ExplainEntity(Scene pScene, int pLayer, float pStartX, float pStartY, String pStream, Font pFont, boolean isEnable) {
            this.mScene = pScene;
            this.mLayer = pLayer;
            this.mStream = pStream;
            this.mFont = pFont;
            this.mStartX = pStartX;
            this.mStartY = pStartY;
            this.mIsEnable = isEnable;
        }

        public void addExplain() {
            this.mText = new Text(this.mStartX, this.mStartY, this.mFont, this.mStream);
            this.mText.setPosition(this.mStartX - (this.mText.getWidth() / 2.0f), this.mStartY);
            if (!this.mIsEnable) {
                this.mText.setColor(0.3f, 0.3f, 0.3f);
            }
            this.mScene.getLayer(this.mLayer).addEntity(this.mText);
        }

        public void disable() {
            if (this.mText != null) {
                this.mText.setVisible(false);
            }
        }

        public void enable() {
            if (this.mText != null) {
                this.mText.setVisible(true);
            }
        }
    }
}
