package com.finger2finger.games.common.store.data;

public class PersonalAccount {
    public static final int LIST_ITEM_COUNT = 7;
    private int bonus_finger_money = 0;
    private int comsume_finger_money = 0;
    private String email = "";
    private int finger_money_count = 0;
    private int golden_count = 0;
    private String id = "";
    private long login_date = 0;

    public int getBonus_finger_money() {
        return this.bonus_finger_money;
    }

    public void setBonus_finger_money(int bonusFingerMoney) {
        this.bonus_finger_money = bonusFingerMoney;
    }

    public int getComsume_finger_money() {
        return this.comsume_finger_money;
    }

    public void setComsume_finger_money(int comsumeFingerMoney) {
        this.comsume_finger_money = comsumeFingerMoney;
    }

    public long getLogin_date() {
        return this.login_date;
    }

    public void setLogin_date(long loginDate) {
        this.login_date = loginDate;
    }

    public String getId() {
        return this.id;
    }

    public String getEmail() {
        return this.email;
    }

    public int getGolden_count() {
        return this.golden_count;
    }

    public int getFinger_money_count() {
        return this.finger_money_count;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public void setEmail(String email2) {
        this.email = email2;
    }

    public void setGolden_count(int goldenCount) {
        this.golden_count = goldenCount;
    }

    public void setFinger_money_count(int fingerMoneyCount) {
        this.finger_money_count = fingerMoneyCount;
    }

    public PersonalAccount(String[] data) throws Exception {
        if (data == null || data.length != 7) {
            throw new IllegalArgumentException();
        }
        this.id = data[0];
        this.email = data[1];
        this.golden_count = Integer.parseInt(data[2]);
        this.finger_money_count = Integer.parseInt(data[3]);
        this.login_date = Long.parseLong(data[4]);
        this.comsume_finger_money = Integer.parseInt(data[5]);
        this.bonus_finger_money = Integer.parseInt(data[6]);
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(this.id).append(TablePath.SEPARATOR_ITEM).append(this.email).append(TablePath.SEPARATOR_ITEM).append(this.golden_count).append(TablePath.SEPARATOR_ITEM).append(this.finger_money_count).append(TablePath.SEPARATOR_ITEM).append(this.login_date).append(TablePath.SEPARATOR_ITEM).append(this.comsume_finger_money).append(TablePath.SEPARATOR_ITEM).append(this.bonus_finger_money);
        return sb.toString();
    }
}
