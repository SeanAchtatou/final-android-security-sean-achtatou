package com.finger2finger.games.common;

import android.content.Intent;
import android.content.res.Resources;
import android.widget.Toast;
import com.f2fgames.games.monkeybreak.lite.R;
import com.finger2finger.games.common.activity.F2FGameActivity;

public class F2FMailSender {
    public static int SEND_IN_GAME = 12345;
    public static int SEND_IN_MAINMENU = 1234;

    public static void setMail(F2FGameActivity pContext, int mailType) {
        if (!Utils.checkEmailEnable(pContext)) {
            Toast.makeText(pContext, pContext.getResources().getString(R.string.F2FMail_Disalbe), 0).show();
            return;
        }
        Intent intent = new Intent("android.intent.action.SEND");
        intent.setType("plain/text");
        intent.putExtra("android.intent.extra.SUBJECT", pContext.getResources().getString(R.string.F2FMail_Title));
        intent.putExtra("android.intent.extra.TEXT", getMailContext(pContext));
        pContext.startActivityForResult(intent, mailType);
    }

    public static String getMailContext(F2FGameActivity pContext) {
        String str = pContext.getPackageName();
        Resources rs = pContext.getResources();
        return ((((((rs.getString(R.string.F2FMail_Content01) + rs.getString(R.string.F2FMail_Content02)) + rs.getString(R.string.F2FMail_Content03)) + rs.getString(R.string.F2FMail_Content04).replace("%s", rs.getString(R.string.app_name))) + rs.getString(R.string.F2FMail_Content05).replace("%s", "https://market.android.com/details?id=" + str)) + rs.getString(R.string.F2FMail_Content06)) + rs.getString(R.string.F2FMail_Content07)) + rs.getString(R.string.F2FMail_Content08);
    }
}
