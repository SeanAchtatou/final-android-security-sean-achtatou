package com.finger2finger.games.common.scene;

import com.finger2finger.games.common.CommonConst;
import com.finger2finger.games.common.activity.F2FGameActivity;
import com.finger2finger.games.common.res.CommonResource;
import com.finger2finger.games.res.PortConst;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.entity.scene.background.ColorBackground;
import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.entity.shape.Shape;
import org.anddev.andengine.entity.shape.modifier.AlphaModifier;
import org.anddev.andengine.entity.shape.modifier.IShapeModifier;
import org.anddev.andengine.entity.shape.modifier.SequenceShapeModifier;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;
import org.anddev.andengine.util.modifier.IModifier;

public class LogoScene extends F2FScene {
    private boolean isCreatePartnerLogo = false;
    /* access modifiers changed from: private */
    public boolean isLoadParnterLogo = false;
    /* access modifiers changed from: private */
    public boolean isReadyLoadParterLogo = false;
    private long lastTime = 0;
    private int mLogoIndex = 0;
    private TextureRegion mLogoTextureRegion;
    private Texture mParterLogoTexture;
    private int mPartnerLogoCount;
    private long mTime4PerImageDuration;

    public LogoScene(F2FGameActivity pContext) {
        super(1, pContext);
        loadScene();
    }

    public void loadScene() {
        this.mContext.commonResource.loadLogoTextures();
        if (PortConst.PARTERNERLOGO_PATH.length > 0) {
            this.mPartnerLogoCount = PortConst.PARTERNERLOGO_PATH.length;
            this.mTime4PerImageDuration = PortConst.PARTERNERLOGO_TIME / ((long) PortConst.PARTERNERLOGO_PATH.length);
            this.mContext.commonResource.loadPartnerLogoTextures();
            this.mLogoTextureRegion = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.PARTERLOGO.mKey);
            this.mParterLogoTexture = this.mLogoTextureRegion.getTexture();
            this.isLoadParnterLogo = true;
        }
        setBackground(new ColorBackground(1.0f, 1.0f, 1.0f));
        TextureRegion logoTextureRegion = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.LOGO_SELF.mKey);
        Sprite logo = new Sprite((((float) CommonConst.CAMERA_WIDTH) - (((float) logoTextureRegion.getWidth()) * CommonConst.RALE_SAMALL_VALUE)) / 2.0f, (((float) CommonConst.CAMERA_HEIGHT) - (((float) logoTextureRegion.getHeight()) * CommonConst.RALE_SAMALL_VALUE)) / 2.0f, ((float) logoTextureRegion.getWidth()) * CommonConst.RALE_SAMALL_VALUE, ((float) logoTextureRegion.getHeight()) * CommonConst.RALE_SAMALL_VALUE, logoTextureRegion);
        logo.setBlendFunction(Shape.BLENDFUNCTION_SOURCE_DEFAULT, 771);
        logo.addShapeModifier(new SequenceShapeModifier(new IShapeModifier.IShapeModifierListener() {
            public /* bridge */ /* synthetic */ void onModifierFinished(IModifier x0, Object x1) {
                onModifierFinished((IModifier<IShape>) x0, (IShape) x1);
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.finger2finger.games.common.scene.LogoScene.access$102(com.finger2finger.games.common.scene.LogoScene, boolean):boolean
             arg types: [com.finger2finger.games.common.scene.LogoScene, int]
             candidates:
              com.finger2finger.games.common.scene.F2FScene.access$102(com.finger2finger.games.common.scene.F2FScene, android.view.MotionEvent):android.view.MotionEvent
              com.finger2finger.games.common.scene.LogoScene.access$102(com.finger2finger.games.common.scene.LogoScene, boolean):boolean */
            public void onModifierFinished(IModifier<IShape> iModifier, IShape pItem) {
                if (LogoScene.this.isLoadParnterLogo) {
                    boolean unused = LogoScene.this.isReadyLoadParterLogo = true;
                } else {
                    LogoScene.this.mContext.setStatus(F2FGameActivity.Status.MAINMENU);
                }
            }
        }, new AlphaModifier(3.0f, 1.0f, 1.0f), new AlphaModifier(2.0f, 1.0f, 0.0f)));
        getTopLayer().addEntity(logo);
        registerUpdateHandler(new IUpdateHandler() {
            public void reset() {
            }

            public void onUpdate(float arg0) {
                if (LogoScene.this.isLoadParnterLogo && LogoScene.this.isReadyLoadParterLogo) {
                    LogoScene.this.updatePartnerLogo();
                }
            }
        });
        this.lastTime = System.currentTimeMillis();
    }

    private void createPartnerLogo() {
        getTopLayer().clear();
        Sprite mSpriteLogo = new Sprite(((float) ((CommonConst.CAMERA_WIDTH - this.mLogoTextureRegion.getWidth()) / 2)) * CommonConst.RALE_SAMALL_VALUE, ((float) ((CommonConst.CAMERA_HEIGHT - this.mLogoTextureRegion.getHeight()) / 2)) * CommonConst.RALE_SAMALL_VALUE, ((float) this.mLogoTextureRegion.getWidth()) * CommonConst.RALE_SAMALL_VALUE, ((float) this.mLogoTextureRegion.getHeight()) * CommonConst.RALE_SAMALL_VALUE, this.mLogoTextureRegion);
        if (mSpriteLogo.getWidth() < mSpriteLogo.getHeight()) {
            mSpriteLogo.setRotation(-90.0f);
        }
        mSpriteLogo.setBlendFunction(Shape.BLENDFUNCTION_SOURCE_DEFAULT, 771);
        getTopLayer().addEntity(mSpriteLogo);
    }

    /* access modifiers changed from: private */
    public void updatePartnerLogo() {
        if (!this.isCreatePartnerLogo) {
            createPartnerLogo();
            this.isCreatePartnerLogo = true;
        }
        if (System.currentTimeMillis() - this.lastTime < this.mTime4PerImageDuration) {
            return;
        }
        if (this.mLogoIndex >= this.mPartnerLogoCount - 1) {
            this.mContext.setStatus(F2FGameActivity.Status.MAINMENU);
            this.isReadyLoadParterLogo = false;
            return;
        }
        this.mLogoIndex++;
        TextureRegionFactory.createFromAsset(this.mParterLogoTexture, this.mContext, PortConst.PARTERNERLOGO_PATH[this.mLogoIndex], 0, 0);
        this.lastTime = System.currentTimeMillis();
    }
}
