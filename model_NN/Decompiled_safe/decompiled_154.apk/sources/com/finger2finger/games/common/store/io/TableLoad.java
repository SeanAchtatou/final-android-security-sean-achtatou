package com.finger2finger.games.common.store.io;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.adknowledge.superrewards.model.SRUserPoints;
import com.finger2finger.games.common.CommonConst;
import com.finger2finger.games.common.GoogleAnalyticsUtils;
import com.finger2finger.games.common.Utils;
import com.finger2finger.games.common.res.ShopConst;
import com.finger2finger.games.common.store.data.GameTable;
import com.finger2finger.games.common.store.data.PersonalAccount;
import com.finger2finger.games.common.store.data.PersonalAccountTable;
import com.finger2finger.games.common.store.data.PersonalPannier;
import com.finger2finger.games.common.store.data.PersonalPannierTable;
import com.finger2finger.games.common.store.data.ProductTable;
import com.finger2finger.games.common.store.data.SaleOffTable;
import com.finger2finger.games.common.store.data.StoreTable;
import com.finger2finger.games.common.store.data.TablePath;
import com.finger2finger.games.res.PortConst;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TableLoad {
    private String accountId = "";
    private char[] ch = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    private long lastLogin_date = 0;
    private final int mActivityAccount = 0;
    private Context mContext;
    private GameTable mGameTable = new GameTable();
    private long mLogin_date = 0;
    private PersonalAccountTable mPersonalAccountTable = new PersonalAccountTable();
    private PersonalPannierTable mPersonalPannierTable = new PersonalPannierTable();
    private ProductTable mProductTable = new ProductTable();
    private SaleOffTable mSaleOffTable = new SaleOffTable();
    private StoreTable mStoreTable = new StoreTable();

    public TableLoad(Context pContext) {
        this.mContext = pContext;
    }

    public StoreTable getmStoreTable() {
        return this.mStoreTable;
    }

    public PersonalAccountTable getmPersonalAccountTable() {
        return this.mPersonalAccountTable;
    }

    public PersonalPannierTable getmPersonalPannierTable() {
        return this.mPersonalPannierTable;
    }

    public ProductTable getmProductTable() {
        return this.mProductTable;
    }

    public GameTable getmGameTable() {
        return this.mGameTable;
    }

    public SaleOffTable getmSaleOffTable() {
        return this.mSaleOffTable;
    }

    public void initialize() throws Exception {
        try {
            Utils.createDir(TablePath.ROOT_PATH);
            Utils.createDir(TablePath.GAME_PATH);
            Utils.createDir(TablePath.PERSONAL_PATH);
            if (!Utils.isFileExist(TablePath.T_PER_ACC_PATH)) {
                try {
                    Utils.createFile(TablePath.T_PER_ACC_PATH);
                    createPersonalAccount();
                } catch (Exception e) {
                    Exception e2 = e;
                    Log.e("f2f_TableLoad_createPersonalAccount_error", e2.toString());
                    GoogleAnalyticsUtils.setTracker("f2f_TableLoad_createPersonalAccount_error,detail is " + e2.toString());
                    CommonConst.errorLevel = CommonConst.ERROR_LEVEL.UNREPAIR;
                    throw e2;
                }
            } else {
                PersonalAccountProcess();
            }
            loadTable();
            updateUserPoints();
            try {
                updateDailySaledCount(this.lastLogin_date);
            } catch (Exception e3) {
                Exception e4 = e3;
                Log.e("f2f_TableLoad_updateDailySaledCount_error", e4.toString());
                GoogleAnalyticsUtils.setTracker("f2f_TableLoad_updateDailySaledCount_error,detail is " + e4.toString());
                CommonConst.errorLevel = CommonConst.ERROR_LEVEL.UNREPAIR;
                throw e4;
            }
        } catch (Exception e5) {
            Exception e6 = e5;
            Log.e("f2f_TableLoad_createDir_error", e6.toString());
            GoogleAnalyticsUtils.setTracker("f2f_TableLoad_createDir_error,detail is " + e6.toString());
            CommonConst.errorLevel = CommonConst.ERROR_LEVEL.UNREPAIR;
            throw e6;
        }
    }

    private void PersonalAccountProcess() throws Exception {
        String[] data = null;
        try {
            data = this.mPersonalAccountTable.readFile();
        } catch (Exception e) {
            Exception e2 = e;
            CommonConst.errorList.add(CommonConst.ERROR_LEVEL.PERSONALACCOUNT_REPAIR);
            Log.e("f2f_TableLoad_PersonalAccountProcess_mPersonalAccountTable_readFile_error", e2.toString());
            GoogleAnalyticsUtils.setTracker("f2f_TableLoad_PersonalAccountProcess_mPersonalAccountTable_readFile_error,detail is " + e2.toString());
        }
        try {
            this.mPersonalAccountTable.serlize(data);
            this.lastLogin_date = this.mPersonalAccountTable.getPersonalAccountList().get(0).getLogin_date();
            if (PortConst.enableAddGold) {
                if (Integer.parseInt(String.valueOf(new SimpleDateFormat("yyyyMMdd").format(new Date(System.currentTimeMillis())))) > Integer.parseInt(String.valueOf(new SimpleDateFormat("yyyyMMdd").format(new Date(this.lastLogin_date))))) {
                    CommonConst.ADD_EVERYDAY_GOLDEN_MONEY = getGameInstallName() + 2;
                    this.mPersonalAccountTable.getPersonalAccountList().get(0).setGolden_count(this.mPersonalAccountTable.getPersonalAccountList().get(0).getGolden_count() + CommonConst.ADD_EVERYDAY_GOLDEN_MONEY);
                    CommonConst.IS_ADD_EVERYDAY_GOLDEN_MONEY = true;
                }
            }
            this.mLogin_date = System.currentTimeMillis();
            this.mPersonalAccountTable.getPersonalAccountList().get(0).setLogin_date(this.mLogin_date);
            this.accountId = this.mPersonalAccountTable.getPersonalAccountList().get(0).getId();
            try {
                this.mPersonalAccountTable.write();
            } catch (Exception e3) {
                Exception e4 = e3;
                Log.e("f2f_TableLoad_mPersonalAccountTable_write_error", e4.toString());
                GoogleAnalyticsUtils.setTracker("f2f_TableLoad_mPersonalAccountTable_write_error,detail is " + e4.toString());
                CommonConst.errorLevel = CommonConst.ERROR_LEVEL.UNREPAIR;
                throw e4;
            }
        } catch (Exception e5) {
            Exception e6 = e5;
            CommonConst.errorList.add(CommonConst.ERROR_LEVEL.PERSONALACCOUNT_REPAIR);
            Log.e("f2f_TableLoad_PersonalAccountProcess_mPersonalAccountTable_serlize_error", e6.toString());
            GoogleAnalyticsUtils.setTracker("f2f_TableLoad_PersonalAccountProcess_mPersonalAccountTable_serlize_error,detail is " + e6.toString());
            throw e6;
        }
    }

    private void createPersonalAccount() throws Exception {
        String emailAddress = "";
        Account[] arr$ = AccountManager.get(this.mContext).getAccountsByType("com.google");
        if (0 < arr$.length) {
            emailAddress = arr$[0].name;
        }
        this.accountId = ((TelephonyManager) this.mContext.getSystemService("phone")).getDeviceId();
        this.mLogin_date = System.currentTimeMillis();
        this.mPersonalAccountTable.getPersonalAccountList().add(new PersonalAccount(new String[]{this.accountId, emailAddress, String.valueOf(10), String.valueOf(0), String.valueOf(this.mLogin_date), String.valueOf(0), String.valueOf(5)}));
        CommonConst.IS_FIRST_PLAY_GAME = true;
        this.mPersonalAccountTable.write();
    }

    public void updateUserPoints() {
        if (Utils.checkNetWork(this.mContext)) {
            try {
                SRUserPoints user = new SRUserPoints(this.mContext.getApplicationContext());
                user.updatePoints(PortConst.AppSupserRewardsHParameter, this.mPersonalAccountTable.getPersonalAccountList().get(0).getId());
                int totalPoint = user.getTotalPoints();
                if (totalPoint > this.mPersonalAccountTable.getPersonalAccountList().get(0).getFinger_money_count()) {
                    this.mPersonalAccountTable.getPersonalAccountList().get(0).setFinger_money_count(totalPoint);
                }
            } catch (Exception e) {
                Log.e("TableLoad_UpdateUserPoints_Error", e.getMessage());
                GoogleAnalyticsUtils.setTracker("TableLoad_UpdateUserPoints_Error,detail is can't update use point.");
            }
        }
    }

    private void loadTable() throws Exception {
        loadStoreTable(false);
        loadPersonalPannierTable(false);
        loadProductTable(false);
        loadGameTable(false);
        loadSaleOffTable(false);
    }

    private String[] loadOriginalData(String[] data) throws Exception {
        if (data == null || data.length == 0) {
            return null;
        }
        return OriginalDataProcess.AES1282Strings(OriginalDataProcess.Strings2AES128(data));
    }

    public void rectInitialData() throws Exception {
        int nCount = CommonConst.errorList.size();
        for (int i = 0; i < nCount; i++) {
            switch (CommonConst.errorList.get(i)) {
                case STORE_TABLE_TABLE_REPAIR:
                    loadStoreTable(true);
                    break;
                case PERSONAL_PANNIER_REPAIR:
                    loadPersonalPannierTable(true);
                    break;
                case PRODUCT_TABLE_REPAIR:
                    loadProductTable(true);
                    break;
                case GAME_TABLE_REPAIR:
                    loadGameTable(true);
                    break;
                case SALE_OFF_TABLE_REPAIR:
                    loadSaleOffTable(true);
                    break;
                case PERSONALACCOUNT_REPAIR:
                    rectPersonalAccount();
                    break;
            }
        }
    }

    public void rectPersonalAccount() throws Exception {
        try {
            Utils.createFile(TablePath.T_PER_ACC_PATH);
            createPersonalAccount();
        } catch (Exception e) {
            Exception e2 = e;
            Log.e("f2f_TableLoad_rectPersonalAccount_error", e2.toString());
            GoogleAnalyticsUtils.setTracker("f2f_TableLoad_rectPersonalAccount_error,detail is " + e2.toString());
            CommonConst.errorLevel = CommonConst.ERROR_LEVEL.UNREPAIR;
            throw e2;
        }
    }

    public void updateDailySaledCount(long pLastLoginMillis) throws Exception {
        if (this.mStoreTable != null && this.mStoreTable.getStoreList() != null && pLastLoginMillis != 0 && System.currentTimeMillis() - pLastLoginMillis > 86400000) {
            for (int i = 0; i < this.mStoreTable.getStoreList().size(); i++) {
                this.mStoreTable.getStoreList().get(i).setSaled_count(0);
            }
            this.mStoreTable.write();
        }
    }

    public boolean checkProductTerm(String StoreId, String pProductId) {
        if (this.mStoreTable == null || this.mStoreTable.getStoreList() == null) {
            return false;
        }
        for (int i = 0; i < this.mStoreTable.getStoreList().size(); i++) {
            if (pProductId.equals(this.mStoreTable.getStoreList().get(i).getProduct_id()) && StoreId.equals(this.mStoreTable.getStoreList().get(i).getId())) {
                if ((86400000 * ((long) this.mStoreTable.getStoreList().get(i).getTerm())) + this.mStoreTable.getStoreList().get(i).getArrival_date() >= System.currentTimeMillis()) {
                    return true;
                }
            }
        }
        return false;
    }

    /* Debug info: failed to restart local var, previous not found, register: 7 */
    public boolean checkProductDailyLimit(String StoreId, String pProductId, int pSaledCount) {
        if (this.mStoreTable == null || this.mStoreTable.getStoreList() == null) {
            return false;
        }
        int i = 0;
        while (i < this.mStoreTable.getStoreList().size()) {
            if (!pProductId.equals(this.mStoreTable.getStoreList().get(i).getProduct_id()) || !StoreId.equals(this.mStoreTable.getStoreList().get(i).getId())) {
                i++;
            } else if (System.currentTimeMillis() - this.mLogin_date >= 86400000 || this.mStoreTable.getStoreList().get(i).getSaled_count() + pSaledCount <= this.mStoreTable.getStoreList().get(i).getDaily_limit()) {
                return false;
            } else {
                return true;
            }
        }
        return true;
    }

    public String ToString(byte[] bs) {
        if (bs == null) {
            throw new IllegalArgumentException();
        }
        StringBuilder builder = new StringBuilder(bs.length * 2);
        for (byte b : bs) {
            builder.append(this.ch[(b & 255) / 16]);
            builder.append(this.ch[b & 15]);
        }
        return builder.toString();
    }

    public void loadStoreTable(boolean pRectInitial) throws Exception {
        if (!Utils.isFileExist(TablePath.T_STORE_PATH) || pRectInitial) {
            try {
                this.mStoreTable.createFile();
                String[] data = loadOriginalData(ShopConst.getTableStoreData());
                if (data != null && data.length != 0) {
                    try {
                        this.mStoreTable.serlize(loadOriginalData(data));
                    } catch (Exception e) {
                        Exception e2 = e;
                        CommonConst.errorList.add(CommonConst.ERROR_LEVEL.STORE_TABLE_TABLE_REPAIR);
                        Log.e("TableLoad_loadStoreTable_createFile_Error", e2.toString());
                        GoogleAnalyticsUtils.setTracker("TableLoad_loadStoreTable_createFile_Error,detail is " + e2.toString());
                    }
                    try {
                        this.mStoreTable.write();
                    } catch (Exception e3) {
                        Exception e4 = e3;
                        CommonConst.errorLevel = CommonConst.ERROR_LEVEL.UNREPAIR;
                        Log.e("TableLoad_loadStoreTable_mStoreTable_write_Error", e4.toString());
                        GoogleAnalyticsUtils.setTracker("TableLoad_loadStoreTable_mStoreTable_write_Error,detail is " + e4.toString());
                        throw e4;
                    }
                }
            } catch (Exception e5) {
                Exception e6 = e5;
                CommonConst.errorLevel = CommonConst.ERROR_LEVEL.UNREPAIR;
                Log.e("TableLoad_loadStoreTable_createFile_Error", e6.toString());
                GoogleAnalyticsUtils.setTracker("TableLoad_loadStoreTable_createFile_Error,detail is " + e6.toString());
                throw e6;
            }
        } else {
            String[] data2 = null;
            try {
                data2 = this.mStoreTable.readFile();
            } catch (Exception e7) {
                Exception e8 = e7;
                CommonConst.errorList.add(CommonConst.ERROR_LEVEL.STORE_TABLE_TABLE_REPAIR);
                Log.e("TableLoad_loadStoreTable_mStoreTable_readFile_Error", e8.toString());
                GoogleAnalyticsUtils.setTracker("TableLoad_loadStoreTable_mStoreTable_readFile_Error,detail is " + e8.toString());
            }
            try {
                this.mStoreTable.serlize(data2);
            } catch (Exception e9) {
                Exception e10 = e9;
                CommonConst.errorList.add(CommonConst.ERROR_LEVEL.STORE_TABLE_TABLE_REPAIR);
                Log.e("TableLoad_loadStoreTable_mStoreTable_serlize_Error", e10.toString());
                GoogleAnalyticsUtils.setTracker("TableLoad_loadStoreTable_mStoreTable_serlize_Error,detail is " + e10.toString());
            }
        }
    }

    public void loadPersonalPannierTable(boolean pRectInitial) throws Exception {
        if (!Utils.isFileExist(TablePath.T_PER_PAN_PATH) || pRectInitial) {
            try {
                this.mPersonalPannierTable.createFile();
                String[] data = loadOriginalData(ShopConst.getTablePersonalPannierData());
                if (data != null && data.length != 0) {
                    try {
                        this.mPersonalPannierTable.serlize(loadOriginalData(data));
                    } catch (Exception e) {
                        Exception e2 = e;
                        CommonConst.errorList.add(CommonConst.ERROR_LEVEL.PERSONAL_PANNIER_REPAIR);
                        Log.e("TableLoad_loadPersonalPannierTable_mPersonalPannierTable_serlize_Error", e2.toString());
                        GoogleAnalyticsUtils.setTracker("TableLoad_loadPersonalPannierTable_mPersonalPannierTable_serlize_Error,detail is " + e2.toString());
                    }
                    ArrayList<PersonalPannier> pplist = this.mPersonalPannierTable.getPersonalPannierList();
                    for (int j = 0; j < pplist.size(); j++) {
                        pplist.get(j).setUser_id(this.accountId);
                    }
                    try {
                        this.mPersonalPannierTable.write();
                    } catch (Exception e3) {
                        Exception e4 = e3;
                        CommonConst.errorLevel = CommonConst.ERROR_LEVEL.UNREPAIR;
                        Log.e("TableLoad_loadPersonalPannierTable_mPersonalPannierTable_write_Error", e4.toString());
                        GoogleAnalyticsUtils.setTracker("TableLoad_loadPersonalPannierTable_mPersonalPannierTable_write_Error,detail is " + e4.toString());
                        throw e4;
                    }
                }
            } catch (Exception e5) {
                Exception e6 = e5;
                CommonConst.errorLevel = CommonConst.ERROR_LEVEL.UNREPAIR;
                Log.e("TableLoad_loadPersonalPannierTable_mPersonalPannierTable_createFile_Error", e6.toString());
                GoogleAnalyticsUtils.setTracker("TableLoad_loadPersonalPannierTable_mPersonalPannierTable_createFile_Error,detail is " + e6.toString());
                throw e6;
            }
        } else {
            String[] data2 = null;
            try {
                data2 = this.mPersonalPannierTable.readFile();
            } catch (Exception e7) {
                Exception e8 = e7;
                CommonConst.errorList.add(CommonConst.ERROR_LEVEL.PERSONAL_PANNIER_REPAIR);
                Log.e("TableLoad_loadPersonalPannierTable_mPersonalPannierTable_readFile_Error", e8.toString());
                GoogleAnalyticsUtils.setTracker("TableLoad_loadPersonalPannierTable_mPersonalPannierTable_readFile_Error,detail is " + e8.toString());
            }
            try {
                this.mPersonalPannierTable.serlize(data2);
            } catch (Exception e9) {
                Exception e10 = e9;
                CommonConst.errorList.add(CommonConst.ERROR_LEVEL.PERSONAL_PANNIER_REPAIR);
                Log.e("TableLoad_loadPersonalPannierTable_mPersonalPannierTable_serlize_Error", e10.toString());
                GoogleAnalyticsUtils.setTracker("TableLoad_loadPersonalPannierTable_mPersonalPannierTable_serlize_Error,detail is " + e10.toString());
            }
        }
    }

    public void loadProductTable(boolean pRectInitial) throws Exception {
        if (!Utils.isFileExist(TablePath.T_PRODUCT_PATH) || pRectInitial) {
            try {
                this.mProductTable.createFile();
                String[] data = loadOriginalData(ShopConst.getTableProductData(this.mContext));
                if (data != null && data.length != 0) {
                    try {
                        this.mProductTable.serlize(loadOriginalData(data));
                    } catch (Exception e) {
                        Exception e2 = e;
                        CommonConst.errorList.add(CommonConst.ERROR_LEVEL.PRODUCT_TABLE_REPAIR);
                        Log.e("TableLoad_loadProductTable_mProductTable_serlize_Error", e2.toString());
                        GoogleAnalyticsUtils.setTracker("TableLoad_loadProductTable_mProductTable_serlize_Error,detail is " + e2.toString());
                    }
                    try {
                        this.mProductTable.write();
                    } catch (Exception e3) {
                        Exception e4 = e3;
                        CommonConst.errorLevel = CommonConst.ERROR_LEVEL.UNREPAIR;
                        Log.e("TableLoad_loadProductTable_mProductTable_write_Error", e4.toString());
                        GoogleAnalyticsUtils.setTracker("TableLoad_loadProductTable_mProductTable_write_Error,detail is " + e4.toString());
                        throw e4;
                    }
                }
            } catch (Exception e5) {
                Exception e6 = e5;
                CommonConst.errorLevel = CommonConst.ERROR_LEVEL.UNREPAIR;
                throw e6;
            }
        } else {
            String[] data2 = null;
            try {
                data2 = this.mProductTable.readFile();
            } catch (Exception e7) {
                Exception e8 = e7;
                CommonConst.errorList.add(CommonConst.ERROR_LEVEL.PRODUCT_TABLE_REPAIR);
                Log.e("TableLoad_loadProductTable_mProductTable_readFile_Error", e8.toString());
                GoogleAnalyticsUtils.setTracker("TableLoad_loadProductTable_mProductTable_readFile_Error,detail is " + e8.toString());
            }
            try {
                this.mProductTable.serlize(data2);
            } catch (Exception e9) {
                Exception e10 = e9;
                CommonConst.errorList.add(CommonConst.ERROR_LEVEL.PRODUCT_TABLE_REPAIR);
                Log.e("TableLoad_loadProductTable_mProductTable_serlize_Error", e10.toString());
                GoogleAnalyticsUtils.setTracker("TableLoad_loadProductTable_mProductTable_serlize_Error,detail is " + e10.toString());
            }
        }
    }

    public void loadGameTable(boolean pRectInitial) throws Exception {
        if (!Utils.isFileExist(TablePath.T_GAME_PATH) || pRectInitial) {
            try {
                this.mGameTable.createFile();
                String[] data = loadOriginalData(ShopConst.getTableGameData());
                if (data != null && data.length != 0) {
                    try {
                        this.mGameTable.serlize(loadOriginalData(data));
                    } catch (Exception e) {
                        Exception e2 = e;
                        CommonConst.errorList.add(CommonConst.ERROR_LEVEL.GAME_TABLE_REPAIR);
                        Log.e("TableLoad_loadGameTable_mGameTable_serlize_Error", e2.toString());
                        GoogleAnalyticsUtils.setTracker("TableLoad_loadGameTable_mGameTable_serlize_Error,detail is " + e2.toString());
                    }
                    try {
                        this.mGameTable.write();
                    } catch (Exception e3) {
                        Exception e4 = e3;
                        CommonConst.errorLevel = CommonConst.ERROR_LEVEL.UNREPAIR;
                        Log.e("TableLoad_loadGameTable_mGameTable_write_Error", e4.toString());
                        GoogleAnalyticsUtils.setTracker("TableLoad_loadGameTable_mGameTable_write_Error,detail is " + e4.toString());
                        throw e4;
                    }
                }
            } catch (Exception e5) {
                Exception e6 = e5;
                CommonConst.errorLevel = CommonConst.ERROR_LEVEL.UNREPAIR;
                Log.e("TableLoad_loadGameTable_mGameTable_createFile_Error", e6.toString());
                GoogleAnalyticsUtils.setTracker("TableLoad_loadGameTable_mGameTable_createFile_Error,detail is " + e6.toString());
                throw e6;
            }
        } else {
            String[] data2 = null;
            try {
                data2 = this.mGameTable.readFile();
            } catch (Exception e7) {
                Exception e8 = e7;
                CommonConst.errorList.add(CommonConst.ERROR_LEVEL.GAME_TABLE_REPAIR);
                Log.e("TableLoad_loadGameTable_mGameTable_readFile_Error", e8.toString());
                GoogleAnalyticsUtils.setTracker("TableLoad_loadGameTable_mGameTable_readFile_Error,detail is " + e8.toString());
            }
            try {
                this.mGameTable.serlize(data2);
            } catch (Exception e9) {
                Exception e10 = e9;
                CommonConst.errorList.add(CommonConst.ERROR_LEVEL.GAME_TABLE_REPAIR);
                Log.e("TableLoad_loadGameTable_mGameTable_serlize_Error", e10.toString());
                GoogleAnalyticsUtils.setTracker("TableLoad_loadGameTable_mGameTable_serlize_Error,detail is " + e10.toString());
            }
        }
    }

    public void loadSaleOffTable(boolean pRectInitial) throws Exception {
        if (!Utils.isFileExist(TablePath.T_SALEOFF_PATH) || pRectInitial) {
            try {
                this.mSaleOffTable.createFile();
                String[] data = loadOriginalData(ShopConst.getTableSaleOffData());
                if (data != null && data.length != 0) {
                    try {
                        this.mSaleOffTable.serlize(loadOriginalData(data));
                    } catch (Exception e) {
                        Exception e2 = e;
                        Log.e("f2f_TableLoad_loadSaleOffTable_serlize_error", e2.toString());
                        GoogleAnalyticsUtils.setTracker("f2f_TableLoad_loadSaleOffTable_serlize_error,detail is " + e2.toString());
                        CommonConst.errorList.add(CommonConst.ERROR_LEVEL.SALE_OFF_TABLE_REPAIR);
                    }
                    try {
                        this.mSaleOffTable.write();
                    } catch (Exception e3) {
                        Exception e4 = e3;
                        Log.e("f2f_TableLoad_loadSaleOffTable_write_error", e4.toString());
                        GoogleAnalyticsUtils.setTracker("f2f_TableLoad_loadSaleOffTable_write_error,detail is " + e4.toString());
                        CommonConst.errorLevel = CommonConst.ERROR_LEVEL.UNREPAIR;
                        throw e4;
                    }
                }
            } catch (Exception e5) {
                Exception e6 = e5;
                CommonConst.errorLevel = CommonConst.ERROR_LEVEL.UNREPAIR;
                Log.e("f2f_TableLoad_loadSaleOffTable_createFile_error", e6.toString());
                GoogleAnalyticsUtils.setTracker("f2f_TableLoad_loadSaleOffTable_createFile_error,detail is " + e6.toString());
                throw e6;
            }
        } else {
            String[] data2 = null;
            try {
                data2 = this.mSaleOffTable.readFile();
            } catch (Exception e7) {
                Exception e8 = e7;
                Log.e("f2f_TableLoad_loadSaleOffTable_readFile_error", e8.toString());
                GoogleAnalyticsUtils.setTracker("f2f_TableLoad_loadSaleOffTable_readFile_error,detail is " + e8.toString());
                CommonConst.errorList.add(CommonConst.ERROR_LEVEL.SALE_OFF_TABLE_REPAIR);
            }
            try {
                this.mSaleOffTable.serlize(data2);
            } catch (Exception e9) {
                Exception e10 = e9;
                Log.e("f2f_TableLoad_loadSaleOffTable_readFile_error", e10.toString());
                GoogleAnalyticsUtils.setTracker("f2f_TableLoad_loadSaleOffTable_readFile_error,detail is " + e10.toString());
                CommonConst.errorList.add(CommonConst.ERROR_LEVEL.SALE_OFF_TABLE_REPAIR);
            }
        }
    }

    public int getPropEnable(String pPersonalPannierId, String pProductId) {
        String userId = this.mPersonalAccountTable.getPersonalAccountList().get(0).getId();
        ArrayList<PersonalPannier> pplist = this.mPersonalPannierTable.getPersonalPannierList();
        for (int i = 0; i < pplist.size(); i++) {
            PersonalPannier pp = pplist.get(i);
            if (pp != null && pp.getId().equals(pPersonalPannierId) && pp.getUser_id().equals(userId) && pp.getProduct_id().equals(pProductId)) {
                return pp.getProduct_count();
            }
        }
        return 0;
    }

    public boolean addProp(String pPersonalPannierId, String pProductId) {
        String userId = this.mPersonalAccountTable.getPersonalAccountList().get(0).getId();
        ArrayList<PersonalPannier> pplist = this.mPersonalPannierTable.getPersonalPannierList();
        int i = 0;
        while (i < pplist.size()) {
            PersonalPannier pp = pplist.get(i);
            if (pp == null || !pp.getId().equals(pPersonalPannierId) || !pp.getUser_id().equals(userId) || !pp.getProduct_id().equals(pProductId)) {
                i++;
            } else {
                pp.setProduct_count(pp.getProduct_count() + 1);
                try {
                    this.mPersonalPannierTable.write();
                    return true;
                } catch (Exception e) {
                    pp.setProduct_count(pp.getProduct_count() - 1);
                    Log.e("TableLoad_Error", e.getMessage());
                    return false;
                }
            }
        }
        return false;
    }

    public boolean plusProp(String pPersonalPannierId, String pProductId) {
        String userId = this.mPersonalAccountTable.getPersonalAccountList().get(0).getId();
        ArrayList<PersonalPannier> pplist = this.mPersonalPannierTable.getPersonalPannierList();
        int i = 0;
        while (i < pplist.size()) {
            PersonalPannier pp = pplist.get(i);
            if (pp == null || !pp.getId().equals(pPersonalPannierId) || !pp.getUser_id().equals(userId) || !pp.getProduct_id().equals(pProductId)) {
                i++;
            } else {
                pp.setProduct_count(pp.getProduct_count() - 1);
                try {
                    this.mPersonalPannierTable.write();
                    return true;
                } catch (Exception e) {
                    pp.setProduct_count(pp.getProduct_count() + 1);
                    Log.e("TableLoad_Error", e.getMessage());
                    return false;
                }
            }
        }
        return false;
    }

    public int getGameInstallName() {
        List<PackageInfo> packs = this.mContext.getPackageManager().getInstalledPackages(0);
        int retValue = 0;
        for (int i = 0; i < packs.size(); i++) {
            if (packs.get(i).packageName.toLowerCase().startsWith(CommonConst.F2F_GAMES_PACKAGE_NAME)) {
                retValue++;
            }
        }
        if (retValue > 5) {
            return 5;
        }
        return retValue - 1;
    }
}
