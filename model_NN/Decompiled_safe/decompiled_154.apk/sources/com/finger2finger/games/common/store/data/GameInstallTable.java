package com.finger2finger.games.common.store.data;

import com.finger2finger.games.common.store.io.Utils;
import java.util.ArrayList;

public class GameInstallTable {
    private ArrayList<GameInstallInfo> installGameList = new ArrayList<>();

    public ArrayList<GameInstallInfo> getPersonalAccountList() {
        return this.installGameList;
    }

    public void setPersonalAccountList(ArrayList<GameInstallInfo> pInstallGameList) {
        this.installGameList = pInstallGameList;
    }

    public void createFile() throws Exception {
        Utils.createFile(TablePath.T_GAME_GAME_INSTALL_PATH);
    }

    public String[] readFile() throws Exception {
        return Utils.readFile(TablePath.T_GAME_GAME_INSTALL_PATH);
    }

    public void serlize(String[] data) throws Exception {
        Exception e;
        GameInstallInfo installInfo = null;
        if (data != null && data.length > 0) {
            int i = 0;
            while (i < data.length) {
                try {
                    installInfo = new GameInstallInfo(data[i].split(TablePath.SEPARATOR_ITEM));
                    try {
                        this.installGameList.add(installInfo);
                        i++;
                    } catch (Exception e2) {
                        e = e2;
                        throw e;
                    }
                } catch (Exception e3) {
                    e = e3;
                    throw e;
                }
            }
        }
    }

    public void write() throws Exception {
        String[] data = new String[this.installGameList.size()];
        for (int i = 0; i < this.installGameList.size(); i++) {
            data[i] = this.installGameList.get(i).toString();
        }
        if (data != null && data.length > 0) {
            Utils.writeFile(TablePath.T_GAME_GAME_INSTALL_PATH, data);
        }
    }
}
