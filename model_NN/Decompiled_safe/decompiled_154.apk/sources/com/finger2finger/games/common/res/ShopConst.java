package com.finger2finger.games.common.res;

import android.content.Context;
import com.f2fgames.games.monkeybreak.lite.R;
import com.finger2finger.games.common.store.data.TablePath;

public class ShopConst {
    private static final String[][] DATA_GAME = new String[0][];
    private static final String[][] DATA_PERPAN = {new String[]{PERSONALPANNIER_ID, "user0001", Product_ID.ADD_TIME, "1"}, new String[]{PERSONALPANNIER_ID, "user0001", Product_ID.ADD_LIFE, "1"}, new String[]{PERSONALPANNIER_ID, "user0001", Product_ID.DAILY_REMOVE_ADVIEW, "0"}, new String[]{PERSONALPANNIER_ID, "user0001", Product_ID.FOREVER_REMOVE_ADVIEW, "0"}};
    public static final String[][] DATA_PRODUCT = {new String[]{Product_ID.ADD_TIME, "CommonResource/store/products/product0001.png", "2", "1"}, new String[]{Product_ID.ADD_LIFE, "CommonResource/store/products/product0002.png", "0", "2"}, new String[]{Product_ID.DAILY_REMOVE_ADVIEW, "CommonResource/store/products/product0003.png", "0", "1"}, new String[]{Product_ID.FOREVER_REMOVE_ADVIEW, "CommonResource/store/products/product0004.png", "0", "200"}};
    private static final String[][] DATA_SALE_OFF = new String[0][];
    private static final String[][] DATA_STORE = {new String[]{"store0001", Product_ID.ADD_TIME, "999", "99999", "1302499022296", "10", "0"}, new String[]{"store0001", Product_ID.ADD_LIFE, "999", "99999", "1302499022296", "10", "0"}, new String[]{"store0001", Product_ID.DAILY_REMOVE_ADVIEW, "1", "99999", "1302499022296", "10", "0"}, new String[]{"store0001", Product_ID.FOREVER_REMOVE_ADVIEW, "1", "1", "1302499022296", "10", "0"}};
    public static final int INITIALIZE_BONUS_FINGER_MONEY = 5;
    public static final int INITIALIZE_COMSUME_FINGER_MONEY = 0;
    public static final int INITIALIZE_DELAYER_COUNT = 1;
    public static final int INITIALIZE_EXTRALIFE_COUNT = 1;
    public static final int INITIALIZE_FINGER_MONEY_COUNT = 0;
    public static final int INITIALIZE_GOLDEN_COUNT = 10;
    public static final int INITIALIZE_PERPAN_ID = 20110100;
    public static final int INITIALIZE_PRODUCT_COUNT = 10;
    public static final int INITIALIZE_PRODUCT_ID = 20110200;
    public static final String PERSONALPANNIER_ID = "personalpannier0001";
    private static final int[] PRODUCT_COMMENT_STRING_ID = {R.string.product0001_comment, R.string.product0002_comment, R.string.product0003_comment, R.string.product0004_comment};
    private static final int[] PRODUCT_NAME_STRING_ID = {R.string.product0001_name, R.string.product0002_name, R.string.product0003_name, R.string.product0004_name};
    public static final int PRODUCT_PIC_INDEX = 1;
    public static final int PROP_TYPE_COUNT = 3;
    public static final String[][] PropInfo = {new String[]{PERSONALPANNIER_ID, Product_ID.ADD_TIME}, new String[]{PERSONALPANNIER_ID, Product_ID.ADD_LIFE}, new String[]{PERSONALPANNIER_ID, Product_ID.DAILY_REMOVE_ADVIEW}, new String[]{PERSONALPANNIER_ID, Product_ID.FOREVER_REMOVE_ADVIEW}};
    public static final long TIME_DAILY_MILLI = 86400000;
    public static final boolean isAES128Enable = true;

    public static class Index {
        public static final int ADD_LIFE = 1;
        public static final int ADD_TIME = 0;
        public static final int DAILY_REMOVE_ADVIEW = 2;
        public static final int FOREVER_REMOVE_ADVIEW = 3;
    }

    public static class Product_ID {
        public static final String ADD_LIFE = "product0002";
        public static final String ADD_TIME = "product0001";
        public static final String DAILY_REMOVE_ADVIEW = "product0003";
        public static final String FOREVER_REMOVE_ADVIEW = "product0004";
    }

    public static String[] getTableProductData(Context mContext) {
        if (DATA_PRODUCT.length <= 0) {
            return null;
        }
        String[] data = new String[DATA_PRODUCT.length];
        new StringBuffer();
        for (int i = 0; i < DATA_PRODUCT.length; i++) {
            StringBuffer sb = new StringBuffer();
            sb.append(DATA_PRODUCT[i][0]).append(TablePath.SEPARATOR_ITEM).append(mContext.getString(PRODUCT_NAME_STRING_ID[i])).append(TablePath.SEPARATOR_ITEM).append(DATA_PRODUCT[i][1]).append(TablePath.SEPARATOR_ITEM).append(DATA_PRODUCT[i][2]).append(TablePath.SEPARATOR_ITEM).append(DATA_PRODUCT[i][3]).append(TablePath.SEPARATOR_ITEM).append(mContext.getString(PRODUCT_COMMENT_STRING_ID[i]));
            data[i] = sb.toString();
        }
        return data;
    }

    public static String[] getTableGameData() {
        if (DATA_GAME.length <= 0) {
            return null;
        }
        String[] data = new String[DATA_GAME.length];
        new StringBuffer();
        for (int i = 0; i < DATA_GAME.length; i++) {
            StringBuffer sb = new StringBuffer();
            sb.append(DATA_GAME[i][0]).append(TablePath.SEPARATOR_ITEM).append(DATA_GAME[i][1]).append(TablePath.SEPARATOR_ITEM).append(DATA_GAME[i][2]).append(TablePath.SEPARATOR_ITEM).append(DATA_GAME[i][3]).append(TablePath.SEPARATOR_ITEM).append(DATA_GAME[i][4]).append(TablePath.SEPARATOR_ITEM).append(DATA_GAME[i][5]).append(TablePath.SEPARATOR_ITEM).append(DATA_GAME[i][6]).append(TablePath.SEPARATOR_ITEM).append(DATA_GAME[i][7]).append(TablePath.SEPARATOR_ITEM).append(DATA_GAME[i][8]);
            data[i] = sb.toString();
        }
        return data;
    }

    public static String[] getTableSaleOffData() {
        if (DATA_SALE_OFF.length <= 0) {
            return null;
        }
        String[] data = new String[DATA_SALE_OFF.length];
        new StringBuffer();
        for (int i = 0; i < DATA_SALE_OFF.length; i++) {
            StringBuffer sb = new StringBuffer();
            sb.append(DATA_SALE_OFF[i][0]).append(TablePath.SEPARATOR_ITEM).append(DATA_SALE_OFF[i][1]).append(TablePath.SEPARATOR_ITEM).append(DATA_SALE_OFF[i][2]).append(TablePath.SEPARATOR_ITEM).append(DATA_SALE_OFF[i][3]).append(TablePath.SEPARATOR_ITEM).append(DATA_SALE_OFF[i][4]).append(TablePath.SEPARATOR_ITEM).append(DATA_SALE_OFF[i][5]).append(TablePath.SEPARATOR_ITEM).append(DATA_SALE_OFF[i][6]);
            data[i] = sb.toString();
        }
        return data;
    }

    public static String[] getTableStoreData() {
        if (DATA_STORE.length <= 0) {
            return null;
        }
        String[] data = new String[DATA_STORE.length];
        new StringBuffer();
        for (int i = 0; i < DATA_STORE.length; i++) {
            StringBuffer sb = new StringBuffer();
            sb.append(DATA_STORE[i][0]).append(TablePath.SEPARATOR_ITEM).append(DATA_STORE[i][1]).append(TablePath.SEPARATOR_ITEM).append(DATA_STORE[i][2]).append(TablePath.SEPARATOR_ITEM).append(DATA_STORE[i][3]).append(TablePath.SEPARATOR_ITEM).append(DATA_STORE[i][4]).append(TablePath.SEPARATOR_ITEM).append(DATA_STORE[i][5]).append(TablePath.SEPARATOR_ITEM).append(DATA_STORE[i][6]);
            data[i] = sb.toString();
        }
        return data;
    }

    public static String[] getTablePersonalPannierData() {
        if (DATA_PERPAN.length <= 0) {
            return null;
        }
        String[] data = new String[DATA_PERPAN.length];
        new StringBuffer();
        for (int i = 0; i < DATA_PERPAN.length; i++) {
            StringBuffer sb = new StringBuffer();
            sb.append(DATA_PERPAN[i][0]).append(TablePath.SEPARATOR_ITEM).append(DATA_PERPAN[i][1]).append(TablePath.SEPARATOR_ITEM).append(DATA_PERPAN[i][2]).append(TablePath.SEPARATOR_ITEM).append(DATA_PERPAN[i][3]).append(TablePath.SEPARATOR_ITEM);
            data[i] = sb.toString();
        }
        return data;
    }
}
