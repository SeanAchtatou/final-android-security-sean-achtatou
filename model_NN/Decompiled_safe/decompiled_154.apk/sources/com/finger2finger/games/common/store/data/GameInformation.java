package com.finger2finger.games.common.store.data;

import com.finger2finger.games.common.CommonConst;
import java.text.SimpleDateFormat;
import java.util.Date;

public class GameInformation {
    public static final int LIST_ITEM_COUNT = 12;
    private int _InviteFriendNum = 0;
    private long _dialyClearAds = -2;
    private int _enableInviteDilaog = 0;
    private int _enableInviteFriend = 0;
    private long _foreverClearAds = -2;
    private int _gameExchangeRatio = 0;
    private int _gameGold = 0;
    private int _gameHelpShow = 0;
    private int _gameMusicOn = 0;
    private String _gameVersion = "";
    private String _loadGameDay = "0";
    private int _maxSore = 0;

    public GameInformation() {
    }

    public GameInformation(int gameMusicOn, String gameVersion, int gameGold, int gameExchangeRatio, int gameHelpShow, int pMaxSore, long pDialyClearAds, long pForeverClearAds, int pInviteFriendNum, String ploadGameDay, int pEnableInviteDilaog, int pEnableInviteFriend) {
        this._gameMusicOn = gameMusicOn;
        this._gameVersion = gameVersion;
        this._gameGold = gameGold;
        this._gameExchangeRatio = gameExchangeRatio;
        this._gameHelpShow = gameHelpShow;
        this._maxSore = pMaxSore;
        this._dialyClearAds = pDialyClearAds;
        this._foreverClearAds = pForeverClearAds;
        if (ploadGameDay == null || ploadGameDay.equals("")) {
            this._loadGameDay = String.valueOf(new SimpleDateFormat("yyyyMMdd").format(new Date(System.currentTimeMillis())));
            this._InviteFriendNum = 0;
            CommonConst.GAME_LOAD_DAY = this._loadGameDay;
            return;
        }
        this._loadGameDay = ploadGameDay;
        this._InviteFriendNum = pInviteFriendNum;
        this._enableInviteDilaog = pEnableInviteDilaog;
        this._enableInviteFriend = pEnableInviteFriend;
    }

    public String get_gameVersion() {
        return this._gameVersion;
    }

    public void set_gameVersion(String version) {
        this._gameVersion = version;
    }

    public int get_gameGold() {
        return this._gameGold;
    }

    public void set_gameGold(int gold) {
        this._gameGold = gold;
    }

    public int get_gameExchangeRatio() {
        return this._gameExchangeRatio;
    }

    public void set_gameExchangeRatio(int exchangeRatio) {
        this._gameExchangeRatio = exchangeRatio;
    }

    public GameInformation(String[] data) throws Exception {
        int i;
        int i2;
        if (data == null || data.length != 12) {
            this._gameMusicOn = CommonConst.GAME_MUSIC_ON ? 0 : 1;
            this._gameVersion = CommonConst.GAME_VERSION;
            this._gameGold = CommonConst.GAME_GOLD;
            this._gameExchangeRatio = CommonConst.GAME_EXCHANGE;
            if (CommonConst.GAME_HELP_SHOW) {
                i = 0;
            } else {
                i = 1;
            }
            this._gameHelpShow = i;
            this._maxSore = CommonConst.GAME_MAX_SCORE;
            this._dialyClearAds = CommonConst.DIALY_CLEAR_ADS_DEFAUT;
            this._foreverClearAds = CommonConst.FOREVER_CLEAR_ADS_DEFAUT;
            this._InviteFriendNum = CommonConst.INVITE_FRIEND_NUM;
            this._loadGameDay = CommonConst.GAME_LOAD_DAY;
            if (this._loadGameDay == null || this._loadGameDay.equals("")) {
                this._loadGameDay = String.valueOf(new SimpleDateFormat("yyyyMMdd").format(new Date(System.currentTimeMillis())));
                CommonConst.GAME_LOAD_DAY = this._loadGameDay;
            }
            this._enableInviteDilaog = CommonConst.ENBALE_SHOW_INVITE_DIALOG ? 0 : 1;
            if (CommonConst.ENABLE_INVITE_FRIEND) {
                i2 = 0;
            } else {
                i2 = 1;
            }
            this._enableInviteFriend = i2;
            return;
        }
        this._gameMusicOn = Integer.parseInt(data[0]);
        this._gameVersion = data[1];
        this._gameGold = Integer.parseInt(data[2]);
        this._gameExchangeRatio = Integer.parseInt(data[3]);
        this._gameHelpShow = Integer.parseInt(data[4]);
        this._maxSore = Integer.parseInt(data[5]);
        this._dialyClearAds = Long.parseLong(data[6]);
        this._foreverClearAds = Long.parseLong(data[7]);
        this._InviteFriendNum = Integer.parseInt(data[8]);
        this._loadGameDay = data[9];
        if (this._loadGameDay == null || this._loadGameDay.equals("")) {
            this._loadGameDay = String.valueOf(new SimpleDateFormat("yyyyMMdd").format(new Date(System.currentTimeMillis())));
            CommonConst.GAME_LOAD_DAY = this._loadGameDay;
        }
        this._enableInviteDilaog = Integer.parseInt(data[10]);
        this._enableInviteFriend = Integer.parseInt(data[11]);
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(this._gameMusicOn).append(TablePath.SEPARATOR_ITEM).append(this._gameVersion).append(TablePath.SEPARATOR_ITEM).append(this._gameGold).append(TablePath.SEPARATOR_ITEM).append(this._gameExchangeRatio).append(TablePath.SEPARATOR_ITEM).append(this._gameHelpShow).append(TablePath.SEPARATOR_ITEM).append(this._maxSore).append(TablePath.SEPARATOR_ITEM).append(this._dialyClearAds).append(TablePath.SEPARATOR_ITEM).append(this._foreverClearAds).append(TablePath.SEPARATOR_ITEM).append(this._InviteFriendNum).append(TablePath.SEPARATOR_ITEM).append(this._loadGameDay).append(TablePath.SEPARATOR_ITEM).append(this._enableInviteDilaog).append(TablePath.SEPARATOR_ITEM).append(this._enableInviteFriend);
        return sb.toString();
    }

    public int get_gameMusicOn() {
        return this._gameMusicOn;
    }

    public void set_gameMusicOn(int musicOn) {
        this._gameMusicOn = musicOn;
    }

    public int get_gameHelpShow() {
        return this._gameHelpShow;
    }

    public void set_gameHelpShow(int helpShow) {
        this._gameHelpShow = helpShow;
    }

    public int get_maxSore() {
        return this._maxSore;
    }

    public void set_maxSore(int sore) {
        this._maxSore = sore;
    }

    public long get_dialyClearAds() {
        return this._dialyClearAds;
    }

    public long get_foreverClearAds() {
        return this._foreverClearAds;
    }

    public void set_dialyClearAds(long dialyClearAds) {
        this._dialyClearAds = dialyClearAds;
    }

    public void set_foreverClearAds(long foreverClearAds) {
        this._foreverClearAds = foreverClearAds;
    }

    public int get_InviteFriendNum() {
        return this._InviteFriendNum;
    }

    public void set_InviteFriendNum(int inviteFriendNum) {
        this._InviteFriendNum = inviteFriendNum;
    }

    public String get_loadGameDay() {
        return this._loadGameDay;
    }

    public void set_loadGameDay(String loadGameDay) {
        this._loadGameDay = loadGameDay;
    }

    public int get_enableInviteDilaog() {
        return this._enableInviteDilaog;
    }

    public void set_enableInviteDilaog(int enableInviteDilaog) {
        this._enableInviteDilaog = enableInviteDilaog;
    }

    public int get_enableInviteFriend() {
        return this._enableInviteFriend;
    }

    public void set_enableInviteFriend(int enableInviteFriend) {
        this._enableInviteFriend = enableInviteFriend;
    }

    public int getLIST_ITEM_COUNT() {
        return 12;
    }
}
