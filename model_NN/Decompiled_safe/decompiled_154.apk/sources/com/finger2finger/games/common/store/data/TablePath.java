package com.finger2finger.games.common.store.data;

import android.app.Activity;
import com.finger2finger.games.res.Const;
import org.anddev.andengine.entity.layer.tiled.tmx.util.constants.TMXConstants;

public class TablePath {
    public static String GAME_PATH = (ROOT_PATH + SEPARATOR_PATH + Const.GAME_NAME);
    public static String HONOR_GAME_PATH = (GAME_PATH + SEPARATOR_PATH + "Thonor.txt");
    public static String HONOR_RECORD_PATH = (GAME_PATH + SEPARATOR_PATH + "ThonorRecord.txt");
    public static String INFO_GAME_PATH = (GAME_PATH + SEPARATOR_PATH + "InfoGame.txt");
    public static String PERSONAL_PATH = (ROOT_PATH + SEPARATOR_PATH + "personal");
    public static String PIC_FILE = "f2f_image_%d.png";
    public static String PIC_PATH = (GAME_PATH + SEPARATOR_PATH + "image" + SEPARATOR_PATH);
    public static String ROOT_PATH = (SEPARATOR_PATH + "Android" + SEPARATOR_PATH + TMXConstants.TAG_DATA + SEPARATOR_PATH + "finger2finger.com");
    public static String SEPARATOR_ITEM = ";";
    public static String SEPARATOR_ITEM_0 = ",";
    public static String SEPARATOR_PATH = "/";
    public static String T_GAME_GAME_INSTALL_PATH = (PERSONAL_PATH + SEPARATOR_PATH + "TInstallInfo.txt");
    public static String T_GAME_PATH = (GAME_PATH + SEPARATOR_PATH + "TGame.txt");
    public static String T_PER_ACC_PATH = (PERSONAL_PATH + SEPARATOR_PATH + "TPersonalAccount.txt");
    public static String T_PER_PAN_PATH = (PERSONAL_PATH + SEPARATOR_PATH + "TPersonalPannier.txt");
    public static String T_PRODUCT_PATH = (GAME_PATH + SEPARATOR_PATH + "TProduct.txt");
    public static String T_PROMOTION_TEMP_PATH = (ROOT_PATH + SEPARATOR_PATH + "promotion_tmp" + SEPARATOR_PATH);
    public static String T_PROMOTION_USE_PATH = (ROOT_PATH + SEPARATOR_PATH + "promotion_use" + SEPARATOR_PATH);
    public static String T_SALEOFF_PATH = (GAME_PATH + SEPARATOR_PATH + "TSaleOff.txt");
    public static String T_STORE_PATH = (GAME_PATH + SEPARATOR_PATH + "TStore.txt");

    public static void initialize(Activity pContext) {
    }
}
