package com.finger2finger.games.common.store.data;

import android.content.Context;
import android.content.SharedPreferences;
import com.finger2finger.games.common.CommonPortConst;
import com.finger2finger.games.common.store.io.OriginalDataProcess;
import com.finger2finger.games.common.store.io.Utils;
import com.finger2finger.games.res.Const;
import java.util.ArrayList;

public class StoreGameInfo {
    private GameInformation mGameInfo = new GameInformation();
    private ArrayList<LevelEntity> mLevelEntity = new ArrayList<>();

    public void load(Context mContext) throws Exception {
        Exception e;
        String[] item;
        Exception e2;
        String[] data = null;
        LevelEntity levelInfo = null;
        try {
            if (!CommonPortConst.isNoSDCard) {
                data = Utils.readFile(TablePath.INFO_GAME_PATH);
            } else {
                String stream = checkPreferences(mContext);
                if (stream != null && !stream.equals("")) {
                    data = OriginalDataProcess.AES1282Strings(stream);
                }
            }
            if (data != null && data.length > 0) {
                int i = 0;
                while (true) {
                    try {
                        LevelEntity levelInfo2 = levelInfo;
                        if (i < data.length) {
                            if (i == 0) {
                                item = Utils.getSplitData(data[i], 12);
                            } else {
                                item = Utils.getSplitData(data[i], 6);
                            }
                            if (i == 0) {
                                try {
                                    this.mGameInfo = new GameInformation(item);
                                    levelInfo = levelInfo2;
                                } catch (Exception e3) {
                                    e2 = e3;
                                    throw e2;
                                }
                            } else {
                                levelInfo = new LevelEntity(item);
                                try {
                                    this.mLevelEntity.add(levelInfo);
                                } catch (Exception e4) {
                                    e2 = e4;
                                }
                            }
                            i++;
                        } else {
                            return;
                        }
                    } catch (Exception e5) {
                        e = e5;
                    }
                }
            } else {
                return;
            }
        } catch (Exception e6) {
            e = e6;
        }
        throw e;
    }

    public String checkPreferences(Context mContext) {
        return mContext.getSharedPreferences(Const.GAME_NAME, 1).getString(Const.GAMEINFO_PREFERENCES, "");
    }

    public String checkVersionPreferences(Context mContext) {
        return mContext.getSharedPreferences(Const.GAME_NAME, 1).getString(Const.VERSION_PREFERENCES, "");
    }

    public void write(Context mContext) throws Exception {
        String[] data = new String[(this.mLevelEntity.size() + 1)];
        data[0] = this.mGameInfo.toString();
        for (int i = 0; i < this.mLevelEntity.size(); i++) {
            data[i + 1] = this.mLevelEntity.get(i).toString();
        }
        if (data != null && data.length > 0) {
            if (!CommonPortConst.isNoSDCard) {
                Utils.writeFile(TablePath.INFO_GAME_PATH, data);
                return;
            }
            SharedPreferences.Editor editor = mContext.getSharedPreferences(Const.GAME_NAME, 1).edit();
            editor.putString(Const.GAMEINFO_PREFERENCES, OriginalDataProcess.Strings2AES128(data));
            editor.putString(Const.VERSION_PREFERENCES, Const.Version);
            editor.commit();
        }
    }

    public GameInformation getMGameInfo() {
        return this.mGameInfo;
    }

    public void setMGameInfo(GameInformation gameInfo) {
        this.mGameInfo = gameInfo;
    }

    public ArrayList<LevelEntity> getMLevelEntity() {
        return this.mLevelEntity;
    }

    public void setMLevelEntity(ArrayList<LevelEntity> levelEntity) {
        this.mLevelEntity = levelEntity;
    }
}
