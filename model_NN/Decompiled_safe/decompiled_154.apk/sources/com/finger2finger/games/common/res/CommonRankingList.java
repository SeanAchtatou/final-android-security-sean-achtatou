package com.finger2finger.games.common.res;

import android.content.Context;
import com.finger2finger.games.common.RankingListInfo;

public class CommonRankingList {
    private static RankingListInfo mRankingListInfo;

    public static void InitializeRankingList(Context mContext) {
        mRankingListInfo = new RankingListInfo(mContext);
        mRankingListInfo.initialize();
        mRankingListInfo.loadPreferences();
    }

    public static RankingListInfo getmRankingListInfo() {
        return mRankingListInfo;
    }
}
