package com.finger2finger.games.common.store.data;

import com.finger2finger.games.common.store.io.Utils;
import java.util.ArrayList;

public class PersonalPannierTable {
    private ArrayList<PersonalPannier> personalPannierList = new ArrayList<>();

    public ArrayList<PersonalPannier> getPersonalPannierList() {
        return this.personalPannierList;
    }

    public void setPersonalPannierList(ArrayList<PersonalPannier> personalPannierList2) {
        this.personalPannierList = personalPannierList2;
    }

    public void createFile() throws Exception {
        Utils.createFile(TablePath.T_PER_PAN_PATH);
    }

    public String[] readFile() throws Exception {
        return Utils.readFile(TablePath.T_PER_PAN_PATH);
    }

    public void serlize(String[] data) throws Exception {
        Exception e;
        PersonalPannier personalStore = null;
        if (data != null && data.length > 0) {
            int i = 0;
            while (i < data.length) {
                try {
                    personalStore = new PersonalPannier(Utils.getSplitData(data[i], 4));
                    try {
                        this.personalPannierList.add(personalStore);
                        i++;
                    } catch (Exception e2) {
                        e = e2;
                        throw e;
                    }
                } catch (Exception e3) {
                    e = e3;
                    throw e;
                }
            }
        }
    }

    public void write() throws Exception {
        String[] data = new String[this.personalPannierList.size()];
        for (int i = 0; i < this.personalPannierList.size(); i++) {
            data[i] = this.personalPannierList.get(i).toString();
        }
        if (data != null && data.length > 0) {
            Utils.writeFile(TablePath.T_PER_PAN_PATH, data);
        }
    }
}
