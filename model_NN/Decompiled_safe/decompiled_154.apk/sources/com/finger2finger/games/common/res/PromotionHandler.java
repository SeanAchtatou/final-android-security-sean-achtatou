package com.finger2finger.games.common.res;

import android.content.SharedPreferences;
import android.util.Log;
import com.finger2finger.games.common.CommonConst;
import com.finger2finger.games.common.DownLoadFile;
import com.finger2finger.games.common.GoogleAnalyticsUtils;
import com.finger2finger.games.common.activity.F2FGameActivity;
import com.finger2finger.games.common.base.BasePromationEntity;
import com.finger2finger.games.common.store.data.TablePath;
import com.finger2finger.games.common.store.io.Utils;
import com.finger2finger.games.res.Const;
import com.finger2finger.games.res.PortConst;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.anddev.andengine.level.LevelLoader;
import org.anddev.andengine.util.SAXUtils;
import org.anddev.andengine.util.constants.TimeConstants;
import org.xml.sax.Attributes;

public class PromotionHandler {
    public static final String PROPERTY_FILE_PATH = "CommonResource/promotiongame/promotion.xml";
    public static final String TAG_PROMOTION = "promotion";
    public static final String TAG_PROMOTION_APKURL0 = "apkurl0";
    public static final String TAG_PROMOTION_APKURL1 = "apkurl1";
    public static final String TAG_PROMOTION_APKURL2 = "apkurl2";
    public static final String TAG_PROMOTION_COUNTRY = "country";
    public static final String TAG_PROMOTION_DURATION = "duration";
    public static final String TAG_PROMOTION_ENTITY = "promotioninfo";
    public static final String TAG_PROMOTION_GAMENAME = "gamename";
    public static final String TAG_PROMOTION_PACKAGE = "package";
    public static final String TAG_PROMOTION_PORT_PROMOTIONBG = "promotionportbg";
    public static final String TAG_PROMOTION_PROMATIONACTION = "promationaction";
    public static final String TAG_PROMOTION_PROMOTIONACTIONPIC = "promotionactionpic";
    public static final String TAG_PROMOTION_PROMOTIONACTIONPICSIZE = "promotionactionpicsize";
    public static final String TAG_PROMOTION_PROMOTIONBG = "promotionbg";
    public static final String TAG_PROMOTION_PROMOTIONBGSIZE = "promotionbgsize";
    public static final String TAG_PROMOTION_PROMOTIONPORTBGSIZE = "promotionportbgsize";
    public static final String TAG_PROMOTION_PROMTIONACTIONPICPOSION = "promtionactionpicposion";
    public static final String TAG_PROMOTION_PROMTIONACTIONPORTPICPOSION = "promtionactionportpicposion";
    public static final String TAG_PROMOTION_PROMTIONICON = "promotionicon";
    public static final String TAG_PROMOTION_PROMTIONICON_SIZE = "promotioniconcsize";
    public static final String TAG_PROMOTION_UPTATEDATE = "promotionUpdateDate";
    public static final String TAG_PROMOTION_UPTATETIME = "updatetime";
    public ArrayList<BasePromationEntity> downPromationInfo = new ArrayList<>();
    public boolean enableCopy = false;
    public boolean isSDStage = false;
    public int mGamePromotionIndex = 0;
    public ArrayList<BasePromationEntity> promationInfo = new ArrayList<>();

    /* JADX WARNING: Removed duplicated region for block: B:27:0x0068 A[SYNTHETIC, Splitter:B:27:0x0068] */
    /* JADX WARNING: Removed duplicated region for block: B:38:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0033 A[SYNTHETIC, Splitter:B:8:0x0033] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void loadInfo(com.finger2finger.games.common.activity.F2FGameActivity r9, java.lang.String r10) throws java.io.IOException {
        /*
            r8 = this;
            org.anddev.andengine.level.LevelLoader r4 = new org.anddev.andengine.level.LevelLoader
            r4.<init>()
            r2 = 0
            java.util.ArrayList<com.finger2finger.games.common.base.BasePromationEntity> r5 = r8.promationInfo
            r5.clear()
            java.lang.String r5 = "promotion"
            com.finger2finger.games.common.res.PromotionHandler$1 r6 = new com.finger2finger.games.common.res.PromotionHandler$1
            r6.<init>()
            r4.registerEntityLoader(r5, r6)
            java.lang.String r5 = "promotioninfo"
            com.finger2finger.games.common.res.PromotionHandler$2 r6 = new com.finger2finger.games.common.res.PromotionHandler$2
            r6.<init>()
            r4.registerEntityLoader(r5, r6)
            if (r10 == 0) goto L_0x0029
            java.lang.String r5 = ""
            boolean r5 = r10.equals(r5)     // Catch:{ IOException -> 0x0059 }
            if (r5 == 0) goto L_0x0038
        L_0x0029:
            r5 = 0
            r8.isSDStage = r5     // Catch:{ IOException -> 0x0059 }
            java.lang.String r5 = "CommonResource/promotiongame/promotion.xml"
            r4.loadLevelFromAsset(r9, r5)     // Catch:{ IOException -> 0x0059 }
        L_0x0031:
            if (r2 == 0) goto L_0x0037
            r2.close()     // Catch:{ IOException -> 0x004e }
            r2 = 0
        L_0x0037:
            return
        L_0x0038:
            r5 = 1
            r8.isSDStage = r5     // Catch:{ IOException -> 0x0059 }
            java.io.File r1 = new java.io.File     // Catch:{ IOException -> 0x0059 }
            java.io.File r5 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ IOException -> 0x0059 }
            r1.<init>(r5, r10)     // Catch:{ IOException -> 0x0059 }
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0059 }
            r3.<init>(r1)     // Catch:{ IOException -> 0x0059 }
            r4.loadLevelFromStream(r3)     // Catch:{ IOException -> 0x007b, all -> 0x0078 }
            r2 = r3
            goto L_0x0031
        L_0x004e:
            r0 = move-exception
            java.lang.String r5 = "PromotionHandler_loadInfo_instream_error"
            java.lang.String r6 = r0.toString()
            android.util.Log.e(r5, r6)
            goto L_0x0037
        L_0x0059:
            r5 = move-exception
            r0 = r5
        L_0x005b:
            java.lang.String r5 = "PromotionHandler_loadInfo_error"
            java.lang.String r6 = r0.toString()     // Catch:{ all -> 0x0065 }
            android.util.Log.e(r5, r6)     // Catch:{ all -> 0x0065 }
            throw r0     // Catch:{ all -> 0x0065 }
        L_0x0065:
            r5 = move-exception
        L_0x0066:
            if (r2 == 0) goto L_0x006c
            r2.close()     // Catch:{ IOException -> 0x006d }
            r2 = 0
        L_0x006c:
            throw r5
        L_0x006d:
            r0 = move-exception
            java.lang.String r6 = "PromotionHandler_loadInfo_instream_error"
            java.lang.String r7 = r0.toString()
            android.util.Log.e(r6, r7)
            goto L_0x006c
        L_0x0078:
            r5 = move-exception
            r2 = r3
            goto L_0x0066
        L_0x007b:
            r5 = move-exception
            r0 = r5
            r2 = r3
            goto L_0x005b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.finger2finger.games.common.res.PromotionHandler.loadInfo(com.finger2finger.games.common.activity.F2FGameActivity, java.lang.String):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x0072  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void loadDownPicInfo(com.finger2finger.games.common.activity.F2FGameActivity r9, java.lang.String r10, boolean r11, java.lang.String r12) throws java.io.IOException {
        /*
            r8 = this;
            org.anddev.andengine.level.LevelLoader r5 = new org.anddev.andengine.level.LevelLoader
            r5.<init>()
            java.util.ArrayList<com.finger2finger.games.common.base.BasePromationEntity> r6 = r8.downPromationInfo
            r6.clear()
            r4 = 0
            r2 = 0
            java.lang.String r6 = "promotion"
            com.finger2finger.games.common.res.PromotionHandler$3 r7 = new com.finger2finger.games.common.res.PromotionHandler$3
            r7.<init>()
            r5.registerEntityLoader(r6, r7)
            java.lang.String r6 = "promotioninfo"
            com.finger2finger.games.common.res.PromotionHandler$4 r7 = new com.finger2finger.games.common.res.PromotionHandler$4
            r7.<init>()
            r5.registerEntityLoader(r6, r7)
            java.io.File r1 = new java.io.File     // Catch:{ IOException -> 0x004e }
            java.io.File r6 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ IOException -> 0x004e }
            r1.<init>(r6, r10)     // Catch:{ IOException -> 0x004e }
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ IOException -> 0x004e }
            r3.<init>(r1)     // Catch:{ IOException -> 0x004e }
            r5.loadLevelFromStream(r3)     // Catch:{ IOException -> 0x007a, all -> 0x0077 }
            r4 = 1
            if (r4 == 0) goto L_0x0047
            if (r11 == 0) goto L_0x0047
            java.util.ArrayList<com.finger2finger.games.common.base.BasePromationEntity> r6 = r8.downPromationInfo
            if (r6 == 0) goto L_0x0047
            java.util.ArrayList<com.finger2finger.games.common.base.BasePromationEntity> r6 = r8.downPromationInfo
            int r6 = r6.size()
            if (r6 <= 0) goto L_0x0047
            java.util.ArrayList<com.finger2finger.games.common.base.BasePromationEntity> r6 = r8.downPromationInfo
            r8.downLoadAllPromotionInfo(r6, r12)
        L_0x0047:
            if (r3 == 0) goto L_0x007e
            r3.close()
            r2 = 0
        L_0x004d:
            return
        L_0x004e:
            r6 = move-exception
            r0 = r6
        L_0x0050:
            java.lang.String r6 = "PromotionHandler_loadInfo_error"
            java.lang.String r7 = r0.toString()     // Catch:{ all -> 0x005a }
            android.util.Log.e(r6, r7)     // Catch:{ all -> 0x005a }
            throw r0     // Catch:{ all -> 0x005a }
        L_0x005a:
            r6 = move-exception
        L_0x005b:
            if (r4 == 0) goto L_0x0070
            if (r11 == 0) goto L_0x0070
            java.util.ArrayList<com.finger2finger.games.common.base.BasePromationEntity> r7 = r8.downPromationInfo
            if (r7 == 0) goto L_0x0070
            java.util.ArrayList<com.finger2finger.games.common.base.BasePromationEntity> r7 = r8.downPromationInfo
            int r7 = r7.size()
            if (r7 <= 0) goto L_0x0070
            java.util.ArrayList<com.finger2finger.games.common.base.BasePromationEntity> r7 = r8.downPromationInfo
            r8.downLoadAllPromotionInfo(r7, r12)
        L_0x0070:
            if (r2 == 0) goto L_0x0076
            r2.close()
            r2 = 0
        L_0x0076:
            throw r6
        L_0x0077:
            r6 = move-exception
            r2 = r3
            goto L_0x005b
        L_0x007a:
            r6 = move-exception
            r0 = r6
            r2 = r3
            goto L_0x0050
        L_0x007e:
            r2 = r3
            goto L_0x004d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.finger2finger.games.common.res.PromotionHandler.loadDownPicInfo(com.finger2finger.games.common.activity.F2FGameActivity, java.lang.String, boolean, java.lang.String):void");
    }

    public void downLoadAllPromotionInfo(ArrayList<BasePromationEntity> pDownPromationInfo, String pCountryName) {
        try {
            int nCount = pDownPromationInfo.size();
            boolean downLoadFlg = false;
            for (int i = 0; i < nCount; i++) {
                String promotionBg = pDownPromationInfo.get(i).promotionbgPath;
                String promotionPortBg = pDownPromationInfo.get(i).promotionPortBg;
                String actionPic = pDownPromationInfo.get(i).promotionactionpic;
                String actionPicIcon = pDownPromationInfo.get(i).promotionIcon;
                downLoadFlg = DownLoadFile.downLoadFile(Const.PROMOTION_URL + pCountryName + TablePath.SEPARATOR_PATH + promotionBg, TablePath.T_PROMOTION_TEMP_PATH + promotionBg);
                if (downLoadFlg) {
                    downLoadFlg = DownLoadFile.downLoadFile(Const.PROMOTION_URL + pCountryName + TablePath.SEPARATOR_PATH + actionPic, TablePath.T_PROMOTION_TEMP_PATH + actionPic);
                }
                if (downLoadFlg) {
                    downLoadFlg = DownLoadFile.downLoadFile(Const.PROMOTION_URL + pCountryName + TablePath.SEPARATOR_PATH + actionPicIcon, TablePath.T_PROMOTION_TEMP_PATH + actionPicIcon);
                }
            }
            if (downLoadFlg) {
                this.enableCopy = true;
            }
        } catch (Exception e) {
            this.enableCopy = false;
            Log.e("PromotionHandler_downLoadAllPromotionInfo_DownLoadFile_error", e.toString());
        }
    }

    public void loadWebXmlInfo(final F2FGameActivity mContext, String pUrlPath, final long pUpdateTime) throws Exception {
        LevelLoader levelLoader = new LevelLoader();
        InputStream instream = null;
        levelLoader.registerEntityLoader(TAG_PROMOTION, new LevelLoader.IEntityLoader() {
            public void onLoadEntity(String pEntityName, Attributes pAttributes) {
            }
        });
        levelLoader.registerEntityLoader(TAG_PROMOTION_ENTITY, new LevelLoader.IEntityLoader() {
            public void onLoadEntity(String pEntityName, Attributes pAttributes) {
                String attributeOrThrow = SAXUtils.getAttributeOrThrow(pAttributes, PromotionHandler.TAG_PROMOTION_GAMENAME);
                String country = SAXUtils.getAttributeOrThrow(pAttributes, PromotionHandler.TAG_PROMOTION_COUNTRY);
                String updateTime = SAXUtils.getAttributeOrThrow(pAttributes, PromotionHandler.TAG_PROMOTION_UPTATETIME);
                try {
                    PromotionHandler.this.savePromotionDateValue(mContext, Integer.parseInt(SAXUtils.getAttributeOrThrow(pAttributes, PromotionHandler.TAG_PROMOTION_UPTATEDATE)));
                } catch (Exception e) {
                    Log.e("PromotionHandler_loadWebXmlInfo_updateDate_error", e.toString());
                }
                if (pUpdateTime != Long.parseLong(updateTime)) {
                    try {
                        if (DownLoadFile.downLoadFile(Const.PROMOTION_URL + country + TablePath.SEPARATOR_PATH + Const.PROMOTION_XML_NAME, TablePath.T_PROMOTION_TEMP_PATH + Const.PROMOTION_XML_NAME)) {
                            PromotionHandler.this.loadDownPicInfo(mContext, TablePath.T_PROMOTION_TEMP_PATH + Const.PROMOTION_XML_NAME, true, country);
                        }
                    } catch (Exception e2) {
                        Log.e("PromotionHandler_loadWebXmlInfo_downLoadFile_error", e2.toString());
                    }
                }
            }
        });
        try {
            instream = getWebStream(pUrlPath);
            levelLoader.loadLevelFromStream(instream);
            if (instream != null) {
                try {
                    instream.close();
                } catch (IOException e) {
                    Log.e("PromotionHandler_loadInfo_instream_error", e.toString());
                }
            }
        } catch (IOException e2) {
            IOException e3 = e2;
            Log.e("PromotionHandler_loadInfo_error", e3.toString());
            throw e3;
        } catch (Throwable th) {
            if (instream != null) {
                try {
                    instream.close();
                } catch (IOException e4) {
                    Log.e("PromotionHandler_loadInfo_instream_error", e4.toString());
                }
            }
            throw th;
        }
    }

    public void showPromtion(F2FGameActivity pContext) throws Exception {
        if (PortConst.enablePromotion) {
            String promtionXmlPath = TablePath.T_PROMOTION_USE_PATH + Const.PROMOTION_XML_NAME;
            boolean fileExist = Utils.isFileExist(promtionXmlPath);
            if (!Const.enableSDCard || !fileExist) {
                loadAssetXml(pContext);
            } else {
                loadSDXml(pContext, promtionXmlPath);
            }
            if (this.promationInfo == null || this.promationInfo.size() <= 0) {
                checkWebInfo(pContext, 0);
            } else {
                checkWebInfo(pContext, this.promationInfo.get(0).updatetime);
            }
            setPromotionGame(pContext);
            return;
        }
        PortConst.showPromtionIcon = false;
    }

    public void checkWebInfo(final F2FGameActivity pContext, final long pUpdateTime) {
        if (Const.enableSDCard && com.finger2finger.games.common.Utils.checkNetWork(pContext)) {
            new Thread(new Runnable() {
                public void run() {
                    try {
                        PromotionFile.inizillie();
                        PromotionHandler.this.loadWebXmlInfo(pContext, Const.PROMOTION_URL + Const.PROMOTION_INDEX_XML_NAME, pUpdateTime);
                    } catch (Exception e) {
                        Exception e2 = e;
                        Log.e("PromotionHandler_checkWebInfo_loadWebXmlInfo_error", e2.toString());
                        GoogleAnalyticsUtils.setTracker("PromotionHandler_checkWebInfo_loadWebXmlInfo_error,detail is " + e2.toString());
                    }
                }
            }).start();
        }
    }

    public InputStream getWebStream(String pUrlPath) throws IOException {
        return ((HttpURLConnection) new URL(pUrlPath).openConnection()).getInputStream();
    }

    public void cutFile() throws IOException {
        this.enableCopy = false;
        PromotionFile.copyFileFromTemp(TablePath.T_PROMOTION_TEMP_PATH, TablePath.T_PROMOTION_USE_PATH);
    }

    public void loadAssetXml(F2FGameActivity pContext) {
        try {
            loadInfo(pContext, "");
        } catch (Exception e) {
            Exception e2 = e;
            PortConst.enablePromotion = false;
            PortConst.showPromtionIcon = false;
            Log.e("PromotionHandler_loadAssetXml_loadInfo_error", e2.toString());
            GoogleAnalyticsUtils.setTracker("PromotionHandler_loadAssetXml_loadInfo_error,detail is " + e2.toString());
        }
    }

    public void loadSDXml(F2FGameActivity pContext, String pFilePath) {
        try {
            loadInfo(pContext, pFilePath);
            int gameIndex = getGameIndex(this.promationInfo, pContext);
            if (PromotionFile.checkFileInfo(TablePath.T_PROMOTION_USE_PATH + Const.PROMOTION_XML_NAME, TablePath.T_PROMOTION_USE_PATH + this.promationInfo.get(gameIndex).promotionbgPath, this.promationInfo.get(gameIndex).promotionbgsize, TablePath.T_PROMOTION_USE_PATH + this.promationInfo.get(gameIndex).promotionPortBg, this.promationInfo.get(gameIndex).promotionPortSize, TablePath.T_PROMOTION_USE_PATH + this.promationInfo.get(gameIndex).promotionactionpic, this.promationInfo.get(gameIndex).promotionactionpicsize, TablePath.T_PROMOTION_USE_PATH + this.promationInfo.get(gameIndex).promotionIcon, this.promationInfo.get(gameIndex).promotioniconSize, false) != CommonConst.PROMATION_FILE_STATUS.ENABLE_USE) {
                loadAssetXml(pContext);
            }
        } catch (Exception e) {
            loadAssetXml(pContext);
            Log.e("PromotionHandler_loadSDXml_loadInfo_error", e.toString());
            GoogleAnalyticsUtils.setTracker("PromotionHandler_loadSDXml_loadInfo_error,detail is " + e.toString());
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 7 */
    public void setPromotionGame(F2FGameActivity pContext) {
        String pBgPath;
        if (this.promationInfo == null || this.promationInfo.size() <= 0) {
            PortConst.enablePromotion = false;
            PortConst.showPromtionIcon = false;
            return;
        }
        int gameIndex = getGameIndex(this.promationInfo, pContext);
        this.mGamePromotionIndex = gameIndex;
        if (!com.finger2finger.games.common.Utils.checkGameInstall(pContext, this.promationInfo.get(gameIndex).packageName)) {
            String pBgPath2 = this.promationInfo.get(gameIndex).promotionbgPath;
            String pActionPath = Const.PROMOTION_FILE_PATH + this.promationInfo.get(gameIndex).promotionactionpic;
            String iconPath = Const.PROMOTION_FILE_PATH + this.promationInfo.get(gameIndex).promotionIcon;
            if (this.isSDStage) {
                pActionPath = TablePath.T_PROMOTION_USE_PATH + this.promationInfo.get(gameIndex).promotionactionpic;
                pBgPath = TablePath.T_PROMOTION_USE_PATH + pBgPath2;
                iconPath = TablePath.T_PROMOTION_USE_PATH + this.promationInfo.get(gameIndex).promotionIcon;
            } else {
                pBgPath = Const.PROMOTION_FILE_PATH + pBgPath2;
            }
            pContext.commonResource.pIsSDCard = this.isSDStage;
            pContext.commonResource.pIconPath = iconPath;
            pContext.commonResource.loadPromotionSource(this.isSDStage, false, pBgPath, pActionPath);
            PortConst.loadingTime = (long) ((this.promationInfo.get(gameIndex).duration + 3) * TimeConstants.MILLISECONDSPERSECOND);
            PortConst.loadLinkUrl = pContext.promotionHandler.promationInfo.get(gameIndex).apkUrlPath0;
            return;
        }
        PortConst.enablePromotion = false;
        PortConst.showPromtionIcon = false;
    }

    public int getGameIndex(ArrayList<BasePromationEntity> pPromationInfo, F2FGameActivity pContext) {
        int infoSize = pPromationInfo.size();
        readPromotionData(pContext);
        if (Const.promotionIndex == -1 || Const.promotionTime == null || Const.promotionTime.equals("")) {
            Const.promotionTime = String.valueOf(new SimpleDateFormat("yyyyMMdd").format(new Date(System.currentTimeMillis())));
            Const.promotionIndex = 0;
        } else if (Const.promotionIndex >= infoSize) {
            Const.promotionIndex = 0;
            Const.promotionTime = String.valueOf(new SimpleDateFormat("yyyyMMdd").format(new Date(System.currentTimeMillis())));
        } else {
            String nowTime = String.valueOf(new SimpleDateFormat("yyyyMMdd").format(new Date(System.currentTimeMillis())));
            if (getQuot(nowTime, Const.promotionTime) >= ((long) Const.promotionUpdateDate)) {
                Const.promotionIndex++;
                if (Const.promotionIndex >= infoSize) {
                    Const.promotionIndex = 0;
                }
                Const.promotionTime = nowTime;
            }
        }
        savePromotionData(Const.promotionTime, Const.promotionIndex, pContext);
        boolean isChoose = false;
        int i = 0;
        while (true) {
            if (i < infoSize) {
                if (Const.promotionIndex <= i && !com.finger2finger.games.common.Utils.checkGameInstall(pContext, this.promationInfo.get(i).packageName)) {
                    Const.promotionIndex = i;
                    isChoose = true;
                    break;
                }
                i++;
            } else {
                break;
            }
        }
        if (!isChoose) {
            int i2 = 0;
            while (true) {
                if (i2 < infoSize) {
                    if (Const.promotionIndex > i2 && !com.finger2finger.games.common.Utils.checkGameInstall(pContext, this.promationInfo.get(i2).packageName)) {
                        Const.promotionIndex = i2;
                        break;
                    }
                    i2++;
                } else {
                    break;
                }
            }
        }
        return Const.promotionIndex;
    }

    public void savePromotionData(String pTime, int pGameIndex, F2FGameActivity pContext) {
        SharedPreferences.Editor editor = pContext.getSharedPreferences(Const.GAME_NAME, 1).edit();
        editor.putString(Const.PROMOTION_TIME, pTime);
        editor.putInt(Const.PROMOTION_INDEX, pGameIndex);
        editor.commit();
    }

    public void savePromotionDateValue(F2FGameActivity pContext, int pDateValue) {
        SharedPreferences.Editor editor = pContext.getSharedPreferences(Const.GAME_NAME, 1).edit();
        editor.putInt(Const.PROMOTION_DATE_VALUE, pDateValue);
        editor.commit();
    }

    public void readPromotionData(F2FGameActivity pContext) {
        SharedPreferences gamePreferences = pContext.getSharedPreferences(Const.GAME_NAME, 1);
        Const.promotionTime = gamePreferences.getString(Const.PROMOTION_TIME, String.valueOf(new SimpleDateFormat("yyyyMMdd").format(new Date(System.currentTimeMillis()))));
        Const.promotionIndex = gamePreferences.getInt(Const.PROMOTION_INDEX, 0);
        Const.promotionUpdateDate = gamePreferences.getInt(Const.PROMOTION_DATE_VALUE, Const.promotionDefaultDate);
    }

    public long getQuot(String time1, String time2) {
        SimpleDateFormat ft = new SimpleDateFormat("yyyyMMdd");
        try {
            return ((((ft.parse(time1).getTime() - ft.parse(time2).getTime()) / 1000) / 60) / 60) / 24;
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }
}
