package com.finger2finger.games.common.scene.dialog;

import com.f2fgames.games.monkeybreak.lite.R;
import com.finger2finger.games.common.CommonConst;
import com.finger2finger.games.common.Utils;
import com.finger2finger.games.common.activity.F2FGameActivity;
import com.finger2finger.games.common.base.BaseFont;
import com.finger2finger.games.common.base.BaseSprite;
import com.finger2finger.games.common.res.CommonResource;
import com.finger2finger.games.res.Resource;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.primitive.Rectangle;
import org.anddev.andengine.entity.scene.CameraScene;
import org.anddev.andengine.entity.text.ChangeableText;
import org.anddev.andengine.input.touch.TouchEvent;

public class PortraitTextDialog extends CameraScene {
    /* access modifiers changed from: private */
    public F2FGameActivity mContext;
    private String mMsgContent;

    public PortraitTextDialog(int layerCount, Camera camera, F2FGameActivity pContext, String pMsgContent) {
        super(layerCount, camera);
        this.mMsgContent = pMsgContent;
        this.mContext = pContext;
        loadScene();
        setBackgroundEnabled(false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.finger2finger.games.common.base.BaseSprite.<init>(float, float, float, org.anddev.andengine.opengl.texture.region.TextureRegion, boolean):void
     arg types: [int, int, float, org.anddev.andengine.opengl.texture.region.TextureRegion, int]
     candidates:
      com.finger2finger.games.common.base.BaseSprite.<init>(float, float, float, float, org.anddev.andengine.opengl.texture.region.TextureRegion):void
      com.finger2finger.games.common.base.BaseSprite.<init>(float, float, float, org.anddev.andengine.opengl.texture.region.TextureRegion, boolean):void */
    public void loadScene() {
        Rectangle rectangle = new Rectangle(0.0f, 0.0f, (float) CommonConst.CAMERA_WIDTH, (float) CommonConst.CAMERA_HEIGHT);
        rectangle.setColor(CommonConst.GrayColor.red, CommonConst.GrayColor.green, CommonConst.GrayColor.blue);
        rectangle.setAlpha(CommonConst.GrayColor.alpha);
        getLayer(0).addEntity(rectangle);
        BaseSprite mStartDialogSpr = new BaseSprite(0.0f, 0.0f, CommonConst.RALE_SAMALL_VALUE * 1.15f, this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.GAME_DIALOG_BG.mKey), false);
        float dlogWith = mStartDialogSpr.getWidth();
        float dlogHeight = mStartDialogSpr.getHeight();
        float dlogPX = (((float) CommonConst.CAMERA_WIDTH) - dlogWith) / 2.0f;
        float dlogPY = (((float) CommonConst.CAMERA_HEIGHT) - dlogHeight) / 2.0f;
        mStartDialogSpr.setPosition(dlogPX, dlogPY);
        getLayer(0).addEntity(mStartDialogSpr);
        AnonymousClass1 r6 = new BaseSprite(0.0f, 0.0f, CommonConst.RALE_SAMALL_VALUE, this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.BUTTON_BLANK.mKey), false) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() != 0) {
                    return true;
                }
                PortraitTextDialog.this.mContext.getEngine().getScene().clearChildScene();
                PortraitTextDialog.this.mContext.getmGameScene().onTextDialogOKBtn();
                return true;
            }
        };
        if (!CommonConst.ENABLE_SHOW_ADVIEW) {
            dlogPX = (((float) CommonConst.CAMERA_WIDTH) - dlogWith) / 2.0f;
            dlogPY = ((((float) CommonConst.CAMERA_HEIGHT) - dlogHeight) - r6.getHeight()) - (5.0f * CommonConst.RALE_SAMALL_VALUE);
            mStartDialogSpr.setPosition(dlogPX, dlogPY);
        }
        float dlogYesWith = r6.getWidth();
        float height = r6.getHeight();
        float dlogYesPX = (dlogPX + dlogWith) - (1.2f * dlogYesWith);
        float dlogYesPY = dlogPY + dlogHeight;
        r6.setPosition(dlogYesPX, dlogYesPY);
        registerTouchArea(r6);
        getLayer(0).addEntity(r6);
        ChangeableText changeableText = new ChangeableText(0.0f, 0.0f, this.mContext.mResource.getBaseFontByKey(Resource.FONT.DIALOG_CONTEXT.mKey), "OK");
        changeableText.setPosition(dlogYesPX + ((r6.getWidth() - changeableText.getWidth()) / 2.0f), dlogYesPY + ((r6.getHeight() - changeableText.getHeight()) / 2.0f));
        getTopLayer().addEntity(changeableText);
        BaseFont mDefaultFont = this.mContext.mResource.getBaseFontByKey(Resource.FONT.DEFOUT_FONT.mKey);
        String msgTitleStr = this.mContext.getResources().getString(R.string.str_tips);
        float textTitlePX = dlogPX + ((dlogWith - ((float) Utils.getMaxCharacterLens(mDefaultFont, msgTitleStr))) / 2.0f);
        float textTitlePY = dlogPY + (25.0f * CommonConst.RALE_SAMALL_VALUE);
        ChangeableText changeableText2 = new ChangeableText(textTitlePX, textTitlePY, mDefaultFont, msgTitleStr);
        changeableText2.setPosition(textTitlePX, textTitlePY);
        getLayer(0).addEntity(changeableText2);
        String autoLineMsg = Utils.getAutoLine(mDefaultFont, this.mMsgContent, (int) (dlogWith - (100.0f * CommonConst.RALE_SAMALL_VALUE)), false, 0);
        ChangeableText mStartContent = new ChangeableText(dlogPX + ((dlogWith - ((float) Utils.getMaxCharacterLens(mDefaultFont, autoLineMsg))) / 2.0f), textTitlePY + (((dlogHeight - ((textTitlePY - dlogPY) + ((float) mDefaultFont.getLineHeight()))) - ((float) Utils.getLineHight(mDefaultFont, autoLineMsg))) / 2.0f), mDefaultFont, autoLineMsg, mDefaultFont.getStringWidth(autoLineMsg + "0000"));
        mStartContent.setScale(0.85f);
        getLayer(0).addEntity(mStartContent);
    }
}
