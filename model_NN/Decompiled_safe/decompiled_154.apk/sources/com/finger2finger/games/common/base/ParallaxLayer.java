package com.finger2finger.games.common.base;

import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.layer.FixedCapacityLayer;
import org.anddev.andengine.entity.shape.Shape;

public class ParallaxLayer extends FixedCapacityLayer {
    private float mMaxHeight = 0.0f;
    float mParallaxFactorY = 1.5f;
    float mParallaxValueY = 0.0f;
    Boolean mRepeatY = true;
    Boolean mShouldCull = false;

    public ParallaxLayer(int pCapacity) {
        super(pCapacity);
    }

    public void setParallaxValue(float pParallaxValueY) {
        this.mParallaxValueY = pParallaxValueY;
    }

    public float getParallaxValue() {
        return this.mParallaxValueY;
    }

    private float getWidthScaled() {
        float width = 0.0f;
        for (int i = 0; i < getEntityCount(); i++) {
            Shape shape = (Shape) getEntity(i);
            if (width < shape.getWidthScaled()) {
                width = shape.getWidthScaled();
            }
        }
        return width;
    }

    private float getHeightScaled() {
        float height = 0.0f;
        for (int i = 0; i < getEntityCount(); i++) {
            height += ((Shape) getEntity(i)).getHeightScaled();
        }
        return height;
    }

    /* access modifiers changed from: protected */
    public void onManagedDraw(GL10 pGL, Camera pCamera) {
        float shapeHeightScaled;
        pGL.glPushMatrix();
        float width = pCamera.getWidth();
        float cameraHeight = this.mMaxHeight <= 0.0f ? pCamera.getHeight() : this.mMaxHeight;
        float widthScaled = getWidthScaled();
        if (getHeightScaled() > cameraHeight) {
            shapeHeightScaled = getHeightScaled();
        } else {
            shapeHeightScaled = cameraHeight;
        }
        float baseOffsetY = this.mParallaxValueY * this.mParallaxFactorY;
        if (this.mRepeatY.booleanValue()) {
            baseOffsetY %= shapeHeightScaled;
            while (baseOffsetY > 0.0f) {
                baseOffsetY -= shapeHeightScaled;
            }
        }
        pGL.glTranslatef(0.0f, baseOffsetY, 0.0f);
        super.onManagedDraw(pGL, pCamera);
        if (this.mRepeatY.booleanValue()) {
            float currentMaxY = baseOffsetY;
            do {
                pGL.glTranslatef(0.0f, shapeHeightScaled, 0.0f);
                currentMaxY += shapeHeightScaled;
                super.onManagedDraw(pGL, pCamera);
            } while (currentMaxY < cameraHeight);
            pGL.glTranslatef(0.0f, (-currentMaxY) + baseOffsetY, 0.0f);
        }
        pGL.glPopMatrix();
    }

    public float getMMaxHeight() {
        return this.mMaxHeight;
    }

    public void setMMaxHeight(float maxHeight) {
        this.mMaxHeight = maxHeight;
    }
}
