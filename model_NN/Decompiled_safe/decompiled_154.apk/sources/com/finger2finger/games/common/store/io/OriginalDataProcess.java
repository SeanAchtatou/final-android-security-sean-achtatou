package com.finger2finger.games.common.store.io;

import com.finger2finger.games.common.CommonConst;
import com.finger2finger.games.common.store.data.TablePath;
import java.util.ArrayList;
import java.util.List;

public class OriginalDataProcess {
    public static final char DEFAULT_ESCAPE_CHARACTER = '\"';
    public static final String DEFAULT_LINE_END = "\n";
    public static final char DEFAULT_QUOTE_CHARACTER = '\"';
    public static final char NO_ESCAPE_CHARACTER = 0;
    public static final char NO_QUOTE_CHARACTER = 0;

    public static String Strings2AES128(String[] data) throws Exception {
        if (data == null) {
            return null;
        }
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < data.length; i++) {
            if (i != 0) {
                sb.append(TablePath.SEPARATOR_ITEM.charAt(0));
            }
            String nextElement = data[i];
            if (nextElement != null) {
                sb.append('\"');
                for (int j = 0; j < nextElement.length(); j++) {
                    char nextChar = nextElement.charAt(j);
                    if (nextChar == '\"') {
                        sb.append('\"').append(nextChar);
                    } else if (nextChar == '\"') {
                        sb.append('\"').append(nextChar);
                    } else {
                        sb.append(nextChar);
                    }
                }
                sb.append('\"');
            }
        }
        sb.append("\n");
        String stream = sb.toString();
        if (stream != null) {
            try {
                stream = F2FCrypto.encrypt(CommonConst.MASTER_PASSWORD, stream);
            } catch (Exception e) {
                throw e;
            }
        }
        return stream;
    }

    public static String[] AES1282Strings(String nextLine) throws Exception {
        try {
            String nextLine2 = F2FCrypto.decrypt(CommonConst.MASTER_PASSWORD, nextLine).replaceAll("\n", "");
            if (nextLine2 == null) {
                return null;
            }
            List<String> tokensOnThisLine = new ArrayList<>();
            StringBuffer sb = new StringBuffer();
            boolean isStart = false;
            for (int i = 0; i < nextLine2.length(); i++) {
                char c = nextLine2.charAt(i);
                if (c == '\"') {
                    if (!isStart) {
                        isStart = true;
                    } else {
                        if (!sb.toString().equals("")) {
                            tokensOnThisLine.add(sb.toString());
                        }
                        sb = new StringBuffer();
                        isStart = false;
                    }
                } else if (isStart) {
                    sb.append(c);
                }
            }
            return (String[]) tokensOnThisLine.toArray(new String[0]);
        } catch (Exception e) {
            throw e;
        }
    }
}
