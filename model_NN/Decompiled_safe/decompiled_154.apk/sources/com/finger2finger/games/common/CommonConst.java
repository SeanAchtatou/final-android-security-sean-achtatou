package com.finger2finger.games.common;

import android.content.Context;
import com.finger2finger.games.common.activity.F2FGameActivity;
import com.finger2finger.games.common.scene.F2FScene;
import com.finger2finger.games.common.store.data.TablePath;
import java.util.ArrayList;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.region.TextureRegion;

public class CommonConst {
    public static int ADD_EVERYDAY_GOLDEN_MONEY = 0;
    public static long ADS_DEFAUT_VALUE = -2;
    public static final int ARROW_LEFT = 0;
    public static final int ARROW_RIGHT = 1;
    public static float AdViewHeight = 80.0f;
    public static final float BTN_BIG_RALE = 1.3f;
    public static float BTN_SCALE_TIME = 1.0f;
    public static float BTN_SPEED = 0.4f;
    public static final float BUTTON_BIG_VALUE = 1.0f;
    public static int CAMERA_HEIGHT = 320;
    public static int CAMERA_MAX_HEIGHT = 480;
    public static int CAMERA_MAX_HEIGHT_PORTAIT = 800;
    public static int CAMERA_MAX_WIDTH = 800;
    public static int CAMERA_MAX_WIDTH_PORTAIT = 480;
    public static int CAMERA_WIDTH = 480;
    public static final float DIALOG_BIG_VALUE = 1.15f;
    public static final float DIALOG_NEXT_BIG_VALUE = 1.0f;
    public static long DIALY_CLEAR_ADS_DEFAUT = -2;
    public static final long DIALY_Millis = 86400000;
    public static String DOWNLOAD_PATH = (TablePath.ROOT_PATH + TablePath.SEPARATOR_PATH + "download");
    public static boolean ENABLE_INVITE_FRIEND = true;
    public static boolean ENABLE_SHOW_ADVIEW = false;
    public static boolean ENBALE_SHOW_INVITE_DIALOG = true;
    public static final int EVERYDAY_ADD_GOLDEN_MONEY = 2;
    public static final String EXTRA_PRODUCT_ID = "product_id";
    public static final String EXTRA_STORE_ID = "store_id";
    public static String F2F_GAMES_PACKAGE_NAME = "com.f2fgames.games";
    public static String FACEBOOK_LINK = "http://www.facebook.com/sharer/sharer.php?u=https://market.android.com/details?id=";
    public static long FOREVER_CLEAR_ADS_DEFAUT = -2;
    public static final int FRAME_THICKNESS = 3;
    public static boolean FROM_GAMESCENE_TO_SHOP = false;
    public static final float GAME_ABOUT_MOVETIME = 0.5f;
    public static final float GAME_ABOUT_SCALE = 0.5f;
    public static final float GAME_ABOUT_TXT_MOVETIME = 0.2f;
    public static int GAME_EXCHANGE = 5;
    public static int GAME_GOLD = 0;
    public static int GAME_HELP_COUNT = 1;
    public static boolean GAME_HELP_SHOW = true;
    public static String GAME_LOAD_DAY = "";
    public static int GAME_MAX_SCORE = 0;
    public static boolean GAME_MUSIC_ON = true;
    public static int GAME_STORAGE_MODE_LEVEL = 0;
    public static boolean GAME_STORAGE_MODE_ON = false;
    public static String GAME_VERSION = "1.0.0M";
    public static int GMAE_MULTIMODE = 1;
    public static final String GOOGLE_MARKET_PACKAGE = "com.android.vending";
    public static final String HELP_FORMART_PICPATH = "CommonResource/help/help%d.png";
    public static int[][] HONORSINFO = new int[0][];
    public static final String HONOR_LIST_DATA = "HonorListData";
    public static final String HONOR_RECORD_DATA = "HonorRecordData";
    public static int INVITE_FRIEND_GOLD_NUMBER = 1;
    public static int INVITE_FRIEND_MAX_NAM = 3;
    public static int INVITE_FRIEND_NUM = 0;
    public static boolean IS_ADD_EVERYDAY_GOLDEN_MONEY = false;
    public static boolean IS_FIRST_PLAY_GAME = false;
    public static boolean IS_GAMEOVER = false;
    public static int LAST_F2FDIALOG_TYPE = -1;
    public static final int LIFE_BONUS = 1;
    public static int MAP_ARRAY_X = 32;
    public static int MAP_ARRAY_Y = 32;
    public static float MAP_HEIGHT = 768.0f;
    public static float MAP_TILE_HEIGHT = 40.0f;
    public static float MAP_TILE_WIDTH = 50.0f;
    public static float MAP_WIDTH = 1024.0f;
    public static String MASTER_PASSWORD = "f2f";
    public static float MIN_TOUCH_DEFAUT = 50.0f;
    public static float MIN_TOUCH_DEFAUT_Y = 30.0f;
    public static String MMAdviewID = "28911";
    public static final float MODE_PICTURE_HEIGHT = 400.0f;
    public static final float MODE_PICTURE_WIDHT = 400.0f;
    public static String NAME_PREFERENCES = "Game";
    public static final int OMISSION_STRING_LENGTH = 40;
    public static int PASS_LEVEL_BY_HIGH_SCORE = 1;
    public static int PASS_LEVEL_GOLD_NUMBER = 1;
    public static int PASS_LEVEL_MAXNUM = 2;
    public static int PASS_LEVEL_NUM = 0;
    public static String PROMOTION_DATE_VALUE = "PROMOTION_DATE_VALUE";
    public static String PROMOTION_FILE_PATH = "CommonResource/promotiongame/";
    public static String PROMOTION_INDEX = "PROMOTION_INDEX";
    public static String PROMOTION_INDEX_XML_NAME = "promotionindex.xml";
    public static String PROMOTION_TIME = "PROMOTION_TIME";
    public static String PROMOTION_URL = "http://download01.finger2finger.com/promotion/";
    public static String PROMOTION_XML_NAME = "promotion.xml";
    public static String QQ_LINK = "http://v.t.qq.com/share/share.php?url=http%3A%2F%2Fwww.finger2finger.com&appkey=appkey&site=&pic=https://market.android.com/publish/images/PQAAAAwdg-kfh9nfhC13XVwynZ8iuZvkuqDugd6GXq1YpN56yIVn54k0RV4MvqoiGId8K9swXfgW0GLDNy7AtnZLoNIAzfqVaUW637J0glfhe8OlRL6ANq3AGE9g.png&title=I%20like%20it";
    public static float RALE_SAMALL_VALUE = 1.0f;
    public static float RALE_SMALL_STAR = 0.4f;
    public static final int RANKINGLIST_COUNT = 10;
    public static final String RANKINGLIST_DATA = "RankingListData";
    public static int REMAIN_TIME = 10;
    public static String RENREN_LINK = "http://share.renren.com/share/buttonshare.do?link=http%3A%2F%2Fwww.finger2finger.com&title=I like it";
    public static String SEPARATOR_ITEM_BRANCH = ":";
    public static String SEPARATOR_ITEM_COMMA = ",";
    public static final String SHOP_TYPEFACE_CONTENT = "CommonResource/fonts/shop_content.otf";
    public static final String SHOP_TYPEFACE_TITLE = "CommonResource/fonts/shop_header.TTF";
    public static String SINA_LINK = "http://v.t.sina.com.cn/share/share.php?url=http%3A%2F%2Fwww.finger2finger.com&title=";
    public static int[] SMS_LEVEL = {-1};
    public static int[][] SMS_SUB_LEVEL = {new int[]{-1, -1}};
    public static final String SPLIT_CHAPTER = "\\t";
    public static final float START_ABOUT_MOVESPEED = -0.5f;
    public static final float START_MENUSTONE_MOVETIME = 0.5f;
    public static final float START_MENU_MOVETIME = 0.65f;
    public static final float START_MENU_SCALETIME = 0.8f;
    public static final float START_MENU_SCALE_FROM = 1.0f;
    public static final float START_MENU_SCALE_TO = 1.2f;
    public static int STAR_NUM = 3;
    public static final int SUBLEVEL_SINGLE_UNIT = 10;
    public static int TIMEOUT_VALUE = 5000;
    public static final int TIME_BONUS = 30;
    public static String TWITTER_LINK = "http://twitter.com/intent/tweet?text=Check+out+this+game+on+the+Android+Market!&url=http%3A%2F%2Fmarket.android.com%2Fdetails%3Fid%3D";
    public static final long UPDATE_USER_POINT_DURATION = 60000;
    public static final String VOTE_SUFFIX_HTTP = "https://market.android.com/details?id=";
    public static final String VOTE_SUFFIX_MARKET = "market://details?id=";
    public static String Version = "1.0.0";
    public static boolean enableCheckWire = false;
    public static boolean enableDownLoadGame = true;
    public static boolean enableDynamicPic = false;
    public static boolean enableFaceBook = false;
    public static boolean enableGmail = false;
    public static boolean enableHoner = false;
    public static boolean enableMMAdView = false;
    public static boolean enableMultiMode = false;
    public static boolean enableQQ = false;
    public static boolean enableRankingList = false;
    public static boolean enableRenRen = false;
    public static boolean enableSDCard = false;
    public static boolean enableSMS = false;
    public static boolean enableSina = false;
    public static boolean enableTwitter = false;
    public static boolean enbaleShowMarketDialog = false;
    public static boolean enbaleShowStarDialog = false;
    public static ERROR_LEVEL errorLevel = ERROR_LEVEL.CORRECT;
    public static ArrayList<ERROR_LEVEL> errorList = new ArrayList<>();
    public static int honorDayCountNum = 3;
    public static int honorDayNum = -2;
    public static int honorInviteCountNum = 5;
    public static int honorInviteNum = 0;
    public static boolean isMultiMode = false;
    public static String levelModePic = "CommonResource/mode/mode_%d.png";
    public static String packageName = "";
    public static int passLevelCountNum = 3;
    public static int passLevelNum = 0;
    public static int promotionDefaultDate = 3;
    public static int promotionIndex = -1;
    public static String promotionTime = "20100815";
    public static int promotionUpdateDate = promotionDefaultDate;

    public enum ERROR_LEVEL {
        CORRECT,
        UNREPAIR,
        GAMEINFO_REPAIR,
        STORE_TABLE_TABLE_REPAIR,
        PERSONAL_PANNIER_REPAIR,
        PRODUCT_TABLE_REPAIR,
        GAME_TABLE_REPAIR,
        SALE_OFF_TABLE_REPAIR,
        PERSONALACCOUNT_REPAIR,
        USER_POINTS_REPAIR,
        HONOR_REPAIR
    }

    public static class F2FALERT_MSGID {
        public static final int DOWNLOAD_MSG = 21;
        public static final int EVERYDAY_START_MSG = 1;
        public static final int EXIT_GAME = 15;
        public static final int FIRST_PLAY_MSG = 0;
        public static final int INSTALL_MSG = 22;
        public static final int INVITE_CALLBACK_MSG = 5;
        public static final int INVITE_IN_GAME_MSG = 4;
        public static final int INVITE_IN_MAINMENU_MSG = 3;
        public static final int MESSAGE_QUEUE = 100;
        public static final int PASSLEVEL_MSG = 2;
        public static final int PRO_LIFE_GO_SHOP = 11;
        public static final int PRO_LIFE_PROP = 9;
        public static final int PRO_REMAIN_MSG = 6;
        public static final int PRO_TIME_GO_SHOP = 10;
        public static final int PRO_TIME_PROP = 8;
        public static final int PRO_TO_BUY_MSG = 7;
        public static final int REPAIR_MSG = 14;
        public static final int SCORE_GAME = 12;
        public static final int SMS_SEND_CONFIRM = 17;
        public static final int SMS_SEND_FAIL = 18;
        public static final int SMS_SEND_OK = 20;
        public static final int SMS_SEND_WAITING = 19;
        public static final int UNREPAIR_MSG = 13;
        public static final int UPDATE_GAME_MSG = 23;
        public static final int WIRELESS_SET = 16;
    }

    public enum GAME_STATUS {
        UNDEFINE,
        NO_DOWNLOAD,
        DOWNLOADING,
        CANDOWNLOADED,
        DOWNLOAD_FAIL,
        NO_INSTALL,
        INSTALL,
        NEED_UPDATE
    }

    public static class GrayColor {
        public static float alpha = 0.6f;
        public static float blue = 0.648f;
        public static float green = 0.648f;
        public static float red = 0.648f;
    }

    public enum HONOR_STATUS {
        MOREGAME_HONOR,
        HARDWORK_HONOR,
        INVITE_HONOR,
        MARKET_HONOR,
        STAR_HONOR,
        CLEARANCE_HONOR
    }

    public enum PROMATION_FILE_STATUS {
        NO_EXIST,
        NEED_UPDATE,
        ENABLE_USE
    }

    public enum PROMATION_TYPE {
        SHOW,
        ZOOM,
        MOVE
    }

    public static String getHonorID(HONOR_STATUS pHonorStatus) {
        switch (pHonorStatus) {
            case MOREGAME_HONOR:
                return "0";
            case HARDWORK_HONOR:
                return "1";
            case INVITE_HONOR:
                return "2";
            case MARKET_HONOR:
                return "3";
            case STAR_HONOR:
                return "4";
            case CLEARANCE_HONOR:
                return "5";
            default:
                return "0";
        }
    }

    public static TextureRegion setLevelModeRegion(Context pContext, Texture pTexture, int pModeID) {
        return null;
    }

    public static void createCustomRandom() {
    }

    public static void clearCustomeRandom() {
    }

    public static F2FScene createCustomozedScene(F2FGameActivity mContext) {
        return null;
    }

    public static PROMATION_TYPE getPromationType(String pTypeValue) {
        if (pTypeValue.equals("S")) {
            return PROMATION_TYPE.SHOW;
        }
        if (pTypeValue.equals("Z")) {
            return PROMATION_TYPE.ZOOM;
        }
        if (pTypeValue.equals("M")) {
            return PROMATION_TYPE.MOVE;
        }
        return PROMATION_TYPE.SHOW;
    }
}
