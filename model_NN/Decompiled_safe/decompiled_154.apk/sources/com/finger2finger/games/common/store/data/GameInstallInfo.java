package com.finger2finger.games.common.store.data;

public class GameInstallInfo {
    public static final int LIST_ITEM_COUNT = 4;
    public long fileCerrentSize;
    public String fileName;
    public String filePath;
    public long fileSize;

    public GameInstallInfo(String[] data) throws Exception {
        if (data == null || data.length != 4) {
            throw new IllegalArgumentException();
        }
        this.fileName = data[0];
        this.filePath = data[1];
        this.fileSize = Long.parseLong(data[2]);
        this.fileCerrentSize = Long.parseLong(data[3]);
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(this.fileName).append(TablePath.SEPARATOR_ITEM).append(this.filePath).append(TablePath.SEPARATOR_ITEM).append(this.fileSize).append(TablePath.SEPARATOR_ITEM).append(this.fileCerrentSize);
        return sb.toString();
    }
}
