package com.finger2finger.games.common.store.data;

public class RankingList {
    public static final int LIST_ITEM_COUNT = 2;
    private String name = "";
    private int score = 0;

    public int getScore() {
        return this.score;
    }

    public String getName() {
        return this.name;
    }

    public void setScore(int score2) {
        this.score = score2;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public RankingList(String[] data) throws Exception {
        if (data == null || data.length == 0) {
            throw new IllegalArgumentException();
        }
        this.score = Integer.parseInt(data[0]);
        if (data.length < 2) {
            this.name = "";
        } else {
            this.name = data[1];
        }
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(this.score).append(TablePath.SEPARATOR_ITEM).append(this.name);
        return sb.toString();
    }
}
