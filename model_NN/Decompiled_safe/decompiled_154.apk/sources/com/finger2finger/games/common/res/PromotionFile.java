package com.finger2finger.games.common.res;

import android.os.Environment;
import com.finger2finger.games.common.CommonConst;
import com.finger2finger.games.common.store.data.TablePath;
import com.finger2finger.games.common.store.io.Utils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class PromotionFile {
    public static void inizillie() throws Exception {
        Utils.createDir(TablePath.T_PROMOTION_TEMP_PATH);
        Utils.createDir(TablePath.T_PROMOTION_USE_PATH);
        delelteFile(TablePath.T_PROMOTION_TEMP_PATH);
    }

    public static void cutFile(String pFromPath, String ToPath) throws IOException {
        File fileFrom = new File(Environment.getExternalStorageDirectory(), pFromPath);
        File fileTo = new File(Environment.getExternalStorageDirectory(), ToPath);
        FileInputStream fis = new FileInputStream(fileFrom);
        FileOutputStream fos = new FileOutputStream(fileTo);
        byte[] buf = new byte[512];
        while (true) {
            int i = fis.read(buf);
            if (i != -1) {
                fos.write(buf, 0, i);
            } else {
                fis.close();
                fos.close();
                fileFrom.delete();
                return;
            }
        }
    }

    public static void copyFileFromTemp(String pFromDir, String pToDir) throws IOException {
        File[] files;
        File destDir = new File(Environment.getExternalStorageDirectory(), pFromDir);
        if (destDir.isDirectory() && (files = destDir.listFiles()) != null) {
            int nCount = files.length;
            for (int i = 0; i < nCount; i++) {
                cutFile(pFromDir + files[i].getName(), pToDir + files[i].getName());
            }
        }
    }

    public static void delelteFile(String pFromDir) {
        File[] files;
        File destDir = new File(Environment.getExternalStorageDirectory(), pFromDir);
        if (destDir.isDirectory() && (files = destDir.listFiles()) != null) {
            for (File delete : files) {
                delete.delete();
            }
        }
    }

    /* JADX INFO: Multiple debug info for r4v1 long: [D('pBgPic' java.lang.String), D('picSize' long)] */
    public static CommonConst.PROMATION_FILE_STATUS checkFileInfo(String pXmlPath, String pBgPic, long pBgPicSize, String promotionPortBg, long pBgPortPicSize, String pActionPic, long pActionPicSize, String pIconPic, long pIconSize, boolean pIsPortScreen) {
        long picSize;
        File bgPicFile;
        File xmlFile = new File(Environment.getExternalStorageDirectory(), pXmlPath);
        if (pIsPortScreen) {
            picSize = pBgPortPicSize;
            bgPicFile = new File(Environment.getExternalStorageDirectory(), promotionPortBg);
        } else {
            picSize = pBgPicSize;
            bgPicFile = new File(Environment.getExternalStorageDirectory(), pBgPic);
        }
        File bgActPicFile = new File(Environment.getExternalStorageDirectory(), pActionPic);
        File iconPicFile = new File(Environment.getExternalStorageDirectory(), pIconPic);
        CommonConst.PROMATION_FILE_STATUS retValue = CommonConst.PROMATION_FILE_STATUS.NO_EXIST;
        if (!xmlFile.exists() || !bgPicFile.exists() || !bgActPicFile.exists() || !iconPicFile.exists() || bgPicFile.length() != picSize || bgActPicFile.length() != pActionPicSize || iconPicFile.length() != pIconSize) {
            return retValue;
        }
        return CommonConst.PROMATION_FILE_STATUS.ENABLE_USE;
    }
}
