package com.finger2finger.games.common;

import org.anddev.andengine.entity.particle.Particle;
import org.anddev.andengine.entity.particle.modifier.BaseDoubleValueSpanModifier;

public class FixedStepXModifier extends BaseDoubleValueSpanModifier {
    private float horizon;
    private float mFromX;
    private float mStep;
    private float mToX;
    private float mTotalStep;
    private float mX;
    private float pinlv;
    private float px;
    private float step;
    private float zhenfu;

    public FixedStepXModifier(float pFromX, float pToX, float pFromTime, float pToTime) {
        this(pFromX, pToX, pFromX, pToX, pFromTime, pToTime);
        this.zhenfu = ((float) ((Math.random() * 5.0d) + 1.0d)) / 20.0f;
        this.pinlv = ((float) ((Math.random() * 5.0d) + 1.0d)) / 200.0f;
        this.horizon = (pFromX + pToX) / 2.0f;
    }

    public FixedStepXModifier(float pFromScaleX, float pToScaleX, float pFromScaleY, float pToScaleY, float pFromTime, float pToTime) {
        super(pFromScaleX, pToScaleX, pFromScaleY, pToScaleY, pFromTime, pToTime);
        this.mStep = 3.0f;
        this.mTotalStep = 0.0f;
        this.mX = 0.0f;
        this.mFromX = 0.0f;
        this.mToX = 0.0f;
        this.step = 1.0f;
    }

    /* access modifiers changed from: protected */
    public void onSetInitialValues(Particle arg0, float arg1, float arg2) {
    }

    /* access modifiers changed from: protected */
    public void onSetValues(Particle arg0, float arg1, float arg2) {
        if (arg0.getVelocityX() >= 10.0f || arg0.getVelocityX() <= -10.0f) {
            this.step = -this.step;
        }
        arg0.setVelocityX(arg0.getVelocityX() + this.step);
    }
}
