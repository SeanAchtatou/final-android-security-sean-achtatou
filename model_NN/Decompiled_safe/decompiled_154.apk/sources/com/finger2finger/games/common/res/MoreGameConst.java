package com.finger2finger.games.common.res;

import android.util.Log;
import com.finger2finger.games.common.Utils;
import com.finger2finger.games.common.activity.F2FGameActivity;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import org.anddev.andengine.level.LevelLoader;
import org.anddev.andengine.util.SAXUtils;
import org.xml.sax.Attributes;

public class MoreGameConst {
    public static final String GOOGLE_MARKET_PACKAGE = "com.android.vending";
    public static final String MARKET_LINK = "market://details?id=";
    public static String[][] MoreGameInfo = {new String[]{"CommonResource/moregame/crabrevenge.png", "market://details?id=com.finger2finger.games.crabrevenge.lite", "crabrevenge", "http://www.finger2finger.com/sample.apk"}, new String[]{"CommonResource/moregame/seastarcorps.png", "market://details?id=com.finger2finger.games.seastarcorps.lite", "seastarcorps", "http://www.finger2finger.com/sample.apk"}, new String[]{"CommonResource/moregame/11.png", "market://details?id=com.finger2finger.games.eleven.lite", "eleven", "http://www.finger2finger.com/sample.apk"}, new String[]{"CommonResource/moregame/airballoon.png", "market://details?id=com.finger2finger.games.airballoon", "airballoon", "http://www.finger2finger.com/sample.apk"}, new String[]{"CommonResource/moregame/greedyangel.png", "market://details?id=com.finger2finger.games.greedyangel.lite", "greedyangel", "http://www.finger2finger.com/sample.apk"}, new String[]{"CommonResource/moregame/easterchicken.png", "market://details?id=com.finger2finger.games.easterchicken.lite", "easterchicken", "http://www.finger2finger.com/sample.apk"}, new String[]{"CommonResource/moregame/deepseafishing.png", "market://details?id=com.finger2finger.games.deepseafishing.lite", "deepseafishing", "http://www.finger2finger.com/sample.apk"}, new String[]{"CommonResource/moregame/monkeyclear.png", "market://details?id=com.finger2finger.games.monkeyclear.lite", "monkeyclear", "http://www.finger2finger.com/sample.apk"}, new String[]{"CommonResource/moregame/paccrab.png", "market://details?id=com.finger2finger.games.paccrab.lite", "paccrab", "http://www.finger2finger.com/sample.apk"}, new String[]{"CommonResource/moregame/halloween.png", "market://details?id=com.finger2finger.snowfor4.halloween.am", "halloween", "http://www.finger2finger.com/sample.apk"}, new String[]{"CommonResource/moregame/powerball.png", "market://details?id=com.finger2finger.games.powerball", "powerball", "http://www.finger2finger.com/sample.apk"}};
    public static final String PROPERTY_FILE_PATH = "CommonResource/moregame/moregame.properties";
    public static final String SUFFIX_MARKET_LINK = "https://market.android.com/details?id=";
    public static final String TAG_MOREGAME = "moregame";
    public static final String TAG_MOREGAME_APK = "apk";
    public static final String TAG_MOREGAME_ENTITY = "entity";
    public static final String TAG_MOREGAME_LINK = "link";
    public static final String TAG_MOREGAME_NAME = "name";
    public static final String TAG_MOREGAME_PATH = "path";
    public static ArrayList<String[]> enableInstallGame = new ArrayList<>();
    public static boolean installMarket = false;

    private static class MoreGame {
        String mApk;
        String mLink;
        String mName;
        String mPath;

        public MoreGame(String pPath, String pLink, String pName, String pApk) {
            this.mPath = pPath;
            this.mLink = pLink;
            this.mName = pName;
            this.mApk = pApk;
        }
    }

    public static void loadMoreGame(F2FGameActivity mContext) {
        final ArrayList<MoreGame> moregames = new ArrayList<>();
        LevelLoader levelLoader = new LevelLoader();
        levelLoader.registerEntityLoader(TAG_MOREGAME, new LevelLoader.IEntityLoader() {
            public void onLoadEntity(String pEntityName, Attributes pAttributes) {
            }
        });
        levelLoader.registerEntityLoader(TAG_MOREGAME_ENTITY, new LevelLoader.IEntityLoader() {
            public void onLoadEntity(String pEntityName, Attributes pAttributes) {
                String link = SAXUtils.getAttributeOrThrow(pAttributes, MoreGameConst.TAG_MOREGAME_LINK);
                moregames.add(new MoreGame(SAXUtils.getAttributeOrThrow(pAttributes, MoreGameConst.TAG_MOREGAME_PATH), link, SAXUtils.getAttributeOrThrow(pAttributes, "name"), SAXUtils.getAttributeOrThrow(pAttributes, MoreGameConst.TAG_MOREGAME_APK)));
            }
        });
        try {
            levelLoader.loadLevelFromAsset(mContext, PROPERTY_FILE_PATH);
            if (moregames.size() > 0) {
                installMarket = Utils.checkGameInstall(mContext, "com.android.vending");
                MoreGameInfo = (String[][]) Array.newInstance(String.class, moregames.size(), 4);
                for (int i = 0; i < moregames.size(); i++) {
                    MoreGameInfo[i][0] = ((MoreGame) moregames.get(i)).mPath;
                    MoreGameInfo[i][1] = installMarket ? "market://details?id=" + ((MoreGame) moregames.get(i)).mLink : "https://market.android.com/details?id=" + ((MoreGame) moregames.get(i)).mLink;
                    MoreGameInfo[i][2] = ((MoreGame) moregames.get(i)).mName;
                    MoreGameInfo[i][3] = ((MoreGame) moregames.get(i)).mApk;
                }
            }
        } catch (IOException e) {
            Log.e("MoreGameConst_loadMoreGame_error", e.getMessage());
        }
    }
}
