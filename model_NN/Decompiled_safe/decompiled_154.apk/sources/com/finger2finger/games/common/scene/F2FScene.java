package com.finger2finger.games.common.scene;

import android.view.GestureDetector;
import android.view.MotionEvent;
import com.finger2finger.games.common.activity.F2FGameActivity;
import java.util.ArrayList;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.extension.input.touch.controller.MultiTouch;
import org.anddev.andengine.extension.input.touch.controller.MultiTouchController;
import org.anddev.andengine.extension.input.touch.controller.MultiTouchException;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.input.touch.controller.SingleTouchControler;
import org.anddev.andengine.sensor.accelerometer.AccelerometerData;
import org.anddev.andengine.sensor.accelerometer.IAccelerometerListener;
import org.anddev.andengine.sensor.orientation.IOrientationListener;
import org.anddev.andengine.sensor.orientation.OrientationData;

public abstract class F2FScene extends Scene implements Scene.IOnSceneTouchListener, IAccelerometerListener, GestureDetector.OnGestureListener {
    public final int LIFE_OVER = 1;
    public final int TIME_OVER = 0;
    /* access modifiers changed from: private */
    public boolean enableAccelerometerSensor = false;
    private boolean enableGestureDetector = false;
    public boolean isChildSceneClick = false;
    public boolean isGameOver = false;
    public boolean isSMSSendCanle = false;
    public boolean isTimeLimitedPlaying = false;
    /* access modifiers changed from: private */
    public AccelerometerData mAccelerometerData = new AccelerometerData();
    protected F2FGameActivity mContext;
    private IOrientationListener mIOrientationListener = new IOrientationListener() {
        public void onOrientationChanged(OrientationData pOrientationData) {
            F2FScene.this.setOnOrientationChanged(pOrientationData);
        }
    };
    public int mMeter = 0;
    /* access modifiers changed from: private */
    public MotionEvent mMotionEventEnd;
    /* access modifiers changed from: private */
    public MotionEvent mMotionEventStart;
    public int mScore = 0;
    /* access modifiers changed from: private */
    public Sprite mShape;
    /* access modifiers changed from: private */
    public TouchEvent mSpriteTouchEvent;
    private int mTouchCount = -1;
    private ArrayList<TouchEvent> mTouchEvents = new ArrayList<>();
    /* access modifiers changed from: private */
    public float mVelocityX;
    /* access modifiers changed from: private */
    public float mVelocityY;
    public GestureDetector myGesture;

    public abstract void loadScene();

    public F2FScene(int pLayerCount, F2FGameActivity pContext) {
        super(pLayerCount);
        this.mContext = pContext;
        registerUpdateHandler(new UpdateSceneHandle());
    }

    public void handleAccelerometerChangedEvent(AccelerometerData pAccelerometerData) {
    }

    public void onAccelerometerChanged(AccelerometerData pAccelerometerData) {
        synchronized (this.mAccelerometerData) {
            this.mAccelerometerData = pAccelerometerData;
        }
    }

    public void enableAccelerometerSensor() {
        this.mContext.getEngine().enableAccelerometerSensor(this.mContext, this);
        this.enableAccelerometerSensor = true;
    }

    public void disableAccelerometerSensor() {
        this.mContext.getEngine().disableAccelerometerSensor(this.mContext);
        this.enableAccelerometerSensor = false;
    }

    public void handleTouchEvent(ArrayList<TouchEvent> arrayList) {
    }

    public void enableSceneTouch() {
        setOnSceneTouchListener(this);
    }

    public void disableSceneTouch() {
        setOnSceneTouchListener(this);
    }

    public void enableMultiTouch() {
        try {
            if (MultiTouch.isSupported(this.mContext)) {
                this.mContext.getEngine().setTouchController(new MultiTouchController());
            }
        } catch (MultiTouchException e) {
        }
    }

    public void disableMultiTouch() {
        this.mContext.getEngine().setTouchController(new SingleTouchControler());
    }

    public void setTouchCount(int touchCount) {
        this.mTouchCount = touchCount;
    }

    public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
        if (!this.enableGestureDetector || this.myGesture == null) {
            ArrayList<TouchEvent> touchEvents = new ArrayList<>();
            touchEvents.add(pSceneTouchEvent);
            handleTouchEvent(touchEvents);
            return true;
        }
        synchronized (this.myGesture) {
            this.myGesture.onTouchEvent(pSceneTouchEvent.getMotionEvent());
        }
        return true;
    }

    private class UpdateSceneHandle implements IUpdateHandler {
        private UpdateSceneHandle() {
        }

        public void onUpdate(float pSecondsElapsed) {
            if (F2FScene.this.mMotionEventStart != null) {
                synchronized (F2FScene.this.mMotionEventStart) {
                    F2FScene.this.handleFlingEvent(F2FScene.this.mMotionEventStart, F2FScene.this.mMotionEventEnd, F2FScene.this.mVelocityX, F2FScene.this.mVelocityY);
                    MotionEvent unused = F2FScene.this.mMotionEventStart = null;
                }
            }
            if (F2FScene.this.mSpriteTouchEvent != null) {
                synchronized (F2FScene.this.mSpriteTouchEvent) {
                    F2FScene.this.handleSpriteTuchEvent(F2FScene.this.mShape, F2FScene.this.mSpriteTouchEvent);
                    TouchEvent unused2 = F2FScene.this.mSpriteTouchEvent = null;
                }
            }
            if (F2FScene.this.enableAccelerometerSensor) {
                synchronized (F2FScene.this.mAccelerometerData) {
                    F2FScene.this.handleAccelerometerChangedEvent(F2FScene.this.mAccelerometerData);
                }
            }
        }

        public void reset() {
        }
    }

    public void enableOrientationSensor() {
        this.mContext.getEngine().enableOrientationSensor(this.mContext, this.mIOrientationListener);
    }

    public void disableOrientationSensor() {
        this.mContext.getEngine().disableOrientationSensor(this.mContext);
    }

    public void setOnOrientationChanged(OrientationData pOrientationData) {
    }

    public void handleFlingEvent(MotionEvent pMotionEventStart, MotionEvent pMotionEventEnd, float pVelocityX, float pVelocityY) {
    }

    public boolean onFling(MotionEvent pMotionEventStart, MotionEvent pMotionEventEnd, float pVelocityX, float pVelocityY) {
        this.mMotionEventStart = pMotionEventStart;
        this.mMotionEventEnd = pMotionEventEnd;
        this.mVelocityX = pVelocityX;
        this.mVelocityY = pVelocityY;
        return true;
    }

    public void onSpriteTuch(Sprite pShape, TouchEvent pSpriteTouchEvent) {
        this.mShape = pShape;
        this.mSpriteTouchEvent = pSpriteTouchEvent;
    }

    public boolean handleSpriteTuchEvent(Sprite pShape, TouchEvent pSpriteTouchEvent) {
        return true;
    }

    public void operSound(boolean pGameResumed) {
    }

    public void showHelpDialog() {
    }

    public void rePlayGame() {
    }

    public boolean onDown(MotionEvent e) {
        return false;
    }

    public void onLongPress(MotionEvent e) {
    }

    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    public void onShowPress(MotionEvent e) {
    }

    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    public void enableGestureDetector() {
        this.myGesture = new GestureDetector(this);
        this.enableGestureDetector = true;
    }

    public void disableGestureDetector() {
        this.enableGestureDetector = false;
    }

    public void checkLife() {
    }

    public void checkTime() {
    }

    public void showOKDialog() {
    }

    public void useProp(int pPropId) {
    }

    public void showScoreGame() {
    }

    public void loadGameOverTips() {
    }

    public void showGameOverDialog() {
    }

    public void setGameOver(int pGameOverType) {
    }

    public Scene getmContextMenuScene() {
        return null;
    }

    public void clearContextMenu() {
    }

    public void showContextMenu() {
    }

    public void backToUpMenu() {
    }

    public void onTextDialogOKBtn() {
    }

    public void onTextDialogCancelBtn() {
    }

    public void downloadImage() {
    }

    public void setActionUpOperation() {
    }

    public void settagetPosition() {
    }

    public void setSMSNextProcess() {
    }
}
