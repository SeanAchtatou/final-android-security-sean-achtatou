package com.finger2finger.games.common.store.activity;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.adknowledge.superrewards.SuperRewardsImpl;
import com.f2fgames.games.monkeybreak.lite.R;
import com.finger2finger.games.common.CommonConst;
import com.finger2finger.games.common.Utils;
import com.finger2finger.games.common.activity.F2FGameActivity;
import com.finger2finger.games.common.store.data.PersonalAccount;
import com.finger2finger.games.common.store.data.PersonalAccountTable;
import com.finger2finger.games.common.store.data.PersonalPannier;
import com.finger2finger.games.common.store.data.PersonalPannierTable;
import com.finger2finger.games.common.store.data.Product;
import com.finger2finger.games.common.store.data.ProductTable;
import com.finger2finger.games.common.store.data.Store;
import com.finger2finger.games.common.store.data.StoreTable;
import com.finger2finger.games.common.store.io.TableLoad;
import com.finger2finger.games.res.PortConst;
import java.util.ArrayList;

public class ShopActivity extends ListActivity {
    private String F2F_STORE;
    private String F2F_STORE_ITEMS;
    private String ITEMS_IN_MYBAG;
    private final int MYBAG = 1;
    private String MY_BAG;
    private String PRICE;
    private final int SHOP = 0;
    private String UNITS;
    private ListView listView;
    private PersonalAccount mPersonalAccount;
    private PersonalAccountTable mPersonalAccountTable;
    /* access modifiers changed from: private */
    public PersonalPannierTable mPersonalPannierTable;
    /* access modifiers changed from: private */
    public ProductTable mProductTable;
    private StoreTable mStoreTable;
    /* access modifiers changed from: private */
    public String mStrOR;
    private TableLoad mTableLoad;
    /* access modifiers changed from: private */
    public int mType = 0;
    private Typeface mTypefaceTitle;
    private ArrayList<Product> propDetails = new ArrayList<>();
    private Button shop_button_getmore;
    private Button shop_button_type;
    private TextView shop_finger_point;
    private TextView shop_gold_point;
    private TextView shop_label_price;
    private TextView shop_label_type;
    private ArrayList<Store> storeList = new ArrayList<>();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.setFullScreen(this);
        setContentView((int) R.layout.my_list);
        this.mTableLoad = F2FGameActivity.getTableLoad();
        if (this.mTableLoad != null) {
            this.mStoreTable = this.mTableLoad.getmStoreTable();
            this.mProductTable = this.mTableLoad.getmProductTable();
            this.storeList = this.mStoreTable.getStoreList();
            this.mPersonalPannierTable = this.mTableLoad.getmPersonalPannierTable();
            this.mPersonalAccountTable = this.mTableLoad.getmPersonalAccountTable();
            this.mPersonalAccount = this.mPersonalAccountTable.getPersonalAccountList().get(0);
            loadResouces();
            findView();
            updateView();
            setTypeface();
        }
    }

    private void setTypeface() {
        this.shop_label_type.setTypeface(this.mTypefaceTitle);
        this.shop_button_type.setTypeface(this.mTypefaceTitle);
        this.shop_label_price.setTypeface(this.mTypefaceTitle);
    }

    private void loadResouces() {
        try {
            this.mTypefaceTitle = Typeface.createFromAsset(getAssets(), CommonConst.SHOP_TYPEFACE_TITLE);
        } catch (Exception e) {
            this.mTypefaceTitle = Typeface.DEFAULT_BOLD;
            Log.e("Shop_load_typeface_title_error", e.getMessage());
        }
        this.F2F_STORE_ITEMS = getResources().getString(R.string.store_label_store_items);
        this.ITEMS_IN_MYBAG = getResources().getString(R.string.store_label_mybag_items);
        this.PRICE = getResources().getString(R.string.store_label_price);
        this.UNITS = getResources().getString(R.string.store_label_units);
        this.F2F_STORE = getResources().getString(R.string.store_button_store);
        this.MY_BAG = getResources().getString(R.string.store_button_mybag);
        this.mStrOR = getResources().getString(R.string.store_label_or);
    }

    private void updateView() {
        updateViewHeader();
        updateViewFoorter();
        updateListView();
    }

    /* access modifiers changed from: private */
    public void updateListView() {
        if (this.mType == 0) {
            buildShopListView();
        } else if (this.mType == 1) {
            buildMyBagListView();
        }
    }

    private void findView() {
        this.listView = getListView();
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                ShopActivity.this.buyGood((int) arg3);
            }
        });
        getViewHeader();
        getViewFooter();
    }

    private void getViewHeader() {
        this.shop_label_type = (TextView) findViewById(R.id.shop_label_type);
        this.shop_button_type = (Button) findViewById(R.id.shop_button_type);
        this.shop_button_type.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int unused = ShopActivity.this.mType = (ShopActivity.this.mType + 1) % 2;
                ShopActivity.this.updateViewHeader();
                ShopActivity.this.updateListView();
            }
        });
        this.shop_label_price = (TextView) findViewById(R.id.shop_label_price);
    }

    private void getViewFooter() {
        this.shop_finger_point = (TextView) findViewById(R.id.shop_finger_point);
        this.shop_gold_point = (TextView) findViewById(R.id.shop_gold_point);
        this.shop_button_getmore = (Button) findViewById(R.id.shop_button_getmore);
        this.shop_button_getmore.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                ShopActivity.this.startSuperRewards();
            }
        });
    }

    /* access modifiers changed from: private */
    public void startSuperRewards() {
        if (Utils.checkNetWork(this)) {
            new SuperRewardsImpl(com.finger2finger.games.framework.R.class).showOffers(this, PortConst.AppSupserRewardsHParameter, this.mPersonalAccount.getId(), com.adknowledge.superrewards.Utils.getCountryCode());
        } else {
            showDialogOK(getString(R.string.store_tip_network_inavailable));
        }
    }

    private void showDialogOK(String pTxt) {
        new AlertDialog.Builder(this).setTitle(getString(R.string.game_title_tips)).setMessage(pTxt).setPositiveButton(getResources().getString(R.string.str_button_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).create().show();
    }

    private void updateViewFoorter() {
        this.shop_finger_point.setText(String.valueOf((this.mPersonalAccount.getFinger_money_count() + this.mPersonalAccount.getBonus_finger_money()) - this.mPersonalAccount.getComsume_finger_money()));
        this.shop_gold_point.setText(String.valueOf(this.mPersonalAccount.getGolden_count()));
    }

    /* access modifiers changed from: private */
    public void updateViewHeader() {
        if (this.mType == 0) {
            this.shop_label_type.setText(this.F2F_STORE_ITEMS);
            this.shop_button_type.setText(this.MY_BAG);
            this.shop_label_price.setText(this.PRICE);
        } else if (this.mType == 1) {
            this.shop_label_type.setText(this.ITEMS_IN_MYBAG);
            this.shop_button_type.setText(this.F2F_STORE);
            this.shop_label_price.setText(this.UNITS);
        }
    }

    /* access modifiers changed from: private */
    public void buyGood(int position) {
        String product_id;
        String store_id = "";
        if (this.mType == 0) {
            store_id = this.storeList.get(position).getId();
            product_id = this.storeList.get(position).getProduct_id();
        } else {
            product_id = this.propDetails.get(position).getId();
            for (int i = 0; i < this.storeList.size(); i++) {
                if (this.propDetails.get(position).getId().equals(this.storeList.get(i).getProduct_id())) {
                    store_id = this.storeList.get(i).getId();
                }
            }
        }
        Intent intent = new Intent(this, PurchaseActivity.class);
        intent.putExtra(CommonConst.EXTRA_STORE_ID, store_id);
        intent.putExtra(CommonConst.EXTRA_PRODUCT_ID, product_id);
        startActivity(intent);
    }

    public void onResume() {
        if (this.mTableLoad != null) {
            this.mTableLoad.updateUserPoints();
            updateViewFoorter();
            updateListView();
        }
        super.onResume();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        finish();
        return true;
    }

    private void buildShopListView() {
        ArrayList<Store> details = new ArrayList<>();
        details.addAll(this.storeList);
        this.listView.setAdapter((ListAdapter) new ShopAdapter(this, details));
    }

    private class ShopAdapter extends BaseAdapter {
        private ArrayList<Store> details;
        private LayoutInflater inflater;
        private Context mContext;

        public ShopAdapter(Context context, ArrayList<Store> details2) {
            this.details = details2;
            this.inflater = LayoutInflater.from(context);
            this.mContext = context;
        }

        public int getCount() {
            return this.details.size();
        }

        public boolean areAllItemsEnabled() {
            return true;
        }

        public boolean isEnabled(int position) {
            return true;
        }

        public Object getItem(int position) {
            return this.details.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            Store store = this.details.get(position);
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = this.inflater.inflate((int) R.layout.my_item, (ViewGroup) null);
                holder.icon = (ImageView) convertView.findViewById(R.id.image_icon);
                holder.name = (TextView) convertView.findViewById(16908308);
                holder.comment = (TextView) convertView.findViewById(R.id.shop_item_explanation);
                holder.text1 = (TextView) convertView.findViewById(R.id.shop_item_finger_point);
                holder.image1 = (ImageView) convertView.findViewById(R.id.shop_item_image_finger);
                holder.text2 = (TextView) convertView.findViewById(R.id.shop_item_gold_point);
                holder.image2 = (ImageView) convertView.findViewById(R.id.shop_item_image_gold);
                holder.button = (Button) convertView.findViewById(R.id.shop_item_button_buymore);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            convertView.setTag(holder);
            Product pr = null;
            int i = 0;
            while (true) {
                if (i >= ShopActivity.this.mProductTable.getProductList().size()) {
                    break;
                } else if (store.getProduct_id().equals(ShopActivity.this.mProductTable.getProductList().get(i).getId())) {
                    pr = ShopActivity.this.mProductTable.getProductList().get(i);
                    break;
                } else {
                    i++;
                }
            }
            if (pr != null) {
                Bitmap bitMap = null;
                try {
                    bitMap = com.finger2finger.games.common.store.io.Utils.loadBitmapFromAssets(this.mContext, pr.getIcon());
                } catch (Exception e) {
                    Log.e("F2FError", e.getMessage());
                }
                holder.icon.setImageBitmap(bitMap);
                holder.name.setText(pr.getName());
                holder.comment.setText(Utils.getOmissionString(pr.getComment(), 40));
                boolean fingerEnable = false;
                if (pr.getFingerprice() > 0) {
                    holder.text1.setText(String.valueOf(pr.getFingerprice()));
                    holder.text1.setVisibility(0);
                    holder.image1.setVisibility(0);
                    fingerEnable = true;
                } else {
                    holder.text1.setVisibility(4);
                    holder.image1.setVisibility(4);
                }
                if (pr.getGoldprice() > 0) {
                    holder.text2.setText((fingerEnable ? ShopActivity.this.mStrOR + " " : "") + String.valueOf(pr.getGoldprice()));
                    holder.text2.setVisibility(0);
                    holder.image2.setVisibility(0);
                } else {
                    holder.text2.setVisibility(4);
                    holder.image2.setVisibility(4);
                }
                holder.button.setVisibility(8);
            }
            return convertView;
        }
    }

    private void buildMyBagListView() {
        this.propDetails = new ArrayList<>();
        for (int i = 0; i < this.mPersonalPannierTable.getPersonalPannierList().size(); i++) {
            PersonalPannier pa = this.mPersonalPannierTable.getPersonalPannierList().get(i);
            for (int j = 0; j < this.mProductTable.getProductList().size(); j++) {
                Product pr = this.mProductTable.getProductList().get(j);
                if (pa.getProduct_id().equals(pr.getId())) {
                    this.propDetails.add(pr);
                }
            }
        }
        this.listView.setAdapter((ListAdapter) new MyBagAdapter(this, this.propDetails));
    }

    private class MyBagAdapter extends BaseAdapter {
        private ArrayList<Product> details;
        private LayoutInflater inflater;
        private Context mContext;

        public MyBagAdapter(Context context, ArrayList<Product> details2) {
            this.details = details2;
            this.inflater = LayoutInflater.from(context);
            this.mContext = context;
        }

        public int getCount() {
            return this.details.size();
        }

        public boolean areAllItemsEnabled() {
            return true;
        }

        public boolean isEnabled(int position) {
            return true;
        }

        public Object getItem(int position) {
            return this.details.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            Product d = this.details.get(position);
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = this.inflater.inflate((int) R.layout.my_item, (ViewGroup) null);
                holder.icon = (ImageView) convertView.findViewById(R.id.image_icon);
                holder.name = (TextView) convertView.findViewById(16908308);
                holder.comment = (TextView) convertView.findViewById(R.id.shop_item_explanation);
                holder.text1 = (TextView) convertView.findViewById(R.id.shop_item_finger_point);
                holder.image1 = (ImageView) convertView.findViewById(R.id.shop_item_image_finger);
                holder.text2 = (TextView) convertView.findViewById(R.id.shop_item_gold_point);
                holder.image2 = (ImageView) convertView.findViewById(R.id.shop_item_image_gold);
                holder.button = (Button) convertView.findViewById(R.id.shop_item_button_buymore);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            convertView.setTag(holder);
            Bitmap bitMap = null;
            try {
                bitMap = com.finger2finger.games.common.store.io.Utils.loadBitmapFromAssets(this.mContext, d.getIcon());
            } catch (Exception e) {
                Log.e("F2FError", e.getMessage());
            }
            holder.icon.setImageBitmap(bitMap);
            holder.name.setText(d.getName());
            holder.image1.setVisibility(4);
            holder.text2.setVisibility(4);
            holder.image2.setVisibility(4);
            holder.button.setVisibility(0);
            holder.button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    ShopActivity.this.buyGood(position);
                }
            });
            int i = 0;
            while (true) {
                if (i >= ShopActivity.this.mPersonalPannierTable.getPersonalPannierList().size()) {
                    break;
                }
                PersonalPannier pa = ShopActivity.this.mPersonalPannierTable.getPersonalPannierList().get(i);
                if (d.getId().equals(pa.getProduct_id())) {
                    holder.comment.setText(Utils.getOmissionString(d.getComment(), 40));
                    holder.text1.setText(String.format(ShopActivity.this.getString(R.string.store_product_storage_count), Integer.valueOf(pa.getProduct_count())));
                    break;
                }
                i++;
            }
            return convertView;
        }
    }

    private static class ViewHolder {
        Button button;
        TextView comment;
        ImageView icon;
        ImageView image1;
        ImageView image2;
        TextView name;
        TextView text1;
        TextView text2;

        private ViewHolder() {
        }
    }
}
