package com.finger2finger.games.common.store.data;

import com.finger2finger.games.common.store.io.Utils;
import java.util.ArrayList;

public class SaleOffTable {
    private ArrayList<SaleOff> saleOffList = new ArrayList<>();

    public void createFile() throws Exception {
        Utils.createFile(TablePath.T_SALEOFF_PATH);
    }

    public String[] readFile() throws Exception {
        return Utils.readFile(TablePath.T_SALEOFF_PATH);
    }

    public void serlize(String[] data) throws Exception {
        Exception e;
        SaleOff saleOff = null;
        if (data != null && data.length > 0) {
            int i = 0;
            while (i < data.length) {
                try {
                    saleOff = new SaleOff(Utils.getSplitData(data[i], 7));
                    try {
                        this.saleOffList.add(saleOff);
                        i++;
                    } catch (Exception e2) {
                        e = e2;
                        throw e;
                    }
                } catch (Exception e3) {
                    e = e3;
                    throw e;
                }
            }
        }
    }

    public void write() throws Exception {
        String[] data = new String[this.saleOffList.size()];
        for (int i = 0; i < this.saleOffList.size(); i++) {
            data[i] = this.saleOffList.get(i).toString();
        }
        if (data != null && data.length > 0) {
            Utils.writeFile(TablePath.T_SALEOFF_PATH, data);
        }
    }
}
