package com.finger2finger.games.common.activity;

import android.app.ListActivity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.f2fgames.games.monkeybreak.lite.R;
import com.finger2finger.games.common.Utils;
import com.finger2finger.games.common.res.CommonRankingList;
import com.finger2finger.games.common.store.data.RankingList;
import java.util.ArrayList;

public class RankingListActivity extends ListActivity {
    private View headerView;
    private ListView listView;
    private MyListAdapter mAdapter;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.setFullScreen(this);
        setContentView((int) R.layout.rankinglist_header);
        this.listView = getListView();
        setView();
    }

    private void setView() {
        buildListView();
    }

    private void buildListView() {
        ArrayList<RankingList> rl = new ArrayList<>();
        RankingList[] mRankingList = CommonRankingList.getmRankingListInfo().getmRankingList();
        if (mRankingList != null && mRankingList.length > 0) {
            for (RankingList add : mRankingList) {
                rl.add(add);
            }
        }
        this.mAdapter = new MyListAdapter(this, rl);
        this.listView.setAdapter((ListAdapter) this.mAdapter);
    }

    private class MyListAdapter extends BaseAdapter {
        private ArrayList<RankingList> details;
        private LayoutInflater inflater;

        public MyListAdapter(Context context, ArrayList<RankingList> details2) {
            this.details = details2;
            this.inflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return this.details.size();
        }

        public boolean areAllItemsEnabled() {
            return true;
        }

        public boolean isEnabled(int position) {
            return true;
        }

        public Object getItem(int position) {
            return this.details.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        /* Debug info: failed to restart local var, previous not found, register: 5 */
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            RankingList d = this.details.get(position);
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = this.inflater.inflate((int) R.layout.rankinglist_item, (ViewGroup) null);
                holder.indexTV = (TextView) convertView.findViewById(R.id.TextView_top10_index);
                holder.nameTV = (TextView) convertView.findViewById(R.id.TextView_top10_name);
                holder.scoreTV = (TextView) convertView.findViewById(R.id.TextView_top10_score);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            convertView.setTag(holder);
            holder.indexTV.setText(String.valueOf(position + 1));
            holder.nameTV.setText(d.getName());
            holder.scoreTV.setText(String.valueOf(d.getScore()));
            return convertView;
        }
    }

    private static class ViewHolder {
        TextView indexTV;
        TextView nameTV;
        TextView scoreTV;

        private ViewHolder() {
        }
    }
}
