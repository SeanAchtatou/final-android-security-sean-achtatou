package com.finger2finger.games.common;

import android.content.Intent;
import com.finger2finger.games.common.activity.F2FGameActivity;
import com.finger2finger.games.common.base.BaseSprite;
import com.finger2finger.games.common.res.CommonResource;
import com.finger2finger.games.common.store.activity.ShopActivity;
import com.finger2finger.games.res.Const;
import com.finger2finger.games.res.PortConst;
import com.finger2finger.games.res.Resource;
import com.finger2finger.games.scene.GameScene;
import org.anddev.andengine.audio.sound.Sound;
import org.anddev.andengine.engine.camera.hud.HUD;
import org.anddev.andengine.entity.shape.modifier.ScaleModifier;
import org.anddev.andengine.entity.shape.modifier.SequenceShapeModifier;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.ChangeableText;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.texture.region.TextureRegion;

public class HudMenu {
    private String LEVEL_LABEL_FORMART = "%d - %d";
    private String REMAIN_BALL_FORMART = Const.X_SCORE_FORMAT;
    private String REMAIN_BALL_NO_FORMART = "%d";
    private String TIME_FORMART = "%02d : %02d";
    private long gameupdateTime;
    public ChangeableText lifeText;
    /* access modifiers changed from: private */
    public F2FGameActivity mContext;
    public boolean mEnableLife = true;
    public boolean mEnablePause = true;
    public boolean mEnableRemainTime = true;
    public boolean mEnableXLife = true;
    private Font mFontHud;
    public HUD mHud;
    public ChangeableText mLevelLabel;
    /* access modifiers changed from: private */
    public GameScene mScene;
    public Sprite mSpriteLifeBar;
    public BaseSprite mSpritePause;
    public Sprite mSpriteScoreBar;
    public Sprite mSpriteTimeLevelBar;
    public int remainLife = 0;
    public int remainTime = 0;
    public ChangeableText remainTimeText;
    public ChangeableText scoreText;
    public BaseSprite shopSprite;

    public HudMenu(F2FGameActivity pContext, Font pFont, GameScene pScene, int pInitialTime, int pInitialLife) {
        this.mContext = pContext;
        this.mFontHud = pFont;
        this.mScene = pScene;
        this.remainTime = pInitialTime;
        this.remainLife = pInitialLife;
    }

    public void createHud() {
        TextureRegion mTRLife = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.HUD_LIFE.mKey);
        TextureRegion mTRScore = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.HUD_SCORE.mKey);
        TextureRegion mTRTimeLevel = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.HUD_TIME_LEVEL.mKey);
        TextureRegion mTRPause = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.BUTTON_PAUSE.mKey);
        this.mHud = new HUD(2);
        if (this.mEnableRemainTime) {
            this.mSpriteTimeLevelBar = new Sprite(((float) CommonConst.CAMERA_WIDTH) - (((float) mTRTimeLevel.getWidth()) * CommonConst.RALE_SAMALL_VALUE), 0.0f, ((float) mTRTimeLevel.getWidth()) * CommonConst.RALE_SAMALL_VALUE, ((float) mTRTimeLevel.getHeight()) * CommonConst.RALE_SAMALL_VALUE, mTRTimeLevel);
            this.mHud.getLayer(0).addEntity(this.mSpriteTimeLevelBar);
            this.remainTimeText = new ChangeableText(this.mSpriteTimeLevelBar.getX() + (60.0f * CommonConst.RALE_SAMALL_VALUE), this.mSpriteTimeLevelBar.getY() + (12.0f * CommonConst.RALE_SAMALL_VALUE), this.mFontHud, getTimeFormart(this.remainTime), 7);
            this.mLevelLabel = new ChangeableText(this.mSpriteTimeLevelBar.getX() + (72.0f * CommonConst.RALE_SAMALL_VALUE), this.mSpriteTimeLevelBar.getY() + (40.0f * CommonConst.RALE_SAMALL_VALUE), this.mFontHud, String.format(this.LEVEL_LABEL_FORMART, Integer.valueOf(this.mContext.getMGameInfo().getMLevelIndex() + 1), Integer.valueOf(this.mContext.getMGameInfo().getMInsideIndex() + 1)), 9);
            this.mHud.getLayer(1).addEntity(this.remainTimeText);
            this.mHud.getLayer(1).addEntity(this.mLevelLabel);
        } else {
            this.mLevelLabel = new ChangeableText(0.0f, 0.0f, this.mFontHud, String.format(this.LEVEL_LABEL_FORMART, Integer.valueOf(this.mContext.getMGameInfo().getMLevelIndex() + 1), Integer.valueOf(this.mContext.getMGameInfo().getMInsideIndex() + 1)), 9);
            this.mLevelLabel.setPosition(((float) CommonConst.CAMERA_WIDTH) - (this.mLevelLabel.getWidth() * 1.2f), this.mLevelLabel.getHeight() * 0.5f);
            this.mHud.getLayer(1).addEntity(this.mLevelLabel);
        }
        float px = 0.0f + (10.0f * CommonConst.RALE_SAMALL_VALUE);
        float py = 0.0f + (10.0f * CommonConst.RALE_SAMALL_VALUE);
        this.mSpriteScoreBar = new Sprite(px, py, ((float) mTRScore.getWidth()) * CommonConst.RALE_SAMALL_VALUE, ((float) mTRScore.getHeight()) * CommonConst.RALE_SAMALL_VALUE, mTRScore);
        this.mHud.getLayer(0).addEntity(this.mSpriteScoreBar);
        this.scoreText = new ChangeableText(this.mSpriteScoreBar.getX() + (70.0f * CommonConst.RALE_SAMALL_VALUE), this.mSpriteScoreBar.getY() + (12.0f * CommonConst.RALE_SAMALL_VALUE), this.mFontHud, getScoreFormartString(0), 4);
        this.mHud.getLayer(1).addEntity(this.scoreText);
        if (this.mEnableLife) {
            this.mSpriteLifeBar = new Sprite(px + this.mSpriteScoreBar.getWidth() + (10.0f * CommonConst.RALE_SAMALL_VALUE), py, ((float) mTRLife.getWidth()) * CommonConst.RALE_SAMALL_VALUE, ((float) mTRLife.getHeight()) * CommonConst.RALE_SAMALL_VALUE, mTRLife);
            this.mHud.getLayer(0).addEntity(this.mSpriteLifeBar);
            this.lifeText = new ChangeableText(this.mSpriteLifeBar.getX() + (70.0f * CommonConst.RALE_SAMALL_VALUE), this.mSpriteLifeBar.getY() + (12.0f * CommonConst.RALE_SAMALL_VALUE), this.mFontHud, getLifeFormartString(this.remainLife), 5);
            this.mHud.getLayer(1).addEntity(this.lifeText);
        }
        if (PortConst.enableStoreMode) {
            TextureRegion shop = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.SHOP_ICON.mKey);
            this.shopSprite = new BaseSprite(((float) shop.getWidth()) * 0.2f * CommonConst.RALE_SAMALL_VALUE, ((float) CommonConst.CAMERA_HEIGHT) - ((((float) shop.getHeight()) * 1.2f) * CommonConst.RALE_SAMALL_VALUE), CommonConst.RALE_SAMALL_VALUE, shop, false) {
                public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                    if (pSceneTouchEvent.getAction() == 0) {
                        if (HudMenu.this.mScene.hasChildScene()) {
                            return true;
                        }
                        HudMenu.this.mContext.startActivity(new Intent(HudMenu.this.mContext, ShopActivity.class));
                        GoogleAnalyticsUtils.setTracker("/GoShop_FromGameScene");
                    }
                    return true;
                }
            };
            this.mHud.registerTouchArea(this.shopSprite);
            this.mHud.getLayer(1).addEntity(this.shopSprite);
        }
        if (this.mEnablePause) {
            this.mSpritePause = new BaseSprite(((float) CommonConst.CAMERA_WIDTH) - ((((float) mTRPause.getWidth()) * 1.2f) * CommonConst.RALE_SAMALL_VALUE), ((float) CommonConst.CAMERA_HEIGHT) - ((((float) mTRPause.getHeight()) * 1.2f) * CommonConst.RALE_SAMALL_VALUE), CommonConst.RALE_SAMALL_VALUE, mTRPause, false) {
                public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                    if (pSceneTouchEvent.getAction() == 1) {
                        HudMenu.this.pauseSpriteUp();
                    }
                    if (pSceneTouchEvent.getAction() == 0) {
                        if (HudMenu.this.mScene.hasChildScene()) {
                            return true;
                        }
                        HudMenu.this.mScene.showContextMenu();
                    }
                    return true;
                }
            };
            this.mHud.getLayer(1).addEntity(this.mSpritePause);
            this.mHud.registerTouchArea(this.mSpritePause);
        }
        this.mContext.getEngine().getCamera().setHUD(this.mHud);
    }

    public void setRemainTime(int pValue) {
        if (this.mEnableRemainTime) {
            this.remainTime = pValue;
            this.remainTimeText.setText(getTimeFormart(pValue));
        }
    }

    public void setTimeBonusEffect() {
        if (this.mEnableRemainTime) {
            this.remainTimeText.addShapeModifier(new SequenceShapeModifier(new ScaleModifier(0.3f, 1.0f, 1.5f), new ScaleModifier(0.3f, 1.5f, 1.0f)));
        }
    }

    public void setRemainLife(int pValue) {
        if (this.mEnableLife) {
            this.lifeText.setText(getLifeFormartString(pValue));
        }
    }

    public void setScore(int pValue) {
        this.scoreText.setText(getScoreFormartString(pValue));
    }

    private String getTimeFormart(int pTime) {
        return String.format(this.TIME_FORMART, Integer.valueOf(pTime / 60), Integer.valueOf(pTime % 60));
    }

    private String getScoreFormartString(int pValue) {
        String str = String.valueOf(pValue);
        int length = str.length();
        for (int i = 0; i < 4 - length; i++) {
            str = "0" + str;
        }
        return str;
    }

    private String getLifeFormartString(int pValue) {
        if (this.mEnableXLife) {
            return String.format(this.REMAIN_BALL_FORMART, Integer.valueOf(pValue));
        }
        return String.format(this.REMAIN_BALL_NO_FORMART, Integer.valueOf(pValue));
    }

    public void destory() {
        if (this.mSpritePause != null) {
            this.mHud.unregisterTouchArea(this.mSpritePause);
        }
        if (this.shopSprite != null) {
            this.mHud.unregisterTouchArea(this.shopSprite);
        }
    }

    public void updateRemainTime() {
        if (!this.mScene.isGameOver && this.mEnableRemainTime && System.currentTimeMillis() - this.gameupdateTime >= 1000) {
            this.gameupdateTime = System.currentTimeMillis();
            this.remainTime--;
            if (this.remainTime >= 0) {
                if (this.remainTime <= 10 && !this.mScene.isTimeLimitedPlaying) {
                    Sound timeLimited = this.mContext.mResource.getSoundByKey(Resource.SOUNDTURE.SOUND_LIMITED_TIME.mKey);
                    if (CommonConst.GAME_MUSIC_ON && this.mContext.mResource != null) {
                        this.mScene.isTimeLimitedPlaying = true;
                        timeLimited.setLooping(true);
                        this.mContext.mResource.playSound(Resource.SOUNDTURE.SOUND_LIMITED_TIME);
                    }
                }
                this.remainTimeText.setText(getTimeFormart(this.remainTime));
                return;
            }
            if (CommonConst.GAME_MUSIC_ON) {
                this.mContext.mResource.pauseSound(Resource.SOUNDTURE.SOUND_LIMITED_TIME);
                this.mScene.isTimeLimitedPlaying = false;
            }
            GoogleAnalyticsUtils.setTracker("play_end_" + String.valueOf(this.mContext.getMGameInfo().getMLevelIndex()) + "_" + String.valueOf(this.mContext.getMGameInfo().getMInsideIndex() + "/failure/time"));
            GameScene gameScene = this.mScene;
            this.mScene.getClass();
            gameScene.setGameOver(0);
        }
    }

    public void pauseSpriteUp() {
        this.mScene.setActionUpOperation();
    }

    public void setRemaidTimePosition(float pX, float pY) {
        this.mHud.getLayer(0).addEntity(this.mSpriteTimeLevelBar);
        this.mSpriteTimeLevelBar.setPosition(pX, pY);
        this.remainTimeText.setPosition(this.mSpriteTimeLevelBar.getX() + (52.0f * CommonConst.RALE_SAMALL_VALUE), this.mSpriteTimeLevelBar.getY() + (13.5f * CommonConst.RALE_SAMALL_VALUE));
        this.mLevelLabel.setPosition(this.mSpriteTimeLevelBar.getX() + (64.5f * CommonConst.RALE_SAMALL_VALUE), this.mSpriteTimeLevelBar.getY() + (40.0f * CommonConst.RALE_SAMALL_VALUE));
    }

    public void setpausePosition(boolean pIsBottomLeft) {
        if (pIsBottomLeft) {
            this.mSpritePause.setPosition(10.0f * CommonConst.RALE_SAMALL_VALUE, ((float) CommonConst.CAMERA_HEIGHT) - this.mSpritePause.getHeight());
            if (PortConst.enableStoreMode) {
                this.shopSprite.setPosition(this.mSpritePause.getX() + this.mSpritePause.getWidth() + (5.0f * CommonConst.RALE_SAMALL_VALUE), this.mSpritePause.getY());
            }
        }
    }

    public void disShowLife(boolean pIsShow) {
        if (this.mEnableLife) {
            this.mSpriteLifeBar.setVisible(pIsShow);
            this.lifeText.setVisible(pIsShow);
        }
    }

    public void disShowScore(boolean pIsShow) {
        this.mSpriteScoreBar.setVisible(pIsShow);
        this.scoreText.setVisible(pIsShow);
    }
}
