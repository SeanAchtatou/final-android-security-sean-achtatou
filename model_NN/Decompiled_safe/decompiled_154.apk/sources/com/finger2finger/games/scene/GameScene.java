package com.finger2finger.games.scene;

import android.content.res.Resources;
import android.util.Log;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.f2fgames.games.monkeybreak.lite.R;
import com.finger2finger.games.common.CommonConst;
import com.finger2finger.games.common.GoogleAnalyticsUtils;
import com.finger2finger.games.common.RemovedObject;
import com.finger2finger.games.common.activity.F2FGameActivity;
import com.finger2finger.games.common.scene.ContextMenuScene;
import com.finger2finger.games.common.scene.F2FScene;
import com.finger2finger.games.common.scene.dialog.DynamicDialog;
import com.finger2finger.games.common.scene.dialog.F2FRecommendationDialog;
import com.finger2finger.games.common.scene.dialog.GameOverDialog;
import com.finger2finger.games.common.scene.dialog.ImageDialog;
import com.finger2finger.games.common.scene.dialog.TextDialog;
import com.finger2finger.games.common.store.data.PersonalAccount;
import com.finger2finger.games.common.store.data.PersonalAccountTable;
import com.finger2finger.games.entity.Ball;
import com.finger2finger.games.entity.Brick;
import com.finger2finger.games.entity.Prop;
import com.finger2finger.games.res.Const;
import com.finger2finger.games.res.PortConst;
import com.finger2finger.games.res.Resource;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import net.youmi.android.AdView;
import org.anddev.andengine.engine.camera.BoundCamera;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.camera.SmoothCamera;
import org.anddev.andengine.engine.camera.hud.HUD;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXLayer;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXObject;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXObjectGroup;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXObjectProperty;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXProperties;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXTile;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXTileProperty;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXTiledMap;
import org.anddev.andengine.entity.primitive.Line;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.entity.shape.Shape;
import org.anddev.andengine.entity.shape.modifier.AlphaModifier;
import org.anddev.andengine.entity.shape.modifier.IShapeModifier;
import org.anddev.andengine.entity.shape.modifier.LoopShapeModifier;
import org.anddev.andengine.entity.shape.modifier.MoveModifier;
import org.anddev.andengine.entity.shape.modifier.ParallelShapeModifier;
import org.anddev.andengine.entity.shape.modifier.RotationModifier;
import org.anddev.andengine.entity.shape.modifier.ScaleModifier;
import org.anddev.andengine.entity.shape.modifier.SequenceShapeModifier;
import org.anddev.andengine.entity.sprite.AnimatedSprite;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.ChangeableText;
import org.anddev.andengine.extension.physics.box2d.FixedStepPhysicsWorld;
import org.anddev.andengine.extension.physics.box2d.PhysicsConnector;
import org.anddev.andengine.extension.physics.box2d.PhysicsFactory;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;
import org.anddev.andengine.sensor.accelerometer.AccelerometerData;
import org.anddev.andengine.util.MathUtils;
import org.anddev.andengine.util.modifier.IModifier;

public class GameScene extends F2FScene {
    private String LEVEL_LABEL_FORMART = "%d - %d";
    private int Layer_Hud1 = 0;
    private int Layer_Hud2 = 1;
    private int Layer_One = 1;
    private int Layer_Three = 3;
    /* access modifiers changed from: private */
    public int Layer_Two = 2;
    /* access modifiers changed from: private */
    public float MAX_SCALE = 1.1f;
    /* access modifiers changed from: private */
    public float MIN_SCALE = 0.8f;
    private long PASSLEVEL_DELAY = 1000;
    private int RANDOM_SPEED_X = 3;
    private int RANDOM_SPEED_Y = 3;
    private String REMAIN_BALL_FORMART = Const.X_SCORE_FORMAT;
    /* access modifiers changed from: private */
    public float SCALE_STEP = 0.1f;
    private float cameraHeight;
    private float cameraWidth;
    private String captionValue;
    private boolean enableArrow = false;
    private float initialCrabPositionX = 0.0f;
    private float initialCrabPositionY = 0.0f;
    private int insideIndex = 0;
    private boolean isBackToMode = false;
    private boolean isBallDead = false;
    public boolean isCheckGetProp = false;
    private boolean isChildSceneClick = false;
    private boolean isCreateBallActive = false;
    /* access modifiers changed from: private */
    public boolean isEnableCrab = true;
    private boolean isFirstBall = true;
    private boolean isFirstCheckPass = true;
    public boolean isFirstPlayGame = true;
    public boolean isFirstUpdateBack = true;
    public boolean isGameOver = false;
    private boolean isJoint = false;
    private boolean isNeedUpdateChaseSprite = false;
    private boolean isNeedUpdateClear = false;
    /* access modifiers changed from: private */
    public boolean isNeedUpdateRemove = false;
    boolean isNextLevel = false;
    private boolean isOver = false;
    /* access modifiers changed from: private */
    public boolean isPassLevel = false;
    private boolean isPropSpeed = false;
    private boolean isPropSpeedFirst = true;
    public boolean isRemainTimeFirst = true;
    boolean isReplay = false;
    private boolean isScaleFromRealSize = false;
    public boolean isTimeLimitedPlaying = false;
    public boolean isUpdate = true;
    private int level = 0;
    private int mBallCount = 3;
    private float mBalljetSpeed = Const.BallJetSpeed;
    private Body mBodyBottomLine;
    private Body mBodyCrab;
    private Body mBodyLeftLine;
    private Body mBodyRightLine;
    private Body mBodyTopLine;
    private Line mBottomLine;
    private float mCenterJetBall_X = 0.0f;
    private float mCenterJetBall_Y = 0.0f;
    private IShape mChaseShape = null;
    private int mCheckPassRemain = 0;
    private ContextMenuScene mContextMenuScene;
    private TextureRegion mDialogBtn1;
    private TextureRegion mDialogBtn2;
    private ArrayList<DynamicDialog> mDynamicDialog = new ArrayList<>();
    private Font mFontCaption;
    private Font mFontDialog;
    private Font mFontHud;
    private Font mFontScoreHint;
    private Font mFontTextDialogTitle;
    private long mGameOverUpdate = 0;
    private HUD mHud;
    private boolean mIsPallAll;
    private boolean mIsSucced;
    private float[] mJetRangle;
    private Line mLeftLine;
    private boolean mMenuBackToUp = false;
    private Sprite mOriginalChaseShape;
    private long mPassLevelDelay = 0;
    private FixedStepPhysicsWorld mPhysicsWorld;
    private int mPropBallPowerCount = 3;
    private int mPropCrabScaleCount = 3;
    private long mPropDuration = 0;
    private int mPropGoldCount = 5;
    private int mPropLifeCount = 3;
    HashMap<String, PropProperty> mPropProperty = new HashMap<>();
    private int mPropSpeedDownCount = 3;
    private int mPropSpeedTimes = 0;
    private int mPropSpeedUpCount = 3;
    private float mRegulateVelocity = 0.5f;
    private ChangeableText mRemainBallText;
    private Line mRightLine;
    private float mSceneSizeScale = 1.0f;
    /* access modifiers changed from: private */
    public int mScore = 0;
    private int mScoreValue = 0;
    private AnimatedSprite mSpriteArrow;
    private ArrayList<Ball> mSpriteBalls = new ArrayList<>();
    private ArrayList<AnimatedSprite> mSpriteClear = new ArrayList<>();
    private Crab mSpriteCrab;
    private ArrayList<AnimatedSprite> mSpriteExplosion = new ArrayList<>();
    /* access modifiers changed from: private */
    public ArrayList<Prop> mSpriteProp = new ArrayList<>();
    private int[] mStarLevel = new int[CommonConst.STAR_NUM];
    private ChildSceneStatus mStatus = ChildSceneStatus.UNDIFINE;
    private String mStrBallDead;
    private String mStrDialogScore;
    private String mStrGameOver;
    private String mStrTileDynamicDialog;
    private String mStrTitleRules;
    private String mStrTitleTips;
    private TMXTiledMap mTMXTiledMap;
    private TextureRegion mTRAccSpeed;
    private TextureRegion mTRAddBall;
    private TiledTextureRegion mTRArrow;
    private TextureRegion mTRBallPowerBar;
    private TextureRegion[] mTRBalls = new TextureRegion[1];
    private TextureRegion mTRCrab;
    private TextureRegion mTRDecSpeed;
    private TextureRegion mTREnlarge;
    private TextureRegion[] mTRGolds = new TextureRegion[3];
    private TextureRegion mTRMenu;
    private TextureRegion mTRRelay;
    private TextureRegion mTRScoreBg;
    private TiledTextureRegion[] mTTRBricks;
    private TiledTextureRegion mTTRClear;
    private TiledTextureRegion mTTRExplosion;
    private TiledTextureRegion mTTRPowerStone;
    private String[] mText;
    private Font[] mTextFont;
    private float mTiledMapScale = 1.0f;
    private String mTitle;
    private Font mTitleFont;
    private Line mTopLine;
    private int mTotalScore = 0;
    private float mUnderLineY = 0.0f;
    private int mValuableRemain = 0;
    private int mValuableTotal = 0;
    private Brick[][] mWallEntity;
    float realRangle = ((float) Math.atan2(1.0d, 1.0d));
    /* access modifiers changed from: private */
    public Queue<RemovedObject> removedSpriteList = new LinkedList();
    float[] sceneTouchdataArrow = new float[3];
    float[] sceneTouchdataDownMove = new float[3];
    float[] sceneTouchdataUp = new float[3];
    /* access modifiers changed from: private */
    public ChangeableText scoreText;
    private int subLevel = 0;
    private TMXLayer tmxLayer;
    float xLastSpeed = -1.0f;
    float xUnitSpeed = Math.abs((float) Math.sin((double) this.realRangle));
    float yLastSpeed = -1.0f;
    float yUnitSpeed = Math.abs((float) Math.cos((double) this.realRangle));

    public enum ChildSceneStatus {
        UNDIFINE,
        GAME_HELP_DIALOG,
        GAME_START_DIALOG,
        GAME_OVER_FAIL,
        GAME_OVER_OK,
        GAME_NEXLLEVEL,
        GAME_PASSALLLEVEL,
        GAME_PASSALLLEVEL_LITE
    }

    static /* synthetic */ int access$1812(GameScene x0, int x1) {
        int i = x0.mScore + x1;
        x0.mScore = i;
        return i;
    }

    public boolean isChildSceneClick() {
        return this.isChildSceneClick;
    }

    public void setChildSceneClick(boolean isChildSceneClick2) {
        this.isChildSceneClick = isChildSceneClick2;
    }

    public GameScene(F2FGameActivity pContext) {
        super(4, pContext);
        enableMultiTouch();
        enableSceneTouch();
        enableAccelerometerSensor();
        this.level = this.mContext.getMGameInfo().getMLevelIndex();
        this.subLevel = this.mContext.getMGameInfo().getMSubLevelIndex();
        this.insideIndex = this.mContext.getMGameInfo().getMInsideIndex();
        loadResources();
        startGame();
    }

    public void loadScene() {
    }

    private void loadResources() {
        loadSceneSize();
        loadTextures();
        loadFont();
        loadStringsFromXml();
        loadTmxTiledMap();
        loadHelpPreferences();
        Const.Type.loadScore();
    }

    private void loadSceneSize() {
        if (this.isScaleFromRealSize) {
            this.mTiledMapScale = CommonConst.RALE_SAMALL_VALUE;
        } else if (CommonConst.CAMERA_WIDTH <= CommonConst.CAMERA_MAX_WIDTH || CommonConst.CAMERA_HEIGHT <= CommonConst.CAMERA_MAX_HEIGHT) {
            this.mTiledMapScale = CommonConst.RALE_SAMALL_VALUE;
        } else {
            this.mTiledMapScale = 1.0f;
        }
        this.mSceneSizeScale = ((float) CommonConst.CAMERA_WIDTH) / ((float) CommonConst.CAMERA_MAX_WIDTH);
        this.cameraWidth = this.mContext.getEngine().getCamera().getWidth();
        this.cameraHeight = this.mContext.getEngine().getCamera().getHeight();
    }

    public void loadHelpPreferences() {
        this.isFirstPlayGame = CommonConst.GAME_HELP_SHOW;
        if (this.isFirstPlayGame) {
            CommonConst.GAME_HELP_SHOW = false;
        }
        this.mContext.setGameInfo();
    }

    private void loadStringsFromXml() {
        Resources res = this.mContext.getResources();
        this.captionValue = res.getString(R.string.str_caption_start);
        this.mStrBallDead = res.getString(R.string.str_ball_dead_hint);
        this.mStrTitleRules = res.getString(R.string.game_title_rules);
        this.mStrTileDynamicDialog = res.getString(R.string.str_tips);
    }

    private void loadTextures() {
        this.mTRCrab = this.mContext.mResource.getTextureRegionByKey(Resource.TEXTURE.GAME_CRAB.mKey);
        this.mTTRBricks = this.mContext.mResource.getArrayTiledTextureRegionByKey(Resource.TILEDTURE.GAME_BRICKS.mKey);
        this.mTTRPowerStone = this.mContext.mResource.getTiledTextureRegionByKey(Resource.TILEDTURE.GAME_POWERSTONE.mKey);
        this.mTTRExplosion = this.mContext.mResource.getTiledTextureRegionByKey(Resource.TILEDTURE.GAME_EXPLOSION.mKey);
        this.mTTRClear = this.mContext.mResource.getTiledTextureRegionByKey(Resource.TILEDTURE.GAME_CLEAR.mKey);
        this.mTRBalls = this.mContext.mResource.getArrayTextureRegionByKey(Resource.TEXTURE.GAME_BALL.mKey);
        this.mTREnlarge = this.mContext.mResource.getTextureRegionByKey(Resource.TEXTURE.GAME_CRAB_ENLARGE.mKey);
        this.mTRAddBall = this.mContext.mResource.getTextureRegionByKey(Resource.TEXTURE.GAME_CREATEBALL.mKey);
        this.mTRAccSpeed = this.mContext.mResource.getTextureRegionByKey(Resource.TEXTURE.GAME_CRAB_ACC.mKey);
        this.mTRDecSpeed = this.mContext.mResource.getTextureRegionByKey(Resource.TEXTURE.GAME_CRAB_DEC.mKey);
        this.mTRGolds = this.mContext.mResource.getArrayTextureRegionByKey(Resource.TEXTURE.GAME_GOLD.mKey);
        this.mTRBallPowerBar = this.mContext.mResource.getTextureRegionByKey(Resource.TEXTURE.BALL_POWER_BAR.mKey);
        this.mTRArrow = this.mContext.mResource.getTiledTextureRegionByKey(Resource.TILEDTURE.GAME_ARROW.mKey);
        this.mTRScoreBg = this.mContext.mResource.getTextureRegionByKey(Resource.TEXTURE.GAME_SCORE.mKey);
    }

    private void loadTmxTiledMap() {
        this.mContext.mResource.loadTmxTiledMap();
        this.mTMXTiledMap = this.mContext.mResource.getTmxTiledMapByKey(Resource.TILEDMAP.TILEDMAP.mKey);
        this.tmxLayer = this.mTMXTiledMap.getTMXLayers().get(1);
        CommonConst.MAP_ARRAY_Y = this.mTMXTiledMap.getTileRows();
        CommonConst.MAP_ARRAY_X = this.mTMXTiledMap.getTileColumns();
        CommonConst.MAP_TILE_WIDTH = ((float) this.mTMXTiledMap.getTileWidth()) * this.mTiledMapScale;
        CommonConst.MAP_TILE_HEIGHT = ((float) this.mTMXTiledMap.getTileHeight()) * this.mTiledMapScale;
        CommonConst.MAP_WIDTH = this.tmxLayer.getWidth() * this.mTiledMapScale;
        CommonConst.MAP_HEIGHT = this.tmxLayer.getHeight() * this.mTiledMapScale;
    }

    private void startGame() {
        initializeBoundCamera();
        initializeHudScene();
        initializeTileMap();
        initializeBorderLine();
        initializeOriginalChaseShape();
        initializeArrow();
        initializeAllProp();
        if (this.isFirstPlayGame) {
            showHelpDialog();
        } else {
            showStartHint();
        }
        this.mContext.getEngine().enableVibrator(this.mContext);
        registerUpdateHandler(this.mPhysicsWorld);
        registerUpdateHandler(new IUpdateHandler() {
            public void onUpdate(float pSecondsElapsed) {
                if (!GameScene.this.isGameOver) {
                    GameScene.this.checkPropOnAreaTouch();
                    GameScene.this.checkPropRemove();
                    GameScene.this.checkPass();
                    if (!GameScene.this.isPassLevel) {
                        GameScene.this.checkWallBreak();
                        GameScene.this.checkBallDead();
                        GameScene.this.checkEntityCount();
                        GameScene.this.createInitialBall();
                        GameScene.this.checkPropSpeed();
                        GameScene.this.checkDynamicDialog();
                        GameScene.this.updateSpriteClear();
                        GameScene.this.updateChaseShape();
                        GameScene.this.doRemoveEntity();
                        GameScene.this.onUpdateSceneTouchEvent();
                        GameScene.this.onUpdateSceneTouchEvent_Arrow();
                    }
                } else if (GameScene.this.isUpdate) {
                    GameScene.this.checkBackToUpScene();
                }
            }

            public void reset() {
            }
        });
    }

    private void destroyAllEntity() {
        if (this.mSpriteCrab != null) {
            if (this.mBodyCrab != null) {
                this.mPhysicsWorld.destroyBody(this.mBodyCrab);
            }
            this.mSpriteCrab.clearShapeModifiers();
        }
        for (int i = 0; i < CommonConst.MAP_ARRAY_Y; i++) {
            for (int j = 0; j < CommonConst.MAP_ARRAY_X; j++) {
                if (this.mWallEntity[i][j] != null && !this.mWallEntity[i][j].isRemove) {
                    this.mWallEntity[i][j].setEnable(false);
                }
            }
        }
        for (int i2 = 0; i2 < this.mSpriteBalls.size(); i2++) {
            Ball ball = this.mSpriteBalls.get(i2);
            if (ball != null && !ball.isRemove) {
                ball.destroy();
            }
        }
        for (int i3 = 0; i3 < this.mSpriteProp.size(); i3++) {
            Prop prop = this.mSpriteProp.get(i3);
            if (prop != null && prop.isEnable) {
                prop.destroy();
            }
        }
        getLayer(this.Layer_One).clear();
        getLayer(this.Layer_Two).clear();
        getLayer(this.Layer_Three).clear();
    }

    /* access modifiers changed from: private */
    public void doRemoveEntity() {
        if (this.isNeedUpdateRemove) {
            synchronized (this.removedSpriteList) {
                RemovedObject rs = this.removedSpriteList.poll();
                if (rs != null) {
                    getLayer(rs._layer).removeEntity(rs._sprite);
                }
            }
            this.isNeedUpdateRemove = false;
        }
    }

    private void createTextAddScore(int px, int py, int pScore) {
        float pX = (((float) py) * CommonConst.MAP_TILE_WIDTH) + (CommonConst.MAP_TILE_WIDTH / 2.0f);
        float pY = (((float) px) * CommonConst.MAP_TILE_HEIGHT) + (CommonConst.MAP_TILE_HEIGHT / 2.0f);
        float cameraMinX = this.mContext.getEngine().getCamera().getMinX();
        float cameraMinY = this.mContext.getEngine().getCamera().getMinY();
        float targetX = cameraMinX + (60.0f * this.mTiledMapScale);
        float targetY = cameraMinY + (30.0f * this.mTiledMapScale);
        float moveTime = Math.max(Math.abs(pY - targetY) / Const.MAP_TILE_HEIGHT, Math.abs(pX - targetX) / Const.MAP_TILE_WIDTH) * 0.1f;
        ChangeableText mGetScore = new ChangeableText(pX, pY, this.mFontScoreHint, String.format(Const.ADD_SCORE_FORMAT, Integer.valueOf(pScore)), String.format(Const.ADD_SCORE_FORMAT, Integer.valueOf(pScore)).length());
        mGetScore.addShapeModifier(new SequenceShapeModifier(new IShapeModifier.IShapeModifierListener() {
            public /* bridge */ /* synthetic */ void onModifierFinished(IModifier x0, Object x1) {
                onModifierFinished((IModifier<IShape>) x0, (IShape) x1);
            }

            public void onModifierFinished(IModifier<IShape> iModifier, IShape arg1) {
                ChangeableText ct = (ChangeableText) arg1;
                ct.setVisible(false);
                synchronized (GameScene.this.removedSpriteList) {
                    GameScene.this.removedSpriteList.offer(new RemovedObject(ct, GameScene.this.Layer_Two));
                }
                GameScene.this.setScore(GameScene.this.mScore);
                boolean unused = GameScene.this.isNeedUpdateRemove = true;
            }
        }, new MoveModifier(moveTime, pX, targetX, pY, targetY)));
        getLayer(this.Layer_Two).addEntity(mGetScore);
    }

    private void initializeOriginalChaseShape() {
        this.mOriginalChaseShape = new Sprite(0.0f, 0.0f, this.mContext.mResource.getTextureRegionByKey(Resource.TEXTURE.GAME_CRAB.mKey));
        this.mOriginalChaseShape.setVisible(false);
        getTopLayer().addEntity(this.mOriginalChaseShape);
    }

    /* access modifiers changed from: private */
    public void checkPass() {
        if (this.isPassLevel) {
            if (this.isFirstCheckPass) {
                this.mCheckPassRemain = 2;
                this.mPassLevelDelay = System.currentTimeMillis();
                this.isFirstCheckPass = false;
            }
            if (System.currentTimeMillis() - this.mPassLevelDelay >= 1000) {
                this.mPassLevelDelay = System.currentTimeMillis();
                this.mCheckPassRemain--;
            }
            if (this.mCheckPassRemain <= 0) {
                setPassLevel();
                this.isGameOver = true;
            }
        }
    }

    /* access modifiers changed from: private */
    public void checkBackToUpScene() {
        if (this.mMenuBackToUp) {
            if (this.isFirstUpdateBack) {
                ((SmoothCamera) this.mContext.getEngine().getCamera()).setChaseShape(this.mOriginalChaseShape);
                this.isFirstUpdateBack = false;
            }
            float cameraMinX = this.mContext.getEngine().getCamera().getMinX();
            float cameraMinY = this.mContext.getEngine().getCamera().getMinY();
            if (cameraMinX <= 0.0f && cameraMinY <= 0.0f) {
                destroyAllEntity();
                this.mMenuBackToUp = false;
                this.isUpdate = false;
                back();
            }
        }
    }

    private void resetAllProp() {
        if (this.mSpriteCrab != null) {
            this.mSpriteCrab.resetScale();
        }
        resetPropSpeed();
        for (int i = 0; i < this.mSpriteProp.size(); i++) {
            Prop prop = this.mSpriteProp.get(i);
            if (prop != null && prop.isEnable) {
                prop.destroy();
            }
        }
    }

    /* access modifiers changed from: private */
    public void createInitialBall() {
        if (!this.isJoint && this.mBodyCrab != null && !checkEffectiveBall() && this.mBallCount > 0) {
            resetAllProp();
            if (!this.isBallDead) {
                resetJointBall();
            } else if (this.isEnableCrab) {
                showBallDeadHint();
                this.isBallDead = false;
                resetJointBall();
            }
        }
    }

    private void resetJointBall() {
        this.isJoint = true;
        this.mSpriteCrab.clearShapeModifiers();
        this.mSpriteCrab.setAlpha(1.0f);
        this.mBodyCrab.setTransform(new Vector2(this.initialCrabPositionX, this.initialCrabPositionY), 0.0f);
        Ball ball = new Ball((this.initialCrabPositionX * 32.0f) - ((((float) this.mTRBalls[0].getWidth()) * this.mTiledMapScale) / 2.0f), (this.initialCrabPositionY * 32.0f) - ((this.mSpriteCrab.getHeight() * 4.0f) / 5.0f), this.mTRBalls[MathUtils.random(0, 0)].clone(), this, this.Layer_Three, this.mPhysicsWorld, this.mTiledMapScale);
        ball.createBall();
        ball.setVisible(true);
        ball.isBeJoint = true;
        ball.isEnable = true;
        if (this.mCenterJetBall_X == 0.0f && this.mCenterJetBall_Y == 0.0f) {
            this.mCenterJetBall_X = ball.mSpriteBall.getX();
            this.mCenterJetBall_Y = ball.mSpriteBall.getY();
        }
        this.mSpriteBalls.add(ball);
        plusBallCount();
    }

    private void checkInCamera(Shape pShape, float cameraMinX, float cameraMinY, float cameraWidth2, float cameraHeight2) {
        if (pShape.getX() + pShape.getWidth() < cameraMinX || pShape.getX() > cameraMinX + cameraWidth2 || pShape.getY() + pShape.getHeight() < cameraMinY || pShape.getY() > cameraMinY + cameraHeight2) {
            if (pShape.isVisible()) {
                pShape.setVisible(false);
            }
        } else if (!pShape.isVisible()) {
            pShape.setVisible(true);
        }
    }

    private void initializeBorderLine() {
        FixtureDef groundFixtureDef = PhysicsFactory.createFixtureDef(1.0f, 0.1f, 0.0f);
        this.mLeftLine = new Line(5.0f, 5.0f, 5.0f, this.mUnderLineY);
        this.mLeftLine.setLineWidth(5.0f);
        this.mBodyLeftLine = PhysicsFactory.createLineBody(this.mPhysicsWorld, this.mLeftLine, groundFixtureDef);
        this.mBottomLine = new Line(5.0f, this.mUnderLineY, CommonConst.MAP_WIDTH - 5.0f, this.mUnderLineY);
        this.mBottomLine.setLineWidth(5.0f);
        this.mBodyBottomLine = PhysicsFactory.createLineBody(this.mPhysicsWorld, this.mBottomLine, groundFixtureDef);
        this.mRightLine = new Line(CommonConst.MAP_WIDTH - 5.0f, this.mUnderLineY, CommonConst.MAP_WIDTH - 5.0f, 5.0f);
        this.mRightLine.setLineWidth(5.0f);
        this.mBodyRightLine = PhysicsFactory.createLineBody(this.mPhysicsWorld, this.mRightLine, groundFixtureDef);
        this.mTopLine = new Line(CommonConst.MAP_WIDTH - 5.0f, 5.0f, 5.0f, 5.0f);
        this.mTopLine.setLineWidth(5.0f);
        this.mBodyTopLine = PhysicsFactory.createLineBody(this.mPhysicsWorld, this.mTopLine, groundFixtureDef);
    }

    private boolean checkBodyCollide(Body body1, Body body2, Body targetBody1, Body targetBody2) {
        if ((!body1.equals(targetBody1) || !body2.equals(targetBody2)) && (!body2.equals(targetBody1) || !body1.equals(targetBody2))) {
            return false;
        }
        return true;
    }

    private void initializePhysicsWorld() {
        this.mPhysicsWorld = new FixedStepPhysicsWorld((int) Const.PhysicsWorld_StepsPerSecond, new Vector2(Const.PhysicsWorld_GravityX, Const.PhysicsWorld_GravityY), Const.PhysicsWorld_AllowSleep, (int) Const.PhysicsWorld_VelocityIterations, (int) Const.PhysicsWorld_PositionIterations);
        this.mPhysicsWorld.setContactListener(new ContactListener() {
            public void beginContact(Contact contact) {
            }

            public void endContact(Contact contact) {
                GameScene.this.checkContact(contact);
            }
        });
    }

    /* access modifiers changed from: private */
    public void checkContact(Contact contact) {
        Brick brick;
        Brick brick2;
        if (contact != null && contact.getFixtureA() != null && contact.getFixtureB() != null && contact.getFixtureA().getBody() != null && contact.getFixtureB().getBody() != null) {
            Body body1 = contact.getFixtureA().getBody();
            Body body2 = contact.getFixtureB().getBody();
            if (body1.getUserData() != null && (body1.getUserData() instanceof Ball)) {
                Ball ball = (Ball) body1.getUserData();
                if (body2.equals(this.mBodyBottomLine)) {
                    ball.isRemove = true;
                } else if (body2.equals(this.mBodyLeftLine) || body2.equals(this.mBodyRightLine) || body2.equals(this.mBodyTopLine) || body2.equals(this.mBodyCrab)) {
                    ball.isHitWall = true;
                    updateSpeed(ball, true);
                } else if ((body2.getUserData() instanceof Brick) && (brick2 = (Brick) body2.getUserData()) != null && !brick2.isRemove && !brick2.is_Collide()) {
                    updateSpeed(ball, true);
                    brick2.set_Collide(true);
                }
            } else if (body2.getUserData() != null && (body2.getUserData() instanceof Ball)) {
                Ball ball2 = (Ball) body2.getUserData();
                if (body1.equals(this.mBodyBottomLine)) {
                    ball2.isRemove = true;
                } else if (body1.equals(this.mBodyLeftLine) || body1.equals(this.mBodyRightLine) || body1.equals(this.mBodyTopLine) || body1.equals(this.mBodyCrab)) {
                    ball2.isHitWall = true;
                    updateSpeed(ball2, true);
                } else if ((body1.getUserData() instanceof Brick) && (brick = (Brick) body1.getUserData()) != null && !brick.isRemove && !brick.is_Collide()) {
                    updateSpeed(ball2, true);
                    brick.set_Collide(true);
                }
            }
        }
    }

    private void updateSpeed(Ball ball, boolean isRegulate) {
        if (ball != null) {
            ball.mBodyBall.setLinearVelocity(getVelocity(ball.mBodyBall.getLinearVelocity().x, ball.mBodyBall.getLinearVelocity().y, isRegulate));
        }
    }

    private void initializeArrow() {
        this.mSpriteArrow = new AnimatedSprite(0.0f, 0.0f, this.mTiledMapScale * ((float) this.mTRArrow.getTileWidth()), this.mTiledMapScale * ((float) this.mTRArrow.getTileHeight()), this.mTRArrow);
        this.mSpriteArrow.setVisible(false);
        getLayer(this.Layer_Three).addEntity(this.mSpriteArrow);
    }

    /* access modifiers changed from: private */
    public void disableArrow() {
        this.enableArrow = false;
        this.mSpriteArrow.stopAnimation();
        this.mSpriteArrow.setCurrentTileIndex(0);
        this.mSpriteArrow.setVisible(false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long[], int, int, boolean):org.anddev.andengine.entity.sprite.AnimatedSprite
     arg types: [long[], int, int, int]
     candidates:
      org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long[], int, int, int):org.anddev.andengine.entity.sprite.AnimatedSprite
      org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long[], int, int, boolean):org.anddev.andengine.entity.sprite.AnimatedSprite */
    public void updateArrow(float pX, float pY) {
        if (!checkEffectiveBall() && this.isEnableCrab) {
            if (!this.enableArrow) {
                this.enableArrow = true;
                this.mSpriteArrow.setVisible(true);
                this.mSpriteArrow.setPosition(this.mCenterJetBall_X + (((float) (this.mTRBalls[0].getWidth() / 2)) * this.mTiledMapScale), this.mCenterJetBall_Y + (((float) ((this.mTRBalls[0].getHeight() / 2) - (this.mTRArrow.getHeight() / 2))) * this.mTiledMapScale));
                this.mSpriteArrow.setRotationCenterX(0.0f);
                this.mSpriteArrow.setScaleCenterX(0.0f);
                this.mSpriteArrow.animate(new long[]{200, 200, 200}, 0, 2, true);
            }
            if (this.enableArrow) {
                float disX = pX - (this.mCenterJetBall_X + (((float) (this.mTRBalls[0].getWidth() / 2)) * this.mTiledMapScale));
                float disY = pY - (this.mCenterJetBall_Y + (((float) ((this.mTRBalls[0].getHeight() / 2) - (this.mTRArrow.getHeight() / 2))) * this.mTiledMapScale));
                float rangle = MathUtils.radToDeg((float) Math.atan2((double) disX, (double) (-disY)));
                float originalWidth = ((float) this.mTRArrow.getTileWidth()) * this.mTiledMapScale;
                if (Math.abs(disX) < Math.abs(disY)) {
                    this.mSpriteArrow.setScale(Math.abs((disY - (0.0f * this.mTiledMapScale)) / originalWidth), 1.0f);
                } else {
                    this.mSpriteArrow.setScale(Math.abs((disX - (0.0f * this.mTiledMapScale)) / originalWidth), 1.0f);
                }
                this.mSpriteArrow.setRotation(rangle - 90.0f);
            }
        }
    }

    public Vector2 getJetVelocity(float distance_x, float distance_y) {
        float realRangle2 = (float) Math.atan2((double) distance_x, (double) distance_y);
        float xSpeed = Math.abs((float) (Math.sin((double) realRangle2) * ((double) this.mBalljetSpeed)));
        float ySpeed = Math.abs((float) (Math.cos((double) realRangle2) * ((double) this.mBalljetSpeed)));
        if (distance_x <= 0.0f) {
            xSpeed = -xSpeed;
        }
        if (distance_y <= 0.0f) {
            ySpeed = -ySpeed;
        }
        return new Vector2(xSpeed, ySpeed);
    }

    public Vector2 getVelocity(float distance_x, float distance_y, boolean isRegulate) {
        float realRangle2 = (float) Math.atan2((double) distance_x, (double) distance_y);
        float xSpeed = Math.abs((float) (Math.sin((double) realRangle2) * ((double) this.mBalljetSpeed)));
        float ySpeed = Math.abs((float) (Math.cos((double) realRangle2) * ((double) this.mBalljetSpeed)));
        if (distance_x <= 0.0f) {
            xSpeed = -xSpeed;
        }
        if (distance_y <= 0.0f) {
            ySpeed = -ySpeed;
        }
        float xSpeed2 = xSpeed + (((float) ((MathUtils.random(10, 30) - 20) / 20)) * this.mTiledMapScale);
        float ySpeed2 = ySpeed + (((float) ((MathUtils.random(10, 30) - 20) / 20)) * this.mTiledMapScale);
        if (isRegulate) {
            if (Math.abs(ySpeed2) < this.mRegulateVelocity * this.mTiledMapScale) {
                ySpeed2 = this.yUnitSpeed * this.mBalljetSpeed;
                if (distance_y <= 0.0f) {
                    ySpeed2 = -ySpeed2;
                }
                if (this.mSpriteCrab.getX() < CommonConst.MAP_WIDTH / 2.0f) {
                    xSpeed2 = this.xUnitSpeed * this.mBalljetSpeed;
                } else {
                    xSpeed2 = (-this.xUnitSpeed) * this.mBalljetSpeed;
                }
            }
            if (Math.abs(xSpeed2) < this.mRegulateVelocity * this.mTiledMapScale) {
                xSpeed2 = this.xUnitSpeed * this.mBalljetSpeed;
                if (distance_y <= 0.0f) {
                    xSpeed2 = -xSpeed2;
                }
                if (this.mSpriteCrab.getY() < CommonConst.MAP_HEIGHT / 2.0f) {
                    ySpeed2 = this.yUnitSpeed * this.mBalljetSpeed;
                } else {
                    ySpeed2 = (-this.yUnitSpeed) * this.mBalljetSpeed;
                }
            }
        }
        return new Vector2(xSpeed2, ySpeed2);
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    /* access modifiers changed from: private */
    public void updateChaseShape() {
        if (this.isNeedUpdateChaseSprite) {
            int i = 0;
            while (i < this.mSpriteBalls.size()) {
                Ball ball = this.mSpriteBalls.get(i);
                if (ball == null || !ball.mSpriteBall.isVisible() || !ball.isEnable) {
                    i++;
                } else if (!ball.equals(this.mChaseShape)) {
                    this.mChaseShape = ball.mSpriteBall;
                    ((SmoothCamera) this.mContext.getEngine().getCamera()).setChaseShape(ball.mSpriteBall);
                    return;
                } else {
                    return;
                }
            }
            if (this.mSpriteCrab != this.mChaseShape) {
                this.mChaseShape = this.mSpriteCrab;
                ((SmoothCamera) this.mContext.getEngine().getCamera()).setChaseShape(this.mSpriteCrab);
            }
            this.isNeedUpdateChaseSprite = false;
        }
    }

    private Ball getChaseBall() {
        for (int i = 0; i < this.mSpriteBalls.size(); i++) {
            Ball ball = this.mSpriteBalls.get(i);
            if (ball != null && ball.mSpriteBall.isVisible() && ball.isEnable && ball.mSpriteBall.equals(this.mChaseShape)) {
                return ball;
            }
        }
        return null;
    }

    /* access modifiers changed from: private */
    public void checkDynamicDialog() {
        int k = 0;
        while (k < this.mSpriteBalls.size()) {
            Ball ball = this.mSpriteBalls.get(k);
            if (ball == null || !ball.mSpriteBall.isVisible() || !ball.isEnable) {
                k++;
            } else {
                int i = 0;
                while (i < this.mDynamicDialog.size()) {
                    DynamicDialog dd = this.mDynamicDialog.get(i);
                    if (dd == null || dd.isDisplayed() || !checkCollides(ball, dd.getmX(), dd.getmY(), dd.getmWidth(), dd.getmHeight())) {
                        i++;
                    } else {
                        dd.setDisplayed(true);
                        setDialogInfo(null, null, this.mStrTileDynamicDialog, new String[]{getDynamicDialogText(dd.getmType())}, this.mFontTextDialogTitle, new Font[]{this.mFontCaption});
                        this.mStatus = ChildSceneStatus.UNDIFINE;
                        showTextDialog();
                        return;
                    }
                }
                return;
            }
        }
    }

    private String getDynamicDialogText(int pType) {
        Resources res = this.mContext.getResources();
        int id = Const.getTextId(pType);
        if (id < 0) {
            return "";
        }
        return res.getString(id);
    }

    private boolean checkCollides(Ball ball, float pX, float pY, float pWidth, float pHeight) {
        float crab_centerx = ball.mSpriteBall.getX() + (ball.mSpriteBall.getWidth() / 2.0f);
        float crab_centery = ball.mSpriteBall.getY() + (ball.mSpriteBall.getHeight() / 2.0f);
        if (crab_centerx - pX < 0.0f || crab_centerx - pX > pWidth || crab_centery - pY < 0.0f || crab_centery - pY > pHeight) {
            return false;
        }
        return true;
    }

    private void createActiveBall(float pX, float pY, float pSpeedX, float pSpeedY) {
        float f = pX;
        float f2 = pY;
        Ball ball = new Ball(f, f2, this.mTRBalls[MathUtils.random(0, 0)].clone(), this, this.Layer_Three, this.mPhysicsWorld, this.mTiledMapScale);
        ball.setScale(0.8f);
        ball.createBall();
        ball.setDynamic();
        ball.mBodyBall.applyLinearImpulse(new Vector2(pSpeedX, pSpeedY), ball.mBodyBall.getWorldCenter());
        ball.isEnable = true;
        this.mSpriteBalls.add(ball);
    }

    private void addBallCount() {
        this.mBallCount++;
        this.mRemainBallText.setText(String.format(this.REMAIN_BALL_FORMART, Integer.valueOf(this.mBallCount)));
    }

    private void plusBallCount() {
        this.mBallCount--;
        this.mRemainBallText.setText(String.format(this.REMAIN_BALL_FORMART, Integer.valueOf(this.mBallCount)));
    }

    /* access modifiers changed from: private */
    public void checkEntityCount() {
        if (this.mSpriteBalls.size() == 0) {
            this.isNeedUpdateChaseSprite = true;
            if (getAddLifeCount() != 0 || this.mBallCount > 0) {
                if (!this.isBallDead && this.mBallCount > 0) {
                    if (this.isFirstBall) {
                        this.isFirstBall = false;
                        return;
                    }
                    this.isBallDead = true;
                    this.isEnableCrab = false;
                    this.mContext.getEngine().vibrate(100);
                    this.mSpriteCrab.addShapeModifier(new LoopShapeModifier(new IShapeModifier.IShapeModifierListener() {
                        public /* bridge */ /* synthetic */ void onModifierFinished(IModifier x0, Object x1) {
                            onModifierFinished((IModifier<IShape>) x0, (IShape) x1);
                        }

                        public void onModifierFinished(IModifier<IShape> iModifier, IShape arg1) {
                            boolean unused = GameScene.this.isEnableCrab = true;
                        }
                    }, 5, new SequenceShapeModifier(new AlphaModifier(0.1f, 1.0f, 0.6f), new AlphaModifier(0.1f, 0.6f, 1.0f))));
                }
            } else if (!this.isOver) {
                this.mGameOverUpdate = System.currentTimeMillis();
                this.isOver = true;
                resetAllProp();
            } else if (System.currentTimeMillis() - this.mGameOverUpdate >= this.PASSLEVEL_DELAY) {
                setGameOver();
            }
        }
    }

    /* access modifiers changed from: private */
    public void checkBallDead() {
        ArrayList<Ball> removedBall = new ArrayList<>();
        for (int i = 0; i < this.mSpriteBalls.size(); i++) {
            Ball ball = this.mSpriteBalls.get(i);
            if (ball != null) {
                if (ball.isRemove || ball.mSpriteBall.getX() < 0.0f || ball.mSpriteBall.getX() > CommonConst.MAP_WIDTH || ball.mSpriteBall.getY() < 0.0f || ball.mSpriteBall.getY() > CommonConst.MAP_HEIGHT || (ball.mBodyBall.getLinearVelocity().x == 0.0f && ball.mBodyBall.getLinearVelocity().y == 0.0f && !ball.isBeJoint)) {
                    ball.destroy();
                    removedBall.add(ball);
                } else if (ball.isHitWall) {
                    if (CommonConst.GAME_MUSIC_ON) {
                        this.mContext.mResource.playSound(Resource.SOUNDTURE.SOUND_HITWALL);
                    }
                    ball.isHitWall = false;
                }
            }
        }
        this.mSpriteBalls.removeAll(removedBall);
    }

    private int getAddLifeCount() {
        int count = 0;
        for (int i = 0; i < this.mSpriteProp.size(); i++) {
            Prop prop = this.mSpriteProp.get(i);
            if (prop != null && prop.mSpriteProp != null && prop.mType == 4 && prop.isEnable) {
                count++;
            }
        }
        return count;
    }

    private void initializeBoundCamera() {
        ((BoundCamera) this.mContext.getEngine().getCamera()).setBounds(0.0f, CommonConst.MAP_WIDTH, 0.0f, CommonConst.MAP_HEIGHT);
        ((BoundCamera) this.mContext.getEngine().getCamera()).setBoundsEnabled(true);
    }

    public void showStartHint() {
        setDialogInfo(null, null, this.mStrTitleRules, new String[]{this.captionValue}, this.mFontTextDialogTitle, new Font[]{this.mFontCaption});
        this.mStatus = ChildSceneStatus.GAME_START_DIALOG;
        showTextDialog();
    }

    private void showBallDeadHint() {
        setDialogInfo(null, null, this.mStrTitleRules, new String[]{this.mStrBallDead}, this.mFontTextDialogTitle, new Font[]{this.mFontCaption});
        this.mStatus = ChildSceneStatus.UNDIFINE;
        showTextDialog();
    }

    public HUD getHud() {
        return this.mHud;
    }

    private void initializeHudScene() {
        this.mHud = new HUD(2);
        Sprite mSpriteScoreBar = new Sprite(0.0f, 0.0f, ((float) this.mTRScoreBg.getWidth()) * this.mSceneSizeScale, ((float) this.mTRScoreBg.getHeight()) * this.mSceneSizeScale, this.mTRScoreBg);
        this.mHud.getLayer(this.Layer_Hud1).addEntity(mSpriteScoreBar);
        this.scoreText = new ChangeableText(mSpriteScoreBar.getX() + (70.0f * this.mSceneSizeScale), mSpriteScoreBar.getY() + (12.0f * this.mSceneSizeScale), this.mFontHud, getScoreFormartString(this.mScore), 4);
        Sprite mSpriteBallPowerBar = new Sprite(((float) CommonConst.CAMERA_WIDTH) - (((float) this.mTRBallPowerBar.getWidth()) * this.mSceneSizeScale), 0.0f, ((float) this.mTRBallPowerBar.getWidth()) * this.mSceneSizeScale, ((float) this.mTRBallPowerBar.getHeight()) * this.mSceneSizeScale, this.mTRBallPowerBar);
        this.mHud.getLayer(this.Layer_Hud1).addEntity(mSpriteBallPowerBar);
        this.mRemainBallText = new ChangeableText(mSpriteBallPowerBar.getX() + (50.0f * this.mSceneSizeScale), mSpriteBallPowerBar.getY() + (10.0f * this.mSceneSizeScale), this.mFontHud, String.format(this.REMAIN_BALL_FORMART, Integer.valueOf(this.mBallCount)), 4);
        ChangeableText mLevelLabel = new ChangeableText(0.0f, 0.0f, this.mFontHud, String.format(this.LEVEL_LABEL_FORMART, Integer.valueOf(this.level + 1), Integer.valueOf(this.insideIndex + 1)), 9);
        mLevelLabel.setPosition((((float) CommonConst.CAMERA_WIDTH) - mLevelLabel.getWidth()) - (20.0f * this.mSceneSizeScale), mSpriteBallPowerBar.getY() + (45.0f * this.mSceneSizeScale));
        this.mHud.getLayer(this.Layer_Hud2).addEntity(this.scoreText);
        this.mHud.getLayer(this.Layer_Hud2).addEntity(this.mRemainBallText);
        this.mHud.getLayer(this.Layer_Hud2).addEntity(mLevelLabel);
        this.mContext.getEngine().getCamera().setHUD(this.mHud);
    }

    public void setScore(int pValue) {
        this.scoreText.setText(getScoreFormartString(pValue));
    }

    private void checkPassLevel() {
        if (this.mValuableRemain <= 0) {
            this.isPassLevel = true;
        }
    }

    private void setPassLevel() {
        if (CommonConst.GAME_MUSIC_ON) {
            this.mContext.mResource.pauseMusic(Resource.MUSICTURE.BACKGROUD_MUSIC);
            if (this.isTimeLimitedPlaying) {
                this.mContext.mResource.pauseSound(Resource.SOUNDTURE.SOUND_LIMITED_TIME);
            }
            this.mContext.mResource.playSound(Resource.SOUNDTURE.SOUND_PASSLEVEL);
        }
        ((SmoothCamera) this.mContext.getEngine().getCamera()).setChaseShape(this.mOriginalChaseShape);
        GoogleAnalyticsUtils.setTracker("play_end_" + String.valueOf(this.mContext.getMGameInfo().getMLevelIndex()) + "_" + String.valueOf(this.mContext.getMGameInfo().getMInsideIndex() + "/success/" + String.valueOf(this.mScore)));
        if (this.insideIndex + 1 >= PortConst.LevelInfo[this.level][0]) {
            loadPassAllLevelTips();
        } else {
            loadNextLevelTips();
        }
        saveGameInfo(true);
    }

    private void loadNextLevelTips() {
        showGameOverDialog(true, false, this.mScore);
        this.mStatus = ChildSceneStatus.GAME_NEXLLEVEL;
    }

    private void loadPassAllLevelTips() {
        if (PortConst.enableLiteVersion) {
            showGameOverDialog(true, true, this.mScore);
            this.mStatus = ChildSceneStatus.GAME_PASSALLLEVEL_LITE;
            return;
        }
        showGameOverDialog(true, true, this.mScore);
        this.mStatus = ChildSceneStatus.GAME_PASSALLLEVEL;
    }

    private void setGameOver() {
        this.isGameOver = true;
        if (CommonConst.GAME_MUSIC_ON) {
            this.mContext.mResource.pauseMusic(Resource.MUSICTURE.BACKGROUD_MUSIC);
            if (this.isTimeLimitedPlaying) {
                this.mContext.mResource.pauseSound(Resource.SOUNDTURE.SOUND_LIMITED_TIME);
            }
            this.mContext.mResource.playSound(Resource.SOUNDTURE.SOUND_NOPASS);
        }
        this.mContext.getEngine().vibrate(200);
        saveGameInfo(false);
        loadGameOverTips();
    }

    /* access modifiers changed from: private */
    public void updateSpriteClear() {
        boolean flag = false;
        if (this.isNeedUpdateClear) {
            for (int i = 0; i < this.mSpriteClear.size(); i++) {
                AnimatedSprite spriteClear = this.mSpriteClear.get(i);
                if (spriteClear.isVisible() && spriteClear.isAnimationRunning()) {
                    flag = true;
                    this.isNeedUpdateClear = true;
                } else if (spriteClear.isVisible() && !spriteClear.isAnimationRunning()) {
                    spriteClear.setVisible(false);
                    if (!flag) {
                        this.isNeedUpdateClear = false;
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void checkPropOnAreaTouch() {
        if (this.isCheckGetProp) {
            for (int i = 0; i < this.mSpriteProp.size(); i++) {
                Prop prop = this.mSpriteProp.get(i);
                if (prop != null && prop.isEnable && prop.is_Click()) {
                    if (CommonConst.GAME_MUSIC_ON) {
                        this.mContext.mResource.playSound(Resource.SOUNDTURE.SOUND_GOLD);
                    }
                    if (prop.mType == 5) {
                        getGold(prop);
                    } else if (prop.mType == 4) {
                        getAddLife();
                        prop.isRemove = true;
                    } else if (prop.mType == 3) {
                        getCrabEnlarge();
                        prop.isRemove = true;
                    } else if (prop.mType == 1) {
                        updateBallSpeed(true);
                        prop.isRemove = true;
                    } else if (prop.mType == 2) {
                        updateBallSpeed(false);
                        prop.isRemove = true;
                    } else if (prop.mType == 0) {
                        createMultiBalls();
                        prop.isRemove = true;
                    }
                    updatePosition(prop.mTouchAreaX);
                    prop.set_Click(false);
                }
            }
            this.isCheckGetProp = false;
        }
    }

    private void getAddLife() {
        addBallCount();
    }

    private void getCrabEnlarge() {
        this.mSpriteCrab.setCrabScale(true);
    }

    private void updateBallSpeed(boolean isAcc) {
        if (isAcc) {
            this.mBalljetSpeed = Const.BallJetSpeed * Const.BallJetAccSpeedScale;
        } else {
            this.mBalljetSpeed = Const.BallJetSpeed * Const.BallJetDecSpeedScale;
        }
        this.isPropSpeed = true;
        this.isPropSpeedFirst = true;
        Ball ball = getChaseBall();
        if (ball != null && !ball.isBeJoint) {
            updateSpeed(ball, false);
        }
    }

    private void resetPropSpeed() {
        if (this.isPropSpeed) {
            this.mBalljetSpeed = Const.BallJetSpeed;
            this.isPropSpeed = false;
            this.isPropSpeedFirst = true;
        }
    }

    /* access modifiers changed from: private */
    public void checkPropSpeed() {
        if (this.isPropSpeed) {
            if (this.isPropSpeedFirst) {
                this.mPropDuration = System.currentTimeMillis();
                this.mPropSpeedTimes = 5;
                this.isPropSpeedFirst = false;
            }
            if (System.currentTimeMillis() - this.mPropDuration >= 1000) {
                this.mPropDuration = System.currentTimeMillis();
                this.mPropSpeedTimes--;
            }
            if (this.mPropSpeedTimes < 0) {
                this.mBalljetSpeed = Const.BallJetSpeed;
                this.isPropSpeed = false;
                this.isPropSpeedFirst = true;
            }
        }
    }

    private void getBomb(Prop prop) {
        int mRow = getTileIndexX(prop.mSpriteProp.getX());
        int mColumn = getTileIndexY(prop.mSpriteProp.getY());
        setExplosion(mRow, mColumn);
        clearAroundEntity(mRow, mColumn);
    }

    private void getGold(Prop prop) {
        float cameraMinX = this.mContext.getEngine().getCamera().getMinX();
        float cameraMinY = this.mContext.getEngine().getCamera().getMinY();
        float targetX = cameraMinX + (60.0f * this.mSceneSizeScale);
        float targetY = cameraMinY + (30.0f * this.mSceneSizeScale);
        float pX = prop.mSpriteProp.getX();
        float pY = prop.mSpriteProp.getY();
        float moveTime = Math.max(Math.abs(pY - targetY) / Const.MAP_TILE_HEIGHT, Math.abs(pX - targetX) / Const.MAP_TILE_WIDTH) * 0.1f;
        prop.isEnable = false;
        prop.isModifier = true;
        prop.mSpriteProp.addShapeModifier(new SequenceShapeModifier(new IShapeModifier.IShapeModifierListener() {
            public /* bridge */ /* synthetic */ void onModifierFinished(IModifier x0, Object x1) {
                onModifierFinished((IModifier<IShape>) x0, (IShape) x1);
            }

            public void onModifierFinished(IModifier<IShape> iModifier, IShape arg1) {
                int i = 0;
                while (i < GameScene.this.mSpriteProp.size()) {
                    Prop prop = (Prop) GameScene.this.mSpriteProp.get(i);
                    if (prop == null || prop.mType != 5 || prop.mSpriteProp == null || !prop.mSpriteProp.equals(arg1)) {
                        i++;
                    } else {
                        GameScene.access$1812(GameScene.this, prop.get_GoldScore());
                        GameScene.this.scoreText.setText(GameScene.this.getScoreFormartString(GameScene.this.mScore));
                        prop.isModifier = false;
                        prop.isRemove = true;
                        return;
                    }
                }
            }
        }, new ParallelShapeModifier(new AlphaModifier(moveTime, 1.0f, 0.5f), new MoveModifier(moveTime, pX, targetX, pY, targetY))));
    }

    /* access modifiers changed from: private */
    public void checkWallBreak() {
        float cameraMinX = this.mContext.getEngine().getCamera().getMinX();
        float cameraMinY = this.mContext.getEngine().getCamera().getMinY();
        for (int i = 0; i < CommonConst.MAP_ARRAY_Y; i++) {
            for (int j = 0; j < CommonConst.MAP_ARRAY_X; j++) {
                if (this.mWallEntity[i][j] != null) {
                    if (this.mWallEntity[i][j].isRemove || !this.mWallEntity[i][j].is_Collide()) {
                        AnimatedSprite valuableAnimatedSprite = this.mWallEntity[i][j].mSpriteEntity;
                        if (valuableAnimatedSprite != null && this.mWallEntity[i][j].mClearTimes > 0) {
                            checkInCamera(valuableAnimatedSprite, cameraMinX, cameraMinY, this.cameraWidth, this.cameraHeight);
                        }
                    } else {
                        this.mWallEntity[i][j].mClearTimes--;
                        if (this.mWallEntity[i][j].mClearTimes <= 0) {
                            clearSprite(i, j);
                        } else {
                            playNoBreakSound();
                            this.mWallEntity[i][j].mSpriteEntity.setCurrentTileIndex(this.mWallEntity[i][j].mClearTimes - 1);
                            animateSprite(i, j);
                        }
                        this.mWallEntity[i][j].set_Collide(false);
                    }
                }
            }
        }
    }

    private void playNoBreakSound() {
        int type = MathUtils.random(0, 2);
        if (CommonConst.GAME_MUSIC_ON) {
            switch (type) {
                case 0:
                    this.mContext.mResource.playSound(Resource.SOUNDTURE.SOUND_HITBLOCK1);
                    return;
                case 1:
                    this.mContext.mResource.playSound(Resource.SOUNDTURE.SOUND_HITBLOCK2);
                    return;
                case 2:
                    this.mContext.mResource.playSound(Resource.SOUNDTURE.SOUND_HITBLOCK3);
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void checkPropRemove() {
        for (int i = 0; i < this.mSpriteProp.size(); i++) {
            Prop prop = this.mSpriteProp.get(i);
            if (prop != null && (prop.isRemove || ((prop.mSpriteProp != null && prop.mSpriteProp.getY() > CommonConst.MAP_HEIGHT) || (prop.mAnimatedSpriteProp != null && prop.mAnimatedSpriteProp.getY() > CommonConst.MAP_HEIGHT)))) {
                prop.destroy();
            }
        }
    }

    private void animateSprite(int i, int j) {
        AnimatedSprite entitySprite;
        if (this.mWallEntity[i][j] != null && (entitySprite = this.mWallEntity[i][j].mSpriteEntity) != null) {
            entitySprite.clearShapeModifiers();
            entitySprite.setScale(1.0f);
            entitySprite.setAlpha(1.0f);
            entitySprite.addShapeModifier(new SequenceShapeModifier(new ParallelShapeModifier(new AlphaModifier(0.25f, 1.0f, 0.5f), new ScaleModifier(0.25f, 1.0f, 1.2f)), new ParallelShapeModifier(new AlphaModifier(0.25f, 0.5f, 1.0f), new ScaleModifier(0.25f, 1.2f, 1.0f))));
        }
    }

    private void clearSprite(int i, int j) {
        if (i < CommonConst.MAP_ARRAY_Y && j < CommonConst.MAP_ARRAY_X && this.mWallEntity[i][j] != null && !this.mWallEntity[i][j].isRemove) {
            this.mWallEntity[i][j].setEnable(false);
            if (this.mWallEntity[i][j].mType == 15) {
                if (CommonConst.GAME_MUSIC_ON) {
                    this.mContext.mResource.playSound(Resource.SOUNDTURE.SOUND_HITPEARL);
                }
                setClearAnimated(i, j);
                this.mValuableRemain--;
                checkPassLevel();
            } else if (this.mWallEntity[i][j].mSpriteEntity != null) {
                processGetEntity(i, j);
                setClearAnimated(i, j);
            }
        }
    }

    private void clearAroundEntity(int pRow, int pColumn) {
        if (pRow - 1 >= 0 && pColumn - 1 >= 0) {
            clearSprite(pRow - 1, pColumn - 1);
        }
        if (pRow - 1 >= 0) {
            clearSprite(pRow - 1, pColumn);
        }
        if (pRow - 1 >= 0 && pColumn + 1 < CommonConst.MAP_ARRAY_Y) {
            clearSprite(pRow - 1, pColumn + 1);
        }
        if (pColumn - 1 >= 0) {
            clearSprite(pRow, pColumn - 1);
        }
        if (pColumn + 1 < CommonConst.MAP_ARRAY_Y) {
            clearSprite(pRow, pColumn + 1);
        }
        if (pRow + 1 < CommonConst.MAP_ARRAY_X && pColumn - 1 >= 0) {
            clearSprite(pRow + 1, pColumn - 1);
        }
        if (pRow + 1 < CommonConst.MAP_ARRAY_X) {
            clearSprite(pRow + 1, pColumn);
        }
        if (pRow + 1 < CommonConst.MAP_ARRAY_X && pColumn + 1 < CommonConst.MAP_ARRAY_Y) {
            clearSprite(pRow + 1, pColumn + 1);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long, boolean):org.anddev.andengine.entity.sprite.AnimatedSprite
     arg types: [int, int]
     candidates:
      org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long, int):org.anddev.andengine.entity.sprite.AnimatedSprite
      org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long[], int):org.anddev.andengine.entity.sprite.AnimatedSprite
      org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long[], boolean):org.anddev.andengine.entity.sprite.AnimatedSprite
      org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long, boolean):org.anddev.andengine.entity.sprite.AnimatedSprite */
    private void setClearAnimated(int pRow, int pColumn) {
        float x = ((((float) pColumn) * CommonConst.MAP_TILE_WIDTH) + (CommonConst.MAP_TILE_WIDTH / 2.0f)) - ((((float) this.mTTRClear.getTileWidth()) * this.mTiledMapScale) / 2.0f);
        float y = ((((float) pRow) * CommonConst.MAP_TILE_HEIGHT) + (CommonConst.MAP_TILE_HEIGHT / 2.0f)) - ((((float) this.mTTRClear.getTileHeight()) * this.mTiledMapScale) / 2.0f);
        boolean isFind = false;
        AnimatedSprite spriteClear = null;
        int i = 0;
        while (true) {
            if (i >= this.mSpriteClear.size()) {
                break;
            }
            spriteClear = this.mSpriteClear.get(i);
            if (!spriteClear.isVisible()) {
                spriteClear.stopAnimation();
                spriteClear.setVisible(true);
                spriteClear.setPosition(x, y);
                isFind = true;
                break;
            }
            i++;
        }
        if (!isFind) {
            spriteClear = new AnimatedSprite(x, y, ((float) this.mTTRClear.getTileWidth()) * this.mTiledMapScale, ((float) this.mTTRClear.getTileHeight()) * this.mTiledMapScale, this.mTTRClear.clone());
            getLayer(this.Layer_Three).addEntity(spriteClear);
            this.mSpriteClear.add(spriteClear);
        }
        this.isNeedUpdateClear = true;
        spriteClear.animate(100L, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long, boolean, org.anddev.andengine.entity.sprite.AnimatedSprite$IAnimationListener):org.anddev.andengine.entity.sprite.AnimatedSprite
     arg types: [int, int, com.finger2finger.games.scene.GameScene$6]
     candidates:
      org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long, int, org.anddev.andengine.entity.sprite.AnimatedSprite$IAnimationListener):org.anddev.andengine.entity.sprite.AnimatedSprite
      org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long[], int, org.anddev.andengine.entity.sprite.AnimatedSprite$IAnimationListener):org.anddev.andengine.entity.sprite.AnimatedSprite
      org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long[], boolean, org.anddev.andengine.entity.sprite.AnimatedSprite$IAnimationListener):org.anddev.andengine.entity.sprite.AnimatedSprite
      org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long, boolean, org.anddev.andengine.entity.sprite.AnimatedSprite$IAnimationListener):org.anddev.andengine.entity.sprite.AnimatedSprite */
    private void setExplosion(int pRow, int pColumn) {
        float x = ((((float) pColumn) * CommonConst.MAP_TILE_WIDTH) + (CommonConst.MAP_TILE_WIDTH / 2.0f)) - ((((float) this.mTTRExplosion.getTileWidth()) * this.mTiledMapScale) / 2.0f);
        float y = ((((float) pRow) * CommonConst.MAP_TILE_HEIGHT) + (CommonConst.MAP_TILE_HEIGHT / 2.0f)) - ((((float) this.mTTRExplosion.getTileHeight()) * this.mTiledMapScale) / 2.0f);
        boolean isFind = false;
        AnimatedSprite spriteExplosion = null;
        for (int i = 0; i < this.mSpriteExplosion.size(); i++) {
            spriteExplosion = this.mSpriteExplosion.get(i);
            if (!spriteExplosion.isVisible()) {
                spriteExplosion.setPosition(x, y);
                spriteExplosion.setVisible(true);
                isFind = true;
            }
        }
        if (!isFind) {
            spriteExplosion = new AnimatedSprite(x, y, ((float) this.mTTRExplosion.getTileWidth()) * this.mTiledMapScale, ((float) this.mTTRExplosion.getTileHeight()) * this.mTiledMapScale, this.mTTRExplosion.clone());
            getLayer(this.Layer_Three).addEntity(spriteExplosion);
            this.mSpriteExplosion.add(spriteExplosion);
        }
        spriteExplosion.animate(180L, false, (AnimatedSprite.IAnimationListener) new AnimatedSprite.IAnimationListener() {
            public void onAnimationEnd(AnimatedSprite arg0) {
                if (arg0 != null) {
                    arg0.setVisible(false);
                }
            }
        });
        this.mContext.getEngine().vibrate(100);
        if (CommonConst.GAME_MUSIC_ON) {
            this.mContext.mResource.playSound(Resource.SOUNDTURE.BOMB_SOUND);
        }
    }

    private void createMultiBalls() {
        Ball ball = getChaseBall();
        if (ball != null && ball.mBodyBall != null && !ball.isBeJoint) {
            float px = ball.mSpriteBall.getX();
            float py = ball.mSpriteBall.getY();
            float xSpeed = ball.mBodyBall.getLinearVelocity().x / 2.0f;
            float ySpeed = ball.mBodyBall.getLinearVelocity().y / 2.0f;
            for (int i = 0; i < 3; i++) {
                createActiveBall(px, py, (((float) MathUtils.random(0, this.RANDOM_SPEED_X)) + xSpeed) - ((float) this.RANDOM_SPEED_X), (((float) MathUtils.random(0, this.RANDOM_SPEED_Y)) + ySpeed) - ((float) this.RANDOM_SPEED_Y));
            }
            ball.isRemove = true;
            this.isNeedUpdateChaseSprite = true;
        }
    }

    private class Vector {
        float mPositionX = 0.0f;
        float mPositionY = 0.0f;

        public Vector(float pPositionX, float pPositionY) {
            this.mPositionX = pPositionX;
            this.mPositionY = pPositionY;
        }
    }

    private Vector getRulePosition(int pRow, int pColumn, TextureRegion pTextureRegion) {
        return new Vector(((((float) pColumn) * CommonConst.MAP_TILE_WIDTH) + (CommonConst.MAP_TILE_WIDTH / 2.0f)) - ((((float) pTextureRegion.getWidth()) * this.mTiledMapScale) / 2.0f), ((((float) pRow) * CommonConst.MAP_TILE_HEIGHT) + (CommonConst.MAP_TILE_HEIGHT / 2.0f)) - ((((float) pTextureRegion.getHeight()) * this.mTiledMapScale) / 2.0f));
    }

    private Vector getRulePosition(int pRow, int pColumn, TiledTextureRegion pTiledTextureRegion) {
        return new Vector(((((float) pColumn) * CommonConst.MAP_TILE_WIDTH) + (CommonConst.MAP_TILE_WIDTH / 2.0f)) - ((((float) pTiledTextureRegion.getTileWidth()) * this.mTiledMapScale) / 2.0f), ((((float) pRow) * CommonConst.MAP_TILE_HEIGHT) + (CommonConst.MAP_TILE_HEIGHT / 2.0f)) - ((((float) pTiledTextureRegion.getTileHeight()) * this.mTiledMapScale) / 2.0f));
    }

    private boolean checkIsActive(String pType) {
        if (this.mPropProperty.get(pType).mScale > MathUtils.random(0, 100)) {
            return true;
        }
        return false;
    }

    private boolean checkPropActive() {
        if (checkSinglePropActive(Const.ObjectName.Ball_Add)) {
            return true;
        }
        if (checkSinglePropActive(Const.ObjectName.Crab_Enlarge)) {
            return true;
        }
        if (checkSinglePropActive(Const.ObjectName.Ball_Power)) {
            return true;
        }
        if (checkSinglePropActive(Const.ObjectName.Ball_SpeedUp)) {
            return true;
        }
        if (checkSinglePropActive(Const.ObjectName.Ball_SpeedDown)) {
            return true;
        }
        if (checkSinglePropActive(Const.ObjectName.Bomb)) {
            return true;
        }
        return false;
    }

    private boolean checkSinglePropActive(String pType) {
        if (this.mPropProperty.get(pType) == null || this.mPropProperty.get(pType).mScale <= 0) {
            return false;
        }
        return true;
    }

    private void createSpeciallyEffect(int px, int py) {
        if (checkPropActive()) {
            switch (MathUtils.random(0, 4)) {
                case 0:
                    if (checkSinglePropActive(Const.ObjectName.Ball_Add) && checkIsActive(Const.ObjectName.Ball_Add)) {
                        createAddBall(px, py);
                        return;
                    }
                    return;
                case 1:
                    if (checkSinglePropActive(Const.ObjectName.Crab_Enlarge) && checkIsActive(Const.ObjectName.Crab_Enlarge)) {
                        createCrabScale(px, py);
                        return;
                    }
                    return;
                case 2:
                    if (checkSinglePropActive(Const.ObjectName.Ball_Power) && checkIsActive(Const.ObjectName.Ball_Power)) {
                        createBallPower(px, py);
                        return;
                    }
                    return;
                case 3:
                    if (checkSinglePropActive(Const.ObjectName.Ball_SpeedUp) && checkIsActive(Const.ObjectName.Ball_SpeedUp)) {
                        createBallSpeedScale(px, py, true);
                        return;
                    }
                    return;
                case 4:
                    if (checkSinglePropActive(Const.ObjectName.Ball_SpeedDown) && checkIsActive(Const.ObjectName.Ball_SpeedDown)) {
                        createBallSpeedScale(px, py, false);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    private void initializeAllProp() {
        for (int i = 0; i < this.mPropSpeedUpCount; i++) {
            createProp(1, 0);
        }
        for (int i2 = 0; i2 < this.mPropSpeedDownCount; i2++) {
            createProp(2, 0);
        }
        for (int i3 = 0; i3 < this.mPropLifeCount; i3++) {
            createProp(4, 0);
        }
        for (int i4 = 0; i4 < this.mPropCrabScaleCount; i4++) {
            createProp(3, 0);
        }
        for (int i5 = 0; i5 < this.mPropBallPowerCount; i5++) {
            createProp(0, 0);
        }
        for (int i6 = 0; i6 < this.mPropGoldCount; i6++) {
            for (int j = 0; j < 3; j++) {
                createProp(5, j);
            }
        }
    }

    private Prop createProp(int type, int goldType) {
        TiledTextureRegion ttr = null;
        TextureRegion tr = null;
        switch (type) {
            case 0:
                ttr = this.mTTRPowerStone;
                break;
            case 1:
                tr = this.mTRAccSpeed;
                break;
            case 2:
                tr = this.mTRDecSpeed;
                break;
            case 3:
                tr = this.mTREnlarge;
                break;
            case 4:
                tr = this.mTRAddBall;
                break;
            case 5:
                tr = this.mTRGolds[goldType];
                break;
        }
        Prop prop = null;
        if (tr != null) {
            prop = new Prop(type, 0.0f, 0.0f, tr.clone(), this, this.Layer_Three);
            if (type == 5) {
                prop.mGoldType = goldType;
            }
        } else if (ttr != null) {
            prop = new Prop(type, 0.0f, 0.0f, ttr.clone(), this, this.Layer_Three);
        }
        if (prop != null) {
            prop.createProp();
            this.mSpriteProp.add(prop);
        }
        return prop;
    }

    private Prop getPropByType(int type, int goldType) {
        for (int i = 0; i < this.mSpriteProp.size(); i++) {
            Prop prop = this.mSpriteProp.get(i);
            if (prop != null && prop.mType == type && !prop.isEnable && !prop.isModifier) {
                if (type != 5) {
                    return prop;
                }
                if (goldType == prop.mGoldType) {
                    return prop;
                }
            }
        }
        return null;
    }

    private void createBallSpeedScale(int pRow, int pColumn, boolean isAddSpeed) {
        Prop prop;
        Vector position = getRulePosition(pRow, pColumn, this.mTRAccSpeed);
        if (isAddSpeed) {
            prop = getPropByType(1, 0);
        } else {
            prop = getPropByType(2, 0);
        }
        if (prop != null) {
            prop.setEnable(position.mPositionX, position.mPositionY);
        }
    }

    private void createAddBall(int pRow, int pColumn) {
        Vector position = getRulePosition(pRow, pColumn, this.mTRAddBall);
        Prop prop = getPropByType(4, 0);
        if (prop != null) {
            prop.setEnable(position.mPositionX, position.mPositionY);
        }
    }

    private void createCrabScale(int pRow, int pColumn) {
        Vector position = getRulePosition(pRow, pColumn, this.mTREnlarge);
        Prop prop = getPropByType(3, 0);
        if (prop != null) {
            prop.setEnable(position.mPositionX, position.mPositionY);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long, boolean):org.anddev.andengine.entity.sprite.AnimatedSprite
     arg types: [int, int]
     candidates:
      org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long, int):org.anddev.andengine.entity.sprite.AnimatedSprite
      org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long[], int):org.anddev.andengine.entity.sprite.AnimatedSprite
      org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long[], boolean):org.anddev.andengine.entity.sprite.AnimatedSprite
      org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long, boolean):org.anddev.andengine.entity.sprite.AnimatedSprite */
    private void createBallPower(int pRow, int pColumn) {
        Vector position = getRulePosition(pRow, pColumn, this.mTTRPowerStone);
        Prop prop = getPropByType(0, 0);
        if (prop != null) {
            prop.setEnable(position.mPositionX, position.mPositionY);
            prop.mAnimatedSpriteProp.addShapeModifier(new LoopShapeModifier(new SequenceShapeModifier(new RotationModifier(3.0f, 0.0f, 360.0f))));
            prop.mAnimatedSpriteProp.animate(500L, true);
        }
    }

    private void processGetEntity(int px, int py) {
        int type = this.mWallEntity[px][py].mType;
        if (type >= 1 && type <= 7) {
            if (CommonConst.GAME_MUSIC_ON) {
                this.mContext.mResource.playSound(Resource.SOUNDTURE.SOUND_GET_STONE);
            }
            int bonus = Const.Type.get_gameScore().get(Integer.valueOf(type)).intValue();
            this.mScore += bonus;
            createTextAddScore(px, py, bonus);
            createGold(px, py);
            createSpeciallyEffect(px, py);
        }
    }

    /* access modifiers changed from: private */
    public String getScoreFormartString(int pValue) {
        String str = String.valueOf(pValue);
        int length = str.length();
        for (int i = 0; i < 4 - length; i++) {
            str = "0" + str;
        }
        return str;
    }

    private void initializeCrab(float px, float py) {
        this.mSpriteCrab = new Crab(px * this.mTiledMapScale, py * this.mTiledMapScale, this.mTiledMapScale * ((float) this.mTRCrab.getWidth()), this.mTiledMapScale * ((float) this.mTRCrab.getHeight()), this.mTRCrab);
        this.mUnderLineY = this.mSpriteCrab.getY() + this.mSpriteCrab.getHeight();
        this.mContext.getEngine().getCamera().setChaseShape(this.mSpriteCrab);
        getTopLayer().addEntity(this.mSpriteCrab);
        this.mBodyCrab = PhysicsFactory.createBoxBody(this.mPhysicsWorld, this.mSpriteCrab, BodyDef.BodyType.StaticBody, PhysicsFactory.createFixtureDef(Const.Crab_Density, Const.Crab_Elasticity, Const.Crab_Friction));
        this.mSpriteCrab.setUpdatePhysics(false);
        this.mPhysicsWorld.registerPhysicsConnector(new PhysicsConnector(this.mSpriteCrab, this.mBodyCrab, true, false, true, true));
        this.initialCrabPositionX = this.mBodyCrab.getPosition().x;
        this.initialCrabPositionY = this.mBodyCrab.getPosition().y;
    }

    public void replayGame() {
        GoogleAnalyticsUtils.setTracker("play_start_" + String.valueOf(this.mContext.getMGameInfo().getMLevelIndex()) + "_" + String.valueOf(this.mContext.getMGameInfo().getMInsideIndex() + "/replay"));
        this.isUpdate = true;
        this.isReplay = true;
        this.isGameOver = true;
        this.mMenuBackToUp = true;
        this.isBackToMode = false;
    }

    private void initializeTileMap() {
        setBackgroundLayer();
        setForegroundLayer();
        setObjectLayer();
        setMapLayer();
        setStarLevel();
    }

    private void setStarLevel() {
        if (this.mTotalScore > 0) {
            for (int k = 0; k < CommonConst.STAR_NUM; k++) {
                this.mStarLevel[k] = (int) (Const.SCORE_LEVEL_SCALE[k] * ((float) this.mTotalScore));
            }
        }
    }

    private void setBackgroundLayer() {
        TMXLayer tmxLayer2 = this.mTMXTiledMap.getTMXLayers().get(0);
        tmxLayer2.setScaleCenter(0.0f, 0.0f);
        tmxLayer2.setScale(this.mTiledMapScale);
        getBottomLayer().addEntity(tmxLayer2);
    }

    private void setForegroundLayer() {
        TMXLayer tmxLayer2 = this.mTMXTiledMap.getTMXLayers().get(2);
        tmxLayer2.setScaleCenter(0.0f, 0.0f);
        tmxLayer2.setScale(this.mTiledMapScale);
        getTopLayer().addEntity(tmxLayer2);
    }

    private void setObjectLayer() {
        for (int i = 0; i < this.mTMXTiledMap.getTMXObjectGroups().size(); i++) {
            TMXObjectGroup tmxObjGroup = this.mTMXTiledMap.getTMXObjectGroups().get(i);
            for (int j = 0; j < tmxObjGroup.getTMXObjects().size(); j++) {
                TMXObject tmxObj = tmxObjGroup.getTMXObjects().get(j);
                String objName = tmxObj.getName();
                String objType = tmxObj.getType();
                HashMap<String, String> properties = new HashMap<>();
                for (int k = 0; k < tmxObj.getTMXObjectProperties().size(); k++) {
                    TMXObjectProperty tmxObjPro = (TMXObjectProperty) tmxObj.getTMXObjectProperties().get(k);
                    properties.put(tmxObjPro.getName(), tmxObjPro.getValue());
                }
                if (objName.equals(Const.ObjectName.Crab)) {
                    createCrab(tmxObj, properties);
                } else if (objName.equals(Const.ObjectName.DynamicDialog)) {
                    createDynamicDialog(objType, tmxObj);
                } else if (objName.equals(Const.ObjectName.PhysicsWorld)) {
                    createPhysicsWorldProperty(properties);
                } else if (objName.equals(Const.ObjectName.Ball)) {
                    createBallProperty(properties);
                } else if (!objName.equals("") && properties.get(Const.TileProperty.Property_DisplayScale) != null) {
                    createPropProperty(objName, properties);
                }
            }
        }
    }

    private void createCrab(TMXObject tmxObj, HashMap<String, String> properties) {
        try {
            Const.Crab_Density = Float.parseFloat(properties.get(Const.TileProperty.Density));
        } catch (Exception e) {
            Log.e("Get_Crab_Density_Error", e.getMessage());
        }
        try {
            Const.Crab_Elasticity = Float.parseFloat(properties.get(Const.TileProperty.Elasticity));
        } catch (Exception e2) {
            Log.e("Get_Crab_Elasticity_Error", e2.getMessage());
        }
        try {
            Const.Crab_Friction = Float.parseFloat(properties.get(Const.TileProperty.Friction));
        } catch (Exception e3) {
            Log.e("Get_Crab_Friction_Error", e3.getMessage());
        }
        initializeCrab((float) tmxObj.getX(), (float) tmxObj.getY());
    }

    private void createDynamicDialog(String objType, TMXObject tmxObj) {
        try {
            this.mDynamicDialog.add(new DynamicDialog(Integer.parseInt(objType), ((float) tmxObj.getX()) * this.mTiledMapScale, ((float) tmxObj.getY()) * this.mTiledMapScale, ((float) tmxObj.getWidth()) * this.mTiledMapScale, ((float) tmxObj.getHeight()) * this.mTiledMapScale));
        } catch (Exception e) {
            Log.e("GetDynamicDialog_Error", e.getMessage());
        }
    }

    private void createPhysicsWorldProperty(HashMap<String, String> properties) {
        try {
            Const.PhysicsWorld_AllowSleep = Integer.parseInt(properties.get(Const.TileProperty.AllowSleep)) == 1;
        } catch (Exception e) {
            Log.e("Get_PhysicsWorld_AllowSleep_Error", e.getMessage());
        }
        try {
            Const.PhysicsWorld_GravityX = Float.parseFloat(properties.get(Const.TileProperty.GravityX));
        } catch (Exception e2) {
            Log.e("Get_PhysicsWorld_GravityX_Error", e2.getMessage());
        }
        try {
            Const.PhysicsWorld_GravityY = Float.parseFloat(properties.get(Const.TileProperty.GravityY));
        } catch (Exception e3) {
            Log.e("Get_PhysicsWorld_GravityY_Error", e3.getMessage());
        }
        try {
            Const.PhysicsWorld_StepsPerSecond = (float) Integer.parseInt(properties.get(Const.TileProperty.StepsPerSecond));
        } catch (Exception e4) {
            Log.e("Get_PhysicsWorld_StepsPerSecond_Error", e4.getMessage());
        }
        try {
            Const.PhysicsWorld_VelocityIterations = (float) Integer.parseInt(properties.get(Const.TileProperty.VelocityIterations));
        } catch (Exception e5) {
            Log.e("Get_PhysicsWorld_VelocityIterations_Error", e5.getMessage());
        }
        try {
            Const.PhysicsWorld_PositionIterations = (float) Integer.parseInt(properties.get(Const.TileProperty.PositionIterations));
        } catch (Exception e6) {
            Log.e("Get_PhysicsWorld_PositionIterations_Error", e6.getMessage());
        }
        initializePhysicsWorld();
    }

    private void createBallProperty(HashMap<String, String> properties) {
        try {
            Const.Ball_Density = Float.parseFloat(properties.get(Const.TileProperty.Density));
        } catch (Exception e) {
            Log.e("Get_Ball_Density_Error", e.getMessage());
        }
        try {
            Const.Ball_Elasticity = Float.parseFloat(properties.get(Const.TileProperty.Elasticity));
        } catch (Exception e2) {
            Log.e("Get_Ball_Elasticity_Error", e2.getMessage());
        }
        try {
            Const.Ball_Friction = Float.parseFloat(properties.get(Const.TileProperty.Friction));
        } catch (Exception e3) {
            Log.e("Get_Ball_Friction_Error", e3.getMessage());
        }
        try {
            this.mBalljetSpeed = Float.parseFloat(properties.get(Const.TileProperty.Speed));
            float f = this.mBalljetSpeed * this.mTiledMapScale;
            this.mBalljetSpeed = f;
            Const.BallJetSpeed = f;
        } catch (Exception e4) {
            Log.e("Get_Ball_Speed_Error", e4.getMessage());
        }
        try {
            String[] rangles = properties.get(Const.TileProperty.Rangle).split(",");
            if (rangles != null) {
                this.mJetRangle = new float[rangles.length];
                for (int k = 0; k < rangles.length; k++) {
                    this.mJetRangle[k] = Float.parseFloat(rangles[k]);
                }
            }
        } catch (Exception e5) {
            Log.e("createJetRangle_Error", e5.getMessage());
        }
    }

    private void createPropProperty(String objName, HashMap<String, String> properties) {
        try {
            this.mPropProperty.put(objName, new PropProperty(Integer.parseInt(properties.get(Const.TileProperty.Property_DisplayScale))));
        } catch (Exception e) {
            Log.e("Get_Prop_Scale_Error", e.getMessage());
        }
    }

    private void setMapLayer() {
        this.mWallEntity = (Brick[][]) Array.newInstance(Brick.class, CommonConst.MAP_ARRAY_Y, CommonConst.MAP_ARRAY_X);
        for (int i = 0; i < CommonConst.MAP_ARRAY_Y; i++) {
            int j = 0;
            while (j < CommonConst.MAP_ARRAY_X) {
                TMXTile pTMXtile = this.tmxLayer.getTMXTile(j, i);
                int type = pTMXtile.getGlobalTileID();
                j = (type <= 0 || createPhysicsWorldObject(type, pTMXtile, i, j)) ? j + 1 : j + 1;
            }
        }
        this.mValuableRemain = this.mValuableTotal;
    }

    private boolean createPhysicsWorldObject(int type, TMXTile pTMXtile, int pRow, int pColumn) {
        float pDensity = Const.Obstacle_Density;
        float pElasticity = Const.Obstacle_Elasticity;
        float pFriction = Const.Obstacle_Friction;
        int pType = -1;
        int pClearTimes = 1;
        TMXProperties<TMXTileProperty> tmxTilePropertes = this.mTMXTiledMap.getTMXTileProperties(type);
        if (tmxTilePropertes != null && tmxTilePropertes.size() > 0) {
            for (int i = 0; i < tmxTilePropertes.size(); i++) {
                if (((TMXTileProperty) tmxTilePropertes.get(i)).getName().equals(Const.TileProperty.BrickType)) {
                    if (((TMXTileProperty) tmxTilePropertes.get(i)).getValue().equals(Const.TileProperty.IS_PEARL)) {
                        pType = 15;
                    } else {
                        try {
                            pType = Integer.parseInt(((TMXTileProperty) tmxTilePropertes.get(i)).getValue());
                        } catch (Exception e) {
                            Log.e("Get_BrickType_Density_Error", e.getMessage());
                        }
                    }
                } else if (((TMXTileProperty) tmxTilePropertes.get(i)).getName().equals(Const.TileProperty.Density)) {
                    try {
                        pDensity = Float.parseFloat(((TMXTileProperty) tmxTilePropertes.get(i)).getValue());
                    } catch (Exception e2) {
                        Log.e("Get_Obstacle_Density_Error", e2.getMessage());
                    }
                } else if (((TMXTileProperty) tmxTilePropertes.get(i)).getName().equals(Const.TileProperty.Elasticity)) {
                    try {
                        pElasticity = Float.parseFloat(((TMXTileProperty) tmxTilePropertes.get(i)).getValue());
                    } catch (Exception e3) {
                        Log.e("Get_Obstacle_Elasticity_Error", e3.getMessage());
                    }
                } else if (((TMXTileProperty) tmxTilePropertes.get(i)).getName().equals(Const.TileProperty.Friction)) {
                    try {
                        pFriction = Float.parseFloat(((TMXTileProperty) tmxTilePropertes.get(i)).getValue());
                    } catch (Exception e4) {
                        Log.e("Get_Obstacle_Friction_Error", e4.getMessage());
                    }
                } else if (((TMXTileProperty) tmxTilePropertes.get(i)).getName().equals(Const.TileProperty.ClearTime)) {
                    try {
                        pClearTimes = Integer.parseInt(((TMXTileProperty) tmxTilePropertes.get(i)).getValue());
                    } catch (Exception e5) {
                        Log.e("Get_Obstacle_ClearTimes_Error", e5.getMessage());
                    }
                }
            }
            if (tmxTilePropertes.containsTMXProperty(Const.TileProperty.Name_Obstacle, Const.TileProperty.Value_Obstacle)) {
                return true;
            }
            if (pType == 15) {
                Brick sse = new Brick(15, (float) pTMXtile.getTileX(), (float) pTMXtile.getTileY(), pTMXtile.getTextureRegion().clone(), this, this.Layer_One, this.mPhysicsWorld, pDensity, pElasticity, pFriction, this.mTiledMapScale);
                sse.createEntity();
                this.mWallEntity[pRow][pColumn] = sse;
                this.mValuableTotal = this.mValuableTotal + 1;
                return true;
            } else if (pType + 1 >= 1 && pType + 1 <= 7) {
                Brick sse2 = new Brick(type, (float) pTMXtile.getTileX(), (float) pTMXtile.getTileY(), this.mTTRBricks[type - 1].clone(), this, this.Layer_One, this.mPhysicsWorld, pDensity, pElasticity, pFriction, pClearTimes, this.mTiledMapScale);
                sse2.createEntity();
                this.mWallEntity[pRow][pColumn] = sse2;
                sse2.mSpriteEntity.setCurrentTileIndex(pClearTimes - 1);
                this.mTotalScore = this.mTotalScore + Const.Type.get_gameScore().get(Integer.valueOf(pType + 1)).intValue();
                return true;
            }
        }
        return false;
    }

    private class PropProperty {
        public int mScale = 0;

        public PropProperty(int pScale) {
            this.mScale = pScale;
        }
    }

    private void createGold(int i, int j) {
        if (MathUtils.random(1, 100) % 3 == 0) {
            int goldType = MathUtils.random(0, 2);
            int score = 10;
            switch (goldType) {
                case 0:
                    score = Const.Type.get_gameScore().get(20).intValue();
                    break;
                case 1:
                    score = Const.Type.get_gameScore().get(21).intValue();
                    break;
                case 2:
                    score = Const.Type.get_gameScore().get(22).intValue();
                    break;
            }
            float x = (((float) j) * CommonConst.MAP_TILE_WIDTH) + (CommonConst.MAP_TILE_WIDTH / 2.0f);
            float y = (((float) i) * CommonConst.MAP_TILE_HEIGHT) + (CommonConst.MAP_TILE_HEIGHT / 2.0f);
            float width = ((float) this.mTRGolds[goldType].getWidth()) * this.mTiledMapScale;
            float height = ((float) this.mTRGolds[goldType].getHeight()) * this.mTiledMapScale;
            float px = x - (width / 2.0f);
            float py = y - (height / 2.0f);
            float target_x = px + (((float) (MathUtils.random(0, 400) - AdView.DEFAULT_BACKGROUND_TRANS)) * this.mTiledMapScale);
            float target_x2 = target_x > 0.0f ? (100.0f * this.mTiledMapScale) + target_x : target_x - (100.0f * this.mTiledMapScale);
            float cameraMinX = this.mContext.getEngine().getCamera().getMinX();
            float cameraMinY = this.mContext.getEngine().getCamera().getMinY();
            if (target_x2 <= cameraMinX) {
                target_x2 = cameraMinX + (10.0f * this.mTiledMapScale);
            } else if (target_x2 + width > this.cameraWidth + cameraMinX) {
                target_x2 = ((this.cameraWidth + cameraMinX) - width) - (10.0f * this.mTiledMapScale);
            }
            float target_y = py + (((float) (MathUtils.random(0, (int) AdView.DEFAULT_BACKGROUND_TRANS) - 100)) * this.mTiledMapScale);
            float target_y2 = target_y > 0.0f ? (50.0f * this.mTiledMapScale) + target_y : target_y - (50.0f * this.mTiledMapScale);
            if (target_y2 <= cameraMinY) {
                target_y2 = cameraMinY + (10.0f * this.mTiledMapScale);
            } else if (target_y2 + height > this.cameraHeight + cameraMinY) {
                target_y2 = ((this.cameraHeight + cameraMinY) - height) - (10.0f * this.mTiledMapScale);
            }
            Prop prop = getPropByType(5, goldType);
            if (prop != null) {
                prop.setEnable(px, py);
                prop.set_GoldScore(score);
                prop.mSpriteProp.addShapeModifier(new MoveModifier(0.5f, px, target_x2, py, target_y2));
            }
        }
    }

    private void loadFont() {
        this.mFontTextDialogTitle = this.mContext.mResource.getBaseFontByKey(Resource.FONT.DIALOG_TITLE.mKey);
        this.mFontCaption = this.mContext.mResource.getBaseFontByKey(Resource.FONT.DIALOG_CONTEXT.mKey);
        this.mFontHud = this.mContext.mResource.getBaseFontByKey(Resource.FONT.HUD_CONTEXT.mKey);
        this.mFontDialog = this.mContext.mResource.getBaseFontByKey(Resource.FONT.DIALOG_CONTEXT.mKey);
        this.mFontScoreHint = this.mContext.mResource.getBaseFontByKey(Resource.FONT.GAMEIN_SCORE.mKey);
    }

    public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
        if (this.isGameOver || this.isPassLevel || this.isBallDead) {
            return true;
        }
        if (pSceneTouchEvent.getAction() == 0) {
            saveSceneTouchEvent_Down_Move(pSceneTouchEvent.getX(), pSceneTouchEvent.getY());
        } else if (pSceneTouchEvent.getAction() == 2) {
            if (this.isChildSceneClick) {
                return true;
            }
            saveSceneTouchEvent_Down_Move(pSceneTouchEvent.getX(), pSceneTouchEvent.getY());
            saveSceneTouchEventArrow(pSceneTouchEvent.getX(), pSceneTouchEvent.getY());
        } else if (pSceneTouchEvent.getAction() == 1) {
            if (this.isChildSceneClick) {
                this.isChildSceneClick = false;
                return true;
            }
            saveSceneTouchEvent_Up(pSceneTouchEvent.getX(), pSceneTouchEvent.getY());
        }
        return true;
    }

    private void saveSceneTouchEventArrow(float x1, float y1) {
        if (this.sceneTouchdataArrow[2] != 2.0f) {
            this.sceneTouchdataArrow[0] = x1;
            this.sceneTouchdataArrow[1] = y1;
            this.sceneTouchdataArrow[2] = 1.0f;
        }
    }

    /* access modifiers changed from: private */
    public void onUpdateSceneTouchEvent_Arrow() {
        if (this.sceneTouchdataArrow[2] == 1.0f) {
            this.sceneTouchdataArrow[2] = 2.0f;
            updateArrow(this.sceneTouchdataArrow[0], this.sceneTouchdataArrow[1]);
            this.sceneTouchdataArrow[2] = 0.0f;
        }
    }

    private boolean checkEffectiveBall() {
        for (int i = 0; i < this.mSpriteBalls.size(); i++) {
            Ball ball = this.mSpriteBalls.get(i);
            if (ball != null && !ball.isBeJoint && !ball.isRemove && ball.isEnable) {
                return true;
            }
        }
        return false;
    }

    private void saveSceneTouchEvent_Down_Move(float x1, float y1) {
        if (this.sceneTouchdataDownMove[2] != 2.0f) {
            this.sceneTouchdataDownMove[0] = x1;
            this.sceneTouchdataDownMove[1] = y1;
            this.sceneTouchdataDownMove[2] = 1.0f;
        }
    }

    private void saveSceneTouchEvent_Up(float x1, float y1) {
        if (this.sceneTouchdataUp[2] != 2.0f) {
            this.sceneTouchdataUp[0] = x1;
            this.sceneTouchdataUp[1] = y1;
            this.sceneTouchdataUp[2] = 1.0f;
        }
    }

    /* access modifiers changed from: private */
    public void onUpdateSceneTouchEvent() {
        if (this.sceneTouchdataDownMove[2] == 1.0f) {
            this.sceneTouchdataDownMove[2] = 2.0f;
            this.isCreateBallActive = checkEffectiveBall();
            if (this.isCreateBallActive) {
                updatePosition(this.sceneTouchdataDownMove[0]);
            }
            this.sceneTouchdataDownMove[2] = 0.0f;
        }
        if (this.sceneTouchdataUp[2] == 1.0f) {
            this.sceneTouchdataUp[2] = 2.0f;
            this.isCreateBallActive = checkEffectiveBall();
            if (!this.isCreateBallActive) {
                createBall(this.sceneTouchdataUp[0], this.sceneTouchdataUp[1]);
                setArrowAnimateDelay(this.sceneTouchdataUp[0], this.sceneTouchdataUp[1]);
            }
            this.sceneTouchdataUp[2] = 0.0f;
        }
    }

    private void setArrow(float pX, float pY) {
        if (!this.mSpriteArrow.isVisible()) {
            this.mSpriteArrow.setPosition(this.mCenterJetBall_X + (((float) (this.mTRBalls[0].getWidth() / 2)) * this.mTiledMapScale), this.mCenterJetBall_Y + (((float) ((this.mTRBalls[0].getHeight() / 2) - (this.mTRArrow.getHeight() / 2))) * this.mTiledMapScale));
            this.mSpriteArrow.setRotationCenterX(0.0f);
            this.mSpriteArrow.setScaleCenterX(0.0f);
            this.mSpriteArrow.setVisible(true);
            float disX = pX - (this.mCenterJetBall_X + (((float) (this.mTRBalls[0].getWidth() / 2)) * this.mTiledMapScale));
            float disY = pY - (this.mCenterJetBall_Y + (((float) ((this.mTRBalls[0].getHeight() / 2) - (this.mTRArrow.getHeight() / 2))) * this.mTiledMapScale));
            float rangle = MathUtils.radToDeg((float) Math.atan2((double) disX, (double) (-disY)));
            float originalWidth = ((float) this.mTRArrow.getTileWidth()) * this.mTiledMapScale;
            if (Math.abs(disX) < Math.abs(disY)) {
                this.mSpriteArrow.setScale(Math.abs((disY - (0.0f * this.mTiledMapScale)) / originalWidth), 1.0f);
            } else {
                this.mSpriteArrow.setScale(Math.abs((disX - (0.0f * this.mTiledMapScale)) / originalWidth), 1.0f);
            }
            this.mSpriteArrow.setRotation(rangle - 90.0f);
        }
    }

    private void setArrowAnimateDelay(float pX, float pY) {
        setArrow(pX, pY);
        this.mSpriteArrow.stopAnimation();
        this.mSpriteArrow.setCurrentTileIndex(0);
        this.mSpriteArrow.animate(100, 1, new AnimatedSprite.IAnimationListener() {
            public void onAnimationEnd(AnimatedSprite pAnimatedSprite) {
                GameScene.this.disableArrow();
            }
        });
    }

    private void updatePosition(float x) {
        this.mBodyCrab.setTransform(new Vector2(x / 32.0f, this.mBodyCrab.getPosition().y), 0.0f);
        updateJointBallPosition();
    }

    private void updateJointBallPosition() {
        if (this.mSpriteCrab != null && this.isJoint) {
            float position_x = ((this.mBodyCrab.getPosition().x * 32.0f) + (this.mSpriteCrab.getWidth() / 2.0f)) - ((((float) this.mTRBalls[0].getWidth()) * this.mTiledMapScale) * 2.0f);
            int i = 0;
            while (i < this.mSpriteBalls.size()) {
                Ball ball = this.mSpriteBalls.get(i);
                if (ball == null || !ball.isBeJoint) {
                    i++;
                } else {
                    ball.mBodyBall.setTransform(new Vector2(position_x / 32.0f, ball.mBodyBall.getPosition().y), 0.0f);
                    return;
                }
            }
        }
    }

    private void createBall(float x, float y) {
        Vector2 vector2 = getJetVelocity(x - (this.mCenterJetBall_X + (((float) (this.mTRBalls[0].getWidth() / 2)) * this.mTiledMapScale)), y - (this.mCenterJetBall_Y + (((float) ((this.mTRBalls[0].getHeight() / 2) - (this.mTRArrow.getHeight() / 2))) * this.mTiledMapScale)));
        int i = 0;
        while (i < this.mSpriteBalls.size()) {
            Ball ball = this.mSpriteBalls.get(i);
            if (ball == null || !ball.isEnable || !ball.isBeJoint) {
                i++;
            } else {
                ball.isBeJoint = false;
                ball.mBodyBall.setType(BodyDef.BodyType.DynamicBody);
                ball.mBodyBall.setLinearVelocity(vector2);
                this.isJoint = false;
                if (CommonConst.GAME_MUSIC_ON) {
                    this.mContext.mResource.playSound(Resource.SOUNDTURE.SOUND_SHOOT);
                }
                this.isNeedUpdateChaseSprite = true;
                return;
            }
        }
    }

    public void handleAccelerometerChangedEvent(AccelerometerData pAccelerometerData) {
        super.onAccelerometerChanged(pAccelerometerData);
        if (this.mSpriteCrab == null || this.mSpriteCrab.isVisible()) {
        }
    }

    private int getTileIndexX(float pX) {
        return (int) (((CommonConst.MAP_TILE_WIDTH / 2.0f) + pX) / CommonConst.MAP_TILE_WIDTH);
    }

    private int getTileIndexY(float pY) {
        return (int) (((CommonConst.MAP_TILE_HEIGHT / 2.0f) + pY) / CommonConst.MAP_TILE_HEIGHT);
    }

    private class Crab extends Sprite {
        private boolean isAccDec = false;
        private boolean isScale = false;
        private float mCurrentScale = GameScene.this.MIN_SCALE;
        private float mInitialScale = GameScene.this.MIN_SCALE;
        private float mOriginalX;
        private float mOriginalY;
        private long mPowerUpdate = 0;
        private long mScaleUpdate = 0;
        private long mSpeedUpdate = 0;

        public Crab(float pX, float pY, float pWidth, float pHeight, TextureRegion pTextureRegion) {
            super(pX, pY, pWidth, pHeight, pTextureRegion);
            this.mOriginalX = pX;
            this.mOriginalY = pY;
            setScale(this.mInitialScale);
        }

        public void resetScale() {
            this.isScale = false;
            this.mCurrentScale = this.mInitialScale;
            setScale(this.mInitialScale);
        }

        /* access modifiers changed from: protected */
        public void onManagedUpdate(float pSecondsElapsed) {
            checkIsScale();
            super.onManagedUpdate(pSecondsElapsed);
        }

        /* access modifiers changed from: private */
        public void setCrabScale(boolean isAdd) {
            float pScale = isAdd ? this.mCurrentScale + GameScene.this.SCALE_STEP : this.mCurrentScale - GameScene.this.SCALE_STEP;
            if (pScale <= GameScene.this.MAX_SCALE && pScale >= GameScene.this.MIN_SCALE) {
                this.isScale = true;
                setScale(pScale);
                this.mCurrentScale = pScale;
                this.mScaleUpdate = System.currentTimeMillis();
            }
        }

        private void checkIsScale() {
            if (this.isScale && System.currentTimeMillis() - this.mScaleUpdate >= Const.CRAB_SCALE_DURATION) {
                this.isScale = false;
                clearShapeModifiers();
                this.mCurrentScale = this.mInitialScale;
                setScale(this.mInitialScale);
            }
        }
    }

    private void setGoldSore(int pGoldenCount) {
        if (PortConst.enableStoreMode) {
            F2FGameActivity f2FGameActivity = this.mContext;
            PersonalAccountTable personalAccountTable = F2FGameActivity.getTableLoad().getmPersonalAccountTable();
            PersonalAccount accout = personalAccountTable.getPersonalAccountList().get(0);
            accout.setGolden_count(accout.getGolden_count() + pGoldenCount);
            try {
                personalAccountTable.write();
            } catch (Exception e) {
                Log.e("f2fError", e.toString());
            }
        }
    }

    public ContextMenuScene getmContextMenuScene() {
        return this.mContextMenuScene;
    }

    public void setDialogInfo(TextureRegion pDialogBtn1, TextureRegion pDialogBtn2, String pTitle, String[] pText, Font pTitleFont, Font[] pTextFont) {
        this.mDialogBtn1 = pDialogBtn1;
        this.mDialogBtn2 = pDialogBtn2;
        this.mTitle = pTitle;
        this.mText = pText;
        this.mTitleFont = pTitleFont;
        this.mTextFont = pTextFont;
    }

    public void setNextLevel() {
        if (CommonConst.GAME_MUSIC_ON) {
            this.mContext.mResource.playMusic(Resource.MUSICTURE.BACKGROUD_MUSIC);
        }
        GoogleAnalyticsUtils.setTracker("play_start_" + String.valueOf(this.mContext.getMGameInfo().getMLevelIndex()) + "_" + String.valueOf(this.mContext.getMGameInfo().getMInsideIndex() + "/previous_level"));
        this.mContext.getMGameInfo().setMInsideIndex(this.insideIndex + 1);
        this.mContext.startNextLevel();
    }

    private void saveGameInfo(boolean isPassLevel2) {
        if (isPassLevel2) {
            if (this.mContext.getMGameInfo().updateScoreByIndex(this.level, this.subLevel, this.insideIndex, this.mScore)) {
                this.mContext.getMGameInfo().updateStarByIndex(this.level, this.subLevel, this.insideIndex, this.mContext.getMGameInfo().getStarClassification(this.mStarLevel, this.mScore));
            }
            if (this.insideIndex + 1 == PortConst.LevelInfo[this.level][0]) {
                this.mContext.getMGameInfo().updateEnableByIndex(this.level, this.subLevel + 1, this.insideIndex, true);
            } else if (this.insideIndex + 1 < PortConst.LevelInfo[this.level][0]) {
                this.mContext.getMGameInfo().updateEnableByIndex(this.level, this.subLevel, this.insideIndex + 1, true);
            }
            this.mContext.setGameInfo();
        }
    }

    private void showGameOverDialog(boolean pIsSucced, boolean isPallAll, int pScoreValue) {
        CommonConst.IS_GAMEOVER = true;
        this.mScoreValue = pScoreValue;
        this.mIsSucced = pIsSucced;
        this.mIsPallAll = isPallAll;
        if (pIsSucced) {
            if (CommonConst.PASS_LEVEL_NUM < CommonConst.PASS_LEVEL_MAXNUM) {
                CommonConst.PASS_LEVEL_NUM++;
            }
            if (this.mContext.getMGameInfo().getStarClassification(this.mStarLevel, this.mScore) == CommonConst.STAR_NUM && PortConst.enableAddGold && PortConst.enableStoreMode) {
                setGoldSore(CommonConst.PASS_LEVEL_BY_HIGH_SCORE);
                this.mContext.showMsgDialog(2);
            } else if (!CommonConst.ENBALE_SHOW_INVITE_DIALOG) {
                showOKDialog();
            } else if (CommonConst.PASS_LEVEL_NUM >= CommonConst.PASS_LEVEL_MAXNUM) {
                CommonConst.ENBALE_SHOW_INVITE_DIALOG = false;
                this.mContext.getMGameInfo().saveGameInfo(this.mContext);
                disableHud();
                setChildScene(new F2FRecommendationDialog(this.mContext.getEngine().getCamera(), this.mContext), false, true, true);
            } else {
                showOKDialog();
            }
        } else {
            showOKDialog();
        }
    }

    public void showScoreGame() {
        showGameOverDialog();
    }

    public void showOKDialog() {
        if (!this.mIsPallAll || !this.mIsSucced || !PortConst.enableScoreGame) {
            showGameOverDialog();
        } else {
            this.mContext.showMsgDialog(12);
        }
    }

    public void showGameOverDialog() {
        boolean z;
        disableHud();
        Camera camera = this.mContext.getEngine().getCamera();
        F2FGameActivity f2FGameActivity = this.mContext;
        if (!this.mIsSucced || this.mIsPallAll) {
            z = false;
        } else {
            z = true;
        }
        setChildScene(new GameOverDialog(camera, f2FGameActivity, z, this.mIsSucced, String.valueOf(this.level + 1) + "-" + String.valueOf(this.insideIndex + 1), this.mContext.getMGameInfo().getMaxLevelScoreByIndex(this.level, this.subLevel, this.insideIndex), this.mScoreValue, false, this.mContext.getMGameInfo().getStarClassification(this.mStarLevel, this.mScore), this.mContext.getMGameInfo().getStarClassification(this.mStarLevel, this.mScore)), false, true, true);
    }

    public void loadGameOverTips() {
        setDialogInfo(this.mTRRelay, this.mTRMenu, this.mStrTitleTips, new String[]{this.mStrDialogScore + String.valueOf(this.mScore), this.mStrGameOver}, this.mFontTextDialogTitle, new Font[]{this.mFontDialog, this.mFontDialog});
        showGameOverDialog(false, true, this.mScore);
        this.mStatus = ChildSceneStatus.GAME_OVER_FAIL;
    }

    public void showHelpDialog() {
        ImageDialog dialog2 = new ImageDialog(2, this.mContext.getEngine().getCamera(), null, 1.0f, this.mContext);
        disableHud();
        setChildScene(dialog2, false, true, true);
        this.mStatus = ChildSceneStatus.GAME_HELP_DIALOG;
    }

    private void showTextDialog() {
        TextDialog dialog = new TextDialog(3, this.mContext.getEngine().getCamera(), this.mTitle, this.mText, this.mTitleFont, this.mTextFont, this.mDialogBtn1, this.mDialogBtn2, this.mContext);
        disableHud();
        setChildScene(dialog, false, true, true);
    }

    public void onMenuBtn() {
        this.isChildSceneClick = true;
        this.isUpdate = true;
        this.isGameOver = true;
        if (this.mIsPallAll) {
            this.isBackToMode = true;
        } else {
            this.isBackToMode = false;
        }
        this.mMenuBackToUp = true;
    }

    public void onReplayBtn() {
        this.isChildSceneClick = true;
        rePlayGame();
    }

    public void onNextLevelBtn() {
        this.isChildSceneClick = true;
        this.isUpdate = true;
        this.isNextLevel = true;
        this.isGameOver = true;
        this.mMenuBackToUp = true;
        this.isBackToMode = false;
    }

    public void onImageDialogCancelBtn() {
        this.isChildSceneClick = true;
        if (this.isFirstPlayGame) {
            showStartHint();
            this.isFirstPlayGame = false;
            return;
        }
        enableHud();
    }

    public void onTextDialogOKBtn() {
        this.isChildSceneClick = true;
        switch (this.mStatus) {
            case GAME_START_DIALOG:
                if (CommonConst.GAME_MUSIC_ON && this.mContext.mResource != null) {
                    this.mContext.mResource.playSound(Resource.SOUNDTURE.SOUND_BEGINE);
                }
                enableHud();
                return;
            case GAME_OVER_FAIL:
                rePlayGame();
                return;
            case GAME_OVER_OK:
                rePlayGame();
                return;
            case GAME_PASSALLLEVEL:
                this.isBackToMode = true;
                this.mMenuBackToUp = true;
                return;
            case GAME_PASSALLLEVEL_LITE:
                rePlayGame();
                return;
            case UNDIFINE:
                enableHud();
                return;
            default:
                return;
        }
    }

    public void onTextDialogCancelBtn() {
        this.isChildSceneClick = true;
        switch (this.mStatus) {
            case GAME_OVER_FAIL:
                this.isBackToMode = false;
                this.mMenuBackToUp = true;
                return;
            case GAME_OVER_OK:
            default:
                return;
            case GAME_PASSALLLEVEL:
                this.isBackToMode = true;
                this.mMenuBackToUp = true;
                return;
            case GAME_PASSALLLEVEL_LITE:
                this.isBackToMode = true;
                this.mMenuBackToUp = true;
                return;
            case UNDIFINE:
                enableHud();
                return;
            case GAME_NEXLLEVEL:
                this.isBackToMode = false;
                this.mMenuBackToUp = true;
                return;
        }
    }

    public void rePlayGame() {
        replayGame();
    }

    public void clearContextMenu() {
        this.mContextMenuScene.clearContextMenu(F2FGameActivity.Status.GAME);
    }

    public void enableHud() {
        if (CommonConst.GAME_MUSIC_ON && this.isTimeLimitedPlaying) {
            this.mContext.mResource.playSound(Resource.SOUNDTURE.SOUND_LIMITED_TIME);
        }
        if (this.mHud != null) {
            this.mHud.setVisible(true);
        }
    }

    public void disableHud() {
        this.mContext.mResource.pauseSound(Resource.SOUNDTURE.SOUND_LIMITED_TIME);
        if (this.mHud != null) {
            this.mHud.setVisible(false);
        }
    }

    public void operSound(boolean isPlay) {
    }

    public void showContextMenu() {
        disableHud();
        if (this.mContextMenuScene == null) {
            this.mContextMenuScene = new ContextMenuScene(this.mContext.getEngine().getCamera(), this.mContext, null, true, true);
            setChildScene(this.mContextMenuScene, false, true, true);
            return;
        }
        this.mContextMenuScene.loadScene(false);
        setChildScene(this.mContextMenuScene, false, true, true);
    }

    public void backToUpMenu() {
        this.isGameOver = true;
        this.mMenuBackToUp = true;
        this.isBackToMode = false;
    }

    public void back() {
        CommonConst.PASS_LEVEL_NUM = 0;
        if (CommonConst.GAME_MUSIC_ON) {
            this.mContext.mResource.playMusic(Resource.MUSICTURE.BACKGROUD_MUSIC);
        }
        disableHud();
        if (this.isReplay) {
            this.mContext.startNextLevel();
        } else if (this.isNextLevel) {
            setNextLevel();
        } else {
            this.mContext.setStatus(F2FGameActivity.Status.SUBLEVEL_OPTION);
            this.mContext.resetGameScene();
        }
    }
}
