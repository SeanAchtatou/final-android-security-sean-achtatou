package com.finger2finger.games.scene;

import com.f2fgames.games.monkeybreak.lite.R;
import com.finger2finger.games.common.CommonConst;
import com.finger2finger.games.common.MainMenuButtons;
import com.finger2finger.games.common.activity.F2FGameActivity;
import com.finger2finger.games.common.scene.F2FScene;
import com.finger2finger.games.res.Const;
import com.finger2finger.games.res.PortConst;
import com.finger2finger.games.res.Resource;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.Text;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.texture.region.TextureRegion;

public class MainMenuScene extends F2FScene {
    private Font mFontPlay;
    private MainMenuButtons mMainMenuButtons;
    private String mStrPlay;

    public MainMenuScene(F2FGameActivity pContext) {
        super(4, pContext);
        loadScene();
    }

    public void loadScene() {
        loadResources();
        initBackgroudScene(this);
        initPlayButton(this, 1);
        initMenuSprites();
        initialBall();
    }

    private void initialBall() {
        TextureRegion tr = this.mContext.mResource.getTextureRegionByKey(Resource.TEXTURE.MENU_BALL.mKey);
        Ball ball = new Ball((float) (Const.CAMERA_WIDTH / 4), (float) (Const.CAMERA_HEIGHT / 2), CommonConst.RALE_SAMALL_VALUE * ((float) tr.getWidth()), CommonConst.RALE_SAMALL_VALUE * ((float) tr.getHeight()), tr);
        ball.setVelocity(200.0f, 200.0f);
        getBottomLayer().addEntity(ball);
    }

    private void initMenuSprites() {
        this.mMainMenuButtons = new MainMenuButtons(this.mContext, this, 2);
        this.mMainMenuButtons.createButtons();
    }

    private void loadResources() {
        loadFont();
        loadStringsFromXml();
    }

    private void loadFont() {
        this.mFontPlay = this.mContext.mResource.getBaseFontByKey(Resource.FONT.MAINMENU_PLAY.mKey);
    }

    private void loadStringsFromXml() {
        this.mStrPlay = this.mContext.getResources().getString(R.string.str_startGame);
    }

    private void initBackgroudScene(Scene pScene) {
        pScene.getBottomLayer().addEntity(new Sprite(0.0f, -25.0f * CommonConst.RALE_SAMALL_VALUE, (float) CommonConst.CAMERA_WIDTH, ((float) CommonConst.CAMERA_HEIGHT) + (25.0f * CommonConst.RALE_SAMALL_VALUE), this.mContext.mResource.getTextureRegionByKey(Resource.TEXTURE.GAME_BACKGROUND.mKey)));
    }

    private void initPlayButton(Scene pScene, int pLayerIndex) {
        TextureRegion gameTitleRegion = this.mContext.mResource.getTextureRegionByKey(Resource.TEXTURE.GAME_MAINMENU_TITLE.mKey);
        float titleWidth = ((float) gameTitleRegion.getWidth()) * CommonConst.RALE_SAMALL_VALUE;
        float titleHeight = ((float) gameTitleRegion.getHeight()) * CommonConst.RALE_SAMALL_VALUE;
        float titlePX = (((float) CommonConst.CAMERA_WIDTH) - titleWidth) / 2.0f;
        float titlePY = ((((float) CommonConst.CAMERA_HEIGHT) - titleHeight) / 2.0f) - (60.0f * CommonConst.RALE_SAMALL_VALUE);
        pScene.getLayer(pLayerIndex).addEntity(new Sprite(titlePX, titlePY, titleWidth, titleHeight, gameTitleRegion));
        TextureRegion startGameRegion = this.mContext.mResource.getTextureRegionByKey(Resource.TEXTURE.MAINMENU_PLAY_BLANK.mKey);
        float bntPlayWidth = ((float) startGameRegion.getWidth()) * CommonConst.RALE_SAMALL_VALUE;
        float bntPlayHeight = ((float) startGameRegion.getHeight()) * CommonConst.RALE_SAMALL_VALUE;
        float bntPlayPX = (((float) CommonConst.CAMERA_WIDTH) - bntPlayWidth) / 2.0f;
        float bntPlayPY = (25.0f * CommonConst.RALE_SAMALL_VALUE) + titlePY + titleHeight;
        Sprite startGame = new Sprite(bntPlayPX, bntPlayPY, bntPlayWidth, bntPlayHeight, startGameRegion) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() != 0) {
                    return true;
                }
                if (PortConst.enableLevelOption) {
                    MainMenuScene.this.mContext.setStatus(F2FGameActivity.Status.LEVEL_OPTION);
                    return true;
                }
                MainMenuScene.this.mContext.setStatus(F2FGameActivity.Status.SUBLEVEL_OPTION);
                return true;
            }
        };
        pScene.getLayer(pLayerIndex).addEntity(startGame);
        pScene.getLayer(pLayerIndex).registerTouchArea(startGame);
        Text text = new Text(0.0f, 0.0f, this.mFontPlay, this.mStrPlay);
        text.setPosition((((((float) startGameRegion.getWidth()) * CommonConst.RALE_SAMALL_VALUE) / 2.0f) + bntPlayPX) - (text.getWidth() / 2.0f), (((((float) startGameRegion.getHeight()) * CommonConst.RALE_SAMALL_VALUE) / 2.0f) + bntPlayPY) - (text.getHeight() / 2.0f));
        getLayer(pLayerIndex).addEntity(text);
    }

    private class Ball extends Sprite {
        public Ball(float pX, float pY, float pWidth, float pHeight, TextureRegion pTextureRegion) {
            super(pX, pY, pWidth, pHeight, pTextureRegion);
        }

        /* access modifiers changed from: protected */
        public void onManagedUpdate(float pSecondsElapsed) {
            boolean isCollision = false;
            if (this.mX < 0.0f) {
                setVelocityX(Const.RALE_SAMALL_VALUE * 200.0f);
                isCollision = true;
            } else if (this.mX + getWidth() > ((float) Const.CAMERA_WIDTH)) {
                setVelocityX(Const.RALE_SAMALL_VALUE * -200.0f);
                isCollision = true;
            }
            if (this.mY < 0.0f) {
                setVelocityY(Const.RALE_SAMALL_VALUE * 200.0f);
                isCollision = true;
            } else if (this.mY + getHeight() > ((float) Const.CAMERA_HEIGHT)) {
                setVelocityY(Const.RALE_SAMALL_VALUE * -200.0f);
                isCollision = true;
            }
            if (isCollision) {
            }
            super.onManagedUpdate(pSecondsElapsed);
        }
    }
}
