package com.finger2finger.games.res;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.Log;
import com.finger2finger.games.common.CommonConst;
import com.finger2finger.games.common.Utils;
import com.finger2finger.games.common.activity.F2FGameActivity;
import com.finger2finger.games.common.base.BaseFont;
import com.finger2finger.games.common.base.BaseFontFactory;
import com.finger2finger.games.common.base.BaseStrokeFont;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.anddev.andengine.audio.music.Music;
import org.anddev.andengine.audio.music.MusicFactory;
import org.anddev.andengine.audio.sound.Sound;
import org.anddev.andengine.audio.sound.SoundFactory;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXLoader;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXTileSet;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXTiledMap;
import org.anddev.andengine.entity.layer.tiled.tmx.util.exception.TMXLoadException;
import org.anddev.andengine.opengl.buffer.BufferObjectManager;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;

public class Resource {
    public F2FGameActivity _context;
    public Engine _engine;
    public boolean enableReleaseResource = false;
    public boolean loadCommonResourceReady = false;
    public boolean loadGameResourceReady = false;
    public boolean loadMainMenuResourceReady = false;
    private HashMap<Integer, TextureRegion[]> msArrayTextureRegions = new HashMap<>();
    private HashMap<Integer, TiledTextureRegion[]> msArrayTiledTextureRegions = new HashMap<>();
    private HashMap<Integer, Bitmap> msBitmap = new HashMap<>();
    private HashMap<Integer, Drawable> msDrawable = new HashMap<>();
    private HashMap<Integer, BaseFont> msFont = new HashMap<>();
    private HashMap<Integer, Music> msMusic = new HashMap<>();
    private HashMap<Integer, Sound> msSound = new HashMap<>();
    private HashMap<Integer, BaseStrokeFont> msStrokeFont = new HashMap<>();
    private HashMap<Integer, TextureRegion> msTextureRegions = new HashMap<>();
    private HashMap<Integer, TiledTextureRegion> msTiledTextureRegions = new HashMap<>();
    private HashMap<Integer, TMXTiledMap> msTmxTiledMap = new HashMap<>();

    public static final class drawable {
        public static final int facebook_icon = 2130837508;
    }

    public static final class string {
        public static final int exit_game_dialog_content = 2131034121;
        public static final int exit_game_dialog_no = 2131034115;
        public static final int exit_game_dialog_title = 2131034217;
        public static final int exit_game_dialog_yes = 2131034114;
        public static final int language = 2131034210;
        public static final int str_dilaog_fail = 2131034130;
        public static final int str_dilaog_highscore = 2131034127;
        public static final int str_dilaog_passlevel = 2131034128;
        public static final int str_dilaog_score = 2131034129;
    }

    public Resource(Engine engine, F2FGameActivity context) {
        this._engine = engine;
        this._context = context;
    }

    public void loadMainMenuResource() {
        loadMenuTextures();
        this.loadMainMenuResourceReady = true;
    }

    public void loadGameResource() {
        loadGameTextures();
        this.loadGameResourceReady = true;
    }

    public void loadCommonResource() {
        if (!this.loadCommonResourceReady) {
            loadFont();
            loadStrokeFont();
            loadMusic();
            loadSound();
            loadBitMap();
            this.loadCommonResourceReady = true;
        }
    }

    public void startLoad() {
        new Thread(new Runnable() {
            public void run() {
                Resource.this.loadCommonResource();
            }
        }).start();
    }

    public void startLoadCommonResource() {
        if (this.enableReleaseResource) {
            new Thread(new Runnable() {
                public void run() {
                    Resource.this.loadCommonResource();
                }
            }).start();
        }
    }

    public void startLoadMainMenuResource() {
        if (this.enableReleaseResource) {
            new Thread(new Runnable() {
                public void run() {
                    Resource.this.releaseGameResource();
                    Resource.this.loadMainMenuResource();
                }
            }).start();
        }
    }

    public void startLoadGameResource() {
        if (this.enableReleaseResource) {
            new Thread(new Runnable() {
                public void run() {
                    Resource.this.releaseMainMenuResource();
                    Resource.this.releaseGameResource();
                    Resource.this.loadGameResource();
                }
            }).start();
        }
    }

    public void releaseLogoResource() {
    }

    public void releaseMainMenuResource() {
    }

    public void releaseGameResource() {
    }

    public void releaseResource() {
        this.msTextureRegions = null;
        this.msArrayTextureRegions = null;
        this.msTiledTextureRegions = null;
        this.msArrayTiledTextureRegions = null;
        this.msMusic = null;
        this.msSound = null;
        this.msFont = null;
        this.msStrokeFont = null;
        this.msBitmap = null;
        this.msDrawable = null;
        this.msTmxTiledMap = null;
    }

    public BaseFont getBaseFontByKey(int key) {
        return this.msFont.get(Integer.valueOf(key));
    }

    public BaseStrokeFont getStrokeFontByKey(int key) {
        return this.msStrokeFont.get(Integer.valueOf(key));
    }

    public TextureRegion getTextureRegionByKey(int key) {
        return this.msTextureRegions.get(Integer.valueOf(key));
    }

    public TiledTextureRegion getTiledTextureRegionByKey(int key) {
        return this.msTiledTextureRegions.get(Integer.valueOf(key));
    }

    public TiledTextureRegion[] getArrayTiledTextureRegionByKey(int key) {
        return this.msArrayTiledTextureRegions.get(Integer.valueOf(key));
    }

    public TextureRegion[] getArrayTextureRegionByKey(int key) {
        return this.msArrayTextureRegions.get(Integer.valueOf(key));
    }

    public Bitmap getBitmapByKey(int key) {
        return this.msBitmap.get(Integer.valueOf(key));
    }

    public Drawable getDrawableByKey(int key) {
        return this.msDrawable.get(Integer.valueOf(key));
    }

    public Sound getSoundByKey(int key) {
        return this.msSound.get(Integer.valueOf(key));
    }

    public Music getMusicByKey(int key) {
        return this.msMusic.get(Integer.valueOf(key));
    }

    public TMXTiledMap getTmxTiledMapByKey(int key) {
        return this.msTmxTiledMap.get(Integer.valueOf(key));
    }

    public void unloadMenuTextures() {
        BufferObjectManager.getActiveInstance().clear();
        this._engine.getTextureManager().unloadTexture(this.msTextureRegions.get(Integer.valueOf(TEXTURE.GAME_BACKGROUND.mKey)).getTexture());
        this._engine.getTextureManager().unloadTexture(this.msTextureRegions.get(Integer.valueOf(TEXTURE.GAME_MAINMENU_TITLE.mKey)).getTexture());
    }

    public void unloadGameTextures() {
        BufferObjectManager.getActiveInstance().clear();
        ArrayList<TMXTileSet> tmxTileSet = this.msTmxTiledMap.get(Integer.valueOf(TILEDMAP.TILEDMAP.mKey)).getTMXTileSets();
        for (int i = 0; i < tmxTileSet.size(); i++) {
            this._engine.getTextureManager().unloadTexture(tmxTileSet.get(i).getTexture());
        }
        this._engine.getTextureManager().unloadTexture(this.msTextureRegions.get(Integer.valueOf(TEXTURE.GAME_CRAB.mKey)).getTexture());
        this._engine.getTextureManager().unloadTexture(this.msTiledTextureRegions.get(Integer.valueOf(TILEDTURE.GAME_EXPLOSION.mKey)).getTexture());
        this._engine.getTextureManager().unloadTexture(this.msArrayTiledTextureRegions.get(Integer.valueOf(TILEDTURE.GAME_BRICKS.mKey))[0].getTexture());
        this._engine.getTextureManager().unloadTexture(this.msArrayTextureRegions.get(Integer.valueOf(TEXTURE.GAME_BALL.mKey))[0].getTexture());
        this._engine.getTextureManager().unloadTexture(this.msTextureRegions.get(Integer.valueOf(TEXTURE.GAME_CRAB_ENLARGE.mKey)).getTexture());
        this._engine.getTextureManager().unloadTexture(this.msTextureRegions.get(Integer.valueOf(TEXTURE.GAME_CRAB_ENLARGE.mKey)).getTexture());
        this._engine.getTextureManager().unloadTexture(this.msArrayTextureRegions.get(Integer.valueOf(TEXTURE.GAME_GOLD.mKey))[0].getTexture());
        this._engine.getTextureManager().unloadTexture(this.msTextureRegions.get(Integer.valueOf(TEXTURE.GAME_SCORE.mKey)).getTexture());
    }

    public void loadMenuTextures() {
        Texture texture = new Texture(1024, 512, TextureOptions.DEFAULT);
        this.msTextureRegions.put(Integer.valueOf(TEXTURE.GAME_BACKGROUND.mKey), TextureRegionFactory.createFromAsset(texture, this._context, TEXTURE.GAME_BACKGROUND.mValue, 0, 0));
        this._engine.getTextureManager().loadTextures(texture);
        Texture gameTitleTexture = new Texture(1024, 256, TextureOptions.DEFAULT);
        this.msTextureRegions.put(Integer.valueOf(TEXTURE.GAME_MAINMENU_TITLE.mKey), TextureRegionFactory.createFromAsset(gameTitleTexture, this._context, Utils.getImageName(TEXTURE.GAME_MAINMENU_TITLE.mValue), 0, 0));
        this.msTextureRegions.put(Integer.valueOf(TEXTURE.MAINMENU_PLAY_BLANK.mKey), TextureRegionFactory.createFromAsset(gameTitleTexture, this._context, TEXTURE.MAINMENU_PLAY_BLANK.mValue, 800, 0));
        this.msTextureRegions.put(Integer.valueOf(TEXTURE.MENU_BALL.mKey), TextureRegionFactory.createFromAsset(gameTitleTexture, this._context, TEXTURE.MENU_BALL.mValue, 982, 0));
        this._engine.getTextureManager().loadTextures(gameTitleTexture);
    }

    public void loadGameTextures() {
        loadTmxTiledMap();
        Texture mCrabTexture = new Texture(256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.msTextureRegions.put(Integer.valueOf(TEXTURE.GAME_CRAB.mKey), TextureRegionFactory.createFromAsset(mCrabTexture, this._context, TEXTURE.GAME_CRAB.mValue, 0, 0));
        this.msTextureRegions.put(Integer.valueOf(TEXTURE.GAME_BOMB.mKey), TextureRegionFactory.createFromAsset(mCrabTexture, this._context, TEXTURE.GAME_BOMB.mValue, 128, 0));
        this.msTiledTextureRegions.put(Integer.valueOf(TILEDTURE.GAME_POWERSTONE.mKey), TextureRegionFactory.createTiledFromAsset(mCrabTexture, this._context, TILEDTURE.GAME_POWERSTONE.mValue, 0, 128, 4, 1));
        Texture mTextureExplosion = new Texture(1024, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.msTiledTextureRegions.put(Integer.valueOf(TILEDTURE.GAME_EXPLOSION.mKey), TextureRegionFactory.createTiledFromAsset(mTextureExplosion, this._context, TILEDTURE.GAME_EXPLOSION.mValue, 0, 0, 9, 1));
        this.msTiledTextureRegions.put(Integer.valueOf(TILEDTURE.GAME_CLEAR.mKey), TextureRegionFactory.createTiledFromAsset(mTextureExplosion, this._context, TILEDTURE.GAME_CLEAR.mValue, 0, 128, 9, 1));
        Texture mTextureBricks = new Texture(1024, 64, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        TiledTextureRegion[] bricks = new TiledTextureRegion[7];
        for (int i = 0; i < 7; i++) {
            bricks[i] = TextureRegionFactory.createTiledFromAsset(mTextureBricks, this._context, String.format(TILEDTURE.GAME_BRICKS.mValue, Integer.valueOf(i + 1)), i * 144, 0, 3, 1);
        }
        this.msArrayTiledTextureRegions.put(Integer.valueOf(TILEDTURE.GAME_BRICKS.mKey), bricks);
        Texture texture = new Texture(128, 64, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        TextureRegion[] mTRBalls = new TextureRegion[1];
        for (int i2 = 0; i2 < 1; i2++) {
            mTRBalls[i2] = TextureRegionFactory.createFromAsset(texture, this._context, String.format(TEXTURE.GAME_BALL.mValue, Integer.valueOf(i2 + 1)), i2 * 32, 0);
        }
        this.msArrayTextureRegions.put(Integer.valueOf(TEXTURE.GAME_BALL.mKey), mTRBalls);
        Texture texture2 = new Texture(512, 128, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.msTextureRegions.put(Integer.valueOf(TEXTURE.GAME_CRAB_ENLARGE.mKey), TextureRegionFactory.createFromAsset(texture2, this._context, TEXTURE.GAME_CRAB_ENLARGE.mValue, 0, 0));
        this.msTextureRegions.put(Integer.valueOf(TEXTURE.GAME_CREATEBALL.mKey), TextureRegionFactory.createFromAsset(texture2, this._context, TEXTURE.GAME_CREATEBALL.mValue, 128, 0));
        this.msTextureRegions.put(Integer.valueOf(TEXTURE.GAME_CRAB_ACC.mKey), TextureRegionFactory.createFromAsset(texture2, this._context, TEXTURE.GAME_CRAB_ACC.mValue, 192, 0));
        this.msTextureRegions.put(Integer.valueOf(TEXTURE.GAME_CRAB_DEC.mKey), TextureRegionFactory.createFromAsset(texture2, this._context, TEXTURE.GAME_CRAB_DEC.mValue, 256, 0));
        TextureRegion[] mTRGolds = new TextureRegion[3];
        for (int i3 = 0; i3 < 3; i3++) {
            mTRGolds[i3] = TextureRegionFactory.createFromAsset(texture2, this._context, String.format(TEXTURE.GAME_GOLD.mValue, Integer.valueOf(i3 + 1)), (i3 * 64) + 320, 0);
        }
        this.msArrayTextureRegions.put(Integer.valueOf(TEXTURE.GAME_GOLD.mKey), mTRGolds);
        Texture mHudTexture = new Texture(512, 128, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.msTextureRegions.put(Integer.valueOf(TEXTURE.GAME_SCORE.mKey), TextureRegionFactory.createFromAsset(mHudTexture, this._context, TEXTURE.GAME_SCORE.mValue, 0, 0));
        this.msTextureRegions.put(Integer.valueOf(TEXTURE.BALL_POWER_BAR.mKey), TextureRegionFactory.createFromAsset(mHudTexture, this._context, TEXTURE.BALL_POWER_BAR.mValue, 160, 0));
        this.msTiledTextureRegions.put(Integer.valueOf(TILEDTURE.GAME_ARROW.mKey), TextureRegionFactory.createTiledFromAsset(mHudTexture, this._context, TILEDTURE.GAME_ARROW.mValue, 0, 64, 4, 1));
        this._engine.getTextureManager().loadTextures(mHudTexture, mCrabTexture, texture, mTextureExplosion, texture2, mTextureBricks);
    }

    public void loadTmxTiledMap() {
        try {
            TMXTiledMap mTMXTiledMap = new TMXLoader(this._context, this._context.getEngine().getTextureManager(), TextureOptions.DEFAULT).loadFromAsset(this._context, String.format(TILEDMAP.TILEDMAP.mValue, Integer.valueOf(this._context.getMGameInfo().getMLevelIndex() + 1), Integer.valueOf(this._context.getMGameInfo().getMInsideIndex() + 1)));
            this.msTmxTiledMap.put(Integer.valueOf(TILEDMAP.TILEDMAP.mKey), mTMXTiledMap);
            this.msTmxTiledMap.put(Integer.valueOf(TILEDMAP.TILEDMAP.mKey), mTMXTiledMap);
        } catch (TMXLoadException e) {
            Log.e("Resource", e.getMessage());
        }
    }

    public void loadMusic() {
        try {
            this.msMusic.put(Integer.valueOf(MUSICTURE.BACKGROUD_MUSIC.mKey), MusicFactory.createMusicFromAsset(this._engine.getMusicManager(), this._context, MUSICTURE.BACKGROUD_MUSIC.mValue));
            this.msMusic.get(Integer.valueOf(MUSICTURE.BACKGROUD_MUSIC.mKey)).setLooping(true);
        } catch (Exception e) {
            Log.e("Resource", e.toString());
        }
    }

    public void loadSound() {
        try {
            this.msSound.put(Integer.valueOf(SOUNDTURE.BOMB_SOUND.mKey), SoundFactory.createSoundFromAsset(this._engine.getSoundManager(), this._context, SOUNDTURE.BOMB_SOUND.mValue));
            this.msSound.put(Integer.valueOf(SOUNDTURE.GAMEOVER_SOUND.mKey), SoundFactory.createSoundFromAsset(this._engine.getSoundManager(), this._context, SOUNDTURE.GAMEOVER_SOUND.mValue));
            this.msSound.put(Integer.valueOf(SOUNDTURE.SOUND_HITWALL.mKey), SoundFactory.createSoundFromAsset(this._engine.getSoundManager(), this._context, SOUNDTURE.SOUND_HITWALL.mValue));
            this.msSound.put(Integer.valueOf(SOUNDTURE.SOUND_HITBLOCK1.mKey), SoundFactory.createSoundFromAsset(this._engine.getSoundManager(), this._context, SOUNDTURE.SOUND_HITBLOCK1.mValue));
            this.msSound.put(Integer.valueOf(SOUNDTURE.SOUND_HITBLOCK2.mKey), SoundFactory.createSoundFromAsset(this._engine.getSoundManager(), this._context, SOUNDTURE.SOUND_HITBLOCK2.mValue));
            this.msSound.put(Integer.valueOf(SOUNDTURE.SOUND_HITBLOCK3.mKey), SoundFactory.createSoundFromAsset(this._engine.getSoundManager(), this._context, SOUNDTURE.SOUND_HITBLOCK3.mValue));
            this.msSound.put(Integer.valueOf(SOUNDTURE.SOUND_HITPEARL.mKey), SoundFactory.createSoundFromAsset(this._engine.getSoundManager(), this._context, SOUNDTURE.SOUND_HITPEARL.mValue));
            this.msSound.put(Integer.valueOf(SOUNDTURE.SOUND_NOPASS.mKey), SoundFactory.createSoundFromAsset(this._engine.getSoundManager(), this._context, SOUNDTURE.SOUND_NOPASS.mValue));
            this.msSound.put(Integer.valueOf(SOUNDTURE.SOUND_GET_STONE.mKey), SoundFactory.createSoundFromAsset(this._engine.getSoundManager(), this._context, SOUNDTURE.SOUND_GET_STONE.mValue));
            this.msSound.put(Integer.valueOf(SOUNDTURE.SOUND_BEGINE.mKey), SoundFactory.createSoundFromAsset(this._engine.getSoundManager(), this._context, SOUNDTURE.SOUND_BEGINE.mValue));
            this.msSound.put(Integer.valueOf(SOUNDTURE.SOUND_SHOOT.mKey), SoundFactory.createSoundFromAsset(this._engine.getSoundManager(), this._context, SOUNDTURE.SOUND_SHOOT.mValue));
            this.msSound.put(Integer.valueOf(SOUNDTURE.SOUND_PASSLEVEL.mKey), SoundFactory.createSoundFromAsset(this._engine.getSoundManager(), this._context, SOUNDTURE.SOUND_PASSLEVEL.mValue));
            this.msSound.put(Integer.valueOf(SOUNDTURE.SOUND_GOLD.mKey), SoundFactory.createSoundFromAsset(this._engine.getSoundManager(), this._context, SOUNDTURE.SOUND_GOLD.mValue));
        } catch (Exception e) {
            Log.e("Resource", e.toString());
        }
    }

    public void loadBitMap() {
        try {
            this.msBitmap.put(Integer.valueOf(BITMAP.FACEBOOK.mKey), BitmapFactory.decodeStream(this._context.getAssets().open(BITMAP.FACEBOOK.mValue)));
        } catch (IOException e) {
            Log.e("Resource", e.toString());
        }
    }

    public void loadFont() {
        BaseFontFactory.setAssetBasePath("fonts/");
        Texture texture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        BaseFont bf = new BaseFont(texture, Typeface.create(Typeface.DEFAULT, 1), 20.0f, true, -65536, CommonConst.RALE_SAMALL_VALUE);
        this.msFont.put(Integer.valueOf(FONT.SUBLEVEL_SCORE.mKey), bf);
        Texture texture2 = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        BaseFont mFontTextDialogTitle = new BaseFont(texture2, Typeface.create(Typeface.DEFAULT, 1), 35.0f, true, -1, CommonConst.RALE_SAMALL_VALUE);
        this.msFont.put(Integer.valueOf(FONT.DIALOG_TITLE.mKey), mFontTextDialogTitle);
        Texture texture3 = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        BaseFont mFontHud = new BaseFont(texture3, Typeface.create(Typeface.DEFAULT, 1), 25.0f, true, -16711936, CommonConst.RALE_SAMALL_VALUE);
        this.msFont.put(Integer.valueOf(FONT.HUD_CONTEXT.mKey), mFontHud);
        Texture texture4 = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        BaseFont mFontDialog = new BaseFont(texture4, Typeface.create(Typeface.DEFAULT, 1), 25.0f, true, -1, CommonConst.RALE_SAMALL_VALUE);
        this.msFont.put(Integer.valueOf(FONT.DIALOG_CONTEXT.mKey), mFontDialog);
        this._engine.getTextureManager().loadTextures(texture, texture3, texture2, texture4);
        this._engine.getFontManager().loadFonts(bf, mFontHud, mFontTextDialogTitle, mFontDialog);
        Texture texture5 = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        BaseFont mFontGetScore = BaseFontFactory.createFromAsset(texture5, this._context, FontConst.FONT_COLLISION_HINT, 35.0f, true, -65536, CommonConst.RALE_SAMALL_VALUE);
        this.msFont.put(Integer.valueOf(FONT.GAMEIN_SCORE.mKey), mFontGetScore);
        Texture texture6 = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        BaseFont mFontAbout_Title = BaseFontFactory.createFromAsset(texture6, this._context, FontConst.START_FONT_ABOUT_TITLE, 30.0f, true, -1, CommonConst.RALE_SAMALL_VALUE);
        this.msFont.put(Integer.valueOf(FONT.MAINMENU_ABOUTTITLE.mKey), mFontAbout_Title);
        Texture texture7 = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        BaseFont mFontAbout = BaseFontFactory.createFromAsset(texture7, this._context, FontConst.START_FONT_ABOUT, 18.0f, true, -1, CommonConst.RALE_SAMALL_VALUE);
        this.msFont.put(Integer.valueOf(FONT.MAINMENU_ABOUTCONTENT.mKey), mFontAbout);
        Texture texture8 = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        BaseFont mFontPlay = BaseFontFactory.createFromAsset(texture8, this._context, FontConst.START_FONT_PLAY, 40.0f, true, -1, CommonConst.RALE_SAMALL_VALUE);
        this.msFont.put(Integer.valueOf(FONT.MAINMENU_PLAY.mKey), mFontPlay);
        Texture mTextureLevelDescrib = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        BaseFont mFontLevelDescrib = BaseFontFactory.createFromAsset(mTextureLevelDescrib, this._context, FontConst.LEVEL_DESCRIB_FONT, 35.0f, true, -65536, CommonConst.RALE_SAMALL_VALUE);
        this.msFont.put(Integer.valueOf(FONT.LEVELDESCRIB.mKey), mFontLevelDescrib);
        this._engine.getTextureManager().loadTextures(texture7, texture6, texture8, mTextureLevelDescrib, texture5);
        this._engine.getFontManager().loadFonts(mFontAbout, mFontAbout_Title, mFontPlay, mFontLevelDescrib, mFontGetScore);
    }

    public void loadStrokeFont() {
        BaseFontFactory.setAssetBasePath("fonts/");
        Texture dialogTitleTexture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        BaseStrokeFont dialogTitle = BaseFontFactory.createStrokeFromAsset(dialogTitleTexture, this._context, FontConst.DIALOG_TITLE_FONT, 40.0f, true, FontConst.DIALOG_TITLE_IN_COLOR, 2, -16777216, false, CommonConst.RALE_SAMALL_VALUE);
        this.msStrokeFont.put(Integer.valueOf(STROKEFONT.DIALOG_TITLE.mKey), dialogTitle);
        this._engine.getTextureManager().loadTexture(dialogTitleTexture);
        this._engine.getFontManager().loadFont(dialogTitle);
        Texture dialogTipTexture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        BaseStrokeFont dialogTip = BaseFontFactory.createStrokeFromAsset(dialogTipTexture, this._context, FontConst.DIALOG_TIP_FONT, 35.0f, true, -16777216, 2, -1, false, CommonConst.RALE_SAMALL_VALUE);
        this.msStrokeFont.put(Integer.valueOf(STROKEFONT.DIALOG_TIP.mKey), dialogTip);
        this._engine.getTextureManager().loadTexture(dialogTipTexture);
        this._engine.getFontManager().loadFont(dialogTip);
        Texture dialogContentTexture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        BaseStrokeFont dialogContent = BaseFontFactory.createStrokeFromAsset(dialogContentTexture, this._context, FontConst.DIALOG_CONTENT_FONT, 35.0f, true, -1, 2, -16777216, false, CommonConst.RALE_SAMALL_VALUE);
        this.msStrokeFont.put(Integer.valueOf(STROKEFONT.DIALOG_CONTENT.mKey), dialogContent);
        this._engine.getTextureManager().loadTexture(dialogContentTexture);
        this._engine.getFontManager().loadFont(dialogContent);
    }

    public enum MUSICTURE {
        BACKGROUD_MUSIC(0, "audio/music/backmusic.mp3");
        
        public final int mKey;
        public final String mValue;

        private MUSICTURE(int key, String value) {
            this.mKey = key;
            this.mValue = value;
        }
    }

    public enum SOUNDTURE {
        BOMB_SOUND(0, "audio/sound/bomb.mp3"),
        GAMEOVER_SOUND(1, "audio/sound/gameover.mp3"),
        SOUND_HITWALL(2, "audio/sound/hitwall.mp3"),
        SOUND_GOLD(3, "audio/sound/gold.mp3"),
        SOUND_LIMITED_TIME(4, "audio/sound/time.mp3"),
        SOUND_GET_STONE(5, "audio/sound/stone.mp3"),
        SOUND_BEGINE(6, "audio/sound/begine.mp3"),
        SOUND_PASSLEVEL(7, "audio/sound/passlevel.mp3"),
        SOUND_NOPASS(8, "audio/sound/nopass.mp3"),
        SOUND_SHOOT(9, "audio/sound/shoot.mp3"),
        SOUND_HITBLOCK1(10, "audio/sound/hitblock01.mp3"),
        SOUND_HITBLOCK2(11, "audio/sound/hitblock02.mp3"),
        SOUND_HITBLOCK3(12, "audio/sound/hitblock03.mp3"),
        SOUND_HITPEARL(13, "audio/sound/hitpearl.mp3");
        
        public final int mKey;
        public final String mValue;

        private SOUNDTURE(int key, String value) {
            this.mKey = key;
            this.mValue = value;
        }
    }

    public void playMusic(MUSICTURE pMusicType) {
        if (this.msMusic.get(Integer.valueOf(pMusicType.mKey)) != null && !this.msMusic.get(Integer.valueOf(pMusicType.mKey)).isPlaying()) {
            this.msMusic.get(Integer.valueOf(pMusicType.mKey)).resume();
        }
    }

    public void pauseMusic(MUSICTURE pMusicType) {
        if (this.msMusic.get(Integer.valueOf(pMusicType.mKey)) != null && this.msMusic.get(Integer.valueOf(pMusicType.mKey)).isPlaying()) {
            this.msMusic.get(Integer.valueOf(pMusicType.mKey)).pause();
        }
    }

    public void realseMusic() {
        for (Map.Entry<Integer, Music> entry : this.msMusic.entrySet()) {
            ((Music) entry.getValue()).stop();
            ((Music) entry.getValue()).release();
        }
    }

    public void realseSound() {
        for (Map.Entry<Integer, Sound> entry : this.msSound.entrySet()) {
            ((Sound) entry.getValue()).stop();
            ((Sound) entry.getValue()).release();
        }
    }

    public void playSound(SOUNDTURE pSoundType) {
        if (this.msSound.get(Integer.valueOf(pSoundType.mKey)) != null) {
            this.msSound.get(Integer.valueOf(pSoundType.mKey)).play();
        }
    }

    public void pauseSound(SOUNDTURE pSoundType) {
        if (this.msSound.get(Integer.valueOf(pSoundType.mKey)) != null) {
            this.msSound.get(Integer.valueOf(pSoundType.mKey)).pause();
        }
    }

    public enum FONT {
        SUBLEVEL_SCORE(0),
        DIALOG_TITLE(1),
        DIALOG_CONTEXT(2),
        HUD_CONTEXT(3),
        MAINMENU_ABOUTTITLE(4),
        MAINMENU_ABOUTCONTENT(5),
        MAINMENU_PLAY(6),
        LEVELDESCRIB(7),
        DEFOUT_FONT(9),
        GAMEIN_SCORE(10);
        
        public final int mKey;

        private FONT(int key) {
            this.mKey = key;
        }
    }

    public enum STROKEFONT {
        MAINMENU_TITLE(0),
        DIALOG_TITLE(1),
        DIALOG_TIP(2),
        DIALOG_CONTENT(3),
        GAME_POINT_FONT(4);
        
        public final int mKey;

        private STROKEFONT(int key) {
            this.mKey = key;
        }
    }

    public enum TILEDTURE {
        GAME_POWERSTONE(101, "textures/gameentity/powerstone.png"),
        GAME_EXPLOSION(102, "textures/gameentity/explosion.png"),
        TILED_POINT(2, "textures/background/tiledpoint.png"),
        GAME_BRICKS(103, "textures/gameentity/brick%02d.png"),
        GAME_CLEAR(104, "textures/gameentity/clear.png"),
        GAME_ARROW(219, "textures/gameentity/arrow.png");
        
        public final int mKey;
        public final String mValue;

        private TILEDTURE(int key, String value) {
            this.mKey = key;
            this.mValue = value;
        }
    }

    public enum TEXTURE {
        GAME_BACKGROUND(3, "textures/background/background.png"),
        GAME_MAINMENU_TITLE(5, "textures/titles/title.png"),
        MAINMENU_PLAY_BLANK(40, "textures/mainmenu/menu_blank.png"),
        GAME_BALL(201, "textures/gameentity/ball%d.png"),
        GAME_CRAB_ENLARGE(203, "textures/gameentity/enlarge.png"),
        GAME_CREATEBALL(205, "textures/gameentity/addball.png"),
        GAME_CRAB_ACC(206, "textures/gameentity/accspeed.png"),
        GAME_CRAB_DEC(207, "textures/gameentity/decspeed.png"),
        GAME_GOLD(208, "textures/gameentity/gold%d.png"),
        GAME_CRAB_POWER(215, "textures/gameentity/powercrab.png"),
        GAME_CRAB(216, "textures/gameentity/crab.png"),
        GAME_BOMB(217, "textures/gameentity/bomb.png"),
        GAME_SCORE(209, "textures/gameentity/score-bg.png"),
        BALL_POWER_BAR(218, "textures/gameentity/ball-power-bg.png"),
        MENU_BALL(219, "textures/gameentity/ball1.png"),
        GAME_NEW_RECORD(302, "textures/background/new_record.png");
        
        public final int mKey;
        public final String mValue;

        private TEXTURE(int key, String value) {
            this.mKey = key;
            this.mValue = value;
        }
    }

    public enum TILEDMAP {
        TILEDMAP(0, "config/tmx/tmxlevel%d_%d.tmx");
        
        public final int mKey;
        public final String mValue;

        private TILEDMAP(int key, String value) {
            this.mKey = key;
            this.mValue = value;
        }
    }

    public enum BITMAP {
        FACEBOOK(0, "CommonResource/buttons/facebook.png");
        
        public final int mKey;
        public final String mValue;

        private BITMAP(int key, String value) {
            this.mKey = key;
            this.mValue = value;
        }
    }
}
