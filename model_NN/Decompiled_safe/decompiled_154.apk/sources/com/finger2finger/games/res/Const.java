package com.finger2finger.games.res;

import com.f2fgames.games.monkeybreak.lite.R;
import com.finger2finger.games.common.CommonConst;
import java.util.HashMap;
import java.util.Map;

public class Const extends CommonConst {
    public static final String ADD_SCORE_FORMAT = "+ %d";
    public static final int BRICK_TYPE_COUNT = 7;
    public static final float BUTTON_BIG_VALUE = 1.2f;
    public static float BallJetAccSpeedScale = 1.5f;
    public static float BallJetDecSpeedScale = 0.5f;
    public static float BallJetSpeed = 12.0f;
    public static float Ball_Density = 3.0f;
    public static float Ball_Elasticity = 0.2f;
    public static float Ball_Friction = 0.1f;
    public static final long CHECK_CROSSING_INTERVAL = 1000;
    public static final float CRAB_BASIC_SPEED = 150.0f;
    public static final long CRAB_POWERFUL_DURATION = 6000;
    public static final long CRAB_SCALE_DURATION = 10000;
    public static final long CRAB_SPEED_DURATION = 6000;
    public static float Crab_Density = 10.0f;
    public static float Crab_Elasticity = 0.3f;
    public static float Crab_Friction = 0.1f;
    public static final boolean DEBUG_MODE = false;
    public static final float DIALOG_BIG_VALUE = 1.15f;
    public static float DOWN_SPEED_Y_SCALE = 50.0f;
    public static final int[][] DynamicDialogTextId = {new int[]{1011, R.string.str_dynamic_dialog_001}};
    public static Map<Integer, Integer> DynamicDialogTextIdMap = new HashMap();
    public static final boolean ENABLE_HUD = false;
    public static final int FRAME_THICKNESS = 5;
    public static final String GAMEINFO_PREFERENCES = "CrabBreak";
    public static final int GAME_BALL_COUNT = 3;
    public static final int GAME_BALL_TYPE = 1;
    public static final int GAME_LIFE = 5;
    public static int GAME_MODEID = 0;
    public static final String GAME_NAME = "crabbreak";
    public static final int GHOST_TYPE_COUNT = 6;
    public static final int GOLD_TYPE_COUNT = 3;
    public static float GroundLine_Density = 3.0f;
    public static float GroundLine_Elasticity = 0.0f;
    public static float GroundLine_Friction = 0.1f;
    public static final float LINE_BORDER = 5.0f;
    public static final float MAIN_MENU_BALL_SPEED = 200.0f;
    public static final boolean ONCE_LOAD_ALL_RESOURES = false;
    public static float Obstacle_Density = 10.0f;
    public static float Obstacle_Elasticity = 0.3f;
    public static float Obstacle_Friction = 0.1f;
    public static final int POWER_CREATE_BALL_COUNT = 3;
    public static final int PROP_SPEED_DURATION = 5;
    public static boolean PhysicsWorld_AllowSleep = true;
    public static float PhysicsWorld_GravityX = 0.0f;
    public static float PhysicsWorld_GravityY = 1.0f;
    public static float PhysicsWorld_PositionIterations = 2.0f;
    public static float PhysicsWorld_StepsPerSecond = 30.0f;
    public static float PhysicsWorld_VelocityIterations = 3.0f;
    public static final int REMAIN_TIME = 10;
    public static final float[] SCORE_LEVEL_SCALE = {0.8f, 1.0f, 1.2f};
    public static final boolean SCREEN_LANDSCAPE = true;
    public static final int SEASTAR_TYPE_COUNT = 6;
    public static final float SpeedDown_Scale = 0.1f;
    public static final float SpeedUp_Scale = 5.0f;
    public static final String VERSION_PREFERENCES = "Version";
    public static final long VIBRATE_DURATION = 100;
    public static final String Version = "1.0.302";
    public static final int WORD_LESS_WIDTH = 100;
    public static final String X_SCORE_FORMAT = "X %d";
    public static boolean enableFaceBook = true;
    public static boolean enableGmail = true;
    public static boolean enableTwitter = true;

    public static class ObjectName {
        public static String Ball = "Ball";
        public static String Ball_Add = "Ball_Add";
        public static String Ball_Power = "Ball_Power";
        public static String Ball_SpeedDown = "Ball_SpeedDown";
        public static String Ball_SpeedUp = "Ball_SpeedUp";
        public static String Bomb = "Bomb";
        public static String Crab = "Crab";
        public static String Crab_Enlarge = "Crab_Enlarge";
        public static String DynamicDialog = "DynamicDialog";
        public static String PhysicsWorld = "PhysicsWorld";
        public static String StarLevel = "StarLevel";
        public static String Timer = "Timer";
    }

    public static class RectBlankColor {
        public static float alpha = 0.6f;
        public static float blue = 1.0f;
        public static float green = 1.0f;
        public static float red = 1.0f;
    }

    public static class RectDarkColor {
        public static float alpha = 1.0f;
        public static float blue = 0.8392f;
        public static float green = 0.6549f;
        public static float red = 0.0824f;
    }

    public static class TileMapLayer {
        public static final int LAYER_BACKGROUND = 0;
        public static final int LAYER_FOREGROUND = 2;
        public static final int LAYER_MAP = 1;
    }

    public static class TileProperty {
        public static final String AllowSleep = "AllowSleep";
        public static final String BrickType = "BrickType";
        public static final String ClearTime = "ClearTime";
        public static final String Density = "Density";
        public static final String Elasticity = "Elasticity";
        public static final String Friction = "Friction";
        public static final String GravityX = "GravityX";
        public static final String GravityY = "GravityY";
        public static final String Ground = "Ground";
        public static final String IS_PEARL = "pearl";
        public static final String Name_Obstacle = "obstacle";
        public static final String PositionIterations = "PositionIterations";
        public static final String Property_DisplayScale = "Scale";
        public static final String Property_Star_Level = "Level";
        public static final String Property_TimeLimit = "Limit";
        public static final String Rangle = "Rangle";
        public static final String Speed = "Speed";
        public static final String StepsPerSecond = "StepsPerSecond";
        public static final String Value_Obstacle = "true";
        public static final String VelocityIterations = "VelocityIterations";
    }

    public static int getTextId(int id) {
        if (DynamicDialogTextIdMap.size() == 0) {
            for (int i = 0; i < DynamicDialogTextId.length; i++) {
                DynamicDialogTextIdMap.put(Integer.valueOf(DynamicDialogTextId[i][0]), Integer.valueOf(DynamicDialogTextId[i][1]));
            }
        }
        if (DynamicDialogTextIdMap.get(Integer.valueOf(id)) == null) {
            return -1;
        }
        return DynamicDialogTextIdMap.get(Integer.valueOf(id)).intValue();
    }

    public static class Type {
        public static final int BALL_ADD = 13;
        public static final int BALL_POWER = 9;
        public static final int BALL_SPEED_DOWN = 11;
        public static final int BALL_SPEED_UP = 10;
        public static final int BOMB = 8;
        public static final int CRAB_ENLARGE = 12;
        public static final int GOLD_1 = 20;
        public static final int GOLD_2 = 21;
        public static final int GOLD_3 = 22;
        public static final int NOTHING = 0;
        public static final int PEARL = 15;
        public static final int SEASTAR0 = 1;
        public static final int SEASTAR1 = 2;
        public static final int SEASTAR2 = 3;
        public static final int SEASTAR3 = 4;
        public static final int SEASTAR4 = 5;
        public static final int SEASTAR5 = 6;
        public static final int SEASTAR6 = 7;
        private static HashMap<Integer, Integer> _gameScore = new HashMap<>();

        public static HashMap<Integer, Integer> get_gameScore() {
            return _gameScore;
        }

        public static void loadScore() {
            if (_gameScore.size() == 0) {
                _gameScore.put(Integer.valueOf(Score.SEASTAR0.mKey), Integer.valueOf(Score.SEASTAR0.mValue));
                _gameScore.put(Integer.valueOf(Score.SEASTAR1.mKey), Integer.valueOf(Score.SEASTAR1.mValue));
                _gameScore.put(Integer.valueOf(Score.SEASTAR2.mKey), Integer.valueOf(Score.SEASTAR2.mValue));
                _gameScore.put(Integer.valueOf(Score.SEASTAR3.mKey), Integer.valueOf(Score.SEASTAR3.mValue));
                _gameScore.put(Integer.valueOf(Score.SEASTAR4.mKey), Integer.valueOf(Score.SEASTAR4.mValue));
                _gameScore.put(Integer.valueOf(Score.SEASTAR5.mKey), Integer.valueOf(Score.SEASTAR5.mValue));
                _gameScore.put(Integer.valueOf(Score.SEASTAR6.mKey), Integer.valueOf(Score.SEASTAR6.mValue));
                _gameScore.put(Integer.valueOf(Score.SEASTAR6.mKey), Integer.valueOf(Score.SEASTAR6.mValue));
                _gameScore.put(Integer.valueOf(Score.SEASTAR6.mKey), Integer.valueOf(Score.SEASTAR6.mValue));
                _gameScore.put(Integer.valueOf(Score.SEASTAR6.mKey), Integer.valueOf(Score.SEASTAR6.mValue));
                _gameScore.put(Integer.valueOf(Score.SEASTAR6.mKey), Integer.valueOf(Score.SEASTAR6.mValue));
                _gameScore.put(Integer.valueOf(Score.GOLD_1.mKey), Integer.valueOf(Score.GOLD_1.mValue));
                _gameScore.put(Integer.valueOf(Score.GOLD_2.mKey), Integer.valueOf(Score.GOLD_2.mValue));
                _gameScore.put(Integer.valueOf(Score.GOLD_3.mKey), Integer.valueOf(Score.GOLD_3.mValue));
            }
        }

        public enum Score {
            SEASTAR0(1, 5),
            SEASTAR1(2, 8),
            SEASTAR2(3, 11),
            SEASTAR3(4, 14),
            SEASTAR4(5, 17),
            SEASTAR5(6, 20),
            SEASTAR6(7, 23),
            GOLD_1(20, 10),
            GOLD_2(21, 20),
            GOLD_3(22, 30);
            
            public final int mKey;
            public final int mValue;

            private Score(int key, int value) {
                this.mKey = key;
                this.mValue = value;
            }
        }
    }
}
