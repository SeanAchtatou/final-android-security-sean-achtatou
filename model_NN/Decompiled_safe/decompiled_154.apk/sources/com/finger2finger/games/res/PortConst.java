package com.finger2finger.games.res;

import com.f2fgames.games.monkeybreak.lite.R;
import com.finger2finger.games.common.CommonPortConst;

public class PortConst extends CommonPortConst {
    public static final String AdviewId = "SDK20112310390544mlalvayo03mlcoq";
    public static final String GoogleAnalytics = "UA-22419714-4";
    public static final int[][] LevelInfo = {new int[]{20}};
    public static final int[] LevelTitle = {R.string.game_mode_01, R.string.game_mode_02, R.string.game_mode_03};
    public static final boolean enableAds = true;
    public static final boolean enableFaceBook = true;
    public static final boolean enableGoogleAnalytics = true;
    public static final boolean enableHelp = true;
    public static final boolean enableInviteFriend = true;
    public static boolean enablePromotion = true;
    public static final boolean enableSDCard = true;
    public static final boolean enableSubLevelOption = true;
    public static final boolean enableTwitter = true;
    public static boolean showPromtionIcon = true;
}
