package com.finger2finger.games.res;

public class FontConst {
    public static final int COLOR_COLLISION_HINT = -65536;
    public static final int COLOR_X_SCORE_HINT = -65536;
    public static final String DIALOG_CONTENT_FONT = FontName.FONT3;
    public static final int DIALOG_CONTENT_IN_COLOR = -1;
    public static final int DIALOG_CONTENT_OUT_COLOR = -16777216;
    public static final int DIALOG_CONTENT_SIZE = 35;
    public static final String DIALOG_TIP_FONT = FontName.FONT3;
    public static final int DIALOG_TIP_IN_COLOR = -16777216;
    public static final int DIALOG_TIP_OUT_COLOR = -1;
    public static final int DIALOG_TIP_SIZE = 35;
    public static final String DIALOG_TITLE_FONT = FontName.FONT3;
    public static final int DIALOG_TITLE_IN_COLOR = -256;
    public static final int DIALOG_TITLE_OUT_COLOR = -16777216;
    public static final int DIALOG_TITLE_SIZE = 40;
    public static final String FONT_COLLISION_HINT = FontName.FONT1;
    public static final String HELP_FORMART_PICPATH = "help/help%d.png";
    public static final int LEVEL1_COLOR_CAPTION = -1;
    public static final int LEVEL1_COLOR_DIALOG = -1;
    public static final int LEVEL1_COLOR_DIALOG_TITLE = -1;
    public static final int LEVEL1_COLOR_HUD = -16711936;
    public static final int LEVEL1_COLOR_MENU = -1;
    public static final String LEVEL1_FONT_CAPTION = FontName.FONT1;
    public static final String LEVEL1_FONT_HUD = FontName.FONT1;
    public static final String LEVEL1_FONT_MENU = FontName.FONT1;
    public static final String LEVEL1_FORMART_MENUTITLE = "%d - %d";
    public static final int LEVEL1_SIZE_CAPTION = 25;
    public static final int LEVEL1_SIZE_DIALOG = 25;
    public static final int LEVEL1_SIZE_DIALOG_TITLE = 35;
    public static final int LEVEL1_SIZE_HUD = 25;
    public static final int LEVEL1_SIZE_MENU = 30;
    public static final int LEVEL_DESCRIB_COLOR = -65536;
    public static final String LEVEL_DESCRIB_FONT = FontName.FONT1;
    public static final int LEVEL_DESCRIB_SIZE = 35;
    public static final int SIZE_COLLISION_HINT = 35;
    public static final int SIZE_X_SCORE_HINT = 40;
    public static final int START_COLOR_ABOUT = -1;
    public static final int START_COLOR_ABOUT_TITLE = -1;
    public static final int START_COLOR_OUT_TITLE = -65281;
    public static final int START_COLOR_PLAY = -1;
    public static final int START_COLOR_TITLE = -1;
    public static final String START_FONT_ABOUT = FontName.FONT1;
    public static final String START_FONT_ABOUT_TITLE = FontName.FONT1;
    public static final String START_FONT_PLAY = FontName.FONT1;
    public static final String START_FONT_TITLE = FontName.FONT1;
    public static final float START_SIZE_ABOUT = 18.0f;
    public static final float START_SIZE_ABOUT_TITLE = 30.0f;
    public static final float START_SIZE_PLAY = 40.0f;
    public static final int START_SIZE_TITLE = 48;
    public static final int SUBLEVEL_COLOR_LEVEL = -65536;
    public static final String SUBLEVEL_FONT_LEVEL = FontName.FONT1;
    public static final int SUBLEVEL_HIGHEST_SCORE_COLOR_LEVEL = -65536;
    public static final String SUBLEVEL_HIGHEST_SCORE_FONT_LEVEL = FontName.FONT1;
    public static final int SUBLEVEL_HIGHEST_SCORE_SIZE_LEVEL = 30;
    public static final int SUBLEVEL_SIZE_LEVEL = 20;

    public static class FontName {
        public static String FONT1 = "AhnbergHand.ttf";
        public static String FONT3 = "04B_03B.TTF";
    }
}
