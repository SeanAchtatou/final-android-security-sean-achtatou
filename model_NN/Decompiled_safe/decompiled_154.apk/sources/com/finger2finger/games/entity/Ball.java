package com.finger2finger.games.entity;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.finger2finger.games.res.Const;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.extension.physics.box2d.FixedStepPhysicsWorld;
import org.anddev.andengine.extension.physics.box2d.PhysicsConnector;
import org.anddev.andengine.extension.physics.box2d.PhysicsFactory;
import org.anddev.andengine.opengl.texture.region.TextureRegion;

public class Ball {
    public boolean isBeJoint = false;
    public boolean isEnable = false;
    public boolean isHitWall = false;
    public boolean isRemove = false;
    public Body mBodyBall = null;
    public long mCollidesDuration = 0;
    public int mLayer;
    private float mMapScale = 1.0f;
    public PhysicsConnector mPhysicsConnector = null;
    private FixedStepPhysicsWorld mPhysicsWorld;
    public float mScale = 1.0f;
    private Scene mScene;
    public Sprite mSpriteBall = null;
    private TextureRegion mTextureRegion;
    private float mX;
    private float mY;

    public Ball(float pX, float pY, TextureRegion pTextureRegion, Scene pScene, int pLayer, FixedStepPhysicsWorld pPhysicsWorld, float pMapScale) {
        this.mTextureRegion = pTextureRegion;
        this.mScene = pScene;
        this.mLayer = pLayer;
        this.mX = pX;
        this.mY = pY;
        this.mPhysicsWorld = pPhysicsWorld;
        this.mMapScale = pMapScale;
    }

    public void setScale(float pScale) {
        this.mScale = pScale;
    }

    public void createBall() {
        this.mSpriteBall = new Sprite(this.mX, this.mY, ((float) this.mTextureRegion.getWidth()) * this.mScale * this.mMapScale, ((float) this.mTextureRegion.getHeight()) * this.mScale * this.mMapScale, this.mTextureRegion);
        this.mScene.getLayer(this.mLayer).addEntity(this.mSpriteBall);
        this.mBodyBall = PhysicsFactory.createBoxBody(this.mPhysicsWorld, this.mSpriteBall, BodyDef.BodyType.StaticBody, PhysicsFactory.createFixtureDef(Const.Ball_Density, Const.Ball_Elasticity, Const.Ball_Friction));
        this.mSpriteBall.setUpdatePhysics(false);
        this.mPhysicsConnector = new PhysicsConnector(this.mSpriteBall, this.mBodyBall, true, true, true, false);
        this.mPhysicsWorld.registerPhysicsConnector(this.mPhysicsConnector);
        this.mBodyBall.setUserData(this);
    }

    public void setVisible(boolean isVisible) {
        this.mSpriteBall.setVisible(isVisible);
    }

    public void setStatic() {
        if (this.mBodyBall != null && this.mBodyBall.getType() == BodyDef.BodyType.DynamicBody) {
            this.mBodyBall.setType(BodyDef.BodyType.StaticBody);
        }
    }

    public void setDynamic() {
        if (this.mBodyBall != null && this.mBodyBall.getType() == BodyDef.BodyType.StaticBody) {
            this.mBodyBall.setType(BodyDef.BodyType.DynamicBody);
        }
    }

    public void destroy() {
        this.mScene.getLayer(this.mLayer).removeEntity(this.mSpriteBall);
        this.mPhysicsWorld.unregisterPhysicsConnector(this.mPhysicsConnector);
        this.mPhysicsWorld.destroyBody(this.mBodyBall);
    }
}
