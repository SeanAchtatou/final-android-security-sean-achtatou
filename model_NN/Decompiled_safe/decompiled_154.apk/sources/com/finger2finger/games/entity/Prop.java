package com.finger2finger.games.entity;

import com.finger2finger.games.common.CommonConst;
import com.finger2finger.games.res.Const;
import com.finger2finger.games.scene.GameScene;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.sprite.AnimatedSprite;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;

public class Prop {
    /* access modifiers changed from: private */
    public boolean _Click = false;
    private int _GoldScore = 0;
    public boolean isEnable = false;
    public boolean isModifier = false;
    public boolean isRemove = false;
    public AnimatedSprite mAnimatedSpriteProp = null;
    public long mCollidesDuration = 0;
    public int mGoldType = -1;
    public int mLayer;
    /* access modifiers changed from: private */
    public Scene mScene;
    public Sprite mSpriteProp = null;
    private TextureRegion mTextureRegion;
    private TiledTextureRegion mTileTextureRegion;
    public float mTouchAreaX = 0.0f;
    public int mType = -1;
    private float mX;
    private float mY;

    public boolean is_Click() {
        return this._Click;
    }

    public void set_Click(boolean click) {
        this._Click = click;
    }

    public int get_GoldScore() {
        return this._GoldScore;
    }

    public void set_GoldScore(int goldScore) {
        this._GoldScore = goldScore;
    }

    public Prop(int pType, float pX, float pY, TextureRegion pTextureRegion, Scene pScene, int pLayer) {
        this.mTextureRegion = pTextureRegion;
        this.mScene = pScene;
        this.mLayer = pLayer;
        this.mX = pX;
        this.mY = pY;
        this.mType = pType;
    }

    public Prop(int pType, float pX, float pY, TiledTextureRegion pTextureRegion, Scene pScene, int pLayer) {
        this.mTileTextureRegion = pTextureRegion;
        this.mScene = pScene;
        this.mLayer = pLayer;
        this.mX = pX;
        this.mY = pY;
        this.mType = pType;
    }

    public void createProp() {
        if (this.mType == 0) {
            this.mAnimatedSpriteProp = new AnimatedSprite(CommonConst.RALE_SAMALL_VALUE * this.mX, CommonConst.RALE_SAMALL_VALUE * this.mY, CommonConst.RALE_SAMALL_VALUE * ((float) this.mTileTextureRegion.getTileWidth()), CommonConst.RALE_SAMALL_VALUE * ((float) this.mTileTextureRegion.getTileHeight()), this.mTileTextureRegion) {
                public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                    ((GameScene) Prop.this.mScene).setChildSceneClick(true);
                    if (Prop.this._Click) {
                        return false;
                    }
                    ((GameScene) Prop.this.mScene).isCheckGetProp = true;
                    Prop.this.mTouchAreaX = pSceneTouchEvent.getX();
                    boolean unused = Prop.this._Click = true;
                    return true;
                }
            };
            this.mAnimatedSpriteProp.setVisible(false);
            this.mScene.getLayer(this.mLayer).addEntity(this.mAnimatedSpriteProp);
            return;
        }
        this.mSpriteProp = new Sprite(CommonConst.RALE_SAMALL_VALUE * this.mX, CommonConst.RALE_SAMALL_VALUE * this.mY, CommonConst.RALE_SAMALL_VALUE * ((float) this.mTextureRegion.getWidth()), CommonConst.RALE_SAMALL_VALUE * ((float) this.mTextureRegion.getHeight()), this.mTextureRegion) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                ((GameScene) Prop.this.mScene).setChildSceneClick(true);
                if (Prop.this._Click) {
                    return false;
                }
                ((GameScene) Prop.this.mScene).isCheckGetProp = true;
                Prop.this.mTouchAreaX = pSceneTouchEvent.getX();
                boolean unused = Prop.this._Click = true;
                return true;
            }
        };
        this.mScene.getLayer(this.mLayer).addEntity(this.mSpriteProp);
        this.mSpriteProp.setVisible(false);
    }

    public void destroy() {
        this._Click = false;
        this.isModifier = false;
        this.isEnable = false;
        if (this.mSpriteProp != null) {
            this.mSpriteProp.clearShapeModifiers();
            this.mScene.unregisterTouchArea(this.mSpriteProp);
            this.mSpriteProp.setVisible(false);
            this.mSpriteProp.setVelocity(0.0f, 0.0f);
            this.mSpriteProp.setPosition(0.0f, 0.0f);
        } else if (this.mAnimatedSpriteProp != null) {
            this.mAnimatedSpriteProp.clearShapeModifiers();
            this.mAnimatedSpriteProp.stopAnimation();
            this.mScene.unregisterTouchArea(this.mAnimatedSpriteProp);
            this.mScene.setVisible(false);
            this.mAnimatedSpriteProp.setVelocity(0.0f, 0.0f);
            this.mAnimatedSpriteProp.setPosition(0.0f, 0.0f);
        }
    }

    public void setEnable(float px, float py) {
        this.isRemove = false;
        this.isEnable = true;
        if (this.mSpriteProp != null) {
            this.mSpriteProp.setPosition(px, py);
            this.mSpriteProp.setAlpha(1.0f);
            this.mSpriteProp.setVisible(true);
            this.mScene.registerTouchArea(this.mSpriteProp);
            this.mSpriteProp.setVelocity(0.0f, Const.DOWN_SPEED_Y_SCALE * CommonConst.RALE_SAMALL_VALUE);
        } else if (this.mAnimatedSpriteProp != null) {
            this.mAnimatedSpriteProp.setPosition(px, py);
            this.mAnimatedSpriteProp.setAlpha(1.0f);
            this.mAnimatedSpriteProp.setVisible(true);
            this.mScene.registerTouchArea(this.mAnimatedSpriteProp);
            this.mAnimatedSpriteProp.setVelocity(0.0f, Const.DOWN_SPEED_Y_SCALE * CommonConst.RALE_SAMALL_VALUE);
        }
    }
}
