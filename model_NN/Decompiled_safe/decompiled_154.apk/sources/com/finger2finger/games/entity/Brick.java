package com.finger2finger.games.entity;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import java.util.Random;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.sprite.AnimatedSprite;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.extension.physics.box2d.FixedStepPhysicsWorld;
import org.anddev.andengine.extension.physics.box2d.PhysicsConnector;
import org.anddev.andengine.extension.physics.box2d.PhysicsFactory;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;
import org.anddev.andengine.util.constants.TimeConstants;

public class Brick {
    private boolean _Collide = false;
    public long interval = ((long) (new Random().nextInt(3000) + TimeConstants.MILLISECONDSPERSECOND));
    public boolean isInitialPosition = true;
    public boolean isRemove = false;
    public Body mBodyEntity = null;
    public int mClearTimes = 0;
    public int mClearTimesBackup = 0;
    private float mDensity = 0.0f;
    private float mElasticity = 0.0f;
    private float mFriction = 0.0f;
    public int mLayer;
    private float mMapScale = 1.0f;
    public PhysicsConnector mPhysicsConnector = null;
    private FixedStepPhysicsWorld mPhysicsWorld;
    private Scene mScene;
    public AnimatedSprite mSpriteEntity = null;
    public Sprite mSpriteEntity2 = null;
    private TiledTextureRegion mTextureRegion;
    private TextureRegion mTextureRegion2;
    public int mType;
    private float mX;
    private float mY;
    public long updateTime = 0;

    public boolean is_Collide() {
        return this._Collide;
    }

    public void set_Collide(boolean collide) {
        this._Collide = collide;
    }

    public Brick(int pType, float pX, float pY, TiledTextureRegion pTextureRegion, Scene pScene, int pLayer, FixedStepPhysicsWorld pPhysicsWorld, float pDensity, float pElasticity, float pFriction, int pClearTimes, float pMapScale) {
        this.mTextureRegion = pTextureRegion;
        this.mScene = pScene;
        this.mLayer = pLayer;
        this.mX = pX;
        this.mY = pY;
        this.mType = pType;
        this.mClearTimes = pClearTimes;
        this.mClearTimesBackup = pClearTimes;
        this.mPhysicsWorld = pPhysicsWorld;
        this.mDensity = pDensity;
        this.mElasticity = pElasticity;
        this.mFriction = pFriction;
        this.mMapScale = pMapScale;
    }

    public Brick(int pType, float pX, float pY, TextureRegion pTextureRegion, Scene pScene, int pLayer, FixedStepPhysicsWorld pPhysicsWorld, float pDensity, float pElasticity, float pFriction, float pMapScale) {
        this.mTextureRegion2 = pTextureRegion;
        this.mScene = pScene;
        this.mLayer = pLayer;
        this.mX = pX;
        this.mY = pY;
        this.mType = pType;
        this.mPhysicsWorld = pPhysicsWorld;
        this.mDensity = pDensity;
        this.mElasticity = pElasticity;
        this.mFriction = pFriction;
        this.mMapScale = pMapScale;
    }

    public void createEntity() {
        if (this.mType == 15) {
            this.mSpriteEntity2 = new Sprite(this.mX * this.mMapScale, this.mY * this.mMapScale, ((float) this.mTextureRegion2.getWidth()) * this.mMapScale, ((float) this.mTextureRegion2.getHeight()) * this.mMapScale, this.mTextureRegion2);
            this.mScene.getLayer(this.mLayer).addEntity(this.mSpriteEntity2);
            this.mSpriteEntity2.setUpdatePhysics(false);
            resetBody();
        } else if (this.mType > 0) {
            this.mSpriteEntity = new AnimatedSprite(this.mX * this.mMapScale, this.mY * this.mMapScale, ((float) this.mTextureRegion.getTileWidth()) * this.mMapScale, ((float) this.mTextureRegion.getTileHeight()) * this.mMapScale, this.mTextureRegion);
            this.mScene.getLayer(this.mLayer).addEntity(this.mSpriteEntity);
            this.mSpriteEntity.setUpdatePhysics(false);
            resetBody();
        }
    }

    public void resetBody() {
        this.mBodyEntity = PhysicsFactory.createBoxBody(this.mPhysicsWorld, this.mSpriteEntity != null ? this.mSpriteEntity : this.mSpriteEntity2, BodyDef.BodyType.StaticBody, PhysicsFactory.createFixtureDef(this.mDensity, this.mElasticity, this.mFriction));
        this.mPhysicsConnector = new PhysicsConnector(this.mSpriteEntity != null ? this.mSpriteEntity : this.mSpriteEntity2, this.mBodyEntity, true, true, true, false);
        this.mPhysicsWorld.registerPhysicsConnector(this.mPhysicsConnector);
        this.mBodyEntity.setUserData(this);
    }

    public void destroyBody() {
        this.mPhysicsWorld.unregisterPhysicsConnector(this.mPhysicsConnector);
        this.mPhysicsWorld.destroyBody(this.mBodyEntity);
    }

    public void setEnable(boolean isEnable) {
        if (isEnable) {
            this.isRemove = false;
            if (this.mSpriteEntity != null) {
                this.mSpriteEntity.setVisible(true);
            } else if (this.mSpriteEntity2 != null) {
                this.mSpriteEntity2.setVisible(true);
            }
            resetBody();
            return;
        }
        this.isRemove = true;
        if (this.mSpriteEntity != null) {
            this.mSpriteEntity.setVisible(false);
        } else if (this.mSpriteEntity2 != null) {
            this.mSpriteEntity2.setVisible(false);
        }
        this.mPhysicsWorld.unregisterPhysicsConnector(this.mPhysicsConnector);
        this.mPhysicsWorld.destroyBody(this.mBodyEntity);
    }
}
