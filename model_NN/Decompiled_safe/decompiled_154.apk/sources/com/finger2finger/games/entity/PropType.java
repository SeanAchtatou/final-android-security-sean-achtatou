package com.finger2finger.games.entity;

public class PropType {
    public static final int ADD_LIFE = 4;
    public static final int BALL_ACC_SPEED = 1;
    public static final int BALL_DEC_SPEED = 2;
    public static final int BALL_POWER = 0;
    public static final int CRAB_ENLARGE = 3;
    public static final int GOLD = 5;
}
