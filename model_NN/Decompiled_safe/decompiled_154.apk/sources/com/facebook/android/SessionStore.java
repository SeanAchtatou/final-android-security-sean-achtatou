package com.facebook.android;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class SessionStore {
    private static final String EXPIRES = "expires_in";
    private static final String KEY = "facebook-session";
    private static final String NAME = "facebook-username";
    private static final String TOKEN = "access_token";
    private static final String UID = "facebook-uid";
    private static String name = "";
    private static String uid = "";

    public SessionStore(String uid2, String name2) {
        uid = uid2;
        name = name2;
    }

    public static boolean save(Facebook session, Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(KEY, 0).edit();
        editor.putString("access_token", session.getAccessToken());
        editor.putLong("expires_in", session.getAccessExpires());
        editor.putString(UID, uid);
        editor.putString(NAME, name);
        Log.d("f2f", "save UID:" + uid);
        Log.d("f2f", "save NAME:" + name);
        Log.d("f2f", "save AccessToken:" + session.getAccessToken());
        Log.d("f2f", "save AccessExpires:" + session.getAccessExpires());
        return editor.commit();
    }

    public static boolean restore(Facebook session, Context context) {
        SharedPreferences savedSession = context.getSharedPreferences(KEY, 0);
        session.setAccessToken(savedSession.getString("access_token", null));
        session.setAccessExpires(savedSession.getLong("expires_in", 0));
        uid = savedSession.getString(UID, null);
        name = savedSession.getString(NAME, null);
        Log.d("f2f", "restore UID:" + uid);
        Log.d("f2f", "restore NAME:" + name);
        Log.d("f2f", "restore AccessToken:" + savedSession.getString("access_token", null));
        Log.d("f2f", "restore AccessExpires:" + savedSession.getLong("expires_in", 0));
        return session.isSessionValid();
    }

    public static void clear(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(KEY, 0).edit();
        editor.clear();
        editor.commit();
    }

    public static String getUid() {
        return uid;
    }

    public static void setUid(String uid2) {
        uid = uid2;
    }

    public static String getName() {
        return name;
    }

    public static void setName(String name2) {
        name = name2;
    }

    public static String getTOKEN() {
        return "access_token";
    }

    public static String getEXPIRES() {
        return "expires_in";
    }

    public static String getKEY() {
        return KEY;
    }

    public static String getNAME() {
        return NAME;
    }

    public static String getUID() {
        return UID;
    }
}
