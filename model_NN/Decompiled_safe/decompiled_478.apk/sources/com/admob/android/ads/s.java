package com.admob.android.ads;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import com.admob.android.ads.InterstitialAd;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.PriorityQueue;

/* compiled from: AdMobImageCache */
public final class s {
    private static s a = null;
    private File b;
    private long c = 0;
    private long d = 524288;
    private PriorityQueue<File> e = null;
    private Hashtable<String, File> f = null;

    public static s a(Context context) {
        if (a == null) {
            a = new s(context, 524288);
        }
        return a;
    }

    /* compiled from: AdMobImageCache */
    static class b implements Comparator<File> {
        b() {
        }

        public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
            File file = (File) obj;
            File file2 = (File) obj2;
            if (file == null && file2 == null) {
                return 0;
            }
            if (file != null && file2 != null) {
                long lastModified = file.lastModified();
                long lastModified2 = file2.lastModified();
                if (lastModified == lastModified2) {
                    return 0;
                }
                if (lastModified >= lastModified2) {
                    return 1;
                }
            } else if (file != null) {
                return 1;
            }
            return -1;
        }
    }

    private s(Context context, long j) {
        File file = new File(context.getCacheDir(), "admob_img_cache");
        if (!file.exists()) {
            file.mkdir();
        } else if (!file.isDirectory()) {
            file.delete();
            file.mkdir();
        }
        this.b = file;
        a(this.b);
    }

    private void a(File file) {
        File[] listFiles = file.listFiles();
        this.e = new PriorityQueue<>(20, new b());
        this.f = new Hashtable<>();
        for (File file2 : listFiles) {
            if (file2 != null && file2.canRead()) {
                this.e.add(file2);
                this.f.put(file2.getName(), file2);
                this.c += file2.length();
            }
        }
    }

    private synchronized void b(File file) {
        if (file != null) {
            if (this.e.remove(file) && (this.f.remove(file.getName()) != null)) {
                this.c -= file.length();
                if (InterstitialAd.c.a(AdManager.LOG, 2)) {
                    Log.v(AdManager.LOG, "Cache: removed file " + file.getName() + " totalBytes " + this.c);
                }
            }
        }
    }

    private synchronized void c(File file) {
        if (file != null) {
            if (this.e.contains(file) || this.f.get(file.getName()) != null) {
                if (InterstitialAd.c.a(AdManager.LOG, 2)) {
                    Log.v(AdManager.LOG, "Cache: trying to add a file that's already in index");
                }
                b(file);
            }
            this.e.add(file);
            this.f.put(file.getName(), file);
            this.c += file.length();
            if (InterstitialAd.c.a(AdManager.LOG, 2)) {
                Log.v(AdManager.LOG, "cache: added file: " + file.getName() + " totalBytes " + this.c);
            }
        }
    }

    public final synchronized Bitmap a(String str) {
        Bitmap bitmap;
        Bitmap decodeFile;
        File file = this.f.get(str);
        if (file == null || (decodeFile = BitmapFactory.decodeFile(file.getAbsolutePath())) == null) {
            bitmap = null;
        } else {
            this.e.remove(file);
            file.setLastModified(System.currentTimeMillis());
            this.e.add(file);
            if (InterstitialAd.c.a(AdManager.LOG, 2)) {
                Log.v(AdManager.LOG, "cache: found bitmap " + file.getName() + " totalBytes " + this.c + " new modified " + file.lastModified());
            }
            bitmap = decodeFile;
        }
        return bitmap;
    }

    public final synchronized void a(String str, Bitmap bitmap) {
        File file = new File(this.b, str);
        File file2 = this.f.get(str);
        if (file2 != null) {
            if (InterstitialAd.c.a(AdManager.LOG, 2)) {
                Log.v(AdManager.LOG, "cache: found bitmap " + file.getName() + " and removing ");
            }
            b(file2);
        }
        if (file.exists()) {
            file.delete();
        }
        try {
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, new FileOutputStream(file));
            c(file);
            if (InterstitialAd.c.a(AdManager.LOG, 2)) {
                Log.v(AdManager.LOG, "cache: added bitmap " + file.getName() + " totalBytes " + this.c + " lastModified " + file.lastModified());
            }
        } catch (FileNotFoundException e2) {
        }
    }

    /* compiled from: AdMobImageCache */
    static class a extends Thread {
        private s a;

        /* synthetic */ a(s sVar) {
            this(sVar, (byte) 0);
        }

        private a(s sVar, byte b) {
            this.a = sVar;
        }

        public final void run() {
            if (this.a != null) {
                this.a.b();
            }
        }
    }

    public static void a() {
        new a(a).start();
    }

    /* access modifiers changed from: private */
    public synchronized void b() {
        while (this.c > this.d && this.e.size() > 0) {
            File peek = this.e.peek();
            if (InterstitialAd.c.a(AdManager.LOG, 2)) {
                Log.v(AdManager.LOG, "cache: evicting bitmap " + peek.getName() + " totalBytes " + this.c);
            }
            b(peek);
            peek.delete();
        }
    }
}
