package bateria.alert;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class Bateria extends Activity {
    protected TextView bateriatex;
    /* access modifiers changed from: private */
    public ImageView imageBateria;
    private ImageButton informacion;
    private AlertDialog mensa;
    private BroadcastReceiver myBatteryReceiver = new BroadcastReceiver() {
        public void onReceive(Context contex, Intent inten) {
            inten.getAction().equals("android.intent.action.BATTERY_CHANGED");
            CharSequence Cargar = Bateria.this.getString(R.string.carga);
            int status = inten.getIntExtra("status", 1);
            Bateria.this.bateriatex.setText(((Object) Cargar) + " " + String.valueOf(inten.getIntExtra("level", 0)) + "%");
            Bateria.this.bateriatex.setTextColor(Bateria.this.getResources().getColor(R.color.blanco));
            Bateria.this.imageBateria.setImageDrawable(Bateria.this.getResources().getDrawable(R.drawable.bate_92));
            int carga = inten.getIntExtra("level", 0);
            if (carga <= 8) {
                Bateria.this.bateriatex.setTextColor(Bateria.this.getResources().getColor(R.color.rojo));
                Bateria.this.imageBateria.setImageDrawable(Bateria.this.getResources().getDrawable(R.drawable.bate_8));
            } else if (carga <= 16) {
                Bateria.this.bateriatex.setTextColor(Bateria.this.getResources().getColor(R.color.rojo));
                Bateria.this.imageBateria.setImageDrawable(Bateria.this.getResources().getDrawable(R.drawable.bate_15));
            } else if (carga <= 25) {
                Bateria.this.imageBateria.setImageDrawable(Bateria.this.getResources().getDrawable(R.drawable.bate_25));
            } else if (carga <= 50) {
                Bateria.this.imageBateria.setImageDrawable(Bateria.this.getResources().getDrawable(R.drawable.bate_50));
            } else if (carga <= 75) {
                Bateria.this.imageBateria.setImageDrawable(Bateria.this.getResources().getDrawable(R.drawable.bate_75));
            } else if (carga <= 92) {
                Bateria.this.imageBateria.setImageDrawable(Bateria.this.getResources().getDrawable(R.drawable.bate_92));
            } else if (carga == 100 || status == 5) {
                if (Bateria.this.sonidoAlarma != null) {
                    Bateria.this.sonidoAlarma.release();
                }
                Bateria.this.sonidoAlarma = MediaPlayer.create(Bateria.this, (int) R.raw.beep);
                Bateria.this.sonidoAlarma.setLooping(true);
                Bateria.this.sonidoAlarma.start();
                Bateria.this.imageBateria.setImageDrawable(Bateria.this.getResources().getDrawable(R.drawable.bate_100));
                Bateria.this.bateriatex.setTextColor(Bateria.this.getResources().getColor(R.color.amarillo));
                Bateria.this.bateriatex.setText(Bateria.this.getString(R.string.cargaCompleta));
            }
        }
    };
    /* access modifiers changed from: private */
    public MediaPlayer sonidoAlarma;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        setVolumeControlStream(3);
        Typeface textoPRI = Typeface.createFromAsset(getAssets(), "fonts/verda.ttf");
        this.bateriatex = (TextView) findViewById(R.id.bateriatex);
        this.bateriatex.setTypeface(textoPRI);
        this.informacion = (ImageButton) findViewById(R.id.informacion);
        this.imageBateria = (ImageView) findViewById(R.id.imageBateria);
        this.informacion.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ((Vibrator) Bateria.this.getSystemService("vibrator")).vibrate(30);
                Bateria.this.informacion();
            }
        });
        registerReceiver(this.myBatteryReceiver, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
    }

    /* access modifiers changed from: protected */
    public void informacion() {
        this.mensa = new AlertDialog.Builder(this).create();
        this.mensa.setIcon((int) R.drawable.iconomensaje);
        this.mensa.setMessage(getString(R.string.textoInfo));
        this.mensa.setTitle(getString(R.string.informacion));
        this.mensa.show();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return false;
        }
        if (this.sonidoAlarma != null) {
            this.sonidoAlarma.release();
        }
        finish();
        return false;
    }

    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.myBatteryReceiver);
    }
}
