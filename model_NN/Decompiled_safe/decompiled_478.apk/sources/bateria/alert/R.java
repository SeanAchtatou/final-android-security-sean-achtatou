package bateria.alert;

public final class R {

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int keywords = 2130771971;
        public static final int primaryTextColor = 2130771969;
        public static final int refreshInterval = 2130771972;
        public static final int secondaryTextColor = 2130771970;
    }

    public static final class color {
        public static final int amarillo = 2131099650;
        public static final int blanco = 2131099652;
        public static final int naranja = 2131099651;
        public static final int rojo = 2131099648;
        public static final int verde = 2131099649;
    }

    public static final class drawable {
        public static final int bate_100 = 2130837504;
        public static final int bate_15 = 2130837505;
        public static final int bate_25 = 2130837506;
        public static final int bate_50 = 2130837507;
        public static final int bate_75 = 2130837508;
        public static final int bate_8 = 2130837509;
        public static final int bate_92 = 2130837510;
        public static final int icon = 2130837511;
        public static final int iconomensaje = 2130837512;
        public static final int info = 2130837513;
        public static final int info_1 = 2130837514;
        public static final int info_2 = 2130837515;
        public static final int info_3 = 2130837516;
        public static final int linea = 2130837517;
    }

    public static final class id {
        public static final int LayoutPrincipal = 2131165184;
        public static final int ad = 2131165188;
        public static final int bateriatex = 2131165185;
        public static final int imageBateria = 2131165186;
        public static final int informacion = 2131165187;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class raw {
        public static final int beep = 2130968576;
    }

    public static final class string {
        public static final int app_name = 2131034113;
        public static final int carga = 2131034112;
        public static final int cargaCompleta = 2131034114;
        public static final int cargando = 2131034115;
        public static final int informacion = 2131034116;
        public static final int textoInfo = 2131034117;
    }

    public static final class styleable {
        public static final int[] com_admob_android_ads_AdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.keywords, R.attr.refreshInterval};
        public static final int com_admob_android_ads_AdView_backgroundColor = 0;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_primaryTextColor = 1;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_secondaryTextColor = 2;
    }
}
