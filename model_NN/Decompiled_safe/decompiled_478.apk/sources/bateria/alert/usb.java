package bateria.alert;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class usb extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.ACTION_POWER_CONNECTED")) {
            Toast.makeText(context, context.getString(R.string.cargando), 1).show();
            Intent myStarterIntent = new Intent(context, Bateria.class);
            myStarterIntent.addFlags(268435456);
            context.startActivity(myStarterIntent);
        }
    }
}
