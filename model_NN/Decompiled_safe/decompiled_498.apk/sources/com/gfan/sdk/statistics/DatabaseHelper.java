package com.gfan.sdk.statistics;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String database_name = "InfoDB.db";
    private static final int database_version = 6;

    DatabaseHelper(Context context) {
        super(context, database_name, (SQLiteDatabase.CursorFactory) null, (int) database_version);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table appbase(id integer primary key autoincrement,clickname text,clickcount integer, app_start_time integer)");
        db.execSQL("create table app_backup_base(id integer primary key autoincrement, version text,starttime integer,endtime integer,timesum integer,mac text,cpidmac text,opid text,sdk_version text,sdk_type text)");
        db.execSQL("create table ridbase(id integer primary key autoincrement,rid text)");
        db.execSQL("create table app_backup_start(id integer primary key autoincrement, version text,starttime integer,mac text,cpidmac text,opid text,sdk_version text,sdk_type text)");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists appbase");
        db.execSQL("drop table if exists app_backup_base");
        db.execSQL("drop table if exists ridbase");
        db.execSQL("drop table if exists app_backup_start");
        onCreate(db);
    }
}
