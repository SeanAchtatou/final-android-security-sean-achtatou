package com.millennialmedia.android;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.MotionEvent;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.millennialmedia.android.MMAdViewSDK;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.URL;
import java.util.List;

public class VideoPlayer extends Activity implements Handler.Callback {
    private static final int MESSAGE_DELAYED_BUTTON = 3;
    private static final int MESSAGE_EVENTLOG_CHECK = 2;
    private static final int MESSAGE_INACTIVITY_ANIMATION = 1;
    private RelativeLayout buttonsLayout;
    private RelativeLayout controlsLayout;
    /* access modifiers changed from: private */
    public String current;
    private int currentVideoPosition = 0;
    private Handler handler;
    private TextView hudSeconds;
    private TextView hudStaticText;
    private boolean isCachedAd;
    private int lastVideoPosition;
    private EventLogSet logSet;
    /* access modifiers changed from: private */
    public Button mPausePlay;
    private Button mRewind;
    private Button mStop;
    /* access modifiers changed from: private */
    public MillennialMediaView mVideoView;
    /* access modifiers changed from: private */
    public boolean paused = false;
    private RelativeLayout relLayout;
    private boolean showBottomBar = true;
    /* access modifiers changed from: private */
    public boolean showCountdownHud = true;
    /* access modifiers changed from: private */
    public VideoAd videoAd;
    private boolean videoCompleted;
    protected VideoServer videoServer;

    /* JADX WARNING: Removed duplicated region for block: B:17:0x01aa  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x01fd  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x020f  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x021c  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x04b5  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x04bd  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x053a  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x06a6  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x06b7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r31) {
        /*
            r30 = this;
            super.onCreate(r31)
            java.lang.String r25 = "Setting up the video player"
            com.millennialmedia.android.MMAdViewSDK.Log.d(r25)
            if (r31 == 0) goto L_0x0489
            java.lang.String r25 = "isCachedAd"
            r0 = r31
            r1 = r25
            boolean r25 = r0.getBoolean(r1)
            r0 = r25
            r1 = r30
            r1.isCachedAd = r0
            java.lang.String r25 = "videoCompleted"
            r0 = r31
            r1 = r25
            boolean r25 = r0.getBoolean(r1)
            r0 = r25
            r1 = r30
            r1.videoCompleted = r0
            java.lang.String r25 = "videoPosition"
            r0 = r31
            r1 = r25
            int r25 = r0.getInt(r1)
            r0 = r25
            r1 = r30
            r1.currentVideoPosition = r0
        L_0x003a:
            android.widget.RelativeLayout r25 = new android.widget.RelativeLayout
            r0 = r25
            r1 = r30
            r0.<init>(r1)
            r0 = r25
            r1 = r30
            r1.relLayout = r0
            r0 = r30
            android.widget.RelativeLayout r0 = r0.relLayout
            r25 = r0
            r26 = 400(0x190, float:5.6E-43)
            r25.setId(r26)
            r0 = r30
            android.widget.RelativeLayout r0 = r0.relLayout
            r25 = r0
            android.view.ViewGroup$LayoutParams r26 = new android.view.ViewGroup$LayoutParams
            r27 = -1
            r28 = -1
            r26.<init>(r27, r28)
            r25.setLayoutParams(r26)
            r0 = r30
            android.widget.RelativeLayout r0 = r0.relLayout
            r25 = r0
            r26 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r25.setBackgroundColor(r26)
            r0 = r30
            android.widget.RelativeLayout r0 = r0.relLayout
            r25 = r0
            r26 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r25.setDrawingCacheBackgroundColor(r26)
            android.widget.RelativeLayout r23 = new android.widget.RelativeLayout
            r0 = r23
            r1 = r30
            r0.<init>(r1)
            r25 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r0 = r23
            r1 = r25
            r0.setBackgroundColor(r1)
            r25 = 410(0x19a, float:5.75E-43)
            r0 = r23
            r1 = r25
            r0.setId(r1)
            android.widget.RelativeLayout$LayoutParams r22 = new android.widget.RelativeLayout$LayoutParams
            r25 = -1
            r26 = -1
            r0 = r22
            r1 = r25
            r2 = r26
            r0.<init>(r1, r2)
            r25 = 13
            r0 = r22
            r1 = r25
            r0.addRule(r1)
            r0 = r23
            r1 = r22
            r0.setLayoutParams(r1)
            r25 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r0 = r23
            r1 = r25
            r0.setDrawingCacheBackgroundColor(r1)
            android.widget.RelativeLayout$LayoutParams r24 = new android.widget.RelativeLayout$LayoutParams
            r25 = -1
            r26 = -1
            r24.<init>(r25, r26)
            r25 = 13
            r24.addRule(r25)
            com.millennialmedia.android.MillennialMediaView r25 = new com.millennialmedia.android.MillennialMediaView
            r0 = r25
            r1 = r30
            r0.<init>(r1)
            r0 = r25
            r1 = r30
            r1.mVideoView = r0
            r0 = r30
            com.millennialmedia.android.MillennialMediaView r0 = r0.mVideoView
            r25 = r0
            r26 = 411(0x19b, float:5.76E-43)
            r25.setId(r26)
            r0 = r30
            com.millennialmedia.android.MillennialMediaView r0 = r0.mVideoView
            r25 = r0
            android.view.SurfaceHolder r25 = r25.getHolder()
            r26 = -2
            r25.setFormat(r26)
            r0 = r30
            com.millennialmedia.android.MillennialMediaView r0 = r0.mVideoView
            r25 = r0
            r0 = r23
            r1 = r25
            r2 = r24
            r0.addView(r1, r2)
            r0 = r30
            com.millennialmedia.android.MillennialMediaView r0 = r0.mVideoView
            r25 = r0
            r26 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r25.setDrawingCacheBackgroundColor(r26)
            r0 = r30
            android.widget.RelativeLayout r0 = r0.relLayout
            r25 = r0
            r0 = r25
            r1 = r23
            r2 = r22
            r0.addView(r1, r2)
            android.widget.RelativeLayout$LayoutParams r8 = new android.widget.RelativeLayout$LayoutParams
            r25 = -1
            r26 = -1
            r0 = r8
            r1 = r25
            r2 = r26
            r0.<init>(r1, r2)
            java.lang.StringBuilder r25 = new java.lang.StringBuilder
            r25.<init>()
            java.lang.String r26 = "Is Cached Ad: "
            java.lang.StringBuilder r25 = r25.append(r26)
            r0 = r30
            boolean r0 = r0.isCachedAd
            r26 = r0
            java.lang.StringBuilder r25 = r25.append(r26)
            java.lang.String r25 = r25.toString()
            com.millennialmedia.android.MMAdViewSDK.Log.v(r25)
            r0 = r30
            boolean r0 = r0.isCachedAd
            r25 = r0
            if (r25 == 0) goto L_0x0532
            android.os.Handler r25 = new android.os.Handler
            r0 = r25
            r1 = r30
            r0.<init>(r1)
            r0 = r25
            r1 = r30
            r1.handler = r0
            r25 = 0
            r0 = r30
            r1 = r25
            r0.setRequestedOrientation(r1)
            if (r31 != 0) goto L_0x04c1
            android.content.Intent r25 = r30.getIntent()
            java.lang.String r26 = "adName"
            java.lang.String r16 = r25.getStringExtra(r26)
            r11 = 0
            com.millennialmedia.android.AdDatabaseHelper r12 = new com.millennialmedia.android.AdDatabaseHelper     // Catch:{ SQLiteException -> 0x04ad }
            r0 = r12
            r1 = r30
            r0.<init>(r1)     // Catch:{ SQLiteException -> 0x04ad }
            r0 = r12
            r1 = r16
            com.millennialmedia.android.VideoAd r25 = r0.getVideoAd(r1)     // Catch:{ SQLiteException -> 0x06d8, all -> 0x06d4 }
            r0 = r25
            r1 = r30
            r1.videoAd = r0     // Catch:{ SQLiteException -> 0x06d8, all -> 0x06d4 }
            if (r12 == 0) goto L_0x06de
            r12.close()
            r11 = r12
        L_0x0191:
            com.millennialmedia.android.EventLogSet r25 = new com.millennialmedia.android.EventLogSet
            r0 = r30
            com.millennialmedia.android.VideoAd r0 = r0.videoAd
            r26 = r0
            r25.<init>(r26)
            r0 = r25
            r1 = r30
            r1.logSet = r0
            r0 = r30
            com.millennialmedia.android.VideoAd r0 = r0.videoAd
            r25 = r0
            if (r25 == 0) goto L_0x01ce
            r0 = r30
            com.millennialmedia.android.VideoAd r0 = r0.videoAd
            r25 = r0
            r0 = r25
            boolean r0 = r0.showControls
            r25 = r0
            r0 = r25
            r1 = r30
            r1.showBottomBar = r0
            r0 = r30
            com.millennialmedia.android.VideoAd r0 = r0.videoAd
            r25 = r0
            r0 = r25
            boolean r0 = r0.showCountdown
            r25 = r0
            r0 = r25
            r1 = r30
            r1.showCountdownHud = r0
        L_0x01ce:
            r0 = r30
            com.millennialmedia.android.EventLogSet r0 = r0.logSet
            r25 = r0
            r0 = r30
            r1 = r25
            r0.logBeginEvent(r1)
        L_0x01db:
            android.widget.RelativeLayout r25 = new android.widget.RelativeLayout
            r0 = r25
            r1 = r30
            r0.<init>(r1)
            r0 = r25
            r1 = r30
            r1.buttonsLayout = r0
            r0 = r30
            android.widget.RelativeLayout r0 = r0.buttonsLayout
            r25 = r0
            r26 = 420(0x1a4, float:5.89E-43)
            r25.setId(r26)
            r0 = r30
            boolean r0 = r0.showCountdownHud
            r25 = r0
            if (r25 == 0) goto L_0x0206
            r25 = 0
            r0 = r30
            r1 = r25
            r0.showHud(r1)
        L_0x0206:
            r7 = 0
            r0 = r30
            com.millennialmedia.android.VideoAd r0 = r0.videoAd
            r25 = r0
            if (r25 == 0) goto L_0x021a
            r0 = r30
            com.millennialmedia.android.VideoAd r0 = r0.videoAd
            r25 = r0
            r0 = r25
            java.util.ArrayList<com.millennialmedia.android.VideoImage> r0 = r0.buttons
            r7 = r0
        L_0x021a:
            if (r7 == 0) goto L_0x0532
            r14 = 0
        L_0x021d:
            int r25 = r7.size()
            r0 = r14
            r1 = r25
            if (r0 >= r1) goto L_0x051e
            r0 = r30
            com.millennialmedia.android.VideoAd r0 = r0.videoAd
            r25 = r0
            r0 = r25
            boolean r0 = r0.storedOnSdCard
            r25 = r0
            if (r25 == 0) goto L_0x0507
            java.io.File r9 = new java.io.File
            java.lang.StringBuilder r25 = new java.lang.StringBuilder
            r25.<init>()
            java.io.File r26 = android.os.Environment.getExternalStorageDirectory()
            java.lang.String r26 = r26.getAbsolutePath()
            java.lang.StringBuilder r25 = r25.append(r26)
            java.lang.String r26 = "/"
            java.lang.StringBuilder r25 = r25.append(r26)
            java.lang.String r26 = "millennialmedia"
            java.lang.StringBuilder r25 = r25.append(r26)
            java.lang.String r25 = r25.toString()
            r0 = r9
            r1 = r25
            r0.<init>(r1)
        L_0x025d:
            android.widget.ImageButton r17 = new android.widget.ImageButton
            r0 = r17
            r1 = r30
            r0.<init>(r1)
            java.lang.Object r31 = r7.get(r14)
            com.millennialmedia.android.VideoImage r31 = (com.millennialmedia.android.VideoImage) r31
            r0 = r17
            r1 = r31
            r1.button = r0
            java.lang.StringBuilder r25 = new java.lang.StringBuilder
            r25.<init>()
            r0 = r25
            r1 = r9
            java.lang.StringBuilder r25 = r0.append(r1)
            java.lang.String r26 = "/"
            java.lang.StringBuilder r25 = r25.append(r26)
            r0 = r30
            com.millennialmedia.android.VideoAd r0 = r0.videoAd
            r26 = r0
            r0 = r26
            java.lang.String r0 = r0.id
            r26 = r0
            java.lang.StringBuilder r25 = r25.append(r26)
            java.lang.String r26 = "/"
            java.lang.StringBuilder r25 = r25.append(r26)
            java.lang.Object r31 = r7.get(r14)
            com.millennialmedia.android.VideoImage r31 = (com.millennialmedia.android.VideoImage) r31
            r0 = r31
            java.lang.String r0 = r0.imageUrl
            r26 = r0
            android.net.Uri r26 = android.net.Uri.parse(r26)
            java.lang.String r26 = r26.getLastPathSegment()
            java.lang.String r27 = "\\.[^\\.]*$"
            java.lang.String r28 = ".dat"
            java.lang.String r26 = r26.replaceFirst(r27, r28)
            java.lang.StringBuilder r25 = r25.append(r26)
            java.lang.String r6 = r25.toString()
            android.net.Uri r25 = android.net.Uri.parse(r6)
            r0 = r17
            r1 = r25
            r0.setImageURI(r1)
            r25 = 0
            r26 = 0
            r27 = 0
            r28 = 0
            r0 = r17
            r1 = r25
            r2 = r26
            r3 = r27
            r4 = r28
            r0.setPadding(r1, r2, r3, r4)
            r25 = 0
            r0 = r17
            r1 = r25
            r0.setBackgroundColor(r1)
            java.lang.Object r31 = r7.get(r14)
            com.millennialmedia.android.VideoImage r31 = (com.millennialmedia.android.VideoImage) r31
            r0 = r31
            float r0 = r0.fromAlpha
            r25 = r0
            r0 = r30
            r1 = r17
            r2 = r25
            r0.setButtonAlpha(r1, r2)
            int r25 = r14 + 1
            r0 = r17
            r1 = r25
            r0.setId(r1)
            android.widget.RelativeLayout$LayoutParams r18 = new android.widget.RelativeLayout$LayoutParams
            r25 = -2
            r26 = -2
            r0 = r18
            r1 = r25
            r2 = r26
            r0.<init>(r1, r2)
            java.lang.StringBuilder r25 = new java.lang.StringBuilder
            r25.<init>()
            java.lang.String r26 = "Array #: "
            java.lang.StringBuilder r25 = r25.append(r26)
            int r26 = r17.getId()
            r27 = 1
            int r26 = r26 - r27
            java.lang.StringBuilder r25 = r25.append(r26)
            java.lang.String r26 = " View ID: "
            java.lang.StringBuilder r25 = r25.append(r26)
            int r26 = r17.getId()
            java.lang.StringBuilder r25 = r25.append(r26)
            java.lang.String r26 = " Relative to "
            java.lang.StringBuilder r25 = r25.append(r26)
            java.lang.Object r31 = r7.get(r14)
            com.millennialmedia.android.VideoImage r31 = (com.millennialmedia.android.VideoImage) r31
            r0 = r31
            int r0 = r0.anchor
            r26 = r0
            java.lang.StringBuilder r25 = r25.append(r26)
            java.lang.String r26 = " position: "
            java.lang.StringBuilder r25 = r25.append(r26)
            java.lang.Object r31 = r7.get(r14)
            com.millennialmedia.android.VideoImage r31 = (com.millennialmedia.android.VideoImage) r31
            r0 = r31
            int r0 = r0.position
            r26 = r0
            java.lang.StringBuilder r25 = r25.append(r26)
            java.lang.String r25 = r25.toString()
            com.millennialmedia.android.MMAdViewSDK.Log.v(r25)
            java.lang.Object r31 = r7.get(r14)
            com.millennialmedia.android.VideoImage r31 = (com.millennialmedia.android.VideoImage) r31
            r0 = r31
            int r0 = r0.position
            r25 = r0
            java.lang.Object r31 = r7.get(r14)
            com.millennialmedia.android.VideoImage r31 = (com.millennialmedia.android.VideoImage) r31
            r0 = r31
            int r0 = r0.anchor
            r26 = r0
            r0 = r18
            r1 = r25
            r2 = r26
            r0.addRule(r1, r2)
            java.lang.Object r31 = r7.get(r14)
            com.millennialmedia.android.VideoImage r31 = (com.millennialmedia.android.VideoImage) r31
            r0 = r31
            int r0 = r0.position2
            r25 = r0
            java.lang.Object r31 = r7.get(r14)
            com.millennialmedia.android.VideoImage r31 = (com.millennialmedia.android.VideoImage) r31
            r0 = r31
            int r0 = r0.anchor2
            r26 = r0
            r0 = r18
            r1 = r25
            r2 = r26
            r0.addRule(r1, r2)
            java.lang.Object r31 = r7.get(r14)
            com.millennialmedia.android.VideoImage r31 = (com.millennialmedia.android.VideoImage) r31
            r0 = r31
            int r0 = r0.paddingLeft
            r25 = r0
            java.lang.Object r31 = r7.get(r14)
            com.millennialmedia.android.VideoImage r31 = (com.millennialmedia.android.VideoImage) r31
            r0 = r31
            int r0 = r0.paddingTop
            r26 = r0
            java.lang.Object r31 = r7.get(r14)
            com.millennialmedia.android.VideoImage r31 = (com.millennialmedia.android.VideoImage) r31
            r0 = r31
            int r0 = r0.paddingRight
            r27 = r0
            java.lang.Object r31 = r7.get(r14)
            com.millennialmedia.android.VideoImage r31 = (com.millennialmedia.android.VideoImage) r31
            r0 = r31
            int r0 = r0.paddingBottom
            r28 = r0
            r0 = r18
            r1 = r25
            r2 = r26
            r3 = r27
            r4 = r28
            r0.setMargins(r1, r2, r3, r4)
            java.lang.Object r5 = r7.get(r14)
            com.millennialmedia.android.VideoImage r5 = (com.millennialmedia.android.VideoImage) r5
            r0 = r5
            java.lang.String r0 = r0.linkUrl
            r25 = r0
            boolean r25 = android.text.TextUtils.isEmpty(r25)
            if (r25 != 0) goto L_0x040d
            com.millennialmedia.android.VideoPlayer$1 r25 = new com.millennialmedia.android.VideoPlayer$1
            r0 = r25
            r1 = r30
            r2 = r5
            r0.<init>(r2)
            r0 = r17
            r1 = r25
            r0.setOnClickListener(r1)
        L_0x040d:
            r0 = r5
            long r0 = r0.appearanceDelay
            r25 = r0
            r27 = 0
            int r25 = (r25 > r27 ? 1 : (r25 == r27 ? 0 : -1))
            if (r25 <= 0) goto L_0x050d
            java.lang.Object r31 = r7.get(r14)
            com.millennialmedia.android.VideoImage r31 = (com.millennialmedia.android.VideoImage) r31
            r0 = r18
            r1 = r31
            r1.layoutParams = r0
            r0 = r30
            android.os.Handler r0 = r0.handler
            r25 = r0
            r26 = 3
            r0 = r25
            r1 = r26
            r2 = r5
            android.os.Message r15 = android.os.Message.obtain(r0, r1, r2)
            r0 = r30
            android.os.Handler r0 = r0.handler
            r25 = r0
            r0 = r5
            long r0 = r0.appearanceDelay
            r26 = r0
            r0 = r25
            r1 = r15
            r2 = r26
            r0.sendMessageDelayed(r1, r2)
        L_0x0448:
            r0 = r5
            long r0 = r0.inactivityTimeout
            r25 = r0
            r27 = 0
            int r25 = (r25 > r27 ? 1 : (r25 == r27 ? 0 : -1))
            if (r25 <= 0) goto L_0x0485
            r0 = r30
            android.os.Handler r0 = r0.handler
            r25 = r0
            r26 = 1
            r0 = r25
            r1 = r26
            r2 = r5
            android.os.Message r15 = android.os.Message.obtain(r0, r1, r2)
            r0 = r30
            android.os.Handler r0 = r0.handler
            r25 = r0
            r0 = r5
            long r0 = r0.inactivityTimeout
            r26 = r0
            r0 = r5
            long r0 = r0.appearanceDelay
            r28 = r0
            long r26 = r26 + r28
            r0 = r5
            long r0 = r0.fadeDuration
            r28 = r0
            long r26 = r26 + r28
            r0 = r25
            r1 = r15
            r2 = r26
            r0.sendMessageDelayed(r1, r2)
        L_0x0485:
            int r14 = r14 + 1
            goto L_0x021d
        L_0x0489:
            android.content.Intent r25 = r30.getIntent()
            java.lang.String r26 = "cached"
            r27 = 0
            boolean r25 = r25.getBooleanExtra(r26, r27)
            r0 = r25
            r1 = r30
            r1.isCachedAd = r0
            r25 = 0
            r0 = r25
            r1 = r30
            r1.currentVideoPosition = r0
            r25 = 0
            r0 = r25
            r1 = r30
            r1.videoCompleted = r0
            goto L_0x003a
        L_0x04ad:
            r25 = move-exception
            r13 = r25
        L_0x04b0:
            r13.printStackTrace()     // Catch:{ all -> 0x04ba }
            if (r11 == 0) goto L_0x0191
            r11.close()
            goto L_0x0191
        L_0x04ba:
            r25 = move-exception
        L_0x04bb:
            if (r11 == 0) goto L_0x04c0
            r11.close()
        L_0x04c0:
            throw r25
        L_0x04c1:
            java.lang.String r25 = "videoAd"
            r0 = r31
            r1 = r25
            android.os.Parcelable r5 = r0.getParcelable(r1)
            com.millennialmedia.android.VideoAd r5 = (com.millennialmedia.android.VideoAd) r5
            r0 = r5
            r1 = r30
            r1.videoAd = r0
            java.lang.String r25 = "logSet"
            r0 = r31
            r1 = r25
            android.os.Parcelable r5 = r0.getParcelable(r1)
            com.millennialmedia.android.EventLogSet r5 = (com.millennialmedia.android.EventLogSet) r5
            r0 = r5
            r1 = r30
            r1.logSet = r0
            java.lang.String r25 = "shouldShowBottomBar"
            r0 = r31
            r1 = r25
            boolean r25 = r0.getBoolean(r1)
            r0 = r25
            r1 = r30
            r1.showBottomBar = r0
            r0 = r30
            com.millennialmedia.android.VideoAd r0 = r0.videoAd
            r25 = r0
            r0 = r25
            boolean r0 = r0.showCountdown
            r25 = r0
            r0 = r25
            r1 = r30
            r1.showCountdownHud = r0
            goto L_0x01db
        L_0x0507:
            java.io.File r9 = r30.getCacheDir()
            goto L_0x025d
        L_0x050d:
            r0 = r30
            android.widget.RelativeLayout r0 = r0.buttonsLayout
            r25 = r0
            r0 = r25
            r1 = r17
            r2 = r18
            r0.addView(r1, r2)
            goto L_0x0448
        L_0x051e:
            r0 = r30
            android.widget.RelativeLayout r0 = r0.relLayout
            r25 = r0
            r0 = r30
            android.widget.RelativeLayout r0 = r0.buttonsLayout
            r26 = r0
            r0 = r25
            r1 = r26
            r2 = r8
            r0.addView(r1, r2)
        L_0x0532:
            r0 = r30
            boolean r0 = r0.showBottomBar
            r25 = r0
            if (r25 == 0) goto L_0x069e
            android.widget.RelativeLayout r25 = new android.widget.RelativeLayout
            r0 = r25
            r1 = r30
            r0.<init>(r1)
            r0 = r25
            r1 = r30
            r1.controlsLayout = r0
            r0 = r30
            android.widget.RelativeLayout r0 = r0.controlsLayout
            r25 = r0
            r26 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r25.setBackgroundColor(r26)
            android.widget.RelativeLayout$LayoutParams r10 = new android.widget.RelativeLayout$LayoutParams
            r25 = -1
            r26 = -2
            r0 = r10
            r1 = r25
            r2 = r26
            r0.<init>(r1, r2)
            r0 = r30
            android.widget.RelativeLayout r0 = r0.controlsLayout
            r25 = r0
            r0 = r25
            r1 = r10
            r0.setLayoutParams(r1)
            r25 = 12
            r0 = r10
            r1 = r25
            r0.addRule(r1)
            android.widget.Button r25 = new android.widget.Button
            r0 = r25
            r1 = r30
            r0.<init>(r1)
            r0 = r25
            r1 = r30
            r1.mRewind = r0
            android.widget.Button r25 = new android.widget.Button
            r0 = r25
            r1 = r30
            r0.<init>(r1)
            r0 = r25
            r1 = r30
            r1.mPausePlay = r0
            android.widget.Button r25 = new android.widget.Button
            r0 = r25
            r1 = r30
            r0.<init>(r1)
            r0 = r25
            r1 = r30
            r1.mStop = r0
            r0 = r30
            android.widget.Button r0 = r0.mRewind
            r25 = r0
            r26 = 17301541(0x1080025, float:2.497936E-38)
            r25.setBackgroundResource(r26)
            r0 = r30
            android.widget.Button r0 = r0.mPausePlay
            r25 = r0
            r26 = 17301539(0x1080023, float:2.4979353E-38)
            r25.setBackgroundResource(r26)
            r0 = r30
            android.widget.Button r0 = r0.mStop
            r25 = r0
            r26 = 17301560(0x1080038, float:2.4979412E-38)
            r25.setBackgroundResource(r26)
            android.widget.RelativeLayout$LayoutParams r19 = new android.widget.RelativeLayout$LayoutParams
            r25 = -2
            r26 = -2
            r0 = r19
            r1 = r25
            r2 = r26
            r0.<init>(r1, r2)
            android.widget.RelativeLayout$LayoutParams r21 = new android.widget.RelativeLayout$LayoutParams
            r25 = -2
            r26 = -2
            r0 = r21
            r1 = r25
            r2 = r26
            r0.<init>(r1, r2)
            android.widget.RelativeLayout$LayoutParams r20 = new android.widget.RelativeLayout$LayoutParams
            r25 = -2
            r26 = -2
            r0 = r20
            r1 = r25
            r2 = r26
            r0.<init>(r1, r2)
            r25 = 14
            r0 = r19
            r1 = r25
            r0.addRule(r1)
            r0 = r30
            android.widget.RelativeLayout r0 = r0.controlsLayout
            r25 = r0
            r0 = r30
            android.widget.Button r0 = r0.mPausePlay
            r26 = r0
            r0 = r25
            r1 = r26
            r2 = r19
            r0.addView(r1, r2)
            r25 = 0
            r0 = r30
            android.widget.Button r0 = r0.mPausePlay
            r26 = r0
            int r26 = r26.getId()
            r0 = r20
            r1 = r25
            r2 = r26
            r0.addRule(r1, r2)
            r0 = r30
            android.widget.RelativeLayout r0 = r0.controlsLayout
            r25 = r0
            r0 = r30
            android.widget.Button r0 = r0.mRewind
            r26 = r0
            r25.addView(r26)
            r25 = 11
            r0 = r21
            r1 = r25
            r0.addRule(r1)
            r0 = r30
            android.widget.RelativeLayout r0 = r0.controlsLayout
            r25 = r0
            r0 = r30
            android.widget.Button r0 = r0.mStop
            r26 = r0
            r0 = r25
            r1 = r26
            r2 = r21
            r0.addView(r1, r2)
            r0 = r30
            android.widget.Button r0 = r0.mRewind
            r25 = r0
            com.millennialmedia.android.VideoPlayer$2 r26 = new com.millennialmedia.android.VideoPlayer$2
            r0 = r26
            r1 = r30
            r0.<init>()
            r25.setOnClickListener(r26)
            r0 = r30
            android.widget.Button r0 = r0.mPausePlay
            r25 = r0
            com.millennialmedia.android.VideoPlayer$3 r26 = new com.millennialmedia.android.VideoPlayer$3
            r0 = r26
            r1 = r30
            r0.<init>()
            r25.setOnClickListener(r26)
            r0 = r30
            android.widget.Button r0 = r0.mStop
            r25 = r0
            com.millennialmedia.android.VideoPlayer$4 r26 = new com.millennialmedia.android.VideoPlayer$4
            r0 = r26
            r1 = r30
            r0.<init>()
            r25.setOnClickListener(r26)
            r0 = r30
            android.widget.RelativeLayout r0 = r0.relLayout
            r25 = r0
            r0 = r30
            android.widget.RelativeLayout r0 = r0.controlsLayout
            r26 = r0
            r0 = r25
            r1 = r26
            r2 = r10
            r0.addView(r1, r2)
        L_0x069e:
            r0 = r30
            android.widget.RelativeLayout r0 = r0.controlsLayout
            r25 = r0
            if (r25 == 0) goto L_0x06af
            r0 = r30
            android.widget.RelativeLayout r0 = r0.controlsLayout
            r25 = r0
            r25.bringToFront()
        L_0x06af:
            r0 = r30
            android.widget.RelativeLayout r0 = r0.buttonsLayout
            r25 = r0
            if (r25 == 0) goto L_0x06c6
            r0 = r30
            android.widget.RelativeLayout r0 = r0.relLayout
            r25 = r0
            r0 = r30
            android.widget.RelativeLayout r0 = r0.buttonsLayout
            r26 = r0
            r25.bringChildToFront(r26)
        L_0x06c6:
            r0 = r30
            android.widget.RelativeLayout r0 = r0.relLayout
            r25 = r0
            r0 = r30
            r1 = r25
            r0.setContentView(r1)
            return
        L_0x06d4:
            r25 = move-exception
            r11 = r12
            goto L_0x04bb
        L_0x06d8:
            r25 = move-exception
            r13 = r25
            r11 = r12
            goto L_0x04b0
        L_0x06de:
            r11 = r12
            goto L_0x0191
        */
        throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.android.VideoPlayer.onCreate(android.os.Bundle):void");
    }

    private boolean canFadeButtons() {
        if (!this.videoAd.stayInPlayer || !this.videoCompleted) {
            return true;
        }
        return false;
    }

    private void setButtonAlpha(ImageButton button, float alpha) {
        AlphaAnimation animation = new AlphaAnimation(alpha, alpha);
        animation.setDuration(0);
        animation.setFillEnabled(true);
        animation.setFillBefore(true);
        animation.setFillAfter(true);
        button.startAnimation(animation);
    }

    private class DelayedAnimationListener implements Animation.AnimationListener {
        private ImageButton button;
        private RelativeLayout.LayoutParams layoutParams;

        DelayedAnimationListener(ImageButton b, RelativeLayout.LayoutParams lp) {
            this.button = b;
            this.layoutParams = lp;
        }

        public void onAnimationStart(Animation animation) {
        }

        public void onAnimationEnd(Animation animation) {
        }

        public void onAnimationRepeat(Animation animation) {
        }
    }

    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            case 1:
                if (canFadeButtons()) {
                    VideoImage videoImage = (VideoImage) msg.obj;
                    AlphaAnimation animation = new AlphaAnimation(videoImage.fromAlpha, videoImage.toAlpha);
                    animation.setDuration(videoImage.fadeDuration);
                    animation.setFillEnabled(true);
                    animation.setFillBefore(true);
                    animation.setFillAfter(true);
                    videoImage.button.startAnimation(animation);
                    break;
                }
                break;
            case 2:
                try {
                    if (this.mVideoView.isPlaying()) {
                        int currentPosition = this.mVideoView.getCurrentPosition();
                        if (currentPosition > this.lastVideoPosition) {
                            if (this.videoAd != null) {
                                for (int i = 0; i < this.videoAd.activities.size(); i++) {
                                    VideoLogEvent videoEvent = this.videoAd.activities.get(i);
                                    if (videoEvent != null && videoEvent.position >= ((long) this.lastVideoPosition) && videoEvent.position < ((long) currentPosition)) {
                                        for (int j = 0; j < videoEvent.activities.length; j++) {
                                            try {
                                                logEvent(videoEvent.activities[j]);
                                            } catch (UnsupportedEncodingException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                }
                            }
                            this.lastVideoPosition = currentPosition;
                        }
                        if (this.showCountdownHud) {
                            long seconds = (this.videoAd.duration - ((long) currentPosition)) / 1000;
                            if (seconds <= 0) {
                                hideHud();
                            } else if (this.hudSeconds != null) {
                                this.hudSeconds.setText(String.valueOf(seconds));
                            }
                        }
                    }
                    this.handler.sendMessageDelayed(Message.obtain(this.handler, 2), 1000);
                    break;
                } catch (IllegalStateException e2) {
                    e2.printStackTrace();
                    break;
                }
            case 3:
                VideoImage videoImage2 = (VideoImage) msg.obj;
                try {
                    if (this.buttonsLayout.indexOfChild(videoImage2.button) == -1) {
                        this.buttonsLayout.addView(videoImage2.button, videoImage2.layoutParams);
                    }
                } catch (IllegalStateException e3) {
                    e3.printStackTrace();
                }
                AlphaAnimation animation2 = new AlphaAnimation(videoImage2.toAlpha, videoImage2.fromAlpha);
                animation2.setDuration(videoImage2.fadeDuration);
                animation2.setAnimationListener(new DelayedAnimationListener(videoImage2.button, videoImage2.layoutParams));
                animation2.setFillEnabled(true);
                animation2.setFillBefore(true);
                animation2.setFillAfter(true);
                MMAdViewSDK.Log.v("Beginning animation to visibility. Fade duration: " + videoImage2.fadeDuration + " Button: " + videoImage2.name + " Time: " + System.currentTimeMillis());
                videoImage2.button.startAnimation(animation2);
                break;
        }
        return true;
    }

    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (this.videoAd != null) {
            if (this.handler != null) {
                this.handler.removeMessages(1);
            }
            for (int i = 0; i < this.videoAd.buttons.size(); i++) {
                VideoImage videoImage = this.videoAd.buttons.get(i);
                setButtonAlpha(videoImage.button, videoImage.fromAlpha);
                if (videoImage.inactivityTimeout > 0) {
                    this.handler.sendMessageDelayed(Message.obtain(this.handler, 1, videoImage), videoImage.inactivityTimeout);
                } else if (ev.getAction() == 1) {
                    if (canFadeButtons()) {
                        AlphaAnimation animation = new AlphaAnimation(videoImage.fromAlpha, videoImage.toAlpha);
                        animation.setDuration(videoImage.fadeDuration);
                        animation.setFillEnabled(true);
                        animation.setFillBefore(true);
                        animation.setFillAfter(true);
                        videoImage.button.startAnimation(animation);
                    }
                } else if (ev.getAction() == 0) {
                    setButtonAlpha(videoImage.button, videoImage.fromAlpha);
                }
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    /* access modifiers changed from: protected */
    public void logBeginEvent(EventLogSet set) {
        if (set != null && set.startActivity != null) {
            try {
                MMAdViewSDK.Log.d("Cached video begin event logged");
                for (String logEvent : set.startActivity) {
                    logEvent(logEvent);
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void logEndEvent(EventLogSet set) {
        MMAdViewSDK.Log.d("Cached video end event logged");
        int i = 0;
        while (i < set.endActivity.length) {
            try {
                logEvent(set.endActivity[i]);
                i++;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void logButtonEvent(VideoImage button) {
        MMAdViewSDK.Log.d("Cached video button event logged");
        int i = 0;
        while (i < button.activity.length) {
            try {
                logEvent(button.activity[i]);
                i++;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void logEvent(String activity) throws UnsupportedEncodingException {
        final String logString = activity;
        MMAdViewSDK.Log.d("Logging event to: " + logString);
        new Thread(new Runnable() {
            public void run() {
                try {
                    new HttpGetRequest().get(logString);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void showHud(boolean restart) {
        if (this.hudStaticText == null || this.hudSeconds == null) {
            RelativeLayout.LayoutParams hudLp = new RelativeLayout.LayoutParams(-2, -2);
            RelativeLayout.LayoutParams hudSecLp = new RelativeLayout.LayoutParams(-2, -2);
            this.hudStaticText = new TextView(this);
            this.hudStaticText.setText(" seconds remaining ...");
            this.hudStaticText.setTextColor(-1);
            this.hudStaticText.setPadding(0, 0, 5, 0);
            this.hudSeconds = new TextView(this);
            if (restart) {
                if (this.videoAd != null) {
                    this.hudSeconds.setText(String.valueOf(this.videoAd.duration / 1000));
                }
            } else if (this.currentVideoPosition != 0) {
                this.hudSeconds.setText(String.valueOf(this.currentVideoPosition / 1000));
            } else if (this.videoAd != null) {
                this.hudSeconds.setText(String.valueOf(this.videoAd.duration / 1000));
            }
            this.hudSeconds.setTextColor(-1);
            this.hudSeconds.setId(401);
            this.hudStaticText.setId(402);
            hudLp.addRule(10);
            hudLp.addRule(11);
            this.buttonsLayout.addView(this.hudStaticText, hudLp);
            hudSecLp.addRule(10);
            hudSecLp.addRule(0, this.hudStaticText.getId());
            this.buttonsLayout.addView(this.hudSeconds, hudSecLp);
            return;
        }
        if (restart) {
            if (this.videoAd != null) {
                this.hudSeconds.setText(String.valueOf(this.videoAd.duration / 1000));
            } else {
                this.hudSeconds.setText("");
            }
        }
        this.hudStaticText.setVisibility(0);
        this.hudSeconds.setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void hideHud() {
        if (this.hudStaticText != null) {
            this.hudStaticText.setVisibility(4);
        }
        if (this.hudSeconds != null) {
            this.hudSeconds.setVisibility(4);
        }
    }

    /* access modifiers changed from: private */
    public void playVideo(final int position) {
        try {
            String path = getIntent().getData().toString();
            MMAdViewSDK.Log.d("playVideo path: " + path);
            if (path == null || path.length() == 0) {
                Toast.makeText(this, "Sorry. There was a problem playing the video", 1).show();
                return;
            }
            SharedPreferences.Editor editor = getSharedPreferences("MillennialMediaSettings", 0).edit();
            editor.putBoolean("lastAdViewed", true);
            editor.commit();
            this.videoCompleted = false;
            if (path.equals(this.current) && this.mVideoView != null) {
                if (!this.isCachedAd) {
                    this.mVideoView.requestFocus();
                    this.mVideoView.start();
                    this.mVideoView.seekTo(position);
                    return;
                } else if (this.videoAd == null) {
                    return;
                } else {
                    if (this.videoAd.storedOnSdCard) {
                        this.mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            public void onCompletion(MediaPlayer mp) {
                                MMAdViewSDK.Log.d("Video Playing Complete");
                                if (VideoPlayer.this.showCountdownHud) {
                                    VideoPlayer.this.hideHud();
                                }
                                if (VideoPlayer.this.videoAd != null) {
                                    VideoPlayer.this.videoPlayerOnCompletion(VideoPlayer.this.videoAd.onCompletionUrl);
                                }
                            }
                        });
                        this.mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                            public void onPrepared(MediaPlayer mp) {
                                MMAdViewSDK.Log.d("Video Prepared");
                                VideoPlayer.this.mVideoView.seekTo(position);
                            }
                        });
                        this.mVideoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                            public boolean onError(MediaPlayer mp, int what, int extra) {
                                return false;
                            }
                        });
                        this.mVideoView.setVideoURI(Uri.parse(path));
                        this.mVideoView.requestFocus();
                        this.mVideoView.start();
                        this.mVideoView.seekTo(position);
                        return;
                    }
                    startServer(path, position, false);
                }
            }
            this.current = path;
            if (this.mVideoView == null) {
                Log.e(MMAdViewSDK.SDKLOG, "Video Player is Null");
            } else if (!this.isCachedAd) {
                this.mVideoView.setVideoURI(Uri.parse(path));
                this.mVideoView.requestFocus();
                this.mVideoView.start();
                this.mVideoView.seekTo(position);
            } else if (this.videoAd != null) {
                if (!this.videoAd.storedOnSdCard) {
                    MMAdViewSDK.Log.d("Cached Ad. Starting Server");
                    startServer(path, position, this.videoAd.storedOnSdCard);
                } else {
                    this.mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        public void onCompletion(MediaPlayer mp) {
                            MMAdViewSDK.Log.d("Video Playing Complete");
                            if (VideoPlayer.this.showCountdownHud) {
                                VideoPlayer.this.hideHud();
                            }
                            if (VideoPlayer.this.videoAd != null) {
                                VideoPlayer.this.videoPlayerOnCompletion(VideoPlayer.this.videoAd.onCompletionUrl);
                            }
                        }
                    });
                    this.mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        public void onPrepared(MediaPlayer mp) {
                            MMAdViewSDK.Log.d("Video Prepared");
                            VideoPlayer.this.mVideoView.seekTo(position);
                        }
                    });
                    this.mVideoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                        public boolean onError(MediaPlayer mp, int what, int extra) {
                            return false;
                        }
                    });
                }
                this.mVideoView.setVideoURI(Uri.parse(path));
                this.mVideoView.start();
                this.mVideoView.seekTo(position);
            }
        } catch (Exception e) {
            Exception e2 = e;
            Log.e(MMAdViewSDK.SDKLOG, "error: " + e2.getMessage(), e2);
            SharedPreferences.Editor editor2 = getSharedPreferences("MillennialMediaSettings", 0).edit();
            editor2.putBoolean("lastAdViewed", true);
            editor2.commit();
            Toast.makeText(this, "Sorry. There was a problem playing the video", 1).show();
            if (this.mVideoView != null) {
                this.mVideoView.stopPlayback();
            }
        }
    }

    public void onStart() {
        super.onStart();
        if (this.videoAd != null && this.videoAd.stayInPlayer && this.videoCompleted && this.videoAd.buttons != null) {
            for (int i = 0; i < this.videoAd.buttons.size(); i++) {
                VideoImage videoImage = this.videoAd.buttons.get(i);
                setButtonAlpha(videoImage.button, videoImage.fromAlpha);
                if (videoImage.button.getParent() == null) {
                    this.buttonsLayout.addView(videoImage.button, videoImage.layoutParams);
                }
                for (int j = 0; j < this.videoAd.buttons.size(); j++) {
                    this.buttonsLayout.bringChildToFront(this.videoAd.buttons.get(j).button);
                }
            }
        }
    }

    public void onResume() {
        super.onResume();
        if (this.mVideoView != null && !this.mVideoView.isPlaying() && !this.videoCompleted) {
            if (this.isCachedAd && !this.handler.hasMessages(2)) {
                this.handler.sendMessageDelayed(Message.obtain(this.handler, 2), 1000);
                if (this.showCountdownHud) {
                    long seconds = (this.videoAd.duration - ((long) this.currentVideoPosition)) / 1000;
                    if (seconds <= 0) {
                        hideHud();
                    } else if (this.hudSeconds != null) {
                        this.hudSeconds.setText(String.valueOf(seconds));
                    }
                }
                for (int i = 0; i < this.videoAd.buttons.size(); i++) {
                    VideoImage button = this.videoAd.buttons.get(i);
                    long delay = 0;
                    if (button.appearanceDelay > 0 && this.buttonsLayout.indexOfChild(button.button) == -1) {
                        Message message = Message.obtain(this.handler, 3, button);
                        delay = button.appearanceDelay - ((long) this.currentVideoPosition);
                        if (delay < 0) {
                            delay = 500;
                        }
                        this.handler.sendMessageDelayed(message, delay);
                    }
                    if (button.inactivityTimeout > 0) {
                        this.handler.sendMessageDelayed(Message.obtain(this.handler, 1, button), button.inactivityTimeout + delay + button.fadeDuration);
                    }
                }
            }
            playVideo(this.currentVideoPosition);
        }
    }

    public void onPause() {
        super.onPause();
        if (this.mVideoView != null) {
            this.currentVideoPosition = this.mVideoView.getCurrentPosition();
        }
        if (this.isCachedAd) {
            this.handler.removeMessages(1);
            this.handler.removeMessages(2);
            this.handler.removeMessages(3);
            stopServer();
        }
    }

    /* access modifiers changed from: private */
    public void dismiss() {
        MMAdViewSDK.Log.d("Video ad player closed");
        if (this.mVideoView != null) {
            this.mVideoView.stopPlayback();
        }
        finish();
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.isCachedAd) {
            stopServer();
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        if (this.mVideoView != null) {
            outState.putInt("videoPosition", this.mVideoView.getCurrentPosition());
        }
        outState.putBoolean("isCachedAd", this.isCachedAd);
        outState.putBoolean("videoCompleted", this.videoCompleted);
        outState.putParcelable("logSet", this.logSet);
        outState.putBoolean("shouldShowBottomBar", this.showBottomBar);
        outState.putParcelable("videoAd", this.videoAd);
        super.onSaveInstanceState(outState);
    }

    private void pauseVideo() {
        if (this.mVideoView != null && this.mVideoView.isPlaying()) {
            this.mVideoView.pause();
            this.paused = true;
            MMAdViewSDK.Log.v("Video paused");
        }
    }

    /* access modifiers changed from: private */
    public void videoPlayerOnCompletion(String url) {
        this.videoCompleted = true;
        logEndEvent(this.logSet);
        stopServer();
        MMAdViewSDK.Log.v("Video player on complete");
        if (url != null) {
            dispatchButtonClick(url);
        }
        if (this.videoAd == null) {
            return;
        }
        if (!this.videoAd.stayInPlayer) {
            dismiss();
            return;
        }
        if (this.videoAd.buttons != null) {
            for (int i = 0; i < this.videoAd.buttons.size(); i++) {
                VideoImage videoImage = this.videoAd.buttons.get(i);
                setButtonAlpha(videoImage.button, videoImage.fromAlpha);
                if (videoImage.button.getParent() == null) {
                    this.buttonsLayout.addView(videoImage.button, videoImage.layoutParams);
                }
                for (int j = 0; j < this.videoAd.buttons.size(); j++) {
                    this.buttonsLayout.bringChildToFront(this.videoAd.buttons.get(j).button);
                }
                MMAdViewSDK.Log.v("Button: " + i + " alpha: " + videoImage.fromAlpha);
            }
        }
        this.handler.removeMessages(1);
        this.handler.removeMessages(2);
        this.handler.removeMessages(3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void dispatchButtonClick(String urlString) {
        String browserAction;
        String locationString;
        int rc;
        MMAdViewSDK.Log.d("Button Clicked: " + urlString);
        if (urlString != null) {
            pauseVideo();
            if (urlString.startsWith("mmsdk")) {
                String action = urlString.substring(8);
                if (action != null) {
                    if (action.equalsIgnoreCase("restartVideo")) {
                        if (this.isCachedAd && this.videoAd != null) {
                            List<VideoImage> buttons = this.videoAd.buttons;
                            if (!(this.buttonsLayout == null || buttons == null)) {
                                this.handler.removeMessages(1);
                                this.handler.removeMessages(2);
                                this.handler.removeMessages(3);
                                this.lastVideoPosition = 0;
                                for (int i = 0; i < buttons.size(); i++) {
                                    MMAdViewSDK.Log.d("i: " + i);
                                    VideoImage buttonData = buttons.get(i);
                                    if (buttonData != null) {
                                        if (buttonData.appearanceDelay > 0) {
                                            this.buttonsLayout.removeView(buttonData.button);
                                            this.handler.sendMessageDelayed(Message.obtain(this.handler, 3, buttonData), buttonData.appearanceDelay);
                                        }
                                        if (buttonData.inactivityTimeout > 0) {
                                            this.handler.sendMessageDelayed(Message.obtain(this.handler, 1, buttonData), buttonData.inactivityTimeout + buttonData.appearanceDelay + buttonData.fadeDuration);
                                        }
                                        if (this.showCountdownHud) {
                                            showHud(true);
                                        }
                                        if (this.handler != null) {
                                            this.handler.sendMessageDelayed(Message.obtain(this.handler, 2), 1000);
                                        }
                                    }
                                }
                            }
                        }
                        if (this.mVideoView != null) {
                            playVideo(0);
                            return;
                        }
                    } else if (action.equalsIgnoreCase("endVideo")) {
                        MMAdViewSDK.Log.d("End");
                        if (this.mVideoView != null) {
                            this.current = null;
                            this.mVideoView.stopPlayback();
                            if (this.videoAd != null) {
                                dismiss();
                                return;
                            }
                            return;
                        }
                    } else {
                        MMAdViewSDK.Log.v("Unrecognized mmsdk:// URL");
                    }
                }
            } else if (urlString.startsWith("mmbrowser") && (browserAction = urlString.substring(12)) != null) {
                MMAdViewSDK.Log.v("Launch browser");
                try {
                    startActivity(new Intent("android.intent.action.VIEW", Uri.parse(browserAction)));
                    return;
                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                    return;
                }
            }
            String mimeTypeString = null;
            String redirectString = urlString;
            do {
                locationString = redirectString;
                if (redirectString != null) {
                    try {
                        URL url = new URL(redirectString);
                        HttpURLConnection.setFollowRedirects(false);
                        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                        conn.setRequestMethod("GET");
                        conn.connect();
                        redirectString = conn.getHeaderField("Location");
                        mimeTypeString = conn.getHeaderField("Content-Type");
                        rc = conn.getResponseCode();
                        MMAdViewSDK.Log.v("Response Code: " + conn.getResponseCode() + " Response Message: " + conn.getResponseMessage());
                        MMAdViewSDK.Log.v("urlString: " + urlString);
                        if (rc < 300) {
                            break;
                        }
                    } catch (MalformedURLException e2) {
                    } catch (IOException e3) {
                        e3.printStackTrace();
                    }
                } else {
                    return;
                }
            } while (rc < 400);
            MMAdViewSDK.Log.v("locationString: " + locationString);
            if (locationString != null) {
                Uri destinationURI = Uri.parse(locationString);
                if (mimeTypeString == null) {
                    mimeTypeString = "";
                }
                if (destinationURI.getScheme().equalsIgnoreCase("mmsdk")) {
                    if (destinationURI.getHost().equalsIgnoreCase("endVideo") && this.mVideoView != null) {
                        this.current = null;
                        this.mVideoView.stopPlayback();
                        dismiss();
                    }
                } else if ((destinationURI.getScheme().equalsIgnoreCase("http") || destinationURI.getScheme().equalsIgnoreCase("https")) && mimeTypeString.equalsIgnoreCase("text/html")) {
                    Intent intent = new Intent(this, MMAdViewOverlayActivity.class);
                    intent.setData(destinationURI);
                    intent.putExtra("cachedAdView", true);
                    startActivityForResult(intent, 0);
                } else if (destinationURI.getScheme().equalsIgnoreCase("market")) {
                    MMAdViewSDK.Log.v("Android Market URL, launch the Market Application");
                    startActivity(new Intent("android.intent.action.VIEW", destinationURI));
                } else if (destinationURI.getScheme().equalsIgnoreCase("rtsp") || (destinationURI.getScheme().equalsIgnoreCase("http") && (mimeTypeString.equalsIgnoreCase("video/mp4") || mimeTypeString.equalsIgnoreCase("video/3gpp")))) {
                    playVideo(0);
                } else if (destinationURI.getScheme().equalsIgnoreCase("tel")) {
                    MMAdViewSDK.Log.v("Telephone Number, launch the phone");
                    startActivity(new Intent("android.intent.action.DIAL", destinationURI));
                } else if (destinationURI.getScheme().equalsIgnoreCase("http")) {
                    Intent intent2 = new Intent(this, MMAdViewOverlayActivity.class);
                    intent2.setData(destinationURI);
                    intent2.putExtra("cachedAdView", true);
                    startActivityForResult(intent2, 0);
                } else if (destinationURI.getScheme().equalsIgnoreCase("mmbrowser")) {
                    String browserAction2 = urlString.substring(12);
                    if (browserAction2 != null) {
                        MMAdViewSDK.Log.v("Launch browser");
                        try {
                            startActivity(new Intent("android.intent.action.VIEW", Uri.parse(browserAction2)));
                        } catch (ActivityNotFoundException e4) {
                            e4.printStackTrace();
                        }
                    }
                } else {
                    MMAdViewSDK.Log.v("Uncertain about content, launch to browser");
                    startActivity(new Intent("android.intent.action.VIEW", destinationURI));
                }
            }
        }
    }

    public synchronized void startServer(String path, int position, boolean isSDCard) {
        if (this.videoServer == null) {
            this.videoServer = new VideoServer(path, isSDCard);
            Thread thread = new Thread(this.videoServer);
            thread.start();
            thread.getId();
            if (this.mVideoView != null) {
                this.mVideoView.setVideoURI(Uri.parse("http://localhost:" + this.videoServer.port + "/" + path + "/video.dat"));
                this.mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    public void onCompletion(MediaPlayer mp) {
                        MMAdViewSDK.Log.d("Video Playing Complete");
                        if (VideoPlayer.this.showCountdownHud) {
                            VideoPlayer.this.hideHud();
                        }
                        if (VideoPlayer.this.videoAd != null) {
                            VideoPlayer.this.videoPlayerOnCompletion(VideoPlayer.this.videoAd.onCompletionUrl);
                        }
                    }
                });
                this.mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    public void onPrepared(MediaPlayer mp) {
                        MMAdViewSDK.Log.d("Video Prepared");
                    }
                });
                this.mVideoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                    public boolean onError(MediaPlayer mp, int what, int extra) {
                        return false;
                    }
                });
                this.mVideoView.seekTo(position);
                this.mVideoView.requestFocus();
                this.mVideoView.start();
            } else {
                Log.e(MMAdViewSDK.SDKLOG, "Null Video View");
            }
        }
    }

    public synchronized void stopServer() {
        MMAdViewSDK.Log.d("Stop video server");
        if (this.videoServer != null) {
            this.videoServer.requestStop();
            this.videoServer = null;
        }
        if (this.mVideoView != null) {
            this.mVideoView.stopPlayback();
        }
    }

    private class VideoServer implements Runnable {
        private String cacheDir;
        boolean done = false;
        private final String filePath;
        Integer port;
        private ServerSocket serverSocket = null;

        public VideoServer(String filePath2, boolean isSDCard) {
            this.filePath = filePath2;
            if (isSDCard) {
                this.cacheDir = Environment.getExternalStorageDirectory().getPath() + "/" + "millennialmedia" + "/";
            } else {
                this.cacheDir = VideoPlayer.this.getCacheDir() + "/";
            }
            try {
                this.serverSocket = new ServerSocket();
                this.serverSocket.bind(null);
                this.serverSocket.setSoTimeout(0);
                this.port = new Integer(this.serverSocket.getLocalPort());
                MMAdViewSDK.Log.v("Video Server Port: " + this.port);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        /* JADX INFO: Multiple debug info for r12v7 java.io.IOException: [D('e' java.io.FileNotFoundException), D('e' java.io.IOException)] */
        /* JADX INFO: Multiple debug info for r12v9 java.io.IOException: [D('e' java.net.SocketTimeoutException), D('e' java.io.IOException)] */
        /* JADX WARNING: Removed duplicated region for block: B:108:0x045f A[SYNTHETIC, Splitter:B:108:0x045f] */
        /* JADX WARNING: Removed duplicated region for block: B:111:0x0464 A[Catch:{ IOException -> 0x046e }] */
        /* JADX WARNING: Removed duplicated region for block: B:113:0x0469 A[Catch:{ IOException -> 0x046e }] */
        /* JADX WARNING: Removed duplicated region for block: B:131:0x050f A[SYNTHETIC, Splitter:B:131:0x050f] */
        /* JADX WARNING: Removed duplicated region for block: B:134:0x0514 A[Catch:{ IOException -> 0x051e }] */
        /* JADX WARNING: Removed duplicated region for block: B:136:0x0519 A[Catch:{ IOException -> 0x051e }] */
        /* JADX WARNING: Removed duplicated region for block: B:174:0x000b A[SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:187:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:36:0x01b4  */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x01c1 A[Catch:{ IOException -> 0x0562 }] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r34 = this;
                r11 = 0
                r23 = 0
                r15 = 0
                r30 = 1024(0x400, float:1.435E-42)
                r0 = r30
                byte[] r0 = new byte[r0]
                r4 = r0
            L_0x000b:
                r0 = r34
                boolean r0 = r0.done
                r30 = r0
                if (r30 != 0) goto L_0x01ac
                r0 = r34
                java.net.ServerSocket r0 = r0.serverSocket     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r30 = r0
                java.net.Socket r11 = r30.accept()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.String r30 = "Accepted new incoming connection"
                com.millennialmedia.android.MMAdViewSDK.Log.v(r30)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.io.InputStream r17 = r11.getInputStream()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.io.OutputStream r23 = r11.getOutputStream()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.StringBuilder r29 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r29.<init>()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
            L_0x002f:
                r0 = r17
                r1 = r4
                r0.read(r1)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.String r30 = new java.lang.String     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r0 = r30
                r1 = r4
                r0.<init>(r1)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r29.append(r30)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.String r26 = r29.toString()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.String r30 = "\r\n\r\n"
                r0 = r26
                r1 = r30
                boolean r30 = r0.contains(r1)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                if (r30 == 0) goto L_0x002f
                java.lang.StringBuilder r30 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r30.<init>()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.String r31 = "Request string: "
                java.lang.StringBuilder r30 = r30.append(r31)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r0 = r30
                r1 = r26
                java.lang.StringBuilder r30 = r0.append(r1)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.String r30 = r30.toString()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                com.millennialmedia.android.MMAdViewSDK.Log.v(r30)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.String r30 = "******************"
                com.millennialmedia.android.MMAdViewSDK.Log.v(r30)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.StringBuilder r30 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r30.<init>()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.String r31 = "HEAD /"
                java.lang.StringBuilder r30 = r30.append(r31)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r0 = r34
                java.lang.String r0 = r0.filePath     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r31 = r0
                java.lang.StringBuilder r30 = r30.append(r31)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.String r31 = "/video.dat"
                java.lang.StringBuilder r30 = r30.append(r31)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.String r30 = r30.toString()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r0 = r26
                r1 = r30
                boolean r30 = r0.startsWith(r1)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                if (r30 == 0) goto L_0x0142
                java.io.File r14 = new java.io.File     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.StringBuilder r30 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r30.<init>()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r0 = r34
                java.lang.String r0 = r0.cacheDir     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r31 = r0
                java.lang.StringBuilder r30 = r30.append(r31)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r0 = r34
                java.lang.String r0 = r0.filePath     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r31 = r0
                java.lang.StringBuilder r30 = r30.append(r31)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.String r31 = "/video.dat"
                java.lang.StringBuilder r30 = r30.append(r31)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.String r30 = r30.toString()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r0 = r14
                r1 = r30
                r0.<init>(r1)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.String r30 = "HTTP/1.1 200 OK\r\n"
                byte[] r30 = r30.getBytes()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r0 = r23
                r1 = r30
                r0.write(r1)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.String r30 = "Content-Type: video/mp4\r\n"
                byte[] r30 = r30.getBytes()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r0 = r23
                r1 = r30
                r0.write(r1)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.String r30 = new java.lang.String     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.StringBuilder r31 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r31.<init>()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.String r32 = "Content-Length: "
                java.lang.StringBuilder r31 = r31.append(r32)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                long r32 = r14.length()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.StringBuilder r31 = r31.append(r32)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.String r32 = "\r\n"
                java.lang.StringBuilder r31 = r31.append(r32)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.String r31 = r31.toString()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r30.<init>(r31)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                byte[] r30 = r30.getBytes()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r0 = r23
                r1 = r30
                r0.write(r1)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.String r30 = "Cache-Control: no-cache\r\n"
                byte[] r30 = r30.getBytes()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r0 = r23
                r1 = r30
                r0.write(r1)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.String r30 = "Connection: close\r\n\r\n"
                byte[] r30 = r30.getBytes()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r0 = r23
                r1 = r30
                r0.write(r1)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
            L_0x0124:
                java.lang.String r30 = "Closing video server socket"
                com.millennialmedia.android.MMAdViewSDK.Log.v(r30)
                if (r11 == 0) goto L_0x012e
                r11.close()     // Catch:{ IOException -> 0x013a }
            L_0x012e:
                if (r15 == 0) goto L_0x0133
                r15.close()     // Catch:{ IOException -> 0x013a }
            L_0x0133:
                if (r23 == 0) goto L_0x000b
                r23.close()     // Catch:{ IOException -> 0x013a }
                goto L_0x000b
            L_0x013a:
                r30 = move-exception
                r12 = r30
                r12.printStackTrace()
                goto L_0x000b
            L_0x0142:
                java.lang.StringBuilder r30 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r30.<init>()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.String r31 = "GET /"
                java.lang.StringBuilder r30 = r30.append(r31)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r0 = r34
                java.lang.String r0 = r0.filePath     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r31 = r0
                java.lang.StringBuilder r30 = r30.append(r31)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.String r31 = "/video.dat"
                java.lang.StringBuilder r30 = r30.append(r31)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.String r30 = r30.toString()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r0 = r26
                r1 = r30
                boolean r30 = r0.startsWith(r1)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                if (r30 == 0) goto L_0x0526
                java.io.File r14 = new java.io.File     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.StringBuilder r30 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r30.<init>()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r0 = r34
                java.lang.String r0 = r0.cacheDir     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r31 = r0
                java.lang.StringBuilder r30 = r30.append(r31)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r0 = r34
                java.lang.String r0 = r0.filePath     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r31 = r0
                java.lang.StringBuilder r30 = r30.append(r31)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.String r31 = "/video.dat"
                java.lang.StringBuilder r30 = r30.append(r31)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.String r30 = r30.toString()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r0 = r14
                r1 = r30
                r0.<init>(r1)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                if (r14 != 0) goto L_0x01e3
                java.lang.String r30 = "Closing video server socket"
                com.millennialmedia.android.MMAdViewSDK.Log.v(r30)
                if (r11 == 0) goto L_0x01a2
                r11.close()     // Catch:{ IOException -> 0x01dc }
            L_0x01a2:
                if (r15 == 0) goto L_0x01a7
                r15.close()     // Catch:{ IOException -> 0x01dc }
            L_0x01a7:
                if (r23 == 0) goto L_0x01ac
                r23.close()     // Catch:{ IOException -> 0x01dc }
            L_0x01ac:
                r0 = r34
                boolean r0 = r0.done
                r30 = r0
                if (r30 == 0) goto L_0x01b9
                java.lang.String r30 = "Detected stop"
                com.millennialmedia.android.MMAdViewSDK.Log.v(r30)
            L_0x01b9:
                r0 = r34
                java.net.ServerSocket r0 = r0.serverSocket     // Catch:{ IOException -> 0x0562 }
                r30 = r0
                if (r30 == 0) goto L_0x01db
                r0 = r34
                java.net.ServerSocket r0 = r0.serverSocket     // Catch:{ IOException -> 0x0562 }
                r30 = r0
                boolean r30 = r30.isBound()     // Catch:{ IOException -> 0x0562 }
                if (r30 == 0) goto L_0x01db
                java.lang.String r30 = "Closing server socket connection"
                com.millennialmedia.android.MMAdViewSDK.Log.v(r30)     // Catch:{ IOException -> 0x0562 }
                r0 = r34
                java.net.ServerSocket r0 = r0.serverSocket     // Catch:{ IOException -> 0x0562 }
                r30 = r0
                r30.close()     // Catch:{ IOException -> 0x0562 }
            L_0x01db:
                return
            L_0x01dc:
                r30 = move-exception
                r12 = r30
                r12.printStackTrace()
                goto L_0x01ac
            L_0x01e3:
                java.io.OutputStream r23 = r11.getOutputStream()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.String r30 = "Range:"
                r0 = r26
                r1 = r30
                boolean r30 = r0.contains(r1)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                if (r30 == 0) goto L_0x0476
                java.lang.String r30 = "Range found in request string "
                com.millennialmedia.android.MMAdViewSDK.Log.v(r30)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.String r30 = "Range: bytes=([0-9]+)-\\s"
                java.util.regex.Pattern r25 = java.util.regex.Pattern.compile(r30)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.util.regex.Matcher r21 = r25.matcher(r26)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r8 = 1
                r5 = 1
            L_0x0206:
                boolean r30 = r21.find()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                if (r30 == 0) goto L_0x022b
                r30 = 1
                r0 = r21
                r1 = r30
                java.lang.String r10 = r0.group(r1)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.Integer r30 = new java.lang.Integer     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r0 = r30
                r1 = r10
                r0.<init>(r1)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                int r30 = r30.intValue()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r0 = r30
                long r0 = (long) r0     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r8 = r0
                long r5 = r14.length()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                goto L_0x0206
            L_0x022b:
                java.lang.String r30 = "Range: bytes=([0-9]+)-([0-9]+)"
                java.util.regex.Pattern r24 = java.util.regex.Pattern.compile(r30)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r0 = r24
                r1 = r26
                java.util.regex.Matcher r20 = r0.matcher(r1)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
            L_0x0239:
                boolean r30 = r20.find()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                if (r30 == 0) goto L_0x027f
                r30 = 1
                r0 = r20
                r1 = r30
                java.lang.String r10 = r0.group(r1)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r30 = 2
                r0 = r20
                r1 = r30
                java.lang.String r7 = r0.group(r1)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.Integer r30 = new java.lang.Integer     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r0 = r30
                r1 = r10
                r0.<init>(r1)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                int r30 = r30.intValue()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r0 = r30
                long r0 = (long) r0     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r8 = r0
                if (r7 == 0) goto L_0x0276
                java.lang.Integer r30 = new java.lang.Integer     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r0 = r30
                r1 = r7
                r0.<init>(r1)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                int r30 = r30.intValue()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r0 = r30
                long r0 = (long) r0     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r5 = r0
                goto L_0x0239
            L_0x0276:
                long r30 = r14.length()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r32 = 1
                long r5 = r30 - r32
                goto L_0x0239
            L_0x027f:
                java.lang.String r30 = "MillennialMediaAdSDK"
                java.lang.StringBuilder r31 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r31.<init>()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.String r32 = "Bytes: "
                java.lang.StringBuilder r31 = r31.append(r32)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r0 = r31
                r1 = r8
                java.lang.StringBuilder r31 = r0.append(r1)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.String r32 = "-"
                java.lang.StringBuilder r31 = r31.append(r32)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r0 = r31
                r1 = r5
                java.lang.StringBuilder r31 = r0.append(r1)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.String r31 = r31.toString()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                android.util.Log.i(r30, r31)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.io.FileInputStream r16 = new java.io.FileInputStream     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r0 = r16
                r1 = r14
                r0.<init>(r1)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r0 = r16
                r1 = r8
                long r27 = r0.skip(r1)     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                java.lang.StringBuilder r30 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                r30.<init>()     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                java.lang.String r31 = "Bytes skipped: "
                java.lang.StringBuilder r30 = r30.append(r31)     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                r0 = r30
                r1 = r27
                java.lang.StringBuilder r30 = r0.append(r1)     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                java.lang.String r30 = r30.toString()     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                com.millennialmedia.android.MMAdViewSDK.Log.v(r30)     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                long r30 = r5 - r27
                r32 = 1
                long r18 = r30 + r32
                java.lang.String r30 = "HTTP/1.1 206 Partial Content\r\n"
                byte[] r30 = r30.getBytes()     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                r0 = r23
                r1 = r30
                r0.write(r1)     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                java.lang.String r30 = "Date: Thu, 17 Feb 2011 01:27:03 GMT\r\n"
                byte[] r30 = r30.getBytes()     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                r0 = r23
                r1 = r30
                r0.write(r1)     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                java.lang.String r30 = "Etag: \"320581-329f19-235b0a40\"\r\n"
                byte[] r30 = r30.getBytes()     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                r0 = r23
                r1 = r30
                r0.write(r1)     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                r30 = 1
                int r30 = (r5 > r30 ? 1 : (r5 == r30 ? 0 : -1))
                if (r30 <= 0) goto L_0x03d8
                java.lang.String r30 = new java.lang.String     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                java.lang.StringBuilder r31 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                r31.<init>()     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                java.lang.String r32 = "Content-Range: "
                java.lang.StringBuilder r31 = r31.append(r32)     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                r0 = r31
                r1 = r8
                java.lang.StringBuilder r31 = r0.append(r1)     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                java.lang.String r32 = "-"
                java.lang.StringBuilder r31 = r31.append(r32)     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                r0 = r31
                r1 = r5
                java.lang.StringBuilder r31 = r0.append(r1)     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                java.lang.String r32 = "\r\n"
                java.lang.StringBuilder r31 = r31.append(r32)     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                java.lang.String r31 = r31.toString()     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                r30.<init>(r31)     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                byte[] r30 = r30.getBytes()     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                r0 = r23
                r1 = r30
                r0.write(r1)     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
            L_0x033c:
                java.lang.String r30 = "Content-Type: video/mp4\r\n"
                byte[] r30 = r30.getBytes()     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                r0 = r23
                r1 = r30
                r0.write(r1)     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                java.lang.String r30 = new java.lang.String     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                java.lang.StringBuilder r31 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                r31.<init>()     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                java.lang.String r32 = "Content-Length: "
                java.lang.StringBuilder r31 = r31.append(r32)     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                r0 = r31
                r1 = r18
                java.lang.StringBuilder r31 = r0.append(r1)     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                java.lang.String r32 = "\r\n"
                java.lang.StringBuilder r31 = r31.append(r32)     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                java.lang.String r31 = r31.toString()     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                r30.<init>(r31)     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                byte[] r30 = r30.getBytes()     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                r0 = r23
                r1 = r30
                r0.write(r1)     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                java.lang.String r30 = "Connection: close\r\n\r\n"
                byte[] r30 = r30.getBytes()     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                r0 = r23
                r1 = r30
                r0.write(r1)     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
            L_0x0383:
                r0 = r16
                r1 = r4
                int r22 = r0.read(r1)     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                if (r22 > 0) goto L_0x041a
                java.lang.String r30 = "Video Did Finish"
                com.millennialmedia.android.MMAdViewSDK.Log.v(r30)     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
            L_0x0391:
                r15 = r16
            L_0x0393:
                java.lang.String r30 = "\r\n"
                byte[] r30 = r30.getBytes()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r0 = r23
                r1 = r30
                r0.write(r1)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.String r30 = "200 OK"
                com.millennialmedia.android.MMAdViewSDK.Log.v(r30)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r30 = 1
                r0 = r30
                r1 = r34
                r1.done = r0     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                goto L_0x0124
            L_0x03af:
                r30 = move-exception
                r12 = r30
            L_0x03b2:
                r12.printStackTrace()     // Catch:{ all -> 0x0545 }
                if (r11 == 0) goto L_0x03ba
                r11.close()     // Catch:{ IOException -> 0x053f }
            L_0x03ba:
                java.lang.String r30 = "Closing video server socket"
                com.millennialmedia.android.MMAdViewSDK.Log.v(r30)
                if (r11 == 0) goto L_0x03c4
                r11.close()     // Catch:{ IOException -> 0x03d0 }
            L_0x03c4:
                if (r15 == 0) goto L_0x03c9
                r15.close()     // Catch:{ IOException -> 0x03d0 }
            L_0x03c9:
                if (r23 == 0) goto L_0x000b
                r23.close()     // Catch:{ IOException -> 0x03d0 }
                goto L_0x000b
            L_0x03d0:
                r30 = move-exception
                r12 = r30
                r12.printStackTrace()
                goto L_0x000b
            L_0x03d8:
                java.lang.String r30 = new java.lang.String     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                java.lang.StringBuilder r31 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                r31.<init>()     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                java.lang.String r32 = "Content-Range: "
                java.lang.StringBuilder r31 = r31.append(r32)     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                r0 = r31
                r1 = r8
                java.lang.StringBuilder r31 = r0.append(r1)     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                java.lang.String r32 = "-"
                java.lang.StringBuilder r31 = r31.append(r32)     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                long r32 = r14.length()     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                java.lang.StringBuilder r31 = r31.append(r32)     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                java.lang.String r32 = "\r\n"
                java.lang.StringBuilder r31 = r31.append(r32)     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                java.lang.String r31 = r31.toString()     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                r30.<init>(r31)     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                byte[] r30 = r30.getBytes()     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                r0 = r23
                r1 = r30
                r0.write(r1)     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                goto L_0x033c
            L_0x0414:
                r30 = move-exception
                r12 = r30
                r15 = r16
                goto L_0x03b2
            L_0x041a:
                r0 = r22
                long r0 = (long) r0     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                r30 = r0
                int r30 = (r18 > r30 ? 1 : (r18 == r30 ? 0 : -1))
                if (r30 < 0) goto L_0x043e
                r30 = 0
                r0 = r23
                r1 = r4
                r2 = r30
                r3 = r22
                r0.write(r1, r2, r3)     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
            L_0x042f:
                r0 = r22
                long r0 = (long) r0     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                r30 = r0
                long r18 = r18 - r30
                r30 = 0
                int r30 = (r18 > r30 ? 1 : (r18 == r30 ? 0 : -1))
                if (r30 > 0) goto L_0x0383
                goto L_0x0391
            L_0x043e:
                r30 = 0
                r0 = r18
                int r0 = (int) r0     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                r31 = r0
                r0 = r23
                r1 = r4
                r2 = r30
                r3 = r31
                r0.write(r1, r2, r3)     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                goto L_0x042f
            L_0x0450:
                r30 = move-exception
                r12 = r30
                r15 = r16
            L_0x0455:
                r12.printStackTrace()     // Catch:{ all -> 0x0545 }
                java.lang.String r30 = "Closing video server socket"
                com.millennialmedia.android.MMAdViewSDK.Log.v(r30)
                if (r11 == 0) goto L_0x0462
                r11.close()     // Catch:{ IOException -> 0x046e }
            L_0x0462:
                if (r15 == 0) goto L_0x0467
                r15.close()     // Catch:{ IOException -> 0x046e }
            L_0x0467:
                if (r23 == 0) goto L_0x01ac
                r23.close()     // Catch:{ IOException -> 0x046e }
                goto L_0x01ac
            L_0x046e:
                r30 = move-exception
                r12 = r30
                r12.printStackTrace()
                goto L_0x01ac
            L_0x0476:
                java.lang.String r30 = "HTTP/1.1 200 OK\r\n"
                byte[] r30 = r30.getBytes()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r0 = r23
                r1 = r30
                r0.write(r1)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.String r30 = "Content-Type: video/mp4\r\n"
                byte[] r30 = r30.getBytes()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r0 = r23
                r1 = r30
                r0.write(r1)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.String r30 = new java.lang.String     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.StringBuilder r31 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r31.<init>()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.String r32 = "Content-Length: "
                java.lang.StringBuilder r31 = r31.append(r32)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                long r32 = r14.length()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.StringBuilder r31 = r31.append(r32)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.String r32 = "\r\n"
                java.lang.StringBuilder r31 = r31.append(r32)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.String r31 = r31.toString()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r30.<init>(r31)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                byte[] r30 = r30.getBytes()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r0 = r23
                r1 = r30
                r0.write(r1)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.String r30 = "Cache-Control: no-cache\r\n"
                byte[] r30 = r30.getBytes()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r0 = r23
                r1 = r30
                r0.write(r1)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.String r30 = "Connection: close\r\n\r\n"
                byte[] r30 = r30.getBytes()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r0 = r23
                r1 = r30
                r0.write(r1)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.io.FileInputStream r16 = new java.io.FileInputStream     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r0 = r16
                r1 = r14
                r0.<init>(r1)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
            L_0x04df:
                r0 = r16
                r1 = r4
                int r22 = r0.read(r1)     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                if (r22 > 0) goto L_0x04f3
                java.lang.String r30 = "MillennialMediaAdSDK"
                java.lang.String r31 = "Video Did Finish"
                android.util.Log.i(r30, r31)     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                r15 = r16
                goto L_0x0393
            L_0x04f3:
                r30 = 0
                r0 = r23
                r1 = r4
                r2 = r30
                r3 = r22
                r0.write(r1, r2, r3)     // Catch:{ SocketTimeoutException -> 0x0414, FileNotFoundException -> 0x0450, IOException -> 0x0500, all -> 0x056a }
                goto L_0x04df
            L_0x0500:
                r30 = move-exception
                r12 = r30
                r15 = r16
            L_0x0505:
                r12.printStackTrace()     // Catch:{ all -> 0x0545 }
                java.lang.String r30 = "Closing video server socket"
                com.millennialmedia.android.MMAdViewSDK.Log.v(r30)
                if (r11 == 0) goto L_0x0512
                r11.close()     // Catch:{ IOException -> 0x051e }
            L_0x0512:
                if (r15 == 0) goto L_0x0517
                r15.close()     // Catch:{ IOException -> 0x051e }
            L_0x0517:
                if (r23 == 0) goto L_0x000b
                r23.close()     // Catch:{ IOException -> 0x051e }
                goto L_0x000b
            L_0x051e:
                r30 = move-exception
                r12 = r30
                r12.printStackTrace()
                goto L_0x000b
            L_0x0526:
                java.lang.String r30 = "HTTP/1.1 400 Bad Request\r\n\r\n"
                byte[] r30 = r30.getBytes()     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                r0 = r23
                r1 = r30
                r0.write(r1)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                java.lang.String r30 = "400 Bad Request"
                com.millennialmedia.android.MMAdViewSDK.Log.v(r30)     // Catch:{ SocketTimeoutException -> 0x03af, FileNotFoundException -> 0x053a, IOException -> 0x056e }
                goto L_0x0124
            L_0x053a:
                r30 = move-exception
                r12 = r30
                goto L_0x0455
            L_0x053f:
                r13 = move-exception
                r13.printStackTrace()     // Catch:{ all -> 0x0545 }
                goto L_0x03ba
            L_0x0545:
                r30 = move-exception
            L_0x0546:
                java.lang.String r31 = "Closing video server socket"
                com.millennialmedia.android.MMAdViewSDK.Log.v(r31)
                if (r11 == 0) goto L_0x0550
                r11.close()     // Catch:{ IOException -> 0x055b }
            L_0x0550:
                if (r15 == 0) goto L_0x0555
                r15.close()     // Catch:{ IOException -> 0x055b }
            L_0x0555:
                if (r23 == 0) goto L_0x055a
                r23.close()     // Catch:{ IOException -> 0x055b }
            L_0x055a:
                throw r30
            L_0x055b:
                r31 = move-exception
                r12 = r31
                r12.printStackTrace()
                goto L_0x055a
            L_0x0562:
                r30 = move-exception
                r12 = r30
                r12.printStackTrace()
                goto L_0x01db
            L_0x056a:
                r30 = move-exception
                r15 = r16
                goto L_0x0546
            L_0x056e:
                r30 = move-exception
                r12 = r30
                goto L_0x0505
            */
            throw new UnsupportedOperationException("Method not decompiled: com.millennialmedia.android.VideoPlayer.VideoServer.run():void");
        }

        public synchronized void requestStop() {
            this.done = true;
            MMAdViewSDK.Log.v("Requested video server stop. Done: " + this.done);
        }
    }
}
