package com.epagames.HelloKittyMemory;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.adwhirl.AdWhirlLayout;
import com.adwhirl.AdWhirlTargeting;
import com.inmobi.androidsdk.impl.Constants;

public class highscoreScreen extends Activity implements AdWhirlLayout.AdWhirlInterface {
    public Button button1;
    public EditText editText1;
    public Highscore highscore;
    public int level;
    public View.OnClickListener mClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            if (v.getId() == R.id.button1) {
                highscoreScreen.this.text = highscoreScreen.this.editText1.getText().toString();
                highscoreScreen.this.highscore.addScore(highscoreScreen.this.text, (long) highscoreScreen.this.level);
                highscoreScreen.this.startActivity(new Intent(highscoreScreen.this, startScreen.class));
                highscoreScreen.this.finish();
            }
        }
    };
    public String text;
    public TextView textView4;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setVolumeControlStream(3);
        setContentView((int) R.layout.highscore);
        this.level = getIntent().getExtras().getInt("level");
        this.button1 = (Button) findViewById(R.id.button1);
        this.button1.setOnClickListener(this.mClickListener);
        this.editText1 = (EditText) findViewById(R.id.editText1);
        this.textView4 = (TextView) findViewById(R.id.textView4);
        this.textView4.setText("You reached level " + this.level);
        this.highscore = new Highscore(this);
        LinearLayout layout = (LinearLayout) findViewById(R.id.layout_game);
        if (layout == null) {
            Log.e("AdWhirl", "Layout is null!");
            return;
        }
        float density = getResources().getDisplayMetrics().density;
        int width = (int) (((float) Constants.INMOBI_ADVIEW_WIDTH) * density);
        AdWhirlTargeting.setTestMode(false);
        layout.setGravity(1);
        layout.invalidate();
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) findViewById(R.id.adwhirl_layout);
        adWhirlLayout.setAdWhirlInterface(this);
        adWhirlLayout.setMaxWidth(width);
        adWhirlLayout.setMaxHeight((int) (((float) 52) * density));
    }

    public void adWhirlGeneric() {
    }
}
