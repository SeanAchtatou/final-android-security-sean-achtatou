package com.epagames.HelloKittyMemory;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import com.adwhirl.AdWhirlLayout;
import com.adwhirl.AdWhirlTargeting;
import com.inmobi.androidsdk.impl.Constants;

public class startScreen extends Activity implements AdWhirlLayout.AdWhirlInterface {
    public Button Start_new_game;
    public Button high;
    public View.OnClickListener mClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            if (v.getId() == R.id.Start_new_game) {
                startScreen.this.startActivity(new Intent(startScreen.this, modeScreen.class));
            } else if (v.getId() == R.id.high) {
                startScreen.this.startActivity(new Intent(startScreen.this, highScreen.class));
            } else if (v.getId() == R.id.more) {
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.setData(Uri.parse("market://search?q=pub:epagames"));
                startScreen.this.startActivity(intent);
            }
        }
    };
    public Button more;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setVolumeControlStream(3);
        setContentView((int) R.layout.main);
        this.Start_new_game = (Button) findViewById(R.id.Start_new_game);
        this.Start_new_game.setOnClickListener(this.mClickListener);
        this.high = (Button) findViewById(R.id.high);
        this.high.setOnClickListener(this.mClickListener);
        this.more = (Button) findViewById(R.id.more);
        this.more.setOnClickListener(this.mClickListener);
        LinearLayout layout = (LinearLayout) findViewById(R.id.layout_start);
        if (layout == null) {
            Log.e("AdWhirl", "Layout is null!");
            return;
        }
        float density = getResources().getDisplayMetrics().density;
        int width = (int) (((float) Constants.INMOBI_ADVIEW_WIDTH) * density);
        AdWhirlTargeting.setTestMode(false);
        layout.setGravity(1);
        layout.invalidate();
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) findViewById(R.id.adwhirl_layout);
        adWhirlLayout.setAdWhirlInterface(this);
        adWhirlLayout.setMaxWidth(width);
        adWhirlLayout.setMaxHeight((int) (((float) 52) * density));
    }

    public void adWhirlGeneric() {
    }
}
