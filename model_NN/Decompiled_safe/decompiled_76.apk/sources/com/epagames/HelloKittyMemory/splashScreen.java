package com.epagames.HelloKittyMemory;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class splashScreen extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.splashscreen);
        new Thread() {
            int wait = 0;

            public void run() {
                try {
                    super.run();
                    while (this.wait < 2000) {
                        sleep(100);
                        this.wait += 100;
                    }
                } catch (Exception e) {
                    System.out.println("EXc=" + e);
                } finally {
                    splashScreen.this.startActivity(new Intent(splashScreen.this, startScreen.class));
                    splashScreen.this.finish();
                }
            }
        }.start();
    }
}
