package com.epagames.HelloKittyMemory;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import com.adwhirl.AdWhirlLayout;
import com.adwhirl.AdWhirlTargeting;
import com.inmobi.androidsdk.impl.Constants;

public class modeScreen extends Activity implements AdWhirlLayout.AdWhirlInterface {
    public Button Free_play;
    public Button Time_attack;
    public View.OnClickListener mClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            if (v.getId() == R.id.Time_attack) {
                Intent i = new Intent(modeScreen.this, gameScreen.class);
                i.putExtra("mode", 1);
                modeScreen.this.startActivity(i);
                modeScreen.this.finish();
            } else if (v.getId() == R.id.Free_play) {
                Intent i2 = new Intent(modeScreen.this, gameScreen.class);
                i2.putExtra("mode", 2);
                modeScreen.this.startActivity(i2);
                modeScreen.this.finish();
            }
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setVolumeControlStream(3);
        setContentView((int) R.layout.mode);
        this.Time_attack = (Button) findViewById(R.id.Time_attack);
        this.Time_attack.setOnClickListener(this.mClickListener);
        this.Free_play = (Button) findViewById(R.id.Free_play);
        this.Free_play.setOnClickListener(this.mClickListener);
        LinearLayout layout = (LinearLayout) findViewById(R.id.layout_mode);
        if (layout == null) {
            Log.e("AdWhirl", "Layout is null!");
            return;
        }
        float density = getResources().getDisplayMetrics().density;
        int width = (int) (((float) Constants.INMOBI_ADVIEW_WIDTH) * density);
        AdWhirlTargeting.setTestMode(false);
        layout.setGravity(1);
        layout.invalidate();
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) findViewById(R.id.adwhirl_layout);
        adWhirlLayout.setAdWhirlInterface(this);
        adWhirlLayout.setMaxWidth(width);
        adWhirlLayout.setMaxHeight((int) (((float) 52) * density));
    }

    public void adWhirlGeneric() {
    }
}
