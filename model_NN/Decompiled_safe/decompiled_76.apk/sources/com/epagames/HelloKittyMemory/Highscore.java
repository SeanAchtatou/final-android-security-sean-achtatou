package com.epagames.HelloKittyMemory;

import android.content.Context;
import android.content.SharedPreferences;

public class Highscore {
    private String[] names = new String[10];
    private SharedPreferences preferences;
    private long[] score = new long[10];

    public Highscore(Context context) {
        this.preferences = context.getSharedPreferences("HIGHSCORES", 0);
        for (int x = 0; x < 10; x++) {
            this.names[x] = this.preferences.getString("name" + x, "-");
            this.score[x] = this.preferences.getLong("score" + x, 0);
        }
    }

    public String getName(int x) {
        return this.names[x];
    }

    public long getScore(int x) {
        return this.score[x];
    }

    public boolean inHighscore(long score2) {
        int position = 0;
        while (position < 10 && this.score[position] > score2) {
            position++;
        }
        if (position == 10) {
            return false;
        }
        return true;
    }

    public boolean addScore(String name, long score2) {
        int position = 0;
        while (position < 10 && this.score[position] > score2) {
            position++;
        }
        if (position == 10) {
            return false;
        }
        for (int x = 9; x > position; x--) {
            this.names[x] = this.names[x - 1];
            this.score[x] = this.score[x - 1];
        }
        this.names[position] = new String(name);
        this.score[position] = score2;
        SharedPreferences.Editor editor = this.preferences.edit();
        for (int x2 = 0; x2 < 10; x2++) {
            editor.putString("name" + x2, this.names[x2]);
            editor.putLong("score" + x2, this.score[x2]);
        }
        editor.commit();
        return true;
    }
}
