package com.epagames.HelloKittyMemory;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.adwhirl.AdWhirlLayout;
import com.adwhirl.AdWhirlTargeting;
import com.inmobi.androidsdk.impl.Constants;
import java.util.Random;

public class gameScreen extends Activity implements AdWhirlLayout.AdWhirlInterface {
    public AlertDialog alert;
    public AlertDialog alert2;
    public boolean[] backs = new boolean[8];
    public AlertDialog.Builder builder;
    public AlertDialog.Builder builder2;
    public boolean[] correct = new boolean[20];
    public MyCount counter;
    public int current;
    public String file;
    public int good;
    public boolean[] guessed = new boolean[20];
    public Highscore highscore;
    public Drawable image;
    public int imageResource;
    public ImageView[] imageView = new ImageView[20];
    public int level;
    public TextView levels;
    public View.OnClickListener mClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            if (v.getId() == R.id.imageView1 && !gameScreen.this.guessed[0] && !gameScreen.this.correct[0]) {
                gameScreen.this.check(0);
            } else if (v.getId() == R.id.imageView2 && !gameScreen.this.guessed[1] && !gameScreen.this.correct[1]) {
                gameScreen.this.check(1);
            } else if (v.getId() == R.id.imageView3 && !gameScreen.this.guessed[2] && !gameScreen.this.correct[2]) {
                gameScreen.this.check(2);
            } else if (v.getId() == R.id.imageView4 && !gameScreen.this.guessed[3] && !gameScreen.this.correct[3]) {
                gameScreen.this.check(3);
            } else if (v.getId() == R.id.imageView5 && !gameScreen.this.guessed[4] && !gameScreen.this.correct[4]) {
                gameScreen.this.check(4);
            } else if (v.getId() == R.id.imageView6 && !gameScreen.this.guessed[5] && !gameScreen.this.correct[5]) {
                gameScreen.this.check(5);
            } else if (v.getId() == R.id.imageView7 && !gameScreen.this.guessed[6] && !gameScreen.this.correct[6]) {
                gameScreen.this.check(6);
            } else if (v.getId() == R.id.imageView8 && !gameScreen.this.guessed[7] && !gameScreen.this.correct[7]) {
                gameScreen.this.check(7);
            } else if (v.getId() == R.id.imageView9 && !gameScreen.this.guessed[8] && !gameScreen.this.correct[8]) {
                gameScreen.this.check(8);
            } else if (v.getId() == R.id.imageView10 && !gameScreen.this.guessed[9] && !gameScreen.this.correct[9]) {
                gameScreen.this.check(9);
            } else if (v.getId() == R.id.imageView11 && !gameScreen.this.guessed[10] && !gameScreen.this.correct[10]) {
                gameScreen.this.check(10);
            } else if (v.getId() == R.id.imageView12 && !gameScreen.this.guessed[11] && !gameScreen.this.correct[11]) {
                gameScreen.this.check(11);
            } else if (v.getId() == R.id.imageView13 && !gameScreen.this.guessed[12] && !gameScreen.this.correct[12]) {
                gameScreen.this.check(12);
            } else if (v.getId() == R.id.imageView14 && !gameScreen.this.guessed[13] && !gameScreen.this.correct[13]) {
                gameScreen.this.check(13);
            } else if (v.getId() == R.id.imageView15 && !gameScreen.this.guessed[14] && !gameScreen.this.correct[14]) {
                gameScreen.this.check(14);
            } else if (v.getId() == R.id.imageView16 && !gameScreen.this.guessed[15] && !gameScreen.this.correct[15]) {
                gameScreen.this.check(15);
            } else if (v.getId() == R.id.imageView17 && !gameScreen.this.guessed[16] && !gameScreen.this.correct[16]) {
                gameScreen.this.check(16);
            } else if (v.getId() == R.id.imageView18 && !gameScreen.this.guessed[17] && !gameScreen.this.correct[17]) {
                gameScreen.this.check(17);
            } else if (v.getId() == R.id.imageView19 && !gameScreen.this.guessed[18] && !gameScreen.this.correct[18]) {
                gameScreen.this.check(18);
            } else if (v.getId() == R.id.imageView20 && !gameScreen.this.guessed[19] && !gameScreen.this.correct[19]) {
                gameScreen.this.check(19);
            }
        }
    };
    public MediaPlayer mediaPlayer;
    public MediaPlayer mediaPlayer2;
    public int mode;
    public boolean pause = false;
    public int[] pos = new int[20];
    public int previous1;
    public int previous2;
    public Random r = new Random();
    public int random_answer;
    public int random_position;
    public long remainingTime = 100;
    public boolean right;
    public TextView time;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setVolumeControlStream(3);
        setContentView((int) R.layout.game);
        this.builder = new AlertDialog.Builder(this);
        this.builder2 = new AlertDialog.Builder(this);
        this.alert = this.builder.create();
        this.alert2 = this.builder.create();
        this.mediaPlayer = MediaPlayer.create(this, (int) R.raw.correct_sound);
        this.mediaPlayer2 = MediaPlayer.create(this, (int) R.raw.cheer);
        this.highscore = new Highscore(this);
        this.mode = getIntent().getExtras().getInt("mode");
        this.imageView[0] = (ImageView) findViewById(R.id.imageView1);
        this.imageView[1] = (ImageView) findViewById(R.id.imageView2);
        this.imageView[2] = (ImageView) findViewById(R.id.imageView3);
        this.imageView[3] = (ImageView) findViewById(R.id.imageView4);
        this.imageView[4] = (ImageView) findViewById(R.id.imageView5);
        this.imageView[5] = (ImageView) findViewById(R.id.imageView6);
        this.imageView[6] = (ImageView) findViewById(R.id.imageView7);
        this.imageView[7] = (ImageView) findViewById(R.id.imageView8);
        this.imageView[8] = (ImageView) findViewById(R.id.imageView9);
        this.imageView[9] = (ImageView) findViewById(R.id.imageView10);
        this.imageView[10] = (ImageView) findViewById(R.id.imageView11);
        this.imageView[11] = (ImageView) findViewById(R.id.imageView12);
        this.imageView[12] = (ImageView) findViewById(R.id.imageView13);
        this.imageView[13] = (ImageView) findViewById(R.id.imageView14);
        this.imageView[14] = (ImageView) findViewById(R.id.imageView15);
        this.imageView[15] = (ImageView) findViewById(R.id.imageView16);
        this.imageView[16] = (ImageView) findViewById(R.id.imageView17);
        this.imageView[17] = (ImageView) findViewById(R.id.imageView18);
        this.imageView[18] = (ImageView) findViewById(R.id.imageView19);
        this.imageView[19] = (ImageView) findViewById(R.id.imageView20);
        this.imageView[0].setOnClickListener(this.mClickListener);
        this.imageView[1].setOnClickListener(this.mClickListener);
        this.imageView[2].setOnClickListener(this.mClickListener);
        this.imageView[3].setOnClickListener(this.mClickListener);
        this.imageView[4].setOnClickListener(this.mClickListener);
        this.imageView[5].setOnClickListener(this.mClickListener);
        this.imageView[6].setOnClickListener(this.mClickListener);
        this.imageView[7].setOnClickListener(this.mClickListener);
        this.imageView[8].setOnClickListener(this.mClickListener);
        this.imageView[9].setOnClickListener(this.mClickListener);
        this.imageView[10].setOnClickListener(this.mClickListener);
        this.imageView[11].setOnClickListener(this.mClickListener);
        this.imageView[12].setOnClickListener(this.mClickListener);
        this.imageView[13].setOnClickListener(this.mClickListener);
        this.imageView[14].setOnClickListener(this.mClickListener);
        this.imageView[15].setOnClickListener(this.mClickListener);
        this.imageView[16].setOnClickListener(this.mClickListener);
        this.imageView[17].setOnClickListener(this.mClickListener);
        this.imageView[18].setOnClickListener(this.mClickListener);
        this.imageView[19].setOnClickListener(this.mClickListener);
        this.levels = (TextView) findViewById(R.id.levels);
        this.time = (TextView) findViewById(R.id.time);
        this.level = 0;
        for (int i = 0; i < 8; i++) {
            this.backs[i] = false;
        }
        initCards();
        LinearLayout layout = (LinearLayout) findViewById(R.id.layout_game);
        if (layout == null) {
            Log.e("AdWhirl", "Layout is null!");
            return;
        }
        float density = getResources().getDisplayMetrics().density;
        int width = (int) (((float) Constants.INMOBI_ADVIEW_WIDTH) * density);
        AdWhirlTargeting.setTestMode(false);
        layout.setGravity(1);
        layout.invalidate();
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) findViewById(R.id.adwhirl_layout);
        adWhirlLayout.setAdWhirlInterface(this);
        adWhirlLayout.setMaxWidth(width);
        adWhirlLayout.setMaxHeight((int) (((float) 52) * density));
    }

    public void onPause() {
        if (this.mode == 1) {
            this.counter.cancel();
            this.pause = true;
        }
        super.onPause();
    }

    public void onResume() {
        if (this.mode == 1 && this.pause) {
            this.counter = new MyCount(this.remainingTime, 1000);
            this.counter.start();
            this.pause = false;
        }
        super.onResume();
    }

    public void onBackPressed() {
        this.builder.setMessage("Are you sure you want to exit?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (gameScreen.this.mode == 1) {
                    gameScreen.this.counter.cancel();
                    if (((long) gameScreen.this.level) > gameScreen.this.highscore.getScore(9)) {
                        Intent i = new Intent(gameScreen.this, highscoreScreen.class);
                        i.putExtra("level", gameScreen.this.level);
                        gameScreen.this.startActivity(i);
                        gameScreen.this.finish();
                        return;
                    }
                    gameScreen.this.finish();
                    return;
                }
                gameScreen.this.finish();
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        this.builder.create().show();
    }

    public void initCards() {
        if (this.mediaPlayer2.isPlaying()) {
            this.mediaPlayer2.stop();
            this.mediaPlayer2.release();
            this.mediaPlayer2 = MediaPlayer.create(this, (int) R.raw.cheer);
        }
        int d = 0;
        for (int i = 0; i < 8; i++) {
            if (this.backs[i]) {
                d++;
            }
        }
        if (d == 8) {
            for (int i2 = 0; i2 < 8; i2++) {
                this.backs[i2] = false;
            }
        }
        int w = this.r.nextInt(8);
        while (this.backs[w]) {
            w = this.r.nextInt(8);
        }
        this.backs[w] = true;
        this.file = "drawable/w";
        this.file = this.file.concat(Integer.toString(w));
        this.imageResource = getResources().getIdentifier(this.file, null, getPackageName());
        findViewById(R.id.relativeLayout1).setBackgroundResource(this.imageResource);
        this.file = "drawable/mdefault";
        this.imageResource = getResources().getIdentifier(this.file, null, getPackageName());
        this.image = getResources().getDrawable(this.imageResource);
        for (int i3 = 0; i3 < 20; i3++) {
            this.imageView[i3].setImageDrawable(this.image);
        }
        for (int i4 = 0; i4 < 20; i4++) {
            this.guessed[i4] = false;
            this.correct[i4] = false;
            this.pos[i4] = -1;
        }
        int p = this.r.nextInt(20);
        int c = this.r.nextInt(32);
        boolean[] c1 = new boolean[32];
        for (int i5 = 0; i5 < 32; i5++) {
            c1[i5] = false;
        }
        for (int i6 = 0; i6 < 10; i6++) {
            while (c1[c]) {
                c = this.r.nextInt(32);
            }
            c1[c] = true;
            for (int j = 0; j < 2; j++) {
                while (this.pos[p] != -1) {
                    p = this.r.nextInt(20);
                }
                this.pos[p] = c;
            }
        }
        this.right = true;
        this.current = -1;
        this.good = 0;
        this.previous1 = -1;
        this.previous2 = -1;
        this.levels.setText("Level: " + this.level);
        if (this.mode == 1) {
            this.counter = new MyCount(100000 - penalty(), 1000);
            this.counter.start();
        }
    }

    public class MyCount extends CountDownTimer {
        public MyCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        public void onFinish() {
            gameScreen.this.builder.setMessage("Time is up!").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    if (((long) gameScreen.this.level) > gameScreen.this.highscore.getScore(9)) {
                        Intent i = new Intent(gameScreen.this, highscoreScreen.class);
                        i.putExtra("level", gameScreen.this.level);
                        gameScreen.this.startActivity(i);
                        gameScreen.this.finish();
                        return;
                    }
                    gameScreen.this.finish();
                }
            });
            gameScreen.this.builder.create().show();
        }

        public void onTick(long millisUntilFinished) {
            gameScreen.this.time.setText("Time left: " + (millisUntilFinished / 1000));
            gameScreen.this.remainingTime = millisUntilFinished;
        }
    }

    public void check(int j) {
        if (!this.right && this.current == -1) {
            this.file = "drawable/mdefault";
            this.imageResource = getResources().getIdentifier(this.file, null, getPackageName());
            this.image = getResources().getDrawable(this.imageResource);
            this.imageView[this.previous1].setImageDrawable(this.image);
            this.imageView[this.previous2].setImageDrawable(this.image);
        }
        if (this.current == -1) {
            this.current = j;
            this.guessed[this.current] = true;
            this.file = "drawable/m";
            this.file = this.file.concat(Integer.toString(this.pos[this.current]));
            this.imageResource = getResources().getIdentifier(this.file, null, getPackageName());
            this.image = getResources().getDrawable(this.imageResource);
            this.imageView[this.current].setImageDrawable(this.image);
        } else {
            this.guessed[j] = true;
            this.file = "drawable/m";
            this.file = this.file.concat(Integer.toString(this.pos[j]));
            this.imageResource = getResources().getIdentifier(this.file, null, getPackageName());
            this.image = getResources().getDrawable(this.imageResource);
            this.imageView[j].setImageDrawable(this.image);
            if (this.pos[this.current] == this.pos[j]) {
                this.correct[this.current] = true;
                this.correct[j] = true;
                this.good += 2;
                this.right = true;
                this.mediaPlayer.start();
                if (this.mode == 1) {
                    this.counter.cancel();
                    this.counter = new MyCount(this.remainingTime + 3000, 1000);
                    this.counter.start();
                }
            } else {
                this.right = false;
                this.previous1 = this.current;
                this.previous2 = j;
            }
            this.guessed[this.current] = false;
            this.guessed[j] = false;
            this.current = -1;
        }
        if (this.good == 20) {
            if (!this.mediaPlayer2.isPlaying()) {
                this.mediaPlayer2.start();
            }
            if (this.mode == 1) {
                this.counter.cancel();
            }
            this.level++;
            this.builder2.setMessage("Level completed!").setPositiveButton("Next level", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    gameScreen.this.initCards();
                }
            });
            this.builder2.create().show();
        }
    }

    public long penalty() {
        long temp = 0;
        if (this.level < 5) {
            temp = (long) (this.level * 10000);
        }
        if (this.level > 4 && this.level < 9) {
            temp = temp + 40000 + ((long) ((this.level - 4) * 5000));
        }
        if (this.level > 8 && this.level < 14) {
            temp = temp + 40000 + 20000 + ((long) ((this.level - 8) * 2000));
        }
        if (this.level <= 13 || this.level >= 19) {
            return temp;
        }
        return temp + 40000 + 20000 + 10000 + ((long) ((this.level - 13) * 1000));
    }

    public void adWhirlGeneric() {
    }
}
