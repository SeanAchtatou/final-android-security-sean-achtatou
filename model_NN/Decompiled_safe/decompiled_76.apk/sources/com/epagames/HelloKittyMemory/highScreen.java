package com.epagames.HelloKittyMemory;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.adwhirl.AdWhirlLayout;
import com.adwhirl.AdWhirlTargeting;
import com.inmobi.androidsdk.impl.Constants;

public class highScreen extends Activity implements AdWhirlLayout.AdWhirlInterface {
    public Button back;
    public Highscore highscore;
    public View.OnClickListener mClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            if (v.getId() == R.id.back) {
                highScreen.this.finish();
            }
        }
    };
    public TextView[] textView = new TextView[20];

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setVolumeControlStream(3);
        setContentView((int) R.layout.highscores);
        this.back = (Button) findViewById(R.id.back);
        this.back.setOnClickListener(this.mClickListener);
        this.highscore = new Highscore(this);
        this.textView[0] = (TextView) findViewById(R.id.textView3);
        this.textView[1] = (TextView) findViewById(R.id.textView4);
        this.textView[2] = (TextView) findViewById(R.id.textView5);
        this.textView[3] = (TextView) findViewById(R.id.textView6);
        this.textView[4] = (TextView) findViewById(R.id.textView7);
        this.textView[5] = (TextView) findViewById(R.id.textView8);
        this.textView[6] = (TextView) findViewById(R.id.textView9);
        this.textView[7] = (TextView) findViewById(R.id.textView10);
        this.textView[8] = (TextView) findViewById(R.id.textView11);
        this.textView[9] = (TextView) findViewById(R.id.textView12);
        this.textView[10] = (TextView) findViewById(R.id.textView13);
        this.textView[11] = (TextView) findViewById(R.id.textView14);
        this.textView[12] = (TextView) findViewById(R.id.textView15);
        this.textView[13] = (TextView) findViewById(R.id.textView16);
        this.textView[14] = (TextView) findViewById(R.id.textView17);
        this.textView[15] = (TextView) findViewById(R.id.textView18);
        this.textView[16] = (TextView) findViewById(R.id.textView19);
        this.textView[17] = (TextView) findViewById(R.id.textView20);
        this.textView[18] = (TextView) findViewById(R.id.textView21);
        this.textView[19] = (TextView) findViewById(R.id.textView22);
        for (int i = 0; i < 10; i++) {
            this.textView[i].setText(this.highscore.getName(i));
        }
        for (int i2 = 10; i2 < 20; i2++) {
            this.textView[i2].setText(Long.toString(this.highscore.getScore(i2 - 10)));
        }
        LinearLayout layout = (LinearLayout) findViewById(R.id.layout_start);
        if (layout == null) {
            Log.e("AdWhirl", "Layout is null!");
            return;
        }
        float density = getResources().getDisplayMetrics().density;
        int width = (int) (((float) Constants.INMOBI_ADVIEW_WIDTH) * density);
        AdWhirlTargeting.setTestMode(false);
        layout.setGravity(1);
        layout.invalidate();
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) findViewById(R.id.adwhirl_layout);
        adWhirlLayout.setAdWhirlInterface(this);
        adWhirlLayout.setMaxWidth(width);
        adWhirlLayout.setMaxHeight((int) (((float) 52) * density));
    }

    public void adWhirlGeneric() {
    }
}
