package com.epagames.HelloKittyMemory;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int black = 2131099651;
        public static final int lightBlue = 2131099650;
        public static final int red = 2131099649;
        public static final int white = 2131099648;
    }

    public static final class drawable {
        public static final int back = 2130837504;
        public static final int icon = 2130837505;
        public static final int logo = 2130837506;
        public static final int m0 = 2130837507;
        public static final int m1 = 2130837508;
        public static final int m10 = 2130837509;
        public static final int m11 = 2130837510;
        public static final int m12 = 2130837511;
        public static final int m13 = 2130837512;
        public static final int m14 = 2130837513;
        public static final int m15 = 2130837514;
        public static final int m16 = 2130837515;
        public static final int m17 = 2130837516;
        public static final int m18 = 2130837517;
        public static final int m19 = 2130837518;
        public static final int m2 = 2130837519;
        public static final int m20 = 2130837520;
        public static final int m21 = 2130837521;
        public static final int m22 = 2130837522;
        public static final int m23 = 2130837523;
        public static final int m24 = 2130837524;
        public static final int m25 = 2130837525;
        public static final int m26 = 2130837526;
        public static final int m27 = 2130837527;
        public static final int m28 = 2130837528;
        public static final int m29 = 2130837529;
        public static final int m3 = 2130837530;
        public static final int m30 = 2130837531;
        public static final int m31 = 2130837532;
        public static final int m4 = 2130837533;
        public static final int m5 = 2130837534;
        public static final int m6 = 2130837535;
        public static final int m7 = 2130837536;
        public static final int m8 = 2130837537;
        public static final int m9 = 2130837538;
        public static final int mdefault = 2130837539;
        public static final int menu = 2130837540;
        public static final int title = 2130837541;
        public static final int title2 = 2130837542;
        public static final int w0 = 2130837543;
        public static final int w1 = 2130837544;
        public static final int w2 = 2130837545;
        public static final int w3 = 2130837546;
        public static final int w4 = 2130837547;
        public static final int w5 = 2130837548;
        public static final int w6 = 2130837549;
        public static final int w7 = 2130837550;
    }

    public static final class id {
        public static final int Free_play = 2131165242;
        public static final int Start_new_game = 2131165237;
        public static final int Time_attack = 2131165241;
        public static final int adwhirl_layout = 2131165193;
        public static final int back = 2131165231;
        public static final int button1 = 2131165214;
        public static final int editText1 = 2131165213;
        public static final int high = 2131165239;
        public static final int imageView1 = 2131165188;
        public static final int imageView10 = 2131165200;
        public static final int imageView11 = 2131165201;
        public static final int imageView12 = 2131165202;
        public static final int imageView13 = 2131165191;
        public static final int imageView14 = 2131165197;
        public static final int imageView15 = 2131165198;
        public static final int imageView16 = 2131165199;
        public static final int imageView17 = 2131165192;
        public static final int imageView18 = 2131165194;
        public static final int imageView19 = 2131165195;
        public static final int imageView2 = 2131165206;
        public static final int imageView20 = 2131165196;
        public static final int imageView3 = 2131165207;
        public static final int imageView4 = 2131165208;
        public static final int imageView5 = 2131165189;
        public static final int imageView6 = 2131165203;
        public static final int imageView7 = 2131165204;
        public static final int imageView8 = 2131165205;
        public static final int imageView9 = 2131165190;
        public static final int layout_game = 2131165184;
        public static final int layout_mode = 2131165240;
        public static final int layout_start = 2131165215;
        public static final int levels = 2131165185;
        public static final int logo = 2131165243;
        public static final int more = 2131165238;
        public static final int relativeLayout1 = 2131165187;
        public static final int relativeLayout2 = 2131165216;
        public static final int textView1 = 2131165209;
        public static final int textView10 = 2131165229;
        public static final int textView11 = 2131165232;
        public static final int textView12 = 2131165234;
        public static final int textView13 = 2131165218;
        public static final int textView14 = 2131165219;
        public static final int textView15 = 2131165221;
        public static final int textView16 = 2131165223;
        public static final int textView17 = 2131165225;
        public static final int textView18 = 2131165227;
        public static final int textView19 = 2131165236;
        public static final int textView2 = 2131165212;
        public static final int textView20 = 2131165230;
        public static final int textView21 = 2131165233;
        public static final int textView22 = 2131165235;
        public static final int textView23 = 2131165217;
        public static final int textView3 = 2131165210;
        public static final int textView4 = 2131165211;
        public static final int textView5 = 2131165220;
        public static final int textView6 = 2131165222;
        public static final int textView7 = 2131165224;
        public static final int textView8 = 2131165226;
        public static final int textView9 = 2131165228;
        public static final int time = 2131165186;
    }

    public static final class layout {
        public static final int game = 2130903040;
        public static final int highscore = 2130903041;
        public static final int highscores = 2130903042;
        public static final int main = 2130903043;
        public static final int mode = 2130903044;
        public static final int splashscreen = 2130903045;
    }

    public static final class raw {
        public static final int cheer = 2130968576;
        public static final int correct_sound = 2130968577;
    }

    public static final class string {
        public static final int Free_play = 2131034126;
        public static final int Score = 2131034120;
        public static final int Start_new_game = 2131034114;
        public static final int Time_attack = 2131034125;
        public static final int app_name = 2131034113;
        public static final int back = 2131034122;
        public static final int h = 2131034124;
        public static final int hello = 2131034112;
        public static final int high = 2131034123;
        public static final int highScore = 2131034115;
        public static final int highScore2 = 2131034118;
        public static final int more = 2131034121;
        public static final int name = 2131034116;
        public static final int name2 = 2131034119;
        public static final int submit = 2131034117;
    }
}
