package com.inmobi.androidsdk.impl.net;

import android.util.Log;
import com.inmobi.androidsdk.EducationType;
import com.inmobi.androidsdk.EthnicityType;
import com.inmobi.androidsdk.GenderType;
import com.inmobi.androidsdk.impl.Constants;
import com.inmobi.androidsdk.impl.UserInfo;
import com.inmobi.androidsdk.impl.net.RequestResponseManager;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;

public final class HttpRequestBuilder {
    static String buildPostBody(UserInfo userInfoRef, RequestResponseManager.ActionType usedFor) {
        String retStr = "";
        try {
            if (RequestResponseManager.ActionType.AdRequest == usedFor) {
                Log.v("", "in ad request post body..");
                String sdkDelegateString = getSDKDelegatePostBodyString(userInfoRef);
                if (!"".equals(sdkDelegateString)) {
                    sdkDelegateString = String.valueOf(sdkDelegateString) + "&";
                }
                retStr = "requestactivity=AdRequest&" + sdkDelegateString + getDevicePostBodyString(userInfoRef) + "&" + getApplicationPostBodyString(userInfoRef);
            } else if (RequestResponseManager.ActionType.DeviceInfoUpload == usedFor) {
                retStr = "requestactivity=DeviceInfo&" + getDevicePostBodyString(userInfoRef) + "&" + getApplicationPostBodyString(userInfoRef);
            } else {
                retStr = "requestactivity=AdClicked&" + getDevicePostBodyString(userInfoRef) + "&" + getApplicationPostBodyString(userInfoRef);
            }
        } catch (Exception e) {
        }
        Log.v(Constants.LOGGING_TAG, retStr);
        return retStr;
    }

    private static String getSDKDelegatePostBodyString(UserInfo userInfoRef) {
        String str;
        StringBuilder sdkPostBody = new StringBuilder();
        sdkPostBody.append("");
        if (userInfoRef.getPostalCode() != null) {
            sdkPostBody.append("u-postalCode=");
            sdkPostBody.append(getURLEncoded(userInfoRef.getPostalCode()));
        }
        if (userInfoRef.getRequestParams() != null) {
            HashMap<String, String> tempHashMap = userInfoRef.getRequestParams();
            for (String key : tempHashMap.keySet()) {
                sdkPostBody.append("&").append(getURLEncoded(key)).append("=").append(getURLEncoded(tempHashMap.get(key)));
            }
        }
        if (userInfoRef.getAreaCode() != null) {
            sdkPostBody.append("&u-areaCode=");
            sdkPostBody.append(getURLEncoded(userInfoRef.getAreaCode()));
        }
        if (userInfoRef.getDateOfBirth() != null) {
            sdkPostBody.append("&u-dateOfBirth=");
            sdkPostBody.append(getURLEncoded(userInfoRef.getDateOfBirth()));
        }
        if (!(userInfoRef.getGender() == GenderType.G_None || userInfoRef.getGender() == null)) {
            sdkPostBody.append("&u-gender=");
            if (userInfoRef.getGender() == GenderType.G_M) {
                str = "M";
            } else {
                str = "F";
            }
            sdkPostBody.append(str);
        }
        if (userInfoRef.getKeywords() != null) {
            sdkPostBody.append("&p-keywords=");
            sdkPostBody.append(getURLEncoded(userInfoRef.getKeywords()));
        }
        if (userInfoRef.getSearchString() != null) {
            sdkPostBody.append("&p-type=");
            sdkPostBody.append(getURLEncoded(userInfoRef.getSearchString()));
        }
        if (userInfoRef.getIncome() > 0) {
            sdkPostBody.append("&u-income=");
            sdkPostBody.append(userInfoRef.getIncome());
        }
        if (!(userInfoRef.getEducation() == EducationType.Edu_None || userInfoRef.getEducation() == null)) {
            sdkPostBody.append("&u-education=");
            sdkPostBody.append(userInfoRef.getEducation());
        }
        if (!(userInfoRef.getEthnicity() == EthnicityType.Eth_None || userInfoRef.getEthnicity() == null)) {
            sdkPostBody.append("&u-ethnicity=");
            sdkPostBody.append(userInfoRef.getEthnicity());
        }
        if (userInfoRef.getAge() > 0) {
            sdkPostBody.append("&u-age=");
            sdkPostBody.append(userInfoRef.getAge());
        }
        if (userInfoRef.getInterests() != null) {
            sdkPostBody.append("&u-interests=");
            sdkPostBody.append(getURLEncoded(userInfoRef.getInterests()));
        }
        String retString = sdkPostBody.toString();
        if (retString == null) {
            return retString;
        }
        try {
            if (retString.length() <= 2 || retString.charAt(0) != '&') {
                return retString;
            }
            return retString.substring(1);
        } catch (Exception e) {
            return retString;
        }
    }

    private static String getDevicePostBodyString(UserInfo userInfoRef) {
        StringBuilder devicePostBodyString = new StringBuilder();
        devicePostBodyString.append("d-deviceModel=");
        devicePostBodyString.append(getURLEncoded(userInfoRef.getDeviceModel()));
        if (userInfoRef.getDeviceMachineHW() != null) {
            devicePostBodyString.append("&d-deviceMachineHW=");
            devicePostBodyString.append(getURLEncoded(userInfoRef.getDeviceMachineHW()));
        }
        devicePostBodyString.append("&d-deviceSystemName=");
        devicePostBodyString.append(getURLEncoded(userInfoRef.getDeviceSystemName()));
        devicePostBodyString.append("&d-deviceSystemVersion=");
        devicePostBodyString.append(getURLEncoded(userInfoRef.getDeviceSystemVersion()));
        if (userInfoRef.getDeviceName() != null) {
            devicePostBodyString.append("&d-deviceName=");
            devicePostBodyString.append(getURLEncoded(userInfoRef.getDeviceName()));
        }
        if (userInfoRef.getDeviceBTHW() != null) {
            devicePostBodyString.append("&d-deviceBTHW=");
            devicePostBodyString.append(getURLEncoded(userInfoRef.getDeviceBTHW()));
        }
        if (userInfoRef.getDeviceStorageSize() != null) {
            devicePostBodyString.append("&d-deviceStorageSize=");
            devicePostBodyString.append(getURLEncoded(userInfoRef.getDeviceStorageSize()));
        }
        if (userInfoRef.getScreenDensity() != null) {
            devicePostBodyString.append("&d-device-screen-density=").append(getURLEncoded(userInfoRef.getScreenDensity()));
        }
        if (userInfoRef.getScreenSize() != null) {
            devicePostBodyString.append("&d-device-screen-size=").append(getURLEncoded(userInfoRef.getScreenSize()));
        }
        if (userInfoRef.getScreenSize() != null) {
            devicePostBodyString.append("&x-inmobi-phone-useragent=").append(getURLEncoded(userInfoRef.getPhoneDefaultUserAgent()));
        }
        return devicePostBodyString.toString();
    }

    private static String getApplicationPostBodyString(UserInfo userInfoRef) {
        StringBuilder appPostBodyString = new StringBuilder();
        if (userInfoRef.getSiteId() != null) {
            appPostBodyString.append("mk-siteid=");
            appPostBodyString.append(getURLEncoded(userInfoRef.getSiteId()));
        }
        appPostBodyString.append("&u-id=");
        appPostBodyString.append(getURLEncoded(userInfoRef.getDeviceId()));
        appPostBodyString.append("&mk-version=");
        appPostBodyString.append(getURLEncoded("pr-SPEC-ATATA-20091223"));
        appPostBodyString.append("&format=xhtml");
        appPostBodyString.append("&mk-ads=1");
        appPostBodyString.append("&h-user-agent=");
        appPostBodyString.append(getURLEncoded(userInfoRef.getUserAgent()));
        appPostBodyString.append("&u-InMobi_androidwebsdkVersion=");
        appPostBodyString.append(getURLEncoded(Constants.SDK_VERSION));
        appPostBodyString.append("&u-appBId=");
        appPostBodyString.append(getURLEncoded(userInfoRef.getAppBId()));
        appPostBodyString.append("&u-appDNM=");
        appPostBodyString.append(getURLEncoded(userInfoRef.getAppDisplayName()));
        appPostBodyString.append("&u-appVer=");
        appPostBodyString.append(getURLEncoded(userInfoRef.getAppVer()));
        appPostBodyString.append("&d-localization=");
        appPostBodyString.append(getURLEncoded(userInfoRef.getLocalization()));
        appPostBodyString.append("&d-netType=");
        appPostBodyString.append(getURLEncoded(userInfoRef.getNetworkType()));
        appPostBodyString.append("&mk-banner-size=");
        appPostBodyString.append(getURLEncoded(userInfoRef.getAdUnitSlot()));
        if (userInfoRef.isValidGeoInfo()) {
            appPostBodyString.append("&u-lat_lon_accu=");
            appPostBodyString.append(getURLEncoded(currentLocationStr(userInfoRef)));
        }
        if (!(userInfoRef.getRefTagKey() == null || userInfoRef.getRefTagValue() == null)) {
            appPostBodyString.append("&").append(getURLEncoded(userInfoRef.getRefTagKey())).append("=").append(getURLEncoded(userInfoRef.getRefTagValue()));
            appPostBodyString.append(getURLEncoded(currentLocationStr(userInfoRef)));
        }
        return appPostBodyString.toString();
    }

    public static String currentLocationStr(UserInfo clientInfo) {
        StringBuilder data = new StringBuilder();
        if (data == null || !clientInfo.isValidGeoInfo()) {
            return "";
        }
        data.append(clientInfo.getLat());
        data.append(",");
        data.append(clientInfo.getLon());
        data.append(",");
        data.append((int) clientInfo.getLocAccuracy());
        return data.toString();
    }

    public static String getURLEncoded(String value) {
        try {
            return URLEncoder.encode(value, "UTF-8");
        } catch (Exception e) {
            return "";
        }
    }

    public static String getURLDecoded(String value, String encoding) {
        try {
            return URLDecoder.decode(value, encoding);
        } catch (Exception e) {
            return "";
        }
    }
}
