package com.inmobi.androidsdk.impl;

public final class Constants {
    public static final int AD_ACTION_ID = 3;
    public static final int AD_BANNER_ID = 5;
    public static final int AD_BGWEBVIEW_HOLDER = 8111983;
    public static final int AD_BY_ID = 4;
    public static final String AD_BY_INMOBI = "Ad by InMobi";
    public static final int AD_P_CHAKRA_ID = 6;
    public static final String AD_SERVER_CACHED_LIFE = "inmobicachedlife";
    public static final String AD_SERVER_CACHED_URL = "inmobicachedserver";
    public static final String AD_SERVER_URL = "http://i.w.inmobi.com/showad.asm?";
    public static final String AD_TEST_SERVER_URL = "http://w.sandbox.mkhoj.com/showad.asm?";
    public static final int AD_TEXT_ID = 2;
    public static final int AD_TILE_ID = 1;
    public static final int AD_TLAYER_HOLDER = 8111982;
    public static final long CACHED_AD_SERVER_LIFE = 43200000;
    public static final String CACHED_AD_SERVER_TIMESTAMP = "inmobi_cached_timestamp";
    public static final int EWV_PHOLDER = 8111981;
    public static final int INMOBI_ADVIEW_HEIGHT = 50;
    public static final int INMOBI_ADVIEW_WIDTH = 320;
    public static final int INMOBI_AD_CLICK_SCREEN_CLOSED = 8111981;
    public static final int INMOBI_ID = 7;
    public static final String LOGGING_TAG = "inmobiandroidwebsdk";
    public static final String SDK_VERSION = "2.0";
}
