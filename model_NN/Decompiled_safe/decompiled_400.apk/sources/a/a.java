package a;

import android.graphics.drawable.Drawable;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Comparator;
import java.util.List;

public class a {

    /* renamed from: a  reason: collision with root package name */
    public static final Comparator f0a = new b();

    public static int a(float f, float f2, float f3, float f4) {
        return ((((int) (((double) (f * 255.0f)) + 0.5d)) & 255) << 16) | ((((int) (((double) (f4 * 255.0f)) + 0.5d)) & 255) << 24) | ((((int) (((double) (f2 * 255.0f)) + 0.5d)) & 255) << 8) | ((((int) (((double) (f3 * 255.0f)) + 0.5d)) & 255) << 0);
    }

    public static Drawable a(Class cls, String str) {
        return Drawable.createFromStream(cls.getClassLoader().getResourceAsStream(str), str);
    }

    public static String a(CharSequence charSequence) {
        if (charSequence == null) {
            return "";
        }
        String obj = charSequence.toString();
        try {
            return URLEncoder.encode(obj, "UTF8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return obj;
        }
    }

    public static String a(String str) {
        byte[] bytes;
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            try {
                bytes = str.getBytes("UTF-8");
            } catch (UnsupportedEncodingException e) {
                bytes = str.getBytes();
            }
            StringBuilder sb = new StringBuilder();
            for (byte b : instance.digest(bytes)) {
                sb.append(Integer.toHexString((b & 240) >>> 4));
                sb.append(Integer.toHexString(b & 15));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e2) {
            throw new RuntimeException(e2);
        }
    }

    public static String a(List list, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append((String) list.get(0));
        int size = list.size();
        for (int i = 1; i < size; i++) {
            sb.append(str).append((String) list.get(i));
        }
        return sb.toString();
    }

    public static StringBuilder a(InputStream inputStream) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
        StringBuilder sb = new StringBuilder();
        char[] cArr = new char[1000];
        for (int i = 0; i >= 0; i = bufferedReader.read(cArr)) {
            sb.append(cArr, 0, i);
        }
        return sb;
    }

    public static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void a(HttpURLConnection httpURLConnection) {
        if (httpURLConnection != null) {
            httpURLConnection.disconnect();
        }
    }
}
