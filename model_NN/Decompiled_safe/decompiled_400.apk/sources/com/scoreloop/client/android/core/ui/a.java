package com.scoreloop.client.android.core.ui;

import com.a.a.d;
import com.a.a.q;

final class a extends d {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ h f134a;

    private a(h hVar) {
        this.f134a = hVar;
    }

    /* synthetic */ a(h hVar, b bVar) {
        this(hVar);
    }

    public void a(q qVar) {
        this.f134a.f141a.c();
    }

    public void a(q qVar, Throwable th) {
        this.f134a.f141a.a().didFail(th);
    }

    public void b(q qVar) {
        this.f134a.f141a.a().userDidCancel();
    }
}
