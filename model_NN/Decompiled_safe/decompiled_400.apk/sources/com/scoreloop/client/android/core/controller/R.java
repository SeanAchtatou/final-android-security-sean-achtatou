package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import org.json.JSONException;
import org.json.JSONObject;

class R extends Request {

    /* renamed from: a  reason: collision with root package name */
    private final Game f61a;
    private final Score b;
    private final Session c;

    public R(RequestCompletionCallback requestCompletionCallback, Game game, Session session, Score score) {
        super(requestCompletionCallback);
        this.f61a = game;
        this.c = session;
        this.b = score;
    }

    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            this.b.a(this.c.a().a());
            this.b.a(this.c.getUser());
            jSONObject.put("score", this.b.a());
            return jSONObject;
        } catch (JSONException e) {
            throw new IllegalStateException("Invalid score data", e);
        }
    }

    public RequestMethod b() {
        return RequestMethod.POST;
    }

    public String c() {
        return String.format("/service/games/%s/scores", this.f61a.getIdentifier());
    }
}
