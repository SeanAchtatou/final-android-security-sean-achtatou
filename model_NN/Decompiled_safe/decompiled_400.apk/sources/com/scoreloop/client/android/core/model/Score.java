package com.scoreloop.client.android.core.model;

import com.scoreloop.client.android.core.utils.Formats;
import com.scoreloop.client.android.core.utils.JSONUtils;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class Score implements MessageTarget {

    /* renamed from: a  reason: collision with root package name */
    private String f101a;
    private String b;
    private Integer c;
    private Double d;
    private Integer e;
    private Double f;
    private Integer g;
    private Date h;
    private User i;
    private Map j;

    public Score(Double d2, Map map, User user) {
        this.f = d2;
        this.i = user;
        this.f101a = user.b();
        if (map != null) {
            this.j = new HashMap(map);
            this.d = (Double) this.j.get(Game.CONTEXT_KEY_MINOR_RESULT);
            this.c = (Integer) this.j.get(Game.CONTEXT_KEY_LEVEL);
            this.e = (Integer) this.j.get(Game.CONTEXT_KEY_MODE);
            this.j.remove(Game.CONTEXT_KEY_MINOR_RESULT);
            this.j.remove(Game.CONTEXT_KEY_LEVEL);
            this.j.remove(Game.CONTEXT_KEY_MODE);
        }
    }

    public Score(JSONObject jSONObject) {
        a(jSONObject);
    }

    public static boolean areModesEqual(Score score, Score score2) {
        if (score != null && score2 != null) {
            return areModesEqual(score.getMode(), score2.getMode());
        }
        throw new IllegalArgumentException();
    }

    public static boolean areModesEqual(Integer num, Score score) {
        if (score != null) {
            return areModesEqual(num, score.getMode());
        }
        throw new IllegalArgumentException();
    }

    public static boolean areModesEqual(Integer num, Integer num2) {
        if (num != null) {
            return num.equals(num2);
        }
        throw new IllegalArgumentException();
    }

    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("device_id", this.f101a);
        jSONObject.put("result", this.f);
        jSONObject.put("level", this.c);
        if (this.i != null) {
            jSONObject.put("user_id", this.i.getIdentifier());
        }
        if (this.h != null) {
            jSONObject.put("updated_at", Formats.f149a.format(this.h));
        }
        jSONObject.put("mode", this.e);
        jSONObject.put("minor_result", this.d);
        if (this.j != null) {
            jSONObject.put("context", JSONUtils.a(this.j));
        }
        return jSONObject;
    }

    public void a(User user) {
        this.i = user;
    }

    public void a(Integer num) {
        this.g = num;
    }

    public void a(String str) {
        this.f101a = str;
    }

    public void a(JSONObject jSONObject) {
        if (jSONObject.has("id")) {
            this.b = jSONObject.getString("id");
        }
        if (jSONObject.has("device_id")) {
            this.f101a = jSONObject.getString("device_id");
        }
        if (jSONObject.has("result")) {
            this.f = Double.valueOf(jSONObject.getDouble("result"));
        }
        if (jSONObject.has("minor_result")) {
            this.d = Double.valueOf(jSONObject.getDouble("minor_result"));
        }
        if (jSONObject.has("level")) {
            this.c = Integer.valueOf(jSONObject.getInt("level"));
        }
        if (jSONObject.has("minor_result")) {
            this.e = Integer.valueOf(jSONObject.getInt("mode"));
        }
        if (jSONObject.has("updated_at")) {
            try {
                this.h = Formats.f149a.parse(jSONObject.getString("updated_at"));
            } catch (ParseException e2) {
                throw new JSONException("Invalid format of the update date");
            }
        }
        if (jSONObject.has("user")) {
            this.i = new User(jSONObject.getJSONObject("user"));
        }
        if (jSONObject.has("context")) {
            this.j = jSONObject.isNull("context") ? null : JSONUtils.a(jSONObject.getJSONObject("context"));
        }
    }

    public String b() {
        return "score";
    }

    public Map getContext() {
        return this.j;
    }

    public String getIdentifier() {
        return this.b;
    }

    public Integer getLevel() {
        return this.c;
    }

    public Double getMinorResult() {
        return this.d;
    }

    public Integer getMode() {
        return this.e;
    }

    public Integer getRank() {
        return this.g;
    }

    public Double getResult() {
        return this.f;
    }

    public Date getUpdatedAt() {
        return this.h;
    }

    public User getUser() {
        return this.i;
    }

    public void setContext(Map map) {
        this.j = map;
    }

    public void setLevel(Integer num) {
        this.c = num;
    }

    public void setMinorResult(Double d2) {
        this.d = d2;
    }

    public void setMode(Integer num) {
        this.e = num;
    }

    public void setResult(Double d2) {
        this.f = d2;
    }
}
