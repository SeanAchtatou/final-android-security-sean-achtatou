package com.scoreloop.client.android.core.controller;

import java.util.Collection;
import org.json.JSONArray;

/* renamed from: com.scoreloop.client.android.core.controller.p  reason: case insensitive filesystem */
class C0016p {

    /* renamed from: a  reason: collision with root package name */
    private final String f80a;
    private final C0002b b;
    private final Object c;

    public C0016p(String str, C0002b bVar, String str2) {
        this.f80a = str;
        this.b = bVar;
        this.c = str2;
    }

    public C0016p(String str, C0002b bVar, Collection collection) {
        this.f80a = str;
        this.b = bVar;
        this.c = new JSONArray(collection);
    }

    public String a() {
        String a2 = this.b.a();
        if (a2 == null) {
            return this.f80a;
        }
        return String.format("%s_%s", this.f80a, a2);
    }

    public Object b() {
        return this.c;
    }
}
