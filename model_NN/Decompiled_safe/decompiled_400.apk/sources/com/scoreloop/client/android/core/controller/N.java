package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Session;

final class N implements RequestControllerObserver {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ boolean f57a = (!U.class.desiredAssertionStatus());
    final /* synthetic */ U b;

    private N(U u) {
        this.b = u;
    }

    public void requestControllerDidFail(RequestController requestController, Exception exc) {
        Session e = this.b.e();
        if (f57a || e.c() == Session.State.AUTHENTICATING) {
            e.a(Session.State.FAILED);
            this.b.b(exc);
            C0021u unused = this.b.c = (C0021u) null;
            return;
        }
        throw new AssertionError();
    }

    public void requestControllerDidReceiveResponse(RequestController requestController) {
        Session e = this.b.e();
        if (!f57a && e.c() != Session.State.AUTHENTICATING) {
            throw new AssertionError();
        } else if (e.a().a() != null) {
            e.getUser().a(e.a().a());
            C0021u unused = this.b.c = (C0021u) null;
        } else {
            throw new IllegalStateException();
        }
    }
}
