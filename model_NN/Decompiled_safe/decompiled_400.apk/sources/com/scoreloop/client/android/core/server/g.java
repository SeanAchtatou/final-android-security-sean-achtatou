package com.scoreloop.client.android.core.server;

import com.scoreloop.client.android.core.utils.Base64;
import com.scoreloop.client.android.core.utils.Logger;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.InterruptedIOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;

class g {

    /* renamed from: a  reason: collision with root package name */
    private final HttpClient f120a = new DefaultHttpClient();
    private String b;
    private final URI c;
    private String d;

    g(URI uri) {
        this.c = uri;
        HttpParams params = this.f120a.getParams();
        HttpConnectionParams.setSoTimeout(params, 90000);
        HttpConnectionParams.setConnectionTimeout(params, 10000);
    }

    private JSONArray a(HttpPost httpPost, Object obj) {
        try {
            String a2 = a(httpPost, obj.toString());
            try {
                JSONArray jSONArray = new JSONArray(a2);
                a("Response:\n", jSONArray.toString(4));
                return jSONArray;
            } catch (JSONException e) {
                b("Invalid raw response:\n", a2);
                throw new f("Server response body failed to parse", e);
            }
        } catch (HttpResponseException e2) {
            throw new f("Failure indicated by HTTP status code", e2);
        } catch (ClientProtocolException e3) {
            throw new f("HTTP protocol error", e3);
        } catch (InterruptedIOException e4) {
            throw new h(e4);
        } catch (IOException e5) {
            throw new d("I/O error occured", e5);
        }
    }

    private void a(String str, String str2) {
        Logger.a("doJSONRequest", "\n ");
        Logger.a("doJSONRequest", str);
        String[] split = str2.split("\\},\\n");
        int i = 0;
        while (i < split.length - 1) {
            Logger.a("doJSONRequest", split[i] + "},\n");
            i++;
        }
        Logger.a("doJSONRequest", split[i]);
    }

    private void a(JSONArray jSONArray) {
        String jSONArray2;
        try {
            jSONArray2 = jSONArray.toString(4);
        } catch (JSONException e) {
            jSONArray2 = jSONArray.toString();
        }
        a("Request:\n", jSONArray2);
    }

    private void b(String str, String str2) {
        Logger.b("doJSONRequest", "\n ");
        Logger.b("doJSONRequest", str);
        String[] split = str2.split("\\},\\n");
        int i = 0;
        while (i < split.length - 1) {
            Logger.b("doJSONRequest", split[i] + "},\n");
            i++;
        }
        Logger.b("doJSONRequest", split[i]);
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return "application/json";
    }

    /* access modifiers changed from: package-private */
    public String a(HttpPost httpPost, String str) {
        if (this.d == null || this.b == null) {
            throw new IllegalStateException("Try setting password and username first");
        }
        httpPost.setHeader("Content-Type", a());
        httpPost.setHeader("Accept", a());
        httpPost.setHeader("X-Scoreloop-SDK-Version", "1");
        httpPost.setHeader("Authorization", "Basic " + Base64.a((this.d + ":" + this.b).getBytes()));
        try {
            httpPost.setEntity(new StringEntity(str, "UTF8"));
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(this.f120a.execute(httpPost).getEntity().getContent()));
            StringBuffer stringBuffer = new StringBuffer();
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    stringBuffer.append(readLine);
                    stringBuffer.append(10);
                } else {
                    bufferedReader.close();
                    return stringBuffer.toString();
                }
            }
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.core.server.g.a(org.apache.http.client.methods.HttpPost, java.lang.Object):org.json.JSONArray
     arg types: [org.apache.http.client.methods.HttpPost, org.json.JSONArray]
     candidates:
      com.scoreloop.client.android.core.server.g.a(java.lang.String, java.lang.String):void
      com.scoreloop.client.android.core.server.g.a(org.apache.http.client.methods.HttpPost, java.lang.String):java.lang.String
      com.scoreloop.client.android.core.server.g.a(org.apache.http.client.methods.HttpPost, org.json.JSONArray):org.json.JSONArray
      com.scoreloop.client.android.core.server.g.a(org.apache.http.client.methods.HttpPost, byte[]):byte[]
      com.scoreloop.client.android.core.server.g.a(org.apache.http.client.methods.HttpPost, java.lang.Object):org.json.JSONArray */
    /* access modifiers changed from: package-private */
    public JSONArray a(HttpPost httpPost, JSONArray jSONArray) {
        a(jSONArray);
        return a(httpPost, (Object) jSONArray);
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.b = str;
    }

    /* access modifiers changed from: package-private */
    public byte[] a(HttpPost httpPost, byte[] bArr) {
        if (this.d == null || this.b == null) {
            throw new IllegalStateException("Try setting password and username first");
        }
        httpPost.setHeader("Content-Type", a());
        httpPost.setHeader("Accept", a());
        httpPost.setHeader("X-Scoreloop-SDK-Version", "1");
        httpPost.setHeader("Authorization", "Basic " + Base64.a((this.d + ":" + this.b).getBytes()));
        httpPost.setEntity(new ByteArrayEntity(bArr));
        HttpEntity entity = this.f120a.execute(httpPost).getEntity();
        byte[] bArr2 = new byte[((int) entity.getContentLength())];
        BufferedInputStream bufferedInputStream = new BufferedInputStream(entity.getContent());
        int length = bArr2.length;
        int i = 0;
        while (length > 0) {
            int read = bufferedInputStream.read(bArr2, i, length);
            if (read == -1) {
                throw new IllegalStateException("Premature EOF");
            }
            length -= read;
            i += read;
        }
        bufferedInputStream.close();
        return bArr2;
    }

    /* access modifiers changed from: package-private */
    public HttpPost b() {
        return new HttpPost(this.c);
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        this.d = str;
    }
}
