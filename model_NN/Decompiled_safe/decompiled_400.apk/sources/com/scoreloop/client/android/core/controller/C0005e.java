package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Session;

/* renamed from: com.scoreloop.client.android.core.controller.e  reason: case insensitive filesystem */
final class C0005e implements SocialProviderControllerObserver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FacebookSocialProviderController f72a;

    private C0005e(FacebookSocialProviderController facebookSocialProviderController) {
        this.f72a = facebookSocialProviderController;
    }

    public void didEnterInvalidCredentials() {
        this.f72a.e.a(false);
        this.f72a.d().didEnterInvalidCredentials();
    }

    public void didFail(Throwable th) {
        this.f72a.e.a(false);
        this.f72a.d().didFail(th);
    }

    public void didSucceed() {
        if (!this.f72a.c) {
            UserController unused = this.f72a.d = new UserController(Session.getCurrentSession(), new V(this.f72a));
            this.f72a.d.setUser(Session.getCurrentSession().getUser());
            this.f72a.d.requestUser();
        }
    }

    public void userDidCancel() {
        this.f72a.d().userDidCancel();
        this.f72a.e.a(false);
    }
}
