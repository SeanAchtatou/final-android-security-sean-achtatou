package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Challenge;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.scoreloop.client.android.core.controller.v  reason: case insensitive filesystem */
class C0022v extends Request {

    /* renamed from: a  reason: collision with root package name */
    protected Challenge f84a;
    protected Game b;
    protected Session c;

    public C0022v(RequestCompletionCallback requestCompletionCallback, Session session, Game game, Challenge challenge) {
        super(requestCompletionCallback);
        this.c = session;
        this.b = game;
        this.f84a = challenge;
    }

    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            User user = this.c.getUser();
            if (this.f84a.getContender() != null && this.f84a.getContender().equals(user)) {
                Score contenderScore = this.f84a.getContenderScore();
                if (contenderScore != null) {
                    contenderScore.a(user.b());
                }
            } else if (this.f84a.getContestant() == null || !this.f84a.getContestant().equals(user)) {
            }
            jSONObject.put("challenge", this.f84a.a());
            return jSONObject;
        } catch (JSONException e) {
            throw new IllegalStateException("Invalid challenge data", e);
        }
    }

    public RequestMethod b() {
        return RequestMethod.POST;
    }

    public String c() {
        return String.format("/service/games/%s/challenges", this.b.getIdentifier());
    }
}
