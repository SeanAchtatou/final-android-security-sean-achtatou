package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Challenge;
import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;

class H extends C0022v {
    public H(RequestCompletionCallback requestCompletionCallback, Session session, Game game, Challenge challenge) {
        super(requestCompletionCallback, session, game, challenge);
    }

    public RequestMethod b() {
        return RequestMethod.PUT;
    }

    public String c() {
        return String.format("/service/games/%s/challenges/%s", this.b.getIdentifier(), this.f84a.getIdentifier());
    }
}
