package com.scoreloop.client.android.core.model;

import com.scoreloop.client.android.core.utils.JSONUtils;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class SocialProvider {
    private static List b;
    private static List c = new ArrayList();

    /* renamed from: a  reason: collision with root package name */
    private JSONObject f105a;

    static {
        c.add(FacebookSocialProvider.class);
        c.add(MySpaceSocialProvider.class);
        c.add(TwitterSocialProvider.class);
        ArrayList arrayList = new ArrayList();
        for (Class newInstance : c) {
            try {
                arrayList.add(newInstance.newInstance());
            } catch (IllegalAccessException e) {
                throw new IllegalStateException(e);
            } catch (InstantiationException e2) {
                throw new IllegalStateException(e2);
            }
        }
        b = arrayList;
    }

    public static void a(User user, JSONObject jSONObject) {
        for (SocialProvider d : b()) {
            d.d(user, jSONObject);
        }
    }

    public static boolean a(Class cls) {
        return c.contains(cls);
    }

    public static List b() {
        return b;
    }

    public static void b(User user, JSONObject jSONObject) {
        for (SocialProvider c2 : b()) {
            c2.c(user, jSONObject);
        }
    }

    public static SocialProvider getSocialProviderForIdentifier(String str) {
        for (SocialProvider socialProvider : b()) {
            if (socialProvider.getIdentifier().equalsIgnoreCase(str)) {
                return socialProvider;
            }
        }
        return null;
    }

    public abstract Class a();

    public void a(User user, String str, String str2, String str3) {
        if (user == null || str == null || str2 == null || str3 == null) {
            throw new IllegalArgumentException();
        }
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("token", str);
            jSONObject.put("token_secret", str2);
            jSONObject.put("uid", str3);
            this.f105a = jSONObject;
            user.a(jSONObject, getName());
        } catch (JSONException e) {
            throw new IllegalStateException(e);
        }
    }

    public void c(User user, JSONObject jSONObject) {
        if (user == null || jSONObject == null) {
            throw new IllegalArgumentException();
        } else if (JSONUtils.a(jSONObject, getName())) {
            try {
                user.a(jSONObject.getJSONObject(getName()), getName());
            } catch (JSONException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void d(User user, JSONObject jSONObject) {
        if (user.b(getName())) {
            this.f105a = user.c(getName());
        }
        if (this.f105a != null) {
            try {
                jSONObject.put(getName(), this.f105a);
            } catch (JSONException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    public abstract String getIdentifier();

    public final String getName() {
        return getIdentifier().split("\\.")[1];
    }

    public final Integer getVersion() {
        return new Integer(getIdentifier().split("\\.")[2].substring(1));
    }

    public abstract boolean isUserConnected(User user);
}
