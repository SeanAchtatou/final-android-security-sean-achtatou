package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.Response;
import com.scoreloop.client.android.core.utils.Logger;
import org.json.JSONObject;

public class ScoreController extends RequestController {
    private Score c;
    private boolean d;
    private ChallengeController e;

    public ScoreController(RequestControllerObserver requestControllerObserver) {
        this(Session.getCurrentSession(), requestControllerObserver);
    }

    public ScoreController(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver);
        this.d = false;
    }

    private void a(Score score) {
        this.c = score;
    }

    private void i() {
        if (e().getChallenge() != null) {
            if (this.d) {
                if (this.e == null) {
                    this.e = new ChallengeController(new C0006f(this));
                }
                this.e.submitChallengeScore(this.c);
                return;
            }
            Logger.b("It seems that a challenge is in progess. Submitted score won't get associated with that challenge. Call setShouldSubmitScoreForChallenge(true); to make this go away.");
        }
        R r = new R(d(), b(), e(), this.c);
        h();
        a(r);
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) {
        int f = response.f();
        JSONObject jSONObject = response.e().getJSONObject("score");
        if (getScore() == null) {
            a(new Score(jSONObject));
        } else {
            getScore().a(jSONObject);
        }
        if (f == 200 || f == 201) {
            return true;
        }
        throw new Exception("Request failed");
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return true;
    }

    public Score getScore() {
        return this.c;
    }

    public void setShouldSubmitScoreForChallenge(boolean z) {
        this.d = z;
    }

    public boolean shouldSubmitScoreForChallenge() {
        return this.d;
    }

    public void submitScore(Score score) {
        if (score == null) {
            throw new IllegalArgumentException("aScore parameter cannot be null");
        }
        this.c = score;
        i();
    }
}
