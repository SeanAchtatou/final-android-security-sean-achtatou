package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.utils.Logger;

class C implements RequestCompletionCallback {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RequestController f51a;

    private C(RequestController requestController) {
        this.f51a = requestController;
    }

    /* synthetic */ C(RequestController requestController, M m) {
        this(requestController);
    }

    public void a(Request request) {
        switch (M.f56a[request.l().ordinal()]) {
            case 1:
                Logger.a("RequestController", "RequestCallback.onRequestCompleted: request completed: " + request.toString());
                try {
                    if (this.f51a.a(request, request.k())) {
                        this.f51a.j();
                        return;
                    }
                    return;
                } catch (Exception e) {
                    this.f51a.c(e);
                    return;
                }
            case 2:
                Logger.a("RequestController", "RequestCallback.onRequestCompleted: request failed: " + request.toString());
                this.f51a.c(request.h());
                return;
            case 3:
                Logger.a("RequestController", "RequestCallback.onRequestCompleted: request cancelled: " + request.toString());
                this.f51a.c(new RequestCancelledException());
                return;
            default:
                throw new IllegalStateException("onRequestCompleted called for not completed request");
        }
    }

    public void b(Request request) {
    }
}
