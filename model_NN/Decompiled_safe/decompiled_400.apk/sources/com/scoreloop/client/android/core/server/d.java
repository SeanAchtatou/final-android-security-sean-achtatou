package com.scoreloop.client.android.core.server;

class d extends Exception {
    public d() {
    }

    public d(String str) {
        super(str);
    }

    public d(String str, Throwable th) {
        super(str, th);
    }

    public d(Throwable th) {
        super(th);
    }
}
