package com.admob.android.ads.view;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Parcelable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import com.admob.android.ads.AdManager;
import com.admob.android.ads.r;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Hashtable;
import java.util.StringTokenizer;

public class AdMobWebView extends WebView {

    /* renamed from: a  reason: collision with root package name */
    String f46a;
    public String b;
    boolean c = true;
    private RelativeLayout d;

    public AdMobWebView(Context context, RelativeLayout relativeLayout, String str) {
        super(context);
        this.d = relativeLayout;
        WebSettings settings = getSettings();
        settings.setLoadsImagesAutomatically(true);
        settings.setPluginsEnabled(true);
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setSaveFormData(false);
        settings.setSavePassword(false);
        settings.setUserAgentString(r.c());
        setWebViewClient(new a(this));
        this.f46a = str;
    }

    /* access modifiers changed from: private */
    public void removeSelf() {
        ((WindowManager) getContext().getSystemService("window")).removeView(this.d);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        removeSelf();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onFocusChanged(boolean z, int i, Rect rect) {
        Log.d(AdManager.LOG, "onFocusChanged(" + z + ")");
        super.onFocusChanged(z, i, rect);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        Log.d(AdManager.LOG, "onSaveInstanceState()");
        return super.onSaveInstanceState();
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i) {
        Log.d(AdManager.LOG, "onWindowVisibilityChanged(" + i + ")");
        super.onWindowVisibilityChanged(i);
    }

    private class a extends WebViewClient {

        /* renamed from: a  reason: collision with root package name */
        private AdMobWebView f47a;

        public a(AdMobWebView adMobWebView) {
            this.f47a = adMobWebView;
        }

        public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
            Hashtable<String, String> a2;
            String str2;
            try {
                URI uri = new URI(str);
                if ("admob".equals(uri.getScheme())) {
                    String host = uri.getHost();
                    if ("launch".equals(host)) {
                        String query = uri.getQuery();
                        if (!(query == null || (a2 = a(query)) == null || (str2 = a2.get("url")) == null)) {
                            AdMobWebView.this.getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str2)));
                            this.f47a.removeSelf();
                            return true;
                        }
                    } else if ("closecanvas".equals(host) && webView == this.f47a) {
                        this.f47a.removeSelf();
                        return true;
                    }
                }
            } catch (URISyntaxException e) {
                Log.w(AdManager.LOG, "Bad link URL in AdMob web view.", e);
            }
            return false;
        }

        private static Hashtable<String, String> a(String str) {
            Hashtable<String, String> hashtable = null;
            if (str != null) {
                hashtable = new Hashtable<>();
                StringTokenizer stringTokenizer = new StringTokenizer(str, "&");
                while (stringTokenizer.hasMoreTokens()) {
                    StringTokenizer stringTokenizer2 = new StringTokenizer(stringTokenizer.nextToken(), "=");
                    if (stringTokenizer2.hasMoreTokens()) {
                        String nextToken = stringTokenizer2.nextToken();
                        if (stringTokenizer2.hasMoreTokens()) {
                            String nextToken2 = stringTokenizer2.nextToken();
                            if (!(nextToken == null || nextToken2 == null)) {
                                hashtable.put(nextToken, nextToken2);
                            }
                        }
                    }
                }
            }
            return hashtable;
        }

        public final void onPageFinished(WebView webView, String str) {
            if ("http://mm.admob.com/static/android/canvas.html".equals(str) && this.f47a.c) {
                this.f47a.c = false;
                this.f47a.loadUrl("javascript:cb('" + AdMobWebView.this.b + "','" + AdMobWebView.this.f46a + "')");
            }
        }
    }
}
