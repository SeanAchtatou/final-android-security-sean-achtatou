package com.admob.android.ads;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

/* compiled from: AdRequester */
final class u {

    /* renamed from: a  reason: collision with root package name */
    private static String f45a = "http://r.admob.com/ad_source.php";
    private static int b;
    private static long c;
    private static String d = null;

    u() {
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x00c9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static com.admob.android.ads.d a(com.admob.android.ads.d.a r14, android.content.Context r15, java.lang.String r16, java.lang.String r17, int r18, int r19, int r20, com.admob.android.ads.g r21, int r22) {
        /*
            java.lang.String r4 = "android.permission.INTERNET"
            int r4 = r15.checkCallingOrSelfPermission(r4)
            r5 = -1
            if (r4 != r5) goto L_0x000e
            java.lang.String r4 = "Cannot request an ad without Internet permissions!  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"android.permission.INTERNET\" />"
            com.admob.android.ads.AdManager.clientError(r4)
        L_0x000e:
            com.admob.android.ads.o.a(r15)
            r11 = 0
            long r12 = android.os.SystemClock.uptimeMillis()
            r0 = r15
            r1 = r16
            r2 = r17
            r3 = r22
            java.lang.String r10 = a(r0, r1, r2, r3)
            java.lang.String r4 = com.admob.android.ads.u.f45a
            r5 = 0
            java.lang.String r6 = com.admob.android.ads.AdManager.getUserId(r15)
            r7 = 0
            r8 = 3000(0xbb8, float:4.204E-42)
            r9 = 0
            com.admob.android.ads.r r15 = com.admob.android.ads.e.a(r4, r5, r6, r7, r8, r9, r10)
            java.lang.String r16 = "AdMobSDK"
            r17 = 3
            boolean r16 = android.util.Log.isLoggable(r16, r17)
            if (r16 == 0) goto L_0x0059
            java.lang.String r16 = "AdMobSDK"
            java.lang.StringBuilder r17 = new java.lang.StringBuilder
            r17.<init>()
            java.lang.String r22 = "Requesting an ad with POST params:  "
            r0 = r17
            r1 = r22
            java.lang.StringBuilder r17 = r0.append(r1)
            r0 = r17
            r1 = r10
            java.lang.StringBuilder r17 = r0.append(r1)
            java.lang.String r17 = r17.toString()
            android.util.Log.d(r16, r17)
        L_0x0059:
            r16 = 0
            boolean r17 = r15.a()
            if (r17 == 0) goto L_0x0103
            byte[] r15 = r15.d()
            java.lang.String r16 = new java.lang.String
            r0 = r16
            r1 = r15
            r0.<init>(r1)
            r15 = r16
        L_0x006f:
            if (r17 == 0) goto L_0x0101
            java.lang.String r16 = "AdMobSDK"
            r17 = 3
            boolean r16 = android.util.Log.isLoggable(r16, r17)
            if (r16 == 0) goto L_0x0082
            java.lang.String r16 = "AdMobSDK"
            java.lang.String r17 = "Ad response: "
            android.util.Log.d(r16, r17)
        L_0x0082:
            java.lang.String r16 = ""
            boolean r16 = r15.equals(r16)
            if (r16 != 0) goto L_0x0101
            org.json.JSONTokener r16 = new org.json.JSONTokener
            r0 = r16
            r1 = r15
            r0.<init>(r1)
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ JSONException -> 0x00f5 }
            r0 = r5
            r1 = r16
            r0.<init>(r1)     // Catch:{ JSONException -> 0x00f5 }
            java.lang.String r15 = "AdMobSDK"
            r16 = 3
            boolean r15 = android.util.Log.isLoggable(r15, r16)     // Catch:{ JSONException -> 0x00f5 }
            if (r15 == 0) goto L_0x00b2
            java.lang.String r15 = "AdMobSDK"
            r16 = 4
            r0 = r5
            r1 = r16
            java.lang.String r16 = r0.toString(r1)     // Catch:{ JSONException -> 0x00f5 }
            android.util.Log.d(r15, r16)     // Catch:{ JSONException -> 0x00f5 }
        L_0x00b2:
            r4 = r14
            r6 = r18
            r7 = r19
            r8 = r20
            r9 = r21
            com.admob.android.ads.d r14 = com.admob.android.ads.d.a(r4, r5, r6, r7, r8, r9)     // Catch:{ JSONException -> 0x00f5 }
        L_0x00bf:
            java.lang.String r15 = "AdMobSDK"
            r16 = 4
            boolean r15 = android.util.Log.isLoggable(r15, r16)
            if (r15 == 0) goto L_0x00f4
            long r15 = android.os.SystemClock.uptimeMillis()
            long r15 = r15 - r12
            if (r14 != 0) goto L_0x00f4
            java.lang.String r17 = "AdMobSDK"
            java.lang.StringBuilder r18 = new java.lang.StringBuilder
            r18.<init>()
            java.lang.String r19 = "No fill.  Server replied that no ads are available ("
            java.lang.StringBuilder r18 = r18.append(r19)
            r0 = r18
            r1 = r15
            java.lang.StringBuilder r15 = r0.append(r1)
            java.lang.String r16 = "ms)"
            java.lang.StringBuilder r15 = r15.append(r16)
            java.lang.String r15 = r15.toString()
            r0 = r17
            r1 = r15
            android.util.Log.i(r0, r1)
        L_0x00f4:
            return r14
        L_0x00f5:
            r14 = move-exception
            java.lang.String r15 = "AdMobSDK"
            java.lang.String r16 = "Problem decoding ad response.  Cannot display ad."
            r0 = r15
            r1 = r16
            r2 = r14
            android.util.Log.w(r0, r1, r2)
        L_0x0101:
            r14 = r11
            goto L_0x00bf
        L_0x0103:
            r15 = r16
            goto L_0x006f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.admob.android.ads.u.a(com.admob.android.ads.d$a, android.content.Context, java.lang.String, java.lang.String, int, int, int, com.admob.android.ads.g, int):com.admob.android.ads.d");
    }

    static String a(Context context, String str, String str2, int i) {
        if (Log.isLoggable(AdManager.LOG, 3)) {
            Log.d(AdManager.LOG, "Ad request:");
        }
        StringBuilder sb = new StringBuilder();
        long currentTimeMillis = System.currentTimeMillis();
        sb.append("z").append("=").append(currentTimeMillis / 1000).append(".").append(currentTimeMillis % 1000);
        a(sb, "rt", "0");
        String publisherId = AdManager.getPublisherId(context);
        if (publisherId == null) {
            throw new IllegalStateException("Publisher ID is not set!  To serve ads you must set your publisher ID assigned from www.admob.com.  Either add it to AndroidManifest.xml under the <application> tag or call AdManager.setPublisherId().");
        }
        a(sb, "s", publisherId);
        a(sb, "l", o.a());
        a(sb, "f", "jsonp");
        a(sb, "client_sdk", "1");
        a(sb, "ex", "1");
        a(sb, "v", AdManager.SDK_VERSION);
        a(sb, "isu", AdManager.getUserId(context));
        a(sb, "so", AdManager.getOrientation(context));
        if (i > 0) {
            a(sb, "screen_width", String.valueOf(i));
        }
        a(sb, "d[coord]", AdManager.a(context));
        a(sb, "d[coord_timestamp]", AdManager.a());
        a(sb, "d[pc]", AdManager.getPostalCode());
        a(sb, "d[dob]", AdManager.b());
        a(sb, "d[gender]", AdManager.c());
        a(sb, "k", str);
        a(sb, "search", str2);
        a(sb, "density", String.valueOf(g.c()));
        if (AdManager.isTestDevice(context)) {
            if (Log.isLoggable(AdManager.LOG, 4)) {
                Log.i(AdManager.LOG, "Making ad request in test mode");
            }
            a(sb, "m", "test");
            a(sb, "test_action", AdManager.getTestAction());
        }
        if (d == null) {
            StringBuilder sb2 = new StringBuilder();
            PackageManager packageManager = context.getPackageManager();
            List<ResolveInfo> queryIntentActivities = packageManager.queryIntentActivities(new Intent("android.intent.action.VIEW", Uri.parse("geo:0,0?q=donuts")), 65536);
            if (queryIntentActivities == null || queryIntentActivities.size() == 0) {
                sb2.append("m");
            }
            List<ResolveInfo> queryIntentActivities2 = packageManager.queryIntentActivities(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pname:com.admob")), 65536);
            if (queryIntentActivities2 == null || queryIntentActivities2.size() == 0) {
                if (sb2.length() > 0) {
                    sb2.append(",");
                }
                sb2.append("a");
            }
            List<ResolveInfo> queryIntentActivities3 = packageManager.queryIntentActivities(new Intent("android.intent.action.VIEW", Uri.parse("tel://6509313940")), 65536);
            if (queryIntentActivities3 == null || queryIntentActivities3.size() == 0) {
                if (sb2.length() > 0) {
                    sb2.append(",");
                }
                sb2.append("t");
            }
            d = sb2.toString();
        }
        String str3 = d;
        if (str3 != null && str3.length() > 0) {
            a(sb, "ic", str3);
        }
        int i2 = b + 1;
        b = i2;
        if (i2 == 1) {
            c = System.currentTimeMillis();
            a(sb, "pub_data[identifier]", AdManager.getApplicationPackageName(context));
            a(sb, "pub_data[version]", String.valueOf(AdManager.getApplicationVersion(context)));
        } else {
            a(sb, "stats[reqs]", String.valueOf(b));
            a(sb, "stats[time]", String.valueOf((System.currentTimeMillis() - c) / 1000));
        }
        return sb.toString();
    }

    private static void a(StringBuilder sb, String str, String str2) {
        if (str2 != null && str2.length() > 0) {
            try {
                sb.append("&").append(URLEncoder.encode(str, "UTF-8")).append("=").append(URLEncoder.encode(str2, "UTF-8"));
                if (Log.isLoggable(AdManager.LOG, 3)) {
                    Log.d(AdManager.LOG, "    " + str + ": " + str2);
                }
            } catch (UnsupportedEncodingException e) {
                Log.e(AdManager.LOG, "UTF-8 encoding is not supported on this device.  Ad requests are impossible.", e);
            }
        }
    }

    static void a(String str) {
        String str2;
        if (str == null) {
            str2 = "http://r.admob.com/ad_source.php";
        } else {
            str2 = str;
        }
        if (!"http://r.admob.com/ad_source.php".equals(str2)) {
            Log.w(AdManager.LOG, "NOT USING PRODUCTION AD SERVER!  Using " + str2);
        }
        f45a = str2;
    }

    static String a() {
        return f45a;
    }
}
