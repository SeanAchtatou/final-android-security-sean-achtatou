package com.admob.android.ads;

import android.content.Context;
import android.util.Log;
import java.lang.ref.WeakReference;

/* compiled from: AdView */
final class a extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private WeakReference<AdView> f21a;

    public a(AdView adView) {
        this.f21a = new WeakReference<>(adView);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.admob.android.ads.AdView.a(com.admob.android.ads.AdView, boolean):boolean
     arg types: [com.admob.android.ads.AdView, int]
     candidates:
      com.admob.android.ads.AdView.a(com.admob.android.ads.AdView, com.admob.android.ads.d):void
      com.admob.android.ads.AdView.a(com.admob.android.ads.AdView, com.admob.android.ads.g):void
      com.admob.android.ads.AdView.a(com.admob.android.ads.d, com.admob.android.ads.g):void
      com.admob.android.ads.AdView.a(com.admob.android.ads.AdView, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.admob.android.ads.AdView.b(com.admob.android.ads.AdView, boolean):void
     arg types: [com.admob.android.ads.AdView, int]
     candidates:
      com.admob.android.ads.AdView.b(com.admob.android.ads.AdView, com.admob.android.ads.g):void
      com.admob.android.ads.AdView.b(com.admob.android.ads.AdView, boolean):void */
    public final void run() {
        AdView adView = this.f21a.get();
        if (adView != null) {
            try {
                Context context = adView.getContext();
                g gVar = new g(null, context, adView);
                int measuredWidth = (int) (((float) adView.getMeasuredWidth()) / g.c());
                if (((float) measuredWidth) <= 310.0f) {
                    if (Log.isLoggable(AdManager.LOG, 3)) {
                        Log.d(AdManager.LOG, "Ignoring request because we need to have a minimum width of 320 device independent pixels to show an ad.");
                    }
                    AdView.c(adView);
                } else if (u.a(AdView.d(adView), context, adView.j, adView.k, adView.getPrimaryTextColor(), adView.getSecondaryTextColor(), adView.getBackgroundColor(), gVar, measuredWidth) == null) {
                    AdView.c(adView);
                }
            } catch (Exception e) {
                Log.e(AdManager.LOG, "Unhandled exception requesting a fresh ad.", e);
                AdView.c(adView);
            } finally {
                boolean unused = adView.o = false;
                adView.a(true);
            }
        }
    }
}
