package com.lu.ipd_2t;

import android.widget.ImageView;

public class Cell {
    public boolean isHinted = false;
    public boolean isMovable = false;
    public boolean isSetIV = false;
    public ImageView m_iv = null;

    public void setM_iv(ImageView iv) {
        this.m_iv = iv;
        this.isMovable = true;
        this.isSetIV = true;
    }
}
