package com.lu.ipd_2t;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int bg480x120 = 2130837504;
        public static final int bg800x180 = 2130837505;
        public static final int black_hint_30 = 2130837506;
        public static final int black_hint_38 = 2130837507;
        public static final int empty = 2130837508;
        public static final int green_black22 = 2130837509;
        public static final int green_black39 = 2130837510;
        public static final int green_hint_black20 = 2130837511;
        public static final int green_hint_white20 = 2130837512;
        public static final int green_white22 = 2130837513;
        public static final int green_white39 = 2130837514;
        public static final int h22_w1 = 2130837515;
        public static final int h25_w1 = 2130837516;
        public static final int h26_w1 = 2130837517;
        public static final int h28_w1 = 2130837518;
        public static final int h2_w1 = 2130837519;
        public static final int h36_w1 = 2130837520;
        public static final int h39_w1 = 2130837521;
        public static final int h49_w1 = 2130837522;
        public static final int h4_w1 = 2130837523;
        public static final int h7_w1 = 2130837524;
        public static final int icon = 2130837525;
        public static final int o1 = 2130837526;
        public static final int o2 = 2130837527;
        public static final int w18_h1 = 2130837528;
        public static final int w20_h1 = 2130837529;
        public static final int w22_h1 = 2130837530;
        public static final int w25_h1 = 2130837531;
        public static final int w26_h1 = 2130837532;
        public static final int w28_h1 = 2130837533;
        public static final int w2_h1 = 2130837534;
        public static final int w36_h1 = 2130837535;
        public static final int w39_h1 = 2130837536;
        public static final int w49_h1 = 2130837537;
        public static final int w4_h1 = 2130837538;
        public static final int w7_h1 = 2130837539;
        public static final int white_hint_30 = 2130837540;
        public static final int white_hint_38 = 2130837541;
    }

    public static final class id {
        public static final int adView = 2131099717;
        public static final int any = 2131099713;
        public static final int c010 = 2131099689;
        public static final int c02 = 2131099683;
        public static final int c03 = 2131099684;
        public static final int c04 = 2131099685;
        public static final int c08 = 2131099687;
        public static final int c09 = 2131099688;
        public static final int c11 = 2131099691;
        public static final int c110 = 2131099699;
        public static final int c111 = 2131099700;
        public static final int c12 = 2131099692;
        public static final int c13 = 2131099693;
        public static final int c14 = 2131099694;
        public static final int c15 = 2131099695;
        public static final int c17 = 2131099696;
        public static final int c18 = 2131099697;
        public static final int c19 = 2131099698;
        public static final int c20 = 2131099721;
        public static final int c21 = 2131099701;
        public static final int c210 = 2131099710;
        public static final int c211 = 2131099711;
        public static final int c212 = 2131099722;
        public static final int c22 = 2131099702;
        public static final int c23 = 2131099703;
        public static final int c24 = 2131099704;
        public static final int c25 = 2131099705;
        public static final int c26 = 2131099706;
        public static final int c27 = 2131099707;
        public static final int c28 = 2131099708;
        public static final int c29 = 2131099709;
        public static final int c_c0 = 2131099657;
        public static final int c_c1 = 2131099719;
        public static final int c_c10 = 2131099675;
        public static final int c_c11 = 2131099677;
        public static final int c_c12 = 2131099679;
        public static final int c_c13 = 2131099681;
        public static final int c_c2 = 2131099659;
        public static final int c_c3 = 2131099661;
        public static final int c_c4 = 2131099663;
        public static final int c_c5 = 2131099665;
        public static final int c_c6 = 2131099667;
        public static final int c_c7 = 2131099669;
        public static final int c_c8 = 2131099671;
        public static final int c_c9 = 2131099673;
        public static final int col0 = 2131099718;
        public static final int col1 = 2131099658;
        public static final int col10 = 2131099676;
        public static final int col11 = 2131099678;
        public static final int col12 = 2131099720;
        public static final int col13 = 2131099680;
        public static final int col2 = 2131099660;
        public static final int col3 = 2131099662;
        public static final int col4 = 2131099664;
        public static final int col5 = 2131099666;
        public static final int col6 = 2131099668;
        public static final int col7 = 2131099670;
        public static final int col8 = 2131099672;
        public static final int col9 = 2131099674;
        public static final int corner0 = 2131099649;
        public static final int help = 2131099716;
        public static final int layout_game = 2131099648;
        public static final int move_mesg = 2131099686;
        public static final int p2 = 2131099714;
        public static final int play_computer = 2131099712;
        public static final int r_r0 = 2131099650;
        public static final int r_r1 = 2131099652;
        public static final int r_r2 = 2131099654;
        public static final int r_r3 = 2131099656;
        public static final int restart = 2131099682;
        public static final int row0 = 2131099651;
        public static final int row1 = 2131099653;
        public static final int row2 = 2131099655;
        public static final int two_players = 2131099715;
        public static final int undo = 2131099690;
    }

    public static final class layout {
        public static final int main_body320 = 2130903040;
        public static final int main_body480 = 2130903041;
    }

    public static final class string {
        public static final int app_name = 2130968577;
        public static final int hello = 2130968576;
    }

    public static final class style {
        public static final int black_x_320 = 2131034116;
        public static final int black_x_480 = 2131034123;
        public static final int cx_320 = 2131034113;
        public static final int cx_480 = 2131034120;
        public static final int emptyx_320 = 2131034112;
        public static final int emptyx_480 = 2131034119;
        public static final int gap_cx_320 = 2131034118;
        public static final int gap_cx_480 = 2131034125;
        public static final int gap_rx_320 = 2131034117;
        public static final int gap_rx_480 = 2131034124;
        public static final int rx_320 = 2131034114;
        public static final int rx_480 = 2131034121;
        public static final int white_x_320 = 2131034115;
        public static final int white_x_480 = 2131034122;
    }
}
