package com.lu.ipd_2t;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class PlayMaker {
    public static int MAX_PIECES = 200;
    public int IMG_BLACK_HINT_ID;
    public int IMG_BLACK_ID;
    public int IMG_EMPTY_ID;
    public int IMG_WHITE_HINT_ID;
    public int IMG_WHITE_ID;
    int[][] PHONE_FIRST_MOVE_2 = {new int[]{3, 1}, new int[]{4, 2}};
    int[][] PHONE_FIRST_MOVE_any = {new int[]{4, 11}};
    int[][] P_13579_2;
    int[][] P_13579_ANY;
    public Cell[][] m_board_cell;
    public boolean m_computer_win;
    int[][] m_good_state;
    int m_group_id;
    int m_how_to_step;
    int m_how_to_x;
    public boolean m_isGameOver_1;
    public boolean m_isGameOver_2;
    int m_max_cols;
    protected int m_max_pieces;
    int m_max_rows;
    String m_move_string;
    public String m_next_move;
    Random m_randomGenerator;
    String[] m_save_left_move_row;
    int m_save_left_move_row_idx;
    String[] m_save_right_move_row;
    int m_save_right_move_row_idx;
    String m_which_move_first;
    int m_which_set;
    int move_c;
    int move_r;

    public PlayMaker() {
        int[] iArr = new int[5];
        iArr[3] = 1;
        iArr[4] = 1;
        int[] iArr2 = new int[5];
        iArr2[3] = 2;
        iArr2[4] = 2;
        int[] iArr3 = new int[5];
        iArr3[3] = 3;
        iArr3[4] = 3;
        int[] iArr4 = new int[5];
        iArr4[2] = 1;
        iArr4[3] = 2;
        iArr4[4] = 3;
        int[] iArr5 = new int[5];
        iArr5[1] = 1;
        iArr5[2] = 1;
        iArr5[3] = 1;
        iArr5[4] = 1;
        int[] iArr6 = new int[5];
        iArr6[1] = 1;
        iArr6[2] = 1;
        iArr6[3] = 2;
        iArr6[4] = 2;
        int[] iArr7 = new int[5];
        iArr7[1] = 1;
        iArr7[2] = 1;
        iArr7[3] = 3;
        iArr7[4] = 3;
        this.P_13579_ANY = new int[][]{new int[5], iArr, iArr2, iArr3, iArr4, iArr5, iArr6, iArr7, new int[]{1, 1, 1, 2, 3}};
        int[] iArr8 = new int[5];
        iArr8[4] = 3;
        int[] iArr9 = new int[5];
        iArr9[4] = 6;
        int[] iArr10 = new int[5];
        iArr10[4] = 9;
        int[] iArr11 = new int[5];
        iArr11[3] = 1;
        iArr11[4] = 1;
        int[] iArr12 = new int[5];
        iArr12[3] = 1;
        iArr12[4] = 4;
        int[] iArr13 = new int[5];
        iArr13[3] = 1;
        iArr13[4] = 7;
        int[] iArr14 = new int[5];
        iArr14[3] = 1;
        iArr14[4] = 10;
        int[] iArr15 = new int[5];
        iArr15[3] = 2;
        iArr15[4] = 2;
        int[] iArr16 = new int[5];
        iArr16[3] = 2;
        iArr16[4] = 5;
        int[] iArr17 = new int[5];
        iArr17[3] = 2;
        iArr17[4] = 8;
        int[] iArr18 = new int[5];
        iArr18[3] = 2;
        iArr18[4] = 11;
        int[] iArr19 = new int[5];
        iArr19[3] = 3;
        iArr19[4] = 3;
        int[] iArr20 = new int[5];
        iArr20[3] = 3;
        iArr20[4] = 6;
        int[] iArr21 = new int[5];
        iArr21[3] = 3;
        iArr21[4] = 9;
        int[] iArr22 = new int[5];
        iArr22[2] = 1;
        iArr22[3] = 1;
        iArr22[4] = 3;
        int[] iArr23 = new int[5];
        iArr23[2] = 1;
        iArr23[3] = 1;
        iArr23[4] = 6;
        int[] iArr24 = new int[5];
        iArr24[2] = 1;
        iArr24[3] = 1;
        iArr24[4] = 9;
        int[] iArr25 = new int[5];
        iArr25[2] = 1;
        iArr25[3] = 3;
        iArr25[4] = 4;
        int[] iArr26 = new int[5];
        iArr26[2] = 1;
        iArr26[3] = 3;
        iArr26[4] = 7;
        int[] iArr27 = new int[5];
        iArr27[2] = 1;
        iArr27[3] = 3;
        iArr27[4] = 10;
        int[] iArr28 = new int[5];
        iArr28[2] = 2;
        iArr28[3] = 2;
        iArr28[4] = 3;
        int[] iArr29 = new int[5];
        iArr29[2] = 2;
        iArr29[3] = 2;
        iArr29[4] = 6;
        int[] iArr30 = new int[5];
        iArr30[2] = 2;
        iArr30[3] = 2;
        iArr30[4] = 9;
        int[] iArr31 = new int[5];
        iArr31[2] = 2;
        iArr31[3] = 3;
        iArr31[4] = 5;
        int[] iArr32 = new int[5];
        iArr32[2] = 2;
        iArr32[3] = 3;
        iArr32[4] = 8;
        int[] iArr33 = new int[5];
        iArr33[2] = 2;
        iArr33[3] = 3;
        iArr33[4] = 11;
        int[] iArr34 = new int[5];
        iArr34[2] = 3;
        iArr34[3] = 3;
        iArr34[4] = 3;
        int[] iArr35 = new int[5];
        iArr35[2] = 3;
        iArr35[3] = 3;
        iArr35[4] = 6;
        int[] iArr36 = new int[5];
        iArr36[2] = 3;
        iArr36[3] = 3;
        iArr36[4] = 9;
        int[] iArr37 = new int[5];
        iArr37[1] = 1;
        iArr37[2] = 1;
        iArr37[3] = 1;
        iArr37[4] = 1;
        int[] iArr38 = new int[5];
        iArr38[1] = 1;
        iArr38[2] = 1;
        iArr38[3] = 1;
        iArr38[4] = 4;
        int[] iArr39 = new int[5];
        iArr39[1] = 1;
        iArr39[2] = 1;
        iArr39[3] = 1;
        iArr39[4] = 7;
        int[] iArr40 = new int[5];
        iArr40[1] = 1;
        iArr40[2] = 1;
        iArr40[3] = 1;
        iArr40[4] = 10;
        int[] iArr41 = new int[5];
        iArr41[1] = 1;
        iArr41[2] = 1;
        iArr41[3] = 2;
        iArr41[4] = 2;
        int[] iArr42 = new int[5];
        iArr42[1] = 1;
        iArr42[2] = 1;
        iArr42[3] = 2;
        iArr42[4] = 5;
        int[] iArr43 = new int[5];
        iArr43[1] = 1;
        iArr43[2] = 1;
        iArr43[3] = 2;
        iArr43[4] = 8;
        int[] iArr44 = new int[5];
        iArr44[1] = 1;
        iArr44[2] = 1;
        iArr44[3] = 2;
        iArr44[4] = 11;
        int[] iArr45 = new int[5];
        iArr45[1] = 1;
        iArr45[2] = 1;
        iArr45[3] = 3;
        iArr45[4] = 3;
        int[] iArr46 = new int[5];
        iArr46[1] = 1;
        iArr46[2] = 1;
        iArr46[3] = 3;
        iArr46[4] = 6;
        int[] iArr47 = new int[5];
        iArr47[1] = 1;
        iArr47[2] = 1;
        iArr47[3] = 3;
        iArr47[4] = 9;
        int[] iArr48 = new int[5];
        iArr48[1] = 1;
        iArr48[2] = 2;
        iArr48[3] = 2;
        iArr48[4] = 4;
        int[] iArr49 = new int[5];
        iArr49[1] = 1;
        iArr49[2] = 2;
        iArr49[3] = 2;
        iArr49[4] = 7;
        int[] iArr50 = new int[5];
        iArr50[1] = 1;
        iArr50[2] = 2;
        iArr50[3] = 2;
        iArr50[4] = 10;
        int[] iArr51 = new int[5];
        iArr51[1] = 1;
        iArr51[2] = 3;
        iArr51[3] = 3;
        iArr51[4] = 4;
        int[] iArr52 = new int[5];
        iArr52[1] = 1;
        iArr52[2] = 3;
        iArr52[3] = 3;
        iArr52[4] = 7;
        int[] iArr53 = new int[5];
        iArr53[1] = 1;
        iArr53[2] = 3;
        iArr53[3] = 3;
        iArr53[4] = 10;
        this.P_13579_2 = new int[][]{new int[5], iArr8, iArr9, iArr10, iArr11, iArr12, iArr13, iArr14, iArr15, iArr16, iArr17, iArr18, iArr19, iArr20, iArr21, iArr22, iArr23, iArr24, iArr25, iArr26, iArr27, iArr28, iArr29, iArr30, iArr31, iArr32, iArr33, iArr34, iArr35, iArr36, iArr37, iArr38, iArr39, iArr40, iArr41, iArr42, iArr43, iArr44, iArr45, iArr46, iArr47, iArr48, iArr49, iArr50, iArr51, iArr52, iArr53, new int[]{1, 1, 1, 1, 3}, new int[]{1, 1, 1, 1, 6}, new int[]{1, 1, 1, 1, 9}, new int[]{1, 1, 1, 3, 4}, new int[]{1, 1, 1, 3, 7}, new int[]{1, 1, 1, 3, 10}, new int[]{1, 1, 2, 2, 3}, new int[]{1, 1, 2, 2, 6}, new int[]{1, 1, 2, 2, 9}, new int[]{1, 1, 2, 3, 5}, new int[]{1, 1, 2, 3, 8}, new int[]{1, 1, 2, 3, 11}, new int[]{1, 1, 3, 3, 3}, new int[]{1, 1, 3, 3, 6}, new int[]{1, 1, 3, 3, 9}};
        this.m_randomGenerator = new Random();
        this.m_which_move_first = "I";
        this.m_max_cols = 20;
        this.m_max_rows = 3;
        this.m_save_left_move_row = new String[20];
        this.m_save_right_move_row = new String[20];
        this.m_next_move = "LEFT";
        this.m_which_set = 0;
        this.m_good_state = null;
        reset();
    }

    public void reset() {
        this.m_next_move = null;
        if (this.m_board_cell == null) {
            this.m_board_cell = (Cell[][]) Array.newInstance(Cell.class, 8, 20);
            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 20; j++) {
                    this.m_board_cell[i][j] = new Cell();
                }
            }
            return;
        }
        for (int i2 = 0; i2 < 8; i2++) {
            for (int j2 = 0; j2 < 20; j2++) {
                this.m_board_cell[i2][j2].isMovable = false;
            }
        }
    }

    public void setGoodState_array(int which) {
        if (this.m_which_set != which || this.m_good_state == null) {
            this.m_which_set = which;
            switch (which) {
                case 1:
                    this.m_good_state = this.P_13579_2;
                    break;
                default:
                    this.m_good_state = this.P_13579_ANY;
                    break;
            }
            if (this.m_max_pieces == MAX_PIECES) {
                for (int i = 0; i < 8; i++) {
                    for (int j = 0; j < 20; j++) {
                        this.m_board_cell[i][j].isHinted = true;
                    }
                }
                return;
            }
            for (int i2 = 0; i2 < 8; i2++) {
                for (int j2 = 0; j2 < 20; j2++) {
                    this.m_board_cell[i2][j2].isHinted = false;
                }
            }
        }
    }

    public int make_a_play(int r, int c) {
        int k1;
        int k2;
        int k12;
        int k22;
        int cnt = 0;
        this.m_isGameOver_2 = false;
        this.m_isGameOver_1 = false;
        this.m_save_right_move_row_idx = -1;
        this.m_save_left_move_row_idx = -1;
        remove_all_hint();
        if ("LEFT".equals(this.m_next_move)) {
            save_left_move(r);
            if (r >= 2) {
                k12 = 0;
                k22 = c;
            } else if (c < 5) {
                k12 = 0;
                k22 = c;
            } else {
                k12 = 6;
                k22 = c;
            }
            for (int i = k12; i <= k22; i++) {
                if (!this.m_board_cell[r][i].isMovable) {
                    if (cnt > 0) {
                        break;
                    }
                } else {
                    this.m_board_cell[r][i].m_iv.setImageResource(this.IMG_WHITE_ID);
                    this.m_board_cell[r][i].isMovable = false;
                    cnt++;
                }
            }
            this.m_move_string = "Left: row " + (r + 1) + ", # grid " + cnt;
            this.m_next_move = "RIGHT";
            mark_move_hint();
            if (!isGameOver()) {
                return 3;
            }
            this.m_isGameOver_1 = true;
            return 1;
        }
        save_right_move(r);
        if (r >= 2) {
            k1 = c;
            k2 = this.m_max_cols;
        } else if (c < 5) {
            k1 = c;
            k2 = 5;
        } else {
            k1 = c;
            k2 = this.m_max_cols;
        }
        for (int i2 = k1; i2 < k2; i2++) {
            if (!this.m_board_cell[r][i2].isMovable) {
                if (cnt > 0) {
                    break;
                }
            } else {
                this.m_board_cell[r][i2].m_iv.setImageResource(this.IMG_BLACK_ID);
                this.m_board_cell[r][i2].isMovable = false;
                cnt++;
            }
        }
        this.m_move_string = "Right: row " + (r + 1) + ", # grid " + cnt;
        this.m_next_move = "LEFT";
        mark_move_hint();
        if (!isGameOver()) {
            return 3;
        }
        this.m_isGameOver_2 = true;
        return 2;
    }

    public int make_play(int r, int c) {
        int k1;
        int k2;
        int k12;
        int k22;
        int k13;
        int k23;
        int k14;
        int k24;
        this.m_isGameOver_2 = false;
        this.m_isGameOver_1 = false;
        this.m_save_right_move_row_idx = -1;
        this.m_save_left_move_row_idx = -1;
        save_left_move(r);
        int cnt = 0;
        if ("I".equals(this.m_which_move_first)) {
            if (r >= 2) {
                k14 = 0;
                k24 = c;
            } else if (c < 5) {
                k14 = 0;
                k24 = c;
            } else {
                k14 = 6;
                k24 = c;
            }
            for (int i = k14; i <= k24; i++) {
                if (this.m_board_cell[r][i].isMovable) {
                    this.m_board_cell[r][i].m_iv.setImageResource(this.IMG_WHITE_ID);
                    this.m_board_cell[r][i].isMovable = false;
                    cnt++;
                }
            }
            this.m_move_string = "Left: row " + (r + 1) + ", # grid " + cnt;
        } else {
            if (r >= 2) {
                k1 = c;
                k2 = this.m_max_cols;
            } else if (c < 5) {
                k1 = c;
                k2 = 5;
            } else {
                k1 = c;
                k2 = this.m_max_cols;
            }
            for (int i2 = k1; i2 < k2; i2++) {
                if (this.m_board_cell[r][i2].isMovable) {
                    this.m_board_cell[r][i2].m_iv.setImageResource(this.IMG_BLACK_ID);
                    this.m_board_cell[r][i2].isMovable = false;
                    cnt++;
                }
            }
            this.m_move_string = "Right: row " + (r + 1) + ", # grid " + cnt;
        }
        if (isGameOver()) {
            this.m_computer_win = false;
            this.m_isGameOver_1 = true;
            return 1;
        }
        if ("I".equals(this.m_which_move_first)) {
            this.move_c = -1;
            this.move_r = -1;
            int right_c = find_best_move('R', false);
            if (right_c < 0) {
                return -1;
            }
            if (this.move_r >= 2) {
                k13 = right_c;
                k23 = this.m_max_cols;
            } else if (right_c < 5) {
                k13 = right_c;
                k23 = 5;
            } else {
                k13 = right_c;
                k23 = this.m_max_cols;
            }
            if (this.m_save_left_move_row_idx != this.move_r) {
                save_right_move(this.move_r);
            }
            int cnt2 = 0;
            for (int i3 = k13; i3 < k23; i3++) {
                if (this.m_board_cell[this.move_r][i3].isMovable) {
                    this.m_board_cell[this.move_r][i3].m_iv.setImageResource(this.IMG_BLACK_ID);
                    this.m_board_cell[this.move_r][i3].isMovable = false;
                    cnt2++;
                }
            }
            this.m_move_string = "Right: row " + (this.move_r + 1) + ", # grid " + cnt2;
        } else {
            this.move_c = -1;
            this.move_r = -1;
            int right_c2 = find_best_move('L', false);
            if (right_c2 < 0) {
                return -1;
            }
            if (this.move_r >= 2) {
                k12 = 0;
                k22 = right_c2;
            } else if (right_c2 < 5) {
                k12 = 0;
                k22 = right_c2;
            } else {
                k12 = 5;
                k22 = right_c2;
            }
            if (this.m_save_left_move_row_idx != this.move_r) {
                save_right_move(this.move_r);
            }
            int cnt3 = 0;
            for (int i4 = k12; i4 <= k22; i4++) {
                if (this.m_board_cell[this.move_r][i4].isMovable) {
                    this.m_board_cell[this.move_r][i4].m_iv.setImageResource(this.IMG_WHITE_ID);
                    this.m_board_cell[this.move_r][i4].isMovable = false;
                    cnt3++;
                }
            }
            this.m_move_string = "Left: row " + (this.move_r + 1) + ", # grid " + cnt3;
        }
        if (isGameOver()) {
            this.m_computer_win = true;
            this.m_isGameOver_2 = true;
            return 2;
        }
        mark_move_hint();
        return 3;
    }

    public int how_to_move(int[] good_st, int[] curr_st) {
        this.m_how_to_x = -1;
        this.m_how_to_step = -1;
        int[] tmp_good = new int[good_st.length];
        int[] tmp_curr = new int[curr_st.length];
        for (int i = 0; i < curr_st.length; i++) {
            tmp_good[i] = good_st[i];
            tmp_curr[i] = curr_st[i];
        }
        int same_cnt = 0;
        for (int i2 = 0; i2 < curr_st.length; i2++) {
            int j = 0;
            while (true) {
                if (j >= tmp_good.length) {
                    break;
                } else if (curr_st[i2] == tmp_good[j]) {
                    tmp_good[j] = -1;
                    tmp_curr[i2] = -1;
                    same_cnt++;
                    break;
                } else {
                    j++;
                }
            }
        }
        if (same_cnt == curr_st.length) {
            this.m_how_to_step = 0;
            this.m_how_to_x = -1;
            return 0;
        } else if (same_cnt + 1 != curr_st.length) {
            return -1;
        } else {
            int d1 = 0;
            int d2 = 0;
            int i3 = 0;
            while (true) {
                if (i3 < curr_st.length) {
                    if (tmp_good[i3] >= 0) {
                        d1 = tmp_good[i3];
                        break;
                    }
                    i3++;
                } else {
                    break;
                }
            }
            int i4 = 0;
            while (true) {
                if (i4 < curr_st.length) {
                    if (tmp_curr[i4] >= 0) {
                        d2 = tmp_curr[i4];
                        break;
                    }
                    i4++;
                } else {
                    break;
                }
            }
            if (d2 - d1 > this.m_max_pieces) {
                return -2;
            }
            this.m_how_to_step = d2 - d1;
            this.m_how_to_x = i4;
            return i4;
        }
    }

    public int find_best_move(char which_move, boolean first_move) {
        int k;
        int k2;
        Row_inf[] row_infArr = new Row_inf[20];
        int ttl_rows = 0;
        int max_cnt = -1;
        ArrayList<Row_inf> ary = new ArrayList<>();
        int k3 = 0;
        for (int i = 0; i < 2; i++) {
            int r_cnt = 0;
            int first_c = -1;
            int last_c = -1;
            for (int j = 0; j < 5; j++) {
                if (this.m_board_cell[i][j].isMovable) {
                    if (first_c < 0) {
                        first_c = j;
                    }
                    r_cnt++;
                    last_c = j;
                }
            }
            if (r_cnt == 0) {
                ary.add(new Row_inf(i, 0, 0, 0, 1));
                k3++;
            } else {
                if (r_cnt > max_cnt) {
                    max_cnt = r_cnt;
                }
                k3++;
                ary.add(new Row_inf(i, r_cnt, first_c, last_c, 1));
                ttl_rows++;
            }
        }
        for (int i2 = 0; i2 < 2; i2++) {
            int r_cnt2 = 0;
            int first_c2 = -1;
            int last_c2 = -1;
            for (int j2 = 6; j2 < 11; j2++) {
                if (this.m_board_cell[i2][j2].isMovable) {
                    if (first_c2 < 0) {
                        first_c2 = j2;
                    }
                    r_cnt2++;
                    last_c2 = j2;
                }
            }
            if (r_cnt2 == 0) {
                k2 = k3 + 1;
                ary.add(new Row_inf(i2, 0, 0, 0, 2));
            } else {
                if (r_cnt2 > max_cnt) {
                    max_cnt = r_cnt2;
                }
                k2 = k3 + 1;
                ary.add(new Row_inf(i2, r_cnt2, first_c2, last_c2, 2));
                ttl_rows++;
            }
        }
        for (int i3 = 2; i3 < 3; i3++) {
            int r_cnt3 = 0;
            int first_c3 = -1;
            int last_c3 = -1;
            for (int j3 = 1; j3 < 12; j3++) {
                if (this.m_board_cell[i3][j3].isMovable) {
                    if (first_c3 < 0) {
                        first_c3 = j3;
                    }
                    r_cnt3++;
                    last_c3 = j3;
                }
            }
            if (r_cnt3 == 0) {
                k = k3 + 1;
                ary.add(new Row_inf(i3, 0, 0, 0, 3));
            } else {
                if (r_cnt3 > max_cnt) {
                    max_cnt = r_cnt3;
                }
                k = k3 + 1;
                ary.add(new Row_inf(i3, r_cnt3, first_c3, last_c3, 3));
                ttl_rows++;
            }
        }
        if (ttl_rows == 0) {
            return -1;
        }
        if (this.m_max_pieces == MAX_PIECES && ttl_rows == 1) {
            for (int i4 = 0; i4 < ary.size(); i4++) {
                Row_inf v = (Row_inf) ary.get(i4);
                if (v.remain_num > 0) {
                    this.move_r = v.row_idx;
                    if (v.remain_num <= 1) {
                        this.move_c = v.first_col;
                    } else if (which_move == 'R') {
                        this.move_c = v.first_col;
                    } else {
                        this.move_c = v.last_col;
                    }
                    return this.move_c;
                }
            }
            return -2;
        }
        Row_inf[] remain_ary = new Row_inf[ary.size()];
        ary.toArray(remain_ary);
        int[] curr_st = new int[remain_ary.length];
        Arrays.sort(remain_ary);
        for (int i5 = 0; i5 < remain_ary.length; i5++) {
            curr_st[i5] = remain_ary[i5].remain_num;
        }
        int not_same_x = -1;
        int steps = 0;
        for (int i6 = 0; i6 < this.m_good_state.length; i6++) {
            not_same_x = how_to_move(this.m_good_state[i6], curr_st);
            steps = this.m_how_to_step;
            if ((not_same_x <= 0 && this.m_how_to_step == 0) || not_same_x >= 0) {
                break;
            }
        }
        this.m_group_id = -1;
        if (not_same_x < 0 || steps <= 0) {
            int k4 = remain_ary.length - 1;
            this.move_r = remain_ary[k4].row_idx;
            if (remain_ary[k4].remain_num <= 1) {
                this.move_c = remain_ary[k4].first_col;
            } else if (which_move == 'R') {
                this.move_c = remain_ary[k4].last_col;
            } else {
                this.move_c = remain_ary[k4].first_col;
            }
            return this.move_c;
        }
        this.m_group_id = remain_ary[not_same_x].group_id;
        this.move_r = remain_ary[not_same_x].row_idx;
        if (remain_ary[not_same_x].remain_num <= 1) {
            this.move_c = remain_ary[not_same_x].first_col;
        } else if (which_move == 'R') {
            this.move_c = (remain_ary[not_same_x].last_col - steps) + 1;
        } else {
            this.move_c = (remain_ary[not_same_x].first_col + steps) - 1;
        }
        return this.move_c;
    }

    public boolean isGameOver() {
        for (int i = 0; i < this.m_max_rows; i++) {
            for (int j = 0; j < this.m_max_cols; j++) {
                if (this.m_board_cell[i][j].isMovable) {
                    return false;
                }
            }
        }
        return true;
    }

    public void save_left_move(int row) {
        this.m_save_left_move_row_idx = row;
        for (int i = 0; i < this.m_max_cols; i++) {
            if (this.m_board_cell[row][i].m_iv == null) {
                this.m_save_left_move_row[i] = "X";
            } else if (this.m_board_cell[row][i].isMovable) {
                this.m_save_left_move_row[i] = "1";
            } else {
                this.m_save_left_move_row[i] = "0";
            }
        }
    }

    public void save_right_move(int row) {
        this.m_save_right_move_row_idx = row;
        for (int i = 0; i < this.m_max_cols; i++) {
            if (this.m_board_cell[row][i].m_iv == null) {
                this.m_save_right_move_row[i] = "X";
            } else if (this.m_board_cell[row][i].isMovable) {
                this.m_save_right_move_row[i] = "1";
            } else {
                this.m_save_right_move_row[i] = "0";
            }
        }
    }

    public String undo_a_play() {
        remove_all_hint();
        if (this.m_next_move.equals("LEFT")) {
            if (this.m_save_right_move_row_idx >= 0) {
                for (int i = 0; i < this.m_max_cols; i++) {
                    if ("1".equals(this.m_save_right_move_row[i])) {
                        this.m_board_cell[this.m_save_right_move_row_idx][i].m_iv.setImageResource(this.IMG_EMPTY_ID);
                        this.m_board_cell[this.m_save_right_move_row_idx][i].isMovable = true;
                    }
                }
            }
            this.m_next_move = "RIGHT";
            mark_move_hint();
        } else {
            if (this.m_save_left_move_row_idx >= 0) {
                for (int i2 = 0; i2 < this.m_max_cols; i2++) {
                    if ("1".equals(this.m_save_left_move_row[i2])) {
                        this.m_board_cell[this.m_save_left_move_row_idx][i2].m_iv.setImageResource(this.IMG_EMPTY_ID);
                        this.m_board_cell[this.m_save_left_move_row_idx][i2].isMovable = true;
                    }
                }
            }
            this.m_next_move = "LEFT";
            mark_move_hint();
        }
        return this.m_next_move;
    }

    public void undo() {
        if (this.m_save_left_move_row_idx >= 0) {
            for (int i = 0; i < this.m_max_cols; i++) {
                if ("1".equals(this.m_save_left_move_row[i])) {
                    this.m_board_cell[this.m_save_left_move_row_idx][i].m_iv.setImageResource(this.IMG_EMPTY_ID);
                    this.m_board_cell[this.m_save_left_move_row_idx][i].isMovable = true;
                }
            }
        }
        if (this.m_save_left_move_row_idx != this.m_save_right_move_row_idx && this.m_save_right_move_row_idx >= 0) {
            for (int i2 = 0; i2 < this.m_max_cols; i2++) {
                if ("1".equals(this.m_save_right_move_row[i2])) {
                    this.m_board_cell[this.m_save_right_move_row_idx][i2].m_iv.setImageResource(this.IMG_EMPTY_ID);
                    this.m_board_cell[this.m_save_right_move_row_idx][i2].isMovable = true;
                }
            }
        }
        mark_move_hint();
    }

    public void computer_first_move() {
        int k1;
        int k2;
        int c = find_best_move('L', true);
        int cnt = 0;
        if (this.move_r >= 2) {
            k1 = 1;
            k2 = c;
        } else if (c < 5) {
            k1 = 0;
            k2 = c;
        } else {
            k1 = 6;
            k2 = c;
        }
        for (int i = k1; i <= k2; i++) {
            if (this.m_board_cell[this.move_r][i].isMovable) {
                this.m_board_cell[this.move_r][i].m_iv.setImageResource(this.IMG_WHITE_ID);
                this.m_board_cell[this.move_r][i].isMovable = false;
                cnt++;
            }
        }
        this.m_next_move = "RIGHT";
        mark_move_hint();
    }

    public void mark_move_hint() {
        if (this.m_max_pieces < this.m_max_cols) {
            if ("LEFT".equals(this.m_next_move)) {
                _mark_left_move_hint();
            } else {
                _mark_right_move_hint();
            }
        }
    }

    public void _mark_left_move_hint() {
        for (int i = 0; i < 2; i++) {
            int cnt = 0;
            for (int j = 0; j < 5; j++) {
                if (this.m_board_cell[i][j].isMovable) {
                    this.m_board_cell[i][j].isHinted = true;
                    this.m_board_cell[i][j].m_iv.setImageResource(this.IMG_WHITE_HINT_ID);
                    cnt++;
                    if (cnt >= this.m_max_pieces) {
                        break;
                    }
                }
            }
            int cnt2 = 0;
            for (int j2 = 6; j2 < this.m_max_cols; j2++) {
                if (this.m_board_cell[i][j2].isMovable) {
                    this.m_board_cell[i][j2].isHinted = true;
                    this.m_board_cell[i][j2].m_iv.setImageResource(this.IMG_WHITE_HINT_ID);
                    cnt2++;
                    if (cnt2 >= this.m_max_pieces) {
                        break;
                    }
                }
            }
        }
        for (int i2 = 2; i2 < 3; i2++) {
            int cnt3 = 0;
            for (int j3 = 0; j3 < this.m_max_cols; j3++) {
                if (this.m_board_cell[i2][j3].isMovable) {
                    this.m_board_cell[i2][j3].isHinted = true;
                    this.m_board_cell[i2][j3].m_iv.setImageResource(this.IMG_WHITE_HINT_ID);
                    cnt3++;
                    if (cnt3 >= this.m_max_pieces) {
                        break;
                    }
                }
            }
        }
    }

    public void _mark_right_move_hint() {
        for (int i = 0; i < 2; i++) {
            int cnt = 0;
            for (int j = 5; j >= 0; j--) {
                if (this.m_board_cell[i][j].isMovable) {
                    this.m_board_cell[i][j].isHinted = true;
                    this.m_board_cell[i][j].m_iv.setImageResource(this.IMG_BLACK_HINT_ID);
                    cnt++;
                    if (cnt >= this.m_max_pieces) {
                        break;
                    }
                }
            }
            int cnt2 = 0;
            for (int j2 = this.m_max_cols - 1; j2 > 6; j2--) {
                if (this.m_board_cell[i][j2].isMovable) {
                    this.m_board_cell[i][j2].isHinted = true;
                    this.m_board_cell[i][j2].m_iv.setImageResource(this.IMG_BLACK_HINT_ID);
                    cnt2++;
                    if (cnt2 >= this.m_max_pieces) {
                        break;
                    }
                }
            }
        }
        for (int i2 = 2; i2 < 3; i2++) {
            int cnt3 = 0;
            for (int j3 = this.m_max_cols - 1; j3 >= 0; j3--) {
                if (this.m_board_cell[i2][j3].isMovable) {
                    this.m_board_cell[i2][j3].isHinted = true;
                    this.m_board_cell[i2][j3].m_iv.setImageResource(this.IMG_BLACK_HINT_ID);
                    cnt3++;
                    if (cnt3 >= this.m_max_pieces) {
                        break;
                    }
                }
            }
        }
    }

    public void remove_all_hint() {
        if (this.m_max_pieces < this.m_max_cols) {
            for (int i = 0; i < this.m_max_rows; i++) {
                for (int j = 0; j < this.m_max_cols; j++) {
                    if (this.m_board_cell[i][j].isMovable && this.m_board_cell[i][j].isHinted) {
                        this.m_board_cell[i][j].m_iv.setImageResource(this.IMG_EMPTY_ID);
                        this.m_board_cell[i][j].isHinted = false;
                    }
                }
            }
        }
    }
}
