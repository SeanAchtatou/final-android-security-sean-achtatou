package com.lu.ipd_2t;

public class Row_inf implements Comparable {
    public int first_col;
    public int group_id;
    public int last_col;
    public int remain_num;
    public int row_idx;
    public boolean same_num = false;

    public Row_inf(int remain_num2) {
        this.remain_num = remain_num2;
    }

    public Row_inf(int row_idx2, int remain_num2) {
        this.row_idx = row_idx2;
        this.remain_num = remain_num2;
    }

    public Row_inf(int row_idx2, int remain_num2, int first_c, int last_c, int group_id2) {
        this.row_idx = row_idx2;
        this.remain_num = remain_num2;
        this.first_col = first_c;
        this.last_col = last_c;
        this.group_id = group_id2;
    }

    public int compareTo(Object obj) {
        Row_inf tmp = (Row_inf) obj;
        if (this.remain_num < tmp.remain_num) {
            return -1;
        }
        if (this.remain_num > tmp.remain_num) {
            return 1;
        }
        return 0;
    }
}
