package org.ndisk.kakogawastudio.throwing5;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int back = 2130837504;
        public static final int back_2 = 2130837505;
        public static final int back_3 = 2130837506;
        public static final int ball = 2130837507;
        public static final int ball1 = 2130837508;
        public static final int ball2 = 2130837509;
        public static final int ball3 = 2130837510;
        public static final int bang = 2130837511;
        public static final int char1 = 2130837512;
        public static final int char2 = 2130837513;
        public static final int char3 = 2130837514;
        public static final int clear = 2130837515;
        public static final int clear2 = 2130837516;
        public static final int clear3 = 2130837517;
        public static final int enemy1 = 2130837518;
        public static final int enemy2 = 2130837519;
        public static final int enemy3 = 2130837520;
        public static final int icon = 2130837521;
        public static final int over = 2130837522;
        public static final int thicon = 2130837523;
        public static final int timeball1 = 2130837524;
        public static final int timeball2 = 2130837525;
        public static final int timeball3 = 2130837526;
        public static final int timeball4 = 2130837527;
        public static final int title = 2130837528;
    }

    public static final class id {
        public static final int ThrowingView01 = 2131099648;
        public static final int ad = 2131099649;
    }

    public static final class layout {
        public static final int main = 2130903040;
        public static final int splash = 2130903041;
    }

    public static final class raw {
        public static final int bgm = 2130968576;
        public static final int bomb = 2130968577;
        public static final int cool = 2130968578;
        public static final int over = 2130968579;
        public static final int shot = 2130968580;
    }

    public static final class string {
        public static final int app_name = 2131034113;
        public static final int hello = 2131034112;
    }
}
