package org.ndisk.kakogawastudio.throwing5;

import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import java.lang.reflect.Array;
import java.util.Random;

public class Enemies {
    private static final int HIT_DATA = -1;
    private static final int NO_DATA = -5;
    private Drawable bang_img;
    private int col = 5;
    private Enemy[][] datas;
    private float enemy_h = 50.0f;
    private float enemy_w = 50.0f;
    private float enemy_x = 500.0f;
    private float enemy_y = 50.0f;
    private Drawable[] imgs;
    private int row = 5;
    private float space_h = 70.0f;
    private float space_w = 80.0f;

    public Enemies(Drawable[] imgs2, Drawable bang_img2, float x, float y, int col2, int row2) {
        this.imgs = imgs2;
        this.bang_img = bang_img2;
        this.enemy_x = x;
        this.enemy_y = y;
        this.col = col2;
        this.row = row2;
    }

    public void init() {
        int n = 0;
        this.datas = (Enemy[][]) Array.newInstance(Enemy.class, this.col, this.row);
        Random rnd = new Random(SystemClock.currentThreadTimeMillis());
        for (int i = 0; i < this.col; i++) {
            int j = 0;
            while (j < this.row) {
                this.datas[i][j] = new Enemy(n + 1, this.enemy_x + (this.space_w * ((float) i)), this.enemy_y + (this.space_h * ((float) j)), (rnd.nextFloat() * 5.0f) - 2.5f, (rnd.nextFloat() * 5.0f) - 2.5f);
                j++;
                n++;
            }
        }
    }

    public void move() {
        for (int i = 0; i < this.col; i++) {
            for (int j = 0; j < this.row; j++) {
                if (this.datas[i][j].show_img >= 0) {
                    this.datas[i][j].show_img = (this.datas[i][j].show_img + 1) % 4;
                    this.datas[i][j].x += this.datas[i][j].dx;
                    if (this.datas[i][j].x < this.enemy_x - this.enemy_w) {
                        this.datas[i][j].dx *= -1.0f;
                    }
                    if (this.datas[i][j].x > this.enemy_x + (((float) this.col) * this.space_w) + ((float) ((int) (this.enemy_w / 2.0f)))) {
                        this.datas[i][j].dx *= -1.0f;
                    }
                    this.datas[i][j].y += this.datas[i][j].dy;
                    if (this.datas[i][j].y < this.enemy_y - this.enemy_h) {
                        this.datas[i][j].dy *= -1.0f;
                    }
                    if (this.datas[i][j].y > this.enemy_y + (((float) this.row) * this.space_h) + this.enemy_h) {
                        this.datas[i][j].dy *= -1.0f;
                    }
                }
            }
        }
    }

    public int checkHit(Point p) {
        int count = 0;
        for (int i = 0; i < this.col; i++) {
            for (int j = 0; j < this.row; j++) {
                if (this.datas[i][j].show_img >= 0) {
                    float x1 = this.datas[i][j].x;
                    float y1 = this.datas[i][j].y;
                    float x2 = x1 + this.enemy_w;
                    float y2 = y1 + this.enemy_h;
                    if (((float) p.x) > x1 && ((float) p.x) < x2 && ((float) p.y) > y1 && ((float) p.y) < y2) {
                        this.datas[i][j].show_img = HIT_DATA;
                        count++;
                    }
                }
            }
        }
        return count;
    }

    public boolean isFinished() {
        boolean f = true;
        for (int i = 0; i < this.col; i++) {
            for (int j = 0; j < this.row; j++) {
                if (this.datas[i][j].show_img >= 0) {
                    f = false;
                }
            }
        }
        return f;
    }

    public void draw(Canvas canvas) {
        if (this.datas != null) {
            for (int i = 0; i < this.col; i++) {
                for (int j = 0; j < this.row; j++) {
                    int n = this.datas[i][j].show_img;
                    Rect r = new Rect((int) this.datas[i][j].x, (int) this.datas[i][j].y, (int) (this.datas[i][j].x + this.enemy_w), (int) (this.datas[i][j].y + this.enemy_h));
                    if (n >= 0) {
                        this.imgs[n].setBounds(r);
                        this.imgs[n].draw(canvas);
                    } else if (n < 0 && n != NO_DATA) {
                        this.datas[i][j].show_img--;
                        this.bang_img.setBounds(r);
                        this.bang_img.draw(canvas);
                    }
                }
            }
        }
    }
}
