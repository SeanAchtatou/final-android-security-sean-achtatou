package org.ndisk.kakogawastudio.throwing5;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;
import java.text.DecimalFormat;

public class ThrowingView extends SurfaceView implements SurfaceHolder.Callback, Runnable {
    private int GAME_CLEAR = 3;
    private int GAME_OVER = 2;
    private int GAME_PLAY = 1;
    private int GAME_TITLE = 0;
    private int MAX_SOUND_COUNT = 5;
    private Drawable[] back;
    private int backimage = 0;
    private Ball ball;
    private Drawable bang_img;
    private float char_x = 100.0f;
    private float char_y = 350.0f;
    private Chara chara;
    private Drawable[] clear_img;
    private int clearimage = 0;
    private int col = 1;
    private long currtimes = 0;
    private float end_x = 255.0f;
    private float end_y = 240.0f;
    private Enemies enemies;
    private Drawable[] enemy_img;
    private float enemy_x = 420.0f;
    private float enemy_y = 50.0f;
    private int game_mode = 0;
    private long gametimes = 600;
    private SurfaceHolder holder;
    private boolean isThreadRunning = true;
    private long lefttime;
    private MediaPlayer mp = null;
    private Drawable over_img;
    private Point press_loc;
    private int row = 1;
    private int score = 0;
    private float score_x = 80.0f;
    private float score_y = 460.0f;
    private SoundPool soundPool = null;
    private int[] sounds = new int[this.MAX_SOUND_COUNT];
    private long stagetimes = 40;
    private long start_stage;
    private long start_time;
    private Thread thread = null;
    private Throwing throwing;
    private TimeUFO timeufo;
    private Drawable[] timeufo_img;
    private Drawable title_img;

    public ThrowingView(Context context) {
        super(context);
        init(context);
    }

    public ThrowingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public void init(Context context) {
        this.holder = getHolder();
        this.holder.addCallback(this);
        setFocusable(true);
        requestFocus();
        this.throwing = (Throwing) context;
        setBoardSize();
        this.soundPool = new SoundPool(this.MAX_SOUND_COUNT, 3, 0);
        this.sounds[0] = this.soundPool.load(context, R.raw.bomb, 1);
        this.sounds[1] = this.soundPool.load(context, R.raw.over, 1);
        this.sounds[2] = this.soundPool.load(context, R.raw.cool, 1);
        this.sounds[3] = this.soundPool.load(context, R.raw.shot, 1);
        this.mp = MediaPlayer.create(context, (int) R.raw.bgm);
        this.mp.setLooping(true);
        this.thread = new Thread(this);
        this.thread.start();
    }

    private void setBoardSize() {
        float w = this.throwing.disp_w;
        float h = this.throwing.disp_h;
        float dw = w / 854.0f;
        float dh = h / 480.0f;
        this.score_x = this.score_x * dw;
        this.score_y = this.score_y * dh;
        this.char_x = this.char_x * dw;
        this.char_y = this.char_y * dh;
        this.enemy_x = this.enemy_x * dw;
        this.enemy_y = this.enemy_y * dh;
        this.end_x = this.end_x * dw;
        this.end_y = this.end_y * dh;
        Resources resources = this.throwing.getResources();
        this.back = new Drawable[3];
        this.back[0] = resources.getDrawable(R.drawable.back);
        this.back[1] = resources.getDrawable(R.drawable.back_2);
        this.back[2] = resources.getDrawable(R.drawable.back_3);
        this.back[0].setBounds(new Rect(0, 0, (int) this.throwing.disp_w, (int) this.throwing.disp_h));
        this.back[1].setBounds(new Rect(0, 0, (int) this.throwing.disp_w, (int) this.throwing.disp_h));
        this.back[2].setBounds(new Rect(0, 0, (int) this.throwing.disp_w, (int) this.throwing.disp_h));
        this.ball = new Ball(new Drawable[]{resources.getDrawable(R.drawable.ball1), resources.getDrawable(R.drawable.ball2), resources.getDrawable(R.drawable.ball3)}, this.char_x, this.char_y, dw, dh);
        this.bang_img = resources.getDrawable(R.drawable.bang);
        this.chara = new Chara(new Drawable[]{resources.getDrawable(R.drawable.char1), resources.getDrawable(R.drawable.char2), resources.getDrawable(R.drawable.char3)}, this.char_x, this.char_y);
        this.enemy_img = new Drawable[4];
        this.enemy_img[0] = resources.getDrawable(R.drawable.enemy1);
        this.enemy_img[1] = resources.getDrawable(R.drawable.enemy2);
        this.enemy_img[2] = resources.getDrawable(R.drawable.enemy3);
        this.enemy_img[3] = this.enemy_img[2];
        this.enemies = new Enemies(this.enemy_img, this.bang_img, this.enemy_x, this.enemy_y, this.col, this.row);
        this.timeufo_img = new Drawable[4];
        this.timeufo_img[0] = resources.getDrawable(R.drawable.timeball1);
        this.timeufo_img[1] = resources.getDrawable(R.drawable.timeball2);
        this.timeufo_img[2] = resources.getDrawable(R.drawable.timeball3);
        this.timeufo_img[3] = resources.getDrawable(R.drawable.timeball4);
        this.timeufo = new TimeUFO(this.timeufo_img, this.bang_img, (float) (Math.random() * ((double) w)), (float) (Math.random() * ((double) h)), 1, 1);
        this.clear_img = new Drawable[3];
        this.clear_img[0] = resources.getDrawable(R.drawable.clear);
        this.clear_img[1] = resources.getDrawable(R.drawable.clear2);
        this.clear_img[2] = resources.getDrawable(R.drawable.clear3);
        this.title_img = resources.getDrawable(R.drawable.title);
        this.over_img = resources.getDrawable(R.drawable.over);
    }

    public void initialstart() {
        this.score = 0;
        this.col = 1;
        this.row = 1;
        this.currtimes = 0;
        gamestart();
    }

    public void gamestart() {
        this.backimage = (int) (Math.random() * 3.0d);
        this.enemies = new Enemies(this.enemy_img, this.bang_img, this.enemy_x, this.enemy_y, this.col, this.row);
        this.timeufo = new TimeUFO(this.timeufo_img, this.bang_img, (float) (Math.random() * ((double) this.enemy_x)), (float) (Math.random() * ((double) this.enemy_y)), 1, 1);
        this.mp.seekTo(0);
        this.mp.start();
        this.enemies.init();
        this.timeufo.init();
        this.chara.init();
        this.ball.init();
        this.start_time = this.currtimes;
        this.game_mode = this.GAME_PLAY;
    }

    public synchronized void run() {
        while (this.isThreadRunning) {
            synchronized (this.holder) {
                this.currtimes++;
                switch (this.game_mode) {
                    case 1:
                        this.timeufo.move();
                        this.enemies.move();
                        this.chara.move();
                        this.ball.move();
                        if (this.ball.isFlying()) {
                            int hit = this.enemies.checkHit(this.ball.getCenter());
                            if (hit > 0) {
                                if (hit > 1) {
                                    this.soundPool.play(this.sounds[2], 1.0f, 1.0f, 0, 0, 1.0f);
                                }
                                this.soundPool.play(this.sounds[0], 1.0f, 1.0f, 0, 0, 1.0f);
                                this.score += hit * 10;
                                this.ball.init();
                            }
                            if (this.timeufo.checkHit(this.ball.getCenter()) > 0) {
                                this.soundPool.play(this.sounds[0], 1.0f, 1.0f, 0, 0, 1.0f);
                                this.currtimes = 0;
                                this.ball.init();
                            }
                        }
                        if (this.enemies.isFinished()) {
                            if (this.game_mode != this.GAME_CLEAR) {
                                this.start_stage = this.currtimes;
                            }
                            gameClear();
                        }
                        if (this.currtimes - this.start_time > this.gametimes) {
                            gameOver();
                        }
                        draw();
                        break;
                    case 2:
                        draw();
                        break;
                    case 3:
                        gameClear();
                        break;
                }
            }
        }
    }

    public void showMsg(String s) {
        Toast.makeText(this.throwing, s, 0).show();
    }

    public void gameOver() {
        this.game_mode = this.GAME_OVER;
        this.mp.pause();
        this.soundPool.play(this.sounds[1], 1.0f, 1.0f, 0, 0, 1.0f);
        draw();
    }

    public void gameClear() {
        if (this.currtimes - this.start_stage > this.stagetimes) {
            if (this.col == 5 && this.row == 5) {
                this.gametimes -= 100;
            }
            this.col++;
            if (this.col > 5) {
                this.col = 5;
            }
            if (this.col == 5 && this.row < 5) {
                this.row++;
            }
            gamestart();
            return;
        }
        if (this.game_mode != this.GAME_CLEAR) {
            this.soundPool.play(this.sounds[2], 1.0f, 1.0f, 0, 0, 1.0f);
            this.clearimage = (int) (Math.random() * 3.0d);
        }
        this.game_mode = this.GAME_CLEAR;
        this.mp.pause();
        draw();
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (this.game_mode == this.GAME_TITLE && event.getAction() == 0) {
            initialstart();
        }
        if (this.game_mode != this.GAME_PLAY) {
            return true;
        }
        int n = event.getAction();
        float x = event.getX();
        float y = event.getY();
        switch (n) {
            case 0:
                this.press_loc = new Point((int) x, (int) y);
                break;
            case 1:
                float dy = x - ((float) this.press_loc.x);
                float dx = ((float) this.press_loc.y) - y;
                this.chara.throwing();
                this.ball.flying(new Point((int) dx, (int) dy));
                this.score--;
                if (this.score < 0) {
                    this.score = 0;
                }
                this.soundPool.play(this.sounds[3], 1.0f, 1.0f, 0, 0, 1.0f);
                break;
        }
        return true;
    }

    public String zeroPadding(int paddingNum, String num) {
        StringBuffer sb = new StringBuffer();
        for (int count = 0; count < paddingNum; count++) {
            sb.append("0");
        }
        return new DecimalFormat(sb.toString()).format((long) Integer.parseInt(num));
    }

    public void draw() {
        Canvas canvas = null;
        try {
            canvas = this.holder.lockCanvas();
            if (canvas != null) {
                this.back[this.backimage].draw(canvas);
                this.chara.draw(canvas);
                this.enemies.draw(canvas);
                this.timeufo.draw(canvas);
                this.ball.draw(canvas);
                Paint p = new Paint();
                p.setTextSize(20.0f);
                p.setFakeBoldText(true);
                p.setColor(-1);
                this.lefttime = (this.gametimes - (this.currtimes - this.start_time)) / 10;
                StringBuffer sbuf = new StringBuffer("　　TIME: ");
                for (int i = 0; ((long) i) < this.lefttime / 2; i++) {
                    sbuf.append("|");
                }
                canvas.drawText("SCORE: " + zeroPadding(7, new StringBuilder(String.valueOf(this.score)).toString()) + sbuf.toString(), this.score_x, this.score_y, p);
                if (this.game_mode == this.GAME_TITLE) {
                    this.title_img.setBounds(new Rect(0, 0, (int) this.throwing.disp_w, (int) this.throwing.disp_h));
                    this.title_img.draw(canvas);
                }
                if (this.game_mode == this.GAME_OVER) {
                    this.over_img.setBounds(new Rect(0, 0, (int) this.throwing.disp_w, (int) this.throwing.disp_h));
                    this.over_img.draw(canvas);
                }
                if (this.game_mode == this.GAME_CLEAR) {
                    this.clear_img[this.clearimage].setBounds(new Rect(0, 0, (int) this.throwing.disp_w, (int) this.throwing.disp_h));
                    this.clear_img[this.clearimage].draw(canvas);
                }
            }
        } finally {
            if (canvas != null) {
                this.holder.unlockCanvasAndPost(canvas);
            }
        }
    }

    public void surfaceChanged(SurfaceHolder holder2, int format, int width, int height) {
    }

    public void surfaceCreated(SurfaceHolder holder2) {
        draw();
    }

    public void surfaceDestroyed(SurfaceHolder holder2) {
        this.mp.release();
        this.isThreadRunning = false;
        this.thread = null;
    }
}
