package org.ndisk.kakogawastudio.throwing5;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

public class Chara {
    private float chara_h = 100.0f;
    private float chara_w = 100.0f;
    private float chara_x = 100.0f;
    private float chara_y = 350.0f;
    private Drawable[] imgs;
    private int show_img = 0;
    private boolean throwing = false;

    public Chara(Drawable[] imgs2, float x, float y) {
        this.imgs = imgs2;
        this.chara_x = x;
        this.chara_y = y;
        init();
    }

    public void init() {
        this.show_img = 0;
        this.throwing = false;
    }

    public void move() {
        if (this.throwing) {
            this.show_img++;
        }
        if (this.show_img >= 10) {
            this.show_img = 0;
            this.throwing = false;
        }
    }

    public void throwing() {
        if (!this.throwing) {
            this.show_img = 0;
            this.throwing = true;
        }
    }

    public void draw(Canvas canvas) {
        int n = this.show_img;
        if (n >= this.imgs.length - 1) {
            n = this.imgs.length - 1;
        }
        this.imgs[n].setBounds(new Rect((int) this.chara_x, (int) this.chara_y, (int) (this.chara_x + this.chara_w), (int) (this.chara_y + this.chara_h)));
        this.imgs[n].draw(canvas);
    }
}
