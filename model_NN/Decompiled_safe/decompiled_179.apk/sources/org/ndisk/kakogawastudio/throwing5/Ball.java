package org.ndisk.kakogawastudio.throwing5;

import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

public class Ball {
    private float HOLI = 460.0f;
    private float ball_h = 20.0f;
    private float ball_w = 20.0f;
    private float ball_x = 150.0f;
    private float ball_y = 300.0f;
    private boolean flying = false;
    private float g = 10.0f;
    private float gd = 0.9f;
    private Drawable[] imgs;
    private float s = 10.0f;
    private int show_img = 0;
    private float x = 100.0f;
    private float y = 400.0f;

    public Ball(Drawable[] imgs2, float x2, float y2, float dw, float dh) {
        this.imgs = imgs2;
        this.x = x2;
        this.y = y2;
        this.HOLI *= dh;
    }

    public void init() {
        this.flying = false;
    }

    public void flying(Point p) {
        this.ball_x = this.x;
        this.ball_y = this.y;
        this.flying = true;
        this.g = (float) (p.x / 10);
        if (this.g > 25.0f) {
            this.g = 25.0f;
        }
        this.s = (float) (p.y / 10);
        if (this.s > 25.0f) {
            this.s = 25.0f;
        }
    }

    public boolean isFlying() {
        return this.flying;
    }

    public void move() {
        if (this.flying) {
            this.ball_x += this.s;
            this.ball_y -= this.g;
            this.g -= this.gd;
            if (this.show_img >= 0) {
                this.show_img = (this.show_img + 1) % 4;
            }
        }
        if (this.ball_y > this.HOLI) {
            this.flying = false;
        }
    }

    public Point getCenter() {
        return new Point((int) (this.ball_x + (this.ball_w / 2.0f)), (int) (this.ball_y + (this.ball_h / 2.0f)));
    }

    public void draw(Canvas canvas) {
        if (this.flying) {
            int n = this.show_img;
            if (n >= this.imgs.length - 1) {
                n = this.imgs.length - 1;
            }
            this.imgs[n].setBounds(new Rect((int) this.ball_x, (int) this.ball_y, (int) (this.ball_x + this.ball_w), (int) (this.ball_y + this.ball_h)));
            this.imgs[n].draw(canvas);
        }
    }
}
