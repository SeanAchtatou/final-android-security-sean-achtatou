package org.ndisk.kakogawastudio.throwing5;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class SplashActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.splash);
        new Handler().postDelayed(new splashHandler(), 5000);
    }

    class splashHandler implements Runnable {
        splashHandler() {
        }

        public void run() {
            SplashActivity.this.startActivity(new Intent(SplashActivity.this.getApplication(), Throwing.class));
            SplashActivity.this.finish();
        }
    }
}
