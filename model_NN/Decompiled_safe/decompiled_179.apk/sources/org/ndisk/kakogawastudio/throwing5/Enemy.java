package org.ndisk.kakogawastudio.throwing5;

/* compiled from: Enemies */
class Enemy {
    public float dx;
    public float dy;
    public int show_img;
    public float x;
    public float y;

    public Enemy(int img, float x2, float y2, float dx2, float dy2) {
        this.show_img = img;
        this.x = x2;
        this.y = y2;
        this.dx = dx2;
        this.dy = dy2;
    }
}
