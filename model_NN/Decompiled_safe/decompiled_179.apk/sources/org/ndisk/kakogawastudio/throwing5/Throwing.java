package org.ndisk.kakogawastudio.throwing5;

import android.app.Activity;
import android.os.Bundle;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;

public class Throwing extends Activity {
    public float disp_h;
    public float disp_w;
    /* access modifiers changed from: private */
    public ThrowingView view;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        Window window = getWindow();
        window.addFlags(1024);
        Display disp = window.getWindowManager().getDefaultDisplay();
        this.disp_w = (float) disp.getWidth();
        this.disp_h = (float) disp.getHeight();
        setContentView((int) R.layout.main);
        this.view = (ThrowingView) findViewById(R.id.ThrowingView01);
        addContentView(View.inflate(this, R.layout.splash, null), new LinearLayout.LayoutParams(-1, -1));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add("GAME START").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                Throwing.this.view.initialstart();
                return false;
            }
        });
        return true;
    }

    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        System.exit(0);
    }
}
