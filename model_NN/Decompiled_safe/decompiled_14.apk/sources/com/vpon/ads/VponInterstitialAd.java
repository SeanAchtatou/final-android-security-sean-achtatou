package com.vpon.ads;

import android.app.Activity;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.Handler;
import android.widget.RelativeLayout;
import com.vpon.ads.VponAdRequest;
import vpadn.C0003a;
import vpadn.E;
import vpadn.I;
import vpadn.Q;
import vpadn.X;
import vpadn.Y;

public class VponInterstitialAd extends RelativeLayout implements LocationListener, VponAd, I {
    /* access modifiers changed from: private */
    public E a = null;
    private VponAdListener b = null;

    /* renamed from: c  reason: collision with root package name */
    private Activity f86c;
    private boolean d = false;
    private boolean e = true;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public VponInterstitialAd(Activity activity, String str, VponPlatform vponPlatform) {
        super(activity);
        String str2 = null;
        this.f86c = activity;
        setBackgroundColor(0);
        X.a(activity).a(this);
        this.a = new E(activity, this);
        this.e = false;
        if (str == null) {
            this.d = true;
            return;
        }
        this.a.a(str);
        if (vponPlatform == VponPlatform.TW) {
            str2 = "tw";
        } else if (vponPlatform == VponPlatform.CN) {
            str2 = "cn";
        }
        if (str2 == null) {
            this.d = true;
        } else {
            this.a.b(str2);
        }
    }

    public void show() {
        if (this.a == null || !this.a.m()) {
            C0003a.b("VponInterstitialAd", "call show() but is not ready!");
        } else {
            this.a.n();
        }
    }

    public boolean isReady() {
        if (this.a != null) {
            return this.a.m();
        }
        return false;
    }

    public void loadAd(VponAdRequest vponAdRequest) {
        if (!Y.d(this.f86c)) {
            C0003a.b("VponInterstitialAd", "[interstitial] permission-checking  is failde in loadAd!!");
            if (this.b != null) {
                this.b.onVponFailedToReceiveAd(this, VponAdRequest.VponErrorCode.INTERNAL_ERROR);
            }
        } else if (!this.d) {
            this.a.a(vponAdRequest);
        } else {
            C0003a.b("VponInterstitialAd", "[interstitial] invalid parameters in loadAd!!");
            if (this.b != null) {
                this.b.onVponFailedToReceiveAd(this, VponAdRequest.VponErrorCode.INTERNAL_ERROR);
            }
        }
    }

    public void setAdListener(VponAdListener vponAdListener) {
        this.b = vponAdListener;
    }

    public void stopLoading() {
    }

    public void onVponAdReceived() {
        if (this.b != null) {
            this.b.onVponReceiveAd(this);
        }
    }

    public void onVponAdFailed(VponAdRequest.VponErrorCode vponErrorCode) {
        if (this.b != null) {
            this.b.onVponFailedToReceiveAd(this, vponErrorCode);
        }
    }

    public void onVponPresent() {
        if (this.b != null) {
            this.b.onVponPresentScreen(this);
        }
    }

    public void onVponDismiss() {
        if (this.b != null) {
            this.b.onVponDismissScreen(this);
        }
    }

    public void onVponLeaveApplication() {
        if (this.b != null) {
            this.b.onVponLeaveApplication(this);
        }
    }

    public void onControllerWebViewReady(int i, int i2) {
    }

    public void onPrepareExpandMode() {
    }

    public void onLeaveExpandMode() {
    }

    public void onLocationChanged(Location location) {
    }

    public void onProviderDisabled(String str) {
    }

    public void onProviderEnabled(String str) {
    }

    public void onStatusChanged(String str, int i, Bundle bundle) {
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        C0003a.a("VponInterstitialAd", "onDetachedFromWindow in VponInterstitialAd");
        super.onDetachedFromWindow();
        if (!this.e) {
            X.a(this.f86c).b(this);
            this.e = true;
        }
        if (this.a != null) {
            this.a.b();
            this.a.o();
            this.a = null;
        }
    }

    public void destroy() {
        Q.a().b();
        if (!this.e) {
            X.a(this.f86c).b(this);
            this.e = true;
        }
        new Handler().post(new Runnable() {
            public void run() {
                if (VponInterstitialAd.this.a != null) {
                    VponInterstitialAd.this.a.b();
                    VponInterstitialAd.this.a.o();
                    VponInterstitialAd.this.a = null;
                }
            }
        });
    }
}
