package com.vpon.ads;

import com.vpon.ads.VponAdRequest;

public interface VponAdListener {
    void onVponDismissScreen(VponAd vponAd);

    void onVponFailedToReceiveAd(VponAd vponAd, VponAdRequest.VponErrorCode vponErrorCode);

    void onVponLeaveApplication(VponAd vponAd);

    void onVponPresentScreen(VponAd vponAd);

    void onVponReceiveAd(VponAd vponAd);
}
