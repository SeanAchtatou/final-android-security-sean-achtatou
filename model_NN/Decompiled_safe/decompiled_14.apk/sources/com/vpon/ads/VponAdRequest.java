package com.vpon.ads;

import android.content.Context;
import android.location.Location;
import android.telephony.TelephonyManager;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class VponAdRequest {
    private Gender a;
    private Set<String> b;

    /* renamed from: c  reason: collision with root package name */
    private Set<String> f83c;
    private Date d;
    private boolean e;
    private Location f;
    private boolean g;
    private int h;

    public enum Gender {
        FEMALE,
        MALE,
        UNKNOWN
    }

    public enum VponErrorCode {
        INTERNAL_ERROR,
        INVALID_REQUEST,
        NO_FILL,
        NETWORK_ERROR
    }

    public int getAge() {
        return this.h;
    }

    public void setAge(int i) {
        this.h = i;
    }

    public boolean isAutoRefresh() {
        return this.g;
    }

    public VponAdRequest() {
        this.a = Gender.UNKNOWN;
        this.b = new HashSet();
        this.f83c = new HashSet();
        this.d = null;
        this.e = false;
        this.f = null;
        this.g = false;
        this.g = false;
    }

    public void setEnableAutoRefresh(boolean z) {
        this.g = z;
    }

    public VponAdRequest setAutoRefresh(boolean z) {
        this.g = z;
        return this;
    }

    @Deprecated
    public VponAdRequest addExtra(String str, Object obj) {
        return this;
    }

    public VponAdRequest addKeyword(String str) {
        this.b.add(str);
        return this;
    }

    public VponAdRequest addKeywords(Set<String> set) {
        for (String add : set) {
            this.b.add(add);
        }
        return this;
    }

    public VponAdRequest addMediationExtra(String str, Object obj) {
        return this;
    }

    public VponAdRequest addTestDevice(String str) {
        this.f83c.add(str);
        return this;
    }

    public VponAdRequest clearBirthday() {
        this.d = null;
        return this;
    }

    public Date getBirthday() {
        return this.d;
    }

    public Gender getGender() {
        return this.a;
    }

    public Set<String> getKeywords() {
        return this.b;
    }

    public Location getLocation() {
        return this.f;
    }

    @Deprecated
    public <T> T getNetworkExtras(Class<T> cls) {
        return null;
    }

    public boolean getPlusOneOptOut() {
        return false;
    }

    public Map<String, Object> getRequestMap(Context context) {
        return null;
    }

    public boolean isTestDevice(Context context) {
        if (this.f83c.contains(((TelephonyManager) context.getSystemService("phone")).getDeviceId())) {
            this.e = true;
        } else {
            this.e = false;
        }
        return this.e;
    }

    public VponAdRequest removeNetworkExtras(Class<?> cls) {
        return null;
    }

    public VponAdRequest setBirthday(Calendar calendar) {
        this.d = calendar.getTime();
        return this;
    }

    public VponAdRequest setBirthday(Date date) {
        this.d = date;
        return this;
    }

    @Deprecated
    public VponAdRequest setBirthday(String str) {
        try {
            this.d = new SimpleDateFormat("yyyy-MM-DD").parse(str);
        } catch (ParseException e2) {
            e2.printStackTrace();
        }
        return this;
    }

    @Deprecated
    public VponAdRequest setExtras(Map<String, Object> map) {
        return this;
    }

    public VponAdRequest setGender(Gender gender) {
        this.a = gender;
        return this;
    }

    public VponAdRequest setKeywords(Set<String> set) {
        for (String add : set) {
            this.b.add(add);
        }
        return this;
    }

    public VponAdRequest setLocation(Location location) {
        this.f = location;
        return this;
    }

    public VponAdRequest setMediationExtras(Map<String, Object> map) {
        return this;
    }

    @Deprecated
    public VponAdRequest setPlusOneOptOut(boolean z) {
        return this;
    }

    public VponAdRequest setTestDevices(Set<String> set) {
        for (String add : set) {
            this.f83c.add(add);
        }
        return this;
    }

    @Deprecated
    public VponAdRequest setTesting(boolean z) {
        this.e = z;
        return this;
    }
}
