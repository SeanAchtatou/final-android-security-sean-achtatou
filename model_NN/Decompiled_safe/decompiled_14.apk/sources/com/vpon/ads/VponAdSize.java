package com.vpon.ads;

import android.content.Context;

public class VponAdSize {
    public static final int AUTO_HEIGHT = -1;
    public static final VponAdSize BANNER = new VponAdSize(320, 50, false);
    public static final int FULL_WIDTH = -2;
    public static final VponAdSize IAB_BANNER = new VponAdSize(468, 60, false);
    public static final VponAdSize IAB_LEADERBOARD = new VponAdSize(728, 90, false);
    public static final VponAdSize IAB_MRECT = new VponAdSize(300, 250, false);
    public static final VponAdSize IAB_WIDE_SKYSCRAPER = new VponAdSize(60, 468, false);
    public static final int LANDSCAPE_AD_HEIGHT = -3;
    public static final int LARGE_AD_HEIGHT = -4;
    public static final String MIX_CUSTOME_HEIGHT = "mix_custom_height";
    public static final String MIX_CUSTOME_WIDTH = "mix_custom_width";
    public static final int PORTRAIT_AD_HEIGHT = -5;
    public static final VponAdSize SMART_BANNER = new VponAdSize(-2, -1, false);
    private int a;
    private int b;

    /* renamed from: c  reason: collision with root package name */
    private boolean f84c = false;
    private boolean d = false;
    private String e = "";

    public VponAdSize(int i, int i2) {
        if (i == -1 || i2 == -2) {
            throw new UnsupportedOperationException("width == AUTO_HEIGHT || height == FULL_WIDTH");
        }
        this.a = i;
        this.b = i2;
        if (i == -2 && i2 == -1) {
            this.f84c = false;
        } else if ((i != -2 || i2 == -1) && (i == -2 || i2 != -1)) {
            this.f84c = true;
        } else {
            this.f84c = false;
            this.d = true;
            if (i == -2) {
                this.e = MIX_CUSTOME_HEIGHT;
            } else {
                this.e = MIX_CUSTOME_WIDTH;
            }
        }
    }

    private VponAdSize(int i, int i2, boolean z) {
        this.a = i;
        this.b = i2;
        this.f84c = false;
    }

    public boolean isMix() {
        return this.d;
    }

    public String getMixModeCustomStr() {
        return this.e;
    }

    public boolean isSizeAppropriate(int i, int i2) {
        if (i == 320 && i2 == 50) {
            return true;
        }
        if (i == 468 && i2 == 60) {
            return true;
        }
        if (i == 728 && i2 == 90) {
            return true;
        }
        if (i == 300 && i2 == 250) {
            return true;
        }
        if (i == 60 && i2 == 468) {
            return true;
        }
        return false;
    }

    public int getWidth() {
        if (this.a >= 0 && this.b >= 0) {
            return this.a;
        }
        throw new UnsupportedOperationException("Cannot get ad width");
    }

    public int getHeight() {
        if (this.a >= 0 && this.b >= 0) {
            return this.b;
        }
        throw new UnsupportedOperationException("Cannot get ad height");
    }

    public boolean isAutoHeight() {
        if (this.a == -1) {
            return true;
        }
        return false;
    }

    public boolean isFullWidth() {
        if (this.b == -2) {
            return true;
        }
        return false;
    }

    public boolean isCustomAdSize() {
        return this.f84c;
    }

    public int getWidthInPixels(Context context) {
        if (this.a > 0) {
            return (int) convertDpToPixel((float) this.a, context);
        }
        return 0;
    }

    public int getHeightInPixels(Context context) {
        if (this.b > 0) {
            return (int) convertDpToPixel((float) this.b, context);
        }
        return 0;
    }

    public static float convertDpToPixel(float f, Context context) {
        return context.getResources().getDisplayMetrics().density * f;
    }

    public static float convertPixelsToDp(float f, Context context) {
        return f / context.getResources().getDisplayMetrics().density;
    }

    public VponAdSize findBestSize(VponAdSize... vponAdSizeArr) {
        return null;
    }

    public String toString() {
        return "[VponAdSize] width" + this.a + " height:" + this.b + " isCustomSize:" + this.f84c;
    }

    public int hashCode() {
        return (((this.f84c ? 1231 : 1237) + ((this.b + 31) * 31)) * 31) + this.a;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        VponAdSize vponAdSize = (VponAdSize) obj;
        if (this.b != vponAdSize.b) {
            return false;
        }
        if (this.f84c != vponAdSize.f84c) {
            return false;
        }
        if (this.a != vponAdSize.a) {
            return false;
        }
        return true;
    }
}
