package com.vpon.webview;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.widget.FrameLayout;
import c.CordovaWebView;
import vpadn.C0003a;
import vpadn.C0006d;
import vpadn.C0008f;
import vpadn.C0018p;
import vpadn.L;
import vpadn.O;

public class VponAdWebView extends CordovaWebView {
    /* access modifiers changed from: private */
    public String h = null;
    /* access modifiers changed from: private */
    public L i;
    private int j;
    private int k;
    /* access modifiers changed from: private */
    public boolean l = true;
    private Activity m = null;
    private int n;

    public VponAdWebView(String str, Activity activity, L l2, C0018p pVar) {
        super(activity, pVar);
        this.h = str;
        this.i = l2;
        this.m = activity;
        i();
        setWebViewClient(new c(pVar, this));
        setWebChromeClient(new b(pVar, this));
    }

    public VponAdWebView(String str, Activity activity, L l2) {
        super(activity);
        this.h = str;
        this.i = l2;
        this.m = activity;
        i();
        setWebViewClient(new c((C0018p) activity, this));
        setWebChromeClient(new b((C0018p) activity, this));
    }

    @SuppressLint({"NewApi"})
    private void i() {
        WebSettings settings = getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setSupportZoom(true);
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        if (Build.VERSION.SDK_INT >= 17) {
            settings.setMediaPlaybackRequiresUserGesture(false);
        }
        settings.setDomStorageEnabled(true);
        settings.setAppCacheMaxSize(8388608);
        if (this.m == null || this.m.getApplicationContext() == null || this.m.getApplicationContext().getCacheDir() == null) {
            C0003a.b("VponAdWebView", "mActivity.getApplicationContext().getCacheDir().getAbsolutePath() has NPE");
        } else {
            settings.setAppCachePath(this.m.getApplicationContext().getCacheDir().getAbsolutePath());
        }
        settings.setAllowFileAccess(true);
        settings.setAppCacheEnabled(true);
        settings.setCacheMode(-1);
        settings.setUserAgentString(String.valueOf(settings.getUserAgentString()) + " (Mobile; vpadn-sdk-a-v4.0.0)");
    }

    public final String h() {
        return this.h;
    }

    public void setVponWebViewId(String str) {
        this.h = str;
    }

    public void setWebViewJsAlertShow(boolean z) {
        this.l = z;
    }

    public void setAdRect(int i2, int i3) {
        this.j = i2;
        this.k = i3;
    }

    public void loadDataWithBaseURL(String str, String str2, String str3, String str4, String str5) {
        String str6;
        this.h.equals("init");
        if (str2.contains("mraid.js")) {
            if (this.h.equals("bannerWebView")) {
                str6 = str2.replace("<head>", "<head><script type='text/javascript' charset='utf-8' src='" + O.a().a("mraid2_banner") + "'></script>");
            } else if (this.h.equals("SdkOpenWebApp")) {
                str6 = str2.replace("<head>", "<head><script type='text/javascript' charset='utf-8' src='" + O.a().a("mraid2_expanded") + "'></script>");
            } else if (this.h.equals("InterstitialAdWebView(new Activity)")) {
                str6 = str2.replace("<head>", "<head><script type='text/javascript' charset='utf-8' src='" + O.a().a("mraid2_intersitial") + "'></script>");
            }
            super.loadDataWithBaseURL(str, str6, str3, str4, str5);
        }
        str6 = str2;
        super.loadDataWithBaseURL(str, str6, str3, str4, str5);
    }

    class b extends C0006d {
        public b(C0018p pVar, CordovaWebView cordovaWebView) {
            super(pVar, cordovaWebView);
        }

        public final void onProgressChanged(WebView webView, int i) {
            if (i == 100 && VponAdWebView.this.i != null) {
                VponAdWebView.this.i.t();
            }
        }

        public final void onReachedMaxAppCacheSize(long j, long j2, WebStorage.QuotaUpdater quotaUpdater) {
            quotaUpdater.updateQuota(2 * j);
        }

        public final boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
            C0003a.b("VponAdWebView", "MESSAGE:" + str2);
            if (VponAdWebView.this.l) {
                return super.onJsAlert(webView, str, str2, jsResult);
            }
            return true;
        }
    }

    class c extends C0008f {
        public c(C0018p pVar, CordovaWebView cordovaWebView) {
            super(pVar, cordovaWebView);
        }

        public final void onReceivedError(WebView webView, int i, String str, String str2) {
            C0003a.b("VponAdWebView", "VponAdWebView onReceivedError errorCode:" + i + " des:" + str + " failingUrl:" + str2);
            VponAdWebView.this.i.a(webView, i, str, str2);
        }
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i2) {
        C0003a.c("VponAdWebView", "-------->visibility:" + i2);
        if (this.i != null) {
            if (i2 == 0) {
                this.i.r();
            } else {
                this.i.s();
            }
        }
        super.onWindowVisibilityChanged(i2);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        Rect rect = new Rect();
        this.m.getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
        int i6 = rect.top;
        int[] iArr = {0, 0};
        getLocationOnScreen(iArr);
        int i7 = iArr[0];
        int i8 = iArr[1] - i6;
        int i9 = (i4 - i2) + i7;
        int i10 = (i5 - i3) + i8;
        if (this.i != null) {
            this.i.a(i7, i8, i9, i10);
        }
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        if (this.i != null) {
            this.i.a(i2, i3);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int i4;
        int i5 = Integer.MAX_VALUE;
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        int mode2 = View.MeasureSpec.getMode(i3);
        int size2 = View.MeasureSpec.getSize(i3);
        int i6 = this.j;
        int i7 = this.k;
        if (mode == Integer.MIN_VALUE || mode == 1073741824) {
            i4 = size;
        } else {
            i4 = Integer.MAX_VALUE;
        }
        if (mode2 == Integer.MIN_VALUE || mode2 == 1073741824) {
            i5 = size2;
        }
        if (i6 > i4 || i7 > i5) {
            C0003a.b("vpon ad display", "Not enough space for ad");
            setVisibility(8);
            setMeasuredDimension(size, size2);
        } else {
            setMeasuredDimension(i6, i7);
        }
        super.onMeasure(i2, i3);
    }

    class a extends FrameLayout {
        public a(Context context) {
            super(context);
            setFocusableInTouchMode(true);
        }

        public final boolean onKeyUp(int i, KeyEvent keyEvent) {
            View focusedChild = getFocusedChild();
            if (!VponAdWebView.this.g() && (focusedChild == null || i != 4 || (!VponAdWebView.this.h.equals("bannerWebViewExpanded") && !VponAdWebView.this.h.equals("bannerWebViewResized")))) {
                return super.onKeyUp(i, keyEvent);
            }
            VponAdWebView.this.f();
            return true;
        }

        public final boolean onKeyDown(int i, KeyEvent keyEvent) {
            return super.onKeyDown(i, keyEvent);
        }
    }

    @SuppressLint({"NewApi"})
    public final void a(View view, WebChromeClient.CustomViewCallback customViewCallback) {
        view.setBackgroundColor(-16777216);
        if (this.h == null || (!this.h.equals("bannerWebViewExpanded") && !this.h.equals("bannerWebViewResized"))) {
            super.a(view, customViewCallback);
        } else {
            a aVar = new a(this.m);
            aVar.addView(view);
            super.a(aVar, customViewCallback);
            aVar.requestFocus();
        }
        if (Build.VERSION.SDK_INT >= 11) {
            ViewGroup viewGroup = (ViewGroup) getParent();
            this.n = viewGroup.getSystemUiVisibility();
            viewGroup.setSystemUiVisibility(256);
        }
    }

    @SuppressLint({"NewApi"})
    public final void f() {
        C0003a.a("VponAdWebView", "enter hideCustomView");
        if (Build.VERSION.SDK_INT >= 11) {
            ((ViewGroup) getParent()).setSystemUiVisibility(this.n);
        }
        super.f();
        if (this.h == null) {
            return;
        }
        if (this.h.equals("bannerWebViewExpanded") || this.h.equals("bannerWebViewResized")) {
            requestFocus();
        }
    }

    @SuppressLint({"NewApi"})
    public final void b(boolean z) {
        super.b(z);
        if (Build.VERSION.SDK_INT >= 11) {
            onPause();
        }
    }

    @SuppressLint({"NewApi"})
    public final void a(boolean z, boolean z2) {
        super.a(z, z2);
        if (Build.VERSION.SDK_INT >= 11) {
            onResume();
        }
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        C0003a.a("VponAdWebView", "------->onKeyUp");
        if (this.h == null || ((!this.h.equals("bannerWebViewExpanded") && !this.h.equals("bannerWebViewResized")) || i2 != 4)) {
            return super.onKeyUp(i2, keyEvent);
        }
        if (this.i != null) {
            this.i.z();
        }
        return true;
    }
}
