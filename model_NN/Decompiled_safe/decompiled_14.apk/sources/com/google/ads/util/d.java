package com.google.ads.util;

import android.os.Build;
import c.NetworkManager;

class d {
    static final d d = new d();
    static final d e = new d(NetworkManager.TYPE_UNKNOWN, "generic", "generic");
    static final d f = new d(NetworkManager.TYPE_UNKNOWN, "generic_x86", "Android");
    public final String a;
    public final String b;

    /* renamed from: c  reason: collision with root package name */
    public final String f80c;

    d() {
        this.a = Build.BOARD;
        this.b = Build.DEVICE;
        this.f80c = Build.BRAND;
    }

    d(String str, String str2, String str3) {
        this.a = str;
        this.b = str2;
        this.f80c = str3;
    }

    public boolean equals(Object o) {
        if (!(o instanceof d)) {
            return false;
        }
        d dVar = (d) o;
        if (!a(this.a, dVar.a) || !a(this.b, dVar.b) || !a(this.f80c, dVar.f80c)) {
            return false;
        }
        return true;
    }

    private static boolean a(String str, String str2) {
        if (str != null) {
            return str.equals(str2);
        }
        return str == str2;
    }

    public int hashCode() {
        int i = 0;
        if (this.a != null) {
            i = 0 + this.a.hashCode();
        }
        if (this.b != null) {
            i += this.b.hashCode();
        }
        if (this.f80c != null) {
            return i + this.f80c.hashCode();
        }
        return i;
    }
}
