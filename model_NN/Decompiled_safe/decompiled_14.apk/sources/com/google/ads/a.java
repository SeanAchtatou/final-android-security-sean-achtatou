package com.google.ads;

import java.util.HashMap;
import java.util.List;

public class a {
    private final String a;
    private final String b;

    /* renamed from: c  reason: collision with root package name */
    private final List<String> f32c;
    private final List<String> d;
    private final HashMap<String, String> e;

    public a(String str, String str2, List<String> list, List<String> list2, HashMap<String, String> hashMap) {
        com.google.ads.util.a.a(str2);
        if (str != null) {
            com.google.ads.util.a.a(str);
        }
        this.a = str;
        this.b = str2;
        this.f32c = list;
        this.e = hashMap;
        this.d = list2;
    }

    public String a() {
        return this.a;
    }

    public String b() {
        return this.b;
    }

    public List<String> c() {
        return this.f32c;
    }

    public List<String> d() {
        return this.d;
    }

    public HashMap<String, String> e() {
        return this.e;
    }
}
