package com.google.ads;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

public class b {

    /* renamed from: c  reason: collision with root package name */
    private static b f41c = null;
    private final BigInteger a = d();
    private BigInteger b = BigInteger.ONE;

    public static synchronized b a() {
        b bVar;
        synchronized (b.class) {
            if (f41c == null) {
                f41c = new b();
            }
            bVar = f41c;
        }
        return bVar;
    }

    public synchronized BigInteger b() {
        return this.a;
    }

    public synchronized BigInteger c() {
        BigInteger bigInteger;
        bigInteger = this.b;
        this.b = this.b.add(BigInteger.ONE);
        return bigInteger;
    }

    private b() {
    }

    private static BigInteger d() {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            UUID randomUUID = UUID.randomUUID();
            instance.update(a(randomUUID.getLeastSignificantBits()));
            instance.update(a(randomUUID.getMostSignificantBits()));
            byte[] bArr = new byte[9];
            bArr[0] = 0;
            System.arraycopy(instance.digest(), 0, bArr, 1, 8);
            return new BigInteger(bArr);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Cannot find MD5 message digest algorithm.");
        }
    }

    private static byte[] a(long j) {
        return BigInteger.valueOf(j).toByteArray();
    }
}
