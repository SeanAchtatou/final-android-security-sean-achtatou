package com.beginnerLab.whattoeat;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.ads.Ad;
import com.google.ads.AdActivity;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.google.ads.InterstitialAd;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.MapBuilder;

public class ResultActivity extends Activity {
    private RelativeLayout adBannerLayout;
    private AdView adMobAdView;
    private ImageButton ibAddress;
    private ImageButton ibBack;
    private ImageButton ibPhone;
    private InterstitialAd interstitial;
    /* access modifiers changed from: private */
    public EasyTracker myTracker;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.result_main);
        this.myTracker = EasyTracker.getInstance(this);
        this.myTracker.activityStart(this);
        adMobMediationInit();
        ((Vibrator) getApplication().getSystemService("vibrator")).vibrate(300);
        final Bundle b = getIntent().getExtras();
        this.ibBack = (ImageButton) findViewById(R.id.imageButton1);
        this.ibBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ResultActivity.this.myTracker.send(MapBuilder.createEvent("click", "button_press", "back_main", null).build());
                ResultActivity.this.finish();
            }
        });
        ((TextView) findViewById(R.id.textView1)).setText(b.getString("name"));
        ((TextView) findViewById(R.id.textView2)).setText(String.valueOf(b.getString("distance")) + AdActivity.TYPE_PARAM);
        ((TextView) findViewById(R.id.textView3)).setText(b.getString("address"));
        TextView txtPhone = (TextView) findViewById(R.id.textView4);
        this.ibPhone = (ImageButton) findViewById(R.id.imageButton3);
        if (b.getString("phone") != "NO TEL INFOMATION") {
            this.ibPhone.setClickable(true);
            txtPhone.setText(b.getString("phone"));
        } else {
            this.ibPhone.setClickable(false);
            txtPhone.setText("NO TEL INFOMATION");
        }
        this.ibPhone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ResultActivity.this.myTracker.send(MapBuilder.createEvent("click", "button_press", "to_callphone", null).build());
                ResultActivity.this.startActivity(new Intent("android.intent.action.CALL", Uri.parse("tel:" + b.getString("phone"))));
            }
        });
        this.ibAddress = (ImageButton) findViewById(R.id.imageButton2);
        this.ibAddress.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ResultActivity.this.myTracker.send(MapBuilder.createEvent("click", "button_press", "to_map", null).build());
                ResultActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://maps.google.com/maps?f=d&saddr=" + b.getDouble("slat") + "%20" + b.getDouble("slng") + "&daddr=" + b.getString("elat") + "%20" + b.getString("elng") + "&hl=zh-tw&dirflg=w")));
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this);
    }

    private void adMobMediationInit() {
        getWindow().setFlags(16777216, 16777216);
        setContentView((int) R.layout.result_main);
        this.adBannerLayout = (RelativeLayout) findViewById(R.id.adLayout);
        AdRequest adReq = new AdRequest();
        this.adMobAdView = new AdView(this, AdSize.SMART_BANNER, "e792587cfdbe4149");
        this.adMobAdView.setAdListener(new AdListener() {
            public void onDismissScreen(Ad arg0) {
                Log.d("admob_banner", "onDismissScreen");
            }

            public void onFailedToReceiveAd(Ad arg0, AdRequest.ErrorCode arg1) {
                Log.d("admob_banner", "onFailedToReceiveAd");
            }

            public void onLeaveApplication(Ad arg0) {
                Log.d("admob_banner", "onLeaveApplication");
            }

            public void onPresentScreen(Ad arg0) {
                Log.d("admob_banner", "onPresentScreen");
            }

            public void onReceiveAd(Ad ad) {
                Log.d("admob_banner", "onReceiveAd ad:" + ad.getClass());
            }
        });
        this.adMobAdView.loadAd(adReq);
        this.adBannerLayout.addView(this.adMobAdView);
    }
}
