package com.beginnerLab.whattoeat;

public final class R {

    public static final class attr {
    }

    public static final class bool {
        public static final int ga_autoActivityTracking = 2131034113;
        public static final int ga_dryRun = 2131034112;
        public static final int ga_reportUncaughtExceptions = 2131034114;
    }

    public static final class dimen {
        public static final int activity_horizontal_margin = 2131099648;
        public static final int activity_vertical_margin = 2131099649;
    }

    public static final class drawable {
        public static final int awte_typo = 2130837504;
        public static final int ic_launcher = 2130837505;
        public static final int img_btn_location_src_selector = 2130837506;
        public static final int img_btn_telephone_src_selector = 2130837507;
        public static final int logo2 = 2130837508;
        public static final int wte_back = 2130837509;
        public static final int wte_bg = 2130837510;
        public static final int wte_foursquare = 2130837511;
        public static final int wte_location = 2130837512;
        public static final int wte_location_press = 2130837513;
        public static final int wte_seprate = 2130837514;
        public static final int wte_shack = 2130837515;
        public static final int wte_telephone = 2130837516;
        public static final int wte_telephone_press = 2130837517;
        public static final int wte_typo = 2130837518;
        public static final int wte_walkman = 2130837519;
    }

    public static final class id {
        public static final int action_settings = 2131296270;
        public static final int adLayout = 2131296259;
        public static final int imageButton1 = 2131296260;
        public static final int imageButton2 = 2131296261;
        public static final int imageButton3 = 2131296263;
        public static final int imageView1 = 2131296256;
        public static final int imageView2 = 2131296265;
        public static final int mainLayout = 2131296268;
        public static final int textView1 = 2131296257;
        public static final int textView2 = 2131296258;
        public static final int textView3 = 2131296262;
        public static final int textView4 = 2131296264;
        public static final int toast_layout_root = 2131296266;
        public static final int toast_text = 2131296267;
        public static final int vponBannerXML = 2131296269;
    }

    public static final class layout {
        public static final int activity_main = 2130903040;
        public static final int result_main = 2130903041;
        public static final int toast_layout = 2130903042;
        public static final int vpon = 2130903043;
    }

    public static final class menu {
        public static final int main = 2131230720;
    }

    public static final class string {
        public static final int action_settings = 2130968579;
        public static final int app_name = 2130968578;
        public static final int ga_logLevel = 2130968577;
        public static final int ga_trackingId = 2130968576;
        public static final int hello_world = 2130968580;
        public static final int main_restaurant = 2130968582;
        public static final int main_shaking = 2130968581;
    }

    public static final class style {
        public static final int AppBaseTheme = 2131165184;
        public static final int AppTheme = 2131165185;
        public static final int dialog = 2131165186;
    }
}
