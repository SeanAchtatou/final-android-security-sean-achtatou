package c.api;

import c.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vpadn.C0017o;
import vpadn.C0018p;
import vpadn.C0019q;
import vpadn.C0021s;
import vpadn.C0024v;

@Deprecated
public abstract class Plugin extends C0019q {
    public C0021s ctx;

    public abstract C0024v execute(String str, JSONArray jSONArray, String str2);

    public boolean isSynch(String str) {
        return false;
    }

    public void initialize(C0018p pVar, CordovaWebView cordovaWebView) {
        super.initialize(pVar, cordovaWebView);
        setContext(pVar);
        setView(cordovaWebView);
    }

    public void setContext(C0018p pVar) {
        this.cordova = pVar;
        this.ctx = new C0021s(this.cordova);
    }

    public void setView(CordovaWebView cordovaWebView) {
        this.webView = cordovaWebView;
    }

    public boolean execute(final String str, final JSONArray jSONArray, C0017o oVar) throws JSONException {
        final String str2 = oVar.a;
        if (!isSynch(str)) {
            this.cordova.e().execute(new Runnable() {
                public final void run() {
                    C0024v vVar;
                    try {
                        vVar = Plugin.this.execute(str, jSONArray, str2);
                    } catch (Throwable th) {
                        vVar = new C0024v(C0024v.a.ERROR, th.getMessage());
                    }
                    Plugin.this.sendPluginResult(vVar, str2);
                }
            });
        } else {
            C0024v execute = execute(str, jSONArray, str2);
            if (execute == null) {
                execute = new C0024v(C0024v.a.NO_RESULT);
            }
            oVar.a(execute);
        }
        return true;
    }

    public void sendJavascript(String str) {
        this.webView.c(str);
    }

    public void sendPluginResult(C0024v vVar, String str) {
        this.webView.a(vVar, str);
    }

    public void success(C0024v vVar, String str) {
        this.webView.a(vVar, str);
    }

    public void success(JSONObject jSONObject, String str) {
        this.webView.a(new C0024v(C0024v.a.OK, jSONObject), str);
    }

    public void success(String str, String str2) {
        this.webView.a(new C0024v(C0024v.a.OK, str), str2);
    }

    public void error(C0024v vVar, String str) {
        this.webView.a(vVar, str);
    }

    public void error(JSONObject jSONObject, String str) {
        this.webView.a(new C0024v(C0024v.a.ERROR, jSONObject), str);
    }

    public void error(String str, String str2) {
        this.webView.a(new C0024v(C0024v.a.ERROR, str), str2);
    }
}
