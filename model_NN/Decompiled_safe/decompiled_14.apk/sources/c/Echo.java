package c;

import android.util.Base64;
import org.json.JSONException;
import vpadn.C0005c;
import vpadn.C0017o;
import vpadn.C0019q;
import vpadn.C0024v;

public class Echo extends C0019q {
    public boolean execute(String str, C0005c cVar, final C0017o oVar) throws JSONException {
        final String str2 = null;
        if ("echo".equals(str)) {
            if (!cVar.b(0)) {
                str2 = cVar.a(0);
            }
            oVar.a(str2);
            return true;
        } else if ("echoAsync".equals(str)) {
            if (!cVar.b(0)) {
                str2 = cVar.a(0);
            }
            this.cordova.e().execute(new Runnable(this) {
                public final void run() {
                    oVar.a(str2);
                }
            });
            return true;
        } else if (!"echoArrayBuffer".equals(str)) {
            return false;
        } else {
            oVar.a(new C0024v(C0024v.a.OK, Base64.decode(cVar.a.getString(0), 0)));
            return true;
        }
    }
}
