package c;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.os.Vibrator;
import org.json.JSONArray;
import org.json.JSONException;
import vpadn.C0017o;
import vpadn.C0018p;
import vpadn.C0019q;
import vpadn.C0024v;

public class Notification extends C0019q {
    public int confirmResult = -1;
    public ProgressDialog progressDialog = null;
    public ProgressDialog spinnerDialog = null;

    public boolean execute(String str, JSONArray jSONArray, C0017o oVar) throws JSONException {
        if (str.equals("beep")) {
            beep(jSONArray.getLong(0));
        } else if (str.equals("vibrate")) {
            vibrate(jSONArray.getLong(0));
        } else if (str.equals("alert")) {
            alert(jSONArray.getString(0), jSONArray.getString(1), jSONArray.getString(2), oVar);
            return true;
        } else if (str.equals("confirm")) {
            confirm(jSONArray.getString(0), jSONArray.getString(1), jSONArray.getString(2), oVar);
            return true;
        } else if (str.equals("activityStart")) {
            activityStart(jSONArray.getString(0), jSONArray.getString(1));
        } else if (str.equals("activityStop")) {
            activityStop();
        } else if (str.equals("progressStart")) {
            progressStart(jSONArray.getString(0), jSONArray.getString(1));
        } else if (str.equals("progressValue")) {
            progressValue(jSONArray.getInt(0));
        } else if (!str.equals("progressStop")) {
            return false;
        } else {
            progressStop();
        }
        oVar.b();
        return true;
    }

    public void beep(long j) {
        Ringtone ringtone = RingtoneManager.getRingtone(this.cordova.a().getBaseContext(), RingtoneManager.getDefaultUri(2));
        if (ringtone != null) {
            for (long j2 = 0; j2 < j; j2 = 1 + j2) {
                ringtone.play();
                long j3 = 5000;
                while (ringtone.isPlaying() && j3 > 0) {
                    j3 -= 100;
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                    }
                }
            }
        }
    }

    public void vibrate(long j) {
        if (j == 0) {
            j = 500;
        }
        ((Vibrator) this.cordova.a().getSystemService("vibrator")).vibrate(j);
    }

    public synchronized void alert(String str, String str2, String str3, C0017o oVar) {
        final C0018p pVar = this.cordova;
        final String str4 = str;
        final String str5 = str2;
        final String str6 = str3;
        final C0017o oVar2 = oVar;
        this.cordova.a().runOnUiThread(new Runnable(this) {
            public final void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(pVar.a());
                builder.setMessage(str4);
                builder.setTitle(str5);
                builder.setCancelable(true);
                String str = str6;
                final C0017o oVar = oVar2;
                builder.setPositiveButton(str, new DialogInterface.OnClickListener(this) {
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        oVar.a(new C0024v(C0024v.a.OK, 0));
                    }
                });
                final C0017o oVar2 = oVar2;
                builder.setOnCancelListener(new DialogInterface.OnCancelListener(this) {
                    public final void onCancel(DialogInterface dialogInterface) {
                        dialogInterface.dismiss();
                        oVar2.a(new C0024v(C0024v.a.OK, 0));
                    }
                });
                builder.create();
                builder.show();
            }
        });
    }

    public synchronized void confirm(String str, String str2, String str3, C0017o oVar) {
        final C0018p pVar = this.cordova;
        final String[] split = str3.split(",");
        final String str4 = str;
        final String str5 = str2;
        final C0017o oVar2 = oVar;
        this.cordova.a().runOnUiThread(new Runnable(this) {
            public final void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(pVar.a());
                builder.setMessage(str4);
                builder.setTitle(str5);
                builder.setCancelable(true);
                if (split.length > 0) {
                    String str = split[0];
                    final C0017o oVar = oVar2;
                    builder.setNegativeButton(str, new DialogInterface.OnClickListener(this) {
                        public final void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            oVar.a(new C0024v(C0024v.a.OK, 1));
                        }
                    });
                }
                if (split.length > 1) {
                    String str2 = split[1];
                    final C0017o oVar2 = oVar2;
                    builder.setNeutralButton(str2, new DialogInterface.OnClickListener(this) {
                        public final void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            oVar2.a(new C0024v(C0024v.a.OK, 2));
                        }
                    });
                }
                if (split.length > 2) {
                    String str3 = split[2];
                    final C0017o oVar3 = oVar2;
                    builder.setPositiveButton(str3, new DialogInterface.OnClickListener(this) {
                        public final void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            oVar3.a(new C0024v(C0024v.a.OK, 3));
                        }
                    });
                }
                final C0017o oVar4 = oVar2;
                builder.setOnCancelListener(new DialogInterface.OnCancelListener(this) {
                    public final void onCancel(DialogInterface dialogInterface) {
                        dialogInterface.dismiss();
                        oVar4.a(new C0024v(C0024v.a.OK, 0));
                    }
                });
                builder.create();
                builder.show();
            }
        });
    }

    public synchronized void activityStart(final String str, final String str2) {
        if (this.spinnerDialog != null) {
            this.spinnerDialog.dismiss();
            this.spinnerDialog = null;
        }
        final C0018p pVar = this.cordova;
        this.cordova.a().runOnUiThread(new Runnable() {
            public final void run() {
                Notification.this.spinnerDialog = ProgressDialog.show(pVar.a(), str, str2, true, true, new DialogInterface.OnCancelListener() {
                    public final void onCancel(DialogInterface dialogInterface) {
                        Notification.this.spinnerDialog = null;
                    }
                });
            }
        });
    }

    public synchronized void activityStop() {
        if (this.spinnerDialog != null) {
            this.spinnerDialog.dismiss();
            this.spinnerDialog = null;
        }
    }

    public synchronized void progressStart(String str, String str2) {
        if (this.progressDialog != null) {
            this.progressDialog.dismiss();
            this.progressDialog = null;
        }
        final C0018p pVar = this.cordova;
        final String str3 = str;
        final String str4 = str2;
        this.cordova.a().runOnUiThread(new Runnable(this) {
            public final void run() {
                this.progressDialog = new ProgressDialog(pVar.a());
                this.progressDialog.setProgressStyle(1);
                this.progressDialog.setTitle(str3);
                this.progressDialog.setMessage(str4);
                this.progressDialog.setCancelable(true);
                this.progressDialog.setMax(100);
                this.progressDialog.setProgress(0);
                ProgressDialog progressDialog = this.progressDialog;
                final Notification notification = this;
                progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener(this) {
                    public final void onCancel(DialogInterface dialogInterface) {
                        notification.progressDialog = null;
                    }
                });
                this.progressDialog.show();
            }
        });
    }

    public synchronized void progressValue(int i) {
        if (this.progressDialog != null) {
            this.progressDialog.setProgress(i);
        }
    }

    public synchronized void progressStop() {
        if (this.progressDialog != null) {
            this.progressDialog.dismiss();
            this.progressDialog = null;
        }
    }
}
