package c;

import android.annotation.TargetApi;
import android.os.Build;
import android.text.format.Time;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Currency;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vpadn.C0013k;
import vpadn.C0017o;
import vpadn.C0019q;
import vpadn.C0024v;

public class Globalization extends C0019q {
    public static final String CURRENCY = "currency";
    public static final String CURRENCYCODE = "currencyCode";
    public static final String DATE = "date";
    public static final String DATESTRING = "dateString";
    public static final String DATETOSTRING = "dateToString";
    public static final String DAYS = "days";
    public static final String FORMATLENGTH = "formatLength";
    public static final String FULL = "full";
    public static final String GETCURRENCYPATTERN = "getCurrencyPattern";
    public static final String GETDATENAMES = "getDateNames";
    public static final String GETDATEPATTERN = "getDatePattern";
    public static final String GETFIRSTDAYOFWEEK = "getFirstDayOfWeek";
    public static final String GETLOCALENAME = "getLocaleName";
    public static final String GETNUMBERPATTERN = "getNumberPattern";
    public static final String GETPREFERREDLANGUAGE = "getPreferredLanguage";
    public static final String ISDAYLIGHTSAVINGSTIME = "isDayLightSavingsTime";
    public static final String ITEM = "item";
    public static final String LONG = "long";
    public static final String MEDIUM = "medium";
    public static final String MONTHS = "months";
    public static final String NARROW = "narrow";
    public static final String NUMBER = "number";
    public static final String NUMBERSTRING = "numberString";
    public static final String NUMBERTOSTRING = "numberToString";
    public static final String OPTIONS = "options";
    public static final String PERCENT = "percent";
    public static final String SELECTOR = "selector";
    public static final String STRINGTODATE = "stringToDate";
    public static final String STRINGTONUMBER = "stringToNumber";
    public static final String TIME = "time";
    public static final String TYPE = "type";
    public static final String WIDE = "wide";

    public boolean execute(String str, JSONArray jSONArray, C0017o oVar) {
        JSONObject i;
        new JSONObject();
        try {
            if (str.equals(GETLOCALENAME)) {
                i = a();
            } else if (str.equals(GETPREFERREDLANGUAGE)) {
                i = b();
            } else if (str.equalsIgnoreCase(DATETOSTRING)) {
                i = a(jSONArray);
            } else if (str.equalsIgnoreCase(STRINGTODATE)) {
                i = b(jSONArray);
            } else if (str.equalsIgnoreCase(GETDATEPATTERN)) {
                i = c(jSONArray);
            } else if (str.equalsIgnoreCase(GETDATENAMES)) {
                if (Build.VERSION.SDK_INT < 9) {
                    throw new C0013k("UNKNOWN_ERROR");
                }
                i = d(jSONArray);
            } else if (str.equalsIgnoreCase(ISDAYLIGHTSAVINGSTIME)) {
                i = e(jSONArray);
            } else if (str.equalsIgnoreCase(GETFIRSTDAYOFWEEK)) {
                i = c();
            } else if (str.equalsIgnoreCase(NUMBERTOSTRING)) {
                i = f(jSONArray);
            } else if (str.equalsIgnoreCase(STRINGTONUMBER)) {
                i = g(jSONArray);
            } else if (str.equalsIgnoreCase(GETNUMBERPATTERN)) {
                i = h(jSONArray);
            } else if (!str.equalsIgnoreCase(GETCURRENCYPATTERN)) {
                return false;
            } else {
                i = i(jSONArray);
            }
            oVar.a(i);
        } catch (C0013k e) {
            oVar.a(new C0024v(C0024v.a.ERROR, e.a()));
        } catch (Exception e2) {
            oVar.a(new C0024v(C0024v.a.JSON_EXCEPTION));
        }
        return true;
    }

    private static JSONObject a() throws C0013k {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("value", Locale.getDefault().toString());
            return jSONObject;
        } catch (Exception e) {
            throw new C0013k("UNKNOWN_ERROR");
        }
    }

    private static JSONObject b() throws C0013k {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("value", Locale.getDefault().getDisplayLanguage().toString());
            return jSONObject;
        } catch (Exception e) {
            throw new C0013k("UNKNOWN_ERROR");
        }
    }

    private JSONObject a(JSONArray jSONArray) throws C0013k {
        try {
            return new JSONObject().put("value", new SimpleDateFormat(c(jSONArray).getString("pattern")).format(new Date(((Long) jSONArray.getJSONObject(0).get(DATE)).longValue())));
        } catch (Exception e) {
            throw new C0013k("FORMATTING_ERROR");
        }
    }

    private JSONObject b(JSONArray jSONArray) throws C0013k {
        JSONObject jSONObject = new JSONObject();
        try {
            Date parse = new SimpleDateFormat(c(jSONArray).getString("pattern")).parse(jSONArray.getJSONObject(0).get(DATESTRING).toString());
            Time time = new Time();
            time.set(parse.getTime());
            jSONObject.put("year", time.year);
            jSONObject.put("month", time.month);
            jSONObject.put("day", time.monthDay);
            jSONObject.put("hour", time.hour);
            jSONObject.put("minute", time.minute);
            jSONObject.put("second", time.second);
            jSONObject.put("millisecond", new Long(0));
            return jSONObject;
        } catch (Exception e) {
            throw new C0013k("PARSING_ERROR");
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x00bb A[Catch:{ Exception -> 0x0142 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private org.json.JSONObject c(org.json.JSONArray r7) throws vpadn.C0013k {
        /*
            r6 = this;
            org.json.JSONObject r4 = new org.json.JSONObject
            r4.<init>()
            vpadn.p r0 = r6.cordova     // Catch:{ Exception -> 0x0142 }
            android.app.Activity r0 = r0.a()     // Catch:{ Exception -> 0x0142 }
            java.text.DateFormat r0 = android.text.format.DateFormat.getDateFormat(r0)     // Catch:{ Exception -> 0x0142 }
            java.text.SimpleDateFormat r0 = (java.text.SimpleDateFormat) r0     // Catch:{ Exception -> 0x0142 }
            vpadn.p r1 = r6.cordova     // Catch:{ Exception -> 0x0142 }
            android.app.Activity r1 = r1.a()     // Catch:{ Exception -> 0x0142 }
            java.text.DateFormat r1 = android.text.format.DateFormat.getTimeFormat(r1)     // Catch:{ Exception -> 0x0142 }
            java.text.SimpleDateFormat r1 = (java.text.SimpleDateFormat) r1     // Catch:{ Exception -> 0x0142 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0142 }
            java.lang.String r3 = r0.toLocalizedPattern()     // Catch:{ Exception -> 0x0142 }
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ Exception -> 0x0142 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0142 }
            java.lang.String r3 = " "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0142 }
            java.lang.String r3 = r1.toLocalizedPattern()     // Catch:{ Exception -> 0x0142 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0142 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0142 }
            r3 = 0
            org.json.JSONObject r3 = r7.getJSONObject(r3)     // Catch:{ Exception -> 0x0142 }
            int r3 = r3.length()     // Catch:{ Exception -> 0x0142 }
            r5 = 1
            if (r3 <= r5) goto L_0x014b
            r2 = 0
            org.json.JSONObject r2 = r7.getJSONObject(r2)     // Catch:{ Exception -> 0x0142 }
            java.lang.String r3 = "options"
            java.lang.Object r2 = r2.get(r3)     // Catch:{ Exception -> 0x0142 }
            org.json.JSONObject r2 = (org.json.JSONObject) r2     // Catch:{ Exception -> 0x0142 }
            java.lang.String r3 = "formatLength"
            boolean r2 = r2.isNull(r3)     // Catch:{ Exception -> 0x0142 }
            if (r2 != 0) goto L_0x014d
            r2 = 0
            org.json.JSONObject r2 = r7.getJSONObject(r2)     // Catch:{ Exception -> 0x0142 }
            java.lang.String r3 = "options"
            java.lang.Object r2 = r2.get(r3)     // Catch:{ Exception -> 0x0142 }
            org.json.JSONObject r2 = (org.json.JSONObject) r2     // Catch:{ Exception -> 0x0142 }
            java.lang.String r3 = "formatLength"
            java.lang.Object r2 = r2.get(r3)     // Catch:{ Exception -> 0x0142 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ Exception -> 0x0142 }
            java.lang.String r3 = "medium"
            boolean r3 = r2.equalsIgnoreCase(r3)     // Catch:{ Exception -> 0x0142 }
            if (r3 == 0) goto L_0x0116
            vpadn.p r0 = r6.cordova     // Catch:{ Exception -> 0x0142 }
            android.app.Activity r0 = r0.a()     // Catch:{ Exception -> 0x0142 }
            java.text.DateFormat r0 = android.text.format.DateFormat.getMediumDateFormat(r0)     // Catch:{ Exception -> 0x0142 }
            java.text.SimpleDateFormat r0 = (java.text.SimpleDateFormat) r0     // Catch:{ Exception -> 0x0142 }
            r3 = r0
        L_0x0087:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0142 }
            java.lang.String r2 = r3.toLocalizedPattern()     // Catch:{ Exception -> 0x0142 }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ Exception -> 0x0142 }
            r0.<init>(r2)     // Catch:{ Exception -> 0x0142 }
            java.lang.String r2 = " "
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0142 }
            java.lang.String r2 = r1.toLocalizedPattern()     // Catch:{ Exception -> 0x0142 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0142 }
            java.lang.String r2 = r0.toString()     // Catch:{ Exception -> 0x0142 }
            r0 = 0
            org.json.JSONObject r0 = r7.getJSONObject(r0)     // Catch:{ Exception -> 0x0142 }
            java.lang.String r5 = "options"
            java.lang.Object r0 = r0.get(r5)     // Catch:{ Exception -> 0x0142 }
            org.json.JSONObject r0 = (org.json.JSONObject) r0     // Catch:{ Exception -> 0x0142 }
            java.lang.String r5 = "selector"
            boolean r0 = r0.isNull(r5)     // Catch:{ Exception -> 0x0142 }
            if (r0 != 0) goto L_0x014b
            r0 = 0
            org.json.JSONObject r0 = r7.getJSONObject(r0)     // Catch:{ Exception -> 0x0142 }
            java.lang.String r5 = "options"
            java.lang.Object r0 = r0.get(r5)     // Catch:{ Exception -> 0x0142 }
            org.json.JSONObject r0 = (org.json.JSONObject) r0     // Catch:{ Exception -> 0x0142 }
            java.lang.String r5 = "selector"
            java.lang.Object r0 = r0.get(r5)     // Catch:{ Exception -> 0x0142 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0142 }
            java.lang.String r5 = "date"
            boolean r5 = r0.equalsIgnoreCase(r5)     // Catch:{ Exception -> 0x0142 }
            if (r5 == 0) goto L_0x0135
            java.lang.String r0 = r3.toLocalizedPattern()     // Catch:{ Exception -> 0x0142 }
        L_0x00dc:
            java.lang.String r1 = android.text.format.Time.getCurrentTimezone()     // Catch:{ Exception -> 0x0142 }
            java.util.TimeZone r1 = java.util.TimeZone.getTimeZone(r1)     // Catch:{ Exception -> 0x0142 }
            java.lang.String r2 = "pattern"
            r4.put(r2, r0)     // Catch:{ Exception -> 0x0142 }
            java.lang.String r0 = "timezone"
            java.util.Calendar r2 = java.util.Calendar.getInstance()     // Catch:{ Exception -> 0x0142 }
            java.util.Date r2 = r2.getTime()     // Catch:{ Exception -> 0x0142 }
            boolean r2 = r1.inDaylightTime(r2)     // Catch:{ Exception -> 0x0142 }
            r3 = 0
            java.lang.String r2 = r1.getDisplayName(r2, r3)     // Catch:{ Exception -> 0x0142 }
            r4.put(r0, r2)     // Catch:{ Exception -> 0x0142 }
            java.lang.String r0 = "utc_offset"
            int r2 = r1.getRawOffset()     // Catch:{ Exception -> 0x0142 }
            int r2 = r2 / 1000
            r4.put(r0, r2)     // Catch:{ Exception -> 0x0142 }
            java.lang.String r0 = "dst_offset"
            int r1 = r1.getDSTSavings()     // Catch:{ Exception -> 0x0142 }
            int r1 = r1 / 1000
            r4.put(r0, r1)     // Catch:{ Exception -> 0x0142 }
            return r4
        L_0x0116:
            java.lang.String r3 = "long"
            boolean r3 = r2.equalsIgnoreCase(r3)     // Catch:{ Exception -> 0x0142 }
            if (r3 != 0) goto L_0x0126
            java.lang.String r3 = "full"
            boolean r2 = r2.equalsIgnoreCase(r3)     // Catch:{ Exception -> 0x0142 }
            if (r2 == 0) goto L_0x014d
        L_0x0126:
            vpadn.p r0 = r6.cordova     // Catch:{ Exception -> 0x0142 }
            android.app.Activity r0 = r0.a()     // Catch:{ Exception -> 0x0142 }
            java.text.DateFormat r0 = android.text.format.DateFormat.getLongDateFormat(r0)     // Catch:{ Exception -> 0x0142 }
            java.text.SimpleDateFormat r0 = (java.text.SimpleDateFormat) r0     // Catch:{ Exception -> 0x0142 }
            r3 = r0
            goto L_0x0087
        L_0x0135:
            java.lang.String r3 = "time"
            boolean r0 = r0.equalsIgnoreCase(r3)     // Catch:{ Exception -> 0x0142 }
            if (r0 == 0) goto L_0x014b
            java.lang.String r0 = r1.toLocalizedPattern()     // Catch:{ Exception -> 0x0142 }
            goto L_0x00dc
        L_0x0142:
            r0 = move-exception
            vpadn.k r0 = new vpadn.k
            java.lang.String r1 = "PATTERN_ERROR"
            r0.<init>(r1)
            throw r0
        L_0x014b:
            r0 = r2
            goto L_0x00dc
        L_0x014d:
            r3 = r0
            goto L_0x0087
        */
        throw new UnsupportedOperationException("Method not decompiled: c.Globalization.c(org.json.JSONArray):org.json.JSONObject");
    }

    @TargetApi(9)
    private JSONObject d(JSONArray jSONArray) throws C0013k {
        int i;
        int i2;
        final Map<String, Integer> displayNames;
        JSONObject jSONObject = new JSONObject();
        JSONArray jSONArray2 = new JSONArray();
        ArrayList arrayList = new ArrayList();
        try {
            if (jSONArray.getJSONObject(0).length() > 0) {
                if (((JSONObject) jSONArray.getJSONObject(0).get(OPTIONS)).isNull(TYPE) || !((String) ((JSONObject) jSONArray.getJSONObject(0).get(OPTIONS)).get(TYPE)).equalsIgnoreCase(NARROW)) {
                    i2 = 0;
                } else {
                    i2 = 1;
                }
                i = (((JSONObject) jSONArray.getJSONObject(0).get(OPTIONS)).isNull("item") || !((String) ((JSONObject) jSONArray.getJSONObject(0).get(OPTIONS)).get("item")).equalsIgnoreCase(DAYS)) ? 0 : 10;
            } else {
                i = 0;
                i2 = 0;
            }
            int i3 = i + i2;
            if (i3 == 1) {
                displayNames = Calendar.getInstance().getDisplayNames(2, 1, Locale.getDefault());
            } else if (i3 == 10) {
                displayNames = Calendar.getInstance().getDisplayNames(7, 2, Locale.getDefault());
            } else if (i3 == 11) {
                displayNames = Calendar.getInstance().getDisplayNames(7, 1, Locale.getDefault());
            } else {
                displayNames = Calendar.getInstance().getDisplayNames(2, 2, Locale.getDefault());
            }
            for (String add : displayNames.keySet()) {
                arrayList.add(add);
            }
            Collections.sort(arrayList, new Comparator<String>(this) {
                public final /* synthetic */ int compare(Object obj, Object obj2) {
                    return ((Integer) displayNames.get((String) obj)).compareTo((Integer) displayNames.get((String) obj2));
                }
            });
            for (int i4 = 0; i4 < arrayList.size(); i4++) {
                jSONArray2.put(arrayList.get(i4));
            }
            return jSONObject.put("value", jSONArray2);
        } catch (Exception e) {
            throw new C0013k("UNKNOWN_ERROR");
        }
    }

    private static JSONObject e(JSONArray jSONArray) throws C0013k {
        try {
            return new JSONObject().put("dst", TimeZone.getTimeZone(Time.getCurrentTimezone()).inDaylightTime(new Date(((Long) jSONArray.getJSONObject(0).get(DATE)).longValue())));
        } catch (Exception e) {
            throw new C0013k("UNKNOWN_ERROR");
        }
    }

    private static JSONObject c() throws C0013k {
        try {
            return new JSONObject().put("value", Calendar.getInstance(Locale.getDefault()).getFirstDayOfWeek());
        } catch (Exception e) {
            throw new C0013k("UNKNOWN_ERROR");
        }
    }

    private JSONObject f(JSONArray jSONArray) throws C0013k {
        try {
            return new JSONObject().put("value", j(jSONArray).format(jSONArray.getJSONObject(0).get(NUMBER)));
        } catch (Exception e) {
            throw new C0013k("FORMATTING_ERROR");
        }
    }

    private JSONObject g(JSONArray jSONArray) throws C0013k {
        try {
            return new JSONObject().put("value", j(jSONArray).parse((String) jSONArray.getJSONObject(0).get(NUMBERSTRING)));
        } catch (Exception e) {
            throw new C0013k("PARSING_ERROR");
        }
    }

    private static JSONObject h(JSONArray jSONArray) throws C0013k {
        DecimalFormat decimalFormat;
        String str;
        JSONObject jSONObject = new JSONObject();
        try {
            DecimalFormat decimalFormat2 = (DecimalFormat) DecimalFormat.getInstance(Locale.getDefault());
            String valueOf = String.valueOf(decimalFormat2.getDecimalFormatSymbols().getDecimalSeparator());
            if (jSONArray.getJSONObject(0).length() > 0 && !((JSONObject) jSONArray.getJSONObject(0).get(OPTIONS)).isNull(TYPE)) {
                String str2 = (String) ((JSONObject) jSONArray.getJSONObject(0).get(OPTIONS)).get(TYPE);
                if (str2.equalsIgnoreCase(CURRENCY)) {
                    DecimalFormat decimalFormat3 = (DecimalFormat) DecimalFormat.getCurrencyInstance(Locale.getDefault());
                    decimalFormat = decimalFormat3;
                    str = decimalFormat3.getDecimalFormatSymbols().getCurrencySymbol();
                } else if (str2.equalsIgnoreCase(PERCENT)) {
                    DecimalFormat decimalFormat4 = (DecimalFormat) DecimalFormat.getPercentInstance(Locale.getDefault());
                    decimalFormat = decimalFormat4;
                    str = String.valueOf(decimalFormat4.getDecimalFormatSymbols().getPercent());
                }
                jSONObject.put("pattern", decimalFormat.toPattern());
                jSONObject.put("symbol", str);
                jSONObject.put("fraction", decimalFormat.getMinimumFractionDigits());
                jSONObject.put("rounding", new Integer(0));
                jSONObject.put("positive", decimalFormat.getPositivePrefix());
                jSONObject.put("negative", decimalFormat.getNegativePrefix());
                jSONObject.put("decimal", String.valueOf(decimalFormat.getDecimalFormatSymbols().getDecimalSeparator()));
                jSONObject.put("grouping", String.valueOf(decimalFormat.getDecimalFormatSymbols().getGroupingSeparator()));
                return jSONObject;
            }
            decimalFormat = decimalFormat2;
            str = valueOf;
            jSONObject.put("pattern", decimalFormat.toPattern());
            jSONObject.put("symbol", str);
            jSONObject.put("fraction", decimalFormat.getMinimumFractionDigits());
            jSONObject.put("rounding", new Integer(0));
            jSONObject.put("positive", decimalFormat.getPositivePrefix());
            jSONObject.put("negative", decimalFormat.getNegativePrefix());
            jSONObject.put("decimal", String.valueOf(decimalFormat.getDecimalFormatSymbols().getDecimalSeparator()));
            jSONObject.put("grouping", String.valueOf(decimalFormat.getDecimalFormatSymbols().getGroupingSeparator()));
            return jSONObject;
        } catch (Exception e) {
            throw new C0013k("PATTERN_ERROR");
        }
    }

    private static JSONObject i(JSONArray jSONArray) throws C0013k {
        JSONObject jSONObject = new JSONObject();
        try {
            String string = jSONArray.getJSONObject(0).getString(CURRENCYCODE);
            DecimalFormat decimalFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(Locale.getDefault());
            Currency instance = Currency.getInstance(string);
            decimalFormat.setCurrency(instance);
            jSONObject.put("pattern", decimalFormat.toPattern());
            jSONObject.put("code", instance.getCurrencyCode());
            jSONObject.put("fraction", decimalFormat.getMinimumFractionDigits());
            jSONObject.put("rounding", new Integer(0));
            jSONObject.put("decimal", String.valueOf(decimalFormat.getDecimalFormatSymbols().getDecimalSeparator()));
            jSONObject.put("grouping", String.valueOf(decimalFormat.getDecimalFormatSymbols().getGroupingSeparator()));
            return jSONObject;
        } catch (Exception e) {
            throw new C0013k("FORMATTING_ERROR");
        }
    }

    private static DecimalFormat j(JSONArray jSONArray) throws JSONException {
        DecimalFormat decimalFormat = (DecimalFormat) DecimalFormat.getInstance(Locale.getDefault());
        try {
            if (jSONArray.getJSONObject(0).length() <= 1 || ((JSONObject) jSONArray.getJSONObject(0).get(OPTIONS)).isNull(TYPE)) {
                return decimalFormat;
            }
            String str = (String) ((JSONObject) jSONArray.getJSONObject(0).get(OPTIONS)).get(TYPE);
            if (str.equalsIgnoreCase(CURRENCY)) {
                return (DecimalFormat) DecimalFormat.getCurrencyInstance(Locale.getDefault());
            }
            if (str.equalsIgnoreCase(PERCENT)) {
                return (DecimalFormat) DecimalFormat.getPercentInstance(Locale.getDefault());
            }
            return decimalFormat;
        } catch (JSONException e) {
            return decimalFormat;
        }
    }
}
