package c;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import java.util.HashMap;
import java.util.StringTokenizer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vpadn.C0004b;
import vpadn.C0017o;
import vpadn.C0019q;
import vpadn.C0020r;
import vpadn.C0024v;

@SuppressLint({"SetJavaScriptEnabled"})
public class InAppBrowser extends C0019q {
    protected static final String LOG_TAG = "InAppBrowser";
    /* access modifiers changed from: private */
    public long a = 104857600;
    /* access modifiers changed from: private */
    public Dialog b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public WebView f19c;
    /* access modifiers changed from: private */
    public EditText d;
    /* access modifiers changed from: private */
    public boolean e = true;
    private C0017o f;

    public boolean execute(String str, JSONArray jSONArray, C0017o oVar) throws JSONException {
        C0024v.a aVar;
        String str2;
        String str3;
        HashMap hashMap;
        Boolean bool;
        String str4;
        C0024v.a aVar2 = C0024v.a.OK;
        this.f = oVar;
        try {
            if (str.equals("open")) {
                String string = jSONArray.getString(0);
                String optString = jSONArray.optString(1);
                if (optString == null || optString.equals("") || optString.equals("null")) {
                    str3 = "_self";
                } else {
                    str3 = optString;
                }
                String optString2 = jSONArray.optString(2);
                if (optString2.equals("null")) {
                    hashMap = null;
                } else {
                    HashMap hashMap2 = new HashMap();
                    StringTokenizer stringTokenizer = new StringTokenizer(optString2, ",");
                    while (stringTokenizer.hasMoreElements()) {
                        StringTokenizer stringTokenizer2 = new StringTokenizer(stringTokenizer.nextToken(), "=");
                        if (stringTokenizer2.hasMoreElements()) {
                            String nextToken = stringTokenizer2.nextToken();
                            if (stringTokenizer2.nextToken().equals("no")) {
                                bool = Boolean.FALSE;
                            } else {
                                bool = Boolean.TRUE;
                            }
                            hashMap2.put(nextToken, bool);
                        }
                    }
                    hashMap = hashMap2;
                }
                Log.d(LOG_TAG, "target = " + str3);
                if (Uri.parse(string).isRelative()) {
                    str4 = String.valueOf(this.webView.getUrl().substring(0, this.webView.getUrl().lastIndexOf("/") + 1)) + string;
                } else {
                    str4 = string;
                }
                if ("_self".equals(str3)) {
                    Log.d(LOG_TAG, "in self");
                    if (str4.startsWith("file://") || str4.startsWith("javascript:") || C0004b.a(str4)) {
                        this.webView.loadUrl(str4);
                        str2 = "";
                        aVar = aVar2;
                    } else if (str4.startsWith("tel:")) {
                        try {
                            Intent intent = new Intent("android.intent.action.DIAL");
                            intent.setData(Uri.parse(str4));
                            this.cordova.a().startActivity(intent);
                            str2 = "";
                            aVar = aVar2;
                        } catch (ActivityNotFoundException e2) {
                            C0020r.e(LOG_TAG, "Error dialing " + str4 + ": " + e2.toString());
                            str2 = "";
                            aVar = aVar2;
                        }
                    } else {
                        str2 = showWebPage(str4, hashMap);
                        aVar = aVar2;
                    }
                } else if ("_system".equals(str3)) {
                    Log.d(LOG_TAG, "in system");
                    str2 = openExternal(str4);
                    aVar = aVar2;
                } else {
                    Log.d(LOG_TAG, "in blank");
                    str2 = showWebPage(str4, hashMap);
                    aVar = aVar2;
                }
            } else if (str.equals("close")) {
                a();
                C0024v vVar = new C0024v(C0024v.a.OK);
                vVar.a(false);
                this.f.a(vVar);
                str2 = "";
                aVar = aVar2;
            } else {
                aVar = C0024v.a.INVALID_ACTION;
                str2 = "";
            }
            C0024v vVar2 = new C0024v(aVar, str2);
            vVar2.a(true);
            this.f.a(vVar2);
        } catch (JSONException e3) {
            this.f.a(new C0024v(C0024v.a.JSON_EXCEPTION));
        }
        return true;
    }

    public String openExternal(String str) {
        try {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setData(Uri.parse(str));
            this.cordova.a().startActivity(intent);
            return "";
        } catch (ActivityNotFoundException e2) {
            Log.d(LOG_TAG, "InAppBrowser: Error loading url " + str + ":" + e2.toString());
            return e2.toString();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c.InAppBrowser.a(org.json.JSONObject, boolean):void
     arg types: [org.json.JSONObject, int]
     candidates:
      c.InAppBrowser.a(c.InAppBrowser, android.app.Dialog):void
      c.InAppBrowser.a(c.InAppBrowser, android.webkit.WebView):void
      c.InAppBrowser.a(c.InAppBrowser, android.widget.EditText):void
      c.InAppBrowser.a(c.InAppBrowser, java.lang.String):void
      c.InAppBrowser.a(org.json.JSONObject, boolean):void */
    /* access modifiers changed from: private */
    public void a() {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put(Globalization.TYPE, "exit");
            a(jSONObject, false);
        } catch (JSONException e2) {
            Log.d(LOG_TAG, "Should never happen");
        }
        if (this.b != null) {
            this.b.dismiss();
        }
    }

    static /* synthetic */ void c(InAppBrowser inAppBrowser) {
        if (inAppBrowser.f19c.canGoBack()) {
            inAppBrowser.f19c.goBack();
        }
    }

    static /* synthetic */ void d(InAppBrowser inAppBrowser) {
        if (inAppBrowser.f19c.canGoForward()) {
            inAppBrowser.f19c.goForward();
        }
    }

    static /* synthetic */ void a(InAppBrowser inAppBrowser, String str) {
        ((InputMethodManager) inAppBrowser.cordova.a().getSystemService("input_method")).hideSoftInputFromWindow(inAppBrowser.d.getWindowToken(), 0);
        if (str.startsWith("http") || str.startsWith("file:")) {
            inAppBrowser.f19c.loadUrl(str);
        } else {
            inAppBrowser.f19c.loadUrl("http://" + str);
        }
        inAppBrowser.f19c.requestFocus();
    }

    public String showWebPage(final String str, HashMap<String, Boolean> hashMap) {
        this.e = true;
        if (hashMap != null) {
            this.e = hashMap.get("location").booleanValue();
        }
        final CordovaWebView cordovaWebView = this.webView;
        this.cordova.a().runOnUiThread(new Runnable() {
            private int a(int i) {
                return (int) TypedValue.applyDimension(1, (float) i, InAppBrowser.this.cordova.a().getResources().getDisplayMetrics());
            }

            public final void run() {
                InAppBrowser.this.b = new Dialog(InAppBrowser.this.cordova.a(), 16973830);
                InAppBrowser.this.b.getWindow().getAttributes().windowAnimations = 16973826;
                InAppBrowser.this.b.requestWindowFeature(1);
                InAppBrowser.this.b.setCancelable(true);
                InAppBrowser.this.b.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    public final void onDismiss(DialogInterface dialogInterface) {
                        try {
                            JSONObject jSONObject = new JSONObject();
                            jSONObject.put(Globalization.TYPE, "exit");
                            InAppBrowser.this.a(jSONObject, false);
                        } catch (JSONException e) {
                            Log.d(InAppBrowser.LOG_TAG, "Should never happen");
                        }
                    }
                });
                LinearLayout linearLayout = new LinearLayout(InAppBrowser.this.cordova.a());
                linearLayout.setOrientation(1);
                RelativeLayout relativeLayout = new RelativeLayout(InAppBrowser.this.cordova.a());
                relativeLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, a(44)));
                relativeLayout.setPadding(a(2), a(2), a(2), a(2));
                relativeLayout.setHorizontalGravity(3);
                relativeLayout.setVerticalGravity(48);
                RelativeLayout relativeLayout2 = new RelativeLayout(InAppBrowser.this.cordova.a());
                relativeLayout2.setLayoutParams(new RelativeLayout.LayoutParams(-2, -2));
                relativeLayout2.setHorizontalGravity(3);
                relativeLayout2.setVerticalGravity(16);
                relativeLayout2.setId(1);
                Button button = new Button(InAppBrowser.this.cordova.a());
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -1);
                layoutParams.addRule(5);
                button.setLayoutParams(layoutParams);
                button.setContentDescription("Back Button");
                button.setId(2);
                button.setText("<");
                button.setOnClickListener(new View.OnClickListener() {
                    public final void onClick(View view) {
                        InAppBrowser.c(InAppBrowser.this);
                    }
                });
                Button button2 = new Button(InAppBrowser.this.cordova.a());
                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -1);
                layoutParams2.addRule(1, 2);
                button2.setLayoutParams(layoutParams2);
                button2.setContentDescription("Forward Button");
                button2.setId(3);
                button2.setText(">");
                button2.setOnClickListener(new View.OnClickListener() {
                    public final void onClick(View view) {
                        InAppBrowser.d(InAppBrowser.this);
                    }
                });
                InAppBrowser.this.d = new EditText(InAppBrowser.this.cordova.a());
                RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -1);
                layoutParams3.addRule(1, 1);
                layoutParams3.addRule(0, 5);
                InAppBrowser.this.d.setLayoutParams(layoutParams3);
                InAppBrowser.this.d.setId(4);
                InAppBrowser.this.d.setSingleLine(true);
                InAppBrowser.this.d.setText(str);
                InAppBrowser.this.d.setInputType(16);
                InAppBrowser.this.d.setImeOptions(2);
                InAppBrowser.this.d.setInputType(0);
                InAppBrowser.this.d.setOnKeyListener(new View.OnKeyListener() {
                    public final boolean onKey(View view, int i, KeyEvent keyEvent) {
                        if (keyEvent.getAction() != 0 || i != 66) {
                            return false;
                        }
                        InAppBrowser.a(InAppBrowser.this, InAppBrowser.this.d.getText().toString());
                        return true;
                    }
                });
                Button button3 = new Button(InAppBrowser.this.cordova.a());
                RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -1);
                layoutParams4.addRule(11);
                button3.setLayoutParams(layoutParams4);
                button2.setContentDescription("Close Button");
                button3.setId(5);
                button3.setText("Done");
                button3.setOnClickListener(new View.OnClickListener() {
                    public final void onClick(View view) {
                        InAppBrowser.this.a();
                    }
                });
                InAppBrowser.this.f19c = new WebView(InAppBrowser.this.cordova.a());
                InAppBrowser.this.f19c.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
                InAppBrowser.this.f19c.setWebChromeClient(new b());
                InAppBrowser.this.f19c.setWebViewClient(new a(cordovaWebView, InAppBrowser.this.d));
                WebSettings settings = InAppBrowser.this.f19c.getSettings();
                settings.setJavaScriptEnabled(true);
                settings.setJavaScriptCanOpenWindowsAutomatically(true);
                settings.setBuiltInZoomControls(true);
                settings.setPluginsEnabled(true);
                settings.setDatabaseEnabled(true);
                settings.setDatabasePath(InAppBrowser.this.cordova.a().getApplicationContext().getDir("inAppBrowserDB", 0).getPath());
                settings.setDomStorageEnabled(true);
                InAppBrowser.this.f19c.loadUrl(str);
                InAppBrowser.this.f19c.setId(6);
                InAppBrowser.this.f19c.getSettings().setLoadWithOverviewMode(true);
                InAppBrowser.this.f19c.getSettings().setUseWideViewPort(true);
                InAppBrowser.this.f19c.requestFocus();
                InAppBrowser.this.f19c.requestFocusFromTouch();
                relativeLayout2.addView(button);
                relativeLayout2.addView(button2);
                relativeLayout.addView(relativeLayout2);
                relativeLayout.addView(InAppBrowser.this.d);
                relativeLayout.addView(button3);
                if (InAppBrowser.this.e) {
                    linearLayout.addView(relativeLayout);
                }
                linearLayout.addView(InAppBrowser.this.f19c);
                WindowManager.LayoutParams layoutParams5 = new WindowManager.LayoutParams();
                layoutParams5.copyFrom(InAppBrowser.this.b.getWindow().getAttributes());
                layoutParams5.width = -1;
                layoutParams5.height = -1;
                InAppBrowser.this.b.setContentView(linearLayout);
                InAppBrowser.this.b.show();
                InAppBrowser.this.b.getWindow().setAttributes(layoutParams5);
            }
        });
        return "";
    }

    /* access modifiers changed from: private */
    public void a(JSONObject jSONObject, boolean z) {
        C0024v vVar = new C0024v(C0024v.a.OK, jSONObject);
        vVar.a(z);
        this.f.a(vVar);
    }

    public class b extends WebChromeClient {
        public b() {
        }

        public final void onExceededDatabaseQuota(String str, String str2, long j, long j2, long j3, WebStorage.QuotaUpdater quotaUpdater) {
            C0020r.b(InAppBrowser.LOG_TAG, "onExceededDatabaseQuota estimatedSize: %d  currentQuota: %d  totalUsedQuota: %d", Long.valueOf(j2), Long.valueOf(j), Long.valueOf(j3));
            if (j2 < InAppBrowser.this.a) {
                C0020r.b(InAppBrowser.LOG_TAG, "calling quotaUpdater.updateQuota newQuota: %d", Long.valueOf(j2));
                quotaUpdater.updateQuota(j2);
                return;
            }
            quotaUpdater.updateQuota(j);
        }
    }

    public class a extends WebViewClient {
        private EditText a;

        public a(CordovaWebView cordovaWebView, EditText editText) {
            this.a = editText;
        }

        public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            super.onPageStarted(webView, str, bitmap);
            if (!str.startsWith("http:") && !str.startsWith("https:") && !str.startsWith("file:")) {
                str = "http://" + str;
            }
            if (!str.equals(this.a.getText().toString())) {
                this.a.setText(str);
            }
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put(Globalization.TYPE, "loadstart");
                jSONObject.put("url", str);
                InAppBrowser.this.a(jSONObject, true);
            } catch (JSONException e) {
                Log.d(InAppBrowser.LOG_TAG, "Should never happen");
            }
        }

        public final void onPageFinished(WebView webView, String str) {
            super.onPageFinished(webView, str);
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put(Globalization.TYPE, "loadstop");
                jSONObject.put("url", str);
                InAppBrowser.this.a(jSONObject, true);
            } catch (JSONException e) {
                Log.d(InAppBrowser.LOG_TAG, "Should never happen");
            }
        }
    }
}
