package c;

import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vpadn.C0017o;
import vpadn.C0019q;
import vpadn.C0020r;
import vpadn.C0024v;

public class App extends C0019q {
    public boolean execute(String str, JSONArray jSONArray, C0017o oVar) throws JSONException {
        C0024v.a aVar = C0024v.a.OK;
        try {
            if (str.equals("clearCache")) {
                clearCache();
            } else if (str.equals("show")) {
                this.cordova.a().runOnUiThread(new Runnable() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: c.CordovaWebView.a(java.lang.String, java.lang.Object):void
                     arg types: [java.lang.String, java.lang.String]
                     candidates:
                      c.CordovaWebView.a(java.lang.String, java.lang.String):java.lang.String
                      c.CordovaWebView.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
                      c.CordovaWebView.a(vpadn.v, java.lang.String):void
                      c.CordovaWebView.a(boolean, boolean):void
                      c.CordovaWebView.a(java.lang.String, java.lang.Object):void */
                    public final void run() {
                        App.this.webView.a("spinner", (Object) "stop");
                    }
                });
            } else if (str.equals("loadUrl")) {
                loadUrl(jSONArray.getString(0), jSONArray.optJSONObject(1));
            } else if (!str.equals("cancelLoadUrl")) {
                if (str.equals("clearHistory")) {
                    clearHistory();
                } else if (str.equals("backHistory")) {
                    backHistory();
                } else if (str.equals("overrideButton")) {
                    overrideButton(jSONArray.getString(0), jSONArray.getBoolean(1));
                } else if (str.equals("overrideBackbutton")) {
                    overrideBackbutton(jSONArray.getBoolean(0));
                } else if (str.equals("exitApp")) {
                    exitApp();
                }
            }
            oVar.a(new C0024v(aVar, ""));
            return true;
        } catch (JSONException e) {
            oVar.a(new C0024v(C0024v.a.JSON_EXCEPTION));
            return false;
        }
    }

    public void clearCache() {
        this.webView.clearCache(true);
    }

    public void loadUrl(String str, JSONObject jSONObject) throws JSONException {
        boolean z;
        boolean z2;
        int i;
        C0020r.b("App", "App.loadUrl(" + str + "," + jSONObject + ")");
        HashMap hashMap = new HashMap();
        if (jSONObject != null) {
            JSONArray names = jSONObject.names();
            z = false;
            z2 = false;
            i = 0;
            for (int i2 = 0; i2 < names.length(); i2++) {
                String string = names.getString(i2);
                if (string.equals("wait")) {
                    i = jSONObject.getInt(string);
                } else if (string.equalsIgnoreCase("openexternal")) {
                    z2 = jSONObject.getBoolean(string);
                } else if (string.equalsIgnoreCase("clearhistory")) {
                    z = jSONObject.getBoolean(string);
                } else {
                    Object obj = jSONObject.get(string);
                    if (obj != null) {
                        if (obj.getClass().equals(String.class)) {
                            hashMap.put(string, (String) obj);
                        } else if (obj.getClass().equals(Boolean.class)) {
                            hashMap.put(string, (Boolean) obj);
                        } else if (obj.getClass().equals(Integer.class)) {
                            hashMap.put(string, (Integer) obj);
                        }
                    }
                }
            }
        } else {
            z = false;
            z2 = false;
            i = 0;
        }
        if (i > 0) {
            try {
                synchronized (this) {
                    wait((long) i);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        this.webView.a(str, z2, z);
    }

    public void clearHistory() {
        this.webView.clearHistory();
    }

    public void backHistory() {
        this.cordova.a().runOnUiThread(new Runnable() {
            public final void run() {
                App.this.webView.c();
            }
        });
    }

    public void overrideBackbutton(boolean z) {
        C0020r.c("App", "WARNING: Back Button Default Behaviour will be overridden.  The backbutton event will be fired!");
        this.webView.a(z);
    }

    public void overrideButton(String str, boolean z) {
        C0020r.c("DroidGap", "WARNING: Volume Button Default Behaviour will be overridden.  The volume event will be fired!");
        this.webView.e(str);
    }

    public boolean isBackbuttonOverridden() {
        return this.webView.d();
    }

    public void exitApp() {
        this.webView.a("exit", (Object) null);
    }
}
