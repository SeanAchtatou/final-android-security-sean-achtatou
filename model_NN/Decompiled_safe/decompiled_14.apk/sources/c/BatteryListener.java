package c;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vpadn.C0017o;
import vpadn.C0019q;
import vpadn.C0024v;

public class BatteryListener extends C0019q {
    private BroadcastReceiver a = null;
    private C0017o b = null;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c.BatteryListener.a(org.json.JSONObject, boolean):void
     arg types: [org.json.JSONObject, int]
     candidates:
      c.BatteryListener.a(c.BatteryListener, android.content.Intent):void
      c.BatteryListener.a(org.json.JSONObject, boolean):void */
    public boolean execute(String str, JSONArray jSONArray, C0017o oVar) {
        if (str.equals("start")) {
            if (this.b != null) {
                oVar.b("Battery listener already running.");
                return true;
            }
            this.b = oVar;
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.BATTERY_CHANGED");
            if (this.a == null) {
                this.a = new BroadcastReceiver() {
                    public final void onReceive(Context context, Intent intent) {
                        BatteryListener.this.a(BatteryListener.a(intent), true);
                    }
                };
                this.cordova.a().registerReceiver(this.a, intentFilter);
            }
            C0024v vVar = new C0024v(C0024v.a.NO_RESULT);
            vVar.a(true);
            oVar.a(vVar);
            return true;
        } else if (!str.equals("stop")) {
            return false;
        } else {
            a();
            a(new JSONObject(), false);
            this.b = null;
            oVar.b();
            return true;
        }
    }

    public void onDestroy() {
        a();
    }

    public void onReset() {
        a();
    }

    private void a() {
        if (this.a != null) {
            try {
                this.cordova.a().unregisterReceiver(this.a);
                this.a = null;
            } catch (Exception e) {
                Log.e("BatteryManager", "Error unregistering battery receiver: " + e.getMessage(), e);
            }
        }
    }

    private static JSONObject a(Intent intent) {
        boolean z = false;
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("level", intent.getIntExtra("level", 0));
            if (intent.getIntExtra("plugged", -1) > 0) {
                z = true;
            }
            jSONObject.put("isPlugged", z);
        } catch (JSONException e) {
            Log.e("BatteryManager", e.getMessage(), e);
        }
        return jSONObject;
    }

    /* access modifiers changed from: private */
    public void a(JSONObject jSONObject, boolean z) {
        if (this.b != null) {
            C0024v vVar = new C0024v(C0024v.a.OK, jSONObject);
            vVar.a(z);
            this.b.a(vVar);
        }
    }
}
