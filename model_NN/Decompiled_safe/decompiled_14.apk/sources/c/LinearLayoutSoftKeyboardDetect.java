package c;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import vpadn.C0020r;

public class LinearLayoutSoftKeyboardDetect extends LinearLayout {
    private int a = 0;
    private int b = 0;

    /* renamed from: c  reason: collision with root package name */
    private int f21c = 0;
    private int d = 0;
    private DroidGap e = null;

    public LinearLayoutSoftKeyboardDetect(Context context, int i, int i2) {
        super(context);
        this.f21c = i;
        this.d = i2;
        this.e = (DroidGap) context;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        C0020r.a("SoftKeyboardDetect", "We are in our onMeasure method");
        int size = View.MeasureSpec.getSize(i2);
        int size2 = View.MeasureSpec.getSize(i);
        C0020r.a("SoftKeyboardDetect", "Old Height = %d", Integer.valueOf(this.a));
        C0020r.a("SoftKeyboardDetect", "Height = %d", Integer.valueOf(size));
        C0020r.a("SoftKeyboardDetect", "Old Width = %d", Integer.valueOf(this.b));
        C0020r.a("SoftKeyboardDetect", "Width = %d", Integer.valueOf(size2));
        if (this.a == 0 || this.a == size) {
            C0020r.b("SoftKeyboardDetect", "Ignore this event");
        } else if (this.d == size2) {
            int i3 = this.d;
            this.d = this.f21c;
            this.f21c = i3;
            C0020r.a("SoftKeyboardDetect", "Orientation Change");
        } else if (size > this.a) {
            if (this.e != null) {
                this.e.a.c("cordova.fireDocumentEvent('hidekeyboard');");
            }
        } else if (size < this.a && this.e != null) {
            this.e.a.c("cordova.fireDocumentEvent('showkeyboard');");
        }
        this.a = size;
        this.b = size2;
    }
}
