package c;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.Looper;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vpadn.C0017o;
import vpadn.C0018p;
import vpadn.C0019q;
import vpadn.C0024v;

public class CompassListener extends C0019q implements SensorEventListener {
    public static int ERROR_FAILED_TO_START = 3;
    public static int RUNNING = 2;
    public static int STARTING = 1;
    public static int STOPPED = 0;
    public long TIMEOUT = 30000;
    private int a = STOPPED;
    private float b = 0.0f;

    /* renamed from: c  reason: collision with root package name */
    private long f3c = 0;
    private long d;
    private SensorManager e;
    private Sensor f;

    public void initialize(C0018p pVar, CordovaWebView cordovaWebView) {
        super.initialize(pVar, cordovaWebView);
        this.e = (SensorManager) pVar.a().getSystemService("sensor");
    }

    public boolean execute(String str, JSONArray jSONArray, C0017o oVar) throws JSONException {
        if (str.equals("start")) {
            start();
            return true;
        } else if (str.equals("stop")) {
            stop();
            return true;
        } else if (str.equals("getStatus")) {
            oVar.a(new C0024v(C0024v.a.OK, getStatus()));
            return true;
        } else if (str.equals("getHeading")) {
            if (this.a != RUNNING) {
                if (start() == ERROR_FAILED_TO_START) {
                    oVar.a(new C0024v(C0024v.a.IO_EXCEPTION, ERROR_FAILED_TO_START));
                    return true;
                }
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    public final void run() {
                        CompassListener.a(CompassListener.this);
                    }
                }, 2000);
            }
            C0024v.a aVar = C0024v.a.OK;
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("magneticHeading", (double) getHeading());
            jSONObject.put("trueHeading", (double) getHeading());
            jSONObject.put("headingAccuracy", 0);
            jSONObject.put("timestamp", this.f3c);
            oVar.a(new C0024v(aVar, jSONObject));
            return true;
        } else if (str.equals("setTimeout")) {
            setTimeout(jSONArray.getLong(0));
            return true;
        } else if (!str.equals("getTimeout")) {
            return false;
        } else {
            oVar.a(new C0024v(C0024v.a.OK, (float) getTimeout()));
            return true;
        }
    }

    public void onDestroy() {
        stop();
    }

    public void onReset() {
        stop();
    }

    public int start() {
        if (this.a == RUNNING || this.a == STARTING) {
            return this.a;
        }
        List<Sensor> sensorList = this.e.getSensorList(3);
        if (sensorList == null || sensorList.size() <= 0) {
            this.a = ERROR_FAILED_TO_START;
        } else {
            this.f = sensorList.get(0);
            this.e.registerListener(this, this.f, 3);
            this.d = System.currentTimeMillis();
            this.a = STARTING;
        }
        return this.a;
    }

    public void stop() {
        if (this.a != STOPPED) {
            this.e.unregisterListener(this);
        }
        this.a = STOPPED;
    }

    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    static /* synthetic */ void a(CompassListener compassListener) {
        if (compassListener.a == STARTING) {
            compassListener.a = ERROR_FAILED_TO_START;
        }
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        float f2 = sensorEvent.values[0];
        this.f3c = System.currentTimeMillis();
        this.b = f2;
        this.a = RUNNING;
        if (this.f3c - this.d > this.TIMEOUT) {
            stop();
        }
    }

    public int getStatus() {
        return this.a;
    }

    public float getHeading() {
        this.d = System.currentTimeMillis();
        return this.b;
    }

    public void setTimeout(long j) {
        this.TIMEOUT = j;
    }

    public long getTimeout() {
        return this.TIMEOUT;
    }
}
