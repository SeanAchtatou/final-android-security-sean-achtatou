package c;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebBackForwardList;
import android.webkit.WebChromeClient;
import android.webkit.WebHistoryItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Stack;
import vpadn.C0004b;
import vpadn.C0006d;
import vpadn.C0008f;
import vpadn.C0014l;
import vpadn.C0015m;
import vpadn.C0018p;
import vpadn.C0020r;
import vpadn.C0023u;
import vpadn.C0024v;

public class CordovaWebView extends WebView {
    private static FrameLayout.LayoutParams s = new FrameLayout.LayoutParams(-1, -1, 17);
    public C0023u a;
    C0008f b;

    /* renamed from: c  reason: collision with root package name */
    public String f4c;
    public boolean d = true;
    public int e = 0;
    public C0015m f;
    public ExposedJsApi g;
    private ArrayList<Integer> h = new ArrayList<>();
    private ArrayList<Integer> i = new ArrayList<>();
    /* access modifiers changed from: private */
    public C0018p j;
    private C0006d k;
    /* access modifiers changed from: private */
    public String l;
    private Stack<String> m = new Stack<>();
    private boolean n;
    private long o = 0;
    /* access modifiers changed from: private */
    public String p;
    private View q;
    private WebChromeClient.CustomViewCallback r;

    public CordovaWebView(Context context) {
        super(context);
        if (C0018p.class.isInstance(context)) {
            this.j = (C0018p) context;
        } else {
            Log.d("CordovaWebView", "Your activity must implement CordovaInterface to work");
        }
        i();
        h();
    }

    public CordovaWebView(Context context, C0018p pVar) {
        super(context);
        if (C0018p.class.isInstance(context)) {
            this.j = (C0018p) context;
        } else {
            this.j = pVar;
            Log.d("CordovaWebView", "Your activity must implement CordovaInterface to work");
        }
        i();
        h();
    }

    public CordovaWebView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        if (C0018p.class.isInstance(context)) {
            this.j = (C0018p) context;
        } else {
            Log.d("CordovaWebView", "Your activity must implement CordovaInterface to work");
        }
        setWebChromeClient(new C0006d(this.j, this));
        C0018p pVar = this.j;
        if (Build.VERSION.SDK_INT < 11) {
            setWebViewClient(new C0008f(this.j, this));
        } else {
            setWebViewClient(new C0014l(this.j, this));
        }
        i();
        h();
    }

    public CordovaWebView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        if (C0018p.class.isInstance(context)) {
            this.j = (C0018p) context;
        } else {
            Log.d("CordovaWebView", "Your activity must implement CordovaInterface to work");
        }
        setWebChromeClient(new C0006d(this.j, this));
        i();
        h();
    }

    @SuppressLint({"NewApi"})
    private void h() {
        boolean z = true;
        setInitialScale(0);
        setVerticalScrollBarEnabled(false);
        WebSettings settings = getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        try {
            Method method = WebSettings.class.getMethod("setNavDump", Boolean.TYPE);
            if (Build.VERSION.SDK_INT < 11) {
                method.invoke(settings, true);
            }
        } catch (NoSuchMethodException e2) {
            Log.d("CordovaWebView", "We are on a modern version of Android, we will deprecate HTC 2.3 devices in 2.8");
        } catch (IllegalArgumentException e3) {
            Log.d("CordovaWebView", "Doing the NavDump failed with bad arguments");
        } catch (IllegalAccessException e4) {
            Log.d("CordovaWebView", "This should never happen: IllegalAccessException means this isn't Android anymore");
        } catch (InvocationTargetException e5) {
            Log.d("CordovaWebView", "This should never happen: InvocationTargetException means this isn't Android anymore.");
        }
        if (Build.VERSION.SDK_INT > 15) {
            settings.setAllowUniversalAccessFromFileURLs(true);
        }
        settings.setDatabaseEnabled(true);
        String path = this.j.a().getApplicationContext().getDir("database", 0).getPath();
        settings.setDatabasePath(path);
        settings.setGeolocationDatabasePath(path);
        settings.setDomStorageEnabled(true);
        settings.setGeolocationEnabled(true);
        settings.setAppCacheMaxSize(5242880);
        settings.setAppCachePath(this.j.a().getApplicationContext().getDir("database", 0).getPath());
        settings.setAppCacheEnabled(true);
        this.a = new C0023u(this, this.j);
        this.f = new C0015m(this, this.j);
        this.g = new ExposedJsApi(this.a, this.f);
        int i2 = Build.VERSION.SDK_INT;
        if (i2 < 11 || i2 > 13) {
            z = false;
        }
        if (z || i2 < 9) {
            Log.i("CordovaWebView", "Disabled addJavascriptInterface() bridge since Android version is old.");
        } else if (i2 >= 11 || !Build.MANUFACTURER.equals(NetworkManager.TYPE_UNKNOWN)) {
            addJavascriptInterface(this.g, "_cordovaNative");
        } else {
            Log.i("CordovaWebView", "Disabled addJavascriptInterface() bridge callback due to a bug on the 2.3 emulator");
        }
    }

    public void setWebViewClient(C0008f fVar) {
        this.b = fVar;
        super.setWebViewClient((WebViewClient) fVar);
    }

    public void setWebChromeClient(C0006d dVar) {
        this.k = dVar;
        super.setWebChromeClient((WebChromeClient) dVar);
    }

    public final C0006d a() {
        return this.k;
    }

    public void loadUrl(String str) {
        if (str.equals("about:blank") || str.startsWith("javascript:")) {
            b(str);
            return;
        }
        String a2 = a("url", (String) null);
        if (a2 == null || this.m.size() > 0) {
            f(str);
        } else {
            f(a2);
        }
    }

    private void f(final String str) {
        C0020r.b("CordovaWebView", ">>> loadUrl(" + str + ")");
        this.l = str;
        if (this.f4c == null) {
            int lastIndexOf = str.lastIndexOf(47);
            if (lastIndexOf > 0) {
                this.f4c = str.substring(0, lastIndexOf + 1);
            } else {
                this.f4c = String.valueOf(this.l) + "/";
            }
            if (this.f4c.startsWith("http://tw.adon.vpon.com/xpon/activity")) {
                this.f4c = "http://tw.adon.vpon.com/xpon/";
            }
            this.a.a();
            if (!this.d) {
                this.m.push(str);
            }
        }
        final int i2 = this.e;
        final int parseInt = Integer.parseInt(a("loadUrlTimeoutValue", "20000"));
        final AnonymousClass1 r5 = new Runnable() {
            public final void run() {
                C0020r.e("CordovaWebView", "CordovaWebView: TIMEOUT ERROR!");
                this.stopLoading();
                if (CordovaWebView.this.b != null) {
                    CordovaWebView.this.b.onReceivedError(this, -6, "The connection to the server was unsuccessful.", str);
                }
            }
        };
        final AnonymousClass2 r0 = new Runnable(this) {
            public final void run() {
                try {
                    synchronized (this) {
                        wait((long) parseInt);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (this.e == i2) {
                    this.j.a().runOnUiThread(r5);
                }
            }
        };
        this.j.a().runOnUiThread(new Runnable(this) {
            public final void run() {
                new Thread(r0).start();
                this.b(str);
            }
        });
    }

    public void loadDataWithBaseURL(String str, String str2, String str3, String str4, String str5) {
        this.l = null;
        this.f4c = str;
        this.p = str2;
        this.a.a();
        final int i2 = this.e;
        final int parseInt = Integer.parseInt(a("loadUrlTimeoutValue", "20000"));
        final AnonymousClass4 r5 = new Runnable() {
            public final void run() {
                C0020r.e("CordovaWebView", "CordovaWebView: TIMEOUT ERROR!--loadDataWithBaseURL");
                if (this != null) {
                    try {
                        this.stopLoading();
                    } catch (Exception e) {
                    }
                }
                if (CordovaWebView.this.b != null) {
                    CordovaWebView.this.b.onReceivedError(this, -6, "The connection to the server was unsuccessful.", CordovaWebView.this.l);
                }
            }
        };
        final AnonymousClass5 r0 = new Runnable(this) {
            public final void run() {
                try {
                    synchronized (this) {
                        wait((long) parseInt);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (this.e == i2) {
                    this.j.a().runOnUiThread(r5);
                }
            }
        };
        this.j.a().runOnUiThread(new Runnable() {
            public final void run() {
                new Thread(r0).start();
                this.a(CordovaWebView.this.p);
            }
        });
    }

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        WebSettings settings = getSettings();
        if (settings == null) {
            Log.e("CordovaWebView", "this.getSettings() == null ??");
            return;
        }
        settings.setDefaultTextEncodingName("utf-8");
        super.loadDataWithBaseURL(this.f4c, str, "text/html", "utf-8", null);
    }

    public final void b(String str) {
        if (C0020r.a(3) && !str.startsWith("javascript:")) {
            C0020r.b("CordovaWebView", ">>> loadUrlNow()");
        }
        if (str != null && this.f4c != null) {
            if (str.startsWith("file://") || str.indexOf(this.f4c) == 0 || str.startsWith("javascript:") || C0004b.a(str)) {
                super.loadUrl(str);
            }
        }
    }

    public final void c(String str) {
        this.f.a(str);
    }

    public final void a(C0024v vVar, String str) {
        this.f.a(vVar, str);
    }

    public final void a(String str, Object obj) {
        if (this.a != null) {
            this.a.a(str, obj);
        }
    }

    public final String b() {
        if (this.m.size() > 0) {
            return this.m.peek();
        }
        return "";
    }

    public final void d(String str) {
        this.m.push(str);
    }

    public final boolean c() {
        if (super.canGoBack() && this.d) {
            WebBackForwardList copyBackForwardList = copyBackForwardList();
            int size = copyBackForwardList.getSize();
            for (int i2 = 0; i2 < size; i2++) {
                C0020r.b("CordovaWebView", "The URL at index: " + Integer.toString(i2) + "is " + copyBackForwardList.getItemAtIndex(i2).getUrl());
            }
            super.goBack();
            return true;
        } else if (this.m.size() <= 1 || this.d) {
            return false;
        } else {
            this.m.pop();
            loadUrl(this.m.pop());
            return true;
        }
    }

    public boolean canGoBack() {
        if ((!super.canGoBack() || !this.d) && this.m.size() <= 1) {
            return false;
        }
        return true;
    }

    public final void a(String str, boolean z, boolean z2) {
        C0020r.b("CordovaWebView", "showWebPage(%s, %b, %b, HashMap", str, Boolean.valueOf(z), Boolean.valueOf(z2));
        if (z2) {
            clearHistory();
        }
        if (z) {
            try {
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.setData(Uri.parse(str));
                this.j.a().startActivity(intent);
            } catch (ActivityNotFoundException e2) {
                C0020r.b("CordovaWebView", "Error loading url " + str, e2);
            }
        } else if (str.startsWith("file://") || str.indexOf(this.f4c) == 0 || C0004b.a(str)) {
            if (z2) {
                this.m.clear();
            }
            loadUrl(str);
        } else {
            C0020r.d("CordovaWebView", "showWebPage: Cannot load URL into webview since it is not in white list.  Loading into browser instead. (URL=" + str + ")");
            try {
                Intent intent2 = new Intent("android.intent.action.VIEW");
                intent2.setData(Uri.parse(str));
                this.j.a().startActivity(intent2);
            } catch (ActivityNotFoundException e3) {
                C0020r.b("CordovaWebView", "Error loading url " + str, e3);
            }
        }
    }

    private void i() {
        if ("false".equals(a("useBrowserHistory", "true"))) {
            this.d = false;
            Log.w("CordovaWebView", "useBrowserHistory=false is deprecated as of Cordova 2.2.0 and will be removed six months after the 2.2.0 release.  Please use the browser history and use history.back().");
        }
        if ("true".equals(a("fullscreen", "false"))) {
            this.j.a().getWindow().clearFlags(2048);
            this.j.a().getWindow().setFlags(1024, 1024);
        }
    }

    private String a(String str, String str2) {
        Object obj;
        Bundle extras = this.j.a().getIntent().getExtras();
        return (extras == null || (obj = extras.get(str)) == null) ? str2 : obj.toString();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        boolean z;
        if (this.h.contains(Integer.valueOf(i2))) {
            if (i2 == 25) {
                C0020r.b("CordovaWebView", "Down Key Hit");
                loadUrl("javascript:cordova.fireDocumentEvent('volumedownbutton');");
                return true;
            } else if (i2 != 24) {
                return super.onKeyDown(i2, keyEvent);
            } else {
                C0020r.b("CordovaWebView", "Up Key Hit");
                loadUrl("javascript:cordova.fireDocumentEvent('volumeupbutton');");
                return true;
            }
        } else if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        } else {
            if (this.d) {
                WebHistoryItem itemAtIndex = copyBackForwardList().getItemAtIndex(0);
                if (itemAtIndex != null) {
                    String url = itemAtIndex.getUrl();
                    String url2 = getUrl();
                    C0020r.b("CordovaWebView", "The current URL is: " + url2);
                    C0020r.b("CordovaWebView", "The URL at item 0 is:" + url);
                    z = url2.equals(url);
                } else {
                    z = false;
                }
                if (!z || this.n) {
                    return true;
                }
                return false;
            } else if (this.m.size() > 1 || this.n) {
                return true;
            } else {
                return false;
            }
        }
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            if (this.q != null) {
                f();
            } else if (this.n) {
                loadUrl("javascript:cordova.fireDocumentEvent('backbutton');");
                return true;
            } else if (!c()) {
                return false;
            } else {
                return true;
            }
        } else if (i2 == 82) {
            if (this.o < keyEvent.getEventTime()) {
                loadUrl("javascript:cordova.fireDocumentEvent('menubutton');");
            }
            this.o = keyEvent.getEventTime();
            return super.onKeyUp(i2, keyEvent);
        } else if (i2 == 84) {
            loadUrl("javascript:cordova.fireDocumentEvent('searchbutton');");
            return true;
        } else if (this.i.contains(Integer.valueOf(i2))) {
            return super.onKeyUp(i2, keyEvent);
        }
        return super.onKeyUp(i2, keyEvent);
    }

    public final void a(boolean z) {
        this.n = z;
    }

    public final void e(String str) {
        if (str.compareTo("volumeup") == 0) {
            this.h.add(24);
        } else if (str.compareTo("volumedown") == 0) {
            this.h.add(25);
        }
    }

    public final boolean d() {
        return this.n;
    }

    public void b(boolean z) {
        C0020r.b("CordovaWebView", "Handle the pause");
        loadUrl("javascript:try{cordova.fireDocumentEvent('pause');}catch(e){console.log('exception firing pause event from native');};");
        if (this.a != null) {
            this.a.a(z);
        }
        if (!z) {
            pauseTimers();
        }
    }

    public void a(boolean z, boolean z2) {
        loadUrl("javascript:try{cordova.fireDocumentEvent('resume');}catch(e){console.log('exception firing resume event from native');};");
        if (this.a != null) {
            this.a.b(z);
        }
        resumeTimers();
    }

    public final void e() {
        loadUrl("javascript:try{cordova.require('cordova/channel').onDestroy.fire();}catch(e){console.log('exception firing destroy event from native');};");
        loadUrl("about:blank");
        if (this.a != null) {
            this.a.b();
        }
    }

    public final void a(Intent intent) {
        if (this.a != null) {
            this.a.a(intent);
        }
    }

    public void a(View view, WebChromeClient.CustomViewCallback customViewCallback) {
        Log.d("CordovaWebView", "showing Custom View");
        if (this.q != null) {
            customViewCallback.onCustomViewHidden();
            return;
        }
        this.q = view;
        this.r = customViewCallback;
        ViewGroup viewGroup = (ViewGroup) getParent();
        viewGroup.addView(view, s);
        setVisibility(8);
        viewGroup.setVisibility(0);
        viewGroup.bringToFront();
    }

    public void f() {
        Log.d("CordovaWebView", "Hidding Custom View");
        if (this.q != null) {
            this.q.setVisibility(8);
            ((ViewGroup) getParent()).removeView(this.q);
            this.q = null;
            this.r.onCustomViewHidden();
            setVisibility(0);
        }
    }

    public final boolean g() {
        return this.q != null;
    }

    public WebBackForwardList restoreState(Bundle bundle) {
        WebBackForwardList restoreState = super.restoreState(bundle);
        Log.d("CordovaWebView", "WebView restoration crew now restoring!");
        this.a.a();
        return restoreState;
    }
}
