package c;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.ValueCallback;
import android.widget.LinearLayout;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.json.JSONException;
import org.json.JSONObject;
import vpadn.C0004b;
import vpadn.C0006d;
import vpadn.C0008f;
import vpadn.C0014l;
import vpadn.C0018p;
import vpadn.C0019q;
import vpadn.C0020r;

public class DroidGap extends Activity implements C0018p {
    private static String e = "DroidGap";
    private static int h = 0;
    private static int i = 1;
    private static int j = 2;
    protected CordovaWebView a;
    protected ProgressDialog b = null;

    /* renamed from: c  reason: collision with root package name */
    protected int f10c = 0;
    protected Dialog d;
    private LinearLayout f;
    private final ExecutorService g = Executors.newCachedThreadPool();
    private int k = 0;
    private C0019q l = null;
    private boolean m;
    private int n = -16777216;
    private int o = 3000;
    private boolean p = true;
    private String q;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c.DroidGap.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      c.DroidGap.a(java.lang.String, java.lang.String):java.lang.String
      c.DroidGap.a(java.lang.String, int):int
      c.DroidGap.a(java.lang.String, java.lang.Object):java.lang.Object
      vpadn.p.a(java.lang.String, java.lang.Object):java.lang.Object
      c.DroidGap.a(java.lang.String, boolean):boolean */
    public void onCreate(Bundle bundle) {
        C0004b.a(this);
        C0020r.b(e, "DroidGap.onCreate()");
        super.onCreate(bundle);
        if (bundle != null) {
            this.q = bundle.getString("callbackClass");
        }
        if (!a("showTitle", false)) {
            getWindow().requestFeature(1);
        }
        if (a("setFullscreen", false)) {
            getWindow().setFlags(1024, 1024);
        } else {
            getWindow().setFlags(2048, 2048);
        }
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        this.f = new LinearLayoutSoftKeyboardDetect(this, defaultDisplay.getWidth(), defaultDisplay.getHeight());
        this.f.setOrientation(1);
        this.f.setBackgroundColor(this.n);
        this.f.setLayoutParams(new LinearLayout.LayoutParams(-1, -1, 0.0f));
        setVolumeControlStream(3);
    }

    public final Activity a() {
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c.DroidGap.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      c.DroidGap.a(java.lang.String, java.lang.String):java.lang.String
      c.DroidGap.a(java.lang.String, int):int
      c.DroidGap.a(java.lang.String, java.lang.Object):java.lang.Object
      vpadn.p.a(java.lang.String, java.lang.Object):java.lang.Object
      c.DroidGap.a(java.lang.String, boolean):boolean */
    public final void a(String str) {
        C0008f lVar;
        if (this.a == null) {
            CordovaWebView cordovaWebView = new CordovaWebView(this);
            if (Build.VERSION.SDK_INT < 11) {
                lVar = new C0008f(this, cordovaWebView);
            } else {
                lVar = new C0014l(this, cordovaWebView);
            }
            C0006d dVar = new C0006d(this, cordovaWebView);
            C0020r.b(e, "DroidGap.init()");
            this.a = cordovaWebView;
            this.a.setId(100);
            this.a.setWebViewClient(lVar);
            this.a.setWebChromeClient(dVar);
            lVar.a(this.a);
            dVar.a(this.a);
            this.a.setLayoutParams(new LinearLayout.LayoutParams(-1, -1, 1.0f));
            this.a.setVisibility(4);
            this.f.addView(this.a);
            setContentView(this.f);
        }
        this.n = a("backgroundColor", -16777216);
        this.f.setBackgroundColor(this.n);
        this.p = a("keepRunning", true);
        String a2 = (this.a == null || !this.a.canGoBack()) ? a("loadingDialog", (String) null) : a("loadingPageDialog", (String) null);
        if (a2 != null) {
            String str2 = "";
            if (a2.length() > 0) {
                int indexOf = a2.indexOf(44);
                if (indexOf > 0) {
                    str2 = a2.substring(0, indexOf);
                    a2 = a2.substring(indexOf + 1);
                } else {
                    str2 = "";
                }
            } else {
                a2 = "Loading Application...";
            }
            if (this.b != null) {
                this.b.dismiss();
                this.b = null;
            }
            this.b = ProgressDialog.show(this, str2, a2, true, true, new DialogInterface.OnCancelListener(this) {
                public final void onCancel(DialogInterface dialogInterface) {
                    this.b = null;
                }
            });
        }
        this.a.loadUrl(str);
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    private boolean a(String str, boolean z) {
        Boolean bool;
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            return z;
        }
        try {
            bool = (Boolean) extras.get(str);
        } catch (ClassCastException e2) {
            if ("true".equals(extras.get(str).toString())) {
                bool = true;
            } else {
                bool = false;
            }
        }
        return bool != null ? bool.booleanValue() : z;
    }

    public final int a(String str, int i2) {
        Integer valueOf;
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            return i2;
        }
        try {
            valueOf = (Integer) extras.get(str);
        } catch (ClassCastException e2) {
            valueOf = Integer.valueOf(Integer.parseInt(extras.get(str).toString()));
        }
        return valueOf != null ? valueOf.intValue() : i2;
    }

    private String a(String str, String str2) {
        String string;
        Bundle extras = getIntent().getExtras();
        if (extras == null || (string = extras.getString(str)) == null) {
            return null;
        }
        return string;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        C0020r.b(e, "Paused the application!");
        if (this.k != j && this.a != null) {
            this.a.b(this.p);
            d();
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (this.a != null) {
            this.a.a(intent);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        C0020r.b(e, "Resuming the App");
        if (this.k == 0) {
            this.k = i;
        } else if (this.a != null) {
            this.a.a(this.p, this.m);
            if ((!this.p || this.m) && this.m) {
                this.p = this.m;
                this.m = false;
            }
        }
    }

    public void onDestroy() {
        C0020r.b(e, "onDestroy()");
        super.onDestroy();
        d();
        if (this.a != null) {
            this.a.e();
        } else {
            c();
        }
    }

    private void b(String str, Object obj) {
        if (this.a != null) {
            this.a.a(str, obj);
        }
    }

    public final void b() {
        if (this.b != null && this.b.isShowing()) {
            this.b.dismiss();
            this.b = null;
        }
    }

    public final void c() {
        this.k = j;
        super.finish();
    }

    public final void a(C0019q qVar, Intent intent, int i2) {
        this.l = qVar;
        this.m = this.p;
        if (qVar != null) {
            this.p = false;
        }
        super.startActivityForResult(intent, i2);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        C0020r.b(e, "Incoming Result");
        super.onActivityResult(i2, i3, intent);
        Log.d(e, "Request code = " + i2);
        ValueCallback<Uri> a2 = this.a.a().a();
        if (i2 == 5173) {
            Log.d(e, "did we get here?");
            if (a2 != null) {
                Uri data = (intent == null || i3 != -1) ? null : intent.getData();
                Log.d(e, "result = " + data);
                a2.onReceiveValue(data);
            } else {
                return;
            }
        }
        C0019q qVar = this.l;
        if (qVar == null) {
            if (this.q != null) {
                this.l = this.a.a.a(this.q);
                qVar = this.l;
            } else {
                return;
            }
        }
        C0020r.b(e, "We have a callback to send this result to");
        qVar.onActivityResult(i2, i3, intent);
    }

    public final void a(String str, String str2, String str3, boolean z) {
        final String str4 = str2;
        final String str5 = str;
        final String str6 = str3;
        final boolean z2 = z;
        runOnUiThread(new Runnable() {
            public final void run() {
                try {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage(str4);
                    builder.setTitle(str5);
                    builder.setCancelable(false);
                    String str = str6;
                    final boolean z = z2;
                    final DroidGap droidGap = this;
                    builder.setPositiveButton(str, new DialogInterface.OnClickListener(this) {
                        public final void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            if (z) {
                                droidGap.c();
                            }
                        }
                    });
                    builder.create();
                    builder.show();
                } catch (Exception e2) {
                    DroidGap.this.finish();
                }
            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        b("onCreateOptionsMenu", menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        b("onPrepareOptionsMenu", menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        b("onOptionsItemSelected", menuItem);
        return true;
    }

    public final void d() {
        if (this.d != null && this.d.isShowing()) {
            this.d.dismiss();
            this.d = null;
        }
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        View focusedChild = this.a.getFocusedChild();
        if ((this.a.g() || focusedChild != null) && i2 == 4) {
            return this.a.onKeyUp(i2, keyEvent);
        }
        return super.onKeyUp(i2, keyEvent);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (this.a.getFocusedChild() == null || i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        return this.a.onKeyDown(i2, keyEvent);
    }

    public final Object a(String str, Object obj) {
        final boolean z = false;
        C0020r.b(e, "onMessage(" + str + "," + obj + ")");
        if ("splashscreen".equals(str)) {
            if ("hide".equals(obj.toString())) {
                d();
            } else if (this.d == null || !this.d.isShowing()) {
                this.f10c = a("splashscreen", 0);
                final int i2 = this.o;
                runOnUiThread(new Runnable() {
                    public final void run() {
                        Display defaultDisplay = DroidGap.this.getWindowManager().getDefaultDisplay();
                        LinearLayout linearLayout = new LinearLayout(this.a());
                        linearLayout.setMinimumHeight(defaultDisplay.getHeight());
                        linearLayout.setMinimumWidth(defaultDisplay.getWidth());
                        linearLayout.setOrientation(1);
                        linearLayout.setBackgroundColor(this.a("backgroundColor", -16777216));
                        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -1, 0.0f));
                        linearLayout.setBackgroundResource(this.f10c);
                        DroidGap.this.d = new Dialog(this, 16973840);
                        if ((DroidGap.this.getWindow().getAttributes().flags & 1024) == 1024) {
                            DroidGap.this.d.getWindow().setFlags(1024, 1024);
                        }
                        DroidGap.this.d.setContentView(linearLayout);
                        DroidGap.this.d.setCancelable(false);
                        DroidGap.this.d.show();
                        new Handler().postDelayed(new Runnable() {
                            public final void run() {
                                DroidGap.this.d();
                            }
                        }, (long) i2);
                    }
                });
            }
        } else if ("spinner".equals(str)) {
            if ("stop".equals(obj.toString())) {
                b();
                this.a.setVisibility(0);
            }
        } else if ("onReceivedError".equals(str)) {
            JSONObject jSONObject = (JSONObject) obj;
            try {
                int i3 = jSONObject.getInt("errorCode");
                final String string = jSONObject.getString("description");
                final String string2 = jSONObject.getString("url");
                final String a2 = a("errorUrl", (String) null);
                if (a2 == null || ((!a2.startsWith("file://") && !C0004b.a(a2)) || string2.equals(a2))) {
                    if (i3 != -2) {
                        z = true;
                    }
                    runOnUiThread(new Runnable(this) {
                        public final void run() {
                            if (z) {
                                this.a.setVisibility(8);
                                this.a("Application Error", String.valueOf(string) + " (" + string2 + ")", "OK", z);
                            }
                        }
                    });
                } else {
                    runOnUiThread(new Runnable(this) {
                        public final void run() {
                            this.b();
                            this.a.a(a2, false, true);
                        }
                    });
                }
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        } else if ("exit".equals(str)) {
            c();
        }
        return null;
    }

    public final ExecutorService e() {
        return this.g;
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (this.l != null) {
            bundle.putString("callbackClass", this.l.getClass().getName());
        }
    }
}
