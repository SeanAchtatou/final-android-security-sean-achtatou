package vpadn;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Debug;
import java.text.DecimalFormat;

public final class Y {
    private static int a = 0;

    public static boolean a(Context context) {
        try {
            PackageManager packageManager = context.getPackageManager();
            if (packageManager.checkPermission("android.permission.WRITE_EXTERNAL_STORAGE", packageManager.getPackageInfo(context.getPackageName(), 0).packageName) == -1) {
                return false;
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean b(Context context) {
        try {
            PackageManager packageManager = context.getPackageManager();
            if (packageManager.checkPermission("android.permission.ACCESS_FINE_LOCATION", packageManager.getPackageInfo(context.getPackageName(), 0).packageName) == -1) {
                return false;
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean c(Context context) {
        try {
            PackageManager packageManager = context.getPackageManager();
            if (packageManager.checkPermission("android.permission.ACCESS_COARSE_LOCATION", packageManager.getPackageInfo(context.getPackageName(), 0).packageName) == -1) {
                return false;
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean d(Context context) {
        if (a == 0) {
            try {
                a = 1;
                String[] strArr = {"android.permission.INTERNET", "android.permission.READ_PHONE_STATE", "android.permission.ACCESS_COARSE_LOCATION", "android.permission.ACCESS_NETWORK_STATE", "android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.ACCESS_WIFI_STATE"};
                new String[1][0] = "android.permission.INTERNET";
                PackageManager packageManager = context.getPackageManager();
                String str = packageManager.getPackageInfo(context.getPackageName(), 0).packageName;
                int i = 0;
                while (true) {
                    if (i >= strArr.length) {
                        break;
                    } else if (packageManager.checkPermission(strArr[i], str) == -1) {
                        C0003a.b("VponUtil", "The permission:" + strArr[i] + " is mandatory for VPON AD, please add this permission to AndroidManifest.xml");
                        a = 2;
                        break;
                    } else {
                        i++;
                    }
                }
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
                a = 0;
            }
        }
        if (a == 1) {
            return true;
        }
        return false;
    }

    public static int a(Double d, Double d2, double d3, double d4) {
        if (d == null || d2 == null || (d3 == 0.0d && d4 == 0.0d)) {
            return -1;
        }
        double doubleValue = 0.017453292519943295d * d.doubleValue();
        double d5 = 0.017453292519943295d * d3;
        double acos = Math.acos((Math.cos(doubleValue) * Math.cos(d5) * Math.cos((0.017453292519943295d * d4) - (0.017453292519943295d * d2.doubleValue()))) + (Math.sin(doubleValue) * Math.sin(d5))) * 6371.0d;
        if (Double.isNaN(acos)) {
            return -1;
        }
        return (int) (acos * 1000.0d);
    }

    public static void a() {
        Double valueOf = Double.valueOf(Double.valueOf((double) Debug.getNativeHeapAllocatedSize()).doubleValue() / new Double(1048576.0d).doubleValue());
        Double valueOf2 = Double.valueOf(Double.valueOf((double) Debug.getNativeHeapSize()).doubleValue() / 1048576.0d);
        Double valueOf3 = Double.valueOf(Double.valueOf((double) Debug.getNativeHeapFreeSize()).doubleValue() / 1048576.0d);
        DecimalFormat decimalFormat = new DecimalFormat();
        decimalFormat.setMaximumFractionDigits(2);
        decimalFormat.setMinimumFractionDigits(2);
        C0003a.a("VponUtil", "debug. =================================");
        C0003a.a("VponUtil", "debug.heap native: allocated " + decimalFormat.format(valueOf) + "MB of " + decimalFormat.format(valueOf2) + "MB (" + decimalFormat.format(valueOf3) + "MB free)");
        C0003a.a("VponUtil", "debug.memory: allocated: " + decimalFormat.format(Double.valueOf((double) (Runtime.getRuntime().totalMemory() / 1048576))) + "MB of " + decimalFormat.format(Double.valueOf((double) (Runtime.getRuntime().maxMemory() / 1048576))) + "MB (" + decimalFormat.format(Double.valueOf((double) (Runtime.getRuntime().freeMemory() / 1048576))) + "MB free)");
    }
}
