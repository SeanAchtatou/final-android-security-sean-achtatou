package vpadn;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.Header;
import org.apache.http.HttpResponse;

public final class Q {
    private static Q b = new Q();
    private Map<String, StringBuffer> a = Collections.synchronizedMap(new HashMap());

    private Q() {
    }

    public static Q a() {
        return b;
    }

    private void a(String str, String str2, boolean z) {
        if (!z) {
            this.a.remove(str);
        }
        StringBuffer stringBuffer = this.a.get(str);
        if (stringBuffer == null) {
            stringBuffer = new StringBuffer();
        }
        if (z) {
            stringBuffer.append("-----------------------------------------------------------\n");
        } else {
            stringBuffer.append("\n");
        }
        stringBuffer.append(str2);
        this.a.put(str, stringBuffer);
    }

    private void a(String str, HttpResponse httpResponse, boolean z) {
        StringBuffer stringBuffer;
        if (!z) {
            this.a.remove(str);
        }
        StringBuffer stringBuffer2 = this.a.get(str);
        if (stringBuffer2 == null) {
            stringBuffer = new StringBuffer();
        } else {
            stringBuffer = stringBuffer2;
        }
        if (z) {
            stringBuffer.append("-----------------------------------------------------------\n");
        } else {
            stringBuffer.append("\n");
        }
        Header[] allHeaders = httpResponse.getAllHeaders();
        int length = allHeaders.length;
        for (int i = 0; i < length; i++) {
            stringBuffer.append(String.valueOf(allHeaders[i].toString()) + "\n");
        }
        this.a.put(str, stringBuffer);
    }

    public final void a(HttpResponse httpResponse, boolean z) {
        a("OK_key", httpResponse, z);
    }

    public final void b(HttpResponse httpResponse, boolean z) {
        a("ERR_key", httpResponse, z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: vpadn.Q.a(java.lang.String, java.lang.String, boolean):void
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      vpadn.Q.a(java.lang.String, org.apache.http.HttpResponse, boolean):void
      vpadn.Q.a(java.lang.String, java.lang.String, boolean):void */
    public final void a(String str, boolean z) {
        a("OK_key", str, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: vpadn.Q.a(java.lang.String, java.lang.String, boolean):void
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      vpadn.Q.a(java.lang.String, org.apache.http.HttpResponse, boolean):void
      vpadn.Q.a(java.lang.String, java.lang.String, boolean):void */
    public final void b(String str, boolean z) {
        a("ERR_key", str, true);
    }

    public final void b() {
        this.a.clear();
    }

    public final void c() {
    }

    public final void d() {
    }

    public final void a(String str) {
    }

    public final void b(String str) {
    }
}
