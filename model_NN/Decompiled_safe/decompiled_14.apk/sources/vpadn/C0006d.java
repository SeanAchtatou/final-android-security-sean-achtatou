package vpadn;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Build;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.GeolocationPermissions;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import c.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;

/* renamed from: vpadn.d  reason: case insensitive filesystem */
public class C0006d extends WebChromeClient {
    private String a = "CordovaLog";
    private long b = 104857600;

    /* renamed from: c  reason: collision with root package name */
    private C0018p f113c;
    private CordovaWebView d;
    private View e;

    public C0006d(C0018p pVar, CordovaWebView cordovaWebView) {
        this.f113c = pVar;
        this.d = cordovaWebView;
    }

    public final void a(CordovaWebView cordovaWebView) {
        this.d = cordovaWebView;
    }

    public boolean onJsAlert(WebView webView, String str, String str2, final JsResult jsResult) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.f113c.a());
        builder.setMessage(str2);
        builder.setTitle("Alert");
        builder.setCancelable(true);
        builder.setPositiveButton(17039370, new DialogInterface.OnClickListener(this) {
            public final void onClick(DialogInterface dialogInterface, int i) {
                jsResult.confirm();
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener(this) {
            public final void onCancel(DialogInterface dialogInterface) {
                jsResult.cancel();
            }
        });
        builder.setOnKeyListener(new DialogInterface.OnKeyListener(this) {
            public final boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                if (i != 4) {
                    return true;
                }
                jsResult.confirm();
                return false;
            }
        });
        builder.create();
        builder.show();
        return true;
    }

    public boolean onJsConfirm(WebView webView, String str, String str2, final JsResult jsResult) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.f113c.a());
        builder.setMessage(str2);
        builder.setTitle("Confirm");
        builder.setCancelable(true);
        builder.setPositiveButton(17039370, new DialogInterface.OnClickListener(this) {
            public final void onClick(DialogInterface dialogInterface, int i) {
                jsResult.confirm();
            }
        });
        builder.setNegativeButton(17039360, new DialogInterface.OnClickListener(this) {
            public final void onClick(DialogInterface dialogInterface, int i) {
                jsResult.cancel();
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener(this) {
            public final void onCancel(DialogInterface dialogInterface) {
                jsResult.cancel();
            }
        });
        builder.setOnKeyListener(new DialogInterface.OnKeyListener(this) {
            public final boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                if (i != 4) {
                    return true;
                }
                jsResult.cancel();
                return false;
            }
        });
        builder.create();
        builder.show();
        return true;
    }

    public boolean onJsPrompt(WebView webView, String str, String str2, String str3, final JsPromptResult jsPromptResult) {
        boolean z;
        if (str.startsWith("file://") || str.indexOf(this.d.f4c) == 0 || C0004b.a(str)) {
            z = true;
        } else {
            z = false;
        }
        if (z && str3 != null && str3.length() > 3 && str3.substring(0, 4).equals("gap:")) {
            try {
                JSONArray jSONArray = new JSONArray(str3.substring(4));
                String exec = this.d.g.exec(jSONArray.getString(0), jSONArray.getString(1), jSONArray.getString(2), str2);
                if (exec == null) {
                    exec = "";
                }
                jsPromptResult.confirm(exec);
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        } else if (z && str3 != null && str3.equals("gap_bridge_mode:")) {
            this.d.g.setNativeToJsBridgeMode(Integer.parseInt(str2));
            jsPromptResult.confirm("");
        } else if (z && str3 != null && str3.equals("gap_poll:")) {
            String retrieveJsMessages = this.d.g.retrieveJsMessages();
            if (retrieveJsMessages == null) {
                retrieveJsMessages = "";
            }
            jsPromptResult.confirm(retrieveJsMessages);
        } else if (str3 == null || !str3.equals("gap_init:")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.f113c.a());
            builder.setMessage(str2);
            final EditText editText = new EditText(this.f113c.a());
            if (str3 != null) {
                editText.setText(str3);
            }
            builder.setView(editText);
            builder.setCancelable(false);
            builder.setPositiveButton(17039370, new DialogInterface.OnClickListener(this) {
                public final void onClick(DialogInterface dialogInterface, int i) {
                    jsPromptResult.confirm(editText.getText().toString());
                }
            });
            builder.setNegativeButton(17039360, new DialogInterface.OnClickListener(this) {
                public final void onClick(DialogInterface dialogInterface, int i) {
                    jsPromptResult.cancel();
                }
            });
            builder.create();
            builder.show();
        } else {
            jsPromptResult.confirm("OK");
        }
        return true;
    }

    public void onExceededDatabaseQuota(String str, String str2, long j, long j2, long j3, WebStorage.QuotaUpdater quotaUpdater) {
        C0020r.b(this.a, "DroidGap:  onExceededDatabaseQuota estimatedSize: %d  currentQuota: %d  totalUsedQuota: %d", Long.valueOf(j2), Long.valueOf(j), Long.valueOf(j3));
        if (j2 < this.b) {
            C0020r.b(this.a, "calling quotaUpdater.updateQuota newQuota: %d", Long.valueOf(j2));
            quotaUpdater.updateQuota(j2);
            return;
        }
        quotaUpdater.updateQuota(j);
    }

    public void onConsoleMessage(String str, int i, String str2) {
        if (Build.VERSION.SDK_INT == 7) {
            C0020r.b(this.a, "%s: Line %d : %s", str2, Integer.valueOf(i), str);
            super.onConsoleMessage(str, i, str2);
        }
    }

    @TargetApi(8)
    public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
        if (consoleMessage.message() != null) {
            C0020r.b(this.a, consoleMessage.message());
        }
        return super.onConsoleMessage(consoleMessage);
    }

    public void onGeolocationPermissionsShowPrompt(String str, GeolocationPermissions.Callback callback) {
        super.onGeolocationPermissionsShowPrompt(str, callback);
        callback.invoke(str, true, false);
    }

    public void onShowCustomView(View view, WebChromeClient.CustomViewCallback customViewCallback) {
        this.d.a(view, customViewCallback);
    }

    public void onHideCustomView() {
        this.d.f();
    }

    public View getVideoLoadingProgressView() {
        if (this.e == null) {
            LinearLayout linearLayout = new LinearLayout(this.d.getContext());
            linearLayout.setOrientation(1);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(13);
            linearLayout.setLayoutParams(layoutParams);
            ProgressBar progressBar = new ProgressBar(this.d.getContext());
            LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
            layoutParams2.gravity = 17;
            progressBar.setLayoutParams(layoutParams2);
            linearLayout.addView(progressBar);
            this.e = linearLayout;
        }
        return this.e;
    }

    public final ValueCallback<Uri> a() {
        return null;
    }
}
