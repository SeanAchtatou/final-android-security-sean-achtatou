package vpadn;

import org.json.JSONArray;
import org.json.JSONException;

/* renamed from: vpadn.c  reason: case insensitive filesystem */
public final class C0005c {
    public JSONArray a;

    public C0005c(JSONArray jSONArray) {
        this.a = jSONArray;
    }

    public final String a(int i) throws JSONException {
        return this.a.getString(0);
    }

    public final boolean b(int i) {
        return this.a.isNull(0);
    }
}
