package vpadn;

public final class N {

    /* renamed from: c  reason: collision with root package name */
    private static N f105c = new N();
    private long a = System.currentTimeMillis();
    private long b = 0;

    private N() {
    }

    public static N a() {
        return f105c;
    }

    public final synchronized long b() {
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - this.a > 1800000) {
            this.a = currentTimeMillis;
            this.b = 0;
        }
        return this.a;
    }

    public final synchronized long c() {
        long j;
        j = this.b;
        this.b = 1 + j;
        return j;
    }
}
