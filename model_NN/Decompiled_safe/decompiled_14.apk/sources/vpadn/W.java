package vpadn;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import android.provider.Settings;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

public final class W implements ServiceConnection {
    private static String f = null;
    private static boolean g = false;
    private final Context a;
    private List<ResolveInfo> b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public Map<String, Integer> f110c = new HashMap();
    private final SharedPreferences d;
    private final Random e = new Random();

    private W(Context context) {
        this.d = context.getSharedPreferences("openudid_prefs", 0);
        this.a = context;
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        String readString;
        try {
            Parcel obtain = Parcel.obtain();
            obtain.writeInt(this.e.nextInt());
            Parcel obtain2 = Parcel.obtain();
            iBinder.transact(1, Parcel.obtain(), obtain2, 0);
            if (obtain.readInt() == obtain2.readInt() && (readString = obtain2.readString()) != null) {
                if (this.f110c.containsKey(readString)) {
                    this.f110c.put(readString, Integer.valueOf(this.f110c.get(readString).intValue() + 1));
                } else {
                    this.f110c.put(readString, 1);
                }
            }
        } catch (RemoteException e2) {
        }
        this.a.unbindService(this);
        c();
    }

    public final void onServiceDisconnected(ComponentName componentName) {
    }

    private void c() {
        if (this.b.size() > 0) {
            ServiceInfo serviceInfo = this.b.get(0).serviceInfo;
            Intent intent = new Intent();
            intent.setComponent(new ComponentName(serviceInfo.applicationInfo.packageName, serviceInfo.name));
            this.a.bindService(intent, this, 1);
            this.b.remove(0);
            return;
        }
        if (!this.f110c.isEmpty()) {
            TreeMap treeMap = new TreeMap(new a(this, (byte) 0));
            treeMap.putAll(this.f110c);
            f = (String) treeMap.firstKey();
        }
        if (f == null) {
            String string = Settings.Secure.getString(this.a.getContentResolver(), "android_id");
            f = string;
            if (string == null || f.equals("9774d56d682e549c") || f.length() < 15) {
                f = new BigInteger(64, new SecureRandom()).toString(16);
            }
        }
        SharedPreferences.Editor edit = this.d.edit();
        edit.putString("openudid", f);
        edit.commit();
        g = true;
    }

    public static String a() {
        if (!g) {
            C0003a.b("OpenUDID", "Initialisation isn't done");
        }
        return f;
    }

    public static boolean b() {
        return g;
    }

    public static void a(Context context) {
        W w = new W(context);
        String string = w.d.getString("openudid", null);
        f = string;
        if (string == null) {
            w.b = context.getPackageManager().queryIntentServices(new Intent("org.OpenUDID.GETUDID"), 0);
            if (w.b != null) {
                w.c();
                return;
            }
            return;
        }
        g = true;
    }

    class a implements Comparator {
        private a() {
        }

        /* synthetic */ a(W w, byte b) {
            this();
        }

        public final int compare(Object obj, Object obj2) {
            if (((Integer) W.this.f110c.get(obj)).intValue() < ((Integer) W.this.f110c.get(obj2)).intValue()) {
                return 1;
            }
            if (W.this.f110c.get(obj) == W.this.f110c.get(obj2)) {
                return 0;
            }
            return -1;
        }
    }
}
