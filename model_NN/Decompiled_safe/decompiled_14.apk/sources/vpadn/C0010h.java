package vpadn;

import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: vpadn.h  reason: case insensitive filesystem */
public final class C0010h {
    public boolean a = false;
    public long b = 0;

    /* renamed from: c  reason: collision with root package name */
    public long f117c = 0;

    public final JSONObject a() throws JSONException {
        return new JSONObject("{loaded:" + this.b + ",total:" + this.f117c + ",lengthComputable:" + (this.a ? "true" : "false") + "}");
    }
}
