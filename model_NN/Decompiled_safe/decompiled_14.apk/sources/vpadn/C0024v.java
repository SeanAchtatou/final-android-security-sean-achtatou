package vpadn;

import android.util.Base64;
import org.json.JSONArray;
import org.json.JSONObject;

/* renamed from: vpadn.v  reason: case insensitive filesystem */
public final class C0024v {
    private static String[] f = {"No result", "OK", "Class not found", "Illegal access", "Instantiation error", "Malformed url", "IO error", "Invalid action", "JSON error", "Error"};
    private final int a;
    private final int b;

    /* renamed from: c  reason: collision with root package name */
    private boolean f125c;
    private String d;
    private String e;

    /* renamed from: vpadn.v$a */
    public enum a {
        NO_RESULT,
        OK,
        CLASS_NOT_FOUND_EXCEPTION,
        ILLEGAL_ACCESS_EXCEPTION,
        INSTANTIATION_EXCEPTION,
        MALFORMED_URL_EXCEPTION,
        IO_EXCEPTION,
        INVALID_ACTION,
        JSON_EXCEPTION,
        ERROR
    }

    public C0024v(a aVar) {
        this(aVar, f[aVar.ordinal()]);
    }

    public C0024v(a aVar, String str) {
        this.f125c = false;
        this.a = aVar.ordinal();
        this.b = str == null ? 5 : 1;
        this.d = str;
    }

    public C0024v(a aVar, JSONArray jSONArray) {
        this.f125c = false;
        this.a = aVar.ordinal();
        this.b = 2;
        this.e = jSONArray.toString();
    }

    public C0024v(a aVar, JSONObject jSONObject) {
        this.f125c = false;
        this.a = aVar.ordinal();
        this.b = 2;
        this.e = jSONObject.toString();
    }

    public C0024v(a aVar, int i) {
        this.f125c = false;
        this.a = aVar.ordinal();
        this.b = 3;
        this.e = String.valueOf(i);
    }

    public C0024v(a aVar, float f2) {
        this.f125c = false;
        this.a = aVar.ordinal();
        this.b = 3;
        this.e = String.valueOf(f2);
    }

    public C0024v(a aVar, boolean z) {
        this.f125c = false;
        this.a = aVar.ordinal();
        this.b = 4;
        this.e = Boolean.toString(z);
    }

    public C0024v(a aVar, byte[] bArr) {
        this.f125c = false;
        this.a = aVar.ordinal();
        this.b = 6;
        this.e = Base64.encodeToString(bArr, 2);
    }

    public final void a(boolean z) {
        this.f125c = z;
    }

    public final int a() {
        return this.a;
    }

    public final int b() {
        return this.b;
    }

    public final String c() {
        if (this.e == null) {
            this.e = JSONObject.quote(this.d);
        }
        return this.e;
    }

    public final String d() {
        return this.d;
    }

    public final boolean e() {
        return this.f125c;
    }
}
