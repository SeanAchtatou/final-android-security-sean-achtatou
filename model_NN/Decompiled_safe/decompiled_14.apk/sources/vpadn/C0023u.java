package vpadn;

import android.content.Intent;
import android.content.res.XmlResourceParser;
import android.util.Log;
import c.CordovaWebView;
import c.Device;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONException;
import org.xmlpull.v1.XmlPullParserException;
import vpadn.C0024v;

/* renamed from: vpadn.u  reason: case insensitive filesystem */
public final class C0023u {
    private static String a = "PluginManager";
    private final ConcurrentHashMap<String, C0022t> b = new ConcurrentHashMap<>();

    /* renamed from: c  reason: collision with root package name */
    private final C0018p f124c;
    private final CordovaWebView d;
    private boolean e;
    private HashMap<String, String> f = new HashMap<>();
    private HashMap<String, String> g = new HashMap<>();

    public C0023u(CordovaWebView cordovaWebView, C0018p pVar) {
        this.f124c = pVar;
        this.d = cordovaWebView;
        this.e = true;
        this.g.put("App", "c.App");
        this.g.put("Geolocation", "c.GeoBroker");
        this.g.put(Device.TAG, "c.Device");
        this.g.put("Accelerometer", "c.AccelListener");
        this.g.put("Compass", "c.CompassListener");
        this.g.put("Media", "c.AudioHandler");
        this.g.put("Camera", "c.CameraLauncher");
        this.g.put("File", "c.FileUtils");
        this.g.put("NetworkStatus", "c.NetworkManager");
        this.g.put("Notification", "c.Notification");
        this.g.put("Storage", "c.Storage");
        this.g.put("FileTransfer", "c.FileTransfer");
        this.g.put("Capture", "c.Capture");
        this.g.put("Battery", "c.BatteryListener");
        this.g.put("Echo", "c.Echo");
        this.g.put("Globalization", "c.Globalization");
        this.g.put("InAppBrowser", "c.InAppBrowser");
        this.g.put("VponSdk", "com.vpon.cordova.VponSDKPlugIn");
    }

    public final void a() {
        String str;
        String str2;
        boolean z;
        C0020r.b(a, "init()");
        if (this.e) {
            if (this.g.size() > 0) {
                Log.i(a, "----->>loadPlugin for HashMap (not in xml)");
                for (Map.Entry next : this.g.entrySet()) {
                    a(new C0022t((String) next.getKey(), (String) next.getValue(), false));
                }
            } else {
                int identifier = this.f124c.a().getResources().getIdentifier("config", "xml", this.f124c.a().getPackageName());
                if (identifier == 0) {
                    identifier = this.f124c.a().getResources().getIdentifier("plugins", "xml", this.f124c.a().getPackageName());
                    C0020r.c(a, "Using plugins.xml instead of config.xml.  plugins.xml will eventually be deprecated");
                }
                if (identifier == 0) {
                    C0020r.e(a, "=====================================================================================");
                    C0020r.e(a, "ERROR: plugin.xml is missing.  Add res/xml/plugins.xml to your project.");
                    C0020r.e(a, "https://git-wip-us.apache.org/repos/asf?p=incubator-cordova-android.git;a=blob;f=framework/res/xml/plugins.xml");
                    C0020r.e(a, "=====================================================================================");
                } else {
                    XmlResourceParser xml = this.f124c.a().getResources().getXml(identifier);
                    String str3 = "";
                    int i = -1;
                    String str4 = "";
                    boolean z2 = false;
                    while (i != 1) {
                        if (i == 2) {
                            String name = xml.getName();
                            if (name.equals("plugin")) {
                                String attributeValue = xml.getAttributeValue(null, "name");
                                String attributeValue2 = xml.getAttributeValue(null, "value");
                                a(new C0022t(attributeValue, attributeValue2, "true".equals(xml.getAttributeValue(null, "onload"))));
                                str = attributeValue;
                                str2 = attributeValue2;
                                z = z2;
                            } else if (name.equals("url-filter")) {
                                this.f.put(xml.getAttributeValue(null, "value"), str4);
                                str = str4;
                                str2 = str3;
                                z = z2;
                            } else if (name.equals("feature")) {
                                xml.getAttributeValue(null, "name");
                                str = str4;
                                str2 = str3;
                                z = true;
                            } else {
                                if (name.equals("param") && z2) {
                                    String attributeValue3 = xml.getAttributeValue(null, "name");
                                    if (attributeValue3.equals("service")) {
                                        str4 = xml.getAttributeValue(null, "value");
                                    } else if (attributeValue3.equals("package")) {
                                        str3 = xml.getAttributeValue(null, "value");
                                    }
                                    if (str4.length() > 0 && str3.length() > 0) {
                                        a(new C0022t(str4, str3, "true".equals(xml.getAttributeValue(null, "onload"))));
                                        str = "";
                                        str2 = "";
                                        z = z2;
                                    }
                                }
                                str = str4;
                                str2 = str3;
                                z = z2;
                            }
                        } else {
                            if (i == 3 && xml.getName().equals("feature")) {
                                str = "";
                                str2 = "";
                                z = false;
                            }
                            str = str4;
                            str2 = str3;
                            z = z2;
                        }
                        try {
                            i = xml.next();
                            z2 = z;
                            str3 = str2;
                            str4 = str;
                        } catch (XmlPullParserException e2) {
                            e2.printStackTrace();
                            z2 = z;
                            str3 = str2;
                            str4 = str;
                        } catch (IOException e3) {
                            e3.printStackTrace();
                            z2 = z;
                            str3 = str2;
                            str4 = str;
                        }
                    }
                }
            }
            this.e = false;
        } else {
            a(false);
            b();
            for (C0022t tVar : this.b.values()) {
                tVar.b = null;
            }
        }
        for (C0022t next2 : this.b.values()) {
            if (next2.f123c) {
                next2.a(this.d, this.f124c);
            }
        }
    }

    public final boolean a(String str, String str2, String str3, String str4) {
        C0019q a2 = a(str);
        if (a2 == null) {
            this.d.a(new C0024v(C0024v.a.CLASS_NOT_FOUND_EXCEPTION), str3);
            return true;
        }
        try {
            C0017o oVar = new C0017o(str3, this.d);
            if (a2.execute(str2, str4, oVar)) {
                return oVar.b;
            }
            this.d.a(new C0024v(C0024v.a.INVALID_ACTION), str3);
            return true;
        } catch (JSONException e2) {
            this.d.a(new C0024v(C0024v.a.JSON_EXCEPTION), str3);
            return true;
        }
    }

    public final C0019q a(String str) {
        C0022t tVar = this.b.get(str);
        if (tVar == null) {
            return null;
        }
        C0019q qVar = tVar.b;
        return qVar == null ? tVar.a(this.d, this.f124c) : qVar;
    }

    private void a(C0022t tVar) {
        this.b.put(tVar.a, tVar);
    }

    public final void a(boolean z) {
        for (C0022t next : this.b.values()) {
            if (next.b != null) {
                next.b.onPause(z);
            }
        }
    }

    public final void b(boolean z) {
        for (C0022t next : this.b.values()) {
            if (next.b != null) {
                next.b.onResume(z);
            }
        }
    }

    public final void b() {
        for (C0022t next : this.b.values()) {
            if (next.b != null) {
                next.b.onDestroy();
            }
        }
    }

    public final Object a(String str, Object obj) {
        Object onMessage;
        Object a2 = this.f124c.a(str, obj);
        if (a2 != null) {
            return a2;
        }
        for (C0022t next : this.b.values()) {
            if (next.b != null && (onMessage = next.b.onMessage(str, obj)) != null) {
                return onMessage;
            }
        }
        return null;
    }

    public final void a(Intent intent) {
        for (C0022t next : this.b.values()) {
            if (next.b != null) {
                next.b.onNewIntent(intent);
            }
        }
    }

    public final boolean b(String str) {
        for (Map.Entry next : this.f.entrySet()) {
            if (str.startsWith((String) next.getKey())) {
                return a((String) next.getValue()).onOverrideUrlLoading(str);
            }
        }
        return false;
    }

    public final void c() {
        for (C0022t tVar : this.b.values()) {
            C0019q qVar = tVar.b;
            if (qVar != null) {
                qVar.onReset();
            }
        }
    }
}
