package vpadn;

import android.content.Intent;
import c.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;

/* renamed from: vpadn.q  reason: case insensitive filesystem */
public class C0019q {
    private static /* synthetic */ boolean a = (!C0019q.class.desiredAssertionStatus());
    public C0018p cordova;
    public String id;
    public CordovaWebView webView;

    public void initialize(C0018p pVar, CordovaWebView cordovaWebView) {
        if (a || this.cordova == null) {
            this.cordova = pVar;
            this.webView = cordovaWebView;
            return;
        }
        throw new AssertionError();
    }

    public boolean execute(String str, String str2, C0017o oVar) throws JSONException {
        return execute(str, new JSONArray(str2), oVar);
    }

    public boolean execute(String str, JSONArray jSONArray, C0017o oVar) throws JSONException {
        return execute(str, new C0005c(jSONArray), oVar);
    }

    public boolean execute(String str, C0005c cVar, C0017o oVar) throws JSONException {
        return false;
    }

    public void onPause(boolean z) {
    }

    public void onResume(boolean z) {
    }

    public void onNewIntent(Intent intent) {
    }

    public void onDestroy() {
    }

    public Object onMessage(String str, Object obj) {
        return null;
    }

    public void onActivityResult(int i, int i2, Intent intent) {
    }

    public boolean onOverrideUrlLoading(String str) {
        return false;
    }

    public void onReset() {
    }
}
