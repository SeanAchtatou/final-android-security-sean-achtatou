package vpadn;

import c.CordovaWebView;

/* renamed from: vpadn.t  reason: case insensitive filesystem */
public final class C0022t {
    public String a = "";
    public C0019q b = null;

    /* renamed from: c  reason: collision with root package name */
    public boolean f123c = false;
    private String d = "";

    public C0022t(String str, String str2, boolean z) {
        this.a = str;
        this.d = str2;
        this.f123c = z;
    }

    public final C0019q a(CordovaWebView cordovaWebView, C0018p pVar) {
        Class<?> cls;
        if (this.b != null) {
            return this.b;
        }
        try {
            String str = this.d;
            if (str != null) {
                cls = Class.forName(str);
            } else {
                cls = null;
            }
            if (cls != null ? C0019q.class.isAssignableFrom(cls) : false) {
                this.b = (C0019q) cls.newInstance();
                this.b.initialize(pVar, cordovaWebView);
                return this.b;
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error adding plugin " + this.d + ".");
        }
        return null;
    }
}
