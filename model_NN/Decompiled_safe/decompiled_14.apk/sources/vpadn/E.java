package vpadn;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import c.NetworkManager;
import com.google.ads.AdActivity;
import com.vpon.ads.VponAdRequest;
import com.vpon.ads.VponAdSize;
import com.vpon.webview.VponAdWebView;
import com.vpon.widget.VponActivity;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONObject;

public final class E extends B implements J, K, L {
    private boolean A = false;
    private boolean B = false;
    private VponAdWebView o;
    /* access modifiers changed from: private */
    public VponAdWebView p;
    private P q;
    private RelativeLayout r = null;
    private ImageView s = null;
    private boolean t = false;
    /* access modifiers changed from: private */
    public I u = null;
    private int v = -1;
    private int w;
    private boolean x = true;
    private String y = NetworkManager.TYPE_NONE;
    /* access modifiers changed from: private */
    public String z = null;

    public E(Activity activity, I i) {
        super(activity);
        this.u = i;
        this.v = Resources.getSystem().getConfiguration().orientation;
    }

    public final void a(boolean z2) {
    }

    public final void u() {
        C0003a.a("VponInterstitialAdController", "call onVponBannerImpression SUCCESS");
        this.q.f107c = true;
    }

    public final void a(F f) {
        C0003a.b("VponInterstitialAdController", "call onVponBannerImpression Failed");
        this.q.f107c = true;
    }

    public final void b(final Object obj) {
        this.a.runOnUiThread(new Runnable() {
            public final void run() {
                E.a(E.this, (P) obj);
            }
        });
    }

    public final void r() {
        if (!this.h) {
            this.h = true;
            a("onshow", (JSONObject) null);
        }
    }

    public final void s() {
        if (this.h) {
            this.h = false;
            a("onhide", (JSONObject) null);
        }
    }

    public final void t() {
        if (this.o != null && this.o.h().equals("init")) {
            this.o.setVponWebViewId("init-finish");
            C0003a.c("VponInterstitialAdController", "Load init html template finish");
        } else if (this.o != null) {
            this.o.stopLoading();
            this.o.e();
            this.o.destroy();
            this.o = null;
        }
    }

    public final void a(int i, int i2) {
        if (this.p == null || this.a != null) {
        }
    }

    public final void a(int i, int i2, int i3, int i4) {
        if (this.p != null) {
            Rect rect = new Rect();
            this.p.getGlobalVisibleRect(rect);
            int round = Math.round(VponAdSize.convertPixelsToDp((float) rect.left, this.a));
            int round2 = Math.round(VponAdSize.convertPixelsToDp((float) rect.top, this.a));
            int round3 = Math.round(VponAdSize.convertPixelsToDp((float) (rect.right - rect.left), this.a));
            int round4 = Math.round(VponAdSize.convertPixelsToDp((float) (rect.bottom - rect.top), this.a));
            C0003a.b("VponInterstitialAdController", "X1:" + round + " Y1:" + round2 + " wDip:" + round3 + " hDip:" + round4);
            try {
                this.i.put("x", round);
                this.i.put("y", round2);
                this.i.put("w", round3);
                this.i.put("h", round4);
                a("ad_pos_change", this.i);
            } catch (Exception e) {
                e.printStackTrace();
                C0003a.b("VponInterstitialAdController", "onWebViewLayoutChanged throw exception");
            }
        }
    }

    public final void a(WebView webView, int i, String str, String str2) {
        this.B = false;
    }

    public final boolean m() {
        return this.B;
    }

    public final void a(VponAdRequest vponAdRequest) {
        try {
            if (!l()) {
                C0003a.b("VponInterstitialAdController", "[Interstitial] Device is not on-line");
                if (this.u != null) {
                    this.u.onVponAdFailed(VponAdRequest.VponErrorCode.NETWORK_ERROR);
                    return;
                }
                return;
            }
            this.m = vponAdRequest;
            JSONObject jSONObject = new JSONObject();
            JSONObject b = M.a().b(this.a, (JSONObject) null);
            if (this.d.equals("tw")) {
                jSONObject.put("pf", "TW");
                b.put("pf", "TW");
            } else {
                jSONObject.put("pf", "CN");
                b.put("pf", "CN");
            }
            i();
            JSONObject a = a(b, vponAdRequest, this.j, this.k);
            a.put("build", "20803102");
            String replaceAll = "<!doctype html> <html> <head> <meta charset='utf-8'/>\n<script type='text/javascript' charset='utf-8' src='http://m.vpon.com/sdk/vpadn-sdk-core-v1.js'></script>\n<script type='text/javascript' charset='utf-8'>\nVPSDK_LoadSdkConstants( JSON_REPLACE1 );\nVPSDK_BuildAdReqUrl( JSON_REPLACE2 );\n</script><body></body></html>".replaceAll("JSON_REPLACE1", jSONObject.toString(4)).replaceAll("JSON_REPLACE2", a.toString(4));
            C0003a.a("VponInterstitialAdController", replaceAll);
            this.o = new VponAdWebView("init", this.a, this, this);
            this.o.loadDataWithBaseURL("file:///android_asset/www/vpon", replaceAll, "text/html", "utf-8", null);
        } catch (Exception e) {
            e.printStackTrace();
            C0003a.b("VponInterstitialAdController", "[Interstitial]loadInitHtmlTemplate throw Exception: " + e.getMessage());
        }
    }

    private JSONObject a(JSONObject jSONObject, VponAdRequest vponAdRequest, long j, long j2) {
        try {
            jSONObject.put("sid", j);
            jSONObject.put("seq", j2);
            jSONObject.put("format", "mi");
            jSONObject.put("bid", this.b);
            if (vponAdRequest.isTestDevice(this.a)) {
                jSONObject.put("adtest", 1);
            } else {
                jSONObject.put("adtest", 0);
            }
            if (this.i.length() >= 4) {
                if (this.i.has("x")) {
                    jSONObject.put("ad_x", this.i.getInt("x"));
                }
                if (this.i.has("y")) {
                    jSONObject.put("ad_y", this.i.getInt("y"));
                }
                if (this.i.has("w")) {
                    jSONObject.put("ad_w", this.i.getInt("w"));
                }
                if (this.i.has("h")) {
                    jSONObject.put("ad_h", this.i.getInt("h"));
                }
            } else {
                jSONObject.put("ad_x", 0);
                jSONObject.put("ad_y", 0);
                jSONObject.put("ad_w", 0);
                jSONObject.put("ad_h", 0);
            }
            if (this.h) {
                jSONObject.put("ad_v", 1);
            } else {
                jSONObject.put("ad_v", 0);
            }
            Set<String> keywords = vponAdRequest.getKeywords();
            JSONArray jSONArray = new JSONArray();
            for (String put : keywords) {
                jSONArray.put(put);
            }
            jSONObject.put("kw", jSONArray);
            JSONObject c2 = M.a().c(this.a, (JSONObject) null);
            int age = vponAdRequest.getAge();
            if (age > 0 && age < 150) {
                c2.put("age", age);
            }
            if (!vponAdRequest.getGender().equals(VponAdRequest.Gender.UNKNOWN)) {
                if (vponAdRequest.getGender().equals(VponAdRequest.Gender.MALE)) {
                    c2.put("gender", 0);
                } else {
                    c2.put("gender", 1);
                }
            }
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-DD");
            Date birthday = vponAdRequest.getBirthday();
            if (birthday != null) {
                c2.put("bday", simpleDateFormat.format(birthday));
            }
            jSONObject.put("ms", C0003a.e("NH/mLeyCBfokzYKUPNGEEg==", c2.toString()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jSONObject;
    }

    public final void n() {
        try {
            C0003a.a("VponInterstitialAdController", "------> showInterstitialAd()");
            if (this.B) {
                this.B = false;
                if (this.a != null) {
                    if (!(this.a == null || this.q == null || this.q.f107c)) {
                        String str = (String) this.g.remove("url_type_impression");
                        if (str != null) {
                            C0003a.a("VponInterstitialAdController", "----------->>>[interstitial] Send impression to server impressionUrl:" + str);
                            new G(str, this, this.e).execute(new Object[0]);
                        } else {
                            C0003a.b("VponInterstitialAdController", "Cannot get interstitial impression URL");
                        }
                    }
                    if (this.u != null) {
                        this.u.onVponPresent();
                    }
                    Intent intent = new Intent(this.a, VponActivity.class);
                    intent.setFlags(65536);
                    intent.setFlags(268435456);
                    Bundle bundle = new Bundle();
                    bundle.putString("adType", "interstitial");
                    bundle.putString(AdActivity.HTML_PARAM, this.z);
                    bundle.putBoolean("isMraidAd", false);
                    bundle.putBoolean("isUseCustomClose", false);
                    String uuid = UUID.randomUUID().toString();
                    bundle.putString("getControllerKey", uuid);
                    O.a().a(uuid, this);
                    this.w = this.a.getRequestedOrientation();
                    this.v = Resources.getSystem().getConfiguration().orientation;
                    bundle.putInt("originalRequestedOrientation", this.w);
                    bundle.putInt("beforeActivityOrientation", this.v);
                    bundle.putString("forceOrientation", this.y);
                    bundle.putBoolean("isAllowOrientationChange", this.x);
                    bundle.putString("click_url", (String) this.g.get("url_type_click"));
                    bundle.putString("url", (String) this.g.get("url_type_banner"));
                    if (this.q != null) {
                        X.a(this.a);
                        Location a = X.a();
                        if (a != null) {
                            P p2 = this.q;
                            P p3 = this.q;
                            bundle.putInt("distance", Y.a(null, null, a.getLatitude(), a.getLongitude()));
                        }
                    }
                    bundle.putLong("session_id", j());
                    bundle.putLong("sequence_number", k());
                    bundle.putBoolean("isFullScreen", (this.a.getWindow().getAttributes().flags & 1024) != 0);
                    intent.putExtras(bundle);
                    if (this.p != null) {
                        this.p.stopLoading();
                        this.p.e();
                        this.p.destroy();
                        this.p = null;
                    }
                    this.a.startActivity(intent);
                    return;
                }
                return;
            }
            C0003a.b("VponInterstitialAdController", "Interstitial Banner is not ready, cannot call showInterstitial");
        } catch (Exception e) {
            e.printStackTrace();
            C0003a.b("VponInterstitialAdController", "showInterstitialAd throw Exception:" + e.getMessage());
        }
    }

    static /* synthetic */ void a(E e, P p2) {
        C0003a.a("VponInterstitialAdController", "------> prepareInterstitialAd()");
        if (e.a != null) {
            e.q = p2;
            String str = e.q.a;
            e.z = str;
            C0003a.a("VponInterstitialAdController", "real get Interstitial Html:" + str);
            if (e.p != null) {
                e.p.stopLoading();
                e.p.removeAllViews();
                e.p.e();
                e.p = null;
            }
            e.p = new VponAdWebView("InterstitialAdWebView(new Activity)", e.a, e, e);
            new Handler().postDelayed(new Runnable() {
                public final void run() {
                    E.b(E.this);
                }
            }, 3000);
            e.B = true;
            if (e.u != null) {
                e.u.onVponAdReceived();
            }
        }
    }

    static /* synthetic */ void b(E e) {
        if (e.B) {
            C0003a.a("VponInterstitialAdController", "---> CALL asyncLoadHtmlToWebViewByUrl()");
            new AsyncTask<Object, Integer, Integer>() {
                /* access modifiers changed from: protected */
                public final /* synthetic */ Object doInBackground(Object... objArr) {
                    String str = (String) E.this.g.get("url_type_banner");
                    if (C0003a.c(E.this.z) || E.this.p == null) {
                        if (E.this.p == null) {
                            C0003a.b("VponInterstitialAdController", "load interstitial ad to webview error-->mShowWebView == null");
                        }
                        if (C0003a.c(E.this.z)) {
                            C0003a.b("VponInterstitialAdController", "load interstitial ad to webview error-->StringUtils.isBlank(mHtml) == false mHtml:" + E.this.z);
                        }
                    } else {
                        E.this.p.loadDataWithBaseURL(str, E.this.z, "text/html", "utf-8", null);
                    }
                    return 1;
                }
            }.execute(new Object[0]);
        }
    }

    static /* synthetic */ void e(E e) {
        if (C0003a.c(e.b)) {
            C0003a.b("VponInterstitialAdController", "[Interstitial] bannerId is blank");
            if (e.u != null) {
                e.u.onVponAdFailed(VponAdRequest.VponErrorCode.INTERNAL_ERROR);
            }
        }
        if (e.a == null) {
            return;
        }
        if (Y.d(e.a)) {
            String str = (String) e.g.get("url_type_banner");
            if (str != null) {
                new H(str, e, e.e).execute(new Object[0]);
                return;
            }
            C0003a.b("VponInterstitialAdController", "[Interstitial] mUrlMap.get(VponControllerInterface.URL_TYPE_BANNER) return null");
            if (e.u != null) {
                e.u.onVponAdFailed(VponAdRequest.VponErrorCode.INTERNAL_ERROR);
                return;
            }
            return;
        }
        C0003a.b("VponInterstitialAdController", "[Interstitial] permission-checking is failed!!");
        if (e.u != null) {
            e.u.onVponAdFailed(VponAdRequest.VponErrorCode.INTERNAL_ERROR);
        }
    }

    public final void p() {
        if (this.u != null) {
            this.u.onVponDismiss();
        }
    }

    public final void o() {
        C0003a.a("VponInterstitialAdController", "Call webViewHandleDestroy()");
        if (this.o != null) {
            this.o.stopLoading();
            this.o.removeAllViews();
            this.o.e();
            this.o = null;
        }
        if (this.p != null) {
            this.p.stopLoading();
            this.p.removeAllViews();
            this.p.e();
            this.p = null;
        }
        this.f = true;
    }

    /* access modifiers changed from: protected */
    public final void c() {
        new Handler().post(new Runnable() {
            public final void run() {
                E.e(E.this);
            }
        });
    }

    /* access modifiers changed from: protected */
    public final void a(final Object obj) {
        new Handler().post(new Runnable() {
            public final void run() {
                C0003a.b("VponInterstitialAdController", "[interstitial] doLoadBannerFail");
                E.this.o();
                if (obj == null || !(obj instanceof String) || !obj.toString().equals("NO_FILL")) {
                    E.this.u.onVponAdFailed(VponAdRequest.VponErrorCode.INTERNAL_ERROR);
                } else {
                    E.this.u.onVponAdFailed(VponAdRequest.VponErrorCode.NO_FILL);
                }
            }
        });
    }

    public final void y() {
        if (this.u != null) {
            this.u.onVponLeaveApplication();
        }
    }

    public final void z() {
    }
}
