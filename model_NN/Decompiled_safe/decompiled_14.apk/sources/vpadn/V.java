package vpadn;

import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;
import org.apache.http.HttpEntity;
import org.apache.http.entity.HttpEntityWrapper;

final class V extends HttpEntityWrapper {
    public V(HttpEntity httpEntity) {
        super(httpEntity);
    }

    public final InputStream getContent() throws IOException {
        return new GZIPInputStream(this.wrappedEntity.getContent());
    }

    public final long getContentLength() {
        return -1;
    }
}
