package de.shandschuh.sparserss.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.os.Environment;
import android.text.TextUtils;
import de.shandschuh.sparserss.Strings;
import de.shandschuh.sparserss.provider.FeedData;
import java.io.File;

public class FeedDataContentProvider extends ContentProvider {
    private static final String ADD = " ADD ";
    private static final String ALTER_TABLE = "ALTER TABLE ";
    /* access modifiers changed from: private */
    public static final String BACKUPOPML = (Environment.getExternalStorageDirectory() + "/sparserss/backup.opml");
    private static final String DATABASE_NAME = "sparserss.db";
    private static final int DATABASE_VERSION = 6;
    private static final String EQUALS_ONE = "=1";
    private static final String FOLDER = (Environment.getExternalStorageDirectory() + "/sparserss/");
    public static final String IMAGEFOLDER = (Environment.getExternalStorageDirectory() + "/sparserss/images/");
    private static final String[] PROJECTION_PRIORITY;
    private static final String TABLE_ENTRIES = "entries";
    protected static final String TABLE_FEEDS = "feeds";
    private static final int URI_ALLENTRIES = 5;
    private static final int URI_ALLENTRIES_ENTRY = 6;
    private static final int URI_ENTRIES = 3;
    private static final int URI_ENTRY = 4;
    private static final int URI_FAVORITES = 7;
    private static final int URI_FAVORITES_ENTRY = 8;
    private static final int URI_FEED = 2;
    private static final int URI_FEEDS = 1;
    private static UriMatcher URI_MATCHER = new UriMatcher(-1);
    private String[] MAXPRIORITY;
    private SQLiteDatabase database;

    public FeedDataContentProvider() {
        String[] strArr = new String[URI_FEEDS];
        strArr[0] = "MAX(priority)";
        this.MAXPRIORITY = strArr;
    }

    static {
        String[] strArr = new String[URI_FEEDS];
        strArr[0] = "priority";
        PROJECTION_PRIORITY = strArr;
        URI_MATCHER.addURI(FeedData.AUTHORITY, TABLE_FEEDS, URI_FEEDS);
        URI_MATCHER.addURI(FeedData.AUTHORITY, "feeds/#", URI_FEED);
        URI_MATCHER.addURI(FeedData.AUTHORITY, "feeds/#/entries", URI_ENTRIES);
        URI_MATCHER.addURI(FeedData.AUTHORITY, "feeds/#/entries/#", URI_ENTRY);
        URI_MATCHER.addURI(FeedData.AUTHORITY, TABLE_ENTRIES, URI_ALLENTRIES);
        URI_MATCHER.addURI(FeedData.AUTHORITY, "entries/#", 6);
        URI_MATCHER.addURI(FeedData.AUTHORITY, "favorites", URI_FAVORITES);
        URI_MATCHER.addURI(FeedData.AUTHORITY, "favorites/#", URI_FAVORITES_ENTRY);
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        public DatabaseHelper(Context context, String name, int version) {
            super(context, name, (SQLiteDatabase.CursorFactory) null, version);
            context.sendBroadcast(new Intent(Strings.ACTION_UPDATEWIDGET));
        }

        public void onCreate(SQLiteDatabase database) {
            database.execSQL(createTable(FeedDataContentProvider.TABLE_FEEDS, FeedData.FeedColumns.COLUMNS, FeedData.FeedColumns.TYPES));
            database.execSQL(createTable(FeedDataContentProvider.TABLE_ENTRIES, FeedData.EntryColumns.COLUMNS, FeedData.EntryColumns.TYPES));
            File backupFile = new File(FeedDataContentProvider.BACKUPOPML);
            if (backupFile.exists()) {
                OPML.importFromFile(backupFile, database);
            }
        }

        private String createTable(String tableName, String[] columns, String[] types) {
            if (tableName == null || columns == null || types == null || types.length != columns.length || types.length == 0) {
                throw new IllegalArgumentException("Invalid parameters for creating table " + tableName);
            }
            StringBuilder stringBuilder = new StringBuilder("CREATE TABLE ");
            stringBuilder.append(tableName);
            stringBuilder.append(" (");
            int i = columns.length;
            for (int n = 0; n < i; n += FeedDataContentProvider.URI_FEEDS) {
                if (n > 0) {
                    stringBuilder.append(", ");
                }
                stringBuilder.append(columns[n]).append(' ').append(types[n]);
            }
            return stringBuilder.append(");").toString();
        }

        public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
            if (oldVersion < FeedDataContentProvider.URI_FEED) {
                database.execSQL(FeedDataContentProvider.ALTER_TABLE + FeedDataContentProvider.TABLE_FEEDS + FeedDataContentProvider.ADD + "priority" + ' ' + "INT");
            }
            if (oldVersion < FeedDataContentProvider.URI_ENTRIES) {
                database.execSQL(FeedDataContentProvider.ALTER_TABLE + FeedDataContentProvider.TABLE_ENTRIES + FeedDataContentProvider.ADD + FeedData.EntryColumns.FAVORITE + ' ' + "INTEGER(1)");
            }
            if (oldVersion < FeedDataContentProvider.URI_ENTRY) {
                database.execSQL(FeedDataContentProvider.ALTER_TABLE + FeedDataContentProvider.TABLE_FEEDS + FeedDataContentProvider.ADD + FeedData.FeedColumns.FETCHMODE + ' ' + "INT");
            }
            if (oldVersion < FeedDataContentProvider.URI_ALLENTRIES) {
                database.execSQL(FeedDataContentProvider.ALTER_TABLE + FeedDataContentProvider.TABLE_FEEDS + FeedDataContentProvider.ADD + FeedData.FeedColumns.REALLASTUPDATE + ' ' + "DATETIME");
            }
            if (oldVersion < 6) {
                String[] strArr = new String[FeedDataContentProvider.URI_FEEDS];
                strArr[0] = "_id";
                Cursor cursor = database.query(FeedDataContentProvider.TABLE_FEEDS, strArr, null, null, null, null, "_id");
                int count = 0;
                while (cursor.moveToNext()) {
                    StringBuilder append = new StringBuilder("UPDATE ").append(FeedDataContentProvider.TABLE_FEEDS).append(" SET ").append("priority").append('=');
                    int count2 = count + FeedDataContentProvider.URI_FEEDS;
                    database.execSQL(append.append(count).append(" WHERE _ID=").append(cursor.getLong(0)).toString());
                    count = count2;
                }
                cursor.close();
            }
        }

        public synchronized SQLiteDatabase getWritableDatabase() {
            SQLiteDatabase writableDatabase;
            File file = new File(Environment.getExternalStorageDirectory() + "/sparserss/sparserss.db");
            if (file.exists()) {
                SQLiteDatabase newDatabase = super.getWritableDatabase();
                try {
                    SQLiteDatabase oldDatabase = SQLiteDatabase.openDatabase(Environment.getExternalStorageDirectory() + "/sparserss/sparserss.db", null, 268435456);
                    Cursor cursor = oldDatabase.query(FeedDataContentProvider.TABLE_ENTRIES, null, null, null, null, null, null);
                    newDatabase.beginTransaction();
                    String[] columnNames = cursor.getColumnNames();
                    int i = columnNames.length;
                    int[] columnIndices = new int[i];
                    for (int n = 0; n < i; n += FeedDataContentProvider.URI_FEEDS) {
                        columnIndices[n] = cursor.getColumnIndex(columnNames[n]);
                    }
                    while (cursor.moveToNext()) {
                        ContentValues values = new ContentValues();
                        for (int n2 = 0; n2 < i; n2 += FeedDataContentProvider.URI_FEEDS) {
                            if (!cursor.isNull(columnIndices[n2])) {
                                values.put(columnNames[n2], cursor.getString(columnIndices[n2]));
                            }
                        }
                        newDatabase.insert(FeedDataContentProvider.TABLE_ENTRIES, null, values);
                    }
                    cursor.close();
                    Cursor cursor2 = oldDatabase.query(FeedDataContentProvider.TABLE_FEEDS, null, null, null, null, null, "_id");
                    String[] columnNames2 = cursor2.getColumnNames();
                    int i2 = columnNames2.length;
                    int[] columnIndices2 = new int[i2];
                    for (int n3 = 0; n3 < i2; n3 += FeedDataContentProvider.URI_FEEDS) {
                        columnIndices2[n3] = cursor2.getColumnIndex(columnNames2[n3]);
                    }
                    int count = 0;
                    while (cursor2.moveToNext()) {
                        ContentValues values2 = new ContentValues();
                        for (int n4 = 0; n4 < i2; n4 += FeedDataContentProvider.URI_FEEDS) {
                            if (!cursor2.isNull(columnIndices2[n4])) {
                                if (FeedData.FeedColumns.ICON.equals(columnNames2[n4])) {
                                    values2.put(FeedData.FeedColumns.ICON, cursor2.getBlob(columnIndices2[n4]));
                                } else {
                                    values2.put(columnNames2[n4], cursor2.getString(columnIndices2[n4]));
                                }
                            }
                        }
                        int count2 = count + FeedDataContentProvider.URI_FEEDS;
                        values2.put("priority", Integer.valueOf(count));
                        newDatabase.insert(FeedDataContentProvider.TABLE_FEEDS, null, values2);
                        count = count2;
                    }
                    cursor2.close();
                    oldDatabase.close();
                    file.delete();
                    newDatabase.setTransactionSuccessful();
                    newDatabase.endTransaction();
                    OPML.exportToFile(FeedDataContentProvider.BACKUPOPML, newDatabase);
                } catch (Exception e) {
                }
                writableDatabase = newDatabase;
            } else {
                writableDatabase = super.getWritableDatabase();
            }
            return writableDatabase;
        }
    }

    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int option = URI_MATCHER.match(uri);
        String table = null;
        StringBuilder where = new StringBuilder();
        switch (option) {
            case URI_FEEDS /*1*/:
                table = TABLE_FEEDS;
                break;
            case URI_FEED /*2*/:
                table = TABLE_FEEDS;
                String feedId = uri.getPathSegments().get(URI_FEEDS);
                final String str = feedId;
                new Thread() {
                    public void run() {
                        FeedDataContentProvider.this.delete(FeedData.EntryColumns.CONTENT_URI(str), null, null);
                    }
                }.start();
                where.append("_id").append('=').append(feedId);
                Cursor priorityCursor = this.database.query(TABLE_FEEDS, PROJECTION_PRIORITY, "_id=" + feedId, null, null, null, null);
                if (!priorityCursor.moveToNext()) {
                    priorityCursor.close();
                    break;
                } else {
                    this.database.execSQL("UPDATE feeds SET priority = priority-1 WHERE priority > " + priorityCursor.getInt(0));
                    priorityCursor.close();
                    break;
                }
            case URI_ENTRIES /*3*/:
                table = TABLE_ENTRIES;
                where.append("feedid").append('=').append(uri.getPathSegments().get(URI_FEEDS));
                break;
            case URI_ENTRY /*4*/:
                table = TABLE_ENTRIES;
                where.append("_id").append('=').append(uri.getPathSegments().get(URI_ENTRIES));
                break;
            case URI_ALLENTRIES /*5*/:
                table = TABLE_ENTRIES;
                break;
            case 6:
            case URI_FAVORITES_ENTRY /*8*/:
                table = TABLE_ENTRIES;
                where.append("_id").append('=').append(uri.getPathSegments().get(URI_FEEDS));
                break;
            case URI_FAVORITES /*7*/:
                table = TABLE_ENTRIES;
                where.append(FeedData.EntryColumns.FAVORITE).append(EQUALS_ONE);
                break;
        }
        if (!TextUtils.isEmpty(selection)) {
            where.append(Strings.DB_AND).append(selection);
        }
        int count = this.database.delete(table, where.toString(), selectionArgs);
        if (table == TABLE_FEEDS) {
            OPML.exportToFile(BACKUPOPML, this.database);
        }
        if (count > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return count;
    }

    public String getType(Uri uri) {
        switch (URI_MATCHER.match(uri)) {
            case URI_FEEDS /*1*/:
                return "vnd.android.cursor.dir/vnd.feeddata.feed";
            case URI_FEED /*2*/:
                return "vnd.android.cursor.item/vnd.feeddata.feed";
            case URI_ENTRIES /*3*/:
            case URI_ALLENTRIES /*5*/:
            case URI_FAVORITES /*7*/:
                return "vnd.android.cursor.dir/vnd.feeddata.entry";
            case URI_ENTRY /*4*/:
            case 6:
            case URI_FAVORITES_ENTRY /*8*/:
                return "vnd.android.cursor.item/vnd.feeddata.entry";
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }

    public Uri insert(Uri uri, ContentValues values) {
        long newId;
        switch (URI_MATCHER.match(uri)) {
            case URI_FEEDS /*1*/:
                Cursor cursor = this.database.query(TABLE_FEEDS, this.MAXPRIORITY, null, null, null, null, null, null);
                if (cursor.moveToNext()) {
                    values.put("priority", Integer.valueOf(cursor.getInt(0) + URI_FEEDS));
                } else {
                    values.put("priority", Integer.valueOf((int) URI_FEEDS));
                }
                cursor.close();
                newId = this.database.insert(TABLE_FEEDS, null, values);
                OPML.exportToFile(BACKUPOPML, this.database);
                break;
            case URI_FEED /*2*/:
            case URI_ENTRY /*4*/:
            default:
                throw new IllegalArgumentException("Illegal insert");
            case URI_ENTRIES /*3*/:
                values.put("feedid", uri.getPathSegments().get(URI_FEEDS));
                newId = this.database.insert(TABLE_ENTRIES, null, values);
                break;
            case URI_ALLENTRIES /*5*/:
                newId = this.database.insert(TABLE_ENTRIES, null, values);
                break;
        }
        if (newId > -1) {
            getContext().getContentResolver().notifyChange(uri, null);
            return ContentUris.withAppendedId(uri, newId);
        }
        throw new SQLException("Could not insert row into " + uri);
    }

    public boolean onCreate() {
        try {
            new File(FOLDER).mkdir();
        } catch (Exception e) {
        }
        this.database = new DatabaseHelper(getContext(), DATABASE_NAME, 6).getWritableDatabase();
        return this.database != null;
    }

    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        int option = URI_MATCHER.match(uri);
        if ((option == URI_FEED || option == URI_FEEDS) && sortOrder == null) {
            sortOrder = "priority";
        }
        switch (option) {
            case URI_FEEDS /*1*/:
                queryBuilder.setTables(TABLE_FEEDS);
                break;
            case URI_FEED /*2*/:
                queryBuilder.setTables(TABLE_FEEDS);
                queryBuilder.appendWhere(new StringBuilder("_id").append('=').append(uri.getPathSegments().get(URI_FEEDS)));
                break;
            case URI_ENTRIES /*3*/:
                queryBuilder.setTables(TABLE_ENTRIES);
                queryBuilder.appendWhere(new StringBuilder("feedid").append('=').append(uri.getPathSegments().get(URI_FEEDS)));
                break;
            case URI_ENTRY /*4*/:
                queryBuilder.setTables(TABLE_ENTRIES);
                queryBuilder.appendWhere(new StringBuilder("_id").append('=').append(uri.getPathSegments().get(URI_ENTRIES)));
                break;
            case URI_ALLENTRIES /*5*/:
                queryBuilder.setTables("entries join (select name, icon, _id as feed_id from feeds) as F on (entries.feedid = F.feed_id)");
                break;
            case 6:
            case URI_FAVORITES_ENTRY /*8*/:
                queryBuilder.setTables(TABLE_ENTRIES);
                queryBuilder.appendWhere(new StringBuilder("_id").append('=').append(uri.getPathSegments().get(URI_FEEDS)));
                break;
            case URI_FAVORITES /*7*/:
                queryBuilder.setTables("entries join (select name, icon, _id as feed_id from feeds) as F on (entries.feedid = F.feed_id)");
                queryBuilder.appendWhere(new StringBuilder(FeedData.EntryColumns.FAVORITE).append(EQUALS_ONE));
                break;
        }
        Cursor cursor = queryBuilder.query(this.database, projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int option = URI_MATCHER.match(uri);
        String table = null;
        StringBuilder where = new StringBuilder();
        switch (option) {
            case URI_FEEDS /*1*/:
                table = TABLE_FEEDS;
                break;
            case URI_FEED /*2*/:
                table = TABLE_FEEDS;
                long feedId = Long.parseLong(uri.getPathSegments().get(URI_FEEDS));
                where.append("_id").append('=').append(feedId);
                if (values != null && values.containsKey("priority")) {
                    int newPriority = values.getAsInteger("priority").intValue();
                    Cursor priorityCursor = this.database.query(TABLE_FEEDS, PROJECTION_PRIORITY, "_id=" + feedId, null, null, null, null);
                    if (!priorityCursor.moveToNext()) {
                        priorityCursor.close();
                        break;
                    } else {
                        int oldPriority = priorityCursor.getInt(0);
                        priorityCursor.close();
                        if (newPriority <= oldPriority) {
                            if (newPriority < oldPriority) {
                                this.database.execSQL("UPDATE feeds SET priority = priority+1 WHERE priority BETWEEN " + newPriority + Strings.DB_AND + (oldPriority - URI_FEEDS));
                                break;
                            }
                        } else {
                            this.database.execSQL("UPDATE feeds SET priority = priority-1 WHERE priority BETWEEN " + (oldPriority + URI_FEEDS) + Strings.DB_AND + newPriority);
                            break;
                        }
                    }
                }
                break;
            case URI_ENTRIES /*3*/:
                table = TABLE_ENTRIES;
                where.append("feedid").append('=').append(uri.getPathSegments().get(URI_FEEDS));
                break;
            case URI_ENTRY /*4*/:
                table = TABLE_ENTRIES;
                where.append("_id").append('=').append(uri.getPathSegments().get(URI_ENTRIES));
                break;
            case URI_ALLENTRIES /*5*/:
                table = TABLE_ENTRIES;
                break;
            case 6:
            case URI_FAVORITES_ENTRY /*8*/:
                table = TABLE_ENTRIES;
                where.append("_id").append('=').append(uri.getPathSegments().get(URI_FEEDS));
                break;
            case URI_FAVORITES /*7*/:
                table = TABLE_ENTRIES;
                where.append(FeedData.EntryColumns.FAVORITE).append(EQUALS_ONE);
                break;
        }
        if (!TextUtils.isEmpty(selection)) {
            if (where.length() > 0) {
                where.append(Strings.DB_AND).append(selection);
            } else {
                where.append(selection);
            }
        }
        int count = this.database.update(table, values, where.toString(), selectionArgs);
        if (table == TABLE_FEEDS && (values.containsKey(FeedData.FeedColumns.NAME) || values.containsKey(FeedData.FeedColumns.URL) || values.containsKey("priority"))) {
            OPML.exportToFile(BACKUPOPML, this.database);
        }
        if (count > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return count;
    }
}
