package de.shandschuh.sparserss.provider;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Xml;
import de.shandschuh.sparserss.Strings;
import de.shandschuh.sparserss.provider.FeedData;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class OPML {
    private static final String AFTERDATE = "</dateCreated></head><body>";
    private static final String CLOSING = "</body></opml>\n";
    private static final String OUTLINE_CLOSING = "\" />";
    private static final String OUTLINE_TITLE = "<outline title=\"";
    private static final String OUTLINE_XMLURL = "\" type=\"rss\" xmlUrl=\"";
    private static final String START = "<?xml version=\"1.0\" encoding=\"utf-8\"?><opml version=\"1.1\"><head><title>Sparse RSS export</title><dateCreated>";
    private static OPMLParser parser = new OPMLParser(null);

    public static void importFromFile(String filename, Context context) throws FileNotFoundException, IOException, SAXException {
        parser.context = context;
        parser.database = null;
        Xml.parse(new InputStreamReader(new FileInputStream(filename)), parser);
    }

    protected static void importFromFile(File file, SQLiteDatabase database) {
        parser.context = null;
        parser.database = database;
        try {
            database.beginTransaction();
            Xml.parse(new InputStreamReader(new FileInputStream(file)), parser);
            database.execSQL("UPDATE " + "feeds" + " SET " + "priority" + '=' + "_id" + "-1");
            database.setTransactionSuccessful();
        } catch (Exception e) {
        } finally {
            database.endTransaction();
        }
    }

    public static void exportToFile(String filename, Context context) throws IOException {
        Cursor cursor = context.getContentResolver().query(FeedData.FeedColumns.CONTENT_URI, new String[]{"_id", FeedData.FeedColumns.NAME, FeedData.FeedColumns.URL}, null, null, null);
        try {
            writeData(filename, cursor);
        } finally {
            cursor.close();
        }
    }

    protected static void exportToFile(String filename, SQLiteDatabase database) {
        Cursor cursor = database.query("feeds", new String[]{"_id", FeedData.FeedColumns.NAME, FeedData.FeedColumns.URL}, null, null, null, null, "priority");
        try {
            writeData(filename, cursor);
        } catch (Exception e) {
        }
        cursor.close();
    }

    private static void writeData(String filename, Cursor cursor) throws IOException {
        StringBuilder builder = new StringBuilder(START);
        builder.append(System.currentTimeMillis());
        builder.append(AFTERDATE);
        while (cursor.moveToNext()) {
            builder.append(OUTLINE_TITLE);
            builder.append(cursor.isNull(1) ? Strings.EMPTY : cursor.getString(1));
            builder.append(OUTLINE_XMLURL);
            builder.append(cursor.getString(2).replace("&", "&amp;"));
            builder.append(OUTLINE_CLOSING);
        }
        builder.append(CLOSING);
        BufferedWriter writer = new BufferedWriter(new FileWriter(filename));
        writer.write(builder.toString());
        writer.close();
    }

    private static class OPMLParser extends DefaultHandler {
        private static final String ATTRIBUTE_TITLE = "title";
        private static final String ATTRIBUTE_XMLURL = "xmlUrl";
        private static final String TAG_BODY = "body";
        private static final String TAG_OUTLINE = "outline";
        private boolean bodyTagEntered;
        /* access modifiers changed from: private */
        public Context context;
        /* access modifiers changed from: private */
        public SQLiteDatabase database;

        private OPMLParser() {
        }

        /* synthetic */ OPMLParser(OPMLParser oPMLParser) {
            this();
        }

        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            String url;
            String str;
            if (!this.bodyTagEntered) {
                if (TAG_BODY.equals(localName)) {
                    this.bodyTagEntered = true;
                }
            } else if (TAG_OUTLINE.equals(localName) && (url = attributes.getValue(Strings.EMPTY, ATTRIBUTE_XMLURL)) != null) {
                String title = attributes.getValue(Strings.EMPTY, "title");
                ContentValues values = new ContentValues();
                values.put(FeedData.FeedColumns.URL, url);
                if (title == null || title.length() <= 0) {
                    str = null;
                } else {
                    str = title;
                }
                values.put(FeedData.FeedColumns.NAME, str);
                if (this.context != null) {
                    Cursor cursor = this.context.getContentResolver().query(FeedData.FeedColumns.CONTENT_URI, null, FeedData.FeedColumns.URL + Strings.DB_ARG, new String[]{url}, null);
                    if (!cursor.moveToFirst()) {
                        this.context.getContentResolver().insert(FeedData.FeedColumns.CONTENT_URI, values);
                    }
                    cursor.close();
                    return;
                }
                this.database.insert("feeds", null, values);
            }
        }

        public void endElement(String uri, String localName, String qName) throws SAXException {
            if (this.bodyTagEntered && TAG_BODY.equals(localName)) {
                this.bodyTagEntered = false;
            }
        }
    }
}
