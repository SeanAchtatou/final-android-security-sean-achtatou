package de.shandschuh.sparserss.provider;

import android.net.Uri;
import android.provider.BaseColumns;

public class FeedData {
    public static final String AUTHORITY = "de.shandschuh.sparserss.provider.FeedData";
    public static final String CONTENT = "content://";
    public static final String FEED_DEFAULTSORTORDER = "priority";
    protected static final String TYPE_BOOLEAN = "INTEGER(1)";
    protected static final String TYPE_DATETIME = "DATETIME";
    protected static final String TYPE_INT = "INT";
    private static final String TYPE_PRIMARY_KEY = "INTEGER PRIMARY KEY AUTOINCREMENT";
    private static final String TYPE_TEXT = "TEXT";

    public static class FeedColumns implements BaseColumns {
        public static final String[] COLUMNS = {"_id", URL, NAME, LASTUPDATE, ICON, ERROR, "priority", FETCHMODE, REALLASTUPDATE};
        public static final Uri CONTENT_URI = Uri.parse(FeedData.CONTENT + FeedData.AUTHORITY + "/feeds");
        public static final String ERROR = "error";
        public static final String FETCHMODE = "fetchmode";
        public static final String ICON = "icon";
        public static final String LASTUPDATE = "lastupdate";
        public static final String NAME = "name";
        public static final String PRIORITY = "priority";
        public static final String REALLASTUPDATE = "reallastupdate";
        public static final String[] TYPES = {FeedData.TYPE_PRIMARY_KEY, "TEXT UNIQUE", FeedData.TYPE_TEXT, FeedData.TYPE_DATETIME, "BLOB", FeedData.TYPE_TEXT, FeedData.TYPE_INT, FeedData.TYPE_INT, FeedData.TYPE_DATETIME};
        public static final String URL = "url";

        public static final Uri CONTENT_URI(String feedId) {
            return Uri.parse(FeedData.CONTENT + FeedData.AUTHORITY + "/feeds/" + feedId);
        }

        public static final Uri CONTENT_URI(long feedId) {
            return Uri.parse(FeedData.CONTENT + FeedData.AUTHORITY + "/feeds/" + feedId);
        }
    }

    public static class EntryColumns implements BaseColumns {
        public static final String ABSTRACT = "abstract";
        public static final String[] COLUMNS = {"_id", "feedid", TITLE, ABSTRACT, DATE, READDATE, LINK, FAVORITE};
        public static Uri CONTENT_URI = Uri.parse(FeedData.CONTENT + FeedData.AUTHORITY + "/entries");
        public static final String DATE = "date";
        public static final String FAVORITE = "favorite";
        public static Uri FAVORITES_CONTENT_URI = Uri.parse(FeedData.CONTENT + FeedData.AUTHORITY + "/favorites");
        public static final String FEED_ID = "feedid";
        public static final String LINK = "link";
        public static final String READDATE = "readdate";
        public static final String TITLE = "title";
        public static final String[] TYPES = {FeedData.TYPE_PRIMARY_KEY, "INTEGER(7)", FeedData.TYPE_TEXT, FeedData.TYPE_TEXT, FeedData.TYPE_DATETIME, FeedData.TYPE_DATETIME, FeedData.TYPE_TEXT, FeedData.TYPE_BOOLEAN};

        public static Uri CONTENT_URI(String feedId) {
            return Uri.parse(FeedData.CONTENT + FeedData.AUTHORITY + "/feeds/" + feedId + "/entries");
        }

        public static Uri ENTRY_CONTENT_URI(String entryId) {
            return Uri.parse(FeedData.CONTENT + FeedData.AUTHORITY + "/entries/" + entryId);
        }
    }
}
