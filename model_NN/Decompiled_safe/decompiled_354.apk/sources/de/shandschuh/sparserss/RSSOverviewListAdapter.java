package de.shandschuh.sparserss;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;
import de.shandschuh.sparserss.provider.FeedData;
import java.util.Date;

public class RSSOverviewListAdapter extends ResourceCursorAdapter {
    private static final String COMMA = ", ";
    private static final String COUNT = "COUNT(*)";
    private static final String COUNT_UNREAD = "COUNT(*) - COUNT(readdate)";
    private String COLON;
    private int errorPosition = getCursor().getColumnIndex(FeedData.FeedColumns.ERROR);
    private int iconPosition = getCursor().getColumnIndex(FeedData.FeedColumns.ICON);
    private int idPosition = getCursor().getColumnIndex("_id");
    private int lastUpdateColumn = getCursor().getColumnIndex(FeedData.FeedColumns.LASTUPDATE);
    private int linkPosition = getCursor().getColumnIndex(FeedData.FeedColumns.URL);
    private int nameColumnPosition = getCursor().getColumnIndex(FeedData.FeedColumns.NAME);

    public RSSOverviewListAdapter(Activity context) {
        super(context, R.layout.listitem, context.managedQuery(FeedData.FeedColumns.CONTENT_URI, null, null, null, null));
        this.COLON = context.getString(R.string.colon);
    }

    public void bindView(View view, Context context, Cursor cursor) {
        TextView textView = (TextView) view.findViewById(16908308);
        textView.setSingleLine();
        Cursor countCursor = context.getContentResolver().query(FeedData.EntryColumns.CONTENT_URI(cursor.getString(this.idPosition)), new String[]{COUNT_UNREAD, COUNT}, null, null, null);
        countCursor.moveToFirst();
        int unreadCount = countCursor.getInt(0);
        int count = countCursor.getInt(1);
        countCursor.close();
        long date = cursor.getLong(this.lastUpdateColumn);
        TextView updateTextView = (TextView) view.findViewById(16908309);
        if (cursor.isNull(this.errorPosition)) {
            updateTextView.setText(new StringBuilder(context.getString(R.string.update)).append(this.COLON).append(date == 0 ? context.getString(R.string.never) : new StringBuilder(EntriesListAdapter.DATEFORMAT.format(new Date(date))).append(COMMA).append(unreadCount).append('/').append(count).append(' ').append(context.getString(R.string.unread))));
        } else {
            updateTextView.setText(new StringBuilder(context.getString(R.string.error)).append(this.COLON).append(cursor.getString(this.errorPosition)));
        }
        if (unreadCount > 0) {
            textView.setTypeface(Typeface.DEFAULT_BOLD);
            textView.setEnabled(true);
            updateTextView.setEnabled(true);
        } else {
            textView.setTypeface(Typeface.DEFAULT);
            textView.setEnabled(false);
            updateTextView.setEnabled(false);
        }
        byte[] iconBytes = cursor.getBlob(this.iconPosition);
        if (iconBytes == null || iconBytes.length <= 0) {
            view.setTag(null);
            textView.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
            textView.setText(cursor.isNull(this.nameColumnPosition) ? cursor.getString(this.linkPosition) : cursor.getString(this.nameColumnPosition));
            return;
        }
        textView.setText(Strings.SPACE + (cursor.isNull(this.nameColumnPosition) ? cursor.getString(this.linkPosition) : cursor.getString(this.nameColumnPosition)));
        textView.setCompoundDrawablesWithIntrinsicBounds(new BitmapDrawable(BitmapFactory.decodeByteArray(iconBytes, 0, iconBytes.length)), (Drawable) null, (Drawable) null, (Drawable) null);
        view.setTag(iconBytes);
    }
}
