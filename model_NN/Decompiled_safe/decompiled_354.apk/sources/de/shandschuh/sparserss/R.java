package de.shandschuh.sparserss;

public final class R {

    public static final class array {
        public static final int settings_fontsizes = 2131034117;
        public static final int settings_fontsizevalues = 2131034114;
        public static final int settings_intervals = 2131034115;
        public static final int settings_intervalvalues = 2131034112;
        public static final int settings_keeptimes = 2131034116;
        public static final int settings_keeptimevalues = 2131034113;
    }

    public static final class attr {
    }

    public static final class drawable {
        public static final int feed = 2130837504;
        public static final int feed_grey = 2130837505;
        public static final int ic_statusbar_rss = 2130837506;
        public static final int ic_statusbar_rss_23 = 2130837507;
        public static final int icon = 2130837508;
        public static final int widget_frame = 2130837509;
    }

    public static final class id {
        public static final int EditText02 = 2131230728;
        public static final int LinearLayout01 = 2131230751;
        public static final int entry_abstract = 2131230723;
        public static final int entry_date = 2131230721;
        public static final int feed_icon = 2131230750;
        public static final int feed_title = 2131230727;
        public static final int feed_url = 2131230729;
        public static final int linear_wrapper = 2131230722;
        public static final int main_layout = 2131230720;
        public static final int news_1 = 2131230731;
        public static final int news_10 = 2131230749;
        public static final int news_2 = 2131230733;
        public static final int news_3 = 2131230735;
        public static final int news_4 = 2131230737;
        public static final int news_5 = 2131230739;
        public static final int news_6 = 2131230741;
        public static final int news_7 = 2131230743;
        public static final int news_8 = 2131230745;
        public static final int news_9 = 2131230747;
        public static final int news_icon_1 = 2131230730;
        public static final int news_icon_10 = 2131230748;
        public static final int news_icon_2 = 2131230732;
        public static final int news_icon_3 = 2131230734;
        public static final int news_icon_4 = 2131230736;
        public static final int news_icon_5 = 2131230738;
        public static final int news_icon_6 = 2131230740;
        public static final int news_icon_7 = 2131230742;
        public static final int news_icon_8 = 2131230744;
        public static final int news_icon_9 = 2131230746;
        public static final int next_button = 2131230726;
        public static final int prev_button = 2131230724;
        public static final int save_button = 2131230752;
        public static final int url_button = 2131230725;
    }

    public static final class layout {
        public static final int entries = 2130903040;
        public static final int entry = 2130903041;
        public static final int feedsettings = 2130903042;
        public static final int homescreenwidget = 2130903043;
        public static final int listitem = 2130903044;
        public static final int main = 2130903045;
        public static final int preferences = 2130903046;
        public static final int tabs = 2130903047;
        public static final int widgetconfig = 2130903048;
        public static final int widgetpreferences = 2130903049;
    }

    public static final class plurals {
        public static final int contextmenu_markasunread = 2131165184;
    }

    public static final class string {
        public static final int all = 2131099721;
        public static final int all_feeds = 2131099720;
        public static final int app_name = 2131099648;
        public static final int button_accept = 2131099734;
        public static final int button_decline = 2131099735;
        public static final int colon = 2131099713;
        public static final int contextmenu_delete = 2131099653;
        public static final int contextmenu_edit = 2131099657;
        public static final int contextmenu_hideread = 2131099662;
        public static final int contextmenu_markasread = 2131099660;
        public static final int contextmenu_markasunread = 2131099661;
        public static final int contextmenu_refresh = 2131099658;
        public static final int contextmenu_showread = 2131099663;
        public static final int dialog_licenseagreement = 2131099737;
        public static final int editfeed_title = 2131099656;
        public static final int error = 2131099717;
        public static final int error_externalstoragenotavailable = 2131099727;
        public static final int error_feedexport = 2131099725;
        public static final int error_feedimport = 2131099724;
        public static final int error_feedurlexists = 2131099723;
        public static final int error_invalidimportfile = 2131099726;
        public static final int favorites = 2131099655;
        public static final int feed_title = 2131099731;
        public static final int feed_url = 2131099728;
        public static final int goto_url = 2131099654;
        public static final int license = 2131099670;
        public static final int license_intro = 2131099671;
        public static final int menu_about = 2131099667;
        public static final int menu_addfeed = 2131099649;
        public static final int menu_allread = 2131099664;
        public static final int menu_disablefeedsort = 2131099669;
        public static final int menu_enablefeedsort = 2131099668;
        public static final int menu_export = 2131099666;
        public static final int menu_import = 2131099665;
        public static final int menu_refresh = 2131099652;
        public static final int menu_settings = 2131099659;
        public static final int message_exportedto = 2131099736;
        public static final int never = 2131099711;
        public static final int newentries = 2131099715;
        public static final int noentries = 2131099651;
        public static final int norssfeeds = 2131099650;
        public static final int optional = 2131099729;
        public static final int overview = 2131099722;
        public static final int priority = 2131099718;
        public static final int question_deletefeed = 2131099733;
        public static final int rss_feeds = 2131099716;
        public static final int select_feeds = 2131099719;
        public static final int select_file = 2131099732;
        public static final int settings_blacktextwhite = 2131099692;
        public static final int settings_blacktextwhite_description = 2131099693;
        public static final int settings_category_contentpresentation = 2131099683;
        public static final int settings_category_network = 2131099704;
        public static final int settings_category_notofications = 2131099677;
        public static final int settings_category_refresh = 2131099672;
        public static final int settings_disablepictures = 2131099688;
        public static final int settings_disablepictures_description = 2131099689;
        public static final int settings_enabled = 2131099673;
        public static final int settings_entrysettings = 2131099700;
        public static final int settings_fetchpictures = 2131099690;
        public static final int settings_fetchpictures_description = 2131099691;
        public static final int settings_fontsize = 2131099694;
        public static final int settings_fontsize_description = 2131099695;
        public static final int settings_hideread = 2131099698;
        public static final int settings_hideread_description = 2131099699;
        public static final int settings_keeptime = 2131099686;
        public static final int settings_keeptime_description = 2131099687;
        public static final int settings_notificationsenabled_description = 2131099678;
        public static final int settings_notificationsringtone = 2131099679;
        public static final int settings_notificationsringtone_description = 2131099680;
        public static final int settings_notificationsvibrate = 2131099681;
        public static final int settings_notificationsvibrate_description = 2131099682;
        public static final int settings_prioritize = 2131099684;
        public static final int settings_prioritize_description = 2131099685;
        public static final int settings_proxy_host = 2131099709;
        public static final int settings_proxy_port = 2131099710;
        public static final int settings_proxy_wifionly = 2131099707;
        public static final int settings_proxy_wifionly_description = 2131099708;
        public static final int settings_refreshenabled_description = 2131099674;
        public static final int settings_refreshinterval = 2131099675;
        public static final int settings_refreshinterval_description = 2131099676;
        public static final int settings_refreshonopen = 2131099696;
        public static final int settings_refreshonopen_description = 2131099697;
        public static final int settings_showtabs = 2131099702;
        public static final int settings_showtabs_description = 2131099703;
        public static final int settings_standarduseragent = 2131099705;
        public static final int settings_standarduseragent_description = 2131099706;
        public static final int settings_visiblefeeds = 2131099701;
        public static final int unread = 2131099714;
        public static final int update = 2131099712;
        public static final int websiteorfeed = 2131099730;
    }

    public static final class xml {
        public static final int widgeticon = 2130968576;
        public static final int widgetinfo = 2130968577;
        public static final int widgettext = 2130968578;
    }
}
