package de.shandschuh.sparserss;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import de.shandschuh.sparserss.service.FetcherService;

public class RefreshBroadcastReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        context.startService(new Intent(context, FetcherService.class).putExtras(intent));
    }
}
