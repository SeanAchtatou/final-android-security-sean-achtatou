package de.shandschuh.sparserss.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.preference.PreferenceManager;
import de.shandschuh.sparserss.MainTabActivity;
import de.shandschuh.sparserss.R;
import de.shandschuh.sparserss.Strings;
import de.shandschuh.sparserss.provider.FeedData;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

public class FetcherService extends Service {
    private static final String CHARSET = "charset=";
    private static final String CONTENT_TYPE_TEXT_HTML = "text/html";
    private static final String COUNT = "COUNT(*)";
    private static final String ENCODING = "encoding=\"";
    private static final int FETCHMODE_DIRECT = 1;
    private static final int FETCHMODE_REENCODE = 2;
    private static final String HREF = "href=\"";
    private static final String HTML_BODY = "<body";
    private static final String KEY_USERAGENT = "User-agent";
    private static final String LINK_RSS = "<link rel=\"alternate\" ";
    private static final String LINK_RSS_SLOPPY = "<link rel=alternate ";
    /* access modifiers changed from: private */
    public static final boolean PRIOR_GINGERBREAD = ((Build.VERSION.RELEASE.startsWith(Strings.ONE) || Build.VERSION.RELEASE.startsWith("2.0") || Build.VERSION.RELEASE.startsWith("2.1") || Build.VERSION.RELEASE.startsWith("2.2")) ? FETCHMODE_DIRECT : PRIOR_GINGERBREAD);
    private static final String SLASH = "/";
    private static final String VALUE_USERAGENT = "Mozilla/5.0";
    /* access modifiers changed from: private */
    public static SharedPreferences preferences = null;
    private static Proxy proxy;
    /* access modifiers changed from: private */
    public NotificationManager notificationManager;
    boolean running = PRIOR_GINGERBREAD;

    static {
        HttpURLConnection.setFollowRedirects(true);
    }

    public void onStart(Intent intent, int startId) {
        handleIntent(intent);
    }

    public void onLowMemory() {
        stopSelf();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        handleIntent(intent);
        return FETCHMODE_DIRECT;
    }

    private void handleIntent(final Intent intent) {
        if (!this.running) {
            if (preferences == null) {
                try {
                    preferences = PreferenceManager.getDefaultSharedPreferences(createPackageContext(Strings.PACKAGE, 0));
                } catch (PackageManager.NameNotFoundException e) {
                    preferences = PreferenceManager.getDefaultSharedPreferences(this);
                }
            }
            this.running = true;
            NetworkInfo networkInfo = ((ConnectivityManager) getSystemService("connectivity")).getActiveNetworkInfo();
            if (networkInfo == null || networkInfo.getState() != NetworkInfo.State.CONNECTED || intent == null) {
                this.running = PRIOR_GINGERBREAD;
                stopSelf();
                return;
            }
            if (!preferences.getBoolean(Strings.SETTINGS_PROXYENABLED, PRIOR_GINGERBREAD) || (networkInfo.getType() != FETCHMODE_DIRECT && preferences.getBoolean(Strings.SETTINGS_PROXYWIFIONLY, PRIOR_GINGERBREAD))) {
                proxy = null;
            } else {
                try {
                    proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(preferences.getString(Strings.SETTINGS_PROXYHOST, Strings.EMPTY), Integer.parseInt(preferences.getString(Strings.SETTINGS_PROXYPORT, Strings.DEFAULTPROXYPORT))));
                } catch (Exception e2) {
                    proxy = null;
                }
            }
            new Thread() {
                public void run() {
                    if (FetcherService.refreshFeedsStatic(FetcherService.this, intent.getStringExtra("feedid")) > 0) {
                        if (FetcherService.preferences.getBoolean(Strings.SETTINGS_NOTIFICATIONSENABLED, FetcherService.PRIOR_GINGERBREAD)) {
                            ContentResolver contentResolver = FetcherService.this.getContentResolver();
                            Uri uri = FeedData.EntryColumns.CONTENT_URI;
                            String[] strArr = new String[FetcherService.FETCHMODE_DIRECT];
                            strArr[0] = FetcherService.COUNT;
                            Cursor cursor = contentResolver.query(uri, strArr, FeedData.EntryColumns.READDATE + Strings.DB_ISNULL, null, null);
                            cursor.moveToFirst();
                            int newCount = cursor.getInt(0);
                            cursor.close();
                            String text = newCount + ' ' + FetcherService.this.getString(R.string.newentries);
                            Notification notification = new Notification(FetcherService.PRIOR_GINGERBREAD ? R.drawable.ic_statusbar_rss : R.drawable.ic_statusbar_rss_23, text, System.currentTimeMillis());
                            PendingIntent contentIntent = PendingIntent.getActivity(FetcherService.this, 0, new Intent(FetcherService.this, MainTabActivity.class), 134217728);
                            if (FetcherService.preferences.getBoolean(Strings.SETTINGS_NOTIFICATIONSVIBRATE, FetcherService.PRIOR_GINGERBREAD)) {
                                notification.defaults = FetcherService.FETCHMODE_REENCODE;
                            }
                            notification.flags = 17;
                            notification.ledARGB = -1;
                            notification.ledOnMS = 300;
                            notification.ledOffMS = 1000;
                            String ringtone = FetcherService.preferences.getString(Strings.SETTINGS_NOTIFICATIONSRINGTONE, null);
                            if (ringtone != null && ringtone.length() > 0) {
                                notification.sound = Uri.parse(ringtone);
                            }
                            notification.setLatestEventInfo(FetcherService.this, FetcherService.this.getString(R.string.rss_feeds), text, contentIntent);
                            FetcherService.this.notificationManager.notify(0, notification);
                        } else {
                            FetcherService.this.notificationManager.cancel(0);
                        }
                    }
                    FetcherService.this.running = FetcherService.PRIOR_GINGERBREAD;
                    FetcherService.this.stopSelf();
                }
            }.start();
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        this.running = PRIOR_GINGERBREAD;
        this.notificationManager = (NotificationManager) getSystemService("notification");
    }

    /* JADX INFO: Multiple debug info for r27v26 byte[]: [D('connection' java.net.HttpURLConnection), D('iconBytes' byte[])] */
    /* JADX INFO: Multiple debug info for r7v8 java.io.InputStream: [D('inputStream' java.io.InputStream), D('fetchMode' int)] */
    /* JADX INFO: Multiple debug info for r27v28 byte[]: [D('byteBuffer' byte[]), D('iconBytes' byte[])] */
    /* JADX INFO: Multiple debug info for r7v9 java.lang.String: [D('inputStream' java.io.InputStream), D('xmlText' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r7v15 int: [D('fetchMode' int), D('index2' int)] */
    /* JADX INFO: Multiple debug info for r27v59 android.content.ContentValues: [D('e' java.lang.Exception), D('values' android.content.ContentValues)] */
    /* JADX INFO: Multiple debug info for r27v67 int: [D('length' int), D('bufferedReader' java.io.BufferedReader)] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x0431  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0110  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x0324 A[SYNTHETIC, Splitter:B:92:0x0324] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int refreshFeedsStatic(android.content.Context r26, java.lang.String r27) {
        /*
            android.content.ContentResolver r5 = r26.getContentResolver()
            if (r27 != 0) goto L_0x008c
            android.net.Uri r27 = de.shandschuh.sparserss.provider.FeedData.FeedColumns.CONTENT_URI
            r6 = r27
        L_0x000a:
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            android.database.Cursor r8 = r5.query(r6, r7, r8, r9, r10)
            java.lang.String r27 = "url"
            r0 = r8
            r1 = r27
            int r20 = r0.getColumnIndex(r1)
            java.lang.String r27 = "_id"
            r0 = r8
            r1 = r27
            int r15 = r0.getColumnIndex(r1)
            java.lang.String r27 = "reallastupdate"
            r0 = r8
            r1 = r27
            int r17 = r0.getColumnIndex(r1)
            java.lang.String r27 = "name"
            r0 = r8
            r1 = r27
            int r19 = r0.getColumnIndex(r1)
            java.lang.String r27 = "fetchmode"
            r0 = r8
            r1 = r27
            int r9 = r0.getColumnIndex(r1)
            java.lang.String r27 = "icon"
            r0 = r8
            r1 = r27
            int r12 = r0.getColumnIndex(r1)
            android.content.SharedPreferences r27 = de.shandschuh.sparserss.service.FetcherService.preferences
            java.lang.String r5 = "standarduseragent"
            r6 = 0
            r0 = r27
            r1 = r5
            r2 = r6
            boolean r27 = r0.getBoolean(r1, r2)
            if (r27 == 0) goto L_0x0094
            r27 = 0
            r16 = r27
        L_0x005b:
            r27 = 0
            de.shandschuh.sparserss.handler.RSSHandler r10 = new de.shandschuh.sparserss.handler.RSSHandler
            r0 = r10
            r1 = r26
            r0.<init>(r1)
            android.content.SharedPreferences r5 = de.shandschuh.sparserss.service.FetcherService.preferences
            java.lang.String r6 = "pictures.fetch"
            r7 = 0
            boolean r5 = r5.getBoolean(r6, r7)
            r10.setFetchImages(r5)
            r18 = r27
        L_0x0073:
            boolean r27 = r8.moveToNext()
            if (r27 != 0) goto L_0x0099
            r8.close()
            if (r18 <= 0) goto L_0x008b
            android.content.Intent r27 = new android.content.Intent
            java.lang.String r5 = "de.shandschuh.sparserss.widget.UPDATE"
            r0 = r27
            r1 = r5
            r0.<init>(r1)
            r26.sendBroadcast(r27)
        L_0x008b:
            return r18
        L_0x008c:
            android.net.Uri r27 = de.shandschuh.sparserss.provider.FeedData.FeedColumns.CONTENT_URI(r27)
            r6 = r27
            goto L_0x000a
        L_0x0094:
            r27 = 1
            r16 = r27
            goto L_0x005b
        L_0x0099:
            java.lang.String r14 = r8.getString(r15)
            r27 = 0
            r0 = r8
            r1 = r20
            java.lang.String r5 = r0.getString(r1)     // Catch:{ Throwable -> 0x0528 }
            r0 = r5
            r1 = r16
            java.net.HttpURLConnection r27 = setupConnection(r0, r1)     // Catch:{ Throwable -> 0x0528 }
            java.lang.String r5 = r27.getContentType()     // Catch:{ Throwable -> 0x0528 }
            int r6 = r8.getInt(r9)     // Catch:{ Throwable -> 0x0528 }
            java.util.Date r7 = new java.util.Date     // Catch:{ Throwable -> 0x0528 }
            r0 = r8
            r1 = r17
            long r21 = r0.getLong(r1)     // Catch:{ Throwable -> 0x0528 }
            r0 = r7
            r1 = r21
            r0.<init>(r1)     // Catch:{ Throwable -> 0x0528 }
            r0 = r8
            r1 = r19
            java.lang.String r11 = r0.getString(r1)     // Catch:{ Throwable -> 0x0528 }
            r10.init(r7, r14, r11)     // Catch:{ Throwable -> 0x0528 }
            if (r6 != 0) goto L_0x053e
            if (r5 == 0) goto L_0x0539
            java.lang.String r6 = "text/html"
            boolean r6 = r5.startsWith(r6)     // Catch:{ Throwable -> 0x0528 }
            if (r6 == 0) goto L_0x0539
            java.io.BufferedReader r13 = new java.io.BufferedReader     // Catch:{ Throwable -> 0x0528 }
            java.io.InputStreamReader r6 = new java.io.InputStreamReader     // Catch:{ Throwable -> 0x0528 }
            java.io.InputStream r7 = r27.getInputStream()     // Catch:{ Throwable -> 0x0528 }
            r6.<init>(r7)     // Catch:{ Throwable -> 0x0528 }
            r13.<init>(r6)     // Catch:{ Throwable -> 0x0528 }
            r6 = 0
            r7 = -1
            r11 = -1
        L_0x00eb:
            java.lang.String r6 = r13.readLine()     // Catch:{ Throwable -> 0x0528 }
            if (r6 != 0) goto L_0x022b
            r6 = r7
            r7 = r11
        L_0x00f3:
            r6 = -1
            if (r7 != r6) goto L_0x0539
            r27.disconnect()     // Catch:{ Throwable -> 0x0528 }
            r0 = r8
            r1 = r20
            java.lang.String r5 = r0.getString(r1)     // Catch:{ Throwable -> 0x0528 }
            r0 = r5
            r1 = r16
            java.net.HttpURLConnection r27 = setupConnection(r0, r1)     // Catch:{ Throwable -> 0x0528 }
            java.lang.String r5 = r27.getContentType()     // Catch:{ Throwable -> 0x0528 }
            r7 = r5
            r6 = r27
        L_0x010e:
            if (r7 == 0) goto L_0x0324
            java.lang.String r27 = "charset="
            r0 = r7
            r1 = r27
            int r27 = r0.indexOf(r1)     // Catch:{ Throwable -> 0x052b, all -> 0x0521 }
            r5 = -1
            r0 = r27
            r1 = r5
            if (r0 <= r1) goto L_0x031c
            r5 = 59
            r0 = r7
            r1 = r5
            r2 = r27
            int r5 = r0.indexOf(r1, r2)     // Catch:{ Throwable -> 0x052b, all -> 0x0521 }
            r11 = -1
            if (r5 <= r11) goto L_0x0308
            int r27 = r27 + 8
            r0 = r7
            r1 = r27
            r2 = r5
            java.lang.String r27 = r0.substring(r1, r2)     // Catch:{ UnsupportedEncodingException -> 0x0313 }
        L_0x0136:
            android.util.Xml.findEncodingByName(r27)     // Catch:{ UnsupportedEncodingException -> 0x0313 }
            r27 = 1
            r5 = r27
            r27 = r6
        L_0x013f:
            android.content.ContentValues r6 = new android.content.ContentValues     // Catch:{ Throwable -> 0x0528 }
            r6.<init>()     // Catch:{ Throwable -> 0x0528 }
            java.lang.String r11 = "fetchmode"
            java.lang.Integer r13 = java.lang.Integer.valueOf(r5)     // Catch:{ Throwable -> 0x0528 }
            r6.put(r11, r13)     // Catch:{ Throwable -> 0x0528 }
            android.content.ContentResolver r11 = r26.getContentResolver()     // Catch:{ Throwable -> 0x0528 }
            android.net.Uri r13 = de.shandschuh.sparserss.provider.FeedData.FeedColumns.CONTENT_URI(r14)     // Catch:{ Throwable -> 0x0528 }
            r21 = 0
            r22 = 0
            r0 = r11
            r1 = r13
            r2 = r6
            r3 = r21
            r4 = r22
            r0.update(r1, r2, r3, r4)     // Catch:{ Throwable -> 0x0528 }
            r6 = r7
            r7 = r5
            r5 = r27
        L_0x0167:
            byte[] r27 = r8.getBlob(r12)     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            if (r27 != 0) goto L_0x01e0
            java.net.URL r11 = new java.net.URL     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            java.net.URL r21 = r5.getURL()     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            java.lang.String r21 = r21.getProtocol()     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            r0 = r13
            r1 = r21
            r0.<init>(r1)     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            java.lang.String r21 = "://"
            r0 = r13
            r1 = r21
            java.lang.StringBuilder r13 = r0.append(r1)     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            java.net.URL r21 = r5.getURL()     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            java.lang.String r21 = r21.getHost()     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            r0 = r13
            r1 = r21
            java.lang.StringBuilder r13 = r0.append(r1)     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            java.lang.String r21 = "/favicon.ico"
            r0 = r13
            r1 = r21
            java.lang.StringBuilder r13 = r0.append(r1)     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            java.lang.String r13 = r13.toString()     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            r11.<init>(r13)     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            r0 = r11
            r1 = r16
            java.net.HttpURLConnection r13 = setupConnection(r0, r1)     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            java.io.InputStream r11 = r13.getInputStream()     // Catch:{ Exception -> 0x0382, all -> 0x03c1 }
            byte[] r27 = getBytes(r11)     // Catch:{ Exception -> 0x0382, all -> 0x03c1 }
            android.content.ContentValues r11 = new android.content.ContentValues     // Catch:{ Exception -> 0x0382, all -> 0x03c1 }
            r11.<init>()     // Catch:{ Exception -> 0x0382, all -> 0x03c1 }
            java.lang.String r21 = "icon"
            r0 = r11
            r1 = r21
            r2 = r27
            r0.put(r1, r2)     // Catch:{ Exception -> 0x0382, all -> 0x03c1 }
            android.content.ContentResolver r21 = r26.getContentResolver()     // Catch:{ Exception -> 0x0382, all -> 0x03c1 }
            android.net.Uri r22 = de.shandschuh.sparserss.provider.FeedData.FeedColumns.CONTENT_URI(r14)     // Catch:{ Exception -> 0x0382, all -> 0x03c1 }
            r23 = 0
            r24 = 0
            r0 = r21
            r1 = r22
            r2 = r11
            r3 = r23
            r4 = r24
            r0.update(r1, r2, r3, r4)     // Catch:{ Exception -> 0x0382, all -> 0x03c1 }
            r13.disconnect()     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
        L_0x01e0:
            switch(r7) {
                case 2: goto L_0x0435;
                default: goto L_0x01e3;
            }     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
        L_0x01e3:
            if (r6 == 0) goto L_0x0410
            java.lang.String r27 = "charset="
            r0 = r6
            r1 = r27
            int r27 = r0.indexOf(r1)     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            r7 = 59
            r0 = r6
            r1 = r7
            r2 = r27
            int r7 = r0.indexOf(r1, r2)     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            java.io.InputStream r11 = r5.getInputStream()     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            r10.setInputStream(r11)     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            r13 = -1
            if (r7 <= r13) goto L_0x0405
            int r27 = r27 + 8
            r0 = r6
            r1 = r27
            r2 = r7
            java.lang.String r27 = r0.substring(r1, r2)     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
        L_0x020c:
            android.util.Xml$Encoding r27 = android.util.Xml.findEncodingByName(r27)     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            r0 = r11
            r1 = r27
            r2 = r10
            android.util.Xml.parse(r0, r1, r2)     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
        L_0x0217:
            r5.disconnect()     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            if (r5 == 0) goto L_0x021f
            r5.disconnect()
        L_0x021f:
            r27 = r5
        L_0x0221:
            int r27 = r10.getNewCount()
            int r27 = r27 + r18
            r18 = r27
            goto L_0x0073
        L_0x022b:
            java.lang.String r21 = "<body"
            r0 = r6
            r1 = r21
            int r21 = r0.indexOf(r1)     // Catch:{ Throwable -> 0x0528 }
            r22 = -1
            r0 = r21
            r1 = r22
            if (r0 <= r1) goto L_0x0240
            r6 = r7
            r7 = r11
            goto L_0x00f3
        L_0x0240:
            java.lang.String r7 = "<link rel=\"alternate\" "
            int r7 = r6.indexOf(r7)     // Catch:{ Throwable -> 0x0528 }
            r21 = -1
            r0 = r7
            r1 = r21
            if (r0 != r1) goto L_0x0253
            java.lang.String r7 = "<link rel=alternate "
            int r7 = r6.indexOf(r7)     // Catch:{ Throwable -> 0x0528 }
        L_0x0253:
            r21 = -1
            r0 = r7
            r1 = r21
            if (r0 <= r1) goto L_0x00eb
            java.lang.String r11 = "href=\""
            int r11 = r6.indexOf(r11, r7)     // Catch:{ Throwable -> 0x0528 }
            r21 = -1
            r0 = r11
            r1 = r21
            if (r0 <= r1) goto L_0x00eb
            int r5 = r11 + 6
            r13 = 34
            int r21 = r11 + 10
            r0 = r6
            r1 = r13
            r2 = r21
            int r13 = r0.indexOf(r1, r2)     // Catch:{ Throwable -> 0x0528 }
            java.lang.String r5 = r6.substring(r5, r13)     // Catch:{ Throwable -> 0x0528 }
            java.lang.String r6 = "&amp;"
            java.lang.String r13 = "&"
            java.lang.String r5 = r5.replace(r6, r13)     // Catch:{ Throwable -> 0x0528 }
            android.content.ContentValues r6 = new android.content.ContentValues     // Catch:{ Throwable -> 0x0528 }
            r6.<init>()     // Catch:{ Throwable -> 0x0528 }
            java.lang.String r13 = "/"
            boolean r13 = r5.startsWith(r13)     // Catch:{ Throwable -> 0x0528 }
            if (r13 == 0) goto L_0x02d7
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0528 }
            r0 = r8
            r1 = r20
            java.lang.String r21 = r0.getString(r1)     // Catch:{ Throwable -> 0x0528 }
            java.lang.String r21 = java.lang.String.valueOf(r21)     // Catch:{ Throwable -> 0x0528 }
            r0 = r13
            r1 = r21
            r0.<init>(r1)     // Catch:{ Throwable -> 0x0528 }
            java.lang.StringBuilder r5 = r13.append(r5)     // Catch:{ Throwable -> 0x0528 }
            java.lang.String r5 = r5.toString()     // Catch:{ Throwable -> 0x0528 }
        L_0x02a9:
            java.lang.String r13 = "url"
            r6.put(r13, r5)     // Catch:{ Throwable -> 0x0528 }
            android.content.ContentResolver r13 = r26.getContentResolver()     // Catch:{ Throwable -> 0x0528 }
            android.net.Uri r21 = de.shandschuh.sparserss.provider.FeedData.FeedColumns.CONTENT_URI(r14)     // Catch:{ Throwable -> 0x0528 }
            r22 = 0
            r23 = 0
            r0 = r13
            r1 = r21
            r2 = r6
            r3 = r22
            r4 = r23
            r0.update(r1, r2, r3, r4)     // Catch:{ Throwable -> 0x0528 }
            r27.disconnect()     // Catch:{ Throwable -> 0x0528 }
            r0 = r5
            r1 = r16
            java.net.HttpURLConnection r27 = setupConnection(r0, r1)     // Catch:{ Throwable -> 0x0528 }
            java.lang.String r5 = r27.getContentType()     // Catch:{ Throwable -> 0x0528 }
            r6 = r7
            r7 = r11
            goto L_0x00f3
        L_0x02d7:
            java.lang.String r13 = "http://"
            boolean r13 = r5.startsWith(r13)     // Catch:{ Throwable -> 0x0528 }
            if (r13 != 0) goto L_0x02a9
            java.lang.String r13 = "https://"
            boolean r13 = r5.startsWith(r13)     // Catch:{ Throwable -> 0x0528 }
            if (r13 != 0) goto L_0x02a9
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0528 }
            r0 = r8
            r1 = r20
            java.lang.String r21 = r0.getString(r1)     // Catch:{ Throwable -> 0x0528 }
            r0 = r13
            r1 = r21
            r0.<init>(r1)     // Catch:{ Throwable -> 0x0528 }
            r21 = 47
            r0 = r13
            r1 = r21
            java.lang.StringBuilder r13 = r0.append(r1)     // Catch:{ Throwable -> 0x0528 }
            java.lang.StringBuilder r5 = r13.append(r5)     // Catch:{ Throwable -> 0x0528 }
            java.lang.String r5 = r5.toString()     // Catch:{ Throwable -> 0x0528 }
            goto L_0x02a9
        L_0x0308:
            int r27 = r27 + 8
            r0 = r7
            r1 = r27
            java.lang.String r27 = r0.substring(r1)     // Catch:{ UnsupportedEncodingException -> 0x0313 }
            goto L_0x0136
        L_0x0313:
            r27 = move-exception
            r27 = 2
            r5 = r27
            r27 = r6
            goto L_0x013f
        L_0x031c:
            r27 = 2
            r5 = r27
            r27 = r6
            goto L_0x013f
        L_0x0324:
            java.io.BufferedReader r27 = new java.io.BufferedReader     // Catch:{ Throwable -> 0x052b, all -> 0x0521 }
            java.io.InputStreamReader r5 = new java.io.InputStreamReader     // Catch:{ Throwable -> 0x052b, all -> 0x0521 }
            java.io.InputStream r11 = r6.getInputStream()     // Catch:{ Throwable -> 0x052b, all -> 0x0521 }
            r5.<init>(r11)     // Catch:{ Throwable -> 0x052b, all -> 0x0521 }
            r0 = r27
            r1 = r5
            r0.<init>(r1)     // Catch:{ Throwable -> 0x052b, all -> 0x0521 }
            r5 = 20
            char[] r5 = new char[r5]     // Catch:{ Throwable -> 0x052b, all -> 0x0521 }
            r0 = r27
            r1 = r5
            int r27 = r0.read(r1)     // Catch:{ Throwable -> 0x052b, all -> 0x0521 }
            java.lang.String r11 = new java.lang.String     // Catch:{ Throwable -> 0x052b, all -> 0x0521 }
            r13 = 0
            r0 = r11
            r1 = r5
            r2 = r13
            r3 = r27
            r0.<init>(r1, r2, r3)     // Catch:{ Throwable -> 0x052b, all -> 0x0521 }
            r6.disconnect()     // Catch:{ Throwable -> 0x052b, all -> 0x0521 }
            java.net.URL r27 = r6.getURL()     // Catch:{ Throwable -> 0x052b, all -> 0x0521 }
            r0 = r27
            r1 = r16
            java.net.HttpURLConnection r27 = setupConnection(r0, r1)     // Catch:{ Throwable -> 0x052b, all -> 0x0521 }
            if (r11 == 0) goto L_0x0379
            java.lang.String r5 = "encoding=\""
            int r5 = r11.indexOf(r5)     // Catch:{ Throwable -> 0x0528 }
        L_0x0362:
            r6 = -1
            if (r5 <= r6) goto L_0x037f
            int r6 = r5 + 10
            r13 = 34
            int r5 = r5 + 11
            int r5 = r11.indexOf(r13, r5)     // Catch:{ UnsupportedEncodingException -> 0x037b }
            java.lang.String r5 = r11.substring(r6, r5)     // Catch:{ UnsupportedEncodingException -> 0x037b }
            android.util.Xml.findEncodingByName(r5)     // Catch:{ UnsupportedEncodingException -> 0x037b }
            r5 = 1
            goto L_0x013f
        L_0x0379:
            r5 = -1
            goto L_0x0362
        L_0x037b:
            r5 = move-exception
            r5 = 2
            goto L_0x013f
        L_0x037f:
            r5 = 1
            goto L_0x013f
        L_0x0382:
            r11 = move-exception
            r25 = r11
            r11 = r27
            r27 = r25
            android.content.ContentValues r27 = new android.content.ContentValues     // Catch:{ all -> 0x0532 }
            r27.<init>()     // Catch:{ all -> 0x0532 }
            java.lang.String r21 = "icon"
            r22 = 0
            r0 = r22
            byte[] r0 = new byte[r0]     // Catch:{ all -> 0x0532 }
            r22 = r0
            r0 = r27
            r1 = r21
            r2 = r22
            r0.put(r1, r2)     // Catch:{ all -> 0x0532 }
            android.content.ContentResolver r21 = r26.getContentResolver()     // Catch:{ all -> 0x0532 }
            android.net.Uri r22 = de.shandschuh.sparserss.provider.FeedData.FeedColumns.CONTENT_URI(r14)     // Catch:{ all -> 0x0532 }
            r23 = 0
            r24 = 0
            r0 = r21
            r1 = r22
            r2 = r27
            r3 = r23
            r4 = r24
            r0.update(r1, r2, r3, r4)     // Catch:{ all -> 0x0532 }
            r13.disconnect()     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            r27 = r11
            goto L_0x01e0
        L_0x03c1:
            r6 = move-exception
        L_0x03c2:
            r13.disconnect()     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            throw r6     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
        L_0x03c6:
            r27 = move-exception
            r25 = r27
            r27 = r5
            r5 = r25
        L_0x03cd:
            boolean r6 = r10.isDone()     // Catch:{ all -> 0x0518 }
            if (r6 != 0) goto L_0x03fe
            boolean r6 = r10.isCancelled()     // Catch:{ all -> 0x0518 }
            if (r6 != 0) goto L_0x03fe
            android.content.ContentValues r6 = new android.content.ContentValues     // Catch:{ all -> 0x0518 }
            r6.<init>()     // Catch:{ all -> 0x0518 }
            java.lang.String r7 = "fetchmode"
            r11 = 0
            java.lang.Integer r11 = java.lang.Integer.valueOf(r11)     // Catch:{ all -> 0x0518 }
            r6.put(r7, r11)     // Catch:{ all -> 0x0518 }
            java.lang.String r7 = "error"
            java.lang.String r5 = r5.getMessage()     // Catch:{ all -> 0x0518 }
            r6.put(r7, r5)     // Catch:{ all -> 0x0518 }
            android.content.ContentResolver r5 = r26.getContentResolver()     // Catch:{ all -> 0x0518 }
            android.net.Uri r7 = de.shandschuh.sparserss.provider.FeedData.FeedColumns.CONTENT_URI(r14)     // Catch:{ all -> 0x0518 }
            r11 = 0
            r13 = 0
            r5.update(r7, r6, r11, r13)     // Catch:{ all -> 0x0518 }
        L_0x03fe:
            if (r27 == 0) goto L_0x0221
            r27.disconnect()
            goto L_0x0221
        L_0x0405:
            int r27 = r27 + 8
            r0 = r6
            r1 = r27
            java.lang.String r27 = r0.substring(r1)     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            goto L_0x020c
        L_0x0410:
            java.io.InputStreamReader r27 = new java.io.InputStreamReader     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            java.io.InputStream r6 = r5.getInputStream()     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            r0 = r27
            r1 = r6
            r0.<init>(r1)     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            r0 = r10
            r1 = r27
            r0.setReader(r1)     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            r0 = r27
            r1 = r10
            android.util.Xml.parse(r0, r1)     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            goto L_0x0217
        L_0x042a:
            r26 = move-exception
            r27 = r26
            r26 = r5
        L_0x042f:
            if (r26 == 0) goto L_0x0434
            r26.disconnect()
        L_0x0434:
            throw r27
        L_0x0435:
            java.io.ByteArrayOutputStream r13 = new java.io.ByteArrayOutputStream     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            r13.<init>()     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            java.io.InputStream r7 = r5.getInputStream()     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            r27 = 4096(0x1000, float:5.74E-42)
            r0 = r27
            byte[] r0 = new byte[r0]     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            r27 = r0
        L_0x0446:
            r0 = r7
            r1 = r27
            int r11 = r0.read(r1)     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            if (r11 > 0) goto L_0x0493
            java.lang.String r7 = r13.toString()     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            if (r7 == 0) goto L_0x049f
            java.lang.String r27 = "encoding=\""
            r0 = r7
            r1 = r27
            int r27 = r0.indexOf(r1)     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
        L_0x045e:
            r11 = -1
            r0 = r27
            r1 = r11
            if (r0 <= r1) goto L_0x04a2
            java.io.StringReader r6 = new java.io.StringReader     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            java.lang.String r11 = new java.lang.String     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            byte[] r13 = r13.toByteArray()     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            int r21 = r27 + 10
            r22 = 34
            int r27 = r27 + 11
            r0 = r7
            r1 = r22
            r2 = r27
            int r27 = r0.indexOf(r1, r2)     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            r0 = r7
            r1 = r21
            r2 = r27
            java.lang.String r27 = r0.substring(r1, r2)     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            r0 = r11
            r1 = r13
            r2 = r27
            r0.<init>(r1, r2)     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            r6.<init>(r11)     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            android.util.Xml.parse(r6, r10)     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            goto L_0x0217
        L_0x0493:
            r21 = 0
            r0 = r13
            r1 = r27
            r2 = r21
            r3 = r11
            r0.write(r1, r2, r3)     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            goto L_0x0446
        L_0x049f:
            r27 = -1
            goto L_0x045e
        L_0x04a2:
            if (r6 == 0) goto L_0x0217
            java.lang.String r27 = "charset="
            r0 = r6
            r1 = r27
            int r27 = r0.indexOf(r1)     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            r7 = -1
            r0 = r27
            r1 = r7
            if (r0 <= r1) goto L_0x04f9
            r7 = 59
            r0 = r6
            r1 = r7
            r2 = r27
            int r7 = r0.indexOf(r1, r2)     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            java.io.StringReader r11 = new java.io.StringReader     // Catch:{ Exception -> 0x04ec }
            java.lang.String r21 = new java.lang.String     // Catch:{ Exception -> 0x04ec }
            byte[] r13 = r13.toByteArray()     // Catch:{ Exception -> 0x04ec }
            r22 = -1
            r0 = r7
            r1 = r22
            if (r0 <= r1) goto L_0x04ef
            int r27 = r27 + 8
            r0 = r6
            r1 = r27
            r2 = r7
            java.lang.String r27 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x04ec }
        L_0x04d6:
            r0 = r21
            r1 = r13
            r2 = r27
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x04ec }
            r0 = r11
            r1 = r21
            r0.<init>(r1)     // Catch:{ Exception -> 0x04ec }
            r10.setReader(r11)     // Catch:{ Exception -> 0x04ec }
            android.util.Xml.parse(r11, r10)     // Catch:{ Exception -> 0x04ec }
            goto L_0x0217
        L_0x04ec:
            r27 = move-exception
            goto L_0x0217
        L_0x04ef:
            int r27 = r27 + 8
            r0 = r6
            r1 = r27
            java.lang.String r27 = r0.substring(r1)     // Catch:{ Exception -> 0x04ec }
            goto L_0x04d6
        L_0x04f9:
            java.io.StringReader r27 = new java.io.StringReader     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            java.lang.String r6 = new java.lang.String     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            byte[] r7 = r13.toByteArray()     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            r6.<init>(r7)     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            r0 = r27
            r1 = r6
            r0.<init>(r1)     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            r0 = r10
            r1 = r27
            r0.setReader(r1)     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            r0 = r27
            r1 = r10
            android.util.Xml.parse(r0, r1)     // Catch:{ Throwable -> 0x03c6, all -> 0x042a }
            goto L_0x0217
        L_0x0518:
            r26 = move-exception
            r25 = r26
            r26 = r27
            r27 = r25
            goto L_0x042f
        L_0x0521:
            r26 = move-exception
            r27 = r26
            r26 = r6
            goto L_0x042f
        L_0x0528:
            r5 = move-exception
            goto L_0x03cd
        L_0x052b:
            r27 = move-exception
            r5 = r27
            r27 = r6
            goto L_0x03cd
        L_0x0532:
            r27 = move-exception
            r6 = r27
            r27 = r11
            goto L_0x03c2
        L_0x0539:
            r7 = r5
            r6 = r27
            goto L_0x010e
        L_0x053e:
            r7 = r6
            r6 = r5
            r5 = r27
            goto L_0x0167
        */
        throw new UnsupportedOperationException("Method not decompiled: de.shandschuh.sparserss.service.FetcherService.refreshFeedsStatic(android.content.Context, java.lang.String):int");
    }

    private static final HttpURLConnection setupConnection(String url, boolean imposeUseragent) throws IOException, NoSuchAlgorithmException, KeyManagementException {
        return setupConnection(new URL(url), imposeUseragent);
    }

    private static final HttpURLConnection setupConnection(URL url, boolean imposeUseragent) throws IOException, NoSuchAlgorithmException, KeyManagementException {
        HttpURLConnection connection = proxy == null ? (HttpURLConnection) url.openConnection() : (HttpURLConnection) url.openConnection(proxy);
        connection.setDoInput(true);
        connection.setDoOutput(PRIOR_GINGERBREAD);
        if (imposeUseragent) {
            connection.setRequestProperty(KEY_USERAGENT, VALUE_USERAGENT);
        }
        connection.setConnectTimeout(30000);
        connection.setReadTimeout(30000);
        connection.setUseCaches(PRIOR_GINGERBREAD);
        connection.setRequestProperty("connection", "close");
        connection.connect();
        return connection;
    }

    public static byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        byte[] buffer = new byte[4096];
        while (true) {
            int n = inputStream.read(buffer);
            if (n <= 0) {
                byte[] result = output.toByteArray();
                output.close();
                inputStream.close();
                return result;
            }
            output.write(buffer, 0, n);
        }
    }
}
