package de.shandschuh.sparserss.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.preference.PreferenceManager;
import de.shandschuh.sparserss.Strings;

public class RefreshService extends Service {
    private static final String SIXTYMINUTES = "3600000";
    private AlarmManager alarmManager;
    private SharedPreferences.OnSharedPreferenceChangeListener listener = new SharedPreferences.OnSharedPreferenceChangeListener() {
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            if (Strings.SETTINGS_REFRESHINTERVAL.equals(key)) {
                RefreshService.this.restartTimer();
            }
        }
    };
    private SharedPreferences preferences = null;
    private Intent refreshBroadcastIntent;
    private PendingIntent timerIntent;

    public IBinder onBind(Intent intent) {
        onRebind(intent);
        return null;
    }

    public void onRebind(Intent intent) {
        super.onRebind(intent);
    }

    public boolean onUnbind(Intent intent) {
        return true;
    }

    public void onCreate() {
        super.onCreate();
        try {
            this.preferences = PreferenceManager.getDefaultSharedPreferences(createPackageContext(Strings.PACKAGE, 0));
        } catch (PackageManager.NameNotFoundException e) {
            this.preferences = PreferenceManager.getDefaultSharedPreferences(this);
        }
        this.refreshBroadcastIntent = new Intent(Strings.ACTION_REFRESHFEEDS);
        this.alarmManager = (AlarmManager) getSystemService("alarm");
        this.preferences.registerOnSharedPreferenceChangeListener(this.listener);
        restartTimer();
    }

    /* access modifiers changed from: private */
    public void restartTimer() {
        if (this.timerIntent == null) {
            this.timerIntent = PendingIntent.getBroadcast(this, 0, this.refreshBroadcastIntent, 0);
        } else {
            this.alarmManager.cancel(this.timerIntent);
        }
        int time = 3600000;
        try {
            time = Math.max(60000, Integer.parseInt(this.preferences.getString(Strings.SETTINGS_REFRESHINTERVAL, SIXTYMINUTES)));
        } catch (Exception e) {
        }
        this.alarmManager.setInexactRepeating(2, 10000, (long) time, this.timerIntent);
    }

    public void onDestroy() {
        if (this.timerIntent != null) {
            this.alarmManager.cancel(this.timerIntent);
        }
        this.preferences.unregisterOnSharedPreferenceChangeListener(this.listener);
        super.onDestroy();
    }
}
