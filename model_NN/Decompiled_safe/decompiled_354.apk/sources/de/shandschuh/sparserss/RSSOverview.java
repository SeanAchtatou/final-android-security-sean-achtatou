package de.shandschuh.sparserss;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.NotificationManager;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import de.shandschuh.sparserss.provider.FeedData;
import de.shandschuh.sparserss.provider.OPML;
import de.shandschuh.sparserss.service.RefreshService;
import java.io.File;
import java.io.FilenameFilter;

public class RSSOverview extends ListActivity {
    private static final int ACTIVITY_APPLICATIONPREFERENCES_ID = 1;
    private static final int CONTEXTMENU_DELETE_ID = 5;
    private static final int CONTEXTMENU_EDIT_ID = 3;
    private static final int CONTEXTMENU_MARKASREAD_ID = 6;
    private static final int CONTEXTMENU_MARKASUNREAD_ID = 7;
    private static final int CONTEXTMENU_REFRESH_ID = 4;
    private static final int DIALOG_ABOUT = 7;
    private static final int DIALOG_ADDFEED_ID = 1;
    private static final int DIALOG_ERROR_EXTERNALSTORAGENOTAVAILABLE = 6;
    private static final int DIALOG_ERROR_FEEDEXPORT = 4;
    private static final int DIALOG_ERROR_FEEDIMPORT = 3;
    private static final int DIALOG_ERROR_FEEDURLEXISTS = 2;
    private static final int DIALOG_ERROR_INVALIDIMPORTFILE = 5;
    private static final String EXTENSION_OPML = ".opml";
    private static final String EXTENSION_PHP = ".php";
    private static final String EXTENSION_XML = ".xml";
    private static final int MENU_ABOUT_ID = 10;
    private static final int MENU_ADDFEED_ID = 1;
    private static final int MENU_ALLREAD = 9;
    private static final int MENU_DISABLEFEEDSORT = 14;
    private static final int MENU_ENABLEFEEDSORT = 13;
    private static final int MENU_EXPORT_ID = 12;
    private static final int MENU_IMPORT_ID = 11;
    private static final int MENU_REFRESH_ID = 2;
    private static final int MENU_SETTINGS_ID = 8;
    static NotificationManager notificationManager;
    boolean feedSort;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (notificationManager == null) {
            notificationManager = (NotificationManager) getSystemService("notification");
        }
        setContentView((int) R.layout.main);
        setListAdapter(new RSSOverviewListAdapter(this));
        getListView().setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {
                menu.setHeaderTitle(((TextView) ((AdapterView.AdapterContextMenuInfo) menuInfo).targetView.findViewById(16908308)).getText());
                menu.add(0, 3, 0, (int) R.string.contextmenu_edit);
                menu.add(0, 4, 0, (int) R.string.contextmenu_refresh);
                menu.add(0, 5, 0, (int) R.string.contextmenu_delete);
                menu.add(0, 6, 0, (int) R.string.contextmenu_markasread);
                menu.add(0, 7, 0, (int) R.string.contextmenu_markasunread);
            }
        });
        getListView().setOnTouchListener(new View.OnTouchListener() {
            private int dragedItem = -1;
            private ImageView dragedView;
            private WindowManager.LayoutParams layoutParams;
            private ListView listView;
            private int minY;
            private WindowManager windowManager;

            {
                this.windowManager = RSSOverview.this.getWindowManager();
                this.minY = 25;
                this.listView = RSSOverview.this.getListView();
            }

            public boolean onTouch(View v, MotionEvent event) {
                if (!RSSOverview.this.feedSort) {
                    return false;
                }
                switch (event.getAction()) {
                    case 0:
                    case 2:
                        if (this.dragedItem != -1) {
                            if (this.dragedView != null) {
                                this.layoutParams.y = Math.max(this.minY, Math.max(0, Math.min((int) event.getY(), this.listView.getHeight() - this.minY)));
                                this.windowManager.updateViewLayout(this.dragedView, this.layoutParams);
                                break;
                            }
                        } else {
                            this.dragedItem = this.listView.pointToPosition((int) event.getX(), (int) event.getY());
                            if (this.dragedItem > -1) {
                                this.dragedView = new ImageView(this.listView.getContext());
                                View item = this.listView.getChildAt(this.dragedItem - this.listView.getFirstVisiblePosition());
                                if (item == null) {
                                    this.dragedItem = -1;
                                    break;
                                } else {
                                    item.setDrawingCacheEnabled(true);
                                    this.dragedView.setImageBitmap(Bitmap.createBitmap(item.getDrawingCache()));
                                    this.layoutParams = new WindowManager.LayoutParams();
                                    this.layoutParams.height = -2;
                                    this.layoutParams.gravity = 48;
                                    this.layoutParams.y = (int) event.getY();
                                    this.windowManager.addView(this.dragedView, this.layoutParams);
                                    break;
                                }
                            }
                        }
                        break;
                    case 1:
                    case 3:
                        if (this.dragedItem > -1) {
                            this.windowManager.removeView(this.dragedView);
                            int newPosition = this.listView.pointToPosition((int) event.getX(), (int) event.getY());
                            if (newPosition == -1) {
                                newPosition = this.listView.getCount() - 1;
                            }
                            if (newPosition != this.dragedItem) {
                                ContentValues values = new ContentValues();
                                values.put("priority", Integer.valueOf(newPosition));
                                RSSOverview.this.getContentResolver().update(FeedData.FeedColumns.CONTENT_URI(this.listView.getItemIdAtPosition(this.dragedItem)), values, null, null);
                            }
                            this.dragedItem = -1;
                            break;
                        }
                        break;
                }
                return true;
            }
        });
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(Strings.SETTINGS_REFRESHENABLED, false)) {
            startService(new Intent(this, RefreshService.class));
        }
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(Strings.SETTINGS_REFRESHONPENENABLED, false)) {
            sendBroadcast(new Intent(Strings.ACTION_REFRESHFEEDS));
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        notificationManager.cancel(0);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 1, 0, (int) R.string.menu_addfeed).setIcon(17301555);
        menu.add(0, 2, 0, (int) R.string.menu_refresh).setIcon(17301581);
        menu.add(0, (int) MENU_SETTINGS_ID, 0, (int) R.string.menu_settings).setIcon(17301577);
        menu.add(0, (int) MENU_ALLREAD, 0, (int) R.string.menu_allread).setIcon(17301580);
        menu.add(0, (int) MENU_ABOUT_ID, 0, (int) R.string.menu_about).setIcon(17301569);
        menu.add(0, (int) MENU_IMPORT_ID, 0, (int) R.string.menu_import);
        menu.add(0, (int) MENU_EXPORT_ID, 0, (int) R.string.menu_export);
        menu.add(0, (int) MENU_ENABLEFEEDSORT, 0, (int) R.string.menu_enablefeedsort);
        menu.add(1, (int) MENU_DISABLEFEEDSORT, 0, (int) R.string.menu_disablefeedsort).setIcon(17301560);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean z;
        if (this.feedSort) {
            z = false;
        } else {
            z = true;
        }
        menu.setGroupVisible(0, z);
        menu.setGroupVisible(1, this.feedSort);
        return true;
    }

    public boolean onMenuItemSelected(int featureId, final MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                showDialog(1);
                return true;
            case 2:
                sendBroadcast(new Intent(Strings.ACTION_REFRESHFEEDS));
                return true;
            case 3:
                String id = Long.toString(((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).id);
                Cursor cursor = getContentResolver().query(FeedData.FeedColumns.CONTENT_URI(id), new String[]{FeedData.FeedColumns.NAME, FeedData.FeedColumns.URL}, null, null, null);
                cursor.moveToFirst();
                createURLDialog(cursor.getString(0), cursor.getString(1), id).show();
                cursor.close();
                return true;
            case 4:
                sendBroadcast(new Intent(Strings.ACTION_REFRESHFEEDS).putExtra("feedid", Long.toString(((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).id)));
                return true;
            case 5:
                Cursor cursor2 = getContentResolver().query(FeedData.FeedColumns.CONTENT_URI(Long.toString(((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).id)), new String[]{FeedData.FeedColumns.NAME}, null, null, null);
                cursor2.moveToFirst();
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setIcon(17301543);
                builder.setTitle(cursor2.getString(0));
                builder.setMessage((int) R.string.question_deletefeed);
                builder.setPositiveButton(17039379, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        final MenuItem menuItem = item;
                        new Thread() {
                            public void run() {
                                RSSOverview.this.getContentResolver().delete(FeedData.FeedColumns.CONTENT_URI(Long.toString(((AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo()).id)), null, null);
                                RSSOverview.this.sendBroadcast(new Intent(Strings.ACTION_UPDATEWIDGET));
                            }
                        }.start();
                    }
                });
                builder.setNegativeButton(17039369, (DialogInterface.OnClickListener) null);
                cursor2.close();
                builder.show();
                return true;
            case 6:
                new Thread() {
                    public void run() {
                        RSSOverview.this.getContentResolver().update(FeedData.EntryColumns.CONTENT_URI(Long.toString(((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).id)), RSSOverview.getReadContentValues(), FeedData.EntryColumns.READDATE + Strings.DB_ISNULL, null);
                    }
                }.start();
                return true;
            case 7:
                new Thread() {
                    public void run() {
                        RSSOverview.this.getContentResolver().update(FeedData.EntryColumns.CONTENT_URI(Long.toString(((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).id)), RSSOverview.getUnreadContentValues(), null, null);
                    }
                }.start();
                return true;
            case MENU_SETTINGS_ID /*8*/:
                startActivityForResult(new Intent(this, ApplicationPreferencesActivity.class), 1);
                return true;
            case MENU_ALLREAD /*9*/:
                new Thread() {
                    public void run() {
                        if (RSSOverview.this.getContentResolver().update(FeedData.EntryColumns.CONTENT_URI, RSSOverview.getReadContentValues(), FeedData.EntryColumns.READDATE + Strings.DB_ISNULL, null) > 0) {
                            RSSOverview.this.getContentResolver().notifyChange(FeedData.FeedColumns.CONTENT_URI, null);
                        }
                    }
                }.start();
                return true;
            case MENU_ABOUT_ID /*10*/:
                showDialog(7);
                return true;
            case MENU_IMPORT_ID /*11*/:
                if (Environment.getExternalStorageState().equals("mounted") || Environment.getExternalStorageState().equals("mounted_ro")) {
                    AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
                    builder2.setTitle((int) R.string.select_file);
                    try {
                        final String[] fileNames = Environment.getExternalStorageDirectory().list(new FilenameFilter() {
                            public boolean accept(File dir, String filename) {
                                return filename.endsWith(RSSOverview.EXTENSION_OPML) || filename.endsWith(RSSOverview.EXTENSION_XML) || filename.endsWith(RSSOverview.EXTENSION_PHP);
                            }
                        });
                        builder2.setItems(fileNames, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    OPML.importFromFile(Environment.getExternalStorageDirectory().toString() + File.separator + fileNames[which], RSSOverview.this);
                                } catch (Exception e) {
                                    RSSOverview.this.showDialog(3);
                                }
                            }
                        });
                        builder2.create().show();
                        return true;
                    } catch (Exception e) {
                        showDialog(3);
                        return true;
                    }
                } else {
                    showDialog(6);
                    return true;
                }
            case MENU_EXPORT_ID /*12*/:
                if (Environment.getExternalStorageState().equals("mounted") || Environment.getExternalStorageState().equals("mounted_ro")) {
                    try {
                        String filename = Environment.getExternalStorageDirectory().toString() + "/sparse_rss_" + System.currentTimeMillis() + EXTENSION_OPML;
                        OPML.exportToFile(filename, this);
                        Toast.makeText(this, String.format(getString(R.string.message_exportedto), filename), 1).show();
                        return true;
                    } catch (Exception e2) {
                        showDialog(4);
                        return true;
                    }
                } else {
                    showDialog(6);
                    return true;
                }
            case MENU_ENABLEFEEDSORT /*13*/:
                this.feedSort = true;
                return true;
            case MENU_DISABLEFEEDSORT /*14*/:
                this.feedSort = false;
                return true;
            default:
                return true;
        }
    }

    public static final ContentValues getReadContentValues() {
        ContentValues values = new ContentValues();
        values.put(FeedData.EntryColumns.READDATE, Long.valueOf(System.currentTimeMillis()));
        return values;
    }

    public static final ContentValues getUnreadContentValues() {
        ContentValues values = new ContentValues();
        values.putNull(FeedData.EntryColumns.READDATE);
        return values;
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView listView, View view, int position, long id) {
        Intent intent = new Intent("android.intent.action.VIEW", FeedData.EntryColumns.CONTENT_URI(Long.toString(id)));
        intent.putExtra(FeedData.FeedColumns.NAME, ((TextView) view.findViewById(16908308)).getText());
        intent.putExtra(FeedData.FeedColumns.ICON, view.getTag() != null ? (byte[]) view.getTag() : null);
        startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        Dialog dialog;
        switch (id) {
            case 1:
                dialog = createURLDialog(null, null, null);
                break;
            case 2:
                dialog = createErrorDialog(R.string.error_feedurlexists);
                break;
            case 3:
                dialog = createErrorDialog(R.string.error_feedimport);
                break;
            case 4:
                dialog = createErrorDialog(R.string.error_feedexport);
                break;
            case 5:
                dialog = createErrorDialog(R.string.error_invalidimportfile);
                break;
            case 6:
                dialog = createErrorDialog(R.string.error_externalstoragenotavailable);
                break;
            case 7:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setIcon(17301659);
                builder.setTitle((int) R.string.menu_about);
                MainTabActivity.INSTANCE.setupLicenseText(builder);
                builder.setPositiveButton(17039370, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                return builder.create();
            default:
                dialog = null;
                break;
        }
        return dialog;
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int id, Dialog dialog) {
        if (id == 1) {
            ((EditText) dialog.findViewById(R.id.feed_url)).setText(Strings.EMPTY);
            ((EditText) dialog.findViewById(R.id.feed_title)).setText(Strings.EMPTY);
        }
        super.onPrepareDialog(id, dialog);
    }

    private Dialog createURLDialog(String title, String url, final String id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate((int) R.layout.feedsettings, (ViewGroup) null);
        final EditText nameEditText = (EditText) view.findViewById(R.id.feed_title);
        final EditText urlEditText = (EditText) view.findViewById(R.id.feed_url);
        if (title != null) {
            builder.setTitle(title);
            nameEditText.setText(title);
        } else {
            builder.setTitle((int) R.string.editfeed_title);
        }
        if (url != null) {
            urlEditText.setText(url);
            int urlLength = url.length();
            if (urlLength > 0) {
                urlEditText.setSelection(urlLength - 1);
            }
            builder.setPositiveButton(17039370, new DialogInterface.OnClickListener() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
                 arg types: [java.lang.String, int]
                 candidates:
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
                public void onClick(DialogInterface dialog, int which) {
                    String str;
                    Cursor cursor = RSSOverview.this.getContentResolver().query(FeedData.FeedColumns.CONTENT_URI, new String[]{"_id"}, FeedData.FeedColumns.URL + Strings.DB_ARG, new String[]{urlEditText.getText().toString()}, null);
                    if (!cursor.moveToFirst() || id.equals(cursor.getString(0))) {
                        ContentValues values = new ContentValues();
                        values.put(FeedData.FeedColumns.URL, urlEditText.getText().toString());
                        String name = nameEditText.getText().toString();
                        if (name.trim().length() > 0) {
                            str = name;
                        } else {
                            str = null;
                        }
                        values.put(FeedData.FeedColumns.NAME, str);
                        values.put(FeedData.FeedColumns.FETCHMODE, (Integer) 0);
                        values.put(FeedData.FeedColumns.ERROR, (String) null);
                        RSSOverview.this.getContentResolver().update(FeedData.FeedColumns.CONTENT_URI(id), values, null, null);
                    } else {
                        RSSOverview.this.showDialog(2);
                    }
                    cursor.close();
                }
            });
        } else {
            builder.setPositiveButton(17039370, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    String url = urlEditText.getText().toString();
                    Cursor cursor = RSSOverview.this.getContentResolver().query(FeedData.FeedColumns.CONTENT_URI, null, FeedData.FeedColumns.URL + Strings.DB_ARG, new String[]{url}, null);
                    if (cursor.moveToFirst()) {
                        RSSOverview.this.showDialog(2);
                    } else {
                        ContentValues values = new ContentValues();
                        if (!url.startsWith(Strings.HTTP) && !url.startsWith(Strings.HTTPS)) {
                            url = Strings.HTTP + url;
                        }
                        values.put(FeedData.FeedColumns.URL, url);
                        values.put(FeedData.FeedColumns.ERROR, (String) null);
                        String name = nameEditText.getText().toString();
                        if (name.trim().length() > 0) {
                            values.put(FeedData.FeedColumns.NAME, name);
                        }
                        RSSOverview.this.getContentResolver().insert(FeedData.FeedColumns.CONTENT_URI, values);
                    }
                    cursor.close();
                }
            });
        }
        builder.setView(view);
        builder.setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
        return builder.create();
    }

    private Dialog createErrorDialog(int messageId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(messageId);
        builder.setTitle((int) R.string.error);
        builder.setIcon(17301543);
        builder.setPositiveButton(17039370, (DialogInterface.OnClickListener) null);
        return builder.create();
    }
}
