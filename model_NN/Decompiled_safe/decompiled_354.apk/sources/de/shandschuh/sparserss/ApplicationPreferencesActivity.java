package de.shandschuh.sparserss;

import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import de.shandschuh.sparserss.service.RefreshService;

public class ApplicationPreferencesActivity extends PreferenceActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.layout.preferences);
        findPreference(Strings.SETTINGS_REFRESHENABLED).setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                if (Boolean.TRUE.equals(newValue)) {
                    new Thread() {
                        public void run() {
                            ApplicationPreferencesActivity.this.startService(new Intent(ApplicationPreferencesActivity.this, RefreshService.class));
                        }
                    }.start();
                    return true;
                }
                ApplicationPreferencesActivity.this.stopService(new Intent(ApplicationPreferencesActivity.this, RefreshService.class));
                return true;
            }
        });
        findPreference(Strings.SETTINGS_SHOWTABS).setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                if (MainTabActivity.INSTANCE == null) {
                    return true;
                }
                MainTabActivity.INSTANCE.setTabWidgetVisible(Boolean.TRUE.equals(newValue));
                return true;
            }
        });
    }
}
