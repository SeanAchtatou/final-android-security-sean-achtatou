package de.shandschuh.sparserss.handler;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.preference.PreferenceManager;
import de.shandschuh.sparserss.Strings;
import de.shandschuh.sparserss.provider.FeedData;
import de.shandschuh.sparserss.provider.FeedDataContentProvider;
import de.shandschuh.sparserss.service.FetcherService;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class RSSHandler extends DefaultHandler {
    public static final String AMP = "&";
    public static final String AMP_SG = "&amp;";
    private static final String ATTRIBUTE_HREF = "href";
    private static final int DATEFORMAT_COUNT = 3;
    private static final StringBuilder DB_FAVORITE = new StringBuilder(" AND (").append(FeedData.EntryColumns.FAVORITE).append(Strings.DB_ISNULL).append(" OR ").append(FeedData.EntryColumns.FAVORITE).append('=').append("0)");
    private static long KEEP_TIME = 172800000;
    private static final int PUBDATEFORMAT_COUNT = 3;
    private static final DateFormat[] PUBDATE_DATEFORMATS = {new SimpleDateFormat("EEE', 'd' 'MMM' 'yyyy' 'HH:mm:ss' 'Z", Locale.US), new SimpleDateFormat("d' 'MMM' 'yyyy' 'HH:mm:ss' 'Z", Locale.US), new SimpleDateFormat("EEE', 'd' 'MMM' 'yyyy' 'HH:mm:ss' 'z", Locale.US)};
    private static final String TAG_CONTENT = "content";
    private static final String TAG_DATE = "date";
    private static final String TAG_DESCRIPTION = "description";
    private static final String TAG_ENCODEDCONTENT = "encoded";
    private static final String TAG_ENTRY = "entry";
    private static final String TAG_FEED = "feed";
    private static final String TAG_ITEM = "item";
    private static final String TAG_LASTBUILDDATE = "lastBuildDate";
    private static final String TAG_LINK = "link";
    private static final String TAG_PUBDATE = "pubDate";
    private static final String TAG_RDF = "rdf";
    private static final String TAG_RSS = "rss";
    private static final String TAG_SUMMARY = "summary";
    private static final String TAG_TITLE = "title";
    private static final String TAG_UPDATED = "updated";
    private static final String[] TIMEZONES = {"MEST", "EST", "PST"};
    private static final int TIMEZONES_COUNT = 3;
    private static final String[] TIMEZONES_REPLACE = {"+0200", "-0500", "-0800"};
    private static final DateFormat[] UPDATE_DATEFORMATS = {new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ"), new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'"), new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSz", Locale.US)};
    private static Pattern imgPattern = Pattern.compile("<img src=\\s*['\"]([^'\"]+)['\"][^>]*>");
    private boolean cancelled;
    private Context context;
    private StringBuilder dateStringBuilder;
    private boolean dateTagEntered;
    private StringBuilder description;
    private boolean descriptionTagEntered;
    private boolean done;
    private Date entryDate;
    private StringBuilder entryLink;
    private Uri feedEntiresUri;
    private boolean feedRefreshed;
    private String feedTitle;
    private boolean fetchImages;
    private String id;
    private InputStream inputStream;
    private Date keepDateBorder;
    private Date lastBuildDate;
    private Date lastUpdateDate;
    private boolean lastUpdateDateTagEntered;
    private boolean linkTagEntered;
    private int newCount;
    private boolean pubDateTagEntered;
    private Reader reader;
    private long realLastUpdate;
    private StringBuilder title;
    private boolean titleTagEntered;
    private boolean updatedTagEntered;

    public RSSHandler(Context context2) {
        KEEP_TIME = Long.parseLong(PreferenceManager.getDefaultSharedPreferences(context2).getString(Strings.SETTINGS_KEEPTIME, "2")) * 86400000;
        this.context = context2;
    }

    public void init(Date lastUpdateDate2, String id2, String title2) {
        final long keepDateBorderTime;
        if (KEEP_TIME > 0) {
            keepDateBorderTime = System.currentTimeMillis() - KEEP_TIME;
        } else {
            keepDateBorderTime = 0;
        }
        this.keepDateBorder = new Date(keepDateBorderTime);
        this.lastUpdateDate = lastUpdateDate2;
        this.id = id2;
        this.feedEntiresUri = FeedData.EntryColumns.CONTENT_URI(id2);
        this.context.getContentResolver().delete(this.feedEntiresUri, "date" + '<' + keepDateBorderTime + ((CharSequence) DB_FAVORITE), null);
        if (this.fetchImages) {
            new Thread() {
                public void run() {
                    File[] files = new File(FeedDataContentProvider.IMAGEFOLDER).listFiles();
                    int i = files != null ? files.length : 0;
                    for (int n = 0; n < i; n++) {
                        if (files[n].lastModified() < keepDateBorderTime) {
                            files[n].delete();
                        }
                    }
                }
            }.start();
        }
        this.newCount = 0;
        this.feedRefreshed = false;
        this.feedTitle = title2;
        this.title = null;
        this.dateStringBuilder = null;
        this.entryLink = null;
        this.description = null;
        this.inputStream = null;
        this.reader = null;
        this.entryDate = null;
        this.lastBuildDate = null;
        this.realLastUpdate = lastUpdateDate2.getTime();
        this.done = false;
        this.cancelled = false;
        this.titleTagEntered = false;
        this.updatedTagEntered = false;
        this.linkTagEntered = false;
        this.descriptionTagEntered = false;
        this.pubDateTagEntered = false;
        this.dateTagEntered = false;
        this.lastUpdateDateTagEntered = false;
    }

    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (TAG_UPDATED.equals(localName)) {
            this.updatedTagEntered = true;
            this.dateStringBuilder = new StringBuilder();
        } else if (TAG_ENTRY.equals(localName) || TAG_ITEM.equals(localName)) {
            this.description = null;
            if (!this.feedRefreshed) {
                ContentValues values = new ContentValues();
                if (this.feedTitle == null && this.title != null && this.title.length() > 0) {
                    values.put(FeedData.FeedColumns.NAME, this.title.toString().trim());
                }
                values.put(FeedData.FeedColumns.ERROR, (String) null);
                values.put(FeedData.FeedColumns.LASTUPDATE, Long.valueOf(System.currentTimeMillis() - 1000));
                if (this.lastBuildDate != null) {
                    this.realLastUpdate = Math.max((this.entryDate == null || !this.entryDate.after(this.lastBuildDate)) ? this.lastBuildDate.getTime() : this.entryDate.getTime(), this.realLastUpdate);
                } else {
                    this.realLastUpdate = Math.max(this.entryDate != null ? this.entryDate.getTime() : System.currentTimeMillis() - 1000, this.realLastUpdate);
                }
                values.put(FeedData.FeedColumns.REALLASTUPDATE, Long.valueOf(this.realLastUpdate));
                this.context.getContentResolver().update(FeedData.FeedColumns.CONTENT_URI(this.id), values, null, null);
                this.title = null;
                this.feedRefreshed = true;
            }
        } else if ("title".equals(localName)) {
            if (this.title == null) {
                this.titleTagEntered = true;
                this.title = new StringBuilder();
            }
        } else if ("link".equals(localName)) {
            this.entryLink = new StringBuilder();
            boolean foundLink = false;
            int n = 0;
            int i = attributes.getLength();
            while (true) {
                if (n >= i) {
                    break;
                } else if (!ATTRIBUTE_HREF.equals(attributes.getLocalName(n))) {
                    n++;
                } else if (attributes.getValue(n) != null) {
                    this.entryLink.append(attributes.getValue(n));
                    foundLink = true;
                    this.linkTagEntered = false;
                } else {
                    this.linkTagEntered = true;
                }
            }
            if (!foundLink) {
                this.linkTagEntered = true;
            }
        } else if (TAG_DESCRIPTION.equals(localName) || TAG_SUMMARY.equals(localName) || TAG_CONTENT.equals(localName)) {
            if (this.description == null) {
                this.descriptionTagEntered = true;
                this.description = new StringBuilder();
            }
        } else if (TAG_PUBDATE.equals(localName)) {
            this.pubDateTagEntered = true;
            this.dateStringBuilder = new StringBuilder();
        } else if ("date".equals(localName)) {
            this.dateTagEntered = true;
            this.dateStringBuilder = new StringBuilder();
        } else if (TAG_LASTBUILDDATE.equals(localName)) {
            this.lastUpdateDateTagEntered = true;
            this.dateStringBuilder = new StringBuilder();
        } else if (TAG_ENCODEDCONTENT.equals(localName)) {
            this.descriptionTagEntered = true;
            this.description = new StringBuilder();
        }
    }

    public void characters(char[] ch, int start, int length) throws SAXException {
        if (this.titleTagEntered) {
            this.title.append(ch, start, length);
        } else if (this.updatedTagEntered) {
            this.dateStringBuilder.append(ch, start, length);
        } else if (this.linkTagEntered) {
            this.entryLink.append(ch, start, length);
        } else if (this.descriptionTagEntered) {
            this.description.append(ch, start, length);
        } else if (this.pubDateTagEntered) {
            this.dateStringBuilder.append(ch, start, length);
        } else if (this.dateTagEntered) {
            this.dateStringBuilder.append(ch, start, length);
        } else if (this.lastUpdateDateTagEntered) {
            this.dateStringBuilder.append(ch, start, length);
        }
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        if ("title".equals(localName)) {
            this.titleTagEntered = false;
        } else if (TAG_DESCRIPTION.equals(localName) || TAG_SUMMARY.equals(localName) || TAG_CONTENT.equals(localName) || TAG_ENCODEDCONTENT.equals(localName)) {
            this.descriptionTagEntered = false;
        } else if ("link".equals(localName)) {
            this.linkTagEntered = false;
        } else if (TAG_UPDATED.equals(localName)) {
            this.entryDate = parseUpdateDate(this.dateStringBuilder.toString());
            this.updatedTagEntered = false;
        } else if (TAG_PUBDATE.equals(localName)) {
            this.entryDate = parsePubdateDate(this.dateStringBuilder.toString().replace(Strings.TWOSPACE, Strings.SPACE));
            this.pubDateTagEntered = false;
        } else if (TAG_LASTBUILDDATE.equals(localName)) {
            this.lastBuildDate = parsePubdateDate(this.dateStringBuilder.toString().replace(Strings.TWOSPACE, Strings.SPACE));
            this.lastUpdateDateTagEntered = false;
        } else if ("date".equals(localName)) {
            this.entryDate = parseUpdateDate(this.dateStringBuilder.toString());
            this.dateTagEntered = false;
        } else if (TAG_ENTRY.equals(localName) || TAG_ITEM.equals(localName)) {
            if (this.title == null || (this.entryDate != null && (!this.entryDate.after(this.lastUpdateDate) || !this.entryDate.after(this.keepDateBorder)))) {
                cancel();
            } else {
                ContentValues values = new ContentValues();
                if (this.entryDate != null && this.entryDate.getTime() > this.realLastUpdate) {
                    this.realLastUpdate = this.entryDate.getTime();
                    values.put(FeedData.FeedColumns.REALLASTUPDATE, Long.valueOf(this.realLastUpdate));
                    this.context.getContentResolver().update(FeedData.FeedColumns.CONTENT_URI(this.id), values, null, null);
                    values.clear();
                }
                if (this.entryDate != null) {
                    values.put("date", Long.valueOf(this.entryDate.getTime()));
                    values.putNull(FeedData.EntryColumns.READDATE);
                }
                values.put("title", this.title.toString().trim().replace("&amp;", "&").replaceAll(Strings.HTML_TAG_REGEX, Strings.EMPTY).replace(Strings.HTML_LT, Strings.LT).replace(Strings.HTML_GT, Strings.GT));
                Vector<String> images = null;
                if (this.description != null) {
                    String descriptionString = this.description.toString().trim().replaceAll(Strings.HTML_SPAN_REGEX, Strings.EMPTY);
                    if (this.fetchImages) {
                        images = new Vector<>(4);
                        Matcher matcher = imgPattern.matcher(this.description);
                        while (matcher.find()) {
                            String match = matcher.group(1).replace(Strings.SPACE, Strings.URL_SPACE);
                            images.add(match);
                            descriptionString = descriptionString.replace(match, Strings.FILEURL + FeedDataContentProvider.IMAGEFOLDER + Strings.IMAGEID_REPLACEMENT + match.substring(match.lastIndexOf(47) + 1));
                        }
                    }
                    values.put(FeedData.EntryColumns.ABSTRACT, descriptionString);
                    this.description = null;
                }
                String entryLinkString = this.entryLink.toString().trim();
                if (entryLinkString.length() > 0) {
                    if (this.context.getContentResolver().update(this.feedEntiresUri, values, "link" + Strings.DB_ARG, new String[]{entryLinkString}) == 0) {
                        values.put("link", entryLinkString);
                        if (this.entryDate == null) {
                            values.put("date", Long.valueOf(System.currentTimeMillis()));
                        } else {
                            values.remove(FeedData.EntryColumns.READDATE);
                        }
                        String id2 = this.context.getContentResolver().insert(this.feedEntiresUri, values).getLastPathSegment();
                        if (this.fetchImages) {
                            new File(FeedDataContentProvider.IMAGEFOLDER).mkdir();
                            int i = images != null ? images.size() : 0;
                            for (int n = 0; n < i; n++) {
                                try {
                                    String match2 = (String) images.get(n);
                                    byte[] data = FetcherService.getBytes(new URL((String) images.get(n)).openStream());
                                    FileOutputStream fileOutputStream = new FileOutputStream(FeedDataContentProvider.IMAGEFOLDER + id2 + Strings.IMAGEFILE_IDSEPARATOR + match2.substring(match2.lastIndexOf(47) + 1));
                                    fileOutputStream.write(data);
                                    fileOutputStream.close();
                                } catch (Exception e) {
                                }
                            }
                        }
                        this.newCount = this.newCount + 1;
                    }
                }
                if (this.entryDate == null) {
                    cancel();
                }
            }
            this.title = null;
        } else if (TAG_RSS.equals(localName) || TAG_RDF.equals(localName) || TAG_FEED.equals(localName)) {
            this.done = true;
        }
    }

    public int getNewCount() {
        return this.newCount;
    }

    public boolean isDone() {
        return this.done;
    }

    public boolean isCancelled() {
        return this.cancelled;
    }

    public void setInputStream(InputStream inputStream2) {
        this.inputStream = inputStream2;
        this.reader = null;
    }

    public void setReader(Reader reader2) {
        this.reader = reader2;
        this.inputStream = null;
    }

    private void cancel() {
        this.cancelled = true;
        this.done = true;
        if (this.inputStream != null) {
            try {
                this.inputStream.close();
            } catch (IOException e) {
            }
        } else if (this.reader != null) {
            try {
                this.reader.close();
            } catch (IOException e2) {
            }
        }
    }

    public void setFetchImages(boolean fetchImages2) {
        this.fetchImages = fetchImages2;
    }

    private static Date parseUpdateDate(String string) {
        int n = 0;
        while (n < 3) {
            try {
                return UPDATE_DATEFORMATS[n].parse(string);
            } catch (ParseException e) {
                n++;
            }
        }
        return null;
    }

    private static Date parsePubdateDate(String string) {
        for (int n = 0; n < 3; n++) {
            string = string.replace(TIMEZONES[n], TIMEZONES_REPLACE[n]);
        }
        int n2 = 0;
        while (n2 < 3) {
            try {
                return PUBDATE_DATEFORMATS[n2].parse(string);
            } catch (ParseException e) {
                n2++;
            }
        }
        return null;
    }
}
