package de.shandschuh.sparserss;

import android.app.ListActivity;
import android.content.ContentUris;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import de.shandschuh.sparserss.provider.FeedData;

public class EntriesListActivity extends ListActivity {
    private static final int CONTEXTMENU_DELETE_ID = 6;
    private static final int CONTEXTMENU_MARKASREAD_ID = 4;
    private static final int CONTEXTMENU_MARKASUNREAD_ID = 5;
    public static final String EXTRA_AUTORELOAD = "autoreload";
    public static final String EXTRA_SHOWFEEDINFO = "show_feedinfo";
    public static final String EXTRA_SHOWREAD = "show_read";
    private static final int MENU_HIDEREAD_ID = 3;
    private static final int MENU_MARKASREAD_ID = 1;
    private static final int MENU_MARKASUNREAD_ID = 2;
    private EntriesListAdapter entriesListAdapter;
    private byte[] iconBytes;
    /* access modifiers changed from: private */
    public Uri uri;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.iconBytes = getIntent().getByteArrayExtra(FeedData.FeedColumns.ICON);
        if (this.iconBytes != null && this.iconBytes.length > 0 && !requestWindowFeature(MENU_HIDEREAD_ID)) {
            this.iconBytes = null;
        }
        setContentView((int) R.layout.entries);
        Intent intent = getIntent();
        this.uri = intent.getData();
        this.entriesListAdapter = new EntriesListAdapter(this, this.uri, intent.getBooleanExtra(EXTRA_SHOWFEEDINFO, false), intent.getBooleanExtra(EXTRA_AUTORELOAD, false));
        setListAdapter(this.entriesListAdapter);
        String title = intent.getStringExtra(FeedData.FeedColumns.NAME);
        if (title != null) {
            setTitle(title);
        }
        if (this.iconBytes != null && this.iconBytes.length > 0) {
            setFeatureDrawable(MENU_HIDEREAD_ID, new BitmapDrawable(BitmapFactory.decodeByteArray(this.iconBytes, 0, this.iconBytes.length)));
        }
        RSSOverview.notificationManager.cancel(0);
        getListView().setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {
                menu.setHeaderTitle(((TextView) ((AdapterView.AdapterContextMenuInfo) menuInfo).targetView.findViewById(16908308)).getText());
                menu.add(0, (int) EntriesListActivity.CONTEXTMENU_MARKASREAD_ID, 0, (int) R.string.contextmenu_markasread).setIcon(17301570);
                menu.add(0, (int) EntriesListActivity.CONTEXTMENU_MARKASUNREAD_ID, 0, (int) R.string.contextmenu_markasunread).setIcon(17301570);
                menu.add(0, (int) EntriesListActivity.CONTEXTMENU_DELETE_ID, 0, (int) R.string.contextmenu_delete).setIcon(17301564);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView listView, View view, int position, long id) {
        TextView textView = (TextView) view.findViewById(16908308);
        textView.setTypeface(Typeface.DEFAULT);
        textView.setEnabled(false);
        view.findViewById(16908309).setEnabled(false);
        this.entriesListAdapter.neutralizeReadState();
        startActivity(new Intent("android.intent.action.VIEW", ContentUris.withAppendedId(this.uri, id)).putExtra(EXTRA_SHOWREAD, this.entriesListAdapter.isShowRead()).putExtra(FeedData.FeedColumns.ICON, this.iconBytes));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, (int) MENU_MARKASREAD_ID, 0, (int) R.string.contextmenu_markasread).setIcon(17301580);
        menu.add(0, (int) MENU_MARKASUNREAD_ID, 0, (int) R.string.contextmenu_markasunread).setIcon(17301585);
        menu.add((int) MENU_MARKASREAD_ID, (int) MENU_HIDEREAD_ID, 0, (int) R.string.contextmenu_hideread).setCheckable(true).setIcon(17301560);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean z;
        if (this.entriesListAdapter.getCount() > 0) {
            z = true;
        } else {
            z = false;
        }
        menu.setGroupVisible(0, z);
        return super.onPrepareOptionsMenu(menu);
    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case MENU_MARKASREAD_ID /*1*/:
                new Thread() {
                    public void run() {
                        EntriesListActivity.this.getContentResolver().update(EntriesListActivity.this.uri, RSSOverview.getReadContentValues(), null, null);
                    }
                }.start();
                this.entriesListAdapter.markAsRead();
                break;
            case MENU_MARKASUNREAD_ID /*2*/:
                new Thread() {
                    public void run() {
                        EntriesListActivity.this.getContentResolver().update(EntriesListActivity.this.uri, RSSOverview.getUnreadContentValues(), null, null);
                    }
                }.start();
                this.entriesListAdapter.markAsUnread();
                break;
            case MENU_HIDEREAD_ID /*3*/:
                if (!item.isChecked()) {
                    item.setChecked(true).setTitle((int) R.string.contextmenu_showread).setIcon(17301591);
                    this.entriesListAdapter.showRead(false);
                    break;
                } else {
                    item.setChecked(false).setTitle((int) R.string.contextmenu_hideread).setIcon(17301560);
                    this.entriesListAdapter.showRead(true);
                    break;
                }
            case CONTEXTMENU_MARKASREAD_ID /*4*/:
                long id = ((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).id;
                getContentResolver().update(ContentUris.withAppendedId(this.uri, id), RSSOverview.getReadContentValues(), null, null);
                this.entriesListAdapter.markAsRead(id);
                break;
            case CONTEXTMENU_MARKASUNREAD_ID /*5*/:
                long id2 = ((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).id;
                getContentResolver().update(ContentUris.withAppendedId(this.uri, id2), RSSOverview.getUnreadContentValues(), null, null);
                this.entriesListAdapter.markAsUnread(id2);
                break;
            case CONTEXTMENU_DELETE_ID /*6*/:
                getContentResolver().delete(ContentUris.withAppendedId(this.uri, ((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).id), null, null);
                this.entriesListAdapter.getCursor().requery();
                break;
        }
        return true;
    }
}
