package de.shandschuh.sparserss.widget;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.view.View;
import de.shandschuh.sparserss.R;
import de.shandschuh.sparserss.provider.FeedData;

public class WidgetConfigActivity extends PreferenceActivity {
    private static final String NAMECOLUMN = ("ifnull(" + FeedData.FeedColumns.NAME + ',' + FeedData.FeedColumns.URL + ") as title");
    public static final String ZERO = "0";
    /* access modifiers changed from: private */
    public int widgetId;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setResult(0);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.widgetId = extras.getInt("appWidgetId", 0);
        }
        if (this.widgetId == 0) {
            finish();
        }
        addPreferencesFromResource(R.layout.widgetpreferences);
        setContentView((int) R.layout.widgetconfig);
        final PreferenceCategory feedsPreferenceCategory = (PreferenceCategory) getPreferenceScreen().getPreference(0);
        Cursor cursor = getContentResolver().query(FeedData.FeedColumns.CONTENT_URI, new String[]{"_id", NAMECOLUMN}, null, null, null);
        if (cursor.moveToFirst()) {
            int[] ids = new int[(cursor.getCount() + 1)];
            CheckBoxPreference checkboxPreference = new CheckBoxPreference(this);
            checkboxPreference.setTitle((int) R.string.all_feeds);
            feedsPreferenceCategory.addPreference(checkboxPreference);
            checkboxPreference.setKey(ZERO);
            checkboxPreference.setDisableDependentsState(true);
            ids[0] = 0;
            int n = 1;
            while (!cursor.isAfterLast()) {
                CheckBoxPreference checkboxPreference2 = new CheckBoxPreference(this);
                checkboxPreference2.setTitle(cursor.getString(1));
                ids[n] = cursor.getInt(0);
                checkboxPreference2.setKey(Integer.toString(ids[n]));
                feedsPreferenceCategory.addPreference(checkboxPreference2);
                checkboxPreference2.setDependency(ZERO);
                cursor.moveToNext();
                n++;
            }
            cursor.close();
            findViewById(R.id.save_button).setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    SharedPreferences.Editor preferences = WidgetConfigActivity.this.getSharedPreferences(SparseRSSAppWidgetProvider.class.getName(), 0).edit();
                    preferences.putBoolean(String.valueOf(WidgetConfigActivity.this.widgetId) + ".hideread", false);
                    StringBuilder builder = new StringBuilder();
                    int i = feedsPreferenceCategory.getPreferenceCount();
                    for (int n = 0; n < i; n++) {
                        CheckBoxPreference preference = (CheckBoxPreference) feedsPreferenceCategory.getPreference(n);
                        if (preference.isChecked()) {
                            if (n == 0) {
                                break;
                            }
                            if (builder.length() > 0) {
                                builder.append(',');
                            }
                            builder.append(preference.getKey());
                        }
                    }
                    String feedIds = builder.toString();
                    preferences.putString(String.valueOf(WidgetConfigActivity.this.widgetId) + ".feeds", feedIds);
                    preferences.commit();
                    SparseRSSAppWidgetProvider.updateAppWidget(WidgetConfigActivity.this, WidgetConfigActivity.this.widgetId, false, feedIds);
                    WidgetConfigActivity.this.setResult(-1, new Intent().putExtra("appWidgetId", WidgetConfigActivity.this.widgetId));
                    WidgetConfigActivity.this.finish();
                }
            });
            return;
        }
        cursor.close();
        setResult(-1, new Intent().putExtra("appWidgetId", this.widgetId));
    }
}
