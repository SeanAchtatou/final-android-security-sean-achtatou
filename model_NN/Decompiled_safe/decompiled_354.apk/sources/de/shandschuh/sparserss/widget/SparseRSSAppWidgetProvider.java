package de.shandschuh.sparserss.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.RemoteViews;
import de.shandschuh.sparserss.MainTabActivity;
import de.shandschuh.sparserss.R;
import de.shandschuh.sparserss.Strings;
import de.shandschuh.sparserss.provider.FeedData;

public class SparseRSSAppWidgetProvider extends AppWidgetProvider {
    private static final int[] ICON_IDS = {R.id.news_icon_1, R.id.news_icon_2, R.id.news_icon_3, R.id.news_icon_4, R.id.news_icon_5, R.id.news_icon_6, R.id.news_icon_7, R.id.news_icon_8, R.id.news_icon_9, R.id.news_icon_10};
    private static final int[] IDS = {R.id.news_1, R.id.news_2, R.id.news_3, R.id.news_4, R.id.news_5, R.id.news_6, R.id.news_7, R.id.news_8, R.id.news_9, R.id.news_10};
    private static final String LIMIT = " limit 10";

    public void onReceive(Context context, Intent intent) {
        if (Strings.ACTION_UPDATEWIDGET.equals(intent.getAction())) {
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
            onUpdate(context, appWidgetManager, appWidgetManager.getAppWidgetIds(new ComponentName(context, SparseRSSAppWidgetProvider.class)));
        }
    }

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        SharedPreferences preferences = context.getSharedPreferences(SparseRSSAppWidgetProvider.class.getName(), 0);
        int i = appWidgetIds.length;
        for (int n = 0; n < i; n++) {
            updateAppWidget(context, appWidgetManager, appWidgetIds[n], preferences.getBoolean(String.valueOf(appWidgetIds[n]) + ".hideread", false), preferences.getString(String.valueOf(appWidgetIds[n]) + ".feeds", Strings.EMPTY));
        }
    }

    static void updateAppWidget(Context context, int appWidgetId, boolean hideRead, String feedIds) {
        updateAppWidget(context, AppWidgetManager.getInstance(context), appWidgetId, hideRead, feedIds);
    }

    /* JADX INFO: Multiple debug info for r9v38 android.graphics.Bitmap: [D('bitmap' android.graphics.Bitmap), D('iconBytes' byte[])] */
    private static void updateAppWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId, boolean hideRead, String feedIds) {
        int k;
        StringBuilder selection = new StringBuilder();
        if (hideRead) {
            selection.append(FeedData.EntryColumns.READDATE).append(Strings.DB_ISNULL);
        }
        if (feedIds.length() > 0) {
            if (selection.length() > 0) {
                selection.append(Strings.DB_AND);
            }
            selection.append("feedid").append(" IN (" + feedIds).append(')');
        }
        Cursor cursor = context.getContentResolver().query(FeedData.EntryColumns.CONTENT_URI, new String[]{FeedData.EntryColumns.TITLE, "_id", FeedData.FeedColumns.ICON}, selection.toString(), null, FeedData.EntryColumns.DATE + Strings.DB_DESC + LIMIT);
        RemoteViews views = new RemoteViews(context.getPackageName(), (int) R.layout.homescreenwidget);
        views.setOnClickPendingIntent(R.id.feed_icon, PendingIntent.getActivity(context, 0, new Intent(context, MainTabActivity.class), 0));
        int k2 = 0;
        while (true) {
            k = k2;
            if (cursor.moveToNext() == 0 || k >= IDS.length) {
                cursor.close();
            } else {
                if (!cursor.isNull(2)) {
                    try {
                        byte[] iconBytes = cursor.getBlob(2);
                        if (iconBytes == null || iconBytes.length <= 0) {
                            views.setViewVisibility(ICON_IDS[k], 8);
                            views.setTextViewText(IDS[k], cursor.getString(0));
                        } else {
                            Bitmap bitmap = BitmapFactory.decodeByteArray(iconBytes, 0, iconBytes.length);
                            if (bitmap != null) {
                                views.setBitmap(ICON_IDS[k], "setImageBitmap", bitmap);
                                views.setViewVisibility(ICON_IDS[k], 0);
                                views.setTextViewText(IDS[k], Strings.SPACE + cursor.getString(0));
                            } else {
                                views.setViewVisibility(ICON_IDS[k], 8);
                                views.setTextViewText(IDS[k], cursor.getString(0));
                            }
                        }
                    } catch (Throwable th) {
                        views.setViewVisibility(ICON_IDS[k], 8);
                        views.setTextViewText(IDS[k], cursor.getString(0));
                    }
                } else {
                    views.setViewVisibility(ICON_IDS[k], 8);
                    views.setTextViewText(IDS[k], cursor.getString(0));
                }
                k2 = k + 1;
                views.setOnClickPendingIntent(IDS[k], PendingIntent.getActivity(context, 0, new Intent("android.intent.action.VIEW", FeedData.EntryColumns.ENTRY_CONTENT_URI(cursor.getString(1))), 268435456));
            }
        }
        cursor.close();
        for (int k3 = k; k3 < IDS.length; k3++) {
            views.setTextViewText(IDS[k3], Strings.EMPTY);
        }
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }
}
