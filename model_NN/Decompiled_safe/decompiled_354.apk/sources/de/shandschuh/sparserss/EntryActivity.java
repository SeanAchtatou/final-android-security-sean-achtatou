package de.shandschuh.sparserss;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import de.shandschuh.sparserss.provider.FeedData;
import java.text.DateFormat;
import java.util.Date;

public class EntryActivity extends Activity {
    private static final String AND_DATE = " and ((date=";
    private static final String AND_ID = " and _id";
    private static final String ASC = "date asc, _id desc limit 1";
    private static final String BODY_END = "</body>";
    private static final String DESC = "date desc, _id asc limit 1";
    private static final String FONTSIZE_END = "</font>";
    private static final String FONTSIZE_MIDDLE = "\">";
    private static final String FONTSIZE_START = "<font size=\"+";
    private static final String FONT_END = "</font></body>";
    private static final String FONT_FONTSIZE_START = "<body link=\"#97ACE5\" text=\"#D0D0D0\"><font size=\"+";
    private static final String FONT_START = "<body link=\"#97ACE5\" text=\"#D0D0D0\">";
    private static final String OR_DATE = " or date ";
    private static final String TEXT_HTML = "text/html";
    private static final String UTF8 = "utf-8";
    private String _id;
    private int abstractPosition;
    private boolean canShowIcon;
    private int datePosition;
    boolean favorite;
    private int favoritePosition;
    private int feedId;
    private int feedIdPosition;
    private byte[] iconBytes;
    private int linkPosition;
    private int readDatePosition;
    int scrollX;
    int scrollY;
    private boolean showRead;
    private int titlePosition;
    /* access modifiers changed from: private */
    public Uri uri;
    private WebView webView;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.canShowIcon = requestWindowFeature(3);
        setContentView((int) R.layout.entry);
        try {
            TextView titleTextView = (TextView) findViewById(16908310);
            titleTextView.setSingleLine(true);
            titleTextView.setHorizontallyScrolling(true);
            titleTextView.setMarqueeRepeatLimit(-1);
            titleTextView.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            titleTextView.setFocusable(true);
            titleTextView.setFocusableInTouchMode(true);
        } catch (Exception e) {
        }
        this.uri = getIntent().getData();
        this.showRead = getIntent().getBooleanExtra(EntriesListActivity.EXTRA_SHOWREAD, true);
        this.iconBytes = getIntent().getByteArrayExtra(FeedData.FeedColumns.ICON);
        Cursor entryCursor = getContentResolver().query(this.uri, null, null, null, null);
        this.titlePosition = entryCursor.getColumnIndex(FeedData.EntryColumns.TITLE);
        this.datePosition = entryCursor.getColumnIndex(FeedData.EntryColumns.DATE);
        this.abstractPosition = entryCursor.getColumnIndex(FeedData.EntryColumns.ABSTRACT);
        this.linkPosition = entryCursor.getColumnIndex(FeedData.EntryColumns.LINK);
        this.feedIdPosition = entryCursor.getColumnIndex("feedid");
        this.favoritePosition = entryCursor.getColumnIndex(FeedData.EntryColumns.FAVORITE);
        this.readDatePosition = entryCursor.getColumnIndex(FeedData.EntryColumns.READDATE);
        entryCursor.close();
        if (RSSOverview.notificationManager == null) {
            RSSOverview.notificationManager = (NotificationManager) getSystemService("notification");
        }
        this.webView = (WebView) findViewById(R.id.entry_abstract);
        this.scrollX = 0;
        this.scrollY = 0;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        RSSOverview.notificationManager.cancel(0);
        this.uri = getIntent().getData();
        reload();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    /* access modifiers changed from: private */
    public void reload() {
        if (this._id == null || !this._id.equals(this.uri.getLastPathSegment())) {
            this._id = this.uri.getLastPathSegment();
            ContentValues values = new ContentValues();
            values.put(FeedData.EntryColumns.READDATE, Long.valueOf(System.currentTimeMillis()));
            Cursor entryCursor = getContentResolver().query(this.uri, null, null, null, null);
            if (entryCursor.moveToFirst()) {
                String abstractText = entryCursor.getString(this.abstractPosition);
                if (entryCursor.isNull(this.readDatePosition)) {
                    getContentResolver().update(this.uri, values, FeedData.EntryColumns.READDATE + Strings.DB_ISNULL, null);
                }
                if (abstractText == null) {
                    String link = entryCursor.getString(this.linkPosition);
                    entryCursor.close();
                    finish();
                    startActivity(new Intent("android.intent.action.VIEW", Uri.parse(link)));
                    return;
                }
                setTitle(entryCursor.getString(this.titlePosition));
                this.feedId = entryCursor.getInt(this.feedIdPosition);
                if (this.canShowIcon) {
                    if (this.iconBytes == null || this.iconBytes.length <= 0) {
                        Cursor iconCursor = getContentResolver().query(FeedData.FeedColumns.CONTENT_URI(Integer.toString(this.feedId)), new String[]{"_id", FeedData.FeedColumns.ICON}, null, null, null);
                        if (iconCursor.moveToFirst()) {
                            this.iconBytes = iconCursor.getBlob(1);
                            if (this.iconBytes != null && this.iconBytes.length > 0) {
                                setFeatureDrawable(3, new BitmapDrawable(BitmapFactory.decodeByteArray(this.iconBytes, 0, this.iconBytes.length)));
                            }
                        }
                        iconCursor.close();
                    } else {
                        setFeatureDrawable(3, new BitmapDrawable(BitmapFactory.decodeByteArray(this.iconBytes, 0, this.iconBytes.length)));
                    }
                }
                long date = entryCursor.getLong(this.datePosition);
                ((TextView) findViewById(R.id.entry_date)).setText(DateFormat.getDateTimeInstance().format(new Date(date)));
                ImageView imageView = (ImageView) findViewById(16908294);
                this.favorite = entryCursor.getInt(this.favoritePosition) == 1;
                imageView.setImageResource(this.favorite ? 17301620 : 17301621);
                final ImageView imageView2 = imageView;
                imageView.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        boolean z;
                        int i;
                        EntryActivity entryActivity = EntryActivity.this;
                        if (EntryActivity.this.favorite) {
                            z = false;
                        } else {
                            z = true;
                        }
                        entryActivity.favorite = z;
                        imageView2.setImageResource(EntryActivity.this.favorite ? 17301620 : 17301621);
                        ContentValues values = new ContentValues();
                        if (EntryActivity.this.favorite) {
                            i = 1;
                        } else {
                            i = 0;
                        }
                        values.put(FeedData.EntryColumns.FAVORITE, Integer.valueOf(i));
                        EntryActivity.this.getContentResolver().update(EntryActivity.this.uri, values, null, null);
                    }
                });
                String abstractText2 = abstractText.replace(Strings.IMAGEID_REPLACEMENT, String.valueOf(this.uri.getLastPathSegment()) + Strings.IMAGEFILE_IDSEPARATOR);
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
                if (preferences.getBoolean(Strings.SETTINGS_DISABLEPICTURES, false)) {
                    abstractText2 = abstractText2.replaceAll(Strings.HTML_IMG_REGEX, Strings.EMPTY);
                }
                int fontsize = Integer.parseInt(preferences.getString(Strings.SETTINGS_FONTSIZE, Strings.ONE));
                if (!preferences.getBoolean(Strings.SETTINGS_BLACKTEXTONWHITE, false)) {
                    if (fontsize > 0) {
                        this.webView.loadDataWithBaseURL(null, FONT_FONTSIZE_START + fontsize + FONTSIZE_MIDDLE + abstractText2 + FONT_END, TEXT_HTML, UTF8, null);
                    } else {
                        this.webView.loadDataWithBaseURL(null, FONT_START + abstractText2 + BODY_END, TEXT_HTML, UTF8, null);
                    }
                    this.webView.setBackgroundColor(17170444);
                } else if (fontsize > 0) {
                    this.webView.loadDataWithBaseURL(null, FONTSIZE_START + fontsize + FONTSIZE_MIDDLE + abstractText2 + FONTSIZE_END, TEXT_HTML, UTF8, null);
                } else {
                    this.webView.loadDataWithBaseURL(null, abstractText2, TEXT_HTML, UTF8, null);
                }
                final String string = entryCursor.getString(this.linkPosition);
                ((Button) findViewById(R.id.url_button)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        EntryActivity.this.startActivityForResult(new Intent("android.intent.action.VIEW", Uri.parse(string)), 0);
                    }
                });
                entryCursor.close();
                setupButton(R.id.prev_button, false, date);
                setupButton(R.id.next_button, true, date);
                this.webView.scrollTo(this.scrollX, this.scrollY);
                return;
            }
            entryCursor.close();
        }
    }

    private void setupButton(int buttonId, boolean successor, long date) {
        StringBuilder queryString = new StringBuilder("feedid").append('=').append(this.feedId).append(AND_DATE).append(date).append(AND_ID).append(successor ? '>' : '<').append(this._id).append(')').append(OR_DATE).append(successor ? '<' : '>').append(date).append(')');
        if (!this.showRead) {
            queryString.append(Strings.DB_AND).append(EntriesListAdapter.READDATEISNULL);
        }
        Cursor cursor = getContentResolver().query(FeedData.EntryColumns.CONTENT_URI, new String[]{"_id"}, queryString.toString(), null, successor ? DESC : ASC);
        Button button = (Button) findViewById(buttonId);
        if (cursor.moveToFirst()) {
            button.setEnabled(true);
            final String id = cursor.getString(0);
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    EntryActivity.this.uri = FeedData.EntryColumns.ENTRY_CONTENT_URI(id);
                    EntryActivity.this.getIntent().setData(EntryActivity.this.uri);
                    EntryActivity.this.scrollX = 0;
                    EntryActivity.this.scrollY = 0;
                    EntryActivity.this.reload();
                }
            });
        } else {
            button.setEnabled(false);
        }
        cursor.close();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.scrollX = this.webView.getScrollX();
        this.scrollY = this.webView.getScrollY();
    }
}
