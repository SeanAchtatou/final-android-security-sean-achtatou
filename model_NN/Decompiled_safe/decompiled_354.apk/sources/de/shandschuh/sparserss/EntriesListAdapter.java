package de.shandschuh.sparserss;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;
import de.shandschuh.sparserss.provider.FeedData;
import java.text.DateFormat;
import java.util.Date;
import java.util.Vector;

public class EntriesListAdapter extends ResourceCursorAdapter {
    public static DateFormat DATEFORMAT = DateFormat.getDateTimeInstance(3, 3);
    public static final String READDATEISNULL = "readdate is null";
    private static final String SQLREAD = "length(readdate) ASC, ";
    private static final int STATE_ALLREAD = 1;
    private static final int STATE_ALLUNREAD = 2;
    private static final int STATE_NEUTRAL = 0;
    private Activity context;
    private int dateColumn;
    private int favoriteColumn;
    private int feedIconColumn;
    private int feedNameColumn;
    private int forcedState;
    private int idColumn;
    private Vector<Long> markedAsRead;
    private Vector<Long> markedAsUnread;
    private int readDateColumn;
    private boolean showFeedInfo;
    private boolean showRead = true;
    private int titleColumnPosition;
    /* access modifiers changed from: private */
    public Uri uri;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.widget.ResourceCursorAdapter.<init>(android.content.Context, int, android.database.Cursor, boolean):void}
     arg types: [android.app.Activity, ?, android.database.Cursor, boolean]
     candidates:
      ClspMth{android.widget.ResourceCursorAdapter.<init>(android.content.Context, int, android.database.Cursor, int):void}
      ClspMth{android.widget.ResourceCursorAdapter.<init>(android.content.Context, int, android.database.Cursor, boolean):void} */
    public EntriesListAdapter(Activity context2, Uri uri2, boolean showFeedInfo2, boolean autoreload) {
        super((Context) context2, (int) R.layout.listitem, createManagedCursor(context2, uri2, true), autoreload);
        this.context = context2;
        this.uri = uri2;
        this.titleColumnPosition = getCursor().getColumnIndex(FeedData.EntryColumns.TITLE);
        this.dateColumn = getCursor().getColumnIndex(FeedData.EntryColumns.DATE);
        this.readDateColumn = getCursor().getColumnIndex(FeedData.EntryColumns.READDATE);
        this.favoriteColumn = getCursor().getColumnIndex(FeedData.EntryColumns.FAVORITE);
        this.idColumn = getCursor().getColumnIndex("_id");
        this.showFeedInfo = showFeedInfo2;
        if (showFeedInfo2) {
            this.feedIconColumn = getCursor().getColumnIndex(FeedData.FeedColumns.ICON);
            this.feedNameColumn = getCursor().getColumnIndex(FeedData.FeedColumns.NAME);
        }
        this.forcedState = 0;
        this.markedAsRead = new Vector<>();
        this.markedAsUnread = new Vector<>();
    }

    public void bindView(View view, Context context2, Cursor cursor) {
        TextView textView = (TextView) view.findViewById(16908308);
        textView.setText(cursor.getString(this.titleColumnPosition));
        TextView dateTextView = (TextView) view.findViewById(16908309);
        ImageView imageView = (ImageView) view.findViewById(16908294);
        final boolean favorite = cursor.getInt(this.favoriteColumn) == STATE_ALLREAD ? STATE_ALLREAD : false;
        final long id = cursor.getLong(this.idColumn);
        imageView.setImageResource(favorite ? 17301620 : 17301621);
        final Context context3 = context2;
        imageView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ContentValues values = new ContentValues();
                values.put(FeedData.EntryColumns.FAVORITE, Integer.valueOf(favorite ? 0 : EntriesListAdapter.STATE_ALLREAD));
                String[] strArr = new String[EntriesListAdapter.STATE_ALLREAD];
                strArr[0] = Long.toString(id);
                view.getContext().getContentResolver().update(EntriesListAdapter.this.uri, values, "_id" + Strings.DB_ARG, strArr);
                context3.getContentResolver().notifyChange(FeedData.EntryColumns.FAVORITES_CONTENT_URI, null);
            }
        });
        if (this.showFeedInfo) {
            byte[] iconBytes = cursor.getBlob(this.feedIconColumn);
            if (iconBytes == null || iconBytes.length <= 0) {
                dateTextView.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                dateTextView.setText(String.valueOf(DATEFORMAT.format(new Date(cursor.getLong(this.dateColumn)))) + ", " + cursor.getString(this.feedNameColumn));
            } else {
                dateTextView.setText(Strings.SPACE + DATEFORMAT.format(new Date(cursor.getLong(this.dateColumn))) + ", " + cursor.getString(this.feedNameColumn));
                dateTextView.setCompoundDrawablesWithIntrinsicBounds(new BitmapDrawable(BitmapFactory.decodeByteArray(iconBytes, 0, iconBytes.length)), (Drawable) null, (Drawable) null, (Drawable) null);
            }
        } else {
            textView.setText(cursor.getString(this.titleColumnPosition));
            dateTextView.setText(DateFormat.getDateTimeInstance().format(new Date(cursor.getLong(this.dateColumn))));
        }
        if ((this.forcedState != STATE_ALLUNREAD || this.markedAsRead.contains(Long.valueOf(id))) && ((this.forcedState == STATE_ALLREAD || !cursor.isNull(this.readDateColumn) || this.markedAsRead.contains(Long.valueOf(id))) && !this.markedAsUnread.contains(Long.valueOf(id)))) {
            textView.setTypeface(Typeface.DEFAULT);
            textView.setEnabled(false);
            dateTextView.setEnabled(false);
            return;
        }
        textView.setTypeface(Typeface.DEFAULT_BOLD);
        textView.setEnabled(true);
        dateTextView.setEnabled(true);
    }

    public void showRead(boolean showRead2) {
        if (showRead2 != this.showRead) {
            changeCursor(createManagedCursor(this.context, this.uri, showRead2));
            this.showRead = showRead2;
        }
    }

    public boolean isShowRead() {
        return this.showRead;
    }

    private static Cursor createManagedCursor(Activity context2, Uri uri2, boolean showRead2) {
        return context2.managedQuery(uri2, null, showRead2 ? null : READDATEISNULL, null, (PreferenceManager.getDefaultSharedPreferences(context2).getBoolean(Strings.SETTINGS_PRIORITIZE, false) ? SQLREAD : Strings.EMPTY) + FeedData.EntryColumns.DATE + Strings.DB_DESC);
    }

    public void markAsRead() {
        this.forcedState = STATE_ALLREAD;
        this.markedAsRead.clear();
        this.markedAsUnread.clear();
        notifyDataSetInvalidated();
    }

    public void markAsUnread() {
        this.forcedState = STATE_ALLUNREAD;
        this.markedAsRead.clear();
        this.markedAsUnread.clear();
        notifyDataSetInvalidated();
    }

    public void neutralizeReadState() {
        this.forcedState = 0;
    }

    public void markAsRead(long id) {
        this.markedAsRead.add(Long.valueOf(id));
        this.markedAsUnread.remove(Long.valueOf(id));
        notifyDataSetInvalidated();
    }

    public void markAsUnread(long id) {
        this.markedAsUnread.add(Long.valueOf(id));
        this.markedAsRead.remove(Long.valueOf(id));
        notifyDataSetInvalidated();
    }
}
