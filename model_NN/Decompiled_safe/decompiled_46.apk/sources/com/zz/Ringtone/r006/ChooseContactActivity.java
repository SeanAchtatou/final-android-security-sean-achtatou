package com.zz.Ringtone.r006;

import android.app.ListActivity;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class ChooseContactActivity extends ListActivity implements TextWatcher {
    private SimpleCursorAdapter mAdapter;
    private TextView mFilter;
    private Uri mRingUri;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle((int) R.string.choose_contact_title);
        this.mRingUri = getIntent().getData();
        setContentView((int) R.layout.choose_contact);
        try {
            this.mAdapter = new SimpleCursorAdapter(this, R.layout.contact_row, createCursor(""), new String[]{"starred", "display_name"}, new int[]{R.id.row_starred, R.id.row_display_name});
            this.mAdapter.setViewBinder(new SimpleCursorAdapter.ViewBinder() {
                public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
                    String strColumnName = cursor.getColumnName(columnIndex);
                    String strValue = cursor.getString(columnIndex);
                    if (strColumnName.equals("custom_ringtone")) {
                        if (strValue == null || strValue.length() <= 0) {
                            view.setVisibility(4);
                        } else {
                            view.setVisibility(0);
                        }
                        return true;
                    } else if (!strColumnName.equals("starred")) {
                        return false;
                    } else {
                        if (strValue == null || !strValue.equals("1")) {
                            view.setVisibility(4);
                        } else {
                            view.setVisibility(0);
                        }
                        return true;
                    }
                }
            });
            setListAdapter(this.mAdapter);
            getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView adapterView, View view, int position, long id) {
                    ChooseContactActivity.this.assignRingtone2Contact();
                }
            });
            this.mFilter = (TextView) findViewById(R.id.search_filter);
            if (this.mFilter != null) {
                this.mFilter.addTextChangedListener(this);
            }
        } catch (Exception e) {
        }
    }

    public void beforeTextChanged(CharSequence csParam, int paramInt1, int paramInt2, int paramInt3) {
    }

    public void onTextChanged(CharSequence csParam, int paramInt1, int paramInt2, int paramInt3) {
    }

    public void afterTextChanged(Editable paramEditable) {
        this.mAdapter.changeCursor(createCursor(this.mFilter.getText().toString()));
    }

    private boolean isClairOrLater() {
        return Build.VERSION.SDK_INT >= 5;
    }

    private Uri getContactUri() {
        if (isClairOrLater()) {
            return Uri.parse("content://com.android.contacts/contacts");
        }
        return Uri.parse("content://contacts/people");
    }

    private Cursor createCursor(String strParam) {
        String selection;
        if (strParam == null || strParam.length() <= 0) {
            selection = null;
        } else {
            selection = "(DISPLAY_NAME LIKE \"%" + strParam + "%\")";
        }
        return managedQuery(getContactUri(), new String[]{"_id", "custom_ringtone", "display_name", "last_time_contacted", "starred", "times_contacted"}, selection, null, "UPPER(DISPLAY_NAME) ASC");
    }

    /* access modifiers changed from: private */
    public void assignRingtone2Contact() {
        Cursor cursor = this.mAdapter.getCursor();
        String strID = cursor.getString(cursor.getColumnIndexOrThrow("_id"));
        String strDisplayname = cursor.getString(cursor.getColumnIndexOrThrow("display_name"));
        ContentValues localContentValues = new ContentValues();
        localContentValues.put("custom_ringtone", this.mRingUri.toString());
        if (isClairOrLater()) {
            getContentResolver().update(Uri.parse("content://com.android.contacts/contacts"), localContentValues, "_id=" + strID, null);
        } else {
            getContentResolver().update(Uri.withAppendedPath(getContactUri(), strID), localContentValues, null, null);
        }
        try {
            Toast.makeText(this, ((Object) getResources().getText(R.string.success_contact_ringtone)) + " " + strDisplayname, 1000).show();
        } catch (Exception e) {
        }
    }
}
