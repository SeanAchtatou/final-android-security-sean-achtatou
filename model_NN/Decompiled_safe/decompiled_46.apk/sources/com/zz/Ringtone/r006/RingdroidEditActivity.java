package com.zz.Ringtone.r006;

import android.app.Activity;

public class RingdroidEditActivity extends Activity {
    private static final int CMD_RESET = 2;
    private static final int CMD_SAVE = 1;
    private static final int CMD_USEALL = 3;
    private static final int REQUEST_CODE_CHOOSE_CONTACT = 2;
    private static final int REQUEST_CODE_RECORD = 1;
}
