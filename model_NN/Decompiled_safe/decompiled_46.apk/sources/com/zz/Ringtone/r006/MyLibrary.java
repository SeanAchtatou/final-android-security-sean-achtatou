package com.zz.Ringtone.r006;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import com.zzbook.util.FileUtil;
import com.zzbook.util.GenUtil;
import com.zzbook.util.MultiDownloadNew;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;

public class MyLibrary extends Activity {
    /* access modifiers changed from: private */
    public boolean bIndownload = false;
    private boolean bSearch = false;
    /* access modifiers changed from: private */
    public ProgressDialog dialog;
    private String mStrPath;
    /* access modifiers changed from: private */
    public ProgressDialog mStreamingProgressDlg;
    /* access modifiers changed from: private */
    public ProgressDialog mStreamingProgressDlgPrev;
    /* access modifiers changed from: private */
    public ArrayList<Thread> mThreadsList = new ArrayList<>();
    /* access modifiers changed from: private */
    public WebView mWebView;
    /* access modifiers changed from: private */
    public int miSetType = 0;
    /* access modifiers changed from: private */
    public MyHandler myHandler;
    /* access modifiers changed from: private */
    public MediaPlayer previewPlayer;
    /* access modifiers changed from: private */
    public MediaPlayer previewPlayerPrev;
    /* access modifiers changed from: private */
    public String strMP3File;
    /* access modifiers changed from: private */
    public ProgressDialog webloaddlg;

    public MyLibrary() {
        if (Environment.getExternalStorageState().equals("mounted")) {
            this.mStrPath = "/sdcard/popringtone/myRing/";
        } else {
            this.mStrPath = "/data/popringtone/myRing/";
        }
        this.myHandler = new MyHandler();
        this.previewPlayer = new MediaPlayer();
        this.previewPlayerPrev = new MediaPlayer();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.mylibrary);
        this.dialog = new ProgressDialog(this);
        this.dialog.setButton(getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                GenUtil.systemPrintln("keydown");
                boolean unused = MyLibrary.this.bIndownload = false;
                MyLibrary.this.stopAllThreads();
            }
        });
        Intent queryIntent = getIntent();
        String searchKeywords = null;
        if ("android.intent.action.SEARCH".equals(queryIntent.getAction())) {
            searchKeywords = queryIntent.getStringExtra("query");
        }
        this.webloaddlg = new ProgressDialog(this);
        this.mWebView = (WebView) findViewById(R.id.mylibraryweb);
        WebSettings localWebSettings = this.mWebView.getSettings();
        localWebSettings.setJavaScriptEnabled(true);
        localWebSettings.setSavePassword(false);
        localWebSettings.setSaveFormData(false);
        localWebSettings.setSupportZoom(true);
        this.webloaddlg.setMessage("web loading...");
        this.webloaddlg.show();
        if (searchKeywords != null) {
            this.bSearch = true;
            this.mWebView.loadUrl(getString(R.string.search_url) + searchKeywords);
        } else {
            this.bSearch = false;
            this.mWebView.loadUrl(getResources().getString(R.string.online_index_mb));
        }
        this.mWebView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (!url.startsWith(MyLibrary.this.getString(R.string.txt_domain))) {
                    MyLibrary.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
                    return true;
                }
                System.out.println(url);
                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {
                MyLibrary.this.webloaddlg.dismiss();
                super.onPageFinished(view, url);
            }
        });
        this.mWebView.addJavascriptInterface(new DemoJSInterface(), "apkshare");
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.apk_menu, menu);
        menu.add(0, 5, 0, (int) R.string.menu__search).setIcon((int) R.drawable.search);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem paramMenuItem) {
        super.onOptionsItemSelected(paramMenuItem);
        switch (paramMenuItem.getItemId()) {
            case RingdroidActivity.CMD_SEARCH /*5*/:
                onSearchRequested();
                return true;
            case R.id.refresh /*2131099685*/:
                this.mWebView.reload();
                return true;
            case R.id.home /*2131099686*/:
                this.mWebView.loadUrl(getString(R.string.online_index_mb));
                return true;
            default:
                return true;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        if (this.mWebView.canGoBack()) {
            this.mWebView.goBack();
            return true;
        } else if (this.bSearch) {
            this.bSearch = false;
            return super.onKeyDown(keyCode, event);
        } else {
            quitSystem();
            return true;
        }
    }

    public void quitSystem() {
        new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.txt_quit_title)).setMessage(getResources().getString(R.string.txt_quit_body)).setIcon((int) R.drawable.icon).setPositiveButton(getResources().getString(R.string.btn_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                MyLibrary.this.finish();
            }
        }).setNegativeButton(getResources().getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).show();
    }

    public boolean removeThread(Thread curThread) {
        return false;
    }

    public void stopAllThreads() {
        for (int iIndex = 0; iIndex < this.mThreadsList.size(); iIndex++) {
            MultiDownloadNew curThread = (MultiDownloadNew) this.mThreadsList.get(iIndex);
            if (curThread != null && curThread.isAlive()) {
                curThread.stopRun();
            }
        }
        this.mThreadsList.clear();
    }

    class MyHandler extends Handler {
        private static final int DISMISS_DIALOG = 7;
        private static final int DISMISS_PROGRESSBAR = 3;
        private static final String INFO = "info";
        private static final int NOTIFY_DATA = 1;
        private static final int SHOW_DIALOG = 6;
        private static final int SHOW_DOWNLOAD_PERCENT = 4;
        private static final int SHOW_MSG = 8;
        private static final int SHOW_PROGRESSBAR = 2;

        MyHandler() {
        }

        private void dismissMyDialog() {
            sendEmptyMessage(DISMISS_DIALOG);
        }

        private void notifyDataSetChanged() {
            Message localMessage = new Message();
            localMessage.what = 1;
            sendMessage(localMessage);
        }

        private void showMyDialog() {
            sendEmptyMessage(6);
        }

        public void handleMessage(Message paramMessage) {
            GenUtil.systemPrintln("paramMessage.what: " + paramMessage.what);
            switch (paramMessage.what) {
                case -2:
                    MyLibrary.this.dialog.dismiss();
                    break;
                case 1:
                    if (MyLibrary.this.dialog != null) {
                        MyLibrary.this.dialog.dismiss();
                        Toast toast = Toast.makeText(MyLibrary.this, (int) R.string.tip_download_end, 2000);
                        toast.setGravity(80, 0, 0);
                        toast.show();
                        MyLibrary.this.DownloadFinished();
                        break;
                    }
                    break;
                case 6:
                    GenUtil.systemPrintln(" show called 1 ");
                    if (MyLibrary.this.dialog != null && MyLibrary.this.bIndownload) {
                        String strMsg = (String) paramMessage.obj;
                        if (strMsg == null) {
                            strMsg = "0%";
                        }
                        GenUtil.systemPrintln(" show called 2 ");
                        MyLibrary.this.dialog.setMessage("Loading " + strMsg);
                        MyLibrary.this.dialog.show();
                        break;
                    }
                case DISMISS_DIALOG /*7*/:
                    GenUtil.systemPrintln(" dismiss called 1 ");
                    if (MyLibrary.this.dialog != null) {
                        GenUtil.systemPrintln(" dismiss called 2");
                        MyLibrary.this.dialog.dismiss();
                        break;
                    }
                    break;
                case 11:
                    MyLibrary.this.mWebView.loadUrl(MyLibrary.this.getString(R.string.search_url) + ((String) paramMessage.obj));
                    break;
                case 20:
                    MyLibrary.this.DownloadFinished();
                    break;
            }
            super.handleMessage(paramMessage);
        }
    }

    class QueryImgTask extends AsyncTask {
        QueryImgTask() {
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(int iPercent) {
            if (!MyLibrary.this.bIndownload) {
                MyLibrary.this.dialog.dismiss();
                return;
            }
            Message m = new Message();
            m.what = 6;
            m.obj = iPercent + "%";
            MyLibrary.this.myHandler.sendMessageDelayed(m, 0);
        }

        /* access modifiers changed from: protected */
        public Object doInBackground(Object[] arrParams) {
            String bookFolderTMP;
            boolean unused = MyLibrary.this.bIndownload = true;
            if (Environment.getExternalStorageState().equals("mounted")) {
                bookFolderTMP = "/sdcard/popringtone/temp/";
            } else {
                bookFolderTMP = "/data/popringtone/temp/";
            }
            int startThread = Integer.parseInt((String) arrParams[2]);
            System.out.println((String) arrParams[0]);
            System.out.println((String) arrParams[1]);
            System.out.println(bookFolderTMP);
            final String strObj = (String) arrParams[1];
            final MultiDownloadNew multiDownload = new MultiDownloadNew(startThread, (String) arrParams[0], (String) arrParams[1], bookFolderTMP);
            MyLibrary.this.mThreadsList.add(multiDownload);
            multiDownload.start();
            MyLibrary.this.myHandler.sendEmptyMessage(6);
            new Thread(new Runnable() {
                public void run() {
                    long time = new Date().getTime();
                    while (multiDownload.getPercntInt() < 100 && MyLibrary.this.bIndownload) {
                        try {
                            Thread.sleep(200);
                            QueryImgTask.this.onProgressUpdate(multiDownload.getPercntInt());
                        } catch (InterruptedException e) {
                            Message m1 = new Message();
                            m1.what = -1;
                            MyLibrary.this.myHandler.sendMessage(m1);
                        }
                    }
                    Message m = new Message();
                    if (multiDownload.getPercntInt() != 100 || !MyLibrary.this.bIndownload) {
                        m.what = -2;
                        MyLibrary.this.dialog.dismiss();
                    } else {
                        m.what = 1;
                        m.obj = strObj;
                    }
                    MyLibrary.this.myHandler.sendMessage(m);
                }
            }).start();
            return true;
        }
    }

    class DemoJSInterface {
        DemoJSInterface() {
        }

        public void audit(String strParam) {
            System.out.println("-----audit----------------------");
            System.out.println(strParam);
            System.out.println("-----audit----------------------");
            GenUtil.systemPrint("-----audit---------------------- " + strParam);
            String unused = MyLibrary.this.strMP3File = strParam;
            if (MyLibrary.this.mStreamingProgressDlg == null) {
                ProgressDialog unused2 = MyLibrary.this.mStreamingProgressDlg = new ProgressDialog(MyLibrary.this);
                MyLibrary.this.mStreamingProgressDlg.setMessage(MyLibrary.this.getString(R.string.mStreaming_message));
                MyLibrary.this.mStreamingProgressDlg.setIndeterminate(true);
                MyLibrary.this.mStreamingProgressDlg.setCancelable(true);
                MyLibrary.this.mStreamingProgressDlg.setButton(MyLibrary.this.getString(R.string.stop), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDlgInterface, int paramInt) {
                        MyLibrary.this.previewPlayer.stop();
                        MyLibrary.this.previewPlayer.reset();
                        GenUtil.systemPrint("audit onClick");
                    }
                });
            }
            MyLibrary.this.mStreamingProgressDlg.setTitle(strParam.substring(strParam.lastIndexOf(47) + 1));
            MyLibrary.this.mStreamingProgressDlg.show();
            new Thread(new Runnable() {
                public void run() {
                    try {
                        GenUtil.systemPrint("strMP3File = " + MyLibrary.this.strMP3File);
                        MyLibrary.this.previewPlayer.setDataSource(MyLibrary.this.strMP3File);
                        MyLibrary.this.previewPlayer.prepare();
                        MyLibrary.this.previewPlayer.start();
                        MyLibrary.this.previewPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            public void onCompletion(MediaPlayer paramMediaPlayer) {
                                try {
                                    MyLibrary.this.mStreamingProgressDlg.dismiss();
                                    MyLibrary.this.previewPlayer.reset();
                                    GenUtil.systemPrint("audit onCompletion called");
                                } catch (Exception e) {
                                }
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }

        public void download(String strParam) {
            String strMP3Local;
            GenUtil.systemPrintln("-----download----------------------");
            GenUtil.systemPrintln(strParam);
            GenUtil.systemPrintln("-----download----------------------");
            QueryImgTask task = new QueryImgTask();
            Object[] arrObjects = new Object[3];
            arrObjects[0] = strParam;
            if (Environment.getExternalStorageState().equals("mounted")) {
                strMP3Local = "/sdcard/popringtone/myRing/";
            } else {
                strMP3Local = "/data/popringtone/myRing/";
            }
            String strFirstHalf = strParam.substring(0, strParam.lastIndexOf(47));
            String strMP3Local2 = strMP3Local + strParam.substring(strParam.lastIndexOf(47) + 1).replace(".", "_" + strFirstHalf.hashCode() + ".");
            String unused = MyLibrary.this.strMP3File = strMP3Local2;
            if (FileUtil.fileExist(MyLibrary.this.strMP3File)) {
                MyLibrary.this.DownloadFinished();
                return;
            }
            arrObjects[1] = strMP3Local2;
            arrObjects[2] = String.valueOf(3);
            Message m = new Message();
            m.what = 6;
            m.obj = "0%";
            MyLibrary.this.myHandler.sendMessageDelayed(m, 0);
            task.execute(arrObjects);
        }
    }

    public void DownloadFinished() {
        sendBroadcast(new Intent("android.intent.action.MEDIA_MOUNTED", Uri.parse("file://" + Environment.getExternalStorageDirectory())));
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(this).setTitle(this.strMP3File.substring(this.strMP3File.lastIndexOf(47) + 1).replaceAll("_", ""));
        localBuilder.setItems((int) R.array.local_ring_option, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                switch (paramInt) {
                    case 0:
                        MyLibrary.this.onCreateDialog(1).show();
                        return;
                    case 1:
                        GenUtil.systemPrint("MyLibrary.this.strMP3File = " + MyLibrary.this.strMP3File);
                        MyLibrary.this.assingToContact(MyLibrary.this.strMP3File);
                        return;
                    case MainActivity.MENU_UNSETRINGTONE:
                        MyLibrary.this.confirmDelete();
                        return;
                    case 3:
                        if (MyLibrary.this.mStreamingProgressDlgPrev == null) {
                            ProgressDialog unused = MyLibrary.this.mStreamingProgressDlgPrev = new ProgressDialog(MyLibrary.this);
                            MyLibrary.this.mStreamingProgressDlgPrev.setTitle(MyLibrary.this.strMP3File.substring(MyLibrary.this.strMP3File.lastIndexOf(47) + 1).replaceAll("_", ""));
                            MyLibrary.this.mStreamingProgressDlgPrev.setIndeterminate(true);
                            MyLibrary.this.mStreamingProgressDlgPrev.setCancelable(true);
                            MyLibrary.this.mStreamingProgressDlgPrev.setButton(MyLibrary.this.getString(R.string.stop), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface paramDlgInterface, int paramInt) {
                                    MyLibrary.this.previewPlayerPrev.stop();
                                    MyLibrary.this.previewPlayerPrev.reset();
                                    Message m = new Message();
                                    m.what = 20;
                                    MyLibrary.this.myHandler.sendMessageDelayed(m, 0);
                                }
                            });
                        }
                        MyLibrary.this.mStreamingProgressDlgPrev.setTitle(MyLibrary.this.strMP3File.substring(MyLibrary.this.strMP3File.lastIndexOf(47) + 1).replaceAll("_", ""));
                        MyLibrary.this.mStreamingProgressDlgPrev.show();
                        new Thread(new Runnable() {
                            public void run() {
                                try {
                                    MyLibrary.this.previewPlayerPrev.setDataSource(MyLibrary.this.strMP3File);
                                    MyLibrary.this.previewPlayerPrev.prepare();
                                    MyLibrary.this.previewPlayerPrev.start();
                                    MyLibrary.this.previewPlayerPrev.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                        public void onCompletion(MediaPlayer paramMediaPlayer) {
                                            try {
                                                MyLibrary.this.mStreamingProgressDlgPrev.dismiss();
                                                MyLibrary.this.previewPlayerPrev.reset();
                                                Message m = new Message();
                                                m.what = 20;
                                                MyLibrary.this.myHandler.sendMessageDelayed(m, 0);
                                            } catch (Exception e) {
                                            }
                                        }
                                    });
                                } catch (Exception e) {
                                }
                            }
                        }).start();
                        return;
                    default:
                        return;
                }
            }
        });
        localBuilder.create().show();
    }

    /* access modifiers changed from: protected */
    public AlertDialog onCreateDialog(int iParam) {
        if (iParam != 1) {
            return null;
        }
        this.miSetType = 0;
        AlertDialog.Builder builder = new AlertDialog.Builder(this).setIcon(17301543).setTitle((int) R.string.ring_picker_title);
        builder.setSingleChoiceItems((int) R.array.ring_types, 0, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                int unused = MyLibrary.this.miSetType = paramInt;
            }
        }).setPositiveButton((int) R.string.alertdialog_ok, new DialogInterface.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                switch (MyLibrary.this.miSetType) {
                    case 0:
                        Log.v("zxl", "MyLibrary.this.strMP3File = " + MyLibrary.this.strMP3File);
                        File file = new File(MyLibrary.this.strMP3File);
                        if (!file.isFile() || !file.exists()) {
                            Toast.makeText(MyLibrary.this, (int) R.string.tip_set_ring_failed, 1000).show();
                            return;
                        }
                        Log.v("zxl", "MyLibrary.this.strMP3File = " + MyLibrary.this.strMP3File);
                        ContentValues values = new ContentValues();
                        values.put("_data", file.getAbsolutePath());
                        values.put("title", file.getName());
                        values.put("_size", Long.valueOf(file.length()));
                        values.put("mime_type", "audio/mp3");
                        values.put("is_ringtone", (Boolean) true);
                        values.put("is_notification", (Boolean) false);
                        values.put("is_alarm", (Boolean) false);
                        values.put("is_music", (Boolean) false);
                        Uri uri = MediaStore.Audio.Media.getContentUriForPath(file.getAbsolutePath());
                        MyLibrary.this.getContentResolver().delete(uri, "_data=\"" + file.getAbsolutePath() + "\"", null);
                        RingtoneManager.setActualDefaultRingtoneUri(MyLibrary.this, 1, MyLibrary.this.getContentResolver().insert(MediaStore.Audio.Media.getContentUriForPath(file.getAbsolutePath()), values));
                        Toast.makeText(MyLibrary.this, (int) R.string.tip_set_ring_success, 1000).show();
                        return;
                    case 1:
                        File file2 = new File(MyLibrary.this.strMP3File);
                        if (!file2.isFile() || !file2.exists()) {
                            Toast.makeText(MyLibrary.this, (int) R.string.tip_set_notification_failed, 1000).show();
                            return;
                        }
                        ContentValues values2 = new ContentValues();
                        values2.put("_data", file2.getAbsolutePath());
                        values2.put("title", file2.getName());
                        values2.put("_size", Long.valueOf(file2.length()));
                        values2.put("mime_type", "audio/mp3");
                        values2.put("is_ringtone", (Boolean) false);
                        values2.put("is_notification", (Boolean) true);
                        values2.put("is_alarm", (Boolean) false);
                        values2.put("is_music", (Boolean) false);
                        Uri uri2 = MediaStore.Audio.Media.getContentUriForPath(file2.getAbsolutePath());
                        MyLibrary.this.getContentResolver().delete(uri2, "_data=\"" + file2.getAbsolutePath() + "\"", null);
                        RingtoneManager.setActualDefaultRingtoneUri(MyLibrary.this, 2, MyLibrary.this.getContentResolver().insert(MediaStore.Audio.Media.getContentUriForPath(file2.getAbsolutePath()), values2));
                        Toast.makeText(MyLibrary.this, (int) R.string.tip_set_notification_success, 1000).show();
                        return;
                    case MainActivity.MENU_UNSETRINGTONE:
                        File file3 = new File(MyLibrary.this.strMP3File);
                        if (!file3.isFile() || !file3.exists()) {
                            Toast.makeText(MyLibrary.this, (int) R.string.tip_set_alarm_failed, 1000).show();
                            return;
                        }
                        ContentValues values3 = new ContentValues();
                        values3.put("_data", file3.getAbsolutePath());
                        values3.put("title", file3.getName());
                        values3.put("_size", Long.valueOf(file3.length()));
                        values3.put("mime_type", "audio/mp3");
                        values3.put("is_ringtone", (Boolean) false);
                        values3.put("is_notification", (Boolean) false);
                        values3.put("is_alarm", (Boolean) true);
                        values3.put("is_music", (Boolean) false);
                        Uri uri3 = MediaStore.Audio.Media.getContentUriForPath(file3.getAbsolutePath());
                        MyLibrary.this.getContentResolver().delete(uri3, "_data=\"" + file3.getAbsolutePath() + "\"", null);
                        RingtoneManager.setActualDefaultRingtoneUri(MyLibrary.this, 4, MyLibrary.this.getContentResolver().insert(MediaStore.Audio.Media.getContentUriForPath(file3.getAbsolutePath()), values3));
                        Toast.makeText(MyLibrary.this, (int) R.string.tip_set_alarm_success, 1000).show();
                        return;
                    default:
                        return;
                }
            }
        }).setNegativeButton((int) R.string.alertdialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
            }
        });
        return builder.create();
    }

    /* access modifiers changed from: private */
    public void confirmDelete() {
        new AlertDialog.Builder(this).setTitle((int) R.string.delete_ringtone).setMessage((int) R.string.confirm_delete_ringdroid).setPositiveButton((int) R.string.delete_ok_button, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                try {
                    String strDir = MyLibrary.this.strMP3File;
                    if (strDir.length() > 0) {
                        File file = new File(strDir);
                        try {
                            if (!file.isFile() || !file.exists()) {
                                Toast.makeText(MyLibrary.this, (int) R.string.tip_delete_txt, 1000).show();
                                return;
                            }
                            file.delete();
                            Toast.makeText(MyLibrary.this, (int) R.string.tip_delete_success, 1000).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(MyLibrary.this, (int) R.string.tip_delete_txt, 1000).show();
                        }
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }).setNegativeButton((int) R.string.delete_cancel_button, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
            }
        }).setCancelable(true).show();
    }

    private void handleIntent(Intent intent) {
        if ("android.intent.action.SEARCH".equals(intent.getAction())) {
            String strQuery = intent.getStringExtra("query");
            Message m = new Message();
            m.what = 11;
            m.obj = getString(R.string.search_url) + strQuery;
            this.myHandler.sendMessageDelayed(m, 0);
        }
    }

    public boolean onSearchRequested() {
        startSearch("search", true, null, false);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent paramIntent) {
        GenUtil.systemPrintln("MyLibrary.onNewIntent invoked!");
        setIntent(paramIntent);
        handleIntent(paramIntent);
    }

    public void assingToContact(String strFileInput) {
        Intent intent = new Intent();
        Cursor cursor = getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, null, "_data like ?", new String[]{"%" + strFileInput}, null);
        GenUtil.systemPrint("strFile = " + strFileInput);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            int id = cursor.getInt(0);
            cursor.close();
            intent.setData(Uri.withAppendedPath(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, id + ""));
            intent.setClass(this, ChooseContactActivity.class);
            startActivity(intent);
            return;
        }
        Toast.makeText(this, "error", 1000).show();
        GenUtil.systemPrint("error");
    }
}
