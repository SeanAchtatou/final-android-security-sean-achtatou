package defpackage;

import android.os.AsyncTask;
import android.webkit.WebView;
import com.google.ads.AdRequest;
import com.google.ads.InterstitialAd;

/*  JADX ERROR: NullPointerException in pass: ExtractFieldInit
    java.lang.NullPointerException
    	at jadx.core.utils.BlockUtils.isAllBlocksEmpty(BlockUtils.java:608)
    	at jadx.core.dex.visitors.ExtractFieldInit.getConstructorsList(ExtractFieldInit.java:241)
    	at jadx.core.dex.visitors.ExtractFieldInit.moveCommonFieldsInit(ExtractFieldInit.java:122)
    	at jadx.core.dex.visitors.ExtractFieldInit.visit(ExtractFieldInit.java:43)
    */
/* renamed from: c  reason: default package */
public final class c extends AsyncTask<AdRequest, String, AdRequest.ErrorCode> {
    private static final String a;
    private static final String b = ("<html><head><script src=\"" + a + "\"></script><script>");
    private static final Object c = new Object();
    private static long d = 5000;
    private String e;
    private String f;
    private b g;
    private d h;
    private WebView i;
    private String j;
    private AdRequest.ErrorCode k;
    private boolean l;
    private boolean m;

    /* renamed from: c$a */
    private class a extends Exception {
        public a(String str) {
            super(str);
        }
    }

    static {
        String[] split = AdRequest.VERSION.split("\\.");
        String str = split[0];
        a = "http://www.gstatic.com/afma/sdk-core-v" + str + split[1] + ".js";
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: android.app.Activity.getApplicationContext():android.content.Context in method: c.<init>(d):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at java.base/java.util.ArrayList.forEach(ArrayList.java:1540)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:59)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: android.app.Activity.getApplicationContext():android.content.Context
        	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
        	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
        	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:540)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 6 more
        */
    c(defpackage.d r1) {
        /*
            r4 = this;
            r3 = 0
            r1 = 0
            r4.<init>()
            r4.h = r5
            r4.j = r1
            r4.f = r1
            r4.k = r1
            r4.l = r3
            r4.m = r3
            android.app.Activity r0 = r5.c()
            if (r0 == 0) goto L_0x0046
            android.webkit.WebView r1 = new android.webkit.WebView
            android.content.Context r0 = r0.getApplicationContext()
            r1.<init>(r0)
            r4.i = r1
            android.webkit.WebView r0 = r4.i
            android.webkit.WebSettings r0 = r0.getSettings()
            r1 = 1
            r0.setJavaScriptEnabled(r1)
            android.webkit.WebView r0 = r4.i
            h r1 = new h
            a$a r2 = defpackage.a.C0000a.URL_REQUEST_TYPE
            r1.<init>(r5, r2, r3)
            r0.setWebViewClient(r1)
            b r0 = new b
            android.webkit.WebView r1 = r4.i
            java.lang.String r1 = defpackage.u.a(r1)
            r0.<init>(r4, r5, r1)
            r4.g = r0
        L_0x0045:
            return
        L_0x0046:
            r4.i = r1
            r4.g = r1
            java.lang.String r0 = "activity was null while trying to create an AdLoader."
            defpackage.t.e(r0)
            goto L_0x0045
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c.<init>(d):void");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.ads.AdRequest.ErrorCode doInBackground(com.google.ads.AdRequest... r14) {
        /*
            r13 = this;
            r12 = 0
            r10 = 0
            java.lang.String r0 = "format"
            monitor-enter(r13)
            android.webkit.WebView r0 = r13.i     // Catch:{ all -> 0x002b }
            if (r0 == 0) goto L_0x000e
            b r0 = r13.g     // Catch:{ all -> 0x002b }
            if (r0 != 0) goto L_0x0017
        L_0x000e:
            java.lang.String r0 = "adRequestWebView was null while trying to load an ad."
            defpackage.t.e(r0)     // Catch:{ all -> 0x002b }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.INTERNAL_ERROR     // Catch:{ all -> 0x002b }
            monitor-exit(r13)     // Catch:{ all -> 0x002b }
        L_0x0016:
            return r0
        L_0x0017:
            r0 = 0
            r0 = r14[r0]     // Catch:{ all -> 0x002b }
            d r1 = r13.h     // Catch:{ all -> 0x002b }
            android.app.Activity r1 = r1.c()     // Catch:{ all -> 0x002b }
            if (r1 != 0) goto L_0x002e
            java.lang.String r0 = "activity was null while forming an ad request."
            defpackage.t.e(r0)     // Catch:{ all -> 0x002b }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.INTERNAL_ERROR     // Catch:{ all -> 0x002b }
            monitor-exit(r13)     // Catch:{ all -> 0x002b }
            goto L_0x0016
        L_0x002b:
            r0 = move-exception
            monitor-exit(r13)
            throw r0
        L_0x002e:
            android.content.Context r2 = r1.getApplicationContext()     // Catch:{ a -> 0x0116 }
            java.util.Map r0 = r0.getRequestMap()     // Catch:{ a -> 0x0116 }
            d r3 = r13.h     // Catch:{ a -> 0x0116 }
            f r3 = r3.j()     // Catch:{ a -> 0x0116 }
            long r4 = r3.h()     // Catch:{ a -> 0x0116 }
            int r6 = (r4 > r10 ? 1 : (r4 == r10 ? 0 : -1))
            if (r6 <= 0) goto L_0x004d
            java.lang.String r6 = "prl"
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ a -> 0x0116 }
            r0.put(r6, r4)     // Catch:{ a -> 0x0116 }
        L_0x004d:
            java.lang.String r4 = r3.g()     // Catch:{ a -> 0x0116 }
            if (r4 == 0) goto L_0x0058
            java.lang.String r5 = "ppcl"
            r0.put(r5, r4)     // Catch:{ a -> 0x0116 }
        L_0x0058:
            java.lang.String r4 = r3.f()     // Catch:{ a -> 0x0116 }
            if (r4 == 0) goto L_0x0063
            java.lang.String r5 = "pcl"
            r0.put(r5, r4)     // Catch:{ a -> 0x0116 }
        L_0x0063:
            long r4 = r3.e()     // Catch:{ a -> 0x0116 }
            int r6 = (r4 > r10 ? 1 : (r4 == r10 ? 0 : -1))
            if (r6 <= 0) goto L_0x0074
            java.lang.String r6 = "pcc"
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ a -> 0x0116 }
            r0.put(r6, r4)     // Catch:{ a -> 0x0116 }
        L_0x0074:
            java.lang.String r4 = "preqs"
            long r5 = defpackage.f.i()     // Catch:{ a -> 0x0116 }
            java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ a -> 0x0116 }
            r0.put(r4, r5)     // Catch:{ a -> 0x0116 }
            java.lang.String r4 = r3.j()     // Catch:{ a -> 0x0116 }
            if (r4 == 0) goto L_0x008c
            java.lang.String r5 = "pai"
            r0.put(r5, r4)     // Catch:{ a -> 0x0116 }
        L_0x008c:
            boolean r4 = r3.k()     // Catch:{ a -> 0x0116 }
            if (r4 == 0) goto L_0x0099
            java.lang.String r4 = "aoi_timeout"
            java.lang.String r5 = "true"
            r0.put(r4, r5)     // Catch:{ a -> 0x0116 }
        L_0x0099:
            boolean r4 = r3.m()     // Catch:{ a -> 0x0116 }
            if (r4 == 0) goto L_0x00a6
            java.lang.String r4 = "aoi_nofill"
            java.lang.String r5 = "true"
            r0.put(r4, r5)     // Catch:{ a -> 0x0116 }
        L_0x00a6:
            java.lang.String r4 = r3.p()     // Catch:{ a -> 0x0116 }
            if (r4 == 0) goto L_0x00b1
            java.lang.String r5 = "pit"
            r0.put(r5, r4)     // Catch:{ a -> 0x0116 }
        L_0x00b1:
            r3.a()     // Catch:{ a -> 0x0116 }
            r3.d()     // Catch:{ a -> 0x0116 }
            d r3 = r13.h     // Catch:{ a -> 0x0116 }
            com.google.ads.Ad r3 = r3.d()     // Catch:{ a -> 0x0116 }
            boolean r3 = r3 instanceof com.google.ads.InterstitialAd     // Catch:{ a -> 0x0116 }
            if (r3 == 0) goto L_0x0121
            java.lang.String r3 = "format"
            java.lang.String r4 = "interstitial_mb"
            r0.put(r3, r4)     // Catch:{ a -> 0x0116 }
        L_0x00c8:
            java.lang.String r3 = "slotname"
            d r4 = r13.h     // Catch:{ a -> 0x0116 }
            java.lang.String r4 = r4.f()     // Catch:{ a -> 0x0116 }
            r0.put(r3, r4)     // Catch:{ a -> 0x0116 }
            java.lang.String r3 = "js"
            java.lang.String r4 = "afma-sdk-a-v4.0.4"
            r0.put(r3, r4)     // Catch:{ a -> 0x0116 }
            java.lang.String r3 = "msid"
            java.lang.String r4 = r2.getPackageName()     // Catch:{ a -> 0x0116 }
            r0.put(r3, r4)     // Catch:{ a -> 0x0116 }
            java.lang.String r3 = "app_name"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ a -> 0x0116 }
            r4.<init>()     // Catch:{ a -> 0x0116 }
            java.lang.String r5 = "4.0.4.android."
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ a -> 0x0116 }
            java.lang.String r5 = r2.getPackageName()     // Catch:{ a -> 0x0116 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ a -> 0x0116 }
            java.lang.String r4 = r4.toString()     // Catch:{ a -> 0x0116 }
            r0.put(r3, r4)     // Catch:{ a -> 0x0116 }
            java.lang.String r3 = "isu"
            java.lang.String r4 = defpackage.u.a(r2)     // Catch:{ a -> 0x0116 }
            r0.put(r3, r4)     // Catch:{ a -> 0x0116 }
            java.lang.String r3 = defpackage.u.c(r2)     // Catch:{ a -> 0x0116 }
            if (r3 != 0) goto L_0x0159
            c$a r0 = new c$a     // Catch:{ a -> 0x0116 }
            java.lang.String r1 = "NETWORK_ERROR"
            r0.<init>(r1)     // Catch:{ a -> 0x0116 }
            throw r0     // Catch:{ a -> 0x0116 }
        L_0x0116:
            r0 = move-exception
            java.lang.String r0 = "Unable to connect to network."
            defpackage.t.c(r0)     // Catch:{ all -> 0x002b }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.NETWORK_ERROR     // Catch:{ all -> 0x002b }
            monitor-exit(r13)     // Catch:{ all -> 0x002b }
            goto L_0x0016
        L_0x0121:
            d r3 = r13.h     // Catch:{ a -> 0x0116 }
            com.google.ads.AdSize r3 = r3.i()     // Catch:{ a -> 0x0116 }
            java.lang.String r4 = r3.toString()     // Catch:{ a -> 0x0116 }
            if (r4 == 0) goto L_0x0133
            java.lang.String r3 = "format"
            r0.put(r3, r4)     // Catch:{ a -> 0x0116 }
            goto L_0x00c8
        L_0x0133:
            java.util.HashMap r4 = new java.util.HashMap     // Catch:{ a -> 0x0116 }
            r4.<init>()     // Catch:{ a -> 0x0116 }
            java.lang.String r5 = "w"
            int r6 = r3.getWidth()     // Catch:{ a -> 0x0116 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ a -> 0x0116 }
            r4.put(r5, r6)     // Catch:{ a -> 0x0116 }
            java.lang.String r5 = "h"
            int r3 = r3.getHeight()     // Catch:{ a -> 0x0116 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ a -> 0x0116 }
            r4.put(r5, r3)     // Catch:{ a -> 0x0116 }
            java.lang.String r3 = "ad_frame"
            r0.put(r3, r4)     // Catch:{ a -> 0x0116 }
            goto L_0x00c8
        L_0x0159:
            java.lang.String r4 = "net"
            r0.put(r4, r3)     // Catch:{ a -> 0x0116 }
            java.lang.String r3 = defpackage.u.d(r2)     // Catch:{ a -> 0x0116 }
            if (r3 == 0) goto L_0x016f
            int r4 = r3.length()     // Catch:{ a -> 0x0116 }
            if (r4 == 0) goto L_0x016f
            java.lang.String r4 = "cap"
            r0.put(r4, r3)     // Catch:{ a -> 0x0116 }
        L_0x016f:
            java.lang.String r3 = "u_audio"
            u$a r4 = defpackage.u.e(r2)     // Catch:{ a -> 0x0116 }
            int r4 = r4.ordinal()     // Catch:{ a -> 0x0116 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ a -> 0x0116 }
            r0.put(r3, r4)     // Catch:{ a -> 0x0116 }
            java.lang.String r3 = "u_so"
            java.lang.String r2 = defpackage.u.f(r2)     // Catch:{ a -> 0x0116 }
            r0.put(r3, r2)     // Catch:{ a -> 0x0116 }
            android.util.DisplayMetrics r1 = defpackage.u.a(r1)     // Catch:{ a -> 0x0116 }
            java.lang.String r2 = "u_sd"
            float r3 = r1.density     // Catch:{ a -> 0x0116 }
            java.lang.Float r3 = java.lang.Float.valueOf(r3)     // Catch:{ a -> 0x0116 }
            r0.put(r2, r3)     // Catch:{ a -> 0x0116 }
            java.lang.String r2 = "u_h"
            int r3 = r1.heightPixels     // Catch:{ a -> 0x0116 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ a -> 0x0116 }
            r0.put(r2, r3)     // Catch:{ a -> 0x0116 }
            java.lang.String r2 = "u_w"
            int r1 = r1.widthPixels     // Catch:{ a -> 0x0116 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ a -> 0x0116 }
            r0.put(r2, r1)     // Catch:{ a -> 0x0116 }
            java.lang.String r1 = "hl"
            java.util.Locale r2 = java.util.Locale.getDefault()     // Catch:{ a -> 0x0116 }
            java.lang.String r2 = r2.getLanguage()     // Catch:{ a -> 0x0116 }
            r0.put(r1, r2)     // Catch:{ a -> 0x0116 }
            boolean r1 = defpackage.u.a()     // Catch:{ a -> 0x0116 }
            if (r1 == 0) goto L_0x01cb
            java.lang.String r1 = "simulator"
            r2 = 1
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ a -> 0x0116 }
            r0.put(r1, r2)     // Catch:{ a -> 0x0116 }
        L_0x01cb:
            com.google.gson.Gson r1 = new com.google.gson.Gson     // Catch:{ a -> 0x0116 }
            r1.<init>()     // Catch:{ a -> 0x0116 }
            java.lang.String r0 = r1.toJson(r0)     // Catch:{ a -> 0x0116 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ a -> 0x0116 }
            r1.<init>()     // Catch:{ a -> 0x0116 }
            java.lang.String r2 = defpackage.c.b     // Catch:{ a -> 0x0116 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ a -> 0x0116 }
            java.lang.String r2 = "AFMA_buildAdURL"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ a -> 0x0116 }
            java.lang.String r2 = "("
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ a -> 0x0116 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ a -> 0x0116 }
            java.lang.String r1 = ");"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ a -> 0x0116 }
            java.lang.String r1 = "</script></head><body></body></html>"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ a -> 0x0116 }
            java.lang.String r2 = r0.toString()     // Catch:{ a -> 0x0116 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ a -> 0x0116 }
            r0.<init>()     // Catch:{ a -> 0x0116 }
            java.lang.String r1 = "adRequestUrlHtml: "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ a -> 0x0116 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ a -> 0x0116 }
            java.lang.String r0 = r0.toString()     // Catch:{ a -> 0x0116 }
            defpackage.t.c(r0)     // Catch:{ a -> 0x0116 }
            android.webkit.WebView r0 = r13.i     // Catch:{ all -> 0x002b }
            r1 = 0
            java.lang.String r3 = "text/html"
            java.lang.String r4 = "utf-8"
            r5 = 0
            r0.loadDataWithBaseURL(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x002b }
            long r6 = r13.b()     // Catch:{ all -> 0x002b }
            long r8 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x002b }
            int r0 = (r6 > r10 ? 1 : (r6 == r10 ? 0 : -1))
            if (r0 <= 0) goto L_0x022f
            r13.wait(r6)     // Catch:{ InterruptedException -> 0x0238 }
        L_0x022f:
            com.google.ads.AdRequest$ErrorCode r0 = r13.k     // Catch:{ all -> 0x002b }
            if (r0 == 0) goto L_0x0254
            com.google.ads.AdRequest$ErrorCode r0 = r13.k     // Catch:{ all -> 0x002b }
            monitor-exit(r13)     // Catch:{ all -> 0x002b }
            goto L_0x0016
        L_0x0238:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x002b }
            r1.<init>()     // Catch:{ all -> 0x002b }
            java.lang.String r2 = "AdLoader InterruptedException while getting the URL: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x002b }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ all -> 0x002b }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x002b }
            defpackage.t.e(r0)     // Catch:{ all -> 0x002b }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.INTERNAL_ERROR     // Catch:{ all -> 0x002b }
            monitor-exit(r13)     // Catch:{ all -> 0x002b }
            goto L_0x0016
        L_0x0254:
            java.lang.String r0 = r13.j     // Catch:{ all -> 0x002b }
            if (r0 != 0) goto L_0x0262
            java.lang.String r0 = "AdLoader timed out while getting the URL."
            defpackage.t.c(r0)     // Catch:{ all -> 0x002b }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.NETWORK_ERROR     // Catch:{ all -> 0x002b }
            monitor-exit(r13)     // Catch:{ all -> 0x002b }
            goto L_0x0016
        L_0x0262:
            r0 = 1
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ all -> 0x002b }
            r1 = 0
            java.lang.String r2 = r13.j     // Catch:{ all -> 0x002b }
            r0[r1] = r2     // Catch:{ all -> 0x002b }
            r13.publishProgress(r0)     // Catch:{ all -> 0x002b }
            long r0 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x002b }
            long r0 = r0 - r8
            long r0 = r6 - r0
            int r2 = (r0 > r10 ? 1 : (r0 == r10 ? 0 : -1))
            if (r2 <= 0) goto L_0x027b
            r13.wait(r0)     // Catch:{ InterruptedException -> 0x0284 }
        L_0x027b:
            com.google.ads.AdRequest$ErrorCode r0 = r13.k     // Catch:{ all -> 0x002b }
            if (r0 == 0) goto L_0x02a0
            com.google.ads.AdRequest$ErrorCode r0 = r13.k     // Catch:{ all -> 0x002b }
            monitor-exit(r13)     // Catch:{ all -> 0x002b }
            goto L_0x0016
        L_0x0284:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x002b }
            r1.<init>()     // Catch:{ all -> 0x002b }
            java.lang.String r2 = "AdLoader InterruptedException while getting the HTML: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x002b }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ all -> 0x002b }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x002b }
            defpackage.t.e(r0)     // Catch:{ all -> 0x002b }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.INTERNAL_ERROR     // Catch:{ all -> 0x002b }
            monitor-exit(r13)     // Catch:{ all -> 0x002b }
            goto L_0x0016
        L_0x02a0:
            java.lang.String r0 = r13.f     // Catch:{ all -> 0x002b }
            if (r0 != 0) goto L_0x02ae
            java.lang.String r0 = "AdLoader timed out while getting the HTML."
            defpackage.t.c(r0)     // Catch:{ all -> 0x002b }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.NETWORK_ERROR     // Catch:{ all -> 0x002b }
            monitor-exit(r13)     // Catch:{ all -> 0x002b }
            goto L_0x0016
        L_0x02ae:
            d r0 = r13.h     // Catch:{ all -> 0x002b }
            g r0 = r0.g()     // Catch:{ all -> 0x002b }
            d r1 = r13.h     // Catch:{ all -> 0x002b }
            h r1 = r1.h()     // Catch:{ all -> 0x002b }
            r1.a()     // Catch:{ all -> 0x002b }
            java.lang.String r1 = r13.e     // Catch:{ all -> 0x002b }
            java.lang.String r2 = r13.f     // Catch:{ all -> 0x002b }
            java.lang.String r3 = "text/html"
            java.lang.String r4 = "utf-8"
            r5 = 0
            r0.loadDataWithBaseURL(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x002b }
            long r1 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x002b }
            long r1 = r1 - r8
            long r1 = r6 - r1
            int r3 = (r1 > r10 ? 1 : (r1 == r10 ? 0 : -1))
            if (r3 <= 0) goto L_0x02d7
            r13.wait(r1)     // Catch:{ InterruptedException -> 0x02df }
        L_0x02d7:
            boolean r1 = r13.m     // Catch:{ all -> 0x002b }
            if (r1 == 0) goto L_0x02fe
            monitor-exit(r13)     // Catch:{ all -> 0x002b }
            r0 = r12
            goto L_0x0016
        L_0x02df:
            r1 = move-exception
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x002b }
            r2.<init>()     // Catch:{ all -> 0x002b }
            java.lang.String r3 = "AdLoader InterruptedException while loading the HTML: "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x002b }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ all -> 0x002b }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x002b }
            defpackage.t.e(r1)     // Catch:{ all -> 0x002b }
            r0.stopLoading()     // Catch:{ all -> 0x002b }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.INTERNAL_ERROR     // Catch:{ all -> 0x002b }
            monitor-exit(r13)     // Catch:{ all -> 0x002b }
            goto L_0x0016
        L_0x02fe:
            r0.stopLoading()     // Catch:{ all -> 0x002b }
            r0 = 1
            r13.l = r0     // Catch:{ all -> 0x002b }
            java.lang.String r0 = "AdLoader timed out while loading the HTML."
            defpackage.t.c(r0)     // Catch:{ all -> 0x002b }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.NETWORK_ERROR     // Catch:{ all -> 0x002b }
            monitor-exit(r13)     // Catch:{ all -> 0x002b }
            goto L_0x0016
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c.doInBackground(com.google.ads.AdRequest[]):com.google.ads.AdRequest$ErrorCode");
    }

    public static void a(long j2) {
        synchronized (c) {
            d = j2;
        }
    }

    private long b() {
        long j2;
        if (!(this.h.d() instanceof InterstitialAd)) {
            return 60000;
        }
        synchronized (c) {
            j2 = d;
        }
        return j2;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a() {
        this.m = true;
        notify();
    }

    public final synchronized void a(AdRequest.ErrorCode errorCode) {
        this.k = errorCode;
        notify();
    }

    public final synchronized void a(String str) {
        this.j = str;
        notify();
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(String str, String str2) {
        this.e = str2;
        this.f = str;
        notify();
    }

    /* access modifiers changed from: protected */
    public final void onCancelled() {
        t.a("AdLoader cancelled.");
        this.i.stopLoading();
        this.g.cancel(false);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onPostExecute(Object x0) {
        AdRequest.ErrorCode errorCode = (AdRequest.ErrorCode) x0;
        synchronized (this) {
            if (errorCode == null) {
                this.h.n();
            } else {
                this.i.stopLoading();
                this.g.cancel(false);
                if (this.l) {
                    this.h.g().setVisibility(8);
                }
                this.h.a(errorCode);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onProgressUpdate(Object[] x0) {
        this.g.execute(((String[]) x0)[0]);
    }
}
