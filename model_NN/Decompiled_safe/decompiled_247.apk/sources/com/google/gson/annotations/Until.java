package com.google.gson.annotations;

public @interface Until {
    double value();
}
