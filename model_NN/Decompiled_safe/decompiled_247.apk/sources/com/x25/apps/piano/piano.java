package com.x25.apps.piano;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.mobclick.android.MobclickAgent;

public class piano extends Activity {
    /* access modifiers changed from: private */
    public ImageButton imageButton_black1;
    /* access modifiers changed from: private */
    public ImageButton imageButton_black2;
    /* access modifiers changed from: private */
    public ImageButton imageButton_black3;
    /* access modifiers changed from: private */
    public ImageButton imageButton_black4;
    /* access modifiers changed from: private */
    public ImageButton imageButton_black5;
    /* access modifiers changed from: private */
    public ImageButton imageButton_white1;
    /* access modifiers changed from: private */
    public ImageButton imageButton_white2;
    /* access modifiers changed from: private */
    public ImageButton imageButton_white3;
    /* access modifiers changed from: private */
    public ImageButton imageButton_white4;
    /* access modifiers changed from: private */
    public ImageButton imageButton_white5;
    /* access modifiers changed from: private */
    public ImageButton imageButton_white6;
    /* access modifiers changed from: private */
    public ImageButton imageButton_white7;
    /* access modifiers changed from: private */
    public ImageButton imageButton_white8;
    /* access modifiers changed from: private */
    public MediaPlayer mediaPlayer01;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.main);
        setRequestedOrientation(0);
        getWindow().setFlags(1024, 1024);
        MobclickAgent.onError(this);
        this.mediaPlayer01 = new MediaPlayer();
        this.imageButton_white1 = (ImageButton) findViewById(R.id.white1);
        this.imageButton_white2 = (ImageButton) findViewById(R.id.white2);
        this.imageButton_white3 = (ImageButton) findViewById(R.id.white3);
        this.imageButton_white4 = (ImageButton) findViewById(R.id.white4);
        this.imageButton_white5 = (ImageButton) findViewById(R.id.white5);
        this.imageButton_white6 = (ImageButton) findViewById(R.id.white6);
        this.imageButton_white7 = (ImageButton) findViewById(R.id.white7);
        this.imageButton_white8 = (ImageButton) findViewById(R.id.white8);
        this.imageButton_black1 = (ImageButton) findViewById(R.id.black1);
        this.imageButton_black2 = (ImageButton) findViewById(R.id.black2);
        this.imageButton_black3 = (ImageButton) findViewById(R.id.black3);
        this.imageButton_black4 = (ImageButton) findViewById(R.id.black4);
        this.imageButton_black5 = (ImageButton) findViewById(R.id.black5);
        this.imageButton_white1.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 0) {
                    piano.this.play(R.raw.white1);
                    piano.this.imageButton_white1.setImageResource(R.drawable.whiteback1);
                }
                if (motionEvent.getAction() != 1) {
                    return false;
                }
                piano.this.imageButton_white1.setImageResource(R.drawable.white1);
                return false;
            }
        });
        this.imageButton_white2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 0) {
                    piano.this.play(R.raw.white2);
                    piano.this.imageButton_white2.setImageResource(R.drawable.whiteback2);
                }
                if (motionEvent.getAction() != 1) {
                    return false;
                }
                piano.this.imageButton_white2.setImageResource(R.drawable.white2);
                return false;
            }
        });
        this.imageButton_white3.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 0) {
                    piano.this.play(R.raw.white3);
                    piano.this.imageButton_white3.setImageResource(R.drawable.whiteback3);
                }
                if (motionEvent.getAction() != 1) {
                    return false;
                }
                piano.this.imageButton_white3.setImageResource(R.drawable.white3);
                return false;
            }
        });
        this.imageButton_white4.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 0) {
                    piano.this.play(R.raw.white4);
                    piano.this.imageButton_white4.setImageResource(R.drawable.whiteback4);
                }
                if (motionEvent.getAction() != 1) {
                    return false;
                }
                piano.this.imageButton_white4.setImageResource(R.drawable.white4);
                return false;
            }
        });
        this.imageButton_white5.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 0) {
                    piano.this.play(R.raw.white5);
                    piano.this.imageButton_white5.setImageResource(R.drawable.whiteback5);
                }
                if (motionEvent.getAction() != 1) {
                    return false;
                }
                piano.this.imageButton_white5.setImageResource(R.drawable.white5);
                return false;
            }
        });
        this.imageButton_white6.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 0) {
                    piano.this.play(R.raw.white6);
                    piano.this.imageButton_white6.setImageResource(R.drawable.whiteback6);
                }
                if (motionEvent.getAction() != 1) {
                    return false;
                }
                piano.this.imageButton_white6.setImageResource(R.drawable.white6);
                return false;
            }
        });
        this.imageButton_white7.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 0) {
                    piano.this.play(R.raw.white7);
                    piano.this.imageButton_white7.setImageResource(R.drawable.whiteback7);
                }
                if (motionEvent.getAction() != 1) {
                    return false;
                }
                piano.this.imageButton_white7.setImageResource(R.drawable.white7);
                return false;
            }
        });
        this.imageButton_white8.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 0) {
                    piano.this.play(R.raw.white8);
                    piano.this.imageButton_white8.setImageResource(R.drawable.whiteback8);
                }
                if (motionEvent.getAction() != 1) {
                    return false;
                }
                piano.this.imageButton_white8.setImageResource(R.drawable.white8);
                return false;
            }
        });
        this.imageButton_black1.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 0) {
                    piano.this.play(R.raw.black1);
                }
                if (motionEvent.getAction() != 1) {
                    return false;
                }
                piano.this.imageButton_black1.setImageResource(R.drawable.black1);
                return false;
            }
        });
        this.imageButton_black2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 0) {
                    piano.this.play(R.raw.black2);
                }
                if (motionEvent.getAction() != 1) {
                    return false;
                }
                piano.this.imageButton_black2.setImageResource(R.drawable.black2);
                return false;
            }
        });
        this.imageButton_black3.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 0) {
                    piano.this.play(R.raw.black3);
                }
                if (motionEvent.getAction() != 1) {
                    return false;
                }
                piano.this.imageButton_black3.setImageResource(R.drawable.black3);
                return false;
            }
        });
        this.imageButton_black4.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 0) {
                    piano.this.play(R.raw.black4);
                }
                if (motionEvent.getAction() != 1) {
                    return false;
                }
                piano.this.imageButton_black4.setImageResource(R.drawable.black4);
                return false;
            }
        });
        this.imageButton_black5.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 0) {
                    piano.this.play(R.raw.black5);
                }
                if (motionEvent.getAction() != 1) {
                    return false;
                }
                piano.this.imageButton_black5.setImageResource(R.drawable.black5);
                return false;
            }
        });
        this.mediaPlayer01.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer arg0) {
                piano.this.mediaPlayer01.release();
                piano.this.mediaPlayer01 = null;
                Toast.makeText(piano.this, "Release!", 0).show();
            }
        });
        this.mediaPlayer01.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            public boolean onError(MediaPlayer arg0, int i, int i1) {
                try {
                    piano.this.mediaPlayer01.release();
                    Toast.makeText(piano.this, "Error!", 0).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return false;
            }
        });
        AdView adView = new AdView(this, AdSize.BANNER, "a14cf9ca3455718");
        ((RelativeLayout) findViewById(R.id.adLayout)).addView(adView);
        adView.loadAd(new AdRequest());
    }

    /* access modifiers changed from: private */
    public void play(int resource) {
        try {
            this.mediaPlayer01.release();
            this.mediaPlayer01 = MediaPlayer.create(this, resource);
            this.mediaPlayer01.start();
        } catch (Exception e) {
            Toast.makeText(this, "Error:" + e.getMessage(), 0).show();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.mediaPlayer01 != null) {
            this.mediaPlayer01.release();
            this.mediaPlayer01 = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }
}
