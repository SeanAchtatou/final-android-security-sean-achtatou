package com.x25.apps.piano;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int about = 2130837504;
        public static final int background = 2130837505;
        public static final int black1 = 2130837506;
        public static final int black2 = 2130837507;
        public static final int black3 = 2130837508;
        public static final int black4 = 2130837509;
        public static final int black5 = 2130837510;
        public static final int blackback1 = 2130837511;
        public static final int blackback2 = 2130837512;
        public static final int blackback3 = 2130837513;
        public static final int blackback4 = 2130837514;
        public static final int blackback5 = 2130837515;
        public static final int header = 2130837516;
        public static final int icon = 2130837517;
        public static final int quit = 2130837518;
        public static final int update = 2130837519;
        public static final int white1 = 2130837520;
        public static final int white2 = 2130837521;
        public static final int white3 = 2130837522;
        public static final int white4 = 2130837523;
        public static final int white5 = 2130837524;
        public static final int white6 = 2130837525;
        public static final int white7 = 2130837526;
        public static final int white8 = 2130837527;
        public static final int whiteback1 = 2130837528;
        public static final int whiteback2 = 2130837529;
        public static final int whiteback3 = 2130837530;
        public static final int whiteback4 = 2130837531;
        public static final int whiteback5 = 2130837532;
        public static final int whiteback6 = 2130837533;
        public static final int whiteback7 = 2130837534;
        public static final int whiteback8 = 2130837535;
    }

    public static final class id {
        public static final int adLayout = 2131099662;
        public static final int black1 = 2131099657;
        public static final int black2 = 2131099658;
        public static final int black3 = 2131099659;
        public static final int black4 = 2131099660;
        public static final int black5 = 2131099661;
        public static final int iv = 2131099648;
        public static final int white1 = 2131099649;
        public static final int white2 = 2131099650;
        public static final int white3 = 2131099651;
        public static final int white4 = 2131099652;
        public static final int white5 = 2131099653;
        public static final int white6 = 2131099654;
        public static final int white7 = 2131099655;
        public static final int white8 = 2131099656;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class raw {
        public static final int black1 = 2130968576;
        public static final int black2 = 2130968577;
        public static final int black3 = 2130968578;
        public static final int black4 = 2130968579;
        public static final int black5 = 2130968580;
        public static final int white1 = 2130968581;
        public static final int white2 = 2130968582;
        public static final int white3 = 2130968583;
        public static final int white4 = 2130968584;
        public static final int white5 = 2130968585;
        public static final int white6 = 2130968586;
        public static final int white7 = 2130968587;
        public static final int white8 = 2130968588;
    }

    public static final class string {
        public static final int about = 2131034115;
        public static final int app_about = 2131034120;
        public static final int app_name = 2131034112;
        public static final int back = 2131034113;
        public static final int cancel = 2131034119;
        public static final int confirm = 2131034118;
        public static final int quit = 2131034116;
        public static final int quit_confirm = 2131034117;
        public static final int update = 2131034114;
    }
}
