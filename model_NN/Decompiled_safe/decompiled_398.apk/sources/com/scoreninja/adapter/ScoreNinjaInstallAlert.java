package com.scoreninja.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;

final class ScoreNinjaInstallAlert extends AlertDialog.Builder {
    private static final String LATER = "Maybe Later";
    private static final String MARKET_URI = "market://search?q=pname:com.scoreninja";
    private static final String NEVER = "Never Install";
    private static final String NOW = "Install Now";
    private static final String NO_SCORE_NINJA = "This game can use ScoreNinja to track global high scores. Please install ScoreNinja from the market.";
    /* access modifiers changed from: private */
    public final Activity parent;

    ScoreNinjaInstallAlert(Context context) {
        super(context);
        this.parent = (Activity) context;
        setMessage(NO_SCORE_NINJA);
        DialogInterface.OnClickListener installListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ScoreNinjaInstallAlert.this.parent.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(ScoreNinjaInstallAlert.MARKET_URI)));
            }
        };
        DialogInterface.OnClickListener laterListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        };
        DialogInterface.OnClickListener neverListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                SharedPreferences.Editor editor = ScoreNinjaInstallAlert.this.parent.getSharedPreferences("ScoreNinjaPrefs", 0).edit();
                editor.putBoolean("neverask", true);
                editor.commit();
            }
        };
        setPositiveButton(NOW, installListener);
        setNeutralButton(LATER, laterListener);
        setNegativeButton(NEVER, neverListener);
    }
}
