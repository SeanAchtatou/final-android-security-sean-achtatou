package com.scoreninja.adapter;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

final class RequestSigner {
    private static final char[] HEX_CHAR = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    private final Mac hmacSha1;

    RequestSigner(String privateKey) {
        this.hmacSha1 = getNewHmacSha1(privateKey);
    }

    /* access modifiers changed from: package-private */
    public String getSignature(String plainText) {
        if (this.hmacSha1 == null) {
            return "";
        }
        this.hmacSha1.update(plainText.getBytes());
        byte[] digest = this.hmacSha1.doFinal();
        char[] hexDigest = new char[40];
        for (int i = 0; i < 20; i++) {
            int byteValue = digest[i] & 255;
            hexDigest[i * 2] = HEX_CHAR[byteValue >> 4];
            hexDigest[(i * 2) + 1] = HEX_CHAR[byteValue & 15];
        }
        return new String(hexDigest);
    }

    private Mac getNewHmacSha1(String privateKey) {
        SecretKey secretKey = new SecretKeySpec(privateKey.getBytes(), "HmacSHA1");
        try {
            Mac mac = Mac.getInstance("HmacSHA1");
            try {
                mac.init(secretKey);
                return mac;
            } catch (InvalidKeyException e) {
                return null;
            }
        } catch (NoSuchAlgorithmException e2) {
            return null;
        }
    }
}
