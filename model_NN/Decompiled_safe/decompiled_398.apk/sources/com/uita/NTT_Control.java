package com.uita;

import com.scoreninja.adapter.ScoreNinjaAdapter;
import com.uita.UitaView;

public class NTT_Control extends NTT_Base {
    private int dis_val;
    private int ident_ct;
    private int ident_scale;

    NTT_Control() {
        this.state = 1000;
    }

    public void Run(int c) {
        switch (this.state) {
            case 1:
                this.dis_val = 0;
                this.state = 2;
                UitaView.AdsOn();
                this.rot = 0;
                return;
            case 2:
                Sprite.Draw(Init(11), Init(240), Init(160));
                if (Uita.GetPressedDebounce() == 1) {
                    Audio.playSound(2);
                    this.state = 3;
                }
                this.dis_val += Inter(Init(1));
                if (((this.dis_val >> 8) & 16) > 5) {
                    Sprite.Draw(Init(29), 61440, 45056);
                }
                GFX.DisplayVal(GameState.DistanceRecord, 350, 228, 0);
                return;
            case 3:
                Sprite.Draw(Init(11), Init(240), Init(160));
                GFX.DisplayVal(GameState.DistanceRecord, 376, 192, 0);
                Sprite.Draw(Init(30), 61440, 59904);
                this.state = 10;
                return;
            case 10:
                UitaView.AdsOff();
                GameState.ShowShipBG = 1;
                GameState.fly_complete = false;
                TaskManager.Add(3);
                TaskManager.AddEnd(6);
                TaskManager.Add(7);
                this.state = 11;
                return;
            case 11:
                if (GameState.fly_complete) {
                    this.state = 60;
                    return;
                }
                return;
            case Sprite.sprSkyBG /*12*/:
                TaskManager.RemoveByType(3);
                TaskManager.RemoveByType(6);
                TaskManager.RemoveByType(7);
                TaskManager.RemoveByType(8);
                TaskManager.RemoveByType(9);
                TaskManager.RemoveByType(10);
                if (Uita.NumJumps == 10) {
                    Uita.NumJumps = 0;
                    Uita.ShowAds = 1;
                    this.state = 1;
                    return;
                }
                this.state = 10;
                return;
            case 40:
            case 50:
            default:
                return;
            case UitaView.UitaThread.PHYS_FUEL_INIT /*60*/:
                TaskManager.RemoveByType(6);
                TaskManager.RemoveByType(7);
                TaskManager.RemoveByType(8);
                TaskManager.RemoveByType(9);
                Sprite.Draw(Init(12), Init(240), Init(160));
                GameState.hs_flag = 1;
                UitaView.Scores();
                this.state = 61;
                return;
            case 61:
                if (GameState.hs_flag != 0) {
                    ScoreNinjaAdapter scoreNinjaAdapter = GameState.AppUita.scoreNinjaAdapter;
                    if (ScoreNinjaAdapter.isInstalled(GameState.AppUita)) {
                        return;
                    }
                }
                GameState.hs_flag = 0;
                GameState.Log("Reset control");
                this.state = 12;
                return;
            case 1000:
                UitaView.AdsOff();
                Sprite.LoadIdent();
                this.ident_ct = 0;
                this.ident_scale = 0;
                this.state = 1001;
                return;
            case 1001:
                Sprite.DrawIdent(this.ident_scale);
                this.ident_scale += Inter(16);
                if (this.ident_scale > 256) {
                    this.ident_scale = Sprite.scaleONE;
                    this.state = 1002;
                    return;
                }
                return;
            case 1002:
                Sprite.DrawIdent(this.ident_scale);
                Sprite.Init(UitaView.ViewContext);
                Audio.Init(UitaView.ViewContext);
                GFX.DisFPS = 1;
                this.state = 1003;
                return;
            case 1003:
                Sprite.DrawIdent(this.ident_scale);
                this.ident_ct += Inter(Sprite.scaleONE);
                if ((this.ident_ct >> 8) > 200) {
                    Sprite.FreeIdent();
                    this.state = 1;
                    return;
                }
                return;
        }
    }
}
