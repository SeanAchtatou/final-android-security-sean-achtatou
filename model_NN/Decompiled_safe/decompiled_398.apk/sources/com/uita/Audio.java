package com.uita;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;

public class Audio {
    private static int[] IDTab = new int[10];
    public static final int SFX_Boing = 1;
    public static final int SFX_LetsGo = 2;
    private static final int SFX_NUM = 10;
    public static final int SFX_OhNo = 3;
    public static final int SFX_Pop3 = 7;
    public static final int SFX_Splash = 6;
    public static final int SFX_TryAgain = 8;
    public static final int SFX_Twoot = 5;
    public static final int SFX_Yahoo = 4;
    private static AudioManager mAudioManager;
    private static Context mContext;
    private static SoundPool mSoundPool;

    public static void Init(Context theContext) {
        GameState.Log("***************************");
        GameState.Log("START OF AUDIO SET UP");
        GameState.Log("***************************");
        mContext = theContext;
        mSoundPool = new SoundPool(10, 3, 0);
        mAudioManager = (AudioManager) mContext.getSystemService("audio");
        addSound(8, R.raw.tryagain);
        addSound(7, R.raw.pop3);
        addSound(1, R.raw.boing_1);
        addSound(2, R.raw.letsgo);
        addSound(3, R.raw.oh_no);
        addSound(4, R.raw.yaahooo);
        addSound(5, R.raw.twoot);
        addSound(6, R.raw.splash);
        GameState.Log("***************************");
        GameState.Log("END OF AUDIO SET UP");
        GameState.Log("***************************");
    }

    public static void addSound(int index, int SoundID) {
        IDTab[index] = mSoundPool.load(mContext, SoundID, 1);
        GameState.Log("Add sound " + index + " : " + IDTab[index]);
    }

    public static void playSound(int index, float freq) {
        float streamVolume = ((float) mAudioManager.getStreamVolume(3)) / ((float) mAudioManager.getStreamMaxVolume(3));
        mSoundPool.play(IDTab[index], streamVolume, streamVolume, 1, 0, freq);
    }

    public static void playSound(int index) {
        playSound(index, 1.0f);
    }
}
