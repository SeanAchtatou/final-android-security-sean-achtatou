package com.uita;

public final class R {

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int keywords = 2130771971;
        public static final int primaryTextColor = 2130771969;
        public static final int refreshInterval = 2130771972;
        public static final int secondaryTextColor = 2130771970;
        public static final int textColor = 2130771973;
    }

    public static final class drawable {
        public static final int angle_arrow = 2130837504;
        public static final int angle_bg = 2130837505;
        public static final int app_uita = 2130837506;
        public static final int arrow = 2130837507;
        public static final int arrow_red = 2130837508;
        public static final int best = 2130837509;
        public static final int boost = 2130837510;
        public static final int boost_and_star = 2130837511;
        public static final int boost_star = 2130837512;
        public static final int cloud1 = 2130837513;
        public static final int cutlass = 2130837514;
        public static final int cutlassb = 2130837515;
        public static final int dig0 = 2130837516;
        public static final int dig1 = 2130837517;
        public static final int dig2 = 2130837518;
        public static final int dig3 = 2130837519;
        public static final int dig4 = 2130837520;
        public static final int dig5 = 2130837521;
        public static final int dig6 = 2130837522;
        public static final int dig7 = 2130837523;
        public static final int dig8 = 2130837524;
        public static final int dig9 = 2130837525;
        public static final int great_jump = 2130837526;
        public static final int ident = 2130837527;
        public static final int lets_go = 2130837528;
        public static final int pirate_falling = 2130837529;
        public static final int pirate_flying = 2130837530;
        public static final int pirate_walk1 = 2130837531;
        public static final int pirate_walk2 = 2130837532;
        public static final int pirate_walk3 = 2130837533;
        public static final int railing1 = 2130837534;
        public static final int sea1 = 2130837535;
        public static final int shipbg1 = 2130837536;
        public static final int skybg1 = 2130837537;
        public static final int skybg1n = 2130837538;
        public static final int splash = 2130837539;
        public static final int tile_test1 = 2130837540;
        public static final int tile_test2 = 2130837541;
        public static final int title = 2130837542;
        public static final int touch_start = 2130837543;
        public static final int touch_try_again = 2130837544;
        public static final int you = 2130837545;
    }

    public static final class id {
        public static final int ad = 2131165185;
        public static final int uita = 2131165184;
    }

    public static final class layout {
        public static final int main = 2130903040;
        public static final int uita_layout = 2130903041;
    }

    public static final class raw {
        public static final int boing_1 = 2130968576;
        public static final int drunkensailor = 2130968577;
        public static final int letsgo = 2130968578;
        public static final int oh_no = 2130968579;
        public static final int pop3 = 2130968580;
        public static final int splash = 2130968581;
        public static final int tryagain = 2130968582;
        public static final int twoot = 2130968583;
        public static final int yaahooo = 2130968584;
    }

    public static final class string {
        public static final int app_name = 2131034112;
        public static final int key_ahighscore = 2131034130;
        public static final int menu_easy = 2131034117;
        public static final int menu_hard = 2131034119;
        public static final int menu_medium = 2131034118;
        public static final int menu_pause = 2131034115;
        public static final int menu_resume = 2131034116;
        public static final int menu_start = 2131034113;
        public static final int menu_stop = 2131034114;
        public static final int message_bad_angle = 2131034128;
        public static final int message_off_pad = 2131034126;
        public static final int message_stopped = 2131034125;
        public static final int message_too_fast = 2131034127;
        public static final int mode_lose = 2131034122;
        public static final int mode_pause = 2131034121;
        public static final int mode_ready = 2131034120;
        public static final int mode_win_prefix = 2131034123;
        public static final int mode_win_suffix = 2131034124;
        public static final int uita_layout_text_text = 2131034129;
    }

    public static final class style {
        public static final int Theme_NoBackground = 2131099648;
    }

    public static final class styleable {
        public static final int[] com_admob_android_ads_AdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.keywords, R.attr.refreshInterval, R.attr.textColor};
        public static final int com_admob_android_ads_AdView_backgroundColor = 0;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_primaryTextColor = 1;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_secondaryTextColor = 2;
        public static final int com_admob_android_ads_AdView_textColor = 5;
    }
}
