package com.uita;

public class NTT_BoostStar extends NTT_Base {
    NTT_BoostStar() {
        this.state = 1;
    }

    public void Run(int c) {
        switch (this.state) {
            case 1:
                this.fr = Init(36);
                this.xpos = Init(240);
                this.ypos = Init(160);
                this.scale = Sprite.scaleTWO;
                this.rot = 0;
                this.state = 2;
                return;
            case 2:
                if (Update() == 1) {
                    TaskManager.Remove(this);
                    return;
                } else {
                    Display();
                    return;
                }
            default:
                return;
        }
    }

    private void Display() {
        Sprite.Draw(this.fr, this.xpos, this.ypos, this.rot, this.scale);
    }

    private int Update() {
        this.rot += 16;
        this.scale -= Inter(32);
        if (this.scale <= 0) {
            return 0 + 1;
        }
        return 0;
    }
}
