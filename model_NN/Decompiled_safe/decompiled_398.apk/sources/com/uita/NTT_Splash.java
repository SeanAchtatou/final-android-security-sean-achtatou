package com.uita;

public class NTT_Splash extends NTT_Base {
    NTT_Splash() {
        this.state = 1;
    }

    public void Run(int c) {
        switch (this.state) {
            case 1:
                this.fr = Init(24);
                this.xpos = GameState.SplashX;
                this.ypos = GameState.SplashY;
                this.yvel = Init(3);
                this.scale = Init(3);
                this.state = 2;
                return;
            case 2:
                if (Update() == 1) {
                    TaskManager.Remove(this);
                    return;
                } else {
                    Display();
                    return;
                }
            default:
                return;
        }
    }

    private void Display() {
        Sprite.Draw(this.fr, this.xpos, this.ypos, 0, this.scale);
    }

    private int Update() {
        int rv = 0;
        this.scale -= Inter(32);
        if (this.scale < 128) {
            rv = 0 + 1;
        }
        this.ypos += Inter(this.yvel);
        return rv;
    }
}
