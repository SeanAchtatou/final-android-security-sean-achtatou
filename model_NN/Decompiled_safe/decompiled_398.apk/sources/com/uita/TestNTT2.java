package com.uita;

public class TestNTT2 extends NTT_Base {
    private int bob_offset;
    private int bob_offset_upd;
    private int cloud_gen_ct;
    private int cloud_gen_ct_bak = 4;
    private final int dis_fall = 1;
    private final int dis_fly = 3;
    private final int dis_fly_hit = 4;
    private final int dis_fly_init = 2;
    private final int dis_norm = 0;
    private int genct;
    private int world_xpos;
    private int world_ypos;
    int xupd;
    private int xvel_bak;
    private int yvel_bak;

    TestNTT2() {
        this.state = 1;
        this.fr_spd = 48;
        this.fr_spd_ct = 0;
        this.fr_index = 0;
        this.fr_base = 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x00bd  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x011e  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x013e  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0197  */
    /* JADX WARNING: Removed duplicated region for block: B:52:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:55:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void Run(int r11) {
        /*
            r10 = this;
            r5 = 2
            r6 = 12
            r9 = 240(0xf0, float:3.36E-43)
            r8 = 1
            r7 = 0
            int r4 = r10.state
            switch(r4) {
                case 1: goto L_0x000d;
                case 2: goto L_0x003b;
                case 10: goto L_0x0050;
                case 11: goto L_0x0072;
                case 12: goto L_0x0095;
                case 20: goto L_0x00db;
                case 30: goto L_0x00f3;
                case 31: goto L_0x0103;
                case 40: goto L_0x0124;
                case 41: goto L_0x0138;
                case 50: goto L_0x0159;
                case 51: goto L_0x017a;
                case 52: goto L_0x01dc;
                case 1000: goto L_0x024d;
                default: goto L_0x000c;
            }
        L_0x000c:
            return
        L_0x000d:
            int r4 = r10.fr_base
            int r4 = r10.Init(r4)
            r10.fr = r4
            r4 = 80
            int r4 = r10.Init(r4)
            r10.xpos = r4
            r4 = 186(0xba, float:2.6E-43)
            int r4 = r10.Init(r4)
            r10.ypos = r4
            r10.rot = r7
            com.uita.GameState.start_run = r7
            com.uita.GameState.reset_after_fall = r7
            com.uita.GameState.sea_mode = r7
            com.uita.GameState.new_high = r7
            com.uita.GameState.PerfectTakeOff = r7
            com.uita.GameState.PerfectTakeOff2 = r7
            r4 = 3
            com.uita.GameState.BoostNum = r4
            r10.cloud_gen_ct = r8
            r10.state = r5
            goto L_0x000c
        L_0x003b:
            r10.DisplayShip()
            r10.AnimUpdate()
            int r4 = com.uita.GameState.start_run
            if (r4 != r8) goto L_0x004c
            r10.SetRun()
            r4 = 10
            r10.state = r4
        L_0x004c:
            r10.Display(r7)
            goto L_0x000c
        L_0x0050:
            r10.DisplayShip()
            r10.AnimUpdate()
            int r3 = r10.UpdateRun()
            if (r3 != r8) goto L_0x0067
            r10.SetFly()
            r4 = 20
            r10.state = r4
        L_0x0063:
            r10.Display(r7)
            goto L_0x000c
        L_0x0067:
            if (r3 != r5) goto L_0x0063
            r4 = 3
            com.uita.Audio.playSound(r4)
            r4 = 11
            r10.state = r4
            goto L_0x0063
        L_0x0072:
            r4 = 4
            int r4 = r10.Init(r4)
            r10.fr = r4
            int r4 = r10.xpos
            r5 = 16
            int r5 = r10.Init(r5)
            int r4 = r4 - r5
            r10.xpos = r4
            int r4 = r10.Init(r8)
            r10.xvel = r4
            r4 = -4
            int r4 = r10.Init(r4)
            r10.yvel = r4
            r10.state = r6
            com.uita.GameState.Distance = r7
        L_0x0095:
            r10.DisplayShip()
            int r4 = r10.xpos
            int r5 = r10.xvel
            int r5 = r10.Inter(r5)
            int r4 = r4 + r5
            r10.xpos = r4
            int r4 = r10.yvel
            r5 = 96
            int r5 = r10.Inter(r5)
            int r4 = r4 + r5
            r10.yvel = r4
            int r4 = r10.ypos
            int r5 = r10.yvel
            int r4 = r4 + r5
            r10.ypos = r4
            int r4 = r10.ypos
            int r4 = r4 >> 8
            r5 = 320(0x140, float:4.48E-43)
            if (r4 <= r5) goto L_0x00d6
            r4 = 6
            com.uita.Audio.playSound(r4)
            com.uita.GameState.reset_after_fall = r8
            r10.state = r8
            int r4 = r10.xpos
            com.uita.GameState.SplashX = r4
            r4 = 248(0xf8, float:3.48E-43)
            int r4 = r10.Init(r4)
            com.uita.GameState.SplashY = r4
            r4 = 9
            com.uita.TaskManager.Add(r4)
        L_0x00d6:
            r10.Display(r8)
            goto L_0x000c
        L_0x00db:
            r10.DisplayShip()
            int r4 = r10.UpdateFlying(r7)
            if (r4 != r8) goto L_0x00ee
            com.uita.GameState.ShowShipBG = r7
            r4 = 6
            com.uita.TaskManager.RemoveByType(r4)
            r4 = 30
            r10.state = r4
        L_0x00ee:
            r10.Display(r5)
            goto L_0x000c
        L_0x00f3:
            r4 = 10
            com.uita.TaskManager.RemoveByType(r4)
            r4 = 10
            com.uita.TaskManager.AddEnd(r4)
            com.uita.GameState.BoostPressed = r7
            r4 = 31
            r10.state = r4
        L_0x0103:
            int r4 = r10.Init(r6)
            int r5 = r10.Init(r9)
            r6 = 160(0xa0, float:2.24E-43)
            int r6 = r10.Init(r6)
            com.uita.Sprite.Draw(r4, r5, r6)
            r4 = 3
            r10.Display(r4)
            int r4 = r10.UpdateFlying(r8)
            if (r4 != r8) goto L_0x000c
            r4 = 40
            r10.state = r4
            goto L_0x000c
        L_0x0124:
            r4 = 8
            com.uita.TaskManager.RemoveByType(r4)
            r4 = 10
            com.uita.TaskManager.RemoveByType(r4)
            r4 = 6
            com.uita.TaskManager.AddEnd(r4)
            com.uita.GameState.sea_mode = r8
            r4 = 41
            r10.state = r4
        L_0x0138:
            int r4 = r10.UpdateFlying(r5)
            if (r4 != r8) goto L_0x0142
            r4 = 50
            r10.state = r4
        L_0x0142:
            int r4 = r10.Init(r6)
            int r5 = r10.Init(r9)
            r6 = 160(0xa0, float:2.24E-43)
            int r6 = r10.Init(r6)
            com.uita.Sprite.Draw(r4, r5, r6)
            r4 = 4
            r10.Display(r4)
            goto L_0x000c
        L_0x0159:
            com.uita.GameState.sea_mode = r7
            r4 = 6
            com.uita.Audio.playSound(r4)
            int r4 = r10.Init(r9)
            com.uita.GameState.SplashX = r4
            r4 = 248(0xf8, float:3.48E-43)
            int r4 = r10.Init(r4)
            com.uita.GameState.SplashY = r4
            r4 = 9
            com.uita.TaskManager.Add(r4)
            r4 = 64
            r10.genct = r4
            r4 = 51
            r10.state = r4
        L_0x017a:
            int r4 = r10.Init(r6)
            int r5 = r10.Init(r9)
            r6 = 160(0xa0, float:2.24E-43)
            int r6 = r10.Init(r6)
            com.uita.Sprite.Draw(r4, r5, r6)
            com.uita.GFX.DisplayDistance()
            int r4 = r10.genct
            int r4 = r4 - r8
            r10.genct = r4
            int r4 = r10.genct
            if (r4 != 0) goto L_0x000c
            r4 = 4
            int r4 = r10.Init(r4)
            r10.fr = r4
            int r4 = r10.Init(r9)
            r10.xpos = r4
            r4 = 320(0x140, float:4.48E-43)
            int r4 = r10.Init(r4)
            r10.ypos = r4
            r10.rot = r7
            r4 = 64
            int r4 = r10.Init(r4)
            r10.bob_offset = r4
            int r4 = r10.Init(r8)
            r10.bob_offset_upd = r4
            com.uita.UitaView.AdsOn()
            r4 = 52
            r10.state = r4
            int r4 = com.uita.GameState.Distance
            int r5 = com.uita.GameState.DistanceRecord
            if (r4 <= r5) goto L_0x01d5
            r4 = 4
            com.uita.Audio.playSound(r4)
            int r4 = com.uita.GameState.Distance
            com.uita.GameState.DistanceRecord = r4
            com.uita.GameState.new_high = r8
            goto L_0x000c
        L_0x01d5:
            r4 = 8
            com.uita.Audio.playSound(r4)
            goto L_0x000c
        L_0x01dc:
            int r4 = r10.Init(r6)
            int r5 = r10.Init(r9)
            r6 = 160(0xa0, float:2.24E-43)
            int r6 = r10.Init(r6)
            com.uita.Sprite.Draw(r4, r5, r6)
            int r4 = r10.bob_offset
            float r4 = (float) r4
            r5 = 1157627904(0x45000000, float:2048.0)
            float r1 = r4 / r5
            float r2 = android.util.FloatMath.sin(r1)
            r4 = 312(0x138, float:4.37E-43)
            int r4 = r10.Init(r4)
            r5 = 1166016512(0x45800000, float:4096.0)
            float r5 = r5 * r2
            int r5 = (int) r5
            int r4 = r4 + r5
            r10.ypos = r4
            int r4 = r10.bob_offset
            int r5 = r10.bob_offset_upd
            int r5 = r10.Inter(r5)
            int r4 = r4 + r5
            r10.bob_offset = r4
            r10.Display(r8)
            int r4 = r10.bob_offset
            r5 = 170(0xaa, float:2.38E-43)
            int r5 = r10.Init(r5)
            if (r4 <= r5) goto L_0x000c
            int r4 = com.uita.GameState.hs_flag
            if (r4 != 0) goto L_0x000c
            r4 = 1124073472(0x43000000, float:128.0)
            float r4 = r4 * r2
            int r4 = (int) r4
            int r4 = -r4
            int r0 = r4 + 256
            r4 = 27
            int r4 = r10.Init(r4)
            r5 = 61440(0xf000, float:8.6096E-41)
            r6 = 40960(0xa000, float:5.7397E-41)
            com.uita.Sprite.Draw(r4, r5, r6, r7, r0)
            int r4 = com.uita.Uita.GetPressedDebounce()
            if (r4 != r8) goto L_0x000c
            com.uita.GameState.fly_complete = r8
            r4 = 5
            r5 = 1056964608(0x3f000000, float:0.5)
            com.uita.Audio.playSound(r4, r5)
            int r4 = com.uita.Uita.NumJumps
            int r4 = r4 + 1
            com.uita.Uita.NumJumps = r4
            goto L_0x000c
        L_0x024d:
            r10.world_xpos = r7
            r10.world_ypos = r7
            r4 = 1001(0x3e9, float:1.403E-42)
            r10.state = r4
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uita.TestNTT2.Run(int):void");
    }

    public void AnimUpdate() {
        int tfr;
        this.fr_spd_ct += Inter(this.fr_spd);
        if (this.fr_spd_ct >= 256) {
            this.fr_spd_ct -= Sprite.scaleONE;
            this.fr_index++;
            if (this.fr_index > 3) {
                this.fr_index = 0;
            }
            if (this.fr_index < 3) {
                tfr = this.fr_index;
            } else {
                tfr = 1;
            }
            this.fr = Init(tfr + this.fr_base);
        }
    }

    public void Display(int type) {
        int dfr;
        int dx;
        int dy;
        int drot;
        switch (type) {
            case 0:
                dfr = this.fr;
                dx = this.xpos;
                dy = this.ypos - (Uita.GetBG_YOffset() << 8);
                drot = this.rot;
                break;
            case 1:
                dfr = this.fr;
                dx = this.xpos;
                dy = this.ypos;
                drot = 0;
                break;
            case 2:
                dfr = this.fr;
                dx = this.xpos;
                dy = this.ypos - (Uita.GetBG_YOffset() << 8);
                drot = this.rot;
                break;
            case 3:
                dfr = this.fr;
                dx = 61440;
                dy = 40960;
                drot = this.rot;
                break;
            case 4:
                dfr = this.fr;
                dx = 61440;
                dy = this.ypos;
                drot = this.rot;
                break;
            default:
                dfr = 0;
                dx = -9999;
                dy = -9999;
                drot = 0;
                break;
        }
        if (type > 0) {
            GFX.DisplayDistance();
        }
        if (type == 3 && GameState.PerfectTakeOff > 0) {
            GameState.PerfectTakeOff += Inter(Sprite.scaleONE);
            int gv = GameState.PerfectTakeOff >> 8;
            if ((gv & 8) == 8 && gv < 96) {
                Sprite.Draw(Init(31), 61440, 58880);
            }
        }
        if (dx > 0) {
            Sprite.Draw(dfr, dx, dy, drot);
        }
    }

    public int UpdateRun() {
        this.xpos += Inter(this.xvel);
        if (Uita.GetPressedDebounce() == 1) {
            Audio.playSound(1);
            GameState.plank_position_chosen = this.xpos;
            return 1;
        } else if (this.xpos > Init(400)) {
            return 2;
        } else {
            return 0;
        }
    }

    public int UpdateFlying(int type) {
        int rv = 0;
        if (GameState.PerfectTakeOff > 0) {
            if (GameState.PerfectTakeOff2 == 1) {
                this.xvel -= Inter(2);
            } else {
                this.xvel -= Inter(2);
            }
            this.yvel += Inter(12);
        } else {
            this.xvel -= Inter(9);
            this.yvel += Inter(26);
        }
        if (this.xvel < Init(1)) {
            this.xvel = Init(1);
        }
        this.xpos += Inter(this.xvel);
        this.ypos += Inter(this.yvel);
        GameState.PirateX = this.xpos;
        GameState.PirateY = this.ypos;
        GameState.PirateYVel = this.yvel;
        SetDistance();
        this.rot = (this.yvel - Init(-8)) * 10;
        if (this.rot > 28672) {
            this.rot = Init(112);
        }
        switch (type) {
            case 0:
                if ((this.xpos >> 8) > 500 || this.ypos < 400) {
                    return 1;
                }
                return 0;
            case 1:
                if ((this.ypos >> 8) > 0 && this.yvel > 0) {
                    rv = 1;
                }
                this.cloud_gen_ct--;
                if (this.cloud_gen_ct == 0) {
                    this.cloud_gen_ct = this.cloud_gen_ct_bak;
                    TaskManager.Add(8);
                }
                if (GameState.BoostPressed != 1) {
                    return rv;
                }
                GameState.BoostPressed = 0;
                Audio.playSound(4);
                TaskManager.Add(11);
                int txv = (this.xvel * 3) / 2;
                if (txv > this.xvel_bak) {
                    txv = this.xvel_bak;
                }
                this.xvel = txv;
                this.yvel = this.yvel_bak;
                return rv;
            case 2:
                if ((this.ypos >> 8) > 400) {
                    return 0 + 1;
                }
                return 0;
            default:
                return 0;
        }
    }

    public void SetRun() {
        this.xvel = Init(6) + (GameState.speed_chosen / 12);
    }

    public void SetFly() {
        this.fr = Init(3);
        this.yvel = Init(-10);
        int ang = GameState.angle_chosen;
        if (ang < 8960) {
            this.xvel -= (8960 - ang) / 6;
        } else if (ang > 14080) {
            this.yvel += ((ang - 14080) / 6) / 2;
        } else {
            this.yvel -= Init(1);
            if (this.xvel > Init(15) && GameState.plank_position_chosen > Init(340)) {
                GameState.PerfectTakeOff = 1;
                Audio.playSound(4);
            }
            int gv = ang >> 8;
            if (gv <= 40) {
                this.yvel -= Init(4);
            } else if (gv < 50) {
                this.yvel -= Init(5);
                this.xvel += Init(5);
                if (gv < 47 && gv != 45 && gv > 43) {
                    this.xvel += Sprite.scaleTWO;
                    this.yvel -= 768;
                } else if (gv == 45) {
                    this.xvel += Init(12);
                    this.yvel -= Init(5);
                    GameState.PerfectTakeOff = 1;
                    GameState.PerfectTakeOff2 = 1;
                }
            } else {
                this.xvel += Init(4);
            }
        }
        this.xvel_bak = this.xvel;
        this.yvel_bak = this.yvel;
    }

    public void SetDistance() {
        int gv;
        int gv2 = this.xpos;
        if (gv2 < 102400) {
            gv = 0;
        } else {
            gv = ((gv2 - 102400) * 16) / Sprite.scaleONE;
        }
        GameState.Distance = gv;
        GameState.Distance = (int) (((double) GameState.Distance) * 1.3d);
    }

    public void DisplayShip() {
        Uita.UpdateBG_YOffset();
        Sprite.Draw(Init(32), Init(240), Init(160) + (((int) ((float) (-Uita.GetBG_YOffset()))) << 8));
    }
}
