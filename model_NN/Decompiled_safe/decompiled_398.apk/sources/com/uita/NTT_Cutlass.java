package com.uita;

import com.uita.UitaView;

public class NTT_Cutlass extends NTT_Base {
    private int ang_upd;
    private int genct;
    private int last_num;
    private boolean spd_snd = false;
    private int x_base;
    private int x_offset;

    NTT_Cutlass() {
        this.state = 1;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:5:0x0042  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void Run(int r6) {
        /*
            r5 = this;
            r4 = 32
            r3 = 2
            r1 = 0
            r2 = 1
            int r0 = r5.state
            switch(r0) {
                case 1: goto L_0x000b;
                case 2: goto L_0x003c;
                case 3: goto L_0x004b;
                case 10: goto L_0x0062;
                case 11: goto L_0x008a;
                case 12: goto L_0x009b;
                case 20: goto L_0x00b8;
                case 21: goto L_0x00c0;
                default: goto L_0x000a;
            }
        L_0x000a:
            return
        L_0x000b:
            r5.state = r3
            r0 = 8
            int r0 = r5.Init(r0)
            r5.fr = r0
            r0 = 6
            int r0 = r5.Init(r0)
            r5.xvel = r0
            r0 = 200(0xc8, float:2.8E-43)
            int r0 = r5.Init(r0)
            r5.x_base = r0
            int r0 = r5.Init(r1)
            r5.x_offset = r0
            r0 = 70
            int r0 = r5.Init(r0)
            r5.ypos = r0
            int r0 = r5.Init(r2)
            r5.scale = r0
            r5.last_num = r1
            com.uita.GameState.Distance = r1
        L_0x003c:
            int r0 = r5.UpdateSpeed()
            if (r0 != r2) goto L_0x0047
            r5.genct = r4
            r0 = 3
            r5.state = r0
        L_0x0047:
            r5.DisplaySpeed()
            goto L_0x000a
        L_0x004b:
            int r0 = r5.genct
            int r0 = r0 - r2
            r5.genct = r0
            int r0 = r5.genct
            if (r0 != 0) goto L_0x0058
            r0 = 10
            r5.state = r0
        L_0x0058:
            int r0 = r5.genct
            r0 = r0 & 2
            if (r0 != r3) goto L_0x000a
            r5.DisplaySpeed()
            goto L_0x000a
        L_0x0062:
            r0 = 11
            r5.state = r0
            r5.rot = r1
            r0 = 400(0x190, float:5.6E-43)
            int r0 = r5.Init(r0)
            r5.xpos = r0
            r0 = 100
            int r0 = r5.Init(r0)
            r5.ypos = r0
            r0 = 7
            int r0 = r5.Init(r0)
            r1 = 25
            int r0 = r0 - r1
            r5.ang_upd = r0
            r0 = 9
            int r0 = r5.Init(r0)
            r5.fr = r0
        L_0x008a:
            int r0 = r5.UpdateAngle()
            if (r0 != r2) goto L_0x0096
            r5.genct = r4
            r0 = 12
            r5.state = r0
        L_0x0096:
            r5.DisplayAngle(r2)
            goto L_0x000a
        L_0x009b:
            int r0 = r5.genct
            int r0 = r0 - r2
            r5.genct = r0
            int r0 = r5.genct
            if (r0 != 0) goto L_0x00a8
            r0 = 20
            r5.state = r0
        L_0x00a8:
            int r0 = r5.genct
            r0 = r0 & 2
            if (r0 != r3) goto L_0x00b3
            r5.DisplayAngle(r2)
            goto L_0x000a
        L_0x00b3:
            r5.DisplayAngle(r1)
            goto L_0x000a
        L_0x00b8:
            com.uita.GameState.start_run = r2
            r0 = 21
            r5.state = r0
            goto L_0x000a
        L_0x00c0:
            boolean r0 = com.uita.GameState.reset_after_fall
            if (r0 == 0) goto L_0x000a
            r5.state = r2
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uita.NTT_Cutlass.Run(int):void");
    }

    private int UpdateSpeed() {
        if (Uita.GetPressedDebounce() == 1) {
            Audio.playSound(5, 0.5f);
            GameState.speed_chosen = this.x_offset;
            return 1;
        }
        this.x_offset += Inter(this.xvel);
        this.xpos = this.x_base + this.x_offset;
        if (this.x_offset > Init(UitaView.UitaThread.PHYS_SPEED_HYPERSPACE)) {
            this.x_offset = Init(0);
            this.spd_snd = false;
        }
        if (this.x_offset > Init(160) && !this.spd_snd) {
            Audio.playSound(7);
            this.spd_snd = true;
        }
        return 0;
    }

    private void DisplaySpeed() {
        int num = (this.x_offset >> 8) / 12;
        if (num == 0) {
            num = 1;
        }
        if (num >= 13) {
            num = 13;
        }
        for (int i = 0; i < num; i++) {
            this.scale = (i * 10) + 160;
            if (i >= 10) {
                Sprite.Draw(Init(28), this.x_base + ((i * 20) << 8), this.ypos, 0, this.scale);
            } else {
                Sprite.Draw(this.fr, this.x_base + ((i * 20) << 8), this.ypos, 0, this.scale);
            }
        }
        if (num != this.last_num) {
            this.last_num = num;
        }
    }

    private int UpdateAngle() {
        if (Uita.GetPressedDebounce() == 1) {
            Audio.playSound(5, 0.5f);
            GameState.angle_chosen = this.rot;
            return 1;
        }
        int snd = 0;
        this.rot += Inter(this.ang_upd);
        if (this.rot >= Init(90)) {
            this.rot = Init(90);
            this.ang_upd = -this.ang_upd;
            snd = 1;
        }
        if (this.rot <= Init(0)) {
            this.rot = Init(0);
            this.ang_upd = -this.ang_upd;
            snd = 1;
        }
        if (snd == 1) {
            Audio.playSound(7);
        }
        return 0;
    }

    private void DisplayAngle(int arr_flag) {
        int xo;
        Sprite.Draw(this.fr, this.xpos, this.ypos);
        if (arr_flag != 1) {
            return;
        }
        if (GFX.large_display == 0) {
            Sprite.Draw(Init(10) | Sprite.SPECIAL_CASE_ROTATE, this.xpos - 12800, this.ypos, this.rot);
            return;
        }
        if (GFX.scr_width == 800) {
            xo = 28;
        } else {
            xo = 24;
        }
        Sprite.Draw(Init(10) | Sprite.SPECIAL_CASE_ROTATE, this.xpos - GFX.AdjustMetricH(xo << 8), this.ypos, this.rot);
    }
}
