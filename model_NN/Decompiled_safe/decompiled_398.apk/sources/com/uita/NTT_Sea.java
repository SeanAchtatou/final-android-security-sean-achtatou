package com.uita;

public class NTT_Sea extends NTT_Base {
    private int alt_xpos;

    NTT_Sea() {
        this.state = 1;
    }

    public void Run(int c) {
        switch (this.state) {
            case 1:
                this.state = 2;
                this.xpos = Init(264);
                this.alt_xpos = Init(264);
                this.ypos = Init(304);
                this.fr = Init(5);
                this.xvel = Init(1);
                return;
            case 2:
                Update();
                Display();
                return;
            default:
                return;
        }
    }

    private void Display() {
        Sprite.Draw(this.fr, this.xpos, this.ypos);
        Sprite.Draw(this.fr, this.alt_xpos, this.ypos + 2048);
        if (GameState.ShowShipBG == 1) {
            Sprite.Draw(Init(6), Init(42), Init(228 - Uita.GetBG_YOffset()));
        }
    }

    private int Update() {
        this.xpos -= Inter(this.xvel);
        this.alt_xpos -= Inter(this.xvel * 2);
        if (GameState.sea_mode == 1) {
            int tv = Init((GameState.Distance >> 8) % 48);
            this.xpos -= tv;
            this.alt_xpos -= tv * 2;
        }
        if (this.xpos < 55296) {
            this.xpos += 12288;
        }
        if (this.alt_xpos < 55296) {
            do {
                this.alt_xpos += 12288;
            } while (this.alt_xpos < 55296);
        }
        return 0;
    }
}
