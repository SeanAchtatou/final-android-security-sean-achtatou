package com.uita;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;

public class Sprite {
    private static Drawable Ident = null;
    public static int SPECIAL_CASE_ROTATE = Integer.MIN_VALUE;
    private static Drawable b = null;
    public static int b_xleft = 0;
    public static int b_xright = 0;
    public static int b_ybottom = 0;
    public static int b_ytop = 0;
    public static int blyr_xright = 0;
    public static int blyr_ybottom = 0;
    public static int interUpdate = 0;
    private static Canvas mCanvas = null;
    public static final int num = 37;
    public static int plyr_xleft = 0;
    public static int plyr_ytop = 0;
    public static final int scaleHALF = 128;
    public static final int scaleONE = 256;
    public static final int scaleTWO = 512;
    public static final int sprAngleArrow = 10;
    public static final int sprAngleBG = 9;
    public static final int sprArrow = 8;
    public static final int sprArrowRed = 28;
    public static final int sprBest = 25;
    public static final int sprBoost1 = 34;
    public static final int sprBoost2 = 35;
    public static final int sprBoostStar = 36;
    public static final int sprCloud1 = 23;
    public static final int sprCutlass = 7;
    public static final int sprDig0 = 13;
    public static final int sprDig1 = 14;
    public static final int sprDig2 = 15;
    public static final int sprDig3 = 16;
    public static final int sprDig4 = 17;
    public static final int sprDig5 = 18;
    public static final int sprDig6 = 19;
    public static final int sprDig7 = 20;
    public static final int sprDig8 = 21;
    public static final int sprDig9 = 22;
    public static final int sprGreatJump = 31;
    public static final int sprLast = 37;
    public static final int sprLetsGo = 30;
    private static Drawable[] sprList = new Drawable[37];
    public static final int sprPirateFalling = 4;
    public static final int sprPirateFlying = 3;
    public static final int sprPirateWalk1 = 0;
    public static final int sprPirateWalk2 = 1;
    public static final int sprPirateWalk3 = 2;
    public static final int sprSea1 = 5;
    public static final int sprShipBG = 32;
    public static final int sprShipRailing = 6;
    public static final int sprSideBlank = 33;
    public static final int sprSkyBG = 12;
    public static final int sprSplash = 24;
    public static final int sprTitle = 11;
    public static final int sprTouchStart = 29;
    public static final int sprTouchTryAgain = 27;
    public static final int sprYou = 26;
    private static int spr_ct;

    public static void Init(Context context) {
        for (int i = 0; i < 37; i++) {
            sprList[i] = null;
        }
        sprList[0] = context.getResources().getDrawable(R.drawable.pirate_walk1);
        sprList[1] = context.getResources().getDrawable(R.drawable.pirate_walk2);
        sprList[2] = context.getResources().getDrawable(R.drawable.pirate_walk3);
        sprList[3] = context.getResources().getDrawable(R.drawable.pirate_flying);
        sprList[4] = context.getResources().getDrawable(R.drawable.pirate_falling);
        sprList[5] = context.getResources().getDrawable(R.drawable.sea1);
        sprList[6] = context.getResources().getDrawable(R.drawable.railing1);
        sprList[7] = context.getResources().getDrawable(R.drawable.cutlassb);
        sprList[8] = context.getResources().getDrawable(R.drawable.arrow);
        sprList[9] = context.getResources().getDrawable(R.drawable.angle_bg);
        sprList[10] = context.getResources().getDrawable(R.drawable.angle_arrow);
        sprList[12] = context.getResources().getDrawable(R.drawable.skybg1);
        sprList[11] = context.getResources().getDrawable(R.drawable.title);
        sprList[32] = context.getResources().getDrawable(R.drawable.shipbg1);
        sprList[13] = context.getResources().getDrawable(R.drawable.dig0);
        sprList[14] = context.getResources().getDrawable(R.drawable.dig1);
        sprList[15] = context.getResources().getDrawable(R.drawable.dig2);
        sprList[16] = context.getResources().getDrawable(R.drawable.dig3);
        sprList[17] = context.getResources().getDrawable(R.drawable.dig4);
        sprList[18] = context.getResources().getDrawable(R.drawable.dig5);
        sprList[19] = context.getResources().getDrawable(R.drawable.dig6);
        sprList[20] = context.getResources().getDrawable(R.drawable.dig7);
        sprList[21] = context.getResources().getDrawable(R.drawable.dig8);
        sprList[22] = context.getResources().getDrawable(R.drawable.dig9);
        sprList[23] = context.getResources().getDrawable(R.drawable.cloud1);
        sprList[24] = context.getResources().getDrawable(R.drawable.splash);
        sprList[25] = context.getResources().getDrawable(R.drawable.best);
        sprList[26] = context.getResources().getDrawable(R.drawable.you);
        sprList[27] = context.getResources().getDrawable(R.drawable.touch_try_again);
        sprList[28] = context.getResources().getDrawable(R.drawable.arrow_red);
        sprList[29] = context.getResources().getDrawable(R.drawable.touch_start);
        sprList[30] = context.getResources().getDrawable(R.drawable.lets_go);
        sprList[31] = context.getResources().getDrawable(R.drawable.great_jump);
        sprList[34] = context.getResources().getDrawable(R.drawable.boost);
        sprList[35] = context.getResources().getDrawable(R.drawable.boost_and_star);
        sprList[36] = context.getResources().getDrawable(R.drawable.boost_star);
    }

    public static void SetCanvas(Canvas c) {
        mCanvas = c;
    }

    /* JADX INFO: Multiple debug info for r5v4 int: [D('dfr' int), D('xpos' int)] */
    /* JADX INFO: Multiple debug info for r6v2 int: [D('ypos' int), D('drot' int)] */
    /* JADX INFO: Multiple debug info for r7v5 int: [D('width' int), D('dwidth' int)] */
    /* JADX INFO: Multiple debug info for r5v10 int: [D('height' int), D('dheight' int)] */
    public static void Draw(int fr, int xpos, int ypos, int rot, int scale) {
        int dxpos = GFX.AdjustMetricH(xpos) >> 8;
        int dypos = GFX.AdjustMetricV(ypos) >> 8;
        int xpos2 = (Integer.MAX_VALUE & fr) >> 8;
        int drot = rot >> 8;
        if (fr == -1) {
            b = Ident;
        } else {
            b = sprList[xpos2];
        }
        int width = b.getIntrinsicWidth();
        int height = b.getIntrinsicHeight();
        int dwidth = (width * scale) / scaleTWO;
        int dheight = (height * scale) / scaleTWO;
        if (dwidth < 1) {
            dwidth = 1;
        }
        if (dheight < 1) {
            dheight = 1;
        }
        mCanvas.save();
        if (drot != 0) {
            if ((fr & SPECIAL_CASE_ROTATE) == SPECIAL_CASE_ROTATE) {
                mCanvas.rotate((float) drot, (float) dxpos, (float) ((dypos + dheight) - GFX.AdjustMetricV(11)));
            } else {
                mCanvas.rotate((float) drot, (float) dxpos, (float) dypos);
            }
        }
        b_xleft = dxpos - dwidth;
        b_xright = dxpos + dwidth;
        b_ytop = dypos - dheight;
        b_ybottom = dypos + dheight;
        b.setBounds(b_xleft, b_ytop, b_xright, b_ybottom);
        b.draw(mCanvas);
        mCanvas.restore();
    }

    public static void Draw(int fr, int xpos, int ypos) {
        Draw(fr, xpos, ypos, 0, scaleONE);
    }

    public static void Draw(int fr, int xpos, int ypos, int rot) {
        Draw(fr, xpos, ypos, rot, scaleONE);
    }

    public static int CheckTouchCollision() {
        if (Uita.TouchX <= 0) {
            return 0;
        }
        int scr_x = Uita.TouchX;
        int scr_y = Uita.TouchY;
        if (scr_x < b_xleft || scr_x > b_xright || scr_y < b_ytop || scr_y > b_ybottom) {
            return 0;
        }
        return 1;
    }

    public static void DisplayValue(int score, int xpos, int ypos) {
    }

    public static void DisplayText(String str, int xpos, int ypos) {
    }

    public static void LoadIdent() {
        Ident = UitaView.ViewContext.getResources().getDrawable(R.drawable.ident);
    }

    public static void DrawIdent(int scale) {
        Draw(-1, 61440, 40960, 0, scale);
    }

    public static void FreeIdent() {
        Ident = null;
    }

    public static void ResetSprCt() {
        spr_ct = 0;
    }

    public static void AddSprCt() {
        spr_ct++;
    }

    public static int GetSprCt() {
        return spr_ct;
    }
}
