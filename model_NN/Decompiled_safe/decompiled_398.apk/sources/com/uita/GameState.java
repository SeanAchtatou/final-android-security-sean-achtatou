package com.uita;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class GameState {
    public static Uita AppUita;
    public static int BoostNum;
    public static int BoostPressed;
    public static int DisLog = 0;
    public static int Distance;
    public static int DistanceRecord;
    public static int PerfectTakeOff;
    public static int PerfectTakeOff2;
    public static int PirateX;
    public static int PirateY;
    public static int PirateYVel;
    public static int ShowShipBG = 0;
    public static int SplashX;
    public static int SplashY;
    public static int angle_chosen = 0;
    public static boolean fly_complete = true;
    public static int hs_flag = 0;
    public static int new_high;
    public static int new_high_ct;
    public static int plank_position_chosen = 0;
    public static boolean reset_after_fall = false;
    public static int rxo;
    public static int ryo;
    public static int sea_mode;
    public static int speed_chosen = 0;
    public static int start_run;

    public static void Init() {
    }

    public static void GetPrefs(Context context) {
        Log.v("DEBUG", "GetPrefs()");
        DistanceRecord = context.getSharedPreferences("UitaPrefs", 0).getInt("Highscore", 0);
    }

    public static void SetPrefs(Context context) {
        Log.v("DEBUG", "SetPrefs()");
        SharedPreferences.Editor editor = context.getSharedPreferences("UitaPrefs", 0).edit();
        editor.putInt("Highscore", DistanceRecord);
        editor.commit();
    }

    public static void Log(String ls) {
        if (DisLog == 1) {
            Log.v("DEBUG", ls);
        }
    }
}
