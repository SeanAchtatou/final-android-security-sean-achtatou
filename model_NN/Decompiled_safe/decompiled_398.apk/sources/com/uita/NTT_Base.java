package com.uita;

public class NTT_Base {
    public int TaskType;
    public int fr;
    public int fr_base;
    public int fr_index;
    public int fr_spd;
    public int fr_spd_ct;
    public int rot;
    public int rotvel;
    public int scale;
    public int state = 0;
    public int xpos;
    public int xvel;
    public int ypos;
    public int yvel;

    NTT_Base() {
    }

    public void Run(int d) {
    }

    public int Init(int val) {
        return val << 8;
    }

    public int Inter(int ival) {
        return (Sprite.interUpdate * ival) >> 8;
    }

    public int GeometricProgression(int orig, int gp) {
        if (gp == 0) {
            return orig;
        }
        int rv = (orig * (Inter(gp) + Sprite.scaleONE)) >> 8;
        if (rv == orig) {
            if (gp > 0) {
                rv++;
            } else {
                rv--;
            }
        }
        return rv;
    }
}
