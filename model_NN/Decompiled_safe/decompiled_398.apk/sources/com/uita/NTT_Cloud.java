package com.uita;

public class NTT_Cloud extends NTT_Base {
    NTT_Cloud() {
        this.state = 1;
    }

    public void Run(int c) {
        switch (this.state) {
            case 1:
                this.fr = Init(23);
                this.xpos = GameState.PirateX;
                this.ypos = GameState.PirateY;
                if (Math.random() > 0.5d) {
                    this.xpos += Init(280);
                    this.ypos += Init((int) (((float) (Math.random() * 320.0d)) - 160.0f));
                } else {
                    if (GameState.PirateYVel < 0) {
                        this.ypos -= Init(200);
                    } else {
                        this.ypos += Init(200);
                    }
                    this.xpos += Init((int) (((float) (Math.random() * 480.0d)) - 240.0f));
                }
                this.state = 2;
                return;
            case 2:
                if (Update() == 1) {
                    TaskManager.Remove(this);
                    return;
                } else {
                    Display();
                    return;
                }
            default:
                return;
        }
    }

    private void Display() {
        Sprite.Draw(this.fr, (this.xpos - GameState.PirateX) + Init(240), (this.ypos - GameState.PirateY) + Init(160));
    }

    private int Update() {
        if ((this.xpos - GameState.PirateX) + Init(240) < Init(-32)) {
            return 0 + 1;
        }
        return 0;
    }
}
