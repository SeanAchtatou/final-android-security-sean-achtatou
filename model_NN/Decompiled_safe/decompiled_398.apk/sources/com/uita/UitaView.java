package com.uita;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.admob.android.ads.AdView;
import java.lang.Thread;

class UitaView extends SurfaceView implements SurfaceHolder.Callback {
    public static final float STANDARD_MILLISECONDS_PER_FRAME = 30.0f;
    public static Context ViewContext = null;
    private static Handler handler;
    /* access modifiers changed from: private */
    public static AdView mAd;
    private static UitaThread thread;
    public boolean BackFromPause = false;
    public boolean OC_Flag = false;
    public boolean SIS_Flag = false;
    /* access modifiers changed from: private */
    public Bitmap mBackgroundImage;

    class UitaThread extends Thread {
        public static final int DIFFICULTY_EASY = 0;
        public static final int DIFFICULTY_HARD = 1;
        public static final int DIFFICULTY_MEDIUM = 2;
        private static final String KEY_DIFFICULTY = "mDifficulty";
        private static final String KEY_RECORD = "GameState.DistanceRecord";
        public static final int PHYS_DOWN_ACCEL_SEC = 35;
        public static final int PHYS_FIRE_ACCEL_SEC = 80;
        public static final int PHYS_FUEL_INIT = 60;
        public static final int PHYS_FUEL_MAX = 100;
        public static final int PHYS_FUEL_SEC = 10;
        public static final int PHYS_SLEW_SEC = 120;
        public static final int PHYS_SPEED_HYPERSPACE = 180;
        public static final int PHYS_SPEED_INIT = 30;
        public static final int PHYS_SPEED_MAX = 120;
        public static final int STATE_LOSE = 1;
        public static final int STATE_PAUSE = 2;
        public static final int STATE_READY = 3;
        public static final int STATE_RUNNING = 4;
        public static final int STATE_WIN = 5;
        public static final int TARGET_ANGLE = 18;
        public static final int TARGET_BOTTOM_PADDING = 17;
        public static final int TARGET_PAD_HEIGHT = 8;
        public static final int TARGET_SPEED = 28;
        public static final double TARGET_WIDTH = 1.6d;
        public static final int UI_BAR = 100;
        public static final int UI_BAR_HEIGHT = 10;
        private Context mContext;
        private int mDifficulty;
        private Handler mHandler;
        /* access modifiers changed from: private */
        public long mLastTime;
        private int mMode;
        public boolean mRun = false;
        private SurfaceHolder mSurfaceHolder;

        public UitaThread(SurfaceHolder surfaceHolder, Context context, Handler handler) {
            this.mSurfaceHolder = surfaceHolder;
            this.mHandler = handler;
            this.mContext = context;
        }

        public void doStart() {
            synchronized (this.mSurfaceHolder) {
                this.mLastTime = System.currentTimeMillis() + 100;
            }
        }

        public void pause() {
            synchronized (this.mSurfaceHolder) {
            }
        }

        public synchronized void restoreStateold(Bundle savedState) {
            Log.v("DEBUG", "restoreState() called");
        }

        public void run() {
            Log.v("DEBUG", ">>> Thread.run() ENTRY");
            while (this.mRun) {
                Canvas c = null;
                try {
                    c = this.mSurfaceHolder.lockCanvas(null);
                    synchronized (this.mSurfaceHolder) {
                        updatePhysics();
                        Uita.UpdateInput();
                        TaskManager.Update(c);
                    }
                } finally {
                    if (c != null) {
                        this.mSurfaceHolder.unlockCanvasAndPost(c);
                    }
                }
            }
            Log.v("DEBUG", ">>>Thread.run() EXIT");
        }

        public Bundle saveStateold(Bundle map) {
            Log.v("DEBUG", "saveState() called");
            return map;
        }

        public void setDifficulty(int difficulty) {
            synchronized (this.mSurfaceHolder) {
                this.mDifficulty = difficulty;
            }
        }

        public void setFiring(boolean firing) {
            synchronized (this.mSurfaceHolder) {
            }
        }

        public void setRunning(boolean b) {
            this.mRun = b;
        }

        public void setSurfaceSize(int width, int height) {
            synchronized (this.mSurfaceHolder) {
            }
        }

        public void unpause() {
            synchronized (this.mSurfaceHolder) {
                this.mLastTime = System.currentTimeMillis() + 100;
            }
        }

        /* access modifiers changed from: package-private */
        /* JADX WARNING: Code restructure failed: missing block: B:61:?, code lost:
            return true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:62:?, code lost:
            return true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:64:?, code lost:
            return false;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean doKeyDown(int r7, android.view.KeyEvent r8) {
            /*
                r6 = this;
                r5 = 19
                r4 = 1
                android.view.SurfaceHolder r1 = r6.mSurfaceHolder
                monitor-enter(r1)
                r0 = 0
                if (r7 != r5) goto L_0x000a
                r0 = 1
            L_0x000a:
                r2 = 20
                if (r7 != r2) goto L_0x000f
                r0 = 1
            L_0x000f:
                r2 = 47
                if (r7 != r2) goto L_0x0014
                r0 = 1
            L_0x0014:
                if (r0 == 0) goto L_0x002a
                int r2 = r6.mMode     // Catch:{ all -> 0x006c }
                r3 = 3
                if (r2 == r3) goto L_0x0024
                int r2 = r6.mMode     // Catch:{ all -> 0x006c }
                if (r2 == r4) goto L_0x0024
                int r2 = r6.mMode     // Catch:{ all -> 0x006c }
                r3 = 5
                if (r2 != r3) goto L_0x002a
            L_0x0024:
                r6.doStart()     // Catch:{ all -> 0x006c }
                monitor-exit(r1)     // Catch:{ all -> 0x006c }
                r1 = r4
            L_0x0029:
                return r1
            L_0x002a:
                int r2 = r6.mMode     // Catch:{ all -> 0x006c }
                r3 = 2
                if (r2 != r3) goto L_0x0037
                if (r0 == 0) goto L_0x0037
                r6.unpause()     // Catch:{ all -> 0x006c }
                monitor-exit(r1)     // Catch:{ all -> 0x006c }
                r1 = r4
                goto L_0x0029
            L_0x0037:
                int r2 = r6.mMode     // Catch:{ all -> 0x006c }
                r3 = 4
                if (r2 != r3) goto L_0x0069
                r2 = 23
                if (r7 == r2) goto L_0x0044
                r2 = 62
                if (r7 != r2) goto L_0x004b
            L_0x0044:
                r2 = 1
                r6.setFiring(r2)     // Catch:{ all -> 0x006c }
                monitor-exit(r1)     // Catch:{ all -> 0x006c }
                r1 = r4
                goto L_0x0029
            L_0x004b:
                r2 = 21
                if (r7 == r2) goto L_0x0053
                r2 = 45
                if (r7 != r2) goto L_0x0056
            L_0x0053:
                monitor-exit(r1)     // Catch:{ all -> 0x006c }
                r1 = r4
                goto L_0x0029
            L_0x0056:
                r2 = 22
                if (r7 == r2) goto L_0x005e
                r2 = 51
                if (r7 != r2) goto L_0x0061
            L_0x005e:
                monitor-exit(r1)     // Catch:{ all -> 0x006c }
                r1 = r4
                goto L_0x0029
            L_0x0061:
                if (r7 != r5) goto L_0x0069
                r6.pause()     // Catch:{ all -> 0x006c }
                monitor-exit(r1)     // Catch:{ all -> 0x006c }
                r1 = r4
                goto L_0x0029
            L_0x0069:
                monitor-exit(r1)     // Catch:{ all -> 0x006c }
                r1 = 0
                goto L_0x0029
            L_0x006c:
                r2 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x006c }
                throw r2
            */
            throw new UnsupportedOperationException("Method not decompiled: com.uita.UitaView.UitaThread.doKeyDown(int, android.view.KeyEvent):boolean");
        }

        /* access modifiers changed from: package-private */
        public boolean doKeyUp(int keyCode, KeyEvent msg) {
            boolean handled = false;
            synchronized (this.mSurfaceHolder) {
                if (this.mMode == 4) {
                    if (keyCode == 23 || keyCode == 62) {
                        setFiring(false);
                        handled = true;
                    } else if (keyCode == 21 || keyCode == 45 || keyCode == 22 || keyCode == 51) {
                        handled = true;
                    }
                }
            }
            return handled;
        }

        private void doDraw(Canvas canvas) {
            if (GameState.ShowShipBG == 1) {
                Uita.UpdateBG_YOffset();
                canvas.drawBitmap(UitaView.this.mBackgroundImage, 0.0f, (float) (-Uita.GetBG_YOffset()), (Paint) null);
            }
        }

        private void updatePhysics() {
            long now = System.currentTimeMillis();
            if (this.mLastTime > now) {
                Sprite.interUpdate = 0;
                return;
            }
            Sprite.interUpdate = (int) (256.0f * (((float) (now - this.mLastTime)) / 30.0f));
            this.mLastTime = now;
        }
    }

    public UitaView(Context context, AttributeSet attrs) {
        super(context, attrs);
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        ViewContext = context;
        handler = new Handler() {
            private boolean first = true;

            public void handleMessage(Message m) {
                super.handleMessage(m);
                if (m.arg1 == 1) {
                    if (Uita.ShowAds == 1) {
                        UitaView.mAd.setVisibility(0);
                        UitaView.mAd.requestFreshAd();
                        UitaView.mAd.setRequestInterval(13);
                    }
                } else if (m.arg1 == 0) {
                    UitaView.mAd.setVisibility(4);
                } else if (m.arg1 == 2) {
                    if (Uita.ShowAds == 1) {
                        UitaView.mAd.requestFreshAd();
                    }
                } else if (m.arg1 == 999) {
                    GameState.AppUita.ShowScore(GameState.Distance);
                }
            }
        };
        Log.v("DEBUG", "creating new thread");
        thread = new UitaThread(holder, context, handler);
        setFocusable(true);
        setFocusableInTouchMode(true);
        if (!this.SIS_Flag) {
            Log.v("DEBUG", "Initialise TASKMANAGER, SPRITE, AUDIO!!");
            TaskManager.Init(0);
            this.mBackgroundImage = BitmapFactory.decodeResource(context.getResources(), R.drawable.shipbg1);
        }
    }

    public static UitaThread getThread() {
        return thread;
    }

    public static Handler getMyHandler() {
        return handler;
    }

    public boolean onKeyDown(int keyCode, KeyEvent msg) {
        return thread.doKeyDown(keyCode, msg);
    }

    public boolean onKeyUp(int keyCode, KeyEvent msg) {
        return thread.doKeyUp(keyCode, msg);
    }

    public void onWindowFocusChanged(boolean hasWindowFocus) {
    }

    public void setTextView(AdView adView) {
        mAd = adView;
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        thread.setSurfaceSize(width, height);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        Log.v("DEBUG", "surfaceCreated");
        Log.v("DEBUG", "Previously paused:" + this.BackFromPause);
        Log.v("DEBUG", "OC_Flag:" + this.OC_Flag);
        boolean nt = false;
        if (this.OC_Flag) {
            this.OC_Flag = false;
            if (!this.BackFromPause) {
                nt = true;
            }
        } else {
            nt = true;
        }
        if (nt) {
            if (thread.getState() == Thread.State.TERMINATED) {
                Log.v("DEBUG", "Back to app : new thread");
                thread = null;
                thread = new UitaThread(holder, getContext(), new Handler());
            } else {
                Log.v("DEBUG", "New app : start thread");
            }
            thread.mLastTime = System.currentTimeMillis() + 100;
            thread.setRunning(true);
            thread.start();
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.v("DEBUG", "surfaceDestroy() called");
        boolean retry = true;
        thread.setRunning(false);
        while (retry) {
            try {
                thread.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }
    }

    public static void AdsOn() {
        Message m = new Message();
        m.arg1 = 1;
        handler.sendMessage(m);
    }

    public static void AdsOff() {
        Message m = new Message();
        m.arg1 = 0;
        handler.sendMessage(m);
    }

    public static void RequestNewAd() {
        Message m = new Message();
        m.arg1 = 2;
        handler.sendMessage(m);
    }

    public static void Scores() {
        Message m = new Message();
        m.arg1 = 999;
        handler.sendMessage(m);
    }
}
