package com.uita;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import com.admob.android.ads.AdView;
import com.scoreninja.adapter.ScoreNinjaAdapter;
import com.uita.UitaView;

public class Uita extends Activity implements View.OnTouchListener {
    public static final String AppID = "uitademo19072011";
    public static int BG_YOffset = 0;
    private static int BG_YOffset_Pos = 0;
    public static int NumJumps = 0;
    public static final String PrivateKey = "577B03685936E579EECD3C194FE23902";
    public static int ShowAds = 1;
    public static int TouchX = -100;
    public static int TouchY = 0;
    public static int Touched = 0;
    public static int TouchedDebounce = 0;
    public static int Touched_old = 0;
    public static final String UitaPrefs = "uitaprefs";
    public static AdView ad;
    private static int[] bgoffsets;
    private static int genct = 0;
    public static UitaView.UitaThread mUitaThread;
    private static Context myContext = null;
    public static float pitch;
    public static float roll;
    public static float yaw;
    public float fUpdate;
    private UitaView mUitaView;
    private MediaPlayer mp;
    public ScoreNinjaAdapter scoreNinjaAdapter;

    static {
        int[] iArr = new int[30];
        iArr[4] = 1;
        iArr[5] = 1;
        iArr[6] = 1;
        iArr[7] = 2;
        iArr[8] = 2;
        iArr[9] = 3;
        iArr[10] = 4;
        iArr[11] = 4;
        iArr[12] = 5;
        iArr[13] = 5;
        iArr[14] = 5;
        iArr[15] = 6;
        iArr[16] = 6;
        iArr[17] = 6;
        iArr[18] = 6;
        iArr[19] = 5;
        iArr[20] = 5;
        iArr[21] = 5;
        iArr[22] = 4;
        iArr[23] = 4;
        iArr[24] = 3;
        iArr[25] = 2;
        iArr[26] = 2;
        iArr[27] = 1;
        iArr[28] = 1;
        iArr[29] = 1;
        bgoffsets = iArr;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        return false;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.v("DEBUG", ">>>> onCreate() called");
        if (savedInstanceState == null) {
            Log.v("DEBUG", ">>>>                            SIS NULL");
        } else {
            Log.v("DEBUG", ">>>>                            SIS NON-NULL");
        }
        requestWindowFeature(1);
        setContentView((int) R.layout.uita_layout);
        this.mUitaView = (UitaView) findViewById(R.id.uita);
        mUitaThread = UitaView.getThread();
        this.scoreNinjaAdapter = new ScoreNinjaAdapter(this, AppID, PrivateKey);
        GameState.AppUita = this;
        GFX.SetDisplayMetrics();
        ad = (AdView) findViewById(R.id.ad);
        this.mUitaView.setTextView(ad);
        GameState.GetPrefs(this);
        if (savedInstanceState == null) {
            this.mUitaView.SIS_Flag = false;
        } else {
            this.mUitaView.SIS_Flag = true;
        }
        this.mUitaView.OC_Flag = true;
        if (this.mUitaView.BackFromPause && this.mUitaView.SIS_Flag) {
            finish();
        }
        this.mUitaView.setOnTouchListener(this);
        this.mp = MediaPlayer.create(this, (int) R.raw.drunkensailor);
        this.mp.setVolume(0.2f, 0.2f);
        this.mp.setLooping(true);
        this.mp.start();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        GameState.Log("OnActivityResult called");
        super.onActivityResult(requestCode, resultCode, data);
        this.scoreNinjaAdapter.onActivityResult(requestCode, resultCode, data);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        GameState.SetPrefs(this);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle inState) {
        super.onRestoreInstanceState(inState);
        GameState.GetPrefs(this);
    }

    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case 0:
                TouchX = (int) event.getX();
                TouchY = (int) event.getY();
                Touched = 1;
                GameState.Log("pressed screen at " + TouchX + "," + TouchY);
                break;
            case 1:
                Touched = 0;
                break;
            case 2:
                TouchX = (int) event.getX();
                TouchY = (int) event.getY();
                break;
        }
        try {
            Thread.sleep(32);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static void UpdateInput() {
        int pn = Touched;
        TouchedDebounce = (Touched_old ^ -1) & pn;
        Touched_old = pn;
    }

    public static int GetPressed() {
        return Touched;
    }

    public static int GetPressedDebounce() {
        return TouchedDebounce;
    }

    public static int GetX() {
        return TouchX;
    }

    public static int GetY() {
        return TouchY;
    }

    public static void UpdateBG_YOffset() {
        genct++;
        if (genct > 2) {
            genct = 0;
            BG_YOffset_Pos++;
            if (BG_YOffset_Pos >= 30) {
                BG_YOffset_Pos = 0;
            }
            BG_YOffset = bgoffsets[BG_YOffset_Pos];
        }
    }

    public static int GetBG_YOffset() {
        return BG_YOffset;
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        Log.v("DEBUG", ">>>> onStart() called");
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        Log.v("DEBUG", ">>>> onRestart() called");
        super.onRestart();
        this.mp.start();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        Log.v("DEBUG", ">>>> onResume() called");
        GameState.GetPrefs(this);
        super.onResume();
        if (GameState.hs_flag > 0) {
            GameState.hs_flag--;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        Log.v("DEBUG", ">>>> onPause() called");
        GameState.SetPrefs(this);
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        Log.v("DEBUG", ">>>> onStop() called");
        super.onStop();
        UitaView.getThread().setRunning(false);
        this.mUitaView.BackFromPause = true;
        this.mp.pause();
        GameState.hs_flag = 0;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        Log.v("DEBUG", ">>>> onDestroy() called");
        super.onDestroy();
    }

    public void ShowScore(int score) {
        this.scoreNinjaAdapter.show(score);
    }
}
