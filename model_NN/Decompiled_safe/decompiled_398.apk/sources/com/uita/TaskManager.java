package com.uita;

import android.content.Context;
import android.graphics.Canvas;
import android.util.Log;

public class TaskManager {
    public static boolean InitFlag = false;
    private static final int MAX_TASK = 64;
    private static NTT_Base[] TaskList = new NTT_Base[MAX_TASK];
    public static final int _NTT_BOOST = 10;
    public static final int _NTT_BOOSTSTAR = 11;
    public static final int _NTT_CLOUD = 8;
    public static final int _NTT_CONTROL = 1;
    public static final int _NTT_CUTLASS = 7;
    public static final int _NTT_ROCK = 5;
    public static final int _NTT_ROCK_CONTROL = 4;
    public static final int _NTT_SEA = 6;
    public static final int _NTT_SPLASH = 9;
    public static final int _NTT_TEST = 2;
    public static final int _NTT_TEST2 = 3;
    private static Canvas mCanvas;
    private static Context mContext;

    public static void Init(int ignore) {
        if (!InitFlag) {
            for (int i = 0; i < MAX_TASK; i++) {
                TaskList[i] = null;
            }
            Log.v("DEBUG", ">>>>>>>>>>>>>Add control task");
            Add(1);
            InitFlag = true;
        }
    }

    public static void Update(Canvas c) {
        Sprite.SetCanvas(c);
        RunAll(c);
    }

    public static void RunAll(Canvas c) {
        int num_running = 0;
        for (int i = 0; i < MAX_TASK; i++) {
            if (TaskList[i] != null) {
                TaskList[i].Run(0);
                num_running++;
            }
        }
    }

    public static void Add(int taskdesc) {
        int task = -1;
        int i = 0;
        while (true) {
            if (i >= MAX_TASK) {
                break;
            } else if (TaskList[i] == null) {
                task = i;
                break;
            } else {
                i++;
            }
        }
        if (task != -1) {
            DoAdd(task, taskdesc);
        } else {
            Log.v("DEBUG", "TASKLIST FULL");
        }
    }

    public static void AddEnd(int taskdesc) {
        int task = -1;
        int i = 63;
        while (true) {
            if (i <= 0) {
                break;
            } else if (TaskList[i] == null) {
                task = i;
                break;
            } else {
                i--;
            }
        }
        if (task != -1) {
            DoAdd(task, taskdesc);
        } else {
            Log.v("DEBUG", "TASKLIST FULL");
        }
    }

    private static void DoAdd(int task, int taskdesc) {
        if (task >= 0) {
            switch (taskdesc) {
                case 1:
                    TaskList[task] = null;
                    TaskList[task] = new NTT_Control();
                    TaskList[task].TaskType = taskdesc;
                    return;
                case 2:
                    TaskList[task] = null;
                    TaskList[task] = new TestNTT();
                    TaskList[task].TaskType = taskdesc;
                    return;
                case 3:
                    TaskList[task] = null;
                    TaskList[task] = new TestNTT2();
                    TaskList[task].TaskType = taskdesc;
                    return;
                case 4:
                case 5:
                default:
                    return;
                case 6:
                    TaskList[task] = null;
                    TaskList[task] = new NTT_Sea();
                    TaskList[task].TaskType = taskdesc;
                    return;
                case 7:
                    TaskList[task] = null;
                    TaskList[task] = new NTT_Cutlass();
                    TaskList[task].TaskType = taskdesc;
                    return;
                case 8:
                    TaskList[task] = null;
                    TaskList[task] = new NTT_Cloud();
                    TaskList[task].TaskType = taskdesc;
                    return;
                case 9:
                    TaskList[task] = null;
                    TaskList[task] = new NTT_Splash();
                    TaskList[task].TaskType = taskdesc;
                    return;
                case 10:
                    TaskList[task] = null;
                    TaskList[task] = new NTT_Boost();
                    TaskList[task].TaskType = taskdesc;
                    return;
                case 11:
                    TaskList[task] = null;
                    TaskList[task] = new NTT_BoostStar();
                    TaskList[task].TaskType = taskdesc;
                    return;
            }
        }
    }

    public static void Remove(NTT_Base task) {
        for (int i = 0; i < MAX_TASK; i++) {
            if (TaskList[i] == task) {
                TaskList[i] = null;
                return;
            }
        }
    }

    public static void RemoveByType(int type) {
        for (int i = 0; i < MAX_TASK; i++) {
            if (TaskList[i] != null && TaskList[i].TaskType == type) {
                TaskList[i] = null;
            }
        }
    }
}
