package com.uita;

import android.graphics.Canvas;
import android.view.Display;

public class GFX {
    public static int DisFPS = 0;
    public static int NORMAL_SCR_HEIGHT = 320;
    public static int NORMAL_SCR_WIDTH = 480;
    public static final int char_width = 20;
    public static int char_width_dis;
    private static int cur_fps = 0;
    private static int fps_ct = 0;
    public static int large_display;
    public static int scr_height;
    public static int scr_width;
    public static int x_adjust;
    public static int y_adjust;

    public static void DisplayValFromString(String str, int xpos, int ypos, int flags) {
        int numchar = str.length();
        int xpos2 = xpos - ((numchar * 20) / 2);
        for (int i = 0; i < numchar; i++) {
            char c = (char) (str.charAt(i) - '0');
            if (c >= 0 && c <= 9) {
                Sprite.Draw(((char) (c + 13)) << 8, xpos2 << 8, ypos << 8);
                xpos2 += 20;
            }
        }
    }

    public static void DisplayVal(int val, int xpos, int ypos, int flags) {
        DisplayValFromString(String.valueOf(val), xpos, ypos, flags);
    }

    public static void DisplayDistance() {
        Sprite.Draw(6400, 20480, 10240);
        int dis = 1;
        if (GameState.new_high == 1) {
            GameState.new_high_ct++;
            if ((GameState.new_high_ct & 16) == 16) {
                dis = 0;
            }
        }
        if (dis == 1) {
            DisplayVal(GameState.DistanceRecord, (20480 >> 8) + 12, (10240 >> 8) + 32, 0);
        }
        Sprite.Draw(6656, 102400, 10240);
        if (dis == 1) {
            DisplayVal(GameState.Distance, (102400 >> 8) + 12, (10240 >> 8) + 32, 0);
        }
    }

    public static void SetDisplayMetrics() {
        Display d = GameState.AppUita.getWindowManager().getDefaultDisplay();
        scr_width = d.getWidth();
        scr_height = d.getHeight();
        x_adjust = Sprite.scaleONE;
        y_adjust = Sprite.scaleONE;
        char_width_dis = 20;
        GameState.Log("width " + scr_width);
        GameState.Log("height " + scr_height);
        large_display = 0;
        if (scr_width != NORMAL_SCR_WIDTH || scr_height != NORMAL_SCR_HEIGHT) {
            GameState.Log("Large Screen!");
            large_display = 1;
            x_adjust = (scr_width * Sprite.scaleONE) / NORMAL_SCR_WIDTH;
            y_adjust = (scr_height * Sprite.scaleONE) / NORMAL_SCR_HEIGHT;
            GameState.Log("wid_adjust: " + x_adjust);
            GameState.Log("hgt_adjust: " + y_adjust);
        }
    }

    public static int AdjustMetricH(int val) {
        if (large_display == 0) {
            return val;
        }
        return (val * x_adjust) / Sprite.scaleONE;
    }

    public static int AdjustMetricV(int val) {
        if (large_display == 0) {
            return val;
        }
        return (val * y_adjust) / Sprite.scaleONE;
    }

    public static void StripForLargeScreens(Canvas c) {
        if (large_display == 1) {
            Sprite.Draw(8448, 140544, 40960);
        }
    }

    public static void DisplayFPS() {
        if (DisFPS == 1) {
            if (fps_ct == 0) {
                int ciu = Sprite.interUpdate;
                if (ciu == 0) {
                    ciu = 1;
                }
                cur_fps = (((int) 33.333332f) * Sprite.scaleONE) / ciu;
            }
            DisplayVal(cur_fps, 460, 32, 0);
            fps_ct = fps_ct < 8 ? fps_ct + 1 : 0;
        }
    }
}
