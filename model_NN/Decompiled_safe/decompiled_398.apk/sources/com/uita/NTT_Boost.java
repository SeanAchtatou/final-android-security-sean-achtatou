package com.uita;

public class NTT_Boost extends NTT_Base {
    private int animct = 0;
    private int b_ct;
    private int b_mod;
    private int be_ct = 0;
    private int scale_spd;

    NTT_Boost() {
        this.state = 1;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void Run(int c) {
        switch (this.state) {
            case 1:
                this.state = 2;
                this.be_ct = 0;
                break;
            case 2:
                break;
            case 10:
                Display();
                if (Update() == 1) {
                    GameState.BoostPressed = 1;
                    this.state = 20;
                    return;
                }
                return;
            case 20:
                GameState.BoostNum--;
                if (GameState.BoostNum == 0) {
                    TaskManager.Remove(this);
                    return;
                }
                Display();
                this.state = 2;
                return;
            default:
                return;
        }
        GameState.Log("Starting boost");
        this.scale = 32;
        this.xpos = Init(410);
        this.ypos = Init(270);
        this.fr = Init(35);
        this.scale_spd = 8;
        this.state = 10;
        Display();
    }

    private void Display() {
        for (int i = 0; i < GameState.BoostNum; i++) {
            Sprite.Draw(Init(7), (this.xpos - Init(24)) + (Init(24) * i), this.ypos - Init(64));
        }
        Sprite.Draw(this.fr, this.xpos, this.ypos, 0, this.scale);
    }

    private int Update() {
        this.scale += this.scale_spd;
        if (this.scale > 256) {
            this.scale = Sprite.scaleONE;
        }
        this.animct += 32;
        this.fr = ((this.animct >> 5) & 8) == 8 ? Init(35) : Init(34);
        if (Uita.GetPressedDebounce() == 1 && this.scale == 256 && Sprite.CheckTouchCollision() == 1) {
            return 1;
        }
        return 0;
    }
}
