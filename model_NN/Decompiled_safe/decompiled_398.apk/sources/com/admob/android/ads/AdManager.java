package com.admob.android.ads;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import com.admob.android.ads.InterstitialAd;
import com.admob.android.ads.j;
import com.google.ads.AdActivity;
import com.uita.Sprite;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;

public class AdManager {
    public static final String LOG = "AdMobSDK";
    public static final String SDK_VERSION = "20101109-ANDROID-3312276cc1406347";
    public static final String SDK_VERSION_DATE = "20101109";
    public static final String TEST_EMULATOR = "emulator";
    private static String a;
    private static int b;
    private static String c;
    private static String d;
    private static String e = j.a.CLICK_TO_BROWSER.toString();
    private static String[] f = null;
    private static String g;
    /* access modifiers changed from: private */
    public static Location h;
    private static boolean i = false;
    private static boolean j = false;
    /* access modifiers changed from: private */
    public static long k;
    private static String l;
    private static GregorianCalendar m;
    private static Gender n;
    private static boolean o = false;
    private static Boolean p = null;

    public enum Gender {
        MALE,
        FEMALE
    }

    static {
        if (InterstitialAd.c.a(LOG, 4)) {
            Log.i(LOG, "AdMob SDK version is 20101109-ANDROID-3312276cc1406347");
        }
    }

    private AdManager() {
    }

    protected static void clientError(String str) {
        if (InterstitialAd.c.a(LOG, 6)) {
            Log.e(LOG, str);
        }
        throw new IllegalArgumentException(str);
    }

    protected static int getScreenWidth(Context context) {
        Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        if (defaultDisplay != null) {
            return defaultDisplay.getWidth();
        }
        return 0;
    }

    static void a(Context context) {
        if (!o) {
            o = true;
            try {
                PackageManager packageManager = context.getPackageManager();
                String packageName = context.getPackageName();
                ApplicationInfo applicationInfo = packageManager.getApplicationInfo(packageName, Sprite.scaleHALF);
                if (applicationInfo != null) {
                    if (applicationInfo.metaData != null) {
                        String a2 = a(applicationInfo.metaData, "ADMOB_PUBLISHER_ID", c);
                        if (a2 != null) {
                            setPublisherId(a2);
                        }
                        String a3 = a(applicationInfo.metaData, "ADMOB_INTERSTITIAL_PUBLISHER_ID", d);
                        if (a3 != null) {
                            setInterstitialPublisherId(a3);
                        }
                        if (!j) {
                            i = applicationInfo.metaData.getBoolean("ADMOB_ALLOW_LOCATION_FOR_ADS", false);
                        }
                    }
                    a = applicationInfo.packageName;
                    if (c != null) {
                        a(c);
                    }
                    if (d != null) {
                        a(d);
                    }
                    if (InterstitialAd.c.a(LOG, 2)) {
                        Log.v(LOG, "Application's package name is " + a);
                    }
                }
                PackageInfo packageInfo = packageManager.getPackageInfo(packageName, 0);
                if (packageInfo != null) {
                    b = packageInfo.versionCode;
                    if (InterstitialAd.c.a(LOG, 2)) {
                        Log.v(LOG, "Application's version number is " + b);
                    }
                }
            } catch (Exception e2) {
            }
        }
    }

    private static String a(Bundle bundle, String str, String str2) {
        String string = bundle.getString(str);
        if (InterstitialAd.c.a(LOG, 3)) {
            Log.d(LOG, "Publisher ID read from AndroidManifest.xml is " + string);
        }
        if (str2 != null || string == null) {
            return null;
        }
        return string;
    }

    public static String getApplicationPackageName(Context context) {
        if (a == null) {
            a(context);
        }
        return a;
    }

    protected static int getApplicationVersion(Context context) {
        if (a == null) {
            a(context);
        }
        return b;
    }

    public static String getPublisherId(Context context) {
        if (c == null) {
            a(context);
        }
        if (c == null && InterstitialAd.c.a(LOG, 6)) {
            Log.e(LOG, "getPublisherId returning null publisher id.  Please set the publisher id in AndroidManifest.xml or using AdManager.setPublisherId(String)");
        }
        return c;
    }

    public static String getInterstitialPublisherId(Context context) {
        if (d == null) {
            a(context);
        }
        if (d == null && InterstitialAd.c.a(LOG, 6)) {
            Log.e(LOG, "getInterstitialPublisherId returning null publisher id.  Please set the publisher id in AndroidManifest.xml or using AdManager.setPublisherId(String)");
        }
        return d;
    }

    public static void setPublisherId(String str) {
        a(str);
        if (InterstitialAd.c.a(LOG, 4)) {
            Log.i(LOG, "Publisher ID set to " + str);
        }
        c = str;
    }

    public static void setInterstitialPublisherId(String str) {
        a(str);
        if (InterstitialAd.c.a(LOG, 4)) {
            Log.i(LOG, "Interstitial Publisher ID set to " + str);
        }
        d = str;
    }

    private static void a(String str) {
        if (str == null || str.length() != 15) {
            clientError("SETUP ERROR:  Incorrect AdMob publisher ID.  Should 15 [a-f,0-9] characters:  " + c);
        }
        if (a != null && str.equalsIgnoreCase("a1496ced2842262") && !"com.admob.android.ads".equals(a) && !"com.example.admob.lunarlander".equals(a)) {
            clientError("SETUP ERROR:  Cannot use the sample publisher ID (a1496ced2842262).  Yours is available on www.admob.com.");
        }
    }

    public static String getTestAction() {
        return e;
    }

    public static void setTestDevices(String[] strArr) {
        if (strArr == null) {
            f = null;
            return;
        }
        String[] strArr2 = (String[]) strArr.clone();
        f = strArr2;
        Arrays.sort(strArr2);
    }

    static String[] getTestDevices() {
        return f;
    }

    public static boolean isTestDevice(Context context) {
        if (f == null) {
            return false;
        }
        String userId = getUserId(context);
        if (userId == null) {
            userId = TEST_EMULATOR;
        }
        if (Arrays.binarySearch(f, userId) >= 0) {
            return true;
        }
        return false;
    }

    public static void setTestAction(String str) {
        e = str;
    }

    public static boolean isEmulator() {
        return "unknown".equals(Build.BOARD) && "generic".equals(Build.DEVICE) && "generic".equals(Build.BRAND);
    }

    public static String getUserId(Context context) {
        if (g == null) {
            String string = Settings.Secure.getString(context.getContentResolver(), "android_id");
            if (string == null || isEmulator()) {
                g = TEST_EMULATOR;
                Log.i(LOG, "To get test ads on the emulator use AdManager.setTestDevices( new String[] { AdManager.TEST_EMULATOR } )");
            } else {
                g = md5(string);
                Log.i(LOG, "To get test ads on this device use AdManager.setTestDevices( new String[] { \"" + g + "\" } )");
            }
            if (InterstitialAd.c.a(LOG, 3)) {
                Log.d(LOG, "The user ID is " + g);
            }
        }
        if (g == TEST_EMULATOR) {
            return null;
        }
        return g;
    }

    protected static String md5(String str) {
        if (str == null || str.length() <= 0) {
            return null;
        }
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes(), 0, str.length());
            return String.format("%032X", new BigInteger(1, instance.digest()));
        } catch (Exception e2) {
            if (InterstitialAd.c.a(LOG, 3)) {
                Log.d(LOG, "Could not generate hash of " + str, e2);
            }
            return str.substring(0, 32);
        }
    }

    public static void setAllowUseOfLocation(boolean z) {
        j = true;
        i = z;
    }

    public static Location getCoordinates(Context context) {
        String str;
        final LocationManager locationManager;
        boolean z;
        if (isEmulator() && !i && InterstitialAd.c.a(LOG, 4)) {
            Log.i(LOG, "Location information is not being used for ad requests. Enable location");
            Log.i(LOG, "based ads with AdManager.setAllowUseOfLocation(true) or by setting ");
            Log.i(LOG, "meta-data ADMOB_ALLOW_LOCATION_FOR_ADS to true in AndroidManifest.xml");
        }
        if (i && context != null && (h == null || System.currentTimeMillis() > k + 900000)) {
            synchronized (context) {
                if (h == null || System.currentTimeMillis() > k + 900000) {
                    k = System.currentTimeMillis();
                    if (context.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
                        if (InterstitialAd.c.a(LOG, 3)) {
                            Log.d(LOG, "Trying to get locations from the network.");
                        }
                        locationManager = (LocationManager) context.getSystemService("location");
                        if (locationManager != null) {
                            Criteria criteria = new Criteria();
                            criteria.setAccuracy(2);
                            criteria.setCostAllowed(false);
                            str = locationManager.getBestProvider(criteria, true);
                            z = true;
                        } else {
                            str = null;
                            z = true;
                        }
                    } else {
                        str = null;
                        locationManager = null;
                        z = false;
                    }
                    if (str == null && context.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0) {
                        if (InterstitialAd.c.a(LOG, 3)) {
                            Log.d(LOG, "Trying to get locations from GPS.");
                        }
                        locationManager = (LocationManager) context.getSystemService("location");
                        if (locationManager != null) {
                            Criteria criteria2 = new Criteria();
                            criteria2.setAccuracy(1);
                            criteria2.setCostAllowed(false);
                            str = locationManager.getBestProvider(criteria2, true);
                            z = true;
                        } else {
                            z = true;
                        }
                    }
                    if (!z) {
                        if (InterstitialAd.c.a(LOG, 3)) {
                            Log.d(LOG, "Cannot access user's location.  Permissions are not set.");
                        }
                    } else if (str != null) {
                        if (InterstitialAd.c.a(LOG, 3)) {
                            Log.d(LOG, "Location provider setup successfully.");
                        }
                        locationManager.requestLocationUpdates(str, 0, 0.0f, new LocationListener() {
                            public final void onLocationChanged(Location location) {
                                Location unused = AdManager.h = location;
                                long unused2 = AdManager.k = System.currentTimeMillis();
                                locationManager.removeUpdates(this);
                                if (InterstitialAd.c.a(AdManager.LOG, 3)) {
                                    Log.d(AdManager.LOG, "Acquired location " + AdManager.h.getLatitude() + "," + AdManager.h.getLongitude() + " at " + new Date(AdManager.k).toString() + ".");
                                }
                            }

                            public final void onProviderDisabled(String str) {
                            }

                            public final void onProviderEnabled(String str) {
                            }

                            public final void onStatusChanged(String str, int i, Bundle bundle) {
                            }
                        }, context.getMainLooper());
                    } else if (InterstitialAd.c.a(LOG, 3)) {
                        Log.d(LOG, "No location providers are available.  Ads will not be geotargeted.");
                    }
                }
            }
        }
        return h;
    }

    static String b(Context context) {
        String str = null;
        Location coordinates = getCoordinates(context);
        if (coordinates != null) {
            str = coordinates.getLatitude() + "," + coordinates.getLongitude();
        }
        if (InterstitialAd.c.a(LOG, 3)) {
            Log.d(LOG, "User coordinates are " + str);
        }
        return str;
    }

    static String a() {
        return String.valueOf(k / 1000);
    }

    public static String getPostalCode() {
        return l;
    }

    public static String getOrientation(Context context) {
        if (((WindowManager) context.getSystemService("window")).getDefaultDisplay().getOrientation() == 1) {
            return "l";
        }
        return "p";
    }

    public static void setPostalCode(String str) {
        l = str;
    }

    public static GregorianCalendar getBirthday() {
        return m;
    }

    static String b() {
        GregorianCalendar birthday = getBirthday();
        if (birthday == null) {
            return null;
        }
        return String.format("%04d%02d%02d", Integer.valueOf(birthday.get(1)), Integer.valueOf(birthday.get(2) + 1), Integer.valueOf(birthday.get(5)));
    }

    public static void setBirthday(GregorianCalendar gregorianCalendar) {
        m = gregorianCalendar;
    }

    public static void setBirthday(int i2, int i3, int i4) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.set(i2, i3 - 1, i4);
        setBirthday(gregorianCalendar);
    }

    public static Gender getGender() {
        return n;
    }

    static String c() {
        if (n == Gender.MALE) {
            return AdActivity.TYPE_PARAM;
        }
        if (n == Gender.FEMALE) {
            return "f";
        }
        return null;
    }

    public static void setGender(Gender gender) {
        n = gender;
    }

    static void setEndpoint(String str) {
        b.a(str);
    }

    static String getEndpoint() {
        return b.a();
    }

    static a a(v vVar) {
        int a2 = vVar.a();
        if (isEmulator()) {
            return a.EMULATOR;
        }
        if (vVar.b() || vVar.c() || a2 == 2 || a2 == 1) {
            return a.VIBRATE;
        }
        int d2 = vVar.d();
        if (d2 == 0 || d2 == 1) {
            return a.VIBRATE;
        }
        return a.SPEAKER;
    }
}
