package jp.co.arttec.satbox.SeaFly.parts;

import android.content.Context;
import jp.co.arttec.satbox.SeaFly.R;
import jp.co.arttec.satbox.SeaFly.util.Item;
import jp.co.arttec.satbox.SeaFly.util.Position;

public class ShieldItem extends ItemBase {
    public ShieldItem(Position position, Context context) {
        super(position, context);
        setImage(context.getResources(), R.drawable.shield_item);
        setItem(Item.Shield);
    }
}
