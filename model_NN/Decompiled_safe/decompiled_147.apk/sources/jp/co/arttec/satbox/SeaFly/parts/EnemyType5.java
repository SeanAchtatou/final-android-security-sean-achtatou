package jp.co.arttec.satbox.SeaFly.parts;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import jp.co.arttec.satbox.SeaFly.R;
import jp.co.arttec.satbox.SeaFly.manager.MessageManager;
import jp.co.arttec.satbox.SeaFly.util.EnemyKind;
import jp.co.arttec.satbox.SeaFly.util.Position;
import jp.co.arttec.satbox.SeaFly.util.Random;

public class EnemyType5 extends EnemyBase {
    private static final int FRAME = 30;
    private Context mContext;
    private int mFrame = 0;

    public EnemyType5(Rect screenRect, Context context) {
        super(screenRect);
        this.mScreenRect = screenRect;
        this.mContext = context;
        setImage(context.getResources(), R.drawable.enemy5);
        calcDirection();
        this.mPosition.x = Random.getInstance().nextInt(screenRect.right - getSize().mWidth);
        this.mPosition.y = screenRect.bottom - 1;
        this.mPower = 1;
        setEnemyKind(EnemyKind.Type5);
        this.mBulletFrequency = 4;
        this.mScore = 500;
        Message message = new Message(new Position(this.mPosition.x, this.mPosition.y - 40), this.mContext, "LOOK!");
        message.setSize(24);
        MessageManager.getInstance(this.mContext).addObject(message);
    }

    public void move() {
        this.mFrame++;
        if (this.mFrame > 30) {
            super.move();
        }
        if (this.mPosition.y < 0) {
            this.mPosition.y = 0;
            this.mDirection.dy = -this.mDirection.dy;
        }
    }

    /* access modifiers changed from: protected */
    public void calcDirection() {
        this.mDirection.dx = 0;
        this.mDirection.dy = -20;
    }

    public void draw(Canvas canvas) {
        if (this.mFrame > 30) {
            super.draw(canvas);
        }
    }
}
