package jp.co.arttec.satbox.SeaFly.event;

public interface BossDefeatListener {
    void defeatBoss();
}
