package jp.co.arttec.satbox.SeaFly.manager;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Vibrator;
import jp.co.arttec.satbox.SeaFly.parts.AllyBase;
import jp.co.arttec.satbox.SeaFly.parts.AllyExplode;
import jp.co.arttec.satbox.SeaFly.parts.BaseObject;
import jp.co.arttec.satbox.SeaFly.parts.RockBase;
import jp.co.arttec.satbox.SeaFly.parts.ShieldExplode;
import jp.co.arttec.satbox.SeaFly.util.EffectSoundFrequency;
import jp.co.arttec.satbox.SeaFly.util.HitObject;
import jp.co.arttec.satbox.SeaFly.util.Position;
import jp.co.arttec.satbox.SeaFly.util.Size;

public class RockManager extends BaseManager {
    public static final int MAX_ROCK_OBJECTS = 3;
    private static RockManager mSelf = null;

    private RockManager(Context context) {
        super(context);
    }

    public static RockManager getInstance(Context context) {
        if (mSelf == null) {
            mSelf = new RockManager(context);
        }
        return mSelf;
    }

    public void add(RockBase rock) {
        if (isAddObject()) {
            super.addObject(rock);
        }
    }

    public boolean isAddObject() {
        if (this.mObject == null) {
            return false;
        }
        int count = 0;
        for (int i = 0; i < this.mObject.length; i++) {
            if (this.mObject[i] != null && !((RockBase) this.mObject[i]).isDestroy()) {
                count++;
            }
        }
        if (3 > count) {
            return true;
        }
        return false;
    }

    public void move() {
        if (this.mObject != null) {
            for (int i = 0; i < this.mObject.length; i++) {
                if (this.mObject[i] != null) {
                    this.mObject[i].move();
                }
            }
        }
    }

    public Position getPosition() {
        return null;
    }

    public Size getSize() {
        return null;
    }

    public void onDraw(Canvas canvas) {
        if (this.mObject != null) {
            for (int i = 0; i < this.mObject.length; i++) {
                if (this.mObject[i] != null) {
                    this.mObject[i].draw(canvas);
                }
            }
        }
    }

    public void setPosition(Position position) {
    }

    public void shot() {
    }

    public void playEffectSound(EffectSoundFrequency sound) {
        super.playEffect(sound);
    }

    public void deleteObject(int index) {
        if (this.mObject != null) {
            try {
                this.mObject[index] = null;
            } catch (IndexOutOfBoundsException e) {
            }
        }
    }

    public BaseObject getObject(int index) {
        if (this.mObject == null) {
            return null;
        }
        try {
            return this.mObject[index];
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    public boolean isHit(AllyManager allyManager) {
        if (this.mObject == null || allyManager == null) {
            return false;
        }
        if (StageManager.getInstance().getGameOverFlag()) {
            return false;
        }
        AllyBase ally = allyManager.getObject();
        if (ally == null) {
            return false;
        }
        for (int i = 0; i < this.mObject.length; i++) {
            RockBase rock = (RockBase) this.mObject[i];
            if (rock != null && rock.isDestroy()) {
                Position rockPosition = rock.getPosition();
                if (HitObject.isHit(rockPosition, rock.getSize(), ally.getPosition(), ally.getSize())) {
                    ally.gainPower(-1);
                    if (ally.getPower() < 0) {
                        EffectSoundManager.getInstance(this.mContext).play(EffectSoundFrequency.Ally_Destroy);
                        StageManager.getInstance().setGameOverFlag(true);
                        allyManager.setImage(this.mContext.getResources(), 0);
                        ExplodeManager.getInstance(this.mContext).addObject(new AllyExplode(ally.getPosition(), this.mContext));
                        return true;
                    }
                    ExplodeManager.getInstance(this.mContext).addObject(new ShieldExplode(rockPosition, this.mContext));
                    if (ally.getPower() == 0) {
                        allyManager.setImage(this.mContext.getResources(), 0 | 1);
                    }
                    ((Vibrator) this.mContext.getSystemService("vibrator")).vibrate(100);
                    rock.gainPower(-1);
                    if (1 > rock.getPower()) {
                        this.mObject[i] = null;
                    }
                    return true;
                }
            }
        }
        return false;
    }

    public void release() {
        super.release();
        if (mSelf != null) {
            mSelf = null;
        }
    }
}
