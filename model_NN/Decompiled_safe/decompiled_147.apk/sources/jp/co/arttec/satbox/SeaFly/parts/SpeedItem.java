package jp.co.arttec.satbox.SeaFly.parts;

import android.content.Context;
import jp.co.arttec.satbox.SeaFly.R;
import jp.co.arttec.satbox.SeaFly.util.Item;
import jp.co.arttec.satbox.SeaFly.util.Position;

public class SpeedItem extends ItemBase {
    public SpeedItem(Position position, Context context) {
        super(position, context);
        setImage(context.getResources(), R.drawable.speed_item);
        setItem(Item.SpeedUp);
    }
}
