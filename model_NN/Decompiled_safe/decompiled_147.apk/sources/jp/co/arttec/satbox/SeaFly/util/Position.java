package jp.co.arttec.satbox.SeaFly.util;

public class Position {
    public int x;
    public int y;

    public Position() {
        this.x = 0;
        this.y = 0;
    }

    public Position(Position pos) {
        this.x = pos.x;
        this.y = pos.y;
    }

    public Position(int x2, int y2) {
        this.x = x2;
        this.y = y2;
    }
}
