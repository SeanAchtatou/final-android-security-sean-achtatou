package jp.co.arttec.satbox.SeaFly.parts;

import android.content.Context;
import android.graphics.Rect;
import jp.co.arttec.satbox.SeaFly.R;
import jp.co.arttec.satbox.SeaFly.util.EnemyKind;
import jp.co.arttec.satbox.SeaFly.util.Random;

public class EnemyType4 extends EnemyBase {
    private static final int FRAME = 5;
    private Context mContext;
    private int mDirectionX = Random.getInstance().nextInt(2);
    private int mFrame;
    private Rect mScreenRect;

    public EnemyType4(Rect screenRect, Context context) {
        super(screenRect);
        this.mScreenRect = screenRect;
        this.mContext = context;
        setImage(context.getResources(), R.drawable.enemy4);
        calcDirection();
        this.mPosition.x = Random.getInstance().nextInt(screenRect.right - getSize().mWidth);
        this.mPosition.y = -getSize().mHeight;
        this.mPower = 2;
        setEnemyKind(EnemyKind.Type4);
        this.mBulletFrequency = 3;
        this.mScore = 300;
        if (this.mDirectionX == 0) {
            this.mDirectionX = -1;
        }
        this.mFrame = 5;
    }

    public void move() {
        calcDirection();
        this.mFrame++;
        super.move();
        if (this.mPosition.x < 0) {
            this.mPosition.x = 0;
            this.mDirectionX = -this.mDirectionX;
            this.mFrame = 0;
        }
        if (this.mPosition.x > this.mScreenRect.right - getSize().mWidth) {
            this.mPosition.x = this.mScreenRect.right - getSize().mWidth;
            this.mDirectionX = -this.mDirectionX;
            this.mFrame = 0;
        }
    }

    /* access modifiers changed from: protected */
    public void calcDirection() {
        if (this.mFrame > 5) {
            this.mDirection.dx = this.mDirectionX * 25;
            this.mDirection.dy = 0;
            if (this.mDirectionX < 0) {
                setImage(this.mContext.getResources(), R.drawable.enemy4_left);
            } else {
                setImage(this.mContext.getResources(), R.drawable.enemy4_right);
            }
        } else {
            this.mDirection.dx = 0;
            this.mDirection.dy = 20;
            setImage(this.mContext.getResources(), R.drawable.enemy4);
        }
    }
}
