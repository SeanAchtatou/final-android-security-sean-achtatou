package jp.co.arttec.satbox.SeaFly.util;

public enum Voice {
    SpeedUp,
    DoubleShot,
    BackShot,
    PowerShot,
    Shield,
    Warning,
    Boss,
    GameStart,
    Nightmare,
    ShieldBroken,
    Black
}
