package jp.co.arttec.satbox.SeaFly.util;

public enum Item {
    SpeedUp,
    DoubleShot,
    BackShot,
    PowerShot,
    Shield,
    Rainbow,
    Star_Red,
    Star_Orange,
    Star_Purple,
    Star_Yellow,
    Star_Green,
    Black,
    Item_Point,
    Warning
}
