package jp.co.arttec.satbox.SeaFly.parts;

import jp.co.arttec.satbox.SeaFly.manager.StageManager;

public class BulletBase extends BaseObject {
    private boolean mIsDestroy = false;
    private int mSpeed = 10;

    public BulletBase() {
        StageManager manager = StageManager.getInstance();
        if (manager != null) {
            this.mSpeed += manager.getStage();
        }
    }

    /* access modifiers changed from: protected */
    public void calcDirection() {
    }

    public void setIsDestroy(boolean isDestroy) {
        this.mIsDestroy = isDestroy;
    }

    public boolean getIsDestroy() {
        return this.mIsDestroy;
    }

    public void setSpeed(int speed) {
        this.mSpeed = speed;
    }

    public int getSpeed() {
        return this.mSpeed;
    }

    public void setPower(int power) {
        this.mPower = power;
    }
}
