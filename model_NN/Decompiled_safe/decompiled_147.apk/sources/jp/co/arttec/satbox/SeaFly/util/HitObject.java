package jp.co.arttec.satbox.SeaFly.util;

public class HitObject {
    private static final double HIT_RATIO = 0.8d;

    public static boolean isHit(Position obj1Position, Size obj1Size, Position obj2Position, Size obj2Size) {
        Position pos1 = new Position(obj1Position.x, obj1Position.y);
        Position pos2 = new Position(obj2Position.x, obj2Position.y);
        Size size1 = new Size(obj1Size.mWidth, obj1Size.mHeight);
        Size size2 = new Size(obj2Size.mWidth, obj2Size.mHeight);
        size1.mWidth = (int) (((double) size1.mWidth) * HIT_RATIO);
        size1.mHeight = (int) (((double) size1.mHeight) * HIT_RATIO);
        size2.mWidth = (int) (((double) size2.mWidth) * HIT_RATIO);
        size2.mHeight = (int) (((double) size2.mHeight) * HIT_RATIO);
        pos1.x += (obj1Size.mWidth - size1.mWidth) / 2;
        pos1.y += (obj1Size.mHeight - size1.mHeight) / 2;
        pos2.x += (obj2Size.mWidth - size2.mWidth) / 2;
        pos2.y += (obj2Size.mHeight - size2.mHeight) / 2;
        if ((pos1.x + size1.mWidth <= pos2.x || pos2.x + size2.mWidth <= pos1.x || pos1.y + size1.mHeight <= pos2.y || pos2.y + size2.mHeight <= pos1.y) && (pos2.x >= pos1.x + size1.mWidth || pos1.x >= pos2.x + size2.mWidth || pos2.y >= pos1.y + size1.mHeight || pos1.y >= pos2.y + size2.mHeight)) {
            return false;
        }
        return true;
    }
}
