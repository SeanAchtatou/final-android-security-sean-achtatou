package jp.co.arttec.satbox.SeaFly.manager;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Vibrator;
import jp.co.arttec.satbox.SeaFly.parts.AllyBase;
import jp.co.arttec.satbox.SeaFly.parts.AllyExplode;
import jp.co.arttec.satbox.SeaFly.parts.BulletBase;
import jp.co.arttec.satbox.SeaFly.parts.EnemyExplode;
import jp.co.arttec.satbox.SeaFly.parts.RockBase;
import jp.co.arttec.satbox.SeaFly.parts.RockExplode;
import jp.co.arttec.satbox.SeaFly.parts.ShieldExplode;
import jp.co.arttec.satbox.SeaFly.util.EffectSoundFrequency;
import jp.co.arttec.satbox.SeaFly.util.EnemyKind;
import jp.co.arttec.satbox.SeaFly.util.HitObject;
import jp.co.arttec.satbox.SeaFly.util.Position;
import jp.co.arttec.satbox.SeaFly.util.Size;

public class EnemyBulletManager extends BaseManager {
    private static EnemyBulletManager mSelf = null;

    private EnemyBulletManager(Context context) {
        super(context);
    }

    public static EnemyBulletManager getInstance(Context context) {
        if (mSelf == null) {
            mSelf = new EnemyBulletManager(context);
        }
        return mSelf;
    }

    public Position getPosition() {
        return null;
    }

    public Size getSize() {
        return null;
    }

    public void onDraw(Canvas canvas) {
        if (this.mObject != null) {
            for (int i = 0; i < this.mObject.length; i++) {
                if (this.mObject[i] != null) {
                    this.mObject[i].draw(canvas);
                }
            }
        }
    }

    public void setPosition(Position position) {
    }

    public void shot() {
    }

    public void move() {
        if (this.mObject != null) {
            for (int i = 0; i < this.mObject.length; i++) {
                if (this.mObject[i] != null) {
                    this.mObject[i].move();
                }
            }
        }
    }

    public boolean isHit(AllyManager allyManager) {
        AllyBase ally;
        if (this.mObject == null || allyManager == null || StageManager.getInstance().getGameOverFlag() || (ally = allyManager.getObject()) == null) {
            return false;
        }
        Size allySize = new Size();
        allySize.mHeight = (int) (((float) ally.getSize().mHeight) * 0.35f);
        allySize.mWidth = (int) (((float) ally.getSize().mWidth) * 0.35f);
        Position allyPosition = new Position(ally.getPosition());
        allyPosition.x += (ally.getSize().mWidth / 2) - (allySize.mWidth / 2);
        allyPosition.y += (ally.getSize().mHeight / 2) - (allySize.mHeight / 2);
        for (int i = 0; i < this.mObject.length; i++) {
            BulletBase enemy = (BulletBase) this.mObject[i];
            if (enemy != null) {
                Position enemyPosition = enemy.getPosition();
                if (HitObject.isHit(enemyPosition, enemy.getSize(), allyPosition, allySize)) {
                    ally.gainPower(-1);
                    if (ally.getPower() < 0) {
                        EffectSoundManager.getInstance(this.mContext).play(EffectSoundFrequency.Ally_Destroy);
                        StageManager.getInstance().setGameOverFlag(true);
                        allyManager.setImage(this.mContext.getResources(), 0);
                        ExplodeManager.getInstance(this.mContext).addObject(new AllyExplode(ally.getPosition(), this.mContext));
                        return true;
                    }
                    ExplodeManager.getInstance(this.mContext).addObject(new ShieldExplode(enemyPosition, this.mContext));
                    if (ally.getPower() == 0) {
                        allyManager.setImage(this.mContext.getResources(), 0 | 1);
                    }
                    ((Vibrator) this.mContext.getSystemService("vibrator")).vibrate(100);
                    enemy.gainPower(-1);
                    if (1 > enemy.getPower()) {
                        this.mObject[i] = null;
                    }
                    return true;
                }
            }
        }
        return false;
    }

    public void isHit(RockManager manager) {
        if (this.mObject != null && manager != null && !StageManager.getInstance().getGameOverFlag()) {
            for (int i = 0; i < this.mObject.length; i++) {
                BulletBase enemy = (BulletBase) this.mObject[i];
                if (enemy != null) {
                    Position enemyPosition = enemy.getPosition();
                    Size enemySize = enemy.getSize();
                    for (int j = 0; j < manager.countObject(); j++) {
                        RockBase rock = (RockBase) manager.getObject(j);
                        if (rock != null && HitObject.isHit(enemyPosition, enemySize, rock.getPosition(), rock.getSize())) {
                            rock.gainPower(-1);
                            if (1 > rock.getPower()) {
                                EffectSoundManager.getInstance(this.mContext).play(EffectSoundFrequency.Meteor_Destroy);
                                if (EnemyKind.Rock1 == rock.getRockKind()) {
                                    ExplodeManager.getInstance(this.mContext).addObject(new EnemyExplode(enemyPosition, this.mContext));
                                } else {
                                    ExplodeManager.getInstance(this.mContext).addObject(new RockExplode(enemyPosition, this.mContext));
                                }
                                manager.deleteObject(j);
                            } else {
                                ExplodeManager.getInstance(this.mContext).addObject(new ShieldExplode(enemyPosition, this.mContext));
                            }
                            this.mObject[i] = null;
                        }
                    }
                }
            }
        }
    }

    public void release() {
        super.release();
        if (mSelf != null) {
            mSelf = null;
        }
    }
}
