package jp.co.arttec.satbox.SeaFly;

import android.content.Intent;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import jp.co.arttec.satbox.SeaFly.manager.EffectSoundManager;
import jp.co.arttec.satbox.SeaFly.opening.TitleActivity;
import jp.co.arttec.satbox.SeaFly.util.BaseActivity;
import jp.co.arttec.satbox.SeaFly.util.EffectSoundFrequency;
import jp.co.arttec.satbox.scoreranklib.GetScoreController;
import jp.co.arttec.satbox.scoreranklib.HttpCommunicationListener;
import jp.co.arttec.satbox.scoreranklib.Score;

public class HighScoreBaseActivity extends BaseActivity {
    /* access modifiers changed from: private */
    public HighScoreAdapter mAdapter;
    /* access modifiers changed from: private */
    public ListView mListView;
    /* access modifiers changed from: private */
    public ArrayList<HighScore> mScoreList;

    /* access modifiers changed from: protected */
    public void getHighScore(int gameId) {
        if (this.mScoreList != null) {
            this.mScoreList.clear();
        } else {
            this.mScoreList = new ArrayList<>();
        }
        final GetScoreController controller = new GetScoreController(gameId);
        controller.setActivity(this);
        controller.setRank(100);
        controller.setOnFinishListener(new HttpCommunicationListener() {
            public void onFinish(boolean result) {
                Toast toast;
                if (result) {
                    toast = Toast.makeText(HighScoreBaseActivity.this, "High Score Rankings acquires succesful.", 1);
                    List<Score> score = controller.getHighScoreList();
                    if (score != null) {
                        int rank = 1;
                        for (Score data : score) {
                            HighScoreBaseActivity.this.mScoreList.add(new HighScore(rank, data.getScore(), data.getName()));
                            rank++;
                        }
                        HighScoreBaseActivity.this.mAdapter = new HighScoreAdapter(HighScoreBaseActivity.this, R.layout.list, HighScoreBaseActivity.this.mScoreList);
                        HighScoreBaseActivity.this.mListView = (ListView) HighScoreBaseActivity.this.findViewById(R.id.world_rank);
                        HighScoreBaseActivity.this.mListView.setAdapter((ListAdapter) HighScoreBaseActivity.this.mAdapter);
                        HighScoreBaseActivity.this.mListView.setScrollingCacheEnabled(false);
                    }
                } else {
                    toast = Toast.makeText(HighScoreBaseActivity.this, "High Score Rankings acquires failure.", 1);
                }
                toast.show();
            }
        });
        controller.getHighScore();
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == 0) {
            switch (event.getKeyCode()) {
                case 4:
                    return true;
            }
        }
        return super.dispatchKeyEvent(event);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.finish /*2131230760*/:
                EffectSoundManager.getInstance(getApplication().getApplicationContext()).play(EffectSoundFrequency.Title_SE);
                startActivity(new Intent(this, TitleActivity.class));
                finish();
                return true;
            case R.id.help /*2131230761*/:
                goAboutPage();
                return true;
            case R.id.applink /*2131230762*/:
                moreApplicationLink();
                return true;
            case R.id.satbox /*2131230763*/:
                goHomePage();
                return true;
            default:
                return true;
        }
    }
}
