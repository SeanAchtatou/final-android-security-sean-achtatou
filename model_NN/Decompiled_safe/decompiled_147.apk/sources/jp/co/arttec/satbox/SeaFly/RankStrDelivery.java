package jp.co.arttec.satbox.SeaFly;

public class RankStrDelivery implements Comparable<Integer> {
    private String item1;
    private String item21;
    private String item22;
    private int rankno;

    public RankStrDelivery(int arankno, String aitem1, String aitem21, String aitem22) {
        this.rankno = arankno;
        this.item1 = aitem1;
        this.item21 = aitem21;
        this.item22 = aitem22;
    }

    public int getRankNo() {
        return this.rankno;
    }

    public String getItem1() {
        return this.item1;
    }

    public String getItem21() {
        return this.item21;
    }

    public String getItem22() {
        return this.item22;
    }

    public int compareTo(Integer another) {
        return 0;
    }
}
