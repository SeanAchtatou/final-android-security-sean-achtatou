package jp.co.arttec.satbox.SeaFly.opening;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import jp.co.arttec.satbox.SeaFly.R;
import jp.co.arttec.satbox.SeaFly.manager.AllyBulletManager;
import jp.co.arttec.satbox.SeaFly.manager.AllyManager;
import jp.co.arttec.satbox.SeaFly.manager.CacheImageManager;
import jp.co.arttec.satbox.SeaFly.manager.EffectSoundManager;
import jp.co.arttec.satbox.SeaFly.manager.EnemyManager;
import jp.co.arttec.satbox.SeaFly.manager.ExplodeManager;
import jp.co.arttec.satbox.SeaFly.manager.ItemManager;
import jp.co.arttec.satbox.SeaFly.manager.MessageManager;
import jp.co.arttec.satbox.SeaFly.manager.PlanetManager;
import jp.co.arttec.satbox.SeaFly.manager.PreferencesManager;
import jp.co.arttec.satbox.SeaFly.manager.RockManager;
import jp.co.arttec.satbox.SeaFly.manager.StarManager;
import jp.co.arttec.satbox.SeaFly.util.BaseActivity;
import jp.co.arttec.satbox.SeaFly.util.Random;

public class ObjectInit extends BaseActivity {
    private static final int OBJ_INIT_END = 1;
    private static final int OBJ_INIT_START = 0;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    new ObjectManagerAsyncTask().execute(new Void[0]);
                    return;
                case 1:
                    ObjectInit.this.startActivity(new Intent(ObjectInit.this, TitleActivity.class));
                    ObjectInit.this.finish();
                    return;
                default:
                    return;
            }
        }
    };

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.init);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.mHandler.sendEmptyMessage(0);
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == 0) {
            switch (event.getKeyCode()) {
                case 4:
                    return true;
            }
        }
        return super.dispatchKeyEvent(event);
    }

    private class ObjectManagerAsyncTask extends AsyncTask<Void, Void, Void> {
        private Context mContext;
        private ProgressDialog mDialog = null;

        public ObjectManagerAsyncTask() {
            this.mContext = ObjectInit.this.getApplication().getApplicationContext();
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            this.mDialog = new ProgressDialog(ObjectInit.this);
            this.mDialog.setTitle("");
            this.mDialog.setMessage("Now Loading...");
            this.mDialog.setProgressStyle(0);
            this.mDialog.show();
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... params) {
            System.gc();
            StarManager.getInstance(this.mContext);
            AllyManager.getInstance(this.mContext);
            EnemyManager.getInstance(this.mContext);
            AllyBulletManager.getInstance(this.mContext);
            RockManager.getInstance(this.mContext);
            ItemManager.getInstance(this.mContext);
            ExplodeManager.getInstance(this.mContext);
            MessageManager.getInstance(this.mContext);
            PlanetManager.getInstance(this.mContext);
            EffectSoundManager.getInstance(this.mContext);
            PreferencesManager.getInstance(this.mContext);
            CacheImageManager.getInstance(this.mContext);
            Random.getInstance();
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void result) {
            this.mDialog.dismiss();
            ObjectInit.this.mHandler.sendEmptyMessage(1);
        }
    }
}
