package jp.co.arttec.satbox.SeaFly.event;

public interface ViewReleaseListener {
    void releaseFinish();
}
