package jp.co.arttec.satbox.SeaFly;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.ArrayList;

public class HighScoreAdapter extends ArrayAdapter<HighScore> {
    private LayoutInflater mInfrater;
    private View[] mView;

    static class ViewHolder {
        TextView mName;
        TextView mRank;
        TextView mScore;

        ViewHolder() {
        }
    }

    public HighScoreAdapter(Context context, int textViewResourceId, ArrayList<HighScore> items) {
        super(context, textViewResourceId, items);
        this.mInfrater = (LayoutInflater) context.getSystemService("layout_inflater");
        this.mView = new View[items.size()];
        for (int i = 0; i < this.mView.length; i++) {
            this.mView[i] = null;
        }
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (this.mView[position] == null) {
            this.mView[position] = getView((HighScore) getItem(position));
        }
        return this.mView[position];
    }

    private View getView(HighScore item) {
        View view = this.mInfrater.inflate((int) R.layout.list, (ViewGroup) null);
        ViewHolder holder = new ViewHolder();
        holder.mRank = (TextView) view.findViewById(R.id.ranking);
        holder.mName = (TextView) view.findViewById(R.id.name);
        holder.mScore = (TextView) view.findViewById(R.id.score);
        view.setTag(holder);
        holder.mRank.setTextColor(Color.parseColor("#FFFFFF"));
        holder.mName.setTextColor(Color.parseColor("#FFFFFF"));
        holder.mScore.setTextColor(Color.parseColor("#FFFFFF"));
        holder.mRank.setText(String.valueOf(item.getRank()));
        holder.mName.setText(item.getName());
        holder.mScore.setText(String.valueOf(item.getHighScore()));
        return view;
    }
}
