package jp.co.arttec.satbox.SeaFly.manager;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferencesManager {
    public static final String KEY_HIGHSCORE = "HighScore";
    public static final String KEY_NAME = "Name";
    public static final String KEY_NIGHTMARE = "NightMare";
    public static final String KEY_STAGE = "StageNormal";
    public static final String KEY_STAGE_NIGHTMARE = "StageNightmare";
    public static final String PREFERENCES_NAME = "GalaxyLaserII";
    private static PreferencesManager mSelf = null;
    private SharedPreferences mSP;

    private PreferencesManager(Context context) {
        this.mSP = context.getSharedPreferences(PREFERENCES_NAME, 0);
    }

    public static PreferencesManager getInstance(Context context) {
        if (mSelf == null) {
            mSelf = new PreferencesManager(context);
        }
        return mSelf;
    }

    public int getHighScore() {
        return this.mSP.getInt(KEY_HIGHSCORE, 0);
    }

    public void setHighScore(int hiScore) {
        if (hiScore > getHighScore()) {
            SharedPreferences.Editor editor = this.mSP.edit();
            editor.putInt(KEY_HIGHSCORE, hiScore);
            editor.commit();
        }
    }

    public boolean getNightMareFlag() {
        return this.mSP.getBoolean(KEY_NIGHTMARE, false);
    }

    public void setNightMareFlag(boolean nightMareFlag) {
        SharedPreferences.Editor editor = this.mSP.edit();
        editor.putBoolean(KEY_NIGHTMARE, nightMareFlag);
        editor.commit();
    }

    public String getName() {
        return this.mSP.getString(KEY_NAME, "");
    }

    public void setName(String name) {
        SharedPreferences.Editor editor = this.mSP.edit();
        editor.putString(KEY_NAME, name);
        editor.commit();
    }

    public int getMaxStage() {
        return this.mSP.getInt(KEY_STAGE, 1);
    }

    public void setMaxStage(int stage) {
        SharedPreferences.Editor editor = this.mSP.edit();
        editor.putInt(KEY_STAGE, stage);
        editor.commit();
    }

    public int getMaxStageNightmare() {
        return this.mSP.getInt(KEY_STAGE_NIGHTMARE, 1);
    }

    public void setMaxStageNightmare(int stage) {
        SharedPreferences.Editor editor = this.mSP.edit();
        editor.putInt(KEY_STAGE_NIGHTMARE, stage);
        editor.commit();
    }

    public void release() {
        if (mSelf != null) {
            mSelf = null;
        }
    }
}
