package jp.co.arttec.satbox.SeaFly.manager;

import android.content.Context;
import android.graphics.Canvas;
import jp.co.arttec.satbox.SeaFly.parts.ExplodeBase;
import jp.co.arttec.satbox.SeaFly.util.Position;
import jp.co.arttec.satbox.SeaFly.util.Size;

public class ExplodeManager extends BaseManager {
    private static ExplodeManager mSelf = null;
    private Context mContext;

    private ExplodeManager(Context context) {
        super(context);
        this.mContext = context;
    }

    public static ExplodeManager getInstance(Context context) {
        if (mSelf == null) {
            mSelf = new ExplodeManager(context);
        }
        return mSelf;
    }

    public Context getContext() {
        return this.mContext;
    }

    public void move() {
        if (this.mObject != null) {
            for (int i = 0; i < this.mObject.length; i++) {
                if (this.mObject[i] != null) {
                    this.mObject[i].move();
                }
            }
        }
    }

    public Position getPosition() {
        return null;
    }

    public Size getSize() {
        return null;
    }

    public void setPosition(Position position) {
    }

    public void shot() {
    }

    public void onDraw(Canvas canvas) {
        if (this.mObject != null) {
            for (int i = 0; i < this.mObject.length; i++) {
                if (this.mObject[i] != null) {
                    this.mObject[i].draw(canvas);
                }
            }
        }
    }

    public void deleteObject() {
        if (this.mObject != null) {
            for (int i = 0; i < this.mObject.length; i++) {
                ExplodeBase explode = (ExplodeBase) this.mObject[i];
                if (explode != null && !explode.isShowExplode()) {
                    this.mObject[i] = null;
                }
            }
        }
    }

    public void release() {
        super.release();
        if (mSelf != null) {
            mSelf = null;
        }
    }
}
