package jp.co.arttec.satbox.SeaFly.manager;

import android.content.Context;
import android.media.SoundPool;
import java.util.HashMap;
import jp.co.arttec.satbox.SeaFly.R;
import jp.co.arttec.satbox.SeaFly.util.EffectSoundFrequency;

public class EffectSoundManager {
    private static EffectSoundManager mSelf = null;
    private Context mContext;
    private HashMap<EffectSoundFrequency, Integer> mMap = new HashMap<>();
    private SoundPool mSoundPool;
    private int[] mSounds;

    private EffectSoundManager(Context context) {
        this.mContext = context;
        this.mMap.put(EffectSoundFrequency.Shot, Integer.valueOf((int) R.raw.shot));
        this.mMap.put(EffectSoundFrequency.Enemy_Destroy, Integer.valueOf((int) R.raw.hit_se));
        this.mMap.put(EffectSoundFrequency.Meteor_Destroy, Integer.valueOf((int) R.raw.baku));
        this.mMap.put(EffectSoundFrequency.Star_Item, Integer.valueOf((int) R.raw.star));
        this.mMap.put(EffectSoundFrequency.Ally_Destroy, Integer.valueOf((int) R.raw.ally_explode));
        this.mMap.put(EffectSoundFrequency.Boss_Destroy, Integer.valueOf((int) R.raw.boss_explode));
        this.mMap.put(EffectSoundFrequency.Title_SE, Integer.valueOf((int) R.raw.se_title));
        this.mMap.put(EffectSoundFrequency.Hit, Integer.valueOf((int) R.raw.hit_se));
        this.mMap.put(EffectSoundFrequency.Powerup, Integer.valueOf((int) R.raw.powerup));
        this.mMap.put(EffectSoundFrequency.Flash, Integer.valueOf((int) R.raw.flash));
        this.mSounds = new int[EffectSoundFrequency.Item_End.ordinal()];
        this.mSoundPool = new SoundPool(this.mSounds.length, 3, 0);
        load();
    }

    public static EffectSoundManager getInstance(Context context) {
        if (mSelf == null) {
            mSelf = new EffectSoundManager(context);
        }
        return mSelf;
    }

    private void load() {
        for (Object key : this.mMap.keySet()) {
            this.mSounds[((EffectSoundFrequency) key).ordinal()] = this.mSoundPool.load(this.mContext, this.mMap.get(key).intValue(), 1);
        }
    }

    public void play(EffectSoundFrequency sound) {
        try {
            this.mSoundPool.play(this.mSounds[sound.ordinal()], 1.0f, 1.0f, 0, 0, 1.0f);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void release() {
        this.mSoundPool.release();
        this.mSoundPool = null;
        mSelf = null;
    }
}
