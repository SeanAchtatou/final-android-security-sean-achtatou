package jp.co.arttec.satbox.SeaFly.parts;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import jp.co.arttec.satbox.SeaFly.R;
import jp.co.arttec.satbox.SeaFly.util.Position;
import jp.co.arttec.satbox.SeaFly.util.Random;
import jp.co.arttec.satbox.SeaFly.util.Size;

public class Star extends BaseObject {
    public Star(Position position, Context context) {
        init(context);
        this.mPosition.x = position.x;
        this.mPosition.y = position.y;
    }

    public Star(Rect screenRect, Context context) {
        init(context);
        this.mPosition.x = Random.getInstance().nextInt(screenRect.right);
        this.mPosition.y = 0;
    }

    private void init(Context context) {
        setImage(context.getResources(), R.drawable.back_star);
        if (this.mSize == null) {
            this.mSize = new Size(2);
        } else {
            Size size = this.mSize;
            this.mSize.mHeight = 2;
            size.mWidth = 2;
        }
        calcDirection();
    }

    /* access modifiers changed from: protected */
    public void calcDirection() {
        this.mDirection.dx = 0;
        switch (Random.getInstance().nextInt(4)) {
            case 0:
                this.mDirection.dy = 2;
                return;
            case 1:
                this.mDirection.dy = 3;
                return;
            case 2:
                this.mDirection.dy = 4;
                return;
            case 3:
                this.mDirection.dy = 5;
                return;
            default:
                return;
        }
    }

    public void draw(Canvas canvas) {
        canvas.drawBitmap(this.mImage, (float) this.mPosition.x, (float) this.mPosition.y, (Paint) null);
    }
}
