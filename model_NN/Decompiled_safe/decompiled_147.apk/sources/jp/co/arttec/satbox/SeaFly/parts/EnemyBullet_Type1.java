package jp.co.arttec.satbox.SeaFly.parts;

import android.content.Context;
import jp.co.arttec.satbox.SeaFly.R;
import jp.co.arttec.satbox.SeaFly.util.Position;

public class EnemyBullet_Type1 extends BulletBase {
    public EnemyBullet_Type1(AllyBase ally, EnemyBase enemy, Context context) {
        setImage(context.getResources(), R.drawable.enemy_bullet);
        this.mPosition.x = (enemy.getPosition().x + (enemy.getSize().mWidth / 2)) - (getSize().mWidth / 2);
        this.mPosition.y = enemy.getPosition().y + enemy.getSize().mWidth;
        calcDirection(ally);
        this.mPower = 1;
    }

    private void calcDirection(AllyBase ally) {
        Position position;
        if (ally == null) {
            position = new Position();
        } else {
            position = ally.getPosition();
        }
        int diff = (int) Math.sqrt((double) (((position.x - this.mPosition.x) * (position.x - this.mPosition.x)) + ((position.y - this.mPosition.y) * (position.y - this.mPosition.y))));
        int speed = getSpeed();
        if (diff == 0) {
            this.mDirection.dx = 0;
            this.mDirection.dy = speed;
            return;
        }
        this.mDirection.dx = ((position.x - this.mPosition.x) * speed) / diff;
        this.mDirection.dy = ((position.y - this.mPosition.y) * speed) / diff;
    }
}
