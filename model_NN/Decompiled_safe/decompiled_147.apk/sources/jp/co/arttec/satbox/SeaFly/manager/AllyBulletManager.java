package jp.co.arttec.satbox.SeaFly.manager;

import android.content.Context;
import android.graphics.Canvas;
import jp.co.arttec.satbox.SeaFly.event.BossDefeatListener;
import jp.co.arttec.satbox.SeaFly.parts.AllyBase;
import jp.co.arttec.satbox.SeaFly.parts.AllyBullet_Back;
import jp.co.arttec.satbox.SeaFly.parts.AllyBullet_Double;
import jp.co.arttec.satbox.SeaFly.parts.AllyBullet_Normal;
import jp.co.arttec.satbox.SeaFly.parts.AllyBullet_Power;
import jp.co.arttec.satbox.SeaFly.parts.BackShotItem;
import jp.co.arttec.satbox.SeaFly.parts.BaseObject;
import jp.co.arttec.satbox.SeaFly.parts.BlackItem;
import jp.co.arttec.satbox.SeaFly.parts.BossExplode_BigOrange;
import jp.co.arttec.satbox.SeaFly.parts.BossExplode_BigPurple;
import jp.co.arttec.satbox.SeaFly.parts.BossExplode_BigYellow;
import jp.co.arttec.satbox.SeaFly.parts.BossExplode_SmallOrange;
import jp.co.arttec.satbox.SeaFly.parts.BossExplode_SmallPurple;
import jp.co.arttec.satbox.SeaFly.parts.BossExplode_SmallYellow;
import jp.co.arttec.satbox.SeaFly.parts.BulletBase;
import jp.co.arttec.satbox.SeaFly.parts.DoubleShotItem;
import jp.co.arttec.satbox.SeaFly.parts.EnemyBase;
import jp.co.arttec.satbox.SeaFly.parts.EnemyExplode;
import jp.co.arttec.satbox.SeaFly.parts.ItemBase;
import jp.co.arttec.satbox.SeaFly.parts.PowerShotItem;
import jp.co.arttec.satbox.SeaFly.parts.RockBase;
import jp.co.arttec.satbox.SeaFly.parts.RockExplode;
import jp.co.arttec.satbox.SeaFly.parts.ShieldExplode;
import jp.co.arttec.satbox.SeaFly.parts.ShieldItem;
import jp.co.arttec.satbox.SeaFly.parts.SpeedItem;
import jp.co.arttec.satbox.SeaFly.util.EffectSoundFrequency;
import jp.co.arttec.satbox.SeaFly.util.EnemyKind;
import jp.co.arttec.satbox.SeaFly.util.HitObject;
import jp.co.arttec.satbox.SeaFly.util.Position;
import jp.co.arttec.satbox.SeaFly.util.Random;
import jp.co.arttec.satbox.SeaFly.util.Size;

public class AllyBulletManager extends BaseManager {
    private static /* synthetic */ int[] $SWITCH_TABLE$jp$co$arttec$satbox$SeaFly$util$EnemyKind;
    private static AllyBulletManager mSelf = null;

    static /* synthetic */ int[] $SWITCH_TABLE$jp$co$arttec$satbox$SeaFly$util$EnemyKind() {
        int[] iArr = $SWITCH_TABLE$jp$co$arttec$satbox$SeaFly$util$EnemyKind;
        if (iArr == null) {
            iArr = new int[EnemyKind.values().length];
            try {
                iArr[EnemyKind.Boss1.ordinal()] = 7;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[EnemyKind.Boss2.ordinal()] = 8;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[EnemyKind.Boss3.ordinal()] = 9;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[EnemyKind.Boss4.ordinal()] = 10;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[EnemyKind.Boss5.ordinal()] = 11;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[EnemyKind.Boss6.ordinal()] = 12;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[EnemyKind.Rock1.ordinal()] = 13;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[EnemyKind.Rock2.ordinal()] = 14;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[EnemyKind.Type1.ordinal()] = 1;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[EnemyKind.Type2.ordinal()] = 2;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[EnemyKind.Type3.ordinal()] = 3;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[EnemyKind.Type4.ordinal()] = 4;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[EnemyKind.Type5.ordinal()] = 5;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[EnemyKind.Type6.ordinal()] = 6;
            } catch (NoSuchFieldError e14) {
            }
            $SWITCH_TABLE$jp$co$arttec$satbox$SeaFly$util$EnemyKind = iArr;
        }
        return iArr;
    }

    private AllyBulletManager(Context context) {
        super(context);
    }

    public static AllyBulletManager getInstance(Context context) {
        if (mSelf == null) {
            mSelf = new AllyBulletManager(context);
        }
        return mSelf;
    }

    public void addBullet(AllyBase ally) {
        if (ally != null) {
            int shot = ally.getShot();
            if (1 == (shot & 1)) {
                addNormalShot(ally);
            }
            if (2 == (shot & 2)) {
                addDoubleShot(ally);
            }
            if (4 == (shot & 4)) {
                addBackShot(ally);
            }
            if (8 == (shot & 8)) {
                addPowerShot(ally);
            }
            EffectSoundManager.getInstance(this.mContext).play(EffectSoundFrequency.Shot);
        }
    }

    private void addNormalShot(AllyBase ally) {
        addObject(new AllyBullet_Normal(ally, this.mContext));
    }

    private void addDoubleShot(AllyBase ally) {
        for (int i = 0; i < 2; i++) {
            addObject(new AllyBullet_Double(ally, this.mContext, i));
        }
    }

    private void addBackShot(AllyBase ally) {
        addObject(new AllyBullet_Back(ally, this.mContext));
    }

    private void addPowerShot(AllyBase ally) {
        addObject(new AllyBullet_Power(ally, this.mContext));
    }

    public void move() {
        if (this.mObject != null) {
            for (int i = 0; i < this.mObject.length; i++) {
                if (this.mObject[i] != null) {
                    this.mObject[i].move();
                }
            }
        }
    }

    public Position getPosition(int index) {
        if (this.mObject == null) {
            return null;
        }
        try {
            BaseObject bullet = this.mObject[index];
            if (bullet == null) {
                return null;
            }
            return bullet.getPosition();
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    public Position getPosition() {
        return null;
    }

    public Size getSize(int index) {
        if (this.mObject == null) {
            return null;
        }
        try {
            BaseObject bullet = this.mObject[index];
            if (this.mObject == null) {
                return null;
            }
            return bullet.getSize();
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    public Size getSize() {
        return null;
    }

    public void setPostion(int index, Position position) {
        if (this.mObject != null) {
            try {
                BaseObject bullet = this.mObject[index];
                if (bullet != null) {
                    bullet.setPosition(position);
                }
            } catch (IndexOutOfBoundsException e) {
            }
        }
    }

    public void setPosition(Position position) {
    }

    public void shot() {
    }

    public void onDraw(Canvas canvas) {
        if (this.mObject != null) {
            for (int i = 0; i < this.mObject.length; i++) {
                if (this.mObject[i] != null) {
                    this.mObject[i].draw(canvas);
                }
            }
        }
    }

    public int hitBullet(EnemyManager enemyManager) {
        int gainScore = 0;
        if (this.mObject != null) {
            for (int i = 0; i < this.mObject.length; i++) {
                BulletBase allyBullet = (BulletBase) this.mObject[i];
                if (allyBullet != null) {
                    Position bulletPosition = allyBullet.getPosition();
                    Size bulletSize = allyBullet.getSize();
                    for (int j = 0; j < enemyManager.countObject(); j++) {
                        EnemyBase enemy = (EnemyBase) enemyManager.getObject(j);
                        if (enemy != null) {
                            if (HitObject.isHit(bulletPosition, bulletSize, enemy.getPosition(), enemy.getSize())) {
                                enemy.gainPower(-1);
                                allyBullet.gainPower(-1);
                                if (1 > allyBullet.getPower()) {
                                    this.mObject[i] = null;
                                }
                                if (enemy.getPower() < 1) {
                                    switch ($SWITCH_TABLE$jp$co$arttec$satbox$SeaFly$util$EnemyKind()[EnemyKind.values()[enemy.getEnemyKind().ordinal()].ordinal()]) {
                                        case 1:
                                            enemyManager.playEffectSound(EffectSoundFrequency.Enemy_Destroy);
                                            ExplodeManager.getInstance(this.mContext).addObject(new EnemyExplode(enemy.getPosition(), this.mContext));
                                            gainScore += enemy.getScore();
                                            break;
                                        case 2:
                                            enemyManager.playEffectSound(EffectSoundFrequency.Enemy_Destroy);
                                            ExplodeManager.getInstance(this.mContext).addObject(new EnemyExplode(enemy.getPosition(), this.mContext));
                                            gainScore += enemy.getScore();
                                            break;
                                        case 3:
                                            enemyManager.playEffectSound(EffectSoundFrequency.Enemy_Destroy);
                                            ExplodeManager.getInstance(this.mContext).addObject(new EnemyExplode(enemy.getPosition(), this.mContext));
                                            gainScore += enemy.getScore();
                                            break;
                                        case 4:
                                            enemyManager.playEffectSound(EffectSoundFrequency.Enemy_Destroy);
                                            ExplodeManager.getInstance(this.mContext).addObject(new EnemyExplode(enemy.getPosition(), this.mContext));
                                            gainScore += enemy.getScore();
                                            break;
                                        case 5:
                                            enemyManager.playEffectSound(EffectSoundFrequency.Enemy_Destroy);
                                            ExplodeManager.getInstance(this.mContext).addObject(new EnemyExplode(enemy.getPosition(), this.mContext));
                                            gainScore += enemy.getScore();
                                            break;
                                        case 6:
                                            enemyManager.playEffectSound(EffectSoundFrequency.Enemy_Destroy);
                                            ExplodeManager.getInstance(this.mContext).addObject(new EnemyExplode(enemy.getPosition(), this.mContext));
                                            gainScore += enemy.getScore();
                                            break;
                                        case 7:
                                            enemyManager.playEffectSound(EffectSoundFrequency.Boss_Destroy);
                                            ExplodeManager.getInstance(this.mContext).addObject(new BossExplode_BigPurple(enemy.getPosition(), this.mContext));
                                            gainScore += enemy.getScore();
                                            break;
                                        case 8:
                                            enemyManager.playEffectSound(EffectSoundFrequency.Boss_Destroy);
                                            ExplodeManager.getInstance(this.mContext).addObject(new BossExplode_SmallPurple(enemy.getPosition(), this.mContext));
                                            gainScore += enemy.getScore();
                                            break;
                                        case 9:
                                            enemyManager.playEffectSound(EffectSoundFrequency.Boss_Destroy);
                                            ExplodeManager.getInstance(this.mContext).addObject(new BossExplode_SmallOrange(enemy.getPosition(), this.mContext));
                                            gainScore += enemy.getScore();
                                            break;
                                        case 10:
                                            enemyManager.playEffectSound(EffectSoundFrequency.Boss_Destroy);
                                            ExplodeManager.getInstance(this.mContext).addObject(new BossExplode_SmallYellow(enemy.getPosition(), this.mContext));
                                            gainScore += enemy.getScore();
                                            break;
                                        case 11:
                                            enemyManager.playEffectSound(EffectSoundFrequency.Boss_Destroy);
                                            ExplodeManager.getInstance(this.mContext).addObject(new BossExplode_BigOrange(enemy.getPosition(), this.mContext));
                                            gainScore += enemy.getScore();
                                            break;
                                        case 12:
                                            enemyManager.playEffectSound(EffectSoundFrequency.Boss_Destroy);
                                            ExplodeManager.getInstance(this.mContext).addObject(new BossExplode_BigYellow(enemy.getPosition(), this.mContext));
                                            gainScore += enemy.getScore();
                                            break;
                                    }
                                    EnemyBulletManager bulletManager = EnemyBulletManager.getInstance(this.mContext);
                                    for (int k = 0; k < bulletManager.countObject(); k++) {
                                        BulletBase bullet = (BulletBase) bulletManager.mObject[k];
                                        if (bullet != null && j == bullet.getId()) {
                                            bulletManager.mObject[k] = null;
                                        }
                                    }
                                    enemyManager.deleteObject(j);
                                    BossDefeatListener listener = enemy.getBossDefeatListener();
                                    if (listener != null) {
                                        listener.defeatBoss();
                                    }
                                } else {
                                    EffectSoundManager.getInstance(this.mContext).play(EffectSoundFrequency.Hit);
                                    ExplodeManager.getInstance(this.mContext).addObject(new ShieldExplode(allyBullet.getPosition(), this.mContext));
                                }
                            }
                        }
                    }
                }
            }
        }
        hitBullet(enemyManager.getEnemyBulletManager());
        return gainScore;
    }

    private void hitBullet(EnemyBulletManager enemyBullet) {
        if (this.mObject != null) {
            for (int i = 0; i < this.mObject.length; i++) {
                BulletBase allyBullet = (BulletBase) this.mObject[i];
                if (allyBullet != null) {
                    Position bulletPosition = allyBullet.getPosition();
                    Size bulletSize = allyBullet.getSize();
                    for (int j = 0; j < enemyBullet.countObject(); j++) {
                        BulletBase enemy = (BulletBase) enemyBullet.mObject[j];
                        if (enemy != null && enemy.getIsDestroy() && HitObject.isHit(bulletPosition, bulletSize, enemy.getPosition(), enemy.getSize())) {
                            enemy.gainPower(-1);
                            this.mObject[i] = null;
                            if (enemy.getPower() < 1) {
                                EffectSoundManager.getInstance(this.mContext).play(EffectSoundFrequency.Enemy_Destroy);
                                enemyBullet.mObject[j] = null;
                            }
                        }
                    }
                }
            }
        }
    }

    public int hitBullet(RockManager rockManager, ItemManager itemManager) {
        int gainScore = 0;
        if (this.mObject != null) {
            for (int i = 0; i < this.mObject.length; i++) {
                BulletBase allyBullet = (BulletBase) this.mObject[i];
                if (allyBullet != null) {
                    Position bulletPosition = allyBullet.getPosition();
                    Size bulletSize = allyBullet.getSize();
                    for (int j = 0; j < rockManager.countObject(); j++) {
                        RockBase rock = (RockBase) rockManager.getObject(j);
                        if (rock != null && rock.isDestroy()) {
                            Position objPosition = rock.getPosition();
                            if (HitObject.isHit(bulletPosition, bulletSize, objPosition, rock.getSize())) {
                                rock.gainPower(-1);
                                allyBullet.gainPower(-1);
                                if (1 > allyBullet.getPower()) {
                                    this.mObject[i] = null;
                                }
                                if (rock.getPower() < 1) {
                                    if (EnemyKind.Rock1 == rock.getRockKind()) {
                                        ExplodeManager.getInstance(this.mContext).addObject(new EnemyExplode(rock.getPosition(), this.mContext));
                                    } else {
                                        ExplodeManager.getInstance(this.mContext).addObject(new RockExplode(rock.getPosition(), this.mContext));
                                    }
                                    gainScore += rock.getScore();
                                    rockManager.playEffectSound(EffectSoundFrequency.Meteor_Destroy);
                                    rockManager.deleteObject(j);
                                    createItem(objPosition, itemManager);
                                } else {
                                    EffectSoundManager.getInstance(this.mContext).play(EffectSoundFrequency.Hit);
                                    ExplodeManager.getInstance(this.mContext).addObject(new ShieldExplode(allyBullet.getPosition(), this.mContext));
                                }
                            }
                        }
                    }
                }
            }
        }
        return gainScore;
    }

    private void createItem(Position position, ItemManager itemManager) {
        ItemBase item;
        Context context = itemManager.getContext();
        switch (Random.getInstance().nextInt(6)) {
            case 0:
                item = new SpeedItem(position, context);
                break;
            case 1:
                item = new DoubleShotItem(position, context);
                break;
            case 2:
                item = new BackShotItem(position, context);
                break;
            case 3:
                item = new PowerShotItem(position, context);
                break;
            case 4:
                item = new ShieldItem(position, context);
                break;
            case 5:
                item = new BlackItem(position, context);
                break;
            default:
                item = null;
                break;
        }
        if (6 == StageManager.getInstance().getStage() && Random.getInstance().nextInt(3) == 0) {
            item = new ShieldItem(position, context);
        }
        if (item != null) {
            itemManager.addObject(item);
        }
    }

    public void release() {
        super.release();
        if (mSelf != null) {
            mSelf = null;
        }
    }
}
