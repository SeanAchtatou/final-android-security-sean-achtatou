package jp.co.arttec.satbox.scoreranklib;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import jp.co.arttec.satbox.SeaFly.manager.PreferencesManager;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GetScoreController extends AsyncTask<Void, Integer, Boolean> implements GAEController {
    private Context _context;
    private Date _date;
    private long _id;
    private int _kindGames;
    private HttpCommunicationListener _listener;
    private String _name;
    private ProgressDialog _progress;
    private int _rank;
    private boolean _reverse;
    private long _score;
    private List<Score> _scoreList;
    private boolean _success;
    private String _version;

    public GetScoreController() {
        this._id = 0;
        this._kindGames = -1;
        this._score = 0;
        this._date = null;
        this._name = null;
        this._version = null;
        this._context = null;
        this._progress = null;
        this._scoreList = null;
        this._success = false;
        this._listener = null;
        this._rank = 10;
        this._reverse = false;
    }

    public GetScoreController(int kindGames) {
        this._id = 0;
        this._kindGames = kindGames;
        this._score = 0;
        this._date = null;
        this._name = null;
        this._version = null;
        this._context = null;
        this._progress = null;
        this._scoreList = null;
        this._success = false;
        this._listener = null;
        this._reverse = false;
    }

    public void setOnFinishListener(HttpCommunicationListener listener) {
        this._listener = listener;
    }

    public void getHighScore() {
        execute(new Void[0]);
    }

    public void registScore() {
    }

    public void setActivity(Context context) {
        this._context = context;
    }

    /* access modifiers changed from: protected */
    public Boolean doInBackground(Void... params) {
        boolean result = true;
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), GAECommonData.CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(httpClient.getParams(), GAECommonData.SO_TIMEOUT);
            StringBuffer request = new StringBuffer(GAECommonData.GAE_URI);
            request.append("scorerank?kindGames=");
            request.append(Integer.toString(this._kindGames));
            request.append("&format=json&rank=");
            request.append(Integer.toString(this._rank));
            if (this._reverse) {
                request.append("&reverse");
            }
            Log.d("RegistScore", request.toString());
            HttpResponse httpResponse = httpClient.execute(new HttpGet(request.toString()));
            if (httpResponse.getStatusLine().getStatusCode() < 400) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent(), "UTF-8"));
                StringBuilder builder = new StringBuilder();
                while (true) {
                    String line = reader.readLine();
                    if (line == null) {
                        break;
                    }
                    builder.append(line);
                }
                createHighScoreList(builder.toString());
            } else {
                result = false;
            }
        } catch (Exception e) {
            Log.d("RegistScore", e.toString());
            result = false;
        }
        return Boolean.valueOf(result);
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        if (this._context != null) {
            this._progress = new ProgressDialog(this._context);
            this._progress.setTitle("");
            this._progress.setMessage("");
            this._progress.setProgressStyle(0);
            this._progress.show();
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Boolean result) {
        if (this._progress != null) {
            this._progress.dismiss();
            this._progress = null;
        }
        if (this._listener != null) {
            this._listener.onFinish(this._success);
        }
    }

    public void createHighScoreList(String line) {
        if (this._scoreList == null) {
            this._scoreList = new ArrayList();
        } else {
            this._scoreList.clear();
        }
        this._success = false;
        try {
            JSONObject jsonObject = new JSONObject(line);
            Log.d("ScoreRankSample", line);
            JSONArray jsonArray = jsonObject.getJSONArray(PreferencesManager.KEY_HIGHSCORE);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject score = jsonArray.getJSONObject(i);
                this._id = (long) i;
                this._kindGames = score.getInt(GAECommonData.KIND_GAMES);
                this._name = score.getString(GAECommonData.NAME);
                this._score = score.getLong(GAECommonData.SCORE);
                this._date = new Date(score.getString(GAECommonData.DATE));
                if (line.matches(GAECommonData.VERSION)) {
                    this._version = score.getString(GAECommonData.VERSION);
                } else {
                    this._version = "";
                }
                this._scoreList.add(new Score(this._id, this._kindGames, this._score, this._date, this._name, this._version));
                this._success = true;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public List<Score> getHighScoreList() {
        return this._scoreList;
    }

    public void setRank(int rank) {
        this._rank = rank;
    }

    public int getRank() {
        return this._rank;
    }

    public void setReverse(boolean reverse) {
        this._reverse = reverse;
    }

    public boolean getReverse() {
        return this._reverse;
    }
}
