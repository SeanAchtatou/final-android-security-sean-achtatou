package jp.co.arttec.satbox.SeaFly.parts;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import jp.co.arttec.satbox.SeaFly.R;
import jp.co.arttec.satbox.SeaFly.util.Position;

public class BossExplode_BigOrange extends ExplodeBase {
    private Context mContext;
    private int mDispWait = 0;
    private int[] mImageList = {R.drawable.explode_big_orange00, R.drawable.explode_big_orange01, R.drawable.explode_big_orange02, R.drawable.explode_big_orange03, R.drawable.explode_dummy};
    private int mWaitTime = 5;

    public BossExplode_BigOrange(Position position, Context context) {
        super(position, context);
        init(context);
        this.mContext = context;
        setImage(context.getResources(), this.mImageList[0]);
        setShowFrame(this.mImageList.length * 10);
    }

    /* access modifiers changed from: protected */
    public void init(Context context) {
        calcDirection();
    }

    /* access modifiers changed from: protected */
    public void calcDirection() {
    }

    public void move() {
        if (this.mDispWait > this.mWaitTime) {
            super.move();
            this.mDispWait = 0;
        } else {
            this.mDispWait++;
        }
        Resources res = this.mContext.getResources();
        if (this.mImageList.length > getFrame()) {
            setImage(res, this.mImageList[getFrame()]);
        } else {
            setImage(res, this.mImageList[this.mImageList.length - 1]);
        }
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
    }
}
