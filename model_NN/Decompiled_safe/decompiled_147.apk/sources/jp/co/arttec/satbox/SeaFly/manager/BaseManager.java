package jp.co.arttec.satbox.SeaFly.manager;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import jp.co.arttec.satbox.SeaFly.parts.BaseObject;
import jp.co.arttec.satbox.SeaFly.util.EffectSoundFrequency;
import jp.co.arttec.satbox.SeaFly.util.Position;
import jp.co.arttec.satbox.SeaFly.util.Size;

public abstract class BaseManager {
    private static final int ObjectSize = 100;
    protected Context mContext;
    protected BaseObject[] mObject = new BaseObject[ObjectSize];

    public abstract Position getPosition();

    public abstract Size getSize();

    public abstract void onDraw(Canvas canvas);

    public abstract void setPosition(Position position);

    public abstract void shot();

    public BaseManager(Context context) {
        this.mContext = context;
        for (int i = 0; i < this.mObject.length; i++) {
            this.mObject[i] = null;
        }
    }

    public void addObject(BaseObject object) {
        if (this.mObject != null) {
            for (int i = 0; i < this.mObject.length; i++) {
                if (this.mObject[i] == null) {
                    this.mObject[i] = object;
                    return;
                }
            }
        }
    }

    public int countObject() {
        if (this.mObject == null) {
            return 0;
        }
        return this.mObject.length;
    }

    public void deleteObject(Rect screenRect) {
        if (this.mObject != null) {
            for (int index = 0; index < this.mObject.length; index++) {
                BaseObject object = this.mObject[index];
                if (object != null) {
                    Size size = object.getSize();
                    if (object.getPosition().x + size.mWidth <= (-size.mWidth) || screenRect.right <= object.getPosition().x || object.getPosition().y + size.mHeight <= (-size.mHeight) || screenRect.bottom <= object.getPosition().y) {
                        this.mObject[index] = null;
                    }
                }
            }
        }
    }

    public void playEffect(EffectSoundFrequency sound) {
        EffectSoundManager.getInstance(this.mContext).play(sound);
    }

    public void release() {
        for (int i = 0; i < this.mObject.length; i++) {
            this.mObject[i] = null;
        }
    }
}
