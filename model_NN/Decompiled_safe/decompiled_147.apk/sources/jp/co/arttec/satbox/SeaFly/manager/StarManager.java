package jp.co.arttec.satbox.SeaFly.manager;

import android.content.Context;
import android.graphics.Canvas;
import jp.co.arttec.satbox.SeaFly.parts.BaseObject;
import jp.co.arttec.satbox.SeaFly.parts.Star;
import jp.co.arttec.satbox.SeaFly.util.Position;
import jp.co.arttec.satbox.SeaFly.util.Size;

public class StarManager extends BaseManager {
    public static final int MAX_STAR_OBJECTS = 15;
    private static StarManager mSelf = null;

    private StarManager(Context context) {
        super(context);
    }

    public static StarManager getInstance(Context context) {
        if (mSelf == null) {
            mSelf = new StarManager(context);
        }
        return mSelf;
    }

    public void add(Star star) {
        if (isAddObject()) {
            super.addObject(star);
        }
    }

    public boolean isAddObject() {
        if (this.mObject == null) {
            return false;
        }
        int count = 0;
        for (BaseObject baseObject : this.mObject) {
            if (baseObject != null) {
                count++;
            }
        }
        if (15 > count) {
            return true;
        }
        return false;
    }

    public void move() {
        if (this.mObject != null) {
            for (int i = 0; i < this.mObject.length; i++) {
                if (this.mObject[i] != null) {
                    this.mObject[i].move();
                }
            }
        }
    }

    public Position getPosition() {
        return null;
    }

    public Size getSize() {
        return null;
    }

    public void onDraw(Canvas canvas) {
        if (this.mObject != null) {
            for (int i = 0; i < this.mObject.length; i++) {
                if (this.mObject[i] != null) {
                    this.mObject[i].draw(canvas);
                }
            }
        }
    }

    public void setPosition(Position position) {
    }

    public void shot() {
    }

    public void release() {
        super.release();
        if (mSelf != null) {
            mSelf = null;
        }
    }
}
