package jp.co.arttec.satbox.SeaFly.util;

public class FPSManager {
    private long mAfterMillisec;
    private long mBeforeMillisec;
    private int mFps;
    private int mFpsCounter = 0;
    private int mFrameDuration;
    private long mStartMillisec = 0;

    public FPSManager(int fps) {
        this.mFps = fps;
        this.mFrameDuration = 1000 / this.mFps;
    }

    public void setFPS(int fps) {
        this.mFps = fps;
        this.mFrameDuration = 1000 / this.mFps;
    }

    public int getFPS() {
        return this.mFps;
    }

    public void startFrame(long ms) {
        this.mBeforeMillisec = ms;
        if (this.mFpsCounter == 0) {
            this.mStartMillisec = this.mBeforeMillisec;
        }
        this.mAfterMillisec = this.mStartMillisec + ((long) ((this.mFpsCounter + 1) * this.mFrameDuration));
    }

    public boolean checkArrival(long ms) {
        if (ms < this.mAfterMillisec) {
            return false;
        }
        int i = this.mFpsCounter + 1;
        this.mFpsCounter = i;
        if (i != this.mFps) {
            return true;
        }
        this.mFpsCounter = 0;
        return true;
    }
}
