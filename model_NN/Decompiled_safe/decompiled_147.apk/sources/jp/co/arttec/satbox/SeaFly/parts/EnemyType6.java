package jp.co.arttec.satbox.SeaFly.parts;

import android.content.Context;
import android.graphics.Rect;
import jp.co.arttec.satbox.SeaFly.R;
import jp.co.arttec.satbox.SeaFly.util.EnemyKind;
import jp.co.arttec.satbox.SeaFly.util.Random;

public class EnemyType6 extends EnemyBase {
    private static final int FRAME = 60;
    private static final int STATE_DOWNLEFT = 2;
    private static final int STATE_DOWNRIGHT = 1;
    private static final int STATE_LEFT = 3;
    private static final int STATE_RIGHT = 4;
    private static final int STATE_UPLEFT = 5;
    private static final int STATE_UPRIGHT = 6;
    private int mFrame;
    private Rect mScreenRect;
    private int mState;

    public EnemyType6(Rect screenRect, Context context) {
        super(screenRect);
        this.mScreenRect = screenRect;
        setImage(context.getResources(), R.drawable.enemy6);
        calcDirection();
        if (1 < Random.getInstance().nextInt(2)) {
            this.mPosition.x = screenRect.right - getSize().mWidth;
            this.mState = 2;
        } else {
            this.mPosition.x = 0;
            this.mState = 1;
        }
        this.mPosition.y = -getSize().mHeight;
        this.mPower = 3;
        setEnemyKind(EnemyKind.Type6);
        this.mBulletFrequency = 5;
        this.mScore = 400;
        this.mFrame = 0;
    }

    public void move() {
        calcDirection();
        super.move();
        if (1 == this.mState && this.mPosition.x > this.mScreenRect.right - this.mSize.mWidth && this.mPosition.y > this.mScreenRect.bottom - this.mSize.mHeight) {
            this.mPosition.x = this.mScreenRect.right - this.mSize.mWidth;
            this.mPosition.y = this.mScreenRect.bottom - this.mSize.mHeight;
            this.mState = 3;
        }
        if (3 == this.mState && this.mPosition.x < 0 && this.mPosition.y == this.mScreenRect.bottom - this.mSize.mHeight) {
            this.mPosition.x = 0;
            this.mState = 6;
        }
        if (2 == this.mState && this.mPosition.x < 0 && this.mPosition.y > this.mScreenRect.bottom - this.mSize.mHeight) {
            this.mPosition.x = 0;
            this.mPosition.y = this.mScreenRect.bottom - this.mSize.mHeight;
            this.mState = 4;
        }
        if (4 == this.mState && this.mPosition.x > this.mScreenRect.right - this.mSize.mWidth && this.mPosition.y == this.mScreenRect.bottom - this.mSize.mHeight) {
            this.mPosition.x = this.mScreenRect.right - this.mSize.mWidth;
            this.mState = 5;
        }
    }

    /* access modifiers changed from: protected */
    public void calcDirection() {
        this.mFrame++;
        int dx = (this.mScreenRect.right - this.mSize.mWidth) / FRAME;
        int dy = (this.mScreenRect.bottom - this.mSize.mHeight) / FRAME;
        switch (this.mState) {
            case 1:
                this.mDirection.dx = dx;
                this.mDirection.dy = dy;
                return;
            case 2:
                this.mDirection.dx = -dx;
                this.mDirection.dy = dy;
                return;
            case 3:
                this.mDirection.dx = -dx;
                this.mDirection.dy = 0;
                return;
            case 4:
                this.mDirection.dx = dx;
                this.mDirection.dy = 0;
                return;
            case 5:
                this.mDirection.dx = -dx;
                this.mDirection.dy = -dy;
                return;
            case 6:
                this.mDirection.dx = dx;
                this.mDirection.dy = -dy;
                return;
            default:
                return;
        }
    }
}
