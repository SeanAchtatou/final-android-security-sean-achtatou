package jp.co.arttec.satbox.SeaFly.parts;

import android.content.Context;
import jp.co.arttec.satbox.SeaFly.R;
import jp.co.arttec.satbox.SeaFly.util.Position;

public class ExplodeBase extends BaseObject {
    private static final int FRAME = 5;
    private int mFrame = 0;
    private int mShowFrame;

    public ExplodeBase(Position position, Context context) {
        init(context);
        this.mPosition.x = position.x;
        this.mPosition.y = position.y;
        setImage(context.getResources(), R.drawable.explode_dummy);
        this.mShowFrame = 5;
    }

    /* access modifiers changed from: protected */
    public void init(Context context) {
        calcDirection();
    }

    /* access modifiers changed from: protected */
    public void calcDirection() {
    }

    public void move() {
        this.mFrame++;
    }

    public boolean isShowExplode() {
        if (this.mFrame < this.mShowFrame) {
            return true;
        }
        return false;
    }

    public void setShowFrame(int showFrame) {
        this.mShowFrame = showFrame;
    }

    public int getFrame() {
        return this.mFrame;
    }
}
