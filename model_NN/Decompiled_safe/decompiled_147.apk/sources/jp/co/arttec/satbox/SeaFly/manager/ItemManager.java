package jp.co.arttec.satbox.SeaFly.manager;

import android.content.Context;
import android.graphics.Canvas;
import jp.co.arttec.satbox.SeaFly.parts.AllyBase;
import jp.co.arttec.satbox.SeaFly.parts.BaseObject;
import jp.co.arttec.satbox.SeaFly.parts.BulletBase;
import jp.co.arttec.satbox.SeaFly.parts.EnemyBase;
import jp.co.arttec.satbox.SeaFly.parts.EnemyExplode;
import jp.co.arttec.satbox.SeaFly.parts.ItemBase;
import jp.co.arttec.satbox.SeaFly.parts.Message;
import jp.co.arttec.satbox.SeaFly.util.EffectSoundFrequency;
import jp.co.arttec.satbox.SeaFly.util.EnemyKind;
import jp.co.arttec.satbox.SeaFly.util.HitObject;
import jp.co.arttec.satbox.SeaFly.util.Item;
import jp.co.arttec.satbox.SeaFly.util.Position;
import jp.co.arttec.satbox.SeaFly.util.Size;

public class ItemManager extends BaseManager {
    private static /* synthetic */ int[] $SWITCH_TABLE$jp$co$arttec$satbox$SeaFly$util$EnemyKind;
    private static /* synthetic */ int[] $SWITCH_TABLE$jp$co$arttec$satbox$SeaFly$util$Item;
    private static ItemManager mSelf = null;
    private Context mContext;

    static /* synthetic */ int[] $SWITCH_TABLE$jp$co$arttec$satbox$SeaFly$util$EnemyKind() {
        int[] iArr = $SWITCH_TABLE$jp$co$arttec$satbox$SeaFly$util$EnemyKind;
        if (iArr == null) {
            iArr = new int[EnemyKind.values().length];
            try {
                iArr[EnemyKind.Boss1.ordinal()] = 7;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[EnemyKind.Boss2.ordinal()] = 8;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[EnemyKind.Boss3.ordinal()] = 9;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[EnemyKind.Boss4.ordinal()] = 10;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[EnemyKind.Boss5.ordinal()] = 11;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[EnemyKind.Boss6.ordinal()] = 12;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[EnemyKind.Rock1.ordinal()] = 13;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[EnemyKind.Rock2.ordinal()] = 14;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[EnemyKind.Type1.ordinal()] = 1;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[EnemyKind.Type2.ordinal()] = 2;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[EnemyKind.Type3.ordinal()] = 3;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[EnemyKind.Type4.ordinal()] = 4;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[EnemyKind.Type5.ordinal()] = 5;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[EnemyKind.Type6.ordinal()] = 6;
            } catch (NoSuchFieldError e14) {
            }
            $SWITCH_TABLE$jp$co$arttec$satbox$SeaFly$util$EnemyKind = iArr;
        }
        return iArr;
    }

    static /* synthetic */ int[] $SWITCH_TABLE$jp$co$arttec$satbox$SeaFly$util$Item() {
        int[] iArr = $SWITCH_TABLE$jp$co$arttec$satbox$SeaFly$util$Item;
        if (iArr == null) {
            iArr = new int[Item.values().length];
            try {
                iArr[Item.BackShot.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[Item.Black.ordinal()] = 12;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[Item.DoubleShot.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[Item.Item_Point.ordinal()] = 13;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[Item.PowerShot.ordinal()] = 4;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[Item.Rainbow.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[Item.Shield.ordinal()] = 5;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[Item.SpeedUp.ordinal()] = 1;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[Item.Star_Green.ordinal()] = 11;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[Item.Star_Orange.ordinal()] = 8;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[Item.Star_Purple.ordinal()] = 9;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[Item.Star_Red.ordinal()] = 7;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[Item.Star_Yellow.ordinal()] = 10;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[Item.Warning.ordinal()] = 14;
            } catch (NoSuchFieldError e14) {
            }
            $SWITCH_TABLE$jp$co$arttec$satbox$SeaFly$util$Item = iArr;
        }
        return iArr;
    }

    private ItemManager(Context context) {
        super(context);
        this.mContext = context;
    }

    public static ItemManager getInstance(Context context) {
        if (mSelf == null) {
            mSelf = new ItemManager(context);
        }
        return mSelf;
    }

    public Context getContext() {
        return this.mContext;
    }

    public void move() {
        if (this.mObject != null) {
            for (int i = 0; i < this.mObject.length; i++) {
                if (this.mObject[i] != null) {
                    this.mObject[i].move();
                }
            }
        }
    }

    public Position getPosition(int index) {
        if (this.mObject == null) {
            return null;
        }
        try {
            BaseObject object = this.mObject[index];
            if (object == null) {
                return null;
            }
            return object.getPosition();
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    public Position getPosition() {
        return null;
    }

    public Size getSize(int index) {
        if (this.mObject == null) {
            return null;
        }
        try {
            BaseObject object = this.mObject[index];
            if (object == null) {
                return null;
            }
            return object.getSize();
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    public Size getSize() {
        return null;
    }

    public void setPostion(int index, Position position) {
        if (this.mObject != null) {
            try {
                BaseObject object = this.mObject[index];
                if (object != null) {
                    object.setPosition(position);
                }
            } catch (IndexOutOfBoundsException e) {
            }
        }
    }

    public void setPosition(Position position) {
    }

    public void shot() {
    }

    public void onDraw(Canvas canvas) {
        if (this.mObject != null) {
            for (int i = 0; i < this.mObject.length; i++) {
                if (this.mObject[i] != null) {
                    this.mObject[i].draw(canvas);
                }
            }
        }
    }

    public int hit(AllyManager allyManager) {
        AllyBase ally;
        int gainScore = 0;
        if (this.mObject == null || allyManager == null || StageManager.getInstance().getGameOverFlag() || (ally = allyManager.getObject()) == null) {
            return 0;
        }
        for (int i = 0; i < this.mObject.length; i++) {
            ItemBase item = (ItemBase) this.mObject[i];
            if (item != null) {
                Position itemPosition = item.getPosition();
                if (HitObject.isHit(itemPosition, item.getSize(), ally.getPosition(), ally.getSize())) {
                    switch ($SWITCH_TABLE$jp$co$arttec$satbox$SeaFly$util$Item()[Item.values()[item.getItem().ordinal()].ordinal()]) {
                        case 1:
                            int shotSpeed = allyManager.getShotSpeed();
                            allyManager.setShotSpeed(shotSpeed - 1);
                            if (shotSpeed == allyManager.getShotSpeed()) {
                                gainScore = 1000;
                            }
                            EffectSoundManager.getInstance(this.mContext).play(EffectSoundFrequency.Powerup);
                            ally.setPowerUp();
                            break;
                        case 2:
                            if (2 != (ally.getShot() & 2)) {
                                ally.setShot(ally.getShot() | 2);
                            } else {
                                gainScore = 1000;
                            }
                            EffectSoundManager.getInstance(this.mContext).play(EffectSoundFrequency.Powerup);
                            ally.setPowerUp();
                            break;
                        case 3:
                            if (4 != (ally.getShot() & 4)) {
                                ally.setShot(ally.getShot() | 4);
                            } else {
                                gainScore = 1000;
                            }
                            EffectSoundManager.getInstance(this.mContext).play(EffectSoundFrequency.Powerup);
                            ally.setPowerUp();
                            break;
                        case 4:
                            if (8 != (ally.getShot() & 8)) {
                                ally.setShot(ally.getShot() | 8);
                                if (1 == (ally.getShot() & 1)) {
                                    ally.setShot(ally.getShot() - 1);
                                }
                            } else {
                                gainScore = 1000;
                            }
                            EffectSoundManager.getInstance(this.mContext).play(EffectSoundFrequency.Powerup);
                            ally.setPowerUp();
                            break;
                        case 5:
                            int shield = ally.getPower();
                            if (6 > shield) {
                                ally.gainPower(1);
                            }
                            if (shield == ally.getPower()) {
                                gainScore = 1000;
                            }
                            EffectSoundManager.getInstance(this.mContext).play(EffectSoundFrequency.Powerup);
                            ally.setPowerUp();
                            allyManager.setImage(this.mContext.getResources(), 0 | 9);
                            break;
                        case 6:
                            EffectSoundManager.getInstance(this.mContext).play(EffectSoundFrequency.Flash);
                            destroyEnemy();
                            break;
                        case 7:
                            gainScore = 10;
                            EffectSoundManager.getInstance(this.mContext).play(EffectSoundFrequency.Star_Item);
                            break;
                        case 8:
                            gainScore = 20;
                            EffectSoundManager.getInstance(this.mContext).play(EffectSoundFrequency.Star_Item);
                            break;
                        case 9:
                            gainScore = 30;
                            EffectSoundManager.getInstance(this.mContext).play(EffectSoundFrequency.Star_Item);
                            break;
                        case 10:
                            gainScore = 40;
                            EffectSoundManager.getInstance(this.mContext).play(EffectSoundFrequency.Star_Item);
                            break;
                        case 11:
                            gainScore = 50;
                            EffectSoundManager.getInstance(this.mContext).play(EffectSoundFrequency.Star_Item);
                            break;
                        case 12:
                            int speed = allyManager.getShotSpeed();
                            allyManager.setShotSpeed(speed + 1);
                            if (speed == allyManager.getShotSpeed()) {
                                int shot = ally.getShot();
                                if ((shot & 8) == 0) {
                                    if ((shot & 2) == 0) {
                                        if ((shot & 4) == 0) {
                                            int power = ally.getPower();
                                            if (power <= 0) {
                                                gainScore = -1000;
                                                break;
                                            } else {
                                                ally.setPower(power - 1);
                                                break;
                                            }
                                        } else {
                                            ally.setShot(shot - 4);
                                            break;
                                        }
                                    } else {
                                        ally.setShot(shot - 2);
                                        break;
                                    }
                                } else {
                                    ally.setShot((shot - 8) | 1);
                                    break;
                                }
                            }
                            break;
                        case 13:
                            gainScore = item.getScore();
                            EffectSoundManager.getInstance(this.mContext).play(EffectSoundFrequency.Star_Item);
                            break;
                    }
                    if (gainScore != 0) {
                        MessageManager.getInstance(this.mContext).addObject(new Message(itemPosition, this.mContext, String.valueOf(gainScore)));
                    }
                    this.mObject[i] = null;
                }
            }
        }
        return gainScore;
    }

    public void release() {
        super.release();
        if (mSelf != null) {
            mSelf = null;
        }
    }

    private void destroyEnemy() {
        EnemyManager enemyManager = EnemyManager.getInstance(this.mContext);
        if (enemyManager != null) {
            for (int i = 0; i < enemyManager.countObject(); i++) {
                EnemyBase enemy = (EnemyBase) enemyManager.mObject[i];
                if (!(enemy == null || EnemyKind.Boss1 == enemy.getEnemyKind() || EnemyKind.Boss2 == enemy.getEnemyKind() || EnemyKind.Boss3 == enemy.getEnemyKind() || EnemyKind.Boss4 == enemy.getEnemyKind() || EnemyKind.Boss5 == enemy.getEnemyKind() || EnemyKind.Boss6 == enemy.getEnemyKind())) {
                    switch ($SWITCH_TABLE$jp$co$arttec$satbox$SeaFly$util$EnemyKind()[EnemyKind.values()[enemy.getEnemyKind().ordinal()].ordinal()]) {
                        case 1:
                            ExplodeManager.getInstance(this.mContext).addObject(new EnemyExplode(enemy.getPosition(), this.mContext));
                            break;
                        case 2:
                            ExplodeManager.getInstance(this.mContext).addObject(new EnemyExplode(enemy.getPosition(), this.mContext));
                            break;
                        case 3:
                            ExplodeManager.getInstance(this.mContext).addObject(new EnemyExplode(enemy.getPosition(), this.mContext));
                            break;
                        case 4:
                            ExplodeManager.getInstance(this.mContext).addObject(new EnemyExplode(enemy.getPosition(), this.mContext));
                            break;
                        case 5:
                            ExplodeManager.getInstance(this.mContext).addObject(new EnemyExplode(enemy.getPosition(), this.mContext));
                            break;
                        case 6:
                            ExplodeManager.getInstance(this.mContext).addObject(new EnemyExplode(enemy.getPosition(), this.mContext));
                            break;
                    }
                    EnemyBulletManager bulletManager = EnemyBulletManager.getInstance(this.mContext);
                    for (int k = 0; k < bulletManager.countObject(); k++) {
                        BulletBase bullet = (BulletBase) bulletManager.mObject[k];
                        if (bullet != null && i == bullet.getId()) {
                            bulletManager.mObject[k] = null;
                        }
                    }
                    enemyManager.deleteObject(i);
                }
            }
        }
    }
}
