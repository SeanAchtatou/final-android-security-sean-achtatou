package jp.co.arttec.satbox.SeaFly.parts;

import android.content.Context;
import android.graphics.Rect;
import jp.co.arttec.satbox.SeaFly.util.Position;
import jp.co.arttec.satbox.SeaFly.util.Random;

public class PlanetBase extends BaseObject {
    public PlanetBase(Position position, Context context) {
        init(context);
        this.mPosition.x = position.x;
        this.mPosition.y = position.y;
    }

    public PlanetBase(Rect screenRect, Context context) {
        init(context);
        this.mPosition.x = Random.getInstance().nextInt(screenRect.right - getSize().mWidth);
        this.mPosition.y = -getSize().mHeight;
    }

    /* access modifiers changed from: protected */
    public void init(Context context) {
        calcDirection();
    }

    /* access modifiers changed from: protected */
    public void calcDirection() {
        this.mDirection.dx = 0;
        this.mDirection.dy = 1;
    }
}
