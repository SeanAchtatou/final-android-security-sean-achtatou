package jp.co.arttec.satbox.SeaFly;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import jp.co.arttec.satbox.SeaFly.manager.EffectSoundManager;
import jp.co.arttec.satbox.SeaFly.opening.TitleActivity;
import jp.co.arttec.satbox.SeaFly.util.EffectSoundFrequency;

public class HighScoreActivity extends HighScoreBaseActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.hiscore);
        ((Button) findViewById(R.id.ok)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                EffectSoundManager.getInstance(HighScoreActivity.this.getApplication().getApplicationContext()).play(EffectSoundFrequency.Title_SE);
                Intent intent = new Intent();
                intent.setClass(HighScoreActivity.this, TitleActivity.class);
                HighScoreActivity.this.startActivity(intent);
                HighScoreActivity.this.finish();
            }
        });
        getHighScore(49);
    }
}
