package jp.co.arttec.satbox.SeaFly.parts;

import android.content.Context;
import android.graphics.Rect;
import jp.co.arttec.satbox.SeaFly.R;
import jp.co.arttec.satbox.SeaFly.util.EnemyKind;
import jp.co.arttec.satbox.SeaFly.util.Position;
import jp.co.arttec.satbox.SeaFly.util.Random;

public class EnemyType3 extends EnemyBase {
    private AllyBase mAlly;
    private int mFrame = 0;
    private boolean mIsSnipe;

    public EnemyType3(Rect screenRect, Context context, AllyBase ally) {
        super(screenRect);
        setImage(context.getResources(), R.drawable.enemy3);
        calcDirection();
        this.mPosition.x = Random.getInstance().nextInt(screenRect.right - getSize().mWidth);
        this.mPosition.y = -getSize().mHeight;
        this.mPower = 1;
        setEnemyKind(EnemyKind.Type3);
        this.mBulletFrequency = 3;
        this.mScore = 400;
        this.mAlly = ally;
        this.mIsSnipe = false;
    }

    public void move() {
        this.mFrame++;
        calcDirection();
        super.move();
    }

    /* access modifiers changed from: protected */
    public void calcDirection() {
        if (this.mFrame < 60) {
            this.mDirection.dx = 0;
            this.mDirection.dy = 2;
        } else if (this.mAlly != null && !this.mIsSnipe) {
            this.mIsSnipe = true;
            Position position = this.mAlly.getPosition();
            int diff = (int) Math.sqrt((double) (((position.x - this.mPosition.x) * (position.x - this.mPosition.x)) + ((position.y - this.mPosition.y) * (position.y - this.mPosition.y))));
            if (diff == 0) {
                this.mDirection.dx = 0;
                this.mDirection.dy = 20;
                return;
            }
            this.mDirection.dx = ((position.x - this.mPosition.x) * 20) / diff;
            this.mDirection.dy = ((position.y - this.mPosition.y) * 20) / diff;
        }
    }
}
