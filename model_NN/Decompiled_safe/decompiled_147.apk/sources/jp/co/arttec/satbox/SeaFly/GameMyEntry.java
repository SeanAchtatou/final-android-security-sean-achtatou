package jp.co.arttec.satbox.SeaFly;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;

public class GameMyEntry {
    private Context context;
    private int entryScore;
    private String subPrefID;

    public GameMyEntry(Context context2, String subPrefID2, int entryScore2) {
        this.context = context2;
        this.subPrefID = subPrefID2;
        this.entryScore = entryScore2;
    }

    public void onMyScoreEntry() {
        SharedPreferences pref = this.context.getSharedPreferences("prefkey", 0);
        ArrayList<RankStrDelivery> itemSort = new ArrayList<>();
        itemSort.clear();
        for (int i = 0; i < 99; i++) {
            String strRankNo = String.format("%02d", Integer.valueOf(i + 1));
            itemSort.add(new RankStrDelivery(pref.getInt("MyScore" + this.subPrefID + strRankNo, 0), pref.getString("MyDate" + this.subPrefID + strRankNo, "9999/99/99 99:99:99"), "", ""));
        }
        Calendar calendar = Calendar.getInstance();
        int myYear = calendar.get(1);
        int myMonth = calendar.get(2) + 1;
        int myDay = calendar.get(5);
        int myHour = calendar.get(11);
        int myMinute = calendar.get(12);
        int mySecond = calendar.get(13);
        itemSort.add(new RankStrDelivery(this.entryScore, String.valueOf(String.format("%04d", Integer.valueOf(myYear))) + "/" + String.format("%02d", Integer.valueOf(myMonth)) + "/" + String.format("%02d", Integer.valueOf(myDay)) + " " + String.format("%02d", Integer.valueOf(myHour)) + ":" + String.format("%02d", Integer.valueOf(myMinute)) + ":" + String.format("%02d", Integer.valueOf(mySecond)), "", ""));
        Collections.sort(itemSort, new Comparator<RankStrDelivery>() {
            public int compare(RankStrDelivery o1, RankStrDelivery o2) {
                return o2.getRankNo() - o1.getRankNo();
            }
        });
        SharedPreferences.Editor editor = pref.edit();
        for (int i2 = 0; i2 < 99; i2++) {
            String strRankNo2 = String.format("%02d", Integer.valueOf(i2 + 1));
            RankStrDelivery rsd = (RankStrDelivery) itemSort.get(i2);
            editor.putInt("MyScore" + this.subPrefID + strRankNo2, rsd.getRankNo());
            editor.putString("MyDate" + this.subPrefID + strRankNo2, rsd.getItem1());
        }
        editor.commit();
    }
}
