package jp.co.arttec.satbox.SeaFly;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.List;
import jp.co.arttec.satbox.SeaFly.opening.TitleActivity;
import jp.co.arttec.satbox.SeaFly.util.BaseActivity;

public class GameRankMy extends BaseActivity {
    private Button btn_world_ranking;
    private ListView mListView;
    private String rankID = "LOCAL";
    /* access modifiers changed from: private */
    public int soundIds;
    /* access modifiers changed from: private */
    public SoundPool soundPool;
    /* access modifiers changed from: private */
    public float spVolume = 0.5f;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.rank);
        this.soundPool = new SoundPool(1, 3, 0);
        this.soundIds = this.soundPool.load(this, R.raw.se_title, 1);
        this.mListView = (ListView) findViewById(R.id.ranklistview);
        this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
            }
        });
        this.btn_world_ranking = (Button) findViewById(R.id.rank_tx_touch);
        this.btn_world_ranking.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                GameRankMy.this.soundPool.play(GameRankMy.this.soundIds, GameRankMy.this.spVolume, GameRankMy.this.spVolume, 1, 0, 1.0f);
                GameRankMy.this.startActivity(new Intent(GameRankMy.this, HighScoreActivity.class));
                GameRankMy.this.finish();
            }
        });
        onScoreEntry(this.rankID);
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.soundPool != null) {
            this.soundPool.stop(this.soundIds);
            this.soundPool.release();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        this.soundPool.play(this.soundIds, this.spVolume, this.spVolume, 1, 0, 1.0f);
        startActivity(new Intent(this, TitleActivity.class));
        finish();
        return true;
    }

    private synchronized void onScoreEntry(String subPrefID) {
        SharedPreferences pref = getSharedPreferences("prefkey", 0);
        List<RankStrDelivery> dataList = new ArrayList<>();
        for (int i = 0; i < 99; i++) {
            int intRankNo = i + 1;
            String strRankNo = String.format("%02d", Integer.valueOf(intRankNo));
            String strScore = String.valueOf(pref.getInt("MyScore" + subPrefID + strRankNo, 0));
            String strDate = pref.getString("MyDate" + subPrefID + strRankNo, "9999/99/99 99:99:99");
            dataList.add(new RankStrDelivery(intRankNo, strScore, strDate.substring(0, 10), strDate.substring(11, 19)));
        }
        ((ListView) findViewById(R.id.ranklistview)).setAdapter((ListAdapter) new RankAdapter(this, R.layout.row_my, dataList));
    }
}
