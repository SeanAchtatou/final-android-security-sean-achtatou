package jp.co.arttec.satbox.SeaFly.parts;

import android.content.Context;
import jp.co.arttec.satbox.SeaFly.util.Item;
import jp.co.arttec.satbox.SeaFly.util.Position;

public class ItemBase extends BaseObject {
    private Item mItem;

    public ItemBase(Position position, Context context) {
        init(context);
        this.mPosition.x = position.x;
        this.mPosition.y = position.y;
    }

    /* access modifiers changed from: protected */
    public void init(Context context) {
        calcDirection();
    }

    public void setItem(Item item) {
        this.mItem = item;
    }

    public Item getItem() {
        return this.mItem;
    }

    /* access modifiers changed from: protected */
    public void calcDirection() {
        this.mDirection.dx = 0;
        this.mDirection.dy = 3;
    }
}
