package jp.co.arttec.satbox.SeaFly.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class LifeView extends SurfaceView implements SurfaceHolder.Callback, Runnable {
    private int mCurrentLife;
    private SurfaceHolder mHolder;
    private int mMaxLife;
    private Thread mThread;

    public LifeView(Context context) {
        super(context);
        initialize(context);
    }

    public LifeView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initialize(context);
    }

    public LifeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize(context);
    }

    private void initialize(Context context) {
        this.mThread = null;
        this.mMaxLife = 1;
        this.mCurrentLife = 0;
        this.mHolder = getHolder();
        this.mHolder.addCallback(this);
        this.mHolder.setFixedSize(getWidth(), getHeight());
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    public void surfaceCreated(SurfaceHolder holder) {
        this.mThread = new Thread(this);
        this.mThread.start();
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        this.mThread = null;
    }

    public void run() {
        Canvas canvas = null;
        Paint paint = new Paint();
        while (this.mThread != null) {
            try {
                canvas = this.mHolder.lockCanvas();
                paint.setColor(-65536);
                canvas.drawRect(0.0f, 0.0f, (float) getWidth(), (float) getHeight(), paint);
                paint.setColor(-16711936);
                canvas.drawRect(0.0f, 0.0f, (float) ((getWidth() * this.mCurrentLife) / this.mMaxLife), (float) getHeight(), paint);
            } finally {
                if (canvas != null) {
                    this.mHolder.unlockCanvasAndPost(canvas);
                }
            }
        }
    }

    public void setMaxLife(int maxLife) {
        this.mMaxLife = maxLife;
        if (this.mMaxLife < 0) {
            this.mMaxLife = 1;
        }
    }

    public void setCurrentLife(int currentLife) {
        this.mCurrentLife = currentLife;
        if (this.mCurrentLife < 0) {
            this.mCurrentLife = 0;
        }
    }
}
