package jp.co.arttec.satbox.SeaFly.util;

public class Random extends java.util.Random {
    private static Random mSelf = null;
    private static final long serialVersionUID = -2770923703715863469L;
    private RandomIntGenerator generator;

    private Random() {
        this(System.currentTimeMillis());
    }

    private Random(long seed) {
        super(seed);
        this.generator = null;
        setSeed(seed);
    }

    private Random(int[] seedArray) {
        this.generator = null;
        init(seedArray);
    }

    public static Random getInstance() {
        if (mSelf == null) {
            mSelf = new Random();
        }
        return mSelf;
    }

    public static Random getInstance(long seed) {
        if (mSelf == null) {
            mSelf = new Random(seed);
        }
        return mSelf;
    }

    public static Random getInstance(int[] seedArray) {
        if (mSelf == null) {
            mSelf = new Random(seedArray);
        }
        return mSelf;
    }

    public synchronized void setSeed(long seed) {
        int[] seedArray = {(int) (-1 & seed), (int) (seed >>> 32)};
        if (seedArray[1] == 0) {
            init(seedArray[0]);
        } else {
            init(seedArray);
        }
    }

    /* access modifiers changed from: protected */
    public int next(int bits) {
        return this.generator.nextInt() >>> (32 - bits);
    }

    public synchronized void setSeed(int[] seedArray) {
        init(seedArray);
    }

    private void init(int seed) {
        if (this.generator == null) {
            this.generator = new RandomIntGenerator(seed);
        } else {
            this.generator.init(seed);
        }
    }

    private void init(int[] seedArray) {
        if (this.generator == null) {
            this.generator = new RandomIntGenerator(seedArray);
        } else {
            this.generator.init(seedArray);
        }
    }
}
