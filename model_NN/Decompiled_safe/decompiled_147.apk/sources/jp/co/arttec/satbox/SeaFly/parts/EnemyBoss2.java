package jp.co.arttec.satbox.SeaFly.parts;

import android.content.Context;
import android.graphics.Rect;
import jp.co.arttec.satbox.SeaFly.R;
import jp.co.arttec.satbox.SeaFly.manager.StageManager;
import jp.co.arttec.satbox.SeaFly.util.EnemyKind;
import jp.co.arttec.satbox.SeaFly.util.Random;
import jp.co.arttec.satbox.scoreranklib.GAECommonData;

public class EnemyBoss2 extends EnemyBase {
    private static final int STATE_DOWN_LEFT = 1;
    private static final int STATE_DOWN_RIGHT = 2;
    private static final int STATE_UP_LEFT = 3;
    private static final int STATE_UP_RIGHT = 4;
    private Rect mScreenRect;
    private int mState = 2;

    public EnemyBoss2(Rect screenRect, Context context) {
        super(screenRect);
        setImage(context.getResources(), R.drawable.enemy_big1);
        this.mPosition.x = (screenRect.right - getSize().mWidth) / 2;
        this.mPosition.y = -this.mSize.mHeight;
        calcDirection();
        this.mPower = (StageManager.getInstance().getStage() * 25) + 80;
        setEnemyKind(EnemyKind.Boss2);
        this.mScore = StageManager.getInstance().getStage() * GAECommonData.CONNECTION_TIMEOUT;
        this.mScreenRect = screenRect;
        this.mBulletFrequency = StageManager.getInstance().getStage() + 7;
    }

    public void move() {
        calcDirection();
        super.move();
        if (this.mPosition.x < 0) {
            this.mPosition.x = 0;
            if (Random.getInstance().nextInt(2) == 0) {
                this.mState = 2;
            } else {
                this.mState = 4;
            }
        }
        if (this.mPosition.x > this.mScreenRect.right - this.mSize.mWidth) {
            this.mPosition.x = this.mScreenRect.right - this.mSize.mWidth;
            if (Random.getInstance().nextInt(2) == 0) {
                this.mState = 1;
            } else {
                this.mState = 3;
            }
        }
        if (this.mPosition.y < 0) {
            this.mPosition.y = 0;
            if (Random.getInstance().nextInt(2) == 0) {
                this.mState = 2;
            } else {
                this.mState = 1;
            }
        }
        if (this.mPosition.y > (this.mScreenRect.bottom / 3) - this.mSize.mHeight) {
            this.mPosition.y = (this.mScreenRect.bottom / 3) - this.mSize.mHeight;
            if (Random.getInstance().nextInt(2) == 0) {
                this.mState = 4;
            } else {
                this.mState = 3;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void calcDirection() {
        switch (this.mState) {
            case 1:
                this.mDirection.dx = -8;
                this.mDirection.dy = 8;
                return;
            case 2:
                this.mDirection.dx = 8;
                this.mDirection.dy = 8;
                return;
            case 3:
                this.mDirection.dx = -8;
                this.mDirection.dy = -8;
                return;
            case 4:
                this.mDirection.dx = 8;
                this.mDirection.dy = -8;
                return;
            default:
                return;
        }
    }
}
