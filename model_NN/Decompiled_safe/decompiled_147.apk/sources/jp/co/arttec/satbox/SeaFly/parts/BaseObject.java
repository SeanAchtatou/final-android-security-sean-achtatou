package jp.co.arttec.satbox.SeaFly.parts;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import jp.co.arttec.satbox.SeaFly.manager.CacheImageManager;
import jp.co.arttec.satbox.SeaFly.util.Direction;
import jp.co.arttec.satbox.SeaFly.util.Position;
import jp.co.arttec.satbox.SeaFly.util.Size;

public abstract class BaseObject {
    protected Direction mDirection = new Direction();
    protected int mID;
    protected Bitmap mImage = null;
    protected String mMessage;
    protected Position mPosition = new Position();
    protected int mPower;
    protected int mScore = 0;
    protected Size mSize = null;

    /* access modifiers changed from: protected */
    public abstract void calcDirection();

    public void gainPower(int gain) {
        this.mPower += gain;
    }

    public void setId(int id) {
        this.mID = id;
    }

    public int getId() {
        return this.mID;
    }

    public void setImage(Resources res, int id) {
        if (CacheImageManager.getObject() != null) {
            this.mImage = CacheImageManager.getObject().getImageFromResource(id);
            if (this.mImage == null) {
                this.mImage = BitmapFactory.decodeResource(res, id);
            }
        } else {
            this.mImage = BitmapFactory.decodeResource(res, id);
        }
        if (this.mSize == null) {
            this.mSize = new Size(this.mImage.getWidth(), this.mImage.getHeight());
            return;
        }
        this.mSize.mWidth = this.mImage.getWidth();
        this.mSize.mHeight = this.mImage.getHeight();
    }

    public void draw(Canvas canvas) {
        canvas.drawBitmap(this.mImage, (float) this.mPosition.x, (float) this.mPosition.y, (Paint) null);
    }

    public void move() {
        this.mPosition.x += this.mDirection.dx;
        this.mPosition.y += this.mDirection.dy;
    }

    public Position getPosition() {
        return this.mPosition;
    }

    public void setPosition(Position position) {
        this.mPosition = position;
    }

    public Size getSize() {
        return this.mSize;
    }

    public void setMessage(String message) {
        this.mMessage = message;
    }

    public String getMessage() {
        return this.mMessage;
    }

    public int getPower() {
        return this.mPower;
    }

    public int getScore() {
        return this.mScore;
    }
}
