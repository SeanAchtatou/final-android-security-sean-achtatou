package jp.co.arttec.satbox.SeaFly.opening;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import jp.co.arttec.satbox.SeaFly.R;
import jp.co.arttec.satbox.SeaFly.util.BaseActivity;

public class SATBOXActivity extends BaseActivity {
    private static final int HIDE = 2;
    private static final int MAIN = 3;
    private static final int SHOW = 1;
    private ImageView _ImageView;
    /* access modifiers changed from: private */
    public Handler _handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    SATBOXActivity.this.logoFadeIn();
                    return;
                case 2:
                    SATBOXActivity.this.logoFadeOut();
                    return;
                case 3:
                    SATBOXActivity.this.gotoMain();
                    return;
                default:
                    return;
            }
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.credit);
        this._ImageView = (ImageView) findViewById(R.id.credit);
        this._handler.sendEmptyMessageDelayed(1, 0);
    }

    /* access modifiers changed from: private */
    public void logoFadeIn() {
        AlphaAnimation alpha = new AlphaAnimation(0.0f, 1.0f);
        alpha.setDuration(500);
        alpha.setRepeatCount(0);
        alpha.setFillAfter(true);
        alpha.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                SATBOXActivity.this._handler.sendEmptyMessageDelayed(2, 1000);
            }
        });
        this._ImageView.startAnimation(alpha);
    }

    /* access modifiers changed from: private */
    public void logoFadeOut() {
        AlphaAnimation alpha = new AlphaAnimation(1.0f, 0.0f);
        alpha.setDuration(500);
        alpha.setRepeatCount(0);
        alpha.setFillAfter(true);
        alpha.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                SATBOXActivity.this._handler.sendEmptyMessageDelayed(3, 0);
            }
        });
        this._ImageView.startAnimation(alpha);
    }

    /* access modifiers changed from: private */
    public void gotoMain() {
        try {
            Intent intent = new Intent(getApplicationContext(), ObjectInit.class);
            intent.addFlags(268435456);
            startActivity(intent);
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
