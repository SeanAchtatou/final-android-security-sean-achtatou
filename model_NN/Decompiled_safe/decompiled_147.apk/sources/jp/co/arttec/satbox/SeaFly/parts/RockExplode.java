package jp.co.arttec.satbox.SeaFly.parts;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import jp.co.arttec.satbox.SeaFly.R;
import jp.co.arttec.satbox.SeaFly.util.Position;

public class RockExplode extends ExplodeBase {
    private Context mContext;
    private int mDispWait = 0;
    private int[] mImageList = {R.drawable.explode_rock00, R.drawable.explode_rock01, R.drawable.explode_rock02, R.drawable.explode_rock03, R.drawable.explode_dummy};
    private int mWaitTime = 3;

    public RockExplode(Position position, Context context) {
        super(position, context);
        init(context);
        this.mContext = context;
        setImage(context.getResources(), this.mImageList[0]);
        setShowFrame(this.mImageList.length);
    }

    /* access modifiers changed from: protected */
    public void init(Context context) {
        calcDirection();
    }

    /* access modifiers changed from: protected */
    public void calcDirection() {
    }

    public void move() {
        if (this.mDispWait > this.mWaitTime) {
            super.move();
            this.mDispWait = 0;
        } else {
            this.mDispWait++;
        }
        Resources res = this.mContext.getResources();
        if (this.mImageList.length > getFrame()) {
            setImage(res, this.mImageList[getFrame()]);
        } else {
            setImage(res, this.mImageList[this.mImageList.length - 1]);
        }
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
    }
}
