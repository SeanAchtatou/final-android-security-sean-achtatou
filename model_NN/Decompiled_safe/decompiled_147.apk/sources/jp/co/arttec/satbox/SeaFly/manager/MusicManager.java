package jp.co.arttec.satbox.SeaFly.manager;

import android.content.Context;
import android.media.MediaPlayer;

public class MusicManager {
    private static MusicManager mSelf = null;
    private Context mContext;
    private MediaPlayer mMediaPlayer = null;

    private MusicManager(Context context) {
        this.mContext = context;
    }

    public static MusicManager getInstance(Context context) {
        if (mSelf == null) {
            mSelf = new MusicManager(context);
        }
        return mSelf;
    }

    public void play(int resId, boolean loop) {
        stop();
        final boolean looping = loop;
        this.mMediaPlayer = MediaPlayer.create(this.mContext, resId);
        this.mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(looping);
                mp.start();
            }
        });
    }

    public void stop() {
        if (this.mMediaPlayer != null && this.mMediaPlayer.isPlaying()) {
            this.mMediaPlayer.stop();
        }
    }

    public void suspend() {
        if (this.mMediaPlayer != null && this.mMediaPlayer.isPlaying()) {
            this.mMediaPlayer.pause();
        }
    }

    public void resume() {
        if (this.mMediaPlayer != null) {
            this.mMediaPlayer.start();
        }
    }

    public void release() {
        stop();
        if (this.mMediaPlayer != null) {
            this.mMediaPlayer.release();
            this.mMediaPlayer = null;
        }
        if (mSelf != null) {
            mSelf = null;
        }
    }
}
