package jp.co.arttec.satbox.SeaFly.util;

public class AllyState {
    public static final int Ally_Dummy = 0;
    public static final int Ally_Left = 2;
    public static final int Ally_Nightmare = 16;
    public static final int Ally_Right = 4;
    public static final int Ally_Shield = 8;
    public static final int Ally_Stand = 1;
}
