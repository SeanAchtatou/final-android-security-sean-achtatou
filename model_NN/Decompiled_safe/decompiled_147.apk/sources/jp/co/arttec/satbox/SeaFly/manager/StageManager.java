package jp.co.arttec.satbox.SeaFly.manager;

import java.util.HashMap;

public class StageManager {
    private static StageManager mSelf = null;
    private boolean mGameOverFlag;
    private int mStage;
    private HashMap<Integer, Integer> mStageMap = new HashMap<>();

    private StageManager() {
        this.mStageMap.put(1, 60);
        this.mStageMap.put(2, 75);
        this.mStageMap.put(3, 75);
        this.mStageMap.put(4, 90);
        this.mStageMap.put(5, 120);
        this.mStageMap.put(6, 120);
        this.mStage = 1;
        this.mGameOverFlag = false;
    }

    public static StageManager getInstance() {
        if (mSelf == null) {
            mSelf = new StageManager();
        }
        return mSelf;
    }

    public int getStageLength(int stage) {
        this.mStage = stage;
        Integer stageLength = this.mStageMap.get(Integer.valueOf(((stage - 1) % 6) + 1));
        if (stageLength != null) {
            return stageLength.intValue();
        }
        return 0;
    }

    public void release() {
        if (mSelf != null) {
            mSelf = null;
        }
    }

    public int getStage() {
        return this.mStage;
    }

    public void setGameOverFlag(boolean gameOverFlag) {
        this.mGameOverFlag = gameOverFlag;
    }

    public boolean getGameOverFlag() {
        return this.mGameOverFlag;
    }
}
