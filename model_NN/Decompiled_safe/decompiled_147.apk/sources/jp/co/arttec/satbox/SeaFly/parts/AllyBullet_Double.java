package jp.co.arttec.satbox.SeaFly.parts;

import android.content.Context;
import jp.co.arttec.satbox.SeaFly.R;

public class AllyBullet_Double extends BulletBase {
    public AllyBullet_Double(AllyBase ally, Context context, int shotNum) {
        setImage(context.getResources(), R.drawable.ally_bullet);
        this.mPosition.x = (ally.getPosition().x + ((ally.getSize().mWidth / 4) * ((shotNum * 2) + 1))) - (getSize().mWidth / 2);
        this.mPosition.y = ally.getPosition().y;
        calcDirection(shotNum);
        this.mPower = 1;
    }

    /* access modifiers changed from: protected */
    public void calcDirection() {
    }

    private void calcDirection(int shotNum) {
        switch (shotNum) {
            case 0:
                this.mDirection.dx = -2;
                this.mDirection.dy = -20;
                return;
            case 1:
                this.mDirection.dx = 2;
                this.mDirection.dy = -20;
                return;
            default:
                return;
        }
    }
}
