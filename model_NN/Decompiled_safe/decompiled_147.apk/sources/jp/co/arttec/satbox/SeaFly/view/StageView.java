package jp.co.arttec.satbox.SeaFly.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import jp.co.arttec.satbox.SeaFly.R;
import jp.co.arttec.satbox.SeaFly.event.BossDefeatListener;
import jp.co.arttec.satbox.SeaFly.event.GameOverListener;
import jp.co.arttec.satbox.SeaFly.event.GameStartListener;
import jp.co.arttec.satbox.SeaFly.event.MusicListener;
import jp.co.arttec.satbox.SeaFly.event.PauseListener;
import jp.co.arttec.satbox.SeaFly.event.ScoreListener;
import jp.co.arttec.satbox.SeaFly.event.ShieldListener;
import jp.co.arttec.satbox.SeaFly.event.ViewReleaseListener;
import jp.co.arttec.satbox.SeaFly.manager.AllyBulletManager;
import jp.co.arttec.satbox.SeaFly.manager.AllyManager;
import jp.co.arttec.satbox.SeaFly.manager.EffectSoundManager;
import jp.co.arttec.satbox.SeaFly.manager.EnemyManager;
import jp.co.arttec.satbox.SeaFly.manager.ExplodeManager;
import jp.co.arttec.satbox.SeaFly.manager.ItemManager;
import jp.co.arttec.satbox.SeaFly.manager.MessageManager;
import jp.co.arttec.satbox.SeaFly.manager.MusicManager;
import jp.co.arttec.satbox.SeaFly.manager.PlanetManager;
import jp.co.arttec.satbox.SeaFly.manager.PreferencesManager;
import jp.co.arttec.satbox.SeaFly.manager.RockManager;
import jp.co.arttec.satbox.SeaFly.manager.StageManager;
import jp.co.arttec.satbox.SeaFly.manager.StarManager;
import jp.co.arttec.satbox.SeaFly.parts.AllyBase;
import jp.co.arttec.satbox.SeaFly.parts.EnemyBase;
import jp.co.arttec.satbox.SeaFly.parts.EnemyBoss1;
import jp.co.arttec.satbox.SeaFly.parts.EnemyBoss2;
import jp.co.arttec.satbox.SeaFly.parts.EnemyBoss3;
import jp.co.arttec.satbox.SeaFly.parts.EnemyBoss4;
import jp.co.arttec.satbox.SeaFly.parts.EnemyBoss5;
import jp.co.arttec.satbox.SeaFly.parts.EnemyBoss6;
import jp.co.arttec.satbox.SeaFly.parts.EnemyType1;
import jp.co.arttec.satbox.SeaFly.parts.EnemyType2;
import jp.co.arttec.satbox.SeaFly.parts.EnemyType3;
import jp.co.arttec.satbox.SeaFly.parts.EnemyType4;
import jp.co.arttec.satbox.SeaFly.parts.EnemyType5;
import jp.co.arttec.satbox.SeaFly.parts.EnemyType6;
import jp.co.arttec.satbox.SeaFly.parts.Planet1;
import jp.co.arttec.satbox.SeaFly.parts.Planet2;
import jp.co.arttec.satbox.SeaFly.parts.Planet3;
import jp.co.arttec.satbox.SeaFly.parts.Planet4;
import jp.co.arttec.satbox.SeaFly.parts.Planet5;
import jp.co.arttec.satbox.SeaFly.parts.Rock1;
import jp.co.arttec.satbox.SeaFly.parts.Rock2;
import jp.co.arttec.satbox.SeaFly.parts.Rock3;
import jp.co.arttec.satbox.SeaFly.parts.Rock4;
import jp.co.arttec.satbox.SeaFly.parts.Star;
import jp.co.arttec.satbox.SeaFly.util.EnemyKind;
import jp.co.arttec.satbox.SeaFly.util.FPSManager;
import jp.co.arttec.satbox.SeaFly.util.Position;
import jp.co.arttec.satbox.SeaFly.util.Random;

public class StageView extends SurfaceView implements SurfaceHolder.Callback, BossDefeatListener {
    public static final int FPS = 30;
    private AllyBulletManager mAllyBulletManager;
    private AllyManager mAllyManager;
    private Bitmap mBackground;
    private Rect mBitmapRect;
    private boolean mBossAppear;
    private boolean mBossBattle;
    private Context mContext;
    private EnemyManager mEnemyManager;
    private ExplodeManager mExplodeManager;
    private int mFrame;
    private int mGainScore;
    private GameOverListener mGameOverListener;
    private GameStartListener mGameStartListener;
    private SurfaceHolder mHolder;
    private ItemManager mItemManager;
    private MessageManager mMessageManager;
    private MusicListener mMusicListener;
    private boolean mPause;
    private PauseListener mPauseListener;
    private PlanetManager mPlanetManager;
    private PreferencesManager mPreferenceManager;
    private Random mRandom;
    private Resources mResources;
    private RockManager mRockManager;
    private ScoreListener mScoreListener;
    private Rect mScreenRect;
    private ShieldListener mShieldListener;
    private int mStage;
    private StarManager mStarManager;
    private Thread mThread;
    private ViewReleaseListener mViewReleaseListener;

    public StageView(Context context) {
        super(context);
        this.mContext = context;
        initialize();
    }

    public StageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        initialize();
    }

    private void initialize() {
        getHolder().addCallback(this);
        setFocusable(true);
        requestFocus();
        this.mResources = this.mContext.getResources();
        this.mStarManager = StarManager.getInstance(this.mContext);
        this.mAllyManager = AllyManager.getInstance(this.mContext);
        this.mEnemyManager = EnemyManager.getInstance(this.mContext);
        this.mAllyBulletManager = AllyBulletManager.getInstance(this.mContext);
        this.mRockManager = RockManager.getInstance(this.mContext);
        this.mItemManager = ItemManager.getInstance(this.mContext);
        this.mExplodeManager = ExplodeManager.getInstance(this.mContext);
        this.mMessageManager = MessageManager.getInstance(this.mContext);
        this.mPlanetManager = PlanetManager.getInstance(this.mContext);
        this.mPreferenceManager = PreferencesManager.getInstance(this.mContext);
        this.mRandom = Random.getInstance();
        EffectSoundManager.getInstance(this.mContext);
        this.mBackground = BitmapFactory.decodeResource(this.mResources, R.drawable.game_background);
        this.mBitmapRect = new Rect(0, 0, this.mBackground.getWidth(), this.mBackground.getHeight());
        this.mPauseListener = null;
        this.mShieldListener = null;
        this.mScoreListener = null;
        this.mGainScore = 0;
        this.mMusicListener = null;
        this.mGameOverListener = null;
        this.mViewReleaseListener = null;
        this.mGameStartListener = null;
        this.mThread = null;
        this.mFrame = 0;
        this.mStage = 1;
        this.mRandom.setSeed((long) this.mStage);
        this.mBossBattle = false;
        this.mBossAppear = false;
        this.mScreenRect = null;
        StageManager.getInstance().setGameOverFlag(false);
        this.mPause = false;
    }

    public void setStage(int stage) {
        this.mStage = stage;
        this.mRandom.setSeed((long) this.mStage);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        this.mHolder = holder;
        AllyBase ally = new AllyBase(this.mContext);
        int allyState = 9;
        if (this.mStage >= 6) {
            ally.setPower(4);
            ally.setShot(14);
            this.mAllyManager.setShotSpeed(6);
        } else if (this.mStage >= 5) {
            ally.setPower(3);
            ally.setShot(7);
            this.mAllyManager.setShotSpeed(7);
        } else if (this.mStage >= 4) {
            ally.setPower(2);
            ally.setShot(3);
            this.mAllyManager.setShotSpeed(8);
        } else if (this.mStage >= 3) {
            allyState = 1;
            ally.setShot(1);
            ally.setPower(1);
        } else {
            allyState = 1;
            ally.setShot(1);
            ally.setPower(0);
        }
        ally.setAllyState(this.mResources, allyState);
        ally.setPosition(new Position((getWidth() - ally.getSize().mWidth) / 2, (getHeight() * 3) / 4));
        this.mAllyManager.addObject(ally);
        if (this.mShieldListener != null) {
            this.mShieldListener.setShield(this.mAllyManager.getShield(), this.mAllyManager.getShotSpeed());
        }
        if (this.mScoreListener != null) {
            this.mScoreListener.setScore(0);
        }
        while (this.mStarManager.isAddObject()) {
            this.mStarManager.addObject(new Star(new Position(this.mRandom.nextInt(getWidth()), this.mRandom.nextInt(getHeight())), this.mContext));
        }
        while (this.mPlanetManager.isAddObject()) {
            this.mPlanetManager.addObject(new Planet1(new Position(this.mRandom.nextInt(getWidth()), this.mRandom.nextInt(getHeight())), this.mContext));
        }
        while (this.mRockManager.isAddObject()) {
            this.mRockManager.addObject(new Rock1(new Position(this.mRandom.nextInt(getWidth()), this.mRandom.nextInt(getHeight())), this.mContext));
        }
        playMusic();
        if (this.mGameStartListener != null) {
            this.mGameStartListener.ready();
        }
        start();
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        release();
    }

    /* access modifiers changed from: private */
    public synchronized void onDraw() {
        int enemy_encount;
        EnemyBase enemy;
        int random;
        FPSManager fps = new FPSManager(30);
        int beforeShield = 0;
        if (this.mAllyManager != null) {
            beforeShield = this.mAllyManager.getShield();
        }
        int beforeSpeed = 0;
        if (this.mAllyManager != null) {
            beforeSpeed = this.mAllyManager.getShotSpeed();
        }
        while (this.mThread != null) {
            if (!this.mPause) {
                fps.startFrame(System.currentTimeMillis());
                Canvas canvas = this.mHolder.lockCanvas();
                if (canvas != null) {
                    if (this.mScreenRect == null) {
                        this.mScreenRect = new Rect(0, 0, canvas.getWidth(), canvas.getHeight());
                    }
                    if (!(this.mBitmapRect == null || this.mScreenRect == null)) {
                        canvas.drawBitmap(this.mBackground, this.mBitmapRect, this.mScreenRect, (Paint) null);
                    }
                    int stage = (this.mStage - 1) % 6;
                    if (2 > stage) {
                        enemy_encount = this.mRandom.nextInt(85);
                    } else {
                        enemy_encount = this.mRandom.nextInt(100);
                    }
                    if (!(this.mBossBattle || enemy_encount >= stage + 1 || this.mEnemyManager == null || this.mAllyManager == null || this.mAllyManager.getObject() == null)) {
                        EnemyBase enemy2 = null;
                        if (1 >= stage) {
                            random = this.mRandom.nextInt(2);
                        } else if (2 >= stage) {
                            random = this.mRandom.nextInt(3);
                        } else if (3 >= stage) {
                            random = this.mRandom.nextInt(4);
                        } else {
                            random = this.mRandom.nextInt(6);
                        }
                        switch (random) {
                            case 0:
                                enemy2 = new EnemyType1(this.mScreenRect, this.mContext);
                                break;
                            case 1:
                                enemy2 = new EnemyType2(this.mScreenRect, this.mContext);
                                break;
                            case 2:
                                enemy2 = new EnemyType3(this.mScreenRect, this.mContext, this.mAllyManager.getObject());
                                break;
                            case 3:
                                enemy2 = new EnemyType4(this.mScreenRect, this.mContext);
                                break;
                            case 4:
                                enemy2 = new EnemyType5(this.mScreenRect, this.mContext);
                                break;
                            case 5:
                                enemy2 = new EnemyType6(this.mScreenRect, this.mContext);
                                break;
                        }
                        if (enemy2 != null) {
                            enemy2.setBulletFrequency(enemy2.getBulletFrequency() + 1);
                            this.mEnemyManager.addObject(enemy2);
                        }
                    }
                    if (!StageManager.getInstance().getGameOverFlag() && this.mBossBattle && !this.mBossAppear) {
                        this.mBossAppear = true;
                        switch ((this.mStage - 1) % 6) {
                            case 0:
                                enemy = new EnemyBoss2(this.mScreenRect, this.mContext);
                                enemy.setBossDefeatListener(this);
                                if (this.mMusicListener != null) {
                                    this.mMusicListener.playMusic(R.raw.boss, true);
                                    break;
                                }
                                break;
                            case 1:
                                enemy = new EnemyBoss1(this.mScreenRect, this.mContext);
                                enemy.setBossDefeatListener(this);
                                if (this.mMusicListener != null) {
                                    this.mMusicListener.playMusic(R.raw.boss, true);
                                    break;
                                }
                                break;
                            case 2:
                                enemy = new EnemyBoss3(this.mScreenRect, this.mContext);
                                enemy.setBossDefeatListener(this);
                                if (this.mMusicListener != null) {
                                    this.mMusicListener.playMusic(R.raw.boss, true);
                                    break;
                                }
                                break;
                            case 3:
                                enemy = new EnemyBoss5(this.mScreenRect, this.mContext);
                                enemy.setBossDefeatListener(this);
                                if (this.mMusicListener != null) {
                                    this.mMusicListener.playMusic(R.raw.boss, true);
                                    break;
                                }
                                break;
                            case 4:
                                enemy = new EnemyBoss4(this.mScreenRect, this.mContext);
                                enemy.setBossDefeatListener(this);
                                if (this.mMusicListener != null) {
                                    this.mMusicListener.playMusic(R.raw.boss, true);
                                    break;
                                }
                                break;
                            case 5:
                                enemy = new EnemyBoss6(this.mScreenRect, this.mContext);
                                enemy.setBossDefeatListener(this);
                                if (this.mMusicListener != null) {
                                    this.mMusicListener.playMusic(R.raw.boss, true);
                                    break;
                                }
                                break;
                            default:
                                enemy = new EnemyBoss1(this.mScreenRect, this.mContext);
                                enemy.setBossDefeatListener(this);
                                if (this.mMusicListener != null) {
                                    this.mMusicListener.playMusic(R.raw.boss, true);
                                    break;
                                }
                                break;
                        }
                        if (enemy != null) {
                            this.mEnemyManager.addObject(enemy);
                        }
                    }
                    moveStar(canvas);
                    movePlanet(canvas);
                    moveMeteor(canvas);
                    moveEnemy(canvas);
                    drawAlly(canvas);
                    moveAllyBullet(canvas);
                    moveItem(canvas);
                    beforeShield = modifyShield(beforeShield);
                    beforeSpeed = modifySpeed(beforeSpeed);
                    modifyScore();
                    drawExplode(canvas);
                    drawMessage(canvas);
                    this.mHolder.unlockCanvasAndPost(canvas);
                    if (StageManager.getInstance().getGameOverFlag()) {
                        this.mFrame = 0;
                        if (this.mMusicListener != null) {
                            this.mMusicListener.stopMusic();
                        }
                        if (this.mGameOverListener != null) {
                            this.mGameOverListener.gameOver();
                            this.mGameOverListener = null;
                        }
                    }
                    this.mFrame++;
                    if (StageManager.getInstance().getStageLength(this.mStage) < this.mFrame / 30 && !this.mBossBattle) {
                        this.mBossBattle = true;
                        if (this.mMusicListener != null) {
                            this.mMusicListener.stopMusic();
                        }
                    }
                    if (this.mFrame > 60 && this.mGameStartListener != null) {
                        this.mGameStartListener.readyEnd();
                        this.mGameStartListener = null;
                    }
                    while (!fps.checkArrival(System.currentTimeMillis())) {
                        try {
                            Thread.sleep(1);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    continue;
                }
            }
            modifyPause();
        }
    }

    public void endView() {
        if (this.mMusicListener != null) {
            this.mMusicListener.stopMusic();
            MusicManager.getInstance(this.mContext).release();
        }
        this.mAllyBulletManager.release();
        this.mEnemyManager.release();
        this.mItemManager.release();
        this.mRockManager.release();
        this.mAllyManager.release();
        this.mMessageManager.release();
        this.mExplodeManager.release();
        this.mPlanetManager.release();
        this.mPreferenceManager.release();
        EffectSoundManager.getInstance(this.mContext).release();
        if (this.mViewReleaseListener != null) {
            this.mViewReleaseListener.releaseFinish();
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        int allyState;
        try {
            Thread.sleep(16);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (this.mAllyManager == null) {
            return super.onTouchEvent(event);
        }
        if (StageManager.getInstance().getGameOverFlag()) {
            this.mAllyManager.setImage(this.mResources, 0);
            return super.onTouchEvent(event);
        }
        int shield = this.mAllyManager.getShield();
        Position allyPosition = this.mAllyManager.getPosition();
        Position touchPosition = this.mAllyManager.getTouchPosition();
        switch (event.getAction()) {
            case 0:
                touchPosition.x = (int) event.getX();
                touchPosition.y = (int) event.getY();
                this.mAllyManager.setTouchPosition(touchPosition);
                this.mAllyManager.setShot(true);
                return true;
            case 1:
                touchPosition.x = 0;
                touchPosition.y = 0;
                this.mAllyManager.setTouchPosition(touchPosition);
                int allyState2 = 0 | 1;
                if (shield > 0) {
                    allyState2 |= 8;
                }
                this.mAllyManager.setImage(this.mResources, allyState2);
                this.mAllyManager.setShot(false);
                return true;
            case 2:
                Position diff = new Position(((int) event.getX()) - touchPosition.x, ((int) event.getY()) - touchPosition.y);
                int x = allyPosition.x + diff.x;
                int y = allyPosition.y + diff.y;
                if (diff.x < -3) {
                    allyState = 0 | 2;
                } else if (diff.x > 3) {
                    allyState = 0 | 4;
                } else {
                    allyState = 0 | 1;
                }
                if (shield > 0) {
                    allyState |= 8;
                }
                this.mAllyManager.setImage(this.mResources, allyState);
                if (x < 0) {
                    x = 0;
                }
                if (this.mAllyManager.getSize().mWidth + x > getWidth()) {
                    x = getWidth() - this.mAllyManager.getSize().mWidth;
                }
                if (y < 0) {
                    y = 0;
                }
                if (this.mAllyManager.getSize().mHeight + y > getHeight()) {
                    y = getHeight() - this.mAllyManager.getSize().mHeight;
                }
                allyPosition.x = x;
                allyPosition.y = y;
                this.mAllyManager.move(allyPosition);
                touchPosition.x = (int) event.getX();
                touchPosition.y = (int) event.getY();
                this.mAllyManager.setTouchPosition(touchPosition);
                return true;
            default:
                return super.onTouchEvent(event);
        }
    }

    private void moveStar(Canvas canvas) {
        if (this.mStarManager != null) {
            while (this.mStarManager.isAddObject()) {
                this.mStarManager.addObject(new Star(this.mScreenRect, this.mContext));
            }
            this.mStarManager.move();
            this.mStarManager.deleteObject(this.mScreenRect);
            this.mStarManager.onDraw(canvas);
        }
    }

    private void movePlanet(Canvas canvas) {
        if (this.mPlanetManager != null) {
            while (this.mPlanetManager.isAddObject()) {
                switch (this.mRandom.nextInt(5)) {
                    case 0:
                        this.mPlanetManager.addObject(new Planet1(this.mScreenRect, this.mContext));
                        break;
                    case 1:
                        this.mPlanetManager.addObject(new Planet2(this.mScreenRect, this.mContext));
                        break;
                    case 2:
                        this.mPlanetManager.addObject(new Planet3(this.mScreenRect, this.mContext));
                        break;
                    case 3:
                        this.mPlanetManager.addObject(new Planet4(this.mScreenRect, this.mContext));
                        break;
                    case 4:
                        this.mPlanetManager.addObject(new Planet5(this.mScreenRect, this.mContext));
                        break;
                }
            }
            this.mPlanetManager.move();
            this.mPlanetManager.deleteObject(this.mScreenRect);
            this.mPlanetManager.onDraw(canvas);
        }
    }

    private void moveMeteor(Canvas canvas) {
        if (this.mRockManager != null) {
            if (this.mRandom.nextInt(400) < 2) {
                switch (this.mRandom.nextInt(5)) {
                    case 0:
                    case 1:
                    case 2:
                        Rock2 rock = new Rock2(this.mScreenRect, this.mContext);
                        rock.setRockKind(EnemyKind.Rock1);
                        this.mRockManager.addObject(rock);
                        break;
                    case 3:
                        if (!this.mBossBattle) {
                            Rock3 rock2 = new Rock3(this.mScreenRect, this.mContext);
                            rock2.setRockKind(EnemyKind.Rock2);
                            this.mRockManager.addObject(rock2);
                            break;
                        }
                        break;
                    case 4:
                        if (!this.mBossBattle) {
                            Rock4 rock3 = new Rock4(this.mScreenRect, this.mContext);
                            rock3.setRockKind(EnemyKind.Rock2);
                            this.mRockManager.addObject(rock3);
                            break;
                        }
                        break;
                }
            }
            this.mRockManager.move();
            this.mRockManager.deleteObject(this.mScreenRect);
            this.mRockManager.onDraw(canvas);
            this.mRockManager.isHit(this.mAllyManager);
        }
    }

    private void moveEnemy(Canvas canvas) {
        if (this.mEnemyManager != null) {
            this.mEnemyManager.move();
            this.mEnemyManager.deleteObject(this.mScreenRect);
            this.mEnemyManager.onDraw(canvas);
            if (this.mAllyManager != null) {
                this.mEnemyManager.shot(this.mAllyManager.getObject(), this.mContext);
                this.mEnemyManager.isHit(this.mAllyManager);
                this.mEnemyManager.isHitBullet(this.mAllyManager);
                this.mEnemyManager.isHit(this.mRockManager);
            }
        }
    }

    private void drawAlly(Canvas canvas) {
        if (this.mAllyManager != null) {
            this.mAllyManager.onDraw(canvas);
            if (this.mAllyManager.isShot() && this.mAllyBulletManager != null) {
                this.mAllyBulletManager.addBullet(this.mAllyManager.getObject());
            }
        }
    }

    private void moveAllyBullet(Canvas canvas) {
        if (this.mAllyBulletManager != null) {
            this.mAllyBulletManager.move();
            this.mAllyBulletManager.deleteObject(this.mScreenRect);
            this.mAllyBulletManager.onDraw(canvas);
            this.mGainScore += this.mAllyBulletManager.hitBullet(this.mEnemyManager);
            this.mGainScore += this.mAllyBulletManager.hitBullet(this.mRockManager, this.mItemManager);
        }
    }

    private void moveItem(Canvas canvas) {
        if (this.mItemManager != null) {
            this.mItemManager.move();
            this.mItemManager.deleteObject(this.mScreenRect);
            this.mItemManager.onDraw(canvas);
            this.mGainScore += this.mItemManager.hit(this.mAllyManager);
        }
    }

    private void drawExplode(Canvas canvas) {
        if (this.mExplodeManager != null) {
            this.mExplodeManager.move();
            this.mExplodeManager.deleteObject();
            this.mExplodeManager.onDraw(canvas);
        }
    }

    private void drawMessage(Canvas canvas) {
        if (this.mExplodeManager != null) {
            this.mMessageManager.move();
            this.mMessageManager.deleteObject();
            this.mMessageManager.onDraw(canvas);
        }
    }

    private int modifyShield(int beforeShield) {
        int shield;
        if (this.mShieldListener == null || this.mAllyManager == null || this.mAllyManager.getObject() == null || beforeShield == (shield = this.mAllyManager.getObject().getPower())) {
            return beforeShield;
        }
        this.mShieldListener.setShield(shield, this.mAllyManager.getShotSpeed());
        return shield;
    }

    private int modifySpeed(int beforeSpeed) {
        int speed;
        if (this.mShieldListener == null || this.mAllyManager == null || beforeSpeed == (speed = this.mAllyManager.getShotSpeed())) {
            return beforeSpeed;
        }
        this.mShieldListener.setShield(this.mAllyManager.getObject().getPower(), speed);
        return speed;
    }

    private void modifyScore() {
        if (this.mScoreListener != null) {
            this.mScoreListener.setScore(this.mGainScore);
            this.mGainScore = 0;
        }
    }

    private void modifyPause() {
        if (this.mPauseListener != null) {
            this.mPauseListener.pause();
        }
    }

    public void setOnShieldListener(ShieldListener listener) {
        this.mShieldListener = listener;
    }

    public void setOnScoreListener(ScoreListener listener) {
        this.mScoreListener = listener;
    }

    public void setOnMusicListener(MusicListener listener) {
        this.mMusicListener = listener;
    }

    public void setOnGameOverListener(GameOverListener listener) {
        this.mGameOverListener = listener;
    }

    public void setOnReleaseListener(ViewReleaseListener listener) {
        this.mViewReleaseListener = listener;
    }

    public void setOnGameStartListener(GameStartListener listener) {
        this.mGameStartListener = listener;
    }

    public void setOnPauseListener(PauseListener listener) {
        this.mPauseListener = listener;
    }

    public void defeatBoss() {
        this.mBossBattle = false;
        this.mBossAppear = false;
        this.mFrame = 0;
        this.mStage++;
        this.mRandom.setSeed((long) this.mStage);
        playMusic();
    }

    public void release() {
        this.mThread = null;
        if (this.mMusicListener != null) {
            this.mMusicListener.suspend();
        }
    }

    public void start() {
        if (this.mMusicListener != null) {
            this.mMusicListener.resume();
        }
        this.mThread = new Thread(new Runnable() {
            public void run() {
                StageView.this.onDraw();
            }
        });
        this.mThread.start();
    }

    private void playMusic() {
        if (this.mMusicListener == null) {
            return;
        }
        if (this.mBossBattle) {
            this.mMusicListener.playMusic(R.raw.boss, true);
            return;
        }
        switch ((this.mStage - 1) % 6) {
            case 0:
            case 1:
                this.mMusicListener.playMusic(R.raw.stage1, true);
                return;
            case 2:
            case 3:
                this.mMusicListener.playMusic(R.raw.stage2, true);
                return;
            case 4:
                this.mMusicListener.playMusic(R.raw.stage3, true);
                return;
            case 5:
                this.mMusicListener.playMusic(R.raw.stage4, true);
                return;
            default:
                this.mMusicListener.playMusic(R.raw.stage1, true);
                return;
        }
    }

    public int getStage() {
        return this.mStage;
    }

    public void setPause(boolean flg) {
        this.mPause = flg;
    }

    public boolean getPause() {
        return this.mPause;
    }
}
