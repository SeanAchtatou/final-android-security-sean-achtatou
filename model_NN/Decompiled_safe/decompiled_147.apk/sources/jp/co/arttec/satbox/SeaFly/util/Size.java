package jp.co.arttec.satbox.SeaFly.util;

public class Size {
    public int mHeight;
    public int mWidth;

    public Size() {
        this.mHeight = 0;
        this.mWidth = 0;
    }

    public Size(int size) {
        this.mHeight = size;
        this.mWidth = size;
    }

    public Size(Size size) {
        this.mWidth = size.mWidth;
        this.mHeight = size.mHeight;
    }

    public Size(int width, int height) {
        this.mWidth = width;
        this.mHeight = height;
    }
}
