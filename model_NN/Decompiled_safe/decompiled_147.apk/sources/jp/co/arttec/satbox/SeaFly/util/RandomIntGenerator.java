package jp.co.arttec.satbox.SeaFly.util;

public class RandomIntGenerator {
    private static final int M = 397;
    private static final int N = 624;
    private final int[] mt = new int[N];
    private int mtIndex = 625;

    public RandomIntGenerator(int seed) {
        init(seed);
    }

    public RandomIntGenerator(int[] seedArray) {
        init(seedArray);
    }

    public int nextInt() {
        return temper(generateInt());
    }

    public void init(int seed) {
        setMt(createLongMt(seed));
    }

    public void init(int[] seedArray) {
        long[] longMt = createLongMt(19650218);
        int max = N > seedArray.length ? N : seedArray.length;
        int i = 1;
        int j = 0;
        for (int counter = 0; counter < max; counter++) {
            if (N <= i) {
                longMt[0] = longMt[623];
                i = 1;
            }
            if (seedArray.length <= j) {
                j = 0;
            }
            longMt[i] = longMt[i] ^ ((longMt[i - 1] ^ (longMt[i - 1] >>> 30)) * 1664525);
            longMt[i] = longMt[i] + ((long) (seedArray[j] + j));
            longMt[i] = longMt[i] & 4294967295L;
            i++;
            j++;
        }
        int i2 = (max % 623) + 1;
        for (int counter2 = 0; counter2 < 623; counter2++) {
            if (N <= i2) {
                longMt[0] = longMt[623];
                i2 = 1;
            }
            longMt[i2] = longMt[i2] ^ ((longMt[i2 - 1] ^ (longMt[i2 - 1] >>> 30)) * 1566083941);
            longMt[i2] = longMt[i2] - ((long) i2);
            longMt[i2] = longMt[i2] & 4294967295L;
            i2++;
        }
        longMt[0] = 2147483648L;
        setMt(longMt);
    }

    private synchronized int generateInt() {
        int ret;
        twist();
        ret = this.mt[this.mtIndex];
        this.mtIndex++;
        return ret;
    }

    private void twist() {
        int[] BIT_MATRIX = new int[2];
        BIT_MATRIX[1] = -1727483681;
        if (this.mtIndex >= N) {
            if (this.mtIndex > N) {
                init(5489);
            }
            for (int i = 0; i < N; i++) {
                int x = (this.mt[i] & Integer.MIN_VALUE) | (this.mt[(i + 1) % N] & Integer.MAX_VALUE);
                this.mt[i] = (this.mt[(i + M) % N] ^ (x >>> 1)) ^ BIT_MATRIX[x & 1];
            }
            this.mtIndex = 0;
        }
    }

    private static int temper(int num) {
        int num2 = num ^ (num >>> 11);
        int num3 = num2 ^ ((num2 << 7) & -1658038656);
        int num4 = num3 ^ ((num3 << 15) & -272236544);
        return num4 ^ (num4 >>> 18);
    }

    private static int toInt(long num) {
        return (int) (num > 2147483647L ? num - 4294967296L : num);
    }

    private static long[] createLongMt(int seed) {
        long[] longMt = new long[N];
        longMt[0] = ((long) seed) & 4294967295L;
        for (int i = 1; i < N; i++) {
            longMt[i] = longMt[i - 1];
            longMt[i] = longMt[i] >>> 30;
            longMt[i] = longMt[i] ^ longMt[i - 1];
            longMt[i] = longMt[i] * 1812433253;
            longMt[i] = longMt[i] + ((long) i);
            longMt[i] = longMt[i] & 4294967295L;
        }
        return longMt;
    }

    private synchronized void setMt(long[] longMt) {
        for (int i = 0; i < N; i++) {
            this.mt[i] = toInt(longMt[i]);
        }
        this.mtIndex = N;
    }
}
