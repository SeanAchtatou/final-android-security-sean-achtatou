package jp.co.arttec.satbox.SeaFly.event;

public interface PauseListener {
    void pause();
}
