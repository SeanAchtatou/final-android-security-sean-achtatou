package jp.co.arttec.satbox.SeaFly.opening;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import jp.co.arttec.satbox.SeaFly.R;
import jp.co.arttec.satbox.SeaFly.manager.EffectSoundManager;
import jp.co.arttec.satbox.SeaFly.util.EffectSoundFrequency;

public class StageSelect extends Activity {
    private int mMaxStage;
    private ArrayAdapter<String> mSelStage;
    /* access modifiers changed from: private */
    public int mSelected;
    /* access modifiers changed from: private */
    public boolean mbStartFlg;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.stage_select);
        this.mMaxStage = getIntent().getIntExtra("Stage", 1);
        if (this.mMaxStage >= 6) {
            this.mMaxStage = 6;
        }
        this.mSelected = 0;
        this.mbStartFlg = false;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.mSelStage = new ArrayAdapter<>(this, 17367048);
        this.mSelStage.setDropDownViewResource(17367049);
        for (int i = 0; i < this.mMaxStage; i++) {
            this.mSelStage.add("Stage " + (i + 1));
        }
        Spinner spinner = (Spinner) findViewById(R.id.select_stage);
        spinner.setAdapter((SpinnerAdapter) this.mSelStage);
        spinner.setSelection(this.mSelected);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (StageSelect.this.mbStartFlg) {
                    EffectSoundManager.getInstance(StageSelect.this.getApplication().getApplicationContext()).play(EffectSoundFrequency.Title_SE);
                } else {
                    StageSelect.this.mbStartFlg = true;
                }
                StageSelect.this.mSelected = position;
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        ((Button) findViewById(R.id.select_ok)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                EffectSoundManager.getInstance(StageSelect.this.getApplication().getApplicationContext()).play(EffectSoundFrequency.Title_SE);
                Intent intent = new Intent();
                intent.putExtra("Stage", StageSelect.this.mSelected + 1);
                StageSelect.this.setResult(-1, intent);
                StageSelect.this.finish();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.mSelStage != null) {
            this.mSelStage.clear();
            this.mSelStage = null;
        }
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == 0) {
            switch (event.getKeyCode()) {
                case 4:
                    return true;
            }
        }
        return super.dispatchKeyEvent(event);
    }
}
