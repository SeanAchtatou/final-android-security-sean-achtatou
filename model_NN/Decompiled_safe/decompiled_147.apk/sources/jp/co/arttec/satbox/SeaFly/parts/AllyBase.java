package jp.co.arttec.satbox.SeaFly.parts;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import jp.co.arttec.satbox.SeaFly.R;
import jp.co.arttec.satbox.SeaFly.manager.CacheImageManager;
import jp.co.arttec.satbox.SeaFly.util.Size;

public class AllyBase extends BaseObject {
    private static final int POWERUP_FRAME = 30;
    private Bitmap mAlly;
    private Bitmap mAllyBitmap;
    private Bitmap mAllyDummy;
    private Bitmap mAllyLeft;
    private Bitmap mAllyLeft_s;
    private Bitmap mAllyRight;
    private Bitmap mAllyRight_s;
    private Bitmap mAllyShield;
    private int mAllyState;
    private Bitmap mAlly_s;
    private Context mContext;
    private boolean mIsPowerUp = false;
    private int mPowerUpFrame = 0;
    private int mShot = 1;

    public AllyBase(Context context) {
        this.mContext = context;
        this.mPower = 3;
        readBitmap();
    }

    public void setPowerUp() {
        this.mIsPowerUp = true;
        this.mPowerUpFrame = 0;
    }

    private void readBitmap() {
        Resources res = this.mContext.getResources();
        CacheImageManager manager = CacheImageManager.getObject();
        if (manager != null) {
            this.mAlly = manager.getImageFromResource(R.drawable.ally);
            this.mAllyShield = manager.getImageFromResource(R.drawable.ally_shield);
            this.mAllyLeft = manager.getImageFromResource(R.drawable.ally_left);
            this.mAllyRight = manager.getImageFromResource(R.drawable.ally_right);
            this.mAlly_s = manager.getImageFromResource(R.drawable.ally_s);
            this.mAllyLeft_s = manager.getImageFromResource(R.drawable.ally_left_s);
            this.mAllyRight_s = manager.getImageFromResource(R.drawable.ally_right_s);
        } else {
            this.mAlly = BitmapFactory.decodeResource(res, R.drawable.ally);
            this.mAllyShield = BitmapFactory.decodeResource(res, R.drawable.ally_shield);
            this.mAllyLeft = BitmapFactory.decodeResource(res, R.drawable.ally_left);
            this.mAllyRight = BitmapFactory.decodeResource(res, R.drawable.ally_right);
            this.mAlly_s = BitmapFactory.decodeResource(res, R.drawable.ally_s);
            this.mAllyLeft_s = BitmapFactory.decodeResource(res, R.drawable.ally_left_s);
            this.mAllyRight_s = BitmapFactory.decodeResource(res, R.drawable.ally_right_s);
        }
        this.mAllyDummy = BitmapFactory.decodeResource(res, R.drawable.ally_dummy);
    }

    /* access modifiers changed from: protected */
    public void calcDirection() {
    }

    public void move() {
    }

    public void setShot(int shot) {
        this.mShot = shot;
    }

    public int getShot() {
        return this.mShot;
    }

    public void setAllyState(Resources res, int allyState) {
        this.mAllyState = allyState;
        this.mAllyBitmap = Bitmap.createBitmap(this.mAlly.getWidth(), this.mAlly.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas offScreen = new Canvas(this.mAllyBitmap);
        if (1 == (allyState & 1)) {
            if (this.mIsPowerUp) {
                offScreen.drawBitmap(this.mAlly_s, 0.0f, 0.0f, (Paint) null);
            } else {
                offScreen.drawBitmap(this.mAlly, 0.0f, 0.0f, (Paint) null);
            }
        } else if (2 == (allyState & 2)) {
            if (this.mIsPowerUp) {
                offScreen.drawBitmap(this.mAllyLeft_s, 0.0f, 0.0f, (Paint) null);
            } else {
                offScreen.drawBitmap(this.mAllyLeft, 0.0f, 0.0f, (Paint) null);
            }
        } else if (4 != (allyState & 4)) {
            offScreen.drawBitmap(this.mAllyDummy, 0.0f, 0.0f, (Paint) null);
        } else if (this.mIsPowerUp) {
            offScreen.drawBitmap(this.mAllyRight_s, 0.0f, 0.0f, (Paint) null);
        } else {
            offScreen.drawBitmap(this.mAllyRight, 0.0f, 0.0f, (Paint) null);
        }
        if (this.mPower > 0) {
            offScreen.drawBitmap(this.mAllyShield, 0.0f, 0.0f, (Paint) null);
        }
        if (this.mSize == null) {
            this.mSize = new Size(this.mAllyBitmap.getWidth(), this.mAllyBitmap.getHeight());
            return;
        }
        this.mSize.mWidth = this.mAllyBitmap.getWidth();
        this.mSize.mHeight = this.mAllyBitmap.getHeight();
    }

    public int getAllyState() {
        return this.mAllyState;
    }

    public void draw(Canvas canvas) {
        canvas.drawBitmap(this.mAllyBitmap, (float) this.mPosition.x, (float) this.mPosition.y, (Paint) null);
        if (this.mIsPowerUp) {
            this.mPowerUpFrame++;
            if (30 < this.mPowerUpFrame) {
                this.mIsPowerUp = false;
            }
        }
    }

    public void setPower(int power) {
        this.mPower = power;
    }
}
