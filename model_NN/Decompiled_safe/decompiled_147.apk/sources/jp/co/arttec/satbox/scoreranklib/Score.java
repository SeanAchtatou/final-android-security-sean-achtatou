package jp.co.arttec.satbox.scoreranklib;

import java.util.Date;

public class Score {
    private Date _date;
    private long _id;
    private int _kindGames;
    private String _name;
    private long _score;
    private String _version;

    public Score(long id, int kindGames, long score, Date date, String name, String version) {
        this._id = id;
        this._kindGames = kindGames;
        this._score = score;
        this._date = date;
        this._name = name;
        this._version = version;
    }

    @Deprecated
    public Score(long id, int kindGames, long score, Date date, String name) {
        this._id = id;
        this._kindGames = kindGames;
        this._score = score;
        this._date = date;
        this._name = name;
        this._version = "";
    }

    public long getId() {
        return this._id;
    }

    public int getKindGames() {
        return this._kindGames;
    }

    public long getScore() {
        return this._score;
    }

    public Date getDate() {
        return this._date;
    }

    public String getName() {
        return this._name;
    }

    public String getVersion() {
        return this._version;
    }
}
