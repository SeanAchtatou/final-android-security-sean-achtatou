package jp.co.arttec.satbox.SeaFly.manager;

import android.content.Context;
import android.graphics.Canvas;
import jp.co.arttec.satbox.SeaFly.parts.BaseObject;
import jp.co.arttec.satbox.SeaFly.parts.PlanetBase;
import jp.co.arttec.satbox.SeaFly.util.Position;
import jp.co.arttec.satbox.SeaFly.util.Size;

public class PlanetManager extends BaseManager {
    public static final int MAX_OBJECTS = 1;
    private static PlanetManager mSelf = null;

    private PlanetManager(Context context) {
        super(context);
    }

    public static PlanetManager getInstance(Context context) {
        if (mSelf == null) {
            mSelf = new PlanetManager(context);
        }
        return mSelf;
    }

    public void add(PlanetBase planet) {
        if (isAddObject()) {
            super.addObject(planet);
        }
    }

    public boolean isAddObject() {
        if (this.mObject == null) {
            return false;
        }
        int count = 0;
        for (BaseObject baseObject : this.mObject) {
            if (baseObject != null) {
                count++;
            }
        }
        if (1 > count) {
            return true;
        }
        return false;
    }

    public void move() {
        if (this.mObject != null) {
            for (int i = 0; i < this.mObject.length; i++) {
                if (this.mObject[i] != null) {
                    this.mObject[i].move();
                }
            }
        }
    }

    public Position getPosition() {
        return null;
    }

    public Size getSize() {
        return null;
    }

    public void onDraw(Canvas canvas) {
        if (this.mObject != null) {
            for (int i = 0; i < this.mObject.length; i++) {
                if (this.mObject[i] != null) {
                    this.mObject[i].draw(canvas);
                }
            }
        }
    }

    public void setPosition(Position position) {
    }

    public void shot() {
    }

    public void release() {
        super.release();
        if (mSelf != null) {
            mSelf = null;
        }
    }
}
