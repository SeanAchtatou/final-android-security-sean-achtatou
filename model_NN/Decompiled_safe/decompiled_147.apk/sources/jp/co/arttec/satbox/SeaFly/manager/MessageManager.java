package jp.co.arttec.satbox.SeaFly.manager;

import android.content.Context;
import android.graphics.Canvas;
import jp.co.arttec.satbox.SeaFly.parts.Message;
import jp.co.arttec.satbox.SeaFly.util.Position;
import jp.co.arttec.satbox.SeaFly.util.Size;

public class MessageManager extends BaseManager {
    private static MessageManager mSelf = null;
    private Context mContext;

    private MessageManager(Context context) {
        super(context);
        this.mContext = context;
    }

    public static MessageManager getInstance(Context context) {
        if (mSelf == null) {
            mSelf = new MessageManager(context);
        }
        return mSelf;
    }

    public Context getContext() {
        return this.mContext;
    }

    public void move() {
        if (this.mObject != null) {
            for (int i = 0; i < this.mObject.length; i++) {
                if (this.mObject[i] != null) {
                    this.mObject[i].move();
                }
            }
        }
    }

    public Position getPosition() {
        return null;
    }

    public Size getSize() {
        return null;
    }

    public void setPosition(Position position) {
    }

    public void shot() {
    }

    public void onDraw(Canvas canvas) {
        if (this.mObject != null) {
            for (int i = 0; i < this.mObject.length; i++) {
                if (this.mObject[i] != null) {
                    this.mObject[i].draw(canvas);
                }
            }
        }
    }

    public void deleteObject() {
        if (this.mObject != null) {
            for (int i = 0; i < this.mObject.length; i++) {
                Message message = (Message) this.mObject[i];
                if (message != null && !message.isShowMessage()) {
                    this.mObject[i] = null;
                }
            }
        }
    }

    public void release() {
        super.release();
        if (mSelf != null) {
            mSelf = null;
        }
    }
}
