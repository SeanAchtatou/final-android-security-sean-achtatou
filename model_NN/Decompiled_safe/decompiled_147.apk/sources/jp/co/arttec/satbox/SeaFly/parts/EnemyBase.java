package jp.co.arttec.satbox.SeaFly.parts;

import android.graphics.Rect;
import jp.co.arttec.satbox.SeaFly.event.BossDefeatListener;
import jp.co.arttec.satbox.SeaFly.util.EnemyKind;

public class EnemyBase extends BaseObject {
    protected int mBulletFrequency = 0;
    private EnemyKind mEnemyKind;
    private BossDefeatListener mListener = null;
    protected Rect mScreenRect;

    public EnemyBase(Rect screenRect) {
        this.mScreenRect = screenRect;
        this.mScore = 10;
    }

    public void setEnemyKind(EnemyKind enemyKind) {
        this.mEnemyKind = enemyKind;
    }

    public EnemyKind getEnemyKind() {
        return this.mEnemyKind;
    }

    /* access modifiers changed from: protected */
    public void calcDirection() {
    }

    public void setBossDefeatListener(BossDefeatListener listener) {
        this.mListener = listener;
    }

    public BossDefeatListener getBossDefeatListener() {
        return this.mListener;
    }

    public int getBulletFrequency() {
        return this.mBulletFrequency;
    }

    public void setBulletFrequency(int bulletFrequency) {
        this.mBulletFrequency = bulletFrequency;
    }

    public int getScore() {
        return this.mScore;
    }
}
