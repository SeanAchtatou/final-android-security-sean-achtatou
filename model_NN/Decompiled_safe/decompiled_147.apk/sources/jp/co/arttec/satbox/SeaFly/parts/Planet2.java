package jp.co.arttec.satbox.SeaFly.parts;

import android.content.Context;
import android.graphics.Rect;
import jp.co.arttec.satbox.SeaFly.R;
import jp.co.arttec.satbox.SeaFly.util.Position;

public class Planet2 extends PlanetBase {
    public Planet2(Position position, Context context) {
        super(position, context);
        init(context);
    }

    public Planet2(Rect screenRect, Context context) {
        super(screenRect, context);
        init(context);
    }

    /* access modifiers changed from: protected */
    public void init(Context context) {
        super.init(context);
        setImage(context.getResources(), R.drawable.planet2);
    }
}
