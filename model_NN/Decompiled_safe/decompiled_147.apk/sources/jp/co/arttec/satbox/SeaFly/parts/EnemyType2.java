package jp.co.arttec.satbox.SeaFly.parts;

import android.content.Context;
import android.graphics.Rect;
import jp.co.arttec.satbox.SeaFly.R;
import jp.co.arttec.satbox.SeaFly.util.EnemyKind;
import jp.co.arttec.satbox.SeaFly.util.Random;

public class EnemyType2 extends EnemyBase {
    private int mFrame = 0;

    public EnemyType2(Rect screenRect, Context context) {
        super(screenRect);
        setImage(context.getResources(), R.drawable.enemy2);
        calcDirection();
        this.mPosition.x = Random.getInstance().nextInt(screenRect.right - getSize().mWidth);
        this.mPosition.y = -getSize().mHeight;
        this.mPower = 1;
        this.mBulletFrequency = 4;
        setEnemyKind(EnemyKind.Type2);
        this.mScore = 200;
    }

    public void move() {
        this.mFrame++;
        calcDirection();
        super.move();
    }

    /* access modifiers changed from: protected */
    public void calcDirection() {
        if (this.mFrame < 15) {
            this.mDirection.dx = 0;
            this.mDirection.dy = 8;
        } else if (this.mFrame < 105) {
            this.mDirection.dx = 0;
            this.mDirection.dy = 0;
        } else {
            this.mDirection.dx = 0;
            this.mDirection.dy = -8;
        }
    }
}
