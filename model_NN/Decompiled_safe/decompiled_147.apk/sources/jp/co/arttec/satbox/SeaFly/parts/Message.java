package jp.co.arttec.satbox.SeaFly.parts;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import jp.co.arttec.satbox.SeaFly.util.Position;

public class Message extends BaseObject {
    private static final int FRAME = 15;
    private int mFrame = 0;
    private int mShowFrame;
    private int mSize;

    public Message(Position position, Context context, String message) {
        init(context);
        this.mPosition.x = position.x;
        this.mPosition.y = position.y;
        setMessage(message);
        this.mShowFrame = 15;
        this.mSize = 18;
    }

    /* access modifiers changed from: protected */
    public void init(Context context) {
        calcDirection();
    }

    /* access modifiers changed from: protected */
    public void calcDirection() {
    }

    public void move() {
        this.mFrame++;
    }

    public boolean isShowMessage() {
        if (this.mFrame < this.mShowFrame) {
            return true;
        }
        return false;
    }

    public void setShowFrame(int showFrame) {
        this.mShowFrame = showFrame;
    }

    public int getFrame() {
        return this.mFrame;
    }

    public void draw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(-1);
        paint.setTextSize((float) this.mSize);
        paint.setAntiAlias(false);
        canvas.drawText(getMessage(), (float) this.mPosition.x, (float) this.mPosition.y, paint);
    }

    public void setSize(int size) {
        this.mSize = size;
    }
}
