package jp.co.arttec.satbox.SeaFly;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import jp.co.arttec.satbox.SeaFly.event.GameOverListener;
import jp.co.arttec.satbox.SeaFly.event.GameStartListener;
import jp.co.arttec.satbox.SeaFly.event.MusicListener;
import jp.co.arttec.satbox.SeaFly.event.PauseListener;
import jp.co.arttec.satbox.SeaFly.event.ScoreListener;
import jp.co.arttec.satbox.SeaFly.event.ShieldListener;
import jp.co.arttec.satbox.SeaFly.event.ViewReleaseListener;
import jp.co.arttec.satbox.SeaFly.manager.EffectSoundManager;
import jp.co.arttec.satbox.SeaFly.manager.MusicManager;
import jp.co.arttec.satbox.SeaFly.manager.PreferencesManager;
import jp.co.arttec.satbox.SeaFly.util.BaseActivity;
import jp.co.arttec.satbox.SeaFly.util.EffectSoundFrequency;
import jp.co.arttec.satbox.SeaFly.view.StageView;
import jp.co.arttec.satbox.scoreranklib.HttpCommunicationListener;
import jp.co.arttec.satbox.scoreranklib.RegistScoreController;

public class GameActivity extends BaseActivity implements ShieldListener, PauseListener, ScoreListener, MusicListener, GameOverListener, ViewReleaseListener, GameStartListener {
    public static final int SHOT_SPEED_FAST = 5;
    public static final int SHOT_SPEED_MIDDLE = 6;
    public static final int SHOT_SPEED_SLOW = 7;
    public static final int SHOT_SPEED_VERY_FAST = 4;
    public static final int SHOT_SPEED_VERY_SLOW = 8;
    private Handler mEventHandler = new Handler() {
        public void handleMessage(Message msg) {
            GameActivity.this.mStageView.release();
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            GameActivity.this.mStageView.endView();
        }
    };
    private Handler mHandler = new Handler();
    /* access modifiers changed from: private */
    public boolean mIsExit;
    /* access modifiers changed from: private */
    public boolean mMenu;
    /* access modifiers changed from: private */
    public MusicManager mMusic;
    /* access modifiers changed from: private */
    public TextView mPauseTextView;
    /* access modifiers changed from: private */
    public ImageView mPauseView;
    /* access modifiers changed from: private */
    public int mScore;
    /* access modifiers changed from: private */
    public TextView mScoreView;
    /* access modifiers changed from: private */
    public ImageView mShieldView;
    /* access modifiers changed from: private */
    public int mStage;
    /* access modifiers changed from: private */
    public StageView mStageView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        initShieldObject();
        initScoreObject();
        initMusicObject();
        initGameOver();
        initViewRelease();
        initStart();
        initPauseObject();
        this.mIsExit = false;
        this.mStageView.setStage(getIntent().getIntExtra("Stage", 1));
        this.mMenu = false;
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == 0) {
            switch (event.getKeyCode()) {
                case 4:
                    this.mMenu = false;
                    this.mAbout = false;
                    this.mStageView.setPause(false);
                    return true;
            }
        }
        return super.dispatchKeyEvent(event);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.mStageView.release();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        this.mStageView.start();
    }

    private void initShieldObject() {
        this.mShieldView = (ImageView) findViewById(R.id.shieldView);
        this.mShieldView.setImageLevel(0);
        this.mStageView = (StageView) findViewById(R.id.stageView);
        this.mStageView.setOnShieldListener(this);
    }

    private void initScoreObject() {
        this.mScore = 0;
        this.mScoreView = (TextView) findViewById(R.id.score);
        this.mStageView.setOnScoreListener(this);
    }

    private void initPauseObject() {
        this.mPauseView = (ImageView) findViewById(R.id.pause_iv);
        this.mPauseTextView = (TextView) findViewById(R.id.pause_tv);
        this.mStageView.setOnPauseListener(this);
    }

    private void initMusicObject() {
        this.mMusic = MusicManager.getInstance(this);
        this.mStageView.setOnMusicListener(this);
    }

    private void initGameOver() {
        this.mStageView.setOnGameOverListener(this);
    }

    private void initStart() {
        this.mStageView.setOnGameStartListener(this);
    }

    private void initViewRelease() {
        this.mStageView.setOnReleaseListener(this);
    }

    public void setShield(final int level, final int speed) {
        this.mHandler.post(new Runnable() {
            public void run() {
                Bitmap imageAlly = BitmapFactory.decodeResource(GameActivity.this.getResources(), R.drawable.shield_ally);
                Bitmap margeShield = Bitmap.createBitmap(imageAlly.getWidth(), imageAlly.getHeight(), Bitmap.Config.ARGB_8888);
                Bitmap imageShield = null;
                switch (level) {
                    case 0:
                        imageShield = null;
                        break;
                    case 1:
                        imageShield = BitmapFactory.decodeResource(GameActivity.this.getResources(), R.drawable.shield1);
                        break;
                    case 2:
                        imageShield = BitmapFactory.decodeResource(GameActivity.this.getResources(), R.drawable.shield2);
                        break;
                    case 3:
                        imageShield = BitmapFactory.decodeResource(GameActivity.this.getResources(), R.drawable.shield3);
                        break;
                    case 4:
                        imageShield = BitmapFactory.decodeResource(GameActivity.this.getResources(), R.drawable.shield4);
                        break;
                    case 5:
                        imageShield = BitmapFactory.decodeResource(GameActivity.this.getResources(), R.drawable.shield5);
                        break;
                    case 6:
                        imageShield = BitmapFactory.decodeResource(GameActivity.this.getResources(), R.drawable.shield6);
                        break;
                }
                Bitmap imageSpeed = null;
                switch (speed) {
                    case 4:
                        imageSpeed = BitmapFactory.decodeResource(GameActivity.this.getResources(), R.drawable.speed5);
                        break;
                    case 5:
                        imageSpeed = BitmapFactory.decodeResource(GameActivity.this.getResources(), R.drawable.speed4);
                        break;
                    case 6:
                        imageSpeed = BitmapFactory.decodeResource(GameActivity.this.getResources(), R.drawable.speed3);
                        break;
                    case 7:
                        imageSpeed = BitmapFactory.decodeResource(GameActivity.this.getResources(), R.drawable.speed2);
                        break;
                    case 8:
                        imageSpeed = BitmapFactory.decodeResource(GameActivity.this.getResources(), R.drawable.speed1);
                        break;
                }
                Canvas offScreen = new Canvas(margeShield);
                offScreen.drawBitmap(imageAlly, 0.0f, 0.0f, (Paint) null);
                if (imageShield != null) {
                    offScreen.drawBitmap(imageShield, 0.0f, 0.0f, (Paint) null);
                }
                if (imageSpeed != null) {
                    offScreen.drawBitmap(imageSpeed, 0.0f, 0.0f, (Paint) null);
                }
                GameActivity.this.mShieldView.setImageBitmap(margeShield);
            }
        });
    }

    public void pause() {
        this.mHandler.post(new Runnable() {
            public void run() {
                int i;
                GameActivity.this.mPauseView.setVisibility(GameActivity.this.mStageView.getPause() ? 0 : 4);
                TextView access$3 = GameActivity.this.mPauseTextView;
                if (GameActivity.this.mStageView.getPause()) {
                    i = 0;
                } else {
                    i = 4;
                }
                access$3.setVisibility(i);
                if (!GameActivity.this.mAbout && !GameActivity.this.mMenu) {
                    GameActivity.this.mStageView.setPause(false);
                }
            }
        });
    }

    public void setScore(int gainScore) {
        this.mScore += gainScore;
        if (this.mScore < 0) {
            this.mScore = 0;
        }
        final String format = String.format("SCORE %07d", Integer.valueOf(this.mScore));
        this.mHandler.post(new Runnable() {
            public void run() {
                GameActivity.this.mScoreView.setText(format);
            }
        });
    }

    public void playMusic(final int resId, final boolean loop) {
        this.mHandler.post(new Runnable() {
            public void run() {
                GameActivity.this.mMusic.play(resId, loop);
            }
        });
    }

    public void stopMusic() {
        this.mHandler.post(new Runnable() {
            public void run() {
                GameActivity.this.mMusic.stop();
            }
        });
    }

    public void resume() {
        this.mHandler.post(new Runnable() {
            public void run() {
                GameActivity.this.mMusic.resume();
            }
        });
    }

    public void suspend() {
        this.mHandler.post(new Runnable() {
            public void run() {
                GameActivity.this.mMusic.suspend();
            }
        });
    }

    public void gameOver() {
        this.mHandler.post(new Runnable() {
            public void run() {
                GameActivity.this.mStage = GameActivity.this.mStageView.getStage();
                TextView gameOverView = (TextView) GameActivity.this.findViewById(R.id.message);
                gameOverView.setText((int) R.string.gameover);
                gameOverView.setVisibility(0);
                new GameMyEntry(GameActivity.this.getApplicationContext(), "LOCAL", GameActivity.this.mScore).onMyScoreEntry();
                PreferencesManager.getInstance(GameActivity.this.getApplication().getApplicationContext()).setHighScore(GameActivity.this.mScore);
            }
        });
        this.mEventHandler.sendEmptyMessageDelayed(0, 3000);
    }

    public void ready() {
        this.mHandler.post(new Runnable() {
            public void run() {
                TextView readyView = (TextView) GameActivity.this.findViewById(R.id.message);
                readyView.setText((int) R.string.ready);
                readyView.setVisibility(0);
            }
        });
    }

    public void readyEnd() {
        this.mHandler.post(new Runnable() {
            public void run() {
                ((TextView) GameActivity.this.findViewById(R.id.message)).setVisibility(4);
            }
        });
    }

    public void releaseFinish() {
        this.mHandler.post(new Runnable() {
            public void run() {
                if (!GameActivity.this.mIsExit) {
                    GameActivity.this.onCreateDialog(0).show();
                } else {
                    GameActivity.this.finish();
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        final View inputView = LayoutInflater.from(this).inflate((int) R.layout.regist, (ViewGroup) null);
        ((TextView) inputView.findViewById(R.id.dialog_edittext)).setText(PreferencesManager.getInstance(getApplication().getApplicationContext()).getName());
        return new AlertDialog.Builder(this).setIcon((int) R.drawable.icon).setTitle((int) R.string.dialog_title).setView(inputView).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                EffectSoundManager.getInstance(GameActivity.this.getApplication().getApplicationContext()).play(EffectSoundFrequency.Title_SE);
                EditText editText = (EditText) inputView.findViewById(R.id.dialog_edittext);
                PreferencesManager.getInstance(GameActivity.this.getApplication().getApplicationContext()).setName(editText.getText().toString());
                GameActivity.this.registScore(editText.getText().toString());
            }
        }).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                EffectSoundManager.getInstance(GameActivity.this.getApplication().getApplicationContext()).play(EffectSoundFrequency.Title_SE);
                Intent intent = new Intent();
                intent.putExtra("Stage", GameActivity.this.mStage);
                GameActivity.this.setResult(-1, intent);
                GameActivity.this.finish();
            }
        }).setCancelable(false).create();
    }

    /* access modifiers changed from: private */
    public void registScore(String name) {
        RegistScoreController controller = new RegistScoreController(49, (long) this.mScore, name);
        controller.setActivity(this);
        controller.setOnFinishListener(new HttpCommunicationListener() {
            public void onFinish(boolean result) {
                Toast toast;
                if (result) {
                    toast = Toast.makeText(GameActivity.this, "Score entry completion.", 1);
                } else {
                    toast = Toast.makeText(GameActivity.this, "Score entry failure.", 1);
                }
                toast.show();
                Intent intent = new Intent();
                intent.putExtra("Stage", GameActivity.this.mStage);
                GameActivity.this.setResult(-1, intent);
                GameActivity.this.finish();
            }
        });
        controller.registScore();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        this.mMenu = true;
        this.mStageView.setPause(true);
        return true;
    }

    public void onOptionsMenuClosed(Menu menu) {
        super.onOptionsMenuClosed(menu);
        this.mMenu = false;
        if (!this.mAbout) {
            this.mStageView.setPause(false);
        }
    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.finish:
                this.mIsExit = true;
                EffectSoundManager.getInstance(getApplication().getApplicationContext()).play(EffectSoundFrequency.Title_SE);
                this.mStageView.release();
                this.mStageView.endView();
                break;
            case R.id.help:
                goAboutPage();
                break;
            case R.id.applink:
                moreApplicationLink();
                break;
            case R.id.satbox:
                goHomePage();
                break;
        }
        return true;
    }
}
