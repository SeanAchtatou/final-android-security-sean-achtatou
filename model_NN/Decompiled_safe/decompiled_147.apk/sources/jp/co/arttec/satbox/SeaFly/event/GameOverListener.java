package jp.co.arttec.satbox.SeaFly.event;

public interface GameOverListener {
    void gameOver();
}
