package jp.co.arttec.satbox.SeaFly.parts;

import android.content.Context;
import jp.co.arttec.satbox.SeaFly.R;

public class EnemyBullet_Type2 extends BulletBase {
    public EnemyBullet_Type2(EnemyBase enemy, Context context) {
        setImage(context.getResources(), R.drawable.enemy_bullet);
        this.mPosition.x = (enemy.getPosition().x + (enemy.getSize().mWidth / 2)) - (getSize().mWidth / 2);
        this.mPosition.y = enemy.getPosition().y;
        calcDirection();
        this.mPower = 1;
    }

    /* access modifiers changed from: protected */
    public void calcDirection() {
        this.mDirection.dx = 0;
        this.mDirection.dy = 15;
    }

    public void calcDirection(int dx, int dy) {
        this.mDirection.dx = dx;
        this.mDirection.dy = dy;
    }
}
