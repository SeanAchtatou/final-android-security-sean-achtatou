package jp.co.arttec.satbox.SeaFly.event;

public interface MusicListener {
    void playMusic(int i, boolean z);

    void resume();

    void stopMusic();

    void suspend();
}
