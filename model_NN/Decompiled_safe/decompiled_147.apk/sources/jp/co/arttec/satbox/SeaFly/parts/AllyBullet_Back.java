package jp.co.arttec.satbox.SeaFly.parts;

import android.content.Context;
import jp.co.arttec.satbox.SeaFly.R;

public class AllyBullet_Back extends BulletBase {
    public AllyBullet_Back(AllyBase ally, Context context) {
        setImage(context.getResources(), R.drawable.ally_bullet_back);
        this.mPosition.x = (ally.getPosition().x + (ally.getSize().mWidth / 2)) - (getSize().mWidth / 2);
        this.mPosition.y = ally.getPosition().y + getSize().mHeight;
        calcDirection();
        this.mPower = 1;
    }

    /* access modifiers changed from: protected */
    public void calcDirection() {
        this.mDirection.dx = 0;
        this.mDirection.dy = 20;
    }
}
