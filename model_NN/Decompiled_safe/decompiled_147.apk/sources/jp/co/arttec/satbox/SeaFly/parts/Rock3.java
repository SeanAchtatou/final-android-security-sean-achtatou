package jp.co.arttec.satbox.SeaFly.parts;

import android.content.Context;
import android.graphics.Rect;
import jp.co.arttec.satbox.SeaFly.R;
import jp.co.arttec.satbox.SeaFly.util.Position;

public class Rock3 extends RockBase {
    public Rock3(Position position, Context context) {
        super(position, context);
        init(context);
    }

    public Rock3(Rect screenRect, Context context) {
        super(screenRect, context);
        init(context);
    }

    /* access modifiers changed from: protected */
    public void init(Context context) {
        super.init(context);
        setImage(context.getResources(), R.drawable.rock3);
        this.mScore = 1000;
        this.mPower = 12;
    }
}
