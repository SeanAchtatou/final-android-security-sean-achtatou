package jp.co.arttec.satbox.SeaFly;

public class HighScore {
    private String mName;
    private int mRank;
    private long mScore;

    public HighScore(int rank, long score, String name) {
        this.mRank = rank;
        this.mScore = score;
        this.mName = name;
    }

    public int getRank() {
        return this.mRank;
    }

    public long getHighScore() {
        return this.mScore;
    }

    public String getName() {
        return this.mName;
    }
}
