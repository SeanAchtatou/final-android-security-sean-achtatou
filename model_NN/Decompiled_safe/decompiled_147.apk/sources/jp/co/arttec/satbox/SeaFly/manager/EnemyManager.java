package jp.co.arttec.satbox.SeaFly.manager;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Vibrator;
import jp.co.arttec.satbox.SeaFly.R;
import jp.co.arttec.satbox.SeaFly.parts.AllyBase;
import jp.co.arttec.satbox.SeaFly.parts.AllyExplode;
import jp.co.arttec.satbox.SeaFly.parts.BaseObject;
import jp.co.arttec.satbox.SeaFly.parts.EnemyBase;
import jp.co.arttec.satbox.SeaFly.parts.EnemyBullet_Type1;
import jp.co.arttec.satbox.SeaFly.parts.EnemyBullet_Type2;
import jp.co.arttec.satbox.SeaFly.parts.ShieldExplode;
import jp.co.arttec.satbox.SeaFly.util.EffectSoundFrequency;
import jp.co.arttec.satbox.SeaFly.util.EnemyKind;
import jp.co.arttec.satbox.SeaFly.util.HitObject;
import jp.co.arttec.satbox.SeaFly.util.Position;
import jp.co.arttec.satbox.SeaFly.util.Random;
import jp.co.arttec.satbox.SeaFly.util.Size;

public class EnemyManager extends BaseManager {
    private static /* synthetic */ int[] $SWITCH_TABLE$jp$co$arttec$satbox$SeaFly$util$EnemyKind;
    private static EnemyManager mSelf = null;
    private EnemyBulletManager mBulletManager;
    private Context mContext;

    static /* synthetic */ int[] $SWITCH_TABLE$jp$co$arttec$satbox$SeaFly$util$EnemyKind() {
        int[] iArr = $SWITCH_TABLE$jp$co$arttec$satbox$SeaFly$util$EnemyKind;
        if (iArr == null) {
            iArr = new int[EnemyKind.values().length];
            try {
                iArr[EnemyKind.Boss1.ordinal()] = 7;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[EnemyKind.Boss2.ordinal()] = 8;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[EnemyKind.Boss3.ordinal()] = 9;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[EnemyKind.Boss4.ordinal()] = 10;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[EnemyKind.Boss5.ordinal()] = 11;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[EnemyKind.Boss6.ordinal()] = 12;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[EnemyKind.Rock1.ordinal()] = 13;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[EnemyKind.Rock2.ordinal()] = 14;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[EnemyKind.Type1.ordinal()] = 1;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[EnemyKind.Type2.ordinal()] = 2;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[EnemyKind.Type3.ordinal()] = 3;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[EnemyKind.Type4.ordinal()] = 4;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[EnemyKind.Type5.ordinal()] = 5;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[EnemyKind.Type6.ordinal()] = 6;
            } catch (NoSuchFieldError e14) {
            }
            $SWITCH_TABLE$jp$co$arttec$satbox$SeaFly$util$EnemyKind = iArr;
        }
        return iArr;
    }

    private EnemyManager(Context context) {
        super(context);
        this.mBulletManager = EnemyBulletManager.getInstance(context);
        this.mContext = context;
    }

    public static EnemyManager getInstance(Context context) {
        if (mSelf == null) {
            mSelf = new EnemyManager(context);
        }
        return mSelf;
    }

    public void move() {
        if (this.mObject != null) {
            for (int i = 0; i < this.mObject.length; i++) {
                if (this.mObject[i] != null) {
                    this.mObject[i].move();
                }
            }
        }
    }

    public Position getPosition(int index) {
        if (this.mObject == null) {
            return null;
        }
        try {
            BaseObject object = this.mObject[index];
            if (object == null) {
                return null;
            }
            return object.getPosition();
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    public Position getPosition() {
        return null;
    }

    public Size getSize(int index) {
        if (this.mObject == null) {
            return null;
        }
        try {
            BaseObject object = this.mObject[index];
            if (object == null) {
                return null;
            }
            return object.getSize();
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    public Size getSize() {
        return null;
    }

    public void setPostion(int index, Position position) {
        if (this.mObject != null) {
            try {
                BaseObject object = this.mObject[index];
                if (object != null) {
                    object.setPosition(position);
                }
            } catch (IndexOutOfBoundsException e) {
            }
        }
    }

    public void setPosition(Position position) {
    }

    public void shot() {
    }

    public void onDraw(Canvas canvas) {
        if (this.mObject != null) {
            for (int i = 0; i < this.mObject.length; i++) {
                if (this.mObject[i] != null) {
                    this.mObject[i].draw(canvas);
                }
            }
            if (this.mBulletManager != null) {
                this.mBulletManager.move();
                this.mBulletManager.onDraw(canvas);
            }
        }
    }

    public void shot(AllyBase ally, Context context) {
        if (this.mObject != null && this.mBulletManager != null) {
            for (int i = 0; i < this.mObject.length; i++) {
                if (this.mObject[i] != null) {
                    EnemyBase enemy = (EnemyBase) this.mObject[i];
                    if (enemy.getBulletFrequency() > Random.getInstance().nextInt(200 - (StageManager.getInstance().getStage() * 15))) {
                        EnemyBullet_Type1 bullet = new EnemyBullet_Type1(ally, enemy, context);
                        bullet.setId(i);
                        switch ($SWITCH_TABLE$jp$co$arttec$satbox$SeaFly$util$EnemyKind()[EnemyKind.values()[enemy.getEnemyKind().ordinal()].ordinal()]) {
                            case 1:
                                bullet.setImage(context.getResources(), R.drawable.enemy_bullet1);
                                break;
                            case 2:
                                bullet.setImage(context.getResources(), R.drawable.enemy_bullet2);
                                break;
                            case 3:
                                bullet.setImage(context.getResources(), R.drawable.enemy_bullet3);
                                break;
                            case 4:
                                bullet.setImage(context.getResources(), R.drawable.enemy_bullet4);
                                break;
                            case 5:
                                bullet.setImage(context.getResources(), R.drawable.enemy_bullet5);
                                break;
                            case 6:
                                bullet.setImage(context.getResources(), R.drawable.enemy_bullet6);
                                break;
                            case 7:
                                bullet.setImage(context.getResources(), R.drawable.boss1_bullet);
                                bullet.setPower(Integer.MAX_VALUE);
                                bullet.setId(-1);
                                break;
                            case 8:
                                EnemyBullet_Type2 enemyBullet_Type2 = new EnemyBullet_Type2(enemy, context);
                                enemyBullet_Type2.setImage(context.getResources(), R.drawable.enemy_big1_bullet);
                                enemyBullet_Type2.setPosition(new Position(enemy.getPosition().x + (enemyBullet_Type2.getSize().mWidth / 2), enemy.getPosition().y + enemy.getSize().mHeight));
                                enemyBullet_Type2.setPower(1);
                                enemyBullet_Type2.setId(-1);
                                this.mBulletManager.addObject(enemyBullet_Type2);
                                bullet = null;
                                break;
                            case 9:
                                EnemyBullet_Type2 enemyBullet_Type22 = new EnemyBullet_Type2(enemy, context);
                                enemyBullet_Type22.setImage(context.getResources(), R.drawable.enemy_big2_bullet);
                                enemyBullet_Type22.setPower(1);
                                enemyBullet_Type22.setId(-1);
                                enemyBullet_Type22.setPosition(new Position(enemy.getPosition().x, enemy.getPosition().y + enemy.getSize().mHeight));
                                this.mBulletManager.addObject(enemyBullet_Type22);
                                EnemyBullet_Type2 enemyBullet_Type23 = new EnemyBullet_Type2(enemy, context);
                                enemyBullet_Type23.setImage(context.getResources(), R.drawable.enemy_big2_bullet);
                                enemyBullet_Type23.setPower(1);
                                enemyBullet_Type23.setId(-1);
                                enemyBullet_Type23.setPosition(new Position((enemy.getPosition().x + enemy.getSize().mWidth) - enemyBullet_Type23.getSize().mWidth, enemy.getPosition().y + enemy.getSize().mHeight));
                                this.mBulletManager.addObject(enemyBullet_Type23);
                                bullet = null;
                                break;
                            case 10:
                                EnemyBullet_Type2 enemyBullet_Type24 = new EnemyBullet_Type2(enemy, context);
                                enemyBullet_Type24.setImage(context.getResources(), R.drawable.enemy_big3_bullet);
                                enemyBullet_Type24.setPower(1);
                                enemyBullet_Type24.setId(-1);
                                enemyBullet_Type24.calcDirection(-5, 15);
                                enemyBullet_Type24.setPosition(new Position((enemy.getPosition().x + (enemy.getSize().mWidth / 2)) - (enemyBullet_Type24.getSize().mWidth / 2), enemy.getPosition().y + enemy.getSize().mHeight));
                                this.mBulletManager.addObject(enemyBullet_Type24);
                                EnemyBullet_Type2 enemyBullet_Type25 = new EnemyBullet_Type2(enemy, context);
                                enemyBullet_Type25.setImage(context.getResources(), R.drawable.enemy_big3_bullet);
                                enemyBullet_Type25.setPower(1);
                                enemyBullet_Type25.setId(-1);
                                enemyBullet_Type25.calcDirection(0, 15);
                                enemyBullet_Type25.setPosition(new Position((enemy.getPosition().x + (enemy.getSize().mWidth / 2)) - (enemyBullet_Type25.getSize().mWidth / 2), enemy.getPosition().y + enemy.getSize().mHeight));
                                this.mBulletManager.addObject(enemyBullet_Type25);
                                EnemyBullet_Type2 enemyBullet_Type26 = new EnemyBullet_Type2(enemy, context);
                                enemyBullet_Type26.setImage(context.getResources(), R.drawable.enemy_big3_bullet);
                                enemyBullet_Type26.setPower(1);
                                enemyBullet_Type26.setId(-1);
                                enemyBullet_Type26.calcDirection(5, 15);
                                enemyBullet_Type26.setPosition(new Position((enemy.getPosition().x + (enemy.getSize().mWidth / 2)) - (enemyBullet_Type26.getSize().mWidth / 2), enemy.getPosition().y + enemy.getSize().mHeight));
                                this.mBulletManager.addObject(enemyBullet_Type26);
                                bullet = null;
                                break;
                            case 11:
                                EnemyBullet_Type2 enemyBullet_Type27 = new EnemyBullet_Type2(enemy, context);
                                enemyBullet_Type27.setImage(context.getResources(), R.drawable.boss2_bullet);
                                enemyBullet_Type27.setPower(1);
                                enemyBullet_Type27.setId(-1);
                                enemyBullet_Type27.setPosition(new Position(enemy.getPosition().x, enemy.getPosition().y + (enemy.getSize().mHeight / 2)));
                                enemyBullet_Type27.setPower(Integer.MAX_VALUE);
                                this.mBulletManager.addObject(enemyBullet_Type27);
                                EnemyBullet_Type2 enemyBullet_Type28 = new EnemyBullet_Type2(enemy, context);
                                enemyBullet_Type28.setImage(context.getResources(), R.drawable.boss2_bullet);
                                enemyBullet_Type28.setPower(1);
                                enemyBullet_Type28.setId(-1);
                                enemyBullet_Type28.setPosition(new Position((enemy.getPosition().x + enemy.getSize().mWidth) - enemyBullet_Type28.getSize().mWidth, enemy.getPosition().y + (enemy.getSize().mHeight / 2)));
                                enemyBullet_Type28.setPower(Integer.MAX_VALUE);
                                this.mBulletManager.addObject(enemyBullet_Type28);
                                bullet.setImage(context.getResources(), R.drawable.enemy_bullet2);
                                bullet.setSpeed(18);
                                break;
                            case 12:
                                EnemyBullet_Type2 enemyBullet_Type29 = new EnemyBullet_Type2(enemy, context);
                                enemyBullet_Type29.setImage(context.getResources(), R.drawable.boss3_bullet);
                                enemyBullet_Type29.setPower(1);
                                enemyBullet_Type29.setId(-1);
                                enemyBullet_Type29.calcDirection(-5, 15);
                                enemyBullet_Type29.setPosition(new Position((enemy.getPosition().x + (enemy.getSize().mWidth / 2)) - (enemyBullet_Type29.getSize().mWidth / 2), enemy.getPosition().y + enemy.getSize().mHeight));
                                enemyBullet_Type29.setPower(Integer.MAX_VALUE);
                                this.mBulletManager.addObject(enemyBullet_Type29);
                                EnemyBullet_Type2 enemyBullet_Type210 = new EnemyBullet_Type2(enemy, context);
                                enemyBullet_Type210.setImage(context.getResources(), R.drawable.boss3_bullet);
                                enemyBullet_Type210.setPower(1);
                                enemyBullet_Type210.setId(-1);
                                enemyBullet_Type210.calcDirection(0, 15);
                                enemyBullet_Type210.setPosition(new Position((enemy.getPosition().x + (enemy.getSize().mWidth / 2)) - (enemyBullet_Type210.getSize().mWidth / 2), enemy.getPosition().y + enemy.getSize().mHeight));
                                enemyBullet_Type210.setPower(Integer.MAX_VALUE);
                                this.mBulletManager.addObject(enemyBullet_Type210);
                                EnemyBullet_Type2 enemyBullet_Type211 = new EnemyBullet_Type2(enemy, context);
                                enemyBullet_Type211.setImage(context.getResources(), R.drawable.boss3_bullet);
                                enemyBullet_Type211.setPower(1);
                                enemyBullet_Type211.setId(-1);
                                enemyBullet_Type211.calcDirection(5, 15);
                                enemyBullet_Type211.setPosition(new Position((enemy.getPosition().x + (enemy.getSize().mWidth / 2)) - (enemyBullet_Type211.getSize().mWidth / 2), enemy.getPosition().y + enemy.getSize().mHeight));
                                enemyBullet_Type211.setPower(Integer.MAX_VALUE);
                                this.mBulletManager.addObject(enemyBullet_Type211);
                                bullet = null;
                                break;
                            default:
                                bullet.setImage(context.getResources(), R.drawable.enemy_bullet);
                                break;
                        }
                        if (bullet != null) {
                            this.mBulletManager.addObject(bullet);
                        }
                    }
                }
            }
        }
    }

    public BaseObject getObject(int index) {
        if (this.mObject == null) {
            return null;
        }
        try {
            return this.mObject[index];
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    public void deleteObject(int index) {
        if (this.mObject != null) {
            try {
                this.mObject[index] = null;
            } catch (IndexOutOfBoundsException e) {
            }
        }
    }

    public void deleteObject(Rect screenRect) {
        super.deleteObject(screenRect);
        this.mBulletManager.deleteObject(screenRect);
    }

    public void playEffectSound(EffectSoundFrequency sound) {
        super.playEffect(sound);
    }

    public void release() {
        super.release();
        this.mBulletManager.release();
        if (mSelf != null) {
            mSelf = null;
        }
    }

    public boolean isHit(AllyManager allyManager) {
        if (this.mObject == null || allyManager == null) {
            return false;
        }
        if (StageManager.getInstance().getGameOverFlag()) {
            return false;
        }
        AllyBase ally = allyManager.getObject();
        if (ally == null) {
            return false;
        }
        for (int i = 0; i < this.mObject.length; i++) {
            EnemyBase enemy = (EnemyBase) this.mObject[i];
            if (enemy != null) {
                Position enemyPosition = enemy.getPosition();
                if (HitObject.isHit(enemyPosition, enemy.getSize(), ally.getPosition(), ally.getSize())) {
                    ally.gainPower(-1);
                    if (ally.getPower() < 0) {
                        EffectSoundManager.getInstance(this.mContext).play(EffectSoundFrequency.Ally_Destroy);
                        StageManager.getInstance().setGameOverFlag(true);
                        allyManager.setImage(this.mContext.getResources(), 0);
                        ExplodeManager.getInstance(this.mContext).addObject(new AllyExplode(ally.getPosition(), this.mContext));
                        return true;
                    }
                    ExplodeManager.getInstance(this.mContext).addObject(new ShieldExplode(enemyPosition, this.mContext));
                    if (ally.getPower() == 0) {
                        allyManager.setImage(this.mContext.getResources(), 0 | 1);
                    }
                    ((Vibrator) this.mContext.getSystemService("vibrator")).vibrate(100);
                    enemy.gainPower(-1);
                    if (1 > enemy.getPower()) {
                        this.mObject[i] = null;
                    }
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isHitBullet(AllyManager allyManager) {
        return this.mBulletManager.isHit(allyManager);
    }

    public void isHit(RockManager rockManager) {
        this.mBulletManager.isHit(rockManager);
    }

    public EnemyBulletManager getEnemyBulletManager() {
        return this.mBulletManager;
    }
}
