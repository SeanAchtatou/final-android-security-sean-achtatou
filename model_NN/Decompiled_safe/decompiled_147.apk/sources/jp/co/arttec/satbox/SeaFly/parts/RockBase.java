package jp.co.arttec.satbox.SeaFly.parts;

import android.content.Context;
import android.graphics.Rect;
import jp.co.arttec.satbox.SeaFly.util.EnemyKind;
import jp.co.arttec.satbox.SeaFly.util.Position;
import jp.co.arttec.satbox.SeaFly.util.Random;

public class RockBase extends BaseObject {
    private boolean mIsDestroy;
    private EnemyKind mKind;

    public RockBase(Position position, Context context) {
        init(context);
        this.mPosition.x = position.x;
        this.mPosition.y = position.y;
    }

    public RockBase(Rect screenRect, Context context) {
        init(context);
        this.mPosition.x = Random.getInstance().nextInt(screenRect.right - getSize().mWidth);
        this.mPosition.y = -getSize().mHeight;
    }

    /* access modifiers changed from: protected */
    public void init(Context context) {
        calcDirection();
        this.mIsDestroy = true;
        this.mKind = EnemyKind.Rock1;
    }

    /* access modifiers changed from: protected */
    public void calcDirection() {
        this.mDirection.dx = 0;
        this.mDirection.dy = Random.getInstance().nextInt(3) + 2;
    }

    public boolean isDestroy() {
        return this.mIsDestroy;
    }

    public void setDestroy(boolean flag) {
        this.mIsDestroy = flag;
    }

    public void setRockKind(EnemyKind kind) {
        this.mKind = kind;
    }

    public EnemyKind getRockKind() {
        return this.mKind;
    }
}
