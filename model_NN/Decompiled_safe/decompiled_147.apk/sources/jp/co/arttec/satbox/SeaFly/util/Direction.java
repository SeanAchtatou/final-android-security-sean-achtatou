package jp.co.arttec.satbox.SeaFly.util;

public class Direction {
    public int dx;
    public int dy;

    public Direction() {
        this.dy = 0;
        this.dx = 0;
    }

    public Direction(int dx2, int dy2) {
        this.dx = dx2;
        this.dy = dy2;
    }
}
