package jp.co.arttec.satbox.SeaFly;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

public class ResizeImage {
    private Rect dst = new Rect((int) (((float) this.x) * this.re_size_width), (int) (((float) this.y) * this.re_size_height), (int) ((((float) this.x) * this.re_size_width) + ((float) this.re_width)), (int) ((((float) this.y) * this.re_size_height) + ((float) this.re_height)));
    private Bitmap my_bitmap;
    private int re_height = ((int) (((float) this.my_bitmap.getHeight()) * this.re_size_height));
    private float re_size_height;
    private float re_size_width;
    private int re_width = ((int) (((float) this.my_bitmap.getWidth()) * this.re_size_width));
    private Rect src = new Rect(0, 0, this.my_bitmap.getWidth(), this.my_bitmap.getHeight());
    private boolean use_flg = false;
    private int x;
    private int y;

    public ResizeImage(Bitmap _source, int _x, int _y, float _r_width, float _r_height) {
        this.my_bitmap = _source;
        this.re_size_width = _r_width;
        this.re_size_height = _r_height;
        this.x = _x;
        this.y = _y;
    }

    public void draw(Canvas canvas) {
        if (this.my_bitmap != null) {
            canvas.drawBitmap(this.my_bitmap, this.src, this.dst, (Paint) null);
        }
    }

    public void draw(Canvas canvas, Paint p) {
        if (this.my_bitmap != null) {
            canvas.drawBitmap(this.my_bitmap, this.src, this.dst, p);
        }
    }

    public void setPos(int _x, int _y) {
        this.x = _x;
        this.y = _y;
        this.dst = new Rect((int) (((float) this.x) * this.re_size_width), (int) (((float) this.y) * this.re_size_height), (int) ((((float) this.x) * this.re_size_width) + ((float) this.re_width)), (int) ((((float) this.y) * this.re_size_height) + ((float) this.re_height)));
    }

    public void setScale(float _scale) {
        this.re_width = (int) (((float) this.my_bitmap.getWidth()) * _scale * this.re_size_width);
        this.re_height = (int) (((float) this.my_bitmap.getHeight()) * _scale * this.re_size_height);
        this.dst = new Rect((int) (((float) this.x) * this.re_size_width), (int) (((float) this.y) * this.re_size_height), (int) ((((float) this.x) * this.re_size_width) + ((float) this.re_width)), (int) ((((float) this.y) * this.re_size_height) + ((float) this.re_height)));
    }

    public int getWidth() {
        return this.re_width;
    }

    public int getHeight() {
        return this.re_height;
    }

    public Rect getSrc() {
        return this.src;
    }

    public Rect getDst() {
        return this.dst;
    }

    public void setDst(Rect _dst) {
        this.dst = _dst;
    }

    public void recycle() {
        if (this.my_bitmap != null) {
            this.my_bitmap.recycle();
        }
    }

    public boolean isUse() {
        return this.use_flg;
    }

    public void setUse(boolean _b) {
        this.use_flg = _b;
    }
}
