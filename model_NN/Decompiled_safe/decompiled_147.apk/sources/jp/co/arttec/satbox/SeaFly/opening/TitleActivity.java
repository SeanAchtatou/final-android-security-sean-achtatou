package jp.co.arttec.satbox.SeaFly.opening;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import jp.co.arttec.satbox.SeaFly.GameActivity;
import jp.co.arttec.satbox.SeaFly.GameRankMy;
import jp.co.arttec.satbox.SeaFly.R;
import jp.co.arttec.satbox.SeaFly.manager.EffectSoundManager;
import jp.co.arttec.satbox.SeaFly.manager.MusicManager;
import jp.co.arttec.satbox.SeaFly.manager.PreferencesManager;
import jp.co.arttec.satbox.SeaFly.util.BaseActivity;
import jp.co.arttec.satbox.SeaFly.util.EffectSoundFrequency;

public class TitleActivity extends BaseActivity {
    private static final int GAME = 1;
    private static final int STAGE_SELECT = 0;
    private int mMax_Nightmare;
    private int mMax_Normal;
    /* access modifiers changed from: private */
    public boolean mNightmareFlag;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.title);
        this.mNightmareFlag = false;
        this.mMax_Normal = PreferencesManager.getInstance(this).getMaxStage();
        this.mMax_Nightmare = PreferencesManager.getInstance(this).getMaxStageNightmare();
        ((Button) findViewById(R.id.start_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                EffectSoundManager.getInstance(TitleActivity.this.getApplication().getApplicationContext()).play(EffectSoundFrequency.Title_SE);
                if (PreferencesManager.getInstance(TitleActivity.this.getApplication().getApplicationContext()).getNightMareFlag()) {
                    TitleActivity.this.showSelectDialog();
                } else {
                    TitleActivity.this.startGame();
                }
            }
        });
        ((ImageButton) findViewById(R.id.satbox_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TitleActivity.this.moreApplicationLink();
            }
        });
        ((Button) findViewById(R.id.ranking_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                EffectSoundManager.getInstance(TitleActivity.this.getApplication().getApplicationContext()).play(EffectSoundFrequency.Title_SE);
                TitleActivity.this.startActivity(new Intent(TitleActivity.this, GameRankMy.class));
                TitleActivity.this.finish();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        MusicManager.getInstance(getApplication().getApplicationContext()).play(R.raw.title_bgm, true);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        MusicManager.getInstance(getApplication().getApplicationContext()).stop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        MusicManager.getInstance(getApplication().getApplicationContext()).release();
    }

    /* access modifiers changed from: private */
    public void showSelectDialog() {
        new AlertDialog.Builder(this).setTitle((int) R.string.select_stage).setIcon((int) R.drawable.icon).setPositiveButton((int) R.string.normal_select, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                EffectSoundManager.getInstance(TitleActivity.this.getApplication().getApplicationContext()).play(EffectSoundFrequency.Title_SE);
                TitleActivity.this.mNightmareFlag = false;
                TitleActivity.this.startGame();
            }
        }).setNegativeButton((int) R.string.nightmare_select, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                EffectSoundManager.getInstance(TitleActivity.this.getApplication().getApplicationContext()).play(EffectSoundFrequency.Title_SE);
                TitleActivity.this.mNightmareFlag = true;
                TitleActivity.this.startGame();
            }
        }).create().show();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.finish:
                EffectSoundManager.getInstance(getApplication().getApplicationContext()).play(EffectSoundFrequency.Title_SE);
                finish();
                return true;
            case R.id.help:
                goAboutPage();
                return true;
            case R.id.applink:
                moreApplicationLink();
                return true;
            case R.id.satbox:
                goHomePage();
                return true;
            default:
                return true;
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 0:
                int stage = data.getIntExtra("Stage", 1);
                Intent intent = new Intent(this, GameActivity.class);
                intent.putExtra(PreferencesManager.KEY_NIGHTMARE, this.mNightmareFlag);
                intent.putExtra("Stage", stage);
                startActivityForResult(intent, 1);
                return;
            case 1:
                if (data != null) {
                    int stage2 = data.getIntExtra("Stage", 1);
                    if (stage2 >= 6) {
                        stage2 = 6;
                    }
                    if (this.mNightmareFlag) {
                        if (stage2 > this.mMax_Nightmare) {
                            this.mMax_Nightmare = stage2;
                            PreferencesManager.getInstance(this).setMaxStageNightmare(this.mMax_Nightmare);
                            return;
                        }
                        return;
                    } else if (stage2 > this.mMax_Normal) {
                        this.mMax_Normal = stage2;
                        PreferencesManager.getInstance(this).setMaxStage(this.mMax_Normal);
                        return;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void startGame() {
        Class<GameActivity> cls = GameActivity.class;
        MusicManager.getInstance(getApplication().getApplicationContext()).stop();
        if (!this.mNightmareFlag) {
            if (1 < this.mMax_Normal) {
                Intent intent = new Intent(this, StageSelect.class);
                intent.putExtra("Stage", this.mMax_Normal);
                startActivityForResult(intent, 0);
                return;
            }
            Class<GameActivity> cls2 = GameActivity.class;
            Intent intent2 = new Intent(this, cls);
            intent2.putExtra(PreferencesManager.KEY_NIGHTMARE, this.mNightmareFlag);
            startActivityForResult(intent2, 1);
        } else if (1 < this.mMax_Nightmare) {
            Intent intent3 = new Intent(this, StageSelect.class);
            intent3.putExtra("Stage", this.mMax_Nightmare);
            startActivityForResult(intent3, 0);
        } else {
            Class<GameActivity> cls3 = GameActivity.class;
            Intent intent4 = new Intent(this, cls);
            intent4.putExtra(PreferencesManager.KEY_NIGHTMARE, this.mNightmareFlag);
            startActivityForResult(intent4, 1);
        }
    }
}
