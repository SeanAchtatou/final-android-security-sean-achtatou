package jp.co.arttec.satbox.SeaFly.parts;

import android.content.Context;
import android.graphics.Rect;
import jp.co.arttec.satbox.SeaFly.R;
import jp.co.arttec.satbox.SeaFly.util.EnemyKind;
import jp.co.arttec.satbox.SeaFly.util.Random;

public class EnemyType1 extends EnemyBase {
    public EnemyType1(Rect screenRect, Context context) {
        super(screenRect);
        setImage(context.getResources(), R.drawable.enemy1);
        calcDirection();
        this.mPosition.x = Random.getInstance().nextInt(screenRect.right - getSize().mWidth);
        this.mPosition.y = -getSize().mHeight;
        this.mBulletFrequency = 1;
        setEnemyKind(EnemyKind.Type1);
        this.mScore = 100;
    }

    /* access modifiers changed from: protected */
    public void calcDirection() {
        this.mDirection.dx = 0;
        this.mDirection.dy = 5;
    }
}
