package jp.co.arttec.satbox.SeaFly.util;

public class Shot {
    public static final int BACK_SHOT = 4;
    public static final int DOUBLE_SHOT = 2;
    public static final int NORMAL_SHOT = 1;
    public static final int POWER_SHOT = 8;
}
