package jp.co.arttec.satbox.SeaFly.event;

public interface GameStartListener {
    void ready();

    void readyEnd();
}
