package jp.co.arttec.satbox.SeaFly.manager;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import jp.co.arttec.satbox.SeaFly.parts.AllyBase;
import jp.co.arttec.satbox.SeaFly.util.Position;
import jp.co.arttec.satbox.SeaFly.util.Size;

public class AllyManager extends BaseManager {
    private static final int MAX_SHOT_FRAME = 4;
    private static final int MIN_SHOT_FRAME = 9;
    private static AllyManager mSelf = null;
    private int mFrame = 0;
    private boolean mIsShot = false;
    private int mShot = 1;
    private int mShotSpeed = MIN_SHOT_FRAME;
    private Position mTouchPosition = new Position();

    private AllyManager(Context context) {
        super(context);
    }

    public void setPowerUp() {
        AllyBase ally;
        if (this.mObject != null && (ally = (AllyBase) this.mObject[0]) != null) {
            ally.setPowerUp();
        }
    }

    public static AllyManager getInstance(Context context) {
        if (mSelf == null) {
            mSelf = new AllyManager(context);
        }
        return mSelf;
    }

    public void setShot(int shot) {
        this.mShot = shot;
    }

    public int getShot() {
        return this.mShot;
    }

    public void setShotSpeed(int shotSpeed) {
        if (shotSpeed > MIN_SHOT_FRAME) {
            shotSpeed = MIN_SHOT_FRAME;
        }
        if (shotSpeed < 4) {
            shotSpeed = 4;
        }
        this.mShotSpeed = shotSpeed;
    }

    public int getShotSpeed() {
        return this.mShotSpeed;
    }

    public void move(Position position) {
        AllyBase ally;
        if (this.mObject != null && (ally = (AllyBase) this.mObject[0]) != null) {
            ally.setPosition(position);
        }
    }

    public void onDraw(Canvas canvas) {
        AllyBase ally;
        if (this.mObject != null && (ally = (AllyBase) this.mObject[0]) != null) {
            ally.draw(canvas);
            this.mFrame++;
        }
    }

    public void setTouchPosition(Position position) {
        this.mTouchPosition = position;
    }

    public Position getTouchPosition() {
        return this.mTouchPosition;
    }

    public Position getPosition() {
        if (this.mObject == null) {
            return null;
        }
        AllyBase ally = (AllyBase) this.mObject[0];
        if (this.mObject[0] == null) {
            return null;
        }
        return ally.getPosition();
    }

    public void setPosition(Position position) {
        if (this.mObject != null) {
            AllyBase ally = (AllyBase) this.mObject[0];
            if (this.mObject[0] != null) {
                ally.setPosition(position);
            }
        }
    }

    public Size getSize() {
        if (this.mObject == null) {
            return new Size(0, 0);
        }
        AllyBase ally = (AllyBase) this.mObject[0];
        if (this.mObject[0] == null) {
            return null;
        }
        return ally.getSize();
    }

    public boolean isShot() {
        if (StageManager.getInstance().getGameOverFlag()) {
            return false;
        }
        if (!this.mIsShot || this.mFrame % this.mShotSpeed != 0) {
            return false;
        }
        this.mFrame = 0;
        return true;
    }

    public void shot() {
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public AllyBase getObject() {
        if (this.mObject == null) {
            return null;
        }
        return (AllyBase) this.mObject[0];
    }

    public int getShield() {
        if (this.mObject == null) {
            return 0;
        }
        AllyBase ally = (AllyBase) this.mObject[0];
        if (this.mObject[0] == null) {
            return 0;
        }
        return ally.getPower();
    }

    public void setShield(int level) {
        if (this.mObject != null) {
            AllyBase ally = (AllyBase) this.mObject[0];
            if (this.mObject[0] != null) {
                ally.setPower(level);
            }
        }
    }

    public void setImage(Resources res, int state) {
        if (this.mObject != null) {
            AllyBase ally = (AllyBase) this.mObject[0];
            if (this.mObject[0] != null) {
                ally.setAllyState(res, state);
            }
        }
    }

    public void release() {
        super.release();
        if (mSelf != null) {
            mSelf = null;
        }
    }

    public void setShot(boolean isShot) {
        this.mIsShot = isShot;
    }
}
