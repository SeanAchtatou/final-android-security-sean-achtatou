package jp.co.arttec.satbox.SeaFly;

public final class R {

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int keywords = 2130771971;
        public static final int primaryTextColor = 2130771969;
        public static final int refreshAnimation = 2130771974;
        public static final int refreshInterval = 2130771972;
        public static final int requestInterval = 2130771973;
        public static final int secondaryTextColor = 2130771970;
        public static final int testMode = 2130771976;
        public static final int visibility = 2130771975;
    }

    public static final class drawable {
        public static final int ad_logo = 2130837504;
        public static final int ally = 2130837505;
        public static final int ally_bullet = 2130837506;
        public static final int ally_bullet_back = 2130837507;
        public static final int ally_bullet_double = 2130837508;
        public static final int ally_bullet_double_back = 2130837509;
        public static final int ally_bullet_power = 2130837510;
        public static final int ally_dummy = 2130837511;
        public static final int ally_explode00 = 2130837512;
        public static final int ally_explode01 = 2130837513;
        public static final int ally_explode02 = 2130837514;
        public static final int ally_explode03 = 2130837515;
        public static final int ally_left = 2130837516;
        public static final int ally_left_s = 2130837517;
        public static final int ally_right = 2130837518;
        public static final int ally_right_s = 2130837519;
        public static final int ally_s = 2130837520;
        public static final int ally_shield = 2130837521;
        public static final int allybullet = 2130837522;
        public static final int back_star = 2130837523;
        public static final int back_star2 = 2130837524;
        public static final int back_star3 = 2130837525;
        public static final int backshot_item = 2130837526;
        public static final int black_item = 2130837527;
        public static final int boss1_bullet = 2130837528;
        public static final int boss2_bullet = 2130837529;
        public static final int boss3_bullet = 2130837530;
        public static final int doubleshot_item = 2130837531;
        public static final int enemy1 = 2130837532;
        public static final int enemy2 = 2130837533;
        public static final int enemy3 = 2130837534;
        public static final int enemy4 = 2130837535;
        public static final int enemy4_left = 2130837536;
        public static final int enemy4_right = 2130837537;
        public static final int enemy5 = 2130837538;
        public static final int enemy6 = 2130837539;
        public static final int enemy_big1 = 2130837540;
        public static final int enemy_big1_bullet = 2130837541;
        public static final int enemy_big2 = 2130837542;
        public static final int enemy_big2_bullet = 2130837543;
        public static final int enemy_big3 = 2130837544;
        public static final int enemy_big3_bullet = 2130837545;
        public static final int enemy_boss1 = 2130837546;
        public static final int enemy_boss2 = 2130837547;
        public static final int enemy_boss3 = 2130837548;
        public static final int enemy_bullet = 2130837549;
        public static final int enemy_bullet1 = 2130837550;
        public static final int enemy_bullet2 = 2130837551;
        public static final int enemy_bullet3 = 2130837552;
        public static final int enemy_bullet4 = 2130837553;
        public static final int enemy_bullet5 = 2130837554;
        public static final int enemy_bullet6 = 2130837555;
        public static final int enemy_explode00 = 2130837556;
        public static final int enemy_explode01 = 2130837557;
        public static final int enemy_explode02 = 2130837558;
        public static final int enemy_explode03 = 2130837559;
        public static final int enemy_star1 = 2130837560;
        public static final int enemy_star2 = 2130837561;
        public static final int enemy_star3 = 2130837562;
        public static final int enemy_star4 = 2130837563;
        public static final int enemy_star5 = 2130837564;
        public static final int enemy_star6 = 2130837565;
        public static final int enemybullet = 2130837566;
        public static final int explode_big_orange00 = 2130837567;
        public static final int explode_big_orange01 = 2130837568;
        public static final int explode_big_orange02 = 2130837569;
        public static final int explode_big_orange03 = 2130837570;
        public static final int explode_dummy = 2130837571;
        public static final int explode_rock00 = 2130837572;
        public static final int explode_rock01 = 2130837573;
        public static final int explode_rock02 = 2130837574;
        public static final int explode_rock03 = 2130837575;
        public static final int explode_small_orange00 = 2130837576;
        public static final int explode_small_orange01 = 2130837577;
        public static final int explode_small_orange02 = 2130837578;
        public static final int explode_small_orange03 = 2130837579;
        public static final int game_background = 2130837580;
        public static final int icon = 2130837581;
        public static final int pc_satbox = 2130837582;
        public static final int planet1 = 2130837583;
        public static final int planet1_b = 2130837584;
        public static final int planet2 = 2130837585;
        public static final int planet2_b = 2130837586;
        public static final int planet2_c = 2130837587;
        public static final int powershot_item = 2130837588;
        public static final int rank_back = 2130837589;
        public static final int rock2 = 2130837590;
        public static final int rock2_background = 2130837591;
        public static final int rock3 = 2130837592;
        public static final int rock4 = 2130837593;
        public static final int sat_logo = 2130837594;
        public static final int shield1 = 2130837595;
        public static final int shield2 = 2130837596;
        public static final int shield3 = 2130837597;
        public static final int shield4 = 2130837598;
        public static final int shield5 = 2130837599;
        public static final int shield6 = 2130837600;
        public static final int shield_ally = 2130837601;
        public static final int shield_explode00 = 2130837602;
        public static final int shield_explode01 = 2130837603;
        public static final int shield_explode02 = 2130837604;
        public static final int shield_explode03 = 2130837605;
        public static final int shield_item = 2130837606;
        public static final int speed1 = 2130837607;
        public static final int speed2 = 2130837608;
        public static final int speed3 = 2130837609;
        public static final int speed4 = 2130837610;
        public static final int speed5 = 2130837611;
        public static final int speed_item = 2130837612;
        public static final int title = 2130837613;
    }

    public static final class id {
        public static final int about_message = 2131230721;
        public static final int adproxy = 2131230736;
        public static final int applink = 2131230762;
        public static final int credit = 2131230722;
        public static final int dialog_edittext = 2131230749;
        public static final int dialog_textview = 2131230748;
        public static final int finish = 2131230760;
        public static final int footer = 2131230734;
        public static final int header = 2131230732;
        public static final int help = 2131230761;
        public static final int help_img_01 = 2131230725;
        public static final int help_img_02 = 2131230727;
        public static final int help_img_03 = 2131230728;
        public static final int help_img_04 = 2131230729;
        public static final int help_img_05 = 2131230730;
        public static final int help_message = 2131230726;
        public static final int layout_about_root = 2131230720;
        public static final int layout_help_root = 2131230723;
        public static final int main = 2131230757;
        public static final int message = 2131230742;
        public static final int name = 2131230738;
        public static final int ok = 2131230735;
        public static final int pause_iv = 2131230744;
        public static final int pause_tv = 2131230745;
        public static final int rank_tx_touch = 2131230747;
        public static final int ranking = 2131230737;
        public static final int ranking_button = 2131230759;
        public static final int ranklistview = 2131230746;
        public static final int satbox = 2131230763;
        public static final int satbox_button = 2131230756;
        public static final int score = 2131230739;
        public static final int select_ad_view = 2131230740;
        public static final int select_ok = 2131230755;
        public static final int select_stage = 2131230754;
        public static final int shieldView = 2131230743;
        public static final int stageView = 2131230741;
        public static final int start_button = 2131230758;
        public static final int title = 2131230731;
        public static final int titleDivider = 2131230724;
        public static final int txtItem1 = 2131230751;
        public static final int txtItem21 = 2131230752;
        public static final int txtItem22 = 2131230753;
        public static final int txtRankNo = 2131230750;
        public static final int world_rank = 2131230733;
    }

    public static final class layout {
        public static final int about = 2130903040;
        public static final int credit = 2130903041;
        public static final int help = 2130903042;
        public static final int hiscore = 2130903043;
        public static final int init = 2130903044;
        public static final int list = 2130903045;
        public static final int main = 2130903046;
        public static final int rank = 2130903047;
        public static final int regist = 2130903048;
        public static final int row_my = 2130903049;
        public static final int stage_select = 2130903050;
        public static final int title = 2130903051;
    }

    public static final class menu {
        public static final int menu = 2131165184;
    }

    public static final class raw {
        public static final int ally_explode = 2130968576;
        public static final int baku = 2130968577;
        public static final int boss = 2130968578;
        public static final int boss_explode = 2130968579;
        public static final int damage_se = 2130968580;
        public static final int flash = 2130968581;
        public static final int hit_se = 2130968582;
        public static final int powerup = 2130968583;
        public static final int se_title = 2130968584;
        public static final int shot = 2130968585;
        public static final int stage1 = 2130968586;
        public static final int stage2 = 2130968587;
        public static final int stage3 = 2130968588;
        public static final int stage4 = 2130968589;
        public static final int star = 2130968590;
        public static final int title_bgm = 2130968591;
    }

    public static final class string {
        public static final int about = 2131034122;
        public static final int about_title = 2131034125;
        public static final int app_name = 2131034112;
        public static final int applink = 2131034123;
        public static final int dialog_title = 2131034117;
        public static final int exit = 2131034121;
        public static final int gameover = 2131034126;
        public static final int help_about_game = 2131034130;
        public static final int help_msg = 2131034128;
        public static final int help_title = 2131034129;
        public static final int input_text = 2131034116;
        public static final int item_b = 2131034133;
        public static final int item_m = 2131034135;
        public static final int item_p = 2131034132;
        public static final int item_s = 2131034131;
        public static final int item_ss = 2131034134;
        public static final int nightmare = 2131034115;
        public static final int nightmare_select = 2131034120;
        public static final int normal = 2131034114;
        public static final int normal_select = 2131034119;
        public static final int ready = 2131034127;
        public static final int satbox = 2131034124;
        public static final int select_stage = 2131034118;
        public static final int version = 2131034113;
    }

    public static final class style {
        public static final int RankTextStyle = 2131099648;
    }

    public static final class styleable {
        public static final int[] com_admob_android_ads_AdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.keywords, R.attr.refreshInterval, R.attr.requestInterval, R.attr.refreshAnimation, R.attr.visibility, R.attr.testMode};
        public static final int com_admob_android_ads_AdView_backgroundColor = 0;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_primaryTextColor = 1;
        public static final int com_admob_android_ads_AdView_refreshAnimation = 6;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_requestInterval = 5;
        public static final int com_admob_android_ads_AdView_secondaryTextColor = 2;
        public static final int com_admob_android_ads_AdView_testMode = 8;
        public static final int com_admob_android_ads_AdView_visibility = 7;
    }
}
