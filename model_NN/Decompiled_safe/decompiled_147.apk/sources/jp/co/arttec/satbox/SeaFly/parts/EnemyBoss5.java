package jp.co.arttec.satbox.SeaFly.parts;

import android.content.Context;
import android.graphics.Rect;
import jp.co.arttec.satbox.SeaFly.R;
import jp.co.arttec.satbox.SeaFly.manager.StageManager;
import jp.co.arttec.satbox.SeaFly.util.EnemyKind;

public class EnemyBoss5 extends EnemyBase {
    private int mFrame = 0;
    private Rect mScreenRect;

    public EnemyBoss5(Rect screenRect, Context context) {
        super(screenRect);
        setImage(context.getResources(), R.drawable.enemy_boss2);
        calcDirection();
        this.mPosition.x = (screenRect.right - getSize().mWidth) / 2;
        this.mPosition.y = -this.mSize.mHeight;
        this.mPower = (StageManager.getInstance().getStage() * 50) + 250;
        setEnemyKind(EnemyKind.Boss5);
        this.mScore = StageManager.getInstance().getStage() * 20000;
        this.mScreenRect = screenRect;
        this.mBulletFrequency = StageManager.getInstance().getStage() + 10;
    }

    public void move() {
        this.mFrame++;
        calcDirection();
        super.move();
        if (this.mPosition.x < 0) {
            this.mPosition.x = 0;
        }
        if (this.mPosition.x > this.mScreenRect.right - getSize().mWidth) {
            this.mPosition.x = this.mScreenRect.right - getSize().mWidth;
        }
    }

    /* access modifiers changed from: protected */
    public void calcDirection() {
        if (this.mFrame < 50) {
            this.mDirection.dx = 0;
            this.mDirection.dy = 4;
        } else if (this.mFrame < 120) {
            this.mDirection.dx = -10;
            this.mDirection.dy = 0;
        } else if (this.mFrame < 200) {
            this.mDirection.dx = 10;
            this.mDirection.dy = 0;
        } else if (this.mFrame < 300) {
            this.mDirection.dx = -10;
            this.mDirection.dy = 0;
            if (this.mPosition.x < (this.mScreenRect.right - getSize().mWidth) / 2) {
                this.mPosition.x = (this.mScreenRect.right - getSize().mWidth) / 2;
                this.mFrame = 300;
            }
        } else if (this.mFrame < 400) {
            this.mDirection.dx = 0;
            this.mDirection.dy = 10;
            if (this.mPosition.y > (-getSize().mHeight) / 5) {
                this.mFrame = 400;
            }
        } else if (this.mFrame < 500) {
            this.mDirection.dx = -10;
            this.mDirection.dy = 0;
        } else if (this.mFrame < 600) {
            this.mDirection.dx = 10;
            this.mDirection.dy = 0;
        } else if (this.mFrame < 700) {
            this.mDirection.dx = -10;
            this.mDirection.dy = 0;
            if (this.mPosition.x < (this.mScreenRect.right - getSize().mWidth) / 2) {
                this.mPosition.x = (this.mScreenRect.right - getSize().mWidth) / 2;
                this.mFrame = 700;
            }
        } else if (this.mFrame < 800) {
            this.mDirection.dx = 0;
            this.mDirection.dy = -6;
            if (this.mPosition.y < (-getSize().mHeight)) {
                this.mPosition.y = -getSize().mHeight;
                this.mFrame = 0;
            }
        }
    }
}
