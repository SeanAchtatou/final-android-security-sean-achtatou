package jp.co.arttec.satbox.SeaFly.manager;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.util.HashMap;
import jp.co.arttec.satbox.SeaFly.R;

public class CacheImageManager {
    private static CacheImageManager mSelf = null;
    private HashMap<Integer, Bitmap> mBitmapResource = new HashMap<>();
    private Context mContext;

    private CacheImageManager(Context context) {
        this.mContext = context;
        readImageFromResource();
    }

    public static CacheImageManager getInstance(Context context) {
        if (mSelf == null) {
            mSelf = new CacheImageManager(context);
        }
        return mSelf;
    }

    public static CacheImageManager getObject() {
        return mSelf;
    }

    private void readImageFromResource() {
        Resources res = this.mContext.getResources();
        try {
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.rock2), BitmapFactory.decodeResource(res, R.drawable.rock2));
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.rock2_background), BitmapFactory.decodeResource(res, R.drawable.rock2_background));
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.rock3), BitmapFactory.decodeResource(res, R.drawable.rock3));
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.rock4), BitmapFactory.decodeResource(res, R.drawable.rock4));
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.planet1), BitmapFactory.decodeResource(res, R.drawable.planet1));
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.planet2), BitmapFactory.decodeResource(res, R.drawable.planet2));
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.enemy1), BitmapFactory.decodeResource(res, R.drawable.enemy1));
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.enemy2), BitmapFactory.decodeResource(res, R.drawable.enemy2));
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.enemy3), BitmapFactory.decodeResource(res, R.drawable.enemy3));
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.enemy4), BitmapFactory.decodeResource(res, R.drawable.enemy4));
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.enemy4_left), BitmapFactory.decodeResource(res, R.drawable.enemy4_left));
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.enemy4_right), BitmapFactory.decodeResource(res, R.drawable.enemy4_right));
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.enemy5), BitmapFactory.decodeResource(res, R.drawable.enemy5));
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.enemy6), BitmapFactory.decodeResource(res, R.drawable.enemy6));
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.enemy_bullet), BitmapFactory.decodeResource(res, R.drawable.enemy_bullet));
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.enemy_bullet1), BitmapFactory.decodeResource(res, R.drawable.enemy_bullet1));
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.enemy_bullet2), BitmapFactory.decodeResource(res, R.drawable.enemy_bullet2));
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.enemy_bullet3), BitmapFactory.decodeResource(res, R.drawable.enemy_bullet3));
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.enemy_bullet4), BitmapFactory.decodeResource(res, R.drawable.enemy_bullet4));
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.enemy_bullet5), BitmapFactory.decodeResource(res, R.drawable.enemy_bullet5));
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.enemy_bullet6), BitmapFactory.decodeResource(res, R.drawable.enemy_bullet6));
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.ally), BitmapFactory.decodeResource(res, R.drawable.ally));
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.ally_left), BitmapFactory.decodeResource(res, R.drawable.ally_left));
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.ally_right), BitmapFactory.decodeResource(res, R.drawable.ally_right));
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.ally_shield), BitmapFactory.decodeResource(res, R.drawable.ally_shield));
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.ally_s), BitmapFactory.decodeResource(res, R.drawable.ally_s));
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.ally_left_s), BitmapFactory.decodeResource(res, R.drawable.ally_left_s));
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.ally_right_s), BitmapFactory.decodeResource(res, R.drawable.ally_right_s));
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.ally_bullet), BitmapFactory.decodeResource(res, R.drawable.ally_bullet));
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.ally_bullet_back), BitmapFactory.decodeResource(res, R.drawable.ally_bullet_back));
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.ally_bullet_double), BitmapFactory.decodeResource(res, R.drawable.ally_bullet_double));
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.ally_bullet_power), BitmapFactory.decodeResource(res, R.drawable.ally_bullet_power));
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.enemy_explode00), BitmapFactory.decodeResource(res, R.drawable.enemy_explode00));
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.enemy_explode01), BitmapFactory.decodeResource(res, R.drawable.enemy_explode01));
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.enemy_explode02), BitmapFactory.decodeResource(res, R.drawable.enemy_explode02));
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.enemy_explode03), BitmapFactory.decodeResource(res, R.drawable.enemy_explode03));
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.explode_rock00), BitmapFactory.decodeResource(res, R.drawable.explode_rock00));
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.explode_rock01), BitmapFactory.decodeResource(res, R.drawable.explode_rock01));
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.explode_rock02), BitmapFactory.decodeResource(res, R.drawable.explode_rock02));
            this.mBitmapResource.put(Integer.valueOf((int) R.drawable.explode_rock03), BitmapFactory.decodeResource(res, R.drawable.explode_rock03));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Bitmap getImageFromResource(int resId) {
        return this.mBitmapResource.get(Integer.valueOf(resId));
    }
}
