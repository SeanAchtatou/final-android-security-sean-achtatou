package jp.co.arttec.satbox.SeaFly.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.ViewGroup;
import jp.co.arttec.satbox.SeaFly.R;
import jp.co.arttec.satbox.SeaFly.manager.EffectSoundManager;

public class BaseActivity extends Activity {
    /* access modifiers changed from: protected */
    public boolean mAbout;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setVolumeControlStream(3);
        this.mAbout = false;
    }

    /* access modifiers changed from: protected */
    public void goAboutPage() {
        EffectSoundManager.getInstance(getApplication().getApplicationContext()).play(EffectSoundFrequency.Title_SE);
        this.mAbout = true;
        new AlertDialog.Builder(this).setTitle((int) R.string.help_title).setIcon((int) R.drawable.icon).setView(getLayoutInflater().inflate((int) R.layout.help, (ViewGroup) findViewById(R.id.layout_help_root))).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                EffectSoundManager.getInstance(BaseActivity.this.getApplication().getApplicationContext()).play(EffectSoundFrequency.Title_SE);
                dialog.dismiss();
                BaseActivity.this.mAbout = false;
            }
        }).create().show();
    }

    /* access modifiers changed from: protected */
    public void goHomePage() {
        EffectSoundManager.getInstance(getApplication().getApplicationContext()).play(EffectSoundFrequency.Title_SE);
        try {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.arttec.co.jp/sat-box")));
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void moreApplicationLink() {
        EffectSoundManager.getInstance(getApplication().getApplicationContext()).play(EffectSoundFrequency.Title_SE);
        try {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pub:SAT-BOX")));
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }
}
