package mediba.ad.sdk.android;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.JarURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import mediba.ad.sdk.android.AdOverlay;
import mediba.ad.sdk.android.AdProxy;

public class AdContainer extends RelativeLayout implements AdProxy.b {
    public static final String IMAGE_PATH = "res/grab.jpg";
    private static final String _TAG = "AdContainer";
    /* access modifiers changed from: private */
    public static ImageView action_icon;
    /* access modifiers changed from: private */
    public static Context context;
    public static Intent intent;
    /* access modifiers changed from: private */
    public static ProgressBar mProgressbar;
    private static float mdensity = -1.0f;
    private final int FP = -1;
    private final int WC = -2;
    private View cachedView;
    private boolean g;
    private ImageView gradation_bg;
    private ImageView gradation_image;
    private LinearLayout gradation_layout;
    private ImageView icon_image;
    /* access modifiers changed from: private */
    public RelativeLayout icon_layout;
    /* access modifiers changed from: private */
    public ImageView image_layout;
    private Long last_update_second = -1;
    private String layoutmode;
    private TextView lower_icon_text;
    protected AdProxy mAdproxy;
    private final MasAdView mAdview;
    /* access modifiers changed from: private */
    public ProgressBar progressbar;
    private TextView signature_view;
    private boolean t;
    private TextView upper_icon_text;

    static {
        if (AdUtil.isLogEnable()) {
            Log.d(_TAG, "Activate AdContainer");
        }
    }

    public AdContainer(AdProxy adproxy, Context context2, MasAdView adview) {
        super(context2);
        Bitmap cbm_a;
        Bitmap cbm_b;
        context = context2;
        setFocusable(false);
        setFocusableInTouchMode(false);
        if (AdUtil.isLogEnable()) {
            Log.d(_TAG, "AdContainer Construct");
        }
        this.last_update_second = -1;
        this.mAdview = adview;
        setId(1);
        mdensity = getResources().getDisplayMetrics().density;
        setBackgroundDrawable(context2.getResources().getDrawable(17301602));
        int icon_size = (int) (48.0f * mdensity);
        int icon_padding = (int) (5.0f * mdensity);
        int progress_padding = (int) (8.0f * mdensity);
        int deviceWidth = MasAdManager.getScreenWidth(context2);
        int screenHeight = MasAdManager.getScreenHeight(context2);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        this.image_layout = new ImageView(context2);
        this.image_layout.setScaleType(ImageView.ScaleType.FIT_XY);
        this.icon_layout = new RelativeLayout(context2);
        this.icon_layout.setBackgroundColor(this.mAdview.getBackgroundColor());
        RelativeLayout.LayoutParams icon = new RelativeLayout.LayoutParams(icon_size, icon_size);
        this.icon_image = new ImageView(context2);
        this.icon_image.setScaleType(ImageView.ScaleType.FIT_XY);
        this.icon_image.setPadding(icon_padding, icon_padding, icon_padding, icon_padding);
        this.icon_image.setId(10);
        this.icon_layout.addView(this.icon_image, icon);
        if (this.mAdview.getBackgroundColor() == 2) {
            cbm_a = getImageBitmap(getClass().getClassLoader().getResource("mediba/ad/sdk/android/images/gradation_w_alpha_left.png"));
            cbm_b = getImageBitmap(getClass().getClassLoader().getResource("mediba/ad/sdk/android/images/gradation_w_alpha_right.png"));
        } else {
            cbm_a = getImageBitmap(getClass().getClassLoader().getResource("mediba/ad/sdk/android/images/gradation_b_alpha_left.png"));
            cbm_b = getImageBitmap(getClass().getClassLoader().getResource("mediba/ad/sdk/android/images/gradation_b_alpha_right.png"));
        }
        this.gradation_layout = new LinearLayout(context2);
        this.gradation_layout.setOrientation(0);
        this.gradation_image = new ImageView(context2);
        this.gradation_image.setImageBitmap(cbm_a);
        this.gradation_image.setScaleType(ImageView.ScaleType.FIT_XY);
        this.gradation_image.setLayoutParams(new Gallery.LayoutParams(icon_size, icon_size));
        this.gradation_bg = new ImageView(context2);
        this.gradation_bg.setImageBitmap(cbm_b);
        this.gradation_bg.setScaleType(ImageView.ScaleType.FIT_XY);
        this.gradation_bg.setLayoutParams(new Gallery.LayoutParams(deviceWidth, -1));
        this.gradation_layout.addView(this.gradation_image);
        this.gradation_layout.addView(this.gradation_bg);
        this.icon_layout.addView(this.gradation_layout);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(icon_size, icon_size);
        layoutParams2.addRule(11);
        mProgressbar = new ProgressBar(context2);
        mProgressbar.setPadding(progress_padding, progress_padding, progress_padding, progress_padding);
        mProgressbar.setMax(2);
        mProgressbar.incrementProgressBy(1);
        mProgressbar.setVisibility(4);
        this.icon_layout.addView(mProgressbar, layoutParams2);
        RelativeLayout.LayoutParams action_set = new RelativeLayout.LayoutParams(icon_size, icon_size);
        action_set.addRule(11);
        action_icon = new ImageView(context2);
        action_icon.setScaleType(ImageView.ScaleType.FIT_XY);
        action_icon.setPadding(icon_padding, icon_padding, icon_padding, icon_padding);
        action_icon.setId(13);
        this.icon_layout.addView(action_icon, action_set);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -2);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams3.setMargins(0, (int) (5.0f * mdensity), 0, -((int) (3.0f * mdensity)));
        layoutParams3.addRule(1, 10);
        layoutParams4.addRule(1, 10);
        layoutParams4.addRule(3, 11);
        layoutParams4.setMargins(0, 0, 0, -((int) (5.0f * mdensity)));
        this.upper_icon_text = new TextView(context2);
        this.upper_icon_text.setId(11);
        this.upper_icon_text.setTextColor(this.mAdview.getPrimaryTextColor());
        this.lower_icon_text = new TextView(context2);
        this.lower_icon_text.setId(12);
        this.lower_icon_text.setTextColor(this.mAdview.getSecondaryTextColor());
        this.icon_layout.addView(this.upper_icon_text, layoutParams3);
        this.icon_layout.addView(this.lower_icon_text, layoutParams4);
        RelativeLayout.LayoutParams signature = new RelativeLayout.LayoutParams(-2, -2);
        signature.addRule(3, 12);
        signature.addRule(11);
        this.signature_view = new TextView(context2);
        this.signature_view.setTextSize(10.0f);
        this.signature_view.setText("Ads by mediba");
        if (this.mAdview.getBackgroundColor() == 1) {
            this.signature_view.setTextColor(Color.rgb(153, 153, 153));
        } else if (this.mAdview.getBackgroundColor() == 2) {
            this.signature_view.setTextColor(Color.rgb(102, 102, 102));
        }
        this.icon_layout.addView(this.signature_view, signature);
        addView(this.icon_layout);
        addView(this.image_layout, layoutParams);
        setEventStatus(null);
    }

    /* access modifiers changed from: package-private */
    public final void setEventStatus(AdProxy adproxy) {
        this.mAdproxy = adproxy;
        if (adproxy == null) {
            setClickable(false);
            return;
        }
        adproxy.setAdContainer(this);
        setFocusable(true);
        setClickable(true);
    }

    static float getDensity() {
        return mdensity;
    }

    protected static void setActionIconVisibility(int visibility) {
        if (visibility == 0) {
            action_icon.setVisibility(0);
        } else if (visibility == 4) {
            action_icon.setVisibility(4);
        }
    }

    protected static void setProgressbarVisibility(int visibility) {
        if (visibility == 0) {
            mProgressbar.setVisibility(0);
        } else if (visibility == 4) {
            mProgressbar.setVisibility(4);
        }
    }

    public void setImageView(String image_path) {
        this.image_layout.setImageDrawable(ImageOperations(getContext(), image_path, "ad.png"));
        this.icon_layout.setVisibility(4);
        this.image_layout.setVisibility(0);
        this.layoutmode = "image";
        invalidate();
    }

    public void setImageLink(final String link_path, final String mode) {
        this.image_layout.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                        AdContainer.this.setPressed(true);
                        break;
                    case 1:
                        if (AdContainer.this.image_layout.isPressed()) {
                            AdContainer.this.image_layout.performClick();
                        }
                        AdContainer.this.setPressed(false);
                        break;
                }
                return false;
            }
        });
        this.image_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AdContainer.this.image_layout.setClickable(false);
                Handler mHandler = new Handler();
                final String str = mode;
                final String str2 = link_path;
                mHandler.post(new Runnable() {
                    public void run() {
                        if (str.equals("nomal")) {
                            AdContainer.intent = new Intent("android.intent.action.VIEW", Uri.parse(str2));
                            AdContainer.context.startActivity(AdContainer.intent);
                        } else if (str.equals("overlay")) {
                            new AdOverlay.Builder(AdContainer.this.getContext(), str2).create().show();
                        }
                    }
                });
                mHandler.postDelayed(new Runnable() {
                    public void run() {
                        AdContainer.this.image_layout.setClickable(true);
                    }
                }, 1000);
            }
        });
    }

    public void setIconView(String upper_text, String lower_text, String icon1_path, String icon2_path) {
        this.upper_icon_text.setTextSize(12.0f);
        this.lower_icon_text.setTextSize(12.0f);
        this.upper_icon_text.setText(upper_text);
        this.lower_icon_text.setText(lower_text);
        Drawable ad = ImageOperations(getContext(), icon1_path, "ad.png");
        Drawable ai = ImageOperations(getContext(), icon2_path, "ai.png");
        if (ad != null && ai != null) {
            this.icon_image.setImageDrawable(ad);
            action_icon.setImageDrawable(ai);
            this.signature_view.setPadding(0, 0, (int) (48.0f * mdensity), 0);
        } else if (ad != null && ai == null) {
            this.icon_image.setImageDrawable(ad);
            action_icon.setVisibility(4);
            int progress_padding = (int) (mdensity * 14.0f);
            mProgressbar.setPadding(progress_padding, progress_padding, progress_padding, progress_padding);
            this.signature_view.setPadding(0, 0, (int) (mdensity * 8.0f), 0);
        } else if (ad == null && ai != null) {
            this.icon_image.setImageDrawable(ai);
            action_icon.setVisibility(4);
            int progress_padding2 = (int) (mdensity * 14.0f);
            mProgressbar.setPadding(progress_padding2, progress_padding2, progress_padding2, progress_padding2);
            this.signature_view.setPadding(0, 0, (int) (mdensity * 8.0f), 0);
        }
        this.image_layout.setVisibility(4);
        this.icon_layout.setVisibility(0);
        this.layoutmode = "icon";
        invalidate();
    }

    public void setIconLink(final String link_path, final String mode) {
        this.icon_layout.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                        AdContainer.this.setPressed(true);
                        break;
                    case 1:
                        if (AdContainer.this.icon_layout.isPressed()) {
                            AdContainer.this.icon_layout.performClick();
                        }
                        AdContainer.this.setPressed(false);
                        break;
                }
                return false;
            }
        });
        this.icon_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AdContainer.this.icon_layout.setClickable(false);
                if (AdContainer.action_icon.getVisibility() != 0) {
                    Handler mHandler = new Handler();
                    mHandler.post(new Runnable() {
                        public void run() {
                            AdContainer.mProgressbar.setVisibility(0);
                        }
                    });
                    final String str = mode;
                    final String str2 = link_path;
                    mHandler.postDelayed(new Runnable() {
                        public void run() {
                            if (str.equals("nomal")) {
                                AdContainer.intent = new Intent("android.intent.action.VIEW", Uri.parse(str2));
                                AdContainer.context.startActivity(AdContainer.intent);
                            } else if (str.equals("overlay")) {
                                new AdOverlay.Builder(AdContainer.this.getContext(), str2).create().show();
                            }
                        }
                    }, 500);
                    mHandler.postDelayed(new Runnable() {
                        public void run() {
                            AdContainer.mProgressbar.setVisibility(4);
                            AdContainer.this.icon_layout.setClickable(true);
                        }
                    }, 1000);
                    return;
                }
                Animation animation = new ScaleAnimation(1.2f, 0.0f, 1.2f, 0.0f, 1, 0.5f, 1, 0.5f);
                animation.setDuration(500);
                AdContainer.action_icon.startAnimation(animation);
                AdContainer.action_icon.setVisibility(4);
                Handler mHandler2 = new Handler();
                mHandler2.postDelayed(new Runnable() {
                    public void run() {
                        AdContainer.mProgressbar.setVisibility(0);
                    }
                }, 600);
                final String str3 = mode;
                final String str4 = link_path;
                mHandler2.postDelayed(new Runnable() {
                    public void run() {
                        if (str3.equals("nomal")) {
                            AdContainer.intent = new Intent("android.intent.action.VIEW", Uri.parse(str4));
                            AdContainer.context.startActivity(AdContainer.intent);
                        } else if (str3.equals("overlay")) {
                            new AdOverlay.Builder(AdContainer.this.getContext(), str4).create().show();
                        }
                    }
                }, 1000);
                mHandler2.postDelayed(new Runnable() {
                    public void run() {
                        AdContainer.mProgressbar.setVisibility(4);
                        AdContainer.action_icon.setVisibility(0);
                        AdContainer.this.icon_layout.setClickable(true);
                    }
                }, 2000);
            }
        });
    }

    public void setView(View localview, RelativeLayout.LayoutParams layoutparam) {
        if (AdUtil.isLogEnable()) {
            Log.d(_TAG, "setView");
        }
        if (localview != null && localview != this.cachedView) {
            this.cachedView = localview;
            this.progressbar = new ProgressBar(getContext());
            this.progressbar.setIndeterminate(true);
            this.progressbar.setId(2);
            if (layoutparam != null) {
                this.progressbar.setLayoutParams(layoutparam);
            }
            this.progressbar.setVisibility(4);
            if (AdUtil.isLogEnable()) {
                Log.d(_TAG, "Loading");
            }
            post(new loading(this));
        } else if (AdUtil.isLogEnable()) {
            Log.d(_TAG, "return");
        }
    }

    private static class loading implements Runnable {
        private WeakReference a;

        public loading(AdContainer adcontainer) {
            if (AdUtil.isLogEnable()) {
                Log.d(AdContainer._TAG, "Loading Construct");
            }
            this.a = new WeakReference(adcontainer);
        }

        public final void run() {
            if (AdUtil.isLogEnable()) {
                Log.d(AdContainer._TAG, "Running Progress");
            }
            try {
                AdContainer adc = (AdContainer) this.a.get();
                if (adc != null) {
                    adc.addView(adc.progressbar);
                }
            } catch (Exception e) {
                Log.e(AdContainer._TAG, "exception caught in AdContainer post run(), " + e.getMessage());
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas paramCanvas) {
        if (isPressed() || isFocused()) {
            paramCanvas.clipRect(3, 3, getWidth() - 3, getHeight() - 3);
        }
        super.onDraw(paramCanvas);
        if (this.last_update_second.longValue() != -1) {
            this.last_update_second = Long.valueOf(SystemClock.uptimeMillis());
        }
    }

    /* access modifiers changed from: protected */
    public final AdProxy getAdProxyInstance() {
        return this.mAdproxy;
    }

    private void k() {
        if (AdUtil.isLogEnable()) {
            Log.d(_TAG, "k");
        }
        if (this.mAdproxy != null && isPressed()) {
            setPressed(false);
            if (!this.g) {
                this.g = true;
            }
            if (this.mAdview != null) {
                this.mAdview.performClick();
            }
        }
    }

    public void a() {
        post(new c(this));
    }

    public final void setPressed(boolean flag) {
        if ((!this.g || !flag) && isPressed() != flag) {
            if (AdProxy.intent != null) {
                getContext().startActivity(AdProxy.intent);
            }
            super.setPressed(flag);
            invalidate();
        }
    }

    private static class c implements Runnable {
        private WeakReference<AdContainer> a;

        public c(AdContainer adcontainer) {
            this.a = new WeakReference<>(adcontainer);
        }

        public final void run() {
            if (AdUtil.isLogEnable()) {
                Log.d(AdContainer._TAG, "Adcontainer.c");
            }
            try {
                AdContainer adcontainer = this.a.get();
                if (adcontainer != null) {
                    adcontainer.swapProgressBar();
                }
            } catch (Exception e) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void swapProgressBar() {
        if (AdUtil.isLogEnable()) {
            Log.d(_TAG, "SwapProgress");
        }
        this.g = false;
        if (this.progressbar != null) {
            this.progressbar.setVisibility(4);
        }
        if (this.cachedView != null) {
            this.cachedView.setVisibility(0);
        }
    }

    public final long g() {
        long l1 = SystemClock.uptimeMillis() - this.last_update_second.longValue();
        if (this.last_update_second.longValue() < 0 || l1 < 0 || l1 > 10000000) {
            return 0;
        }
        return l1;
    }

    static MasAdView a(AdContainer adcontainer) {
        return adcontainer.mAdview;
    }

    public Object fetch(String address) throws MalformedURLException, IOException {
        Object content = new URL(address).getContent();
        if (AdUtil.isLogEnable()) {
            Log.d(_TAG, "fetching:" + address);
        }
        return content;
    }

    private Drawable ImageOperations(Context ctx, String url, String saveFilename) {
        try {
            return Drawable.createFromStream((InputStream) fetch(url), "src");
        } catch (MalformedURLException e) {
            MalformedURLException e2 = e;
            Log.e(_TAG, "exception caught in AdContainer.ImageOperations(), " + e2.getMessage());
            e2.printStackTrace();
            return null;
        } catch (IOException e3) {
            IOException e4 = e3;
            Log.e(_TAG, "exception caught in AdContainer.ImageOperations(), " + e4.getMessage());
            e4.printStackTrace();
            return null;
        } catch (Exception e5) {
            Exception e6 = e5;
            e6.printStackTrace();
            Log.e(_TAG, "exception caught in AdContainer.ImageOperations(), " + e6.getMessage());
            return null;
        }
    }

    public static Context getAdContext() {
        if (AdUtil.isLogEnable()) {
            Log.d(_TAG, "context:" + context.toString());
        }
        return context;
    }

    public Bitmap getImageBitmap(String url) {
        Bitmap tmp_bm = null;
        try {
            BufferedInputStream bis = new BufferedInputStream(((HttpURLConnection) new URL(url).openConnection()).getInputStream(), 1048576);
            tmp_bm = BitmapFactory.decodeStream(bis, null, new BitmapFactory.Options());
            bis.close();
            return tmp_bm;
        } catch (MalformedURLException e) {
            MalformedURLException e2 = e;
            Log.e(_TAG, "exception caught in AdContainer.getImageBitmap(), " + e2.getMessage());
            e2.printStackTrace();
            return tmp_bm;
        } catch (IOException e3) {
            IOException e4 = e3;
            Log.e(_TAG, "exception caught in AdContainer.getImageBitmap(), " + e4.getMessage());
            e4.printStackTrace();
            return tmp_bm;
        } catch (Exception e5) {
            Exception e6 = e5;
            Log.e(_TAG, "exception caught in AdContainer.getImageBitmap(), " + e6.getMessage());
            e6.printStackTrace();
            return tmp_bm;
        }
    }

    public Bitmap getImageBitmap(URL url) {
        Bitmap tmp_bm = null;
        try {
            BufferedInputStream bis = new BufferedInputStream(((JarURLConnection) url.openConnection()).getInputStream(), 1048576);
            tmp_bm = BitmapFactory.decodeStream(bis, null, new BitmapFactory.Options());
            bis.close();
            return tmp_bm;
        } catch (MalformedURLException e) {
            MalformedURLException e2 = e;
            Log.e(_TAG, "exception caught in AdContainer.getImageBitmap(), " + e2.getMessage());
            e2.printStackTrace();
            return tmp_bm;
        } catch (IOException e3) {
            IOException e4 = e3;
            Log.e(_TAG, "exception caught in AdContainer.getImageBitmap(), " + e4.getMessage());
            e4.printStackTrace();
            return tmp_bm;
        } catch (Exception e5) {
            Exception e6 = e5;
            Log.e(_TAG, "exception caught in AdContainer.getImageBitmap(), " + e6.getMessage());
            e6.printStackTrace();
            return tmp_bm;
        }
    }

    public void fadeIn(int time) {
        setVisibility(0);
        AlphaAnimation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration((long) time);
        animation.startNow();
        animation.setFillAfter(true);
        animation.setInterpolator(new AccelerateInterpolator());
        startAnimation(animation);
    }

    public void fadeOut(int time) {
        AlphaAnimation animation = new AlphaAnimation(1.0f, 0.0f);
        animation.setDuration((long) time);
        animation.startNow();
        animation.setFillAfter(true);
        animation.setInterpolator(new AccelerateInterpolator());
        startAnimation(animation);
    }

    public void slideDownIn(int time) {
        AnimationSet as = new AnimationSet(true);
        TranslateAnimation ta = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, -1.0f, 1, 0.0f);
        ta.setDuration((long) time);
        ta.startNow();
        ta.setFillAfter(true);
        ta.setInterpolator(new AccelerateInterpolator());
        as.addAnimation(ta);
        AlphaAnimation aa = new AlphaAnimation(0.0f, 1.0f);
        aa.setDuration((long) time);
        aa.startNow();
        aa.setFillAfter(true);
        aa.setInterpolator(new AccelerateInterpolator());
        as.addAnimation(aa);
        startAnimation(as);
    }

    public void slideDownOut(int time) {
        AnimationSet as = new AnimationSet(true);
        TranslateAnimation ta = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, 1.0f);
        ta.setDuration((long) time);
        ta.startNow();
        ta.setFillAfter(true);
        ta.setInterpolator(new AccelerateInterpolator());
        as.addAnimation(ta);
        AlphaAnimation aa = new AlphaAnimation(1.0f, 0.0f);
        aa.setDuration((long) time);
        aa.startNow();
        aa.setFillAfter(true);
        aa.setInterpolator(new AccelerateInterpolator());
        as.addAnimation(aa);
        startAnimation(as);
    }

    public void slideUpIn(int time) {
        AnimationSet as = new AnimationSet(true);
        TranslateAnimation ta = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
        ta.setDuration((long) time);
        ta.startNow();
        ta.setFillAfter(true);
        ta.setInterpolator(new AccelerateInterpolator());
        as.addAnimation(ta);
        AlphaAnimation aa = new AlphaAnimation(0.0f, 1.0f);
        aa.setDuration((long) time);
        aa.startNow();
        aa.setFillAfter(true);
        aa.setInterpolator(new AccelerateInterpolator());
        as.addAnimation(aa);
        startAnimation(as);
    }

    public void slideUpOut(int time) {
        AnimationSet as = new AnimationSet(true);
        TranslateAnimation ta = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, -1.0f);
        ta.setDuration((long) time);
        ta.startNow();
        ta.setFillAfter(true);
        ta.setInterpolator(new AccelerateInterpolator());
        as.addAnimation(ta);
        AlphaAnimation aa = new AlphaAnimation(1.0f, 0.0f);
        aa.setDuration((long) time);
        aa.startNow();
        aa.setFillAfter(true);
        aa.setInterpolator(new AccelerateInterpolator());
        as.addAnimation(aa);
        startAnimation(as);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        setPressed(false);
        return super.onKeyDown(keyCode, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        setPressed(true);
        return super.onKeyUp(keyCode, event);
    }

    public boolean dispatchTrackballEvent(MotionEvent event) {
        switch (event.getAction()) {
            case 0:
                setPressed(true);
                break;
            case 1:
                setPressed(false);
                break;
        }
        return super.onTrackballEvent(event);
    }
}
