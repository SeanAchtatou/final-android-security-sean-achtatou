package mediba.ad.sdk.android;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

public class MasAdManager {
    private static String AndroidID;
    private static String DeviceID;
    private static String _TAG = "MasAdManager";
    private static String a;
    public static String address;
    private static String adid;
    public static String age;
    private static String apikey;
    private static int b;
    public static String gender;
    private static String params;

    static {
        if (AdUtil.isLogEnable()) {
            Log.i(_TAG, "mediba_ad_sdk_android version 1.0.0.0");
        }
    }

    public static String getADID(Context context) {
        if (adid == null) {
            parseManifest(context);
        }
        if (adid == null && Log.isLoggable(_TAG, 6)) {
            Log.e(_TAG, "ADID not defined");
        }
        return adid;
    }

    public static void setADID(String s) {
        if (!s.startsWith("A")) {
            clientError("ADIDが不正にセットされています。");
        }
        adid = s.substring(1, s.length());
    }

    public static String getAPIKEY(Context context) {
        if (apikey == null) {
            parseManifest(context);
        }
        if (apikey == null && Log.isLoggable(_TAG, 6)) {
            Log.e(_TAG, "APKEY not defined");
        }
        return apikey;
    }

    public static void setAPIKEY(String s) {
        apikey = s;
    }

    private static void parseManifest(Context context) {
        try {
            PackageManager packagemanager = context.getPackageManager();
            ApplicationInfo applicationinfo = packagemanager.getApplicationInfo(context.getPackageName(), 128);
            if (applicationinfo != null) {
                if (MasAdView.getTestMode()) {
                    setAPIKEY("z8YiPhJxo0");
                    setADID("A30100520133");
                } else if (applicationinfo.metaData != null) {
                    String s = applicationinfo.metaData.getString("apkey");
                    if (AdUtil.isLogEnable()) {
                        Log.d(_TAG, "Manifest APKEY:" + s);
                    }
                    if (apikey == null && s != null) {
                        setAPIKEY(s);
                    }
                    String s2 = applicationinfo.metaData.getString("aid");
                    if (AdUtil.isLogEnable()) {
                        Log.d(_TAG, "Manifest AID:" + s2);
                    }
                    if (adid == null && s2 != null) {
                        setADID(s2);
                    }
                }
                a = applicationinfo.packageName;
                if (AdUtil.isLogEnable()) {
                    Log.i(_TAG, "Application's package name is " + a);
                }
            }
            PackageInfo packageinfo = packagemanager.getPackageInfo(context.getPackageName(), 0);
            if (packageinfo != null) {
                b = packageinfo.versionCode;
                if (AdUtil.isLogEnable()) {
                    Log.i(_TAG, "Application's version number is " + b);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(_TAG, "Exception" + params);
        }
    }

    public static String getAndroidId(Context context) {
        if (AndroidID == null) {
            AndroidID = Settings.Secure.getString(context.getContentResolver(), "android_id");
        }
        if (AdUtil.isLogEnable()) {
            Log.d(_TAG, "AndroidID: " + AndroidID);
        }
        return AndroidID;
    }

    public static String getDeviceId(Context context, boolean permission_RPS) {
        if (DeviceID == null && permission_RPS) {
            DeviceID = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        }
        if (AdUtil.isLogEnable()) {
            Log.d(_TAG, "deviceId: " + DeviceID);
        }
        return DeviceID;
    }

    public static int getScreenWidth(Context context) {
        Display window = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        if (window != null) {
            return window.getWidth();
        }
        return 0;
    }

    public static int getScreenHeight(Context context) {
        Display window = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
        if (window != null) {
            return window.getHeight();
        }
        return 0;
    }

    public static void clientError(String string) {
        Log.e(_TAG, string);
    }

    public static String getIpAddress(Context context) {
        try {
            Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
            while (en.hasMoreElements()) {
                Enumeration<InetAddress> enumIpAddr = en.nextElement().getInetAddresses();
                while (true) {
                    if (enumIpAddr.hasMoreElements()) {
                        InetAddress inetAddress = enumIpAddr.nextElement();
                        if (!inetAddress.isLoopbackAddress()) {
                            return inetAddress.getHostAddress().toString();
                        }
                    }
                }
            }
        } catch (SocketException e) {
            SocketException ex = e;
            ex.printStackTrace();
            Log.e(_TAG, ex.getMessage());
        } catch (Exception e2) {
            Exception ex2 = e2;
            ex2.printStackTrace();
            Log.e(_TAG, ex2.getMessage());
        }
        return null;
    }

    /* JADX INFO: Multiple debug info for r0v3 java.lang.String: [D('telManager' android.telephony.TelephonyManager), D('n_operator' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r3v1 java.lang.String: [D('Lang' java.lang.String), D('n_operator' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r0v9 java.lang.String: [D('isWifi' boolean), D('Country' java.lang.String)] */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x00bb  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00ea  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getParams(android.content.Context r10, boolean r11) {
        /*
            java.lang.String r0 = "phone"
            java.lang.Object r0 = r10.getSystemService(r0)
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0
            java.lang.String r0 = r0.getNetworkOperatorName()
            java.lang.String r1 = " "
            java.lang.String r2 = ""
            java.lang.String r3 = r0.replace(r1, r2)
            java.lang.String r0 = "connectivity"
            java.lang.Object r0 = r10.getSystemService(r0)
            android.net.ConnectivityManager r0 = (android.net.ConnectivityManager) r0
            r1 = 0
            r2 = 0
            if (r11 == 0) goto L_0x00e6
            r11 = 1
            android.net.NetworkInfo r11 = r0.getNetworkInfo(r11)     // Catch:{ Exception -> 0x00d4 }
            boolean r11 = r11.isConnectedOrConnecting()     // Catch:{ Exception -> 0x00d4 }
            r2 = r11
        L_0x002a:
            r11 = 0
            android.net.NetworkInfo r11 = r0.getNetworkInfo(r11)     // Catch:{ Exception -> 0x00de }
            boolean r11 = r11.isConnectedOrConnecting()     // Catch:{ Exception -> 0x00de }
            r0 = r2
        L_0x0034:
            java.lang.String r7 = getOsVer()
            java.lang.String r1 = getDisplay(r10)
            android.content.res.Resources r2 = r10.getResources()
            android.content.res.Configuration r2 = r2.getConfiguration()
            int r2 = r2.orientation
            java.lang.String r6 = java.lang.Integer.toString(r2)
            r5 = r3
            if (r11 == 0) goto L_0x00ea
            java.lang.String r11 = "1"
            r4 = r11
        L_0x0050:
            android.content.res.Resources r11 = r10.getResources()
            android.content.res.Configuration r11 = r11.getConfiguration()
            java.util.Locale r11 = r11.locale
            java.lang.String r3 = r11.getLanguage()
            android.content.res.Resources r10 = r10.getResources()
            android.content.res.Configuration r10 = r10.getConfiguration()
            java.util.Locale r10 = r10.locale
            java.lang.String r0 = r10.getCountry()
            java.lang.String r2 = getGender()
            java.lang.String r11 = getAge()
            java.lang.String r10 = getAddress()
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "nv01"
            extend_request_param(r8, r9, r7)
            java.lang.String r7 = "nv02"
            extend_request_param(r8, r7, r1)
            java.lang.String r1 = "nv03"
            extend_request_param(r8, r1, r6)
            java.lang.String r1 = "nv04"
            extend_request_param(r8, r1, r5)
            java.lang.String r1 = "nv05"
            extend_request_param(r8, r1, r4)
            java.lang.String r1 = "nv06"
            extend_request_param(r8, r1, r3)
            java.lang.String r1 = "nv07"
            extend_request_param(r8, r1, r0)
            java.lang.String r0 = "K001"
            extend_request_param(r8, r0, r2)
            java.lang.String r0 = "K003"
            extend_request_param(r8, r0, r11)
            java.lang.String r11 = "K006"
            extend_request_param(r8, r11, r10)
            java.lang.String r10 = r8.toString()
            mediba.ad.sdk.android.MasAdManager.params = r10
            boolean r10 = mediba.ad.sdk.android.AdUtil.isLogEnable()
            if (r10 == 0) goto L_0x00d1
            java.lang.String r10 = mediba.ad.sdk.android.MasAdManager._TAG
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            java.lang.String r0 = "extend_request_param"
            r11.<init>(r0)
            java.lang.String r0 = mediba.ad.sdk.android.MasAdManager.params
            java.lang.StringBuilder r11 = r11.append(r0)
            java.lang.String r11 = r11.toString()
            android.util.Log.d(r10, r11)
        L_0x00d1:
            java.lang.String r10 = mediba.ad.sdk.android.MasAdManager.params
            return r10
        L_0x00d4:
            r11 = move-exception
            java.lang.String r11 = mediba.ad.sdk.android.MasAdManager._TAG
            java.lang.String r4 = "no supported device.isWifi"
            android.util.Log.w(r11, r4)
            goto L_0x002a
        L_0x00de:
            r11 = move-exception
            java.lang.String r11 = mediba.ad.sdk.android.MasAdManager._TAG
            java.lang.String r0 = "no supported device.isMobile"
            android.util.Log.w(r11, r0)
        L_0x00e6:
            r0 = r2
            r11 = r1
            goto L_0x0034
        L_0x00ea:
            if (r0 == 0) goto L_0x00f1
            java.lang.String r11 = "2"
            r4 = r11
            goto L_0x0050
        L_0x00f1:
            r11 = 0
            r4 = r11
            goto L_0x0050
        */
        throw new UnsupportedOperationException("Method not decompiled: mediba.ad.sdk.android.MasAdManager.getParams(android.content.Context, boolean):java.lang.String");
    }

    private static void extend_request_param(StringBuilder stringbuilder, String string, String string2) {
        if (string2 != null && string2.length() > 0) {
            try {
                stringbuilder.append("&").append(string).append("=").append(string2);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static String getOsVer() {
        String OsVer = Build.VERSION.RELEASE;
        String OsVerXX = "1.6";
        if (OsVer == null || OsVer.length() < 3) {
            Log.e(_TAG, "OsVer is null. forced to set Version = 1.6");
        } else {
            OsVerXX = OsVer.substring(0, 3);
        }
        if (AdUtil.isLogEnable()) {
            Log.d(_TAG, OsVerXX);
        }
        return OsVerXX;
    }

    public static String getDisplay(Context context) {
        int ori = context.getResources().getConfiguration().orientation;
        int x = ori == 1 ? getScreenWidth(context) : getScreenHeight(context);
        int y = ori == 1 ? getScreenHeight(context) : getScreenWidth(context);
        int display = 0;
        switch (x) {
            case 240:
                if (y != 320) {
                    if (y != 400) {
                        display = 0;
                        break;
                    } else {
                        display = 2;
                        break;
                    }
                } else {
                    display = 1;
                    break;
                }
            case 320:
                if (y != 480) {
                    display = 0;
                    break;
                } else {
                    display = 3;
                    break;
                }
            case 360:
                if (y != 640) {
                    display = 0;
                    break;
                } else {
                    display = 4;
                    break;
                }
            case 480:
                if (y != 800) {
                    if (y != 854) {
                        if (y != 864) {
                            if (y != 960) {
                                if (y != 1024) {
                                    display = 0;
                                    break;
                                } else {
                                    display = 9;
                                    break;
                                }
                            } else {
                                display = 8;
                                break;
                            }
                        } else {
                            display = 7;
                            break;
                        }
                    } else {
                        display = 6;
                        break;
                    }
                } else {
                    display = 5;
                    break;
                }
        }
        return Integer.toString(display);
    }

    public static String getGender() {
        return gender;
    }

    public static String getAge() {
        return age;
    }

    public static String getAddress() {
        return address;
    }
}
