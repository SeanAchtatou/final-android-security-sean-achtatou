package mediba.ad.sdk.android;

import android.content.Context;
import android.util.Log;
import java.lang.ref.WeakReference;
import org.json.JSONException;

public class AdRequestThread extends Thread {
    private static final String _TAG = "AdRequestThread";
    private WeakReference adv;

    public AdRequestThread(MasAdView adView) {
        this.adv = new WeakReference(adView);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: mediba.ad.sdk.android.MasAdView.a(mediba.ad.sdk.android.MasAdView, boolean):boolean
     arg types: [mediba.ad.sdk.android.MasAdView, int]
     candidates:
      mediba.ad.sdk.android.MasAdView.a(mediba.ad.sdk.android.MasAdView, mediba.ad.sdk.android.AdContainer):void
      mediba.ad.sdk.android.MasAdView.a(mediba.ad.sdk.android.MasAdView, mediba.ad.sdk.android.AdProxy):void
      mediba.ad.sdk.android.MasAdView.a(mediba.ad.sdk.android.AdProxy, mediba.ad.sdk.android.AdContainer):void
      mediba.ad.sdk.android.MasAdView.a(mediba.ad.sdk.android.MasAdView, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: mediba.ad.sdk.android.MasAdView.b(mediba.ad.sdk.android.MasAdView, boolean):void
     arg types: [mediba.ad.sdk.android.MasAdView, int]
     candidates:
      mediba.ad.sdk.android.MasAdView.b(mediba.ad.sdk.android.MasAdView, mediba.ad.sdk.android.AdContainer):void
      mediba.ad.sdk.android.MasAdView.b(mediba.ad.sdk.android.MasAdView, boolean):void */
    public void run() {
        if (AdUtil.isLogEnable()) {
            Log.d(_TAG, "Starting AdRequestThread");
        }
        MasAdView adview = (MasAdView) this.adv.get();
        Context context = adview.getContext();
        AdContainer adcontainer = new AdContainer(null, context, adview);
        int i = (int) (((float) adview.getMeasuredWidth()) / AdContainer.getDensity());
        if (((float) i) <= 310.0f) {
            Log.w(_TAG, "表示領域の横幅が狭すぎます。");
        }
        try {
            if (AdRequestor.request(MasAdView.d(adview), context, adview.getPrimaryTextColor(), adview.getSecondaryTextColor(), adview.getBackgroundColor(), adcontainer, i) == null) {
                MasAdView.c(adview);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            AdUtil.onFailedToReceiveAd(adview);
        } catch (Exception e2) {
            e2.printStackTrace();
            AdUtil.onFailedToReceiveAd(adview);
        }
        MasAdView.a(adview, false);
        MasAdView.b(adview, true);
    }
}
