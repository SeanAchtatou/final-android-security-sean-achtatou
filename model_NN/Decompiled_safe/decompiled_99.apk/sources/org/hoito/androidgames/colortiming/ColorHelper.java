package org.hoito.androidgames.colortiming;

import android.graphics.Color;
import java.util.Random;

public class ColorHelper {
    public static final int COLORBLUE = 1;
    public static final int COLORGREEN = 3;
    public static final int COLORORANGE = 6;
    public static final int COLORPURPLE = 4;
    public static final int COLORRED = 2;
    public static final int COLORYELLOW = 5;

    public static int makeColor(int currentcolor) {
        if (currentcolor == 1) {
            return Color.rgb(0, 0, 255);
        }
        if (currentcolor == 2) {
            return Color.rgb(255, 0, 0);
        }
        if (currentcolor == 3) {
            return Color.rgb(0, 204, 0);
        }
        if (currentcolor == 4) {
            return Color.rgb(204, 0, 204);
        }
        if (currentcolor == 5) {
            return Color.rgb(255, 255, 0);
        }
        if (currentcolor == 6) {
            return Color.rgb(255, 153, 0);
        }
        return 0;
    }

    public static int generateRandomColor() {
        int somecolor = new Random().nextInt(6) + 1;
        if (somecolor == 7) {
            return 6;
        }
        return somecolor;
    }

    public static String generateColorName() {
        return new String[]{"blue", "red", "green", "purple", "yellow", "orange"}[new Random().nextInt(6)];
    }
}
