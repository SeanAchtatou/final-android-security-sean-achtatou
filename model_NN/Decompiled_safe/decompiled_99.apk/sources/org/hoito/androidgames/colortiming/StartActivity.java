package org.hoito.androidgames.colortiming;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class StartActivity extends Activity {
    private TextView gamemodeTextView = null;
    View.OnClickListener startbuttonClicklistener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setClass(StartActivity.this, GameActivity.class);
            StartActivity.this.startActivity(intent);
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.start);
        LinearLayout mainlayout = (LinearLayout) findViewById(R.id.startlayout);
        ((Button) mainlayout.findViewById(R.id.startbutton)).setOnClickListener(this.startbuttonClicklistener);
        this.gamemodeTextView = (TextView) mainlayout.findViewById(R.id.currentgamemode);
        this.gamemodeTextView.setText("game mode: " + PreferencesHelper.getGamemodeArrayEntry(PreferencesHelper.getGamemodePrefsEntry(getSharedPreferences("applicationwideprefs", 0).getAll()), getResources()));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.rootmenu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuitem_preferences:
                Intent intent = new Intent();
                intent.setClass(this, PreferencesActivity.class);
                startActivityForResult(intent, 0);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0 && resultCode == 1) {
            this.gamemodeTextView.setText("game mode: " + PreferencesHelper.getGamemodeArrayEntry(data.getStringExtra("gamemodestr"), getResources()));
        }
    }
}
