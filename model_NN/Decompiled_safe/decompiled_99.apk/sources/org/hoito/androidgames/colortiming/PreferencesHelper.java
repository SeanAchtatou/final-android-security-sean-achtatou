package org.hoito.androidgames.colortiming;

import android.content.res.Resources;
import java.util.Map;

public class PreferencesHelper {
    public static String getGamemodePrefsEntry(Map<String, ?> prefsmap) {
        String entryvalueStr = null;
        if (prefsmap.size() > 0) {
            for (Map.Entry pref : prefsmap.entrySet()) {
                if (pref.getKey().equals("gameprefs_gamemode") && pref.getValue().toString().length() > 0 && !pref.getValue().toString().equalsIgnoreCase("noselection")) {
                    entryvalueStr = pref.getValue().toString();
                }
            }
        }
        return entryvalueStr;
    }

    public static String getGamemodeArrayEntry(String entryvalue, Resources resources) {
        String entryStr = null;
        String[] entryvalues = resources.getStringArray(R.array.prefs_entries_values);
        String[] entries = resources.getStringArray(R.array.prefs_entries);
        if (entryvalue == null) {
            return entries[0];
        }
        for (int i = 0; i < entryvalues.length; i++) {
            if (entryvalue.equals(entryvalues[i])) {
                entryStr = entries[i];
            }
        }
        return entryStr;
    }
}
