package org.hoito.androidgames.colortiming;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.PreferenceActivity;

public class PreferencesActivity extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        setSummaries();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        SharedPreferences.Editor prefseditor = getSharedPreferences("applicationwideprefs", 0).edit();
        if (key.equalsIgnoreCase("gameprefs_gamemode")) {
            ListPreference listPreference = (ListPreference) findPreference("gameprefs_gamemode");
            if (listPreference.getValue() == null || listPreference.getValue().length() <= 0 || listPreference.getValue().equalsIgnoreCase("noselection")) {
                listPreference.setSummary("no selection");
            } else {
                listPreference.setSummary("Current choice: " + ((Object) listPreference.getEntry()));
            }
            prefseditor.putString("gameprefs_gamemode", listPreference.getValue());
            prefseditor.commit();
            Intent in = getIntent();
            in.putExtra("gamemodestr", listPreference.getValue());
            setResult(1, in);
        }
    }

    private void setSummaries() {
        SharedPreferences prefs = getSharedPreferences("applicationwideprefs", 0);
        ListPreference gamemodepreference = (ListPreference) findPreference("gameprefs_gamemode");
        String savedStr = prefs.getString("gameprefs_gamemode", "");
        if (savedStr.length() <= 0 || savedStr.equalsIgnoreCase("noselection")) {
            gamemodepreference.setSummary("no selection");
        } else {
            gamemodepreference.setSummary("Current choice: " + PreferencesHelper.getGamemodeArrayEntry(savedStr, getResources()));
        }
    }
}
