package org.hoito.androidgames.colortiming;

import android.content.res.AssetManager;
import android.graphics.Typeface;

public class SituationHolder {
    private int correctones = 0;
    private int currentcolor = 0;
    private int currentcolorcode = 0;
    private int currentfontsize = 10;
    private Typeface[] fonts = new Typeface[5];
    private boolean gameactive = false;
    private boolean increasingCounter = true;
    private int misses = 0;
    private boolean prepareforanewcolor = false;

    public SituationHolder(AssetManager assetManager) {
        this.fonts[0] = Typeface.createFromAsset(assetManager, "fonts/badabum.ttf");
        this.fonts[1] = Typeface.createFromAsset(assetManager, "fonts/Chantelli_Antiqua.ttf");
        this.fonts[2] = Typeface.createFromAsset(assetManager, "fonts/CranberryBlues.ttf");
        this.fonts[3] = Typeface.createFromAsset(assetManager, "fonts/Sliced_AB.ttf");
        this.fonts[4] = Typeface.createFromAsset(assetManager, "fonts/YanoneKaffeesatz_Regular.ttf");
    }

    public boolean isGameactive() {
        return this.gameactive;
    }

    public void setGameactive(boolean gameactive2) {
        this.gameactive = gameactive2;
    }

    public boolean isPrepareforanewcolor() {
        return this.prepareforanewcolor;
    }

    public void setPrepareforanewcolor(boolean prepareforanewcolor2) {
        this.prepareforanewcolor = prepareforanewcolor2;
    }

    public int getCurrentcolor() {
        return this.currentcolor;
    }

    public void setCurrentcolor(int currentcolor2) {
        this.currentcolor = currentcolor2;
    }

    public int getCurrentcolorcode() {
        return this.currentcolorcode;
    }

    public void setCurrentcolorcode(int currentcolorcode2) {
        this.currentcolorcode = currentcolorcode2;
    }

    public boolean isIncreasingCounter() {
        return this.increasingCounter;
    }

    public void setIncreasingCounter(boolean increasingCounter2) {
        this.increasingCounter = increasingCounter2;
    }

    public int getCorrectones() {
        return this.correctones;
    }

    public void setCorrectones(int correctones2) {
        this.correctones = correctones2;
    }

    public int getMisses() {
        return this.misses;
    }

    public void setMisses(int misses2) {
        this.misses = misses2;
    }

    public int getCurrentfontsize() {
        return this.currentfontsize;
    }

    public void setCurrentfontsize(int currentfontsize2) {
        this.currentfontsize = currentfontsize2;
    }

    public Typeface[] getFonts() {
        return this.fonts;
    }

    public void setFonts(Typeface[] fonts2) {
        this.fonts = fonts2;
    }
}
