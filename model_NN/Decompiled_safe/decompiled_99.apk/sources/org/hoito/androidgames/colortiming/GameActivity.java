package org.hoito.androidgames.colortiming;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.Random;

public class GameActivity extends Activity {
    /* access modifiers changed from: private */
    public int counter = 0;
    private TextView currentscoreTextView = null;
    /* access modifiers changed from: private */
    public String gamemode;
    /* access modifiers changed from: private */
    public Handler handler = null;
    SituationHolder sh = null;
    View.OnClickListener somecolorClicklistener = new View.OnClickListener() {
        public void onClick(View v) {
            if (GameActivity.this.sh.isGameactive()) {
                if (!GameActivity.this.sh.isPrepareforanewcolor()) {
                    String colortag = v.getTag().toString();
                    if (colortag.equals("blue") && GameActivity.this.sh.getCurrentcolor() == 1) {
                        GameActivity.this.sh.setCorrectones(GameActivity.this.sh.getCorrectones() + 1);
                    } else if (colortag.equals("red") && GameActivity.this.sh.getCurrentcolor() == 2) {
                        GameActivity.this.sh.setCorrectones(GameActivity.this.sh.getCorrectones() + 1);
                    } else if (colortag.equals("green") && GameActivity.this.sh.getCurrentcolor() == 3) {
                        GameActivity.this.sh.setCorrectones(GameActivity.this.sh.getCorrectones() + 1);
                    } else if (colortag.equals("purple") && GameActivity.this.sh.getCurrentcolor() == 4) {
                        GameActivity.this.sh.setCorrectones(GameActivity.this.sh.getCorrectones() + 1);
                    } else if (colortag.equals("yellow") && GameActivity.this.sh.getCurrentcolor() == 5) {
                        GameActivity.this.sh.setCorrectones(GameActivity.this.sh.getCorrectones() + 1);
                    } else if (!colortag.equals("orange") || GameActivity.this.sh.getCurrentcolor() != 6) {
                        GameActivity.this.sh.setMisses(GameActivity.this.sh.getMisses() + 1);
                    } else {
                        GameActivity.this.sh.setCorrectones(GameActivity.this.sh.getCorrectones() + 1);
                    }
                }
                GameActivity.this.updateScores("current scores");
                if (GameActivity.this.sh.getMisses() > 5) {
                    GameActivity.this.handler.removeCallbacks(GameActivity.this.somerunnable);
                    GameActivity.this.sh.setGameactive(false);
                    GameActivity.this.updateScores("Final scores");
                    return;
                }
                GameActivity.this.sh.setPrepareforanewcolor(true);
            }
        }
    };
    /* access modifiers changed from: private */
    public Runnable somerunnable = new Runnable() {
        public void run() {
            if (GameActivity.this.sh.getCurrentcolor() == 0) {
                GameActivity.this.sh.setCurrentcolor(ColorHelper.generateRandomColor());
                GameActivity.this.sh.setCurrentcolorcode(ColorHelper.makeColor(GameActivity.this.sh.getCurrentcolor()));
                GameActivity.this.counter = 0;
                GameActivity.this.targetcolorTextView.setText(ColorHelper.generateColorName());
            }
            if (GameActivity.this.sh.isIncreasingCounter()) {
                GameActivity gameActivity = GameActivity.this;
                gameActivity.counter = gameActivity.counter + 10;
                if (GameActivity.this.counter > 255) {
                    GameActivity.this.counter = 255;
                    GameActivity.this.sh.setIncreasingCounter(false);
                }
            } else {
                GameActivity gameActivity2 = GameActivity.this;
                gameActivity2.counter = gameActivity2.counter - 10;
                if (GameActivity.this.counter <= 0) {
                    if (!GameActivity.this.sh.isPrepareforanewcolor()) {
                        GameActivity.this.sh.setMisses(GameActivity.this.sh.getMisses() + 1);
                        GameActivity.this.updateScores("current scores");
                    }
                    if (GameActivity.this.sh.getMisses() > 5) {
                        GameActivity.this.handler.removeCallbacks(GameActivity.this.somerunnable);
                        GameActivity.this.sh.setGameactive(false);
                        GameActivity.this.updateScores("Final scores");
                    } else {
                        GameActivity.this.sh.setCurrentcolor(ColorHelper.generateRandomColor());
                        GameActivity.this.sh.setCurrentcolorcode(ColorHelper.makeColor(GameActivity.this.sh.getCurrentcolor()));
                        GameActivity.this.counter = 0;
                        GameActivity.this.targetcolorTextView.setText(ColorHelper.generateColorName());
                        if (GameActivity.this.gamemode.equals("second")) {
                            GameActivity.this.targetcolorTextView.setTypeface(GameActivity.this.chooseRandomFont());
                        } else if (GameActivity.this.gamemode.equals("third")) {
                            GameActivity.this.targetcolorTextView.setTextSize((float) GameActivity.this.generateRandomFontsize());
                        }
                    }
                    GameActivity.this.counter = 0;
                    GameActivity.this.sh.setIncreasingCounter(true);
                    GameActivity.this.sh.setPrepareforanewcolor(false);
                }
            }
            GameActivity.this.temporarycolorcode = Color.rgb(Math.round((Float.parseFloat(String.valueOf(Color.red(GameActivity.this.sh.getCurrentcolorcode()))) / 255.0f) * ((float) GameActivity.this.counter)), Math.round((Float.parseFloat(String.valueOf(Color.green(GameActivity.this.sh.getCurrentcolorcode()))) / 255.0f) * ((float) GameActivity.this.counter)), Math.round((Float.parseFloat(String.valueOf(Color.blue(GameActivity.this.sh.getCurrentcolorcode()))) / 255.0f) * ((float) GameActivity.this.counter)));
            GameActivity.this.targetcolorTextView.setTextColor(GameActivity.this.temporarycolorcode);
            if (!GameActivity.this.sh.isGameactive()) {
                return;
            }
            if (GameActivity.this.counter != 0 || !GameActivity.this.gamemode.equals("forth")) {
                GameActivity.this.handler.postDelayed(GameActivity.this.somerunnable, 20);
            } else {
                GameActivity.this.handler.postDelayed(GameActivity.this.somerunnable, (long) GameActivity.this.generateRandomDelay());
            }
        }
    };
    /* access modifiers changed from: private */
    public TextView targetcolorTextView = null;
    /* access modifiers changed from: private */
    public int temporarycolorcode = 0;
    private Thread thread = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.game);
        LinearLayout mainlayout = (LinearLayout) findViewById(R.id.gamelayout);
        this.targetcolorTextView = (TextView) mainlayout.findViewById(R.id.boxie1);
        this.currentscoreTextView = (TextView) mainlayout.findViewById(R.id.currentscore);
        LinearLayout bluebox = (LinearLayout) findViewById(R.id.colorblue);
        LinearLayout redbox = (LinearLayout) findViewById(R.id.colorred);
        LinearLayout greenbox = (LinearLayout) findViewById(R.id.colorgreen);
        LinearLayout purplebox = (LinearLayout) findViewById(R.id.colorpurple);
        LinearLayout yellowbox = (LinearLayout) findViewById(R.id.coloryellow);
        LinearLayout orangebox = (LinearLayout) findViewById(R.id.colororange);
        bluebox.setOnClickListener(this.somecolorClicklistener);
        redbox.setOnClickListener(this.somecolorClicklistener);
        greenbox.setOnClickListener(this.somecolorClicklistener);
        purplebox.setOnClickListener(this.somecolorClicklistener);
        yellowbox.setOnClickListener(this.somecolorClicklistener);
        orangebox.setOnClickListener(this.somecolorClicklistener);
        this.gamemode = PreferencesHelper.getGamemodePrefsEntry(getSharedPreferences("applicationwideprefs", 0).getAll());
        if (this.gamemode == null) {
            this.gamemode = "first";
        }
        this.sh = new SituationHolder(getAssets());
        if (this.gamemode.equals("fifth")) {
            bluebox.setBackgroundDrawable(getResources().getDrawable(R.drawable.bluebox));
            redbox.setBackgroundDrawable(getResources().getDrawable(R.drawable.redbox));
            greenbox.setBackgroundDrawable(getResources().getDrawable(R.drawable.greenbox));
            purplebox.setBackgroundDrawable(getResources().getDrawable(R.drawable.purplebox));
            yellowbox.setBackgroundDrawable(getResources().getDrawable(R.drawable.yellowbox));
            orangebox.setBackgroundDrawable(getResources().getDrawable(R.drawable.orangebox));
        }
        startGame();
    }

    /* access modifiers changed from: private */
    public void updateScores(String scoretext) {
        this.currentscoreTextView.setText(String.valueOf(scoretext) + ": " + this.sh.getCorrectones() + " correct, " + this.sh.getMisses() + " wrong");
    }

    public int generateRandomDelay() {
        return new Random().nextInt(1000) + 20;
    }

    public int generateRandomFontsize() {
        return new Random().nextInt(30) + 8;
    }

    public Typeface chooseRandomFont() {
        int number = new Random().nextInt(5);
        if (number == 5) {
            number = 4;
        }
        return this.sh.getFonts()[number];
    }

    private void startGame() {
        this.sh.setCorrectones(0);
        this.sh.setMisses(0);
        this.sh.setCurrentcolor(0);
        this.sh.setGameactive(true);
        this.counter = 0;
        updateScores("current scores");
        if (this.gamemode.equals("second")) {
            this.targetcolorTextView.setTypeface(chooseRandomFont());
        }
        if (this.thread == null) {
            this.handler = new Handler();
            this.thread = new Thread() {
                public void run() {
                    GameActivity.this.handler.postDelayed(GameActivity.this.somerunnable, 10);
                }
            };
            this.thread.start();
            return;
        }
        this.handler.removeCallbacks(this.somerunnable);
        this.handler.postDelayed(this.somerunnable, 10);
    }
}
