package org.hoito.androidgames.colortiming;

public final class R {

    public static final class array {
        public static final int prefs_entries = 2131034112;
        public static final int prefs_entries_values = 2131034113;
    }

    public static final class attr {
    }

    public static final class color {
        public static final int blue = 2131099648;
        public static final int green = 2131099650;
        public static final int orange = 2131099653;
        public static final int purple = 2131099651;
        public static final int red = 2131099649;
        public static final int yellow = 2131099652;
    }

    public static final class drawable {
        public static final int bluebox = 2130837504;
        public static final int greenbox = 2130837505;
        public static final int icon = 2130837506;
        public static final int orangebox = 2130837507;
        public static final int purplebox = 2130837508;
        public static final int redbox = 2130837509;
        public static final int yellowbox = 2130837510;
    }

    public static final class id {
        public static final int boxie1 = 2131296257;
        public static final int colorblue = 2131296259;
        public static final int colorgreen = 2131296260;
        public static final int colororange = 2131296261;
        public static final int colorpurple = 2131296263;
        public static final int colorred = 2131296264;
        public static final int colorrows1 = 2131296258;
        public static final int colorrows2 = 2131296262;
        public static final int coloryellow = 2131296265;
        public static final int currentgamemode = 2131296271;
        public static final int currentscore = 2131296266;
        public static final int gamelayout = 2131296256;
        public static final int menuitem_preferences = 2131296274;
        public static final int startbutton = 2131296270;
        public static final int startlayout = 2131296267;
        public static final int textView1 = 2131296272;
        public static final int textView2 = 2131296268;
        public static final int textView3 = 2131296269;
        public static final int textView4 = 2131296273;
    }

    public static final class layout {
        public static final int game = 2130903040;
        public static final int start = 2130903041;
    }

    public static final class menu {
        public static final int rootmenu = 2131230720;
    }

    public static final class string {
        public static final int app_name = 2131165184;
        public static final int app_version = 2131165185;
        public static final int colorblue = 2131165186;
        public static final int colorgreen = 2131165188;
        public static final int colororange = 2131165191;
        public static final int colorpurple = 2131165189;
        public static final int colorred = 2131165187;
        public static final int coloryellow = 2131165190;
        public static final int emptyvalue = 2131165195;
        public static final int gameinfo = 2131165194;
        public static final int prefs_gamemode = 2131165192;
        public static final int prefs_settings = 2131165193;
    }

    public static final class xml {
        public static final int preferences = 2130968576;
    }
}
