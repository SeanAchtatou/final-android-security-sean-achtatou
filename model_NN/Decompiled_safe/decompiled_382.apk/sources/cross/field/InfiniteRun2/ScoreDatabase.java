package cross.field.InfiniteRun2;

import android.content.Context;
import cross.field.InfiniteRun2.Table;

public class ScoreDatabase {
    private DatabaseAccess db;
    private Graphics g;

    public ScoreDatabase(Context context) {
        this.db = new DatabaseAccess(context, Table.DATABASE_NAME, null, 1);
        DatabaseAccess.engine = this.db;
    }

    public void data() {
        String[] columns = {Table.Ranking.COL1};
        this.db.setInsertData(columns, new String[]{"131475420", "21477603"});
        this.db.insert(Table.Ranking.TABLE_NAME);
        this.db.update(Table.DATABASE_NAME, null);
        try {
            String[][] result = this.db.select(Table.Ranking.TABLE_NAME, columns, null, null, null, null, null, null);
            for (int i = 0; i < result.length; i++) {
                for (String drawString : result[i]) {
                    this.g.drawString(drawString, 300, 300, 50, -16777216);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insert(String[] data) {
        this.db.setInsertData(new String[]{Table.Ranking.COL1}, data);
        this.db.insert(Table.Ranking.TABLE_NAME);
    }

    public void delete(String table, String whereClause) {
        this.db.delete(table, whereClause);
    }

    public void reset() {
        this.db.setInsertData(new String[]{Table.Ranking.COL1}, new String[]{"", ""});
        this.db.update(Table.DATABASE_NAME, "*");
    }

    public String[][] select() {
        String[][] result = null;
        try {
            return this.db.select(Table.Ranking.TABLE_NAME, new String[]{Table.Ranking.COL1}, null, null, null, null, "SCORE desc", "10");
        } catch (Exception e) {
            e.printStackTrace();
            return result;
        }
    }
}
