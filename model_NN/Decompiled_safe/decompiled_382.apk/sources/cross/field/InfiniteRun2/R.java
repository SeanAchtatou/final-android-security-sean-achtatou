package cross.field.InfiniteRun2;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int black = 2130968576;
    }

    public static final class drawable {
        public static final int bg_main = 2130837504;
        public static final int bg_result = 2130837505;
        public static final int bg_score = 2130837506;
        public static final int bg_title = 2130837507;
        public static final int block = 2130837508;
        public static final int chara00 = 2130837509;
        public static final int chara01 = 2130837510;
        public static final int chara10 = 2130837511;
        public static final int chara11 = 2130837512;
        public static final int chara20 = 2130837513;
        public static final int chara21 = 2130837514;
        public static final int chara30 = 2130837515;
        public static final int chara31 = 2130837516;
        public static final int icon = 2130837517;
        public static final int logo_0 = 2130837518;
        public static final int logo_1 = 2130837519;
        public static final int logo_2 = 2130837520;
        public static final int logo_3 = 2130837521;
        public static final int logo_4 = 2130837522;
        public static final int logo_5 = 2130837523;
        public static final int logo_6 = 2130837524;
        public static final int logo_7 = 2130837525;
        public static final int logo_8 = 2130837526;
        public static final int logo_9 = 2130837527;
    }

    public static final class id {
        public static final int adView = 2131099653;
        public static final int bg = 2131099648;
        public static final int linearLayout1 = 2131099649;
        public static final int linearLayout2 = 2131099652;
        public static final int menu = 2131099654;
        public static final int more = 2131099655;
        public static final int over = 2131099650;
        public static final int rank_1 = 2131099657;
        public static final int rank_10 = 2131099666;
        public static final int rank_2 = 2131099658;
        public static final int rank_3 = 2131099659;
        public static final int rank_4 = 2131099660;
        public static final int rank_5 = 2131099661;
        public static final int rank_6 = 2131099662;
        public static final int rank_7 = 2131099663;
        public static final int rank_8 = 2131099664;
        public static final int rank_9 = 2131099665;
        public static final int result = 2131099651;
        public static final int score = 2131099656;
        public static final int start = 2131099668;
        public static final int title = 2131099667;
    }

    public static final class layout {
        public static final int result = 2130903040;
        public static final int score = 2130903041;
        public static final int title = 2130903042;
    }

    public static final class string {
        public static final int app_name = 2131034113;
        public static final int hello = 2131034112;
    }
}
