package cross.field.InfiniteRun2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import java.lang.reflect.Array;

public abstract class SQLiteEngine extends SQLiteOpenHelper {
    protected static SQLiteEngine engine;
    private SQLiteDatabase db;
    private ContentValues values;

    public SQLiteEngine(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public long insert(String table) {
        try {
            this.db = engine.getWritableDatabase();
            this.db.beginTransaction();
            long rowID = 0;
            try {
                rowID = this.db.insert(table, null, this.values);
                this.db.setTransactionSuccessful();
            } catch (Exception e) {
            } finally {
                this.db.endTransaction();
                this.db.close();
                this.values = null;
            }
            return rowID;
        } catch (SQLiteException e2) {
            return -1;
        }
    }

    public void delete(String table, String whereClause) {
        try {
            this.db = engine.getWritableDatabase();
            this.db.beginTransaction();
            try {
                this.db.delete(table, whereClause, null);
                this.db.setTransactionSuccessful();
            } catch (Exception e) {
            } finally {
                this.db.endTransaction();
                this.db.close();
                this.values = null;
            }
        } catch (SQLiteException e2) {
        }
    }

    public void update(String table, String whereClause) {
        try {
            this.db = engine.getWritableDatabase();
            this.db.beginTransaction();
            try {
                this.db.update(table, this.values, whereClause, null);
                this.db.setTransactionSuccessful();
            } catch (Exception e) {
            } finally {
                this.db.endTransaction();
                this.db.close();
                this.values = null;
            }
        } catch (SQLiteException e2) {
        }
    }

    public String[][] select(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) throws Exception {
        this.db = engine.getReadableDatabase();
        Cursor cursor = this.db.query(table, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
        cursor.moveToFirst();
        String[][] list = (String[][]) Array.newInstance(String.class, cursor.getCount(), cursor.getColumnCount());
        for (int i = 0; i < list.length; i++) {
            for (int j = 0; j < columns.length; j++) {
                list[i][j] = cursor.getString(j);
            }
            cursor.moveToNext();
        }
        cursor.close();
        this.db.close();
        return list;
    }

    public void setInsertData(String[] columns, String[] data) {
        this.values = new ContentValues();
        if (columns != null && data != null) {
            for (int i = 0; i < columns.length; i++) {
                if (data[i] == null || i >= data.length) {
                    this.values.putNull(columns[i]);
                } else {
                    this.values.put(columns[i], data[i]);
                }
            }
        }
    }
}
