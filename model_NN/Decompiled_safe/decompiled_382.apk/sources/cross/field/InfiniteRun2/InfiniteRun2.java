package cross.field.InfiniteRun2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Display;
import android.view.WindowManager;

public class InfiniteRun2 extends Activity {
    public static int disp_height;
    public static int disp_width;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().addFlags(1024);
        Display disp = ((WindowManager) getSystemService("window")).getDefaultDisplay();
        disp_width = disp.getWidth();
        disp_height = disp.getHeight();
        startActivity(new Intent(this, GameManager.class));
    }

    public void onResume() {
        super.onResume();
    }

    public void onPause() {
        super.onPause();
        finish();
    }
}
