package cross.field.InfiniteRun2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Display;
import android.view.WindowManager;

public class GameManager extends Activity {
    public static final int EXIT = 5;
    public static final int MAIN = 1;
    public static final int MORE = 4;
    public static final int RESULT = 2;
    public static final int SCORE = 3;
    public static final int TITLE = 0;
    public static int disp_height;
    public static int disp_width;
    public static int scene;
    public static long score;
    public static ScoreDatabase sqldb;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().addFlags(1024);
        Display disp = ((WindowManager) getSystemService("window")).getDefaultDisplay();
        disp_width = disp.getWidth();
        disp_height = disp.getHeight();
        scene = 0;
        gameMode();
        sqldb = new ScoreDatabase(this);
    }

    public void onResume() {
        super.onResume();
        gameMode();
    }

    public int gameMode() {
        switch (scene) {
            case 0:
                startActivity(new Intent(this, TitleActivity.class));
                break;
            case 1:
                startActivity(new Intent(this, MainActivity.class));
                break;
            case 2:
                startActivity(new Intent(this, ResultActivity.class));
                break;
            case 3:
                startActivity(new Intent(this, ScoreActivity.class));
                break;
            case 5:
                finish();
                break;
        }
        return scene;
    }

    public void onDestroy() {
        super.onDestroy();
        scene = 5;
    }
}
