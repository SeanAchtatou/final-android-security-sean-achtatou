package cross.field.InfiniteRun2;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class DatabaseAccess extends SQLiteEngine {
    public DatabaseAccess(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE RANK (SCORE INTEGER)");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS RANK");
        onCreate(db);
    }

    public static SQLiteEngine getInstance(Context context) {
        if (engine == null) {
            engine = new DatabaseAccess(context, Table.DATABASE_NAME, null, 1);
        }
        return engine;
    }
}
