package cross.field.InfiniteRun2;

public class BackGround {
    private Graphics g;
    private int h;
    private int id;
    private int w;
    private int x;
    private int y;

    public BackGround(Graphics g2, int id2, int x2, int y2, int w2, int h2) {
        this.g = g2;
        this.x = x2;
        this.y = y2;
        this.w = w2;
        this.h = h2;
        this.id = id2;
    }

    public void action() {
    }

    public void draw() {
        this.g.drawImage(this.id, this.x, this.y, this.w, this.h);
    }
}
