package cross.field.InfiniteRun2;

import android.app.Activity;
import android.os.Bundle;
import android.view.Display;
import android.view.WindowManager;
import android.widget.LinearLayout;
import jp.Adlantis.Android.AdlantisView;

public class MainActivity extends Activity {
    public static int ad_height;
    public static int ad_width;
    public static int disp_height;
    public static int disp_width;
    public static long score;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().addFlags(1024);
        Display disp = ((WindowManager) getSystemService("window")).getDefaultDisplay();
        disp_width = disp.getWidth();
        disp_height = disp.getHeight();
        float ah = 72.0f * (((float) disp_height) / 854.0f);
        AdlantisView adView = new AdlantisView(this);
        adView.setVisibility(0);
        adView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        LinearLayout linearlayout = new LinearLayout(this);
        linearlayout.addView(adView);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, (int) ah);
        ad_width = disp_width;
        ad_height = (int) ah;
        setContentView(new MainManager(this));
        addContentView(linearlayout, layoutParams);
    }

    public void onDestroy() {
        super.onDestroy();
    }
}
