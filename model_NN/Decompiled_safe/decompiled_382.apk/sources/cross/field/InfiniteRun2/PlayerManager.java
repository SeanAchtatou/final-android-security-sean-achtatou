package cross.field.InfiniteRun2;

public class PlayerManager {
    private Graphics g;
    public Player player;
    private int player_h = 60;
    private int player_w = 60;
    private int player_x = 0;
    private int player_y = 0;
    private int scene;

    public PlayerManager(Graphics g2, int scene2) {
        this.g = g2;
        this.scene = scene2;
        init();
    }

    public void init() {
        float dw = this.g.ratio_width;
        float dh = this.g.ratio_height;
        this.player_w = (int) (((float) this.player_w) * dw);
        this.player_h = (int) (((float) this.player_h) * dh);
        this.player_x = MainManager.INIT_X;
        this.player_y = MainManager.INIT_Y;
        Graphics graphics = this.g;
        this.g.getClass();
        this.player = new Player(graphics, 1, this.player_x, this.player_y, this.player_w, this.player_h);
    }

    public void action() {
        switch (this.scene) {
            case 0:
                this.player.action();
                return;
            case 1:
                this.player.action();
                return;
            case 2:
            case 3:
            default:
                return;
            case 4:
                this.player.action();
                return;
        }
    }

    public void draw() {
        switch (this.scene) {
            case 0:
                this.player.draw();
                return;
            case 1:
                this.player.draw();
                return;
            case 2:
            case 3:
            default:
                return;
            case 4:
                this.player.draw();
                return;
        }
    }

    public void sceneChange(int scene2) {
        this.scene = scene2;
        switch (scene2) {
            case 0:
                this.player.sceneChange(scene2);
                return;
            case 1:
                this.player.sceneChange(scene2);
                return;
            case 2:
            case 3:
            default:
                return;
            case 4:
                this.player.sceneChange(scene2);
                return;
        }
    }

    public void orientation(float dx, float dy) {
        this.player.orientation(dx, dy);
    }
}
