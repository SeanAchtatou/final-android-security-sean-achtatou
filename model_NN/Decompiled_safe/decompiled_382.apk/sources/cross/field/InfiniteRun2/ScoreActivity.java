package cross.field.InfiniteRun2;

import android.app.Activity;
import android.os.Bundle;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

public class ScoreActivity extends Activity implements View.OnClickListener {
    public static int disp_height;
    public static int disp_width;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().addFlags(1024);
        Display disp = ((WindowManager) getSystemService("window")).getDefaultDisplay();
        disp_width = disp.getWidth();
        disp_height = disp.getHeight();
        setContentView((int) R.layout.score);
        findViewById(R.id.menu).setOnClickListener(this);
        ranking();
    }

    public void ranking() {
        String[][] result = GameManager.sqldb.select();
        int[] num = new int[10];
        for (int i = 0; i < result.length; i++) {
            num[i] = Integer.parseInt(result[i][0]);
        }
        int i2 = num[0];
        for (int i3 = 0; i3 < num.length; i3++) {
            for (int j = 1; j < num.length; j++) {
                if (num[j] > num[j - 1]) {
                    int sub = num[j - 1];
                    num[j - 1] = num[j];
                    num[j] = sub;
                }
            }
        }
        ((TextView) findViewById(R.id.rank_1)).setText("�@�@�P�F" + num[0] + "��");
        int i4 = 0 + 1;
        ((TextView) findViewById(R.id.rank_2)).setText("�@�@�Q�F" + num[i4] + "��");
        int i5 = i4 + 1;
        ((TextView) findViewById(R.id.rank_3)).setText("�@�@�R�F" + num[i5] + "��");
        int i6 = i5 + 1;
        ((TextView) findViewById(R.id.rank_4)).setText("�@�@�S�F" + num[i6] + "��");
        int i7 = i6 + 1;
        ((TextView) findViewById(R.id.rank_5)).setText("�@�@�T�F" + num[i7] + "��");
        int i8 = i7 + 1;
        ((TextView) findViewById(R.id.rank_6)).setText("�@�@�U�F" + num[i8] + "��");
        int i9 = i8 + 1;
        ((TextView) findViewById(R.id.rank_7)).setText("�@�@�V�F" + num[i9] + "��");
        int i10 = i9 + 1;
        ((TextView) findViewById(R.id.rank_8)).setText("�@�@�W�F" + num[i10] + "��");
        int i11 = i10 + 1;
        ((TextView) findViewById(R.id.rank_9)).setText("�@�@�X�F" + num[i11] + "��");
        ((TextView) findViewById(R.id.rank_10)).setText("�@�P�O�F" + num[i11 + 1] + "��");
    }

    public void onResume() {
        super.onResume();
        gameMode();
    }

    public int gameMode() {
        switch (GameManager.scene) {
        }
        return GameManager.scene;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.menu:
                GameManager.scene = 0;
                finish();
                return;
            case R.id.more:
            case R.id.score:
            default:
                return;
        }
    }

    public void onDestroy() {
        super.onDestroy();
        GameManager.scene = 0;
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == 0) {
            switch (event.getKeyCode()) {
                case 4:
                    GameManager.scene = 0;
                    finish();
                    break;
            }
        }
        return super.dispatchKeyEvent(event);
    }
}
