package cross.field.InfiniteRun2;

import java.util.Random;

public class GroundManager {
    public static final int CLIFF = 6;
    public static int CLIFF_TIME = 10;
    public static final int DOWN = 4;
    public static int GROUND_BOTTOM = 754;
    public static final int GROUND_HEIGHT_MAX = 100;
    public static final int GROUND_HEIGHT_MIN = 10;
    public static int GROUND_MAX = 30;
    public static int GROUND_TIME = 10;
    public static int GROUND_TOP = 100;
    public static final int GROUND_WALL_MAX = 100;
    public static final int INIT = 0;
    public static final int PLAIN = 1;
    public static final int ROUGH = 2;
    public static final int UP = 3;
    public static final int WALL = 5;
    public static int ground_bottom = GROUND_BOTTOM;
    public static int ground_height_max = 100;
    public static int ground_height_min = 10;
    public static int ground_top = GROUND_TOP;
    private Graphics g;
    public Ground[] ground;
    public int ground_h = 20;
    private int ground_time;
    public int ground_w = 80;
    private int mode;
    private Random random;

    public GroundManager(Graphics g2, int scene) {
        this.g = g2;
        this.mode = 0;
        this.random = new Random();
        ground_height_min = (int) (g2.ratio_height * 10.0f);
        ground_height_max = (int) (g2.ratio_height * 100.0f);
        ground_top = (int) (g2.ratio_height * ((float) GROUND_TOP));
        ground_bottom = (int) (g2.ratio_height * ((float) GROUND_BOTTOM));
        initGround();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void action() {
        if (this.ground_time <= 0) {
            this.ground_time = this.random.nextInt(GROUND_TIME);
            int old = this.mode;
            while (true) {
                this.mode = this.random.nextInt(7);
                if (this.mode != 0 && this.mode != old && this.mode != 1) {
                    break;
                }
            }
            if (this.mode == 6) {
                this.ground_time = this.random.nextInt(CLIFF_TIME);
            }
            if (this.mode == 5) {
                this.ground_time = 1;
            }
            this.ground[GROUND_MAX - 1].v = false;
        }
        this.ground_time--;
        switch (this.mode) {
            case 0:
                initGround();
                this.mode = 1;
                break;
            case 1:
                if (this.ground[GROUND_MAX - 1].y <= ground_top || this.ground[GROUND_MAX - 1].y >= ground_bottom) {
                    this.ground_time = 0;
                    break;
                }
            case 2:
                if (this.random.nextBoolean()) {
                    this.ground[GROUND_MAX - 1].y += this.random.nextInt(ground_height_min);
                } else {
                    this.ground[GROUND_MAX - 1].y += -this.random.nextInt(ground_height_min);
                }
                if (this.ground[GROUND_MAX - 1].y <= ground_top || this.ground[GROUND_MAX - 1].y >= ground_bottom) {
                    this.ground_time = 0;
                    break;
                }
            case 3:
                this.ground[GROUND_MAX - 1].y += -ground_height_min;
                if (this.ground[GROUND_MAX - 1].y <= ground_top) {
                    this.ground[GROUND_MAX - 1].y += -ground_height_min;
                    this.ground_time = 0;
                    break;
                }
                break;
            case 4:
                this.ground[GROUND_MAX - 1].y += ground_height_min;
                if (this.ground[GROUND_MAX - 1].y >= ground_bottom) {
                    this.ground[GROUND_MAX - 1].y += -ground_height_min;
                    this.ground_time = 0;
                    break;
                }
                break;
            case 5:
                while (true) {
                    if (this.random.nextBoolean()) {
                        int y = this.ground[GROUND_MAX - 1].y + this.random.nextInt(100);
                        if (y < ground_bottom) {
                            this.ground[GROUND_MAX - 1].y = y;
                            break;
                        }
                    } else {
                        int y2 = this.ground[GROUND_MAX - 1].y + (this.random.nextInt(100) * -1);
                        if (y2 > ground_top) {
                            this.ground[GROUND_MAX - 1].y = y2;
                            break;
                        }
                    }
                }
            case 6:
                this.ground[GROUND_MAX - 1].v = true;
                break;
        }
        this.ground[GROUND_MAX - 1].h = this.g.disp_height - this.ground[GROUND_MAX - 1].y;
        for (int i = 0; i < GROUND_MAX - 1; i++) {
            this.ground[i].v = this.ground[i + 1].v;
            this.ground[i].y = this.ground[i + 1].y;
            this.ground[i].h = this.g.disp_height - this.ground[i].y;
        }
    }

    public void draw() {
        for (int i = 0; i < GROUND_MAX; i++) {
            this.ground[i].draw();
        }
    }

    public void initGround() {
        int xp2;
        this.ground = new Ground[GROUND_MAX];
        int cnt = 0;
        while (true) {
            int xp1 = (this.g.disp_width + cnt) / GROUND_MAX;
            xp2 = xp1;
            if (xp1 * GROUND_MAX > this.g.disp_width) {
                break;
            }
            cnt++;
        }
        for (int i = 0; i < GROUND_MAX; i++) {
            Ground[] groundArr = this.ground;
            Graphics graphics = this.g;
            this.g.getClass();
            groundArr[i] = new Ground(graphics, 9, xp2 * i, MainManager.INIT_Y, xp2, this.g.disp_height - MainManager.INIT_Y);
        }
    }

    public boolean hitGround(int x1, int y1, int w1, int h1, int x2, int y2, int w2, int h2) {
        int top1 = y1;
        int bottom1 = y1 + h1;
        int left1 = x1;
        int right1 = x1 + w1;
        int top2 = y2;
        int bottom2 = y2 + h2;
        int left2 = x2;
        int right2 = x2 + w2;
        if ((top1 > top2 || top2 > bottom1 || left1 > left2 || left2 > right1) && ((top1 > bottom2 || bottom2 > bottom1 || left1 > left2 || left2 > right1) && ((top1 > top2 || top2 > bottom1 || left1 > right2 || right2 > right1) && (top1 > bottom2 || bottom2 > bottom1 || left1 > right2 || right2 > right1)))) {
            return false;
        }
        return true;
    }
}
