package cross.field.InfiniteRun2;

public class Ground {
    private Graphics g;
    public int h;
    private int id;
    public boolean v = false;
    public int w;
    public int x;
    public int y;

    public Ground(Graphics g2, int id2, int x2, int y2, int w2, int h2) {
        this.g = g2;
        this.x = x2;
        this.y = y2;
        this.w = w2;
        this.h = h2;
        this.id = id2;
    }

    public void init(Graphics g2, int id2, int x2, int y2, int w2, int h2) {
        this.g = g2;
        this.x = x2;
        this.y = y2;
        this.w = w2;
        this.h = h2;
        this.id = id2;
    }

    public void action() {
    }

    public void draw() {
        if (!this.v) {
            this.g.drawImage(this.id, this.x, this.y, this.w, this.h);
        }
    }
}
