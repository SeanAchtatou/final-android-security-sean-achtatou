package cross.field.InfiniteRun2;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import cross.field.InfiniteRun2.Table;

public class MainManager extends SurfaceView implements SensorEventListener, SurfaceHolder.Callback, Runnable {
    public static final int END = 4;
    public static final int INIT_X = ((int) (((double) MainActivity.disp_width) * 0.3d));
    public static final int INIT_Y = ((int) (((double) MainActivity.disp_height) * 0.9d));
    public static final int PLAY = 1;
    public static final int RECORD = 3;
    public static final float SCROLL_SPEED = 15.0f;
    public static final int START = 0;
    private MainActivity activity;
    private BackGround bg;
    private int bg_h = 854;
    private int bg_w = 480;
    private int bg_x = 0;
    private int bg_y = 0;
    private float dx = 0.0f;
    private float dy = 0.0f;
    private Graphics g;
    private GroundManager ground;
    private SurfaceHolder holder = getHolder();
    private PlayerManager player;
    private Point point;
    private long point_score = 0;
    private int scene;
    private Thread thread;

    public MainManager(Context context) {
        super(context);
        this.holder.addCallback(this);
        this.holder.setFixedSize(getWidth(), getHeight());
        setFocusable(true);
        this.activity = (MainActivity) context;
        this.g = new Graphics(this.holder, context);
        setClickable(true);
        init();
    }

    public void init() {
        this.scene = 0;
        float dw = this.g.ratio_width;
        float dh = this.g.ratio_height;
        this.bg_x = (int) (((float) this.bg_x) * dw);
        this.bg_y = (int) (((float) this.bg_y) * dh);
        this.bg_w = (int) (((float) this.bg_w) * dw);
        this.bg_h = (int) (((float) this.bg_h) * dh);
        Graphics graphics = this.g;
        this.g.getClass();
        this.bg = new BackGround(graphics, 0, this.bg_x, this.bg_y, this.bg_w, this.bg_h);
        this.ground = new GroundManager(this.g, 0);
        this.point = new Point(this.g, 0, MainActivity.ad_height + ((int) (20.0f * dh)), 30, 30);
        this.player = new PlayerManager(this.g, 0);
        this.player.player.x = this.ground.ground[(int) (((double) GroundManager.GROUND_MAX) * 0.2d)].x;
        this.player.player.gx = (int) (((double) GroundManager.GROUND_MAX) * 0.2d);
    }

    public void run() {
        while (this.thread != null) {
            update();
            draw();
            sceneChange();
            try {
                Thread.sleep(50);
            } catch (Exception e) {
            }
        }
    }

    public void update() {
        switch (this.scene) {
            case 0:
                this.bg.action();
                this.ground.action();
                this.player.action();
                this.scene = 1;
                return;
            case 1:
                this.bg.action();
                this.ground.action();
                this.player.player.orientation(this.dx, this.dy);
                this.player.action();
                this.point_score += 10;
                return;
            case 2:
            case 3:
            default:
                return;
        }
    }

    public void draw() {
        this.g.lock();
        switch (this.scene) {
            case 0:
                this.bg.draw();
                this.ground.draw();
                this.player.draw();
                break;
            case 1:
                this.bg.draw();
                this.ground.draw();
                this.player.draw();
                this.point.drawPoint(this.point_score);
                break;
            case 3:
                this.bg.draw();
                this.ground.draw();
                this.player.draw();
                break;
            case 4:
                this.bg.draw();
                this.ground.draw();
                this.player.draw();
                break;
        }
        this.g.unlock();
    }

    public void sceneChange() {
        switch (this.scene) {
            case 0:
                this.player.sceneChange(this.scene);
                return;
            case 1:
                this.player.sceneChange(this.scene);
                if (this.player.player.y - this.player.player.h > this.g.disp_height || this.player.player.x + this.player.player.w < 0 || this.player.player.x - this.player.player.w > this.g.disp_width || hitPB()) {
                    this.scene = 3;
                    Player player2 = this.player.player;
                    this.g.getClass();
                    player2.image_id = 8;
                    return;
                }
                return;
            case 2:
            default:
                return;
            case 3:
                this.player.sceneChange(this.scene);
                GameManager.sqldb.insert(new String[]{String.valueOf(this.point_score / 10)});
                String[][] result = GameManager.sqldb.select();
                int[] num = new int[result.length];
                for (int i = 0; i < result.length; i++) {
                    num[i] = Integer.parseInt(result[i][0]);
                }
                for (int i2 = 0; i2 < num.length; i2++) {
                    for (int j = 1; j < num.length; j++) {
                        if (num[j] > num[j - 1]) {
                            int sub = num[j - 1];
                            num[j - 1] = num[j];
                            num[j] = sub;
                        }
                    }
                }
                GameManager.sqldb.delete(Table.Ranking.TABLE_NAME, null);
                int i3 = 0;
                while (i3 < 10 && i3 <= num.length - 1) {
                    GameManager.sqldb.insert(new String[]{String.valueOf(num[i3])});
                    i3++;
                }
                this.scene = 4;
                return;
            case 4:
                MainActivity.score = this.point_score;
                GameManager.score = this.point_score;
                GameManager.scene = 2;
                this.activity.finish();
                return;
        }
    }

    public boolean hitPB() {
        boolean hit = false;
        this.player.player.fall();
        if (((((float) this.player.player.y) - this.player.player.yspeed) - ((float) GroundManager.ground_height_min)) - 3.0f > ((float) this.ground.ground[this.player.player.gx].y) && !this.ground.ground[this.player.player.gx].v) {
            hit = true;
        }
        if (!hit) {
            ride();
        }
        return hit;
    }

    public void ride() {
        if (this.player.player.y < this.ground.ground[this.player.player.gx].y || this.ground.ground[this.player.player.gx].v) {
            Player player2 = this.player.player;
            this.g.getClass();
            player2.image_id = 7;
            this.player.player.anm_cnt = 0;
            return;
        }
        this.player.player.y = this.ground.ground[this.player.player.gx].y;
        this.player.player.yspeed = 0.0f;
        if (2 > this.player.player.jump_count) {
            this.player.player.jump_count++;
        }
    }

    public boolean hitSquare(int x1, int y1, int w1, int h1, int x2, int y2, int w2, int h2) {
        int top1 = y1;
        int bottom1 = y1 + h1;
        int left1 = x1;
        int right1 = x1 + w1;
        int top2 = y2;
        int bottom2 = y2 + h2;
        int left2 = x2;
        int right2 = x2 + w2;
        if ((top1 > top2 || top2 > bottom1 || left1 > left2 || left2 > right1) && ((top1 > bottom2 || bottom2 > bottom1 || left1 > left2 || left2 > right1) && ((top1 > top2 || top2 > bottom1 || left1 > right2 || right2 > right1) && (top1 > bottom2 || bottom2 > bottom1 || left1 > right2 || right2 > right1)))) {
            return false;
        }
        return true;
    }

    public boolean hitSquare(float x1, float y1, float w1, float h1, float x2, float y2, float w2, float h2) {
        float top1 = y1;
        float bottom1 = y1 + h1;
        float left1 = x1;
        float right1 = x1 + w1;
        float top2 = y2;
        float bottom2 = y2 + h2;
        float left2 = x2;
        float right2 = x2 + w2;
        if ((top1 > top2 || top2 > bottom1 || left1 > left2 || left2 > right1) && ((top1 > bottom2 || bottom2 > bottom1 || left1 > left2 || left2 > right1) && ((top1 > top2 || top2 > bottom1 || left1 > right2 || right2 > right1) && (top1 > bottom2 || bottom2 > bottom1 || left1 > right2 || right2 > right1)))) {
            return false;
        }
        return true;
    }

    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case 0:
                if (this.player.player != null && this.scene == 1 && this.player.player.jump_count > 0) {
                    this.player.player.jump_count--;
                    this.player.player.jump();
                    break;
                }
        }
        return super.onTouchEvent(event);
    }

    public void surfaceCreated(SurfaceHolder arg0) {
        this.thread = new Thread(this);
        this.thread.start();
    }

    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == 3) {
            float x = (float) (((int) (event.values[2] * 100.0f)) / 100);
            float y = (float) (((int) ((event.values[1] + 5.0f) * 100.0f)) / 100);
            this.dx = -x;
            this.dy = -y;
        }
    }

    public void surfaceDestroyed(SurfaceHolder arg0) {
        this.thread = null;
    }

    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
    }
}
