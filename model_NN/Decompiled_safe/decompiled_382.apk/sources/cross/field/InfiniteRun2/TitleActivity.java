package cross.field.InfiniteRun2;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;

public class TitleActivity extends Activity implements View.OnClickListener {
    public static int disp_height;
    public static int disp_width;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().addFlags(1024);
        Display disp = ((WindowManager) getSystemService("window")).getDefaultDisplay();
        disp_width = disp.getWidth();
        disp_height = disp.getHeight();
        setContentView((int) R.layout.title);
        findViewById(R.id.start).setOnClickListener(this);
        findViewById(R.id.score).setOnClickListener(this);
        findViewById(R.id.more).setOnClickListener(this);
    }

    public void onResume() {
        super.onResume();
        gameMode();
    }

    public int gameMode() {
        switch (GameManager.scene) {
            case 2:
                finish();
                break;
            case 3:
                finish();
                break;
            case 5:
                finish();
                break;
        }
        return GameManager.scene;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.more:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://androida.me")));
                return;
            case R.id.score:
                startActivity(new Intent(this, ScoreActivity.class));
                return;
            case R.id.start:
                startActivity(new Intent(this, MainActivity.class));
                return;
            default:
                return;
        }
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == 0) {
            switch (event.getKeyCode()) {
                case 4:
                    GameManager.scene = 5;
                    finish();
                    break;
            }
        }
        return super.dispatchKeyEvent(event);
    }
}
