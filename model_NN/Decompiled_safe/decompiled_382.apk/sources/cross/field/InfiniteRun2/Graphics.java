package cross.field.InfiniteRun2;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;

public class Graphics extends SurfaceView implements SurfaceHolder.Callback, Runnable {
    public static final float XPERIA_HEIGHT = 854.0f;
    public static final float XPERIA_WIDTH = 480.0f;
    public final int BG = 0;
    public final int GROUND = 9;
    public final int LOGO_0 = 10;
    public final int LOGO_1 = 11;
    public final int LOGO_2 = 12;
    public final int LOGO_3 = 13;
    public final int LOGO_4 = 14;
    public final int LOGO_5 = 15;
    public final int LOGO_6 = 16;
    public final int LOGO_7 = 17;
    public final int LOGO_8 = 18;
    public final int LOGO_9 = 19;
    public final int MAX = 20;
    public final int PLAYER_FRONT_0 = 1;
    public final int PLAYER_FRONT_1 = 2;
    public final int PLAYER_JUMP_D = 8;
    public final int PLAYER_JUMP_R = 7;
    public final int PLAYER_LEFT_0 = 3;
    public final int PLAYER_LEFT_1 = 4;
    public final int PLAYER_RIGHT_0 = 5;
    public final int PLAYER_RIGHT_1 = 6;
    private Canvas canvas;
    private Context context;
    public int disp_height;
    public int disp_width;
    private SurfaceHolder holder;
    private Drawable[] image;
    private Paint paint;
    public float ratio_height;
    public float ratio_width;

    public Graphics(SurfaceHolder holder2, Context context2) {
        super(context2);
        this.context = context2;
        this.holder = holder2;
        this.paint = new Paint();
        this.paint.setAntiAlias(true);
        Resources r = getResources();
        this.image = new Drawable[20];
        this.image[0] = r.getDrawable(R.drawable.bg_main);
        this.image[1] = r.getDrawable(R.drawable.chara00);
        this.image[2] = r.getDrawable(R.drawable.chara01);
        this.image[5] = r.getDrawable(R.drawable.chara10);
        this.image[6] = r.getDrawable(R.drawable.chara11);
        this.image[3] = r.getDrawable(R.drawable.chara20);
        this.image[4] = r.getDrawable(R.drawable.chara21);
        this.image[7] = r.getDrawable(R.drawable.chara30);
        this.image[8] = r.getDrawable(R.drawable.chara31);
        this.image[9] = r.getDrawable(R.drawable.block);
        this.image[10] = r.getDrawable(R.drawable.logo_0);
        this.image[11] = r.getDrawable(R.drawable.logo_1);
        this.image[12] = r.getDrawable(R.drawable.logo_2);
        this.image[13] = r.getDrawable(R.drawable.logo_3);
        this.image[14] = r.getDrawable(R.drawable.logo_4);
        this.image[15] = r.getDrawable(R.drawable.logo_5);
        this.image[16] = r.getDrawable(R.drawable.logo_6);
        this.image[17] = r.getDrawable(R.drawable.logo_7);
        this.image[18] = r.getDrawable(R.drawable.logo_8);
        this.image[19] = r.getDrawable(R.drawable.logo_9);
        getDisplaySize();
        getDisplayRatio();
    }

    public void getDisplaySize() {
        Display disp = ((WindowManager) this.context.getSystemService("window")).getDefaultDisplay();
        this.disp_width = disp.getWidth();
        this.disp_height = disp.getHeight();
    }

    public void getDisplayRatio() {
        getDisplaySize();
        this.ratio_width = ((float) this.disp_width) / 480.0f;
        this.ratio_height = ((float) this.disp_height) / 854.0f;
    }

    public void drawImage(int id, int x, int y, int w, int h) {
        this.image[id].setBounds(x, y, x + w, y + h);
        this.image[id].draw(this.canvas);
    }

    public void drawAlpha(int id, int x, int y, int w, int h, int alpha) {
        this.image[id].setBounds(x, y, x + w, y + h);
        this.image[id].setAlpha(alpha);
        this.image[id].draw(this.canvas);
    }

    public void fade(int alpha) {
        for (int i = 0; i < 20; i++) {
            this.image[i].setAlpha(alpha);
        }
    }

    public void drawString(String string, int x, int y, int font, int color) {
        this.paint.setTextSize((float) font);
        this.paint.setColor(color);
        this.canvas.drawText(string, (float) x, (float) y, this.paint);
    }

    public void drawRoundedSquare(int x, int y, int size, int color, boolean fill) {
        this.paint.setColor(color);
        this.canvas.drawRoundRect(new RectF((float) x, (float) y, (float) (x + size), (float) (y + size)), 10.0f, 10.0f, this.paint);
        if (fill) {
            this.paint.setStyle(Paint.Style.FILL);
        } else {
            this.paint.setStyle(Paint.Style.STROKE);
        }
    }

    public void lock() {
        this.canvas = this.holder.lockCanvas();
    }

    public void unlock() {
        this.holder.unlockCanvasAndPost(this.canvas);
    }

    public void setcolor(int color) {
        this.paint.setColor(color);
    }

    public void setfontsize(int fontsize) {
        this.paint.setTextSize((float) fontsize);
    }

    public int stringwidth(String string) {
        return (int) this.paint.measureText(string);
    }

    public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
    }

    public void surfaceCreated(SurfaceHolder arg0) {
    }

    public void surfaceDestroyed(SurfaceHolder arg0) {
    }

    public void run() {
    }
}
