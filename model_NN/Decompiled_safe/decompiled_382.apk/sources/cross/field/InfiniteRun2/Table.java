package cross.field.InfiniteRun2;

public class Table {
    public static final String DATABASE_NAME = "game.db";

    public interface Ranking {
        public static final String COL1 = "SCORE";
        public static final int COLUMNS_NUM = 1;
        public static final String TABLE_NAME = "RANK";
    }
}
