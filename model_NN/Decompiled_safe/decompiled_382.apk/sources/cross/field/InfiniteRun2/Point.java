package cross.field.InfiniteRun2;

public class Point {
    private Graphics g;
    private int h;
    private int w;
    private int x;
    private int y;

    public Point(Graphics g2, int x2, int y2, int w2, int h2) {
        this.g = g2;
        this.x = (int) (((float) x2) * g2.ratio_width);
        this.y = (int) (((float) y2) * g2.ratio_height);
        this.w = (int) (((float) w2) * g2.ratio_width);
        this.h = (int) (((float) h2) * g2.ratio_height);
    }

    public void action() {
    }

    public void draw() {
    }

    public void drawPoint(long score) {
        int disit = String.valueOf(score).length();
        if (score < 10) {
            Graphics graphics = this.g;
            this.g.getClass();
            graphics.drawImage(10, this.x, this.y, this.w, this.h);
            return;
        }
        for (int i = 1; i < disit; i++) {
            int ten = 1;
            for (int j = i; j < disit; j++) {
                ten *= 10;
            }
            Graphics graphics2 = this.g;
            this.g.getClass();
            graphics2.drawImage(((int) ((score / ((long) ten)) % 10)) + 10, this.x + (this.w * (i - 1)), this.y, this.w, this.h);
        }
    }
}
