package cross.field.InfiniteRun2;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

public class ResultActivity extends Activity implements View.OnClickListener {
    public static int disp_height;
    public static int disp_width;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().addFlags(1024);
        Display disp = ((WindowManager) getSystemService("window")).getDefaultDisplay();
        disp_width = disp.getWidth();
        disp_height = disp.getHeight();
        setContentView((int) R.layout.result);
        ((TextView) findViewById(R.id.result)).setText(String.valueOf(GameManager.score / 10) + "m");
        findViewById(R.id.menu).setOnClickListener(this);
        findViewById(R.id.more).setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.menu:
                GameManager.scene = 0;
                finish();
                return;
            case R.id.more:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://androida.me")));
                return;
            case R.id.score:
                GameManager.scene = 3;
                finish();
                return;
            default:
                return;
        }
    }

    public void onDestroy() {
        super.onDestroy();
        GameManager.scene = 0;
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == 0) {
            switch (event.getKeyCode()) {
                case 4:
                    GameManager.scene = 0;
                    finish();
                    break;
            }
        }
        return super.dispatchKeyEvent(event);
    }
}
