package com.woodlawn.animationbase;

public final class R {

    public static final class anim {
        public static final int loading = 2130968576;
    }

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int keywords = 2130771971;
        public static final int primaryTextColor = 2130771969;
        public static final int refreshInterval = 2130771972;
        public static final int secondaryTextColor = 2130771970;
    }

    public static final class drawable {
        public static final int icon = 2130837504;
        public static final int treelogo32 = 2130837505;
    }

    public static final class id {
        public static final int AnimationView = 2131230720;
        public static final int Button01 = 2131230726;
        public static final int Button02 = 2131230727;
        public static final int Button03 = 2131230730;
        public static final int Button04 = 2131230728;
        public static final int ad = 2131230731;
        public static final int help = 2131230733;
        public static final int kill = 2131230734;
        public static final int layout_root = 2131230724;
        public static final int loading_icon = 2131230722;
        public static final int loading_layout = 2131230721;
        public static final int loading_text = 2131230723;
        public static final int newGame = 2131230735;
        public static final int request = 2131230732;
        public static final int search = 2131230736;
        public static final int text = 2131230729;
        public static final int title = 2131230725;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class menu {
        public static final int options_menu = 2131165184;
    }

    public static final class raw {
        public static final int bounce = 2131034112;
        public static final int bouncewall = 2131034113;
        public static final int trumpet = 2131034114;
    }

    public static final class string {
        public static final int app_name = 2131099648;
    }

    public static final class styleable {
        public static final int[] com_admob_android_ads_AdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.keywords, R.attr.refreshInterval};
        public static final int com_admob_android_ads_AdView_backgroundColor = 0;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_primaryTextColor = 1;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_secondaryTextColor = 2;
    }
}
