package com.woodlawn.animationbase;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.SurfaceHolder;

public class AnimationThread extends Thread {
    private static int STATE_LOSE = 3;
    private static int STATE_PAUSE = 4;
    private static int STATE_PLAY = 2;
    private static int STATE_RUNNING = 1;
    private static String TAG = "AnimationThread";
    private boolean initialized = false;
    public Handler mHandler;
    private final Paint mPaint = new Paint();
    private boolean onHold = false;
    private Quarterback quarterback;
    private boolean run;
    private SurfaceHolder surfaceHolder;
    private int threadState;

    public AnimationThread(SurfaceHolder surfaceHolder2, Quarterback quarterback2, Handler handler) {
        this.surfaceHolder = surfaceHolder2;
        this.quarterback = quarterback2;
        this.mHandler = handler;
    }

    public void pause() {
        if (this.threadState != STATE_PAUSE) {
            this.threadState = STATE_PAUSE;
        } else {
            this.threadState = STATE_RUNNING;
        }
    }

    private void updateGameState() {
        this.quarterback.update();
    }

    private void setInitialGameState() {
        this.quarterback.init();
    }

    private void doDraw(Canvas c) {
        this.quarterback.draw(c, this.mPaint);
    }

    public void play() {
        this.threadState = STATE_RUNNING;
        this.onHold = false;
    }

    public void setMyActivity(BrickBustingActivityDemo myActivity) {
        ((BrickBustingQB) this.quarterback).setPreferences(myActivity.getPreferences(0));
    }

    public void run() {
        long timer = System.currentTimeMillis();
        while (this.run) {
            Canvas c = null;
            long now = System.currentTimeMillis();
            boolean frame = false;
            if (now - timer >= 24) {
                frame = true;
                timer = now;
            }
            if (this.threadState == STATE_RUNNING) {
                if (frame) {
                    updateGameState();
                }
                this.onHold = false;
            } else if (this.threadState == STATE_PLAY && !this.initialized) {
                setInitialGameState();
            } else if (this.threadState == STATE_LOSE) {
                this.initialized = false;
            }
            if (frame) {
                try {
                    if (this.threadState == STATE_RUNNING) {
                        c = this.surfaceHolder.lockCanvas(null);
                        doDraw(c);
                    }
                } finally {
                    if (c != null) {
                        this.surfaceHolder.unlockCanvasAndPost(c);
                    }
                }
            }
            if (((BrickBustingQB) this.quarterback).gameOver() && !this.onHold) {
                this.threadState = STATE_LOSE;
                Message msg = this.mHandler.obtainMessage();
                Bundle b = new Bundle();
                if (((BrickBustingQB) this.quarterback).whoWon().equals("Computer")) {
                    b.putInt("outcome", 0);
                } else {
                    b.putInt("outcome", 1);
                }
                msg.setData(b);
                this.mHandler.sendMessage(msg);
                this.onHold = true;
            }
        }
    }

    public void setSurfaceSize(int width, int height) {
        synchronized (this.surfaceHolder) {
        }
    }

    public void setRunning(boolean run2) {
        Log.e(TAG, "setRunning called, run = " + run2);
        this.run = run2;
    }
}
