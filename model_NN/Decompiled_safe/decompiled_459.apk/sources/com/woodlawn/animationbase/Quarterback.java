package com.woodlawn.animationbase;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.MotionEvent;

public interface Quarterback {
    void draw(Canvas canvas, Paint paint);

    void handleInput(MotionEvent motionEvent);

    void init();

    void reset();

    void update();
}
