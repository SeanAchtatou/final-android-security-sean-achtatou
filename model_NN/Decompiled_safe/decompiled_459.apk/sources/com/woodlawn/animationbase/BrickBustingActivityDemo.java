package com.woodlawn.animationbase;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import java.io.InputStream;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class BrickBustingActivityDemo extends Activity implements AdListener {
    private static final int ABANDON_GAME_DIALOG = 6;
    private static final int CONTINUE_SPEED_DIALOG = 10;
    private static final int FIRST_TIME_DIALOG = 1;
    private static final int GAME_QUIT_DIALOG = 5;
    private static final int HELP_DIALOG = 2;
    private static final int KILL_BALL_DIALOG = 7;
    public static String PREFERENCE_HIGH_SCORE = "HIGH_SCORE";
    public static String PREFERENCE_HIGH_SCORE_RANDOM = "HIGH_SCORE_RANDOM";
    public static String PREFERENCE_LAST_HEALTH = "LAST_HEALTH";
    public static String PREFERENCE_LAST_LEVEL = "LAST_LEVEL";
    public static String PREFERENCE_LAST_SCORE = "LAST_SCORE";
    public static String PREFERENCE_UPDATE = "UPDATE";
    private static final int RANDOM_SPEED_DIALOG = 9;
    private static final int REGULAR_SPEED_DIALOG = 8;
    private static final float SPEED_FAST = 4.0f;
    private static final float SPEED_FASTER = 8.0f;
    private static final float SPEED_MEDIUM = 2.75f;
    private static final int UPDATE_DIALOG = 11;
    private boolean firsttime = true;
    private boolean loading;
    private float speed = 0.0f;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        ((AnimationView) findViewById(R.id.AnimationView)).setActivity(this);
        setSpeed(SPEED_MEDIUM);
        setupMainMenu();
        SharedPreferences preferences = getPreferences(0);
        if (preferences.getInt("FIRST_TIME", -1) != 3) {
            showDialog(1);
            SharedPreferences.Editor edit = preferences.edit();
            edit.putInt("FIRST_TIME", 3);
            edit.commit();
        }
        setVolumeControlStream(3);
        this.loading = true;
        View a = findViewById(R.id.ad);
        AdRequest adRequest = new AdRequest();
        AdView adView = (AdView) a;
        adView.setAdListener(this);
        adView.loadAd(adRequest);
        ((ImageView) findViewById(R.id.loading_icon)).startAnimation(AnimationUtils.loadAnimation(this, R.anim.loading));
        if (preferences.getInt(PREFERENCE_UPDATE, 1) == 1) {
            new UpdateTask(this, null).execute(new String[0]);
        }
        hideMainMenu();
    }

    /* access modifiers changed from: private */
    public void showMainMenu() {
        if (this.loading) {
            ImageView loadingIcon = (ImageView) findViewById(R.id.loading_icon);
            loadingIcon.clearAnimation();
            loadingIcon.setVisibility(REGULAR_SPEED_DIALOG);
            findViewById(R.id.loading_layout).setVisibility(REGULAR_SPEED_DIALOG);
            this.loading = false;
        }
        findViewById(R.id.layout_root).setVisibility(0);
        View a = findViewById(R.id.ad);
        a.setVisibility(0);
        a.setClickable(true);
    }

    /* access modifiers changed from: private */
    public void hideMainMenu() {
        findViewById(R.id.layout_root).setVisibility(4);
        View a = findViewById(R.id.ad);
        a.setVisibility(4);
        a.setClickable(false);
        ((AdView) a).loadAd(new AdRequest());
    }

    public void onPause() {
        ((AnimationView) findViewById(R.id.AnimationView)).pause();
        saveScore();
        super.onPause();
    }

    public void onSaveInstanceState(Bundle outState) {
        AnimationView animationView = (AnimationView) findViewById(R.id.AnimationView);
        super.onSaveInstanceState(outState);
    }

    public void onResume() {
        ((AnimationView) findViewById(R.id.AnimationView)).pause();
        super.onResume();
    }

    public void onBackPressed() {
        ((AnimationView) findViewById(R.id.AnimationView)).pause();
        showDialog(GAME_QUIT_DIALOG);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options_menu, menu);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        ((AnimationView) findViewById(R.id.AnimationView)).pause();
        return super.onPrepareOptionsMenu(menu);
    }

    public void onOptionsMenuClosed(Menu item) {
        ((AnimationView) findViewById(R.id.AnimationView)).pause();
        super.onOptionsMenuClosed(item);
    }

    public void setSpeed(float speed2) {
        this.speed = speed2;
    }

    public void initNewGame() {
        AnimationView view = (AnimationView) findViewById(R.id.AnimationView);
        if (this.firsttime) {
            view.newGame(this.speed, this.firsttime);
            this.firsttime = false;
            return;
        }
        view.newGame(this.speed, this.firsttime);
    }

    public void initRandomGame() {
        AnimationView view = (AnimationView) findViewById(R.id.AnimationView);
        if (this.firsttime) {
            view.newRandomGame(this.speed, this.firsttime);
            this.firsttime = false;
            return;
        }
        view.newRandomGame(this.speed, this.firsttime);
    }

    public void continueGame() {
        AnimationView view = (AnimationView) findViewById(R.id.AnimationView);
        SharedPreferences preferences = getPreferences(0);
        if (this.firsttime) {
            view.continueGame(this.firsttime, preferences.getInt(PREFERENCE_LAST_LEVEL, 0), preferences.getInt(PREFERENCE_LAST_HEALTH, 3), preferences.getInt(PREFERENCE_LAST_SCORE, 0), this.speed);
            this.firsttime = false;
            return;
        }
        view.continueGame(this.firsttime, preferences.getInt(PREFERENCE_LAST_LEVEL, 0), preferences.getInt(PREFERENCE_LAST_HEALTH, 3), preferences.getInt(PREFERENCE_LAST_SCORE, 0), this.speed);
    }

    public void gameOver(int outcome) {
        SharedPreferences preferences = getPreferences(0);
        AnimationView view = (AnimationView) findViewById(R.id.AnimationView);
        TextView text = (TextView) findViewById(R.id.text);
        saveScore();
        if (outcome == 0) {
            text.setText("You Lost!\nYour Score: " + AnimationView.commas(view.getScore()) + "\n\n" + "High Score: " + AnimationView.commas(preferences.getInt(PREFERENCE_HIGH_SCORE, 0)) + "\n\nRandom High Score: " + AnimationView.commas(preferences.getInt(PREFERENCE_HIGH_SCORE_RANDOM, 0)));
            showMainMenu();
            return;
        }
        text.setText("You Won!\nYour Score: " + AnimationView.commas(view.getScore()) + "\n\n" + "High Score: " + AnimationView.commas(preferences.getInt(PREFERENCE_HIGH_SCORE, 0)) + "\n\nRandom High Score: " + AnimationView.commas(preferences.getInt(PREFERENCE_HIGH_SCORE_RANDOM, 0)));
        showMainMenu();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        new AlertDialog.Builder(this);
        switch (id) {
            case 1:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Welcome! Version 2.7 keeps track of random game high scores and adds blue balls to the game, which give you extra balls to play with.\n\nThanks for giving Brick Busting a try.\n\nHave fun!").setCancelable(true).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        BrickBustingActivityDemo.this.dismissDialog(1);
                    }
                });
                return builder.create();
            case 2:
                AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
                builder2.setMessage("Bust all the bricks and advance to the next level. There are 30 levels to complete.\n\nIn random mode bust all the bricks to win.\n\nIf you get stuck, use the menu to kill the ball.\n\nYellow Ball = Bonus\nRed Ball = Death\nGreen Ball = Extra Life\nBlue Ball = Extra Ball\n\nHave fun!").setCancelable(true).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ((AnimationView) BrickBustingActivityDemo.this.findViewById(R.id.AnimationView)).pause();
                    }
                });
                return builder2.create();
            case 3:
            case 4:
            default:
                return null;
            case GAME_QUIT_DIALOG /*5*/:
                AlertDialog.Builder builder3 = new AlertDialog.Builder(this);
                builder3.setMessage("        Quit?        ").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        BrickBustingActivityDemo.this.finish();
                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ((AnimationView) BrickBustingActivityDemo.this.findViewById(R.id.AnimationView)).pause();
                    }
                });
                return builder3.create();
            case ABANDON_GAME_DIALOG /*6*/:
                AlertDialog.Builder builder4 = new AlertDialog.Builder(this);
                builder4.setMessage("    Abandon Game?    ").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPreferences preferences = BrickBustingActivityDemo.this.getPreferences(0);
                        ((TextView) BrickBustingActivityDemo.this.findViewById(R.id.text)).setText("High Score: " + AnimationView.commas(preferences.getInt(BrickBustingActivityDemo.PREFERENCE_HIGH_SCORE, 0)) + "\n\nRandom High Score: " + AnimationView.commas(preferences.getInt(BrickBustingActivityDemo.PREFERENCE_HIGH_SCORE_RANDOM, 0)));
                        BrickBustingActivityDemo.this.showMainMenu();
                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ((AnimationView) BrickBustingActivityDemo.this.findViewById(R.id.AnimationView)).pause();
                    }
                });
                return builder4.create();
            case KILL_BALL_DIALOG /*7*/:
                AlertDialog.Builder builder5 = new AlertDialog.Builder(this);
                builder5.setMessage("     Kill Ball?     ").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        AnimationView view = (AnimationView) BrickBustingActivityDemo.this.findViewById(R.id.AnimationView);
                        view.pause();
                        view.killBall();
                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ((AnimationView) BrickBustingActivityDemo.this.findViewById(R.id.AnimationView)).pause();
                    }
                });
                return builder5.create();
            case REGULAR_SPEED_DIALOG /*8*/:
                AlertDialog.Builder builder6 = new AlertDialog.Builder(this);
                builder6.setTitle("Select Speed").setSingleChoiceItems(new CharSequence[]{"Medium", "Fast", "Faster"}, 0, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        if (item == 2) {
                            BrickBustingActivityDemo.this.setSpeed(BrickBustingActivityDemo.SPEED_FASTER);
                        } else if (item == 1) {
                            BrickBustingActivityDemo.this.setSpeed(BrickBustingActivityDemo.SPEED_FAST);
                        } else if (item == 0) {
                            BrickBustingActivityDemo.this.setSpeed(BrickBustingActivityDemo.SPEED_MEDIUM);
                        }
                    }
                }).setCancelable(false).setPositiveButton("Play", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        BrickBustingActivityDemo.this.initNewGame();
                    }
                });
                return builder6.create();
            case RANDOM_SPEED_DIALOG /*9*/:
                AlertDialog.Builder builder7 = new AlertDialog.Builder(this);
                builder7.setTitle("Select Speed").setSingleChoiceItems(new CharSequence[]{"Medium", "Fast", "Faster"}, 0, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        if (item == 2) {
                            BrickBustingActivityDemo.this.setSpeed(BrickBustingActivityDemo.SPEED_FASTER);
                        } else if (item == 1) {
                            BrickBustingActivityDemo.this.setSpeed(BrickBustingActivityDemo.SPEED_FAST);
                        } else if (item == 0) {
                            BrickBustingActivityDemo.this.setSpeed(BrickBustingActivityDemo.SPEED_MEDIUM);
                        }
                    }
                }).setCancelable(false).setPositiveButton("Play", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        BrickBustingActivityDemo.this.initRandomGame();
                    }
                });
                return builder7.create();
            case CONTINUE_SPEED_DIALOG /*10*/:
                AlertDialog.Builder builder8 = new AlertDialog.Builder(this);
                builder8.setTitle("Select Speed").setSingleChoiceItems(new CharSequence[]{"Medium", "Fast", "Faster"}, 0, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        if (item == 2) {
                            BrickBustingActivityDemo.this.setSpeed(BrickBustingActivityDemo.SPEED_FASTER);
                        } else if (item == 1) {
                            BrickBustingActivityDemo.this.setSpeed(BrickBustingActivityDemo.SPEED_FAST);
                        } else if (item == 0) {
                            BrickBustingActivityDemo.this.setSpeed(BrickBustingActivityDemo.SPEED_MEDIUM);
                        }
                    }
                }).setCancelable(false).setPositiveButton("Play", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        BrickBustingActivityDemo.this.continueGame();
                    }
                });
                return builder8.create();
            case UPDATE_DIALOG /*11*/:
                AlertDialog.Builder builder9 = new AlertDialog.Builder(this);
                builder9.setMessage("Update Available").setPositiveButton("Update?", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent market = new Intent("android.intent.action.VIEW");
                        market.setData(Uri.parse("market://details?id=com.woodlawn.animationbase"));
                        BrickBustingActivityDemo.this.startActivity(market);
                    }
                }).setNegativeButton("No Thanks", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        BrickBustingActivityDemo.this.noUpdates();
                        BrickBustingActivityDemo.this.dismissDialog(BrickBustingActivityDemo.UPDATE_DIALOG);
                    }
                }).setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                    }
                });
                return builder9.create();
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean onOptionsItemSelected(MenuItem item) {
        AnimationView view = (AnimationView) findViewById(R.id.AnimationView);
        switch (item.getItemId()) {
            case R.id.request /*2131230732*/:
                Intent email = new Intent("android.intent.action.SEND");
                email.setType("plain/text");
                email.putExtra("android.intent.extra.EMAIL", new String[]{"feedback@100woodlawn.com"});
                email.putExtra("android.intent.extra.SUBJECT", "Brick Busting Request/Feedback");
                startActivity(Intent.createChooser(email, "Request a Feature"));
                break;
            case R.id.help /*2131230733*/:
                showDialog(2);
                view.pause();
                return true;
            case R.id.kill /*2131230734*/:
                showDialog(KILL_BALL_DIALOG);
                view.pause();
                return true;
            case R.id.newGame /*2131230735*/:
                showDialog(ABANDON_GAME_DIALOG);
                view.pause();
                return true;
            case R.id.search /*2131230736*/:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pub:\"100 Woodlawn\"")));
                finish();
                return true;
        }
        return false;
    }

    private void setupMainMenu() {
        SharedPreferences preferences = getPreferences(0);
        ((TextView) findViewById(R.id.text)).setText("High Score: " + AnimationView.commas(preferences.getInt(PREFERENCE_HIGH_SCORE, 0)) + "\n\nRandom High Score: " + AnimationView.commas(preferences.getInt(PREFERENCE_HIGH_SCORE_RANDOM, 0)));
        ((TextView) findViewById(R.id.title)).setTypeface(Typeface.createFromAsset(getAssets(), "CCElephantmenGreat-Reg.ttf"));
        ((Button) findViewById(R.id.Button04)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View dialog) {
                BrickBustingActivityDemo.this.hideMainMenu();
                BrickBustingActivityDemo.this.showDialog(BrickBustingActivityDemo.RANDOM_SPEED_DIALOG);
            }
        });
        ((Button) findViewById(R.id.Button03)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View dialog) {
                BrickBustingActivityDemo.this.finish();
            }
        });
        ((Button) findViewById(R.id.Button02)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View dialog) {
                BrickBustingActivityDemo.this.hideMainMenu();
                BrickBustingActivityDemo.this.showDialog(BrickBustingActivityDemo.CONTINUE_SPEED_DIALOG);
            }
        });
        ((Button) findViewById(R.id.Button01)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View dialog) {
                BrickBustingActivityDemo.this.hideMainMenu();
                BrickBustingActivityDemo.this.showDialog(BrickBustingActivityDemo.REGULAR_SPEED_DIALOG);
            }
        });
    }

    public void noUpdates() {
        SharedPreferences.Editor edit = getPreferences(0).edit();
        edit.putInt(PREFERENCE_UPDATE, 2);
        edit.commit();
    }

    public void onDismissScreen(Ad arg0) {
    }

    public void onFailedToReceiveAd(Ad arg0, AdRequest.ErrorCode arg1) {
        if (this.loading) {
            showMainMenu();
        }
    }

    public void onLeaveApplication(Ad arg0) {
    }

    public void onPresentScreen(Ad arg0) {
    }

    public void onReceiveAd(Ad arg0) {
        if (this.loading) {
            showMainMenu();
        }
    }

    public void saveScore() {
        SharedPreferences preferences = getPreferences(0);
        AnimationView view = (AnimationView) findViewById(R.id.AnimationView);
        int score = view.getScore();
        String preference = PREFERENCE_HIGH_SCORE;
        if (view.isRandom()) {
            preference = PREFERENCE_HIGH_SCORE_RANDOM;
        }
        if (preferences.getInt(preference, 0) < score) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putInt(preference, score);
            editor.commit();
        }
    }

    private class UpdateTask extends AsyncTask<String, String, Boolean> {
        private UpdateTask() {
        }

        /* synthetic */ UpdateTask(BrickBustingActivityDemo brickBustingActivityDemo, UpdateTask updateTask) {
            this();
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(String... arg0) {
            PackageInfo info;
            try {
                InputStream stream = new DefaultHttpClient().execute(new HttpGet("http://www.100woodlawn.com/u/bb.upd")).getEntity().getContent();
                String rawVersion = "";
                for (int c = stream.read(); c != -1; c = stream.read()) {
                    rawVersion = String.valueOf(rawVersion) + ((char) c);
                }
                stream.close();
                try {
                    info = BrickBustingActivityDemo.this.getPackageManager().getPackageInfo(BrickBustingActivityDemo.this.getPackageName(), 0);
                } catch (PackageManager.NameNotFoundException e) {
                    info = null;
                }
                String version = "";
                if (info != null) {
                    version = info.versionName;
                }
                if (!rawVersion.equals(version)) {
                    return true;
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            return false;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean results) {
            if (results.booleanValue()) {
                BrickBustingActivityDemo.this.showDialog(BrickBustingActivityDemo.UPDATE_DIALOG);
            }
        }
    }
}
