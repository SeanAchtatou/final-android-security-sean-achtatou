package com.woodlawn.animationbase;

import android.content.Context;
import android.media.SoundPool;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import java.util.HashMap;

public class AnimationView extends SurfaceView implements SurfaceHolder.Callback {
    private static String TAG = "AnimationView";
    /* access modifiers changed from: private */
    public BrickBustingActivityDemo myActivity;
    private Quarterback qb;
    private AnimationThread thread;

    public AnimationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        if (!isInEditMode()) {
            this.qb = new BrickBustingQB((float) getWidth(), (float) getHeight());
            this.thread = new AnimationThread(holder, this.qb, new Handler() {
                public void handleMessage(Message m) {
                    AnimationView.this.myActivity.gameOver(m.getData().getInt("outcome"));
                }
            });
        }
        setFocusable(true);
        SoundPool sp = new SoundPool(5, 3, 0);
        HashMap<String, Integer> soundMap = new HashMap<>();
        soundMap.put("brickHit", Integer.valueOf(sp.load(context, R.raw.bounce, 1)));
        soundMap.put("wallHit", Integer.valueOf(sp.load(context, R.raw.bouncewall, 1)));
        soundMap.put("trumpet", Integer.valueOf(sp.load(context, R.raw.trumpet, 1)));
        ((BrickBustingQB) this.qb).setSoundPool(sp);
        ((BrickBustingQB) this.qb).setSoundMap(soundMap);
        Log.d(TAG, "@@@ done creating view!");
    }

    public void setActivity(BrickBustingActivityDemo activity) {
        this.thread.setMyActivity(activity);
        this.myActivity = activity;
    }

    public void surfaceChanged(SurfaceHolder arg0, int format, int width, int height) {
        ((BrickBustingQB) this.qb).setXMax((float) width);
        ((BrickBustingQB) this.qb).setYMax((float) height);
        this.thread.setSurfaceSize(width, height);
    }

    public void surfaceCreated(SurfaceHolder arg0) {
        Log.d(TAG, "@@@ surfaceCreated called!");
        this.thread.setRunning(true);
    }

    public void newGame(float speed, boolean firsttime) {
        ((BrickBustingQB) this.qb).setRandom(false);
        ((BrickBustingQB) this.qb).setSpeed(speed);
        this.qb.init();
        this.thread.play();
        if (firsttime) {
            this.thread.start();
        }
    }

    public void newRandomGame(float speed, boolean firsttime) {
        ((BrickBustingQB) this.qb).setRandom(true);
        ((BrickBustingQB) this.qb).setSpeed(speed);
        this.qb.init();
        this.thread.play();
        if (firsttime) {
            this.thread.start();
        }
    }

    public int getScore() {
        return ((BrickBustingQB) this.qb).getScore();
    }

    public void continueGame(boolean firsttime, int level, int health, int score, float speed) {
        ((BrickBustingQB) this.qb).setRandom(false);
        ((BrickBustingQB) this.qb).setSpeed(speed);
        ((BrickBustingQB) this.qb).continueInit(level, health, score);
        this.thread.play();
        if (firsttime) {
            this.thread.start();
        }
    }

    public void surfaceDestroyed(SurfaceHolder arg0) {
        boolean retry = true;
        this.thread.setRunning(false);
        while (retry) {
            try {
                this.thread.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        this.qb.handleInput(event);
        if (event.getAction() != 1) {
            return true;
        }
        return false;
    }

    public void pause() {
        this.thread.pause();
    }

    public void killBall() {
        ((BrickBustingQB) this.qb).killBall(-1);
    }

    public boolean isRandom() {
        return ((BrickBustingQB) this.qb).getRandom();
    }

    public static String commas(int number) {
        String text = String.valueOf(number);
        for (int x = text.length() - 3; x > 0; x -= 3) {
            text = String.valueOf(text.substring(0, x)) + "," + text.substring(x, text.length());
        }
        return text;
    }
}
