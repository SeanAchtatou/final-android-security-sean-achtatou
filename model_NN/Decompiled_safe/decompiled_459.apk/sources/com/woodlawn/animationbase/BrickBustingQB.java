package com.woodlawn.animationbase;

import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.media.SoundPool;
import android.view.MotionEvent;
import com.woodlawn.brickbusting.Brick;
import com.woodlawn.brickbusting.Levels;
import com.woodlawn.brickbusting.Pair;
import com.woodlawn.brickbusting.Powerup;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class BrickBustingQB implements Quarterback {
    private float ballRadius = 5.0f;
    private List<Pair> balls;
    private float base_xspeed = 1.5f;
    private float base_yspeed = 2.75f;
    private float brickHeight = 0.0f;
    private float brickWidth = 0.0f;
    private ArrayList<Brick> bricks;
    private int[][] currentLevel;
    private int currentLevelIndex = 0;
    private boolean firstInput = true;
    private int health = 3;
    private int myScore = 0;
    private float paddleHeight = 0.0f;
    private float paddleWidth = 0.0f;
    private ArrayList<Powerup> powerups;
    private SharedPreferences preferences;
    private boolean random;
    private HashMap<String, Integer> soundMap;
    private SoundPool soundPool;
    private List<Pair> speeds;
    private int unbreakableBrickCount = 0;
    private float x_current = 50.0f;
    private float x_max = 0.0f;
    private float x_min = 0.0f;
    private float x_myPaddle = 50.0f;
    private float x_new = 50.0f;
    private float y_max = 0.0f;
    private float y_min = 0.0f;
    private float y_myPaddle = 300.0f;

    public BrickBustingQB(float width, float height) {
        this.x_max = width;
        this.y_max = height;
        Levels.init();
        this.currentLevel = loadLevel(this.currentLevelIndex);
        this.powerups = new ArrayList<>();
    }

    public void setSoundPool(SoundPool soundPool2) {
        this.soundPool = soundPool2;
    }

    public void setSoundMap(HashMap<String, Integer> soundMap2) {
        this.soundMap = soundMap2;
    }

    public void setRandom(boolean random2) {
        this.random = random2;
    }

    public void setSpeed(float speed) {
        this.base_yspeed = speed;
    }

    private int[][] loadLevel(int currentLevel2) {
        this.bricks = new ArrayList<>();
        int[][] l = Levels.map.get(Levels.key[currentLevel2]);
        float x = (this.x_max - ((((float) Levels.width) * this.brickWidth) + ((float) (Levels.width - 1)))) / 2.0f;
        float y = this.ballRadius * 10.0f;
        int[][] returnLevel = (int[][]) Array.newInstance(Integer.TYPE, Levels.height, Levels.width);
        this.unbreakableBrickCount = 0;
        if (!this.random) {
            for (int i = 0; i < Levels.height; i++) {
                for (int j = 0; j < Levels.width; j++) {
                    if (l[i][j] > 0) {
                        this.bricks.add(new Brick((this.brickWidth * ((float) j)) + x + ((float) (j * 1)), (this.brickHeight * ((float) i)) + y + ((float) (i * 1)), this.brickWidth, this.brickHeight, l[i][j], i, j));
                        returnLevel[i][j] = 1;
                        if (l[i][j] == 10) {
                            this.unbreakableBrickCount = this.unbreakableBrickCount + 1;
                        }
                    } else {
                        returnLevel[i][j] = -1;
                    }
                }
            }
        } else {
            Random r = new Random();
            for (int i2 = 0; i2 < Levels.height; i2++) {
                for (int j2 = 0; j2 < Levels.width; j2++) {
                    if (r.nextInt() % 2 == 1) {
                        this.bricks.add(new Brick((this.brickWidth * ((float) j2)) + x + ((float) (j2 * 1)), (this.brickHeight * ((float) i2)) + y + ((float) (i2 * 1)), this.brickWidth, this.brickHeight, (r.nextInt(6) * 10) + r.nextInt(3) + 1, i2, j2));
                        returnLevel[i2][j2] = 1;
                    } else {
                        returnLevel[i2][j2] = -1;
                    }
                }
            }
        }
        Random r2 = new Random();
        this.powerups = new ArrayList<>();
        int bonusNum = 1;
        if (this.bricks.size() > 10) {
            bonusNum = r2.nextInt(this.bricks.size() / 10) + 1;
        }
        int deathNum = 1;
        if (this.bricks.size() > 10) {
            deathNum = r2.nextInt(this.bricks.size() / 5) + 1;
        }
        int ballNum = 1;
        if (this.bricks.size() > 10) {
            ballNum = r2.nextInt(this.bricks.size() / 10) + 1;
        }
        int i3 = 0;
        while (i3 < bonusNum) {
            Brick b = this.bricks.get(r2.nextInt(this.bricks.size()));
            if (!b.hasPowerup()) {
                b.setPowerup(new Powerup((((float) b.getJ()) * this.brickWidth) + x + ((float) b.getJ()) + (this.brickWidth / 2.0f), (this.brickHeight * ((float) b.getI())) + y + ((float) b.getI()) + (this.brickHeight / 2.0f), this.ballRadius, 3, b.getI(), b.getJ(), this.base_yspeed / 2.0f, 3));
            } else {
                i3--;
            }
            i3++;
        }
        int i4 = 0;
        while (i4 < deathNum) {
            Brick b2 = this.bricks.get(r2.nextInt(this.bricks.size()));
            if (!b2.hasPowerup()) {
                b2.setPowerup(new Powerup((((float) b2.getJ()) * this.brickWidth) + x + ((float) b2.getJ()) + (this.brickWidth / 2.0f), (this.brickHeight * ((float) b2.getI())) + y + ((float) b2.getI()) + (this.brickHeight / 2.0f), this.ballRadius, 3, b2.getI(), b2.getJ(), this.base_yspeed / 2.0f, 1));
            } else {
                i4--;
            }
            i4++;
        }
        int i5 = 0;
        while (i5 < ballNum) {
            Brick b3 = this.bricks.get(r2.nextInt(this.bricks.size()));
            if (!b3.hasPowerup()) {
                b3.setPowerup(new Powerup((((float) b3.getJ()) * this.brickWidth) + x + ((float) b3.getJ()) + (this.brickWidth / 2.0f), (this.brickHeight * ((float) b3.getI())) + y + ((float) b3.getI()) + (this.brickHeight / 2.0f), this.ballRadius, 3, b3.getI(), b3.getJ(), this.base_yspeed / 2.0f, 4));
            } else {
                i5--;
            }
            i5++;
        }
        int i6 = 0;
        while (i6 < 1) {
            Brick b4 = this.bricks.get(r2.nextInt(this.bricks.size()));
            if (!b4.hasPowerup()) {
                b4.setPowerup(new Powerup((((float) b4.getJ()) * this.brickWidth) + x + ((float) b4.getJ()) + (this.brickWidth / 2.0f), (this.brickHeight * ((float) b4.getI())) + y + ((float) b4.getI()) + (this.brickHeight / 2.0f), this.ballRadius, 3, b4.getI(), b4.getJ(), this.base_yspeed / 2.0f, 2));
            } else {
                i6--;
            }
            i6++;
        }
        return returnLevel;
    }

    public String whoWon() {
        if (gameOver() && this.currentLevelIndex == -1) {
            return "Player";
        }
        if (gameOver()) {
            return "Computer";
        }
        return "No One";
    }

    public boolean gameOver() {
        if (this.health <= 0 || this.currentLevelIndex == -1) {
            return true;
        }
        return false;
    }

    public void draw(Canvas c, Paint paint) {
        paint.setColor(-16777216);
        c.drawRect(0.0f, 0.0f, (float) c.getWidth(), (float) c.getHeight(), paint);
        if (this.firstInput) {
            paint.setColor(Color.argb(255, 128, 128, 128));
        } else {
            paint.setColor(Color.argb(255, 190, 190, 190));
        }
        c.drawRect(0.0f, 1.0f + this.y_max, this.x_max, (this.y_max * 0.16666667f) + this.y_max, paint);
        Iterator<Brick> it = this.bricks.iterator();
        while (it.hasNext()) {
            it.next().draw(c, paint);
        }
        Iterator<Powerup> it2 = this.powerups.iterator();
        while (it2.hasNext()) {
            it2.next().draw(c, paint);
        }
        paint.setColor(Color.argb(255, 217, 71, 58));
        Typeface oldTypeface = paint.getTypeface();
        paint.setTypeface(Typeface.create(Typeface.DEFAULT, 1));
        paint.setTextSize(24.0f);
        paint.setColor(Color.argb(255, 66, 160, (int) Powerup.BONUS_1_VALUE));
        c.drawRoundRect(new RectF(0.0f + this.x_myPaddle, 0.0f + this.y_myPaddle, this.paddleWidth + this.x_myPaddle, this.paddleHeight + this.y_myPaddle), 5.0f, 5.0f, paint);
        paint.setTextSize(this.ballRadius * 2.0f);
        paint.setTypeface(oldTypeface);
        c.drawText("SCORE: " + AnimationView.commas(this.myScore), 4.0f, (this.ballRadius * 2.0f) + 2.0f, paint);
        paint.setColor(Color.argb(255, 217, 71, 58));
        Rect tr = new Rect();
        if (this.random) {
            paint.getTextBounds("Random", 0, "Random".length(), tr);
            c.drawText("Random", (this.x_max / 2.0f) - ((float) (tr.right / 2)), (this.ballRadius * 2.0f) + 2.0f, paint);
        } else if (this.currentLevelIndex == -1) {
            paint.getTextBounds(Levels.key[Levels.key.length - 1], 0, Levels.key[Levels.key.length - 1].length(), tr);
            c.drawText(Levels.key[Levels.key.length - 1], (this.x_max / 2.0f) - ((float) (tr.right / 2)), (this.ballRadius * 2.0f) + 2.0f, paint);
        } else {
            paint.getTextBounds(Levels.key[this.currentLevelIndex], 0, Levels.key[this.currentLevelIndex].length(), tr);
            c.drawText(Levels.key[this.currentLevelIndex], (this.x_max / 2.0f) - ((float) (tr.right / 2)), (this.ballRadius * 2.0f) + 2.0f, paint);
        }
        paint.setColor(-1);
        c.drawLine(0.0f, this.y_min, this.x_max, 4.0f + (this.ballRadius * 2.0f), paint);
        for (int i = 1; i < this.health; i++) {
            c.drawCircle((this.x_max - (((float) i) * ((this.ballRadius * 2.0f) + 8.0f))) + ((float) (i * 4)), this.ballRadius + 2.0f, this.ballRadius, paint);
        }
        for (Pair ball : this.balls) {
            c.drawCircle(ball.x, ball.y, this.ballRadius, paint);
        }
    }

    public void continueInit(int level, int health2, int score) {
        this.myScore = score;
        this.health = health2;
        this.currentLevelIndex = level;
        this.currentLevel = loadLevel(this.currentLevelIndex);
        this.balls = new ArrayList();
        this.speeds = new ArrayList();
        this.balls.add(new Pair(this.x_myPaddle + (this.paddleWidth / 2.0f), this.y_myPaddle - (this.ballRadius / 2.0f)));
        this.speeds.add(new Pair(0.0f, 0.0f));
    }

    public void init() {
        this.myScore = 0;
        this.health = 3;
        this.currentLevelIndex = 0;
        this.currentLevel = loadLevel(this.currentLevelIndex);
        this.balls = new ArrayList();
        this.speeds = new ArrayList();
        this.balls.add(new Pair(this.x_myPaddle + (this.paddleWidth / 2.0f), this.y_myPaddle - (this.ballRadius / 2.0f)));
        this.speeds.add(new Pair(0.0f, 0.0f));
    }

    public void setPreferences(SharedPreferences preferences2) {
        this.preferences = preferences2;
    }

    public void nextLevel() {
        this.health++;
        this.currentLevelIndex++;
        this.myScore += 1000;
        if (this.currentLevelIndex == Levels.key.length) {
            this.currentLevelIndex = -1;
            return;
        }
        this.currentLevel = loadLevel(this.currentLevelIndex);
        this.balls = new ArrayList();
        this.speeds = new ArrayList();
        this.balls.add(new Pair(this.x_myPaddle + (this.paddleWidth / 2.0f), this.y_myPaddle - (this.ballRadius / 2.0f)));
        this.speeds.add(new Pair(0.0f, 0.0f));
        SharedPreferences.Editor editor = this.preferences.edit();
        editor.putInt(BrickBustingActivityDemo.PREFERENCE_LAST_LEVEL, this.currentLevelIndex);
        editor.putInt(BrickBustingActivityDemo.PREFERENCE_LAST_HEALTH, this.health);
        editor.putInt(BrickBustingActivityDemo.PREFERENCE_LAST_SCORE, this.myScore);
        editor.commit();
    }

    public void reset() {
    }

    public void setXMax(float x_max2) {
        this.brickWidth = Brick.calculateWidth(x_max2);
        this.x_max = x_max2;
        this.paddleWidth = 0.25f * x_max2;
    }

    public void setYMax(float y_max2) {
        this.brickHeight = Brick.calculateHeight(y_max2);
        this.y_max = y_max2 - (0.14285715f * y_max2);
        this.paddleHeight = 0.034883723f * y_max2;
        this.ballRadius = 0.011627907f * y_max2;
        this.base_yspeed = 0.006395349f * y_max2;
        this.y_myPaddle = this.y_max - this.paddleHeight;
        this.y_min = (this.ballRadius * 2.0f) + 4.0f;
        Iterator<Brick> it = this.bricks.iterator();
        while (it.hasNext()) {
            Brick b = it.next();
            if (b.hasPowerup()) {
                b.setPowerupRadius(this.ballRadius);
            }
        }
    }

    public void update() {
        int result;
        RectF rectF = new RectF(0.0f + this.x_myPaddle, 0.0f + this.y_myPaddle, this.paddleWidth + this.x_myPaddle, this.paddleHeight + this.y_myPaddle);
        for (int i = 0; i < this.powerups.size(); i++) {
            Powerup p = this.powerups.get(i);
            p.update();
            int result2 = p.collision(rectF);
            if (result2 == 3) {
                this.myScore = this.myScore + Powerup.BONUS_1_VALUE;
                this.powerups.remove(i);
            } else if (result2 == 1) {
                this.health = this.health - 1;
                this.powerups.remove(i);
            } else if (result2 == 2) {
                this.health = this.health + 1;
                this.powerups.remove(i);
            } else if (result2 == 4) {
                this.balls.add(new Pair(this.x_myPaddle + (this.paddleWidth / 2.0f), this.y_myPaddle - (this.ballRadius / 2.0f)));
                this.speeds.add(new Pair(this.base_xspeed, this.base_yspeed));
                this.powerups.remove(i);
            }
        }
        int iterator = 0;
        while (iterator < this.balls.size()) {
            if (this.speeds.get(iterator).y == 0.0f) {
                this.balls.get(iterator).x = this.x_myPaddle + (this.paddleWidth / 2.0f);
                return;
            }
            RectF rectF2 = new RectF((this.balls.get(iterator).x - this.ballRadius) + this.speeds.get(iterator).x, (this.balls.get(iterator).y - this.ballRadius) + this.speeds.get(iterator).y, this.balls.get(iterator).x + this.ballRadius + this.speeds.get(iterator).x, this.balls.get(iterator).y + this.ballRadius + this.speeds.get(iterator).y);
            RectF rectF3 = new RectF(this.balls.get(iterator).x - this.ballRadius, this.balls.get(iterator).y - this.ballRadius, this.balls.get(iterator).x + this.ballRadius, this.balls.get(iterator).y + this.ballRadius);
            boolean brickHit = false;
            ArrayList<Brick> bricksHit = new ArrayList<>();
            int index = 0;
            while (index < this.bricks.size()) {
                Brick brick = this.bricks.get(index);
                if (!bricksHit.contains(brick)) {
                    int i2 = brick.getI();
                    int j = brick.getJ();
                    if ((i2 == 0 || j == 0 || i2 == Levels.height - 1 || j == Levels.width - 1 || this.currentLevel[i2 + 1][j] < 0 || this.currentLevel[i2][j + 1] < 0 || this.currentLevel[i2 - 1][j] < 0 || this.currentLevel[i2][j - 1] < 0) && (result = brick.collision(rectF3, rectF2, this.speeds.get(iterator).x, this.speeds.get(iterator).y)) >= 0) {
                        playHitBrick();
                        brickHit = true;
                        if (result != 10) {
                            this.myScore = this.myScore + 5;
                        }
                        float oldspeed = this.speeds.get(iterator).y;
                        this.speeds.get(iterator).y = brick.updateYSpeed(rectF2, this.speeds.get(iterator).x, this.speeds.get(iterator).y);
                        if (oldspeed == this.speeds.get(iterator).y) {
                            this.speeds.get(iterator).x = brick.updateXSpeed(rectF2, this.speeds.get(iterator).x, this.speeds.get(iterator).y);
                        }
                        if (result == 0) {
                            Brick b = this.bricks.remove(index);
                            if (b.hasPowerup()) {
                                this.powerups.add(b.getPowerup());
                            }
                            if (this.bricks.isEmpty() || this.bricks.size() == this.unbreakableBrickCount) {
                                playTrumpet();
                                if (!this.random) {
                                    nextLevel();
                                    return;
                                }
                                this.currentLevelIndex = -1;
                            } else {
                                this.currentLevel[i2][j] = -1;
                            }
                        }
                        index = 0;
                        bricksHit.add(brick);
                    }
                }
                index++;
            }
            boolean ballMissed = false;
            if (!brickHit) {
                if (this.balls.get(iterator).y + this.speeds.get(iterator).y + (this.ballRadius / 2.0f) >= this.y_max) {
                    killBall(iterator);
                    ballMissed = true;
                    iterator--;
                } else if (this.balls.get(iterator).y > this.y_myPaddle && this.balls.get(iterator).x + this.speeds.get(iterator).x >= this.x_myPaddle && this.balls.get(iterator).x + this.speeds.get(iterator).x <= this.x_myPaddle + this.paddleWidth) {
                    this.speeds.get(iterator).x = this.speeds.get(iterator).x * -1.0f;
                    playHitWall();
                } else if (this.balls.get(iterator).x + this.speeds.get(iterator).x > this.x_myPaddle && this.balls.get(iterator).x + this.speeds.get(iterator).x < this.x_myPaddle + this.paddleWidth && this.balls.get(iterator).y + this.speeds.get(iterator).y + (this.ballRadius / 2.0f) > this.y_myPaddle) {
                    playHitWall();
                    float segment = this.paddleWidth / 5.0f;
                    if (this.balls.get(iterator).x + this.speeds.get(iterator).x <= this.x_myPaddle || this.balls.get(iterator).x + this.speeds.get(iterator).x >= this.x_myPaddle + segment) {
                        if (this.balls.get(iterator).x + this.speeds.get(iterator).x <= this.x_myPaddle + segment || this.balls.get(iterator).x + this.speeds.get(iterator).x >= this.x_myPaddle + (2.0f * segment)) {
                            if (this.balls.get(iterator).x + this.speeds.get(iterator).x <= this.x_myPaddle + (3.0f * segment) || this.balls.get(iterator).x + this.speeds.get(iterator).x >= this.x_myPaddle + (4.0f * segment)) {
                                if (this.balls.get(iterator).x + this.speeds.get(iterator).x > this.x_myPaddle + (4.0f * segment) && this.balls.get(iterator).x + this.speeds.get(iterator).x < this.x_myPaddle + this.paddleWidth && Math.abs(this.speeds.get(iterator).x + 0.75f) <= this.brickHeight / 2.0f) {
                                    this.speeds.get(iterator).x = this.speeds.get(iterator).x + 0.75f;
                                }
                            } else if (Math.abs(this.speeds.get(iterator).x + 0.5f) <= this.brickHeight / 2.0f) {
                                this.speeds.get(iterator).x = this.speeds.get(iterator).x + 0.5f;
                            }
                        } else if (Math.abs(this.speeds.get(iterator).x - 0.5f) <= this.brickHeight / 2.0f) {
                            this.speeds.get(iterator).x = this.speeds.get(iterator).x - 0.5f;
                        }
                    } else if (Math.abs(this.speeds.get(iterator).x - 0.75f) <= this.brickHeight / 2.0f) {
                        this.speeds.get(iterator).x = this.speeds.get(iterator).x - 0.75f;
                    }
                    this.speeds.get(iterator).y = this.speeds.get(iterator).y * -1.0f;
                } else if ((this.balls.get(iterator).y + this.speeds.get(iterator).y) - (this.ballRadius / 2.0f) <= this.y_min) {
                    this.speeds.get(iterator).y = this.speeds.get(iterator).y * -1.0f;
                    playHitWall();
                } else if (this.balls.get(iterator).x + this.speeds.get(iterator).x + (this.ballRadius / 2.0f) >= this.x_max || (this.balls.get(iterator).x + this.speeds.get(iterator).x) - (this.ballRadius / 2.0f) <= this.x_min) {
                    this.speeds.get(iterator).x = this.speeds.get(iterator).x * -1.0f;
                    playHitWall();
                }
            }
            if (!ballMissed) {
                this.balls.get(iterator).x = this.balls.get(iterator).x + this.speeds.get(iterator).x;
                this.balls.get(iterator).y = this.balls.get(iterator).y + this.speeds.get(iterator).y;
            }
            iterator++;
        }
    }

    public void handleInput(MotionEvent event) {
        if (event.getY() >= this.y_max) {
            this.x_new = event.getX();
            if (this.firstInput) {
                this.x_current = this.x_new;
                this.firstInput = false;
            } else {
                if (this.x_new != this.x_current) {
                    float temp = this.x_current - this.x_new;
                    if (this.x_myPaddle - temp <= this.x_max - this.paddleWidth && this.x_myPaddle - temp >= 0.0f) {
                        this.x_myPaddle -= this.x_current - this.x_new;
                    }
                }
                this.x_current = this.x_new;
            }
            if (event.getAction() == 1) {
                this.firstInput = true;
                if (this.speeds.get(0).y == 0.0f) {
                    this.speeds.get(0).y = this.base_yspeed * -1.0f;
                    this.speeds.get(0).x = this.base_xspeed;
                    return;
                }
                return;
            }
            return;
        }
        this.firstInput = true;
    }

    public int getScore() {
        return this.myScore;
    }

    public void killBall(int ball) {
        if (ball == -1 || this.balls.size() == 1) {
            this.balls = new ArrayList();
            this.speeds = new ArrayList();
            this.balls.add(new Pair(this.x_myPaddle + (this.paddleWidth / 2.0f), this.y_myPaddle - (this.ballRadius / 2.0f)));
            this.speeds.add(new Pair(0.0f, 0.0f));
            this.health--;
            return;
        }
        this.balls.remove(ball);
        this.speeds.remove(ball);
    }

    private void playHitBrick() {
        this.soundPool.play(this.soundMap.get("brickHit").intValue(), 1.0f, 1.0f, 0, 0, 1.0f);
    }

    private void playHitWall() {
        this.soundPool.play(this.soundMap.get("wallHit").intValue(), 1.0f, 1.0f, 0, 0, 1.0f);
    }

    private void playTrumpet() {
        this.soundPool.play(this.soundMap.get("trumpet").intValue(), 1.0f, 1.0f, 0, 0, 1.0f);
    }

    public boolean getRandom() {
        return this.random;
    }
}
