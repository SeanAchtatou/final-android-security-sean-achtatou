package com.woodlawn.brickbusting;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;

public class Powerup {
    public static final int BALL = 4;
    public static final int BONUS_1 = 3;
    public static final int BONUS_1_VALUE = 250;
    public static final int DEATH = 1;
    public static final int LIFE = 2;
    public static final int NO_COLLISION = -1;
    private RectF ball;
    private int color = 0;
    private int i;
    private int j;
    private float radius;
    private float speed;
    private int type;
    private float x;
    private float y;

    public static float calculateWidth(float width) {
        return 0.05625f * width;
    }

    public static float calculateHeight(float height) {
        return 0.023255814f * height;
    }

    public Powerup(float x2, float y2, float radius2, int health, int i2, int j2, float speed2, int type2) {
        this.x = x2;
        this.y = y2;
        this.radius = radius2;
        this.i = i2;
        this.j = j2;
        this.speed = speed2;
        this.ball = new RectF(x2 - radius2, y2 - radius2, x2 + radius2, y2 + radius2);
        this.type = type2;
        if (type2 == 1) {
            this.color = -65536;
        } else if (type2 == 2) {
            this.color = -16711936;
        } else if (type2 == 3) {
            this.color = -256;
        } else if (type2 == 4) {
            this.color = -16776961;
        }
    }

    public int getI() {
        return this.i;
    }

    public int getJ() {
        return this.j;
    }

    public void draw(Canvas c, Paint paint) {
        int oldColor = paint.getColor();
        int oldAlpha = paint.getAlpha();
        paint.setColor(this.color);
        c.drawCircle(this.x, this.y, this.radius, paint);
        paint.setColor(oldColor);
        paint.setAlpha(oldAlpha);
    }

    public void setRadius(float radius2) {
        this.radius = radius2;
    }

    public int collision(RectF paddle) {
        this.ball = new RectF(this.x - this.radius, this.y - this.radius, this.x + this.radius, this.y + this.radius);
        if (RectF.intersects(this.ball, paddle)) {
            return this.type;
        }
        return -1;
    }

    public void update() {
        this.y += this.speed;
    }
}
