package com.woodlawn.brickbusting;

public class Pair {
    public float x;
    public float y;

    public Pair() {
    }

    public Pair(float x2, float y2) {
        this.x = x2;
        this.y = y2;
    }
}
