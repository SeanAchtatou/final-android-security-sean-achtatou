package com.woodlawn.brickbusting;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;

public class Brick {
    private static final String[] COLOR = {COLOR_RED, COLOR_ORANGE, COLOR_YELLOW, COLOR_GREEN, COLOR_BLUE, COLOR_PURPLE};
    private static final String COLOR_BLUE = "#7F92FF";
    private static final String COLOR_GREEN = "#7FFF8E";
    private static final String COLOR_ORANGE = "#FFB27F";
    private static final String COLOR_PURPLE = "#D67FFF";
    private static final String COLOR_RED = "#FF7F7F";
    private static final String COLOR_YELLOW = "#FFE97F";
    private int baseAlpha = 150;
    private RectF brick;
    private int color = 0;
    private int health = 0;
    private int i;
    private int j;
    private Powerup powerup;

    public static float calculateWidth(float width) {
        return 0.073125f * width;
    }

    public static float calculateHeight(float height) {
        return 0.034883723f * height;
    }

    public Brick(float x, float y, float brickWidth, float brickHeight, int health2, int i2, int j2) {
        this.brick = new RectF(x, y, x + brickWidth, y + brickHeight);
        if (health2 != 10) {
            this.health = health2 % 10;
        } else {
            this.health = health2;
        }
        this.i = i2;
        this.j = j2;
        this.color = Color.parseColor(COLOR[health2 / 10]);
    }

    public int getI() {
        return this.i;
    }

    public int getJ() {
        return this.j;
    }

    public void setPowerup(Powerup powerup2) {
        this.powerup = powerup2;
    }

    public boolean hasPowerup() {
        return this.powerup != null;
    }

    public Powerup getPowerup() {
        return this.powerup;
    }

    public void setPowerupRadius(float radius) {
        this.powerup.setRadius(radius);
    }

    public void draw(Canvas c, Paint paint) {
        int oldColor = paint.getColor();
        int oldAlpha = paint.getAlpha();
        paint.setColor(this.color);
        c.drawRect(this.brick, paint);
        if (this.health != 10) {
            paint.setColor(0);
            paint.setAlpha(this.baseAlpha - ((4 - this.health) * 50));
        } else {
            paint.setARGB(255, 204, 204, 204);
        }
        c.drawRect(this.brick, paint);
        paint.setColor(0);
        paint.setAlpha(25);
        float borderb = 0.2f * (this.brick.bottom - this.brick.top);
        float borderr = 0.1f * (this.brick.right - this.brick.left);
        c.drawRect(this.brick.left, this.brick.bottom - borderb, this.brick.right - borderr, this.brick.bottom, paint);
        c.drawRect(this.brick.right - borderr, this.brick.top, this.brick.right, this.brick.bottom, paint);
        paint.setColor(oldColor);
        paint.setAlpha(oldAlpha);
    }

    public int collision(RectF currentBall, RectF futureBall, float x_speed, float y_speed) {
        if (!RectF.intersects(this.brick, futureBall)) {
            return -1;
        }
        if (this.health == 10) {
            return 10;
        }
        this.health--;
        return this.health;
    }

    public float updateYSpeed(RectF ball, float x_speed, float y_speed) {
        RectF temp = new RectF(ball.left - x_speed, ball.top - y_speed, ball.right - x_speed, ball.bottom - y_speed);
        if (temp.bottom <= this.brick.top && temp.right >= this.brick.left && temp.left <= this.brick.right) {
            return y_speed * -1.0f;
        }
        if (temp.top < this.brick.bottom || temp.right < this.brick.left || temp.left > this.brick.right) {
            return y_speed;
        }
        return y_speed * -1.0f;
    }

    public float updateXSpeed(RectF ball, float x_speed, float y_speed) {
        RectF temp = new RectF(ball.left - x_speed, ball.top - y_speed, ball.right - x_speed, ball.bottom - y_speed);
        if (temp.right <= this.brick.left && temp.top <= this.brick.bottom && temp.bottom >= this.brick.top) {
            return x_speed * -1.0f;
        }
        if (temp.left < this.brick.right || temp.top > this.brick.bottom || temp.bottom < this.brick.top) {
            return x_speed;
        }
        return x_speed * -1.0f;
    }
}
