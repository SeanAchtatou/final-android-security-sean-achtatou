package com.imepu.picssearch.json;

import org.json.JSONException;
import org.json.JSONObject;

public class PrcmEvaluate {
    private int comment;
    private int like;
    private int view;
    private int warn;

    public PrcmEvaluate(JSONObject json) throws JSONException {
        this.view = json.getInt("view");
        this.warn = json.getInt("warn");
        this.comment = json.getInt("comment");
        this.like = json.getInt("like");
    }

    public int getView() {
        return this.view;
    }

    public int getWarn() {
        return this.warn;
    }

    public int getComment() {
        return this.comment;
    }

    public int getLike() {
        return this.like;
    }
}
