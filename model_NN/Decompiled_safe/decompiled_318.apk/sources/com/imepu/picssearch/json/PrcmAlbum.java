package com.imepu.picssearch.json;

import org.json.JSONException;
import org.json.JSONObject;

public class PrcmAlbum {
    private String albumId;
    private String albumUrl;
    private String createdAt;
    private String likeComment;
    private int pictureCount;
    private String title;
    private String updatedAt;
    private int userId;

    public PrcmAlbum(JSONObject json) throws JSONException {
        this.userId = json.getInt("user_id");
        this.title = json.getString("title");
        this.likeComment = json.getString("like_comment");
        this.updatedAt = json.getString("updated_at");
        this.createdAt = json.getString("created_at");
        this.albumId = json.getString("album_id");
        this.albumUrl = json.getString("album_url");
        this.pictureCount = json.getInt("picture_count");
    }

    public int getUserId() {
        return this.userId;
    }

    public String getTitle() {
        return this.title;
    }

    public String getLikeComment() {
        return this.likeComment;
    }

    public String getAlbumId() {
        return this.albumId;
    }

    public int getPictureCount() {
        return this.pictureCount;
    }

    public String getUpdatedAt() {
        return this.updatedAt;
    }

    public String getCreatedAt() {
        return this.createdAt;
    }

    public String getAlbumUrl() {
        return this.albumUrl;
    }
}
