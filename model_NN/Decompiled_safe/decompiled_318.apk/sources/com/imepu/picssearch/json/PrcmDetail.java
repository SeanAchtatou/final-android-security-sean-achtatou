package com.imepu.picssearch.json;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PrcmDetail {
    private PrcmAlbum album;
    private String caption;
    private PrcmEvaluate evaluate;
    private PrcmPic pic;
    private JSONArray tags;

    public PrcmDetail(JSONObject json) throws JSONException {
        this.pic = new PrcmPic(json.getJSONObject("picture"));
        this.caption = json.getString("caption");
        this.album = new PrcmAlbum(json.getJSONObject("album"));
        this.tags = json.getJSONArray("tags");
        this.evaluate = new PrcmEvaluate(json.getJSONObject("evaluate"));
    }

    public PrcmPic getPic() {
        return this.pic;
    }

    public String getCaption() {
        return this.caption.replaceAll("\\r", "");
    }

    public PrcmAlbum getAlbum() {
        return this.album;
    }

    public JSONArray getTags() {
        return this.tags;
    }

    public PrcmEvaluate getEvaluate() {
        return this.evaluate;
    }
}
