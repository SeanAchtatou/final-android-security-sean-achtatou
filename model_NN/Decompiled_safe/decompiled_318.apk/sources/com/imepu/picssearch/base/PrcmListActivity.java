package com.imepu.picssearch.base;

import android.os.AsyncTask;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import com.imepu.picssearch.emma.R;
import com.imepu.picssearch.http.PrcmApi;
import com.imepu.picssearch.http.PrcmException;
import com.imepu.picssearch.json.PrcmComment;
import com.imepu.picssearch.json.PrcmCommentsList;
import com.imepu.picssearch.task.CommentRequest;
import com.imepu.picssearch.task.TaskResult;
import java.util.ArrayList;

public abstract class PrcmListActivity extends PrcmActivity {
    private ArrayAdapter<PrcmComment> adapter;
    /* access modifiers changed from: private */
    public ArrayList<PrcmComment> arrayList = new ArrayList<>();
    private AdapterView.OnItemClickListener listClickListener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
            PrcmListActivity.this.onItemClick((PrcmComment) PrcmListActivity.this.arrayList.get(arg2));
        }
    };
    ListView listView = null;
    /* access modifiers changed from: private */
    public ProgressBar progress;
    protected int sinceId = -1;

    /* access modifiers changed from: protected */
    public abstract ArrayAdapter<PrcmComment> getCommentsListAdapter(PrcmListActivity prcmListActivity, ArrayList<PrcmComment> arrayList2);

    /* access modifiers changed from: protected */
    public abstract void onItemClick(PrcmComment prcmComment);

    public abstract void setTaskParameter(CommentRequest commentRequest);

    /* access modifiers changed from: private */
    public void apiResultComment(PrcmCommentsList list) {
        visibleList(true);
        if (!list.getComments().isEmpty()) {
            this.arrayList.addAll(list.getComments());
            this.adapter.notifyDataSetChanged();
            this.sinceId = list.getSinceId();
        }
        if (this.arrayList.isEmpty()) {
            visibleList(false);
            _findTextViewAndSetRandomText(R.id.loading_message, R.array.comment_empty_message);
        }
    }

    public void postOnCreate() {
        this.adapter = getCommentsListAdapter(this, this.arrayList);
        this.listView = (ListView) findViewById(16908298);
        this.listView.setOnItemClickListener(this.listClickListener);
        this.listView.setAdapter((ListAdapter) this.adapter);
        this.progress = (ProgressBar) findViewById(R.id.progress_bar);
        visibleList(false);
        nextCommentsLoad();
    }

    public void nextCommentsLoad() {
        this.progress.setVisibility(0);
        CommentRequest request = new CommentRequest(30, this.sinceId);
        setTaskParameter(request);
        new CommentsTask().execute(request);
    }

    public int getItemCount() {
        return this.arrayList.size();
    }

    private void visibleList(boolean bool) {
        int i;
        int i2;
        View list = findViewById(16908298);
        View loading = findViewById(R.id.loading);
        if (list != null) {
            if (bool) {
                i2 = 0;
            } else {
                i2 = 8;
            }
            list.setVisibility(i2);
        }
        if (loading != null) {
            if (bool) {
                i = 8;
            } else {
                i = 0;
            }
            loading.setVisibility(i);
        }
    }

    public class CommentsTask extends AsyncTask<CommentRequest, Void, TaskResult> {
        public static final int DEFAULT_SINCE_ID = -1;

        public CommentsTask() {
        }

        /* access modifiers changed from: protected */
        public TaskResult doInBackground(CommentRequest... value) {
            try {
                return new TaskResult().setResult(PrcmApi.comments(value[0]));
            } catch (PrcmException e) {
                PrcmException e2 = e;
                e2.printStackTrace();
                return new TaskResult().setError(e2);
            } catch (Exception e3) {
                Exception e4 = e3;
                e4.printStackTrace();
                return new TaskResult().setError(e4);
            }
        }

        /* Debug info: failed to restart local var, previous not found, register: 4 */
        /* access modifiers changed from: protected */
        public void onPostExecute(TaskResult result) {
            PrcmListActivity.this.progress.setVisibility(4);
            if (result.isNotError()) {
                PrcmListActivity.this.apiResultComment((PrcmCommentsList) result.getResult());
            } else if (result.getException() instanceof PrcmException) {
                PrcmListActivity.this._findTextViewAndSetText(R.id.loading_message, result.getException().getMessage());
            } else {
                PrcmListActivity.this._findTextViewAndSetText(R.id.loading_message, PrcmListActivity.this.getString(R.string.network_error));
            }
        }
    }
}
