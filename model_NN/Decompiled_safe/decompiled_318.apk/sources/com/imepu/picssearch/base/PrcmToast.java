package com.imepu.picssearch.base;

import android.content.Context;
import android.widget.Toast;

public class PrcmToast {
    private static Toast sToast = null;

    public static Toast getToast(Toast toast) {
        if (sToast != null) {
            sToast.cancel();
        }
        sToast = toast;
        return sToast;
    }

    public static void show(Context context, String Message) {
        getToast(Toast.makeText(context, Message, 0)).show();
    }

    public static void showLong(Context context, String Message) {
        getToast(Toast.makeText(context, Message, 1)).show();
    }
}
