package com.imepu.picssearch.base;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.imepu.picssearch.adapter.ImageGridAdapter;
import com.imepu.picssearch.emma.R;
import com.imepu.picssearch.json.PrcmPic;
import com.imepu.picssearch.json.PrcmPicsList;
import com.imepu.picssearch.task.ListRequest;
import java.util.ArrayList;
import java.util.ListIterator;

public abstract class PrcmGridActivity extends PrcmActivity {
    private ImageGridAdapter adapter;
    protected ArrayList<PrcmPic> adapterList = new ArrayList<>();
    protected GridView gridView;
    private AdapterView.OnItemClickListener listener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
            PrcmGridActivity.this.showDetail(position);
        }
    };
    private ProgressBar progress;
    protected int sinceId;

    /* access modifiers changed from: protected */
    public abstract void startTask(ListRequest listRequest);

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.grid);
        this.gridView = (GridView) findViewById(R.id.myGrid);
        this.gridView.setOnItemClickListener(this.listener);
        this.adapter = new ImageGridAdapter(this, this.adapterList);
        this.gridView.setAdapter((ListAdapter) this.adapter);
        this.progress = (ProgressBar) findViewById(R.id.progress_bar);
        visibleGrid(false);
        loadInitialPage();
    }

    public void apiResult(PrcmPicsList list) {
        visibleGrid(true);
        if (list.getPrcmPics() != null) {
            this.adapterList.addAll(list.getPrcmPics());
            this.adapter.notifyDataSetChanged();
        }
        if (this.adapterList.size() == 0) {
            visibleGrid(false);
            ((TextView) findViewById(R.id.loading_message)).setText(getString(R.string.nodata));
        }
        setSinceId(list.getSinceId());
    }

    public void setSinceId(int sinceId2) {
        this.sinceId = sinceId2;
    }

    /* access modifiers changed from: protected */
    public void loadInitialPage() {
        this.gridView.removeAllViewsInLayout();
        this.sinceId = -1;
        loadNextPage();
    }

    public void loadNextPage() {
        if (this.sinceId != 0) {
            ListRequest request = new ListRequest();
            request.setCount(50);
            request.setSinceId(Integer.valueOf(this.sinceId));
            setProgressVisibility(0);
            startTask(request);
        }
    }

    /* access modifiers changed from: protected */
    public void setProgressVisibility(int visibility) {
        if (this.progress != null) {
            this.progress.setVisibility(visibility);
        }
    }

    public void showDetail(int position) {
        ArrayList<PrcmPic> slideShowList = new ArrayList<>();
        ListIterator<PrcmPic> iterator = this.adapterList.listIterator(position);
        int i = 0;
        while (iterator.hasNext() && (i = i + 1) <= 20) {
            slideShowList.add(iterator.next());
        }
        showSlideShow(slideShowList, 0);
    }

    private void visibleGrid(boolean bool) {
        int i;
        int i2;
        View grid = findViewById(R.id.myGrid);
        View loading = findViewById(R.id.loading);
        if (grid != null) {
            if (bool) {
                i2 = 0;
            } else {
                i2 = 8;
            }
            grid.setVisibility(i2);
        }
        if (loading != null) {
            if (bool) {
                i = 8;
            } else {
                i = 0;
            }
            loading.setVisibility(i);
        }
    }
}
