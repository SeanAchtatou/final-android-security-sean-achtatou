package com.imepu.picssearch.base;

import android.content.Context;
import android.view.WindowManager;

public class PrcmDisplay {
    private static float mDip = 0.0f;
    private static int mDispHeight = 0;
    private static int mDispWidth = 0;
    private static int mDispWidthDip = 0;

    public static float getDip(Context context) {
        if (mDip == 0.0f) {
            mDip = context.getResources().getDisplayMetrics().scaledDensity;
        }
        return mDip;
    }

    public static int getWidthDip(Context context) {
        if (mDispWidthDip == 0) {
            mDispWidthDip = ((int) getDip(context)) * getWidth(context);
        }
        return mDispWidthDip;
    }

    public static int getWidth(Context context) {
        if (mDispWidth == 0) {
            mDispWidth = ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getWidth();
        }
        return mDispWidth;
    }

    public static int getHeight(Context context) {
        if (mDispHeight == 0) {
            mDispHeight = ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getHeight();
        }
        return mDispHeight;
    }
}
