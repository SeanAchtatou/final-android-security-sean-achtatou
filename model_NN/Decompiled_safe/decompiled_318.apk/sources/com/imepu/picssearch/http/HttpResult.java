package com.imepu.picssearch.http;

import org.json.JSONException;
import org.json.JSONObject;

public class HttpResult {
    public static final int STATUS_BAD_REQUEST = 400;
    public static final int STATUS_FORBIDDEN = 403;
    public static final int STATUS_INTERNAL_SERVER_ERROR = 500;
    public static final int STATUS_NOT_FOUND = 404;
    public static final int STATUS_OK = 200;
    public static final int STATUS_REQUEST_TIME_OUT = 408;
    public static final int STATUS_UNAUTHORIZED = 401;
    private String HttpBody;
    private int StatusCode;

    public String getHttpBody() {
        return this.HttpBody;
    }

    public void setHttpBody(String httpBody) {
        this.HttpBody = httpBody;
    }

    public int getStatusCode() {
        return this.StatusCode;
    }

    public void setStatusCode(int statusCode) {
        this.StatusCode = statusCode;
    }

    public String getMessage() {
        try {
            return new JSONObject(getHttpBody()).getString("message");
        } catch (JSONException e) {
            return e.getMessage().toString();
        }
    }
}
