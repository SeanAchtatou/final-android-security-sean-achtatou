package com.imepu.picssearch.http;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.imepu.picssearch.constants.Constants;
import java.io.ByteArrayOutputStream;
import java.lang.ref.SoftReference;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

public class PrcmHttpGet {
    private static final int DEFAULT_CONNECTIONT_TIME_OUT = 15000;
    private static final int DEFAULT_SOCKET_TIME_OUT = 30000;
    private static final HttpHost host = new HttpHost(Constants.API_HOST, 80, "http");

    public static HttpResult doGetRequest(String url) throws Exception {
        try {
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpParams params = httpClient.getParams();
            HttpConnectionParams.setConnectionTimeout(params, DEFAULT_CONNECTIONT_TIME_OUT);
            HttpConnectionParams.setSoTimeout(params, DEFAULT_SOCKET_TIME_OUT);
            httpClient.setParams(params);
            return _readResponse(httpClient.execute(host, new HttpGet(url)));
        } catch (OutOfMemoryError e) {
            OutOfMemoryError e2 = e;
            e2.printStackTrace();
            throw new Exception(e2.getMessage());
        }
    }

    public static HttpResult doGetRequest(String url, String user, String password) throws Exception {
        try {
            DefaultHttpClient httpClient = new DefaultHttpClient();
            httpClient.getCredentialsProvider().setCredentials(new AuthScope(Constants.API_HOST, 80), new UsernamePasswordCredentials(user, password));
            HttpParams params = httpClient.getParams();
            HttpConnectionParams.setConnectionTimeout(params, DEFAULT_CONNECTIONT_TIME_OUT);
            HttpConnectionParams.setSoTimeout(params, DEFAULT_SOCKET_TIME_OUT);
            httpClient.setParams(params);
            return _readResponse(httpClient.execute(host, new HttpGet(url)));
        } catch (OutOfMemoryError e) {
            OutOfMemoryError e2 = e;
            e2.printStackTrace();
            throw new Exception(e2.getMessage());
        }
    }

    private static HttpResult _readResponse(HttpResponse response) throws Exception {
        HttpResult result = new HttpResult();
        ByteArrayOutputStream oStream = new ByteArrayOutputStream();
        try {
            response.getEntity().writeTo(oStream);
            result.setStatusCode(response.getStatusLine().getStatusCode());
            result.setHttpBody(oStream.toString());
            return result;
        } finally {
            if (oStream != null) {
                oStream.close();
            }
        }
    }

    public static SoftReference<Bitmap> getImage(String url) {
        byte[] byteArray = getByteArrayFromURL(url);
        return new SoftReference<>(BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length));
    }

    /* JADX WARN: Type inference failed for: r10v7, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0058 A[SYNTHETIC, Splitter:B:25:0x0058] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x005d A[Catch:{ Exception -> 0x0066 }] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0062 A[Catch:{ Exception -> 0x0066 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static byte[] getByteArrayFromURL(java.lang.String r11) {
        /*
            r10 = 1024(0x400, float:1.435E-42)
            byte[] r1 = new byte[r10]
            r7 = 0
            byte[] r7 = (byte[]) r7
            r2 = 0
            r4 = 0
            r5 = 0
            r8 = 0
            java.net.URL r9 = new java.net.URL     // Catch:{ Exception -> 0x007f }
            r9.<init>(r11)     // Catch:{ Exception -> 0x007f }
            java.net.URLConnection r10 = r9.openConnection()     // Catch:{ Exception -> 0x007f }
            r0 = r10
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x007f }
            r2 = r0
            java.lang.String r10 = "GET"
            r2.setRequestMethod(r10)     // Catch:{ Exception -> 0x007f }
            r2.connect()     // Catch:{ Exception -> 0x007f }
            java.io.InputStream r4 = r2.getInputStream()     // Catch:{ Exception -> 0x007f }
            java.io.ByteArrayOutputStream r6 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x007f }
            r6.<init>()     // Catch:{ Exception -> 0x007f }
        L_0x0029:
            int r8 = r4.read(r1)     // Catch:{ Exception -> 0x0049, all -> 0x007c }
            r10 = -1
            if (r8 != r10) goto L_0x0044
            byte[] r7 = r6.toByteArray()     // Catch:{ Exception -> 0x0049, all -> 0x007c }
            if (r2 == 0) goto L_0x0039
            r2.disconnect()     // Catch:{ Exception -> 0x0071 }
        L_0x0039:
            if (r4 == 0) goto L_0x003e
            r4.close()     // Catch:{ Exception -> 0x0071 }
        L_0x003e:
            if (r6 == 0) goto L_0x0043
            r6.close()     // Catch:{ Exception -> 0x0071 }
        L_0x0043:
            return r7
        L_0x0044:
            r10 = 0
            r6.write(r1, r10, r8)     // Catch:{ Exception -> 0x0049, all -> 0x007c }
            goto L_0x0029
        L_0x0049:
            r10 = move-exception
            r3 = r10
            r5 = r6
        L_0x004c:
            r3.printStackTrace()     // Catch:{ all -> 0x0055 }
            java.lang.RuntimeException r10 = new java.lang.RuntimeException     // Catch:{ all -> 0x0055 }
            r10.<init>(r3)     // Catch:{ all -> 0x0055 }
            throw r10     // Catch:{ all -> 0x0055 }
        L_0x0055:
            r10 = move-exception
        L_0x0056:
            if (r2 == 0) goto L_0x005b
            r2.disconnect()     // Catch:{ Exception -> 0x0066 }
        L_0x005b:
            if (r4 == 0) goto L_0x0060
            r4.close()     // Catch:{ Exception -> 0x0066 }
        L_0x0060:
            if (r5 == 0) goto L_0x0065
            r5.close()     // Catch:{ Exception -> 0x0066 }
        L_0x0065:
            throw r10
        L_0x0066:
            r10 = move-exception
            r3 = r10
            r3.printStackTrace()
            java.lang.RuntimeException r10 = new java.lang.RuntimeException
            r10.<init>(r3)
            throw r10
        L_0x0071:
            r10 = move-exception
            r3 = r10
            r3.printStackTrace()
            java.lang.RuntimeException r10 = new java.lang.RuntimeException
            r10.<init>(r3)
            throw r10
        L_0x007c:
            r10 = move-exception
            r5 = r6
            goto L_0x0056
        L_0x007f:
            r10 = move-exception
            r3 = r10
            goto L_0x004c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.imepu.picssearch.http.PrcmHttpGet.getByteArrayFromURL(java.lang.String):byte[]");
    }
}
