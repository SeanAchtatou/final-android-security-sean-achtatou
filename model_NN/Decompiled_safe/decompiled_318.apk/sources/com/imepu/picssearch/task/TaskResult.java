package com.imepu.picssearch.task;

public class TaskResult {
    private Exception exception;
    private boolean isError = false;
    private Object result;

    public TaskResult setError(Exception e) {
        this.exception = e;
        this.isError = true;
        return this;
    }

    public TaskResult setResult(Object obj) {
        this.result = obj;
        return this;
    }

    public Object getResult() {
        return this.result;
    }

    public boolean isError() {
        return this.isError;
    }

    public boolean isNotError() {
        return !isError();
    }

    public Exception getException() {
        return this.exception;
    }
}
