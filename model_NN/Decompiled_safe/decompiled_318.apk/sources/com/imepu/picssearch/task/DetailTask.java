package com.imepu.picssearch.task;

import android.os.AsyncTask;
import com.imepu.picssearch.SlideShowActivity;
import com.imepu.picssearch.base.PrcmToast;
import com.imepu.picssearch.http.PrcmApi;
import com.imepu.picssearch.json.PrcmDetail;

public class DetailTask extends AsyncTask<String, Void, TaskResult> {
    private SlideShowActivity context;

    public DetailTask(SlideShowActivity context2) {
        this.context = context2;
    }

    /* access modifiers changed from: protected */
    public TaskResult doInBackground(String... value) {
        try {
            return new TaskResult().setResult(PrcmApi.detail(value[0]));
        } catch (Exception e) {
            Exception e2 = e;
            e2.printStackTrace();
            return new TaskResult().setError(e2);
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    /* access modifiers changed from: protected */
    public void onPostExecute(TaskResult result) {
        if (result.isError()) {
            PrcmToast.show(this.context, result.getException().getMessage());
        } else {
            this.context.apiResult((PrcmDetail) result.getResult());
        }
    }
}
