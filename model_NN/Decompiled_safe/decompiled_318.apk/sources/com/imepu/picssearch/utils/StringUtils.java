package com.imepu.picssearch.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class StringUtils {
    public static boolean isEmpty(String str) {
        if (str == null || str.equals("")) {
            return true;
        }
        return false;
    }

    public static boolean isNotEmpty(String str) {
        return !isEmpty(str);
    }

    public static String shortenStr(String str, int num) {
        if (str.length() <= num) {
            return str;
        }
        return String.valueOf(str.substring(0, num)) + "...";
    }

    public static String inputStreemToString(InputStream in) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
        StringBuffer buf = new StringBuffer();
        while (true) {
            String str = reader.readLine();
            if (str == null) {
                return buf.toString();
            }
            buf.append(str);
            buf.append("\n");
        }
    }
}
