package com.imepu.picssearch.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import java.lang.ref.SoftReference;

public class ImageCache {
    public static SoftReference<Bitmap> getImage(Context context, String key) {
        Log.d("ImageCache::getImage", key);
        return CacheUtils.getFile(context, key);
    }

    public static void setImage(Context context, String key, Bitmap bitmap) {
        try {
            Log.d("ImageCache::setImage", key);
            CacheUtils.saveBitmap(context, key, bitmap);
        } catch (Exception e) {
            e.printStackTrace();
        } catch (OutOfMemoryError e2) {
            e2.printStackTrace();
        }
    }

    public static void clear(Context context) {
        CacheUtils.deleteAll(context);
    }
}
