package com.imepu.picssearch.utils;

import android.content.Context;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.imepu.picssearch.constants.Constants;

public final class Analytics {
    public static GoogleAnalyticsTracker getTracker(Context context) {
        GoogleAnalyticsTracker tracker = GoogleAnalyticsTracker.getInstance();
        tracker.start(Constants.ANALYTICS_ID, 60, context);
        return tracker;
    }

    public static void Track(GoogleAnalyticsTracker tracker, String app_name) {
        StackTraceElement element = Thread.currentThread().getStackTrace()[3];
        String tag = element.getFileName();
        tracker.trackPageView("/" + app_name + "/" + tag + "/" + element.getMethodName());
    }

    public static void stop(GoogleAnalyticsTracker tracker) {
        tracker.stop();
    }
}
