package com.imepu.picssearch.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.File;
import java.lang.ref.SoftReference;

public class CacheUtils {
    private static String getFileName(String url) {
        return String.valueOf(url.hashCode());
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x001f A[SYNTHETIC, Splitter:B:9:0x001f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void saveByteData(android.content.Context r8, java.lang.String r9, byte[] r10) {
        /*
            java.lang.String r3 = getFileName(r9)
            r4 = 0
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x001b }
            java.io.File r6 = r8.getCacheDir()     // Catch:{ Exception -> 0x001b }
            r2.<init>(r6, r3)     // Catch:{ Exception -> 0x001b }
            java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x001b }
            r5.<init>(r2)     // Catch:{ Exception -> 0x001b }
            r5.write(r10)     // Catch:{ Exception -> 0x002c }
            r5.close()     // Catch:{ Exception -> 0x002c }
            r4 = r5
        L_0x001a:
            return
        L_0x001b:
            r6 = move-exception
            r0 = r6
        L_0x001d:
            if (r4 == 0) goto L_0x001a
            r4.close()     // Catch:{ IOException -> 0x0023 }
            goto L_0x001a
        L_0x0023:
            r1 = move-exception
            java.lang.String r6 = "save"
            java.lang.String r7 = "finally"
            android.util.Log.w(r6, r7)
            goto L_0x001a
        L_0x002c:
            r6 = move-exception
            r0 = r6
            r4 = r5
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.imepu.picssearch.utils.CacheUtils.saveByteData(android.content.Context, java.lang.String, byte[]):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0023 A[SYNTHETIC, Splitter:B:9:0x0023] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void saveBitmap(android.content.Context r8, java.lang.String r9, android.graphics.Bitmap r10) {
        /*
            java.lang.String r3 = getFileName(r9)
            r4 = 0
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x001f }
            java.io.File r6 = r8.getCacheDir()     // Catch:{ Exception -> 0x001f }
            r2.<init>(r6, r3)     // Catch:{ Exception -> 0x001f }
            java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x001f }
            r5.<init>(r2)     // Catch:{ Exception -> 0x001f }
            android.graphics.Bitmap$CompressFormat r6 = android.graphics.Bitmap.CompressFormat.PNG     // Catch:{ Exception -> 0x0030 }
            r7 = 90
            r10.compress(r6, r7, r5)     // Catch:{ Exception -> 0x0030 }
            r5.close()     // Catch:{ Exception -> 0x0030 }
            r4 = r5
        L_0x001e:
            return
        L_0x001f:
            r6 = move-exception
            r0 = r6
        L_0x0021:
            if (r4 == 0) goto L_0x001e
            r4.close()     // Catch:{ IOException -> 0x0027 }
            goto L_0x001e
        L_0x0027:
            r1 = move-exception
            java.lang.String r6 = "save"
            java.lang.String r7 = "finally"
            android.util.Log.w(r6, r7)
            goto L_0x001e
        L_0x0030:
            r6 = move-exception
            r0 = r6
            r4 = r5
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: com.imepu.picssearch.utils.CacheUtils.saveBitmap(android.content.Context, java.lang.String, android.graphics.Bitmap):void");
    }

    public static SoftReference<Bitmap> getFile(Context context, String url) {
        try {
            return new SoftReference<>(BitmapFactory.decodeFile(context.getCacheDir() + "/" + getFileName(url)));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } catch (OutOfMemoryError e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static void deleteAll(Context context) {
        File dir = context.getCacheDir();
        if (dir.isDirectory()) {
            for (File file : dir.listFiles()) {
                if (file.isFile()) {
                    file.delete();
                }
            }
        }
    }
}
