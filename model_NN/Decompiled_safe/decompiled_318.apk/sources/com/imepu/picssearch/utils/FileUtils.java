package com.imepu.picssearch.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;
import java.io.File;
import java.lang.ref.SoftReference;

public class FileUtils {
    public static String getFileName(String url) {
        int point = url.lastIndexOf("/");
        if (point != -1) {
            return url.substring(point + 1, url.length());
        }
        return String.valueOf(url.hashCode());
    }

    public static SoftReference<Bitmap> getFile(Context context, String url) {
        try {
            SoftReference<Bitmap> bitmap = new SoftReference<>(BitmapFactory.decodeFile(String.valueOf(getDownloadPath()) + "/" + getFileName(url)));
            if (bitmap.get() != null) {
                return bitmap;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } catch (OutOfMemoryError e2) {
            e2.printStackTrace();
        }
        return null;
    }

    /* JADX INFO: Multiple debug info for r8v7 java.lang.String: [D('contentResolver' android.content.ContentResolver), D('filePath' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r0v6 byte[]: [D('DEFAULT_BUFFER_SIZE' int), D('buffer' byte[])] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0094 A[Catch:{ all -> 0x0103 }] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0099 A[SYNTHETIC, Splitter:B:31:0x0099] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x009e A[SYNTHETIC, Splitter:B:34:0x009e] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00c1 A[SYNTHETIC, Splitter:B:44:0x00c1] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00c6 A[SYNTHETIC, Splitter:B:47:0x00c6] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x011b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String saveImage(android.content.Context r7, android.content.ContentResolver r8, java.lang.String r9, java.lang.String r10, java.io.InputStream r11) {
        /*
            r3 = 0
            r0 = 0
            r2 = 0
            makeDownloadDir()     // Catch:{ Exception -> 0x0109, all -> 0x00ba }
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x0109, all -> 0x00ba }
            java.lang.String r4 = getDownloadPath()     // Catch:{ Exception -> 0x0109, all -> 0x00ba }
            r1.<init>(r4, r9)     // Catch:{ Exception -> 0x0109, all -> 0x00ba }
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x010f, all -> 0x00f1 }
            r4.<init>(r1)     // Catch:{ Exception -> 0x010f, all -> 0x00f1 }
            r0 = 4096(0x1000, float:5.74E-42)
            byte[] r0 = new byte[r0]     // Catch:{ Exception -> 0x0084, all -> 0x00f7 }
            r3 = 0
        L_0x0019:
            r5 = -1
            int r3 = r11.read(r0)     // Catch:{ Exception -> 0x0084, all -> 0x00f7 }
            if (r5 != r3) goto L_0x007f
            r11.close()     // Catch:{ Exception -> 0x0084, all -> 0x00f7 }
            r4.close()     // Catch:{ Exception -> 0x0084, all -> 0x00f7 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0084, all -> 0x00f7 }
            java.lang.String r3 = getDownloadPath()     // Catch:{ Exception -> 0x0084, all -> 0x00f7 }
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ Exception -> 0x0084, all -> 0x00f7 }
            r0.<init>(r3)     // Catch:{ Exception -> 0x0084, all -> 0x00f7 }
            java.lang.String r3 = "/"
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x0084, all -> 0x00f7 }
            java.lang.StringBuilder r0 = r0.append(r9)     // Catch:{ Exception -> 0x0084, all -> 0x00f7 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0084, all -> 0x00f7 }
            android.content.ContentValues r2 = new android.content.ContentValues     // Catch:{ Exception -> 0x0115, all -> 0x00fd }
            r3 = 7
            r2.<init>(r3)     // Catch:{ Exception -> 0x0115, all -> 0x00fd }
            java.lang.String r3 = "title"
            r2.put(r3, r10)     // Catch:{ Exception -> 0x0115, all -> 0x00fd }
            java.lang.String r10 = "_display_name"
            r2.put(r10, r9)     // Catch:{ Exception -> 0x0115, all -> 0x00fd }
            java.lang.String r10 = "datetaken"
            long r5 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0115, all -> 0x00fd }
            java.lang.Long r3 = java.lang.Long.valueOf(r5)     // Catch:{ Exception -> 0x0115, all -> 0x00fd }
            r2.put(r10, r3)     // Catch:{ Exception -> 0x0115, all -> 0x00fd }
            java.lang.String r10 = "mime_type"
            java.lang.String r9 = getMimeType(r9)     // Catch:{ Exception -> 0x0115, all -> 0x00fd }
            r2.put(r10, r9)     // Catch:{ Exception -> 0x0115, all -> 0x00fd }
            java.lang.String r9 = "_data"
            r2.put(r9, r0)     // Catch:{ Exception -> 0x0115, all -> 0x00fd }
            android.net.Uri r9 = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI     // Catch:{ Exception -> 0x0115, all -> 0x00fd }
            r8.insert(r9, r2)     // Catch:{ Exception -> 0x0115, all -> 0x00fd }
            if (r4 == 0) goto L_0x0076
            r4.close()     // Catch:{ Throwable -> 0x00dc }
        L_0x0076:
            if (r11 == 0) goto L_0x00ed
            r11.close()     // Catch:{ Throwable -> 0x00e5 }
            r8 = r0
            r7 = r1
            r9 = r4
        L_0x007e:
            return r8
        L_0x007f:
            r5 = 0
            r4.write(r0, r5, r3)     // Catch:{ Exception -> 0x0084, all -> 0x00f7 }
            goto L_0x0019
        L_0x0084:
            r8 = move-exception
            r10 = r2
            r9 = r1
            r0 = r4
        L_0x0088:
            java.lang.String r1 = "I'm sorry. Error."
            com.imepu.picssearch.base.PrcmToast.showLong(r7, r1)     // Catch:{ all -> 0x0103 }
            java.lang.String r7 = "save"
            android.util.Log.w(r7, r8)     // Catch:{ all -> 0x0103 }
            if (r9 == 0) goto L_0x0097
            r9.delete()     // Catch:{ all -> 0x0103 }
        L_0x0097:
            if (r0 == 0) goto L_0x009c
            r0.close()     // Catch:{ Throwable -> 0x00a5 }
        L_0x009c:
            if (r11 == 0) goto L_0x011b
            r11.close()     // Catch:{ Throwable -> 0x00ae }
            r8 = r10
            r7 = r9
            r9 = r0
            goto L_0x007e
        L_0x00a5:
            r7 = move-exception
            java.lang.String r7 = "save"
            java.lang.String r8 = "finally"
            android.util.Log.w(r7, r8)
            goto L_0x009c
        L_0x00ae:
            r7 = move-exception
            java.lang.String r7 = "save"
            java.lang.String r8 = "finally"
            android.util.Log.w(r7, r8)
            r8 = r10
            r7 = r9
            r9 = r0
            goto L_0x007e
        L_0x00ba:
            r7 = move-exception
            r10 = r7
            r8 = r2
            r9 = r3
            r7 = r0
        L_0x00bf:
            if (r9 == 0) goto L_0x00c4
            r9.close()     // Catch:{ Throwable -> 0x00ca }
        L_0x00c4:
            if (r11 == 0) goto L_0x00c9
            r11.close()     // Catch:{ Throwable -> 0x00d3 }
        L_0x00c9:
            throw r10
        L_0x00ca:
            r7 = move-exception
            java.lang.String r7 = "save"
            java.lang.String r8 = "finally"
            android.util.Log.w(r7, r8)
            goto L_0x00c4
        L_0x00d3:
            r7 = move-exception
            java.lang.String r7 = "save"
            java.lang.String r8 = "finally"
            android.util.Log.w(r7, r8)
            goto L_0x00c9
        L_0x00dc:
            r7 = move-exception
            java.lang.String r7 = "save"
            java.lang.String r8 = "finally"
            android.util.Log.w(r7, r8)
            goto L_0x0076
        L_0x00e5:
            r7 = move-exception
            java.lang.String r7 = "save"
            java.lang.String r8 = "finally"
            android.util.Log.w(r7, r8)
        L_0x00ed:
            r8 = r0
            r7 = r1
            r9 = r4
            goto L_0x007e
        L_0x00f1:
            r7 = move-exception
            r10 = r7
            r8 = r2
            r9 = r3
            r7 = r1
            goto L_0x00bf
        L_0x00f7:
            r7 = move-exception
            r10 = r7
            r8 = r2
            r9 = r4
            r7 = r1
            goto L_0x00bf
        L_0x00fd:
            r7 = move-exception
            r10 = r7
            r8 = r0
            r9 = r4
            r7 = r1
            goto L_0x00bf
        L_0x0103:
            r7 = move-exception
            r8 = r10
            r10 = r7
            r7 = r9
            r9 = r0
            goto L_0x00bf
        L_0x0109:
            r8 = move-exception
            r10 = r2
            r9 = r0
            r0 = r3
            goto L_0x0088
        L_0x010f:
            r8 = move-exception
            r10 = r2
            r9 = r1
            r0 = r3
            goto L_0x0088
        L_0x0115:
            r8 = move-exception
            r10 = r0
            r9 = r1
            r0 = r4
            goto L_0x0088
        L_0x011b:
            r8 = r10
            r7 = r9
            r9 = r0
            goto L_0x007e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.imepu.picssearch.utils.FileUtils.saveImage(android.content.Context, android.content.ContentResolver, java.lang.String, java.lang.String, java.io.InputStream):java.lang.String");
    }

    private static String getSdCardPath() {
        return Environment.getExternalStorageDirectory().toString();
    }

    private static String getDownloadPath() {
        return String.valueOf(getSdCardPath()) + "/download";
    }

    private static void makeDownloadDir() {
        makeDir(new File(getDownloadPath()));
    }

    private static void makeDir(File dir) {
        if (!dir.exists()) {
            dir.mkdir();
            Log.d("save", String.valueOf(dir.toString()) + " create");
        }
    }

    private static String getMimeType(String filename) {
        int point = filename.lastIndexOf(".");
        if (point != -1) {
            String extension = filename.substring(point + 1, filename.length());
            if (extension.equals("jpeg") || extension.equals("jpg")) {
                return "image/jpeg";
            }
            if (extension.equals("png")) {
                return "image/png";
            }
            if (extension.equals("gif")) {
                return "image/gif";
            }
        }
        return "image/jpeg";
    }
}
