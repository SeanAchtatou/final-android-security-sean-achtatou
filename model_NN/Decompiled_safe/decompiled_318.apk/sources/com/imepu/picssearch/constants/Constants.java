package com.imepu.picssearch.constants;

public class Constants {
    public static final String ADMOB_ID = "a14e3575973adef";
    public static final String ANALYTICS_ID = "UA-24950801-1";
    public static final String API_HOST = "api.prcm.jp";
    public static final String HOST = "prcm.jp";
    public static final String H_HOST = "r0.prcm.jp";
    public static final String IMAGE_DOWNLOAD_SERVICE = "jp.sharakova.android.picssearch.service.ImageDownloadService";
    public static final String MY_ALBUM_URL = "http://prcm.jp/pics/my-album";
    public static final int REQUEST_EDIT_CODE = 2;
    public static final int REQUEST_GALLERY_CODE = 1;
    public static final int REQUEST_SHARE_CODE = 3;
    public static final String TOPPAGE_URL = "http://prcm.jp/pics";
}
