package com.imepu.picssearch.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.HorizontalScrollView;

public class HSView extends HorizontalScrollView {
    private static final int SWIPE_MIN_DISTANCE = 5;
    private static final int SWIPE_THRESHOLD_VELOCITY = 300;
    /* access modifiers changed from: private */
    public int mActiveFeature = 0;
    /* access modifiers changed from: private */
    public GestureDetector mGestureDetector;
    /* access modifiers changed from: private */
    public int mItemSize = 0;
    private Runnable pageChangeRunnable;
    private View.OnTouchListener touchListener = new View.OnTouchListener() {
        public boolean onTouch(View v, MotionEvent event) {
            if (HSView.this.mGestureDetector.onTouchEvent(event)) {
                return true;
            }
            if (event.getAction() != 1 && event.getAction() != 3) {
                return false;
            }
            int scrollX = HSView.this.getScrollX();
            int featureWidth = v.getMeasuredWidth();
            HSView.this.mActiveFeature = ((featureWidth / 2) + scrollX) / featureWidth;
            HSView.this.movePage(HSView.this.mActiveFeature);
            return true;
        }
    };

    public HSView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public HSView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public HSView(Context context) {
        super(context);
        init();
    }

    private void init() {
        setOnTouchListener(this.touchListener);
        this.mGestureDetector = new GestureDetector(new MyGestureDetector());
    }

    public void setPageSize(int pageNo) {
        this.mItemSize = pageNo;
    }

    public int getPageNo() {
        return this.mActiveFeature;
    }

    public boolean hasNextPage() {
        return this.mActiveFeature < this.mItemSize - 1;
    }

    public boolean hasPrevPage() {
        return this.mActiveFeature > 0;
    }

    public void moveLeftPage() {
        if (hasPrevPage()) {
            movePage(this.mActiveFeature - 1);
        }
    }

    public void moveRightPage() {
        if (hasNextPage()) {
            movePage(this.mActiveFeature + 1);
        }
    }

    /* access modifiers changed from: private */
    public void movePage(int pageNo) {
        this.mActiveFeature = pageNo;
        smoothScrollTo(this.mActiveFeature * getMeasuredWidth(), 0);
        callRunnable();
    }

    public void setPageChangeRunnable(Runnable callback) {
        this.pageChangeRunnable = callback;
    }

    class MyGestureDetector extends GestureDetector.SimpleOnGestureListener {
        MyGestureDetector() {
        }

        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            int i;
            try {
                if (e1.getX() - e2.getX() <= 5.0f || Math.abs(velocityX) <= 300.0f) {
                    if (e2.getX() - e1.getX() > 5.0f && Math.abs(velocityX) > 300.0f) {
                        HSView hSView = HSView.this;
                        if (HSView.this.mActiveFeature > 0) {
                            i = HSView.this.mActiveFeature - 1;
                        } else {
                            i = 0;
                        }
                        hSView.mActiveFeature = i;
                        HSView.this.movePage(HSView.this.mActiveFeature);
                        return true;
                    }
                    return false;
                }
                HSView.this.mActiveFeature = HSView.this.mActiveFeature < HSView.this.mItemSize - 1 ? HSView.this.mActiveFeature + 1 : HSView.this.mItemSize - 1;
                HSView.this.movePage(HSView.this.mActiveFeature);
                return true;
            } catch (Exception e) {
                Log.e("Fling", "There was an error processing the Fling event:" + e.getMessage());
            }
        }
    }

    private void callRunnable() {
        if (this.pageChangeRunnable != null) {
            this.pageChangeRunnable.run();
        }
    }
}
