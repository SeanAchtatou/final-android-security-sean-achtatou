package com.imepu.picssearch.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import com.imepu.picssearch.constants.Constants;
import com.imepu.picssearch.emma.R;
import com.imepu.picssearch.utils.FileUtils;
import java.util.ArrayList;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class ImageDownloadService extends Service {
    /* access modifiers changed from: private */
    public static ArrayList<String> downloadTask = new ArrayList<>();
    /* access modifiers changed from: private */
    public String imageTitle;
    /* access modifiers changed from: private */
    public String imageUrl;
    private final IBinder mBinder = new LocalBinder();
    /* access modifiers changed from: private */
    public NotificationManager mNM;

    public class LocalBinder extends Binder {
        public LocalBinder() {
        }

        /* access modifiers changed from: package-private */
        public ImageDownloadService getService() {
            return ImageDownloadService.this;
        }
    }

    public void onCreate() {
        this.mNM = (NotificationManager) getSystemService("notification");
    }

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        if (intent != null && Constants.IMAGE_DOWNLOAD_SERVICE.equals(intent.getAction())) {
            this.imageUrl = intent.getStringExtra("image_url");
            this.imageTitle = intent.getStringExtra("image_title");
            downloadTask.add(this.imageUrl);
            showNotification(this.imageTitle, this.imageUrl);
            new Thread(null, new Runnable() {
                public void run() {
                    String path = ImageDownloadService.this.imageUrl;
                    String title = ImageDownloadService.this.imageTitle;
                    String message = null;
                    try {
                        FileUtils.saveImage(ImageDownloadService.this.getApplicationContext(), ImageDownloadService.this.getContentResolver(), FileUtils.getFileName(path), ImageDownloadService.this.imageTitle, new DefaultHttpClient().execute(new HttpGet(path)).getEntity().getContent());
                        message = String.valueOf(title) + " " + ImageDownloadService.this.getString(R.string.download_image_service_complete);
                    } catch (Exception e) {
                        message = ImageDownloadService.this.getString(R.string.download_image_service_error);
                    } finally {
                        ImageDownloadService.downloadTask.remove(path);
                        ImageDownloadService.this.mNM.cancel(path.hashCode());
                        ImageDownloadService.this.showNotification(message, path);
                    }
                }
            }, "ImageDownloadService").start();
        }
    }

    public void onDestroy() {
        this.mNM.cancelAll();
    }

    public IBinder onBind(Intent intent) {
        return this.mBinder;
    }

    /* access modifiers changed from: private */
    public void showNotification(String message, String filePath) {
        Notification notification = new Notification(R.drawable.icon, message, System.currentTimeMillis());
        notification.setLatestEventInfo(this, getString(R.string.download_image_service_message), message, PendingIntent.getActivity(getApplicationContext(), 0, null, 0));
        this.mNM.notify(filePath.hashCode(), notification);
    }
}
