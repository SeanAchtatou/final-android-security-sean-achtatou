package oauth.signpost.basic;

import cn.com.mzba.service.SystemConfig;
import commonshttp.HttpRequestAdapter;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import oauth.signpost.AbstractOAuthProvider;
import oauth.signpost.http.HttpRequest;
import oauth.signpost.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;

public class DefaultOAuthProvider extends AbstractOAuthProvider {
    private static final long serialVersionUID = 1;

    public DefaultOAuthProvider(String requestTokenEndpointUrl, String accessTokenEndpointUrl, String authorizationWebsiteUrl) {
        super(requestTokenEndpointUrl, accessTokenEndpointUrl, authorizationWebsiteUrl);
    }

    /* access modifiers changed from: protected */
    public HttpRequest createRequest(String endpointUrl) throws MalformedURLException, IOException {
        HttpURLConnection connection = (HttpURLConnection) new URL(endpointUrl).openConnection();
        connection.setRequestMethod(SystemConfig.HTTP_POST);
        connection.setAllowUserInteraction(false);
        connection.setRequestProperty("Content-Length", "0");
        return new HttpURLConnectionRequestAdapter(connection);
    }

    /* access modifiers changed from: protected */
    public HttpResponse sendRequest(HttpRequest request) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) request.unwrap();
        connection.connect();
        return new HttpURLConnectionResponseAdapter(connection);
    }

    /* access modifiers changed from: protected */
    public void closeConnection(HttpRequest request, HttpResponse response) {
        HttpURLConnection connection = (HttpURLConnection) request.unwrap();
        if (connection != null) {
            connection.disconnect();
        }
    }

    /* access modifiers changed from: protected */
    public HttpResponse sendRequestForTencent(HttpRequest request) throws Exception {
        return null;
    }

    /* access modifiers changed from: protected */
    public HttpRequest createRequestForTencent(String endpointUrl) throws Exception {
        HttpPost request = new HttpPost(endpointUrl);
        request.addHeader("Content-Type", "application/x-www-form-urlencoded");
        return new HttpRequestAdapter(request);
    }
}
