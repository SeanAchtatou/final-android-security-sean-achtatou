package oauth.signpost;

import java.util.HashMap;
import java.util.Map;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import oauth.signpost.exception.OAuthNotAuthorizedException;
import oauth.signpost.http.HttpParameters;
import oauth.signpost.http.HttpRequest;
import oauth.signpost.http.HttpResponse;

public abstract class AbstractOAuthProvider implements OAuthProvider {
    private static final long serialVersionUID = 1;
    private String accessTokenEndpointUrl;
    private String authorizationWebsiteUrl;
    private Map<String, String> defaultHeaders = new HashMap();
    private boolean isOAuth10a;
    private transient OAuthProviderListener listener;
    private String requestTokenEndpointUrl;
    private HttpParameters responseParameters = new HttpParameters();

    /* access modifiers changed from: protected */
    public abstract HttpRequest createRequest(String str) throws Exception;

    /* access modifiers changed from: protected */
    public abstract HttpRequest createRequestForTencent(String str) throws Exception;

    /* access modifiers changed from: protected */
    public abstract HttpResponse sendRequest(HttpRequest httpRequest) throws Exception;

    /* access modifiers changed from: protected */
    public abstract HttpResponse sendRequestForTencent(HttpRequest httpRequest) throws Exception;

    public AbstractOAuthProvider(String requestTokenEndpointUrl2, String accessTokenEndpointUrl2, String authorizationWebsiteUrl2) {
        this.requestTokenEndpointUrl = requestTokenEndpointUrl2;
        this.accessTokenEndpointUrl = accessTokenEndpointUrl2;
        this.authorizationWebsiteUrl = authorizationWebsiteUrl2;
    }

    public String retrieveRequestToken(OAuthConsumer consumer, String callbackUrl) throws OAuthMessageSignerException, OAuthNotAuthorizedException, OAuthExpectationFailedException, OAuthCommunicationException {
        consumer.setTokenWithSecret(null, null);
        retrieveToken(consumer, this.requestTokenEndpointUrl, OAuth.OAUTH_CALLBACK, callbackUrl);
        String callbackConfirmed = this.responseParameters.getFirst(OAuth.OAUTH_CALLBACK_CONFIRMED);
        this.responseParameters.remove((Object) OAuth.OAUTH_CALLBACK_CONFIRMED);
        this.isOAuth10a = Boolean.TRUE.toString().equals(callbackConfirmed);
        if (this.isOAuth10a) {
            return OAuth.addQueryParameters(this.authorizationWebsiteUrl, "oauth_token", consumer.getToken());
        }
        return OAuth.addQueryParameters(this.authorizationWebsiteUrl, "oauth_token", consumer.getToken(), OAuth.OAUTH_CALLBACK, callbackUrl);
    }

    public String retrieveRequestTokenForTencent(OAuthConsumer consumer, String callbackUrl) throws OAuthMessageSignerException, OAuthNotAuthorizedException, OAuthExpectationFailedException, OAuthCommunicationException {
        consumer.setTokenWithSecret(null, null);
        retrieveTokenForTencent(consumer, this.requestTokenEndpointUrl, OAuth.OAUTH_CALLBACK, callbackUrl);
        String callbackConfirmed = this.responseParameters.getFirst(OAuth.OAUTH_CALLBACK_CONFIRMED);
        this.responseParameters.remove((Object) OAuth.OAUTH_CALLBACK_CONFIRMED);
        this.isOAuth10a = Boolean.TRUE.toString().equals(callbackConfirmed);
        if (this.isOAuth10a) {
            return OAuth.addQueryParameters(this.authorizationWebsiteUrl, "oauth_token", consumer.getToken());
        }
        return OAuth.addQueryParameters(this.authorizationWebsiteUrl, "oauth_token", consumer.getToken(), OAuth.OAUTH_CALLBACK, callbackUrl);
    }

    public void retrieveAccessToken(OAuthConsumer consumer, String oauthVerifier) throws OAuthMessageSignerException, OAuthNotAuthorizedException, OAuthExpectationFailedException, OAuthCommunicationException {
        if (consumer.getToken() == null || consumer.getTokenSecret() == null) {
            throw new OAuthExpectationFailedException("Authorized request token or token secret not set. Did you retrieve an authorized request token before?");
        } else if (!this.isOAuth10a || oauthVerifier == null) {
            retrieveToken(consumer, this.accessTokenEndpointUrl, new String[0]);
        } else {
            retrieveToken(consumer, this.accessTokenEndpointUrl, OAuth.OAUTH_VERIFIER, oauthVerifier);
        }
    }

    public void retrieveAccessTokenForTencent(OAuthConsumer consumer, String oauthVerifier) throws OAuthMessageSignerException, OAuthNotAuthorizedException, OAuthExpectationFailedException, OAuthCommunicationException {
        if (consumer.getToken() == null || consumer.getTokenSecret() == null) {
            throw new OAuthExpectationFailedException("Authorized request token or token secret not set. Did you retrieve an authorized request token before?");
        } else if (!this.isOAuth10a || oauthVerifier == null) {
            retrieveTokenForTencent(consumer, this.accessTokenEndpointUrl, new String[0]);
        } else {
            retrieveTokenForTencent(consumer, this.accessTokenEndpointUrl, OAuth.OAUTH_VERIFIER, oauthVerifier);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: oauth.signpost.http.HttpParameters.putAll(java.lang.String[], boolean):void
     arg types: [java.lang.String[], int]
     candidates:
      oauth.signpost.http.HttpParameters.putAll(java.util.Map<? extends java.lang.String, ? extends java.util.SortedSet<java.lang.String>>, boolean):void
      oauth.signpost.http.HttpParameters.putAll(java.lang.String[], boolean):void */
    /* access modifiers changed from: protected */
    public void retrieveToken(OAuthConsumer consumer, String endpointUrl, String... additionalParameters) throws OAuthMessageSignerException, OAuthCommunicationException, OAuthNotAuthorizedException, OAuthExpectationFailedException {
        Map<String, String> defaultHeaders2 = getRequestHeaders();
        if (consumer.getConsumerKey() == null || consumer.getConsumerSecret() == null) {
            throw new OAuthExpectationFailedException("Consumer key or secret not set");
        }
        HttpRequest request = null;
        HttpResponse response = null;
        try {
            request = createRequest(endpointUrl);
            for (String header : defaultHeaders2.keySet()) {
                request.setHeader(header, defaultHeaders2.get(header));
            }
            if (additionalParameters != null) {
                HttpParameters httpParams = new HttpParameters();
                httpParams.putAll(additionalParameters, true);
                consumer.setAdditionalParameters(httpParams);
            }
            if (this.listener != null) {
                this.listener.prepareRequest(request);
            }
            consumer.sign(request);
            if (this.listener != null) {
                this.listener.prepareSubmission(request);
            }
            response = sendRequest(request);
            int statusCode = response.getStatusCode();
            boolean requestHandled = false;
            if (this.listener != null) {
                requestHandled = this.listener.onResponseReceived(request, response);
            }
            if (requestHandled) {
                try {
                    closeConnection(request, response);
                } catch (Exception e) {
                    throw new OAuthCommunicationException(e);
                }
            } else {
                if (statusCode >= 300) {
                    handleUnexpectedResponse(statusCode, response);
                }
                HttpParameters responseParams = OAuth.decodeForm(response.getContent());
                String token = responseParams.getFirst("oauth_token");
                String secret = responseParams.getFirst("oauth_token_secret");
                responseParams.remove((Object) "oauth_token");
                responseParams.remove((Object) "oauth_token_secret");
                setResponseParameters(responseParams);
                if (token == null || secret == null) {
                    throw new OAuthExpectationFailedException("Request token or token secret not set in server reply. The service provider you use is probably buggy.");
                }
                consumer.setTokenWithSecret(token, secret);
                try {
                    closeConnection(request, response);
                } catch (Exception e2) {
                    throw new OAuthCommunicationException(e2);
                }
            }
        } catch (OAuthNotAuthorizedException e3) {
            throw e3;
        } catch (OAuthExpectationFailedException e4) {
            throw e4;
        } catch (Exception e5) {
            throw new OAuthCommunicationException(e5);
        } catch (Throwable th) {
            try {
                closeConnection(request, response);
                throw th;
            } catch (Exception e6) {
                throw new OAuthCommunicationException(e6);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: oauth.signpost.http.HttpParameters.putAll(java.lang.String[], boolean):void
     arg types: [java.lang.String[], int]
     candidates:
      oauth.signpost.http.HttpParameters.putAll(java.util.Map<? extends java.lang.String, ? extends java.util.SortedSet<java.lang.String>>, boolean):void
      oauth.signpost.http.HttpParameters.putAll(java.lang.String[], boolean):void */
    /* access modifiers changed from: protected */
    public void retrieveTokenForTencent(OAuthConsumer consumer, String endpointUrl, String... additionalParameters) throws OAuthMessageSignerException, OAuthCommunicationException, OAuthNotAuthorizedException, OAuthExpectationFailedException {
        Map<String, String> defaultHeaders2 = getRequestHeaders();
        if (consumer.getConsumerKey() == null || consumer.getConsumerSecret() == null) {
            throw new OAuthExpectationFailedException("Consumer key or secret not set");
        }
        HttpRequest request = null;
        HttpResponse response = null;
        try {
            request = createRequestForTencent(endpointUrl);
            for (String header : defaultHeaders2.keySet()) {
                request.setHeader(header, defaultHeaders2.get(header));
            }
            if (additionalParameters != null) {
                HttpParameters httpParams = new HttpParameters();
                httpParams.putAll(additionalParameters, true);
                consumer.setAdditionalParameters(httpParams);
            }
            if (this.listener != null) {
                this.listener.prepareRequest(request);
            }
            consumer.sign(request);
            if (this.listener != null) {
                this.listener.prepareSubmission(request);
            }
            response = sendRequestForTencent(request);
            int statusCode = response.getStatusCode();
            boolean requestHandled = false;
            if (this.listener != null) {
                requestHandled = this.listener.onResponseReceived(request, response);
            }
            if (requestHandled) {
                try {
                    closeConnection(request, response);
                } catch (Exception e) {
                    throw new OAuthCommunicationException(e);
                }
            } else {
                if (statusCode >= 300) {
                    handleUnexpectedResponse(statusCode, response);
                }
                HttpParameters responseParams = OAuth.decodeForm(response.getContent());
                String token = responseParams.getFirst("oauth_token");
                String secret = responseParams.getFirst("oauth_token_secret");
                responseParams.remove((Object) "oauth_token");
                responseParams.remove((Object) "oauth_token_secret");
                setResponseParameters(responseParams);
                if (token == null || secret == null) {
                    throw new OAuthExpectationFailedException("Request token or token secret not set in server reply. The service provider you use is probably buggy.");
                }
                consumer.setTokenWithSecret(token, secret);
                try {
                    closeConnection(request, response);
                } catch (Exception e2) {
                    throw new OAuthCommunicationException(e2);
                }
            }
        } catch (OAuthNotAuthorizedException e3) {
            throw e3;
        } catch (OAuthExpectationFailedException e4) {
            throw e4;
        } catch (Exception e5) {
            throw new OAuthCommunicationException(e5);
        } catch (Throwable th) {
            try {
                closeConnection(request, response);
                throw th;
            } catch (Exception e6) {
                throw new OAuthCommunicationException(e6);
            }
        }
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:5:0x001f  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x004c A[LOOP:0: B:3:0x001a->B:7:0x004c, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0054  */
    protected void handleUnexpectedResponse(int r7, oauth.signpost.http.HttpResponse r8) throws java.lang.Exception {
        /*
            r6 = this;
            if (r8 != 0) goto L_0x0003
            return
        L_0x0003:
            java.io.BufferedReader r1 = new java.io.BufferedReader
            java.io.InputStreamReader r3 = new java.io.InputStreamReader
            java.io.InputStream r4 = r8.getContent()
            r3.<init>(r4)
            r1.<init>(r3)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r0 = r1.readLine()
        L_0x001a:
            if (r0 != 0) goto L_0x004c
            switch(r7) {
                case 401: goto L_0x0054;
                default: goto L_0x001f;
            }
        L_0x001f:
            oauth.signpost.exception.OAuthCommunicationException r3 = new oauth.signpost.exception.OAuthCommunicationException
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = "Service provider responded in error: "
            r4.<init>(r5)
            java.lang.StringBuilder r4 = r4.append(r7)
            java.lang.String r5 = " ("
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = r8.getReasonPhrase()
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = ")"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r4 = r4.toString()
            java.lang.String r5 = r2.toString()
            r3.<init>(r4, r5)
            throw r3
        L_0x004c:
            r2.append(r0)
            java.lang.String r0 = r1.readLine()
            goto L_0x001a
        L_0x0054:
            oauth.signpost.exception.OAuthNotAuthorizedException r3 = new oauth.signpost.exception.OAuthNotAuthorizedException
            java.lang.String r4 = r2.toString()
            r3.<init>(r4)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: oauth.signpost.AbstractOAuthProvider.handleUnexpectedResponse(int, oauth.signpost.http.HttpResponse):void");
    }

    /* access modifiers changed from: protected */
    public void closeConnection(HttpRequest request, HttpResponse response) throws Exception {
    }

    public HttpParameters getResponseParameters() {
        return this.responseParameters;
    }

    /* access modifiers changed from: protected */
    public String getResponseParameter(String key) {
        return this.responseParameters.getFirst(key);
    }

    public void setResponseParameters(HttpParameters parameters) {
        this.responseParameters = parameters;
    }

    public void setOAuth10a(boolean isOAuth10aProvider) {
        this.isOAuth10a = isOAuth10aProvider;
    }

    public boolean isOAuth10a() {
        return this.isOAuth10a;
    }

    public String getRequestTokenEndpointUrl() {
        return this.requestTokenEndpointUrl;
    }

    public String getAccessTokenEndpointUrl() {
        return this.accessTokenEndpointUrl;
    }

    public String getAuthorizationWebsiteUrl() {
        return this.authorizationWebsiteUrl;
    }

    public void setRequestHeader(String header, String value) {
        this.defaultHeaders.put(header, value);
    }

    public Map<String, String> getRequestHeaders() {
        return this.defaultHeaders;
    }

    public void setListener(OAuthProviderListener listener2) {
        this.listener = listener2;
    }

    public void removeListener(OAuthProviderListener listener2) {
        this.listener = null;
    }
}
