package oauth.signpost.signature;

import cn.com.mzba.oauth.OAuth;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import oauth.signpost.exception.OAuthMessageSignerException;
import oauth.signpost.http.HttpParameters;
import oauth.signpost.http.HttpRequest;

public class HmacSha1MessageSigner extends OAuthMessageSigner {
    private static final String MAC_NAME = "HmacSHA1";

    public String getSignatureMethod() {
        return OAuth.SIGNATURE_METHOD;
    }

    public String sign(HttpRequest request, HttpParameters requestParams) throws OAuthMessageSignerException {
        try {
            SecretKey key = new SecretKeySpec((String.valueOf(oauth.signpost.OAuth.percentEncode(getConsumerSecret())) + '&' + oauth.signpost.OAuth.percentEncode(getTokenSecret())).getBytes("UTF-8"), MAC_NAME);
            Mac mac = Mac.getInstance(MAC_NAME);
            mac.init(key);
            String sbs = new SignatureBaseString(request, requestParams).generate();
            oauth.signpost.OAuth.debugOut("SBS", sbs);
            return base64Encode(mac.doFinal(sbs.getBytes("UTF-8"))).trim();
        } catch (GeneralSecurityException e) {
            throw new OAuthMessageSignerException(e);
        } catch (UnsupportedEncodingException e2) {
            throw new OAuthMessageSignerException(e2);
        }
    }
}
