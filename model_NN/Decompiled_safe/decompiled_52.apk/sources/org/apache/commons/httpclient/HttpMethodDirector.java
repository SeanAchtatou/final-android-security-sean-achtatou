package org.apache.commons.httpclient;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.apache.commons.httpclient.auth.AuthChallengeException;
import org.apache.commons.httpclient.auth.AuthChallengeParser;
import org.apache.commons.httpclient.auth.AuthChallengeProcessor;
import org.apache.commons.httpclient.auth.AuthScheme;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.auth.AuthState;
import org.apache.commons.httpclient.auth.AuthenticationException;
import org.apache.commons.httpclient.auth.CredentialsNotAvailableException;
import org.apache.commons.httpclient.auth.CredentialsProvider;
import org.apache.commons.httpclient.auth.MalformedChallengeException;
import org.apache.commons.httpclient.params.HostParams;
import org.apache.commons.httpclient.params.HttpClientParams;
import org.apache.commons.httpclient.params.HttpParams;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

class HttpMethodDirector {
    private static final Log LOG;
    public static final String PROXY_AUTH_CHALLENGE = "Proxy-Authenticate";
    public static final String PROXY_AUTH_RESP = "Proxy-Authorization";
    public static final String WWW_AUTH_CHALLENGE = "WWW-Authenticate";
    public static final String WWW_AUTH_RESP = "Authorization";
    static Class class$org$apache$commons$httpclient$HttpMethodDirector;
    private AuthChallengeProcessor authProcessor = null;
    private HttpConnection conn;
    private ConnectMethod connectMethod;
    private HttpConnectionManager connectionManager;
    private HostConfiguration hostConfiguration;
    private HttpClientParams params;
    private Set redirectLocations = null;
    private boolean releaseConnection = false;
    private HttpState state;

    static {
        Class cls;
        if (class$org$apache$commons$httpclient$HttpMethodDirector == null) {
            cls = class$("org.apache.commons.httpclient.HttpMethodDirector");
            class$org$apache$commons$httpclient$HttpMethodDirector = cls;
        } else {
            cls = class$org$apache$commons$httpclient$HttpMethodDirector;
        }
        LOG = LogFactory.getLog(cls);
    }

    static Class class$(String x0) {
        try {
            return Class.forName(x0);
        } catch (ClassNotFoundException x1) {
            throw new NoClassDefFoundError(x1.getMessage());
        }
    }

    public HttpMethodDirector(HttpConnectionManager connectionManager2, HostConfiguration hostConfiguration2, HttpClientParams params2, HttpState state2) {
        this.connectionManager = connectionManager2;
        this.hostConfiguration = hostConfiguration2;
        this.params = params2;
        this.state = state2;
        this.authProcessor = new AuthChallengeProcessor(this.params);
    }

    /* JADX INFO: finally extract failed */
    public void executeMethod(HttpMethod method) throws IOException, HttpException {
        if (method == null) {
            throw new IllegalArgumentException("Method may not be null");
        }
        this.hostConfiguration.getParams().setDefaults(this.params);
        method.getParams().setDefaults(this.hostConfiguration.getParams());
        Collection<Header> defaults = (Collection) this.hostConfiguration.getParams().getParameter(HostParams.DEFAULT_HEADERS);
        if (defaults != null) {
            for (Header addRequestHeader : defaults) {
                method.addRequestHeader(addRequestHeader);
            }
        }
        try {
            int maxRedirects = this.params.getIntParameter(HttpClientParams.MAX_REDIRECTS, 100);
            int redirectCount = 0;
            while (true) {
                if (this.conn != null && !this.hostConfiguration.hostEquals(this.conn)) {
                    this.conn.setLocked(false);
                    this.conn.releaseConnection();
                    this.conn = null;
                }
                if (this.conn == null) {
                    this.conn = this.connectionManager.getConnectionWithTimeout(this.hostConfiguration, this.params.getConnectionManagerTimeout());
                    this.conn.setLocked(true);
                    if (this.params.isAuthenticationPreemptive() || this.state.isAuthenticationPreemptive()) {
                        LOG.debug("Preemptively sending default basic credentials");
                        method.getHostAuthState().setPreemptive();
                        method.getHostAuthState().setAuthAttempted(true);
                        if (this.conn.isProxied() && !this.conn.isSecure()) {
                            method.getProxyAuthState().setPreemptive();
                            method.getProxyAuthState().setAuthAttempted(true);
                        }
                    }
                }
                authenticate(method);
                executeWithRetry(method);
                if (this.connectMethod == null) {
                    boolean retry = false;
                    if (isRedirectNeeded(method) && processRedirectResponse(method)) {
                        retry = true;
                        redirectCount++;
                        if (redirectCount >= maxRedirects) {
                            LOG.error("Narrowly avoided an infinite loop in execute");
                            throw new RedirectException(new StringBuffer().append("Maximum redirects (").append(maxRedirects).append(") exceeded").toString());
                        } else if (LOG.isDebugEnabled()) {
                            LOG.debug(new StringBuffer().append("Execute redirect ").append(redirectCount).append(" of ").append(maxRedirects).toString());
                        }
                    }
                    if (isAuthenticationNeeded(method) && processAuthenticationResponse(method)) {
                        LOG.debug("Retry authentication");
                        retry = true;
                    }
                    if (!retry) {
                        break;
                    } else if (method.getResponseBodyAsStream() != null) {
                        method.getResponseBodyAsStream().close();
                    }
                } else {
                    fakeResponse(method);
                    break;
                }
            }
            if (this.conn != null) {
                this.conn.setLocked(false);
            }
            if ((this.releaseConnection || method.getResponseBodyAsStream() == null) && this.conn != null) {
                this.conn.releaseConnection();
            }
        } catch (Throwable th) {
            if (this.conn != null) {
                this.conn.setLocked(false);
            }
            if ((this.releaseConnection || method.getResponseBodyAsStream() == null) && this.conn != null) {
                this.conn.releaseConnection();
            }
            throw th;
        }
    }

    private void authenticate(HttpMethod method) {
        try {
            if (this.conn.isProxied() && !this.conn.isSecure()) {
                authenticateProxy(method);
            }
            authenticateHost(method);
        } catch (AuthenticationException e) {
            AuthenticationException e2 = e;
            LOG.error(e2.getMessage(), e2);
        }
    }

    private boolean cleanAuthHeaders(HttpMethod method, String name) {
        Header[] authheaders = method.getRequestHeaders(name);
        boolean clean = true;
        for (Header authheader : authheaders) {
            if (authheader.isAutogenerated()) {
                method.removeRequestHeader(authheader);
            } else {
                clean = false;
            }
        }
        return clean;
    }

    private void authenticateHost(HttpMethod method) throws AuthenticationException {
        AuthState authstate;
        AuthScheme authscheme;
        if (!cleanAuthHeaders(method, "Authorization") || (authscheme = (authstate = method.getHostAuthState()).getAuthScheme()) == null) {
            return;
        }
        if (authstate.isAuthRequested() || !authscheme.isConnectionBased()) {
            String host = method.getParams().getVirtualHost();
            if (host == null) {
                host = this.conn.getHost();
            }
            AuthScope authscope = new AuthScope(host, this.conn.getPort(), authscheme.getRealm(), authscheme.getSchemeName());
            if (LOG.isDebugEnabled()) {
                LOG.debug(new StringBuffer().append("Authenticating with ").append(authscope).toString());
            }
            Credentials credentials = this.state.getCredentials(authscope);
            if (credentials != null) {
                String authstring = authscheme.authenticate(credentials, method);
                if (authstring != null) {
                    method.addRequestHeader(new Header("Authorization", authstring, true));
                }
            } else if (LOG.isWarnEnabled()) {
                LOG.warn(new StringBuffer().append("Required credentials not available for ").append(authscope).toString());
                if (method.getHostAuthState().isPreemptive()) {
                    LOG.warn("Preemptive authentication requested but no default credentials available");
                }
            }
        }
    }

    private void authenticateProxy(HttpMethod method) throws AuthenticationException {
        AuthState authstate;
        AuthScheme authscheme;
        if (!cleanAuthHeaders(method, "Proxy-Authorization") || (authscheme = (authstate = method.getProxyAuthState()).getAuthScheme()) == null) {
            return;
        }
        if (authstate.isAuthRequested() || !authscheme.isConnectionBased()) {
            AuthScope authscope = new AuthScope(this.conn.getProxyHost(), this.conn.getProxyPort(), authscheme.getRealm(), authscheme.getSchemeName());
            if (LOG.isDebugEnabled()) {
                LOG.debug(new StringBuffer().append("Authenticating with ").append(authscope).toString());
            }
            Credentials credentials = this.state.getProxyCredentials(authscope);
            if (credentials != null) {
                String authstring = authscheme.authenticate(credentials, method);
                if (authstring != null) {
                    method.addRequestHeader(new Header("Proxy-Authorization", authstring, true));
                }
            } else if (LOG.isWarnEnabled()) {
                LOG.warn(new StringBuffer().append("Required proxy credentials not available for ").append(authscope).toString());
                if (method.getProxyAuthState().isPreemptive()) {
                    LOG.warn("Preemptive authentication requested but no default proxy credentials available");
                }
            }
        }
    }

    private void applyConnectionParams(HttpMethod method) throws IOException {
        int timeout = 0;
        Object param = method.getParams().getParameter("http.socket.timeout");
        if (param == null) {
            param = this.conn.getParams().getParameter("http.socket.timeout");
        }
        if (param != null) {
            timeout = ((Integer) param).intValue();
        }
        this.conn.setSocketTimeout(timeout);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00c8, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00c9, code lost:
        r7 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00d0, code lost:
        if (r10.conn.isOpen() != false) goto L_0x00d2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00d2, code lost:
        org.apache.commons.httpclient.HttpMethodDirector.LOG.debug("Closing the connection.");
        r10.conn.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00de, code lost:
        r10.releaseConnection = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00e0, code lost:
        throw r7;
     */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00c8 A[ExcHandler: RuntimeException (r2v1 'e' java.lang.RuntimeException A[CUSTOM_DECLARE]), Splitter:B:2:0x0008] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void executeWithRetry(org.apache.commons.httpclient.HttpMethod r11) throws java.io.IOException, org.apache.commons.httpclient.HttpException {
        /*
            r10 = this;
            r9 = 1
            java.lang.String r2 = "Method retry handler returned false. Automatic recovery will not be attempted"
            java.lang.String r8 = "Closing the connection."
            r5 = 0
        L_0x0006:
            int r5 = r5 + 1
            org.apache.commons.logging.Log r2 = org.apache.commons.httpclient.HttpMethodDirector.LOG     // Catch:{ HttpException -> 0x0072, IOException -> 0x008e, RuntimeException -> 0x00c8 }
            boolean r2 = r2.isTraceEnabled()     // Catch:{ HttpException -> 0x0072, IOException -> 0x008e, RuntimeException -> 0x00c8 }
            if (r2 == 0) goto L_0x002e
            org.apache.commons.logging.Log r2 = org.apache.commons.httpclient.HttpMethodDirector.LOG     // Catch:{ HttpException -> 0x0072, IOException -> 0x008e, RuntimeException -> 0x00c8 }
            java.lang.StringBuffer r3 = new java.lang.StringBuffer     // Catch:{ HttpException -> 0x0072, IOException -> 0x008e, RuntimeException -> 0x00c8 }
            r3.<init>()     // Catch:{ HttpException -> 0x0072, IOException -> 0x008e, RuntimeException -> 0x00c8 }
            java.lang.String r4 = "Attempt number "
            java.lang.StringBuffer r3 = r3.append(r4)     // Catch:{ HttpException -> 0x0072, IOException -> 0x008e, RuntimeException -> 0x00c8 }
            java.lang.StringBuffer r3 = r3.append(r5)     // Catch:{ HttpException -> 0x0072, IOException -> 0x008e, RuntimeException -> 0x00c8 }
            java.lang.String r4 = " to process request"
            java.lang.StringBuffer r3 = r3.append(r4)     // Catch:{ HttpException -> 0x0072, IOException -> 0x008e, RuntimeException -> 0x00c8 }
            java.lang.String r3 = r3.toString()     // Catch:{ HttpException -> 0x0072, IOException -> 0x008e, RuntimeException -> 0x00c8 }
            r2.trace(r3)     // Catch:{ HttpException -> 0x0072, IOException -> 0x008e, RuntimeException -> 0x00c8 }
        L_0x002e:
            org.apache.commons.httpclient.HttpConnection r2 = r10.conn     // Catch:{ HttpException -> 0x0072, IOException -> 0x008e, RuntimeException -> 0x00c8 }
            org.apache.commons.httpclient.params.HttpConnectionParams r2 = r2.getParams()     // Catch:{ HttpException -> 0x0072, IOException -> 0x008e, RuntimeException -> 0x00c8 }
            boolean r2 = r2.isStaleCheckingEnabled()     // Catch:{ HttpException -> 0x0072, IOException -> 0x008e, RuntimeException -> 0x00c8 }
            if (r2 == 0) goto L_0x003f
            org.apache.commons.httpclient.HttpConnection r2 = r10.conn     // Catch:{ HttpException -> 0x0072, IOException -> 0x008e, RuntimeException -> 0x00c8 }
            r2.closeIfStale()     // Catch:{ HttpException -> 0x0072, IOException -> 0x008e, RuntimeException -> 0x00c8 }
        L_0x003f:
            org.apache.commons.httpclient.HttpConnection r2 = r10.conn     // Catch:{ HttpException -> 0x0072, IOException -> 0x008e, RuntimeException -> 0x00c8 }
            boolean r2 = r2.isOpen()     // Catch:{ HttpException -> 0x0072, IOException -> 0x008e, RuntimeException -> 0x00c8 }
            if (r2 != 0) goto L_0x0067
            org.apache.commons.httpclient.HttpConnection r2 = r10.conn     // Catch:{ HttpException -> 0x0072, IOException -> 0x008e, RuntimeException -> 0x00c8 }
            r2.open()     // Catch:{ HttpException -> 0x0072, IOException -> 0x008e, RuntimeException -> 0x00c8 }
            org.apache.commons.httpclient.HttpConnection r2 = r10.conn     // Catch:{ HttpException -> 0x0072, IOException -> 0x008e, RuntimeException -> 0x00c8 }
            boolean r2 = r2.isProxied()     // Catch:{ HttpException -> 0x0072, IOException -> 0x008e, RuntimeException -> 0x00c8 }
            if (r2 == 0) goto L_0x0067
            org.apache.commons.httpclient.HttpConnection r2 = r10.conn     // Catch:{ HttpException -> 0x0072, IOException -> 0x008e, RuntimeException -> 0x00c8 }
            boolean r2 = r2.isSecure()     // Catch:{ HttpException -> 0x0072, IOException -> 0x008e, RuntimeException -> 0x00c8 }
            if (r2 == 0) goto L_0x0067
            boolean r2 = r11 instanceof org.apache.commons.httpclient.ConnectMethod     // Catch:{ HttpException -> 0x0072, IOException -> 0x008e, RuntimeException -> 0x00c8 }
            if (r2 != 0) goto L_0x0067
            boolean r2 = r10.executeConnect()     // Catch:{ HttpException -> 0x0072, IOException -> 0x008e, RuntimeException -> 0x00c8 }
            if (r2 != 0) goto L_0x0067
        L_0x0066:
            return
        L_0x0067:
            r10.applyConnectionParams(r11)     // Catch:{ HttpException -> 0x0072, IOException -> 0x008e, RuntimeException -> 0x00c8 }
            org.apache.commons.httpclient.HttpState r2 = r10.state     // Catch:{ HttpException -> 0x0072, IOException -> 0x008e, RuntimeException -> 0x00c8 }
            org.apache.commons.httpclient.HttpConnection r3 = r10.conn     // Catch:{ HttpException -> 0x0072, IOException -> 0x008e, RuntimeException -> 0x00c8 }
            r11.execute(r2, r3)     // Catch:{ HttpException -> 0x0072, IOException -> 0x008e, RuntimeException -> 0x00c8 }
            goto L_0x0066
        L_0x0072:
            r2 = move-exception
            r7 = r2
            throw r7     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
        L_0x0075:
            r2 = move-exception
            r7 = r2
            org.apache.commons.httpclient.HttpConnection r2 = r10.conn
            boolean r2 = r2.isOpen()
            if (r2 == 0) goto L_0x008b
            org.apache.commons.logging.Log r2 = org.apache.commons.httpclient.HttpMethodDirector.LOG
            java.lang.String r3 = "Closing the connection."
            r2.debug(r8)
            org.apache.commons.httpclient.HttpConnection r2 = r10.conn
            r2.close()
        L_0x008b:
            r10.releaseConnection = r9
            throw r7
        L_0x008e:
            r2 = move-exception
            r7 = r2
            org.apache.commons.logging.Log r2 = org.apache.commons.httpclient.HttpMethodDirector.LOG     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            java.lang.String r3 = "Closing the connection."
            r2.debug(r3)     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            org.apache.commons.httpclient.HttpConnection r2 = r10.conn     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            r2.close()     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            boolean r2 = r11 instanceof org.apache.commons.httpclient.HttpMethodBase     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            if (r2 == 0) goto L_0x00e1
            r0 = r11
            org.apache.commons.httpclient.HttpMethodBase r0 = (org.apache.commons.httpclient.HttpMethodBase) r0     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            r2 = r0
            org.apache.commons.httpclient.MethodRetryHandler r1 = r2.getMethodRetryHandler()     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            if (r1 == 0) goto L_0x00e1
            org.apache.commons.httpclient.HttpConnection r3 = r10.conn     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            org.apache.commons.httpclient.HttpRecoverableException r4 = new org.apache.commons.httpclient.HttpRecoverableException     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            java.lang.String r2 = r7.getMessage()     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            r4.<init>(r2)     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            boolean r6 = r11.isRequestSent()     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            r2 = r11
            boolean r2 = r1.retryMethod(r2, r3, r4, r5, r6)     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            if (r2 != 0) goto L_0x00e1
            org.apache.commons.logging.Log r2 = org.apache.commons.httpclient.HttpMethodDirector.LOG     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            java.lang.String r3 = "Method retry handler returned false. Automatic recovery will not be attempted"
            r2.debug(r3)     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            throw r7     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
        L_0x00c8:
            r2 = move-exception
            r7 = r2
            org.apache.commons.httpclient.HttpConnection r2 = r10.conn
            boolean r2 = r2.isOpen()
            if (r2 == 0) goto L_0x00de
            org.apache.commons.logging.Log r2 = org.apache.commons.httpclient.HttpMethodDirector.LOG
            java.lang.String r3 = "Closing the connection."
            r2.debug(r8)
            org.apache.commons.httpclient.HttpConnection r2 = r10.conn
            r2.close()
        L_0x00de:
            r10.releaseConnection = r9
            throw r7
        L_0x00e1:
            org.apache.commons.httpclient.params.HttpMethodParams r2 = r11.getParams()     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            java.lang.String r3 = "http.method.retry-handler"
            java.lang.Object r1 = r2.getParameter(r3)     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            org.apache.commons.httpclient.HttpMethodRetryHandler r1 = (org.apache.commons.httpclient.HttpMethodRetryHandler) r1     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            if (r1 != 0) goto L_0x00f4
            org.apache.commons.httpclient.DefaultHttpMethodRetryHandler r1 = new org.apache.commons.httpclient.DefaultHttpMethodRetryHandler     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            r1.<init>()     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
        L_0x00f4:
            boolean r2 = r1.retryMethod(r11, r7, r5)     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            if (r2 != 0) goto L_0x0102
            org.apache.commons.logging.Log r2 = org.apache.commons.httpclient.HttpMethodDirector.LOG     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            java.lang.String r3 = "Method retry handler returned false. Automatic recovery will not be attempted"
            r2.debug(r3)     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            throw r7     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
        L_0x0102:
            org.apache.commons.logging.Log r2 = org.apache.commons.httpclient.HttpMethodDirector.LOG     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            boolean r2 = r2.isInfoEnabled()     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            if (r2 == 0) goto L_0x0138
            org.apache.commons.logging.Log r2 = org.apache.commons.httpclient.HttpMethodDirector.LOG     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            java.lang.StringBuffer r3 = new java.lang.StringBuffer     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            r3.<init>()     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            java.lang.String r4 = "I/O exception ("
            java.lang.StringBuffer r3 = r3.append(r4)     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            java.lang.Class r4 = r7.getClass()     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            java.lang.String r4 = r4.getName()     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            java.lang.StringBuffer r3 = r3.append(r4)     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            java.lang.String r4 = ") caught when processing request: "
            java.lang.StringBuffer r3 = r3.append(r4)     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            java.lang.String r4 = r7.getMessage()     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            java.lang.StringBuffer r3 = r3.append(r4)     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            r2.info(r3)     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
        L_0x0138:
            org.apache.commons.logging.Log r2 = org.apache.commons.httpclient.HttpMethodDirector.LOG     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            boolean r2 = r2.isDebugEnabled()     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            if (r2 == 0) goto L_0x0149
            org.apache.commons.logging.Log r2 = org.apache.commons.httpclient.HttpMethodDirector.LOG     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            java.lang.String r3 = r7.getMessage()     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            r2.debug(r3, r7)     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
        L_0x0149:
            org.apache.commons.logging.Log r2 = org.apache.commons.httpclient.HttpMethodDirector.LOG     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            java.lang.String r3 = "Retrying request"
            r2.info(r3)     // Catch:{ IOException -> 0x0075, RuntimeException -> 0x00c8 }
            goto L_0x0006
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.httpclient.HttpMethodDirector.executeWithRetry(org.apache.commons.httpclient.HttpMethod):void");
    }

    private boolean executeConnect() throws IOException, HttpException {
        int code;
        boolean z;
        this.connectMethod = new ConnectMethod(this.hostConfiguration);
        this.connectMethod.getParams().setDefaults(this.hostConfiguration.getParams());
        while (true) {
            if (!this.conn.isOpen()) {
                this.conn.open();
            }
            if (this.params.isAuthenticationPreemptive() || this.state.isAuthenticationPreemptive()) {
                LOG.debug("Preemptively sending default basic credentials");
                this.connectMethod.getProxyAuthState().setPreemptive();
                this.connectMethod.getProxyAuthState().setAuthAttempted(true);
            }
            try {
                authenticateProxy(this.connectMethod);
            } catch (AuthenticationException e) {
                AuthenticationException e2 = e;
                LOG.error(e2.getMessage(), e2);
            }
            applyConnectionParams(this.connectMethod);
            this.connectMethod.execute(this.state, this.conn);
            code = this.connectMethod.getStatusCode();
            boolean retry = false;
            AuthState authstate = this.connectMethod.getProxyAuthState();
            if (code == 407) {
                z = true;
            } else {
                z = false;
            }
            authstate.setAuthRequested(z);
            if (authstate.isAuthRequested() && processAuthenticationResponse(this.connectMethod)) {
                retry = true;
            }
            if (!retry) {
                break;
            } else if (this.connectMethod.getResponseBodyAsStream() != null) {
                this.connectMethod.getResponseBodyAsStream().close();
            }
        }
        if (code < 200 || code >= 300) {
            this.conn.close();
            return false;
        }
        this.conn.tunnelCreated();
        this.connectMethod = null;
        return true;
    }

    private void fakeResponse(HttpMethod method) throws IOException, HttpException {
        LOG.debug("CONNECT failed, fake the response for the original method");
        if (method instanceof HttpMethodBase) {
            ((HttpMethodBase) method).fakeResponse(this.connectMethod.getStatusLine(), this.connectMethod.getResponseHeaderGroup(), this.connectMethod.getResponseBodyAsStream());
            method.getProxyAuthState().setAuthScheme(this.connectMethod.getProxyAuthState().getAuthScheme());
            this.connectMethod = null;
            return;
        }
        this.releaseConnection = true;
        LOG.warn("Unable to fake response on method as it is not derived from HttpMethodBase.");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.httpclient.URI.<init>(java.lang.String, boolean, java.lang.String):void
     arg types: [java.lang.String, int, java.lang.String]
     candidates:
      org.apache.commons.httpclient.URI.<init>(java.lang.String, java.lang.String, java.lang.String):void
      org.apache.commons.httpclient.URI.<init>(org.apache.commons.httpclient.URI, java.lang.String, boolean):void
      org.apache.commons.httpclient.URI.<init>(java.lang.String, boolean, java.lang.String):void */
    private boolean processRedirectResponse(HttpMethod method) throws RedirectException {
        URIException ex;
        URI redirectUri;
        Header locationHeader = method.getResponseHeader("location");
        if (locationHeader == null) {
            LOG.error(new StringBuffer().append("Received redirect response ").append(method.getStatusCode()).append(" but no location header").toString());
            return false;
        }
        String location = locationHeader.getValue();
        if (LOG.isDebugEnabled()) {
            LOG.debug(new StringBuffer().append("Redirect requested to location '").append(location).append("'").toString());
        }
        try {
            URI currentUri = new URI(this.conn.getProtocol().getScheme(), (String) null, this.conn.getHost(), this.conn.getPort(), method.getPath());
            try {
                URI redirectUri2 = new URI(location, true, method.getParams().getUriCharset());
                try {
                    if (!redirectUri2.isRelativeURI()) {
                        method.getParams().setDefaults(this.params);
                        redirectUri = redirectUri2;
                    } else if (this.params.isParameterTrue(HttpClientParams.REJECT_RELATIVE_REDIRECT)) {
                        LOG.warn(new StringBuffer().append("Relative redirect location '").append(location).append("' not allowed").toString());
                        return false;
                    } else {
                        LOG.debug("Redirect URI is not absolute - parsing as relative");
                        redirectUri = new URI(currentUri, redirectUri2);
                    }
                    method.setURI(redirectUri);
                    this.hostConfiguration.setHost(redirectUri);
                    if (this.params.isParameterFalse(HttpClientParams.ALLOW_CIRCULAR_REDIRECTS)) {
                        if (this.redirectLocations == null) {
                            this.redirectLocations = new HashSet();
                        }
                        this.redirectLocations.add(currentUri);
                        try {
                            if (redirectUri.hasQuery()) {
                                redirectUri.setQuery(null);
                            }
                            if (this.redirectLocations.contains(redirectUri)) {
                                throw new CircularRedirectException(new StringBuffer().append("Circular redirect to '").append(redirectUri).append("'").toString());
                            }
                        } catch (URIException e) {
                            return false;
                        }
                    }
                    if (LOG.isDebugEnabled()) {
                        LOG.debug(new StringBuffer().append("Redirecting from '").append(currentUri.getEscapedURI()).append("' to '").append(redirectUri.getEscapedURI()).toString());
                    }
                    method.getHostAuthState().invalidate();
                    return true;
                } catch (URIException e2) {
                    ex = e2;
                    throw new InvalidRedirectLocationException(new StringBuffer().append("Invalid redirect location: ").append(location).toString(), location, ex);
                }
            } catch (URIException e3) {
                ex = e3;
                throw new InvalidRedirectLocationException(new StringBuffer().append("Invalid redirect location: ").append(location).toString(), location, ex);
            }
        } catch (URIException e4) {
            ex = e4;
            throw new InvalidRedirectLocationException(new StringBuffer().append("Invalid redirect location: ").append(location).toString(), location, ex);
        }
    }

    private boolean processAuthenticationResponse(HttpMethod method) {
        LOG.trace("enter HttpMethodBase.processAuthenticationResponse(HttpState, HttpConnection)");
        try {
            switch (method.getStatusCode()) {
                case HttpStatus.SC_UNAUTHORIZED /*401*/:
                    return processWWWAuthChallenge(method);
                case HttpStatus.SC_PROXY_AUTHENTICATION_REQUIRED /*407*/:
                    return processProxyAuthChallenge(method);
                default:
                    return false;
            }
        } catch (Exception e) {
            Exception e2 = e;
            if (LOG.isErrorEnabled()) {
                LOG.error(e2.getMessage(), e2);
            }
            return false;
        }
    }

    private boolean processWWWAuthChallenge(HttpMethod method) throws MalformedChallengeException, AuthenticationException {
        AuthState authstate = method.getHostAuthState();
        Map challenges = AuthChallengeParser.parseChallenges(method.getResponseHeaders("WWW-Authenticate"));
        if (challenges.isEmpty()) {
            LOG.debug("Authentication challenge(s) not found");
            return false;
        }
        AuthScheme authscheme = null;
        try {
            authscheme = this.authProcessor.processChallenge(authstate, challenges);
        } catch (AuthChallengeException e) {
            AuthChallengeException e2 = e;
            if (LOG.isWarnEnabled()) {
                LOG.warn(e2.getMessage());
            }
        }
        if (authscheme == null) {
            return false;
        }
        String host = method.getParams().getVirtualHost();
        if (host == null) {
            host = this.conn.getHost();
        }
        AuthScope authscope = new AuthScope(host, this.conn.getPort(), authscheme.getRealm(), authscheme.getSchemeName());
        if (LOG.isDebugEnabled()) {
            LOG.debug(new StringBuffer().append("Authentication scope: ").append(authscope).toString());
        }
        if (!authstate.isAuthAttempted() || !authscheme.isComplete()) {
            authstate.setAuthAttempted(true);
            Credentials credentials = this.state.getCredentials(authscope);
            if (credentials == null) {
                credentials = promptForCredentials(authscheme, method.getParams(), authscope);
            }
            if (credentials != null) {
                return true;
            }
            if (LOG.isInfoEnabled()) {
                LOG.info(new StringBuffer().append("No credentials available for ").append(authscope).toString());
            }
            return false;
        } else if (promptForCredentials(authscheme, method.getParams(), authscope) != null) {
            return true;
        } else {
            if (LOG.isInfoEnabled()) {
                LOG.info(new StringBuffer().append("Failure authenticating with ").append(authscope).toString());
            }
            return false;
        }
    }

    private boolean processProxyAuthChallenge(HttpMethod method) throws MalformedChallengeException, AuthenticationException {
        AuthState authstate = method.getProxyAuthState();
        Map proxyChallenges = AuthChallengeParser.parseChallenges(method.getResponseHeaders("Proxy-Authenticate"));
        if (proxyChallenges.isEmpty()) {
            LOG.debug("Proxy authentication challenge(s) not found");
            return false;
        }
        AuthScheme authscheme = null;
        try {
            authscheme = this.authProcessor.processChallenge(authstate, proxyChallenges);
        } catch (AuthChallengeException e) {
            AuthChallengeException e2 = e;
            if (LOG.isWarnEnabled()) {
                LOG.warn(e2.getMessage());
            }
        }
        if (authscheme == null) {
            return false;
        }
        AuthScope authscope = new AuthScope(this.conn.getProxyHost(), this.conn.getProxyPort(), authscheme.getRealm(), authscheme.getSchemeName());
        if (LOG.isDebugEnabled()) {
            LOG.debug(new StringBuffer().append("Proxy authentication scope: ").append(authscope).toString());
        }
        if (!authstate.isAuthAttempted() || !authscheme.isComplete()) {
            authstate.setAuthAttempted(true);
            Credentials credentials = this.state.getProxyCredentials(authscope);
            if (credentials == null) {
                credentials = promptForProxyCredentials(authscheme, method.getParams(), authscope);
            }
            if (credentials != null) {
                return true;
            }
            if (LOG.isInfoEnabled()) {
                LOG.info(new StringBuffer().append("No credentials available for ").append(authscope).toString());
            }
            return false;
        } else if (promptForProxyCredentials(authscheme, method.getParams(), authscope) != null) {
            return true;
        } else {
            if (LOG.isInfoEnabled()) {
                LOG.info(new StringBuffer().append("Failure authenticating with ").append(authscope).toString());
            }
            return false;
        }
    }

    private boolean isRedirectNeeded(HttpMethod method) {
        switch (method.getStatusCode()) {
            case HttpStatus.SC_MOVED_PERMANENTLY /*301*/:
            case HttpStatus.SC_MOVED_TEMPORARILY /*302*/:
            case HttpStatus.SC_SEE_OTHER /*303*/:
            case HttpStatus.SC_TEMPORARY_REDIRECT /*307*/:
                LOG.debug("Redirect required");
                return method.getFollowRedirects();
            case HttpStatus.SC_NOT_MODIFIED /*304*/:
            case HttpStatus.SC_USE_PROXY /*305*/:
            case 306:
            default:
                return false;
        }
    }

    private boolean isAuthenticationNeeded(HttpMethod method) {
        boolean z;
        method.getHostAuthState().setAuthRequested(method.getStatusCode() == 401);
        AuthState proxyAuthState = method.getProxyAuthState();
        if (method.getStatusCode() == 407) {
            z = true;
        } else {
            z = false;
        }
        proxyAuthState.setAuthRequested(z);
        if (!method.getHostAuthState().isAuthRequested() && !method.getProxyAuthState().isAuthRequested()) {
            return false;
        }
        LOG.debug("Authorization required");
        if (method.getDoAuthentication()) {
            return true;
        }
        LOG.info("Authentication requested but doAuthentication is disabled");
        return false;
    }

    private Credentials promptForCredentials(AuthScheme authScheme, HttpParams params2, AuthScope authscope) {
        LOG.debug("Credentials required");
        Credentials creds = null;
        CredentialsProvider credProvider = (CredentialsProvider) params2.getParameter(CredentialsProvider.PROVIDER);
        if (credProvider != null) {
            try {
                creds = credProvider.getCredentials(authScheme, authscope.getHost(), authscope.getPort(), false);
            } catch (CredentialsNotAvailableException e) {
                LOG.warn(e.getMessage());
            }
            if (creds != null) {
                this.state.setCredentials(authscope, creds);
                if (LOG.isDebugEnabled()) {
                    LOG.debug(new StringBuffer().append(authscope).append(" new credentials given").toString());
                }
            }
        } else {
            LOG.debug("Credentials provider not available");
        }
        return creds;
    }

    private Credentials promptForProxyCredentials(AuthScheme authScheme, HttpParams params2, AuthScope authscope) {
        LOG.debug("Proxy credentials required");
        Credentials creds = null;
        CredentialsProvider credProvider = (CredentialsProvider) params2.getParameter(CredentialsProvider.PROVIDER);
        if (credProvider != null) {
            try {
                creds = credProvider.getCredentials(authScheme, authscope.getHost(), authscope.getPort(), true);
            } catch (CredentialsNotAvailableException e) {
                LOG.warn(e.getMessage());
            }
            if (creds != null) {
                this.state.setProxyCredentials(authscope, creds);
                if (LOG.isDebugEnabled()) {
                    LOG.debug(new StringBuffer().append(authscope).append(" new credentials given").toString());
                }
            }
        } else {
            LOG.debug("Proxy credentials provider not available");
        }
        return creds;
    }

    public HostConfiguration getHostConfiguration() {
        return this.hostConfiguration;
    }

    public HttpState getState() {
        return this.state;
    }

    public HttpConnectionManager getConnectionManager() {
        return this.connectionManager;
    }

    public HttpParams getParams() {
        return this.params;
    }
}
