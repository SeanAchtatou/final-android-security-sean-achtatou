package org.apache.commons.httpclient.cookie;

public final class CookieOrigin {
    private final String host;
    private final String path;
    private final int port;
    private final boolean secure;

    public CookieOrigin(String host2, int port2, String path2, boolean secure2) {
        if (host2 == null) {
            throw new IllegalArgumentException("Host of origin may not be null");
        } else if (host2.trim().equals("")) {
            throw new IllegalArgumentException("Host of origin may not be blank");
        } else if (port2 < 0) {
            throw new IllegalArgumentException(new StringBuffer().append("Invalid port: ").append(port2).toString());
        } else if (path2 == null) {
            throw new IllegalArgumentException("Path of origin may not be null.");
        } else {
            this.host = host2;
            this.port = port2;
            this.path = path2;
            this.secure = secure2;
        }
    }

    public String getHost() {
        return this.host;
    }

    public String getPath() {
        return this.path;
    }

    public int getPort() {
        return this.port;
    }

    public boolean isSecure() {
        return this.secure;
    }
}
