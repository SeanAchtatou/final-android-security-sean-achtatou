package org.apache.commons.httpclient.auth;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public abstract class AuthPolicy {
    public static final String AUTH_SCHEME_PRIORITY = "http.auth.scheme-priority";
    public static final String BASIC = "Basic";
    public static final String DIGEST = "Digest";
    protected static final Log LOG;
    public static final String NTLM = "NTLM";
    private static final HashMap SCHEMES = new HashMap();
    private static final ArrayList SCHEME_LIST = new ArrayList();
    static Class class$org$apache$commons$httpclient$auth$AuthPolicy;
    static Class class$org$apache$commons$httpclient$auth$BasicScheme;
    static Class class$org$apache$commons$httpclient$auth$DigestScheme;
    static Class class$org$apache$commons$httpclient$auth$NTLMScheme;

    static {
        Class cls;
        Class cls2;
        Class cls3;
        Class cls4;
        if (class$org$apache$commons$httpclient$auth$NTLMScheme == null) {
            cls = class$("org.apache.commons.httpclient.auth.NTLMScheme");
            class$org$apache$commons$httpclient$auth$NTLMScheme = cls;
        } else {
            cls = class$org$apache$commons$httpclient$auth$NTLMScheme;
        }
        registerAuthScheme(NTLM, cls);
        if (class$org$apache$commons$httpclient$auth$DigestScheme == null) {
            cls2 = class$("org.apache.commons.httpclient.auth.DigestScheme");
            class$org$apache$commons$httpclient$auth$DigestScheme = cls2;
        } else {
            cls2 = class$org$apache$commons$httpclient$auth$DigestScheme;
        }
        registerAuthScheme(DIGEST, cls2);
        if (class$org$apache$commons$httpclient$auth$BasicScheme == null) {
            cls3 = class$("org.apache.commons.httpclient.auth.BasicScheme");
            class$org$apache$commons$httpclient$auth$BasicScheme = cls3;
        } else {
            cls3 = class$org$apache$commons$httpclient$auth$BasicScheme;
        }
        registerAuthScheme(BASIC, cls3);
        if (class$org$apache$commons$httpclient$auth$AuthPolicy == null) {
            cls4 = class$("org.apache.commons.httpclient.auth.AuthPolicy");
            class$org$apache$commons$httpclient$auth$AuthPolicy = cls4;
        } else {
            cls4 = class$org$apache$commons$httpclient$auth$AuthPolicy;
        }
        LOG = LogFactory.getLog(cls4);
    }

    static Class class$(String x0) {
        try {
            return Class.forName(x0);
        } catch (ClassNotFoundException x1) {
            throw new NoClassDefFoundError(x1.getMessage());
        }
    }

    public static synchronized void registerAuthScheme(String id, Class clazz) {
        synchronized (AuthPolicy.class) {
            if (id == null) {
                throw new IllegalArgumentException("Id may not be null");
            } else if (clazz == null) {
                throw new IllegalArgumentException("Authentication scheme class may not be null");
            } else {
                SCHEMES.put(id.toLowerCase(), clazz);
                SCHEME_LIST.add(id.toLowerCase());
            }
        }
    }

    public static synchronized void unregisterAuthScheme(String id) {
        synchronized (AuthPolicy.class) {
            if (id == null) {
                throw new IllegalArgumentException("Id may not be null");
            }
            SCHEMES.remove(id.toLowerCase());
            SCHEME_LIST.remove(id.toLowerCase());
        }
    }

    public static synchronized AuthScheme getAuthScheme(String id) throws IllegalStateException {
        AuthScheme authScheme;
        synchronized (AuthPolicy.class) {
            if (id == null) {
                throw new IllegalArgumentException("Id may not be null");
            }
            Class clazz = (Class) SCHEMES.get(id.toLowerCase());
            if (clazz != null) {
                try {
                    authScheme = (AuthScheme) clazz.newInstance();
                } catch (Exception e) {
                    LOG.error(new StringBuffer().append("Error initializing authentication scheme: ").append(id).toString(), e);
                    throw new IllegalStateException(new StringBuffer().append(id).append(" authentication scheme implemented by ").append(clazz.getName()).append(" could not be initialized").toString());
                }
            } else {
                throw new IllegalStateException(new StringBuffer().append("Unsupported authentication scheme ").append(id).toString());
            }
        }
        return authScheme;
    }

    public static synchronized List getDefaultAuthPrefs() {
        List list;
        synchronized (AuthPolicy.class) {
            list = (List) SCHEME_LIST.clone();
        }
        return list;
    }
}
