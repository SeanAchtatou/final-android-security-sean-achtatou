package cn.com.mzba.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import cn.com.mzba.model.Emotions;
import cn.com.mzba.model.Status;
import cn.com.mzba.model.User;
import java.util.ArrayList;
import java.util.List;

public class DataHelper {
    public static final String DATABASE_NAME = "kdwb.db";
    public static final int DATABASE_VERSION = 1;
    private SQLiteDatabase sqliteDatabase = this.sqliteHelper.getWritableDatabase();
    private SqliteHelper sqliteHelper;

    public DataHelper(Context context) {
        this.sqliteHelper = new SqliteHelper(context, DATABASE_NAME, null, 1);
    }

    public void close() {
        this.sqliteHelper.close();
        this.sqliteDatabase.close();
    }

    public List<UserInfo> getAllUserInfos() {
        List<UserInfo> users = new ArrayList<>();
        Cursor cursor = this.sqliteDatabase.query(SqliteHelper.TABLE_USER, null, null, null, null, null, null);
        while (cursor.moveToNext()) {
            UserInfo userinfo = new UserInfo();
            userinfo.setId(cursor.getString(0));
            userinfo.setWeiboId(cursor.getString(1));
            userinfo.setUserId(cursor.getString(2));
            userinfo.setToken(cursor.getString(3));
            userinfo.setTokenSecret(cursor.getString(4));
            users.add(userinfo);
        }
        cursor.close();
        return users;
    }

    public boolean isHaveUserInfo(String userId) {
        Cursor cursor = this.sqliteDatabase.query(SqliteHelper.TABLE_USER, null, "userId=?", new String[]{userId}, null, null, null);
        boolean exists = cursor.moveToFirst();
        cursor.close();
        return exists;
    }

    public int updateUserInfo(UserInfo user) {
        ContentValues values = new ContentValues();
        values.put(UserInfo.WEIBOID, user.getWeiboId());
        values.put(UserInfo.USERID, user.getUserId());
        values.put(UserInfo.TOKEN, user.getToken());
        values.put(UserInfo.TOKENSECRET, user.getTokenSecret());
        return this.sqliteDatabase.update(SqliteHelper.TABLE_USER, values, "userId=" + user.getUserId(), null);
    }

    public Long addUserInfo(UserInfo user) {
        ContentValues values = new ContentValues();
        values.put(UserInfo.USERID, user.getUserId());
        values.put(UserInfo.WEIBOID, user.getWeiboId());
        values.put(UserInfo.TOKEN, user.getToken());
        values.put(UserInfo.TOKENSECRET, user.getTokenSecret());
        return Long.valueOf(this.sqliteDatabase.insert(SqliteHelper.TABLE_USER, "_id", values));
    }

    public int deleteUserInfo(String UserId) {
        return this.sqliteDatabase.delete(SqliteHelper.TABLE_USER, "userId=" + UserId, null);
    }

    public UserInfo getUserInfoByUserId(String userId) {
        UserInfo userinfo = new UserInfo();
        Cursor cursor = this.sqliteDatabase.query(SqliteHelper.TABLE_USER, null, "userId=?", new String[]{userId}, null, null, null, null);
        if (cursor.moveToFirst()) {
            userinfo.setId(cursor.getString(0));
            userinfo.setWeiboId(cursor.getString(1));
            userinfo.setUserId(cursor.getString(2));
            userinfo.setToken(cursor.getString(3));
            userinfo.setTokenSecret(cursor.getString(4));
            userinfo.setUserName(cursor.getString(5));
        }
        cursor.close();
        return userinfo;
    }

    public UserInfo getUserInfoByWeiboId(String weiboId) {
        UserInfo userinfo = new UserInfo();
        Cursor cursor = this.sqliteDatabase.query(SqliteHelper.TABLE_USER, null, "weiboId=?", new String[]{weiboId}, null, null, null, null);
        if (cursor.moveToFirst()) {
            userinfo.setId(cursor.getString(0));
            userinfo.setWeiboId(cursor.getString(1));
            userinfo.setUserId(cursor.getString(2));
            userinfo.setToken(cursor.getString(3));
            userinfo.setTokenSecret(cursor.getString(4));
            userinfo.setUserName(cursor.getString(5));
        }
        cursor.close();
        return userinfo;
    }

    public void addEmotions(List<Emotions> emotionsList) {
        for (Emotions emotinos : emotionsList) {
            ContentValues values = new ContentValues();
            values.put(Emotions.PHRASE, emotinos.getPhrase());
            values.put(Emotions.IMAGENAME, emotinos.getImageName());
            values.put(Emotions.URL, emotinos.getUrl());
            values.put(Emotions.TYPE, emotinos.getType());
            values.put(Emotions.CATEGORY, emotinos.getCategory());
            this.sqliteDatabase.insert(SqliteHelper.TABLE_EMOTIONS, "_id", values);
        }
    }

    public List<Emotions> getEmotinos() {
        List<Emotions> list = new ArrayList<>();
        Cursor cursor = this.sqliteDatabase.query(SqliteHelper.TABLE_EMOTIONS, null, null, null, null, null, null);
        while (cursor.moveToNext()) {
            Emotions emotions = new Emotions();
            emotions.setPhrase(cursor.getString(1));
            emotions.setImageName(cursor.getString(2));
            emotions.setUrl(cursor.getString(3));
            emotions.setType(cursor.getString(4));
            emotions.setCategory(cursor.getString(5));
            list.add(emotions);
        }
        cursor.close();
        return list;
    }

    public List<Status> getStatusByWeiboId(String weiboId) {
        List<Status> statusList = new ArrayList<>();
        Cursor cursor = this.sqliteDatabase.query(SqliteHelper.TABLE_STATUS, null, "weiboId=?", new String[]{weiboId}, null, null, null);
        while (cursor.moveToNext()) {
            Status status = new Status();
            User user = new User();
            status.setId(cursor.getString(1));
            user.setScreenName(cursor.getString(2));
            user.setProfileImageUrl(cursor.getString(3));
            status.setCreatedAt(cursor.getString(4));
            status.setText(cursor.getString(5));
            status.setThumbnailPic(cursor.getString(6));
            if (cursor.getInt(7) == 1) {
                status.setRetweetedStatus(true);
                Status rtStatus = new Status();
                rtStatus.setText(cursor.getString(8));
                rtStatus.setThumbnailPic(cursor.getString(9));
                rtStatus.setComments(cursor.getString(12));
                rtStatus.setRt(cursor.getString(13));
                status.setStatus(rtStatus);
            }
            status.setComments(cursor.getString(10));
            status.setRt(cursor.getString(11));
            status.setSource(cursor.getString(14));
            status.setWeiboId(cursor.getString(15));
            status.setUser(user);
            statusList.add(status);
        }
        cursor.close();
        return statusList;
    }

    public int deleteStatusByWeiboId(String weiboId) {
        int result = this.sqliteDatabase.delete(SqliteHelper.TABLE_STATUS, "weiboId=?", new String[]{weiboId});
        this.sqliteDatabase.execSQL("delete from status");
        return result;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void addStatus(List<Status> statusList) {
        for (Status status : statusList) {
            ContentValues values = new ContentValues();
            User user = status.getUser();
            if (status.isRetweetedStatus()) {
                Status rtStatus = status.getStatus();
                values.put("isRtStatus", (Integer) 1);
                values.put("rtText", rtStatus.getText());
                values.put("rtPic", rtStatus.getThumbnailPic());
                values.put("rtCommentCount", rtStatus.getComments());
                values.put("rtRtCount", rtStatus.getRt());
            }
            values.put("statusId", status.getId());
            values.put(UserInfo.USERNAME, user.getScreenName());
            values.put(UserInfo.USERICON, user.getProfileImageUrl());
            values.put("pic", status.getThumbnailPic());
            values.put("time", status.getCreatedAt());
            values.put("text", status.getText());
            values.put("commentCount", status.getComments());
            values.put("rtCount", status.getRt());
            values.put("source", status.getSource());
            values.put(UserInfo.WEIBOID, status.getWeiboId());
            this.sqliteDatabase.insert(SqliteHelper.TABLE_STATUS, "_id", values);
        }
    }

    public void deleteStatus(String id) {
        this.sqliteDatabase.delete(SqliteHelper.TABLE_STATUS, "statusId=?", new String[]{id});
    }
}
