package cn.com.mzba.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SqliteHelper extends SQLiteOpenHelper {
    public static final String TABLEEMOTIONS = "create table if not exists emotions('_id' int primary key,'phrase' varchar,'imageName' varchar,'url' varchar,'type' varchar,'category' varchar)";
    public static final String TABLESTATUS = "create table if not exists status(_id int primary key,statusId varchar,userName varchar,userIcon varchar,time varchar,text varchar,pic varchar,isRtStatus int,rtText varchar,rtPic varchar,commentCount varchar,rtCount varchar,rtCommentCount varchar,rtRtCount varchar,source varchar,weiboId varchar)";
    public static final String TABLEUSER = "create table if not exists user ('_id' int primary key,'weiboId' varchar,  'userId' varchar, 'token' varchar, 'tokenSecret' varchar, 'userName' varchar, 'userIcon' blob)";
    public static final String TABLE_EMOTIONS = "emotions";
    public static final String TABLE_STATUS = "status";
    public static final String TABLE_USER = "user";

    public SqliteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLEUSER);
        db.execSQL(TABLEEMOTIONS);
        db.execSQL(TABLESTATUS);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if existsuser");
        db.execSQL("drop table if existsemotions");
        db.execSQL("drop table if existsstatus");
        onCreate(db);
    }

    public void updateColumn(SQLiteDatabase db, String tableName, String oldColumn, String newColumn, String typeColumn) {
        try {
            db.execSQL("ALTER TABLE " + tableName + " CHANGE " + oldColumn + " " + newColumn + " " + typeColumn);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
