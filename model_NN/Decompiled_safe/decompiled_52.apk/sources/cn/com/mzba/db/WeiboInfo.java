package cn.com.mzba.db;

public class WeiboInfo {
    private String content;
    private boolean haveImage = false;
    private String id;
    private String source;
    private String time;
    private String transmitContent;
    private String userIcon;
    private String userId;
    private String userName;

    public String getSource() {
        return this.source;
    }

    public void setSource(String source2) {
        this.source = source2;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId2) {
        this.userId = userId2;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName2) {
        this.userName = userName2;
    }

    public String getUserIcon() {
        return this.userIcon;
    }

    public void setUserIcon(String userIcon2) {
        this.userIcon = userIcon2;
    }

    public String getTime() {
        return this.time;
    }

    public void setTime(String time2) {
        this.time = time2;
    }

    public boolean isHaveImage() {
        return this.haveImage;
    }

    public void setHaveImage(boolean haveImage2) {
        this.haveImage = haveImage2;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content2) {
        this.content = content2;
    }

    public String getTransmitContent() {
        return this.transmitContent;
    }

    public void setTransmitContent(String transmitContent2) {
        this.transmitContent = transmitContent2;
    }
}
