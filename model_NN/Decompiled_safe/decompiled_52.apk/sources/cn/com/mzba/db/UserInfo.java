package cn.com.mzba.db;

import android.graphics.drawable.Drawable;
import java.io.Serializable;

public class UserInfo implements Serializable {
    public static final String ID = "_id";
    public static final String TOKEN = "token";
    public static final String TOKENSECRET = "tokenSecret";
    public static final String USERICON = "userIcon";
    public static final String USERID = "userId";
    public static final String USERNAME = "userName";
    public static final String WEIBOID = "weiboId";
    private static final long serialVersionUID = 1;
    private String id;
    private String token;
    private String tokenSecret;
    private Drawable userIcon;
    private String userId;
    private String userName;
    private String weiboId;

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getWeiboId() {
        return this.weiboId;
    }

    public void setWeiboId(String weiboId2) {
        this.weiboId = weiboId2;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId2) {
        this.userId = userId2;
    }

    public String getToken() {
        return this.token;
    }

    public void setToken(String token2) {
        this.token = token2;
    }

    public String getTokenSecret() {
        return this.tokenSecret;
    }

    public void setTokenSecret(String tokenSecret2) {
        this.tokenSecret = tokenSecret2;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName2) {
        this.userName = userName2;
    }

    public Drawable getUserIcon() {
        return this.userIcon;
    }

    public void setUserIcon(Drawable userIcon2) {
        this.userIcon = userIcon2;
    }
}
