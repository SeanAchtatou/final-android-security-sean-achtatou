package cn.com.mzba.db;

import java.io.Serializable;

public class UnReadInfo implements Serializable {
    private static final long serialVersionUID = 1;
    private int comments;
    private int dm;
    private int followers;
    private int mentions;

    public int getComments() {
        return this.comments;
    }

    public void setComments(int comments2) {
        this.comments = comments2;
    }

    public int getFollowers() {
        return this.followers;
    }

    public void setFollowers(int followers2) {
        this.followers = followers2;
    }

    public int getMentions() {
        return this.mentions;
    }

    public void setMentions(int mentions2) {
        this.mentions = mentions2;
    }

    public int getDm() {
        return this.dm;
    }

    public void setDm(int dm2) {
        this.dm = dm2;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }
}
