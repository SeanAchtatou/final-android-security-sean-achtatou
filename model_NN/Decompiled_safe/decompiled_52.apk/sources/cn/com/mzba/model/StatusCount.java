package cn.com.mzba.model;

public class StatusCount {
    private String comments;
    private String id;
    private String rt;
    private String weiboId;

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getComments() {
        return this.comments;
    }

    public void setComments(String comments2) {
        this.comments = comments2;
    }

    public String getRt() {
        return this.rt;
    }

    public void setRt(String rt2) {
        this.rt = rt2;
    }

    public String getWeiboId() {
        return this.weiboId;
    }

    public void setWeiboId(String weiboId2) {
        this.weiboId = weiboId2;
    }
}
