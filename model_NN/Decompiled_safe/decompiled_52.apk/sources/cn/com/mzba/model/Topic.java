package cn.com.mzba.model;

import java.io.Serializable;

public class Topic implements Serializable {
    private static final long serialVersionUID = 3588982067295810552L;
    private String hotword;
    private String id;
    private String number;
    private String weiboId;

    public String getWeiboId() {
        return this.weiboId;
    }

    public void setWeiboId(String weiboId2) {
        this.weiboId = weiboId2;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getHotword() {
        return this.hotword;
    }

    public void setHotword(String hotword2) {
        this.hotword = hotword2;
    }

    public String getNumber() {
        return this.number;
    }

    public void setNumber(String number2) {
        this.number = number2;
    }
}
