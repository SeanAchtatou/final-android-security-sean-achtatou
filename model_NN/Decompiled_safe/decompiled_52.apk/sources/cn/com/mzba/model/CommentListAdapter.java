package cn.com.mzba.model;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import cn.com.mzba.kdwb.R;
import cn.com.mzba.service.DateUtil;
import cn.com.mzba.service.TextAutoLink;
import java.util.List;

public class CommentListAdapter extends BaseAdapter {
    private Activity activity;
    private List<Comment> commentList;

    public static class ViewHolder {
        TextView tvContent;
        TextView tvTime;
        TextView tvUserName;
    }

    public CommentListAdapter(Activity activity2, List<Comment> commentList2) {
        this.activity = activity2;
        this.commentList = commentList2;
    }

    public int getCount() {
        return this.commentList.size() + 2;
    }

    public Object getItem(int position) {
        return this.commentList.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if (position == 0) {
            return LayoutInflater.from(this.activity.getApplicationContext()).inflate((int) R.layout.listheader, (ViewGroup) null);
        }
        if (position == getCount() - 1) {
            return LayoutInflater.from(this.activity.getApplicationContext()).inflate((int) R.layout.listfooter, (ViewGroup) null);
        }
        ViewHolder holder = new ViewHolder();
        if (convertView == null || convertView.getId() != R.id.commentlistitem_layout) {
            view = LayoutInflater.from(this.activity.getApplicationContext()).inflate((int) R.layout.commentlistitem, (ViewGroup) null);
            holder.tvUserName = (TextView) view.findViewById(R.id.comment_userName);
            holder.tvTime = (TextView) view.findViewById(R.id.comment_time);
            holder.tvContent = (TextView) view.findViewById(R.id.comment_text);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }
        Comment comment = this.commentList.get(position - 1);
        if (comment != null) {
            TextAutoLink.addURLSpan(this.activity, comment.getText(), holder.tvContent);
            holder.tvTime.setText(DateUtil.getCreateAt(comment.getCreated_at()));
            holder.tvUserName.setText(comment.getUser().getScreenName());
        }
        return view;
    }

    public void addMoreDatas(List<Comment> mores) {
        this.commentList.addAll(mores);
        notifyDataSetChanged();
    }
}
