package cn.com.mzba.model;

import java.io.Serializable;

public class Status implements Serializable {
    private static final long serialVersionUID = 1;
    private String bmiddlePic;
    private String comments;
    private String createdAt;
    private String cursorId;
    private boolean favorited;
    private String id;
    private String inReplyToScreenName;
    private String inReplyToStatusId;
    private String inReplyToUserId;
    private String mid;
    private String originalPic;
    private boolean retweetedStatus;
    private String rt;
    private String source;
    private Status status;
    private String text;
    private String thumbnailPic;
    private boolean truncated;
    private User user;
    private String weiboId;

    public String getCursorId() {
        return this.cursorId;
    }

    public void setCursorId(String cursorId2) {
        this.cursorId = cursorId2;
    }

    public String getWeiboId() {
        return this.weiboId;
    }

    public void setWeiboId(String weiboId2) {
        this.weiboId = weiboId2;
    }

    public String getBmiddlePic() {
        return this.bmiddlePic;
    }

    public void setBmiddlePic(String bmiddlePic2) {
        this.bmiddlePic = bmiddlePic2;
    }

    public Status getStatus() {
        return this.status;
    }

    public void setStatus(Status status2) {
        this.status = status2;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user2) {
        this.user = user2;
    }

    public String getCreatedAt() {
        return this.createdAt;
    }

    public void setCreatedAt(String createdAt2) {
        this.createdAt = createdAt2;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text2) {
        this.text = text2;
    }

    public String getSource() {
        return this.source;
    }

    public void setSource(String source2) {
        this.source = source2;
    }

    public boolean isFavorited() {
        return this.favorited;
    }

    public void setFavorited(boolean favorited2) {
        this.favorited = favorited2;
    }

    public boolean isTruncated() {
        return this.truncated;
    }

    public void setTruncated(boolean truncated2) {
        this.truncated = truncated2;
    }

    public String getInReplyToStatusId() {
        return this.inReplyToStatusId;
    }

    public void setInReplyToStatusId(String inReplyToStatusId2) {
        this.inReplyToStatusId = inReplyToStatusId2;
    }

    public String getInReplyToUserId() {
        return this.inReplyToUserId;
    }

    public void setInReplyToUserId(String inReplyToUserId2) {
        this.inReplyToUserId = inReplyToUserId2;
    }

    public String getInReplyToScreenName() {
        return this.inReplyToScreenName;
    }

    public void setInReplyToScreenName(String inReplyToScreenName2) {
        this.inReplyToScreenName = inReplyToScreenName2;
    }

    public String getThumbnailPic() {
        return this.thumbnailPic;
    }

    public void setThumbnailPic(String thumbnailPic2) {
        this.thumbnailPic = thumbnailPic2;
    }

    public String getBmiddle_pic() {
        return this.bmiddlePic;
    }

    public void setBmiddle_pic(String bmiddlePic2) {
        this.bmiddlePic = bmiddlePic2;
    }

    public String getOriginalPic() {
        return this.originalPic;
    }

    public void setOriginalPic(String originalPic2) {
        this.originalPic = originalPic2;
    }

    public boolean isRetweetedStatus() {
        return this.retweetedStatus;
    }

    public void setRetweetedStatus(boolean retweetedStatus2) {
        this.retweetedStatus = retweetedStatus2;
    }

    public String getMid() {
        return this.mid;
    }

    public void setMid(String mid2) {
        this.mid = mid2;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public String getRt() {
        return this.rt;
    }

    public void setRt(String rt2) {
        this.rt = rt2;
    }

    public String getComments() {
        return this.comments;
    }

    public void setComments(String comments2) {
        this.comments = comments2;
    }

    public String toString() {
        return "Weibo [createdAt=" + this.createdAt + ", id=" + this.id + ", text=" + this.text + ", source=" + this.source + ", favorited=" + this.favorited + ", truncated=" + this.truncated + ", inReplyToStatusId=" + this.inReplyToStatusId + ", inReplyToUserId=" + this.inReplyToUserId + ", inReplyToScreenName=" + this.inReplyToScreenName + ", thumbnailPic=" + this.thumbnailPic + ", bmiddlePic=" + this.bmiddlePic + ", originalPic=" + this.originalPic + ", retweetedStatus=" + this.retweetedStatus + ", user=" + this.user + "]";
    }
}
