package cn.com.mzba.model;

import java.io.Serializable;

public class Emotions implements Serializable {
    public static final String CATEGORY = "category";
    public static final String ID = "_id";
    public static final String IMAGENAME = "imageName";
    public static final String PHRASE = "phrase";
    public static final String TYPE = "type";
    public static final String URL = "url";
    private static final long serialVersionUID = 1;
    private String category;
    private String imageName;
    private String isCommon;
    private String isHot;
    private String orderNumber;
    private String phrase;
    private String type;
    private String url;
    private String weiboId;

    public String getWeiboId() {
        return this.weiboId;
    }

    public void setWeiboId(String weiboId2) {
        this.weiboId = weiboId2;
    }

    public String getImageName() {
        return this.imageName;
    }

    public void setImageName(String imageName2) {
        this.imageName = imageName2;
    }

    public String getPhrase() {
        return this.phrase;
    }

    public void setPhrase(String phrase2) {
        this.phrase = phrase2;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type2) {
        this.type = type2;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public String getIsHot() {
        return this.isHot;
    }

    public void setIsHot(String isHot2) {
        this.isHot = isHot2;
    }

    public String getIsCommon() {
        return this.isCommon;
    }

    public void setIsCommon(String isCommon2) {
        this.isCommon = isCommon2;
    }

    public String getOrderNumber() {
        return this.orderNumber;
    }

    public void setOrderNumber(String orderNumber2) {
        this.orderNumber = orderNumber2;
    }

    public String getCategory() {
        return this.category;
    }

    public void setCategory(String category2) {
        this.category = category2;
    }
}
