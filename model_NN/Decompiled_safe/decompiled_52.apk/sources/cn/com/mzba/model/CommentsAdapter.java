package cn.com.mzba.model;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import cn.com.mzba.kdwb.R;
import cn.com.mzba.service.AsyncImageLoader;
import cn.com.mzba.service.DateUtil;
import cn.com.mzba.service.ImageCallback;
import cn.com.mzba.service.TextAutoLink;
import java.util.List;

public class CommentsAdapter extends BaseAdapter {
    private Activity activity;
    private AsyncImageLoader imageLoader = new AsyncImageLoader();
    private List<Comment> statusList;

    public static class ViewHolder {
        ImageView ivHavePic;
        ImageView ivIsV;
        ImageView ivPicture;
        ImageView ivTransmitPicture;
        ImageView ivUserIcon;
        LinearLayout transmitLayout;
        TextView tvComments;
        TextView tvContent;
        TextView tvSource;
        TextView tvTime;
        TextView tvTransmit;
        TextView tvTransmitContent;
        TextView tvUserName;
    }

    public CommentsAdapter(Activity activity2, List<Comment> status) {
        this.activity = activity2;
        this.statusList = status;
    }

    public int getCount() {
        return this.statusList.size() + 2;
    }

    public Object getItem(int position) {
        return this.statusList.get(position);
    }

    public long getItemId(int position) {
        if (position == 0) {
            return 0;
        }
        if (position <= 0 || position >= getCount() - 1) {
            return -1;
        }
        return getItemId(position - 1);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if (position == 0) {
            return LayoutInflater.from(this.activity.getApplicationContext()).inflate((int) R.layout.listheader, (ViewGroup) null);
        }
        if (position == getCount() - 1) {
            return LayoutInflater.from(this.activity.getApplicationContext()).inflate((int) R.layout.listfooter, (ViewGroup) null);
        }
        ViewHolder holder = new ViewHolder();
        if (convertView == null || convertView.getId() != R.id.homelistitem_layout) {
            view = LayoutInflater.from(this.activity.getApplicationContext()).inflate((int) R.layout.homelistitem, (ViewGroup) null);
            holder.ivUserIcon = (ImageView) view.findViewById(R.id.userIcon);
            holder.ivHavePic = (ImageView) view.findViewById(R.id.weiboImage);
            holder.ivPicture = (ImageView) view.findViewById(R.id.staticPic);
            holder.tvUserName = (TextView) view.findViewById(R.id.userName);
            holder.tvTime = (TextView) view.findViewById(R.id.weiboTime);
            holder.tvContent = (TextView) view.findViewById(R.id.weiboContent);
            holder.tvSource = (TextView) view.findViewById(R.id.source);
            holder.tvComments = (TextView) view.findViewById(R.id.commentsCount);
            holder.tvTransmit = (TextView) view.findViewById(R.id.rtCount);
            holder.ivTransmitPicture = (ImageView) view.findViewById(R.id.rtstaticPic);
            holder.tvTransmitContent = (TextView) view.findViewById(R.id.transmitContent);
            holder.transmitLayout = (LinearLayout) view.findViewById(R.id.transmit_layout);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }
        Comment status = this.statusList.get(position - 1);
        if (status != null) {
            User user = status.getUser();
            holder.tvUserName.setText(user.getScreenName());
            holder.ivUserIcon.setImageDrawable(this.imageLoader.loadDrawable(user.getProfileImageUrl(), holder.ivUserIcon, new ImageCallback() {
                public void imageLoaded(Drawable imageDrawable, ImageView imageView, String imageUrl) {
                    imageView.setImageDrawable(imageDrawable);
                }
            }));
            String time = status.getCreated_at();
            if (time != null && !"".equals(time)) {
                holder.tvTime.setText(DateUtil.getCreateAt(time));
            }
            TextAutoLink.addURLSpan(this.activity, status.getText(), holder.tvContent);
            holder.transmitLayout.setVisibility(0);
            Status commentStatus = status.getStatus();
            if (commentStatus != null) {
                TextAutoLink.addURLSpan(this.activity, commentStatus.getText(), holder.tvTransmitContent);
                holder.transmitLayout.setBackgroundResource(R.drawable.popup);
            }
        }
        return view;
    }

    public void addMoreDatas(List<Comment> mores) {
        this.statusList.addAll(mores);
        notifyDataSetChanged();
    }

    public void delateDatas(int size) {
        for (int i = 0; i <= size - 1; i++) {
            this.statusList.remove(i);
        }
    }
}
