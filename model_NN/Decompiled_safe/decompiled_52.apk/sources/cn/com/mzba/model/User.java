package cn.com.mzba.model;

import java.io.Serializable;

public class User implements Serializable {
    private static final long serialVersionUID = 1;
    private boolean black;
    private String city;
    private String createdAt;
    private String description;
    private String domain;
    private String favouritesCount;
    private String followersCount;
    private boolean following;
    private String friendsCount;
    private String gender;
    private String id;
    private String location;
    private String name;
    private String nextCursor;
    private String profileImageUrl;
    private String province;
    private String screenName;
    private Status status;
    private String statusesCount;
    private String topicsCount;
    private String url;
    private boolean verified;
    private String weiboId;
    private String weiboid;

    public String getWeiboId() {
        return this.weiboId;
    }

    public void setWeiboId(String weiboId2) {
        this.weiboId = weiboId2;
    }

    public String getNextCursor() {
        return this.nextCursor;
    }

    public void setNextCursor(String nextCursor2) {
        this.nextCursor = nextCursor2;
    }

    public Status getStatus() {
        return this.status;
    }

    public void setStatus(Status status2) {
        this.status = status2;
    }

    public String getWeiboid() {
        return this.weiboid;
    }

    public void setWeiboid(String weiboid2) {
        this.weiboid = weiboid2;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getScreenName() {
        return this.screenName;
    }

    public void setScreenName(String screenName2) {
        this.screenName = screenName2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getProvince() {
        return this.province;
    }

    public void setProvince(String province2) {
        this.province = province2;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city2) {
        this.city = city2;
    }

    public String getLocation() {
        return this.location;
    }

    public void setLocation(String location2) {
        this.location = location2;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description2) {
        this.description = description2;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public String getProfileImageUrl() {
        return this.profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl2) {
        this.profileImageUrl = profileImageUrl2;
    }

    public String getDomain() {
        return this.domain;
    }

    public void setDomain(String domain2) {
        this.domain = domain2;
    }

    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender2) {
        this.gender = gender2;
    }

    public String getFollowersCount() {
        return this.followersCount;
    }

    public void setFollowersCount(String followersCount2) {
        this.followersCount = followersCount2;
    }

    public String getFriendsCount() {
        return this.friendsCount;
    }

    public void setFriendsCount(String friendsCount2) {
        this.friendsCount = friendsCount2;
    }

    public String getStatusesCount() {
        return this.statusesCount;
    }

    public void setStatusesCount(String statusesCount2) {
        this.statusesCount = statusesCount2;
    }

    public String getTopicsCount() {
        return this.topicsCount;
    }

    public void setTopicsCount(String topicsCount2) {
        this.topicsCount = topicsCount2;
    }

    public String getFavouritesCount() {
        return this.favouritesCount;
    }

    public void setFavouritesCount(String favouritesCount2) {
        this.favouritesCount = favouritesCount2;
    }

    public String getCreatedAt() {
        return this.createdAt;
    }

    public void setCreatedAt(String createdAt2) {
        this.createdAt = createdAt2;
    }

    public boolean isFollowing() {
        return this.following;
    }

    public void setFollowing(boolean following2) {
        this.following = following2;
    }

    public boolean isVerified() {
        return this.verified;
    }

    public void setVerified(boolean verified2) {
        this.verified = verified2;
    }

    public boolean isBlack() {
        return this.black;
    }

    public void setBlack(boolean black2) {
        this.black = black2;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public String toString() {
        return "User [id=" + this.id + ", screenName=" + this.screenName + ", name=" + this.name + ", province=" + this.province + ", city=" + this.city + ", location=" + this.location + ", description=" + this.description + ", url=" + this.url + ", profileImageUrl=" + this.profileImageUrl + ", domain=" + this.domain + ", gender=" + this.gender + ", followersCount=" + this.followersCount + ", friendsCount=" + this.friendsCount + ", statusesCount=" + this.statusesCount + ", favouritesCount=" + this.favouritesCount + ", createdAt=" + this.createdAt + ", following=" + this.following + ", verified=" + this.verified + "]";
    }
}
