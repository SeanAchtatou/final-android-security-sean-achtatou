package cn.com.mzba.model;

import java.util.Comparator;

public class ComparatorStatus implements Comparator<Status> {
    public int compare(Status status0, Status status1) {
        return status0.getId().compareTo(status1.getId());
    }
}
