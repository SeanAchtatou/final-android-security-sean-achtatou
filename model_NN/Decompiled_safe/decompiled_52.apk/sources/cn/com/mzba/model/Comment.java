package cn.com.mzba.model;

import java.io.Serializable;

public class Comment implements Serializable {
    private static final long serialVersionUID = 1;
    private Comment comment;
    private String created_at;
    private boolean favorited;
    private String id;
    private String source;
    private Status status;
    private String text;
    private boolean truncated;
    private User user;
    private String weiboId;

    public String getWeiboId() {
        return this.weiboId;
    }

    public void setWeiboId(String weiboId2) {
        this.weiboId = weiboId2;
    }

    public String getCreated_at() {
        return this.created_at;
    }

    public void setCreated_at(String createdAt) {
        this.created_at = createdAt;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text2) {
        this.text = text2;
    }

    public String getSource() {
        return this.source;
    }

    public void setSource(String source2) {
        this.source = source2;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user2) {
        this.user = user2;
    }

    public Status getStatus() {
        return this.status;
    }

    public void setStatus(Status status2) {
        this.status = status2;
    }

    public Comment getComment() {
        return this.comment;
    }

    public void setComment(Comment comment2) {
        this.comment = comment2;
    }

    public boolean isFavorited() {
        return this.favorited;
    }

    public void setFavorited(boolean favorited2) {
        this.favorited = favorited2;
    }

    public boolean isTruncated() {
        return this.truncated;
    }

    public void setTruncated(boolean truncated2) {
        this.truncated = truncated2;
    }
}
