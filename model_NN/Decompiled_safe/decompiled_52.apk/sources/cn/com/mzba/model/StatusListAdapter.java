package cn.com.mzba.model;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import cn.com.mzba.kdwb.CommentActivity;
import cn.com.mzba.kdwb.R;
import cn.com.mzba.service.AsyncImageLoader;
import cn.com.mzba.service.DateUtil;
import cn.com.mzba.service.ImageCallback;
import cn.com.mzba.service.TextAutoLink;
import java.util.Collections;
import java.util.List;

public class StatusListAdapter extends BaseAdapter {
    /* access modifiers changed from: private */
    public Activity activity;
    private AsyncImageLoader imageLoader = new AsyncImageLoader();
    private List<Status> statusList;

    public static class ViewHolder {
        ImageView ivHavePic;
        ImageView ivIsV;
        ImageView ivPicture;
        ImageView ivTransmitPicture;
        ImageView ivUserIcon;
        LinearLayout transmitLayout;
        TextView tvComments;
        TextView tvContent;
        TextView tvSource;
        TextView tvTime;
        TextView tvTransmit;
        TextView tvTransmitContent;
        TextView tvUserName;
    }

    public StatusListAdapter(Activity activity2, List<Status> status) {
        this.activity = activity2;
        this.statusList = status;
    }

    public int getCount() {
        return this.statusList.size() + 2;
    }

    public Object getItem(int position) {
        return this.statusList.get(position);
    }

    public long getItemId(int position) {
        if (position == 0) {
            return 0;
        }
        if (position <= 0 || position >= getCount() - 1) {
            return -1;
        }
        return getItemId(position - 1);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if (position == 0) {
            return LayoutInflater.from(this.activity.getApplicationContext()).inflate((int) R.layout.listheader, (ViewGroup) null);
        }
        if (position == getCount() - 1) {
            return LayoutInflater.from(this.activity.getApplicationContext()).inflate((int) R.layout.listfooter, (ViewGroup) null);
        }
        ViewHolder holder = new ViewHolder();
        if (convertView == null || convertView.getId() != R.id.homelistitem_layout) {
            view = LayoutInflater.from(this.activity.getApplicationContext()).inflate((int) R.layout.homelistitem, (ViewGroup) null);
            holder.ivUserIcon = (ImageView) view.findViewById(R.id.userIcon);
            holder.ivHavePic = (ImageView) view.findViewById(R.id.weiboImage);
            holder.ivPicture = (ImageView) view.findViewById(R.id.staticPic);
            holder.tvUserName = (TextView) view.findViewById(R.id.userName);
            holder.tvTime = (TextView) view.findViewById(R.id.weiboTime);
            holder.tvContent = (TextView) view.findViewById(R.id.weiboContent);
            holder.tvSource = (TextView) view.findViewById(R.id.source);
            holder.tvComments = (TextView) view.findViewById(R.id.commentsCount);
            holder.tvTransmit = (TextView) view.findViewById(R.id.rtCount);
            holder.ivTransmitPicture = (ImageView) view.findViewById(R.id.rtstaticPic);
            holder.tvTransmitContent = (TextView) view.findViewById(R.id.transmitContent);
            holder.transmitLayout = (LinearLayout) view.findViewById(R.id.transmit_layout);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }
        Status status = this.statusList.get(position - 1);
        if (status != null) {
            String id = status.getId();
            User user = status.getUser();
            holder.tvUserName.setText(user.getScreenName());
            holder.ivUserIcon.setImageDrawable(this.imageLoader.loadDrawable(user.getProfileImageUrl(), holder.ivUserIcon, new ImageCallback() {
                public void imageLoaded(Drawable imageDrawable, ImageView imageView, String imageUrl) {
                    imageView.setImageDrawable(imageDrawable);
                }
            }));
            String thumbnailPic = status.getThumbnailPic();
            Log.i(StatusListAdapter.class.getCanonicalName(), "image src=" + thumbnailPic);
            if (thumbnailPic == null || thumbnailPic.equals("") || thumbnailPic.equals("null")) {
                holder.ivPicture.setVisibility(8);
                holder.ivHavePic.setVisibility(8);
            } else {
                holder.ivPicture.setVisibility(0);
                holder.ivHavePic.setVisibility(0);
                holder.ivHavePic.setImageResource(R.drawable.pic);
                Drawable picDrawable = this.imageLoader.loadDrawable(thumbnailPic, holder.ivPicture, new ImageCallback() {
                    public void imageLoaded(Drawable imageDrawable, ImageView imageView, String imageUrl) {
                        imageView.setImageDrawable(imageDrawable);
                    }
                });
                if (picDrawable != null) {
                    holder.ivPicture.setImageDrawable(picDrawable);
                }
            }
            String time = status.getCreatedAt();
            if (time != null && !"".equals(time)) {
                holder.tvTime.setText(DateUtil.getCreateAt(time));
            }
            TextAutoLink.addURLSpan(this.activity, status.getText(), holder.tvContent);
            String source = status.getSource();
            if (source != null) {
                holder.tvSource.setText(Html.fromHtml(source));
            }
            if (status.isRetweetedStatus()) {
                holder.transmitLayout.setVisibility(0);
                Status transmitstatus = status.getStatus();
                TextAutoLink.addURLSpan(this.activity, transmitstatus.getText(), holder.tvTransmitContent);
                String transmitThumbnailPic = transmitstatus.getThumbnailPic();
                if (transmitThumbnailPic == null || transmitThumbnailPic.equals("") || transmitThumbnailPic.equals("null")) {
                    holder.ivTransmitPicture.setVisibility(8);
                } else {
                    holder.ivTransmitPicture.setVisibility(0);
                    Drawable picDrawable2 = this.imageLoader.loadDrawable(transmitThumbnailPic, holder.ivTransmitPicture, new ImageCallback() {
                        public void imageLoaded(Drawable imageDrawable, ImageView imageView, String imageUrl) {
                            imageView.setImageDrawable(imageDrawable);
                        }
                    });
                    if (picDrawable2 != null) {
                        holder.ivTransmitPicture.setImageDrawable(picDrawable2);
                    }
                }
                holder.transmitLayout.setBackgroundResource(R.drawable.popup);
            } else {
                holder.transmitLayout.setVisibility(8);
            }
            String comments = status.getComments();
            String transmit = status.getRt();
            if (comments == null || "0".equals(comments)) {
                holder.tvComments.setVisibility(8);
            } else {
                holder.tvComments.setVisibility(0);
                holder.tvComments.setText(comments);
            }
            if (transmit == null || "0".equals(transmit)) {
                holder.tvTransmit.setVisibility(8);
            } else {
                holder.tvTransmit.setVisibility(0);
                holder.tvTransmit.setText(transmit);
            }
            final String str = id;
            final String str2 = comments;
            holder.tvComments.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent comment = new Intent(StatusListAdapter.this.activity.getApplicationContext(), CommentActivity.class);
                    comment.putExtra("id", str);
                    comment.putExtra("count", str2);
                    StatusListAdapter.this.activity.startActivity(comment);
                }
            });
            final String str3 = id;
            holder.tvTransmit.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent transmit = new Intent(StatusListAdapter.this.activity.getApplicationContext(), CommentActivity.class);
                    transmit.putExtra("id", str3);
                    StatusListAdapter.this.activity.startActivity(transmit);
                }
            });
        }
        return view;
    }

    public void addMoreDatas(List<Status> mores) {
        this.statusList.addAll(mores);
        Collections.sort(this.statusList, Collections.reverseOrder(new ComparatorStatus()));
        notifyDataSetChanged();
    }
}
