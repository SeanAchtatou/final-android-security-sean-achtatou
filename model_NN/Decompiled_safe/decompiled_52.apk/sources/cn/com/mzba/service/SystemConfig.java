package cn.com.mzba.service;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Intent;
import android.os.Build;

public class SystemConfig {
    public static final String CONSUMERKEY_NETEASE = "37neJqfpTWIuY6i1";
    public static final String CONSUMERKEY_SINA = "558136723";
    public static final String CONSUMERKEY_SOHU = "pAJXaLm0sIfFtn4RzB1m";
    public static final String CONSUMERKEY_TENCENT = "67145a8457c24627a47ffd1f20642bd5";
    public static final String CONSUMERSECRET_NETEASE = "lQGyuz93zhmu6UBy6Fqjko73rKF2P1mA";
    public static final String CONSUMERSECRET_SINA = "315d1d5f23216f4e415130cc9348d833";
    public static final String CONSUMERSECRET_SOHU = "%r(n9^EQd1Jhr-UsIHdrtU^Qce%)3q*^X7Ei^$hJ";
    public static final String CONSUMERSECRET_TENCENT = "56571322af1d4669310a4a728655d11f";
    public static final String ERROR = "error";
    public static final String HTTP_DELETE = "DELETE";
    public static final String HTTP_GET = "GET";
    public static final String HTTP_POST = "POST";
    public static boolean ISACCESS = false;
    public static final String NETEASE_WEIBO = "netease_weibo";
    public static final String OAUTH_TOKEN = "oauth_token";
    public static final String OAUTH_TOKEN_SECRET = "oauth_token_secret";
    public static boolean PRE_FROYO = false;
    public static final int SDK_VERSION_FROYO = 8;
    public static final String SINA_WEIBO = "sina_weibo";
    public static final String SOHU_WEIBO = "sohu_weibo";
    public static final String SUCCESS = "success";
    public static final String TENCENT_WEIBO = "tencent_weibo";
    public static final String accessTokenEndpointUrl_netease = "http://api.t.163.com/oauth/access_token";
    public static final String accessTokenEndpointUrl_sina = "http://api.t.sina.com.cn/oauth/access_token";
    public static final String accessTokenEndpointUrl_sohu = "http://api.t.sohu.com/oauth/access_token";
    public static final String accessTokenEndpointUrl_tencent = "https://open.t.qq.com/cgi-bin/access_token";
    public static final String authorizationWebsiteUrl_netease = "http://api.t.163.com/oauth/authorize?client_type=mobile";
    public static final String authorizationWebsiteUrl_sina = "http://api.t.sina.com.cn/oauth/authorize?display=mobile";
    public static final String authorizationWebsiteUrl_sohu = "http://api.t.sohu.com/oauth/authorize";
    public static final String authorizationWebsiteUrl_tencent = "https://open.t.qq.com/cgi-bin/authorize";
    public static boolean autoLoadMode = false;
    public static boolean autoRemind = false;
    public static String checkTime = null;
    public static final String requestTokenEndpointUrl_netease = "http://api.t.163.com/oauth/request_token";
    public static final String requestTokenEndpointUrl_sina = "http://api.t.sina.com.cn/oauth/request_token";
    public static final String requestTokenEndpointUrl_sohu = "http://api.t.sohu.com/oauth/request_token";
    public static final String requestTokenEndpointUrl_tencent = "https://open.t.qq.com/cgi-bin/request_token";
    public static String textSize;

    static {
        boolean z;
        if (getSDKVersionNumber() < 8) {
            z = true;
        } else {
            z = false;
        }
        PRE_FROYO = z;
    }

    public static int getSDKVersionNumber() {
        try {
            return Integer.valueOf(Build.VERSION.SDK).intValue();
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public static void back(Activity activity) {
        ActivityManager activityManager = (ActivityManager) activity.getSystemService("activity");
        if (PRE_FROYO) {
            activityManager.restartPackage(activity.getPackageName());
            return;
        }
        Intent startMain = new Intent("android.intent.action.MAIN");
        startMain.addCategory("android.intent.category.HOME");
        startMain.setFlags(268435456);
        activity.startActivity(startMain);
        System.exit(0);
    }
}
