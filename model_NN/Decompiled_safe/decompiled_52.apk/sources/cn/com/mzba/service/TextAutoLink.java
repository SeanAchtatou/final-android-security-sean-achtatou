package cn.com.mzba.service;

import android.app.Activity;
import android.text.SpannableString;
import android.text.style.URLSpan;
import android.widget.TextView;

public class TextAutoLink {
    public static char[] strarray;

    public static void addURLSpan(Activity activity, String str, TextView textView) {
        SpannableString ss = new SpannableString(str);
        try {
            strarray = str.toCharArray();
            int length = str.length() - 10;
            int start = str.indexOf("http://");
            if (start != -1) {
                ss.setSpan(new URLSpan(str), start, str.length(), 33);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            int l = str.length();
            StringBuffer sb = null;
            boolean start2 = false;
            int startIndex = 0;
            for (int i = 0; i < l; i++) {
                if (strarray[i] == '@') {
                    start2 = true;
                    sb = new StringBuffer("weibo://weibo.view/");
                    startIndex = i;
                } else if (start2 && strarray[i] == ':') {
                    ss.setSpan(new URLSpan(sb.toString()), startIndex, i, 33);
                    sb = null;
                    start2 = false;
                }
            }
            boolean start3 = false;
            int startIndex2 = 0;
            for (int i2 = 0; i2 < l; i2++) {
                if (strarray[i2] == '#') {
                    if (!start3) {
                        start3 = true;
                        sb = new StringBuffer("weibo://weibo.view/");
                        startIndex2 = i2;
                    } else {
                        sb.append('#');
                        ss.setSpan(new URLSpan(sb.toString()), startIndex2, i2 + 1, 33);
                        sb = null;
                        start3 = false;
                    }
                } else if (start3) {
                    sb.append(strarray[i2]);
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        textView.setText(ss);
        textView.setTextSize(Float.parseFloat(SystemConfig.textSize == null ? "18" : SystemConfig.textSize));
        strarray = null;
    }
}
