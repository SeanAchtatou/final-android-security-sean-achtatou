package cn.com.mzba.service;

public class StringUtil {
    public static String arrayToString(String[] values) {
        String result = "";
        if (values == null || values.length <= 0) {
            return result;
        }
        for (int i = 0; i < values.length; i++) {
            result = String.valueOf(result) + values[i] + ",";
        }
        return result.substring(0, result.length() - 1);
    }

    public static String[] StringToArray(String value) {
        return value.split(",");
    }
}
