package cn.com.mzba.service;

import android.test.AndroidTestCase;
import cn.com.mzba.db.SqliteHelper;
import commonshttp.CommonsHttpOAuthConsumer;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.basic.DefaultOAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import oauth.signpost.http.HttpParameters;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

public class Test extends AndroidTestCase {
    public void test() throws Exception {
        OAuthConsumer consumer = new DefaultOAuthConsumer(SystemConfig.CONSUMERKEY_SOHU, SystemConfig.CONSUMERSECRET_SOHU);
        consumer.setTokenWithSecret("326314828bdbb4e76c113a5d88813dc0", "04408f0ef0e92a14242bfdb133607fbc");
        HttpURLConnection request = (HttpURLConnection) new URL("http://api.t.sohu.com/statuses/update.json").openConnection();
        request.setDoOutput(true);
        request.setRequestMethod(SystemConfig.HTTP_POST);
        HttpParameters para = new HttpParameters();
        para.put(SqliteHelper.TABLE_STATUS, URLEncoder.encode("为什么受伤的总是我？", "utf-8").replaceAll("\\+", "%20"));
        consumer.setAdditionalParameters(para);
        consumer.sign(request);
        OutputStream ot = request.getOutputStream();
        ot.write(("status=" + URLEncoder.encode("为什么受伤的总是我？", "utf-8")).replaceAll("\\+", "%20").getBytes());
        ot.flush();
        ot.close();
        System.out.println("Sending request...");
        request.connect();
        System.out.println("Response: " + request.getResponseCode() + " " + request.getResponseMessage());
        BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream()));
        while (true) {
            String b = reader.readLine();
            if (b != null) {
                System.out.println(b);
            } else {
                return;
            }
        }
    }

    public void test2() throws Exception {
        OAuthConsumer consumer = new CommonsHttpOAuthConsumer(SystemConfig.CONSUMERKEY_SOHU, SystemConfig.CONSUMERSECRET_SOHU);
        consumer.setTokenWithSecret("326314828bdbb4e76c113a5d88813dc0", "04408f0ef0e92a14242bfdb133607fbc");
        HttpPost post = new HttpPost("http://api.t.sohu.com/statuses/update.json");
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair(SqliteHelper.TABLE_STATUS, "hehdfsfdsfdsfhaha"));
        try {
            post.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        post.getParams().setBooleanParameter(HttpMethodParams.USE_EXPECT_CONTINUE, false);
        try {
            consumer.sign(post);
        } catch (OAuthMessageSignerException e2) {
            e2.printStackTrace();
        } catch (OAuthExpectationFailedException e3) {
            e3.printStackTrace();
        } catch (OAuthCommunicationException e4) {
            e4.printStackTrace();
        }
        try {
            System.out.println(String.valueOf(new DefaultHttpClient().execute(post).getStatusLine().getStatusCode()) + "****");
        } catch (ClientProtocolException e5) {
            e5.printStackTrace();
        } catch (IOException e6) {
            e6.printStackTrace();
        }
    }
}
