package cn.com.mzba.service;

import android.util.Log;
import cn.com.mzba.db.ConfigHelper;
import cn.com.mzba.db.SqliteHelper;
import cn.com.mzba.model.Status;
import cn.com.mzba.model.User;
import cn.com.mzba.oauth.OAuth;
import cn.com.mzba.oauth.Parameter;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

public class UserHttpUtils {
    public static OAuth oauthNetease = ServiceUtils.getNeteaseOauth();
    public static OAuth oauthSina = ServiceUtils.getSinaOauth();
    public static OAuth oauthSohu = ServiceUtils.getSohuOauth();
    public static OAuth oauthTencent = ServiceUtils.getTencentOauth();
    public static String userTokenNetease = ServiceUtils.userTokenNetease;
    public static String userTokenNeteaseSecret = ServiceUtils.userTokenNeteaseSecret;
    public static String userTokenSina = ServiceUtils.userTokenSina;
    public static String userTokenSinaSecret = ServiceUtils.userTokenSinaSecret;
    public static String userTokenSohu = ServiceUtils.userTokenSohu;
    public static String userTokenSohuSecret = ServiceUtils.userTokenSohuSecret;
    public static String userTokenTencent = ServiceUtils.userTokenTencent;
    public static String userTokenTencentSecret = ServiceUtils.userTokenTencentSecret;

    /* JADX INFO: Multiple debug info for r8v9 java.lang.String: [D('builder' java.lang.StringBuilder), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r9v5 org.json.JSONObject: [D('data' org.json.JSONObject), D('is' java.io.InputStream)] */
    /* JADX INFO: Multiple debug info for r8v38 java.lang.String: [D('builder' java.lang.StringBuilder), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r9v19 org.json.JSONObject: [D('data' org.json.JSONObject), D('is' java.io.InputStream)] */
    /* JADX INFO: Multiple debug info for r8v62 java.lang.String: [D('parameter' java.lang.String), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r8v64 org.json.JSONObject: [D('data' org.json.JSONObject), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r8v68 java.util.ArrayList: [D('weiboId' java.lang.String), D('params' java.util.List<org.apache.http.message.BasicNameValuePair>)] */
    /* JADX INFO: Multiple debug info for r8v77 java.lang.String: [D('buf' char[]), D('content' java.lang.String)] */
    /* JADX WARN: Type inference failed for: r9v9, types: [java.net.URLConnection] */
    /* JADX WARN: Type inference failed for: r9v23, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x022d  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x032e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static cn.com.mzba.model.User getUserById(java.lang.String r8, java.lang.String r9) {
        /*
            cn.com.mzba.model.User r7 = new cn.com.mzba.model.User
            r7.<init>()
            java.lang.String r1 = "sina_weibo"
            boolean r1 = r8.equals(r1)
            if (r1 == 0) goto L_0x00df
            java.lang.String r3 = "http://api.t.sina.com.cn/users/show.json"
            java.util.ArrayList r8 = new java.util.ArrayList
            r8.<init>()
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r2 = "user_id"
            r1.<init>(r2, r9)
            r8.add(r1)
            cn.com.mzba.oauth.OAuth r9 = cn.com.mzba.service.UserHttpUtils.oauthSina
            java.lang.String r1 = cn.com.mzba.service.UserHttpUtils.userTokenSina
            java.lang.String r2 = cn.com.mzba.service.UserHttpUtils.userTokenSinaSecret
            org.apache.http.HttpResponse r4 = r9.signRequest(r1, r2, r3, r8)
            if (r4 == 0) goto L_0x00d4
            r8 = 200(0xc8, float:2.8E-43)
            org.apache.http.StatusLine r9 = r4.getStatusLine()
            int r9 = r9.getStatusCode()
            if (r8 != r9) goto L_0x00d4
            org.apache.http.HttpEntity r8 = r4.getEntity()     // Catch:{ Exception -> 0x00da }
            java.io.InputStream r1 = r8.getContent()     // Catch:{ Exception -> 0x00da }
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ Exception -> 0x00da }
            java.io.InputStreamReader r8 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x00da }
            r8.<init>(r1)     // Catch:{ Exception -> 0x00da }
            r3.<init>(r8)     // Catch:{ Exception -> 0x00da }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00da }
            org.apache.http.HttpEntity r8 = r4.getEntity()     // Catch:{ Exception -> 0x00da }
            long r5 = r8.getContentLength()     // Catch:{ Exception -> 0x00da }
            int r8 = (int) r5     // Catch:{ Exception -> 0x00da }
            r9.<init>(r8)     // Catch:{ Exception -> 0x00da }
            r8 = 4000(0xfa0, float:5.605E-42)
            char[] r8 = new char[r8]     // Catch:{ Exception -> 0x00da }
            r2 = 0
        L_0x005b:
            int r2 = r3.read(r8)     // Catch:{ Exception -> 0x00da }
            r5 = -1
            if (r2 != r5) goto L_0x00d5
            r3.close()     // Catch:{ Exception -> 0x00da }
            r1.close()     // Catch:{ Exception -> 0x00da }
            java.lang.String r8 = r9.toString()     // Catch:{ Exception -> 0x00da }
            org.apache.http.HttpEntity r9 = r4.getEntity()     // Catch:{ Exception -> 0x00da }
            r9.consumeContent()     // Catch:{ Exception -> 0x00da }
            org.json.JSONObject r9 = new org.json.JSONObject     // Catch:{ Exception -> 0x00da }
            r9.<init>(r8)     // Catch:{ Exception -> 0x00da }
            if (r9 == 0) goto L_0x00d4
            java.lang.String r8 = "id"
            java.lang.String r8 = r9.getString(r8)     // Catch:{ Exception -> 0x00da }
            r7.setId(r8)     // Catch:{ Exception -> 0x00da }
            java.lang.String r8 = "screen_name"
            java.lang.String r8 = r9.getString(r8)     // Catch:{ Exception -> 0x00da }
            r7.setScreenName(r8)     // Catch:{ Exception -> 0x00da }
            java.lang.String r8 = "profile_image_url"
            java.lang.String r8 = r9.getString(r8)     // Catch:{ Exception -> 0x00da }
            r7.setProfileImageUrl(r8)     // Catch:{ Exception -> 0x00da }
            java.lang.String r8 = "location"
            java.lang.String r8 = r9.getString(r8)     // Catch:{ Exception -> 0x00da }
            r7.setLocation(r8)     // Catch:{ Exception -> 0x00da }
            java.lang.String r8 = "description"
            java.lang.String r8 = r9.getString(r8)     // Catch:{ Exception -> 0x00da }
            r7.setDescription(r8)     // Catch:{ Exception -> 0x00da }
            java.lang.String r8 = "gender"
            java.lang.String r8 = r9.getString(r8)     // Catch:{ Exception -> 0x00da }
            r7.setGender(r8)     // Catch:{ Exception -> 0x00da }
            java.lang.String r8 = "friends_count"
            java.lang.String r8 = r9.getString(r8)     // Catch:{ Exception -> 0x00da }
            r7.setFriendsCount(r8)     // Catch:{ Exception -> 0x00da }
            java.lang.String r8 = "statuses_count"
            java.lang.String r8 = r9.getString(r8)     // Catch:{ Exception -> 0x00da }
            r7.setStatusesCount(r8)     // Catch:{ Exception -> 0x00da }
            java.lang.String r8 = "followers_count"
            java.lang.String r8 = r9.getString(r8)     // Catch:{ Exception -> 0x00da }
            r7.setFollowersCount(r8)     // Catch:{ Exception -> 0x00da }
            java.lang.String r8 = "favourites_count"
            java.lang.String r8 = r9.getString(r8)     // Catch:{ Exception -> 0x00da }
            r7.setFavouritesCount(r8)     // Catch:{ Exception -> 0x00da }
        L_0x00d4:
            return r7
        L_0x00d5:
            r5 = 0
            r9.append(r8, r5, r2)     // Catch:{ Exception -> 0x00da }
            goto L_0x005b
        L_0x00da:
            r8 = move-exception
            r8.printStackTrace()
            goto L_0x00d4
        L_0x00df:
            java.lang.String r1 = "tencent_weibo"
            boolean r1 = r8.equals(r1)
            if (r1 == 0) goto L_0x01d8
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            if (r9 != 0) goto L_0x01be
            java.lang.String r3 = "http://open.t.qq.com/api/user/info"
            cn.com.mzba.oauth.Parameter r8 = new cn.com.mzba.oauth.Parameter
            java.lang.String r1 = "fotmat"
            java.lang.String r2 = "json"
            r8.<init>(r1, r2)
            r6.add(r8)
        L_0x00fc:
            cn.com.mzba.oauth.OAuth r1 = cn.com.mzba.service.UserHttpUtils.oauthTencent     // Catch:{ Exception -> 0x01b8 }
            java.lang.String r2 = "GET"
            java.lang.String r4 = cn.com.mzba.service.UserHttpUtils.userTokenTencent     // Catch:{ Exception -> 0x01b8 }
            java.lang.String r5 = cn.com.mzba.service.UserHttpUtils.userTokenTencentSecret     // Catch:{ Exception -> 0x01b8 }
            java.lang.String r8 = r1.getPostParameter(r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x01b8 }
            cn.com.mzba.oauth.OAuth r1 = cn.com.mzba.service.UserHttpUtils.oauthTencent     // Catch:{ Exception -> 0x01b8 }
            java.lang.String r8 = r1.httpGet(r3, r8)     // Catch:{ Exception -> 0x01b8 }
            if (r8 == 0) goto L_0x00d4
            java.lang.Class<cn.com.mzba.service.UserHttpUtils> r1 = cn.com.mzba.service.UserHttpUtils.class
            java.lang.String r1 = r1.getCanonicalName()     // Catch:{ Exception -> 0x01b8 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01b8 }
            java.lang.String r3 = "userInfo:"
            r2.<init>(r3)     // Catch:{ Exception -> 0x01b8 }
            java.lang.StringBuilder r2 = r2.append(r8)     // Catch:{ Exception -> 0x01b8 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x01b8 }
            android.util.Log.i(r1, r2)     // Catch:{ Exception -> 0x01b8 }
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ Exception -> 0x01b8 }
            r1.<init>(r8)     // Catch:{ Exception -> 0x01b8 }
            java.lang.String r8 = "data"
            org.json.JSONObject r8 = r1.getJSONObject(r8)     // Catch:{ Exception -> 0x01b8 }
            if (r8 == 0) goto L_0x00d4
            java.lang.String r1 = "name"
            java.lang.String r1 = r8.getString(r1)     // Catch:{ Exception -> 0x01b8 }
            r7.setId(r1)     // Catch:{ Exception -> 0x01b8 }
            java.lang.String r1 = "nick"
            java.lang.String r1 = r8.getString(r1)     // Catch:{ Exception -> 0x01b8 }
            r7.setScreenName(r1)     // Catch:{ Exception -> 0x01b8 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01b8 }
            java.lang.String r2 = "head"
            java.lang.String r2 = r8.getString(r2)     // Catch:{ Exception -> 0x01b8 }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ Exception -> 0x01b8 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x01b8 }
            java.lang.String r2 = "/40"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x01b8 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x01b8 }
            r7.setProfileImageUrl(r1)     // Catch:{ Exception -> 0x01b8 }
            java.lang.String r1 = "location"
            java.lang.String r1 = r8.getString(r1)     // Catch:{ Exception -> 0x01b8 }
            r7.setLocation(r1)     // Catch:{ Exception -> 0x01b8 }
            java.lang.String r1 = "introduction"
            java.lang.String r1 = r8.getString(r1)     // Catch:{ Exception -> 0x01b8 }
            r7.setDescription(r1)     // Catch:{ Exception -> 0x01b8 }
            java.lang.String r1 = "idolnum"
            java.lang.String r1 = r8.getString(r1)     // Catch:{ Exception -> 0x01b8 }
            r7.setFriendsCount(r1)     // Catch:{ Exception -> 0x01b8 }
            java.lang.String r1 = "tweetnum"
            java.lang.String r1 = r8.getString(r1)     // Catch:{ Exception -> 0x01b8 }
            r7.setStatusesCount(r1)     // Catch:{ Exception -> 0x01b8 }
            java.lang.String r1 = "fansnum"
            java.lang.String r1 = r8.getString(r1)     // Catch:{ Exception -> 0x01b8 }
            r7.setFollowersCount(r1)     // Catch:{ Exception -> 0x01b8 }
            if (r9 == 0) goto L_0x00d4
            java.lang.Boolean r9 = new java.lang.Boolean     // Catch:{ Exception -> 0x01b8 }
            java.lang.String r1 = "ismyidol"
            java.lang.String r1 = r8.getString(r1)     // Catch:{ Exception -> 0x01b8 }
            r9.<init>(r1)     // Catch:{ Exception -> 0x01b8 }
            boolean r9 = r9.booleanValue()     // Catch:{ Exception -> 0x01b8 }
            r7.setFollowing(r9)     // Catch:{ Exception -> 0x01b8 }
            java.lang.Boolean r9 = new java.lang.Boolean     // Catch:{ Exception -> 0x01b8 }
            java.lang.String r1 = "ismyblack"
            java.lang.String r8 = r8.getString(r1)     // Catch:{ Exception -> 0x01b8 }
            r9.<init>(r8)     // Catch:{ Exception -> 0x01b8 }
            boolean r8 = r9.booleanValue()     // Catch:{ Exception -> 0x01b8 }
            r7.setBlack(r8)     // Catch:{ Exception -> 0x01b8 }
            goto L_0x00d4
        L_0x01b8:
            r8 = move-exception
            r8.printStackTrace()
            goto L_0x00d4
        L_0x01be:
            java.lang.String r3 = "http://open.t.qq.com/api/user/other_info"
            cn.com.mzba.oauth.Parameter r8 = new cn.com.mzba.oauth.Parameter
            java.lang.String r1 = "name"
            r8.<init>(r1, r9)
            r6.add(r8)
            cn.com.mzba.oauth.Parameter r8 = new cn.com.mzba.oauth.Parameter
            java.lang.String r1 = "fotmat"
            java.lang.String r2 = "json"
            r8.<init>(r1, r2)
            r6.add(r8)
            goto L_0x00fc
        L_0x01d8:
            java.lang.String r1 = "sohu_weibo"
            boolean r1 = r8.equals(r1)
            if (r1 == 0) goto L_0x02d9
            java.lang.String r3 = "http://api.t.sohu.com/users/show/id.json"
            r8 = 0
            java.lang.String r1 = ""
            if (r9 == 0) goto L_0x03e6
            java.lang.String r2 = ""
            boolean r2 = r9.equals(r2)     // Catch:{ MalformedURLException -> 0x02c1, IOException -> 0x02c7 }
            if (r2 != 0) goto L_0x03e6
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x02c1, IOException -> 0x02c7 }
            java.lang.String r2 = "?id="
            r1.<init>(r2)     // Catch:{ MalformedURLException -> 0x02c1, IOException -> 0x02c7 }
            java.lang.StringBuilder r9 = r1.append(r9)     // Catch:{ MalformedURLException -> 0x02c1, IOException -> 0x02c7 }
            java.lang.String r9 = r9.toString()     // Catch:{ MalformedURLException -> 0x02c1, IOException -> 0x02c7 }
        L_0x01fe:
            java.net.URL r1 = new java.net.URL     // Catch:{ MalformedURLException -> 0x02c1, IOException -> 0x02c7 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x02c1, IOException -> 0x02c7 }
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ MalformedURLException -> 0x02c1, IOException -> 0x02c7 }
            r2.<init>(r3)     // Catch:{ MalformedURLException -> 0x02c1, IOException -> 0x02c7 }
            java.lang.StringBuilder r9 = r2.append(r9)     // Catch:{ MalformedURLException -> 0x02c1, IOException -> 0x02c7 }
            java.lang.String r9 = r9.toString()     // Catch:{ MalformedURLException -> 0x02c1, IOException -> 0x02c7 }
            r1.<init>(r9)     // Catch:{ MalformedURLException -> 0x02c1, IOException -> 0x02c7 }
            java.net.URLConnection r9 = r1.openConnection()     // Catch:{ MalformedURLException -> 0x02c1, IOException -> 0x02c7 }
            r0 = r9
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x02c1, IOException -> 0x02c7 }
            r8 = r0
            java.lang.String r9 = "GET"
            r8.setRequestMethod(r9)     // Catch:{ MalformedURLException -> 0x02c1, IOException -> 0x02c7 }
        L_0x0221:
            cn.com.mzba.oauth.OAuth r9 = cn.com.mzba.service.UserHttpUtils.oauthSohu
            java.lang.String r1 = cn.com.mzba.service.UserHttpUtils.userTokenSohu
            java.lang.String r2 = cn.com.mzba.service.UserHttpUtils.userTokenSohuSecret
            java.net.HttpURLConnection r3 = r9.signRequest(r1, r2, r8)
            if (r3 == 0) goto L_0x00d4
            r8 = 200(0xc8, float:2.8E-43)
            int r9 = r3.getResponseCode()     // Catch:{ IOException -> 0x02bb, JSONException -> 0x02d3 }
            if (r8 != r9) goto L_0x00d4
            java.io.InputStream r9 = r3.getInputStream()     // Catch:{ IOException -> 0x02bb, JSONException -> 0x02d3 }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ IOException -> 0x02bb, JSONException -> 0x02d3 }
            java.io.InputStreamReader r8 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x02bb, JSONException -> 0x02d3 }
            r8.<init>(r9)     // Catch:{ IOException -> 0x02bb, JSONException -> 0x02d3 }
            r2.<init>(r8)     // Catch:{ IOException -> 0x02bb, JSONException -> 0x02d3 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x02bb, JSONException -> 0x02d3 }
            r8.<init>()     // Catch:{ IOException -> 0x02bb, JSONException -> 0x02d3 }
            r1 = 4000(0xfa0, float:5.605E-42)
            char[] r4 = new char[r1]     // Catch:{ IOException -> 0x02bb, JSONException -> 0x02d3 }
            r1 = 0
        L_0x024d:
            int r1 = r2.read(r4)     // Catch:{ IOException -> 0x02bb, JSONException -> 0x02d3 }
            r5 = -1
            if (r1 != r5) goto L_0x02cd
            r2.close()     // Catch:{ IOException -> 0x02bb, JSONException -> 0x02d3 }
            r9.close()     // Catch:{ IOException -> 0x02bb, JSONException -> 0x02d3 }
            java.lang.String r8 = r8.toString()     // Catch:{ IOException -> 0x02bb, JSONException -> 0x02d3 }
            r3.disconnect()     // Catch:{ IOException -> 0x02bb, JSONException -> 0x02d3 }
            org.json.JSONObject r9 = new org.json.JSONObject     // Catch:{ IOException -> 0x02bb, JSONException -> 0x02d3 }
            r9.<init>(r8)     // Catch:{ IOException -> 0x02bb, JSONException -> 0x02d3 }
            if (r9 == 0) goto L_0x00d4
            java.lang.String r8 = "id"
            java.lang.String r8 = r9.getString(r8)     // Catch:{ IOException -> 0x02bb, JSONException -> 0x02d3 }
            r7.setId(r8)     // Catch:{ IOException -> 0x02bb, JSONException -> 0x02d3 }
            java.lang.String r8 = "screen_name"
            java.lang.String r8 = r9.getString(r8)     // Catch:{ IOException -> 0x02bb, JSONException -> 0x02d3 }
            r7.setScreenName(r8)     // Catch:{ IOException -> 0x02bb, JSONException -> 0x02d3 }
            java.lang.String r8 = "profile_image_url"
            java.lang.String r8 = r9.getString(r8)     // Catch:{ IOException -> 0x02bb, JSONException -> 0x02d3 }
            r7.setProfileImageUrl(r8)     // Catch:{ IOException -> 0x02bb, JSONException -> 0x02d3 }
            java.lang.String r8 = "location"
            java.lang.String r8 = r9.getString(r8)     // Catch:{ IOException -> 0x02bb, JSONException -> 0x02d3 }
            r7.setLocation(r8)     // Catch:{ IOException -> 0x02bb, JSONException -> 0x02d3 }
            java.lang.String r8 = "description"
            java.lang.String r8 = r9.getString(r8)     // Catch:{ IOException -> 0x02bb, JSONException -> 0x02d3 }
            r7.setDescription(r8)     // Catch:{ IOException -> 0x02bb, JSONException -> 0x02d3 }
            java.lang.String r8 = "friends_count"
            java.lang.String r8 = r9.getString(r8)     // Catch:{ IOException -> 0x02bb, JSONException -> 0x02d3 }
            r7.setFriendsCount(r8)     // Catch:{ IOException -> 0x02bb, JSONException -> 0x02d3 }
            java.lang.String r8 = "statuses_count"
            java.lang.String r8 = r9.getString(r8)     // Catch:{ IOException -> 0x02bb, JSONException -> 0x02d3 }
            r7.setStatusesCount(r8)     // Catch:{ IOException -> 0x02bb, JSONException -> 0x02d3 }
            java.lang.String r8 = "followers_count"
            java.lang.String r8 = r9.getString(r8)     // Catch:{ IOException -> 0x02bb, JSONException -> 0x02d3 }
            r7.setFollowersCount(r8)     // Catch:{ IOException -> 0x02bb, JSONException -> 0x02d3 }
            java.lang.String r8 = "favourites_count"
            java.lang.String r8 = r9.getString(r8)     // Catch:{ IOException -> 0x02bb, JSONException -> 0x02d3 }
            r7.setFavouritesCount(r8)     // Catch:{ IOException -> 0x02bb, JSONException -> 0x02d3 }
            goto L_0x00d4
        L_0x02bb:
            r8 = move-exception
            r8.printStackTrace()
            goto L_0x00d4
        L_0x02c1:
            r9 = move-exception
            r9.printStackTrace()
            goto L_0x0221
        L_0x02c7:
            r9 = move-exception
            r9.printStackTrace()
            goto L_0x0221
        L_0x02cd:
            r5 = 0
            r8.append(r4, r5, r1)     // Catch:{ IOException -> 0x02bb, JSONException -> 0x02d3 }
            goto L_0x024d
        L_0x02d3:
            r8 = move-exception
            r8.printStackTrace()
            goto L_0x00d4
        L_0x02d9:
            java.lang.String r1 = "netease_weibo"
            boolean r8 = r8.equals(r1)
            if (r8 == 0) goto L_0x00d4
            java.lang.String r3 = "http://api.t.163.com/users/show.json"
            r8 = 0
            java.lang.String r1 = ""
            if (r9 == 0) goto L_0x03e3
            java.lang.String r2 = ""
            boolean r2 = r9.equals(r2)     // Catch:{ MalformedURLException -> 0x03cb, IOException -> 0x03d1 }
            if (r2 != 0) goto L_0x03e3
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x03cb, IOException -> 0x03d1 }
            java.lang.String r2 = "?id="
            r1.<init>(r2)     // Catch:{ MalformedURLException -> 0x03cb, IOException -> 0x03d1 }
            java.lang.StringBuilder r9 = r1.append(r9)     // Catch:{ MalformedURLException -> 0x03cb, IOException -> 0x03d1 }
            java.lang.String r9 = r9.toString()     // Catch:{ MalformedURLException -> 0x03cb, IOException -> 0x03d1 }
        L_0x02ff:
            java.net.URL r1 = new java.net.URL     // Catch:{ MalformedURLException -> 0x03cb, IOException -> 0x03d1 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x03cb, IOException -> 0x03d1 }
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ MalformedURLException -> 0x03cb, IOException -> 0x03d1 }
            r2.<init>(r3)     // Catch:{ MalformedURLException -> 0x03cb, IOException -> 0x03d1 }
            java.lang.StringBuilder r9 = r2.append(r9)     // Catch:{ MalformedURLException -> 0x03cb, IOException -> 0x03d1 }
            java.lang.String r9 = r9.toString()     // Catch:{ MalformedURLException -> 0x03cb, IOException -> 0x03d1 }
            r1.<init>(r9)     // Catch:{ MalformedURLException -> 0x03cb, IOException -> 0x03d1 }
            java.net.URLConnection r9 = r1.openConnection()     // Catch:{ MalformedURLException -> 0x03cb, IOException -> 0x03d1 }
            r0 = r9
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x03cb, IOException -> 0x03d1 }
            r8 = r0
            java.lang.String r9 = "GET"
            r8.setRequestMethod(r9)     // Catch:{ MalformedURLException -> 0x03cb, IOException -> 0x03d1 }
        L_0x0322:
            cn.com.mzba.oauth.OAuth r9 = cn.com.mzba.service.UserHttpUtils.oauthNetease
            java.lang.String r1 = cn.com.mzba.service.UserHttpUtils.userTokenNetease
            java.lang.String r2 = cn.com.mzba.service.UserHttpUtils.userTokenNeteaseSecret
            java.net.HttpURLConnection r3 = r9.signRequest(r1, r2, r8)
            if (r3 == 0) goto L_0x00d4
            r8 = 200(0xc8, float:2.8E-43)
            int r9 = r3.getResponseCode()     // Catch:{ IOException -> 0x03c5, JSONException -> 0x03dd }
            if (r8 != r9) goto L_0x00d4
            java.io.InputStream r9 = r3.getInputStream()     // Catch:{ IOException -> 0x03c5, JSONException -> 0x03dd }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ IOException -> 0x03c5, JSONException -> 0x03dd }
            java.io.InputStreamReader r8 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x03c5, JSONException -> 0x03dd }
            r8.<init>(r9)     // Catch:{ IOException -> 0x03c5, JSONException -> 0x03dd }
            r2.<init>(r8)     // Catch:{ IOException -> 0x03c5, JSONException -> 0x03dd }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x03c5, JSONException -> 0x03dd }
            r8.<init>()     // Catch:{ IOException -> 0x03c5, JSONException -> 0x03dd }
            r1 = 4000(0xfa0, float:5.605E-42)
            char[] r4 = new char[r1]     // Catch:{ IOException -> 0x03c5, JSONException -> 0x03dd }
            r1 = 0
        L_0x034e:
            int r1 = r2.read(r4)     // Catch:{ IOException -> 0x03c5, JSONException -> 0x03dd }
            r5 = -1
            if (r1 != r5) goto L_0x03d7
            r2.close()     // Catch:{ IOException -> 0x03c5, JSONException -> 0x03dd }
            r9.close()     // Catch:{ IOException -> 0x03c5, JSONException -> 0x03dd }
            java.lang.String r8 = r8.toString()     // Catch:{ IOException -> 0x03c5, JSONException -> 0x03dd }
            r3.disconnect()     // Catch:{ IOException -> 0x03c5, JSONException -> 0x03dd }
            org.json.JSONObject r9 = new org.json.JSONObject     // Catch:{ IOException -> 0x03c5, JSONException -> 0x03dd }
            r9.<init>(r8)     // Catch:{ IOException -> 0x03c5, JSONException -> 0x03dd }
            if (r9 == 0) goto L_0x00d4
            java.lang.String r8 = "id"
            java.lang.String r8 = r9.getString(r8)     // Catch:{ IOException -> 0x03c5, JSONException -> 0x03dd }
            r7.setId(r8)     // Catch:{ IOException -> 0x03c5, JSONException -> 0x03dd }
            java.lang.String r8 = "screen_name"
            java.lang.String r8 = r9.getString(r8)     // Catch:{ IOException -> 0x03c5, JSONException -> 0x03dd }
            r7.setScreenName(r8)     // Catch:{ IOException -> 0x03c5, JSONException -> 0x03dd }
            java.lang.String r8 = "profile_image_url"
            java.lang.String r8 = r9.getString(r8)     // Catch:{ IOException -> 0x03c5, JSONException -> 0x03dd }
            r7.setProfileImageUrl(r8)     // Catch:{ IOException -> 0x03c5, JSONException -> 0x03dd }
            java.lang.String r8 = "location"
            java.lang.String r8 = r9.getString(r8)     // Catch:{ IOException -> 0x03c5, JSONException -> 0x03dd }
            r7.setLocation(r8)     // Catch:{ IOException -> 0x03c5, JSONException -> 0x03dd }
            java.lang.String r8 = "description"
            java.lang.String r8 = r9.getString(r8)     // Catch:{ IOException -> 0x03c5, JSONException -> 0x03dd }
            r7.setDescription(r8)     // Catch:{ IOException -> 0x03c5, JSONException -> 0x03dd }
            java.lang.String r8 = "gender"
            java.lang.String r8 = r9.getString(r8)     // Catch:{ IOException -> 0x03c5, JSONException -> 0x03dd }
            r7.setGender(r8)     // Catch:{ IOException -> 0x03c5, JSONException -> 0x03dd }
            java.lang.String r8 = "friends_count"
            java.lang.String r8 = r9.getString(r8)     // Catch:{ IOException -> 0x03c5, JSONException -> 0x03dd }
            r7.setFriendsCount(r8)     // Catch:{ IOException -> 0x03c5, JSONException -> 0x03dd }
            java.lang.String r8 = "statuses_count"
            java.lang.String r8 = r9.getString(r8)     // Catch:{ IOException -> 0x03c5, JSONException -> 0x03dd }
            r7.setStatusesCount(r8)     // Catch:{ IOException -> 0x03c5, JSONException -> 0x03dd }
            java.lang.String r8 = "followers_count"
            java.lang.String r8 = r9.getString(r8)     // Catch:{ IOException -> 0x03c5, JSONException -> 0x03dd }
            r7.setFollowersCount(r8)     // Catch:{ IOException -> 0x03c5, JSONException -> 0x03dd }
            java.lang.String r8 = "favourites_count"
            java.lang.String r8 = r9.getString(r8)     // Catch:{ IOException -> 0x03c5, JSONException -> 0x03dd }
            r7.setFavouritesCount(r8)     // Catch:{ IOException -> 0x03c5, JSONException -> 0x03dd }
            goto L_0x00d4
        L_0x03c5:
            r8 = move-exception
            r8.printStackTrace()
            goto L_0x00d4
        L_0x03cb:
            r9 = move-exception
            r9.printStackTrace()
            goto L_0x0322
        L_0x03d1:
            r9 = move-exception
            r9.printStackTrace()
            goto L_0x0322
        L_0x03d7:
            r5 = 0
            r8.append(r4, r5, r1)     // Catch:{ IOException -> 0x03c5, JSONException -> 0x03dd }
            goto L_0x034e
        L_0x03dd:
            r8 = move-exception
            r8.printStackTrace()
            goto L_0x00d4
        L_0x03e3:
            r9 = r1
            goto L_0x02ff
        L_0x03e6:
            r9 = r1
            goto L_0x01fe
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.com.mzba.service.UserHttpUtils.getUserById(java.lang.String, java.lang.String):cn.com.mzba.model.User");
    }

    /* JADX INFO: Multiple debug info for r0v1 java.io.InputStream: [D('is' java.io.InputStream), D('params' java.util.List<org.apache.http.message.BasicNameValuePair>)] */
    /* JADX INFO: Multiple debug info for r7v12 java.lang.String: [D('buf' char[]), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r0v2 int: [D('is' java.io.InputStream), D('i' int)] */
    /* JADX INFO: Multiple debug info for r7v16 org.json.JSONObject: [D('e' java.lang.Exception), D('data' org.json.JSONObject)] */
    public static List<User> getBlocking(int page, int count) {
        List<User> users = new ArrayList<>();
        List<BasicNameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("page", String.valueOf(page)));
        params.add(new BasicNameValuePair("count", String.valueOf(count)));
        HttpResponse response = oauthSina.signRequest(userTokenSina, userTokenSinaSecret, "http://api.t.sina.com.cn/blocks/blocking.json", params);
        if (response != null && 200 == response.getStatusLine().getStatusCode()) {
            try {
                InputStream is = response.getEntity().getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                StringBuilder builder = new StringBuilder((int) response.getEntity().getContentLength());
                char[] buf = new char[4000];
                while (true) {
                    int len = reader.read(buf);
                    if (len == -1) {
                        break;
                    }
                    builder.append(buf, 0, len);
                }
                reader.close();
                is.close();
                String content = builder.toString();
                response.getEntity().consumeContent();
                JSONArray datas = new JSONArray(content);
                if (datas != null) {
                    int i = 0;
                    while (true) {
                        int i2 = i;
                        if (i2 >= datas.length()) {
                            break;
                        }
                        JSONObject data = datas.getJSONObject(i2);
                        User user = new User();
                        String userId = data.getString("id");
                        user.setId(userId);
                        user.setScreenName(data.getString("screen_name"));
                        user.setProfileImageUrl(data.getString("profile_image_url"));
                        user.setFollowing(new Boolean(data.getString("following")).booleanValue());
                        user.setBlack(existsBlocks(userId));
                        users.add(user);
                        i = i2 + 1;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return users;
    }

    public static String createBlocks(String userId) {
        List<BasicNameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("user_id", userId));
        HttpResponse response = oauthSina.signRequest(userTokenSina, userTokenSinaSecret, "http://api.t.sina.com.cn/blocks/create.json", params);
        if (response == null || 200 != response.getStatusLine().getStatusCode()) {
            return SystemConfig.ERROR;
        }
        return SystemConfig.SUCCESS;
    }

    public static String destroyBlocks(String userId) {
        List<BasicNameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("user_id", userId));
        HttpResponse response = oauthSina.signRequest(userTokenSina, userTokenSinaSecret, "http://api.t.sina.com.cn/blocks/destroy.json", params);
        if (response == null || 200 != response.getStatusLine().getStatusCode()) {
            return SystemConfig.ERROR;
        }
        return SystemConfig.SUCCESS;
    }

    public static boolean existsBlocks(String userId) {
        List<BasicNameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("user_id", userId));
        HttpResponse response = oauthSina.signRequest(userTokenSina, userTokenSinaSecret, "http://api.t.sina.com.cn/blocks/exists.json", params);
        if (response == null || 200 != response.getStatusLine().getStatusCode()) {
            return false;
        }
        return true;
    }

    /* JADX INFO: Multiple debug info for r9v6 java.net.HttpURLConnection: [D('uri' java.net.URL), D('conn' java.net.HttpURLConnection)] */
    /* JADX INFO: Multiple debug info for r7v14 java.lang.String: [D('buf' char[]), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r8v16 org.json.JSONArray: [D('datas' org.json.JSONArray), D('builder' java.lang.StringBuilder)] */
    /* JADX INFO: Multiple debug info for r7v20 int: [D('status' cn.com.mzba.model.Status), D('i' int)] */
    /* JADX INFO: Multiple debug info for r9v10 java.net.HttpURLConnection: [D('uri' java.net.URL), D('conn' java.net.HttpURLConnection)] */
    /* JADX INFO: Multiple debug info for r7v33 java.lang.String: [D('buf' char[]), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r8v34 org.json.JSONArray: [D('datas' org.json.JSONArray), D('builder' java.lang.StringBuilder)] */
    /* JADX INFO: Multiple debug info for r7v39 int: [D('status' cn.com.mzba.model.Status), D('i' int)] */
    /* JADX INFO: Multiple debug info for r7v42 java.lang.String: [D('parameter' java.lang.String), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r7v44 org.json.JSONObject: [D('data' org.json.JSONObject), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r8v42 org.json.JSONObject: [D('info' org.json.JSONObject), D('tweet' org.json.JSONObject)] */
    /* JADX INFO: Multiple debug info for r7v58 java.util.ArrayList: [D('weiboId' java.lang.String), D('params' java.util.List<org.apache.http.message.BasicNameValuePair>)] */
    /* JADX INFO: Multiple debug info for r7v67 java.lang.String: [D('buf' char[]), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r9v26 int: [D('is' java.io.InputStream), D('i' int)] */
    public static List<User> getUserFriends(String weiboId, String userId, int page, int count, int cursor) {
        String url;
        List<User> users = new ArrayList<>();
        if (weiboId.equals(SystemConfig.SINA_WEIBO)) {
            List<BasicNameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("user_id", userId));
            params.add(new BasicNameValuePair("cursor", String.valueOf(page)));
            params.add(new BasicNameValuePair("count", String.valueOf(count)));
            HttpResponse response = oauthSina.signRequest(userTokenSina, userTokenSinaSecret, "http://api.t.sina.com.cn/statuses/friends.json", params);
            if (response != null && 200 == response.getStatusLine().getStatusCode()) {
                try {
                    InputStream is = response.getEntity().getContent();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                    StringBuilder builder = new StringBuilder((int) response.getEntity().getContentLength());
                    char[] buf = new char[4000];
                    while (true) {
                        int len = reader.read(buf);
                        if (len == -1) {
                            break;
                        }
                        builder.append(buf, 0, len);
                    }
                    reader.close();
                    is.close();
                    String content = builder.toString();
                    response.getEntity().consumeContent();
                    JSONArray datas = new JSONArray(content);
                    int i = 0;
                    while (true) {
                        int i2 = i;
                        if (i2 >= datas.length()) {
                            break;
                        }
                        User user = new User();
                        JSONObject data = datas.getJSONObject(i2);
                        JSONObject statusObject = data.getJSONObject(SqliteHelper.TABLE_STATUS);
                        user.setId(data.getString("id"));
                        user.setScreenName(data.getString("screen_name"));
                        user.setProfileImageUrl(data.getString("profile_image_url"));
                        user.setFollowing(new Boolean(data.getString("following")).booleanValue());
                        Status status = new Status();
                        status.setText(statusObject.getString("text"));
                        user.setStatus(status);
                        users.add(user);
                        i = i2 + 1;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (weiboId.equals(SystemConfig.TENCENT_WEIBO)) {
            List<Parameter> params2 = new ArrayList<>();
            if (userId == null || ConfigHelper.USERINFO_TENCENT.getUserId().equals(userId)) {
                url = "http://open.t.qq.com/api/friends/idollist";
                params2.add(new Parameter("fotmat", "json"));
                params2.add(new Parameter("reqnum", String.valueOf(count)));
                params2.add(new Parameter("startindex", "0"));
            } else {
                url = "http://open.t.qq.com/api/friends/user_idollist";
                params2.add(new Parameter("name", userId));
                params2.add(new Parameter("fotmat", "json"));
                params2.add(new Parameter("reqnum", String.valueOf(count)));
                params2.add(new Parameter("startindex", "0"));
            }
            try {
                String content2 = oauthTencent.httpGet(url, oauthTencent.getPostParameter(SystemConfig.HTTP_GET, url, userTokenTencent, userTokenTencentSecret, params2));
                if (content2 != null) {
                    Log.i(UserHttpUtils.class.getCanonicalName(), "userFriends:" + content2);
                    JSONArray infos = new JSONObject(content2).getJSONObject("data").getJSONArray("info");
                    for (int i3 = 0; i3 < infos.length(); i3++) {
                        JSONObject info = infos.getJSONObject(i3);
                        if (info != null) {
                            User user2 = new User();
                            user2.setId(info.getString("name"));
                            user2.setScreenName(info.getString("nick"));
                            user2.setProfileImageUrl(String.valueOf(info.getString("head")) + "/40");
                            Status status2 = new Status();
                            JSONObject tweet = info.getJSONObject("tweet");
                            status2.setText(tweet.getString("tweet"));
                            status2.setId(tweet.getString("id"));
                            user2.setStatus(status2);
                            users.add(user2);
                        }
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        } else if (weiboId.equals(SystemConfig.SOHU_WEIBO)) {
            try {
                HttpURLConnection conn = (HttpURLConnection) new URL(String.valueOf("http://api.t.sohu.com/statuses/friends.json") + ("?user_id=" + userId + "&page=" + page + "&count=" + count)).openConnection();
                try {
                    conn.setRequestMethod(SystemConfig.HTTP_GET);
                    HttpURLConnection response2 = oauthSohu.signRequest(userTokenSohu, userTokenSohuSecret, conn);
                    if (response2 != null) {
                        try {
                            if (200 == response2.getResponseCode()) {
                                InputStream is2 = response2.getInputStream();
                                BufferedReader reader2 = new BufferedReader(new InputStreamReader(is2));
                                StringBuilder builder2 = new StringBuilder();
                                char[] buf2 = new char[4000];
                                while (true) {
                                    int len2 = reader2.read(buf2);
                                    if (len2 == -1) {
                                        break;
                                    }
                                    builder2.append(buf2, 0, len2);
                                }
                                reader2.close();
                                is2.close();
                                String content3 = builder2.toString();
                                response2.disconnect();
                                JSONArray datas2 = new JSONArray(content3);
                                for (int i4 = 0; i4 < datas2.length(); i4++) {
                                    User user3 = new User();
                                    JSONObject data2 = datas2.getJSONObject(i4);
                                    JSONObject statusObject2 = data2.getJSONObject(SqliteHelper.TABLE_STATUS);
                                    user3.setId(data2.getString("id"));
                                    user3.setScreenName(data2.getString("screen_name"));
                                    user3.setFollowing(new Boolean(data2.getString("following")).booleanValue());
                                    user3.setProfileImageUrl(data2.getString("profile_image_url"));
                                    Status status3 = new Status();
                                    status3.setText(statusObject2.getString("text"));
                                    user3.setStatus(status3);
                                    users.add(user3);
                                }
                            }
                        } catch (Exception e3) {
                            e3.printStackTrace();
                        }
                    }
                } catch (MalformedURLException e4) {
                    e1 = e4;
                } catch (IOException e5) {
                    e = e5;
                    e.printStackTrace();
                    return users;
                }
            } catch (MalformedURLException e6) {
                e1 = e6;
                e1.printStackTrace();
                return users;
            } catch (IOException e7) {
                e = e7;
                e.printStackTrace();
                return users;
            }
        } else if (weiboId.equals(SystemConfig.NETEASE_WEIBO)) {
            try {
                HttpURLConnection conn2 = (HttpURLConnection) new URL(String.valueOf("http://api.t.163.com/statuses/friends.json") + ("?user_id=" + userId + "&cursor=" + cursor)).openConnection();
                try {
                    conn2.setRequestMethod(SystemConfig.HTTP_GET);
                    HttpURLConnection response3 = oauthNetease.signRequest(userTokenNetease, userTokenNeteaseSecret, conn2);
                    if (response3 != null) {
                        try {
                            if (200 == response3.getResponseCode()) {
                                InputStream is3 = response3.getInputStream();
                                BufferedReader reader3 = new BufferedReader(new InputStreamReader(is3));
                                StringBuilder builder3 = new StringBuilder();
                                char[] buf3 = new char[4000];
                                while (true) {
                                    int len3 = reader3.read(buf3);
                                    if (len3 == -1) {
                                        break;
                                    }
                                    builder3.append(buf3, 0, len3);
                                }
                                reader3.close();
                                is3.close();
                                String content4 = builder3.toString();
                                response3.disconnect();
                                JSONArray datas3 = new JSONArray(content4);
                                for (int i5 = 0; i5 < datas3.length(); i5++) {
                                    User user4 = new User();
                                    JSONObject data3 = datas3.getJSONObject(i5);
                                    JSONObject statusObject3 = data3.getJSONObject(SqliteHelper.TABLE_STATUS);
                                    user4.setId(data3.getString("id"));
                                    user4.setScreenName(data3.getString("screen_name"));
                                    user4.setFollowing(new Boolean(data3.getString("following")).booleanValue());
                                    user4.setProfileImageUrl(data3.getString("profile_image_url"));
                                    Status status4 = new Status();
                                    status4.setText(statusObject3.getString("text"));
                                    user4.setStatus(status4);
                                    users.add(user4);
                                }
                            }
                        } catch (Exception e8) {
                            e8.printStackTrace();
                        }
                    }
                } catch (MalformedURLException e9) {
                    e1 = e9;
                } catch (IOException e10) {
                    e = e10;
                    e.printStackTrace();
                    return users;
                }
            } catch (MalformedURLException e11) {
                e1 = e11;
                e1.printStackTrace();
                return users;
            } catch (IOException e12) {
                e = e12;
                e.printStackTrace();
                return users;
            }
        }
        return users;
    }

    /* JADX INFO: Multiple debug info for r9v6 java.net.HttpURLConnection: [D('uri' java.net.URL), D('conn' java.net.HttpURLConnection)] */
    /* JADX INFO: Multiple debug info for r7v14 java.lang.String: [D('buf' char[]), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r8v16 org.json.JSONArray: [D('datas' org.json.JSONArray), D('builder' java.lang.StringBuilder)] */
    /* JADX INFO: Multiple debug info for r7v20 int: [D('status' cn.com.mzba.model.Status), D('i' int)] */
    /* JADX INFO: Multiple debug info for r9v10 java.net.HttpURLConnection: [D('uri' java.net.URL), D('conn' java.net.HttpURLConnection)] */
    /* JADX INFO: Multiple debug info for r7v33 java.lang.String: [D('buf' char[]), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r8v34 org.json.JSONArray: [D('datas' org.json.JSONArray), D('builder' java.lang.StringBuilder)] */
    /* JADX INFO: Multiple debug info for r7v39 int: [D('status' cn.com.mzba.model.Status), D('i' int)] */
    /* JADX INFO: Multiple debug info for r7v42 java.lang.String: [D('parameter' java.lang.String), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r7v44 org.json.JSONObject: [D('data' org.json.JSONObject), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r8v42 org.json.JSONObject: [D('info' org.json.JSONObject), D('tweet' org.json.JSONObject)] */
    /* JADX INFO: Multiple debug info for r7v58 java.util.ArrayList: [D('weiboId' java.lang.String), D('params' java.util.List<org.apache.http.message.BasicNameValuePair>)] */
    /* JADX INFO: Multiple debug info for r7v67 java.lang.String: [D('buf' char[]), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r9v26 int: [D('is' java.io.InputStream), D('i' int)] */
    public static List<User> getUserFollowers(String weiboId, String userId, int page, int count, int cursor) {
        String url;
        List<User> users = new ArrayList<>();
        if (weiboId.equals(SystemConfig.SINA_WEIBO)) {
            List<BasicNameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("user_id", userId));
            params.add(new BasicNameValuePair("cursor", String.valueOf(page)));
            params.add(new BasicNameValuePair("count", String.valueOf(count)));
            HttpResponse response = oauthSina.signRequest(userTokenSina, userTokenSinaSecret, "http://api.t.sina.com.cn/statuses/followers.json", params);
            if (response != null && 200 == response.getStatusLine().getStatusCode()) {
                try {
                    InputStream is = response.getEntity().getContent();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                    StringBuilder builder = new StringBuilder((int) response.getEntity().getContentLength());
                    char[] buf = new char[4000];
                    while (true) {
                        int len = reader.read(buf);
                        if (len == -1) {
                            break;
                        }
                        builder.append(buf, 0, len);
                    }
                    reader.close();
                    is.close();
                    String content = builder.toString();
                    response.getEntity().consumeContent();
                    JSONArray datas = new JSONArray(content);
                    int i = 0;
                    while (true) {
                        int i2 = i;
                        if (i2 >= datas.length()) {
                            break;
                        }
                        User user = new User();
                        JSONObject data = datas.getJSONObject(i2);
                        JSONObject statusObject = data.getJSONObject(SqliteHelper.TABLE_STATUS);
                        user.setId(data.getString("id"));
                        user.setScreenName(data.getString("screen_name"));
                        user.setFollowing(new Boolean(data.getString("following")).booleanValue());
                        user.setProfileImageUrl(data.getString("profile_image_url"));
                        Status status = new Status();
                        status.setText(statusObject.getString("text"));
                        user.setStatus(status);
                        users.add(user);
                        i = i2 + 1;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (weiboId.equals(SystemConfig.TENCENT_WEIBO)) {
            List<Parameter> params2 = new ArrayList<>();
            if (userId == null || ConfigHelper.USERINFO_TENCENT.getUserId().equals(userId)) {
                url = "http://open.t.qq.com/api/friends/fanslist";
                params2.add(new Parameter("fotmat", "json"));
                params2.add(new Parameter("reqnum", String.valueOf(count)));
                params2.add(new Parameter("startindex", "0"));
            } else {
                url = "http://open.t.qq.com/api/friends/user_fanslist";
                params2.add(new Parameter("name", userId));
                params2.add(new Parameter("fotmat", "json"));
                params2.add(new Parameter("reqnum", String.valueOf(count)));
                params2.add(new Parameter("startindex", "0"));
            }
            try {
                String content2 = oauthTencent.httpGet(url, oauthTencent.getPostParameter(SystemConfig.HTTP_GET, url, userTokenTencent, userTokenTencentSecret, params2));
                if (content2 != null) {
                    Log.i(UserHttpUtils.class.getCanonicalName(), "userFollowers:" + content2);
                    JSONArray infos = new JSONObject(content2).getJSONObject("data").getJSONArray("info");
                    for (int i3 = 0; i3 < infos.length(); i3++) {
                        JSONObject info = infos.getJSONObject(i3);
                        if (info != null) {
                            User user2 = new User();
                            user2.setId(info.getString("name"));
                            user2.setScreenName(info.getString("nick"));
                            user2.setProfileImageUrl(String.valueOf(info.getString("head")) + "/40");
                            Status status2 = new Status();
                            JSONObject tweet = info.getJSONObject("tweet");
                            status2.setText(tweet.getString("tweet"));
                            status2.setId(tweet.getString("id"));
                            user2.setStatus(status2);
                            users.add(user2);
                        }
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        } else if (weiboId.equals(SystemConfig.SOHU_WEIBO)) {
            try {
                HttpURLConnection conn = (HttpURLConnection) new URL(String.valueOf("http://api.t.sohu.com/statuses/followers.json") + ("?user_id=" + userId + "&page=" + page + "&count=" + count)).openConnection();
                try {
                    conn.setRequestMethod(SystemConfig.HTTP_GET);
                    HttpURLConnection response2 = oauthSohu.signRequest(userTokenSohu, userTokenSohuSecret, conn);
                    if (response2 != null) {
                        try {
                            if (200 == response2.getResponseCode()) {
                                InputStream is2 = response2.getInputStream();
                                BufferedReader reader2 = new BufferedReader(new InputStreamReader(is2));
                                StringBuilder builder2 = new StringBuilder();
                                char[] buf2 = new char[4000];
                                while (true) {
                                    int len2 = reader2.read(buf2);
                                    if (len2 == -1) {
                                        break;
                                    }
                                    builder2.append(buf2, 0, len2);
                                }
                                reader2.close();
                                is2.close();
                                String content3 = builder2.toString();
                                response2.disconnect();
                                JSONArray datas2 = new JSONArray(content3);
                                for (int i4 = 0; i4 < datas2.length(); i4++) {
                                    User user3 = new User();
                                    JSONObject data2 = datas2.getJSONObject(i4);
                                    JSONObject statusObject2 = data2.getJSONObject(SqliteHelper.TABLE_STATUS);
                                    user3.setId(data2.getString("id"));
                                    user3.setScreenName(data2.getString("screen_name"));
                                    user3.setFollowing(new Boolean(data2.getString("following")).booleanValue());
                                    user3.setProfileImageUrl(data2.getString("profile_image_url"));
                                    Status status3 = new Status();
                                    status3.setText(statusObject2.getString("text"));
                                    user3.setStatus(status3);
                                    users.add(user3);
                                }
                            }
                        } catch (Exception e3) {
                            e3.printStackTrace();
                        }
                    }
                } catch (MalformedURLException e4) {
                    e1 = e4;
                } catch (IOException e5) {
                    e = e5;
                    e.printStackTrace();
                    return users;
                }
            } catch (MalformedURLException e6) {
                e1 = e6;
                e1.printStackTrace();
                return users;
            } catch (IOException e7) {
                e = e7;
                e.printStackTrace();
                return users;
            }
        } else if (weiboId.equals(SystemConfig.NETEASE_WEIBO)) {
            try {
                HttpURLConnection conn2 = (HttpURLConnection) new URL(String.valueOf("http://api.t.163.com/statuses/followers.json") + ("?user_id=" + userId + "&cursor=" + cursor)).openConnection();
                try {
                    conn2.setRequestMethod(SystemConfig.HTTP_GET);
                    HttpURLConnection response3 = oauthNetease.signRequest(userTokenNetease, userTokenNeteaseSecret, conn2);
                    if (response3 != null) {
                        try {
                            if (200 == response3.getResponseCode()) {
                                InputStream is3 = response3.getInputStream();
                                BufferedReader reader3 = new BufferedReader(new InputStreamReader(is3));
                                StringBuilder builder3 = new StringBuilder();
                                char[] buf3 = new char[4000];
                                while (true) {
                                    int len3 = reader3.read(buf3);
                                    if (len3 == -1) {
                                        break;
                                    }
                                    builder3.append(buf3, 0, len3);
                                }
                                reader3.close();
                                is3.close();
                                String content4 = builder3.toString();
                                response3.disconnect();
                                JSONArray datas3 = new JSONArray(content4);
                                for (int i5 = 0; i5 < datas3.length(); i5++) {
                                    User user4 = new User();
                                    JSONObject data3 = datas3.getJSONObject(i5);
                                    JSONObject statusObject3 = data3.getJSONObject(SqliteHelper.TABLE_STATUS);
                                    user4.setId(data3.getString("id"));
                                    user4.setScreenName(data3.getString("screen_name"));
                                    user4.setFollowing(new Boolean(data3.getString("following")).booleanValue());
                                    user4.setProfileImageUrl(data3.getString("profile_image_url"));
                                    Status status4 = new Status();
                                    status4.setText(statusObject3.getString("text"));
                                    user4.setStatus(status4);
                                    users.add(user4);
                                }
                            }
                        } catch (Exception e8) {
                            e8.printStackTrace();
                        }
                    }
                } catch (MalformedURLException e9) {
                    e1 = e9;
                } catch (IOException e10) {
                    e = e10;
                    e.printStackTrace();
                    return users;
                }
            } catch (MalformedURLException e11) {
                e1 = e11;
                e1.printStackTrace();
                return users;
            } catch (IOException e12) {
                e = e12;
                e.printStackTrace();
                return users;
            }
        }
        return users;
    }

    public static String attendance(String weiboId, String userId) {
        if (weiboId.equals(SystemConfig.SINA_WEIBO)) {
            List<BasicNameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("user_id", userId));
            HttpResponse response = oauthSina.signRequest(userTokenSina, userTokenSinaSecret, "http://api.t.sina.com.cn/friendships/create.json", params);
            if (response == null || 200 != response.getStatusLine().getStatusCode()) {
                return SystemConfig.ERROR;
            }
            return SystemConfig.SUCCESS;
        } else if (weiboId.equals(SystemConfig.TENCENT_WEIBO)) {
            List<Parameter> params2 = new ArrayList<>();
            try {
                params2.add(new Parameter("format", "json"));
                params2.add(new Parameter("Name", userId));
                params2.add(new Parameter("clientip", "127.0.0.1"));
                String parameter = oauthTencent.getPostParameter(SystemConfig.HTTP_POST, "http://open.t.qq.com/api/friends/add", userTokenTencent, userTokenTencentSecret, params2);
                if (new JSONObject(oauthTencent.httpPost(String.valueOf("http://open.t.qq.com/api/friends/add") + "?" + parameter, parameter)).getString("msg").equals("ok")) {
                    return SystemConfig.SUCCESS;
                }
                return SystemConfig.ERROR;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else if (weiboId.equals(SystemConfig.SOHU_WEIBO)) {
            List<BasicNameValuePair> params3 = new ArrayList<>();
            params3.add(new BasicNameValuePair("id", userId));
            HttpResponse response2 = oauthSohu.signRequest(userTokenSohu, userTokenSohuSecret, "http://api.t.sohu.com/friendships/create/" + userId + ".json", params3);
            if (response2 == null || 200 != response2.getStatusLine().getStatusCode()) {
                return SystemConfig.ERROR;
            }
            return SystemConfig.SUCCESS;
        } else if (!weiboId.equals(SystemConfig.NETEASE_WEIBO)) {
            return null;
        } else {
            List<BasicNameValuePair> params4 = new ArrayList<>();
            params4.add(new BasicNameValuePair("user_id", userId));
            HttpResponse response3 = oauthNetease.signRequest(userTokenNetease, userTokenNeteaseSecret, "http://api.t.163.com/friendships/create.json", params4);
            if (response3 == null || 200 != response3.getStatusLine().getStatusCode()) {
                return SystemConfig.ERROR;
            }
            return SystemConfig.SUCCESS;
        }
    }

    public static String cancelAttendance(String weiboId, String userId) {
        if (weiboId.equals(SystemConfig.SINA_WEIBO)) {
            List<BasicNameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("user_id", userId));
            HttpResponse response = oauthSina.signRequest(userTokenSina, userTokenSinaSecret, "http://api.t.sina.com.cn/friendships/destroy.json", params);
            if (response == null || 200 != response.getStatusLine().getStatusCode()) {
                return SystemConfig.ERROR;
            }
            return SystemConfig.SUCCESS;
        } else if (weiboId.equals(SystemConfig.TENCENT_WEIBO)) {
            List<Parameter> params2 = new ArrayList<>();
            try {
                params2.add(new Parameter("format", "json"));
                params2.add(new Parameter("Name", userId));
                String parameter = oauthTencent.getPostParameter(SystemConfig.HTTP_POST, "http://open.t.qq.com/api/friends/del", userTokenTencent, userTokenTencentSecret, params2);
                if (new JSONObject(oauthTencent.httpPost(String.valueOf("http://open.t.qq.com/api/friends/del") + "?" + parameter, parameter)).getString("msg").equals("ok")) {
                    return SystemConfig.SUCCESS;
                }
                return SystemConfig.ERROR;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else if (weiboId.equals(SystemConfig.SOHU_WEIBO)) {
            List<BasicNameValuePair> params3 = new ArrayList<>();
            params3.add(new BasicNameValuePair("id", userId));
            HttpResponse response2 = oauthSohu.signRequest(userTokenSohu, userTokenSohuSecret, "http://api.t.sohu.com/friendships/destroy/" + userId + ".json", params3);
            if (response2 == null || 200 != response2.getStatusLine().getStatusCode()) {
                return SystemConfig.ERROR;
            }
            return SystemConfig.SUCCESS;
        } else if (!weiboId.equals(SystemConfig.NETEASE_WEIBO)) {
            return null;
        } else {
            List<BasicNameValuePair> params4 = new ArrayList<>();
            params4.add(new BasicNameValuePair("user_id", userId));
            HttpResponse response3 = oauthNetease.signRequest(userTokenNetease, userTokenNeteaseSecret, "http://api.t.163.com/friendships/destroy.json", params4);
            if (response3 == null || 200 != response3.getStatusLine().getStatusCode()) {
                return SystemConfig.ERROR;
            }
            return SystemConfig.SUCCESS;
        }
    }
}
