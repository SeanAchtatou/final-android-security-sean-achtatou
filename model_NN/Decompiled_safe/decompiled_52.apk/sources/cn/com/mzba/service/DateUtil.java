package cn.com.mzba.service;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
    public static SimpleDateFormat DATA_FORMAT_SHORT = new SimpleDateFormat("MM-dd HH:mm");

    /* JADX INFO: Multiple debug info for r0v4 long: [D('date' java.util.Date), D('now' long)] */
    /* JADX INFO: Multiple debug info for r8v12 java.lang.String: [D('date' java.util.Date), D('destDate' java.lang.String)] */
    public static String getCreateAt(String srcDate) {
        Date date;
        if (srcDate.length() == 30) {
            date = new Date(srcDate);
        } else {
            date = new Date(Long.parseLong(srcDate));
        }
        Long seconds = Long.valueOf((System.currentTimeMillis() - date.getTime()) / 1000);
        long hours = seconds.longValue() / 3600;
        long minutes = seconds.longValue() / 60;
        if (seconds.longValue() / 86400 > 0) {
            return DATA_FORMAT_SHORT.format(date);
        }
        if (hours > 0) {
            return String.valueOf(hours) + "小时前";
        }
        if (minutes > 0) {
            return String.valueOf(minutes) + "分钟前";
        }
        return "刚刚";
    }
}
