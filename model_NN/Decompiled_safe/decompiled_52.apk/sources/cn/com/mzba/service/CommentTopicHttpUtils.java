package cn.com.mzba.service;

import android.util.Log;
import cn.com.mzba.db.SqliteHelper;
import cn.com.mzba.model.Status;
import cn.com.mzba.model.StatusCount;
import cn.com.mzba.model.Topic;
import cn.com.mzba.model.User;
import cn.com.mzba.oauth.OAuth;
import cn.com.mzba.oauth.Parameter;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

public class CommentTopicHttpUtils {
    public static final String TAG = CommentTopicHttpUtils.class.getCanonicalName();
    public static OAuth oauthNetease = ServiceUtils.getNeteaseOauth();
    public static OAuth oauthSina = ServiceUtils.getSinaOauth();
    public static OAuth oauthSohu = ServiceUtils.getSohuOauth();
    public static OAuth oauthTencent = ServiceUtils.getTencentOauth();
    public static String userTokenNetease = ServiceUtils.userTokenNetease;
    public static String userTokenNeteaseSecret = ServiceUtils.userTokenNeteaseSecret;
    public static String userTokenSina = ServiceUtils.userTokenSina;
    public static String userTokenSinaSecret = ServiceUtils.userTokenSinaSecret;
    public static String userTokenSohu = ServiceUtils.userTokenSohu;
    public static String userTokenSohuSecret = ServiceUtils.userTokenSohuSecret;
    public static String userTokenTencent = ServiceUtils.userTokenTencent;
    public static String userTokenTencentSecret = ServiceUtils.userTokenTencentSecret;

    /* JADX INFO: Multiple debug info for r6v12 java.lang.String: [D('buf' char[]), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r8v4 int: [D('is' java.io.InputStream), D('i' int)] */
    /* JADX INFO: Multiple debug info for r6v16 org.json.JSONObject: [D('e' java.lang.Exception), D('data' org.json.JSONObject)] */
    public static List<Topic> getTopicByUserId(String userId, int page, int count) {
        List<Topic> topics = new ArrayList<>();
        List<BasicNameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("user_id", userId));
        params.add(new BasicNameValuePair("page", String.valueOf(page)));
        params.add(new BasicNameValuePair("count", String.valueOf(count)));
        HttpResponse response = oauthSina.signRequest(userTokenSina, userTokenSinaSecret, "http://api.t.sina.com.cn/trends.json", params);
        if (response != null && 200 == response.getStatusLine().getStatusCode()) {
            try {
                InputStream is = response.getEntity().getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                StringBuilder builder = new StringBuilder((int) response.getEntity().getContentLength());
                char[] buf = new char[4000];
                while (true) {
                    int len = reader.read(buf);
                    if (len == -1) {
                        break;
                    }
                    builder.append(buf, 0, len);
                }
                reader.close();
                is.close();
                String content = builder.toString();
                response.getEntity().consumeContent();
                JSONArray datas = new JSONArray(content);
                if (datas != null) {
                    int i = 0;
                    while (true) {
                        int i2 = i;
                        if (i2 >= datas.length()) {
                            break;
                        }
                        JSONObject data = datas.getJSONObject(i2);
                        Topic topic = new Topic();
                        topic.setId(data.getString("trend_id"));
                        topic.setHotword(data.getString("hotword"));
                        topic.setNumber(data.getString("num"));
                        topics.add(topic);
                        i = i2 + 1;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return topics;
    }

    /* JADX INFO: Multiple debug info for r22v7 java.lang.String: [D('builder' java.lang.StringBuilder), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r8v3 java.lang.String[]: [D('response' org.apache.http.HttpResponse), D('idArray' java.lang.String[])] */
    /* JADX INFO: Multiple debug info for r22v15 int: [D('rtStatus' cn.com.mzba.model.Status), D('i' int)] */
    /* JADX INFO: Multiple debug info for r22v21 java.lang.String: [D('data' org.json.JSONObject), D('thumbnail_pic' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r10v9 'rtStatus'  cn.com.mzba.model.Status: [D('rtStatus' cn.com.mzba.model.Status), D('transmitText' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r11v13 java.lang.String: [D('user_retweeted' org.json.JSONObject), D('transmitUserName' java.lang.String)] */
    public static List<Status> getStaticByTopicName(String topicName) {
        Status rtStatus;
        List<Status> list = new ArrayList<>();
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("trend_name", topicName));
        HttpResponse response = oauthSina.signRequest(userTokenSina, userTokenSinaSecret, "http://api.t.sina.com.cn/trends/statuses.json", arrayList);
        if (response != null && 200 == response.getStatusLine().getStatusCode()) {
            try {
                InputStream is = response.getEntity().getContent();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));
                StringBuilder sb = new StringBuilder((int) response.getEntity().getContentLength());
                char[] temp = new char[4000];
                while (true) {
                    int len = bufferedReader.read(temp);
                    if (len == -1) {
                        break;
                    }
                    sb.append(temp, 0, len);
                }
                bufferedReader.close();
                is.close();
                String content = sb.toString();
                response.getEntity().consumeContent();
                JSONArray jSONArray = new JSONArray(content);
                String[] idArray = new String[jSONArray.length()];
                for (int i = 0; i < jSONArray.length(); i++) {
                    Status status = new Status();
                    User userStatus = new User();
                    JSONObject data = jSONArray.getJSONObject(i);
                    if (data != null) {
                        JSONObject user = data.getJSONObject(SqliteHelper.TABLE_USER);
                        if (data.has("retweeted_status")) {
                            rtStatus = new Status();
                            status.setRetweetedStatus(true);
                            JSONObject retweeted = data.getJSONObject("retweeted_status");
                            String transmitText = "@" + retweeted.getJSONObject(SqliteHelper.TABLE_USER).getString("screen_name") + ":" + retweeted.getString("text");
                            String thumbnail_pic = "";
                            if (retweeted.has("thumbnail_pic")) {
                                thumbnail_pic = retweeted.getString("thumbnail_pic");
                            }
                            rtStatus.setId(retweeted.getString("id"));
                            rtStatus.setText(transmitText);
                            rtStatus.setThumbnailPic(thumbnail_pic);
                            status.setStatus(rtStatus);
                            String str = transmitText;
                        } else {
                            rtStatus = null;
                        }
                        String id = data.getString("id");
                        idArray[i] = id;
                        String userId = user.getString("id");
                        String userName = user.getString("screen_name");
                        String userIcon = user.getString("profile_image_url");
                        String time = data.getString("created_at");
                        String text = data.getString("text");
                        String source = data.getString("source");
                        if (data.has("thumbnail_pic")) {
                            status.setThumbnailPic(data.getString("thumbnail_pic"));
                        }
                        status.setId(id);
                        status.setText(text);
                        status.setCreatedAt(time);
                        status.setSource("来自：" + source);
                        userStatus.setId(userId);
                        userStatus.setProfileImageUrl(userIcon);
                        userStatus.setScreenName(userName);
                        status.setUser(userStatus);
                        list.add(status);
                        Status status2 = rtStatus;
                    }
                }
                List<StatusCount> counts = StatusHttpUtils.getCounts("", StringUtil.arrayToString(idArray));
                for (Status status3 : list) {
                    for (StatusCount sc : counts) {
                        if (status3.getId().equals(sc.getId())) {
                            status3.setComments(sc.getComments());
                            status3.setRt(sc.getRt());
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    public static String followTopic(String topicName) {
        List<BasicNameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("trend_name", topicName));
        HttpResponse response = oauthSina.signRequest(userTokenSina, userTokenSinaSecret, "http://api.t.sina.com.cn/trends/follow.json", params);
        if (response == null || 200 != response.getStatusLine().getStatusCode()) {
            return SystemConfig.ERROR;
        }
        return SystemConfig.SUCCESS;
    }

    public static String destroyTopic(String topicId) {
        List<BasicNameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("trend_id", topicId));
        HttpResponse response = oauthSina.signRequest(userTokenSina, userTokenSinaSecret, "http://api.t.sina.com.cn/trends/destroy.json", params);
        if (response == null || 200 != response.getStatusLine().getStatusCode()) {
            return SystemConfig.ERROR;
        }
        return SystemConfig.SUCCESS;
    }

    /* JADX INFO: Multiple debug info for r12v1 java.net.HttpURLConnection: [D('response' java.net.HttpURLConnection), D('sinceId' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r8v14 java.lang.String: [D('builder' java.lang.StringBuilder), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r11v3 org.json.JSONArray: [D('reader' java.io.Reader), D('datas' org.json.JSONArray)] */
    /* JADX INFO: Multiple debug info for r12v2 int: [D('response' java.net.HttpURLConnection), D('i' int)] */
    /* JADX INFO: Multiple debug info for r9v17 org.json.JSONObject: [D('commentid' java.lang.String), D('userObject' org.json.JSONObject)] */
    /* JADX INFO: Multiple debug info for r12v3 java.net.HttpURLConnection: [D('response' java.net.HttpURLConnection), D('sinceId' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r8v33 java.lang.String: [D('builder' java.lang.StringBuilder), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r11v8 org.json.JSONArray: [D('reader' java.io.Reader), D('datas' org.json.JSONArray)] */
    /* JADX INFO: Multiple debug info for r12v4 int: [D('response' java.net.HttpURLConnection), D('i' int)] */
    /* JADX INFO: Multiple debug info for r9v41 org.json.JSONObject: [D('commentid' java.lang.String), D('userObject' org.json.JSONObject)] */
    /* JADX INFO: Multiple debug info for r8v47 java.lang.String: [D('parameter' java.lang.String), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r8v49 org.json.JSONObject: [D('content' java.lang.String), D('object' org.json.JSONObject)] */
    /* JADX INFO: Multiple debug info for r8v55 java.util.ArrayList: [D('weiboId' java.lang.String), D('params' java.util.List<org.apache.http.message.BasicNameValuePair>)] */
    /* JADX INFO: Multiple debug info for r8v64 java.lang.String: [D('buf' char[]), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r11v20 org.json.JSONArray: [D('datas' org.json.JSONArray), D('len' int)] */
    /* JADX INFO: Multiple debug info for r12v14 int: [D('reader' java.io.BufferedReader), D('i' int)] */
    /* JADX INFO: Multiple debug info for r9v77 org.json.JSONObject: [D('commentid' java.lang.String), D('userObject' org.json.JSONObject)] */
    /* JADX WARN: Type inference failed for: r9v25, types: [java.net.URLConnection] */
    /* JADX WARN: Type inference failed for: r9v49, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List<cn.com.mzba.model.Comment> getCommentListById(java.lang.String r8, java.lang.String r9, int r10, int r11, java.lang.String r12) {
        /*
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>()
            java.lang.String r1 = "sina_weibo"
            boolean r1 = r8.equals(r1)
            if (r1 == 0) goto L_0x00f2
            java.lang.String r3 = "http://api.t.sina.com.cn/statuses/comments.json"
            java.util.ArrayList r8 = new java.util.ArrayList
            r8.<init>()
            org.apache.http.message.BasicNameValuePair r12 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r1 = "id"
            r12.<init>(r1, r9)
            r8.add(r12)
            org.apache.http.message.BasicNameValuePair r9 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r12 = "page"
            java.lang.String r10 = java.lang.String.valueOf(r10)
            r9.<init>(r12, r10)
            r8.add(r9)
            org.apache.http.message.BasicNameValuePair r9 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r10 = "count"
            java.lang.String r11 = java.lang.String.valueOf(r11)
            r9.<init>(r10, r11)
            r8.add(r9)
            cn.com.mzba.oauth.OAuth r9 = cn.com.mzba.service.CommentTopicHttpUtils.oauthSina
            java.lang.String r10 = cn.com.mzba.service.CommentTopicHttpUtils.userTokenSina
            java.lang.String r11 = cn.com.mzba.service.CommentTopicHttpUtils.userTokenSinaSecret
            org.apache.http.HttpResponse r1 = r9.signRequest(r10, r11, r3, r8)
            if (r1 == 0) goto L_0x009c
            r8 = 200(0xc8, float:2.8E-43)
            org.apache.http.StatusLine r9 = r1.getStatusLine()
            int r9 = r9.getStatusCode()
            if (r8 != r9) goto L_0x009c
            org.apache.http.HttpEntity r8 = r1.getEntity()     // Catch:{ Exception -> 0x00a2 }
            java.io.InputStream r10 = r8.getContent()     // Catch:{ Exception -> 0x00a2 }
            java.io.BufferedReader r12 = new java.io.BufferedReader     // Catch:{ Exception -> 0x00a2 }
            java.io.InputStreamReader r8 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x00a2 }
            r8.<init>(r10)     // Catch:{ Exception -> 0x00a2 }
            r12.<init>(r8)     // Catch:{ Exception -> 0x00a2 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00a2 }
            org.apache.http.HttpEntity r8 = r1.getEntity()     // Catch:{ Exception -> 0x00a2 }
            long r2 = r8.getContentLength()     // Catch:{ Exception -> 0x00a2 }
            int r8 = (int) r2     // Catch:{ Exception -> 0x00a2 }
            r9.<init>(r8)     // Catch:{ Exception -> 0x00a2 }
            r8 = 4000(0xfa0, float:5.605E-42)
            char[] r8 = new char[r8]     // Catch:{ Exception -> 0x00a2 }
            r11 = 0
        L_0x0077:
            int r11 = r12.read(r8)     // Catch:{ Exception -> 0x00a2 }
            r2 = -1
            if (r11 != r2) goto L_0x009d
            r12.close()     // Catch:{ Exception -> 0x00a2 }
            r10.close()     // Catch:{ Exception -> 0x00a2 }
            java.lang.String r8 = r9.toString()     // Catch:{ Exception -> 0x00a2 }
            org.apache.http.HttpEntity r9 = r1.getEntity()     // Catch:{ Exception -> 0x00a2 }
            r9.consumeContent()     // Catch:{ Exception -> 0x00a2 }
            org.json.JSONArray r11 = new org.json.JSONArray     // Catch:{ Exception -> 0x00a2 }
            r11.<init>(r8)     // Catch:{ Exception -> 0x00a2 }
            r8 = 0
            r12 = r8
        L_0x0096:
            int r8 = r11.length()     // Catch:{ Exception -> 0x00a2 }
            if (r12 < r8) goto L_0x00a7
        L_0x009c:
            return r7
        L_0x009d:
            r2 = 0
            r9.append(r8, r2, r11)     // Catch:{ Exception -> 0x00a2 }
            goto L_0x0077
        L_0x00a2:
            r8 = move-exception
            r8.printStackTrace()
            goto L_0x009c
        L_0x00a7:
            cn.com.mzba.model.Comment r8 = new cn.com.mzba.model.Comment     // Catch:{ Exception -> 0x00a2 }
            r8.<init>()     // Catch:{ Exception -> 0x00a2 }
            cn.com.mzba.model.User r3 = new cn.com.mzba.model.User     // Catch:{ Exception -> 0x00a2 }
            r3.<init>()     // Catch:{ Exception -> 0x00a2 }
            org.json.JSONObject r10 = r11.getJSONObject(r12)     // Catch:{ Exception -> 0x00a2 }
            java.lang.String r9 = "id"
            java.lang.String r9 = r10.getString(r9)     // Catch:{ Exception -> 0x00a2 }
            java.lang.String r1 = "text"
            java.lang.String r1 = r10.getString(r1)     // Catch:{ Exception -> 0x00a2 }
            java.lang.String r2 = "created_at"
            java.lang.String r2 = r10.getString(r2)     // Catch:{ Exception -> 0x00a2 }
            r8.setId(r9)     // Catch:{ Exception -> 0x00a2 }
            r8.setText(r1)     // Catch:{ Exception -> 0x00a2 }
            r8.setCreated_at(r2)     // Catch:{ Exception -> 0x00a2 }
            java.lang.String r9 = "user"
            org.json.JSONObject r9 = r10.getJSONObject(r9)     // Catch:{ Exception -> 0x00a2 }
            java.lang.String r10 = "screen_name"
            java.lang.String r10 = r9.getString(r10)     // Catch:{ Exception -> 0x00a2 }
            r3.setScreenName(r10)     // Catch:{ Exception -> 0x00a2 }
            java.lang.String r10 = "id"
            java.lang.String r9 = r9.getString(r10)     // Catch:{ Exception -> 0x00a2 }
            r3.setId(r9)     // Catch:{ Exception -> 0x00a2 }
            r8.setUser(r3)     // Catch:{ Exception -> 0x00a2 }
            r7.add(r8)     // Catch:{ Exception -> 0x00a2 }
            int r8 = r12 + 1
            r12 = r8
            goto L_0x0096
        L_0x00f2:
            java.lang.String r1 = "tencent_weibo"
            boolean r1 = r8.equals(r1)
            if (r1 == 0) goto L_0x01d1
            java.lang.String r3 = "http://open.t.qq.com/api/t/re_list"
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            cn.com.mzba.oauth.Parameter r8 = new cn.com.mzba.oauth.Parameter
            java.lang.String r11 = "Reqnum"
            r12 = 1
            java.lang.String r12 = java.lang.String.valueOf(r12)
            r8.<init>(r11, r12)
            r6.add(r8)
            cn.com.mzba.oauth.Parameter r8 = new cn.com.mzba.oauth.Parameter
            java.lang.String r11 = "Flag"
            r12 = 1
            java.lang.String r12 = java.lang.String.valueOf(r12)
            r8.<init>(r11, r12)
            r6.add(r8)
            cn.com.mzba.oauth.Parameter r8 = new cn.com.mzba.oauth.Parameter
            java.lang.String r11 = "Pageflag"
            r12 = 1
            int r10 = r10 - r12
            java.lang.String r10 = java.lang.String.valueOf(r10)
            r8.<init>(r11, r10)
            r6.add(r8)
            cn.com.mzba.oauth.Parameter r8 = new cn.com.mzba.oauth.Parameter
            java.lang.String r10 = "rootid"
            r8.<init>(r10, r9)
            r6.add(r8)
            cn.com.mzba.oauth.Parameter r8 = new cn.com.mzba.oauth.Parameter
            java.lang.String r9 = "format"
            java.lang.String r10 = "json"
            r8.<init>(r9, r10)
            r6.add(r8)
            cn.com.mzba.oauth.OAuth r1 = cn.com.mzba.service.CommentTopicHttpUtils.oauthTencent     // Catch:{ Exception -> 0x01cb }
            java.lang.String r2 = "GET"
            java.lang.String r4 = cn.com.mzba.service.CommentTopicHttpUtils.userTokenTencent     // Catch:{ Exception -> 0x01cb }
            java.lang.String r5 = cn.com.mzba.service.CommentTopicHttpUtils.userTokenTencentSecret     // Catch:{ Exception -> 0x01cb }
            java.lang.String r8 = r1.getPostParameter(r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x01cb }
            cn.com.mzba.oauth.OAuth r9 = cn.com.mzba.service.CommentTopicHttpUtils.oauthTencent     // Catch:{ Exception -> 0x01cb }
            java.lang.String r8 = r9.httpGet(r3, r8)     // Catch:{ Exception -> 0x01cb }
            if (r8 == 0) goto L_0x009c
            java.lang.String r9 = cn.com.mzba.service.CommentTopicHttpUtils.TAG     // Catch:{ Exception -> 0x01cb }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01cb }
            java.lang.String r11 = "commentList:"
            r10.<init>(r11)     // Catch:{ Exception -> 0x01cb }
            java.lang.StringBuilder r10 = r10.append(r8)     // Catch:{ Exception -> 0x01cb }
            java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x01cb }
            android.util.Log.i(r9, r10)     // Catch:{ Exception -> 0x01cb }
            org.json.JSONObject r9 = new org.json.JSONObject     // Catch:{ Exception -> 0x01cb }
            r9.<init>(r8)     // Catch:{ Exception -> 0x01cb }
            java.lang.String r8 = "data"
            org.json.JSONObject r8 = r9.getJSONObject(r8)     // Catch:{ Exception -> 0x01cb }
            java.lang.String r9 = "info"
            org.json.JSONArray r12 = r8.getJSONArray(r9)     // Catch:{ Exception -> 0x01cb }
            r8 = 0
            r10 = r8
        L_0x0180:
            int r8 = r12.length()     // Catch:{ Exception -> 0x01cb }
            if (r10 >= r8) goto L_0x009c
            org.json.JSONObject r11 = r12.getJSONObject(r10)     // Catch:{ Exception -> 0x01cb }
            cn.com.mzba.model.Comment r8 = new cn.com.mzba.model.Comment     // Catch:{ Exception -> 0x01cb }
            r8.<init>()     // Catch:{ Exception -> 0x01cb }
            cn.com.mzba.model.User r3 = new cn.com.mzba.model.User     // Catch:{ Exception -> 0x01cb }
            r3.<init>()     // Catch:{ Exception -> 0x01cb }
            java.lang.String r9 = "id"
            java.lang.String r9 = r11.getString(r9)     // Catch:{ Exception -> 0x01cb }
            java.lang.String r1 = "text"
            java.lang.String r1 = r11.getString(r1)     // Catch:{ Exception -> 0x01cb }
            java.lang.String r2 = "timestamp"
            java.lang.String r2 = r11.getString(r2)     // Catch:{ Exception -> 0x01cb }
            r8.setId(r9)     // Catch:{ Exception -> 0x01cb }
            r8.setText(r1)     // Catch:{ Exception -> 0x01cb }
            r8.setCreated_at(r2)     // Catch:{ Exception -> 0x01cb }
            java.lang.String r9 = "nick"
            java.lang.String r9 = r11.getString(r9)     // Catch:{ Exception -> 0x01cb }
            r3.setScreenName(r9)     // Catch:{ Exception -> 0x01cb }
            java.lang.String r9 = "name"
            java.lang.String r9 = r11.getString(r9)     // Catch:{ Exception -> 0x01cb }
            r3.setId(r9)     // Catch:{ Exception -> 0x01cb }
            r8.setUser(r3)     // Catch:{ Exception -> 0x01cb }
            r7.add(r8)     // Catch:{ Exception -> 0x01cb }
            int r8 = r10 + 1
            r10 = r8
            goto L_0x0180
        L_0x01cb:
            r8 = move-exception
            r8.printStackTrace()
            goto L_0x009c
        L_0x01d1:
            java.lang.String r10 = "sohu_weibo"
            boolean r10 = r8.equals(r10)
            if (r10 == 0) goto L_0x02ef
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            java.lang.String r10 = "http://api.t.sohu.com/statuses/comments/"
            r8.<init>(r10)
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r9 = ".json"
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r10 = r8.toString()
            r8 = 0
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x02d2, IOException -> 0x02d8 }
            java.lang.String r1 = "?count="
            r9.<init>(r1)     // Catch:{ MalformedURLException -> 0x02d2, IOException -> 0x02d8 }
            java.lang.StringBuilder r9 = r9.append(r11)     // Catch:{ MalformedURLException -> 0x02d2, IOException -> 0x02d8 }
            java.lang.String r9 = r9.toString()     // Catch:{ MalformedURLException -> 0x02d2, IOException -> 0x02d8 }
            if (r12 == 0) goto L_0x0217
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x02d2, IOException -> 0x02d8 }
            java.lang.String r9 = java.lang.String.valueOf(r9)     // Catch:{ MalformedURLException -> 0x02d2, IOException -> 0x02d8 }
            r11.<init>(r9)     // Catch:{ MalformedURLException -> 0x02d2, IOException -> 0x02d8 }
            java.lang.String r9 = "&since_id="
            java.lang.StringBuilder r9 = r11.append(r9)     // Catch:{ MalformedURLException -> 0x02d2, IOException -> 0x02d8 }
            java.lang.StringBuilder r9 = r9.append(r12)     // Catch:{ MalformedURLException -> 0x02d2, IOException -> 0x02d8 }
            java.lang.String r9 = r9.toString()     // Catch:{ MalformedURLException -> 0x02d2, IOException -> 0x02d8 }
        L_0x0217:
            java.net.URL r3 = new java.net.URL     // Catch:{ MalformedURLException -> 0x02d2, IOException -> 0x02d8 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x02d2, IOException -> 0x02d8 }
            java.lang.String r10 = java.lang.String.valueOf(r10)     // Catch:{ MalformedURLException -> 0x02d2, IOException -> 0x02d8 }
            r11.<init>(r10)     // Catch:{ MalformedURLException -> 0x02d2, IOException -> 0x02d8 }
            java.lang.StringBuilder r9 = r11.append(r9)     // Catch:{ MalformedURLException -> 0x02d2, IOException -> 0x02d8 }
            java.lang.String r9 = r9.toString()     // Catch:{ MalformedURLException -> 0x02d2, IOException -> 0x02d8 }
            r3.<init>(r9)     // Catch:{ MalformedURLException -> 0x02d2, IOException -> 0x02d8 }
            java.net.URLConnection r9 = r3.openConnection()     // Catch:{ MalformedURLException -> 0x02d2, IOException -> 0x02d8 }
            r0 = r9
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x02d2, IOException -> 0x02d8 }
            r8 = r0
            java.lang.String r9 = "GET"
            r8.setRequestMethod(r9)     // Catch:{ MalformedURLException -> 0x02d2, IOException -> 0x02d8 }
        L_0x023a:
            cn.com.mzba.oauth.OAuth r9 = cn.com.mzba.service.CommentTopicHttpUtils.oauthSohu
            java.lang.String r10 = cn.com.mzba.service.CommentTopicHttpUtils.userTokenSohu
            java.lang.String r11 = cn.com.mzba.service.CommentTopicHttpUtils.userTokenSohuSecret
            java.net.HttpURLConnection r12 = r9.signRequest(r10, r11, r8)
            if (r12 == 0) goto L_0x009c
            r8 = 200(0xc8, float:2.8E-43)
            int r9 = r12.getResponseCode()     // Catch:{ IOException -> 0x02e3, JSONException -> 0x02e9 }
            if (r8 != r9) goto L_0x009c
            java.io.InputStream r9 = r12.getInputStream()     // Catch:{ IOException -> 0x02e3, JSONException -> 0x02e9 }
            java.io.BufferedReader r11 = new java.io.BufferedReader     // Catch:{ IOException -> 0x02e3, JSONException -> 0x02e9 }
            java.io.InputStreamReader r8 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x02e3, JSONException -> 0x02e9 }
            r8.<init>(r9)     // Catch:{ IOException -> 0x02e3, JSONException -> 0x02e9 }
            r11.<init>(r8)     // Catch:{ IOException -> 0x02e3, JSONException -> 0x02e9 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x02e3, JSONException -> 0x02e9 }
            r8.<init>()     // Catch:{ IOException -> 0x02e3, JSONException -> 0x02e9 }
            r10 = 4000(0xfa0, float:5.605E-42)
            char[] r1 = new char[r10]     // Catch:{ IOException -> 0x02e3, JSONException -> 0x02e9 }
            r10 = 0
        L_0x0266:
            int r10 = r11.read(r1)     // Catch:{ IOException -> 0x02e3, JSONException -> 0x02e9 }
            r2 = -1
            if (r10 != r2) goto L_0x02de
            r11.close()     // Catch:{ IOException -> 0x02e3, JSONException -> 0x02e9 }
            r9.close()     // Catch:{ IOException -> 0x02e3, JSONException -> 0x02e9 }
            java.lang.String r8 = r8.toString()     // Catch:{ IOException -> 0x02e3, JSONException -> 0x02e9 }
            r12.disconnect()     // Catch:{ IOException -> 0x02e3, JSONException -> 0x02e9 }
            org.json.JSONArray r11 = new org.json.JSONArray     // Catch:{ IOException -> 0x02e3, JSONException -> 0x02e9 }
            r11.<init>(r8)     // Catch:{ IOException -> 0x02e3, JSONException -> 0x02e9 }
            r8 = 0
            r12 = r8
        L_0x0281:
            int r8 = r11.length()     // Catch:{ IOException -> 0x02e3, JSONException -> 0x02e9 }
            if (r12 >= r8) goto L_0x009c
            cn.com.mzba.model.Comment r8 = new cn.com.mzba.model.Comment     // Catch:{ IOException -> 0x02e3, JSONException -> 0x02e9 }
            r8.<init>()     // Catch:{ IOException -> 0x02e3, JSONException -> 0x02e9 }
            cn.com.mzba.model.User r3 = new cn.com.mzba.model.User     // Catch:{ IOException -> 0x02e3, JSONException -> 0x02e9 }
            r3.<init>()     // Catch:{ IOException -> 0x02e3, JSONException -> 0x02e9 }
            org.json.JSONObject r10 = r11.getJSONObject(r12)     // Catch:{ IOException -> 0x02e3, JSONException -> 0x02e9 }
            java.lang.String r9 = "id"
            java.lang.String r9 = r10.getString(r9)     // Catch:{ IOException -> 0x02e3, JSONException -> 0x02e9 }
            java.lang.String r1 = "text"
            java.lang.String r1 = r10.getString(r1)     // Catch:{ IOException -> 0x02e3, JSONException -> 0x02e9 }
            java.lang.String r2 = "created_at"
            java.lang.String r2 = r10.getString(r2)     // Catch:{ IOException -> 0x02e3, JSONException -> 0x02e9 }
            r8.setId(r9)     // Catch:{ IOException -> 0x02e3, JSONException -> 0x02e9 }
            r8.setText(r1)     // Catch:{ IOException -> 0x02e3, JSONException -> 0x02e9 }
            r8.setCreated_at(r2)     // Catch:{ IOException -> 0x02e3, JSONException -> 0x02e9 }
            java.lang.String r9 = "user"
            org.json.JSONObject r9 = r10.getJSONObject(r9)     // Catch:{ IOException -> 0x02e3, JSONException -> 0x02e9 }
            java.lang.String r10 = "screen_name"
            java.lang.String r10 = r9.getString(r10)     // Catch:{ IOException -> 0x02e3, JSONException -> 0x02e9 }
            r3.setScreenName(r10)     // Catch:{ IOException -> 0x02e3, JSONException -> 0x02e9 }
            java.lang.String r10 = "id"
            java.lang.String r9 = r9.getString(r10)     // Catch:{ IOException -> 0x02e3, JSONException -> 0x02e9 }
            r3.setId(r9)     // Catch:{ IOException -> 0x02e3, JSONException -> 0x02e9 }
            r8.setUser(r3)     // Catch:{ IOException -> 0x02e3, JSONException -> 0x02e9 }
            r7.add(r8)     // Catch:{ IOException -> 0x02e3, JSONException -> 0x02e9 }
            int r8 = r12 + 1
            r12 = r8
            goto L_0x0281
        L_0x02d2:
            r9 = move-exception
            r9.printStackTrace()
            goto L_0x023a
        L_0x02d8:
            r9 = move-exception
            r9.printStackTrace()
            goto L_0x023a
        L_0x02de:
            r2 = 0
            r8.append(r1, r2, r10)     // Catch:{ IOException -> 0x02e3, JSONException -> 0x02e9 }
            goto L_0x0266
        L_0x02e3:
            r8 = move-exception
            r8.printStackTrace()
            goto L_0x009c
        L_0x02e9:
            r8 = move-exception
            r8.printStackTrace()
            goto L_0x009c
        L_0x02ef:
            java.lang.String r10 = "netease_weibo"
            boolean r8 = r8.equals(r10)
            if (r8 == 0) goto L_0x009c
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            java.lang.String r10 = "http://api.t.163.com/statuses/comments/"
            r8.<init>(r10)
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r9 = ".json"
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r10 = r8.toString()
            java.lang.String r8 = cn.com.mzba.service.CommentTopicHttpUtils.TAG
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            java.lang.String r1 = "url:"
            r9.<init>(r1)
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r9 = r9.toString()
            android.util.Log.i(r8, r9)
            r8 = 0
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0429, IOException -> 0x042f }
            java.lang.String r1 = "?count="
            r9.<init>(r1)     // Catch:{ MalformedURLException -> 0x0429, IOException -> 0x042f }
            java.lang.StringBuilder r9 = r9.append(r11)     // Catch:{ MalformedURLException -> 0x0429, IOException -> 0x042f }
            java.lang.String r9 = r9.toString()     // Catch:{ MalformedURLException -> 0x0429, IOException -> 0x042f }
            if (r12 == 0) goto L_0x0349
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0429, IOException -> 0x042f }
            java.lang.String r9 = java.lang.String.valueOf(r9)     // Catch:{ MalformedURLException -> 0x0429, IOException -> 0x042f }
            r11.<init>(r9)     // Catch:{ MalformedURLException -> 0x0429, IOException -> 0x042f }
            java.lang.String r9 = "&since_id="
            java.lang.StringBuilder r9 = r11.append(r9)     // Catch:{ MalformedURLException -> 0x0429, IOException -> 0x042f }
            java.lang.StringBuilder r9 = r9.append(r12)     // Catch:{ MalformedURLException -> 0x0429, IOException -> 0x042f }
            java.lang.String r9 = r9.toString()     // Catch:{ MalformedURLException -> 0x0429, IOException -> 0x042f }
        L_0x0349:
            java.net.URL r3 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0429, IOException -> 0x042f }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0429, IOException -> 0x042f }
            java.lang.String r10 = java.lang.String.valueOf(r10)     // Catch:{ MalformedURLException -> 0x0429, IOException -> 0x042f }
            r11.<init>(r10)     // Catch:{ MalformedURLException -> 0x0429, IOException -> 0x042f }
            java.lang.StringBuilder r9 = r11.append(r9)     // Catch:{ MalformedURLException -> 0x0429, IOException -> 0x042f }
            java.lang.String r9 = r9.toString()     // Catch:{ MalformedURLException -> 0x0429, IOException -> 0x042f }
            r3.<init>(r9)     // Catch:{ MalformedURLException -> 0x0429, IOException -> 0x042f }
            java.net.URLConnection r9 = r3.openConnection()     // Catch:{ MalformedURLException -> 0x0429, IOException -> 0x042f }
            r0 = r9
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x0429, IOException -> 0x042f }
            r8 = r0
            java.lang.String r9 = "GET"
            r8.setRequestMethod(r9)     // Catch:{ MalformedURLException -> 0x0429, IOException -> 0x042f }
        L_0x036c:
            cn.com.mzba.oauth.OAuth r9 = cn.com.mzba.service.CommentTopicHttpUtils.oauthNetease
            java.lang.String r10 = cn.com.mzba.service.CommentTopicHttpUtils.userTokenNetease
            java.lang.String r11 = cn.com.mzba.service.CommentTopicHttpUtils.userTokenNeteaseSecret
            java.net.HttpURLConnection r12 = r9.signRequest(r10, r11, r8)
            if (r12 == 0) goto L_0x009c
            java.lang.String r8 = cn.com.mzba.service.CommentTopicHttpUtils.TAG     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            java.lang.String r10 = "code:"
            r9.<init>(r10)     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            int r10 = r12.getResponseCode()     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            java.lang.String r9 = r9.toString()     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            android.util.Log.i(r8, r9)     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            r8 = 200(0xc8, float:2.8E-43)
            int r9 = r12.getResponseCode()     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            if (r8 != r9) goto L_0x009c
            java.io.InputStream r9 = r12.getInputStream()     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            java.io.BufferedReader r11 = new java.io.BufferedReader     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            java.io.InputStreamReader r8 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            r8.<init>(r9)     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            r11.<init>(r8)     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            r8.<init>()     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            r10 = 4000(0xfa0, float:5.605E-42)
            char[] r1 = new char[r10]     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            r10 = 0
        L_0x03b0:
            int r10 = r11.read(r1)     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            r2 = -1
            if (r10 != r2) goto L_0x0435
            r11.close()     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            r9.close()     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            java.lang.String r8 = r8.toString()     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            r12.disconnect()     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            org.json.JSONArray r11 = new org.json.JSONArray     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            r11.<init>(r8)     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            java.lang.Class<cn.com.mzba.service.CommentTopicHttpUtils> r8 = cn.com.mzba.service.CommentTopicHttpUtils.class
            java.lang.String r8 = r8.getCanonicalName()     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            java.lang.String r9 = r11.toString()     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            android.util.Log.i(r8, r9)     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            r8 = 0
            r12 = r8
        L_0x03d8:
            int r8 = r11.length()     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            if (r12 >= r8) goto L_0x009c
            cn.com.mzba.model.Comment r8 = new cn.com.mzba.model.Comment     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            r8.<init>()     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            cn.com.mzba.model.User r3 = new cn.com.mzba.model.User     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            r3.<init>()     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            org.json.JSONObject r10 = r11.getJSONObject(r12)     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            java.lang.String r9 = "id"
            java.lang.String r9 = r10.getString(r9)     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            java.lang.String r1 = "text"
            java.lang.String r1 = r10.getString(r1)     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            java.lang.String r2 = "created_at"
            java.lang.String r2 = r10.getString(r2)     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            r8.setId(r9)     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            r8.setText(r1)     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            r8.setCreated_at(r2)     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            java.lang.String r9 = "user"
            org.json.JSONObject r9 = r10.getJSONObject(r9)     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            java.lang.String r10 = "screen_name"
            java.lang.String r10 = r9.getString(r10)     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            r3.setScreenName(r10)     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            java.lang.String r10 = "id"
            java.lang.String r9 = r9.getString(r10)     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            r3.setId(r9)     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            r8.setUser(r3)     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            r7.add(r8)     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            int r8 = r12 + 1
            r12 = r8
            goto L_0x03d8
        L_0x0429:
            r9 = move-exception
            r9.printStackTrace()
            goto L_0x036c
        L_0x042f:
            r9 = move-exception
            r9.printStackTrace()
            goto L_0x036c
        L_0x0435:
            r2 = 0
            r8.append(r1, r2, r10)     // Catch:{ IOException -> 0x043b, JSONException -> 0x0441 }
            goto L_0x03b0
        L_0x043b:
            r8 = move-exception
            r8.printStackTrace()
            goto L_0x009c
        L_0x0441:
            r8 = move-exception
            r8.printStackTrace()
            goto L_0x009c
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.com.mzba.service.CommentTopicHttpUtils.getCommentListById(java.lang.String, java.lang.String, int, int, java.lang.String):java.util.List");
    }

    /* JADX INFO: Multiple debug info for r10v11 java.lang.String: [D('builder' java.lang.StringBuilder), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r1v8 org.json.JSONArray: [D('datas' org.json.JSONArray), D('response' java.net.HttpURLConnection)] */
    /* JADX INFO: Multiple debug info for r2v1 int: [D('temp' char[]), D('i' int)] */
    /* JADX INFO: Multiple debug info for r5v1 java.lang.String: [D('user' org.json.JSONObject), D('userIcon' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r7v2 java.lang.String: [D('userName' java.lang.String), D('time' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r6v2 java.lang.String: [D('userId' java.lang.String), D('text' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r10v29 java.lang.String: [D('builder' java.lang.StringBuilder), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r1v11 org.json.JSONArray: [D('datas' org.json.JSONArray), D('response' java.net.HttpURLConnection)] */
    /* JADX INFO: Multiple debug info for r2v4 int: [D('temp' char[]), D('i' int)] */
    /* JADX INFO: Multiple debug info for r5v7 java.lang.String: [D('user' org.json.JSONObject), D('userIcon' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r7v5 java.lang.String: [D('userName' java.lang.String), D('time' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r6v6 java.lang.String: [D('userId' java.lang.String), D('text' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r11v25 java.lang.String: [D('page' int), D('params' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r13v12 java.util.ArrayList: [D('sinceId' java.lang.String), D('params' java.util.List<org.apache.http.message.BasicNameValuePair>)] */
    /* JADX INFO: Multiple debug info for r11v37 java.lang.String: [D('builder' java.lang.StringBuilder), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r2v10 org.json.JSONArray: [D('datas' org.json.JSONArray), D('response' org.apache.http.HttpResponse)] */
    /* JADX INFO: Multiple debug info for r3v32 int: [D('temp' char[]), D('i' int)] */
    /* JADX WARN: Type inference failed for: r11v16, types: [java.net.URLConnection] */
    /* JADX WARN: Type inference failed for: r11v28, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List<cn.com.mzba.model.Comment> getCommentTimeline(java.lang.String r10, int r11, int r12, java.lang.String r13) {
        /*
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            java.lang.String r1 = "sina_weibo"
            boolean r1 = r10.equals(r1)
            if (r1 == 0) goto L_0x0117
            java.lang.String r1 = "http://api.t.sina.com.cn/statuses/comments_to_me.json"
            java.util.ArrayList r13 = new java.util.ArrayList
            r13.<init>()
            org.apache.http.message.BasicNameValuePair r2 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r3 = "page"
            java.lang.String r11 = java.lang.String.valueOf(r11)
            r2.<init>(r3, r11)
            r13.add(r2)
            org.apache.http.message.BasicNameValuePair r11 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r2 = "count"
            java.lang.String r12 = java.lang.String.valueOf(r12)
            r11.<init>(r2, r12)
            r13.add(r11)
            cn.com.mzba.oauth.OAuth r11 = cn.com.mzba.service.CommentTopicHttpUtils.oauthSina
            java.lang.String r12 = cn.com.mzba.service.CommentTopicHttpUtils.userTokenSina
            java.lang.String r2 = cn.com.mzba.service.CommentTopicHttpUtils.userTokenSinaSecret
            org.apache.http.HttpResponse r2 = r11.signRequest(r12, r2, r1, r13)
            if (r2 == 0) goto L_0x0092
            r11 = 200(0xc8, float:2.8E-43)
            org.apache.http.StatusLine r12 = r2.getStatusLine()
            int r12 = r12.getStatusCode()
            if (r11 != r12) goto L_0x0092
            org.apache.http.HttpEntity r11 = r2.getEntity()     // Catch:{ Exception -> 0x0098 }
            java.io.InputStream r12 = r11.getContent()     // Catch:{ Exception -> 0x0098 }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0098 }
            java.io.InputStreamReader r11 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0098 }
            r11.<init>(r12)     // Catch:{ Exception -> 0x0098 }
            r1.<init>(r11)     // Catch:{ Exception -> 0x0098 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0098 }
            org.apache.http.HttpEntity r13 = r2.getEntity()     // Catch:{ Exception -> 0x0098 }
            long r5 = r13.getContentLength()     // Catch:{ Exception -> 0x0098 }
            int r13 = (int) r5     // Catch:{ Exception -> 0x0098 }
            r11.<init>(r13)     // Catch:{ Exception -> 0x0098 }
            r13 = 4000(0xfa0, float:5.605E-42)
            char[] r3 = new char[r13]     // Catch:{ Exception -> 0x0098 }
            r13 = 0
        L_0x006d:
            int r13 = r1.read(r3)     // Catch:{ Exception -> 0x0098 }
            r5 = -1
            if (r13 != r5) goto L_0x0093
            r1.close()     // Catch:{ Exception -> 0x0098 }
            r12.close()     // Catch:{ Exception -> 0x0098 }
            java.lang.String r11 = r11.toString()     // Catch:{ Exception -> 0x0098 }
            org.apache.http.HttpEntity r12 = r2.getEntity()     // Catch:{ Exception -> 0x0098 }
            r12.consumeContent()     // Catch:{ Exception -> 0x0098 }
            org.json.JSONArray r2 = new org.json.JSONArray     // Catch:{ Exception -> 0x0098 }
            r2.<init>(r11)     // Catch:{ Exception -> 0x0098 }
            r11 = 0
            r3 = r11
        L_0x008c:
            int r11 = r2.length()     // Catch:{ Exception -> 0x0098 }
            if (r3 < r11) goto L_0x009d
        L_0x0092:
            return r4
        L_0x0093:
            r5 = 0
            r11.append(r3, r5, r13)     // Catch:{ Exception -> 0x0098 }
            goto L_0x006d
        L_0x0098:
            r10 = move-exception
            r10.printStackTrace()
            goto L_0x0092
        L_0x009d:
            cn.com.mzba.model.Comment r11 = new cn.com.mzba.model.Comment     // Catch:{ Exception -> 0x0098 }
            r11.<init>()     // Catch:{ Exception -> 0x0098 }
            cn.com.mzba.model.User r5 = new cn.com.mzba.model.User     // Catch:{ Exception -> 0x0098 }
            r5.<init>()     // Catch:{ Exception -> 0x0098 }
            cn.com.mzba.model.Status r13 = new cn.com.mzba.model.Status     // Catch:{ Exception -> 0x0098 }
            r13.<init>()     // Catch:{ Exception -> 0x0098 }
            org.json.JSONObject r1 = r2.getJSONObject(r3)     // Catch:{ Exception -> 0x0098 }
            if (r1 == 0) goto L_0x0112
            java.lang.String r12 = "status"
            org.json.JSONObject r12 = r1.getJSONObject(r12)     // Catch:{ Exception -> 0x0098 }
            java.lang.String r6 = "text"
            java.lang.String r6 = r12.getString(r6)     // Catch:{ Exception -> 0x0098 }
            r13.setText(r6)     // Catch:{ Exception -> 0x0098 }
            java.lang.String r6 = "id"
            java.lang.String r12 = r12.getString(r6)     // Catch:{ Exception -> 0x0098 }
            r13.setId(r12)     // Catch:{ Exception -> 0x0098 }
            r11.setStatus(r13)     // Catch:{ Exception -> 0x0098 }
            java.lang.String r12 = "user"
            org.json.JSONObject r12 = r1.getJSONObject(r12)     // Catch:{ Exception -> 0x0098 }
            java.lang.String r13 = "id"
            java.lang.String r13 = r12.getString(r13)     // Catch:{ Exception -> 0x0098 }
            r5.setId(r13)     // Catch:{ Exception -> 0x0098 }
            java.lang.String r13 = "screen_name"
            java.lang.String r13 = r12.getString(r13)     // Catch:{ Exception -> 0x0098 }
            r5.setScreenName(r13)     // Catch:{ Exception -> 0x0098 }
            java.lang.String r13 = "profile_image_url"
            java.lang.String r12 = r12.getString(r13)     // Catch:{ Exception -> 0x0098 }
            r5.setProfileImageUrl(r12)     // Catch:{ Exception -> 0x0098 }
            r11.setUser(r5)     // Catch:{ Exception -> 0x0098 }
            java.lang.String r12 = "id"
            java.lang.String r12 = r1.getString(r12)     // Catch:{ Exception -> 0x0098 }
            r11.setId(r12)     // Catch:{ Exception -> 0x0098 }
            java.lang.String r12 = "text"
            java.lang.String r12 = r1.getString(r12)     // Catch:{ Exception -> 0x0098 }
            r11.setText(r12)     // Catch:{ Exception -> 0x0098 }
            java.lang.String r12 = "created_at"
            java.lang.String r12 = r1.getString(r12)     // Catch:{ Exception -> 0x0098 }
            r11.setCreated_at(r12)     // Catch:{ Exception -> 0x0098 }
            r11.setWeiboId(r10)     // Catch:{ Exception -> 0x0098 }
            r4.add(r11)     // Catch:{ Exception -> 0x0098 }
        L_0x0112:
            int r11 = r3 + 1
            r3 = r11
            goto L_0x008c
        L_0x0117:
            java.lang.String r1 = "tencent_weibo"
            boolean r1 = r10.equals(r1)
            if (r1 != 0) goto L_0x0092
            java.lang.String r1 = "sohu_weibo"
            boolean r1 = r10.equals(r1)
            if (r1 == 0) goto L_0x0274
            java.lang.String r13 = "http://api.t.sohu.com/statuses/comments_timeline.json"
            r10 = 0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0256, IOException -> 0x025c }
            java.lang.String r2 = "?count="
            r1.<init>(r2)     // Catch:{ MalformedURLException -> 0x0256, IOException -> 0x025c }
            java.lang.StringBuilder r12 = r1.append(r12)     // Catch:{ MalformedURLException -> 0x0256, IOException -> 0x025c }
            java.lang.String r1 = "&page="
            java.lang.StringBuilder r12 = r12.append(r1)     // Catch:{ MalformedURLException -> 0x0256, IOException -> 0x025c }
            java.lang.StringBuilder r11 = r12.append(r11)     // Catch:{ MalformedURLException -> 0x0256, IOException -> 0x025c }
            java.lang.String r11 = r11.toString()     // Catch:{ MalformedURLException -> 0x0256, IOException -> 0x025c }
            java.net.URL r12 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0256, IOException -> 0x025c }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0256, IOException -> 0x025c }
            java.lang.String r13 = java.lang.String.valueOf(r13)     // Catch:{ MalformedURLException -> 0x0256, IOException -> 0x025c }
            r1.<init>(r13)     // Catch:{ MalformedURLException -> 0x0256, IOException -> 0x025c }
            java.lang.StringBuilder r11 = r1.append(r11)     // Catch:{ MalformedURLException -> 0x0256, IOException -> 0x025c }
            java.lang.String r11 = r11.toString()     // Catch:{ MalformedURLException -> 0x0256, IOException -> 0x025c }
            r12.<init>(r11)     // Catch:{ MalformedURLException -> 0x0256, IOException -> 0x025c }
            java.net.URLConnection r11 = r12.openConnection()     // Catch:{ MalformedURLException -> 0x0256, IOException -> 0x025c }
            r0 = r11
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x0256, IOException -> 0x025c }
            r10 = r0
            java.lang.String r11 = "GET"
            r10.setRequestMethod(r11)     // Catch:{ MalformedURLException -> 0x0256, IOException -> 0x025c }
        L_0x0166:
            cn.com.mzba.oauth.OAuth r11 = cn.com.mzba.service.CommentTopicHttpUtils.oauthSohu
            java.lang.String r12 = cn.com.mzba.service.CommentTopicHttpUtils.userTokenSohu
            java.lang.String r13 = cn.com.mzba.service.CommentTopicHttpUtils.userTokenSohuSecret
            java.net.HttpURLConnection r1 = r11.signRequest(r12, r13, r10)
            if (r1 == 0) goto L_0x0092
            r10 = 200(0xc8, float:2.8E-43)
            int r11 = r1.getResponseCode()     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            if (r10 != r11) goto L_0x0092
            java.io.InputStream r11 = r1.getInputStream()     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            java.io.BufferedReader r13 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            java.io.InputStreamReader r10 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            r10.<init>(r11)     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            r13.<init>(r10)     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            r10.<init>()     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            r12 = 4000(0xfa0, float:5.605E-42)
            char[] r2 = new char[r12]     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            r12 = 0
        L_0x0192:
            int r12 = r13.read(r2)     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            r3 = -1
            if (r12 != r3) goto L_0x0262
            r13.close()     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            r11.close()     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            java.lang.String r10 = r10.toString()     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            r1.disconnect()     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            org.json.JSONArray r1 = new org.json.JSONArray     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            r1.<init>(r10)     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            r10 = 0
            r2 = r10
        L_0x01ad:
            int r10 = r1.length()     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            if (r2 >= r10) goto L_0x0092
            cn.com.mzba.model.Comment r10 = new cn.com.mzba.model.Comment     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            r10.<init>()     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            cn.com.mzba.model.User r8 = new cn.com.mzba.model.User     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            r8.<init>()     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            org.json.JSONObject r13 = r1.getJSONObject(r2)     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            cn.com.mzba.model.Status r11 = new cn.com.mzba.model.Status     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            r11.<init>()     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            cn.com.mzba.model.User r12 = new cn.com.mzba.model.User     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            r12.<init>()     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            if (r13 == 0) goto L_0x0251
            java.lang.String r3 = "user"
            org.json.JSONObject r5 = r13.getJSONObject(r3)     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            java.lang.String r3 = "id"
            java.lang.String r3 = r13.getString(r3)     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            java.lang.String r6 = "id"
            java.lang.String r6 = r5.getString(r6)     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            java.lang.String r7 = "screen_name"
            java.lang.String r7 = r5.getString(r7)     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            java.lang.String r9 = "profile_image_url"
            java.lang.String r5 = r5.getString(r9)     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            r8.setId(r6)     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            r8.setProfileImageUrl(r5)     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            r8.setScreenName(r7)     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            r10.setUser(r8)     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            java.lang.String r5 = "created_at"
            java.lang.String r7 = r13.getString(r5)     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            java.lang.String r5 = "text"
            java.lang.String r6 = r13.getString(r5)     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            java.lang.String r5 = "source"
            java.lang.String r5 = r13.getString(r5)     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            r10.setId(r3)     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            r10.setText(r6)     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            r10.setCreated_at(r7)     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            java.lang.String r6 = "来自："
            r3.<init>(r6)     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            java.lang.StringBuilder r3 = r3.append(r5)     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            r10.setSource(r3)     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            java.lang.String r3 = "in_reply_to_status_text"
            java.lang.String r3 = r13.getString(r3)     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            r11.setText(r3)     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            java.lang.String r3 = "in_reply_to_status_id"
            java.lang.String r3 = r13.getString(r3)     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            r11.setId(r3)     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            java.lang.String r3 = "in_reply_to_screen_name"
            java.lang.String r3 = r13.getString(r3)     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            r12.setScreenName(r3)     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            java.lang.String r3 = "in_reply_to_user_id"
            java.lang.String r13 = r13.getString(r3)     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            r12.setId(r13)     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            r11.setUser(r12)     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            r10.setStatus(r11)     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            r4.add(r10)     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
        L_0x0251:
            int r10 = r2 + 1
            r2 = r10
            goto L_0x01ad
        L_0x0256:
            r11 = move-exception
            r11.printStackTrace()
            goto L_0x0166
        L_0x025c:
            r11 = move-exception
            r11.printStackTrace()
            goto L_0x0166
        L_0x0262:
            r3 = 0
            r10.append(r2, r3, r12)     // Catch:{ IOException -> 0x0268, JSONException -> 0x026e }
            goto L_0x0192
        L_0x0268:
            r10 = move-exception
            r10.printStackTrace()
            goto L_0x0092
        L_0x026e:
            r10 = move-exception
            r10.printStackTrace()
            goto L_0x0092
        L_0x0274:
            java.lang.String r11 = "netease_weibo"
            boolean r10 = r10.equals(r11)
            if (r10 == 0) goto L_0x0092
            java.lang.String r1 = "http://api.t.163.com/statuses/comments_to_me.json"
            r10 = 0
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x03b8, IOException -> 0x03be }
            java.lang.String r2 = "?count="
            r11.<init>(r2)     // Catch:{ MalformedURLException -> 0x03b8, IOException -> 0x03be }
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ MalformedURLException -> 0x03b8, IOException -> 0x03be }
            java.lang.String r12 = "&since_id="
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ MalformedURLException -> 0x03b8, IOException -> 0x03be }
            java.lang.StringBuilder r11 = r11.append(r13)     // Catch:{ MalformedURLException -> 0x03b8, IOException -> 0x03be }
            java.lang.String r11 = r11.toString()     // Catch:{ MalformedURLException -> 0x03b8, IOException -> 0x03be }
            java.net.URL r12 = new java.net.URL     // Catch:{ MalformedURLException -> 0x03b8, IOException -> 0x03be }
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x03b8, IOException -> 0x03be }
            java.lang.String r1 = java.lang.String.valueOf(r1)     // Catch:{ MalformedURLException -> 0x03b8, IOException -> 0x03be }
            r13.<init>(r1)     // Catch:{ MalformedURLException -> 0x03b8, IOException -> 0x03be }
            java.lang.StringBuilder r11 = r13.append(r11)     // Catch:{ MalformedURLException -> 0x03b8, IOException -> 0x03be }
            java.lang.String r11 = r11.toString()     // Catch:{ MalformedURLException -> 0x03b8, IOException -> 0x03be }
            r12.<init>(r11)     // Catch:{ MalformedURLException -> 0x03b8, IOException -> 0x03be }
            java.net.URLConnection r11 = r12.openConnection()     // Catch:{ MalformedURLException -> 0x03b8, IOException -> 0x03be }
            r0 = r11
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x03b8, IOException -> 0x03be }
            r10 = r0
            java.lang.String r11 = "GET"
            r10.setRequestMethod(r11)     // Catch:{ MalformedURLException -> 0x03b8, IOException -> 0x03be }
        L_0x02bb:
            cn.com.mzba.oauth.OAuth r11 = cn.com.mzba.service.CommentTopicHttpUtils.oauthNetease
            java.lang.String r12 = cn.com.mzba.service.CommentTopicHttpUtils.userTokenNetease
            java.lang.String r13 = cn.com.mzba.service.CommentTopicHttpUtils.userTokenNeteaseSecret
            java.net.HttpURLConnection r1 = r11.signRequest(r12, r13, r10)
            if (r1 == 0) goto L_0x0092
            r10 = 200(0xc8, float:2.8E-43)
            int r11 = r1.getResponseCode()     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            if (r10 != r11) goto L_0x0092
            java.io.InputStream r11 = r1.getInputStream()     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            java.io.BufferedReader r13 = new java.io.BufferedReader     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            java.io.InputStreamReader r10 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            r10.<init>(r11)     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            r13.<init>(r10)     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            r10.<init>()     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            r12 = 4000(0xfa0, float:5.605E-42)
            char[] r2 = new char[r12]     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            r12 = 0
        L_0x02e7:
            int r12 = r13.read(r2)     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            r3 = -1
            if (r12 != r3) goto L_0x03c4
            r13.close()     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            r11.close()     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            java.lang.String r10 = r10.toString()     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            r1.disconnect()     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            org.json.JSONArray r1 = new org.json.JSONArray     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            r1.<init>(r10)     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            java.lang.Class<cn.com.mzba.service.CommentTopicHttpUtils> r10 = cn.com.mzba.service.CommentTopicHttpUtils.class
            java.lang.String r10 = r10.getCanonicalName()     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            java.lang.String r11 = r1.toString()     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            android.util.Log.i(r10, r11)     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            r10 = 0
            r2 = r10
        L_0x030f:
            int r10 = r1.length()     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            if (r2 >= r10) goto L_0x0092
            cn.com.mzba.model.Comment r10 = new cn.com.mzba.model.Comment     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            r10.<init>()     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            cn.com.mzba.model.User r8 = new cn.com.mzba.model.User     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            r8.<init>()     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            org.json.JSONObject r13 = r1.getJSONObject(r2)     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            cn.com.mzba.model.Status r11 = new cn.com.mzba.model.Status     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            r11.<init>()     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            cn.com.mzba.model.User r12 = new cn.com.mzba.model.User     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            r12.<init>()     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            if (r13 == 0) goto L_0x03b3
            java.lang.String r3 = "user"
            org.json.JSONObject r5 = r13.getJSONObject(r3)     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            java.lang.String r3 = "id"
            java.lang.String r3 = r13.getString(r3)     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            java.lang.String r6 = "id"
            java.lang.String r6 = r5.getString(r6)     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            java.lang.String r7 = "screen_name"
            java.lang.String r7 = r5.getString(r7)     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            java.lang.String r9 = "profile_image_url"
            java.lang.String r5 = r5.getString(r9)     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            r8.setId(r6)     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            r8.setProfileImageUrl(r5)     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            r8.setScreenName(r7)     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            r10.setUser(r8)     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            java.lang.String r5 = "created_at"
            java.lang.String r7 = r13.getString(r5)     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            java.lang.String r5 = "text"
            java.lang.String r6 = r13.getString(r5)     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            java.lang.String r5 = "source"
            java.lang.String r5 = r13.getString(r5)     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            r10.setId(r3)     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            r10.setText(r6)     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            r10.setCreated_at(r7)     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            java.lang.String r6 = "来自："
            r3.<init>(r6)     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            java.lang.StringBuilder r3 = r3.append(r5)     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            r10.setSource(r3)     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            java.lang.String r3 = "in_reply_to_status_text"
            java.lang.String r3 = r13.getString(r3)     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            r11.setText(r3)     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            java.lang.String r3 = "in_reply_to_status_id"
            java.lang.String r3 = r13.getString(r3)     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            r11.setId(r3)     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            java.lang.String r3 = "in_reply_to_user_name"
            java.lang.String r3 = r13.getString(r3)     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            r12.setScreenName(r3)     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            java.lang.String r3 = "in_reply_to_user_id"
            java.lang.String r13 = r13.getString(r3)     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            r12.setId(r13)     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            r11.setUser(r12)     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            r10.setStatus(r11)     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            r4.add(r10)     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
        L_0x03b3:
            int r10 = r2 + 1
            r2 = r10
            goto L_0x030f
        L_0x03b8:
            r11 = move-exception
            r11.printStackTrace()
            goto L_0x02bb
        L_0x03be:
            r11 = move-exception
            r11.printStackTrace()
            goto L_0x02bb
        L_0x03c4:
            r3 = 0
            r10.append(r2, r3, r12)     // Catch:{ IOException -> 0x03ca, JSONException -> 0x03d0 }
            goto L_0x02e7
        L_0x03ca:
            r10 = move-exception
            r10.printStackTrace()
            goto L_0x0092
        L_0x03d0:
            r10 = move-exception
            r10.printStackTrace()
            goto L_0x0092
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.com.mzba.service.CommentTopicHttpUtils.getCommentTimeline(java.lang.String, int, int, java.lang.String):java.util.List");
    }

    /* JADX INFO: Multiple debug info for r7v4 org.apache.http.HttpResponse: [D('params' java.util.List<org.apache.http.message.BasicNameValuePair>), D('response' org.apache.http.HttpResponse)] */
    /* JADX INFO: Multiple debug info for r7v9 java.util.ArrayList: [D('weiboId' java.lang.String), D('params' java.util.List<org.apache.http.message.BasicNameValuePair>)] */
    /* JADX INFO: Multiple debug info for r7v10 org.apache.http.HttpResponse: [D('params' java.util.List<org.apache.http.message.BasicNameValuePair>), D('response' org.apache.http.HttpResponse)] */
    /* JADX INFO: Multiple debug info for r7v23 org.json.JSONObject: [D('data' org.json.JSONObject), D('parameter' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r7v33 java.util.ArrayList: [D('weiboId' java.lang.String), D('params' java.util.List<org.apache.http.message.BasicNameValuePair>)] */
    /* JADX INFO: Multiple debug info for r7v34 org.apache.http.HttpResponse: [D('params' java.util.List<org.apache.http.message.BasicNameValuePair>), D('response' org.apache.http.HttpResponse)] */
    public static String comment(String weiboId, String id, String cid, String status) {
        String url;
        if (weiboId.equals(SystemConfig.SINA_WEIBO)) {
            List<BasicNameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("id", id));
            params.add(new BasicNameValuePair("comment", URLEncoder.encode(status)));
            if (cid != null) {
                params.add(new BasicNameValuePair("cid", cid));
            }
            HttpResponse response = oauthSina.signRequest(userTokenSina, userTokenSinaSecret, "http://api.t.sina.com.cn/statuses/comment.json", params);
            if (response != null) {
                if (200 == response.getStatusLine().getStatusCode()) {
                    return SystemConfig.SUCCESS;
                }
                return SystemConfig.ERROR;
            }
        } else if (weiboId.equals(SystemConfig.TENCENT_WEIBO)) {
            List<Parameter> params2 = new ArrayList<>();
            if (cid == null) {
                url = "http://open.t.qq.com/api/t/reply";
                params2.add(new Parameter("format", "json"));
                try {
                    params2.add(new Parameter("content", URLEncoder.encode(status, "UTF-8")));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                params2.add(new Parameter("clientip", "127.0.0.1"));
                params2.add(new Parameter("reid", id));
            } else {
                url = "http://open.t.qq.com/api/t/comment";
                params2.add(new Parameter("format", "json"));
                try {
                    params2.add(new Parameter("content", URLEncoder.encode(status, "UTF-8")));
                } catch (UnsupportedEncodingException e2) {
                    e2.printStackTrace();
                }
                params2.add(new Parameter("clientip", "127.0.0.1"));
                params2.add(new Parameter("reid", id));
            }
            try {
                String parameter = oauthTencent.getPostParameter(SystemConfig.HTTP_POST, url, userTokenTencent, userTokenTencentSecret, params2);
                JSONObject data = new JSONObject(oauthTencent.httpPost(String.valueOf(url) + "?" + parameter, parameter));
                if (data.getString("msg").equals("ok") || data.getJSONObject("data").getString("id") != null) {
                    return SystemConfig.SUCCESS;
                }
                return SystemConfig.ERROR;
            } catch (Exception e3) {
                e3.printStackTrace();
                return null;
            }
        } else if (weiboId.equals(SystemConfig.SOHU_WEIBO)) {
            List<BasicNameValuePair> params3 = new ArrayList<>();
            params3.add(new BasicNameValuePair("id", id));
            params3.add(new BasicNameValuePair("comment", URLEncoder.encode(status)));
            HttpResponse response2 = oauthSohu.signRequest(userTokenSohu, userTokenSohuSecret, "http://api.t.sohu.com/statuses/comment.json", params3);
            if (response2 != null) {
                if (200 == response2.getStatusLine().getStatusCode()) {
                    return SystemConfig.SUCCESS;
                }
                return SystemConfig.ERROR;
            }
        } else if (weiboId.equals(SystemConfig.NETEASE_WEIBO)) {
            List<BasicNameValuePair> params4 = new ArrayList<>();
            params4.add(new BasicNameValuePair("id", id));
            params4.add(new BasicNameValuePair(SqliteHelper.TABLE_STATUS, status));
            HttpResponse response3 = oauthNetease.signRequest(userTokenNetease, userTokenNeteaseSecret, "http://api.t.163.com/statuses/reply.json", params4);
            if (response3 != null) {
                if (200 == response3.getStatusLine().getStatusCode()) {
                    return SystemConfig.SUCCESS;
                }
                return SystemConfig.ERROR;
            }
        }
        return null;
    }

    /* JADX WARN: Type inference failed for: r9v16, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String deleteComment(java.lang.String r13, java.lang.String r14) {
        /*
            r12 = 200(0xc8, float:2.8E-43)
            r7 = 0
            java.lang.String r9 = "sina_weibo"
            boolean r9 = r13.equals(r9)
            if (r9 == 0) goto L_0x003b
            java.lang.String r8 = "http://api.t.sina.com.cn/statuses/comment_destroy/:id.json"
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            org.apache.http.message.BasicNameValuePair r9 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r10 = "id"
            r9.<init>(r10, r14)
            r4.add(r9)
            cn.com.mzba.oauth.OAuth r9 = cn.com.mzba.service.CommentTopicHttpUtils.oauthSina
            java.lang.String r10 = cn.com.mzba.service.CommentTopicHttpUtils.userTokenSina
            java.lang.String r11 = cn.com.mzba.service.CommentTopicHttpUtils.userTokenSinaSecret
            org.apache.http.HttpResponse r6 = r9.signRequest(r10, r11, r8, r4)
            if (r6 == 0) goto L_0x0038
            org.apache.http.StatusLine r9 = r6.getStatusLine()
            int r9 = r9.getStatusCode()
            if (r12 != r9) goto L_0x0035
            java.lang.String r7 = "success"
        L_0x0034:
            return r7
        L_0x0035:
            java.lang.String r7 = "error"
            goto L_0x0034
        L_0x0038:
            java.lang.String r7 = "error"
            goto L_0x0034
        L_0x003b:
            java.lang.String r9 = "tencent_weibo"
            boolean r9 = r13.equals(r9)
            if (r9 == 0) goto L_0x0048
            java.lang.String r7 = cn.com.mzba.service.StatusHttpUtils.deleteStatus(r13, r14)
            goto L_0x0034
        L_0x0048:
            java.lang.String r9 = "sohu_weibo"
            boolean r9 = r13.equals(r9)
            if (r9 == 0) goto L_0x00a5
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            java.lang.String r10 = "http://api.t.sohu.com/statuses/comment_destroy/"
            r9.<init>(r10)
            java.lang.StringBuilder r9 = r9.append(r14)
            java.lang.String r10 = ".json"
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r5 = r9.toString()
            r1 = 0
            java.net.URL r8 = new java.net.URL     // Catch:{ MalformedURLException -> 0x008d, IOException -> 0x0093 }
            r8.<init>(r5)     // Catch:{ MalformedURLException -> 0x008d, IOException -> 0x0093 }
            java.net.URLConnection r9 = r8.openConnection()     // Catch:{ MalformedURLException -> 0x008d, IOException -> 0x0093 }
            r0 = r9
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x008d, IOException -> 0x0093 }
            r1 = r0
            java.lang.String r9 = "DELETE"
            r1.setRequestMethod(r9)     // Catch:{ MalformedURLException -> 0x008d, IOException -> 0x0093 }
        L_0x0078:
            cn.com.mzba.oauth.OAuth r9 = cn.com.mzba.service.CommentTopicHttpUtils.oauthSohu
            java.lang.String r10 = cn.com.mzba.service.CommentTopicHttpUtils.userTokenSohu
            java.lang.String r11 = cn.com.mzba.service.CommentTopicHttpUtils.userTokenSohuSecret
            java.net.HttpURLConnection r6 = r9.signRequest(r10, r11, r1)
            if (r6 == 0) goto L_0x00a2
            int r9 = r6.getResponseCode()     // Catch:{ Exception -> 0x009c }
            if (r12 != r9) goto L_0x0099
            java.lang.String r7 = "success"
            goto L_0x0034
        L_0x008d:
            r9 = move-exception
            r3 = r9
            r3.printStackTrace()
            goto L_0x0078
        L_0x0093:
            r9 = move-exception
            r2 = r9
            r2.printStackTrace()
            goto L_0x0078
        L_0x0099:
            java.lang.String r7 = "error"
            goto L_0x0034
        L_0x009c:
            r9 = move-exception
            r2 = r9
            r2.printStackTrace()
            goto L_0x0034
        L_0x00a2:
            java.lang.String r7 = "error"
            goto L_0x0034
        L_0x00a5:
            java.lang.String r9 = "netease_weibo"
            boolean r9 = r13.equals(r9)
            if (r9 == 0) goto L_0x0034
            java.lang.String r7 = "error"
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.com.mzba.service.CommentTopicHttpUtils.deleteComment(java.lang.String, java.lang.String):java.lang.String");
    }

    /* JADX INFO: Multiple debug info for r7v7 org.apache.http.HttpResponse: [D('params' java.util.List<org.apache.http.message.BasicNameValuePair>), D('response' org.apache.http.HttpResponse)] */
    /* JADX INFO: Multiple debug info for r7v16 org.apache.http.HttpResponse: [D('params' java.util.List<org.apache.http.message.BasicNameValuePair>), D('response' org.apache.http.HttpResponse)] */
    /* JADX INFO: Multiple debug info for r7v28 org.json.JSONObject: [D('data' org.json.JSONObject), D('parameter' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r7v34 java.util.ArrayList: [D('weiboId' java.lang.String), D('params' java.util.List<org.apache.http.message.BasicNameValuePair>)] */
    /* JADX INFO: Multiple debug info for r7v35 org.apache.http.HttpResponse: [D('params' java.util.List<org.apache.http.message.BasicNameValuePair>), D('response' org.apache.http.HttpResponse)] */
    public static String repost(String weiboId, String id, String status, boolean isComment) {
        if (weiboId.equals(SystemConfig.SINA_WEIBO)) {
            List<BasicNameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("id", id));
            params.add(new BasicNameValuePair(SqliteHelper.TABLE_STATUS, URLEncoder.encode(status)));
            if (isComment) {
                params.add(new BasicNameValuePair("is_comment", "1"));
            } else {
                params.add(new BasicNameValuePair("is_comment", "0"));
            }
            HttpResponse response = oauthSina.signRequest(userTokenSina, userTokenSinaSecret, "http://api.t.sina.com.cn/statuses/repost.json", params);
            if (response != null) {
                if (200 == response.getStatusLine().getStatusCode()) {
                    return SystemConfig.SUCCESS;
                }
                return SystemConfig.ERROR;
            }
        } else if (weiboId.equals(SystemConfig.TENCENT_WEIBO)) {
            List<Parameter> params2 = new ArrayList<>();
            params2.add(new Parameter("format", "json"));
            try {
                params2.add(new Parameter("content", URLEncoder.encode(status, "UTF-8")));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            params2.add(new Parameter("clientip", "127.0.0.1"));
            params2.add(new Parameter("Reid", id));
            try {
                String parameter = oauthTencent.getPostParameter(SystemConfig.HTTP_POST, "http://open.t.qq.com/api/t/re_add", userTokenTencent, userTokenTencentSecret, params2);
                JSONObject data = new JSONObject(oauthTencent.httpPost(String.valueOf("http://open.t.qq.com/api/t/re_add") + "?" + parameter, parameter));
                if (data.getString("msg").equals("ok") || data.getJSONObject("data").getString("id") != null) {
                    return SystemConfig.SUCCESS;
                }
                return SystemConfig.ERROR;
            } catch (Exception e2) {
                e2.printStackTrace();
                return null;
            }
        } else if (weiboId.equals(SystemConfig.SOHU_WEIBO)) {
            List<BasicNameValuePair> params3 = new ArrayList<>();
            params3.add(new BasicNameValuePair("id", id));
            params3.add(new BasicNameValuePair(SqliteHelper.TABLE_STATUS, URLEncoder.encode(status)));
            HttpResponse response2 = oauthSohu.signRequest(userTokenSohu, userTokenSohuSecret, "http://api.t.sohu.com/statuses/transmit/" + id + ".json", params3);
            if (response2 != null) {
                if (200 == response2.getStatusLine().getStatusCode()) {
                    return SystemConfig.SUCCESS;
                }
                return SystemConfig.ERROR;
            }
        } else if (weiboId.equals(SystemConfig.NETEASE_WEIBO)) {
            String url = "http://api.t.163.com/statuses/retweet/" + id + ".json";
            List<BasicNameValuePair> params4 = new ArrayList<>();
            params4.add(new BasicNameValuePair("id", id));
            params4.add(new BasicNameValuePair(SqliteHelper.TABLE_STATUS, URLEncoder.encode(status)));
            if (isComment) {
                params4.add(new BasicNameValuePair("is_comment", "1"));
            } else {
                params4.add(new BasicNameValuePair("is_comment", "0"));
            }
            HttpResponse response3 = oauthNetease.signRequest(userTokenNetease, userTokenNeteaseSecret, url, params4);
            if (response3 != null) {
                Log.i(TAG, "code:" + response3.getStatusLine().getStatusCode());
                if (200 == response3.getStatusLine().getStatusCode()) {
                    return SystemConfig.SUCCESS;
                }
                return SystemConfig.ERROR;
            }
        }
        return null;
    }

    public static String addFavorite(String weiboId, String id) {
        if (weiboId.equals(SystemConfig.SINA_WEIBO)) {
            List<BasicNameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("id", id));
            HttpResponse response = oauthSina.signRequest(userTokenSina, userTokenSinaSecret, "http://api.t.sina.com.cn/favorites/create.json", params);
            if (response == null) {
                return null;
            }
            if (200 == response.getStatusLine().getStatusCode()) {
                return SystemConfig.SUCCESS;
            }
            return SystemConfig.ERROR;
        } else if (weiboId.equals(SystemConfig.TENCENT_WEIBO)) {
            List<Parameter> params2 = new ArrayList<>();
            params2.add(new Parameter("format", "json"));
            params2.add(new Parameter("id", id));
            try {
                String parameter = oauthTencent.getPostParameter(SystemConfig.HTTP_POST, "http://open.t.qq.com/api/fav/addt", userTokenTencent, userTokenTencentSecret, params2);
                JSONObject data = new JSONObject(oauthTencent.httpPost(String.valueOf("http://open.t.qq.com/api/fav/addt") + "?" + parameter, parameter));
                if (data.getString("msg").equals("ok") || data.getJSONObject("data").getString("tweetid") != null) {
                    return SystemConfig.SUCCESS;
                }
                return SystemConfig.ERROR;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else if (weiboId.equals(SystemConfig.SOHU_WEIBO)) {
            List<BasicNameValuePair> params3 = new ArrayList<>();
            params3.add(new BasicNameValuePair("id", id));
            HttpResponse response2 = oauthSohu.signRequest(userTokenSohu, userTokenSohuSecret, "http://api.t.sohu.com/favourites/create/" + id + ".json", params3);
            if (response2 == null) {
                return null;
            }
            if (200 == response2.getStatusLine().getStatusCode()) {
                return SystemConfig.SUCCESS;
            }
            return SystemConfig.ERROR;
        } else if (!weiboId.equals(SystemConfig.NETEASE_WEIBO)) {
            return null;
        } else {
            List<BasicNameValuePair> params4 = new ArrayList<>();
            params4.add(new BasicNameValuePair("id", id));
            HttpResponse response3 = oauthNetease.signRequest(userTokenNetease, userTokenNeteaseSecret, "http://api.t.163.com/favorites/create/" + id + ".json", params4);
            if (response3 == null) {
                return null;
            }
            if (200 == response3.getStatusLine().getStatusCode()) {
                return SystemConfig.SUCCESS;
            }
            return SystemConfig.ERROR;
        }
    }

    public static String deleteFavorite(String weiboId, String id) {
        if (weiboId.equals(SystemConfig.SINA_WEIBO)) {
            List<BasicNameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("id", id));
            HttpResponse response = oauthSina.signRequest(userTokenSina, userTokenSinaSecret, "http://api.t.sina.com.cn/favorites/destroy/:id.json", params);
            if (response == null) {
                return null;
            }
            if (200 == response.getStatusLine().getStatusCode()) {
                return SystemConfig.SUCCESS;
            }
            return SystemConfig.ERROR;
        } else if (weiboId.equals(SystemConfig.TENCENT_WEIBO)) {
            List<Parameter> params2 = new ArrayList<>();
            params2.add(new Parameter("format", "json"));
            params2.add(new Parameter("id", id));
            try {
                String parameter = oauthTencent.getPostParameter(SystemConfig.HTTP_POST, "http://open.t.qq.com/api/fav/delt", userTokenTencent, userTokenTencentSecret, params2);
                JSONObject data = new JSONObject(oauthTencent.httpPost(String.valueOf("http://open.t.qq.com/api/fav/delt") + "?" + parameter, parameter));
                if (data.getString("msg").equals("ok") || data.getJSONObject("data").getString("tweetid") != null) {
                    return SystemConfig.SUCCESS;
                }
                return SystemConfig.ERROR;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else if (weiboId.equals(SystemConfig.SOHU_WEIBO)) {
            List<BasicNameValuePair> params3 = new ArrayList<>();
            params3.add(new BasicNameValuePair("id", id));
            HttpResponse response2 = oauthSohu.signRequest(userTokenSohu, userTokenSohuSecret, "http://api.t.sohu.com/favourites/destroy/" + id + ".json", params3);
            if (response2 == null) {
                return null;
            }
            if (200 == response2.getStatusLine().getStatusCode()) {
                return SystemConfig.SUCCESS;
            }
            return SystemConfig.ERROR;
        } else if (!weiboId.equals(SystemConfig.NETEASE_WEIBO)) {
            return null;
        } else {
            List<BasicNameValuePair> params4 = new ArrayList<>();
            params4.add(new BasicNameValuePair("id", id));
            HttpResponse response3 = oauthNetease.signRequest(userTokenNetease, userTokenNeteaseSecret, "http://api.t.163.com/favorites/destroy/" + id + ".json", params4);
            if (response3 == null) {
                return null;
            }
            Log.i(TAG, "code:" + response3.getStatusLine().getStatusCode());
            if (200 == response3.getStatusLine().getStatusCode()) {
                return SystemConfig.SUCCESS;
            }
            return SystemConfig.ERROR;
        }
    }

    public static String deleteDirect(String weiboId, String id) {
        if (weiboId.equals(SystemConfig.SINA_WEIBO)) {
            List<BasicNameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("id", String.valueOf(id)));
            HttpResponse response = oauthSina.signRequest(userTokenSina, userTokenSinaSecret, "http://api.t.sina.com.cn/direct_messages/destroy/:id.json", params);
            if (response == null) {
                return SystemConfig.ERROR;
            }
            try {
                if (200 == response.getStatusLine().getStatusCode()) {
                    return SystemConfig.SUCCESS;
                }
                return SystemConfig.ERROR;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else if (weiboId.equals(SystemConfig.TENCENT_WEIBO)) {
            List<Parameter> params2 = new ArrayList<>();
            params2.add(new Parameter("format", "json"));
            params2.add(new Parameter("id", id));
            try {
                String parameter = oauthTencent.getPostParameter(SystemConfig.HTTP_POST, "http://open.t.qq.com/api/private/del", userTokenTencent, userTokenTencentSecret, params2);
                JSONObject data = new JSONObject(oauthTencent.httpPost(String.valueOf("http://open.t.qq.com/api/private/del") + "?" + parameter, parameter));
                if (data.getString("msg").equals("ok") || data.getJSONObject("data").getString("tweetid") != null) {
                    return SystemConfig.SUCCESS;
                }
                return SystemConfig.ERROR;
            } catch (Exception e2) {
                e2.printStackTrace();
                return null;
            }
        } else if (weiboId.equals(SystemConfig.SOHU_WEIBO)) {
            List<BasicNameValuePair> params3 = new ArrayList<>();
            params3.add(new BasicNameValuePair("id", id));
            HttpResponse response2 = oauthSohu.signRequest(userTokenSohu, userTokenSohuSecret, "http://api.t.sohu.com/direct_messages/destroy/" + id + ".json", params3);
            if (response2 == null) {
                return null;
            }
            if (200 == response2.getStatusLine().getStatusCode()) {
                return SystemConfig.SUCCESS;
            }
            return SystemConfig.ERROR;
        } else if (!weiboId.equals(SystemConfig.NETEASE_WEIBO)) {
            return null;
        } else {
            List<BasicNameValuePair> params4 = new ArrayList<>();
            params4.add(new BasicNameValuePair("id", id));
            HttpResponse response3 = oauthNetease.signRequest(userTokenNetease, userTokenNeteaseSecret, "http://api.t.163.com/direct_messages/destroy/" + id + ".json", params4);
            if (response3 == null) {
                return null;
            }
            Log.i(TAG, "code:" + response3.getStatusLine().getStatusCode());
            if (200 == response3.getStatusLine().getStatusCode()) {
                return SystemConfig.SUCCESS;
            }
            return SystemConfig.ERROR;
        }
    }

    /* JADX INFO: Multiple debug info for r7v4 org.apache.http.HttpResponse: [D('params' java.util.List<org.apache.http.message.BasicNameValuePair>), D('response' org.apache.http.HttpResponse)] */
    /* JADX INFO: Multiple debug info for r7v9 java.util.ArrayList: [D('weiboId' java.lang.String), D('params' java.util.List<org.apache.http.message.BasicNameValuePair>)] */
    /* JADX INFO: Multiple debug info for r7v10 org.apache.http.HttpResponse: [D('params' java.util.List<org.apache.http.message.BasicNameValuePair>), D('response' org.apache.http.HttpResponse)] */
    /* JADX INFO: Multiple debug info for r7v22 org.json.JSONObject: [D('data' org.json.JSONObject), D('parameter' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r7v28 java.util.ArrayList: [D('weiboId' java.lang.String), D('params' java.util.List<org.apache.http.message.BasicNameValuePair>)] */
    /* JADX INFO: Multiple debug info for r7v29 org.apache.http.HttpResponse: [D('params' java.util.List<org.apache.http.message.BasicNameValuePair>), D('response' org.apache.http.HttpResponse)] */
    public static String sendDirect(String weiboId, String userId, String userName, String status) {
        if (weiboId.equals(SystemConfig.SINA_WEIBO)) {
            List<BasicNameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("id", userId));
            params.add(new BasicNameValuePair("text", URLEncoder.encode(status)));
            HttpResponse response = oauthSina.signRequest(userTokenSina, userTokenSinaSecret, "http://api.t.sina.com.cn/direct_messages/new.json", params);
            if (response == null) {
                return SystemConfig.ERROR;
            }
            try {
                if (200 == response.getStatusLine().getStatusCode()) {
                    return SystemConfig.SUCCESS;
                }
                return SystemConfig.ERROR;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else if (weiboId.equals(SystemConfig.TENCENT_WEIBO)) {
            List<Parameter> params2 = new ArrayList<>();
            params2.add(new Parameter("format", "json"));
            try {
                params2.add(new Parameter("content", URLEncoder.encode(status, "UTF-8")));
            } catch (UnsupportedEncodingException e2) {
                e2.printStackTrace();
            }
            params2.add(new Parameter("clientip", "127.0.0.1"));
            params2.add(new Parameter("name", userId));
            try {
                String parameter = oauthTencent.getPostParameter(SystemConfig.HTTP_POST, "http://open.t.qq.com/api/private/add", userTokenTencent, userTokenTencentSecret, params2);
                JSONObject data = new JSONObject(oauthTencent.httpPost(String.valueOf("http://open.t.qq.com/api/private/add") + "?" + parameter, parameter));
                if (data.getString("msg").equals("ok") || data.getJSONObject("data").getString("tweetid") != null) {
                    return SystemConfig.SUCCESS;
                }
                return SystemConfig.ERROR;
            } catch (Exception e3) {
                e3.printStackTrace();
                return null;
            }
        } else {
            if (weiboId.equals(SystemConfig.SOHU_WEIBO)) {
                List<BasicNameValuePair> params3 = new ArrayList<>();
                params3.add(new BasicNameValuePair(SqliteHelper.TABLE_USER, userId));
                params3.add(new BasicNameValuePair("text", URLEncoder.encode(status)));
                HttpResponse response2 = oauthSohu.signRequest(userTokenSohu, userTokenSohuSecret, "http://api.t.sohu.com/direct_messages/new.json", params3);
                if (response2 != null) {
                    Log.i(TAG, "code:" + response2.getStatusLine().getStatusCode());
                    if (200 == response2.getStatusLine().getStatusCode()) {
                        return SystemConfig.SUCCESS;
                    }
                    return SystemConfig.ERROR;
                }
            } else if (weiboId.equals(SystemConfig.NETEASE_WEIBO)) {
                List<BasicNameValuePair> params4 = new ArrayList<>();
                params4.add(new BasicNameValuePair(SqliteHelper.TABLE_USER, userName));
                params4.add(new BasicNameValuePair("text", URLEncoder.encode(status)));
                HttpResponse response3 = oauthNetease.signRequest(userTokenNetease, userTokenNeteaseSecret, "http://api.t.163.com/direct_messages/new.json", params4);
                if (response3 != null) {
                    Log.i(TAG, "code:" + response3.getStatusLine().getStatusCode());
                    if (200 == response3.getStatusLine().getStatusCode()) {
                        return SystemConfig.SUCCESS;
                    }
                    return SystemConfig.ERROR;
                }
            }
            return null;
        }
    }
}
