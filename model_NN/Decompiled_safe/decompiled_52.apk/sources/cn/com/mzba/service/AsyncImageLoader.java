package cn.com.mzba.service;

import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.widget.ImageView;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

public class AsyncImageLoader {
    /* access modifiers changed from: private */
    public HashMap<String, SoftReference<Drawable>> imageCache = new HashMap<>();

    public Drawable loadDrawable(final String imageUrl, final ImageView imageView, final ImageCallback imagecallback) {
        Drawable drawable;
        if (this.imageCache.containsKey(imageUrl) && (drawable = (Drawable) this.imageCache.get(imageUrl).get()) != null) {
            return drawable;
        }
        final Handler handler = new Handler() {
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                imagecallback.imageLoaded((Drawable) msg.obj, imageView, imageUrl);
            }
        };
        new Thread() {
            public void run() {
                Drawable drawable = AsyncImageLoader.loadImageFromUrl(imageUrl);
                AsyncImageLoader.this.imageCache.put(imageUrl, new SoftReference(drawable));
                handler.sendMessage(handler.obtainMessage(0, drawable));
            }
        }.start();
        return null;
    }

    public static Drawable loadImageFromUrl(String urlPath) {
        InputStream is = null;
        try {
            is = ((HttpURLConnection) new URL(urlPath).openConnection()).getInputStream();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Drawable.createFromStream(is, "src");
    }
}
