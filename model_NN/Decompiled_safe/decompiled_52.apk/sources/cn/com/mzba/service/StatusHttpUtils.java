package cn.com.mzba.service;

import android.util.Log;
import cn.com.mzba.db.SqliteHelper;
import cn.com.mzba.db.UnReadInfo;
import cn.com.mzba.model.Emotions;
import cn.com.mzba.oauth.OAuth;
import cn.com.mzba.oauth.Parameter;
import cn.com.mzba.oauth.SignatureUtil;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.cookie.CookieSpec;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.StringPart;
import org.apache.http.HttpResponse;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

public class StatusHttpUtils {
    private static String TAG = StatusHttpUtils.class.getCanonicalName();
    public static OAuth oauthNetease = ServiceUtils.getNeteaseOauth();
    public static OAuth oauthSina = ServiceUtils.getSinaOauth();
    public static OAuth oauthSohu = ServiceUtils.getSohuOauth();
    public static OAuth oauthTencent = ServiceUtils.getTencentOauth();
    public static String userTokenNetease = ServiceUtils.userTokenNetease;
    public static String userTokenNeteaseSecret = ServiceUtils.userTokenNeteaseSecret;
    public static String userTokenSina = ServiceUtils.userTokenSina;
    public static String userTokenSinaSecret = ServiceUtils.userTokenSinaSecret;
    public static String userTokenSohu = ServiceUtils.userTokenSohu;
    public static String userTokenSohuSecret = ServiceUtils.userTokenSohuSecret;
    public static String userTokenTencent = ServiceUtils.userTokenTencent;
    public static String userTokenTencentSecret = ServiceUtils.userTokenTencentSecret;

    /* JADX INFO: Multiple debug info for r22v11 java.lang.String: [D('builder' java.lang.StringBuilder), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r5v7 java.lang.String[]: [D('response' java.net.HttpURLConnection), D('idArray' java.lang.String[])] */
    /* JADX INFO: Multiple debug info for r24v5 int: [D('i' int), D('len' int)] */
    /* JADX INFO: Multiple debug info for r9v1 java.lang.String: [D('user' org.json.JSONObject), D('text' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r24v7 java.lang.String: [D('params' java.lang.String), D('count' int)] */
    /* JADX INFO: Multiple debug info for r22v36 java.lang.String: [D('builder' java.lang.StringBuilder), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r5v13 java.lang.String[]: [D('response' java.net.HttpURLConnection), D('idArray' java.lang.String[])] */
    /* JADX INFO: Multiple debug info for r24v19 int: [D('i' int), D('len' int)] */
    /* JADX INFO: Multiple debug info for r6v16 cn.com.mzba.model.Status: [D('transmitId' java.lang.String), D('rtStatus' cn.com.mzba.model.Status)] */
    /* JADX INFO: Multiple debug info for r10v9 java.lang.String: [D('user' org.json.JSONObject), D('time' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r22v46 java.lang.String: [D('data' org.json.JSONObject), D('thumbnail_pic' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r22v56 java.lang.String: [D('url' java.lang.String), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r25v32 java.lang.String: [D('info' org.json.JSONObject), D('redirectCount' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r24v38 org.json.JSONObject: [D('type' int), D('source' org.json.JSONObject)] */
    /* JADX INFO: Multiple debug info for r24v39 java.lang.String: [D('rtName' java.lang.String), D('source' org.json.JSONObject)] */
    /* JADX INFO: Multiple debug info for r5v22 org.apache.http.HttpResponse: [D('response' org.apache.http.HttpResponse), D('params' java.util.List<org.apache.http.message.BasicNameValuePair>)] */
    /* JADX INFO: Multiple debug info for r22v90 java.lang.String: [D('builder' java.lang.StringBuilder), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r5v23 java.lang.String[]: [D('response' org.apache.http.HttpResponse), D('idArray' java.lang.String[])] */
    /* JADX INFO: Multiple debug info for r22v98 int: [D('rtStatus' cn.com.mzba.model.Status), D('i' int)] */
    /* JADX INFO: Multiple debug info for r22v104 java.lang.String: [D('data' org.json.JSONObject), D('thumbnail_pic' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r6v45 'rtStatus'  cn.com.mzba.model.Status: [D('rtStatus' cn.com.mzba.model.Status), D('transmitText' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r7v53 java.lang.String: [D('user_retweeted' org.json.JSONObject), D('transmitUserName' java.lang.String)] */
    /* JADX WARNING: Removed duplicated region for block: B:137:0x0820  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x05a5  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List<cn.com.mzba.model.Status> getFriendTimeLines(java.lang.String r21, java.lang.String r22, int r23, int r24, java.lang.String r25) {
        /*
            java.util.ArrayList r11 = new java.util.ArrayList
            r11.<init>()
            java.lang.String r5 = "sina_weibo"
            r0 = r21
            r1 = r5
            boolean r5 = r0.equals(r1)
            if (r5 == 0) goto L_0x02b3
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            java.lang.Class<cn.com.mzba.service.StatusHttpUtils> r6 = cn.com.mzba.service.StatusHttpUtils.class
            java.lang.String r6 = r6.getCanonicalName()
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            java.lang.String r8 = "sinceId:"
            r7.<init>(r8)
            r0 = r7
            r1 = r25
            java.lang.StringBuilder r7 = r0.append(r1)
            java.lang.String r7 = r7.toString()
            android.util.Log.i(r6, r7)
            org.apache.http.message.BasicNameValuePair r6 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r7 = "page"
            java.lang.String r23 = java.lang.String.valueOf(r23)
            r0 = r6
            r1 = r7
            r2 = r23
            r0.<init>(r1, r2)
            r5.add(r6)
            org.apache.http.message.BasicNameValuePair r23 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r6 = "count"
            java.lang.String r24 = java.lang.String.valueOf(r24)
            r0 = r23
            r1 = r6
            r2 = r24
            r0.<init>(r1, r2)
            r0 = r5
            r1 = r23
            r0.add(r1)
            org.apache.http.message.BasicNameValuePair r23 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r24 = "since_id"
            r23.<init>(r24, r25)
            r0 = r5
            r1 = r23
            r0.add(r1)
            cn.com.mzba.oauth.OAuth r23 = cn.com.mzba.service.StatusHttpUtils.oauthSina
            java.lang.String r24 = cn.com.mzba.service.StatusHttpUtils.userTokenSina
            java.lang.String r25 = cn.com.mzba.service.StatusHttpUtils.userTokenSinaSecret
            r0 = r23
            r1 = r24
            r2 = r25
            r3 = r22
            r4 = r5
            org.apache.http.HttpResponse r5 = r0.signRequest(r1, r2, r3, r4)
            if (r5 == 0) goto L_0x010e
            r22 = 200(0xc8, float:2.8E-43)
            org.apache.http.StatusLine r23 = r5.getStatusLine()
            int r23 = r23.getStatusCode()
            r0 = r22
            r1 = r23
            if (r0 != r1) goto L_0x010e
            org.apache.http.HttpEntity r22 = r5.getEntity()     // Catch:{ Exception -> 0x011a }
            java.io.InputStream r23 = r22.getContent()     // Catch:{ Exception -> 0x011a }
            java.io.BufferedReader r25 = new java.io.BufferedReader     // Catch:{ Exception -> 0x011a }
            java.io.InputStreamReader r22 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x011a }
            r22.<init>(r23)     // Catch:{ Exception -> 0x011a }
            r0 = r25
            r1 = r22
            r0.<init>(r1)     // Catch:{ Exception -> 0x011a }
            java.lang.StringBuilder r22 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x011a }
            org.apache.http.HttpEntity r24 = r5.getEntity()     // Catch:{ Exception -> 0x011a }
            long r6 = r24.getContentLength()     // Catch:{ Exception -> 0x011a }
            r0 = r6
            int r0 = (int) r0     // Catch:{ Exception -> 0x011a }
            r24 = r0
            r0 = r22
            r1 = r24
            r0.<init>(r1)     // Catch:{ Exception -> 0x011a }
            r24 = 4000(0xfa0, float:5.605E-42)
            r0 = r24
            char[] r0 = new char[r0]     // Catch:{ Exception -> 0x011a }
            r6 = r0
            r24 = 0
        L_0x00be:
            r0 = r25
            r1 = r6
            int r24 = r0.read(r1)     // Catch:{ Exception -> 0x011a }
            r7 = -1
            r0 = r24
            r1 = r7
            if (r0 != r1) goto L_0x010f
            r25.close()     // Catch:{ Exception -> 0x011a }
            r23.close()     // Catch:{ Exception -> 0x011a }
            java.lang.String r22 = r22.toString()     // Catch:{ Exception -> 0x011a }
            org.apache.http.HttpEntity r23 = r5.getEntity()     // Catch:{ Exception -> 0x011a }
            r23.consumeContent()     // Catch:{ Exception -> 0x011a }
            org.json.JSONArray r23 = new org.json.JSONArray     // Catch:{ Exception -> 0x011a }
            r0 = r23
            r1 = r22
            r0.<init>(r1)     // Catch:{ Exception -> 0x011a }
            int r22 = r23.length()     // Catch:{ Exception -> 0x011a }
            r0 = r22
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ Exception -> 0x011a }
            r5 = r0
            r22 = 0
            r24 = r22
        L_0x00f2:
            int r22 = r23.length()     // Catch:{ Exception -> 0x011a }
            r0 = r24
            r1 = r22
            if (r0 < r1) goto L_0x011f
            java.lang.String r22 = cn.com.mzba.service.StringUtil.arrayToString(r5)     // Catch:{ Exception -> 0x011a }
            java.util.List r22 = getCounts(r21, r22)     // Catch:{ Exception -> 0x011a }
            java.util.Iterator r25 = r11.iterator()     // Catch:{ Exception -> 0x011a }
        L_0x0108:
            boolean r23 = r25.hasNext()     // Catch:{ Exception -> 0x011a }
            if (r23 != 0) goto L_0x026c
        L_0x010e:
            return r11
        L_0x010f:
            r7 = 0
            r0 = r22
            r1 = r6
            r2 = r7
            r3 = r24
            r0.append(r1, r2, r3)     // Catch:{ Exception -> 0x011a }
            goto L_0x00be
        L_0x011a:
            r21 = move-exception
            r21.printStackTrace()
            goto L_0x010e
        L_0x011f:
            cn.com.mzba.model.Status r8 = new cn.com.mzba.model.Status     // Catch:{ Exception -> 0x011a }
            r8.<init>()     // Catch:{ Exception -> 0x011a }
            cn.com.mzba.model.User r16 = new cn.com.mzba.model.User     // Catch:{ Exception -> 0x011a }
            r16.<init>()     // Catch:{ Exception -> 0x011a }
            r25 = 0
            org.json.JSONObject r22 = r23.getJSONObject(r24)     // Catch:{ Exception -> 0x011a }
            if (r22 == 0) goto L_0x0a19
            java.lang.String r6 = "user"
            r0 = r22
            r1 = r6
            org.json.JSONObject r10 = r0.getJSONObject(r1)     // Catch:{ Exception -> 0x011a }
            java.lang.String r6 = ""
            java.lang.String r7 = "retweeted_status"
            r0 = r22
            r1 = r7
            boolean r7 = r0.has(r1)     // Catch:{ Exception -> 0x011a }
            if (r7 == 0) goto L_0x0a11
            cn.com.mzba.model.Status r6 = new cn.com.mzba.model.Status     // Catch:{ Exception -> 0x011a }
            r6.<init>()     // Catch:{ Exception -> 0x011a }
            r25 = 1
            r0 = r8
            r1 = r25
            r0.setRetweetedStatus(r1)     // Catch:{ Exception -> 0x011a }
            java.lang.String r25 = "retweeted_status"
            r0 = r22
            r1 = r25
            org.json.JSONObject r25 = r0.getJSONObject(r1)     // Catch:{ Exception -> 0x011a }
            java.lang.String r7 = "user"
            r0 = r25
            r1 = r7
            org.json.JSONObject r7 = r0.getJSONObject(r1)     // Catch:{ Exception -> 0x011a }
            java.lang.String r9 = "screen_name"
            java.lang.String r7 = r7.getString(r9)     // Catch:{ Exception -> 0x011a }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x011a }
            java.lang.String r12 = "@"
            r9.<init>(r12)     // Catch:{ Exception -> 0x011a }
            java.lang.StringBuilder r7 = r9.append(r7)     // Catch:{ Exception -> 0x011a }
            java.lang.String r9 = ":"
            java.lang.StringBuilder r7 = r7.append(r9)     // Catch:{ Exception -> 0x011a }
            java.lang.String r9 = "text"
            r0 = r25
            r1 = r9
            java.lang.String r9 = r0.getString(r1)     // Catch:{ Exception -> 0x011a }
            java.lang.StringBuilder r7 = r7.append(r9)     // Catch:{ Exception -> 0x011a }
            java.lang.String r9 = r7.toString()     // Catch:{ Exception -> 0x011a }
            java.lang.String r7 = ""
            java.lang.String r12 = "thumbnail_pic"
            r0 = r25
            r1 = r12
            boolean r12 = r0.has(r1)     // Catch:{ Exception -> 0x011a }
            if (r12 == 0) goto L_0x01a5
            java.lang.String r7 = "thumbnail_pic"
            r0 = r25
            r1 = r7
            java.lang.String r7 = r0.getString(r1)     // Catch:{ Exception -> 0x011a }
        L_0x01a5:
            java.lang.String r12 = "id"
            r0 = r25
            r1 = r12
            java.lang.String r25 = r0.getString(r1)     // Catch:{ Exception -> 0x011a }
            r0 = r6
            r1 = r25
            r0.setId(r1)     // Catch:{ Exception -> 0x011a }
            r6.setText(r9)     // Catch:{ Exception -> 0x011a }
            r6.setThumbnailPic(r7)     // Catch:{ Exception -> 0x011a }
            r8.setStatus(r6)     // Catch:{ Exception -> 0x011a }
            r25 = r9
        L_0x01bf:
            java.lang.String r25 = "id"
            r0 = r22
            r1 = r25
            java.lang.String r25 = r0.getString(r1)     // Catch:{ Exception -> 0x011a }
            r5[r24] = r25     // Catch:{ Exception -> 0x011a }
            java.lang.String r7 = "id"
            java.lang.String r14 = r10.getString(r7)     // Catch:{ Exception -> 0x011a }
            java.lang.String r7 = "screen_name"
            java.lang.String r15 = r10.getString(r7)     // Catch:{ Exception -> 0x011a }
            java.lang.String r7 = "profile_image_url"
            java.lang.String r13 = r10.getString(r7)     // Catch:{ Exception -> 0x011a }
            java.lang.String r7 = "created_at"
            r0 = r22
            r1 = r7
            java.lang.String r12 = r0.getString(r1)     // Catch:{ Exception -> 0x011a }
            java.lang.String r7 = "text"
            r0 = r22
            r1 = r7
            java.lang.String r9 = r0.getString(r1)     // Catch:{ Exception -> 0x011a }
            java.lang.String r7 = "source"
            r0 = r22
            r1 = r7
            java.lang.String r7 = r0.getString(r1)     // Catch:{ Exception -> 0x011a }
            java.lang.String r10 = ""
            java.lang.String r17 = "thumbnail_pic"
            r0 = r22
            r1 = r17
            boolean r17 = r0.has(r1)     // Catch:{ Exception -> 0x011a }
            if (r17 == 0) goto L_0x0a0d
            java.lang.String r10 = "thumbnail_pic"
            r0 = r22
            r1 = r10
            java.lang.String r22 = r0.getString(r1)     // Catch:{ Exception -> 0x011a }
            r0 = r8
            r1 = r22
            r0.setThumbnailPic(r1)     // Catch:{ Exception -> 0x011a }
        L_0x0215:
            r0 = r8
            r1 = r25
            r0.setId(r1)     // Catch:{ Exception -> 0x011a }
            r8.setText(r9)     // Catch:{ Exception -> 0x011a }
            r8.setCreatedAt(r12)     // Catch:{ Exception -> 0x011a }
            java.lang.StringBuilder r22 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x011a }
            java.lang.String r25 = "来自："
            r0 = r22
            r1 = r25
            r0.<init>(r1)     // Catch:{ Exception -> 0x011a }
            r0 = r22
            r1 = r7
            java.lang.StringBuilder r22 = r0.append(r1)     // Catch:{ Exception -> 0x011a }
            java.lang.String r22 = r22.toString()     // Catch:{ Exception -> 0x011a }
            r0 = r8
            r1 = r22
            r0.setSource(r1)     // Catch:{ Exception -> 0x011a }
            r0 = r8
            r1 = r21
            r0.setWeiboId(r1)     // Catch:{ Exception -> 0x011a }
            r0 = r16
            r1 = r14
            r0.setId(r1)     // Catch:{ Exception -> 0x011a }
            r0 = r16
            r1 = r13
            r0.setProfileImageUrl(r1)     // Catch:{ Exception -> 0x011a }
            r0 = r16
            r1 = r15
            r0.setScreenName(r1)     // Catch:{ Exception -> 0x011a }
            r0 = r8
            r1 = r16
            r0.setUser(r1)     // Catch:{ Exception -> 0x011a }
            r0 = r8
            r1 = r21
            r0.setWeiboId(r1)     // Catch:{ Exception -> 0x011a }
            r11.add(r8)     // Catch:{ Exception -> 0x011a }
            r22 = r6
        L_0x0266:
            int r22 = r24 + 1
            r24 = r22
            goto L_0x00f2
        L_0x026c:
            java.lang.Object r24 = r25.next()     // Catch:{ Exception -> 0x011a }
            cn.com.mzba.model.Status r24 = (cn.com.mzba.model.Status) r24     // Catch:{ Exception -> 0x011a }
            java.util.Iterator r5 = r22.iterator()     // Catch:{ Exception -> 0x011a }
        L_0x0276:
            boolean r23 = r5.hasNext()     // Catch:{ Exception -> 0x011a }
            if (r23 == 0) goto L_0x0108
            java.lang.Object r23 = r5.next()     // Catch:{ Exception -> 0x011a }
            cn.com.mzba.model.StatusCount r23 = (cn.com.mzba.model.StatusCount) r23     // Catch:{ Exception -> 0x011a }
            java.lang.String r6 = r23.getWeiboId()     // Catch:{ Exception -> 0x011a }
            r0 = r21
            r1 = r6
            boolean r6 = r0.equals(r1)     // Catch:{ Exception -> 0x011a }
            if (r6 == 0) goto L_0x0276
            java.lang.String r6 = r24.getId()     // Catch:{ Exception -> 0x011a }
            java.lang.String r7 = r23.getId()     // Catch:{ Exception -> 0x011a }
            boolean r6 = r6.equals(r7)     // Catch:{ Exception -> 0x011a }
            if (r6 == 0) goto L_0x0276
            java.lang.String r6 = r23.getComments()     // Catch:{ Exception -> 0x011a }
            r0 = r24
            r1 = r6
            r0.setComments(r1)     // Catch:{ Exception -> 0x011a }
            java.lang.String r23 = r23.getRt()     // Catch:{ Exception -> 0x011a }
            r0 = r24
            r1 = r23
            r0.setRt(r1)     // Catch:{ Exception -> 0x011a }
            goto L_0x0276
        L_0x02b3:
            java.lang.String r5 = "tencent_weibo"
            r0 = r21
            r1 = r5
            boolean r5 = r0.equals(r1)
            if (r5 == 0) goto L_0x0514
            java.util.ArrayList r10 = new java.util.ArrayList
            r10.<init>()
            cn.com.mzba.oauth.Parameter r25 = new cn.com.mzba.oauth.Parameter
            java.lang.String r5 = "Reqnum"
            java.lang.String r24 = java.lang.String.valueOf(r24)
            r0 = r25
            r1 = r5
            r2 = r24
            r0.<init>(r1, r2)
            r0 = r10
            r1 = r25
            r0.add(r1)
            cn.com.mzba.oauth.Parameter r24 = new cn.com.mzba.oauth.Parameter
            java.lang.String r25 = "type"
            r5 = 0
            java.lang.String r5 = java.lang.String.valueOf(r5)
            r0 = r24
            r1 = r25
            r2 = r5
            r0.<init>(r1, r2)
            r0 = r10
            r1 = r24
            r0.add(r1)
            cn.com.mzba.oauth.Parameter r24 = new cn.com.mzba.oauth.Parameter
            java.lang.String r25 = "Pageflag"
            r5 = 1
            int r23 = r23 - r5
            java.lang.String r23 = java.lang.String.valueOf(r23)
            r0 = r24
            r1 = r25
            r2 = r23
            r0.<init>(r1, r2)
            r0 = r10
            r1 = r24
            r0.add(r1)
            cn.com.mzba.oauth.Parameter r23 = new cn.com.mzba.oauth.Parameter
            java.lang.String r24 = "format"
            java.lang.String r25 = "json"
            r23.<init>(r24, r25)
            r0 = r10
            r1 = r23
            r0.add(r1)
            cn.com.mzba.oauth.OAuth r5 = cn.com.mzba.service.StatusHttpUtils.oauthTencent     // Catch:{ Exception -> 0x050e }
            java.lang.String r6 = "GET"
            java.lang.String r8 = cn.com.mzba.service.StatusHttpUtils.userTokenTencent     // Catch:{ Exception -> 0x050e }
            java.lang.String r9 = cn.com.mzba.service.StatusHttpUtils.userTokenTencentSecret     // Catch:{ Exception -> 0x050e }
            r7 = r22
            java.lang.String r23 = r5.getPostParameter(r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x050e }
            cn.com.mzba.oauth.OAuth r24 = cn.com.mzba.service.StatusHttpUtils.oauthTencent     // Catch:{ Exception -> 0x050e }
            r0 = r24
            r1 = r22
            r2 = r23
            java.lang.String r22 = r0.httpGet(r1, r2)     // Catch:{ Exception -> 0x050e }
            java.lang.String r23 = cn.com.mzba.service.StatusHttpUtils.TAG     // Catch:{ Exception -> 0x050e }
            r0 = r23
            r1 = r22
            android.util.Log.i(r0, r1)     // Catch:{ Exception -> 0x050e }
            if (r22 == 0) goto L_0x010e
            org.json.JSONObject r23 = new org.json.JSONObject     // Catch:{ Exception -> 0x050e }
            r0 = r23
            r1 = r22
            r0.<init>(r1)     // Catch:{ Exception -> 0x050e }
            java.lang.String r22 = "data"
            r0 = r23
            r1 = r22
            org.json.JSONObject r22 = r0.getJSONObject(r1)     // Catch:{ Exception -> 0x050e }
            java.lang.String r23 = "info"
            org.json.JSONArray r5 = r22.getJSONArray(r23)     // Catch:{ Exception -> 0x050e }
            r22 = 0
            r23 = r22
        L_0x035b:
            int r22 = r5.length()     // Catch:{ Exception -> 0x050e }
            r0 = r23
            r1 = r22
            if (r0 >= r1) goto L_0x010e
            cn.com.mzba.model.Status r8 = new cn.com.mzba.model.Status     // Catch:{ Exception -> 0x050e }
            r8.<init>()     // Catch:{ Exception -> 0x050e }
            cn.com.mzba.model.User r16 = new cn.com.mzba.model.User     // Catch:{ Exception -> 0x050e }
            r16.<init>()     // Catch:{ Exception -> 0x050e }
            r22 = 0
            r0 = r5
            r1 = r23
            org.json.JSONObject r25 = r0.getJSONObject(r1)     // Catch:{ Exception -> 0x050e }
            if (r25 == 0) goto L_0x0508
            java.lang.String r24 = "type"
            r0 = r25
            r1 = r24
            int r24 = r0.getInt(r1)     // Catch:{ Exception -> 0x050e }
            r6 = 2
            r0 = r24
            r1 = r6
            if (r0 != r1) goto L_0x0a09
            java.lang.String r22 = "source"
            r0 = r25
            r1 = r22
            org.json.JSONObject r24 = r0.getJSONObject(r1)     // Catch:{ Exception -> 0x050e }
            cn.com.mzba.model.Status r6 = new cn.com.mzba.model.Status     // Catch:{ Exception -> 0x050e }
            r6.<init>()     // Catch:{ Exception -> 0x050e }
            r22 = 1
            r0 = r8
            r1 = r22
            r0.setRetweetedStatus(r1)     // Catch:{ Exception -> 0x050e }
            java.lang.String r22 = "text"
            r0 = r24
            r1 = r22
            java.lang.String r7 = r0.getString(r1)     // Catch:{ Exception -> 0x050e }
            java.lang.String r22 = "id"
            r0 = r24
            r1 = r22
            java.lang.String r22 = r0.getString(r1)     // Catch:{ Exception -> 0x050e }
            java.lang.String r9 = "nick"
            r0 = r24
            r1 = r9
            java.lang.String r24 = r0.getString(r1)     // Catch:{ Exception -> 0x050e }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x050e }
            java.lang.String r10 = "@"
            r9.<init>(r10)     // Catch:{ Exception -> 0x050e }
            r0 = r9
            r1 = r24
            java.lang.StringBuilder r24 = r0.append(r1)     // Catch:{ Exception -> 0x050e }
            java.lang.String r9 = ":"
            r0 = r24
            r1 = r9
            java.lang.StringBuilder r24 = r0.append(r1)     // Catch:{ Exception -> 0x050e }
            r0 = r24
            r1 = r7
            java.lang.StringBuilder r24 = r0.append(r1)     // Catch:{ Exception -> 0x050e }
            java.lang.String r24 = r24.toString()     // Catch:{ Exception -> 0x050e }
            r0 = r6
            r1 = r24
            r0.setText(r1)     // Catch:{ Exception -> 0x050e }
            r0 = r6
            r1 = r22
            r0.setId(r1)     // Catch:{ Exception -> 0x050e }
            r8.setStatus(r6)     // Catch:{ Exception -> 0x050e }
        L_0x03ef:
            java.lang.String r22 = "id"
            r0 = r25
            r1 = r22
            java.lang.String r24 = r0.getString(r1)     // Catch:{ Exception -> 0x050e }
            java.lang.String r22 = "name"
            r0 = r25
            r1 = r22
            java.lang.String r14 = r0.getString(r1)     // Catch:{ Exception -> 0x050e }
            java.lang.String r22 = "nick"
            r0 = r25
            r1 = r22
            java.lang.String r15 = r0.getString(r1)     // Catch:{ Exception -> 0x050e }
            java.lang.String r22 = "head"
            r0 = r25
            r1 = r22
            java.lang.String r13 = r0.getString(r1)     // Catch:{ Exception -> 0x050e }
            java.lang.String r22 = "timestamp"
            r0 = r25
            r1 = r22
            java.lang.String r12 = r0.getString(r1)     // Catch:{ Exception -> 0x050e }
            java.lang.String r22 = "text"
            r0 = r25
            r1 = r22
            java.lang.String r9 = r0.getString(r1)     // Catch:{ Exception -> 0x050e }
            java.lang.String r22 = "from"
            r0 = r25
            r1 = r22
            java.lang.String r7 = r0.getString(r1)     // Catch:{ Exception -> 0x050e }
            java.lang.String r22 = "image"
            r0 = r25
            r1 = r22
            java.lang.String r22 = r0.getString(r1)     // Catch:{ Exception -> 0x050e }
            if (r22 == 0) goto L_0x0a05
            java.lang.String r10 = "null"
            r0 = r22
            r1 = r10
            boolean r10 = r0.equals(r1)     // Catch:{ Exception -> 0x050e }
            if (r10 != 0) goto L_0x0a05
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x050e }
            r17 = 2
            int r18 = r22.length()     // Catch:{ Exception -> 0x050e }
            r19 = 2
            int r18 = r18 - r19
            r0 = r22
            r1 = r17
            r2 = r18
            java.lang.String r22 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x050e }
            java.lang.String r22 = java.lang.String.valueOf(r22)     // Catch:{ Exception -> 0x050e }
            r0 = r10
            r1 = r22
            r0.<init>(r1)     // Catch:{ Exception -> 0x050e }
            java.lang.String r22 = "/120"
            r0 = r10
            r1 = r22
            java.lang.StringBuilder r22 = r0.append(r1)     // Catch:{ Exception -> 0x050e }
            java.lang.String r22 = r22.toString()     // Catch:{ Exception -> 0x050e }
            r10 = r22
        L_0x047b:
            java.lang.String r22 = "mcount"
            r0 = r25
            r1 = r22
            java.lang.String r22 = r0.getString(r1)     // Catch:{ Exception -> 0x050e }
            java.lang.String r17 = "count"
            r0 = r25
            r1 = r17
            java.lang.String r25 = r0.getString(r1)     // Catch:{ Exception -> 0x050e }
            r8.setThumbnailPic(r10)     // Catch:{ Exception -> 0x050e }
            r0 = r8
            r1 = r24
            r0.setId(r1)     // Catch:{ Exception -> 0x050e }
            r8.setText(r9)     // Catch:{ Exception -> 0x050e }
            r8.setCreatedAt(r12)     // Catch:{ Exception -> 0x050e }
            java.lang.StringBuilder r24 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x050e }
            java.lang.String r9 = "来自："
            r0 = r24
            r1 = r9
            r0.<init>(r1)     // Catch:{ Exception -> 0x050e }
            r0 = r24
            r1 = r7
            java.lang.StringBuilder r24 = r0.append(r1)     // Catch:{ Exception -> 0x050e }
            java.lang.String r24 = r24.toString()     // Catch:{ Exception -> 0x050e }
            r0 = r8
            r1 = r24
            r0.setSource(r1)     // Catch:{ Exception -> 0x050e }
            r0 = r8
            r1 = r21
            r0.setWeiboId(r1)     // Catch:{ Exception -> 0x050e }
            r0 = r16
            r1 = r14
            r0.setId(r1)     // Catch:{ Exception -> 0x050e }
            java.lang.StringBuilder r24 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x050e }
            java.lang.String r7 = java.lang.String.valueOf(r13)     // Catch:{ Exception -> 0x050e }
            r0 = r24
            r1 = r7
            r0.<init>(r1)     // Catch:{ Exception -> 0x050e }
            java.lang.String r7 = "/40"
            r0 = r24
            r1 = r7
            java.lang.StringBuilder r24 = r0.append(r1)     // Catch:{ Exception -> 0x050e }
            java.lang.String r24 = r24.toString()     // Catch:{ Exception -> 0x050e }
            r0 = r16
            r1 = r24
            r0.setProfileImageUrl(r1)     // Catch:{ Exception -> 0x050e }
            r0 = r16
            r1 = r15
            r0.setScreenName(r1)     // Catch:{ Exception -> 0x050e }
            r0 = r8
            r1 = r16
            r0.setUser(r1)     // Catch:{ Exception -> 0x050e }
            r0 = r8
            r1 = r21
            r0.setWeiboId(r1)     // Catch:{ Exception -> 0x050e }
            r0 = r8
            r1 = r22
            r0.setComments(r1)     // Catch:{ Exception -> 0x050e }
            r0 = r8
            r1 = r25
            r0.setRt(r1)     // Catch:{ Exception -> 0x050e }
            r11.add(r8)     // Catch:{ Exception -> 0x050e }
            r22 = r6
        L_0x0508:
            int r22 = r23 + 1
            r23 = r22
            goto L_0x035b
        L_0x050e:
            r21 = move-exception
            r21.printStackTrace()
            goto L_0x010e
        L_0x0514:
            java.lang.String r5 = "sohu_weibo"
            r0 = r21
            r1 = r5
            boolean r5 = r0.equals(r1)
            if (r5 == 0) goto L_0x07a2
            r5 = 0
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0672, IOException -> 0x067c }
            java.lang.String r7 = "?page="
            r6.<init>(r7)     // Catch:{ MalformedURLException -> 0x0672, IOException -> 0x067c }
            r0 = r6
            r1 = r23
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x0672, IOException -> 0x067c }
            java.lang.String r6 = "&count="
            r0 = r23
            r1 = r6
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x0672, IOException -> 0x067c }
            java.lang.StringBuilder r23 = r23.append(r24)     // Catch:{ MalformedURLException -> 0x0672, IOException -> 0x067c }
            java.lang.String r23 = r23.toString()     // Catch:{ MalformedURLException -> 0x0672, IOException -> 0x067c }
            if (r25 == 0) goto L_0x0564
            java.lang.StringBuilder r24 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0672, IOException -> 0x067c }
            java.lang.String r23 = java.lang.String.valueOf(r23)     // Catch:{ MalformedURLException -> 0x0672, IOException -> 0x067c }
            r0 = r24
            r1 = r23
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x0672, IOException -> 0x067c }
            java.lang.String r23 = "&since_id="
            r0 = r24
            r1 = r23
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x0672, IOException -> 0x067c }
            r0 = r23
            r1 = r25
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x0672, IOException -> 0x067c }
            java.lang.String r23 = r23.toString()     // Catch:{ MalformedURLException -> 0x0672, IOException -> 0x067c }
        L_0x0564:
            java.net.URL r24 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0672, IOException -> 0x067c }
            java.lang.StringBuilder r25 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0672, IOException -> 0x067c }
            java.lang.String r22 = java.lang.String.valueOf(r22)     // Catch:{ MalformedURLException -> 0x0672, IOException -> 0x067c }
            r0 = r25
            r1 = r22
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x0672, IOException -> 0x067c }
            r0 = r25
            r1 = r23
            java.lang.StringBuilder r22 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x0672, IOException -> 0x067c }
            java.lang.String r22 = r22.toString()     // Catch:{ MalformedURLException -> 0x0672, IOException -> 0x067c }
            r0 = r24
            r1 = r22
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x0672, IOException -> 0x067c }
            java.net.URLConnection r22 = r24.openConnection()     // Catch:{ MalformedURLException -> 0x0672, IOException -> 0x067c }
            java.net.HttpURLConnection r22 = (java.net.HttpURLConnection) r22     // Catch:{ MalformedURLException -> 0x0672, IOException -> 0x067c }
            java.lang.String r23 = "GET"
            r22.setRequestMethod(r23)     // Catch:{ MalformedURLException -> 0x09fb, IOException -> 0x09f8 }
        L_0x0591:
            cn.com.mzba.oauth.OAuth r23 = cn.com.mzba.service.StatusHttpUtils.oauthSohu
            java.lang.String r24 = cn.com.mzba.service.StatusHttpUtils.userTokenSohu
            java.lang.String r25 = cn.com.mzba.service.StatusHttpUtils.userTokenSohuSecret
            r0 = r23
            r1 = r24
            r2 = r25
            r3 = r22
            java.net.HttpURLConnection r5 = r0.signRequest(r1, r2, r3)
            if (r5 == 0) goto L_0x010e
            r22 = 200(0xc8, float:2.8E-43)
            int r23 = r5.getResponseCode()     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            r0 = r22
            r1 = r23
            if (r0 != r1) goto L_0x010e
            java.io.InputStream r23 = r5.getInputStream()     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            java.io.BufferedReader r25 = new java.io.BufferedReader     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            java.io.InputStreamReader r22 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            r22.<init>(r23)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            r0 = r25
            r1 = r22
            r0.<init>(r1)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            java.lang.StringBuilder r22 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            int r24 = r5.getContentLength()     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            r0 = r22
            r1 = r24
            r0.<init>(r1)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            r24 = 4000(0xfa0, float:5.605E-42)
            r0 = r24
            char[] r0 = new char[r0]     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            r6 = r0
            r24 = 0
        L_0x05d9:
            r0 = r25
            r1 = r6
            int r24 = r0.read(r1)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            r7 = -1
            r0 = r24
            r1 = r7
            if (r0 != r1) goto L_0x0686
            r25.close()     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            r23.close()     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            java.lang.String r22 = r22.toString()     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            r5.disconnect()     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            org.json.JSONArray r23 = new org.json.JSONArray     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            r0 = r23
            r1 = r22
            r0.<init>(r1)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            int r22 = r23.length()     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            r0 = r22
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            r5 = r0
            r22 = 0
            r24 = r22
        L_0x0609:
            int r22 = r23.length()     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            r0 = r24
            r1 = r22
            if (r0 < r1) goto L_0x0698
            java.lang.String r22 = cn.com.mzba.service.StringUtil.arrayToString(r5)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            java.util.List r22 = getCounts(r21, r22)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            java.util.Iterator r25 = r11.iterator()     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
        L_0x061f:
            boolean r23 = r25.hasNext()     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            if (r23 == 0) goto L_0x010e
            java.lang.Object r24 = r25.next()     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            cn.com.mzba.model.Status r24 = (cn.com.mzba.model.Status) r24     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            java.util.Iterator r5 = r22.iterator()     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
        L_0x062f:
            boolean r23 = r5.hasNext()     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            if (r23 == 0) goto L_0x061f
            java.lang.Object r23 = r5.next()     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            cn.com.mzba.model.StatusCount r23 = (cn.com.mzba.model.StatusCount) r23     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            java.lang.String r6 = r23.getWeiboId()     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            r0 = r21
            r1 = r6
            boolean r6 = r0.equals(r1)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            if (r6 == 0) goto L_0x062f
            java.lang.String r6 = r24.getId()     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            java.lang.String r7 = r23.getId()     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            boolean r6 = r6.equals(r7)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            if (r6 == 0) goto L_0x062f
            java.lang.String r6 = r23.getComments()     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            r0 = r24
            r1 = r6
            r0.setComments(r1)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            java.lang.String r23 = r23.getRt()     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            r0 = r24
            r1 = r23
            r0.setRt(r1)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            goto L_0x062f
        L_0x066c:
            r21 = move-exception
            r21.printStackTrace()
            goto L_0x010e
        L_0x0672:
            r22 = move-exception
            r23 = r22
            r22 = r5
        L_0x0677:
            r23.printStackTrace()
            goto L_0x0591
        L_0x067c:
            r22 = move-exception
            r23 = r22
            r22 = r5
        L_0x0681:
            r23.printStackTrace()
            goto L_0x0591
        L_0x0686:
            r7 = 0
            r0 = r22
            r1 = r6
            r2 = r7
            r3 = r24
            r0.append(r1, r2, r3)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            goto L_0x05d9
        L_0x0692:
            r21 = move-exception
            r21.printStackTrace()
            goto L_0x010e
        L_0x0698:
            cn.com.mzba.model.Status r8 = new cn.com.mzba.model.Status     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            r8.<init>()     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            cn.com.mzba.model.User r15 = new cn.com.mzba.model.User     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            r15.<init>()     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            r25 = 0
            org.json.JSONObject r22 = r23.getJSONObject(r24)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            if (r22 == 0) goto L_0x0a01
            java.lang.String r6 = "user"
            r0 = r22
            r1 = r6
            org.json.JSONObject r10 = r0.getJSONObject(r1)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            java.lang.String r6 = "in_reply_to_status_text"
            r0 = r22
            r1 = r6
            java.lang.String r7 = r0.getString(r1)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            if (r7 == 0) goto L_0x070c
            java.lang.String r6 = ""
            boolean r6 = r7.equals(r6)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            if (r6 != 0) goto L_0x070c
            cn.com.mzba.model.Status r25 = new cn.com.mzba.model.Status     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            r25.<init>()     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            r6 = 1
            r8.setRetweetedStatus(r6)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            java.lang.String r6 = "in_reply_to_screen_name"
            r0 = r22
            r1 = r6
            java.lang.String r9 = r0.getString(r1)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            java.lang.String r6 = "in_reply_to_status_id"
            r0 = r22
            r1 = r6
            java.lang.String r6 = r0.getString(r1)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            java.lang.String r13 = "@"
            r12.<init>(r13)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            java.lang.StringBuilder r9 = r12.append(r9)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            java.lang.String r12 = ":"
            java.lang.StringBuilder r9 = r9.append(r12)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            java.lang.StringBuilder r7 = r9.append(r7)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            java.lang.String r7 = r7.toString()     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            r0 = r25
            r1 = r7
            r0.setText(r1)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            r0 = r25
            r1 = r6
            r0.setId(r1)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            r0 = r8
            r1 = r25
            r0.setStatus(r1)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
        L_0x070c:
            r6 = r25
            java.lang.String r25 = "id"
            r0 = r22
            r1 = r25
            java.lang.String r25 = r0.getString(r1)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            r5[r24] = r25     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            java.lang.String r7 = "id"
            java.lang.String r13 = r10.getString(r7)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            java.lang.String r7 = "screen_name"
            java.lang.String r14 = r10.getString(r7)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            java.lang.String r7 = "profile_image_url"
            java.lang.String r12 = r10.getString(r7)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            java.lang.String r7 = "created_at"
            r0 = r22
            r1 = r7
            java.lang.String r10 = r0.getString(r1)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            java.lang.String r7 = "text"
            r0 = r22
            r1 = r7
            java.lang.String r9 = r0.getString(r1)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            java.lang.String r7 = "source"
            r0 = r22
            r1 = r7
            java.lang.String r7 = r0.getString(r1)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            java.lang.String r16 = "small_pic"
            r0 = r22
            r1 = r16
            java.lang.String r22 = r0.getString(r1)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            r0 = r8
            r1 = r22
            r0.setThumbnailPic(r1)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            r0 = r8
            r1 = r25
            r0.setId(r1)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            r8.setText(r9)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            r8.setCreatedAt(r10)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            java.lang.StringBuilder r22 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            java.lang.String r25 = "来自："
            r0 = r22
            r1 = r25
            r0.<init>(r1)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            r0 = r22
            r1 = r7
            java.lang.StringBuilder r22 = r0.append(r1)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            java.lang.String r22 = r22.toString()     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            r0 = r8
            r1 = r22
            r0.setSource(r1)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            r0 = r8
            r1 = r21
            r0.setWeiboId(r1)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            r15.setId(r13)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            r15.setProfileImageUrl(r12)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            r15.setScreenName(r14)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            r8.setUser(r15)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            r0 = r8
            r1 = r21
            r0.setWeiboId(r1)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            r11.add(r8)     // Catch:{ IOException -> 0x066c, JSONException -> 0x0692 }
            r22 = r6
        L_0x079c:
            int r22 = r24 + 1
            r24 = r22
            goto L_0x0609
        L_0x07a2:
            java.lang.String r23 = "netease_weibo"
            r0 = r21
            r1 = r23
            boolean r23 = r0.equals(r1)
            if (r23 == 0) goto L_0x010e
            r23 = 0
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x09c4, IOException -> 0x09d0 }
            java.lang.String r6 = "?count="
            r5.<init>(r6)     // Catch:{ MalformedURLException -> 0x09c4, IOException -> 0x09d0 }
            r0 = r5
            r1 = r24
            java.lang.StringBuilder r24 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x09c4, IOException -> 0x09d0 }
            java.lang.String r24 = r24.toString()     // Catch:{ MalformedURLException -> 0x09c4, IOException -> 0x09d0 }
            if (r25 == 0) goto L_0x07e1
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x09c4, IOException -> 0x09d0 }
            java.lang.String r24 = java.lang.String.valueOf(r24)     // Catch:{ MalformedURLException -> 0x09c4, IOException -> 0x09d0 }
            r0 = r5
            r1 = r24
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x09c4, IOException -> 0x09d0 }
            java.lang.String r24 = "&max_id="
            r0 = r5
            r1 = r24
            java.lang.StringBuilder r24 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x09c4, IOException -> 0x09d0 }
            java.lang.StringBuilder r24 = r24.append(r25)     // Catch:{ MalformedURLException -> 0x09c4, IOException -> 0x09d0 }
            java.lang.String r24 = r24.toString()     // Catch:{ MalformedURLException -> 0x09c4, IOException -> 0x09d0 }
        L_0x07e1:
            java.net.URL r25 = new java.net.URL     // Catch:{ MalformedURLException -> 0x09c4, IOException -> 0x09d0 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x09c4, IOException -> 0x09d0 }
            java.lang.String r22 = java.lang.String.valueOf(r22)     // Catch:{ MalformedURLException -> 0x09c4, IOException -> 0x09d0 }
            r0 = r5
            r1 = r22
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x09c4, IOException -> 0x09d0 }
            r0 = r5
            r1 = r24
            java.lang.StringBuilder r22 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x09c4, IOException -> 0x09d0 }
            java.lang.String r22 = r22.toString()     // Catch:{ MalformedURLException -> 0x09c4, IOException -> 0x09d0 }
            r0 = r25
            r1 = r22
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x09c4, IOException -> 0x09d0 }
            java.net.URLConnection r22 = r25.openConnection()     // Catch:{ MalformedURLException -> 0x09c4, IOException -> 0x09d0 }
            java.net.HttpURLConnection r22 = (java.net.HttpURLConnection) r22     // Catch:{ MalformedURLException -> 0x09c4, IOException -> 0x09d0 }
            java.lang.String r23 = "GET"
            r22.setRequestMethod(r23)     // Catch:{ MalformedURLException -> 0x09f6, IOException -> 0x09f4 }
        L_0x080c:
            cn.com.mzba.oauth.OAuth r23 = cn.com.mzba.service.StatusHttpUtils.oauthNetease
            java.lang.String r24 = cn.com.mzba.service.StatusHttpUtils.userTokenNetease
            java.lang.String r25 = cn.com.mzba.service.StatusHttpUtils.userTokenNeteaseSecret
            r0 = r23
            r1 = r24
            r2 = r25
            r3 = r22
            java.net.HttpURLConnection r5 = r0.signRequest(r1, r2, r3)
            if (r5 == 0) goto L_0x010e
            r22 = 200(0xc8, float:2.8E-43)
            int r23 = r5.getResponseCode()     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            r0 = r22
            r1 = r23
            if (r0 != r1) goto L_0x010e
            java.io.InputStream r23 = r5.getInputStream()     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            java.io.BufferedReader r25 = new java.io.BufferedReader     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            java.io.InputStreamReader r22 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            r22.<init>(r23)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            r0 = r25
            r1 = r22
            r0.<init>(r1)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            java.lang.StringBuilder r22 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            r22.<init>()     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            r24 = 4000(0xfa0, float:5.605E-42)
            r0 = r24
            char[] r0 = new char[r0]     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            r6 = r0
            r24 = 0
        L_0x084c:
            r0 = r25
            r1 = r6
            int r24 = r0.read(r1)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            r7 = -1
            r0 = r24
            r1 = r7
            if (r0 != r1) goto L_0x09dc
            r25.close()     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            r23.close()     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            java.lang.String r22 = r22.toString()     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            r5.disconnect()     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            org.json.JSONArray r23 = new org.json.JSONArray     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            r0 = r23
            r1 = r22
            r0.<init>(r1)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            int r22 = r23.length()     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            r0 = r22
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            r5 = r0
            r22 = 0
            r24 = r22
        L_0x087c:
            int r22 = r23.length()     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            r0 = r24
            r1 = r22
            if (r0 >= r1) goto L_0x010e
            cn.com.mzba.model.Status r8 = new cn.com.mzba.model.Status     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            r8.<init>()     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            cn.com.mzba.model.User r15 = new cn.com.mzba.model.User     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            r15.<init>()     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            r25 = 0
            org.json.JSONObject r22 = r23.getJSONObject(r24)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            if (r22 == 0) goto L_0x09fe
            java.lang.String r6 = "user"
            r0 = r22
            r1 = r6
            org.json.JSONObject r9 = r0.getJSONObject(r1)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            java.lang.String r6 = "root_in_reply_to_status_text"
            r0 = r22
            r1 = r6
            java.lang.String r6 = r0.getString(r1)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            java.lang.String r7 = "null"
            boolean r7 = r6.equals(r7)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            if (r7 != 0) goto L_0x08f8
            cn.com.mzba.model.Status r25 = new cn.com.mzba.model.Status     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            r25.<init>()     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            r7 = 1
            r8.setRetweetedStatus(r7)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            java.lang.String r7 = "root_in_reply_to_user_name"
            r0 = r22
            r1 = r7
            java.lang.String r7 = r0.getString(r1)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            java.lang.String r10 = "root_in_reply_to_user_id"
            r0 = r22
            r1 = r10
            java.lang.String r10 = r0.getString(r1)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            r0 = r25
            r1 = r10
            r0.setId(r1)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            java.lang.String r12 = "@"
            r10.<init>(r12)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            java.lang.StringBuilder r7 = r10.append(r7)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            java.lang.String r10 = ":"
            java.lang.StringBuilder r7 = r7.append(r10)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            java.lang.StringBuilder r6 = r7.append(r6)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            java.lang.String r6 = r6.toString()     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            r0 = r25
            r1 = r6
            r0.setText(r1)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            r0 = r8
            r1 = r25
            r0.setStatus(r1)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
        L_0x08f8:
            r6 = r25
            java.lang.String r25 = "id"
            r0 = r22
            r1 = r25
            java.lang.String r25 = r0.getString(r1)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            java.lang.String r7 = cn.com.mzba.service.StatusHttpUtils.TAG     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            java.lang.String r12 = "id:"
            r10.<init>(r12)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            r0 = r10
            r1 = r25
            java.lang.StringBuilder r10 = r0.append(r1)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            java.lang.String r10 = r10.toString()     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            android.util.Log.i(r7, r10)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            r5[r24] = r25     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            java.lang.String r7 = "id"
            java.lang.String r13 = r9.getString(r7)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            java.lang.String r7 = "screen_name"
            java.lang.String r14 = r9.getString(r7)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            java.lang.String r7 = "profile_image_url"
            java.lang.String r12 = r9.getString(r7)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            java.lang.String r7 = "created_at"
            r0 = r22
            r1 = r7
            java.lang.String r10 = r0.getString(r1)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            java.lang.String r7 = "text"
            r0 = r22
            r1 = r7
            java.lang.String r9 = r0.getString(r1)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            java.lang.String r7 = "source"
            r0 = r22
            r1 = r7
            java.lang.String r7 = r0.getString(r1)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            r0 = r8
            r1 = r25
            r0.setId(r1)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            r8.setText(r9)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            r8.setCreatedAt(r10)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            java.lang.StringBuilder r25 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            java.lang.String r9 = "来自："
            r0 = r25
            r1 = r9
            r0.<init>(r1)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            r0 = r25
            r1 = r7
            java.lang.StringBuilder r25 = r0.append(r1)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            java.lang.String r25 = r25.toString()     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            r0 = r8
            r1 = r25
            r0.setSource(r1)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            r0 = r8
            r1 = r21
            r0.setWeiboId(r1)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            r15.setId(r13)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            r15.setProfileImageUrl(r12)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            r15.setScreenName(r14)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            r8.setUser(r15)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            r0 = r8
            r1 = r21
            r0.setWeiboId(r1)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            java.lang.String r25 = "comments_count"
            r0 = r22
            r1 = r25
            java.lang.String r25 = r0.getString(r1)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            r0 = r8
            r1 = r25
            r0.setComments(r1)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            java.lang.String r25 = "retweet_count"
            r0 = r22
            r1 = r25
            java.lang.String r25 = r0.getString(r1)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            r0 = r8
            r1 = r25
            r0.setRt(r1)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            java.lang.String r25 = "cursor_id"
            r0 = r22
            r1 = r25
            java.lang.String r22 = r0.getString(r1)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            r0 = r8
            r1 = r22
            r0.setCursorId(r1)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            r11.add(r8)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            r22 = r6
        L_0x09be:
            int r22 = r24 + 1
            r24 = r22
            goto L_0x087c
        L_0x09c4:
            r22 = move-exception
            r20 = r22
            r22 = r23
            r23 = r20
        L_0x09cb:
            r23.printStackTrace()
            goto L_0x080c
        L_0x09d0:
            r22 = move-exception
            r20 = r22
            r22 = r23
            r23 = r20
        L_0x09d7:
            r23.printStackTrace()
            goto L_0x080c
        L_0x09dc:
            r7 = 0
            r0 = r22
            r1 = r6
            r2 = r7
            r3 = r24
            r0.append(r1, r2, r3)     // Catch:{ IOException -> 0x09e8, JSONException -> 0x09ee }
            goto L_0x084c
        L_0x09e8:
            r21 = move-exception
            r21.printStackTrace()
            goto L_0x010e
        L_0x09ee:
            r21 = move-exception
            r21.printStackTrace()
            goto L_0x010e
        L_0x09f4:
            r23 = move-exception
            goto L_0x09d7
        L_0x09f6:
            r23 = move-exception
            goto L_0x09cb
        L_0x09f8:
            r23 = move-exception
            goto L_0x0681
        L_0x09fb:
            r23 = move-exception
            goto L_0x0677
        L_0x09fe:
            r22 = r25
            goto L_0x09be
        L_0x0a01:
            r22 = r25
            goto L_0x079c
        L_0x0a05:
            r10 = r22
            goto L_0x047b
        L_0x0a09:
            r6 = r22
            goto L_0x03ef
        L_0x0a0d:
            r22 = r10
            goto L_0x0215
        L_0x0a11:
            r20 = r6
            r6 = r25
            r25 = r20
            goto L_0x01bf
        L_0x0a19:
            r22 = r25
            goto L_0x0266
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.com.mzba.service.StatusHttpUtils.getFriendTimeLines(java.lang.String, java.lang.String, int, int, java.lang.String):java.util.List");
    }

    /* JADX INFO: Multiple debug info for r8v6 org.apache.http.HttpResponse: [D('params' java.util.List<org.apache.http.message.BasicNameValuePair>), D('response' org.apache.http.HttpResponse)] */
    /* JADX INFO: Multiple debug info for r8v12 java.util.ArrayList: [D('weiboId' java.lang.String), D('params' java.util.List<org.apache.http.message.BasicNameValuePair>)] */
    /* JADX INFO: Multiple debug info for r8v13 org.apache.http.HttpResponse: [D('params' java.util.List<org.apache.http.message.BasicNameValuePair>), D('response' org.apache.http.HttpResponse)] */
    /* JADX INFO: Multiple debug info for r8v24 org.json.JSONObject: [D('data' org.json.JSONObject), D('parameter' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r8v29 java.util.ArrayList: [D('weiboId' java.lang.String), D('params' java.util.List<org.apache.http.message.BasicNameValuePair>)] */
    /* JADX INFO: Multiple debug info for r8v30 org.apache.http.HttpResponse: [D('params' java.util.List<org.apache.http.message.BasicNameValuePair>), D('response' org.apache.http.HttpResponse)] */
    public static String updateStatus(List<String> weiboIds, String status) {
        String result = null;
        for (String weiboId : weiboIds) {
            if (weiboId.equals(SystemConfig.SINA_WEIBO)) {
                List<BasicNameValuePair> params = new ArrayList<>();
                params.add(new BasicNameValuePair(SqliteHelper.TABLE_STATUS, status));
                HttpResponse response = oauthSina.signRequest(userTokenSina, userTokenSinaSecret, "http://api.t.sina.com.cn/statuses/update.json", params);
                result = response != null ? 200 == response.getStatusLine().getStatusCode() ? SystemConfig.SUCCESS : SystemConfig.ERROR : SystemConfig.ERROR;
            } else if (weiboId.equals(SystemConfig.TENCENT_WEIBO)) {
                List<Parameter> params2 = new ArrayList<>();
                try {
                    params2.add(new Parameter("format", "json"));
                    params2.add(new Parameter("content", URLEncoder.encode(status, "UTF-8")));
                    params2.add(new Parameter("clientip", "127.0.0.1"));
                    String parameter = oauthTencent.getPostParameter(SystemConfig.HTTP_POST, "http://open.t.qq.com/api/t/add", userTokenTencent, userTokenTencentSecret, params2);
                    JSONObject data = new JSONObject(oauthTencent.httpPost(String.valueOf("http://open.t.qq.com/api/t/add") + "?" + parameter, parameter));
                    result = (data.getString("msg").equals("ok") || data.getJSONObject("data").getString("id") != null) ? SystemConfig.SUCCESS : SystemConfig.ERROR;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (weiboId.equals(SystemConfig.SOHU_WEIBO)) {
                List<BasicNameValuePair> params3 = new ArrayList<>();
                params3.add(new BasicNameValuePair(SqliteHelper.TABLE_STATUS, status));
                HttpResponse response2 = oauthSohu.signRequest(userTokenSohu, userTokenSohuSecret, "http://api.t.sohu.com/statuses/update.json", params3);
                result = response2 != null ? 200 == response2.getStatusLine().getStatusCode() ? SystemConfig.SUCCESS : SystemConfig.ERROR : SystemConfig.ERROR;
            } else if (weiboId.equals(SystemConfig.NETEASE_WEIBO)) {
                List<BasicNameValuePair> params4 = new ArrayList<>();
                params4.add(new BasicNameValuePair(SqliteHelper.TABLE_STATUS, status));
                HttpResponse response3 = oauthNetease.signRequest(userTokenNetease, userTokenNeteaseSecret, "http://api.t.163.com/statuses/update.json", params4);
                result = response3 != null ? 200 == response3.getStatusLine().getStatusCode() ? SystemConfig.SUCCESS : SystemConfig.ERROR : SystemConfig.ERROR;
            }
        }
        return result;
    }

    /* JADX INFO: Multiple debug info for r4v7 int: [D('parameter' java.lang.String), D('length' int)] */
    /* JADX INFO: Multiple debug info for r23v18 int: [D('httpClient' org.apache.commons.httpclient.HttpClient), D('statusCode' int)] */
    /* JADX INFO: Multiple debug info for r3v15 java.lang.String: [D('httpPost' org.apache.commons.httpclient.methods.PostMethod), D('response' java.lang.String)] */
    public static String uploadStatus(List<String> weiboIds, String status, File file) {
        String result = null;
        for (String weiboId : weiboIds) {
            if (weiboId.equals(SystemConfig.SINA_WEIBO)) {
                Log.i(TAG, "weiboId:" + weiboId);
                try {
                    result = oauthSina.uploadStatus(userTokenSina, userTokenSinaSecret, file, URLEncoder.encode(status, "UTF-8"), "http://api.t.sina.com.cn/statuses/upload.json");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else if (weiboId.equals(SystemConfig.TENCENT_WEIBO)) {
                ArrayList arrayList = new ArrayList();
                try {
                    arrayList.add(new Parameter("format", "json"));
                    arrayList.add(new Parameter("content", URLEncoder.encode(status, "UTF-8")));
                    arrayList.add(new Parameter("clientip", "127.0.0.1"));
                    String parameter = oauthTencent.getPostParameter(SystemConfig.HTTP_POST, "http://open.t.qq.com/api/t/add_pic", userTokenTencent, userTokenTencentSecret, arrayList);
                    HttpClient httpClient = new HttpClient();
                    PostMethod httpPost = new PostMethod(String.valueOf("http://open.t.qq.com/api/t/add_pic") + "?" + parameter);
                    List<Parameter> listParams = SignatureUtil.getQueryParameters(parameter);
                    Part[] parts = new Part[(listParams.size() + 1)];
                    int i = 0;
                    for (Parameter p : listParams) {
                        parts[i] = new StringPart(p.getName(), SignatureUtil.decode(p.getValue()), "UTF-8");
                        i++;
                    }
                    int i2 = i + 1;
                    parts[i] = new FilePart("pic", file.getName(), file, null, "UTF-8");
                    httpPost.setRequestEntity(new MultipartRequestEntity(parts, httpPost.getParams()));
                    if (httpClient.executeMethod(httpPost) == 200) {
                        String response = httpPost.getResponseBodyAsString();
                        Log.i(TAG, "result:" + response);
                        JSONObject jSONObject = new JSONObject(response);
                        result = (jSONObject.getString("msg").equals("ok") || jSONObject.getJSONObject("data").getString("id") != null) ? SystemConfig.SUCCESS : SystemConfig.ERROR;
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            } else if (weiboId.equals(SystemConfig.SOHU_WEIBO)) {
                try {
                    result = oauthSohu.uploadStatus(userTokenSohu, userTokenSohuSecret, file, URLEncoder.encode(status, "UTF-8"), "http://api.t.sohu.com/statuses/upload.json");
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
            } else if (weiboId.equals(SystemConfig.NETEASE_WEIBO)) {
                try {
                    result = oauthNetease.uploadStatusForNetease(userTokenNetease, userTokenNeteaseSecret, file, status, "http://api.t.163.com/statuses/upload.json");
                } catch (Exception e4) {
                    e4.printStackTrace();
                }
            }
        }
        return result;
    }

    public static String deleteStatus(String weiboId, String id) {
        if (weiboId.equals(SystemConfig.SINA_WEIBO)) {
            List<BasicNameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("id", id));
            HttpResponse response = oauthSina.signRequest(userTokenSina, userTokenSinaSecret, "http://api.t.sina.com.cn/statuses/destroy/:id.json", params);
            if (response == null || 200 != response.getStatusLine().getStatusCode()) {
                return SystemConfig.ERROR;
            }
            return SystemConfig.SUCCESS;
        } else if (weiboId.equals(SystemConfig.TENCENT_WEIBO)) {
            List<Parameter> params2 = new ArrayList<>();
            try {
                params2.add(new Parameter("format", "json"));
                params2.add(new Parameter("id", id));
                String parameter = oauthTencent.getPostParameter(SystemConfig.HTTP_POST, "http://open.t.qq.com/api/t/del", userTokenTencent, userTokenTencentSecret, params2);
                JSONObject data = new JSONObject(oauthTencent.httpPost(String.valueOf("http://open.t.qq.com/api/t/del") + "?" + parameter, parameter));
                if (data.getString("msg").equals("ok") || data.getJSONObject("data").getString("id") != null) {
                    return SystemConfig.SUCCESS;
                }
                return SystemConfig.ERROR;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else if (weiboId.equals(SystemConfig.SOHU_WEIBO)) {
            List<BasicNameValuePair> params3 = new ArrayList<>();
            params3.add(new BasicNameValuePair("id", id));
            HttpResponse response2 = oauthSohu.signRequest(userTokenSohu, userTokenSohuSecret, "http://api.t.sohu.com/statuses/destroy/" + id + ".json", params3);
            if (response2 == null || 200 != response2.getStatusLine().getStatusCode()) {
                return SystemConfig.ERROR;
            }
            return SystemConfig.SUCCESS;
        } else if (!weiboId.equals(SystemConfig.NETEASE_WEIBO)) {
            return null;
        } else {
            List<BasicNameValuePair> params4 = new ArrayList<>();
            params4.add(new BasicNameValuePair("id", id));
            HttpResponse response3 = oauthNetease.signRequest(userTokenNetease, userTokenNeteaseSecret, "http://api.t.163.com/statuses/destroy/" + id + ".json", params4);
            if (response3 == null || 200 != response3.getStatusLine().getStatusCode()) {
                return SystemConfig.ERROR;
            }
            return SystemConfig.SUCCESS;
        }
    }

    /* JADX INFO: Multiple debug info for r21v9 java.lang.String: [D('builder' java.lang.StringBuilder), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r25v2 java.lang.String[]: [D('response' java.net.HttpURLConnection), D('idArray' java.lang.String[])] */
    /* JADX INFO: Multiple debug info for r23v6 int: [D('i' int), D('len' int)] */
    /* JADX INFO: Multiple debug info for r8v1 java.lang.String: [D('user' org.json.JSONObject), D('text' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r22v25 java.lang.String: [D('builder' java.lang.StringBuilder), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r5v16 java.lang.String[]: [D('response' java.net.HttpURLConnection), D('idArray' java.lang.String[])] */
    /* JADX INFO: Multiple debug info for r24v24 int: [D('i' int), D('len' int)] */
    /* JADX INFO: Multiple debug info for r6v24 cn.com.mzba.model.Status: [D('transmitId' java.lang.String), D('rtStatus' cn.com.mzba.model.Status)] */
    /* JADX INFO: Multiple debug info for r22v40 java.lang.String: [D('data' org.json.JSONObject), D('thumbnail_pic' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r22v50 java.lang.String: [D('parameter' java.lang.String), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r25v16 java.lang.String: [D('info' org.json.JSONObject), D('redirectCount' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r24v44 org.json.JSONObject: [D('type' int), D('source' org.json.JSONObject)] */
    /* JADX INFO: Multiple debug info for r24v45 java.lang.String: [D('rtName' java.lang.String), D('source' org.json.JSONObject)] */
    /* JADX INFO: Multiple debug info for r5v21 org.apache.http.HttpResponse: [D('response' org.apache.http.HttpResponse), D('params' java.util.List<org.apache.http.message.BasicNameValuePair>)] */
    /* JADX INFO: Multiple debug info for r22v92 java.lang.String: [D('builder' java.lang.StringBuilder), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r5v22 java.lang.String[]: [D('response' org.apache.http.HttpResponse), D('idArray' java.lang.String[])] */
    /* JADX INFO: Multiple debug info for r22v100 int: [D('rtStatus' cn.com.mzba.model.Status), D('i' int)] */
    /* JADX INFO: Multiple debug info for r22v106 java.lang.String: [D('data' org.json.JSONObject), D('thumbnail_pic' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r6v49 'rtStatus'  cn.com.mzba.model.Status: [D('rtStatus' cn.com.mzba.model.Status), D('transmitText' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r7v38 java.lang.String: [D('user_retweeted' org.json.JSONObject), D('transmitUserName' java.lang.String)] */
    /* JADX WARN: Type inference failed for: r22v11, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x061f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List<cn.com.mzba.model.Status> getUserTimeLines(java.lang.String r21, java.lang.String r22, int r23, int r24, java.lang.String r25) {
        /*
            java.util.ArrayList r11 = new java.util.ArrayList
            r11.<init>()
            java.lang.String r5 = "sina_weibo"
            r0 = r21
            r1 = r5
            boolean r5 = r0.equals(r1)
            if (r5 == 0) goto L_0x02a0
            java.lang.String r7 = "http://api.t.sina.com.cn/statuses/user_timeline.json"
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            org.apache.http.message.BasicNameValuePair r6 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r8 = "user_id"
            r0 = r6
            r1 = r8
            r2 = r22
            r0.<init>(r1, r2)
            r5.add(r6)
            org.apache.http.message.BasicNameValuePair r22 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r6 = "page"
            java.lang.String r23 = java.lang.String.valueOf(r23)
            r0 = r22
            r1 = r6
            r2 = r23
            r0.<init>(r1, r2)
            r0 = r5
            r1 = r22
            r0.add(r1)
            org.apache.http.message.BasicNameValuePair r22 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r23 = "count"
            java.lang.String r24 = java.lang.String.valueOf(r24)
            r22.<init>(r23, r24)
            r0 = r5
            r1 = r22
            r0.add(r1)
            org.apache.http.message.BasicNameValuePair r22 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r23 = "since_id"
            r0 = r22
            r1 = r23
            r2 = r25
            r0.<init>(r1, r2)
            r0 = r5
            r1 = r22
            r0.add(r1)
            cn.com.mzba.oauth.OAuth r22 = cn.com.mzba.service.StatusHttpUtils.oauthSina
            java.lang.String r23 = cn.com.mzba.service.StatusHttpUtils.userTokenSina
            java.lang.String r24 = cn.com.mzba.service.StatusHttpUtils.userTokenSinaSecret
            r0 = r22
            r1 = r23
            r2 = r24
            r3 = r7
            r4 = r5
            org.apache.http.HttpResponse r5 = r0.signRequest(r1, r2, r3, r4)
            if (r5 == 0) goto L_0x0107
            r22 = 200(0xc8, float:2.8E-43)
            org.apache.http.StatusLine r23 = r5.getStatusLine()
            int r23 = r23.getStatusCode()
            r0 = r22
            r1 = r23
            if (r0 != r1) goto L_0x0107
            org.apache.http.HttpEntity r22 = r5.getEntity()     // Catch:{ Exception -> 0x0113 }
            java.io.InputStream r23 = r22.getContent()     // Catch:{ Exception -> 0x0113 }
            java.io.BufferedReader r25 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0113 }
            java.io.InputStreamReader r22 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0113 }
            r22.<init>(r23)     // Catch:{ Exception -> 0x0113 }
            r0 = r25
            r1 = r22
            r0.<init>(r1)     // Catch:{ Exception -> 0x0113 }
            java.lang.StringBuilder r22 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0113 }
            org.apache.http.HttpEntity r24 = r5.getEntity()     // Catch:{ Exception -> 0x0113 }
            long r6 = r24.getContentLength()     // Catch:{ Exception -> 0x0113 }
            r0 = r6
            int r0 = (int) r0     // Catch:{ Exception -> 0x0113 }
            r24 = r0
            r0 = r22
            r1 = r24
            r0.<init>(r1)     // Catch:{ Exception -> 0x0113 }
            r24 = 4000(0xfa0, float:5.605E-42)
            r0 = r24
            char[] r0 = new char[r0]     // Catch:{ Exception -> 0x0113 }
            r6 = r0
            r24 = 0
        L_0x00b7:
            r0 = r25
            r1 = r6
            int r24 = r0.read(r1)     // Catch:{ Exception -> 0x0113 }
            r7 = -1
            r0 = r24
            r1 = r7
            if (r0 != r1) goto L_0x0108
            r25.close()     // Catch:{ Exception -> 0x0113 }
            r23.close()     // Catch:{ Exception -> 0x0113 }
            java.lang.String r22 = r22.toString()     // Catch:{ Exception -> 0x0113 }
            org.apache.http.HttpEntity r23 = r5.getEntity()     // Catch:{ Exception -> 0x0113 }
            r23.consumeContent()     // Catch:{ Exception -> 0x0113 }
            org.json.JSONArray r23 = new org.json.JSONArray     // Catch:{ Exception -> 0x0113 }
            r0 = r23
            r1 = r22
            r0.<init>(r1)     // Catch:{ Exception -> 0x0113 }
            int r22 = r23.length()     // Catch:{ Exception -> 0x0113 }
            r0 = r22
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ Exception -> 0x0113 }
            r5 = r0
            r22 = 0
            r24 = r22
        L_0x00eb:
            int r22 = r23.length()     // Catch:{ Exception -> 0x0113 }
            r0 = r24
            r1 = r22
            if (r0 < r1) goto L_0x0118
            java.lang.String r22 = cn.com.mzba.service.StringUtil.arrayToString(r5)     // Catch:{ Exception -> 0x0113 }
            java.util.List r22 = getCounts(r21, r22)     // Catch:{ Exception -> 0x0113 }
            java.util.Iterator r25 = r11.iterator()     // Catch:{ Exception -> 0x0113 }
        L_0x0101:
            boolean r23 = r25.hasNext()     // Catch:{ Exception -> 0x0113 }
            if (r23 != 0) goto L_0x0259
        L_0x0107:
            return r11
        L_0x0108:
            r7 = 0
            r0 = r22
            r1 = r6
            r2 = r7
            r3 = r24
            r0.append(r1, r2, r3)     // Catch:{ Exception -> 0x0113 }
            goto L_0x00b7
        L_0x0113:
            r21 = move-exception
            r21.printStackTrace()
            goto L_0x0107
        L_0x0118:
            cn.com.mzba.model.Status r8 = new cn.com.mzba.model.Status     // Catch:{ Exception -> 0x0113 }
            r8.<init>()     // Catch:{ Exception -> 0x0113 }
            cn.com.mzba.model.User r16 = new cn.com.mzba.model.User     // Catch:{ Exception -> 0x0113 }
            r16.<init>()     // Catch:{ Exception -> 0x0113 }
            r25 = 0
            org.json.JSONObject r22 = r23.getJSONObject(r24)     // Catch:{ Exception -> 0x0113 }
            if (r22 == 0) goto L_0x0a86
            java.lang.String r6 = "user"
            r0 = r22
            r1 = r6
            org.json.JSONObject r10 = r0.getJSONObject(r1)     // Catch:{ Exception -> 0x0113 }
            java.lang.String r6 = ""
            java.lang.String r7 = "retweeted_status"
            r0 = r22
            r1 = r7
            boolean r7 = r0.has(r1)     // Catch:{ Exception -> 0x0113 }
            if (r7 == 0) goto L_0x0a7e
            cn.com.mzba.model.Status r6 = new cn.com.mzba.model.Status     // Catch:{ Exception -> 0x0113 }
            r6.<init>()     // Catch:{ Exception -> 0x0113 }
            r25 = 1
            r0 = r8
            r1 = r25
            r0.setRetweetedStatus(r1)     // Catch:{ Exception -> 0x0113 }
            java.lang.String r25 = "retweeted_status"
            r0 = r22
            r1 = r25
            org.json.JSONObject r25 = r0.getJSONObject(r1)     // Catch:{ Exception -> 0x0113 }
            java.lang.String r7 = "user"
            r0 = r25
            r1 = r7
            org.json.JSONObject r7 = r0.getJSONObject(r1)     // Catch:{ Exception -> 0x0113 }
            java.lang.String r9 = "screen_name"
            java.lang.String r7 = r7.getString(r9)     // Catch:{ Exception -> 0x0113 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0113 }
            java.lang.String r12 = "@"
            r9.<init>(r12)     // Catch:{ Exception -> 0x0113 }
            java.lang.StringBuilder r7 = r9.append(r7)     // Catch:{ Exception -> 0x0113 }
            java.lang.String r9 = ":"
            java.lang.StringBuilder r7 = r7.append(r9)     // Catch:{ Exception -> 0x0113 }
            java.lang.String r9 = "text"
            r0 = r25
            r1 = r9
            java.lang.String r9 = r0.getString(r1)     // Catch:{ Exception -> 0x0113 }
            java.lang.StringBuilder r7 = r7.append(r9)     // Catch:{ Exception -> 0x0113 }
            java.lang.String r9 = r7.toString()     // Catch:{ Exception -> 0x0113 }
            java.lang.String r7 = ""
            java.lang.String r12 = "thumbnail_pic"
            r0 = r25
            r1 = r12
            boolean r12 = r0.has(r1)     // Catch:{ Exception -> 0x0113 }
            if (r12 == 0) goto L_0x019e
            java.lang.String r7 = "thumbnail_pic"
            r0 = r25
            r1 = r7
            java.lang.String r7 = r0.getString(r1)     // Catch:{ Exception -> 0x0113 }
        L_0x019e:
            java.lang.String r12 = "id"
            r0 = r25
            r1 = r12
            java.lang.String r25 = r0.getString(r1)     // Catch:{ Exception -> 0x0113 }
            r0 = r6
            r1 = r25
            r0.setId(r1)     // Catch:{ Exception -> 0x0113 }
            r6.setText(r9)     // Catch:{ Exception -> 0x0113 }
            r6.setThumbnailPic(r7)     // Catch:{ Exception -> 0x0113 }
            r8.setStatus(r6)     // Catch:{ Exception -> 0x0113 }
            r25 = r9
        L_0x01b8:
            java.lang.String r25 = "id"
            r0 = r22
            r1 = r25
            java.lang.String r25 = r0.getString(r1)     // Catch:{ Exception -> 0x0113 }
            r5[r24] = r25     // Catch:{ Exception -> 0x0113 }
            java.lang.String r7 = "id"
            java.lang.String r14 = r10.getString(r7)     // Catch:{ Exception -> 0x0113 }
            java.lang.String r7 = "screen_name"
            java.lang.String r15 = r10.getString(r7)     // Catch:{ Exception -> 0x0113 }
            java.lang.String r7 = "profile_image_url"
            java.lang.String r13 = r10.getString(r7)     // Catch:{ Exception -> 0x0113 }
            java.lang.String r7 = "created_at"
            r0 = r22
            r1 = r7
            java.lang.String r12 = r0.getString(r1)     // Catch:{ Exception -> 0x0113 }
            java.lang.String r7 = "text"
            r0 = r22
            r1 = r7
            java.lang.String r9 = r0.getString(r1)     // Catch:{ Exception -> 0x0113 }
            java.lang.String r7 = "source"
            r0 = r22
            r1 = r7
            java.lang.String r7 = r0.getString(r1)     // Catch:{ Exception -> 0x0113 }
            java.lang.String r10 = ""
            java.lang.String r17 = "thumbnail_pic"
            r0 = r22
            r1 = r17
            boolean r17 = r0.has(r1)     // Catch:{ Exception -> 0x0113 }
            if (r17 == 0) goto L_0x0a7a
            java.lang.String r10 = "thumbnail_pic"
            r0 = r22
            r1 = r10
            java.lang.String r22 = r0.getString(r1)     // Catch:{ Exception -> 0x0113 }
            r0 = r8
            r1 = r22
            r0.setThumbnailPic(r1)     // Catch:{ Exception -> 0x0113 }
        L_0x020e:
            r0 = r8
            r1 = r25
            r0.setId(r1)     // Catch:{ Exception -> 0x0113 }
            r8.setText(r9)     // Catch:{ Exception -> 0x0113 }
            r8.setCreatedAt(r12)     // Catch:{ Exception -> 0x0113 }
            java.lang.StringBuilder r22 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0113 }
            java.lang.String r25 = "来自："
            r0 = r22
            r1 = r25
            r0.<init>(r1)     // Catch:{ Exception -> 0x0113 }
            r0 = r22
            r1 = r7
            java.lang.StringBuilder r22 = r0.append(r1)     // Catch:{ Exception -> 0x0113 }
            java.lang.String r22 = r22.toString()     // Catch:{ Exception -> 0x0113 }
            r0 = r8
            r1 = r22
            r0.setSource(r1)     // Catch:{ Exception -> 0x0113 }
            r0 = r16
            r1 = r14
            r0.setId(r1)     // Catch:{ Exception -> 0x0113 }
            r0 = r16
            r1 = r13
            r0.setProfileImageUrl(r1)     // Catch:{ Exception -> 0x0113 }
            r0 = r16
            r1 = r15
            r0.setScreenName(r1)     // Catch:{ Exception -> 0x0113 }
            r0 = r8
            r1 = r16
            r0.setUser(r1)     // Catch:{ Exception -> 0x0113 }
            r11.add(r8)     // Catch:{ Exception -> 0x0113 }
            r22 = r6
        L_0x0253:
            int r22 = r24 + 1
            r24 = r22
            goto L_0x00eb
        L_0x0259:
            java.lang.Object r24 = r25.next()     // Catch:{ Exception -> 0x0113 }
            cn.com.mzba.model.Status r24 = (cn.com.mzba.model.Status) r24     // Catch:{ Exception -> 0x0113 }
            java.util.Iterator r5 = r22.iterator()     // Catch:{ Exception -> 0x0113 }
        L_0x0263:
            boolean r23 = r5.hasNext()     // Catch:{ Exception -> 0x0113 }
            if (r23 == 0) goto L_0x0101
            java.lang.Object r23 = r5.next()     // Catch:{ Exception -> 0x0113 }
            cn.com.mzba.model.StatusCount r23 = (cn.com.mzba.model.StatusCount) r23     // Catch:{ Exception -> 0x0113 }
            java.lang.String r6 = r23.getWeiboId()     // Catch:{ Exception -> 0x0113 }
            r0 = r21
            r1 = r6
            boolean r6 = r0.equals(r1)     // Catch:{ Exception -> 0x0113 }
            if (r6 == 0) goto L_0x0263
            java.lang.String r6 = r24.getId()     // Catch:{ Exception -> 0x0113 }
            java.lang.String r7 = r23.getId()     // Catch:{ Exception -> 0x0113 }
            boolean r6 = r6.equals(r7)     // Catch:{ Exception -> 0x0113 }
            if (r6 == 0) goto L_0x0263
            java.lang.String r6 = r23.getComments()     // Catch:{ Exception -> 0x0113 }
            r0 = r24
            r1 = r6
            r0.setComments(r1)     // Catch:{ Exception -> 0x0113 }
            java.lang.String r23 = r23.getRt()     // Catch:{ Exception -> 0x0113 }
            r0 = r24
            r1 = r23
            r0.setRt(r1)     // Catch:{ Exception -> 0x0113 }
            goto L_0x0263
        L_0x02a0:
            java.lang.String r5 = "tencent_weibo"
            r0 = r21
            r1 = r5
            boolean r5 = r0.equals(r1)
            if (r5 == 0) goto L_0x0576
            java.lang.String r7 = ""
            java.util.ArrayList r10 = new java.util.ArrayList
            r10.<init>()
            if (r22 == 0) goto L_0x02c4
            cn.com.mzba.db.UserInfo r23 = cn.com.mzba.db.ConfigHelper.USERINFO_TENCENT
            java.lang.String r23 = r23.getUserId()
            r0 = r23
            r1 = r22
            boolean r23 = r0.equals(r1)
            if (r23 == 0) goto L_0x050d
        L_0x02c4:
            java.lang.String r7 = "http://open.t.qq.com/api/statuses/broadcast_timeline"
            cn.com.mzba.oauth.Parameter r22 = new cn.com.mzba.oauth.Parameter
            java.lang.String r23 = "Reqnum"
            java.lang.String r24 = java.lang.String.valueOf(r24)
            r22.<init>(r23, r24)
            r0 = r10
            r1 = r22
            r0.add(r1)
            cn.com.mzba.oauth.Parameter r22 = new cn.com.mzba.oauth.Parameter
            java.lang.String r23 = "type"
            r24 = 0
            java.lang.String r24 = java.lang.String.valueOf(r24)
            r22.<init>(r23, r24)
            r0 = r10
            r1 = r22
            r0.add(r1)
            cn.com.mzba.oauth.Parameter r22 = new cn.com.mzba.oauth.Parameter
            java.lang.String r23 = "Pageflag"
            r24 = 0
            java.lang.String r24 = java.lang.String.valueOf(r24)
            r22.<init>(r23, r24)
            r0 = r10
            r1 = r22
            r0.add(r1)
            cn.com.mzba.oauth.Parameter r22 = new cn.com.mzba.oauth.Parameter
            java.lang.String r23 = "fotmat"
            java.lang.String r24 = "json"
            r22.<init>(r23, r24)
            r0 = r10
            r1 = r22
            r0.add(r1)
        L_0x030c:
            cn.com.mzba.oauth.OAuth r5 = cn.com.mzba.service.StatusHttpUtils.oauthTencent     // Catch:{ Exception -> 0x0570 }
            java.lang.String r6 = "GET"
            java.lang.String r8 = cn.com.mzba.service.StatusHttpUtils.userTokenTencent     // Catch:{ Exception -> 0x0570 }
            java.lang.String r9 = cn.com.mzba.service.StatusHttpUtils.userTokenTencentSecret     // Catch:{ Exception -> 0x0570 }
            java.lang.String r22 = r5.getPostParameter(r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x0570 }
            cn.com.mzba.oauth.OAuth r23 = cn.com.mzba.service.StatusHttpUtils.oauthTencent     // Catch:{ Exception -> 0x0570 }
            r0 = r23
            r1 = r7
            r2 = r22
            java.lang.String r22 = r0.httpGet(r1, r2)     // Catch:{ Exception -> 0x0570 }
            if (r22 == 0) goto L_0x0107
            java.lang.String r23 = cn.com.mzba.service.StatusHttpUtils.TAG     // Catch:{ Exception -> 0x0570 }
            java.lang.StringBuilder r24 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0570 }
            java.lang.String r25 = "userStatus:"
            r24.<init>(r25)     // Catch:{ Exception -> 0x0570 }
            r0 = r24
            r1 = r22
            java.lang.StringBuilder r24 = r0.append(r1)     // Catch:{ Exception -> 0x0570 }
            java.lang.String r24 = r24.toString()     // Catch:{ Exception -> 0x0570 }
            android.util.Log.i(r23, r24)     // Catch:{ Exception -> 0x0570 }
            org.json.JSONObject r23 = new org.json.JSONObject     // Catch:{ Exception -> 0x0570 }
            r0 = r23
            r1 = r22
            r0.<init>(r1)     // Catch:{ Exception -> 0x0570 }
            java.lang.String r22 = "data"
            r0 = r23
            r1 = r22
            org.json.JSONObject r22 = r0.getJSONObject(r1)     // Catch:{ Exception -> 0x0570 }
            java.lang.String r23 = "info"
            org.json.JSONArray r5 = r22.getJSONArray(r23)     // Catch:{ Exception -> 0x0570 }
            r22 = 0
            r23 = r22
        L_0x035a:
            int r22 = r5.length()     // Catch:{ Exception -> 0x0570 }
            r0 = r23
            r1 = r22
            if (r0 >= r1) goto L_0x0107
            cn.com.mzba.model.Status r8 = new cn.com.mzba.model.Status     // Catch:{ Exception -> 0x0570 }
            r8.<init>()     // Catch:{ Exception -> 0x0570 }
            cn.com.mzba.model.User r16 = new cn.com.mzba.model.User     // Catch:{ Exception -> 0x0570 }
            r16.<init>()     // Catch:{ Exception -> 0x0570 }
            r22 = 0
            r0 = r5
            r1 = r23
            org.json.JSONObject r25 = r0.getJSONObject(r1)     // Catch:{ Exception -> 0x0570 }
            if (r25 == 0) goto L_0x0507
            java.lang.String r24 = "type"
            r0 = r25
            r1 = r24
            int r24 = r0.getInt(r1)     // Catch:{ Exception -> 0x0570 }
            r6 = 2
            r0 = r24
            r1 = r6
            if (r0 != r1) goto L_0x0a76
            java.lang.String r22 = "source"
            r0 = r25
            r1 = r22
            org.json.JSONObject r24 = r0.getJSONObject(r1)     // Catch:{ Exception -> 0x0570 }
            cn.com.mzba.model.Status r6 = new cn.com.mzba.model.Status     // Catch:{ Exception -> 0x0570 }
            r6.<init>()     // Catch:{ Exception -> 0x0570 }
            r22 = 1
            r0 = r8
            r1 = r22
            r0.setRetweetedStatus(r1)     // Catch:{ Exception -> 0x0570 }
            java.lang.String r22 = "text"
            r0 = r24
            r1 = r22
            java.lang.String r7 = r0.getString(r1)     // Catch:{ Exception -> 0x0570 }
            java.lang.String r22 = "id"
            r0 = r24
            r1 = r22
            java.lang.String r22 = r0.getString(r1)     // Catch:{ Exception -> 0x0570 }
            java.lang.String r9 = "nick"
            r0 = r24
            r1 = r9
            java.lang.String r24 = r0.getString(r1)     // Catch:{ Exception -> 0x0570 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0570 }
            java.lang.String r10 = "@"
            r9.<init>(r10)     // Catch:{ Exception -> 0x0570 }
            r0 = r9
            r1 = r24
            java.lang.StringBuilder r24 = r0.append(r1)     // Catch:{ Exception -> 0x0570 }
            java.lang.String r9 = ":"
            r0 = r24
            r1 = r9
            java.lang.StringBuilder r24 = r0.append(r1)     // Catch:{ Exception -> 0x0570 }
            r0 = r24
            r1 = r7
            java.lang.StringBuilder r24 = r0.append(r1)     // Catch:{ Exception -> 0x0570 }
            java.lang.String r24 = r24.toString()     // Catch:{ Exception -> 0x0570 }
            r0 = r6
            r1 = r24
            r0.setText(r1)     // Catch:{ Exception -> 0x0570 }
            r0 = r6
            r1 = r22
            r0.setId(r1)     // Catch:{ Exception -> 0x0570 }
            r8.setStatus(r6)     // Catch:{ Exception -> 0x0570 }
        L_0x03ee:
            java.lang.String r22 = "id"
            r0 = r25
            r1 = r22
            java.lang.String r24 = r0.getString(r1)     // Catch:{ Exception -> 0x0570 }
            java.lang.String r22 = "name"
            r0 = r25
            r1 = r22
            java.lang.String r14 = r0.getString(r1)     // Catch:{ Exception -> 0x0570 }
            java.lang.String r22 = "nick"
            r0 = r25
            r1 = r22
            java.lang.String r15 = r0.getString(r1)     // Catch:{ Exception -> 0x0570 }
            java.lang.String r22 = "head"
            r0 = r25
            r1 = r22
            java.lang.String r13 = r0.getString(r1)     // Catch:{ Exception -> 0x0570 }
            java.lang.String r22 = "timestamp"
            r0 = r25
            r1 = r22
            java.lang.String r12 = r0.getString(r1)     // Catch:{ Exception -> 0x0570 }
            java.lang.String r22 = "text"
            r0 = r25
            r1 = r22
            java.lang.String r9 = r0.getString(r1)     // Catch:{ Exception -> 0x0570 }
            java.lang.String r22 = "from"
            r0 = r25
            r1 = r22
            java.lang.String r7 = r0.getString(r1)     // Catch:{ Exception -> 0x0570 }
            java.lang.String r22 = "image"
            r0 = r25
            r1 = r22
            java.lang.String r22 = r0.getString(r1)     // Catch:{ Exception -> 0x0570 }
            if (r22 == 0) goto L_0x0a72
            java.lang.String r10 = "null"
            r0 = r22
            r1 = r10
            boolean r10 = r0.equals(r1)     // Catch:{ Exception -> 0x0570 }
            if (r10 != 0) goto L_0x0a72
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0570 }
            r17 = 2
            int r18 = r22.length()     // Catch:{ Exception -> 0x0570 }
            r19 = 2
            int r18 = r18 - r19
            r0 = r22
            r1 = r17
            r2 = r18
            java.lang.String r22 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x0570 }
            java.lang.String r22 = java.lang.String.valueOf(r22)     // Catch:{ Exception -> 0x0570 }
            r0 = r10
            r1 = r22
            r0.<init>(r1)     // Catch:{ Exception -> 0x0570 }
            java.lang.String r22 = "/120"
            r0 = r10
            r1 = r22
            java.lang.StringBuilder r22 = r0.append(r1)     // Catch:{ Exception -> 0x0570 }
            java.lang.String r22 = r22.toString()     // Catch:{ Exception -> 0x0570 }
            r10 = r22
        L_0x047a:
            java.lang.String r22 = "mcount"
            r0 = r25
            r1 = r22
            java.lang.String r22 = r0.getString(r1)     // Catch:{ Exception -> 0x0570 }
            java.lang.String r17 = "count"
            r0 = r25
            r1 = r17
            java.lang.String r25 = r0.getString(r1)     // Catch:{ Exception -> 0x0570 }
            r8.setThumbnailPic(r10)     // Catch:{ Exception -> 0x0570 }
            r0 = r8
            r1 = r24
            r0.setId(r1)     // Catch:{ Exception -> 0x0570 }
            r8.setText(r9)     // Catch:{ Exception -> 0x0570 }
            r8.setCreatedAt(r12)     // Catch:{ Exception -> 0x0570 }
            java.lang.StringBuilder r24 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0570 }
            java.lang.String r9 = "来自："
            r0 = r24
            r1 = r9
            r0.<init>(r1)     // Catch:{ Exception -> 0x0570 }
            r0 = r24
            r1 = r7
            java.lang.StringBuilder r24 = r0.append(r1)     // Catch:{ Exception -> 0x0570 }
            java.lang.String r24 = r24.toString()     // Catch:{ Exception -> 0x0570 }
            r0 = r8
            r1 = r24
            r0.setSource(r1)     // Catch:{ Exception -> 0x0570 }
            r0 = r8
            r1 = r21
            r0.setWeiboId(r1)     // Catch:{ Exception -> 0x0570 }
            r0 = r16
            r1 = r14
            r0.setId(r1)     // Catch:{ Exception -> 0x0570 }
            java.lang.StringBuilder r24 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0570 }
            java.lang.String r7 = java.lang.String.valueOf(r13)     // Catch:{ Exception -> 0x0570 }
            r0 = r24
            r1 = r7
            r0.<init>(r1)     // Catch:{ Exception -> 0x0570 }
            java.lang.String r7 = "/40"
            r0 = r24
            r1 = r7
            java.lang.StringBuilder r24 = r0.append(r1)     // Catch:{ Exception -> 0x0570 }
            java.lang.String r24 = r24.toString()     // Catch:{ Exception -> 0x0570 }
            r0 = r16
            r1 = r24
            r0.setProfileImageUrl(r1)     // Catch:{ Exception -> 0x0570 }
            r0 = r16
            r1 = r15
            r0.setScreenName(r1)     // Catch:{ Exception -> 0x0570 }
            r0 = r8
            r1 = r16
            r0.setUser(r1)     // Catch:{ Exception -> 0x0570 }
            r0 = r8
            r1 = r21
            r0.setWeiboId(r1)     // Catch:{ Exception -> 0x0570 }
            r0 = r8
            r1 = r22
            r0.setComments(r1)     // Catch:{ Exception -> 0x0570 }
            r0 = r8
            r1 = r25
            r0.setRt(r1)     // Catch:{ Exception -> 0x0570 }
            r11.add(r8)     // Catch:{ Exception -> 0x0570 }
            r22 = r6
        L_0x0507:
            int r22 = r23 + 1
            r23 = r22
            goto L_0x035a
        L_0x050d:
            java.lang.String r7 = "http://open.t.qq.com/api/statuses/user_timeline"
            cn.com.mzba.oauth.Parameter r23 = new cn.com.mzba.oauth.Parameter
            java.lang.String r25 = "Reqnum"
            java.lang.String r24 = java.lang.String.valueOf(r24)
            r0 = r23
            r1 = r25
            r2 = r24
            r0.<init>(r1, r2)
            r0 = r10
            r1 = r23
            r0.add(r1)
            cn.com.mzba.oauth.Parameter r23 = new cn.com.mzba.oauth.Parameter
            java.lang.String r24 = "type"
            r25 = 0
            java.lang.String r25 = java.lang.String.valueOf(r25)
            r23.<init>(r24, r25)
            r0 = r10
            r1 = r23
            r0.add(r1)
            cn.com.mzba.oauth.Parameter r23 = new cn.com.mzba.oauth.Parameter
            java.lang.String r24 = "Pageflag"
            r25 = 0
            java.lang.String r25 = java.lang.String.valueOf(r25)
            r23.<init>(r24, r25)
            r0 = r10
            r1 = r23
            r0.add(r1)
            cn.com.mzba.oauth.Parameter r23 = new cn.com.mzba.oauth.Parameter
            java.lang.String r24 = "fotmat"
            java.lang.String r25 = "json"
            r23.<init>(r24, r25)
            r0 = r10
            r1 = r23
            r0.add(r1)
            cn.com.mzba.oauth.Parameter r23 = new cn.com.mzba.oauth.Parameter
            java.lang.String r24 = "Name"
            r0 = r23
            r1 = r24
            r2 = r22
            r0.<init>(r1, r2)
            r0 = r10
            r1 = r23
            r0.add(r1)
            goto L_0x030c
        L_0x0570:
            r21 = move-exception
            r21.printStackTrace()
            goto L_0x0107
        L_0x0576:
            java.lang.String r5 = "sohu_weibo"
            r0 = r21
            r1 = r5
            boolean r5 = r0.equals(r1)
            if (r5 == 0) goto L_0x0821
            java.lang.String r7 = ""
            r5 = 0
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x06ec, IOException -> 0x06f6 }
            java.lang.String r8 = "?page="
            r6.<init>(r8)     // Catch:{ MalformedURLException -> 0x06ec, IOException -> 0x06f6 }
            r0 = r6
            r1 = r23
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x06ec, IOException -> 0x06f6 }
            java.lang.String r6 = "&count="
            r0 = r23
            r1 = r6
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x06ec, IOException -> 0x06f6 }
            java.lang.StringBuilder r23 = r23.append(r24)     // Catch:{ MalformedURLException -> 0x06ec, IOException -> 0x06f6 }
            java.lang.String r24 = "&since_id="
            java.lang.StringBuilder r23 = r23.append(r24)     // Catch:{ MalformedURLException -> 0x06ec, IOException -> 0x06f6 }
            r0 = r23
            r1 = r25
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x06ec, IOException -> 0x06f6 }
            java.lang.String r23 = r23.toString()     // Catch:{ MalformedURLException -> 0x06ec, IOException -> 0x06f6 }
            if (r22 == 0) goto L_0x0a6e
            java.lang.String r24 = ""
            r0 = r22
            r1 = r24
            boolean r24 = r0.equals(r1)     // Catch:{ MalformedURLException -> 0x06ec, IOException -> 0x06f6 }
            if (r24 != 0) goto L_0x0a6e
            java.lang.StringBuilder r24 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x06ec, IOException -> 0x06f6 }
            java.lang.String r23 = java.lang.String.valueOf(r23)     // Catch:{ MalformedURLException -> 0x06ec, IOException -> 0x06f6 }
            r0 = r24
            r1 = r23
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x06ec, IOException -> 0x06f6 }
            java.lang.String r23 = "&id="
            r0 = r24
            r1 = r23
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x06ec, IOException -> 0x06f6 }
            r0 = r23
            r1 = r22
            java.lang.StringBuilder r22 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x06ec, IOException -> 0x06f6 }
            java.lang.String r22 = r22.toString()     // Catch:{ MalformedURLException -> 0x06ec, IOException -> 0x06f6 }
        L_0x05e2:
            java.net.URL r23 = new java.net.URL     // Catch:{ MalformedURLException -> 0x06ec, IOException -> 0x06f6 }
            java.lang.StringBuilder r24 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x06ec, IOException -> 0x06f6 }
            java.lang.String r25 = java.lang.String.valueOf(r7)     // Catch:{ MalformedURLException -> 0x06ec, IOException -> 0x06f6 }
            r24.<init>(r25)     // Catch:{ MalformedURLException -> 0x06ec, IOException -> 0x06f6 }
            r0 = r24
            r1 = r22
            java.lang.StringBuilder r22 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x06ec, IOException -> 0x06f6 }
            java.lang.String r22 = r22.toString()     // Catch:{ MalformedURLException -> 0x06ec, IOException -> 0x06f6 }
            r0 = r23
            r1 = r22
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x06ec, IOException -> 0x06f6 }
            java.net.URLConnection r22 = r23.openConnection()     // Catch:{ MalformedURLException -> 0x06ec, IOException -> 0x06f6 }
            java.net.HttpURLConnection r22 = (java.net.HttpURLConnection) r22     // Catch:{ MalformedURLException -> 0x06ec, IOException -> 0x06f6 }
            java.lang.String r23 = "GET"
            r22.setRequestMethod(r23)     // Catch:{ MalformedURLException -> 0x0a5c, IOException -> 0x0a59 }
        L_0x060b:
            cn.com.mzba.oauth.OAuth r23 = cn.com.mzba.service.StatusHttpUtils.oauthSohu
            java.lang.String r24 = cn.com.mzba.service.StatusHttpUtils.userTokenSohu
            java.lang.String r25 = cn.com.mzba.service.StatusHttpUtils.userTokenSohuSecret
            r0 = r23
            r1 = r24
            r2 = r25
            r3 = r22
            java.net.HttpURLConnection r5 = r0.signRequest(r1, r2, r3)
            if (r5 == 0) goto L_0x0107
            r22 = 200(0xc8, float:2.8E-43)
            int r23 = r5.getResponseCode()     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            r0 = r22
            r1 = r23
            if (r0 != r1) goto L_0x0107
            java.io.InputStream r23 = r5.getInputStream()     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            java.io.BufferedReader r25 = new java.io.BufferedReader     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            java.io.InputStreamReader r22 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            r22.<init>(r23)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            r0 = r25
            r1 = r22
            r0.<init>(r1)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            java.lang.StringBuilder r22 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            int r24 = r5.getContentLength()     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            r0 = r22
            r1 = r24
            r0.<init>(r1)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            r24 = 4000(0xfa0, float:5.605E-42)
            r0 = r24
            char[] r0 = new char[r0]     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            r6 = r0
            r24 = 0
        L_0x0653:
            r0 = r25
            r1 = r6
            int r24 = r0.read(r1)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            r7 = -1
            r0 = r24
            r1 = r7
            if (r0 != r1) goto L_0x0700
            r25.close()     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            r23.close()     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            java.lang.String r22 = r22.toString()     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            r5.disconnect()     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            org.json.JSONArray r23 = new org.json.JSONArray     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            r0 = r23
            r1 = r22
            r0.<init>(r1)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            int r22 = r23.length()     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            r0 = r22
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            r5 = r0
            r22 = 0
            r24 = r22
        L_0x0683:
            int r22 = r23.length()     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            r0 = r24
            r1 = r22
            if (r0 < r1) goto L_0x0712
            java.lang.String r22 = cn.com.mzba.service.StringUtil.arrayToString(r5)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            java.util.List r22 = getCounts(r21, r22)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            java.util.Iterator r25 = r11.iterator()     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
        L_0x0699:
            boolean r23 = r25.hasNext()     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            if (r23 == 0) goto L_0x0107
            java.lang.Object r24 = r25.next()     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            cn.com.mzba.model.Status r24 = (cn.com.mzba.model.Status) r24     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            java.util.Iterator r5 = r22.iterator()     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
        L_0x06a9:
            boolean r23 = r5.hasNext()     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            if (r23 == 0) goto L_0x0699
            java.lang.Object r23 = r5.next()     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            cn.com.mzba.model.StatusCount r23 = (cn.com.mzba.model.StatusCount) r23     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            java.lang.String r6 = r23.getWeiboId()     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            r0 = r21
            r1 = r6
            boolean r6 = r0.equals(r1)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            if (r6 == 0) goto L_0x06a9
            java.lang.String r6 = r24.getId()     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            java.lang.String r7 = r23.getId()     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            boolean r6 = r6.equals(r7)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            if (r6 == 0) goto L_0x06a9
            java.lang.String r6 = r23.getComments()     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            r0 = r24
            r1 = r6
            r0.setComments(r1)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            java.lang.String r23 = r23.getRt()     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            r0 = r24
            r1 = r23
            r0.setRt(r1)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            goto L_0x06a9
        L_0x06e6:
            r21 = move-exception
            r21.printStackTrace()
            goto L_0x0107
        L_0x06ec:
            r22 = move-exception
            r23 = r22
            r22 = r5
        L_0x06f1:
            r23.printStackTrace()
            goto L_0x060b
        L_0x06f6:
            r22 = move-exception
            r23 = r22
            r22 = r5
        L_0x06fb:
            r23.printStackTrace()
            goto L_0x060b
        L_0x0700:
            r7 = 0
            r0 = r22
            r1 = r6
            r2 = r7
            r3 = r24
            r0.append(r1, r2, r3)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            goto L_0x0653
        L_0x070c:
            r21 = move-exception
            r21.printStackTrace()
            goto L_0x0107
        L_0x0712:
            cn.com.mzba.model.Status r8 = new cn.com.mzba.model.Status     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            r8.<init>()     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            cn.com.mzba.model.User r16 = new cn.com.mzba.model.User     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            r16.<init>()     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            r25 = 0
            org.json.JSONObject r22 = r23.getJSONObject(r24)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            if (r22 == 0) goto L_0x0a6a
            java.lang.String r6 = "user"
            r0 = r22
            r1 = r6
            org.json.JSONObject r10 = r0.getJSONObject(r1)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            java.lang.String r6 = "in_reply_to_status_text"
            r0 = r22
            r1 = r6
            java.lang.String r7 = r0.getString(r1)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            if (r7 == 0) goto L_0x077e
            cn.com.mzba.model.Status r25 = new cn.com.mzba.model.Status     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            r25.<init>()     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            r6 = 1
            r8.setRetweetedStatus(r6)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            java.lang.String r6 = "in_reply_to_screen_name"
            r0 = r22
            r1 = r6
            java.lang.String r9 = r0.getString(r1)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            java.lang.String r6 = "in_reply_to_status_id"
            r0 = r22
            r1 = r6
            java.lang.String r6 = r0.getString(r1)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            java.lang.String r13 = "@"
            r12.<init>(r13)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            java.lang.StringBuilder r9 = r12.append(r9)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            java.lang.String r12 = ":"
            java.lang.StringBuilder r9 = r9.append(r12)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            java.lang.StringBuilder r7 = r9.append(r7)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            java.lang.String r7 = r7.toString()     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            r0 = r25
            r1 = r7
            r0.setText(r1)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            r0 = r25
            r1 = r6
            r0.setId(r1)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            r0 = r8
            r1 = r25
            r0.setStatus(r1)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
        L_0x077e:
            r6 = r25
            java.lang.String r25 = "id"
            r0 = r22
            r1 = r25
            java.lang.String r25 = r0.getString(r1)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            r5[r24] = r25     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            java.lang.String r7 = "id"
            java.lang.String r14 = r10.getString(r7)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            java.lang.String r7 = "screen_name"
            java.lang.String r15 = r10.getString(r7)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            java.lang.String r7 = "profile_image_url"
            java.lang.String r13 = r10.getString(r7)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            java.lang.String r7 = "created_at"
            r0 = r22
            r1 = r7
            java.lang.String r12 = r0.getString(r1)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            java.lang.String r7 = "text"
            r0 = r22
            r1 = r7
            java.lang.String r9 = r0.getString(r1)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            java.lang.String r7 = "source"
            r0 = r22
            r1 = r7
            java.lang.String r7 = r0.getString(r1)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            java.lang.String r10 = ""
            java.lang.String r17 = "small_pic"
            r0 = r22
            r1 = r17
            boolean r17 = r0.has(r1)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            if (r17 == 0) goto L_0x0a66
            java.lang.String r10 = "small_pic"
            r0 = r22
            r1 = r10
            java.lang.String r22 = r0.getString(r1)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            r0 = r8
            r1 = r22
            r0.setThumbnailPic(r1)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
        L_0x07d6:
            r0 = r8
            r1 = r25
            r0.setId(r1)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            r8.setText(r9)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            r8.setCreatedAt(r12)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            java.lang.StringBuilder r22 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            java.lang.String r25 = "来自："
            r0 = r22
            r1 = r25
            r0.<init>(r1)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            r0 = r22
            r1 = r7
            java.lang.StringBuilder r22 = r0.append(r1)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            java.lang.String r22 = r22.toString()     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            r0 = r8
            r1 = r22
            r0.setSource(r1)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            r0 = r16
            r1 = r14
            r0.setId(r1)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            r0 = r16
            r1 = r13
            r0.setProfileImageUrl(r1)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            r0 = r16
            r1 = r15
            r0.setScreenName(r1)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            r0 = r8
            r1 = r16
            r0.setUser(r1)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            r11.add(r8)     // Catch:{ IOException -> 0x06e6, JSONException -> 0x070c }
            r22 = r6
        L_0x081b:
            int r22 = r24 + 1
            r24 = r22
            goto L_0x0683
        L_0x0821:
            java.lang.String r23 = "netease_weibo"
            r0 = r21
            r1 = r23
            boolean r21 = r0.equals(r1)
            if (r21 == 0) goto L_0x0107
            java.lang.String r7 = ""
            r21 = 0
            java.lang.StringBuilder r23 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0a35, IOException -> 0x0a3b }
            java.lang.String r5 = "?count="
            r0 = r23
            r1 = r5
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x0a35, IOException -> 0x0a3b }
            java.lang.StringBuilder r23 = r23.append(r24)     // Catch:{ MalformedURLException -> 0x0a35, IOException -> 0x0a3b }
            java.lang.String r24 = "&since_id="
            java.lang.StringBuilder r23 = r23.append(r24)     // Catch:{ MalformedURLException -> 0x0a35, IOException -> 0x0a3b }
            r0 = r23
            r1 = r25
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x0a35, IOException -> 0x0a3b }
            java.lang.String r23 = r23.toString()     // Catch:{ MalformedURLException -> 0x0a35, IOException -> 0x0a3b }
            if (r22 == 0) goto L_0x0a62
            java.lang.String r24 = ""
            r0 = r22
            r1 = r24
            boolean r24 = r0.equals(r1)     // Catch:{ MalformedURLException -> 0x0a35, IOException -> 0x0a3b }
            if (r24 != 0) goto L_0x0a62
            java.lang.StringBuilder r24 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0a35, IOException -> 0x0a3b }
            java.lang.String r23 = java.lang.String.valueOf(r23)     // Catch:{ MalformedURLException -> 0x0a35, IOException -> 0x0a3b }
            r0 = r24
            r1 = r23
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x0a35, IOException -> 0x0a3b }
            java.lang.String r23 = "&user_id="
            r0 = r24
            r1 = r23
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x0a35, IOException -> 0x0a3b }
            r0 = r23
            r1 = r22
            java.lang.StringBuilder r22 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x0a35, IOException -> 0x0a3b }
            java.lang.String r22 = r22.toString()     // Catch:{ MalformedURLException -> 0x0a35, IOException -> 0x0a3b }
        L_0x0882:
            java.net.URL r23 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0a35, IOException -> 0x0a3b }
            java.lang.StringBuilder r24 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0a35, IOException -> 0x0a3b }
            java.lang.String r25 = java.lang.String.valueOf(r7)     // Catch:{ MalformedURLException -> 0x0a35, IOException -> 0x0a3b }
            r24.<init>(r25)     // Catch:{ MalformedURLException -> 0x0a35, IOException -> 0x0a3b }
            r0 = r24
            r1 = r22
            java.lang.StringBuilder r22 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x0a35, IOException -> 0x0a3b }
            java.lang.String r22 = r22.toString()     // Catch:{ MalformedURLException -> 0x0a35, IOException -> 0x0a3b }
            r0 = r23
            r1 = r22
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x0a35, IOException -> 0x0a3b }
            java.net.URLConnection r22 = r23.openConnection()     // Catch:{ MalformedURLException -> 0x0a35, IOException -> 0x0a3b }
            r0 = r22
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x0a35, IOException -> 0x0a3b }
            r21 = r0
            java.lang.String r22 = "GET"
            r21.setRequestMethod(r22)     // Catch:{ MalformedURLException -> 0x0a35, IOException -> 0x0a3b }
        L_0x08af:
            cn.com.mzba.oauth.OAuth r22 = cn.com.mzba.service.StatusHttpUtils.oauthNetease
            java.lang.String r23 = cn.com.mzba.service.StatusHttpUtils.userTokenNetease
            java.lang.String r24 = cn.com.mzba.service.StatusHttpUtils.userTokenNeteaseSecret
            r0 = r22
            r1 = r23
            r2 = r24
            r3 = r21
            java.net.HttpURLConnection r25 = r0.signRequest(r1, r2, r3)
            if (r25 == 0) goto L_0x0107
            r21 = 200(0xc8, float:2.8E-43)
            int r22 = r25.getResponseCode()     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            r0 = r21
            r1 = r22
            if (r0 != r1) goto L_0x0107
            java.io.InputStream r22 = r25.getInputStream()     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            java.io.BufferedReader r24 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            java.io.InputStreamReader r21 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            r21.<init>(r22)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            r0 = r24
            r1 = r21
            r0.<init>(r1)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            r21.<init>()     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            r23 = 4000(0xfa0, float:5.605E-42)
            r0 = r23
            char[] r0 = new char[r0]     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            r5 = r0
            r23 = 0
        L_0x08ef:
            r0 = r24
            r1 = r5
            int r23 = r0.read(r1)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            r6 = -1
            r0 = r23
            r1 = r6
            if (r0 != r1) goto L_0x0a41
            r24.close()     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            r22.close()     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            java.lang.String r21 = r21.toString()     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            r25.disconnect()     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            org.json.JSONArray r22 = new org.json.JSONArray     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            r0 = r22
            r1 = r21
            r0.<init>(r1)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            int r21 = r22.length()     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            r0 = r21
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            r25 = r0
            r21 = 0
            r23 = r21
        L_0x0920:
            int r21 = r22.length()     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            r0 = r23
            r1 = r21
            if (r0 >= r1) goto L_0x0107
            cn.com.mzba.model.Status r7 = new cn.com.mzba.model.Status     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            r7.<init>()     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            cn.com.mzba.model.User r14 = new cn.com.mzba.model.User     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            r14.<init>()     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            r24 = 0
            org.json.JSONObject r21 = r22.getJSONObject(r23)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            if (r21 == 0) goto L_0x0a5f
            java.lang.String r5 = "user"
            r0 = r21
            r1 = r5
            org.json.JSONObject r8 = r0.getJSONObject(r1)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            java.lang.String r5 = "root_in_reply_to_status_text"
            r0 = r21
            r1 = r5
            java.lang.String r5 = r0.getString(r1)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            java.lang.String r6 = "null"
            boolean r6 = r5.equals(r6)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            if (r6 != 0) goto L_0x099c
            cn.com.mzba.model.Status r24 = new cn.com.mzba.model.Status     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            r24.<init>()     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            r6 = 1
            r7.setRetweetedStatus(r6)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            java.lang.String r6 = "root_in_reply_to_user_name"
            r0 = r21
            r1 = r6
            java.lang.String r6 = r0.getString(r1)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            java.lang.String r9 = "root_in_reply_to_user_id"
            r0 = r21
            r1 = r9
            java.lang.String r9 = r0.getString(r1)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            r0 = r24
            r1 = r9
            r0.setId(r1)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            java.lang.String r10 = "@"
            r9.<init>(r10)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            java.lang.StringBuilder r6 = r9.append(r6)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            java.lang.String r9 = ":"
            java.lang.StringBuilder r6 = r6.append(r9)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            java.lang.StringBuilder r5 = r6.append(r5)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            java.lang.String r5 = r5.toString()     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            r0 = r24
            r1 = r5
            r0.setText(r1)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            r0 = r7
            r1 = r24
            r0.setStatus(r1)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
        L_0x099c:
            r5 = r24
            java.lang.String r24 = "id"
            r0 = r21
            r1 = r24
            java.lang.String r24 = r0.getString(r1)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            r25[r23] = r24     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            java.lang.String r6 = "id"
            java.lang.String r12 = r8.getString(r6)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            java.lang.String r6 = "screen_name"
            java.lang.String r13 = r8.getString(r6)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            java.lang.String r6 = "profile_image_url"
            java.lang.String r10 = r8.getString(r6)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            java.lang.String r6 = "created_at"
            r0 = r21
            r1 = r6
            java.lang.String r9 = r0.getString(r1)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            java.lang.String r6 = "text"
            r0 = r21
            r1 = r6
            java.lang.String r8 = r0.getString(r1)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            java.lang.String r6 = "source"
            r0 = r21
            r1 = r6
            java.lang.String r6 = r0.getString(r1)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            r0 = r7
            r1 = r24
            r0.setId(r1)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            r7.setText(r8)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            r7.setCreatedAt(r9)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            java.lang.StringBuilder r24 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            java.lang.String r8 = "来自："
            r0 = r24
            r1 = r8
            r0.<init>(r1)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            r0 = r24
            r1 = r6
            java.lang.StringBuilder r24 = r0.append(r1)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            java.lang.String r24 = r24.toString()     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            r0 = r7
            r1 = r24
            r0.setSource(r1)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            r14.setId(r12)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            r14.setProfileImageUrl(r10)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            r14.setScreenName(r13)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            r7.setUser(r14)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            java.lang.String r24 = "comments_count"
            r0 = r21
            r1 = r24
            java.lang.String r24 = r0.getString(r1)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            r0 = r7
            r1 = r24
            r0.setComments(r1)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            java.lang.String r24 = "retweet_count"
            r0 = r21
            r1 = r24
            java.lang.String r21 = r0.getString(r1)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            r0 = r7
            r1 = r21
            r0.setRt(r1)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            r11.add(r7)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            r21 = r5
        L_0x0a2f:
            int r21 = r23 + 1
            r23 = r21
            goto L_0x0920
        L_0x0a35:
            r22 = move-exception
            r22.printStackTrace()
            goto L_0x08af
        L_0x0a3b:
            r22 = move-exception
            r22.printStackTrace()
            goto L_0x08af
        L_0x0a41:
            r6 = 0
            r0 = r21
            r1 = r5
            r2 = r6
            r3 = r23
            r0.append(r1, r2, r3)     // Catch:{ IOException -> 0x0a4d, JSONException -> 0x0a53 }
            goto L_0x08ef
        L_0x0a4d:
            r21 = move-exception
            r21.printStackTrace()
            goto L_0x0107
        L_0x0a53:
            r21 = move-exception
            r21.printStackTrace()
            goto L_0x0107
        L_0x0a59:
            r23 = move-exception
            goto L_0x06fb
        L_0x0a5c:
            r23 = move-exception
            goto L_0x06f1
        L_0x0a5f:
            r21 = r24
            goto L_0x0a2f
        L_0x0a62:
            r22 = r23
            goto L_0x0882
        L_0x0a66:
            r22 = r10
            goto L_0x07d6
        L_0x0a6a:
            r22 = r25
            goto L_0x081b
        L_0x0a6e:
            r22 = r23
            goto L_0x05e2
        L_0x0a72:
            r10 = r22
            goto L_0x047a
        L_0x0a76:
            r6 = r22
            goto L_0x03ee
        L_0x0a7a:
            r22 = r10
            goto L_0x020e
        L_0x0a7e:
            r20 = r6
            r6 = r25
            r25 = r20
            goto L_0x01b8
        L_0x0a86:
            r22 = r25
            goto L_0x0253
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.com.mzba.service.StatusHttpUtils.getUserTimeLines(java.lang.String, java.lang.String, int, int, java.lang.String):java.util.List");
    }

    /* JADX INFO: Multiple debug info for r21v12 java.lang.String: [D('builder' java.lang.StringBuilder), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r25v2 java.lang.String[]: [D('response' java.net.HttpURLConnection), D('idArray' java.lang.String[])] */
    /* JADX INFO: Multiple debug info for r23v8 int: [D('i' int), D('len' int)] */
    /* JADX INFO: Multiple debug info for r8v1 java.lang.String: [D('user' org.json.JSONObject), D('text' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r22v20 java.lang.String: [D('builder' java.lang.StringBuilder), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r5v15 java.lang.String[]: [D('response' java.net.HttpURLConnection), D('idArray' java.lang.String[])] */
    /* JADX INFO: Multiple debug info for r24v24 int: [D('i' int), D('len' int)] */
    /* JADX INFO: Multiple debug info for r6v24 cn.com.mzba.model.Status: [D('transmitId' java.lang.String), D('rtStatus' cn.com.mzba.model.Status)] */
    /* JADX INFO: Multiple debug info for r22v35 java.lang.String: [D('data' org.json.JSONObject), D('thumbnail_pic' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r23v34 java.lang.String: [D('page' int), D('params' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r22v37 java.lang.String: [D('user_Id' java.lang.String), D('parameter' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r22v38 java.lang.String: [D('parameter' java.lang.String), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r25v21 java.lang.String: [D('info' org.json.JSONObject), D('redirectCount' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r24v44 org.json.JSONObject: [D('type' int), D('source' org.json.JSONObject)] */
    /* JADX INFO: Multiple debug info for r24v45 java.lang.String: [D('rtName' java.lang.String), D('source' org.json.JSONObject)] */
    /* JADX INFO: Multiple debug info for r22v68 java.util.ArrayList: [D('user_Id' java.lang.String), D('params' java.util.List<org.apache.http.message.BasicNameValuePair>)] */
    /* JADX INFO: Multiple debug info for r22v73 java.lang.String: [D('builder' java.lang.StringBuilder), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r5v22 java.lang.String[]: [D('response' org.apache.http.HttpResponse), D('idArray' java.lang.String[])] */
    /* JADX INFO: Multiple debug info for r22v81 int: [D('rtStatus' cn.com.mzba.model.Status), D('i' int)] */
    /* JADX INFO: Multiple debug info for r22v87 java.lang.String: [D('data' org.json.JSONObject), D('thumbnail_pic' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r6v45 'rtStatus'  cn.com.mzba.model.Status: [D('rtStatus' cn.com.mzba.model.Status), D('transmitText' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r7v35 java.lang.String: [D('user_retweeted' org.json.JSONObject), D('transmitUserName' java.lang.String)] */
    /* JADX WARN: Type inference failed for: r22v11, types: [java.net.URLConnection] */
    /* JADX WARN: Type inference failed for: r23v38, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List<cn.com.mzba.model.Status> getFavoritedTimeLines(java.lang.String r21, java.lang.String r22, int r23, int r24, java.lang.String r25) {
        /*
            java.util.ArrayList r11 = new java.util.ArrayList
            r11.<init>()
            java.lang.String r5 = "sina_weibo"
            r0 = r21
            r1 = r5
            boolean r5 = r0.equals(r1)
            if (r5 == 0) goto L_0x0271
            java.lang.String r7 = "http://api.t.sina.com.cn/favorites.json"
            java.util.ArrayList r22 = new java.util.ArrayList
            r22.<init>()
            org.apache.http.message.BasicNameValuePair r24 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r25 = "page"
            java.lang.String r23 = java.lang.String.valueOf(r23)
            r0 = r24
            r1 = r25
            r2 = r23
            r0.<init>(r1, r2)
            r0 = r22
            r1 = r24
            r0.add(r1)
            cn.com.mzba.oauth.OAuth r23 = cn.com.mzba.service.StatusHttpUtils.oauthSina
            java.lang.String r24 = cn.com.mzba.service.StatusHttpUtils.userTokenSina
            java.lang.String r25 = cn.com.mzba.service.StatusHttpUtils.userTokenSinaSecret
            r0 = r23
            r1 = r24
            r2 = r25
            r3 = r7
            r4 = r22
            org.apache.http.HttpResponse r5 = r0.signRequest(r1, r2, r3, r4)
            if (r5 == 0) goto L_0x00d8
            r22 = 200(0xc8, float:2.8E-43)
            org.apache.http.StatusLine r23 = r5.getStatusLine()
            int r23 = r23.getStatusCode()
            r0 = r22
            r1 = r23
            if (r0 != r1) goto L_0x00d8
            org.apache.http.HttpEntity r22 = r5.getEntity()     // Catch:{ Exception -> 0x00e4 }
            java.io.InputStream r23 = r22.getContent()     // Catch:{ Exception -> 0x00e4 }
            java.io.BufferedReader r25 = new java.io.BufferedReader     // Catch:{ Exception -> 0x00e4 }
            java.io.InputStreamReader r22 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x00e4 }
            r22.<init>(r23)     // Catch:{ Exception -> 0x00e4 }
            r0 = r25
            r1 = r22
            r0.<init>(r1)     // Catch:{ Exception -> 0x00e4 }
            java.lang.StringBuilder r22 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e4 }
            org.apache.http.HttpEntity r24 = r5.getEntity()     // Catch:{ Exception -> 0x00e4 }
            long r6 = r24.getContentLength()     // Catch:{ Exception -> 0x00e4 }
            r0 = r6
            int r0 = (int) r0     // Catch:{ Exception -> 0x00e4 }
            r24 = r0
            r0 = r22
            r1 = r24
            r0.<init>(r1)     // Catch:{ Exception -> 0x00e4 }
            r24 = 4000(0xfa0, float:5.605E-42)
            r0 = r24
            char[] r0 = new char[r0]     // Catch:{ Exception -> 0x00e4 }
            r6 = r0
            r24 = 0
        L_0x0088:
            r0 = r25
            r1 = r6
            int r24 = r0.read(r1)     // Catch:{ Exception -> 0x00e4 }
            r7 = -1
            r0 = r24
            r1 = r7
            if (r0 != r1) goto L_0x00d9
            r25.close()     // Catch:{ Exception -> 0x00e4 }
            r23.close()     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r22 = r22.toString()     // Catch:{ Exception -> 0x00e4 }
            org.apache.http.HttpEntity r23 = r5.getEntity()     // Catch:{ Exception -> 0x00e4 }
            r23.consumeContent()     // Catch:{ Exception -> 0x00e4 }
            org.json.JSONArray r23 = new org.json.JSONArray     // Catch:{ Exception -> 0x00e4 }
            r0 = r23
            r1 = r22
            r0.<init>(r1)     // Catch:{ Exception -> 0x00e4 }
            int r22 = r23.length()     // Catch:{ Exception -> 0x00e4 }
            r0 = r22
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ Exception -> 0x00e4 }
            r5 = r0
            r22 = 0
            r24 = r22
        L_0x00bc:
            int r22 = r23.length()     // Catch:{ Exception -> 0x00e4 }
            r0 = r24
            r1 = r22
            if (r0 < r1) goto L_0x00e9
            java.lang.String r22 = cn.com.mzba.service.StringUtil.arrayToString(r5)     // Catch:{ Exception -> 0x00e4 }
            java.util.List r22 = getCounts(r21, r22)     // Catch:{ Exception -> 0x00e4 }
            java.util.Iterator r25 = r11.iterator()     // Catch:{ Exception -> 0x00e4 }
        L_0x00d2:
            boolean r23 = r25.hasNext()     // Catch:{ Exception -> 0x00e4 }
            if (r23 != 0) goto L_0x022a
        L_0x00d8:
            return r11
        L_0x00d9:
            r7 = 0
            r0 = r22
            r1 = r6
            r2 = r7
            r3 = r24
            r0.append(r1, r2, r3)     // Catch:{ Exception -> 0x00e4 }
            goto L_0x0088
        L_0x00e4:
            r21 = move-exception
            r21.printStackTrace()
            goto L_0x00d8
        L_0x00e9:
            cn.com.mzba.model.Status r8 = new cn.com.mzba.model.Status     // Catch:{ Exception -> 0x00e4 }
            r8.<init>()     // Catch:{ Exception -> 0x00e4 }
            cn.com.mzba.model.User r16 = new cn.com.mzba.model.User     // Catch:{ Exception -> 0x00e4 }
            r16.<init>()     // Catch:{ Exception -> 0x00e4 }
            r25 = 0
            org.json.JSONObject r22 = r23.getJSONObject(r24)     // Catch:{ Exception -> 0x00e4 }
            if (r22 == 0) goto L_0x09e6
            java.lang.String r6 = "user"
            r0 = r22
            r1 = r6
            org.json.JSONObject r10 = r0.getJSONObject(r1)     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r6 = ""
            java.lang.String r7 = "retweeted_status"
            r0 = r22
            r1 = r7
            boolean r7 = r0.has(r1)     // Catch:{ Exception -> 0x00e4 }
            if (r7 == 0) goto L_0x09de
            cn.com.mzba.model.Status r6 = new cn.com.mzba.model.Status     // Catch:{ Exception -> 0x00e4 }
            r6.<init>()     // Catch:{ Exception -> 0x00e4 }
            r25 = 1
            r0 = r8
            r1 = r25
            r0.setRetweetedStatus(r1)     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r25 = "retweeted_status"
            r0 = r22
            r1 = r25
            org.json.JSONObject r25 = r0.getJSONObject(r1)     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r7 = "user"
            r0 = r25
            r1 = r7
            org.json.JSONObject r7 = r0.getJSONObject(r1)     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r9 = "screen_name"
            java.lang.String r7 = r7.getString(r9)     // Catch:{ Exception -> 0x00e4 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r12 = "@"
            r9.<init>(r12)     // Catch:{ Exception -> 0x00e4 }
            java.lang.StringBuilder r7 = r9.append(r7)     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r9 = ":"
            java.lang.StringBuilder r7 = r7.append(r9)     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r9 = "text"
            r0 = r25
            r1 = r9
            java.lang.String r9 = r0.getString(r1)     // Catch:{ Exception -> 0x00e4 }
            java.lang.StringBuilder r7 = r7.append(r9)     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r9 = r7.toString()     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r7 = ""
            java.lang.String r12 = "thumbnail_pic"
            r0 = r25
            r1 = r12
            boolean r12 = r0.has(r1)     // Catch:{ Exception -> 0x00e4 }
            if (r12 == 0) goto L_0x016f
            java.lang.String r7 = "thumbnail_pic"
            r0 = r25
            r1 = r7
            java.lang.String r7 = r0.getString(r1)     // Catch:{ Exception -> 0x00e4 }
        L_0x016f:
            java.lang.String r12 = "id"
            r0 = r25
            r1 = r12
            java.lang.String r25 = r0.getString(r1)     // Catch:{ Exception -> 0x00e4 }
            r0 = r6
            r1 = r25
            r0.setId(r1)     // Catch:{ Exception -> 0x00e4 }
            r6.setText(r9)     // Catch:{ Exception -> 0x00e4 }
            r6.setThumbnailPic(r7)     // Catch:{ Exception -> 0x00e4 }
            r8.setStatus(r6)     // Catch:{ Exception -> 0x00e4 }
            r25 = r9
        L_0x0189:
            java.lang.String r25 = "id"
            r0 = r22
            r1 = r25
            java.lang.String r25 = r0.getString(r1)     // Catch:{ Exception -> 0x00e4 }
            r5[r24] = r25     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r7 = "id"
            java.lang.String r14 = r10.getString(r7)     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r7 = "screen_name"
            java.lang.String r15 = r10.getString(r7)     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r7 = "profile_image_url"
            java.lang.String r13 = r10.getString(r7)     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r7 = "created_at"
            r0 = r22
            r1 = r7
            java.lang.String r12 = r0.getString(r1)     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r7 = "text"
            r0 = r22
            r1 = r7
            java.lang.String r9 = r0.getString(r1)     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r7 = "source"
            r0 = r22
            r1 = r7
            java.lang.String r7 = r0.getString(r1)     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r10 = ""
            java.lang.String r17 = "thumbnail_pic"
            r0 = r22
            r1 = r17
            boolean r17 = r0.has(r1)     // Catch:{ Exception -> 0x00e4 }
            if (r17 == 0) goto L_0x09da
            java.lang.String r10 = "thumbnail_pic"
            r0 = r22
            r1 = r10
            java.lang.String r22 = r0.getString(r1)     // Catch:{ Exception -> 0x00e4 }
            r0 = r8
            r1 = r22
            r0.setThumbnailPic(r1)     // Catch:{ Exception -> 0x00e4 }
        L_0x01df:
            r0 = r8
            r1 = r25
            r0.setId(r1)     // Catch:{ Exception -> 0x00e4 }
            r8.setText(r9)     // Catch:{ Exception -> 0x00e4 }
            r8.setCreatedAt(r12)     // Catch:{ Exception -> 0x00e4 }
            java.lang.StringBuilder r22 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r25 = "来自："
            r0 = r22
            r1 = r25
            r0.<init>(r1)     // Catch:{ Exception -> 0x00e4 }
            r0 = r22
            r1 = r7
            java.lang.StringBuilder r22 = r0.append(r1)     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r22 = r22.toString()     // Catch:{ Exception -> 0x00e4 }
            r0 = r8
            r1 = r22
            r0.setSource(r1)     // Catch:{ Exception -> 0x00e4 }
            r0 = r16
            r1 = r14
            r0.setId(r1)     // Catch:{ Exception -> 0x00e4 }
            r0 = r16
            r1 = r13
            r0.setProfileImageUrl(r1)     // Catch:{ Exception -> 0x00e4 }
            r0 = r16
            r1 = r15
            r0.setScreenName(r1)     // Catch:{ Exception -> 0x00e4 }
            r0 = r8
            r1 = r16
            r0.setUser(r1)     // Catch:{ Exception -> 0x00e4 }
            r11.add(r8)     // Catch:{ Exception -> 0x00e4 }
            r22 = r6
        L_0x0224:
            int r22 = r24 + 1
            r24 = r22
            goto L_0x00bc
        L_0x022a:
            java.lang.Object r24 = r25.next()     // Catch:{ Exception -> 0x00e4 }
            cn.com.mzba.model.Status r24 = (cn.com.mzba.model.Status) r24     // Catch:{ Exception -> 0x00e4 }
            java.util.Iterator r5 = r22.iterator()     // Catch:{ Exception -> 0x00e4 }
        L_0x0234:
            boolean r23 = r5.hasNext()     // Catch:{ Exception -> 0x00e4 }
            if (r23 == 0) goto L_0x00d2
            java.lang.Object r23 = r5.next()     // Catch:{ Exception -> 0x00e4 }
            cn.com.mzba.model.StatusCount r23 = (cn.com.mzba.model.StatusCount) r23     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r6 = r23.getWeiboId()     // Catch:{ Exception -> 0x00e4 }
            r0 = r21
            r1 = r6
            boolean r6 = r0.equals(r1)     // Catch:{ Exception -> 0x00e4 }
            if (r6 == 0) goto L_0x0234
            java.lang.String r6 = r24.getId()     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r7 = r23.getId()     // Catch:{ Exception -> 0x00e4 }
            boolean r6 = r6.equals(r7)     // Catch:{ Exception -> 0x00e4 }
            if (r6 == 0) goto L_0x0234
            java.lang.String r6 = r23.getComments()     // Catch:{ Exception -> 0x00e4 }
            r0 = r24
            r1 = r6
            r0.setComments(r1)     // Catch:{ Exception -> 0x00e4 }
            java.lang.String r23 = r23.getRt()     // Catch:{ Exception -> 0x00e4 }
            r0 = r24
            r1 = r23
            r0.setRt(r1)     // Catch:{ Exception -> 0x00e4 }
            goto L_0x0234
        L_0x0271:
            java.lang.String r5 = "tencent_weibo"
            r0 = r21
            r1 = r5
            boolean r5 = r0.equals(r1)
            if (r5 == 0) goto L_0x04d1
            java.lang.String r7 = "http://open.t.qq.com/api/fav/list_t"
            java.util.ArrayList r10 = new java.util.ArrayList
            r10.<init>()
            cn.com.mzba.oauth.Parameter r23 = new cn.com.mzba.oauth.Parameter
            java.lang.String r25 = "Reqnum"
            java.lang.String r24 = java.lang.String.valueOf(r24)
            r0 = r23
            r1 = r25
            r2 = r24
            r0.<init>(r1, r2)
            r0 = r10
            r1 = r23
            r0.add(r1)
            cn.com.mzba.oauth.Parameter r23 = new cn.com.mzba.oauth.Parameter
            java.lang.String r24 = "type"
            r25 = 0
            java.lang.String r25 = java.lang.String.valueOf(r25)
            r23.<init>(r24, r25)
            r0 = r10
            r1 = r23
            r0.add(r1)
            cn.com.mzba.oauth.Parameter r23 = new cn.com.mzba.oauth.Parameter
            java.lang.String r24 = "Pageflag"
            r25 = 0
            java.lang.String r25 = java.lang.String.valueOf(r25)
            r23.<init>(r24, r25)
            r0 = r10
            r1 = r23
            r0.add(r1)
            cn.com.mzba.oauth.Parameter r23 = new cn.com.mzba.oauth.Parameter
            java.lang.String r24 = "fotmat"
            java.lang.String r25 = "json"
            r23.<init>(r24, r25)
            r0 = r10
            r1 = r23
            r0.add(r1)
            cn.com.mzba.oauth.Parameter r23 = new cn.com.mzba.oauth.Parameter
            java.lang.String r24 = "Name"
            r0 = r23
            r1 = r24
            r2 = r22
            r0.<init>(r1, r2)
            r0 = r10
            r1 = r23
            r0.add(r1)
            cn.com.mzba.oauth.OAuth r5 = cn.com.mzba.service.StatusHttpUtils.oauthTencent     // Catch:{ Exception -> 0x04cb }
            java.lang.String r6 = "GET"
            java.lang.String r8 = cn.com.mzba.service.StatusHttpUtils.userTokenTencent     // Catch:{ Exception -> 0x04cb }
            java.lang.String r9 = cn.com.mzba.service.StatusHttpUtils.userTokenTencentSecret     // Catch:{ Exception -> 0x04cb }
            java.lang.String r22 = r5.getPostParameter(r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x04cb }
            cn.com.mzba.oauth.OAuth r23 = cn.com.mzba.service.StatusHttpUtils.oauthTencent     // Catch:{ Exception -> 0x04cb }
            r0 = r23
            r1 = r7
            r2 = r22
            java.lang.String r22 = r0.httpGet(r1, r2)     // Catch:{ Exception -> 0x04cb }
            if (r22 == 0) goto L_0x00d8
            org.json.JSONObject r23 = new org.json.JSONObject     // Catch:{ Exception -> 0x04cb }
            r0 = r23
            r1 = r22
            r0.<init>(r1)     // Catch:{ Exception -> 0x04cb }
            java.lang.String r22 = "data"
            r0 = r23
            r1 = r22
            org.json.JSONObject r22 = r0.getJSONObject(r1)     // Catch:{ Exception -> 0x04cb }
            java.lang.String r23 = "info"
            org.json.JSONArray r5 = r22.getJSONArray(r23)     // Catch:{ Exception -> 0x04cb }
            r22 = 0
            r23 = r22
        L_0x0318:
            int r22 = r5.length()     // Catch:{ Exception -> 0x04cb }
            r0 = r23
            r1 = r22
            if (r0 >= r1) goto L_0x00d8
            cn.com.mzba.model.Status r8 = new cn.com.mzba.model.Status     // Catch:{ Exception -> 0x04cb }
            r8.<init>()     // Catch:{ Exception -> 0x04cb }
            cn.com.mzba.model.User r16 = new cn.com.mzba.model.User     // Catch:{ Exception -> 0x04cb }
            r16.<init>()     // Catch:{ Exception -> 0x04cb }
            r22 = 0
            r0 = r5
            r1 = r23
            org.json.JSONObject r25 = r0.getJSONObject(r1)     // Catch:{ Exception -> 0x04cb }
            if (r25 == 0) goto L_0x04c5
            java.lang.String r24 = "type"
            r0 = r25
            r1 = r24
            int r24 = r0.getInt(r1)     // Catch:{ Exception -> 0x04cb }
            r6 = 2
            r0 = r24
            r1 = r6
            if (r0 != r1) goto L_0x09d6
            java.lang.String r22 = "source"
            r0 = r25
            r1 = r22
            org.json.JSONObject r24 = r0.getJSONObject(r1)     // Catch:{ Exception -> 0x04cb }
            cn.com.mzba.model.Status r6 = new cn.com.mzba.model.Status     // Catch:{ Exception -> 0x04cb }
            r6.<init>()     // Catch:{ Exception -> 0x04cb }
            r22 = 1
            r0 = r8
            r1 = r22
            r0.setRetweetedStatus(r1)     // Catch:{ Exception -> 0x04cb }
            java.lang.String r22 = "text"
            r0 = r24
            r1 = r22
            java.lang.String r7 = r0.getString(r1)     // Catch:{ Exception -> 0x04cb }
            java.lang.String r22 = "id"
            r0 = r24
            r1 = r22
            java.lang.String r22 = r0.getString(r1)     // Catch:{ Exception -> 0x04cb }
            java.lang.String r9 = "nick"
            r0 = r24
            r1 = r9
            java.lang.String r24 = r0.getString(r1)     // Catch:{ Exception -> 0x04cb }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04cb }
            java.lang.String r10 = "@"
            r9.<init>(r10)     // Catch:{ Exception -> 0x04cb }
            r0 = r9
            r1 = r24
            java.lang.StringBuilder r24 = r0.append(r1)     // Catch:{ Exception -> 0x04cb }
            java.lang.String r9 = ":"
            r0 = r24
            r1 = r9
            java.lang.StringBuilder r24 = r0.append(r1)     // Catch:{ Exception -> 0x04cb }
            r0 = r24
            r1 = r7
            java.lang.StringBuilder r24 = r0.append(r1)     // Catch:{ Exception -> 0x04cb }
            java.lang.String r24 = r24.toString()     // Catch:{ Exception -> 0x04cb }
            r0 = r6
            r1 = r24
            r0.setText(r1)     // Catch:{ Exception -> 0x04cb }
            r0 = r6
            r1 = r22
            r0.setId(r1)     // Catch:{ Exception -> 0x04cb }
            r8.setStatus(r6)     // Catch:{ Exception -> 0x04cb }
        L_0x03ac:
            java.lang.String r22 = "id"
            r0 = r25
            r1 = r22
            java.lang.String r24 = r0.getString(r1)     // Catch:{ Exception -> 0x04cb }
            java.lang.String r22 = "name"
            r0 = r25
            r1 = r22
            java.lang.String r14 = r0.getString(r1)     // Catch:{ Exception -> 0x04cb }
            java.lang.String r22 = "nick"
            r0 = r25
            r1 = r22
            java.lang.String r15 = r0.getString(r1)     // Catch:{ Exception -> 0x04cb }
            java.lang.String r22 = "head"
            r0 = r25
            r1 = r22
            java.lang.String r13 = r0.getString(r1)     // Catch:{ Exception -> 0x04cb }
            java.lang.String r22 = "timestamp"
            r0 = r25
            r1 = r22
            java.lang.String r12 = r0.getString(r1)     // Catch:{ Exception -> 0x04cb }
            java.lang.String r22 = "text"
            r0 = r25
            r1 = r22
            java.lang.String r9 = r0.getString(r1)     // Catch:{ Exception -> 0x04cb }
            java.lang.String r22 = "from"
            r0 = r25
            r1 = r22
            java.lang.String r7 = r0.getString(r1)     // Catch:{ Exception -> 0x04cb }
            java.lang.String r22 = "image"
            r0 = r25
            r1 = r22
            java.lang.String r22 = r0.getString(r1)     // Catch:{ Exception -> 0x04cb }
            if (r22 == 0) goto L_0x09d2
            java.lang.String r10 = "null"
            r0 = r22
            r1 = r10
            boolean r10 = r0.equals(r1)     // Catch:{ Exception -> 0x04cb }
            if (r10 != 0) goto L_0x09d2
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04cb }
            r17 = 2
            int r18 = r22.length()     // Catch:{ Exception -> 0x04cb }
            r19 = 2
            int r18 = r18 - r19
            r0 = r22
            r1 = r17
            r2 = r18
            java.lang.String r22 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x04cb }
            java.lang.String r22 = java.lang.String.valueOf(r22)     // Catch:{ Exception -> 0x04cb }
            r0 = r10
            r1 = r22
            r0.<init>(r1)     // Catch:{ Exception -> 0x04cb }
            java.lang.String r22 = "/120"
            r0 = r10
            r1 = r22
            java.lang.StringBuilder r22 = r0.append(r1)     // Catch:{ Exception -> 0x04cb }
            java.lang.String r22 = r22.toString()     // Catch:{ Exception -> 0x04cb }
            r10 = r22
        L_0x0438:
            java.lang.String r22 = "mcount"
            r0 = r25
            r1 = r22
            java.lang.String r22 = r0.getString(r1)     // Catch:{ Exception -> 0x04cb }
            java.lang.String r17 = "count"
            r0 = r25
            r1 = r17
            java.lang.String r25 = r0.getString(r1)     // Catch:{ Exception -> 0x04cb }
            r8.setThumbnailPic(r10)     // Catch:{ Exception -> 0x04cb }
            r0 = r8
            r1 = r24
            r0.setId(r1)     // Catch:{ Exception -> 0x04cb }
            r8.setText(r9)     // Catch:{ Exception -> 0x04cb }
            r8.setCreatedAt(r12)     // Catch:{ Exception -> 0x04cb }
            java.lang.StringBuilder r24 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04cb }
            java.lang.String r9 = "来自："
            r0 = r24
            r1 = r9
            r0.<init>(r1)     // Catch:{ Exception -> 0x04cb }
            r0 = r24
            r1 = r7
            java.lang.StringBuilder r24 = r0.append(r1)     // Catch:{ Exception -> 0x04cb }
            java.lang.String r24 = r24.toString()     // Catch:{ Exception -> 0x04cb }
            r0 = r8
            r1 = r24
            r0.setSource(r1)     // Catch:{ Exception -> 0x04cb }
            r0 = r8
            r1 = r21
            r0.setWeiboId(r1)     // Catch:{ Exception -> 0x04cb }
            r0 = r16
            r1 = r14
            r0.setId(r1)     // Catch:{ Exception -> 0x04cb }
            java.lang.StringBuilder r24 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04cb }
            java.lang.String r7 = java.lang.String.valueOf(r13)     // Catch:{ Exception -> 0x04cb }
            r0 = r24
            r1 = r7
            r0.<init>(r1)     // Catch:{ Exception -> 0x04cb }
            java.lang.String r7 = "/40"
            r0 = r24
            r1 = r7
            java.lang.StringBuilder r24 = r0.append(r1)     // Catch:{ Exception -> 0x04cb }
            java.lang.String r24 = r24.toString()     // Catch:{ Exception -> 0x04cb }
            r0 = r16
            r1 = r24
            r0.setProfileImageUrl(r1)     // Catch:{ Exception -> 0x04cb }
            r0 = r16
            r1 = r15
            r0.setScreenName(r1)     // Catch:{ Exception -> 0x04cb }
            r0 = r8
            r1 = r16
            r0.setUser(r1)     // Catch:{ Exception -> 0x04cb }
            r0 = r8
            r1 = r21
            r0.setWeiboId(r1)     // Catch:{ Exception -> 0x04cb }
            r0 = r8
            r1 = r22
            r0.setComments(r1)     // Catch:{ Exception -> 0x04cb }
            r0 = r8
            r1 = r25
            r0.setRt(r1)     // Catch:{ Exception -> 0x04cb }
            r11.add(r8)     // Catch:{ Exception -> 0x04cb }
            r22 = r6
        L_0x04c5:
            int r22 = r23 + 1
            r23 = r22
            goto L_0x0318
        L_0x04cb:
            r21 = move-exception
            r21.printStackTrace()
            goto L_0x00d8
        L_0x04d1:
            java.lang.String r5 = "sohu_weibo"
            r0 = r21
            r1 = r5
            boolean r5 = r0.equals(r1)
            if (r5 == 0) goto L_0x0759
            java.lang.String r7 = "http://api.t.sohu.com/favourites.json"
            r22 = 0
            java.lang.StringBuilder r24 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x062c, IOException -> 0x0632 }
            java.lang.String r5 = "?page="
            r0 = r24
            r1 = r5
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x062c, IOException -> 0x0632 }
            r0 = r24
            r1 = r23
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x062c, IOException -> 0x0632 }
            java.lang.String r23 = r23.toString()     // Catch:{ MalformedURLException -> 0x062c, IOException -> 0x0632 }
            if (r25 == 0) goto L_0x051b
            java.lang.StringBuilder r24 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x062c, IOException -> 0x0632 }
            java.lang.String r23 = java.lang.String.valueOf(r23)     // Catch:{ MalformedURLException -> 0x062c, IOException -> 0x0632 }
            r0 = r24
            r1 = r23
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x062c, IOException -> 0x0632 }
            java.lang.String r23 = "&since_id="
            r0 = r24
            r1 = r23
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x062c, IOException -> 0x0632 }
            r0 = r23
            r1 = r25
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x062c, IOException -> 0x0632 }
            java.lang.String r23 = r23.toString()     // Catch:{ MalformedURLException -> 0x062c, IOException -> 0x0632 }
        L_0x051b:
            java.net.URL r24 = new java.net.URL     // Catch:{ MalformedURLException -> 0x062c, IOException -> 0x0632 }
            java.lang.StringBuilder r25 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x062c, IOException -> 0x0632 }
            java.lang.String r5 = java.lang.String.valueOf(r7)     // Catch:{ MalformedURLException -> 0x062c, IOException -> 0x0632 }
            r0 = r25
            r1 = r5
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x062c, IOException -> 0x0632 }
            r0 = r25
            r1 = r23
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x062c, IOException -> 0x0632 }
            java.lang.String r23 = r23.toString()     // Catch:{ MalformedURLException -> 0x062c, IOException -> 0x0632 }
            r0 = r24
            r1 = r23
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x062c, IOException -> 0x0632 }
            java.net.URLConnection r23 = r24.openConnection()     // Catch:{ MalformedURLException -> 0x062c, IOException -> 0x0632 }
            r0 = r23
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x062c, IOException -> 0x0632 }
            r22 = r0
            java.lang.String r23 = "GET"
            r22.setRequestMethod(r23)     // Catch:{ MalformedURLException -> 0x062c, IOException -> 0x0632 }
        L_0x054b:
            cn.com.mzba.oauth.OAuth r23 = cn.com.mzba.service.StatusHttpUtils.oauthSohu
            java.lang.String r24 = cn.com.mzba.service.StatusHttpUtils.userTokenSohu
            java.lang.String r25 = cn.com.mzba.service.StatusHttpUtils.userTokenSohuSecret
            r0 = r23
            r1 = r24
            r2 = r25
            r3 = r22
            java.net.HttpURLConnection r5 = r0.signRequest(r1, r2, r3)
            if (r5 == 0) goto L_0x00d8
            r22 = 200(0xc8, float:2.8E-43)
            int r23 = r5.getResponseCode()     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            r0 = r22
            r1 = r23
            if (r0 != r1) goto L_0x00d8
            java.io.InputStream r23 = r5.getInputStream()     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            java.io.BufferedReader r25 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            java.io.InputStreamReader r22 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            r22.<init>(r23)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            r0 = r25
            r1 = r22
            r0.<init>(r1)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            java.lang.StringBuilder r22 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            int r24 = r5.getContentLength()     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            r0 = r22
            r1 = r24
            r0.<init>(r1)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            r24 = 4000(0xfa0, float:5.605E-42)
            r0 = r24
            char[] r0 = new char[r0]     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            r6 = r0
            r24 = 0
        L_0x0593:
            r0 = r25
            r1 = r6
            int r24 = r0.read(r1)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            r7 = -1
            r0 = r24
            r1 = r7
            if (r0 != r1) goto L_0x0638
            r25.close()     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            r23.close()     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            java.lang.String r22 = r22.toString()     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            r5.disconnect()     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            org.json.JSONArray r23 = new org.json.JSONArray     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            r0 = r23
            r1 = r22
            r0.<init>(r1)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            int r22 = r23.length()     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            r0 = r22
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            r5 = r0
            r22 = 0
            r24 = r22
        L_0x05c3:
            int r22 = r23.length()     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            r0 = r24
            r1 = r22
            if (r0 < r1) goto L_0x064a
            java.lang.String r22 = cn.com.mzba.service.StringUtil.arrayToString(r5)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            java.util.List r22 = getCounts(r21, r22)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            java.util.Iterator r25 = r11.iterator()     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
        L_0x05d9:
            boolean r23 = r25.hasNext()     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            if (r23 == 0) goto L_0x00d8
            java.lang.Object r24 = r25.next()     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            cn.com.mzba.model.Status r24 = (cn.com.mzba.model.Status) r24     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            java.util.Iterator r5 = r22.iterator()     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
        L_0x05e9:
            boolean r23 = r5.hasNext()     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            if (r23 == 0) goto L_0x05d9
            java.lang.Object r23 = r5.next()     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            cn.com.mzba.model.StatusCount r23 = (cn.com.mzba.model.StatusCount) r23     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            java.lang.String r6 = r23.getWeiboId()     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            r0 = r21
            r1 = r6
            boolean r6 = r0.equals(r1)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            if (r6 == 0) goto L_0x05e9
            java.lang.String r6 = r24.getId()     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            java.lang.String r7 = r23.getId()     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            boolean r6 = r6.equals(r7)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            if (r6 == 0) goto L_0x05e9
            java.lang.String r6 = r23.getComments()     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            r0 = r24
            r1 = r6
            r0.setComments(r1)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            java.lang.String r23 = r23.getRt()     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            r0 = r24
            r1 = r23
            r0.setRt(r1)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            goto L_0x05e9
        L_0x0626:
            r21 = move-exception
            r21.printStackTrace()
            goto L_0x00d8
        L_0x062c:
            r23 = move-exception
            r23.printStackTrace()
            goto L_0x054b
        L_0x0632:
            r23 = move-exception
            r23.printStackTrace()
            goto L_0x054b
        L_0x0638:
            r7 = 0
            r0 = r22
            r1 = r6
            r2 = r7
            r3 = r24
            r0.append(r1, r2, r3)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            goto L_0x0593
        L_0x0644:
            r21 = move-exception
            r21.printStackTrace()
            goto L_0x00d8
        L_0x064a:
            cn.com.mzba.model.Status r8 = new cn.com.mzba.model.Status     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            r8.<init>()     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            cn.com.mzba.model.User r16 = new cn.com.mzba.model.User     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            r16.<init>()     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            r25 = 0
            org.json.JSONObject r22 = r23.getJSONObject(r24)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            if (r22 == 0) goto L_0x09ce
            java.lang.String r6 = "user"
            r0 = r22
            r1 = r6
            org.json.JSONObject r10 = r0.getJSONObject(r1)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            java.lang.String r6 = "in_reply_to_status_text"
            r0 = r22
            r1 = r6
            java.lang.String r7 = r0.getString(r1)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            if (r7 == 0) goto L_0x06b6
            cn.com.mzba.model.Status r25 = new cn.com.mzba.model.Status     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            r25.<init>()     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            r6 = 1
            r8.setRetweetedStatus(r6)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            java.lang.String r6 = "in_reply_to_screen_name"
            r0 = r22
            r1 = r6
            java.lang.String r9 = r0.getString(r1)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            java.lang.String r6 = "in_reply_to_status_id"
            r0 = r22
            r1 = r6
            java.lang.String r6 = r0.getString(r1)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            java.lang.String r13 = "@"
            r12.<init>(r13)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            java.lang.StringBuilder r9 = r12.append(r9)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            java.lang.String r12 = ":"
            java.lang.StringBuilder r9 = r9.append(r12)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            java.lang.StringBuilder r7 = r9.append(r7)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            java.lang.String r7 = r7.toString()     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            r0 = r25
            r1 = r7
            r0.setText(r1)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            r0 = r25
            r1 = r6
            r0.setId(r1)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            r0 = r8
            r1 = r25
            r0.setStatus(r1)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
        L_0x06b6:
            r6 = r25
            java.lang.String r25 = "id"
            r0 = r22
            r1 = r25
            java.lang.String r25 = r0.getString(r1)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            r5[r24] = r25     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            java.lang.String r7 = "id"
            java.lang.String r14 = r10.getString(r7)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            java.lang.String r7 = "screen_name"
            java.lang.String r15 = r10.getString(r7)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            java.lang.String r7 = "profile_image_url"
            java.lang.String r13 = r10.getString(r7)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            java.lang.String r7 = "created_at"
            r0 = r22
            r1 = r7
            java.lang.String r12 = r0.getString(r1)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            java.lang.String r7 = "text"
            r0 = r22
            r1 = r7
            java.lang.String r9 = r0.getString(r1)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            java.lang.String r7 = "source"
            r0 = r22
            r1 = r7
            java.lang.String r7 = r0.getString(r1)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            java.lang.String r10 = ""
            java.lang.String r17 = "small_pic"
            r0 = r22
            r1 = r17
            boolean r17 = r0.has(r1)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            if (r17 == 0) goto L_0x09ca
            java.lang.String r10 = "small_pic"
            r0 = r22
            r1 = r10
            java.lang.String r22 = r0.getString(r1)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            r0 = r8
            r1 = r22
            r0.setThumbnailPic(r1)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
        L_0x070e:
            r0 = r8
            r1 = r25
            r0.setId(r1)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            r8.setText(r9)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            r8.setCreatedAt(r12)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            java.lang.StringBuilder r22 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            java.lang.String r25 = "来自："
            r0 = r22
            r1 = r25
            r0.<init>(r1)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            r0 = r22
            r1 = r7
            java.lang.StringBuilder r22 = r0.append(r1)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            java.lang.String r22 = r22.toString()     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            r0 = r8
            r1 = r22
            r0.setSource(r1)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            r0 = r16
            r1 = r14
            r0.setId(r1)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            r0 = r16
            r1 = r13
            r0.setProfileImageUrl(r1)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            r0 = r16
            r1 = r15
            r0.setScreenName(r1)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            r0 = r8
            r1 = r16
            r0.setUser(r1)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            r11.add(r8)     // Catch:{ IOException -> 0x0626, JSONException -> 0x0644 }
            r22 = r6
        L_0x0753:
            int r22 = r24 + 1
            r24 = r22
            goto L_0x05c3
        L_0x0759:
            java.lang.String r23 = "netease_weibo"
            r0 = r21
            r1 = r23
            boolean r21 = r0.equals(r1)
            if (r21 == 0) goto L_0x00d8
            java.lang.StringBuilder r21 = new java.lang.StringBuilder
            java.lang.String r23 = "http://api.t.163.com/favorites/"
            r0 = r21
            r1 = r23
            r0.<init>(r1)
            java.lang.StringBuilder r21 = r21.append(r22)
            java.lang.String r23 = ".json"
            r0 = r21
            r1 = r23
            java.lang.StringBuilder r21 = r0.append(r1)
            java.lang.String r7 = r21.toString()
            r21 = 0
            java.lang.StringBuilder r23 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x099f, IOException -> 0x09a5 }
            java.lang.String r5 = "?count="
            r0 = r23
            r1 = r5
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x099f, IOException -> 0x09a5 }
            java.lang.StringBuilder r23 = r23.append(r24)     // Catch:{ MalformedURLException -> 0x099f, IOException -> 0x09a5 }
            java.lang.String r23 = r23.toString()     // Catch:{ MalformedURLException -> 0x099f, IOException -> 0x09a5 }
            if (r25 == 0) goto L_0x07bb
            java.lang.StringBuilder r24 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x099f, IOException -> 0x09a5 }
            java.lang.String r23 = java.lang.String.valueOf(r23)     // Catch:{ MalformedURLException -> 0x099f, IOException -> 0x09a5 }
            r0 = r24
            r1 = r23
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x099f, IOException -> 0x09a5 }
            java.lang.String r23 = "&since_id="
            r0 = r24
            r1 = r23
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x099f, IOException -> 0x09a5 }
            r0 = r23
            r1 = r25
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x099f, IOException -> 0x09a5 }
            java.lang.String r23 = r23.toString()     // Catch:{ MalformedURLException -> 0x099f, IOException -> 0x09a5 }
        L_0x07bb:
            if (r22 == 0) goto L_0x09c6
            java.lang.String r24 = ""
            r0 = r22
            r1 = r24
            boolean r24 = r0.equals(r1)     // Catch:{ MalformedURLException -> 0x099f, IOException -> 0x09a5 }
            if (r24 != 0) goto L_0x09c6
            java.lang.StringBuilder r24 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x099f, IOException -> 0x09a5 }
            java.lang.String r23 = java.lang.String.valueOf(r23)     // Catch:{ MalformedURLException -> 0x099f, IOException -> 0x09a5 }
            r0 = r24
            r1 = r23
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x099f, IOException -> 0x09a5 }
            java.lang.String r23 = "&id="
            r0 = r24
            r1 = r23
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x099f, IOException -> 0x09a5 }
            r0 = r23
            r1 = r22
            java.lang.StringBuilder r22 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x099f, IOException -> 0x09a5 }
            java.lang.String r22 = r22.toString()     // Catch:{ MalformedURLException -> 0x099f, IOException -> 0x09a5 }
        L_0x07ec:
            java.net.URL r23 = new java.net.URL     // Catch:{ MalformedURLException -> 0x099f, IOException -> 0x09a5 }
            java.lang.StringBuilder r24 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x099f, IOException -> 0x09a5 }
            java.lang.String r25 = java.lang.String.valueOf(r7)     // Catch:{ MalformedURLException -> 0x099f, IOException -> 0x09a5 }
            r24.<init>(r25)     // Catch:{ MalformedURLException -> 0x099f, IOException -> 0x09a5 }
            r0 = r24
            r1 = r22
            java.lang.StringBuilder r22 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x099f, IOException -> 0x09a5 }
            java.lang.String r22 = r22.toString()     // Catch:{ MalformedURLException -> 0x099f, IOException -> 0x09a5 }
            r0 = r23
            r1 = r22
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x099f, IOException -> 0x09a5 }
            java.net.URLConnection r22 = r23.openConnection()     // Catch:{ MalformedURLException -> 0x099f, IOException -> 0x09a5 }
            r0 = r22
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x099f, IOException -> 0x09a5 }
            r21 = r0
            java.lang.String r22 = "GET"
            r21.setRequestMethod(r22)     // Catch:{ MalformedURLException -> 0x099f, IOException -> 0x09a5 }
        L_0x0819:
            cn.com.mzba.oauth.OAuth r22 = cn.com.mzba.service.StatusHttpUtils.oauthNetease
            java.lang.String r23 = cn.com.mzba.service.StatusHttpUtils.userTokenNetease
            java.lang.String r24 = cn.com.mzba.service.StatusHttpUtils.userTokenNeteaseSecret
            r0 = r22
            r1 = r23
            r2 = r24
            r3 = r21
            java.net.HttpURLConnection r25 = r0.signRequest(r1, r2, r3)
            if (r25 == 0) goto L_0x00d8
            r21 = 200(0xc8, float:2.8E-43)
            int r22 = r25.getResponseCode()     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            r0 = r21
            r1 = r22
            if (r0 != r1) goto L_0x00d8
            java.io.InputStream r22 = r25.getInputStream()     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            java.io.BufferedReader r24 = new java.io.BufferedReader     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            java.io.InputStreamReader r21 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            r21.<init>(r22)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            r0 = r24
            r1 = r21
            r0.<init>(r1)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            r21.<init>()     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            r23 = 4000(0xfa0, float:5.605E-42)
            r0 = r23
            char[] r0 = new char[r0]     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            r5 = r0
            r23 = 0
        L_0x0859:
            r0 = r24
            r1 = r5
            int r23 = r0.read(r1)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            r6 = -1
            r0 = r23
            r1 = r6
            if (r0 != r1) goto L_0x09ab
            r24.close()     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            r22.close()     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            java.lang.String r21 = r21.toString()     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            r25.disconnect()     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            org.json.JSONArray r22 = new org.json.JSONArray     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            r0 = r22
            r1 = r21
            r0.<init>(r1)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            int r21 = r22.length()     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            r0 = r21
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            r25 = r0
            r21 = 0
            r23 = r21
        L_0x088a:
            int r21 = r22.length()     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            r0 = r23
            r1 = r21
            if (r0 >= r1) goto L_0x00d8
            cn.com.mzba.model.Status r7 = new cn.com.mzba.model.Status     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            r7.<init>()     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            cn.com.mzba.model.User r14 = new cn.com.mzba.model.User     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            r14.<init>()     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            r24 = 0
            org.json.JSONObject r21 = r22.getJSONObject(r23)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            if (r21 == 0) goto L_0x09c3
            java.lang.String r5 = "user"
            r0 = r21
            r1 = r5
            org.json.JSONObject r8 = r0.getJSONObject(r1)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            java.lang.String r5 = "root_in_reply_to_status_text"
            r0 = r21
            r1 = r5
            java.lang.String r5 = r0.getString(r1)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            java.lang.String r6 = "null"
            boolean r6 = r5.equals(r6)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            if (r6 != 0) goto L_0x0906
            cn.com.mzba.model.Status r24 = new cn.com.mzba.model.Status     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            r24.<init>()     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            r6 = 1
            r7.setRetweetedStatus(r6)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            java.lang.String r6 = "root_in_reply_to_user_name"
            r0 = r21
            r1 = r6
            java.lang.String r6 = r0.getString(r1)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            java.lang.String r9 = "root_in_reply_to_user_id"
            r0 = r21
            r1 = r9
            java.lang.String r9 = r0.getString(r1)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            r0 = r24
            r1 = r9
            r0.setId(r1)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            java.lang.String r10 = "@"
            r9.<init>(r10)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            java.lang.StringBuilder r6 = r9.append(r6)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            java.lang.String r9 = ":"
            java.lang.StringBuilder r6 = r6.append(r9)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            java.lang.StringBuilder r5 = r6.append(r5)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            java.lang.String r5 = r5.toString()     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            r0 = r24
            r1 = r5
            r0.setText(r1)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            r0 = r7
            r1 = r24
            r0.setStatus(r1)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
        L_0x0906:
            r5 = r24
            java.lang.String r24 = "id"
            r0 = r21
            r1 = r24
            java.lang.String r24 = r0.getString(r1)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            r25[r23] = r24     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            java.lang.String r6 = "id"
            java.lang.String r12 = r8.getString(r6)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            java.lang.String r6 = "screen_name"
            java.lang.String r13 = r8.getString(r6)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            java.lang.String r6 = "profile_image_url"
            java.lang.String r10 = r8.getString(r6)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            java.lang.String r6 = "created_at"
            r0 = r21
            r1 = r6
            java.lang.String r9 = r0.getString(r1)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            java.lang.String r6 = "text"
            r0 = r21
            r1 = r6
            java.lang.String r8 = r0.getString(r1)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            java.lang.String r6 = "source"
            r0 = r21
            r1 = r6
            java.lang.String r6 = r0.getString(r1)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            r0 = r7
            r1 = r24
            r0.setId(r1)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            r7.setText(r8)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            r7.setCreatedAt(r9)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            java.lang.StringBuilder r24 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            java.lang.String r8 = "来自："
            r0 = r24
            r1 = r8
            r0.<init>(r1)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            r0 = r24
            r1 = r6
            java.lang.StringBuilder r24 = r0.append(r1)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            java.lang.String r24 = r24.toString()     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            r0 = r7
            r1 = r24
            r0.setSource(r1)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            r14.setId(r12)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            r14.setProfileImageUrl(r10)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            r14.setScreenName(r13)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            r7.setUser(r14)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            java.lang.String r24 = "comments_count"
            r0 = r21
            r1 = r24
            java.lang.String r24 = r0.getString(r1)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            r0 = r7
            r1 = r24
            r0.setComments(r1)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            java.lang.String r24 = "retweet_count"
            r0 = r21
            r1 = r24
            java.lang.String r21 = r0.getString(r1)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            r0 = r7
            r1 = r21
            r0.setRt(r1)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            r11.add(r7)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            r21 = r5
        L_0x0999:
            int r21 = r23 + 1
            r23 = r21
            goto L_0x088a
        L_0x099f:
            r22 = move-exception
            r22.printStackTrace()
            goto L_0x0819
        L_0x09a5:
            r22 = move-exception
            r22.printStackTrace()
            goto L_0x0819
        L_0x09ab:
            r6 = 0
            r0 = r21
            r1 = r5
            r2 = r6
            r3 = r23
            r0.append(r1, r2, r3)     // Catch:{ IOException -> 0x09b7, JSONException -> 0x09bd }
            goto L_0x0859
        L_0x09b7:
            r21 = move-exception
            r21.printStackTrace()
            goto L_0x00d8
        L_0x09bd:
            r21 = move-exception
            r21.printStackTrace()
            goto L_0x00d8
        L_0x09c3:
            r21 = r24
            goto L_0x0999
        L_0x09c6:
            r22 = r23
            goto L_0x07ec
        L_0x09ca:
            r22 = r10
            goto L_0x070e
        L_0x09ce:
            r22 = r25
            goto L_0x0753
        L_0x09d2:
            r10 = r22
            goto L_0x0438
        L_0x09d6:
            r6 = r22
            goto L_0x03ac
        L_0x09da:
            r22 = r10
            goto L_0x01df
        L_0x09de:
            r20 = r6
            r6 = r25
            r25 = r20
            goto L_0x0189
        L_0x09e6:
            r22 = r25
            goto L_0x0224
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.com.mzba.service.StatusHttpUtils.getFavoritedTimeLines(java.lang.String, java.lang.String, int, int, java.lang.String):java.util.List");
    }

    /* JADX INFO: Multiple debug info for r8v11 java.lang.String: [D('builder' java.lang.StringBuilder), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r9v6 org.json.JSONArray: [D('is' java.io.InputStream), D('datas' org.json.JSONArray)] */
    /* JADX INFO: Multiple debug info for r1v10 int: [D('len' int), D('i' int)] */
    /* JADX INFO: Multiple debug info for r9v8 java.lang.String: [D('params' java.lang.String), D('ids' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r1v17 java.lang.String: [D('parameter' java.lang.String), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r2v10 org.json.JSONArray: [D('data' org.json.JSONObject), D('object' org.json.JSONArray)] */
    /* JADX INFO: Multiple debug info for r3v11 java.lang.String: [D('reCount' java.lang.String), D('re' org.json.JSONObject)] */
    /* JADX INFO: Multiple debug info for r8v21 java.util.ArrayList: [D('weiboId' java.lang.String), D('params' java.util.List<org.apache.http.message.BasicNameValuePair>)] */
    /* JADX INFO: Multiple debug info for r3v18 org.apache.http.HttpResponse: [D('url' java.lang.String), D('response' org.apache.http.HttpResponse)] */
    /* JADX INFO: Multiple debug info for r8v27 java.lang.String: [D('builder' java.lang.StringBuilder), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r1v27 int: [D('len' int), D('i' int)] */
    /* JADX WARN: Type inference failed for: r9v11, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List<cn.com.mzba.model.StatusCount> getCounts(java.lang.String r8, java.lang.String r9) {
        /*
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>()
            java.lang.String r1 = "sina_weibo"
            boolean r1 = r8.equals(r1)
            if (r1 == 0) goto L_0x00bb
            java.lang.String r3 = "http://api.t.sina.com.cn/statuses/counts.json"
            java.util.ArrayList r8 = new java.util.ArrayList
            r8.<init>()
            org.apache.http.message.BasicNameValuePair r1 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r2 = "ids"
            r1.<init>(r2, r9)
            r8.add(r1)
            cn.com.mzba.oauth.OAuth r9 = cn.com.mzba.service.StatusHttpUtils.oauthSina
            java.lang.String r1 = cn.com.mzba.service.StatusHttpUtils.userTokenSina
            java.lang.String r2 = cn.com.mzba.service.StatusHttpUtils.userTokenSinaSecret
            org.apache.http.HttpResponse r3 = r9.signRequest(r1, r2, r3, r8)
            if (r3 == 0) goto L_0x0080
            r8 = 200(0xc8, float:2.8E-43)
            org.apache.http.StatusLine r9 = r3.getStatusLine()
            int r9 = r9.getStatusCode()
            if (r8 != r9) goto L_0x0080
            org.apache.http.HttpEntity r8 = r3.getEntity()     // Catch:{ Exception -> 0x0086 }
            java.io.InputStream r9 = r8.getContent()     // Catch:{ Exception -> 0x0086 }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0086 }
            java.io.InputStreamReader r8 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0086 }
            r8.<init>(r9)     // Catch:{ Exception -> 0x0086 }
            r2.<init>(r8)     // Catch:{ Exception -> 0x0086 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0086 }
            org.apache.http.HttpEntity r1 = r3.getEntity()     // Catch:{ Exception -> 0x0086 }
            long r4 = r1.getContentLength()     // Catch:{ Exception -> 0x0086 }
            int r1 = (int) r4     // Catch:{ Exception -> 0x0086 }
            r8.<init>(r1)     // Catch:{ Exception -> 0x0086 }
            r1 = 4000(0xfa0, float:5.605E-42)
            char[] r4 = new char[r1]     // Catch:{ Exception -> 0x0086 }
            r1 = 0
        L_0x005b:
            int r1 = r2.read(r4)     // Catch:{ Exception -> 0x0086 }
            r5 = -1
            if (r1 != r5) goto L_0x0081
            r2.close()     // Catch:{ Exception -> 0x0086 }
            r9.close()     // Catch:{ Exception -> 0x0086 }
            java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x0086 }
            org.apache.http.HttpEntity r9 = r3.getEntity()     // Catch:{ Exception -> 0x0086 }
            r9.consumeContent()     // Catch:{ Exception -> 0x0086 }
            org.json.JSONArray r9 = new org.json.JSONArray     // Catch:{ Exception -> 0x0086 }
            r9.<init>(r8)     // Catch:{ Exception -> 0x0086 }
            r8 = 0
            r1 = r8
        L_0x007a:
            int r8 = r9.length()     // Catch:{ Exception -> 0x0086 }
            if (r1 < r8) goto L_0x008b
        L_0x0080:
            return r7
        L_0x0081:
            r5 = 0
            r8.append(r4, r5, r1)     // Catch:{ Exception -> 0x0086 }
            goto L_0x005b
        L_0x0086:
            r8 = move-exception
            r8.printStackTrace()
            goto L_0x0080
        L_0x008b:
            cn.com.mzba.model.StatusCount r2 = new cn.com.mzba.model.StatusCount     // Catch:{ Exception -> 0x0086 }
            r2.<init>()     // Catch:{ Exception -> 0x0086 }
            org.json.JSONObject r8 = r9.getJSONObject(r1)     // Catch:{ Exception -> 0x0086 }
            java.lang.String r3 = "comments"
            java.lang.String r3 = r8.getString(r3)     // Catch:{ Exception -> 0x0086 }
            r2.setComments(r3)     // Catch:{ Exception -> 0x0086 }
            java.lang.String r3 = "id"
            java.lang.String r3 = r8.getString(r3)     // Catch:{ Exception -> 0x0086 }
            r2.setId(r3)     // Catch:{ Exception -> 0x0086 }
            java.lang.String r3 = "rt"
            java.lang.String r8 = r8.getString(r3)     // Catch:{ Exception -> 0x0086 }
            r2.setRt(r8)     // Catch:{ Exception -> 0x0086 }
            java.lang.String r8 = "sina_weibo"
            r2.setWeiboId(r8)     // Catch:{ Exception -> 0x0086 }
            r7.add(r2)     // Catch:{ Exception -> 0x0086 }
            int r8 = r1 + 1
            r1 = r8
            goto L_0x007a
        L_0x00bb:
            java.lang.String r1 = "tencent_weibo"
            boolean r1 = r8.equals(r1)
            if (r1 == 0) goto L_0x0144
            java.lang.String r3 = "http://open.t.qq.com/api/t/re_count"
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            cn.com.mzba.oauth.Parameter r1 = new cn.com.mzba.oauth.Parameter
            java.lang.String r2 = "ids"
            r1.<init>(r2, r9)
            r6.add(r1)
            cn.com.mzba.oauth.Parameter r1 = new cn.com.mzba.oauth.Parameter
            java.lang.String r2 = "Flag"
            java.lang.String r4 = "2"
            r1.<init>(r2, r4)
            r6.add(r1)
            cn.com.mzba.oauth.OAuth r1 = cn.com.mzba.service.StatusHttpUtils.oauthTencent     // Catch:{ Exception -> 0x013e }
            java.lang.String r2 = "GET"
            java.lang.String r4 = cn.com.mzba.service.StatusHttpUtils.userTokenTencent     // Catch:{ Exception -> 0x013e }
            java.lang.String r5 = cn.com.mzba.service.StatusHttpUtils.userTokenTencentSecret     // Catch:{ Exception -> 0x013e }
            java.lang.String r1 = r1.getPostParameter(r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x013e }
            cn.com.mzba.oauth.OAuth r2 = cn.com.mzba.service.StatusHttpUtils.oauthTencent     // Catch:{ Exception -> 0x013e }
            java.lang.String r1 = r2.httpGet(r3, r1)     // Catch:{ Exception -> 0x013e }
            if (r1 == 0) goto L_0x0080
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ Exception -> 0x013e }
            r2.<init>(r1)     // Catch:{ Exception -> 0x013e }
            java.lang.String r1 = "data"
            org.json.JSONArray r2 = r2.getJSONArray(r1)     // Catch:{ Exception -> 0x013e }
            java.lang.String[] r1 = cn.com.mzba.service.StringUtil.StringToArray(r9)     // Catch:{ Exception -> 0x013e }
            r9 = 0
        L_0x0104:
            int r3 = r2.length()     // Catch:{ Exception -> 0x013e }
            if (r9 >= r3) goto L_0x0080
            org.json.JSONObject r3 = r2.getJSONObject(r9)     // Catch:{ Exception -> 0x013e }
            cn.com.mzba.model.StatusCount r5 = new cn.com.mzba.model.StatusCount     // Catch:{ Exception -> 0x013e }
            r5.<init>()     // Catch:{ Exception -> 0x013e }
            r4 = r1[r9]     // Catch:{ Exception -> 0x013e }
            java.lang.String r3 = r3.getString(r4)     // Catch:{ Exception -> 0x013e }
            org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ Exception -> 0x013e }
            r4.<init>(r3)     // Catch:{ Exception -> 0x013e }
            java.lang.String r3 = "mcount"
            java.lang.String r3 = r4.getString(r3)     // Catch:{ Exception -> 0x013e }
            r5.setComments(r3)     // Catch:{ Exception -> 0x013e }
            java.lang.String r3 = "count"
            java.lang.String r3 = r4.getString(r3)     // Catch:{ Exception -> 0x013e }
            r5.setRt(r3)     // Catch:{ Exception -> 0x013e }
            r3 = r1[r9]     // Catch:{ Exception -> 0x013e }
            r5.setId(r3)     // Catch:{ Exception -> 0x013e }
            r5.setWeiboId(r8)     // Catch:{ Exception -> 0x013e }
            r7.add(r5)     // Catch:{ Exception -> 0x013e }
            int r9 = r9 + 1
            goto L_0x0104
        L_0x013e:
            r8 = move-exception
            r8.printStackTrace()
            goto L_0x0080
        L_0x0144:
            java.lang.String r1 = "sohu_weibo"
            boolean r8 = r8.equals(r1)
            if (r8 == 0) goto L_0x0080
            java.lang.String r3 = "http://api.t.sohu.com/statuses/counts.json"
            r8 = 0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0202, IOException -> 0x0208 }
            java.lang.String r2 = "?ids="
            r1.<init>(r2)     // Catch:{ MalformedURLException -> 0x0202, IOException -> 0x0208 }
            java.lang.StringBuilder r9 = r1.append(r9)     // Catch:{ MalformedURLException -> 0x0202, IOException -> 0x0208 }
            java.lang.String r9 = r9.toString()     // Catch:{ MalformedURLException -> 0x0202, IOException -> 0x0208 }
            java.net.URL r1 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0202, IOException -> 0x0208 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0202, IOException -> 0x0208 }
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ MalformedURLException -> 0x0202, IOException -> 0x0208 }
            r2.<init>(r3)     // Catch:{ MalformedURLException -> 0x0202, IOException -> 0x0208 }
            java.lang.StringBuilder r9 = r2.append(r9)     // Catch:{ MalformedURLException -> 0x0202, IOException -> 0x0208 }
            java.lang.String r9 = r9.toString()     // Catch:{ MalformedURLException -> 0x0202, IOException -> 0x0208 }
            r1.<init>(r9)     // Catch:{ MalformedURLException -> 0x0202, IOException -> 0x0208 }
            java.net.URLConnection r9 = r1.openConnection()     // Catch:{ MalformedURLException -> 0x0202, IOException -> 0x0208 }
            r0 = r9
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x0202, IOException -> 0x0208 }
            r8 = r0
            java.lang.String r9 = "GET"
            r8.setRequestMethod(r9)     // Catch:{ MalformedURLException -> 0x0202, IOException -> 0x0208 }
        L_0x0181:
            cn.com.mzba.oauth.OAuth r9 = cn.com.mzba.service.StatusHttpUtils.oauthSohu
            java.lang.String r1 = cn.com.mzba.service.StatusHttpUtils.userTokenSohu
            java.lang.String r2 = cn.com.mzba.service.StatusHttpUtils.userTokenSohuSecret
            java.net.HttpURLConnection r3 = r9.signRequest(r1, r2, r8)
            if (r3 == 0) goto L_0x0080
            r8 = 200(0xc8, float:2.8E-43)
            int r9 = r3.getResponseCode()     // Catch:{ IOException -> 0x0213, JSONException -> 0x0219 }
            if (r8 != r9) goto L_0x0080
            java.io.InputStream r9 = r3.getInputStream()     // Catch:{ IOException -> 0x0213, JSONException -> 0x0219 }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0213, JSONException -> 0x0219 }
            java.io.InputStreamReader r8 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0213, JSONException -> 0x0219 }
            r8.<init>(r9)     // Catch:{ IOException -> 0x0213, JSONException -> 0x0219 }
            r2.<init>(r8)     // Catch:{ IOException -> 0x0213, JSONException -> 0x0219 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0213, JSONException -> 0x0219 }
            int r1 = r3.getContentLength()     // Catch:{ IOException -> 0x0213, JSONException -> 0x0219 }
            r8.<init>(r1)     // Catch:{ IOException -> 0x0213, JSONException -> 0x0219 }
            r1 = 4000(0xfa0, float:5.605E-42)
            char[] r4 = new char[r1]     // Catch:{ IOException -> 0x0213, JSONException -> 0x0219 }
            r1 = 0
        L_0x01b1:
            int r1 = r2.read(r4)     // Catch:{ IOException -> 0x0213, JSONException -> 0x0219 }
            r5 = -1
            if (r1 != r5) goto L_0x020e
            r2.close()     // Catch:{ IOException -> 0x0213, JSONException -> 0x0219 }
            r9.close()     // Catch:{ IOException -> 0x0213, JSONException -> 0x0219 }
            java.lang.String r8 = r8.toString()     // Catch:{ IOException -> 0x0213, JSONException -> 0x0219 }
            r3.disconnect()     // Catch:{ IOException -> 0x0213, JSONException -> 0x0219 }
            org.json.JSONArray r9 = new org.json.JSONArray     // Catch:{ IOException -> 0x0213, JSONException -> 0x0219 }
            r9.<init>(r8)     // Catch:{ IOException -> 0x0213, JSONException -> 0x0219 }
            r8 = 0
            r1 = r8
        L_0x01cc:
            int r8 = r9.length()     // Catch:{ IOException -> 0x0213, JSONException -> 0x0219 }
            if (r1 >= r8) goto L_0x0080
            cn.com.mzba.model.StatusCount r2 = new cn.com.mzba.model.StatusCount     // Catch:{ IOException -> 0x0213, JSONException -> 0x0219 }
            r2.<init>()     // Catch:{ IOException -> 0x0213, JSONException -> 0x0219 }
            org.json.JSONObject r8 = r9.getJSONObject(r1)     // Catch:{ IOException -> 0x0213, JSONException -> 0x0219 }
            java.lang.String r3 = "comments_count"
            java.lang.String r3 = r8.getString(r3)     // Catch:{ IOException -> 0x0213, JSONException -> 0x0219 }
            r2.setComments(r3)     // Catch:{ IOException -> 0x0213, JSONException -> 0x0219 }
            java.lang.String r3 = "id"
            java.lang.String r3 = r8.getString(r3)     // Catch:{ IOException -> 0x0213, JSONException -> 0x0219 }
            r2.setId(r3)     // Catch:{ IOException -> 0x0213, JSONException -> 0x0219 }
            java.lang.String r3 = "transmit_count"
            java.lang.String r8 = r8.getString(r3)     // Catch:{ IOException -> 0x0213, JSONException -> 0x0219 }
            r2.setRt(r8)     // Catch:{ IOException -> 0x0213, JSONException -> 0x0219 }
            java.lang.String r8 = "sohu_weibo"
            r2.setWeiboId(r8)     // Catch:{ IOException -> 0x0213, JSONException -> 0x0219 }
            r7.add(r2)     // Catch:{ IOException -> 0x0213, JSONException -> 0x0219 }
            int r8 = r1 + 1
            r1 = r8
            goto L_0x01cc
        L_0x0202:
            r9 = move-exception
            r9.printStackTrace()
            goto L_0x0181
        L_0x0208:
            r9 = move-exception
            r9.printStackTrace()
            goto L_0x0181
        L_0x020e:
            r5 = 0
            r8.append(r4, r5, r1)     // Catch:{ IOException -> 0x0213, JSONException -> 0x0219 }
            goto L_0x01b1
        L_0x0213:
            r8 = move-exception
            r8.printStackTrace()
            goto L_0x0080
        L_0x0219:
            r8 = move-exception
            r8.printStackTrace()
            goto L_0x0080
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.com.mzba.service.StatusHttpUtils.getCounts(java.lang.String, java.lang.String):java.util.List");
    }

    public static UnReadInfo getUnReadInfo() {
        UnReadInfo unread = new UnReadInfo();
        HttpResponse response = ServiceUtils.getSinaOauth().signRequest(userTokenSina, userTokenSinaSecret, "http://api.t.sina.com.cn/statuses/unread.json", new ArrayList<>());
        if (response != null && 200 == response.getStatusLine().getStatusCode()) {
            try {
                InputStream is = response.getEntity().getContent();
                Reader reader = new BufferedReader(new InputStreamReader(is));
                StringBuilder builder = new StringBuilder((int) response.getEntity().getContentLength());
                char[] temp = new char[4000];
                while (true) {
                    int len = reader.read(temp);
                    if (len == -1) {
                        break;
                    }
                    builder.append(temp, 0, len);
                }
                reader.close();
                is.close();
                String content = builder.toString();
                response.getEntity().consumeContent();
                JSONObject data = new JSONObject(content);
                if (data != null) {
                    unread.setComments(data.getInt("comments"));
                    unread.setDm(data.getInt("dm"));
                    unread.setFollowers(data.getInt(MyAction.FOLLOWERS));
                    unread.setMentions(data.getInt("mentions"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return unread;
    }

    public static void resetCount(int type) {
        OAuth oauth2 = ServiceUtils.getSinaOauth();
        List<BasicNameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("page", String.valueOf(type)));
        oauth2.signRequest(userTokenSina, userTokenSinaSecret, "http://api.t.sina.com.cn/statuses/reset_count.json", params);
    }

    /* JADX INFO: Multiple debug info for r0v1 java.io.InputStream: [D('is' java.io.InputStream), D('params' java.util.List<org.apache.http.message.BasicNameValuePair>)] */
    /* JADX INFO: Multiple debug info for r7v9 java.lang.String: [D('buf' char[]), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r1v3 int: [D('len' int), D('i' int)] */
    /* JADX INFO: Multiple debug info for r7v13 org.json.JSONObject: [D('e' java.lang.Exception), D('data' org.json.JSONObject)] */
    public static List<Emotions> loadEmotions(String token, String tokenSecret) {
        List<Emotions> list = new ArrayList<>();
        List<BasicNameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair(Emotions.TYPE, "face"));
        HttpResponse response = oauthSina.signRequest(token, tokenSecret, "http://api.t.sina.com.cn/emotions.json", params);
        if (response != null && 200 == response.getStatusLine().getStatusCode()) {
            try {
                InputStream is = response.getEntity().getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                StringBuilder builder = new StringBuilder((int) response.getEntity().getContentLength());
                char[] buf = new char[4000];
                while (true) {
                    int len = reader.read(buf);
                    if (len == -1) {
                        break;
                    }
                    builder.append(buf, 0, len);
                }
                reader.close();
                is.close();
                String content = builder.toString();
                response.getEntity().consumeContent();
                JSONArray datas = new JSONArray(content);
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 >= datas.length()) {
                        break;
                    }
                    JSONObject data = datas.getJSONObject(i2);
                    Emotions emotions = new Emotions();
                    String imageUrl = data.getString(Emotions.URL);
                    emotions.setImageName(imageUrl.substring(imageUrl.lastIndexOf(CookieSpec.PATH_DELIM) + 1, imageUrl.length() - 4));
                    emotions.setPhrase(data.getString(Emotions.PHRASE));
                    emotions.setUrl(imageUrl);
                    emotions.setType(data.getString(Emotions.TYPE));
                    emotions.setCategory(data.getString(Emotions.CATEGORY));
                    list.add(emotions);
                    i = i2 + 1;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    /* JADX INFO: Multiple debug info for r19v3 cn.com.mzba.model.Status: [D('rtStatus' cn.com.mzba.model.Status), D('status' cn.com.mzba.model.Status)] */
    /* JADX INFO: Multiple debug info for r19v6 cn.com.mzba.model.Status: [D('rtStatus' cn.com.mzba.model.Status), D('status' cn.com.mzba.model.Status)] */
    /* JADX INFO: Multiple debug info for r3v21 java.lang.String: [D('builder' java.lang.StringBuilder), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r4v10 org.json.JSONObject: [D('data' org.json.JSONObject), D('is' java.io.InputStream)] */
    /* JADX INFO: Multiple debug info for r7v1 cn.com.mzba.model.Status: [D('response' java.net.HttpURLConnection), D('status' cn.com.mzba.model.Status)] */
    /* JADX INFO: Multiple debug info for r6v2 java.lang.String: [D('reader' java.io.Reader), D('transmitText' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r10v2 java.lang.String: [D('userIcon' java.lang.String), D('userObject' org.json.JSONObject)] */
    /* JADX INFO: Multiple debug info for r8v2 java.lang.String: [D('temp' char[]), D('transmitUserName' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r19v23 cn.com.mzba.model.Status: [D('rtStatus' cn.com.mzba.model.Status), D('status' cn.com.mzba.model.Status)] */
    /* JADX INFO: Multiple debug info for r19v26 cn.com.mzba.model.Status: [D('rtStatus' cn.com.mzba.model.Status), D('status' cn.com.mzba.model.Status)] */
    /* JADX INFO: Multiple debug info for r3v60 java.lang.String: [D('builder' java.lang.StringBuilder), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r4v21 org.json.JSONObject: [D('data' org.json.JSONObject), D('is' java.io.InputStream)] */
    /* JADX INFO: Multiple debug info for r19v30 java.util.List<cn.com.mzba.model.StatusCount>: [D('counts' java.util.List<cn.com.mzba.model.StatusCount>), D('id' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r19v31 cn.com.mzba.model.Status: [D('counts' java.util.List<cn.com.mzba.model.StatusCount>), D('status' cn.com.mzba.model.Status)] */
    /* JADX INFO: Multiple debug info for r19v32 cn.com.mzba.model.Status: [D('counts' java.util.List<cn.com.mzba.model.StatusCount>), D('status' cn.com.mzba.model.Status)] */
    /* JADX INFO: Multiple debug info for r6v8 cn.com.mzba.model.Status: [D('reader' java.io.Reader), D('status' cn.com.mzba.model.Status)] */
    /* JADX INFO: Multiple debug info for r7v3 java.lang.String: [D('response' java.net.HttpURLConnection), D('transmitText' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r4v24 java.lang.String: [D('data' org.json.JSONObject), D('thumbnail_pic' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r5v28 cn.com.mzba.model.Status: [D('rtStatus' cn.com.mzba.model.Status), D('len' int)] */
    /* JADX INFO: Multiple debug info for r8v7 java.lang.String: [D('temp' char[]), D('transmitUserName' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r3v81 java.util.List<cn.com.mzba.model.StatusCount>: [D('transmitId' java.lang.String), D('counts' java.util.List<cn.com.mzba.model.StatusCount>)] */
    /* JADX INFO: Multiple debug info for r19v40 cn.com.mzba.model.Status: [D('rtStatus' cn.com.mzba.model.Status), D('status' cn.com.mzba.model.Status)] */
    /* JADX INFO: Multiple debug info for r19v42 java.lang.String: [D('parameter' java.lang.String), D('id' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r19v43 java.lang.String: [D('parameter' java.lang.String), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r3v93 org.json.JSONObject: [D('object' org.json.JSONObject), D('data' org.json.JSONObject)] */
    /* JADX INFO: Multiple debug info for r3v96 java.lang.String: [D('object' org.json.JSONObject), D('redirectCount' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r19v61 cn.com.mzba.model.Status: [D('commentCount' java.lang.String), D('status' cn.com.mzba.model.Status)] */
    /* JADX INFO: Multiple debug info for r5v37 cn.com.mzba.model.Status: [D('rtStatus' cn.com.mzba.model.Status), D('url' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r4v38 java.lang.String: [D('rtName' java.lang.String), D('source' org.json.JSONObject)] */
    /* JADX INFO: Multiple debug info for r19v74 cn.com.mzba.model.Status: [D('rtStatus' cn.com.mzba.model.Status), D('status' cn.com.mzba.model.Status)] */
    /* JADX INFO: Multiple debug info for r5v39 java.io.InputStream: [D('is' java.io.InputStream), D('url' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r3v107 java.lang.String: [D('content' java.lang.String), D('buffer' char[])] */
    /* JADX INFO: Multiple debug info for r6v19 cn.com.mzba.model.Status: [D('status' cn.com.mzba.model.Status), D('len' int)] */
    /* JADX INFO: Multiple debug info for r5v40 org.json.JSONObject: [D('is' java.io.InputStream), D('userObject' org.json.JSONObject)] */
    /* JADX INFO: Multiple debug info for r11v1 java.lang.String: [D('user' cn.com.mzba.model.User), D('time' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r19v77 java.util.List<cn.com.mzba.model.StatusCount>: [D('sc' java.util.List<cn.com.mzba.model.StatusCount>), D('id' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r5v43 cn.com.mzba.model.Status: [D('rtStatus' cn.com.mzba.model.Status), D('source' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r4v50 org.json.JSONObject: [D('data' org.json.JSONObject), D('user_retweeted' org.json.JSONObject)] */
    /* JADX INFO: Multiple debug info for r4v51 java.lang.String: [D('user_retweeted' org.json.JSONObject), D('transmitUserName' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r18v53 java.util.List<cn.com.mzba.model.StatusCount>: [D('weiboId' java.lang.String), D('rtSc' java.util.List<cn.com.mzba.model.StatusCount>)] */
    /* JADX INFO: Multiple debug info for r19v92 java.lang.String: [D('retweeted' org.json.JSONObject), D('transmitPic2' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r7v30 java.lang.String: [D('reader' java.io.BufferedReader), D('userName' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r5v44 java.lang.String: [D('userId' java.lang.String), D('userObject' org.json.JSONObject)] */
    /* JADX WARN: Type inference failed for: r4v11, types: [java.net.URLConnection] */
    /* JADX WARN: Type inference failed for: r4v29, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static cn.com.mzba.model.Status getStatusById(java.lang.String r18, java.lang.String r19) {
        /*
            r10 = 0
            r9 = 0
            cn.com.mzba.model.User r11 = new cn.com.mzba.model.User
            r11.<init>()
            cn.com.mzba.oauth.OAuth r3 = cn.com.mzba.service.ServiceUtils.getSinaOauth()
            java.lang.String r4 = "sina_weibo"
            r0 = r18
            r1 = r4
            boolean r4 = r0.equals(r1)
            if (r4 == 0) goto L_0x01f3
            java.lang.String r5 = "http://api.t.sina.com.cn/statuses/show/:id.json"
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            org.apache.http.message.BasicNameValuePair r6 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r7 = "id"
            r0 = r6
            r1 = r7
            r2 = r19
            r0.<init>(r1, r2)
            r4.add(r6)
            java.lang.String r6 = cn.com.mzba.service.StatusHttpUtils.userTokenSina
            java.lang.String r7 = cn.com.mzba.service.StatusHttpUtils.userTokenSinaSecret
            org.apache.http.HttpResponse r8 = r3.signRequest(r6, r7, r5, r4)
            if (r8 == 0) goto L_0x078c
            r3 = 200(0xc8, float:2.8E-43)
            org.apache.http.StatusLine r4 = r8.getStatusLine()
            int r4 = r4.getStatusCode()
            if (r3 != r4) goto L_0x078c
            org.apache.http.HttpEntity r3 = r8.getEntity()     // Catch:{ Exception -> 0x01e7 }
            java.io.InputStream r5 = r3.getContent()     // Catch:{ Exception -> 0x01e7 }
            java.io.BufferedReader r7 = new java.io.BufferedReader     // Catch:{ Exception -> 0x01e7 }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x01e7 }
            r3.<init>(r5)     // Catch:{ Exception -> 0x01e7 }
            r7.<init>(r3)     // Catch:{ Exception -> 0x01e7 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01e7 }
            org.apache.http.HttpEntity r3 = r8.getEntity()     // Catch:{ Exception -> 0x01e7 }
            long r12 = r3.getContentLength()     // Catch:{ Exception -> 0x01e7 }
            int r3 = (int) r12     // Catch:{ Exception -> 0x01e7 }
            r4.<init>(r3)     // Catch:{ Exception -> 0x01e7 }
            r3 = 4000(0xfa0, float:5.605E-42)
            char[] r3 = new char[r3]     // Catch:{ Exception -> 0x01e7 }
            r6 = 0
        L_0x0066:
            int r6 = r7.read(r3)     // Catch:{ Exception -> 0x01e7 }
            r12 = -1
            if (r6 != r12) goto L_0x01e1
            r7.close()     // Catch:{ Exception -> 0x01e7 }
            r5.close()     // Catch:{ Exception -> 0x01e7 }
            java.lang.String r3 = r4.toString()     // Catch:{ Exception -> 0x01e7 }
            org.apache.http.HttpEntity r4 = r8.getEntity()     // Catch:{ Exception -> 0x01e7 }
            r4.consumeContent()     // Catch:{ Exception -> 0x01e7 }
            org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ Exception -> 0x01e7 }
            r4.<init>(r3)     // Catch:{ Exception -> 0x01e7 }
            if (r4 == 0) goto L_0x078c
            cn.com.mzba.model.Status r6 = new cn.com.mzba.model.Status     // Catch:{ Exception -> 0x01e7 }
            r6.<init>()     // Catch:{ Exception -> 0x01e7 }
            java.lang.String r3 = "user"
            org.json.JSONObject r5 = r4.getJSONObject(r3)     // Catch:{ Exception -> 0x077d }
            if (r11 == 0) goto L_0x00b0
            java.lang.String r3 = "screen_name"
            java.lang.String r7 = r5.getString(r3)     // Catch:{ Exception -> 0x077d }
            java.lang.String r3 = "profile_image_url"
            java.lang.String r3 = r5.getString(r3)     // Catch:{ Exception -> 0x077d }
            java.lang.String r8 = "id"
            java.lang.String r5 = r5.getString(r8)     // Catch:{ Exception -> 0x077d }
            r11.setScreenName(r7)     // Catch:{ Exception -> 0x077d }
            r11.setId(r5)     // Catch:{ Exception -> 0x077d }
            r11.setProfileImageUrl(r3)     // Catch:{ Exception -> 0x077d }
            r6.setUser(r11)     // Catch:{ Exception -> 0x077d }
        L_0x00b0:
            java.lang.String r3 = "text"
            java.lang.String r7 = r4.getString(r3)     // Catch:{ Exception -> 0x077d }
            java.lang.String r3 = "created_at"
            java.lang.String r11 = r4.getString(r3)     // Catch:{ Exception -> 0x077d }
            java.lang.String r3 = "favorited"
            boolean r3 = r4.getBoolean(r3)     // Catch:{ Exception -> 0x077d }
            java.lang.String r5 = "source"
            java.lang.String r5 = r4.getString(r5)     // Catch:{ Exception -> 0x077d }
            java.lang.String r8 = "thumbnail_pic"
            boolean r8 = r4.has(r8)     // Catch:{ Exception -> 0x077d }
            if (r8 == 0) goto L_0x00e2
            java.lang.String r8 = "thumbnail_pic"
            java.lang.String r8 = r4.getString(r8)     // Catch:{ Exception -> 0x077d }
            java.lang.String r10 = "original_pic"
            java.lang.String r10 = r4.getString(r10)     // Catch:{ Exception -> 0x077d }
            r6.setThumbnailPic(r8)     // Catch:{ Exception -> 0x077d }
            r6.setOriginalPic(r10)     // Catch:{ Exception -> 0x077d }
        L_0x00e2:
            java.util.List r19 = getCounts(r18, r19)     // Catch:{ Exception -> 0x077d }
            if (r19 == 0) goto L_0x0113
            boolean r8 = r19.isEmpty()     // Catch:{ Exception -> 0x077d }
            if (r8 != 0) goto L_0x0113
            r8 = 0
            r0 = r19
            r1 = r8
            java.lang.Object r8 = r0.get(r1)     // Catch:{ Exception -> 0x077d }
            cn.com.mzba.model.StatusCount r8 = (cn.com.mzba.model.StatusCount) r8     // Catch:{ Exception -> 0x077d }
            java.lang.String r8 = r8.getComments()     // Catch:{ Exception -> 0x077d }
            r6.setComments(r8)     // Catch:{ Exception -> 0x077d }
            r8 = 0
            r0 = r19
            r1 = r8
            java.lang.Object r19 = r0.get(r1)     // Catch:{ Exception -> 0x077d }
            cn.com.mzba.model.StatusCount r19 = (cn.com.mzba.model.StatusCount) r19     // Catch:{ Exception -> 0x077d }
            java.lang.String r19 = r19.getRt()     // Catch:{ Exception -> 0x077d }
            r0 = r6
            r1 = r19
            r0.setRt(r1)     // Catch:{ Exception -> 0x077d }
        L_0x0113:
            r6.setText(r7)     // Catch:{ Exception -> 0x077d }
            r6.setCreatedAt(r11)     // Catch:{ Exception -> 0x077d }
            r6.setSource(r5)     // Catch:{ Exception -> 0x077d }
            r6.setFavorited(r3)     // Catch:{ Exception -> 0x077d }
            java.lang.String r19 = "retweeted_status"
            r0 = r4
            r1 = r19
            boolean r19 = r0.has(r1)     // Catch:{ Exception -> 0x077d }
            if (r19 == 0) goto L_0x07a6
            r19 = 1
            r0 = r6
            r1 = r19
            r0.setRetweetedStatus(r1)     // Catch:{ Exception -> 0x077d }
            cn.com.mzba.model.Status r5 = new cn.com.mzba.model.Status     // Catch:{ Exception -> 0x077d }
            r5.<init>()     // Catch:{ Exception -> 0x077d }
            java.lang.String r19 = "retweeted_status"
            r0 = r4
            r1 = r19
            org.json.JSONObject r19 = r0.getJSONObject(r1)     // Catch:{ Exception -> 0x0783 }
            java.lang.String r3 = "user"
            r0 = r19
            r1 = r3
            org.json.JSONObject r4 = r0.getJSONObject(r1)     // Catch:{ Exception -> 0x0783 }
            java.lang.String r3 = "id"
            r0 = r19
            r1 = r3
            java.lang.String r3 = r0.getString(r1)     // Catch:{ Exception -> 0x0783 }
            java.lang.String r7 = "screen_name"
            java.lang.String r4 = r4.getString(r7)     // Catch:{ Exception -> 0x0783 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0783 }
            java.lang.String r8 = "@"
            r7.<init>(r8)     // Catch:{ Exception -> 0x0783 }
            java.lang.StringBuilder r4 = r7.append(r4)     // Catch:{ Exception -> 0x0783 }
            java.lang.String r7 = ":"
            java.lang.StringBuilder r4 = r4.append(r7)     // Catch:{ Exception -> 0x0783 }
            java.lang.String r7 = "text"
            r0 = r19
            r1 = r7
            java.lang.String r7 = r0.getString(r1)     // Catch:{ Exception -> 0x0783 }
            java.lang.StringBuilder r4 = r4.append(r7)     // Catch:{ Exception -> 0x0783 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0783 }
            r5.setText(r4)     // Catch:{ Exception -> 0x0783 }
            r5.setId(r3)     // Catch:{ Exception -> 0x0783 }
            java.lang.String r4 = "thumbnail_pic"
            r0 = r19
            r1 = r4
            boolean r4 = r0.has(r1)     // Catch:{ Exception -> 0x0783 }
            if (r4 == 0) goto L_0x01a6
            java.lang.String r4 = "thumbnail_pic"
            r0 = r19
            r1 = r4
            java.lang.String r4 = r0.getString(r1)     // Catch:{ Exception -> 0x0783 }
            java.lang.String r7 = "original_pic"
            r0 = r19
            r1 = r7
            java.lang.String r19 = r0.getString(r1)     // Catch:{ Exception -> 0x0783 }
            r5.setThumbnailPic(r4)     // Catch:{ Exception -> 0x0783 }
            r0 = r5
            r1 = r19
            r0.setOriginalPic(r1)     // Catch:{ Exception -> 0x0783 }
        L_0x01a6:
            r0 = r18
            r1 = r3
            java.util.List r18 = getCounts(r0, r1)     // Catch:{ Exception -> 0x0783 }
            if (r18 == 0) goto L_0x01d9
            boolean r19 = r18.isEmpty()     // Catch:{ Exception -> 0x0783 }
            if (r19 != 0) goto L_0x01d9
            r19 = 0
            java.lang.Object r19 = r18.get(r19)     // Catch:{ Exception -> 0x0783 }
            cn.com.mzba.model.StatusCount r19 = (cn.com.mzba.model.StatusCount) r19     // Catch:{ Exception -> 0x0783 }
            java.lang.String r19 = r19.getComments()     // Catch:{ Exception -> 0x0783 }
            r0 = r5
            r1 = r19
            r0.setComments(r1)     // Catch:{ Exception -> 0x0783 }
            r19 = 0
            java.lang.Object r18 = r18.get(r19)     // Catch:{ Exception -> 0x0783 }
            cn.com.mzba.model.StatusCount r18 = (cn.com.mzba.model.StatusCount) r18     // Catch:{ Exception -> 0x0783 }
            java.lang.String r18 = r18.getRt()     // Catch:{ Exception -> 0x0783 }
            r0 = r5
            r1 = r18
            r0.setRt(r1)     // Catch:{ Exception -> 0x0783 }
        L_0x01d9:
            r6.setStatus(r5)     // Catch:{ Exception -> 0x0783 }
            r18 = r5
            r19 = r6
        L_0x01e0:
            return r19
        L_0x01e1:
            r12 = 0
            r4.append(r3, r12, r6)     // Catch:{ Exception -> 0x01e7 }
            goto L_0x0066
        L_0x01e7:
            r18 = move-exception
            r19 = r9
            r3 = r10
        L_0x01eb:
            r18.printStackTrace()
            r18 = r19
            r19 = r3
            goto L_0x01e0
        L_0x01f3:
            java.lang.String r3 = "tencent_weibo"
            r0 = r18
            r1 = r3
            boolean r3 = r0.equals(r1)
            if (r3 == 0) goto L_0x03a8
            java.lang.String r5 = "http://open.t.qq.com/api/t/show"
            java.util.ArrayList r8 = new java.util.ArrayList
            r8.<init>()
            cn.com.mzba.oauth.Parameter r3 = new cn.com.mzba.oauth.Parameter
            java.lang.String r4 = "id"
            r0 = r3
            r1 = r4
            r2 = r19
            r0.<init>(r1, r2)
            r8.add(r3)
            cn.com.mzba.oauth.OAuth r3 = cn.com.mzba.service.StatusHttpUtils.oauthTencent     // Catch:{ Exception -> 0x039b }
            java.lang.String r4 = "GET"
            java.lang.String r6 = cn.com.mzba.service.StatusHttpUtils.userTokenTencent     // Catch:{ Exception -> 0x039b }
            java.lang.String r7 = cn.com.mzba.service.StatusHttpUtils.userTokenTencentSecret     // Catch:{ Exception -> 0x039b }
            java.lang.String r19 = r3.getPostParameter(r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x039b }
            cn.com.mzba.oauth.OAuth r3 = cn.com.mzba.service.StatusHttpUtils.oauthTencent     // Catch:{ Exception -> 0x039b }
            r0 = r3
            r1 = r5
            r2 = r19
            java.lang.String r19 = r0.httpGet(r1, r2)     // Catch:{ Exception -> 0x039b }
            if (r19 == 0) goto L_0x078c
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ Exception -> 0x039b }
            r0 = r3
            r1 = r19
            r0.<init>(r1)     // Catch:{ Exception -> 0x039b }
            java.lang.String r19 = cn.com.mzba.service.StatusHttpUtils.TAG     // Catch:{ Exception -> 0x039b }
            java.lang.String r4 = r3.toString()     // Catch:{ Exception -> 0x039b }
            r0 = r19
            r1 = r4
            android.util.Log.i(r0, r1)     // Catch:{ Exception -> 0x039b }
            java.lang.String r19 = "data"
            r0 = r3
            r1 = r19
            org.json.JSONObject r3 = r0.getJSONObject(r1)     // Catch:{ Exception -> 0x039b }
            if (r3 == 0) goto L_0x078c
            cn.com.mzba.model.Status r7 = new cn.com.mzba.model.Status     // Catch:{ Exception -> 0x039b }
            r7.<init>()     // Catch:{ Exception -> 0x039b }
            java.lang.String r19 = "type"
            r0 = r3
            r1 = r19
            int r19 = r0.getInt(r1)     // Catch:{ Exception -> 0x076b }
            r4 = 2
            r0 = r19
            r1 = r4
            if (r0 != r1) goto L_0x07a3
            java.lang.String r19 = "source"
            r0 = r3
            r1 = r19
            org.json.JSONObject r4 = r0.getJSONObject(r1)     // Catch:{ Exception -> 0x076b }
            cn.com.mzba.model.Status r5 = new cn.com.mzba.model.Status     // Catch:{ Exception -> 0x076b }
            r5.<init>()     // Catch:{ Exception -> 0x076b }
            r19 = 1
            r0 = r7
            r1 = r19
            r0.setRetweetedStatus(r1)     // Catch:{ Exception -> 0x0771 }
            java.lang.String r19 = "text"
            r0 = r4
            r1 = r19
            java.lang.String r6 = r0.getString(r1)     // Catch:{ Exception -> 0x0771 }
            java.lang.String r19 = "id"
            r0 = r4
            r1 = r19
            java.lang.String r19 = r0.getString(r1)     // Catch:{ Exception -> 0x0771 }
            java.lang.String r8 = "nick"
            java.lang.String r4 = r4.getString(r8)     // Catch:{ Exception -> 0x0771 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0771 }
            java.lang.String r9 = "@"
            r8.<init>(r9)     // Catch:{ Exception -> 0x0771 }
            java.lang.StringBuilder r4 = r8.append(r4)     // Catch:{ Exception -> 0x0771 }
            java.lang.String r8 = ":"
            java.lang.StringBuilder r4 = r4.append(r8)     // Catch:{ Exception -> 0x0771 }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ Exception -> 0x0771 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0771 }
            r5.setText(r4)     // Catch:{ Exception -> 0x0771 }
            r0 = r5
            r1 = r19
            r0.setId(r1)     // Catch:{ Exception -> 0x0771 }
            r7.setStatus(r5)     // Catch:{ Exception -> 0x0771 }
            r4 = r5
        L_0x02b2:
            java.lang.String r19 = "id"
            r0 = r3
            r1 = r19
            java.lang.String r6 = r0.getString(r1)     // Catch:{ Exception -> 0x0777 }
            java.lang.String r19 = "name"
            r0 = r3
            r1 = r19
            java.lang.String r13 = r0.getString(r1)     // Catch:{ Exception -> 0x0777 }
            java.lang.String r19 = "nick"
            r0 = r3
            r1 = r19
            java.lang.String r14 = r0.getString(r1)     // Catch:{ Exception -> 0x0777 }
            java.lang.String r19 = "head"
            r0 = r3
            r1 = r19
            java.lang.String r12 = r0.getString(r1)     // Catch:{ Exception -> 0x0777 }
            java.lang.String r19 = "timestamp"
            r0 = r3
            r1 = r19
            java.lang.String r10 = r0.getString(r1)     // Catch:{ Exception -> 0x0777 }
            java.lang.String r19 = "text"
            r0 = r3
            r1 = r19
            java.lang.String r8 = r0.getString(r1)     // Catch:{ Exception -> 0x0777 }
            java.lang.String r19 = "from"
            r0 = r3
            r1 = r19
            java.lang.String r5 = r0.getString(r1)     // Catch:{ Exception -> 0x0777 }
            java.lang.String r19 = "image"
            r0 = r3
            r1 = r19
            java.lang.String r19 = r0.getString(r1)     // Catch:{ Exception -> 0x0777 }
            if (r19 == 0) goto L_0x079f
            java.lang.String r9 = "null"
            r0 = r19
            r1 = r9
            boolean r9 = r0.equals(r1)     // Catch:{ Exception -> 0x0777 }
            if (r9 != 0) goto L_0x079f
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0777 }
            r15 = 2
            int r16 = r19.length()     // Catch:{ Exception -> 0x0777 }
            r17 = 2
            int r16 = r16 - r17
            r0 = r19
            r1 = r15
            r2 = r16
            java.lang.String r19 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x0777 }
            java.lang.String r19 = java.lang.String.valueOf(r19)     // Catch:{ Exception -> 0x0777 }
            r0 = r9
            r1 = r19
            r0.<init>(r1)     // Catch:{ Exception -> 0x0777 }
            java.lang.String r19 = "/120"
            r0 = r9
            r1 = r19
            java.lang.StringBuilder r19 = r0.append(r1)     // Catch:{ Exception -> 0x0777 }
            java.lang.String r19 = r19.toString()     // Catch:{ Exception -> 0x0777 }
            r9 = r19
        L_0x0334:
            java.lang.String r19 = "mcount"
            r0 = r3
            r1 = r19
            java.lang.String r19 = r0.getString(r1)     // Catch:{ Exception -> 0x0777 }
            java.lang.String r15 = "count"
            java.lang.String r3 = r3.getString(r15)     // Catch:{ Exception -> 0x0777 }
            r7.setThumbnailPic(r9)     // Catch:{ Exception -> 0x0777 }
            r7.setId(r6)     // Catch:{ Exception -> 0x0777 }
            r7.setText(r8)     // Catch:{ Exception -> 0x0777 }
            r7.setCreatedAt(r10)     // Catch:{ Exception -> 0x0777 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0777 }
            java.lang.String r8 = "来自："
            r6.<init>(r8)     // Catch:{ Exception -> 0x0777 }
            java.lang.StringBuilder r5 = r6.append(r5)     // Catch:{ Exception -> 0x0777 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0777 }
            r7.setSource(r5)     // Catch:{ Exception -> 0x0777 }
            r0 = r7
            r1 = r18
            r0.setWeiboId(r1)     // Catch:{ Exception -> 0x0777 }
            r11.setId(r13)     // Catch:{ Exception -> 0x0777 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0777 }
            java.lang.String r6 = java.lang.String.valueOf(r12)     // Catch:{ Exception -> 0x0777 }
            r5.<init>(r6)     // Catch:{ Exception -> 0x0777 }
            java.lang.String r6 = "/40"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0777 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0777 }
            r11.setProfileImageUrl(r5)     // Catch:{ Exception -> 0x0777 }
            r11.setScreenName(r14)     // Catch:{ Exception -> 0x0777 }
            r7.setUser(r11)     // Catch:{ Exception -> 0x0777 }
            r0 = r7
            r1 = r18
            r0.setWeiboId(r1)     // Catch:{ Exception -> 0x0777 }
            r0 = r7
            r1 = r19
            r0.setComments(r1)     // Catch:{ Exception -> 0x0777 }
            r7.setRt(r3)     // Catch:{ Exception -> 0x0777 }
            r18 = r4
            r19 = r7
            goto L_0x01e0
        L_0x039b:
            r18 = move-exception
            r19 = r9
            r3 = r10
        L_0x039f:
            r18.printStackTrace()
            r18 = r19
            r19 = r3
            goto L_0x01e0
        L_0x03a8:
            java.lang.String r3 = "sohu_weibo"
            r0 = r18
            r1 = r3
            boolean r3 = r0.equals(r1)
            if (r3 == 0) goto L_0x0586
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "http://api.t.sohu.com/statuses/show/"
            r3.<init>(r4)
            r0 = r3
            r1 = r19
            java.lang.StringBuilder r3 = r0.append(r1)
            java.lang.String r4 = ".json"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = r3.toString()
            r3 = 0
            java.net.URL r5 = new java.net.URL     // Catch:{ MalformedURLException -> 0x055a, IOException -> 0x0560 }
            r5.<init>(r4)     // Catch:{ MalformedURLException -> 0x055a, IOException -> 0x0560 }
            java.net.URLConnection r4 = r5.openConnection()     // Catch:{ MalformedURLException -> 0x055a, IOException -> 0x0560 }
            r0 = r4
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x055a, IOException -> 0x0560 }
            r3 = r0
            java.lang.String r4 = "GET"
            r3.setRequestMethod(r4)     // Catch:{ MalformedURLException -> 0x055a, IOException -> 0x0560 }
        L_0x03de:
            cn.com.mzba.oauth.OAuth r4 = cn.com.mzba.service.StatusHttpUtils.oauthSohu
            java.lang.String r5 = cn.com.mzba.service.StatusHttpUtils.userTokenSohu
            java.lang.String r6 = cn.com.mzba.service.StatusHttpUtils.userTokenSohuSecret
            java.net.HttpURLConnection r7 = r4.signRequest(r5, r6, r3)
            if (r7 == 0) goto L_0x078c
            r3 = 200(0xc8, float:2.8E-43)
            int r4 = r7.getResponseCode()     // Catch:{ IOException -> 0x056c, JSONException -> 0x0579 }
            if (r3 != r4) goto L_0x078c
            java.io.InputStream r4 = r7.getInputStream()     // Catch:{ IOException -> 0x056c, JSONException -> 0x0579 }
            java.io.BufferedReader r6 = new java.io.BufferedReader     // Catch:{ IOException -> 0x056c, JSONException -> 0x0579 }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x056c, JSONException -> 0x0579 }
            r3.<init>(r4)     // Catch:{ IOException -> 0x056c, JSONException -> 0x0579 }
            r6.<init>(r3)     // Catch:{ IOException -> 0x056c, JSONException -> 0x0579 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x056c, JSONException -> 0x0579 }
            r3.<init>()     // Catch:{ IOException -> 0x056c, JSONException -> 0x0579 }
            r5 = 4000(0xfa0, float:5.605E-42)
            char[] r8 = new char[r5]     // Catch:{ IOException -> 0x056c, JSONException -> 0x0579 }
            r5 = 0
        L_0x040a:
            int r5 = r6.read(r8)     // Catch:{ IOException -> 0x056c, JSONException -> 0x0579 }
            r12 = -1
            if (r5 != r12) goto L_0x0566
            r6.close()     // Catch:{ IOException -> 0x056c, JSONException -> 0x0579 }
            r4.close()     // Catch:{ IOException -> 0x056c, JSONException -> 0x0579 }
            java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x056c, JSONException -> 0x0579 }
            r7.disconnect()     // Catch:{ IOException -> 0x056c, JSONException -> 0x0579 }
            org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ IOException -> 0x056c, JSONException -> 0x0579 }
            r4.<init>(r3)     // Catch:{ IOException -> 0x056c, JSONException -> 0x0579 }
            if (r4 == 0) goto L_0x079b
            cn.com.mzba.model.Status r6 = new cn.com.mzba.model.Status     // Catch:{ IOException -> 0x056c, JSONException -> 0x0579 }
            r6.<init>()     // Catch:{ IOException -> 0x056c, JSONException -> 0x0579 }
            java.lang.String r3 = "user"
            org.json.JSONObject r13 = r4.getJSONObject(r3)     // Catch:{ IOException -> 0x0753, JSONException -> 0x073b }
            java.lang.String r3 = "in_reply_to_status_text"
            java.lang.String r7 = r4.getString(r3)     // Catch:{ IOException -> 0x0753, JSONException -> 0x073b }
            if (r7 == 0) goto L_0x0798
            java.lang.String r3 = ""
            boolean r3 = r7.equals(r3)     // Catch:{ IOException -> 0x0753, JSONException -> 0x073b }
            if (r3 != 0) goto L_0x0798
            cn.com.mzba.model.Status r5 = new cn.com.mzba.model.Status     // Catch:{ IOException -> 0x0753, JSONException -> 0x073b }
            r5.<init>()     // Catch:{ IOException -> 0x0753, JSONException -> 0x073b }
            r3 = 1
            r6.setRetweetedStatus(r3)     // Catch:{ IOException -> 0x0759, JSONException -> 0x0741 }
            java.lang.String r3 = "in_reply_to_screen_name"
            java.lang.String r8 = r4.getString(r3)     // Catch:{ IOException -> 0x0759, JSONException -> 0x0741 }
            java.lang.String r3 = "in_reply_to_status_id"
            java.lang.String r3 = r4.getString(r3)     // Catch:{ IOException -> 0x0759, JSONException -> 0x0741 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0759, JSONException -> 0x0741 }
            java.lang.String r10 = "@"
            r9.<init>(r10)     // Catch:{ IOException -> 0x0759, JSONException -> 0x0741 }
            java.lang.StringBuilder r8 = r9.append(r8)     // Catch:{ IOException -> 0x0759, JSONException -> 0x0741 }
            java.lang.String r9 = ":"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ IOException -> 0x0759, JSONException -> 0x0741 }
            java.lang.StringBuilder r7 = r8.append(r7)     // Catch:{ IOException -> 0x0759, JSONException -> 0x0741 }
            java.lang.String r7 = r7.toString()     // Catch:{ IOException -> 0x0759, JSONException -> 0x0741 }
            r5.setText(r7)     // Catch:{ IOException -> 0x0759, JSONException -> 0x0741 }
            r5.setId(r3)     // Catch:{ IOException -> 0x0759, JSONException -> 0x0741 }
            r0 = r18
            r1 = r3
            java.util.List r3 = getCounts(r0, r1)     // Catch:{ IOException -> 0x0759, JSONException -> 0x0741 }
            r7 = 0
            java.lang.Object r7 = r3.get(r7)     // Catch:{ IOException -> 0x0759, JSONException -> 0x0741 }
            cn.com.mzba.model.StatusCount r7 = (cn.com.mzba.model.StatusCount) r7     // Catch:{ IOException -> 0x0759, JSONException -> 0x0741 }
            java.lang.String r7 = r7.getWeiboId()     // Catch:{ IOException -> 0x0759, JSONException -> 0x0741 }
            r0 = r18
            r1 = r7
            boolean r7 = r0.equals(r1)     // Catch:{ IOException -> 0x0759, JSONException -> 0x0741 }
            if (r7 == 0) goto L_0x04ab
            r7 = 0
            java.lang.Object r7 = r3.get(r7)     // Catch:{ IOException -> 0x0759, JSONException -> 0x0741 }
            cn.com.mzba.model.StatusCount r7 = (cn.com.mzba.model.StatusCount) r7     // Catch:{ IOException -> 0x0759, JSONException -> 0x0741 }
            java.lang.String r7 = r7.getComments()     // Catch:{ IOException -> 0x0759, JSONException -> 0x0741 }
            r5.setComments(r7)     // Catch:{ IOException -> 0x0759, JSONException -> 0x0741 }
            r7 = 0
            java.lang.Object r3 = r3.get(r7)     // Catch:{ IOException -> 0x0759, JSONException -> 0x0741 }
            cn.com.mzba.model.StatusCount r3 = (cn.com.mzba.model.StatusCount) r3     // Catch:{ IOException -> 0x0759, JSONException -> 0x0741 }
            java.lang.String r3 = r3.getRt()     // Catch:{ IOException -> 0x0759, JSONException -> 0x0741 }
            r5.setRt(r3)     // Catch:{ IOException -> 0x0759, JSONException -> 0x0741 }
        L_0x04ab:
            r6.setStatus(r5)     // Catch:{ IOException -> 0x0759, JSONException -> 0x0741 }
            r3 = r5
        L_0x04af:
            java.lang.String r5 = "id"
            java.lang.String r10 = r13.getString(r5)     // Catch:{ IOException -> 0x075f, JSONException -> 0x0747 }
            java.lang.String r5 = "screen_name"
            java.lang.String r12 = r13.getString(r5)     // Catch:{ IOException -> 0x075f, JSONException -> 0x0747 }
            java.lang.String r5 = "profile_image_url"
            java.lang.String r9 = r13.getString(r5)     // Catch:{ IOException -> 0x075f, JSONException -> 0x0747 }
            java.lang.String r5 = "created_at"
            java.lang.String r8 = r4.getString(r5)     // Catch:{ IOException -> 0x075f, JSONException -> 0x0747 }
            java.lang.String r5 = "text"
            java.lang.String r7 = r4.getString(r5)     // Catch:{ IOException -> 0x075f, JSONException -> 0x0747 }
            java.lang.String r5 = "source"
            java.lang.String r5 = r4.getString(r5)     // Catch:{ IOException -> 0x075f, JSONException -> 0x0747 }
            java.lang.String r13 = "small_pic"
            java.lang.String r4 = r4.getString(r13)     // Catch:{ IOException -> 0x075f, JSONException -> 0x0747 }
            r6.setThumbnailPic(r4)     // Catch:{ IOException -> 0x075f, JSONException -> 0x0747 }
            r0 = r6
            r1 = r19
            r0.setId(r1)     // Catch:{ IOException -> 0x075f, JSONException -> 0x0747 }
            r6.setText(r7)     // Catch:{ IOException -> 0x075f, JSONException -> 0x0747 }
            r6.setCreatedAt(r8)     // Catch:{ IOException -> 0x075f, JSONException -> 0x0747 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x075f, JSONException -> 0x0747 }
            java.lang.String r7 = "来自："
            r4.<init>(r7)     // Catch:{ IOException -> 0x075f, JSONException -> 0x0747 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x075f, JSONException -> 0x0747 }
            java.lang.String r4 = r4.toString()     // Catch:{ IOException -> 0x075f, JSONException -> 0x0747 }
            r6.setSource(r4)     // Catch:{ IOException -> 0x075f, JSONException -> 0x0747 }
            r11.setId(r10)     // Catch:{ IOException -> 0x075f, JSONException -> 0x0747 }
            r11.setProfileImageUrl(r9)     // Catch:{ IOException -> 0x075f, JSONException -> 0x0747 }
            r11.setScreenName(r12)     // Catch:{ IOException -> 0x075f, JSONException -> 0x0747 }
            r6.setUser(r11)     // Catch:{ IOException -> 0x075f, JSONException -> 0x0747 }
            r0 = r6
            r1 = r18
            r0.setWeiboId(r1)     // Catch:{ IOException -> 0x075f, JSONException -> 0x0747 }
            r4 = r6
        L_0x050d:
            java.util.List r19 = getCounts(r18, r19)     // Catch:{ IOException -> 0x0765, JSONException -> 0x074d }
            r5 = 0
            r0 = r19
            r1 = r5
            java.lang.Object r5 = r0.get(r1)     // Catch:{ IOException -> 0x0765, JSONException -> 0x074d }
            cn.com.mzba.model.StatusCount r5 = (cn.com.mzba.model.StatusCount) r5     // Catch:{ IOException -> 0x0765, JSONException -> 0x074d }
            java.lang.String r5 = r5.getWeiboId()     // Catch:{ IOException -> 0x0765, JSONException -> 0x074d }
            r0 = r18
            r1 = r5
            boolean r18 = r0.equals(r1)     // Catch:{ IOException -> 0x0765, JSONException -> 0x074d }
            if (r18 == 0) goto L_0x0792
            r18 = 0
            r0 = r19
            r1 = r18
            java.lang.Object r18 = r0.get(r1)     // Catch:{ IOException -> 0x0765, JSONException -> 0x074d }
            cn.com.mzba.model.StatusCount r18 = (cn.com.mzba.model.StatusCount) r18     // Catch:{ IOException -> 0x0765, JSONException -> 0x074d }
            java.lang.String r18 = r18.getComments()     // Catch:{ IOException -> 0x0765, JSONException -> 0x074d }
            r0 = r4
            r1 = r18
            r0.setComments(r1)     // Catch:{ IOException -> 0x0765, JSONException -> 0x074d }
            r18 = 0
            r0 = r19
            r1 = r18
            java.lang.Object r18 = r0.get(r1)     // Catch:{ IOException -> 0x0765, JSONException -> 0x074d }
            cn.com.mzba.model.StatusCount r18 = (cn.com.mzba.model.StatusCount) r18     // Catch:{ IOException -> 0x0765, JSONException -> 0x074d }
            java.lang.String r18 = r18.getRt()     // Catch:{ IOException -> 0x0765, JSONException -> 0x074d }
            r0 = r4
            r1 = r18
            r0.setRt(r1)     // Catch:{ IOException -> 0x0765, JSONException -> 0x074d }
            r18 = r3
            r19 = r4
            goto L_0x01e0
        L_0x055a:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x03de
        L_0x0560:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x03de
        L_0x0566:
            r12 = 0
            r3.append(r8, r12, r5)     // Catch:{ IOException -> 0x056c, JSONException -> 0x0579 }
            goto L_0x040a
        L_0x056c:
            r18 = move-exception
            r19 = r9
            r3 = r10
        L_0x0570:
            r18.printStackTrace()
            r18 = r19
            r19 = r3
            goto L_0x01e0
        L_0x0579:
            r18 = move-exception
            r19 = r9
            r3 = r10
        L_0x057d:
            r18.printStackTrace()
            r18 = r19
            r19 = r3
            goto L_0x01e0
        L_0x0586:
            java.lang.String r3 = "netease_weibo"
            r0 = r18
            r1 = r3
            boolean r3 = r0.equals(r1)
            if (r3 == 0) goto L_0x078c
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "http://api.t.163.com/statuses/show/"
            r3.<init>(r4)
            r0 = r3
            r1 = r19
            java.lang.StringBuilder r3 = r0.append(r1)
            java.lang.String r4 = ".json"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = r3.toString()
            r3 = 0
            java.net.URL r5 = new java.net.URL     // Catch:{ MalformedURLException -> 0x06f1, IOException -> 0x06f7 }
            r5.<init>(r4)     // Catch:{ MalformedURLException -> 0x06f1, IOException -> 0x06f7 }
            java.net.URLConnection r4 = r5.openConnection()     // Catch:{ MalformedURLException -> 0x06f1, IOException -> 0x06f7 }
            r0 = r4
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x06f1, IOException -> 0x06f7 }
            r3 = r0
            java.lang.String r4 = "GET"
            r3.setRequestMethod(r4)     // Catch:{ MalformedURLException -> 0x06f1, IOException -> 0x06f7 }
        L_0x05bc:
            cn.com.mzba.oauth.OAuth r4 = cn.com.mzba.service.StatusHttpUtils.oauthNetease
            java.lang.String r5 = cn.com.mzba.service.StatusHttpUtils.userTokenNetease
            java.lang.String r6 = cn.com.mzba.service.StatusHttpUtils.userTokenNeteaseSecret
            java.net.HttpURLConnection r7 = r4.signRequest(r5, r6, r3)
            if (r7 == 0) goto L_0x078c
            r3 = 200(0xc8, float:2.8E-43)
            int r4 = r7.getResponseCode()     // Catch:{ IOException -> 0x0703, JSONException -> 0x0710 }
            if (r3 != r4) goto L_0x078c
            java.io.InputStream r4 = r7.getInputStream()     // Catch:{ IOException -> 0x0703, JSONException -> 0x0710 }
            java.io.BufferedReader r6 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0703, JSONException -> 0x0710 }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0703, JSONException -> 0x0710 }
            r3.<init>(r4)     // Catch:{ IOException -> 0x0703, JSONException -> 0x0710 }
            r6.<init>(r3)     // Catch:{ IOException -> 0x0703, JSONException -> 0x0710 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0703, JSONException -> 0x0710 }
            r3.<init>()     // Catch:{ IOException -> 0x0703, JSONException -> 0x0710 }
            r5 = 4000(0xfa0, float:5.605E-42)
            char[] r8 = new char[r5]     // Catch:{ IOException -> 0x0703, JSONException -> 0x0710 }
            r5 = 0
        L_0x05e8:
            int r5 = r6.read(r8)     // Catch:{ IOException -> 0x0703, JSONException -> 0x0710 }
            r12 = -1
            if (r5 != r12) goto L_0x06fd
            r6.close()     // Catch:{ IOException -> 0x0703, JSONException -> 0x0710 }
            r4.close()     // Catch:{ IOException -> 0x0703, JSONException -> 0x0710 }
            java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x0703, JSONException -> 0x0710 }
            r7.disconnect()     // Catch:{ IOException -> 0x0703, JSONException -> 0x0710 }
            org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ IOException -> 0x0703, JSONException -> 0x0710 }
            r4.<init>(r3)     // Catch:{ IOException -> 0x0703, JSONException -> 0x0710 }
            java.lang.String r3 = cn.com.mzba.service.StatusHttpUtils.TAG     // Catch:{ IOException -> 0x0703, JSONException -> 0x0710 }
            java.lang.String r5 = r4.toString()     // Catch:{ IOException -> 0x0703, JSONException -> 0x0710 }
            android.util.Log.i(r3, r5)     // Catch:{ IOException -> 0x0703, JSONException -> 0x0710 }
            if (r4 == 0) goto L_0x078c
            cn.com.mzba.model.Status r7 = new cn.com.mzba.model.Status     // Catch:{ IOException -> 0x0703, JSONException -> 0x0710 }
            r7.<init>()     // Catch:{ IOException -> 0x0703, JSONException -> 0x0710 }
            java.lang.String r3 = "user"
            org.json.JSONObject r10 = r4.getJSONObject(r3)     // Catch:{ IOException -> 0x072c, JSONException -> 0x071d }
            java.lang.String r3 = "in_reply_to_status_text"
            java.lang.String r6 = r4.getString(r3)     // Catch:{ IOException -> 0x072c, JSONException -> 0x071d }
            if (r6 == 0) goto L_0x0789
            java.lang.String r3 = "null"
            boolean r3 = r6.equals(r3)     // Catch:{ IOException -> 0x072c, JSONException -> 0x071d }
            if (r3 != 0) goto L_0x0789
            cn.com.mzba.model.Status r3 = new cn.com.mzba.model.Status     // Catch:{ IOException -> 0x072c, JSONException -> 0x071d }
            r3.<init>()     // Catch:{ IOException -> 0x072c, JSONException -> 0x071d }
            r5 = 1
            r7.setRetweetedStatus(r5)     // Catch:{ IOException -> 0x0731, JSONException -> 0x0722 }
            java.lang.String r5 = "in_reply_to_screen_name"
            java.lang.String r8 = r4.getString(r5)     // Catch:{ IOException -> 0x0731, JSONException -> 0x0722 }
            java.lang.String r5 = "in_reply_to_status_id"
            java.lang.String r5 = r4.getString(r5)     // Catch:{ IOException -> 0x0731, JSONException -> 0x0722 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0731, JSONException -> 0x0722 }
            java.lang.String r12 = "@"
            r9.<init>(r12)     // Catch:{ IOException -> 0x0731, JSONException -> 0x0722 }
            java.lang.StringBuilder r8 = r9.append(r8)     // Catch:{ IOException -> 0x0731, JSONException -> 0x0722 }
            java.lang.String r9 = ":"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ IOException -> 0x0731, JSONException -> 0x0722 }
            java.lang.StringBuilder r6 = r8.append(r6)     // Catch:{ IOException -> 0x0731, JSONException -> 0x0722 }
            java.lang.String r6 = r6.toString()     // Catch:{ IOException -> 0x0731, JSONException -> 0x0722 }
            r3.setText(r6)     // Catch:{ IOException -> 0x0731, JSONException -> 0x0722 }
            r3.setId(r5)     // Catch:{ IOException -> 0x0731, JSONException -> 0x0722 }
            r7.setStatus(r3)     // Catch:{ IOException -> 0x0731, JSONException -> 0x0722 }
            r5 = r3
        L_0x065f:
            java.lang.String r3 = "id"
            java.lang.String r12 = r10.getString(r3)     // Catch:{ IOException -> 0x0736, JSONException -> 0x0727 }
            java.lang.String r3 = "screen_name"
            java.lang.String r13 = r10.getString(r3)     // Catch:{ IOException -> 0x0736, JSONException -> 0x0727 }
            java.lang.String r3 = "profile_image_url"
            java.lang.String r10 = r10.getString(r3)     // Catch:{ IOException -> 0x0736, JSONException -> 0x0727 }
            java.lang.String r3 = "created_at"
            java.lang.String r9 = r4.getString(r3)     // Catch:{ IOException -> 0x0736, JSONException -> 0x0727 }
            java.lang.String r3 = "text"
            java.lang.String r8 = r4.getString(r3)     // Catch:{ IOException -> 0x0736, JSONException -> 0x0727 }
            java.lang.String r3 = "source"
            java.lang.String r6 = r4.getString(r3)     // Catch:{ IOException -> 0x0736, JSONException -> 0x0727 }
            java.lang.String r3 = "gender"
            boolean r3 = r4.has(r3)     // Catch:{ IOException -> 0x0736, JSONException -> 0x0727 }
            if (r3 == 0) goto L_0x0694
            java.lang.String r3 = "gender"
            java.lang.String r3 = r4.getString(r3)     // Catch:{ IOException -> 0x0736, JSONException -> 0x0727 }
            r11.setGender(r3)     // Catch:{ IOException -> 0x0736, JSONException -> 0x0727 }
        L_0x0694:
            r0 = r7
            r1 = r19
            r0.setId(r1)     // Catch:{ IOException -> 0x0736, JSONException -> 0x0727 }
            r7.setText(r8)     // Catch:{ IOException -> 0x0736, JSONException -> 0x0727 }
            r7.setCreatedAt(r9)     // Catch:{ IOException -> 0x0736, JSONException -> 0x0727 }
            java.lang.StringBuilder r19 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0736, JSONException -> 0x0727 }
            java.lang.String r3 = "来自："
            r0 = r19
            r1 = r3
            r0.<init>(r1)     // Catch:{ IOException -> 0x0736, JSONException -> 0x0727 }
            r0 = r19
            r1 = r6
            java.lang.StringBuilder r19 = r0.append(r1)     // Catch:{ IOException -> 0x0736, JSONException -> 0x0727 }
            java.lang.String r19 = r19.toString()     // Catch:{ IOException -> 0x0736, JSONException -> 0x0727 }
            r0 = r7
            r1 = r19
            r0.setSource(r1)     // Catch:{ IOException -> 0x0736, JSONException -> 0x0727 }
            java.lang.String r19 = "comments_count"
            r0 = r4
            r1 = r19
            java.lang.String r19 = r0.getString(r1)     // Catch:{ IOException -> 0x0736, JSONException -> 0x0727 }
            r0 = r7
            r1 = r19
            r0.setComments(r1)     // Catch:{ IOException -> 0x0736, JSONException -> 0x0727 }
            java.lang.String r19 = "retweet_count"
            r0 = r4
            r1 = r19
            java.lang.String r19 = r0.getString(r1)     // Catch:{ IOException -> 0x0736, JSONException -> 0x0727 }
            r0 = r7
            r1 = r19
            r0.setRt(r1)     // Catch:{ IOException -> 0x0736, JSONException -> 0x0727 }
            r11.setId(r12)     // Catch:{ IOException -> 0x0736, JSONException -> 0x0727 }
            r11.setProfileImageUrl(r10)     // Catch:{ IOException -> 0x0736, JSONException -> 0x0727 }
            r11.setScreenName(r13)     // Catch:{ IOException -> 0x0736, JSONException -> 0x0727 }
            r7.setUser(r11)     // Catch:{ IOException -> 0x0736, JSONException -> 0x0727 }
            r0 = r7
            r1 = r18
            r0.setWeiboId(r1)     // Catch:{ IOException -> 0x0736, JSONException -> 0x0727 }
            r18 = r5
            r19 = r7
            goto L_0x01e0
        L_0x06f1:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x05bc
        L_0x06f7:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x05bc
        L_0x06fd:
            r12 = 0
            r3.append(r8, r12, r5)     // Catch:{ IOException -> 0x0703, JSONException -> 0x0710 }
            goto L_0x05e8
        L_0x0703:
            r18 = move-exception
            r19 = r9
            r3 = r10
        L_0x0707:
            r18.printStackTrace()
            r18 = r19
            r19 = r3
            goto L_0x01e0
        L_0x0710:
            r18 = move-exception
            r19 = r9
            r3 = r10
        L_0x0714:
            r18.printStackTrace()
            r18 = r19
            r19 = r3
            goto L_0x01e0
        L_0x071d:
            r18 = move-exception
            r19 = r9
            r3 = r7
            goto L_0x0714
        L_0x0722:
            r18 = move-exception
            r19 = r3
            r3 = r7
            goto L_0x0714
        L_0x0727:
            r18 = move-exception
            r19 = r5
            r3 = r7
            goto L_0x0714
        L_0x072c:
            r18 = move-exception
            r19 = r9
            r3 = r7
            goto L_0x0707
        L_0x0731:
            r18 = move-exception
            r19 = r3
            r3 = r7
            goto L_0x0707
        L_0x0736:
            r18 = move-exception
            r19 = r5
            r3 = r7
            goto L_0x0707
        L_0x073b:
            r18 = move-exception
            r19 = r9
            r3 = r6
            goto L_0x057d
        L_0x0741:
            r18 = move-exception
            r19 = r5
            r3 = r6
            goto L_0x057d
        L_0x0747:
            r18 = move-exception
            r19 = r3
            r3 = r6
            goto L_0x057d
        L_0x074d:
            r18 = move-exception
            r19 = r3
            r3 = r4
            goto L_0x057d
        L_0x0753:
            r18 = move-exception
            r19 = r9
            r3 = r6
            goto L_0x0570
        L_0x0759:
            r18 = move-exception
            r19 = r5
            r3 = r6
            goto L_0x0570
        L_0x075f:
            r18 = move-exception
            r19 = r3
            r3 = r6
            goto L_0x0570
        L_0x0765:
            r18 = move-exception
            r19 = r3
            r3 = r4
            goto L_0x0570
        L_0x076b:
            r18 = move-exception
            r19 = r9
            r3 = r7
            goto L_0x039f
        L_0x0771:
            r18 = move-exception
            r19 = r5
            r3 = r7
            goto L_0x039f
        L_0x0777:
            r18 = move-exception
            r19 = r4
            r3 = r7
            goto L_0x039f
        L_0x077d:
            r18 = move-exception
            r19 = r9
            r3 = r6
            goto L_0x01eb
        L_0x0783:
            r18 = move-exception
            r19 = r5
            r3 = r6
            goto L_0x01eb
        L_0x0789:
            r5 = r9
            goto L_0x065f
        L_0x078c:
            r18 = r9
            r19 = r10
            goto L_0x01e0
        L_0x0792:
            r18 = r3
            r19 = r4
            goto L_0x01e0
        L_0x0798:
            r3 = r9
            goto L_0x04af
        L_0x079b:
            r3 = r9
            r4 = r10
            goto L_0x050d
        L_0x079f:
            r9 = r19
            goto L_0x0334
        L_0x07a3:
            r4 = r9
            goto L_0x02b2
        L_0x07a6:
            r18 = r9
            r19 = r6
            goto L_0x01e0
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.com.mzba.service.StatusHttpUtils.getStatusById(java.lang.String, java.lang.String):cn.com.mzba.model.Status");
    }

    /* JADX INFO: Multiple debug info for r22v9 java.lang.String: [D('builder' java.lang.StringBuilder), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r5v7 java.lang.String[]: [D('response' java.net.HttpURLConnection), D('idArray' java.lang.String[])] */
    /* JADX INFO: Multiple debug info for r24v5 int: [D('i' int), D('len' int)] */
    /* JADX INFO: Multiple debug info for r9v1 java.lang.String: [D('user' org.json.JSONObject), D('text' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r7v18 char[]: [D('url' java.lang.String), D('temp' char[])] */
    /* JADX INFO: Multiple debug info for r23v31 java.lang.String: [D('builder' java.lang.StringBuilder), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r6v8 java.lang.String[]: [D('response' java.net.HttpURLConnection), D('idArray' java.lang.String[])] */
    /* JADX INFO: Multiple debug info for r25v20 int: [D('i' int), D('len' int)] */
    /* JADX INFO: Multiple debug info for r7v26 cn.com.mzba.model.Status: [D('transmitId' java.lang.String), D('rtStatus' cn.com.mzba.model.Status)] */
    /* JADX INFO: Multiple debug info for r23v47 java.lang.String: [D('data' org.json.JSONObject), D('thumbnail_pic' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r23v67 java.lang.String: [D('parameter' java.lang.String), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r5v22 java.lang.String: [D('info' org.json.JSONObject), D('redirectCount' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r25v39 org.json.JSONObject: [D('type' int), D('source' org.json.JSONObject)] */
    /* JADX INFO: Multiple debug info for r25v40 java.lang.String: [D('rtName' java.lang.String), D('source' org.json.JSONObject)] */
    /* JADX INFO: Multiple debug info for r25v45 java.util.ArrayList: [D('sinceId' java.lang.String), D('params' java.util.List<org.apache.http.message.BasicNameValuePair>)] */
    /* JADX INFO: Multiple debug info for r23v104 java.lang.String: [D('builder' java.lang.StringBuilder), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r6v17 java.lang.String[]: [D('response' org.apache.http.HttpResponse), D('idArray' java.lang.String[])] */
    /* JADX INFO: Multiple debug info for r22v24 java.util.List<cn.com.mzba.model.StatusCount>: [D('counts' java.util.List<cn.com.mzba.model.StatusCount>), D('weiboId' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r23v116 int: [D('rtStatus' cn.com.mzba.model.Status), D('i' int)] */
    /* JADX INFO: Multiple debug info for r23v122 java.lang.String: [D('data' org.json.JSONObject), D('thumbnail_pic' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r7v46 'rtStatus'  cn.com.mzba.model.Status: [D('rtStatus' cn.com.mzba.model.Status), D('transmitText' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r8v32 java.lang.String: [D('user_retweeted' org.json.JSONObject), D('transmitUserName' java.lang.String)] */
    /* JADX WARN: Type inference failed for: r23v14, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x0526  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List<cn.com.mzba.model.Status> getMentionsStatus(java.lang.String r22, int r23, int r24, java.lang.String r25) {
        /*
            java.util.ArrayList r11 = new java.util.ArrayList
            r11.<init>()
            java.lang.String r5 = "sina_weibo"
            r0 = r22
            r1 = r5
            boolean r5 = r0.equals(r1)
            if (r5 == 0) goto L_0x0256
            java.lang.String r7 = "http://api.t.sina.com.cn/statuses/mentions.json"
            java.util.ArrayList r25 = new java.util.ArrayList
            r25.<init>()
            org.apache.http.message.BasicNameValuePair r5 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r6 = "page"
            java.lang.String r23 = java.lang.String.valueOf(r23)
            r0 = r5
            r1 = r6
            r2 = r23
            r0.<init>(r1, r2)
            r0 = r25
            r1 = r5
            r0.add(r1)
            org.apache.http.message.BasicNameValuePair r23 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r5 = "count"
            java.lang.String r24 = java.lang.String.valueOf(r24)
            r0 = r23
            r1 = r5
            r2 = r24
            r0.<init>(r1, r2)
            r0 = r25
            r1 = r23
            r0.add(r1)
            cn.com.mzba.oauth.OAuth r23 = cn.com.mzba.service.StatusHttpUtils.oauthSina
            java.lang.String r24 = cn.com.mzba.service.StatusHttpUtils.userTokenSina
            java.lang.String r5 = cn.com.mzba.service.StatusHttpUtils.userTokenSinaSecret
            r0 = r23
            r1 = r24
            r2 = r5
            r3 = r7
            r4 = r25
            org.apache.http.HttpResponse r6 = r0.signRequest(r1, r2, r3, r4)
            if (r6 == 0) goto L_0x00e7
            r23 = 200(0xc8, float:2.8E-43)
            org.apache.http.StatusLine r24 = r6.getStatusLine()
            int r24 = r24.getStatusCode()
            r0 = r23
            r1 = r24
            if (r0 != r1) goto L_0x00e7
            org.apache.http.HttpEntity r23 = r6.getEntity()     // Catch:{ Exception -> 0x00f3 }
            java.io.InputStream r24 = r23.getContent()     // Catch:{ Exception -> 0x00f3 }
            java.io.BufferedReader r5 = new java.io.BufferedReader     // Catch:{ Exception -> 0x00f3 }
            java.io.InputStreamReader r23 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x00f3 }
            r23.<init>(r24)     // Catch:{ Exception -> 0x00f3 }
            r0 = r5
            r1 = r23
            r0.<init>(r1)     // Catch:{ Exception -> 0x00f3 }
            java.lang.StringBuilder r23 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00f3 }
            org.apache.http.HttpEntity r25 = r6.getEntity()     // Catch:{ Exception -> 0x00f3 }
            long r7 = r25.getContentLength()     // Catch:{ Exception -> 0x00f3 }
            r0 = r7
            int r0 = (int) r0     // Catch:{ Exception -> 0x00f3 }
            r25 = r0
            r0 = r23
            r1 = r25
            r0.<init>(r1)     // Catch:{ Exception -> 0x00f3 }
            r25 = 4000(0xfa0, float:5.605E-42)
            r0 = r25
            char[] r0 = new char[r0]     // Catch:{ Exception -> 0x00f3 }
            r7 = r0
            r25 = 0
        L_0x009a:
            int r25 = r5.read(r7)     // Catch:{ Exception -> 0x00f3 }
            r8 = -1
            r0 = r25
            r1 = r8
            if (r0 != r1) goto L_0x00e8
            r5.close()     // Catch:{ Exception -> 0x00f3 }
            r24.close()     // Catch:{ Exception -> 0x00f3 }
            java.lang.String r23 = r23.toString()     // Catch:{ Exception -> 0x00f3 }
            org.apache.http.HttpEntity r24 = r6.getEntity()     // Catch:{ Exception -> 0x00f3 }
            r24.consumeContent()     // Catch:{ Exception -> 0x00f3 }
            org.json.JSONArray r24 = new org.json.JSONArray     // Catch:{ Exception -> 0x00f3 }
            r0 = r24
            r1 = r23
            r0.<init>(r1)     // Catch:{ Exception -> 0x00f3 }
            int r23 = r24.length()     // Catch:{ Exception -> 0x00f3 }
            r0 = r23
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ Exception -> 0x00f3 }
            r6 = r0
            r23 = 0
            r25 = r23
        L_0x00cb:
            int r23 = r24.length()     // Catch:{ Exception -> 0x00f3 }
            r0 = r25
            r1 = r23
            if (r0 < r1) goto L_0x00f8
            java.lang.String r23 = cn.com.mzba.service.StringUtil.arrayToString(r6)     // Catch:{ Exception -> 0x00f3 }
            java.util.List r22 = getCounts(r22, r23)     // Catch:{ Exception -> 0x00f3 }
            java.util.Iterator r25 = r11.iterator()     // Catch:{ Exception -> 0x00f3 }
        L_0x00e1:
            boolean r23 = r25.hasNext()     // Catch:{ Exception -> 0x00f3 }
            if (r23 != 0) goto L_0x021c
        L_0x00e7:
            return r11
        L_0x00e8:
            r8 = 0
            r0 = r23
            r1 = r7
            r2 = r8
            r3 = r25
            r0.append(r1, r2, r3)     // Catch:{ Exception -> 0x00f3 }
            goto L_0x009a
        L_0x00f3:
            r22 = move-exception
            r22.printStackTrace()
            goto L_0x00e7
        L_0x00f8:
            cn.com.mzba.model.Status r9 = new cn.com.mzba.model.Status     // Catch:{ Exception -> 0x00f3 }
            r9.<init>()     // Catch:{ Exception -> 0x00f3 }
            cn.com.mzba.model.User r17 = new cn.com.mzba.model.User     // Catch:{ Exception -> 0x00f3 }
            r17.<init>()     // Catch:{ Exception -> 0x00f3 }
            r5 = 0
            org.json.JSONObject r23 = r24.getJSONObject(r25)     // Catch:{ Exception -> 0x00f3 }
            if (r23 == 0) goto L_0x0969
            java.lang.String r7 = "user"
            r0 = r23
            r1 = r7
            org.json.JSONObject r12 = r0.getJSONObject(r1)     // Catch:{ Exception -> 0x00f3 }
            java.lang.String r7 = ""
            java.lang.String r8 = "retweeted_status"
            r0 = r23
            r1 = r8
            boolean r8 = r0.has(r1)     // Catch:{ Exception -> 0x00f3 }
            if (r8 == 0) goto L_0x0962
            cn.com.mzba.model.Status r7 = new cn.com.mzba.model.Status     // Catch:{ Exception -> 0x00f3 }
            r7.<init>()     // Catch:{ Exception -> 0x00f3 }
            r5 = 1
            r9.setRetweetedStatus(r5)     // Catch:{ Exception -> 0x00f3 }
            java.lang.String r5 = "retweeted_status"
            r0 = r23
            r1 = r5
            org.json.JSONObject r5 = r0.getJSONObject(r1)     // Catch:{ Exception -> 0x00f3 }
            java.lang.String r8 = "user"
            org.json.JSONObject r8 = r5.getJSONObject(r8)     // Catch:{ Exception -> 0x00f3 }
            java.lang.String r10 = "screen_name"
            java.lang.String r8 = r8.getString(r10)     // Catch:{ Exception -> 0x00f3 }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00f3 }
            java.lang.String r13 = "@"
            r10.<init>(r13)     // Catch:{ Exception -> 0x00f3 }
            java.lang.StringBuilder r8 = r10.append(r8)     // Catch:{ Exception -> 0x00f3 }
            java.lang.String r10 = ":"
            java.lang.StringBuilder r8 = r8.append(r10)     // Catch:{ Exception -> 0x00f3 }
            java.lang.String r10 = "text"
            java.lang.String r10 = r5.getString(r10)     // Catch:{ Exception -> 0x00f3 }
            java.lang.StringBuilder r8 = r8.append(r10)     // Catch:{ Exception -> 0x00f3 }
            java.lang.String r10 = r8.toString()     // Catch:{ Exception -> 0x00f3 }
            java.lang.String r8 = ""
            java.lang.String r13 = "thumbnail_pic"
            boolean r13 = r5.has(r13)     // Catch:{ Exception -> 0x00f3 }
            if (r13 == 0) goto L_0x016c
            java.lang.String r8 = "thumbnail_pic"
            java.lang.String r8 = r5.getString(r8)     // Catch:{ Exception -> 0x00f3 }
        L_0x016c:
            java.lang.String r13 = "id"
            java.lang.String r5 = r5.getString(r13)     // Catch:{ Exception -> 0x00f3 }
            r7.setId(r5)     // Catch:{ Exception -> 0x00f3 }
            r7.setText(r10)     // Catch:{ Exception -> 0x00f3 }
            r7.setThumbnailPic(r8)     // Catch:{ Exception -> 0x00f3 }
            r9.setStatus(r7)     // Catch:{ Exception -> 0x00f3 }
            r5 = r10
        L_0x017f:
            java.lang.String r5 = "id"
            r0 = r23
            r1 = r5
            java.lang.String r5 = r0.getString(r1)     // Catch:{ Exception -> 0x00f3 }
            r6[r25] = r5     // Catch:{ Exception -> 0x00f3 }
            java.lang.String r8 = "id"
            java.lang.String r15 = r12.getString(r8)     // Catch:{ Exception -> 0x00f3 }
            java.lang.String r8 = "screen_name"
            java.lang.String r16 = r12.getString(r8)     // Catch:{ Exception -> 0x00f3 }
            java.lang.String r8 = "profile_image_url"
            java.lang.String r14 = r12.getString(r8)     // Catch:{ Exception -> 0x00f3 }
            java.lang.String r8 = "created_at"
            r0 = r23
            r1 = r8
            java.lang.String r13 = r0.getString(r1)     // Catch:{ Exception -> 0x00f3 }
            java.lang.String r8 = "text"
            r0 = r23
            r1 = r8
            java.lang.String r10 = r0.getString(r1)     // Catch:{ Exception -> 0x00f3 }
            java.lang.String r8 = "source"
            r0 = r23
            r1 = r8
            java.lang.String r8 = r0.getString(r1)     // Catch:{ Exception -> 0x00f3 }
            java.lang.String r12 = ""
            java.lang.String r18 = "thumbnail_pic"
            r0 = r23
            r1 = r18
            boolean r18 = r0.has(r1)     // Catch:{ Exception -> 0x00f3 }
            if (r18 == 0) goto L_0x095e
            java.lang.String r12 = "thumbnail_pic"
            r0 = r23
            r1 = r12
            java.lang.String r23 = r0.getString(r1)     // Catch:{ Exception -> 0x00f3 }
            r0 = r9
            r1 = r23
            r0.setThumbnailPic(r1)     // Catch:{ Exception -> 0x00f3 }
        L_0x01d4:
            r9.setId(r5)     // Catch:{ Exception -> 0x00f3 }
            r9.setText(r10)     // Catch:{ Exception -> 0x00f3 }
            r9.setCreatedAt(r13)     // Catch:{ Exception -> 0x00f3 }
            java.lang.StringBuilder r23 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00f3 }
            java.lang.String r5 = "来自："
            r0 = r23
            r1 = r5
            r0.<init>(r1)     // Catch:{ Exception -> 0x00f3 }
            r0 = r23
            r1 = r8
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ Exception -> 0x00f3 }
            java.lang.String r23 = r23.toString()     // Catch:{ Exception -> 0x00f3 }
            r0 = r9
            r1 = r23
            r0.setSource(r1)     // Catch:{ Exception -> 0x00f3 }
            r0 = r17
            r1 = r15
            r0.setId(r1)     // Catch:{ Exception -> 0x00f3 }
            r0 = r17
            r1 = r14
            r0.setProfileImageUrl(r1)     // Catch:{ Exception -> 0x00f3 }
            r0 = r17
            r1 = r16
            r0.setScreenName(r1)     // Catch:{ Exception -> 0x00f3 }
            r0 = r9
            r1 = r17
            r0.setUser(r1)     // Catch:{ Exception -> 0x00f3 }
            r11.add(r9)     // Catch:{ Exception -> 0x00f3 }
            r23 = r7
        L_0x0216:
            int r23 = r25 + 1
            r25 = r23
            goto L_0x00cb
        L_0x021c:
            java.lang.Object r24 = r25.next()     // Catch:{ Exception -> 0x00f3 }
            cn.com.mzba.model.Status r24 = (cn.com.mzba.model.Status) r24     // Catch:{ Exception -> 0x00f3 }
            java.util.Iterator r5 = r22.iterator()     // Catch:{ Exception -> 0x00f3 }
        L_0x0226:
            boolean r23 = r5.hasNext()     // Catch:{ Exception -> 0x00f3 }
            if (r23 == 0) goto L_0x00e1
            java.lang.Object r23 = r5.next()     // Catch:{ Exception -> 0x00f3 }
            cn.com.mzba.model.StatusCount r23 = (cn.com.mzba.model.StatusCount) r23     // Catch:{ Exception -> 0x00f3 }
            java.lang.String r6 = r24.getId()     // Catch:{ Exception -> 0x00f3 }
            java.lang.String r7 = r23.getId()     // Catch:{ Exception -> 0x00f3 }
            boolean r6 = r6.equals(r7)     // Catch:{ Exception -> 0x00f3 }
            if (r6 == 0) goto L_0x0226
            java.lang.String r6 = r23.getComments()     // Catch:{ Exception -> 0x00f3 }
            r0 = r24
            r1 = r6
            r0.setComments(r1)     // Catch:{ Exception -> 0x00f3 }
            java.lang.String r23 = r23.getRt()     // Catch:{ Exception -> 0x00f3 }
            r0 = r24
            r1 = r23
            r0.setRt(r1)     // Catch:{ Exception -> 0x00f3 }
            goto L_0x0226
        L_0x0256:
            java.lang.String r5 = "tencent_weibo"
            r0 = r22
            r1 = r5
            boolean r5 = r0.equals(r1)
            if (r5 == 0) goto L_0x0495
            java.lang.String r7 = "http://open.t.qq.com/api/statuses/mentions_timeline"
            java.util.ArrayList r10 = new java.util.ArrayList
            r10.<init>()
            cn.com.mzba.oauth.Parameter r23 = new cn.com.mzba.oauth.Parameter
            java.lang.String r25 = "Reqnum"
            java.lang.String r24 = java.lang.String.valueOf(r24)
            r0 = r23
            r1 = r25
            r2 = r24
            r0.<init>(r1, r2)
            r0 = r10
            r1 = r23
            r0.add(r1)
            cn.com.mzba.oauth.Parameter r23 = new cn.com.mzba.oauth.Parameter
            java.lang.String r24 = "type"
            r25 = 0
            java.lang.String r25 = java.lang.String.valueOf(r25)
            r23.<init>(r24, r25)
            r0 = r10
            r1 = r23
            r0.add(r1)
            cn.com.mzba.oauth.Parameter r23 = new cn.com.mzba.oauth.Parameter
            java.lang.String r24 = "Pageflag"
            r25 = 0
            java.lang.String r25 = java.lang.String.valueOf(r25)
            r23.<init>(r24, r25)
            r0 = r10
            r1 = r23
            r0.add(r1)
            cn.com.mzba.oauth.Parameter r23 = new cn.com.mzba.oauth.Parameter
            java.lang.String r24 = "fotmat"
            java.lang.String r25 = "json"
            r23.<init>(r24, r25)
            r0 = r10
            r1 = r23
            r0.add(r1)
            cn.com.mzba.oauth.OAuth r5 = cn.com.mzba.service.StatusHttpUtils.oauthTencent     // Catch:{ Exception -> 0x048f }
            java.lang.String r6 = "GET"
            java.lang.String r8 = cn.com.mzba.service.StatusHttpUtils.userTokenTencent     // Catch:{ Exception -> 0x048f }
            java.lang.String r9 = cn.com.mzba.service.StatusHttpUtils.userTokenTencentSecret     // Catch:{ Exception -> 0x048f }
            java.lang.String r23 = r5.getPostParameter(r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x048f }
            cn.com.mzba.oauth.OAuth r24 = cn.com.mzba.service.StatusHttpUtils.oauthTencent     // Catch:{ Exception -> 0x048f }
            r0 = r24
            r1 = r7
            r2 = r23
            java.lang.String r23 = r0.httpGet(r1, r2)     // Catch:{ Exception -> 0x048f }
            if (r23 == 0) goto L_0x00e7
            org.json.JSONObject r24 = new org.json.JSONObject     // Catch:{ Exception -> 0x048f }
            r0 = r24
            r1 = r23
            r0.<init>(r1)     // Catch:{ Exception -> 0x048f }
            java.lang.String r23 = "data"
            r0 = r24
            r1 = r23
            org.json.JSONObject r23 = r0.getJSONObject(r1)     // Catch:{ Exception -> 0x048f }
            java.lang.String r24 = "info"
            org.json.JSONArray r6 = r23.getJSONArray(r24)     // Catch:{ Exception -> 0x048f }
            r23 = 0
            r24 = r23
        L_0x02ea:
            int r23 = r6.length()     // Catch:{ Exception -> 0x048f }
            r0 = r24
            r1 = r23
            if (r0 >= r1) goto L_0x00e7
            cn.com.mzba.model.Status r9 = new cn.com.mzba.model.Status     // Catch:{ Exception -> 0x048f }
            r9.<init>()     // Catch:{ Exception -> 0x048f }
            cn.com.mzba.model.User r17 = new cn.com.mzba.model.User     // Catch:{ Exception -> 0x048f }
            r17.<init>()     // Catch:{ Exception -> 0x048f }
            r23 = 0
            r0 = r6
            r1 = r24
            org.json.JSONObject r5 = r0.getJSONObject(r1)     // Catch:{ Exception -> 0x048f }
            if (r5 == 0) goto L_0x0489
            java.lang.String r25 = "type"
            r0 = r5
            r1 = r25
            int r25 = r0.getInt(r1)     // Catch:{ Exception -> 0x048f }
            r7 = 2
            r0 = r25
            r1 = r7
            if (r0 != r1) goto L_0x095a
            java.lang.String r23 = "source"
            r0 = r5
            r1 = r23
            org.json.JSONObject r25 = r0.getJSONObject(r1)     // Catch:{ Exception -> 0x048f }
            cn.com.mzba.model.Status r7 = new cn.com.mzba.model.Status     // Catch:{ Exception -> 0x048f }
            r7.<init>()     // Catch:{ Exception -> 0x048f }
            r23 = 1
            r0 = r9
            r1 = r23
            r0.setRetweetedStatus(r1)     // Catch:{ Exception -> 0x048f }
            java.lang.String r23 = "text"
            r0 = r25
            r1 = r23
            java.lang.String r8 = r0.getString(r1)     // Catch:{ Exception -> 0x048f }
            java.lang.String r23 = "id"
            r0 = r25
            r1 = r23
            java.lang.String r23 = r0.getString(r1)     // Catch:{ Exception -> 0x048f }
            java.lang.String r10 = "nick"
            r0 = r25
            r1 = r10
            java.lang.String r25 = r0.getString(r1)     // Catch:{ Exception -> 0x048f }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x048f }
            java.lang.String r12 = "@"
            r10.<init>(r12)     // Catch:{ Exception -> 0x048f }
            r0 = r10
            r1 = r25
            java.lang.StringBuilder r25 = r0.append(r1)     // Catch:{ Exception -> 0x048f }
            java.lang.String r10 = ":"
            r0 = r25
            r1 = r10
            java.lang.StringBuilder r25 = r0.append(r1)     // Catch:{ Exception -> 0x048f }
            r0 = r25
            r1 = r8
            java.lang.StringBuilder r25 = r0.append(r1)     // Catch:{ Exception -> 0x048f }
            java.lang.String r25 = r25.toString()     // Catch:{ Exception -> 0x048f }
            r0 = r7
            r1 = r25
            r0.setText(r1)     // Catch:{ Exception -> 0x048f }
            r0 = r7
            r1 = r23
            r0.setId(r1)     // Catch:{ Exception -> 0x048f }
            r9.setStatus(r7)     // Catch:{ Exception -> 0x048f }
        L_0x037c:
            java.lang.String r23 = "id"
            r0 = r5
            r1 = r23
            java.lang.String r25 = r0.getString(r1)     // Catch:{ Exception -> 0x048f }
            java.lang.String r23 = "name"
            r0 = r5
            r1 = r23
            java.lang.String r15 = r0.getString(r1)     // Catch:{ Exception -> 0x048f }
            java.lang.String r23 = "nick"
            r0 = r5
            r1 = r23
            java.lang.String r16 = r0.getString(r1)     // Catch:{ Exception -> 0x048f }
            java.lang.String r23 = "head"
            r0 = r5
            r1 = r23
            java.lang.String r14 = r0.getString(r1)     // Catch:{ Exception -> 0x048f }
            java.lang.String r23 = "timestamp"
            r0 = r5
            r1 = r23
            java.lang.String r13 = r0.getString(r1)     // Catch:{ Exception -> 0x048f }
            java.lang.String r23 = "text"
            r0 = r5
            r1 = r23
            java.lang.String r10 = r0.getString(r1)     // Catch:{ Exception -> 0x048f }
            java.lang.String r23 = "from"
            r0 = r5
            r1 = r23
            java.lang.String r8 = r0.getString(r1)     // Catch:{ Exception -> 0x048f }
            java.lang.String r23 = "image"
            r0 = r5
            r1 = r23
            java.lang.String r23 = r0.getString(r1)     // Catch:{ Exception -> 0x048f }
            if (r23 == 0) goto L_0x0956
            java.lang.String r12 = "null"
            r0 = r23
            r1 = r12
            boolean r12 = r0.equals(r1)     // Catch:{ Exception -> 0x048f }
            if (r12 != 0) goto L_0x0956
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x048f }
            r18 = 2
            int r19 = r23.length()     // Catch:{ Exception -> 0x048f }
            r20 = 2
            int r19 = r19 - r20
            r0 = r23
            r1 = r18
            r2 = r19
            java.lang.String r23 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x048f }
            java.lang.String r23 = java.lang.String.valueOf(r23)     // Catch:{ Exception -> 0x048f }
            r0 = r12
            r1 = r23
            r0.<init>(r1)     // Catch:{ Exception -> 0x048f }
            java.lang.String r23 = "/120"
            r0 = r12
            r1 = r23
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ Exception -> 0x048f }
            java.lang.String r23 = r23.toString()     // Catch:{ Exception -> 0x048f }
            r12 = r23
        L_0x0400:
            java.lang.String r23 = "mcount"
            r0 = r5
            r1 = r23
            java.lang.String r23 = r0.getString(r1)     // Catch:{ Exception -> 0x048f }
            java.lang.String r18 = "count"
            r0 = r5
            r1 = r18
            java.lang.String r5 = r0.getString(r1)     // Catch:{ Exception -> 0x048f }
            r9.setThumbnailPic(r12)     // Catch:{ Exception -> 0x048f }
            r0 = r9
            r1 = r25
            r0.setId(r1)     // Catch:{ Exception -> 0x048f }
            r9.setText(r10)     // Catch:{ Exception -> 0x048f }
            r9.setCreatedAt(r13)     // Catch:{ Exception -> 0x048f }
            java.lang.StringBuilder r25 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x048f }
            java.lang.String r10 = "来自："
            r0 = r25
            r1 = r10
            r0.<init>(r1)     // Catch:{ Exception -> 0x048f }
            r0 = r25
            r1 = r8
            java.lang.StringBuilder r25 = r0.append(r1)     // Catch:{ Exception -> 0x048f }
            java.lang.String r25 = r25.toString()     // Catch:{ Exception -> 0x048f }
            r0 = r9
            r1 = r25
            r0.setSource(r1)     // Catch:{ Exception -> 0x048f }
            r0 = r9
            r1 = r22
            r0.setWeiboId(r1)     // Catch:{ Exception -> 0x048f }
            r0 = r17
            r1 = r15
            r0.setId(r1)     // Catch:{ Exception -> 0x048f }
            java.lang.StringBuilder r25 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x048f }
            java.lang.String r8 = java.lang.String.valueOf(r14)     // Catch:{ Exception -> 0x048f }
            r0 = r25
            r1 = r8
            r0.<init>(r1)     // Catch:{ Exception -> 0x048f }
            java.lang.String r8 = "/40"
            r0 = r25
            r1 = r8
            java.lang.StringBuilder r25 = r0.append(r1)     // Catch:{ Exception -> 0x048f }
            java.lang.String r25 = r25.toString()     // Catch:{ Exception -> 0x048f }
            r0 = r17
            r1 = r25
            r0.setProfileImageUrl(r1)     // Catch:{ Exception -> 0x048f }
            r0 = r17
            r1 = r16
            r0.setScreenName(r1)     // Catch:{ Exception -> 0x048f }
            r0 = r9
            r1 = r17
            r0.setUser(r1)     // Catch:{ Exception -> 0x048f }
            r0 = r9
            r1 = r22
            r0.setWeiboId(r1)     // Catch:{ Exception -> 0x048f }
            r0 = r9
            r1 = r23
            r0.setComments(r1)     // Catch:{ Exception -> 0x048f }
            r9.setRt(r5)     // Catch:{ Exception -> 0x048f }
            r11.add(r9)     // Catch:{ Exception -> 0x048f }
            r23 = r7
        L_0x0489:
            int r23 = r24 + 1
            r24 = r23
            goto L_0x02ea
        L_0x048f:
            r22 = move-exception
            r22.printStackTrace()
            goto L_0x00e7
        L_0x0495:
            java.lang.String r5 = "sohu_weibo"
            r0 = r22
            r1 = r5
            boolean r5 = r0.equals(r1)
            if (r5 == 0) goto L_0x0729
            java.lang.String r7 = "http://api.t.sohu.com/statuses/mentions_timeline.json"
            r5 = 0
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x05f5, IOException -> 0x05ff }
            java.lang.String r8 = "?page="
            r6.<init>(r8)     // Catch:{ MalformedURLException -> 0x05f5, IOException -> 0x05ff }
            r0 = r6
            r1 = r23
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x05f5, IOException -> 0x05ff }
            java.lang.String r6 = "&count="
            r0 = r23
            r1 = r6
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x05f5, IOException -> 0x05ff }
            java.lang.StringBuilder r23 = r23.append(r24)     // Catch:{ MalformedURLException -> 0x05f5, IOException -> 0x05ff }
            java.lang.String r23 = r23.toString()     // Catch:{ MalformedURLException -> 0x05f5, IOException -> 0x05ff }
            if (r25 == 0) goto L_0x04e7
            java.lang.StringBuilder r24 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x05f5, IOException -> 0x05ff }
            java.lang.String r23 = java.lang.String.valueOf(r23)     // Catch:{ MalformedURLException -> 0x05f5, IOException -> 0x05ff }
            r0 = r24
            r1 = r23
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x05f5, IOException -> 0x05ff }
            java.lang.String r23 = "&since_id="
            r0 = r24
            r1 = r23
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x05f5, IOException -> 0x05ff }
            r0 = r23
            r1 = r25
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x05f5, IOException -> 0x05ff }
            java.lang.String r23 = r23.toString()     // Catch:{ MalformedURLException -> 0x05f5, IOException -> 0x05ff }
        L_0x04e7:
            java.net.URL r24 = new java.net.URL     // Catch:{ MalformedURLException -> 0x05f5, IOException -> 0x05ff }
            java.lang.StringBuilder r25 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x05f5, IOException -> 0x05ff }
            java.lang.String r6 = java.lang.String.valueOf(r7)     // Catch:{ MalformedURLException -> 0x05f5, IOException -> 0x05ff }
            r0 = r25
            r1 = r6
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x05f5, IOException -> 0x05ff }
            r0 = r25
            r1 = r23
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x05f5, IOException -> 0x05ff }
            java.lang.String r23 = r23.toString()     // Catch:{ MalformedURLException -> 0x05f5, IOException -> 0x05ff }
            r0 = r24
            r1 = r23
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x05f5, IOException -> 0x05ff }
            java.net.URLConnection r23 = r24.openConnection()     // Catch:{ MalformedURLException -> 0x05f5, IOException -> 0x05ff }
            java.net.HttpURLConnection r23 = (java.net.HttpURLConnection) r23     // Catch:{ MalformedURLException -> 0x05f5, IOException -> 0x05ff }
            java.lang.String r24 = "GET"
            r23.setRequestMethod(r24)     // Catch:{ MalformedURLException -> 0x0948, IOException -> 0x0945 }
        L_0x0513:
            cn.com.mzba.oauth.OAuth r24 = cn.com.mzba.service.StatusHttpUtils.oauthSohu
            java.lang.String r25 = cn.com.mzba.service.StatusHttpUtils.userTokenSohu
            java.lang.String r5 = cn.com.mzba.service.StatusHttpUtils.userTokenSohuSecret
            r0 = r24
            r1 = r25
            r2 = r5
            r3 = r23
            java.net.HttpURLConnection r6 = r0.signRequest(r1, r2, r3)
            if (r6 == 0) goto L_0x00e7
            r23 = 200(0xc8, float:2.8E-43)
            int r24 = r6.getResponseCode()     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            r0 = r23
            r1 = r24
            if (r0 != r1) goto L_0x00e7
            java.io.InputStream r24 = r6.getInputStream()     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            java.io.BufferedReader r5 = new java.io.BufferedReader     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            java.io.InputStreamReader r23 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            r23.<init>(r24)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            r0 = r5
            r1 = r23
            r0.<init>(r1)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            java.lang.StringBuilder r23 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            int r25 = r6.getContentLength()     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            r0 = r23
            r1 = r25
            r0.<init>(r1)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            r25 = 4000(0xfa0, float:5.605E-42)
            r0 = r25
            char[] r0 = new char[r0]     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            r7 = r0
            r25 = 0
        L_0x0559:
            int r25 = r5.read(r7)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            r8 = -1
            r0 = r25
            r1 = r8
            if (r0 != r1) goto L_0x0609
            r5.close()     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            r24.close()     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            java.lang.String r23 = r23.toString()     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            r6.disconnect()     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            org.json.JSONArray r24 = new org.json.JSONArray     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            r0 = r24
            r1 = r23
            r0.<init>(r1)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            int r23 = r24.length()     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            r0 = r23
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            r6 = r0
            r23 = 0
            r25 = r23
        L_0x0586:
            int r23 = r24.length()     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            r0 = r25
            r1 = r23
            if (r0 < r1) goto L_0x061b
            r0 = r6
            int r0 = r0.length     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            r23 = r0
            if (r23 <= 0) goto L_0x00e7
            java.lang.String r23 = cn.com.mzba.service.StringUtil.arrayToString(r6)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            java.util.List r23 = getCounts(r22, r23)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            java.util.Iterator r5 = r11.iterator()     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
        L_0x05a2:
            boolean r24 = r5.hasNext()     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            if (r24 == 0) goto L_0x00e7
            java.lang.Object r25 = r5.next()     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            cn.com.mzba.model.Status r25 = (cn.com.mzba.model.Status) r25     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            java.util.Iterator r6 = r23.iterator()     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
        L_0x05b2:
            boolean r24 = r6.hasNext()     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            if (r24 == 0) goto L_0x05a2
            java.lang.Object r24 = r6.next()     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            cn.com.mzba.model.StatusCount r24 = (cn.com.mzba.model.StatusCount) r24     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            java.lang.String r7 = r24.getWeiboId()     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            r0 = r22
            r1 = r7
            boolean r7 = r0.equals(r1)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            if (r7 == 0) goto L_0x05b2
            java.lang.String r7 = r25.getId()     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            java.lang.String r8 = r24.getId()     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            boolean r7 = r7.equals(r8)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            if (r7 == 0) goto L_0x05b2
            java.lang.String r7 = r24.getComments()     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            r0 = r25
            r1 = r7
            r0.setComments(r1)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            java.lang.String r24 = r24.getRt()     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            r0 = r25
            r1 = r24
            r0.setRt(r1)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            goto L_0x05b2
        L_0x05ef:
            r22 = move-exception
            r22.printStackTrace()
            goto L_0x00e7
        L_0x05f5:
            r23 = move-exception
            r24 = r23
            r23 = r5
        L_0x05fa:
            r24.printStackTrace()
            goto L_0x0513
        L_0x05ff:
            r23 = move-exception
            r24 = r23
            r23 = r5
        L_0x0604:
            r24.printStackTrace()
            goto L_0x0513
        L_0x0609:
            r8 = 0
            r0 = r23
            r1 = r7
            r2 = r8
            r3 = r25
            r0.append(r1, r2, r3)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            goto L_0x0559
        L_0x0615:
            r22 = move-exception
            r22.printStackTrace()
            goto L_0x00e7
        L_0x061b:
            cn.com.mzba.model.Status r9 = new cn.com.mzba.model.Status     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            r9.<init>()     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            cn.com.mzba.model.User r17 = new cn.com.mzba.model.User     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            r17.<init>()     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            r5 = 0
            org.json.JSONObject r23 = r24.getJSONObject(r25)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            if (r23 == 0) goto L_0x0952
            java.lang.String r7 = "user"
            r0 = r23
            r1 = r7
            org.json.JSONObject r12 = r0.getJSONObject(r1)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            java.lang.String r7 = "in_reply_to_status_text"
            r0 = r23
            r1 = r7
            java.lang.String r8 = r0.getString(r1)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            if (r8 == 0) goto L_0x0685
            java.lang.String r7 = ""
            boolean r7 = r8.equals(r7)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            if (r7 != 0) goto L_0x0685
            cn.com.mzba.model.Status r5 = new cn.com.mzba.model.Status     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            r5.<init>()     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            r7 = 1
            r9.setRetweetedStatus(r7)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            java.lang.String r7 = "in_reply_to_screen_name"
            r0 = r23
            r1 = r7
            java.lang.String r10 = r0.getString(r1)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            java.lang.String r7 = "in_reply_to_status_id"
            r0 = r23
            r1 = r7
            java.lang.String r7 = r0.getString(r1)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            java.lang.String r14 = "@"
            r13.<init>(r14)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            java.lang.StringBuilder r10 = r13.append(r10)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            java.lang.String r13 = ":"
            java.lang.StringBuilder r10 = r10.append(r13)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            java.lang.StringBuilder r8 = r10.append(r8)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            java.lang.String r8 = r8.toString()     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            r5.setText(r8)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            r5.setId(r7)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            r9.setStatus(r5)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
        L_0x0685:
            r7 = r5
            java.lang.String r5 = "id"
            r0 = r23
            r1 = r5
            java.lang.String r5 = r0.getString(r1)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            r6[r25] = r5     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            java.lang.String r8 = "id"
            java.lang.String r15 = r12.getString(r8)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            java.lang.String r8 = "screen_name"
            java.lang.String r16 = r12.getString(r8)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            java.lang.String r8 = "profile_image_url"
            java.lang.String r14 = r12.getString(r8)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            java.lang.String r8 = "created_at"
            r0 = r23
            r1 = r8
            java.lang.String r13 = r0.getString(r1)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            java.lang.String r8 = "text"
            r0 = r23
            r1 = r8
            java.lang.String r10 = r0.getString(r1)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            java.lang.String r8 = "source"
            r0 = r23
            r1 = r8
            java.lang.String r8 = r0.getString(r1)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            java.lang.String r12 = ""
            java.lang.String r18 = "small_pic"
            r0 = r23
            r1 = r18
            boolean r18 = r0.has(r1)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            if (r18 == 0) goto L_0x094e
            java.lang.String r12 = "small_pic"
            r0 = r23
            r1 = r12
            java.lang.String r23 = r0.getString(r1)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            r0 = r9
            r1 = r23
            r0.setThumbnailPic(r1)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
        L_0x06db:
            r9.setId(r5)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            r9.setText(r10)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            r9.setCreatedAt(r13)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            java.lang.StringBuilder r23 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            java.lang.String r5 = "来自："
            r0 = r23
            r1 = r5
            r0.<init>(r1)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            r0 = r23
            r1 = r8
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            java.lang.String r23 = r23.toString()     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            r0 = r9
            r1 = r23
            r0.setSource(r1)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            r0 = r17
            r1 = r15
            r0.setId(r1)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            r0 = r17
            r1 = r14
            r0.setProfileImageUrl(r1)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            r0 = r17
            r1 = r16
            r0.setScreenName(r1)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            r0 = r9
            r1 = r17
            r0.setUser(r1)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            r0 = r9
            r1 = r22
            r0.setWeiboId(r1)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            r11.add(r9)     // Catch:{ IOException -> 0x05ef, JSONException -> 0x0615 }
            r23 = r7
        L_0x0723:
            int r23 = r25 + 1
            r25 = r23
            goto L_0x0586
        L_0x0729:
            java.lang.String r23 = "netease_weibo"
            boolean r22 = r22.equals(r23)
            if (r22 == 0) goto L_0x00e7
            java.lang.String r7 = "http://api.t.163.com/statuses/mentions.json"
            r22 = 0
            java.lang.StringBuilder r23 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0921, IOException -> 0x0927 }
            java.lang.String r5 = "?count="
            r0 = r23
            r1 = r5
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x0921, IOException -> 0x0927 }
            java.lang.StringBuilder r23 = r23.append(r24)     // Catch:{ MalformedURLException -> 0x0921, IOException -> 0x0927 }
            java.lang.String r23 = r23.toString()     // Catch:{ MalformedURLException -> 0x0921, IOException -> 0x0927 }
            if (r25 == 0) goto L_0x076c
            java.lang.StringBuilder r24 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0921, IOException -> 0x0927 }
            java.lang.String r23 = java.lang.String.valueOf(r23)     // Catch:{ MalformedURLException -> 0x0921, IOException -> 0x0927 }
            r0 = r24
            r1 = r23
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x0921, IOException -> 0x0927 }
            java.lang.String r23 = "&since_id="
            r0 = r24
            r1 = r23
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x0921, IOException -> 0x0927 }
            r0 = r23
            r1 = r25
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x0921, IOException -> 0x0927 }
            java.lang.String r23 = r23.toString()     // Catch:{ MalformedURLException -> 0x0921, IOException -> 0x0927 }
        L_0x076c:
            java.net.URL r24 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0921, IOException -> 0x0927 }
            java.lang.StringBuilder r25 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0921, IOException -> 0x0927 }
            java.lang.String r5 = java.lang.String.valueOf(r7)     // Catch:{ MalformedURLException -> 0x0921, IOException -> 0x0927 }
            r0 = r25
            r1 = r5
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x0921, IOException -> 0x0927 }
            r0 = r25
            r1 = r23
            java.lang.StringBuilder r23 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x0921, IOException -> 0x0927 }
            java.lang.String r23 = r23.toString()     // Catch:{ MalformedURLException -> 0x0921, IOException -> 0x0927 }
            r0 = r24
            r1 = r23
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x0921, IOException -> 0x0927 }
            java.net.URLConnection r23 = r24.openConnection()     // Catch:{ MalformedURLException -> 0x0921, IOException -> 0x0927 }
            r0 = r23
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x0921, IOException -> 0x0927 }
            r22 = r0
            java.lang.String r23 = "GET"
            r22.setRequestMethod(r23)     // Catch:{ MalformedURLException -> 0x0921, IOException -> 0x0927 }
        L_0x079c:
            cn.com.mzba.oauth.OAuth r23 = cn.com.mzba.service.StatusHttpUtils.oauthNetease
            java.lang.String r24 = cn.com.mzba.service.StatusHttpUtils.userTokenNetease
            java.lang.String r25 = cn.com.mzba.service.StatusHttpUtils.userTokenNeteaseSecret
            r0 = r23
            r1 = r24
            r2 = r25
            r3 = r22
            java.net.HttpURLConnection r5 = r0.signRequest(r1, r2, r3)
            if (r5 == 0) goto L_0x00e7
            r22 = 200(0xc8, float:2.8E-43)
            int r23 = r5.getResponseCode()     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            r0 = r22
            r1 = r23
            if (r0 != r1) goto L_0x00e7
            java.io.InputStream r23 = r5.getInputStream()     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            java.io.BufferedReader r25 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            java.io.InputStreamReader r22 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            r22.<init>(r23)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            r0 = r25
            r1 = r22
            r0.<init>(r1)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            java.lang.StringBuilder r22 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            r22.<init>()     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            r24 = 4000(0xfa0, float:5.605E-42)
            r0 = r24
            char[] r0 = new char[r0]     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            r6 = r0
            r24 = 0
        L_0x07dc:
            r0 = r25
            r1 = r6
            int r24 = r0.read(r1)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            r7 = -1
            r0 = r24
            r1 = r7
            if (r0 != r1) goto L_0x092d
            r25.close()     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            r23.close()     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            java.lang.String r22 = r22.toString()     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            r5.disconnect()     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            org.json.JSONArray r23 = new org.json.JSONArray     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            r0 = r23
            r1 = r22
            r0.<init>(r1)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            int r22 = r23.length()     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            r0 = r22
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            r5 = r0
            r22 = 0
            r24 = r22
        L_0x080c:
            int r22 = r23.length()     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            r0 = r24
            r1 = r22
            if (r0 >= r1) goto L_0x00e7
            cn.com.mzba.model.Status r8 = new cn.com.mzba.model.Status     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            r8.<init>()     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            cn.com.mzba.model.User r15 = new cn.com.mzba.model.User     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            r15.<init>()     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            r25 = 0
            org.json.JSONObject r22 = r23.getJSONObject(r24)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            if (r22 == 0) goto L_0x094b
            java.lang.String r6 = "user"
            r0 = r22
            r1 = r6
            org.json.JSONObject r9 = r0.getJSONObject(r1)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            java.lang.String r6 = "in_reply_to_status_text"
            r0 = r22
            r1 = r6
            java.lang.String r6 = r0.getString(r1)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            java.lang.String r7 = "null"
            boolean r7 = r6.equals(r7)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            if (r7 != 0) goto L_0x0888
            cn.com.mzba.model.Status r25 = new cn.com.mzba.model.Status     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            r25.<init>()     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            r7 = 1
            r8.setRetweetedStatus(r7)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            java.lang.String r7 = "in_reply_to_screen_name"
            r0 = r22
            r1 = r7
            java.lang.String r7 = r0.getString(r1)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            java.lang.String r10 = "in_reply_to_status_id"
            r0 = r22
            r1 = r10
            java.lang.String r10 = r0.getString(r1)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            r0 = r25
            r1 = r10
            r0.setId(r1)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            java.lang.String r12 = "@"
            r10.<init>(r12)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            java.lang.StringBuilder r7 = r10.append(r7)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            java.lang.String r10 = ":"
            java.lang.StringBuilder r7 = r7.append(r10)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            java.lang.StringBuilder r6 = r7.append(r6)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            java.lang.String r6 = r6.toString()     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            r0 = r25
            r1 = r6
            r0.setText(r1)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            r0 = r8
            r1 = r25
            r0.setStatus(r1)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
        L_0x0888:
            r6 = r25
            java.lang.String r25 = "id"
            r0 = r22
            r1 = r25
            java.lang.String r25 = r0.getString(r1)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            r5[r24] = r25     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            java.lang.String r7 = "id"
            java.lang.String r13 = r9.getString(r7)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            java.lang.String r7 = "screen_name"
            java.lang.String r14 = r9.getString(r7)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            java.lang.String r7 = "profile_image_url"
            java.lang.String r12 = r9.getString(r7)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            java.lang.String r7 = "created_at"
            r0 = r22
            r1 = r7
            java.lang.String r10 = r0.getString(r1)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            java.lang.String r7 = "text"
            r0 = r22
            r1 = r7
            java.lang.String r9 = r0.getString(r1)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            java.lang.String r7 = "source"
            r0 = r22
            r1 = r7
            java.lang.String r7 = r0.getString(r1)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            r0 = r8
            r1 = r25
            r0.setId(r1)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            r8.setText(r9)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            r8.setCreatedAt(r10)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            java.lang.StringBuilder r25 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            java.lang.String r9 = "来自："
            r0 = r25
            r1 = r9
            r0.<init>(r1)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            r0 = r25
            r1 = r7
            java.lang.StringBuilder r25 = r0.append(r1)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            java.lang.String r25 = r25.toString()     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            r0 = r8
            r1 = r25
            r0.setSource(r1)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            r15.setId(r13)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            r15.setProfileImageUrl(r12)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            r15.setScreenName(r14)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            r8.setUser(r15)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            java.lang.String r25 = "comments_count"
            r0 = r22
            r1 = r25
            java.lang.String r25 = r0.getString(r1)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            r0 = r8
            r1 = r25
            r0.setComments(r1)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            java.lang.String r25 = "retweet_count"
            r0 = r22
            r1 = r25
            java.lang.String r22 = r0.getString(r1)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            r0 = r8
            r1 = r22
            r0.setRt(r1)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            r11.add(r8)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            r22 = r6
        L_0x091b:
            int r22 = r24 + 1
            r24 = r22
            goto L_0x080c
        L_0x0921:
            r23 = move-exception
            r23.printStackTrace()
            goto L_0x079c
        L_0x0927:
            r23 = move-exception
            r23.printStackTrace()
            goto L_0x079c
        L_0x092d:
            r7 = 0
            r0 = r22
            r1 = r6
            r2 = r7
            r3 = r24
            r0.append(r1, r2, r3)     // Catch:{ IOException -> 0x0939, JSONException -> 0x093f }
            goto L_0x07dc
        L_0x0939:
            r22 = move-exception
            r22.printStackTrace()
            goto L_0x00e7
        L_0x093f:
            r22 = move-exception
            r22.printStackTrace()
            goto L_0x00e7
        L_0x0945:
            r24 = move-exception
            goto L_0x0604
        L_0x0948:
            r24 = move-exception
            goto L_0x05fa
        L_0x094b:
            r22 = r25
            goto L_0x091b
        L_0x094e:
            r23 = r12
            goto L_0x06db
        L_0x0952:
            r23 = r5
            goto L_0x0723
        L_0x0956:
            r12 = r23
            goto L_0x0400
        L_0x095a:
            r7 = r23
            goto L_0x037c
        L_0x095e:
            r23 = r12
            goto L_0x01d4
        L_0x0962:
            r21 = r7
            r7 = r5
            r5 = r21
            goto L_0x017f
        L_0x0969:
            r23 = r5
            goto L_0x0216
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.com.mzba.service.StatusHttpUtils.getMentionsStatus(java.lang.String, int, int, java.lang.String):java.util.List");
    }

    /* JADX INFO: Multiple debug info for r21v9 java.lang.String: [D('builder' java.lang.StringBuilder), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r23v5 int: [D('len' int), D('i' int)] */
    /* JADX INFO: Multiple debug info for r21v15 java.lang.String: [D('data' org.json.JSONObject), D('source' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r7v5 char[]: [D('url' java.lang.String), D('temp' char[])] */
    /* JADX INFO: Multiple debug info for r22v31 java.lang.String: [D('builder' java.lang.StringBuilder), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r24v14 int: [D('len' int), D('i' int)] */
    /* JADX INFO: Multiple debug info for r5v15 java.lang.String: [D('user' org.json.JSONObject), D('userIcon' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r8v4 java.lang.String: [D('userName' java.lang.String), D('time' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r22v37 java.lang.String: [D('data' org.json.JSONObject), D('source' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r22v59 java.lang.String: [D('parameter' java.lang.String), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r5v21 java.lang.String: [D('info' org.json.JSONObject), D('redirectCount' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r24v31 org.json.JSONObject: [D('type' int), D('source' org.json.JSONObject)] */
    /* JADX INFO: Multiple debug info for r24v32 java.lang.String: [D('rtName' java.lang.String), D('source' org.json.JSONObject)] */
    /* JADX INFO: Multiple debug info for r21v26 java.util.ArrayList: [D('weiboId' java.lang.String), D('params' java.util.List<org.apache.http.message.BasicNameValuePair>)] */
    /* JADX INFO: Multiple debug info for r21v32 java.lang.String: [D('builder' java.lang.StringBuilder), D('content' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r23v35 int: [D('len' int), D('i' int)] */
    /* JADX INFO: Multiple debug info for r21v36 org.json.JSONObject: [D('e' java.lang.Exception), D('data' org.json.JSONObject)] */
    /* JADX WARN: Type inference failed for: r22v14, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x03f8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List<cn.com.mzba.model.Status> getDirectStatus(java.lang.String r21, int r22, int r23, java.lang.String r24) {
        /*
            java.util.ArrayList r11 = new java.util.ArrayList
            r11.<init>()
            java.lang.String r5 = "sina_weibo"
            r0 = r21
            r1 = r5
            boolean r5 = r0.equals(r1)
            if (r5 == 0) goto L_0x0128
            java.lang.String r7 = "http://api.t.sina.com.cn/direct_messages.json"
            java.util.ArrayList r21 = new java.util.ArrayList
            r21.<init>()
            cn.com.mzba.oauth.OAuth r22 = cn.com.mzba.service.StatusHttpUtils.oauthSina
            java.lang.String r23 = cn.com.mzba.service.StatusHttpUtils.userTokenSina
            java.lang.String r24 = cn.com.mzba.service.StatusHttpUtils.userTokenSinaSecret
            r0 = r22
            r1 = r23
            r2 = r24
            r3 = r7
            r4 = r21
            org.apache.http.HttpResponse r5 = r0.signRequest(r1, r2, r3, r4)
            if (r5 == 0) goto L_0x00a5
            r21 = 200(0xc8, float:2.8E-43)
            org.apache.http.StatusLine r22 = r5.getStatusLine()
            int r22 = r22.getStatusCode()
            r0 = r21
            r1 = r22
            if (r0 != r1) goto L_0x00a5
            org.apache.http.HttpEntity r21 = r5.getEntity()     // Catch:{ Exception -> 0x00b1 }
            java.io.InputStream r22 = r21.getContent()     // Catch:{ Exception -> 0x00b1 }
            java.io.BufferedReader r24 = new java.io.BufferedReader     // Catch:{ Exception -> 0x00b1 }
            java.io.InputStreamReader r21 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x00b1 }
            r21.<init>(r22)     // Catch:{ Exception -> 0x00b1 }
            r0 = r24
            r1 = r21
            r0.<init>(r1)     // Catch:{ Exception -> 0x00b1 }
            java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00b1 }
            org.apache.http.HttpEntity r23 = r5.getEntity()     // Catch:{ Exception -> 0x00b1 }
            long r6 = r23.getContentLength()     // Catch:{ Exception -> 0x00b1 }
            r0 = r6
            int r0 = (int) r0     // Catch:{ Exception -> 0x00b1 }
            r23 = r0
            r0 = r21
            r1 = r23
            r0.<init>(r1)     // Catch:{ Exception -> 0x00b1 }
            r23 = 4000(0xfa0, float:5.605E-42)
            r0 = r23
            char[] r0 = new char[r0]     // Catch:{ Exception -> 0x00b1 }
            r6 = r0
            r23 = 0
        L_0x0070:
            r0 = r24
            r1 = r6
            int r23 = r0.read(r1)     // Catch:{ Exception -> 0x00b1 }
            r7 = -1
            r0 = r23
            r1 = r7
            if (r0 != r1) goto L_0x00a6
            r24.close()     // Catch:{ Exception -> 0x00b1 }
            r22.close()     // Catch:{ Exception -> 0x00b1 }
            java.lang.String r21 = r21.toString()     // Catch:{ Exception -> 0x00b1 }
            org.apache.http.HttpEntity r22 = r5.getEntity()     // Catch:{ Exception -> 0x00b1 }
            r22.consumeContent()     // Catch:{ Exception -> 0x00b1 }
            org.json.JSONArray r22 = new org.json.JSONArray     // Catch:{ Exception -> 0x00b1 }
            r0 = r22
            r1 = r21
            r0.<init>(r1)     // Catch:{ Exception -> 0x00b1 }
            r21 = 0
            r23 = r21
        L_0x009b:
            int r21 = r22.length()     // Catch:{ Exception -> 0x00b1 }
            r0 = r23
            r1 = r21
            if (r0 < r1) goto L_0x00b6
        L_0x00a5:
            return r11
        L_0x00a6:
            r7 = 0
            r0 = r21
            r1 = r6
            r2 = r7
            r3 = r23
            r0.append(r1, r2, r3)     // Catch:{ Exception -> 0x00b1 }
            goto L_0x0070
        L_0x00b1:
            r21 = move-exception
            r21.printStackTrace()
            goto L_0x00a5
        L_0x00b6:
            org.json.JSONObject r21 = r22.getJSONObject(r23)     // Catch:{ Exception -> 0x00b1 }
            cn.com.mzba.model.User r5 = new cn.com.mzba.model.User     // Catch:{ Exception -> 0x00b1 }
            r5.<init>()     // Catch:{ Exception -> 0x00b1 }
            cn.com.mzba.model.Status r24 = new cn.com.mzba.model.Status     // Catch:{ Exception -> 0x00b1 }
            r24.<init>()     // Catch:{ Exception -> 0x00b1 }
            java.lang.String r6 = "sender"
            r0 = r21
            r1 = r6
            org.json.JSONObject r6 = r0.getJSONObject(r1)     // Catch:{ Exception -> 0x00b1 }
            java.lang.String r7 = "screen_name"
            java.lang.String r7 = r6.getString(r7)     // Catch:{ Exception -> 0x00b1 }
            r5.setScreenName(r7)     // Catch:{ Exception -> 0x00b1 }
            java.lang.String r7 = "profile_image_url"
            java.lang.String r7 = r6.getString(r7)     // Catch:{ Exception -> 0x00b1 }
            r5.setProfileImageUrl(r7)     // Catch:{ Exception -> 0x00b1 }
            java.lang.String r7 = "id"
            java.lang.String r6 = r6.getString(r7)     // Catch:{ Exception -> 0x00b1 }
            r5.setId(r6)     // Catch:{ Exception -> 0x00b1 }
            java.lang.String r6 = "text"
            r0 = r21
            r1 = r6
            java.lang.String r6 = r0.getString(r1)     // Catch:{ Exception -> 0x00b1 }
            r0 = r24
            r1 = r6
            r0.setText(r1)     // Catch:{ Exception -> 0x00b1 }
            java.lang.String r6 = "created_at"
            r0 = r21
            r1 = r6
            java.lang.String r6 = r0.getString(r1)     // Catch:{ Exception -> 0x00b1 }
            r0 = r24
            r1 = r6
            r0.setCreatedAt(r1)     // Catch:{ Exception -> 0x00b1 }
            java.lang.String r6 = "id"
            r0 = r21
            r1 = r6
            java.lang.String r21 = r0.getString(r1)     // Catch:{ Exception -> 0x00b1 }
            r0 = r24
            r1 = r21
            r0.setId(r1)     // Catch:{ Exception -> 0x00b1 }
            r0 = r24
            r1 = r5
            r0.setUser(r1)     // Catch:{ Exception -> 0x00b1 }
            r0 = r11
            r1 = r24
            r0.add(r1)     // Catch:{ Exception -> 0x00b1 }
            int r21 = r23 + 1
            r23 = r21
            goto L_0x009b
        L_0x0128:
            java.lang.String r5 = "tencent_weibo"
            r0 = r21
            r1 = r5
            boolean r5 = r0.equals(r1)
            if (r5 == 0) goto L_0x0367
            java.lang.String r7 = "http://open.t.qq.com/api/private/recv"
            java.util.ArrayList r10 = new java.util.ArrayList
            r10.<init>()
            cn.com.mzba.oauth.Parameter r22 = new cn.com.mzba.oauth.Parameter
            java.lang.String r24 = "Reqnum"
            java.lang.String r23 = java.lang.String.valueOf(r23)
            r0 = r22
            r1 = r24
            r2 = r23
            r0.<init>(r1, r2)
            r0 = r10
            r1 = r22
            r0.add(r1)
            cn.com.mzba.oauth.Parameter r22 = new cn.com.mzba.oauth.Parameter
            java.lang.String r23 = "type"
            r24 = 0
            java.lang.String r24 = java.lang.String.valueOf(r24)
            r22.<init>(r23, r24)
            r0 = r10
            r1 = r22
            r0.add(r1)
            cn.com.mzba.oauth.Parameter r22 = new cn.com.mzba.oauth.Parameter
            java.lang.String r23 = "Pageflag"
            r24 = 0
            java.lang.String r24 = java.lang.String.valueOf(r24)
            r22.<init>(r23, r24)
            r0 = r10
            r1 = r22
            r0.add(r1)
            cn.com.mzba.oauth.Parameter r22 = new cn.com.mzba.oauth.Parameter
            java.lang.String r23 = "fotmat"
            java.lang.String r24 = "json"
            r22.<init>(r23, r24)
            r0 = r10
            r1 = r22
            r0.add(r1)
            cn.com.mzba.oauth.OAuth r5 = cn.com.mzba.service.StatusHttpUtils.oauthTencent     // Catch:{ Exception -> 0x0361 }
            java.lang.String r6 = "GET"
            java.lang.String r8 = cn.com.mzba.service.StatusHttpUtils.userTokenTencent     // Catch:{ Exception -> 0x0361 }
            java.lang.String r9 = cn.com.mzba.service.StatusHttpUtils.userTokenTencentSecret     // Catch:{ Exception -> 0x0361 }
            java.lang.String r22 = r5.getPostParameter(r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x0361 }
            cn.com.mzba.oauth.OAuth r23 = cn.com.mzba.service.StatusHttpUtils.oauthTencent     // Catch:{ Exception -> 0x0361 }
            r0 = r23
            r1 = r7
            r2 = r22
            java.lang.String r22 = r0.httpGet(r1, r2)     // Catch:{ Exception -> 0x0361 }
            if (r22 == 0) goto L_0x00a5
            org.json.JSONObject r23 = new org.json.JSONObject     // Catch:{ Exception -> 0x0361 }
            r0 = r23
            r1 = r22
            r0.<init>(r1)     // Catch:{ Exception -> 0x0361 }
            java.lang.String r22 = "data"
            r0 = r23
            r1 = r22
            org.json.JSONObject r22 = r0.getJSONObject(r1)     // Catch:{ Exception -> 0x0361 }
            java.lang.String r23 = "info"
            org.json.JSONArray r6 = r22.getJSONArray(r23)     // Catch:{ Exception -> 0x0361 }
            r22 = 0
            r23 = r22
        L_0x01bc:
            int r22 = r6.length()     // Catch:{ Exception -> 0x0361 }
            r0 = r23
            r1 = r22
            if (r0 >= r1) goto L_0x00a5
            cn.com.mzba.model.Status r9 = new cn.com.mzba.model.Status     // Catch:{ Exception -> 0x0361 }
            r9.<init>()     // Catch:{ Exception -> 0x0361 }
            cn.com.mzba.model.User r17 = new cn.com.mzba.model.User     // Catch:{ Exception -> 0x0361 }
            r17.<init>()     // Catch:{ Exception -> 0x0361 }
            r22 = 0
            r0 = r6
            r1 = r23
            org.json.JSONObject r5 = r0.getJSONObject(r1)     // Catch:{ Exception -> 0x0361 }
            if (r5 == 0) goto L_0x035b
            java.lang.String r24 = "type"
            r0 = r5
            r1 = r24
            int r24 = r0.getInt(r1)     // Catch:{ Exception -> 0x0361 }
            r7 = 2
            r0 = r24
            r1 = r7
            if (r0 != r1) goto L_0x06bf
            java.lang.String r22 = "source"
            r0 = r5
            r1 = r22
            org.json.JSONObject r24 = r0.getJSONObject(r1)     // Catch:{ Exception -> 0x0361 }
            cn.com.mzba.model.Status r7 = new cn.com.mzba.model.Status     // Catch:{ Exception -> 0x0361 }
            r7.<init>()     // Catch:{ Exception -> 0x0361 }
            r22 = 1
            r0 = r9
            r1 = r22
            r0.setRetweetedStatus(r1)     // Catch:{ Exception -> 0x0361 }
            java.lang.String r22 = "text"
            r0 = r24
            r1 = r22
            java.lang.String r8 = r0.getString(r1)     // Catch:{ Exception -> 0x0361 }
            java.lang.String r22 = "id"
            r0 = r24
            r1 = r22
            java.lang.String r22 = r0.getString(r1)     // Catch:{ Exception -> 0x0361 }
            java.lang.String r10 = "nick"
            r0 = r24
            r1 = r10
            java.lang.String r24 = r0.getString(r1)     // Catch:{ Exception -> 0x0361 }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0361 }
            java.lang.String r12 = "@"
            r10.<init>(r12)     // Catch:{ Exception -> 0x0361 }
            r0 = r10
            r1 = r24
            java.lang.StringBuilder r24 = r0.append(r1)     // Catch:{ Exception -> 0x0361 }
            java.lang.String r10 = ":"
            r0 = r24
            r1 = r10
            java.lang.StringBuilder r24 = r0.append(r1)     // Catch:{ Exception -> 0x0361 }
            r0 = r24
            r1 = r8
            java.lang.StringBuilder r24 = r0.append(r1)     // Catch:{ Exception -> 0x0361 }
            java.lang.String r24 = r24.toString()     // Catch:{ Exception -> 0x0361 }
            r0 = r7
            r1 = r24
            r0.setText(r1)     // Catch:{ Exception -> 0x0361 }
            r0 = r7
            r1 = r22
            r0.setId(r1)     // Catch:{ Exception -> 0x0361 }
            r9.setStatus(r7)     // Catch:{ Exception -> 0x0361 }
        L_0x024e:
            java.lang.String r22 = "id"
            r0 = r5
            r1 = r22
            java.lang.String r24 = r0.getString(r1)     // Catch:{ Exception -> 0x0361 }
            java.lang.String r22 = "name"
            r0 = r5
            r1 = r22
            java.lang.String r15 = r0.getString(r1)     // Catch:{ Exception -> 0x0361 }
            java.lang.String r22 = "nick"
            r0 = r5
            r1 = r22
            java.lang.String r16 = r0.getString(r1)     // Catch:{ Exception -> 0x0361 }
            java.lang.String r22 = "head"
            r0 = r5
            r1 = r22
            java.lang.String r14 = r0.getString(r1)     // Catch:{ Exception -> 0x0361 }
            java.lang.String r22 = "timestamp"
            r0 = r5
            r1 = r22
            java.lang.String r13 = r0.getString(r1)     // Catch:{ Exception -> 0x0361 }
            java.lang.String r22 = "text"
            r0 = r5
            r1 = r22
            java.lang.String r10 = r0.getString(r1)     // Catch:{ Exception -> 0x0361 }
            java.lang.String r22 = "from"
            r0 = r5
            r1 = r22
            java.lang.String r8 = r0.getString(r1)     // Catch:{ Exception -> 0x0361 }
            java.lang.String r22 = "image"
            r0 = r5
            r1 = r22
            java.lang.String r22 = r0.getString(r1)     // Catch:{ Exception -> 0x0361 }
            if (r22 == 0) goto L_0x06bb
            java.lang.String r12 = "null"
            r0 = r22
            r1 = r12
            boolean r12 = r0.equals(r1)     // Catch:{ Exception -> 0x0361 }
            if (r12 != 0) goto L_0x06bb
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0361 }
            r18 = 2
            int r19 = r22.length()     // Catch:{ Exception -> 0x0361 }
            r20 = 2
            int r19 = r19 - r20
            r0 = r22
            r1 = r18
            r2 = r19
            java.lang.String r22 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x0361 }
            java.lang.String r22 = java.lang.String.valueOf(r22)     // Catch:{ Exception -> 0x0361 }
            r0 = r12
            r1 = r22
            r0.<init>(r1)     // Catch:{ Exception -> 0x0361 }
            java.lang.String r22 = "/120"
            r0 = r12
            r1 = r22
            java.lang.StringBuilder r22 = r0.append(r1)     // Catch:{ Exception -> 0x0361 }
            java.lang.String r22 = r22.toString()     // Catch:{ Exception -> 0x0361 }
            r12 = r22
        L_0x02d2:
            java.lang.String r22 = "mcount"
            r0 = r5
            r1 = r22
            java.lang.String r22 = r0.getString(r1)     // Catch:{ Exception -> 0x0361 }
            java.lang.String r18 = "count"
            r0 = r5
            r1 = r18
            java.lang.String r5 = r0.getString(r1)     // Catch:{ Exception -> 0x0361 }
            r9.setThumbnailPic(r12)     // Catch:{ Exception -> 0x0361 }
            r0 = r9
            r1 = r24
            r0.setId(r1)     // Catch:{ Exception -> 0x0361 }
            r9.setText(r10)     // Catch:{ Exception -> 0x0361 }
            r9.setCreatedAt(r13)     // Catch:{ Exception -> 0x0361 }
            java.lang.StringBuilder r24 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0361 }
            java.lang.String r10 = "来自："
            r0 = r24
            r1 = r10
            r0.<init>(r1)     // Catch:{ Exception -> 0x0361 }
            r0 = r24
            r1 = r8
            java.lang.StringBuilder r24 = r0.append(r1)     // Catch:{ Exception -> 0x0361 }
            java.lang.String r24 = r24.toString()     // Catch:{ Exception -> 0x0361 }
            r0 = r9
            r1 = r24
            r0.setSource(r1)     // Catch:{ Exception -> 0x0361 }
            r0 = r9
            r1 = r21
            r0.setWeiboId(r1)     // Catch:{ Exception -> 0x0361 }
            r0 = r17
            r1 = r15
            r0.setId(r1)     // Catch:{ Exception -> 0x0361 }
            java.lang.StringBuilder r24 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0361 }
            java.lang.String r8 = java.lang.String.valueOf(r14)     // Catch:{ Exception -> 0x0361 }
            r0 = r24
            r1 = r8
            r0.<init>(r1)     // Catch:{ Exception -> 0x0361 }
            java.lang.String r8 = "/40"
            r0 = r24
            r1 = r8
            java.lang.StringBuilder r24 = r0.append(r1)     // Catch:{ Exception -> 0x0361 }
            java.lang.String r24 = r24.toString()     // Catch:{ Exception -> 0x0361 }
            r0 = r17
            r1 = r24
            r0.setProfileImageUrl(r1)     // Catch:{ Exception -> 0x0361 }
            r0 = r17
            r1 = r16
            r0.setScreenName(r1)     // Catch:{ Exception -> 0x0361 }
            r0 = r9
            r1 = r17
            r0.setUser(r1)     // Catch:{ Exception -> 0x0361 }
            r0 = r9
            r1 = r21
            r0.setWeiboId(r1)     // Catch:{ Exception -> 0x0361 }
            r0 = r9
            r1 = r22
            r0.setComments(r1)     // Catch:{ Exception -> 0x0361 }
            r9.setRt(r5)     // Catch:{ Exception -> 0x0361 }
            r11.add(r9)     // Catch:{ Exception -> 0x0361 }
            r22 = r7
        L_0x035b:
            int r22 = r23 + 1
            r23 = r22
            goto L_0x01bc
        L_0x0361:
            r21 = move-exception
            r21.printStackTrace()
            goto L_0x00a5
        L_0x0367:
            java.lang.String r5 = "sohu_weibo"
            r0 = r21
            r1 = r5
            boolean r5 = r0.equals(r1)
            if (r5 == 0) goto L_0x0510
            java.lang.String r7 = "http://api.t.sohu.com/direct_messages.json"
            r5 = 0
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x04e4, IOException -> 0x04ee }
            java.lang.String r8 = "?page="
            r6.<init>(r8)     // Catch:{ MalformedURLException -> 0x04e4, IOException -> 0x04ee }
            r0 = r6
            r1 = r22
            java.lang.StringBuilder r22 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x04e4, IOException -> 0x04ee }
            java.lang.String r6 = "&count="
            r0 = r22
            r1 = r6
            java.lang.StringBuilder r22 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x04e4, IOException -> 0x04ee }
            java.lang.StringBuilder r22 = r22.append(r23)     // Catch:{ MalformedURLException -> 0x04e4, IOException -> 0x04ee }
            java.lang.String r22 = r22.toString()     // Catch:{ MalformedURLException -> 0x04e4, IOException -> 0x04ee }
            if (r24 == 0) goto L_0x03b9
            java.lang.StringBuilder r23 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x04e4, IOException -> 0x04ee }
            java.lang.String r22 = java.lang.String.valueOf(r22)     // Catch:{ MalformedURLException -> 0x04e4, IOException -> 0x04ee }
            r0 = r23
            r1 = r22
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x04e4, IOException -> 0x04ee }
            java.lang.String r22 = "&since_id="
            r0 = r23
            r1 = r22
            java.lang.StringBuilder r22 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x04e4, IOException -> 0x04ee }
            r0 = r22
            r1 = r24
            java.lang.StringBuilder r22 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x04e4, IOException -> 0x04ee }
            java.lang.String r22 = r22.toString()     // Catch:{ MalformedURLException -> 0x04e4, IOException -> 0x04ee }
        L_0x03b9:
            java.net.URL r23 = new java.net.URL     // Catch:{ MalformedURLException -> 0x04e4, IOException -> 0x04ee }
            java.lang.StringBuilder r24 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x04e4, IOException -> 0x04ee }
            java.lang.String r6 = java.lang.String.valueOf(r7)     // Catch:{ MalformedURLException -> 0x04e4, IOException -> 0x04ee }
            r0 = r24
            r1 = r6
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x04e4, IOException -> 0x04ee }
            r0 = r24
            r1 = r22
            java.lang.StringBuilder r22 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x04e4, IOException -> 0x04ee }
            java.lang.String r22 = r22.toString()     // Catch:{ MalformedURLException -> 0x04e4, IOException -> 0x04ee }
            r0 = r23
            r1 = r22
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x04e4, IOException -> 0x04ee }
            java.net.URLConnection r22 = r23.openConnection()     // Catch:{ MalformedURLException -> 0x04e4, IOException -> 0x04ee }
            java.net.HttpURLConnection r22 = (java.net.HttpURLConnection) r22     // Catch:{ MalformedURLException -> 0x04e4, IOException -> 0x04ee }
            java.lang.String r23 = "GET"
            r22.setRequestMethod(r23)     // Catch:{ MalformedURLException -> 0x06b8, IOException -> 0x06b5 }
        L_0x03e5:
            cn.com.mzba.oauth.OAuth r23 = cn.com.mzba.service.StatusHttpUtils.oauthSohu
            java.lang.String r24 = cn.com.mzba.service.StatusHttpUtils.userTokenSohu
            java.lang.String r5 = cn.com.mzba.service.StatusHttpUtils.userTokenSohuSecret
            r0 = r23
            r1 = r24
            r2 = r5
            r3 = r22
            java.net.HttpURLConnection r6 = r0.signRequest(r1, r2, r3)
            if (r6 == 0) goto L_0x00a5
            r22 = 200(0xc8, float:2.8E-43)
            int r23 = r6.getResponseCode()     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            r0 = r22
            r1 = r23
            if (r0 != r1) goto L_0x00a5
            java.io.InputStream r23 = r6.getInputStream()     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            java.io.BufferedReader r5 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            java.io.InputStreamReader r22 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            r22.<init>(r23)     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            r0 = r5
            r1 = r22
            r0.<init>(r1)     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            java.lang.StringBuilder r22 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            int r24 = r6.getContentLength()     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            r0 = r22
            r1 = r24
            r0.<init>(r1)     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            r24 = 4000(0xfa0, float:5.605E-42)
            r0 = r24
            char[] r0 = new char[r0]     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            r7 = r0
            r24 = 0
        L_0x042b:
            int r24 = r5.read(r7)     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            r8 = -1
            r0 = r24
            r1 = r8
            if (r0 != r1) goto L_0x04f8
            r5.close()     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            r23.close()     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            java.lang.String r22 = r22.toString()     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            r6.disconnect()     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            org.json.JSONArray r23 = new org.json.JSONArray     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            r0 = r23
            r1 = r22
            r0.<init>(r1)     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            r22 = 0
            r24 = r22
        L_0x044f:
            int r22 = r23.length()     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            r0 = r24
            r1 = r22
            if (r0 >= r1) goto L_0x00a5
            cn.com.mzba.model.Status r6 = new cn.com.mzba.model.Status     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            r6.<init>()     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            cn.com.mzba.model.User r9 = new cn.com.mzba.model.User     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            r9.<init>()     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            org.json.JSONObject r22 = r23.getJSONObject(r24)     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            if (r22 == 0) goto L_0x04de
            java.lang.String r5 = "sender"
            r0 = r22
            r1 = r5
            org.json.JSONObject r5 = r0.getJSONObject(r1)     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            java.lang.String r7 = "id"
            java.lang.String r7 = r5.getString(r7)     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            java.lang.String r8 = "screen_name"
            java.lang.String r8 = r5.getString(r8)     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            java.lang.String r10 = "profile_image_url"
            java.lang.String r5 = r5.getString(r10)     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            r9.setId(r7)     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            r9.setProfileImageUrl(r5)     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            r9.setScreenName(r8)     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            java.lang.String r5 = "id"
            r0 = r22
            r1 = r5
            java.lang.String r5 = r0.getString(r1)     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            java.lang.String r7 = "created_at"
            r0 = r22
            r1 = r7
            java.lang.String r8 = r0.getString(r1)     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            java.lang.String r7 = "text"
            r0 = r22
            r1 = r7
            java.lang.String r7 = r0.getString(r1)     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            java.lang.String r10 = "source"
            r0 = r22
            r1 = r10
            java.lang.String r22 = r0.getString(r1)     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            r6.setId(r5)     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            r6.setText(r7)     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            r6.setCreatedAt(r8)     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            java.lang.String r7 = "来自："
            r5.<init>(r7)     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            r0 = r5
            r1 = r22
            java.lang.StringBuilder r22 = r0.append(r1)     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            java.lang.String r22 = r22.toString()     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            r0 = r6
            r1 = r22
            r0.setSource(r1)     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            r6.setUser(r9)     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            r0 = r6
            r1 = r21
            r0.setWeiboId(r1)     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            r11.add(r6)     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
        L_0x04de:
            int r22 = r24 + 1
            r24 = r22
            goto L_0x044f
        L_0x04e4:
            r22 = move-exception
            r23 = r22
            r22 = r5
        L_0x04e9:
            r23.printStackTrace()
            goto L_0x03e5
        L_0x04ee:
            r22 = move-exception
            r23 = r22
            r22 = r5
        L_0x04f3:
            r23.printStackTrace()
            goto L_0x03e5
        L_0x04f8:
            r8 = 0
            r0 = r22
            r1 = r7
            r2 = r8
            r3 = r24
            r0.append(r1, r2, r3)     // Catch:{ IOException -> 0x0504, JSONException -> 0x050a }
            goto L_0x042b
        L_0x0504:
            r21 = move-exception
            r21.printStackTrace()
            goto L_0x00a5
        L_0x050a:
            r21 = move-exception
            r21.printStackTrace()
            goto L_0x00a5
        L_0x0510:
            java.lang.String r22 = "netease_weibo"
            boolean r21 = r21.equals(r22)
            if (r21 == 0) goto L_0x00a5
            java.lang.String r7 = "http://api.t.163.com/direct_messages.json"
            r21 = 0
            java.lang.StringBuilder r22 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0691, IOException -> 0x0697 }
            java.lang.String r5 = "?count="
            r0 = r22
            r1 = r5
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x0691, IOException -> 0x0697 }
            java.lang.StringBuilder r22 = r22.append(r23)     // Catch:{ MalformedURLException -> 0x0691, IOException -> 0x0697 }
            java.lang.String r22 = r22.toString()     // Catch:{ MalformedURLException -> 0x0691, IOException -> 0x0697 }
            if (r24 == 0) goto L_0x0553
            java.lang.StringBuilder r23 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0691, IOException -> 0x0697 }
            java.lang.String r22 = java.lang.String.valueOf(r22)     // Catch:{ MalformedURLException -> 0x0691, IOException -> 0x0697 }
            r0 = r23
            r1 = r22
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x0691, IOException -> 0x0697 }
            java.lang.String r22 = "&since_id="
            r0 = r23
            r1 = r22
            java.lang.StringBuilder r22 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x0691, IOException -> 0x0697 }
            r0 = r22
            r1 = r24
            java.lang.StringBuilder r22 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x0691, IOException -> 0x0697 }
            java.lang.String r22 = r22.toString()     // Catch:{ MalformedURLException -> 0x0691, IOException -> 0x0697 }
        L_0x0553:
            java.net.URL r23 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0691, IOException -> 0x0697 }
            java.lang.StringBuilder r24 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0691, IOException -> 0x0697 }
            java.lang.String r5 = java.lang.String.valueOf(r7)     // Catch:{ MalformedURLException -> 0x0691, IOException -> 0x0697 }
            r0 = r24
            r1 = r5
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x0691, IOException -> 0x0697 }
            r0 = r24
            r1 = r22
            java.lang.StringBuilder r22 = r0.append(r1)     // Catch:{ MalformedURLException -> 0x0691, IOException -> 0x0697 }
            java.lang.String r22 = r22.toString()     // Catch:{ MalformedURLException -> 0x0691, IOException -> 0x0697 }
            r0 = r23
            r1 = r22
            r0.<init>(r1)     // Catch:{ MalformedURLException -> 0x0691, IOException -> 0x0697 }
            java.net.URLConnection r22 = r23.openConnection()     // Catch:{ MalformedURLException -> 0x0691, IOException -> 0x0697 }
            r0 = r22
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x0691, IOException -> 0x0697 }
            r21 = r0
            java.lang.String r22 = "GET"
            r21.setRequestMethod(r22)     // Catch:{ MalformedURLException -> 0x0691, IOException -> 0x0697 }
        L_0x0583:
            cn.com.mzba.oauth.OAuth r22 = cn.com.mzba.service.StatusHttpUtils.oauthNetease
            java.lang.String r23 = cn.com.mzba.service.StatusHttpUtils.userTokenNetease
            java.lang.String r24 = cn.com.mzba.service.StatusHttpUtils.userTokenNeteaseSecret
            r0 = r22
            r1 = r23
            r2 = r24
            r3 = r21
            java.net.HttpURLConnection r5 = r0.signRequest(r1, r2, r3)
            if (r5 == 0) goto L_0x00a5
            r21 = 200(0xc8, float:2.8E-43)
            int r22 = r5.getResponseCode()     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            r0 = r21
            r1 = r22
            if (r0 != r1) goto L_0x00a5
            java.io.InputStream r22 = r5.getInputStream()     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            java.io.BufferedReader r24 = new java.io.BufferedReader     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            java.io.InputStreamReader r21 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            r21.<init>(r22)     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            r0 = r24
            r1 = r21
            r0.<init>(r1)     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            r21.<init>()     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            r23 = 4000(0xfa0, float:5.605E-42)
            r0 = r23
            char[] r0 = new char[r0]     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            r6 = r0
            r23 = 0
        L_0x05c3:
            r0 = r24
            r1 = r6
            int r23 = r0.read(r1)     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            r7 = -1
            r0 = r23
            r1 = r7
            if (r0 != r1) goto L_0x069d
            r24.close()     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            r22.close()     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            java.lang.String r21 = r21.toString()     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            r5.disconnect()     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            org.json.JSONArray r22 = new org.json.JSONArray     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            r0 = r22
            r1 = r21
            r0.<init>(r1)     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            r21 = 0
            r23 = r21
        L_0x05ea:
            int r21 = r22.length()     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            r0 = r23
            r1 = r21
            if (r0 >= r1) goto L_0x00a5
            cn.com.mzba.model.Status r5 = new cn.com.mzba.model.Status     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            r5.<init>()     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            cn.com.mzba.model.User r9 = new cn.com.mzba.model.User     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            r9.<init>()     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            org.json.JSONObject r21 = r22.getJSONObject(r23)     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            if (r21 == 0) goto L_0x068b
            java.lang.String r24 = "sender"
            r0 = r21
            r1 = r24
            org.json.JSONObject r8 = r0.getJSONObject(r1)     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            java.lang.String r24 = "id"
            r0 = r21
            r1 = r24
            java.lang.String r24 = r0.getString(r1)     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            java.lang.String r6 = "created_at"
            r0 = r21
            r1 = r6
            java.lang.String r7 = r0.getString(r1)     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            java.lang.String r6 = "text"
            r0 = r21
            r1 = r6
            java.lang.String r6 = r0.getString(r1)     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            java.lang.String r10 = "source"
            r0 = r21
            r1 = r10
            java.lang.String r21 = r0.getString(r1)     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            r0 = r5
            r1 = r24
            r0.setId(r1)     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            r5.setText(r6)     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            r5.setCreatedAt(r7)     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            java.lang.StringBuilder r24 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            java.lang.String r6 = "来自："
            r0 = r24
            r1 = r6
            r0.<init>(r1)     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            r0 = r24
            r1 = r21
            java.lang.StringBuilder r21 = r0.append(r1)     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            java.lang.String r21 = r21.toString()     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            r0 = r5
            r1 = r21
            r0.setSource(r1)     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            java.lang.String r21 = "id"
            r0 = r8
            r1 = r21
            java.lang.String r24 = r0.getString(r1)     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            java.lang.String r21 = "screen_name"
            r0 = r8
            r1 = r21
            java.lang.String r6 = r0.getString(r1)     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            java.lang.String r21 = "profile_image_url"
            r0 = r8
            r1 = r21
            java.lang.String r21 = r0.getString(r1)     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            r0 = r9
            r1 = r24
            r0.setId(r1)     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            r0 = r9
            r1 = r21
            r0.setProfileImageUrl(r1)     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            r9.setScreenName(r6)     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            r5.setUser(r9)     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            r11.add(r5)     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
        L_0x068b:
            int r21 = r23 + 1
            r23 = r21
            goto L_0x05ea
        L_0x0691:
            r22 = move-exception
            r22.printStackTrace()
            goto L_0x0583
        L_0x0697:
            r22 = move-exception
            r22.printStackTrace()
            goto L_0x0583
        L_0x069d:
            r7 = 0
            r0 = r21
            r1 = r6
            r2 = r7
            r3 = r23
            r0.append(r1, r2, r3)     // Catch:{ IOException -> 0x06a9, JSONException -> 0x06af }
            goto L_0x05c3
        L_0x06a9:
            r21 = move-exception
            r21.printStackTrace()
            goto L_0x00a5
        L_0x06af:
            r21 = move-exception
            r21.printStackTrace()
            goto L_0x00a5
        L_0x06b5:
            r23 = move-exception
            goto L_0x04f3
        L_0x06b8:
            r23 = move-exception
            goto L_0x04e9
        L_0x06bb:
            r12 = r22
            goto L_0x02d2
        L_0x06bf:
            r7 = r22
            goto L_0x024e
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.com.mzba.service.StatusHttpUtils.getDirectStatus(java.lang.String, int, int, java.lang.String):java.util.List");
    }
}
