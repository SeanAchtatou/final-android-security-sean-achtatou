package cn.com.mzba.service;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import cn.com.mzba.db.ConfigHelper;
import cn.com.mzba.oauth.OAuth;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.commons.httpclient.cookie.CookieSpec;

public class ServiceUtils {
    public static Drawable drawable = null;
    public static OAuth neteaseOauth;
    public static OAuth sinaOauth;
    public static OAuth sohuOauth;
    public static OAuth tencentOauth;
    public static URL url;
    public static String userTokenNetease;
    public static String userTokenNeteaseSecret;
    public static String userTokenSina;
    public static String userTokenSinaSecret;
    public static String userTokenSohu;
    public static String userTokenSohuSecret;
    public static String userTokenTencent;
    public static String userTokenTencentSecret;

    public static OAuth getSinaOauth() {
        if (sinaOauth == null) {
            sinaOauth = new OAuth(SystemConfig.SINA_WEIBO, SystemConfig.CONSUMERKEY_SINA, SystemConfig.CONSUMERSECRET_SINA);
            if (ConfigHelper.USERINFO_SINA != null) {
                userTokenSina = ConfigHelper.USERINFO_SINA.getToken();
                userTokenSinaSecret = ConfigHelper.USERINFO_SINA.getTokenSecret();
            }
        }
        return sinaOauth;
    }

    public static OAuth getTencentOauth() {
        if (tencentOauth == null) {
            tencentOauth = new OAuth(SystemConfig.TENCENT_WEIBO, SystemConfig.CONSUMERKEY_TENCENT, SystemConfig.CONSUMERSECRET_TENCENT);
            if (ConfigHelper.USERINFO_TENCENT != null) {
                userTokenTencent = ConfigHelper.USERINFO_TENCENT.getToken();
                userTokenTencentSecret = ConfigHelper.USERINFO_TENCENT.getTokenSecret();
            }
        }
        return tencentOauth;
    }

    public static OAuth getSohuOauth() {
        if (sohuOauth == null) {
            sohuOauth = new OAuth(SystemConfig.SOHU_WEIBO, SystemConfig.CONSUMERKEY_SOHU, SystemConfig.CONSUMERSECRET_SOHU);
            if (ConfigHelper.USERINFO_SOHU != null) {
                userTokenSohu = ConfigHelper.USERINFO_SOHU.getToken();
                userTokenSohuSecret = ConfigHelper.USERINFO_SOHU.getTokenSecret();
            }
        }
        return sohuOauth;
    }

    public static OAuth getNeteaseOauth() {
        if (neteaseOauth == null) {
            neteaseOauth = new OAuth(SystemConfig.NETEASE_WEIBO, SystemConfig.CONSUMERKEY_NETEASE, SystemConfig.CONSUMERSECRET_NETEASE);
            if (ConfigHelper.USERINFO_NETEASE != null) {
                userTokenNetease = ConfigHelper.USERINFO_NETEASE.getToken();
                userTokenNeteaseSecret = ConfigHelper.USERINFO_NETEASE.getTokenSecret();
            }
        }
        return neteaseOauth;
    }

    public static Bitmap downImage(String path) {
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL(path).openConnection();
            conn.setRequestMethod(SystemConfig.HTTP_GET);
            conn.setReadTimeout(5000);
            if (conn.getResponseCode() != 200) {
                return null;
            }
            InputStream is = conn.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(is);
            is.close();
            return bitmap;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void sharedPreferences(Activity activity, String userId) {
        SharedPreferences.Editor editor = activity.getSharedPreferences("shared", 0).edit();
        editor.putString("name", userId);
        editor.commit();
    }

    public static String getUserNameFromSharedPreferences(Activity activity) {
        return activity.getSharedPreferences("shared", 0).getString("name", "");
    }

    public static byte[] readStream(InputStream is) throws Exception {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        byte[] buffer = new byte[2048];
        while (true) {
            int len = is.read(buffer);
            if (len == -1) {
                is.close();
                return os.toByteArray();
            }
            os.write(buffer, 0, len);
        }
    }

    public static byte[] getImageFromUrlPath(String urlPath) {
        byte[] data = null;
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL(urlPath).openConnection();
            conn.setRequestMethod(SystemConfig.HTTP_GET);
            conn.setReadTimeout(5000);
            if (conn.getResponseCode() == 200) {
                return readStream(conn.getInputStream());
            }
            System.out.println("请求失败");
            return data;
        } catch (Exception e) {
            e.printStackTrace();
            return data;
        }
    }

    public static Bitmap getBitmapFromByte(byte[] temp) {
        if (temp != null) {
            return BitmapFactory.decodeByteArray(temp, 0, temp.length);
        }
        return null;
    }

    public static Bitmap drawableToBitmap(Drawable drawable2) {
        Bitmap.Config config;
        int intrinsicWidth = drawable2.getIntrinsicWidth();
        int intrinsicHeight = drawable2.getIntrinsicHeight();
        if (drawable2.getOpacity() != -1) {
            config = Bitmap.Config.ARGB_8888;
        } else {
            config = Bitmap.Config.RGB_565;
        }
        Bitmap bitmap = Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, config);
        Canvas canvas = new Canvas(bitmap);
        drawable2.setBounds(0, 0, drawable2.getIntrinsicWidth(), drawable2.getIntrinsicHeight());
        drawable2.draw(canvas);
        return bitmap;
    }

    public static void downLoadImage(String imagePath) {
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL(imagePath).openConnection();
            conn.setRequestMethod(SystemConfig.HTTP_GET);
            conn.setReadTimeout(5000);
            if (conn.getResponseCode() == 200) {
                byte[] data = readStream(conn.getInputStream());
                FileOutputStream fs = new FileOutputStream(getFile(imagePath));
                fs.write(data);
                fs.close();
                return;
            }
            System.out.println("请求失败");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static File getFile(String path) {
        File dir = new File(String.valueOf(Environment.getExternalStorageDirectory() + CookieSpec.PATH_DELIM) + "kdwb/download");
        if (!dir.exists()) {
            dir.mkdirs();
        }
        File file = new File(dir + File.separator + path.substring(path.lastIndexOf(CookieSpec.PATH_DELIM) + 1));
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return file;
    }

    public static boolean isConnectInternet(Activity activity) {
        NetworkInfo networkInfo = ((ConnectivityManager) activity.getSystemService("connectivity")).getActiveNetworkInfo();
        if (networkInfo != null) {
            return networkInfo.isAvailable();
        }
        return false;
    }
}
