package cn.com.mzba.service;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;

public interface ImageCallback {
    void imageLoaded(Drawable drawable, ImageView imageView, String str);
}
