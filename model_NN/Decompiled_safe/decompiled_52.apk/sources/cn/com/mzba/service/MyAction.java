package cn.com.mzba.service;

public class MyAction {
    public static final String AUTOREMIND = "ACTION_AUTO_REMIND";
    public static final String COMMENT = "comment";
    public static final String DELETESTATUS = "ACTION_DELETE_STATUS";
    public static final String EMOSTION_CLICK = "ACTION_EMOSTION_CLICK";
    public static final String FAVORITED = "favorited";
    public static final String FOLLOWERS = "followers";
    public static final String FRIENDS = "friends";
    public static final String REDIRECT = "redirect";
    public static final String USERSTATUS = "user_status";
    public static final String VERIFIER = "ACTION_VERFIER";
}
