package cn.com.mzba.kdwb;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import cn.com.mzba.db.UserInfo;
import cn.com.mzba.model.Comment;
import cn.com.mzba.model.CommentsAdapter;
import cn.com.mzba.service.CommentTopicHttpUtils;
import cn.com.mzba.service.ServiceUtils;
import cn.com.mzba.service.SystemConfig;
import java.util.List;

public class MessageCommentActivity extends ListActivity implements View.OnClickListener {
    /* access modifiers changed from: private */
    public CommentsAdapter adapter;
    private Button btnNetease;
    private Button btnSina;
    private Button btnSohu;
    private Button btnTencent;
    /* access modifiers changed from: private */
    public int count = 20;
    /* access modifiers changed from: private */
    public int page = 1;
    /* access modifiers changed from: private */
    public String sinceId;
    /* access modifiers changed from: private */
    public String weiboId = SystemConfig.SINA_WEIBO;
    /* access modifiers changed from: private */
    public List<Comment> weiboList;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.messagecommentlist);
        this.btnSina = (Button) findViewById(R.id.third_sina);
        this.btnSina.setOnClickListener(this);
        this.btnTencent = (Button) findViewById(R.id.third_tencent);
        this.btnTencent.setOnClickListener(this);
        this.btnNetease = (Button) findViewById(R.id.third_netease);
        this.btnNetease.setOnClickListener(this);
        this.btnSohu = (Button) findViewById(R.id.third_sohu);
        this.btnSohu.setOnClickListener(this);
        this.btnSina.setBackgroundResource(R.drawable.blog_thrid_left_d);
        if (ServiceUtils.isConnectInternet(this)) {
            new LoadWeiBoInfo().execute("");
            return;
        }
        Toast.makeText(this, "网络出现异常", 1).show();
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        if (position == 0) {
            this.page = 1;
            new LoadWeiBoInfo().execute("");
        } else if (position == getListAdapter().getCount() - 1) {
            showDialog(0);
            new Thread() {
                public void run() {
                    MessageCommentActivity messageCommentActivity = MessageCommentActivity.this;
                    messageCommentActivity.page = messageCommentActivity.page + 1;
                    final List<Comment> list = CommentTopicHttpUtils.getCommentTimeline(MessageCommentActivity.this.weiboId, MessageCommentActivity.this.page, MessageCommentActivity.this.count, MessageCommentActivity.this.sinceId);
                    if (list == null || list.isEmpty()) {
                        MessageCommentActivity.this.removeDialog(0);
                        Toast.makeText(MessageCommentActivity.this, "加载数据失败", 1).show();
                        return;
                    }
                    MessageCommentActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            MessageCommentActivity.this.adapter.addMoreDatas(list);
                            MessageCommentActivity.this.sinceId = ((Comment) MessageCommentActivity.this.weiboList.get(0)).getId();
                            MessageCommentActivity.this.removeDialog(0);
                        }
                    });
                }
            }.start();
        } else {
            onclick(this.weiboList.get(position - 1).getUser().getId(), this.weiboList.get(position - 1).getId(), this.weiboList.get(position - 1).getStatus().getId());
        }
    }

    public void onclick(final String userId, final String id, final String cid) {
        Context dialogContext = new ContextThemeWrapper(getParent(), 16973836);
        ListAdapter adapter2 = new ArrayAdapter(dialogContext, 17367043, new String[]{"回复评论", "查看资料", "查看原微博"});
        AlertDialog.Builder builder = new AlertDialog.Builder(dialogContext);
        builder.setTitle((int) R.string.choice_do);
        builder.setSingleChoiceItems(adapter2, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                switch (which) {
                    case 0:
                        Intent intent = new Intent();
                        intent.setClass(MessageCommentActivity.this, NewCommentActivity.class);
                        Bundle extras = new Bundle();
                        extras.putString("key", "comment");
                        extras.putString("id", id);
                        extras.putString("cid", cid);
                        extras.putString(UserInfo.WEIBOID, MessageCommentActivity.this.weiboId);
                        intent.putExtras(extras);
                        MessageCommentActivity.this.startActivity(intent);
                        return;
                    case 1:
                        Intent intent3 = new Intent();
                        intent3.setClass(MessageCommentActivity.this, UserInfoActivity.class);
                        Bundle extras3 = new Bundle();
                        extras3.putString(UserInfo.USERID, userId);
                        intent3.putExtras(extras3);
                        MessageCommentActivity.this.startActivity(intent3);
                        return;
                    case 2:
                        Intent intent2 = new Intent();
                        intent2.setClass(MessageCommentActivity.this, DetailActivity.class);
                        intent2.putExtra("id", id);
                        intent2.putExtra(UserInfo.WEIBOID, MessageCommentActivity.this.weiboId);
                        MessageCommentActivity.this.startActivity(intent2);
                        return;
                    default:
                        return;
                }
            }
        });
        builder.setNegativeButton("返回", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        ProgressDialog dialog = new ProgressDialog(getParent());
        dialog.setMessage("加载中...请稍候");
        dialog.setIndeterminate(true);
        dialog.setCancelable(true);
        return dialog;
    }

    public class LoadWeiBoInfo extends AsyncTask<String, String, String> {
        public LoadWeiBoInfo() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            MessageCommentActivity.this.weiboList = CommentTopicHttpUtils.getCommentTimeline(MessageCommentActivity.this.weiboId, MessageCommentActivity.this.page, MessageCommentActivity.this.count, "");
            if (MessageCommentActivity.this.weiboList != null && !MessageCommentActivity.this.weiboList.isEmpty()) {
                MessageCommentActivity.this.sinceId = ((Comment) MessageCommentActivity.this.weiboList.get(0)).getId();
            }
            return "";
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            if (MessageCommentActivity.this.weiboList != null && !MessageCommentActivity.this.weiboList.isEmpty()) {
                MessageCommentActivity.this.adapter = new CommentsAdapter(MessageCommentActivity.this, MessageCommentActivity.this.weiboList);
                MessageCommentActivity.this.setListAdapter(MessageCommentActivity.this.adapter);
            }
            MessageCommentActivity.this.removeDialog(0);
            super.onPostExecute((Object) result);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            MessageCommentActivity.this.showDialog(0);
            super.onPreExecute();
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.third_sina /*2131361992*/:
                if (this.weiboList != null) {
                    this.weiboList.clear();
                }
                if (this.adapter != null) {
                    this.adapter.notifyDataSetChanged();
                }
                this.weiboId = SystemConfig.SINA_WEIBO;
                new LoadWeiBoInfo().execute("");
                this.btnSina.setBackgroundResource(R.drawable.blog_thrid_left_d);
                this.btnTencent.setBackgroundResource(R.drawable.blog_thrid_center_n);
                this.btnSohu.setBackgroundResource(R.drawable.blog_thrid_center_n);
                this.btnNetease.setBackgroundResource(R.drawable.blog_thrid_right_n);
                return;
            case R.id.third_tencent /*2131361993*/:
                if (this.weiboList != null) {
                    this.weiboList.clear();
                }
                if (this.adapter != null) {
                    this.adapter.notifyDataSetChanged();
                }
                this.weiboId = SystemConfig.TENCENT_WEIBO;
                new LoadWeiBoInfo().execute("");
                this.btnSina.setBackgroundResource(R.drawable.blog_thrid_left_n);
                this.btnTencent.setBackgroundResource(R.drawable.blog_thrid_center_d);
                this.btnSohu.setBackgroundResource(R.drawable.blog_thrid_center_n);
                this.btnNetease.setBackgroundResource(R.drawable.blog_thrid_right_n);
                return;
            case R.id.third_sohu /*2131361994*/:
                if (this.weiboList != null) {
                    this.weiboList.clear();
                }
                if (this.adapter != null) {
                    this.adapter.notifyDataSetChanged();
                }
                this.weiboId = SystemConfig.SOHU_WEIBO;
                new LoadWeiBoInfo().execute("");
                this.btnSina.setBackgroundResource(R.drawable.blog_thrid_left_n);
                this.btnTencent.setBackgroundResource(R.drawable.blog_thrid_center_n);
                this.btnSohu.setBackgroundResource(R.drawable.blog_thrid_center_d);
                this.btnNetease.setBackgroundResource(R.drawable.blog_thrid_right_n);
                return;
            case R.id.third_netease /*2131361995*/:
                if (this.weiboList != null) {
                    this.weiboList.clear();
                }
                if (this.adapter != null) {
                    this.adapter.notifyDataSetChanged();
                }
                this.weiboId = SystemConfig.NETEASE_WEIBO;
                new LoadWeiBoInfo().execute("");
                this.btnSina.setBackgroundResource(R.drawable.blog_thrid_left_n);
                this.btnTencent.setBackgroundResource(R.drawable.blog_thrid_center_n);
                this.btnSohu.setBackgroundResource(R.drawable.blog_thrid_center_n);
                this.btnNetease.setBackgroundResource(R.drawable.blog_thrid_right_d);
                return;
            default:
                return;
        }
    }
}
