package cn.com.mzba.kdwb;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import cn.com.mzba.kdwb.R;
import cn.com.mzba.model.Emotions;
import java.util.List;

public class GridViewAdapter extends BaseAdapter {
    private Activity activity;
    public List<Emotions> emotions;

    public GridViewAdapter(Activity activity2, List<Emotions> emotions2) {
        this.emotions = emotions2;
        this.activity = activity2;
    }

    public int getCount() {
        return this.emotions.size();
    }

    public Object getItem(int position) {
        return this.emotions.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    /* Debug info: failed to restart local var, previous not found, register: 7 */
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        Class<R.drawable> cls = R.drawable.class;
        if (convertView == null) {
            imageView = new ImageView(this.activity.getApplicationContext());
        } else {
            imageView = (ImageView) convertView;
        }
        try {
            imageView.setImageResource(R.drawable.class.getDeclaredField(this.emotions.get(position).getImageName()).getInt(R.drawable.class));
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e2) {
            e2.printStackTrace();
        } catch (IllegalArgumentException e3) {
            e3.printStackTrace();
        } catch (IllegalAccessException e4) {
            e4.printStackTrace();
        }
        return imageView;
    }
}
