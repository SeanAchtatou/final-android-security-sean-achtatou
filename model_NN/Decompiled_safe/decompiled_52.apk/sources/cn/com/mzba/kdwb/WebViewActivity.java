package cn.com.mzba.kdwb;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import cn.com.mzba.model.Emotions;
import cn.com.mzba.service.MyAction;
import cn.com.mzba.service.ServiceUtils;
import cn.com.mzba.service.SystemConfig;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WebViewActivity extends Activity {
    /* access modifiers changed from: private */
    public int index = 0;
    /* access modifiers changed from: private */
    public String verifier = "";
    private WebView webView;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(2);
        setContentView((int) R.layout.webview);
        this.webView = (WebView) findViewById(R.id.webView);
        this.webView.getSettings().setJavaScriptEnabled(true);
        this.webView.getSettings().setSupportZoom(true);
        this.webView.getSettings().setBuiltInZoomControls(true);
        this.webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int newProgress) {
                WebViewActivity.this.setProgress(newProgress * 100);
            }
        });
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey(Emotions.URL)) {
            String url = extras.getString(Emotions.URL);
            if (ServiceUtils.isConnectInternet(this)) {
                this.webView.loadUrl(url);
            } else {
                Toast.makeText(this, "网络连接失败", 1).show();
                return;
            }
        }
        this.webView.addJavascriptInterface(new JavaScriptInterface(), "Methods");
        this.webView.setWebViewClient(new WebViewClient() {
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                Log.i(WeiboActivity.class.getCanonicalName(), url);
                Matcher m = Pattern.compile("^app:AuthorizeActivity.*oauth_verifier=(\\d+)").matcher(url);
                if (m.find() && WebViewActivity.this.index == 0) {
                    WebViewActivity webViewActivity = WebViewActivity.this;
                    webViewActivity.index = webViewActivity.index + 1;
                    WebViewActivity.this.verifier = m.group(1);
                    Intent intent = new Intent();
                    intent.setAction(MyAction.VERIFIER);
                    Bundle extras = new Bundle();
                    extras.putString("verifier", WebViewActivity.this.verifier);
                    intent.putExtras(extras);
                    WebViewActivity.this.sendBroadcast(intent);
                    WebViewActivity.this.finish();
                }
                if (url.indexOf("http://api.t.163.com/oauth/authorize") != -1 && url.indexOf("authorize=1") != -1) {
                    SystemConfig.ISACCESS = true;
                    Toast.makeText(WebViewActivity.this, "请重新返回“绑定界面”进行最后授权！", 1).show();
                }
            }

            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                handler.proceed();
            }
        });
    }

    class JavaScriptInterface {
        JavaScriptInterface() {
        }

        public void getHTML(String html) {
            Log.i(WebViewActivity.class.getCanonicalName(), html);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || !this.webView.canGoBack()) {
            return super.onKeyDown(keyCode, event);
        }
        this.webView.goBack();
        return true;
    }
}
