package cn.com.mzba.kdwb;

import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import cn.com.mzba.db.UserInfo;
import cn.com.mzba.model.User;
import cn.com.mzba.service.AsyncImageLoader;
import cn.com.mzba.service.ImageCallback;
import cn.com.mzba.service.MyAction;
import cn.com.mzba.service.ServiceUtils;
import cn.com.mzba.service.SystemConfig;
import cn.com.mzba.service.UserHttpUtils;
import java.util.List;

public class AttentionOrFansActivity extends ListActivity {
    /* access modifiers changed from: private */
    public ListAdapter adapter;
    /* access modifiers changed from: private */
    public AsyncImageLoader asyncImageLoader = new AsyncImageLoader();
    private Button btnBack;
    private ImageButton btnIndex;
    /* access modifiers changed from: private */
    public int count = 20;
    /* access modifiers changed from: private */
    public int cursor = 0;
    /* access modifiers changed from: private */
    public boolean isAttendance;
    /* access modifiers changed from: private */
    public String key;
    /* access modifiers changed from: private */
    public List<User> list;
    /* access modifiers changed from: private */
    public int page = 1;
    private TextView textView;
    /* access modifiers changed from: private */
    public String userId;
    /* access modifiers changed from: private */
    public List<User> users;
    /* access modifiers changed from: private */
    public String weiboId;

    public static class ViewHolder {
        Button btnAttendance;
        ImageView ivUserIcon;
        TextView tvContent;
        TextView tvUserName;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.attentionorfanslist);
        this.textView = (TextView) findViewById(R.id.attentionorfans_textView);
        this.btnIndex = (ImageButton) findViewById(R.id.attentionorfans_index);
        this.btnIndex.setOnClickListener(new ButtonOnClickListener());
        this.btnBack = (Button) findViewById(R.id.attentionorfans_back);
        this.btnBack.setOnClickListener(new ButtonOnClickListener());
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        Intent intent = getIntent();
        this.userId = intent.getStringExtra(UserInfo.USERID);
        this.key = intent.getStringExtra("key");
        this.weiboId = intent.getStringExtra(UserInfo.WEIBOID);
        if (this.key.equals(MyAction.FRIENDS)) {
            this.textView.setText("关注");
        } else if (this.key.equals(MyAction.FOLLOWERS)) {
            this.textView.setText("粉丝");
        }
        if (ServiceUtils.isConnectInternet(this)) {
            new LoadWeiBoInfo().execute("");
            return;
        }
        Toast.makeText(this, "网络出现异常", 1).show();
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        if (position == 0) {
            refresh();
        } else if (position == getListAdapter().getCount() - 1) {
            loadMoreData();
        } else {
            Intent intent = new Intent(this, UserInfoActivity.class);
            intent.putExtra(UserInfo.WEIBOID, this.weiboId);
            intent.putExtra(UserInfo.USERID, this.userId);
            startActivity(intent);
        }
    }

    private void refresh() {
        this.page = 1;
        new LoadWeiBoInfo().execute("");
    }

    private void loadMoreData() {
        showDialog(0);
        new Thread() {
            public void run() {
                AttentionOrFansActivity attentionOrFansActivity = AttentionOrFansActivity.this;
                attentionOrFansActivity.page = attentionOrFansActivity.page + 1;
                AttentionOrFansActivity.this.cursor = AttentionOrFansActivity.this.page * 30;
                if (AttentionOrFansActivity.this.key.equals(MyAction.FRIENDS)) {
                    if (AttentionOrFansActivity.this.weiboId.equals(SystemConfig.NETEASE_WEIBO)) {
                        AttentionOrFansActivity.this.list = UserHttpUtils.getUserFriends(AttentionOrFansActivity.this.weiboId, AttentionOrFansActivity.this.userId, AttentionOrFansActivity.this.page, AttentionOrFansActivity.this.count, AttentionOrFansActivity.this.cursor);
                    } else {
                        AttentionOrFansActivity.this.list = UserHttpUtils.getUserFriends(AttentionOrFansActivity.this.weiboId, AttentionOrFansActivity.this.userId, AttentionOrFansActivity.this.page, AttentionOrFansActivity.this.count, 0);
                    }
                } else if (AttentionOrFansActivity.this.key.equals(MyAction.FOLLOWERS)) {
                    if (AttentionOrFansActivity.this.weiboId.equals(SystemConfig.NETEASE_WEIBO)) {
                        AttentionOrFansActivity.this.list = UserHttpUtils.getUserFollowers(AttentionOrFansActivity.this.weiboId, AttentionOrFansActivity.this.userId, AttentionOrFansActivity.this.page, AttentionOrFansActivity.this.count, 30);
                    } else {
                        AttentionOrFansActivity.this.list = UserHttpUtils.getUserFollowers(AttentionOrFansActivity.this.weiboId, AttentionOrFansActivity.this.userId, AttentionOrFansActivity.this.page, AttentionOrFansActivity.this.count, 0);
                    }
                }
                if (AttentionOrFansActivity.this.list == null || AttentionOrFansActivity.this.list.isEmpty()) {
                    AttentionOrFansActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            AttentionOrFansActivity.this.removeDialog(0);
                            Toast.makeText(AttentionOrFansActivity.this, "加载数据失败", 1).show();
                        }
                    });
                } else {
                    AttentionOrFansActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            AttentionOrFansActivity.this.users.addAll(AttentionOrFansActivity.this.list);
                            AttentionOrFansActivity.this.adapter.notifyDataSetChanged();
                            AttentionOrFansActivity.this.removeDialog(0);
                        }
                    });
                }
            }
        }.start();
    }

    public class ListAdapter extends BaseAdapter {
        public ListAdapter() {
        }

        public int getCount() {
            return AttentionOrFansActivity.this.users.size() + 2;
        }

        public Object getItem(int position) {
            return AttentionOrFansActivity.this.users.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View view;
            if (position == 0) {
                return LayoutInflater.from(AttentionOrFansActivity.this.getApplicationContext()).inflate((int) R.layout.listheader, (ViewGroup) null);
            }
            if (position == getCount() - 1) {
                return LayoutInflater.from(AttentionOrFansActivity.this.getApplicationContext()).inflate((int) R.layout.listfooter, (ViewGroup) null);
            }
            ViewHolder holder = new ViewHolder();
            if (convertView == null || convertView.getId() != R.id.attentionorfanslistitem_layout) {
                view = LayoutInflater.from(AttentionOrFansActivity.this.getApplicationContext()).inflate((int) R.layout.attentionorfanslistitem, (ViewGroup) null);
                holder.ivUserIcon = (ImageView) view.findViewById(R.id.attentionorfans_userIcon);
                holder.tvUserName = (TextView) view.findViewById(R.id.attentionorfans_userName);
                holder.tvContent = (TextView) view.findViewById(R.id.attentionorfans_text);
                holder.btnAttendance = (Button) view.findViewById(R.id.attendance);
                view.setTag(holder);
            } else {
                view = convertView;
                holder = (ViewHolder) view.getTag();
            }
            Drawable drawable = AttentionOrFansActivity.this.asyncImageLoader.loadDrawable(((User) AttentionOrFansActivity.this.users.get(position - 1)).getProfileImageUrl(), holder.ivUserIcon, new ImageCallback() {
                public void imageLoaded(Drawable imageDrawable, ImageView imageView, String imageUrl) {
                    imageView.setImageDrawable(imageDrawable);
                }
            });
            if (drawable == null) {
                holder.ivUserIcon.setImageResource(R.drawable.portrait);
            } else {
                holder.ivUserIcon.setImageDrawable(drawable);
            }
            holder.tvUserName.setText(((User) AttentionOrFansActivity.this.users.get(position - 1)).getScreenName());
            holder.tvContent.setText(((User) AttentionOrFansActivity.this.users.get(position - 1)).getStatus().getText());
            if (((User) AttentionOrFansActivity.this.users.get(position - 1)).isFollowing()) {
                AttentionOrFansActivity.this.isAttendance = true;
                holder.btnAttendance.setBackgroundResource(R.drawable.attend_cancel_n);
                holder.btnAttendance.setText("取消关注");
            } else {
                AttentionOrFansActivity.this.isAttendance = false;
                holder.btnAttendance.setBackgroundResource(R.drawable.attend_do_n);
                holder.btnAttendance.setText("关注");
            }
            holder.btnAttendance.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (AttentionOrFansActivity.this.isAttendance) {
                        AttentionOrFansActivity.this.showDialog(0);
                        new Thread() {
                            public void run() {
                                if (UserHttpUtils.cancelAttendance(AttentionOrFansActivity.this.weiboId, AttentionOrFansActivity.this.userId).equals(SystemConfig.SUCCESS)) {
                                    AttentionOrFansActivity.this.isAttendance = false;
                                    AttentionOrFansActivity.this.runOnUiThread(new Runnable() {
                                        public void run() {
                                            AttentionOrFansActivity.this.adapter.notifyDataSetChanged();
                                            AttentionOrFansActivity.this.removeDialog(0);
                                        }
                                    });
                                    return;
                                }
                                AttentionOrFansActivity.this.runOnUiThread(new Runnable() {
                                    public void run() {
                                        AttentionOrFansActivity.this.removeDialog(0);
                                        Toast.makeText(AttentionOrFansActivity.this, "取消关注失败", 1).show();
                                    }
                                });
                            }
                        }.start();
                        return;
                    }
                    AttentionOrFansActivity.this.showDialog(0);
                    new Thread() {
                        public void run() {
                            if (UserHttpUtils.attendance(AttentionOrFansActivity.this.weiboId, AttentionOrFansActivity.this.userId).equals(SystemConfig.SUCCESS)) {
                                AttentionOrFansActivity.this.isAttendance = true;
                                AttentionOrFansActivity.this.runOnUiThread(new Runnable() {
                                    public void run() {
                                        AttentionOrFansActivity.this.adapter.notifyDataSetChanged();
                                        AttentionOrFansActivity.this.removeDialog(0);
                                    }
                                });
                                return;
                            }
                            AttentionOrFansActivity.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    AttentionOrFansActivity.this.removeDialog(0);
                                    Toast.makeText(AttentionOrFansActivity.this, "关注失败", 1).show();
                                }
                            });
                        }
                    }.start();
                }
            });
            return view;
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("加载中...请稍候");
        dialog.setIndeterminate(true);
        dialog.setCancelable(true);
        return dialog;
    }

    public class LoadWeiBoInfo extends AsyncTask<String, String, String> {
        public LoadWeiBoInfo() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            if (AttentionOrFansActivity.this.key.equals(MyAction.FRIENDS)) {
                AttentionOrFansActivity.this.users = UserHttpUtils.getUserFriends(AttentionOrFansActivity.this.weiboId, AttentionOrFansActivity.this.userId, AttentionOrFansActivity.this.page, AttentionOrFansActivity.this.count, 0);
                return "";
            } else if (!AttentionOrFansActivity.this.key.equals(MyAction.FOLLOWERS)) {
                return "";
            } else {
                AttentionOrFansActivity.this.users = UserHttpUtils.getUserFollowers(AttentionOrFansActivity.this.weiboId, AttentionOrFansActivity.this.userId, AttentionOrFansActivity.this.page, AttentionOrFansActivity.this.count, 0);
                return "";
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            if (AttentionOrFansActivity.this.users != null && !AttentionOrFansActivity.this.users.isEmpty()) {
                AttentionOrFansActivity.this.adapter = new ListAdapter();
                AttentionOrFansActivity.this.setListAdapter(AttentionOrFansActivity.this.adapter);
            }
            AttentionOrFansActivity.this.removeDialog(0);
            super.onPostExecute((Object) result);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            AttentionOrFansActivity.this.showDialog(0);
            super.onPreExecute();
        }
    }

    public class ButtonOnClickListener implements View.OnClickListener {
        public ButtonOnClickListener() {
        }

        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.attentionorfans_back /*2131361819*/:
                    AttentionOrFansActivity.this.finish();
                    return;
                case R.id.attentionorfans_textView /*2131361820*/:
                default:
                    return;
                case R.id.attentionorfans_index /*2131361821*/:
                    Intent intent = new Intent();
                    intent.setClass(AttentionOrFansActivity.this, HomeActivity.class);
                    AttentionOrFansActivity.this.startActivity(intent);
                    AttentionOrFansActivity.this.finish();
                    return;
            }
        }
    }
}
