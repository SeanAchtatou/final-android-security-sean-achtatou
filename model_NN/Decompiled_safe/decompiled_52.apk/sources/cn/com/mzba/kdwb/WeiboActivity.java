package cn.com.mzba.kdwb;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;
import cn.com.mzba.db.ConfigHelper;
import cn.com.mzba.db.UserInfo;
import cn.com.mzba.model.Status;
import cn.com.mzba.model.StatusListAdapter;
import cn.com.mzba.service.CommentTopicHttpUtils;
import cn.com.mzba.service.MyAction;
import cn.com.mzba.service.ServiceUtils;
import cn.com.mzba.service.StatusHttpUtils;
import cn.com.mzba.service.SystemConfig;
import java.util.List;

public class WeiboActivity extends ListActivity {
    /* access modifiers changed from: private */
    public StatusListAdapter adapter;
    private Button btnBack;
    private ImageButton btnIndex;
    /* access modifiers changed from: private */
    public int count = 20;
    /* access modifiers changed from: private */
    public String firstId;
    /* access modifiers changed from: private */
    public String key;
    /* access modifiers changed from: private */
    public List<Status> list;
    /* access modifiers changed from: private */
    public int page = 1;
    /* access modifiers changed from: private */
    public ProgressDialog progress;
    /* access modifiers changed from: private */
    public String userId;
    /* access modifiers changed from: private */
    public String weiboId;
    /* access modifiers changed from: private */
    public List<Status> weiboList;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.weibolist);
        Intent intent = getIntent();
        this.userId = intent.getStringExtra(UserInfo.USERID);
        this.key = intent.getStringExtra("key");
        this.weiboId = intent.getStringExtra(UserInfo.WEIBOID);
        this.btnIndex = (ImageButton) findViewById(R.id.weibo_index);
        this.btnBack = (Button) findViewById(R.id.weibo_back);
        this.btnIndex.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(WeiboActivity.this, HomeActivity.class);
                WeiboActivity.this.startActivity(intent);
            }
        });
        this.btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                WeiboActivity.this.finish();
            }
        });
        if (ServiceUtils.isConnectInternet(this)) {
            new LoadWeiBoInfo().execute("");
            getListView().setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                /* Debug info: failed to restart local var, previous not found, register: 6 */
                public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
                    String thridWeiboUserId;
                    if (WeiboActivity.this.weiboId.equals(SystemConfig.SINA_WEIBO)) {
                        String thridWeiboUserId2 = ConfigHelper.USERINFO_SINA.getUserId();
                        if (thridWeiboUserId2 != null && WeiboActivity.this.userId.equals(thridWeiboUserId2)) {
                            if (WeiboActivity.this.key.equals(MyAction.USERSTATUS)) {
                                WeiboActivity.this.deleteWeibo(((Status) WeiboActivity.this.weiboList.get(position - 1)).getId());
                            } else if (WeiboActivity.this.key.equals(MyAction.FAVORITED)) {
                                WeiboActivity.this.cancelFav(((Status) WeiboActivity.this.weiboList.get(position - 1)).getId());
                            }
                        }
                    } else if (WeiboActivity.this.weiboId.equals(SystemConfig.TENCENT_WEIBO)) {
                        String thridWeiboUserId3 = ConfigHelper.USERINFO_TENCENT.getUserId();
                        if (thridWeiboUserId3 != null && WeiboActivity.this.userId.equals(thridWeiboUserId3)) {
                            if (WeiboActivity.this.key.equals(MyAction.USERSTATUS)) {
                                WeiboActivity.this.deleteWeibo(((Status) WeiboActivity.this.weiboList.get(position - 1)).getId());
                            } else if (WeiboActivity.this.key.equals(MyAction.FAVORITED)) {
                                WeiboActivity.this.cancelFav(((Status) WeiboActivity.this.weiboList.get(position - 1)).getId());
                            }
                        }
                    } else if (WeiboActivity.this.weiboId.equals(SystemConfig.SOHU_WEIBO)) {
                        String thridWeiboUserId4 = ConfigHelper.USERINFO_SOHU.getUserId();
                        if (thridWeiboUserId4 != null && WeiboActivity.this.userId.equals(thridWeiboUserId4)) {
                            if (WeiboActivity.this.key.equals(MyAction.USERSTATUS)) {
                                WeiboActivity.this.deleteWeibo(((Status) WeiboActivity.this.weiboList.get(position - 1)).getId());
                            } else if (WeiboActivity.this.key.equals(MyAction.FAVORITED)) {
                                WeiboActivity.this.cancelFav(((Status) WeiboActivity.this.weiboList.get(position - 1)).getId());
                            }
                        }
                    } else if (WeiboActivity.this.weiboId.equals(SystemConfig.NETEASE_WEIBO) && (thridWeiboUserId = ConfigHelper.USERINFO_NETEASE.getUserId()) != null && WeiboActivity.this.userId.equals(thridWeiboUserId)) {
                        if (WeiboActivity.this.key.equals(MyAction.USERSTATUS)) {
                            WeiboActivity.this.deleteWeibo(((Status) WeiboActivity.this.weiboList.get(position - 1)).getId());
                        } else if (WeiboActivity.this.key.equals(MyAction.FAVORITED)) {
                            WeiboActivity.this.cancelFav(((Status) WeiboActivity.this.weiboList.get(position - 1)).getId());
                        }
                    }
                    return true;
                }
            });
            return;
        }
        Toast.makeText(this, "网络出现异常", 1).show();
    }

    /* access modifiers changed from: private */
    public void deleteWeibo(String id) {
        final String result = StatusHttpUtils.deleteStatus(this.weiboId, id);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("确定要删除该微博吗？");
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                WeiboActivity.this.progress = new ProgressDialog(WeiboActivity.this);
                WeiboActivity.this.progress.setMessage("删除微博中...");
                WeiboActivity.this.progress.show();
                final String str = result;
                new Thread() {
                    public void run() {
                        if (str == null || !str.equals(SystemConfig.SUCCESS)) {
                            WeiboActivity.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    WeiboActivity.this.progress.dismiss();
                                    Toast.makeText(WeiboActivity.this, "删除微博失败", 1).show();
                                }
                            });
                        } else {
                            WeiboActivity.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    WeiboActivity.this.adapter.notifyDataSetChanged();
                                    WeiboActivity.this.progress.dismiss();
                                    Toast.makeText(WeiboActivity.this, "删除微博成功", 1).show();
                                }
                            });
                        }
                    }
                }.start();
            }
        });
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    /* access modifiers changed from: private */
    public void cancelFav(final String id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("确定要取消收藏该微博吗？");
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                new ProgressDialog(WeiboActivity.this);
                WeiboActivity.this.progress.setMessage("取消收藏微博中...");
                WeiboActivity.this.progress.show();
                final String str = id;
                new Thread() {
                    public void run() {
                        String result = CommentTopicHttpUtils.deleteFavorite(WeiboActivity.this.weiboId, str);
                        if (result == null || !result.equals(SystemConfig.SUCCESS)) {
                            WeiboActivity.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    WeiboActivity.this.progress.dismiss();
                                    Toast.makeText(WeiboActivity.this, "取消收藏微博失败", 1).show();
                                }
                            });
                        } else {
                            WeiboActivity.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    WeiboActivity.this.adapter.notifyDataSetChanged();
                                    WeiboActivity.this.progress.dismiss();
                                    Toast.makeText(WeiboActivity.this, "取消收藏微博成功", 1).show();
                                }
                            });
                        }
                    }
                }.start();
            }
        });
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        if (position == 0) {
            this.page = 1;
            showDialog(0);
            new Thread() {
                public void run() {
                    if (WeiboActivity.this.key.equals(MyAction.USERSTATUS)) {
                        WeiboActivity.this.weiboList = StatusHttpUtils.getUserTimeLines(WeiboActivity.this.weiboId, WeiboActivity.this.userId, WeiboActivity.this.page, WeiboActivity.this.count, WeiboActivity.this.firstId);
                    } else if (WeiboActivity.this.key.equals(MyAction.FAVORITED)) {
                        WeiboActivity.this.weiboList = StatusHttpUtils.getFavoritedTimeLines(WeiboActivity.this.weiboId, WeiboActivity.this.userId, WeiboActivity.this.page, WeiboActivity.this.count, WeiboActivity.this.firstId);
                    }
                    WeiboActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            WeiboActivity.this.adapter.notifyDataSetChanged();
                            WeiboActivity.this.removeDialog(0);
                        }
                    });
                }
            }.start();
        } else if (position == getListAdapter().getCount() - 1) {
            showDialog(0);
            new Thread() {
                public void run() {
                    WeiboActivity weiboActivity = WeiboActivity.this;
                    weiboActivity.page = weiboActivity.page + 1;
                    if (WeiboActivity.this.key.equals(MyAction.USERSTATUS)) {
                        WeiboActivity.this.list = StatusHttpUtils.getUserTimeLines(WeiboActivity.this.weiboId, WeiboActivity.this.userId, WeiboActivity.this.page, WeiboActivity.this.count, WeiboActivity.this.firstId);
                    } else if (WeiboActivity.this.key.equals(MyAction.FAVORITED)) {
                        WeiboActivity.this.list = StatusHttpUtils.getFavoritedTimeLines(WeiboActivity.this.weiboId, WeiboActivity.this.userId, WeiboActivity.this.page, WeiboActivity.this.count, WeiboActivity.this.firstId);
                    }
                    if (WeiboActivity.this.list == null || WeiboActivity.this.list.isEmpty()) {
                        WeiboActivity.this.removeDialog(0);
                        Toast.makeText(WeiboActivity.this, "加载数据失败", 1).show();
                        return;
                    }
                    WeiboActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            WeiboActivity.this.adapter.addMoreDatas(WeiboActivity.this.list);
                            WeiboActivity.this.removeDialog(0);
                        }
                    });
                }
            }.start();
        } else {
            String keyid = this.weiboList.get(position - 1).getId();
            Bundle data = new Bundle();
            data.putString("key", keyid);
            Intent intent = new Intent();
            intent.setClass(this, DetailActivity.class);
            intent.putExtras(data);
            startActivity(intent);
        }
    }

    public void loadMyWeiboInfo() {
        if (this.key.equals(MyAction.USERSTATUS)) {
            this.weiboList = StatusHttpUtils.getUserTimeLines(this.weiboId, this.userId, this.page, this.count, this.firstId);
        } else if (this.key.equals(MyAction.FAVORITED)) {
            this.weiboList = StatusHttpUtils.getFavoritedTimeLines(this.weiboId, this.userId, this.page, this.count, this.firstId);
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("加载中...请稍候");
        dialog.setIndeterminate(true);
        dialog.setCancelable(true);
        return dialog;
    }

    public class LoadWeiBoInfo extends AsyncTask<String, String, String> {
        public LoadWeiBoInfo() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            WeiboActivity.this.loadMyWeiboInfo();
            return "";
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            if (WeiboActivity.this.weiboList != null && !WeiboActivity.this.weiboList.isEmpty()) {
                WeiboActivity.this.firstId = ((Status) WeiboActivity.this.weiboList.get(0)).getId();
                WeiboActivity.this.adapter = new StatusListAdapter(WeiboActivity.this, WeiboActivity.this.weiboList);
                WeiboActivity.this.setListAdapter(WeiboActivity.this.adapter);
            }
            WeiboActivity.this.removeDialog(0);
            super.onPostExecute((Object) result);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            WeiboActivity.this.showDialog(0);
            super.onPreExecute();
        }
    }
}
