package cn.com.mzba.kdwb;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;
import cn.com.mzba.db.ConfigHelper;
import cn.com.mzba.db.UnReadInfo;
import cn.com.mzba.db.UserInfo;
import cn.com.mzba.model.Emotions;
import cn.com.mzba.model.Status;
import cn.com.mzba.model.StatusListAdapter;
import cn.com.mzba.service.MyAction;
import cn.com.mzba.service.ServiceUtils;
import cn.com.mzba.service.StatusHttpUtils;
import cn.com.mzba.service.SystemConfig;
import java.util.List;

public class HomeListActivity extends ListActivity implements View.OnClickListener {
    public static StatusListAdapter adapter;
    private Button btnNetease;
    private ImageButton btnNewWeibo;
    private ImageButton btnRefresh;
    private Button btnSina;
    private Button btnSohu;
    private Button btnTencent;
    /* access modifiers changed from: private */
    public int count = 20;
    /* access modifiers changed from: private */
    public String firstId;
    /* access modifiers changed from: private */
    public int lastItem;
    /* access modifiers changed from: private */
    public List<Status> moreList = null;
    /* access modifiers changed from: private */
    public String netease_url = "http://api.t.163.com/statuses/home_timeline.json";
    /* access modifiers changed from: private */
    public int page = 1;
    private NewTipBroadcast receiver;
    /* access modifiers changed from: private */
    public String sina_url = "http://api.t.sina.com.cn/statuses/friends_timeline.json";
    /* access modifiers changed from: private */
    public String sohu_url = "http://api.t.sohu.com/statuses/friends_timeline.json";
    /* access modifiers changed from: private */
    public String tencent_url = "http://open.t.qq.com/api/statuses/home_timeline";
    /* access modifiers changed from: private */
    public String weiboId = SystemConfig.SINA_WEIBO;
    /* access modifiers changed from: private */
    public List<Status> weiboList;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.homelist);
        this.btnSina = (Button) findViewById(R.id.third_sina);
        this.btnSina.setOnClickListener(this);
        this.btnTencent = (Button) findViewById(R.id.third_tencent);
        this.btnTencent.setOnClickListener(this);
        this.btnNetease = (Button) findViewById(R.id.third_netease);
        this.btnNetease.setOnClickListener(this);
        this.btnSohu = (Button) findViewById(R.id.third_sohu);
        this.btnSohu.setOnClickListener(this);
        if (SystemConfig.autoRemind && SystemConfig.checkTime != null) {
            startService(new Intent(this, AutoRemindStatusService.class));
        }
        this.receiver = new NewTipBroadcast();
        IntentFilter filter = new IntentFilter();
        filter.addAction(MyAction.AUTOREMIND);
        registerReceiver(this.receiver, filter);
        this.btnNewWeibo = (ImageButton) findViewById(R.id.newWeibo);
        this.btnRefresh = (ImageButton) findViewById(R.id.refresh);
        this.btnRefresh.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (ServiceUtils.isConnectInternet(HomeListActivity.this)) {
                    HomeListActivity.this.refresh();
                } else {
                    Toast.makeText(HomeListActivity.this, "网络出现异常", 1).show();
                }
            }
        });
        this.btnNewWeibo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(HomeListActivity.this, NewWeiboActivity.class);
                HomeListActivity.this.startActivity(intent);
            }
        });
        if (SystemConfig.autoLoadMode) {
            getListView().setOnScrollListener(new AbsListView.OnScrollListener() {
                public void onScrollStateChanged(AbsListView view, int scrollState) {
                    if (HomeListActivity.this.lastItem == HomeListActivity.adapter.getCount() - 2 && scrollState == 0) {
                        HomeListActivity.this.loadMoreData();
                    }
                }

                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                    HomeListActivity.this.lastItem = firstVisibleItem + visibleItemCount;
                }
            });
        }
        this.btnSina.setBackgroundResource(R.drawable.blog_thrid_left_d);
        if (ServiceUtils.isConnectInternet(this)) {
            new LoadWeiBoInfo().execute("");
            return;
        }
        Toast.makeText(this, "网络出现异常", 1).show();
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        if (position == 0) {
            refresh();
        } else if (position == getListAdapter().getCount() - 1) {
            loadMoreData();
        } else {
            String statusId = this.weiboList.get(position - 1).getId();
            Log.i(HomeListActivity.class.getCanonicalName(), "statusId:" + statusId);
            Intent intent = new Intent();
            intent.setClass(this, DetailActivity.class);
            intent.putExtra("id", statusId);
            intent.putExtra(UserInfo.WEIBOID, this.weiboId);
            startActivity(intent);
        }
    }

    /* access modifiers changed from: private */
    public void refresh() {
        this.page = 1;
        showDialog(0);
        new Thread() {
            public void run() {
                Log.i(HomeListActivity.class.getCanonicalName(), "firstId:" + HomeListActivity.this.firstId);
                if (HomeListActivity.this.weiboId.equals(SystemConfig.SINA_WEIBO)) {
                    HomeListActivity.this.weiboList = StatusHttpUtils.getFriendTimeLines(HomeListActivity.this.weiboId, HomeListActivity.this.sina_url, HomeListActivity.this.page, HomeListActivity.this.count, HomeListActivity.this.firstId);
                } else if (HomeListActivity.this.weiboId.equals(SystemConfig.TENCENT_WEIBO)) {
                    HomeListActivity.this.weiboList = StatusHttpUtils.getFriendTimeLines(HomeListActivity.this.weiboId, HomeListActivity.this.tencent_url, HomeListActivity.this.page, HomeListActivity.this.count, null);
                } else if (HomeListActivity.this.weiboId.equals(SystemConfig.SOHU_WEIBO)) {
                    HomeListActivity.this.weiboList = StatusHttpUtils.getFriendTimeLines(HomeListActivity.this.weiboId, HomeListActivity.this.sohu_url, HomeListActivity.this.page, HomeListActivity.this.count, HomeListActivity.this.firstId);
                } else if (HomeListActivity.this.weiboId.equals(SystemConfig.NETEASE_WEIBO)) {
                    HomeListActivity.this.weiboList = StatusHttpUtils.getFriendTimeLines(HomeListActivity.this.weiboId, HomeListActivity.this.netease_url, HomeListActivity.this.page, HomeListActivity.this.count, null);
                }
                if (HomeListActivity.this.weiboList != null && !HomeListActivity.this.weiboList.isEmpty()) {
                    HomeListActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            HomeListActivity.adapter.notifyDataSetChanged();
                            HomeListActivity.this.removeDialog(0);
                        }
                    });
                }
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public void loadMoreData() {
        showDialog(0);
        new Thread() {
            public void run() {
                HomeListActivity homeListActivity = HomeListActivity.this;
                homeListActivity.page = homeListActivity.page + 1;
                if (HomeListActivity.this.weiboId.equals(SystemConfig.SINA_WEIBO)) {
                    HomeListActivity.this.moreList = StatusHttpUtils.getFriendTimeLines(HomeListActivity.this.weiboId, HomeListActivity.this.sina_url, HomeListActivity.this.page, HomeListActivity.this.count, null);
                } else if (HomeListActivity.this.weiboId.equals(SystemConfig.TENCENT_WEIBO)) {
                    HomeListActivity.this.moreList = StatusHttpUtils.getFriendTimeLines(HomeListActivity.this.weiboId, HomeListActivity.this.tencent_url, HomeListActivity.this.page, HomeListActivity.this.count, null);
                } else if (HomeListActivity.this.weiboId.equals(SystemConfig.SOHU_WEIBO)) {
                    HomeListActivity.this.moreList = StatusHttpUtils.getFriendTimeLines(HomeListActivity.this.weiboId, HomeListActivity.this.sohu_url, HomeListActivity.this.page, HomeListActivity.this.count, null);
                } else if (HomeListActivity.this.weiboId.equals(SystemConfig.NETEASE_WEIBO)) {
                    HomeListActivity.this.moreList = StatusHttpUtils.getFriendTimeLines(HomeListActivity.this.weiboId, HomeListActivity.this.netease_url, HomeListActivity.this.page, HomeListActivity.this.count, null);
                }
                if (HomeListActivity.this.moreList == null || HomeListActivity.this.moreList.isEmpty()) {
                    HomeListActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            HomeListActivity.this.removeDialog(0);
                            Toast.makeText(HomeListActivity.this, "加载数据失败", 1).show();
                        }
                    });
                    return;
                }
                HomeListActivity.this.firstId = ((Status) HomeListActivity.this.moreList.get(0)).getId();
                HomeListActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        HomeListActivity.adapter.addMoreDatas(HomeListActivity.this.moreList);
                        HomeListActivity.this.removeDialog(0);
                    }
                });
            }
        }.start();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("加载中...请稍候");
        dialog.setIndeterminate(true);
        dialog.setCancelable(true);
        return dialog;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public class LoadWeiBoInfo extends AsyncTask<String, String, String> {
        public LoadWeiBoInfo() {
        }

        /* Debug info: failed to restart local var, previous not found, register: 8 */
        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            if (HomeListActivity.this.weiboList == null || HomeListActivity.this.weiboList.isEmpty()) {
                if (HomeListActivity.this.weiboId.equals(SystemConfig.SINA_WEIBO) && ConfigHelper.USERINFO_SINA != null) {
                    HomeListActivity.this.weiboList = StatusHttpUtils.getFriendTimeLines(HomeListActivity.this.weiboId, HomeListActivity.this.sina_url, HomeListActivity.this.page, HomeListActivity.this.count, "");
                    if (HomeListActivity.this.weiboList != null && !HomeListActivity.this.weiboList.isEmpty()) {
                        HomeListActivity.this.firstId = ((Status) HomeListActivity.this.weiboList.get(0)).getId();
                    }
                } else if (HomeListActivity.this.weiboId.equals(SystemConfig.TENCENT_WEIBO) && ConfigHelper.USERINFO_TENCENT != null) {
                    HomeListActivity.this.weiboList = StatusHttpUtils.getFriendTimeLines(HomeListActivity.this.weiboId, HomeListActivity.this.tencent_url, HomeListActivity.this.page, HomeListActivity.this.count, "");
                    if (HomeListActivity.this.weiboList != null && !HomeListActivity.this.weiboList.isEmpty()) {
                        HomeListActivity.this.firstId = ((Status) HomeListActivity.this.weiboList.get(0)).getId();
                    }
                } else if (HomeListActivity.this.weiboId.equals(SystemConfig.SOHU_WEIBO) && ConfigHelper.USERINFO_SOHU != null) {
                    HomeListActivity.this.weiboList = StatusHttpUtils.getFriendTimeLines(HomeListActivity.this.weiboId, HomeListActivity.this.sohu_url, HomeListActivity.this.page, HomeListActivity.this.count, null);
                    if (HomeListActivity.this.weiboList != null && !HomeListActivity.this.weiboList.isEmpty()) {
                        HomeListActivity.this.firstId = ((Status) HomeListActivity.this.weiboList.get(0)).getId();
                    }
                } else if (HomeListActivity.this.weiboId.equals(SystemConfig.NETEASE_WEIBO) && ConfigHelper.USERINFO_NETEASE != null) {
                    HomeListActivity.this.weiboList = StatusHttpUtils.getFriendTimeLines(HomeListActivity.this.weiboId, HomeListActivity.this.netease_url, HomeListActivity.this.page, HomeListActivity.this.count, null);
                    if (HomeListActivity.this.weiboList != null && !HomeListActivity.this.weiboList.isEmpty()) {
                        HomeListActivity.this.firstId = ((Status) HomeListActivity.this.weiboList.get(0)).getCursorId();
                    }
                }
            }
            return "";
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            if (HomeListActivity.this.weiboList != null && !HomeListActivity.this.weiboList.isEmpty()) {
                HomeListActivity.this.firstId = ((Status) HomeListActivity.this.weiboList.get(0)).getId();
                HomeListActivity.adapter = new StatusListAdapter(HomeListActivity.this, HomeListActivity.this.weiboList);
                HomeListActivity.this.setListAdapter(HomeListActivity.adapter);
            }
            HomeListActivity.this.removeDialog(0);
            super.onPostExecute((Object) result);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            HomeListActivity.this.showDialog(0);
            super.onPreExecute();
        }
    }

    public class NewTipBroadcast extends BroadcastReceiver {
        public NewTipBroadcast() {
        }

        public void onReceive(Context context, Intent intent) {
            Class<HomeActivity> cls = HomeActivity.class;
            UnReadInfo readinfo = (UnReadInfo) intent.getSerializableExtra("unread");
            if (readinfo.getComments() != 0) {
                int count = readinfo.getComments();
                Class<HomeActivity> cls2 = HomeActivity.class;
                Intent intentComments = new Intent(HomeListActivity.this, cls);
                intentComments.putExtra(Emotions.TYPE, "message");
                intentComments.putExtra("key", 1);
                intentComments.setFlags(268435456);
                HomeListActivity.this.showNotifications(intentComments, count, "条新评论");
            } else if (readinfo.getDm() != 0) {
                int count2 = readinfo.getDm();
                Class<HomeActivity> cls3 = HomeActivity.class;
                Intent intentDm = new Intent(HomeListActivity.this, cls);
                intentDm.putExtra(Emotions.TYPE, "message");
                intentDm.putExtra("key", 2);
                intentDm.setFlags(268435456);
                HomeListActivity.this.showNotifications(intentDm, count2, "条新私信");
            } else if (readinfo.getMentions() != 0) {
                int count3 = readinfo.getMentions();
                Class<HomeActivity> cls4 = HomeActivity.class;
                Intent intentMentions = new Intent(HomeListActivity.this, cls);
                intentMentions.putExtra(Emotions.TYPE, "message");
                intentMentions.putExtra("key", 0);
                intentMentions.setFlags(268435456);
                HomeListActivity.this.showNotifications(intentMentions, count3, "条提到我");
            } else if (readinfo.getFollowers() != 0) {
                int count4 = readinfo.getFollowers();
                Intent intentFollowers = new Intent(HomeListActivity.this, AttentionOrFansActivity.class);
                intentFollowers.setFlags(268435456);
                HomeListActivity.this.showNotifications(intentFollowers, count4, "个新粉丝");
            }
        }
    }

    /* access modifiers changed from: private */
    public void showNotifications(Intent intent, int count2, String type) {
        Notification notification = new Notification();
        notification.icon = R.drawable.icon;
        notification.defaults = 1;
        notification.tickerText = "未读信息通知";
        notification.flags = 16;
        notification.setLatestEventInfo(this, "", String.valueOf(count2) + type, PendingIntent.getActivity(this, 0, intent, 134217728));
        ((NotificationManager) getSystemService("notification")).notify(0, notification);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("提示");
        builder.setMessage("确定要退出程序?");
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                SystemConfig.back(HomeListActivity.this);
            }
        });
        builder.setNeutralButton("取消", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
        return true;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.third_sina /*2131361992*/:
                if (this.weiboList != null) {
                    this.weiboList.clear();
                }
                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
                this.weiboId = SystemConfig.SINA_WEIBO;
                new LoadWeiBoInfo().execute("");
                this.btnSina.setBackgroundResource(R.drawable.blog_thrid_left_d);
                this.btnTencent.setBackgroundResource(R.drawable.blog_thrid_center_n);
                this.btnSohu.setBackgroundResource(R.drawable.blog_thrid_center_n);
                this.btnNetease.setBackgroundResource(R.drawable.blog_thrid_right_n);
                return;
            case R.id.third_tencent /*2131361993*/:
                if (this.weiboList != null) {
                    this.weiboList.clear();
                }
                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
                this.weiboId = SystemConfig.TENCENT_WEIBO;
                new LoadWeiBoInfo().execute("");
                this.btnSina.setBackgroundResource(R.drawable.blog_thrid_left_n);
                this.btnTencent.setBackgroundResource(R.drawable.blog_thrid_center_d);
                this.btnSohu.setBackgroundResource(R.drawable.blog_thrid_center_n);
                this.btnNetease.setBackgroundResource(R.drawable.blog_thrid_right_n);
                return;
            case R.id.third_sohu /*2131361994*/:
                if (this.weiboList != null) {
                    this.weiboList.clear();
                }
                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
                this.weiboId = SystemConfig.SOHU_WEIBO;
                new LoadWeiBoInfo().execute("");
                this.btnSina.setBackgroundResource(R.drawable.blog_thrid_left_n);
                this.btnTencent.setBackgroundResource(R.drawable.blog_thrid_center_n);
                this.btnSohu.setBackgroundResource(R.drawable.blog_thrid_center_d);
                this.btnNetease.setBackgroundResource(R.drawable.blog_thrid_right_n);
                return;
            case R.id.third_netease /*2131361995*/:
                if (this.weiboList != null) {
                    this.weiboList.clear();
                }
                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
                this.weiboId = SystemConfig.NETEASE_WEIBO;
                new LoadWeiBoInfo().execute("");
                this.btnSina.setBackgroundResource(R.drawable.blog_thrid_left_n);
                this.btnTencent.setBackgroundResource(R.drawable.blog_thrid_center_n);
                this.btnSohu.setBackgroundResource(R.drawable.blog_thrid_center_n);
                this.btnNetease.setBackgroundResource(R.drawable.blog_thrid_right_d);
                return;
            default:
                return;
        }
    }
}
