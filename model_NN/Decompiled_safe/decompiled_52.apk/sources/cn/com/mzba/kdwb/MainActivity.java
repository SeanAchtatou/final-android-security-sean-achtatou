package cn.com.mzba.kdwb;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.LinearLayout;
import android.widget.Toast;
import cn.com.mzba.db.ConfigHelper;
import cn.com.mzba.db.DataHelper;
import cn.com.mzba.db.UserInfo;
import cn.com.mzba.model.Emotions;
import cn.com.mzba.service.ServiceUtils;
import cn.com.mzba.service.SystemConfig;
import cn.com.mzba.service.UserHttpUtils;
import java.util.List;

public class MainActivity extends Activity {
    public static List<Emotions> emotionsList;
    private DataHelper dataHelper;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.main);
        AutoScreen.AutoBackground(this, (LinearLayout) findViewById(R.id.layout), R.drawable.background, R.drawable.background2);
        this.dataHelper = new DataHelper(this);
        List<UserInfo> userinfos = this.dataHelper.getAllUserInfos();
        if (userinfos.isEmpty()) {
            new Thread() {
                public void run() {
                    try {
                        Thread.sleep(3000);
                        if (ServiceUtils.isConnectInternet(MainActivity.this)) {
                            Intent intent = new Intent();
                            intent.setClass(MainActivity.this, AccountBindActivity.class);
                            MainActivity.this.startActivity(intent);
                            MainActivity.this.finish();
                            return;
                        }
                        Toast.makeText(MainActivity.this, "网络出现异常", 1).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.start();
            return;
        }
        for (UserInfo userinfo : userinfos) {
            if (userinfo.getWeiboId() != null) {
                if (userinfo.getWeiboId().equals(SystemConfig.SINA_WEIBO)) {
                    ConfigHelper.USERINFO_SINA = this.dataHelper.getUserInfoByWeiboId(SystemConfig.SINA_WEIBO);
                } else if (userinfo.getWeiboId().equals(SystemConfig.TENCENT_WEIBO)) {
                    ConfigHelper.USERINFO_TENCENT = this.dataHelper.getUserInfoByWeiboId(SystemConfig.TENCENT_WEIBO);
                    if (ConfigHelper.USERINFO_TENCENT.getUserId() == null) {
                        new Thread() {
                            public void run() {
                                ConfigHelper.USERINFO_TENCENT.setUserId(UserHttpUtils.getUserById(SystemConfig.TENCENT_WEIBO, null).getId());
                            }
                        }.start();
                    }
                } else if (userinfo.getWeiboId().equals(SystemConfig.SOHU_WEIBO)) {
                    ConfigHelper.USERINFO_SOHU = this.dataHelper.getUserInfoByWeiboId(SystemConfig.SOHU_WEIBO);
                    if (ConfigHelper.USERINFO_SOHU.getUserId() == null) {
                        new Thread() {
                            public void run() {
                                ConfigHelper.USERINFO_SOHU.setUserId(UserHttpUtils.getUserById(SystemConfig.SOHU_WEIBO, null).getId());
                            }
                        }.start();
                    }
                } else if (userinfo.getWeiboId().equals(SystemConfig.NETEASE_WEIBO)) {
                    ConfigHelper.USERINFO_NETEASE = this.dataHelper.getUserInfoByWeiboId(SystemConfig.NETEASE_WEIBO);
                    if (ConfigHelper.USERINFO_NETEASE.getUserId() == null) {
                        new Thread() {
                            public void run() {
                                ConfigHelper.USERINFO_NETEASE.setUserId(UserHttpUtils.getUserById(SystemConfig.NETEASE_WEIBO, null).getId());
                            }
                        }.start();
                    }
                }
            }
        }
        startActivity(new Intent(this, HomeActivity.class));
        finish();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("提示");
        builder.setMessage("确定要退出程序?");
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                SystemConfig.back(MainActivity.this);
            }
        });
        builder.setNeutralButton("取消", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.dataHelper.close();
    }
}
