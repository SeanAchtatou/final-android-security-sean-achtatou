package cn.com.mzba.kdwb;

import android.app.ActivityGroup;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import cn.com.mzba.service.StatusHttpUtils;

public class MessageActivity extends ActivityGroup {
    /* access modifiers changed from: private */
    public Button btnComment;
    /* access modifiers changed from: private */
    public Button btnMe;
    /* access modifiers changed from: private */
    public Button btnPersional;
    /* access modifiers changed from: private */
    public int flag = 0;
    private LinearLayout message_body_layout;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.message);
        this.message_body_layout = (LinearLayout) findViewById(R.id.message_body_layout);
        this.btnMe = (Button) findViewById(R.id.message_me);
        this.btnComment = (Button) findViewById(R.id.message_comment);
        this.btnPersional = (Button) findViewById(R.id.message_persional);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.flag = extras.getInt("keyid");
            if (this.flag == 0) {
                StatusHttpUtils.resetCount(2);
            } else if (this.flag == 1) {
                StatusHttpUtils.resetCount(1);
            } else {
                StatusHttpUtils.resetCount(3);
            }
        }
        showView(this.flag);
        if (this.flag == 0) {
            this.btnMe.setBackgroundResource(R.drawable.title_button_group_left_selected);
            this.btnComment.setBackgroundResource(R.drawable.title_button_group_middle_normal);
            this.btnPersional.setBackgroundResource(R.drawable.title_button_group_right_normal);
        } else if (this.flag == 1) {
            this.btnMe.setBackgroundResource(R.drawable.title_button_group_left_normal);
            this.btnComment.setBackgroundResource(R.drawable.title_button_group_middle_selected);
            this.btnPersional.setBackgroundResource(R.drawable.title_button_group_right_normal);
        } else {
            this.btnMe.setBackgroundResource(R.drawable.title_button_group_left_normal);
            this.btnComment.setBackgroundResource(R.drawable.title_button_group_middle_normal);
            this.btnPersional.setBackgroundResource(R.drawable.title_button_group_right_selected);
        }
        this.btnMe.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MessageActivity.this.flag = 0;
                MessageActivity.this.showView(MessageActivity.this.flag);
                MessageActivity.this.btnMe.setBackgroundResource(R.drawable.title_button_group_left_selected);
                MessageActivity.this.btnComment.setBackgroundResource(R.drawable.title_button_group_middle_normal);
                MessageActivity.this.btnPersional.setBackgroundResource(R.drawable.title_button_group_right_normal);
            }
        });
        this.btnComment.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MessageActivity.this.flag = 1;
                MessageActivity.this.showView(MessageActivity.this.flag);
                MessageActivity.this.btnMe.setBackgroundResource(R.drawable.title_button_group_left_normal);
                MessageActivity.this.btnComment.setBackgroundResource(R.drawable.title_button_group_middle_selected);
                MessageActivity.this.btnPersional.setBackgroundResource(R.drawable.title_button_group_right_normal);
            }
        });
        this.btnPersional.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MessageActivity.this.flag = 2;
                MessageActivity.this.showView(MessageActivity.this.flag);
                MessageActivity.this.btnMe.setBackgroundResource(R.drawable.title_button_group_left_normal);
                MessageActivity.this.btnComment.setBackgroundResource(R.drawable.title_button_group_middle_normal);
                MessageActivity.this.btnPersional.setBackgroundResource(R.drawable.title_button_group_right_selected);
            }
        });
    }

    public void showView(int flag2) {
        switch (flag2) {
            case 0:
                this.message_body_layout.removeAllViews();
                this.message_body_layout.addView(getLocalActivityManager().startActivity("messagemelist", new Intent(this, MessageMeListActivity.class)).getDecorView());
                return;
            case 1:
                this.message_body_layout.removeAllViews();
                this.message_body_layout.addView(getLocalActivityManager().startActivity("messagecommentlist", new Intent(this, MessageCommentActivity.class)).getDecorView());
                return;
            case 2:
                this.message_body_layout.removeAllViews();
                this.message_body_layout.addView(getLocalActivityManager().startActivity("info", new Intent(this, MessageDirectActivity.class)).getDecorView());
                return;
            default:
                return;
        }
    }
}
