package cn.com.mzba.kdwb;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import cn.com.mzba.db.UserInfo;
import cn.com.mzba.service.CommentTopicHttpUtils;
import cn.com.mzba.service.ServiceUtils;
import cn.com.mzba.service.SystemConfig;

public class NewDirectActivity extends Activity {
    private Button btnBack;
    /* access modifiers changed from: private */
    public Button btnSend;
    private LinearLayout clearLayout;
    /* access modifiers changed from: private */
    public String sendResult;
    boolean success = false;
    /* access modifiers changed from: private */
    public EditText text;
    /* access modifiers changed from: private */
    public int textCount;
    /* access modifiers changed from: private */
    public TextView tvTextSize;
    /* access modifiers changed from: private */
    public String userId;
    /* access modifiers changed from: private */
    public String userName;
    /* access modifiers changed from: private */
    public String weiboId;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.newdirect);
        this.tvTextSize = (TextView) findViewById(R.id.direct_textSize);
        this.clearLayout = (LinearLayout) findViewById(R.id.direct_clear_layout);
        this.clearLayout.setOnClickListener(new ButtonOnClickListener());
        this.btnBack = (Button) findViewById(R.id.direct_back);
        this.btnBack.setOnClickListener(new ButtonOnClickListener());
        this.btnSend = (Button) findViewById(R.id.direct_send);
        this.btnSend.setOnClickListener(new ButtonOnClickListener());
        this.text = (EditText) findViewById(R.id.direct_content);
        this.text.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                NewDirectActivity.this.textCount = (NewDirectActivity.this.textCount + count) - before;
                if (NewDirectActivity.this.textCount <= 140) {
                    NewDirectActivity.this.btnSend.setClickable(true);
                    NewDirectActivity.this.tvTextSize.setText(new StringBuilder(String.valueOf(140 - NewDirectActivity.this.textCount)).toString());
                    NewDirectActivity.this.btnSend.setEnabled(true);
                    return;
                }
                NewDirectActivity.this.btnSend.setClickable(false);
                NewDirectActivity.this.tvTextSize.setText("0");
                NewDirectActivity.this.text.setFilters(new InputFilter[]{new InputFilter() {
                    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                        if (source.length() < 1) {
                            return dest.subSequence(dstart, dend);
                        }
                        return "";
                    }
                }});
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
            }
        });
        Intent intent = getIntent();
        this.userId = intent.getStringExtra(UserInfo.USERID);
        this.userName = intent.getStringExtra(UserInfo.USERNAME);
        this.weiboId = intent.getStringExtra(UserInfo.WEIBOID);
    }

    public class ButtonOnClickListener implements View.OnClickListener {
        public ButtonOnClickListener() {
        }

        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.direct_back /*2131361947*/:
                    NewDirectActivity.this.finish();
                    return;
                case R.id.direct_title /*2131361948*/:
                case R.id.direct_bottom_layout /*2131361950*/:
                case R.id.direct_content /*2131361951*/:
                default:
                    return;
                case R.id.direct_send /*2131361949*/:
                    if (ServiceUtils.isConnectInternet(NewDirectActivity.this)) {
                        new CommentBlog().execute("");
                        return;
                    }
                    Toast.makeText(NewDirectActivity.this, "网络出现异常", 1).show();
                    return;
                case R.id.direct_clear_layout /*2131361952*/:
                    AlertDialog.Builder builder = new AlertDialog.Builder(NewDirectActivity.this);
                    builder.setTitle("提示");
                    builder.setMessage("确定要清空所有内容吗?");
                    builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            NewDirectActivity.this.text.setText("");
                        }
                    });
                    builder.setNeutralButton("取消", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder.show();
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("发送微博中...请稍候");
        dialog.setIndeterminate(true);
        dialog.setCancelable(true);
        return dialog;
    }

    public class CommentBlog extends AsyncTask<String, String, String> {
        public CommentBlog() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            NewDirectActivity.this.sendResult = CommentTopicHttpUtils.sendDirect(NewDirectActivity.this.weiboId, NewDirectActivity.this.userId, NewDirectActivity.this.userName, NewDirectActivity.this.text.getText().toString());
            return "";
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            NewDirectActivity.this.removeDialog(0);
            if (NewDirectActivity.this.sendResult == null || !NewDirectActivity.this.sendResult.equals(SystemConfig.SUCCESS)) {
                Toast.makeText(NewDirectActivity.this, "发送微博失败", 1).show();
            } else {
                Toast.makeText(NewDirectActivity.this, "发送微博成功", 1).show();
            }
            super.onPostExecute((Object) result);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            NewDirectActivity.this.showDialog(0);
            super.onPreExecute();
        }
    }
}
