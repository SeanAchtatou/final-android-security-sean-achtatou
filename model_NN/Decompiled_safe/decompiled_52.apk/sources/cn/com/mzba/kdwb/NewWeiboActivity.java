package cn.com.mzba.kdwb;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;
import cn.com.mzba.service.ImageThumbnail;
import cn.com.mzba.service.ServiceUtils;
import cn.com.mzba.service.StatusHttpUtils;
import cn.com.mzba.service.SystemConfig;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.cookie.CookieSpec;

public class NewWeiboActivity extends Activity implements View.OnClickListener {
    private static final int CAMERA_WITH_DATA = 3023;
    private static final int PHOTO_PICKED_WITH_DATA = 3021;
    /* access modifiers changed from: private */
    public Bitmap bitMap;
    private Button btnBack;
    /* access modifiers changed from: private */
    public Button btnSend;
    private LinearLayout clearLayout;
    /* access modifiers changed from: private */
    public boolean hasImage;
    private boolean hasNetease;
    private boolean hasSina = true;
    private boolean hasSohu;
    private boolean hasTencent;
    /* access modifiers changed from: private */
    public ImageView imageView;
    private ImageButton ivCamera;
    private ImageButton ivNetease;
    private ImageButton ivSina;
    private ImageButton ivSohu;
    private ImageButton ivTencent;
    private LinearLayout neteaseLayout;
    private LinearLayout sinaLayout;
    private LinearLayout sohuLayout;
    boolean success = false;
    private LinearLayout tentcentLayout;
    public EditText text;
    /* access modifiers changed from: private */
    public int textCount;
    /* access modifiers changed from: private */
    public TextView tvTextSize;
    private List<String> weiboIds = new ArrayList();

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.newweibo);
        initLayout();
    }

    private void initLayout() {
        this.hasImage = false;
        this.tvTextSize = (TextView) findViewById(R.id.textSize);
        this.clearLayout = (LinearLayout) findViewById(R.id.clear_layout);
        this.clearLayout.setOnClickListener(this);
        this.imageView = (ImageView) findViewById(R.id.newWeibo_image);
        this.imageView.setOnClickListener(this);
        this.btnBack = (Button) findViewById(R.id.newweibo_back);
        this.btnBack.setOnClickListener(this);
        this.btnSend = (Button) findViewById(R.id.newweibo_send);
        this.btnSend.setOnClickListener(this);
        this.text = (EditText) findViewById(R.id.newWeibo_content);
        this.text.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                NewWeiboActivity.this.textCount = (NewWeiboActivity.this.textCount + count) - before;
                if (NewWeiboActivity.this.textCount <= 140) {
                    NewWeiboActivity.this.btnSend.setClickable(true);
                    NewWeiboActivity.this.tvTextSize.setText(new StringBuilder(String.valueOf(140 - NewWeiboActivity.this.textCount)).toString());
                    NewWeiboActivity.this.btnSend.setEnabled(true);
                    return;
                }
                NewWeiboActivity.this.btnSend.setClickable(false);
                NewWeiboActivity.this.tvTextSize.setText("0");
                NewWeiboActivity.this.text.setFilters(new InputFilter[]{new InputFilter() {
                    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                        if (source.length() < 1) {
                            return dest.subSequence(dstart, dend);
                        }
                        return "";
                    }
                }});
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
            }
        });
        this.sinaLayout = (LinearLayout) findViewById(R.id.newweibo_sina);
        this.sinaLayout.setOnClickListener(this);
        this.tentcentLayout = (LinearLayout) findViewById(R.id.newweibo_tencent);
        this.tentcentLayout.setOnClickListener(this);
        this.sohuLayout = (LinearLayout) findViewById(R.id.newweibo_sohu);
        this.sohuLayout.setOnClickListener(this);
        this.neteaseLayout = (LinearLayout) findViewById(R.id.newweibo_netease);
        this.neteaseLayout.setOnClickListener(this);
        this.ivSina = (ImageButton) findViewById(R.id.imagebutton_sina);
        this.ivSina.setOnClickListener(this);
        this.ivSina.setBackgroundResource(R.drawable.checkbox_selected_bg);
        this.ivTencent = (ImageButton) findViewById(R.id.imagebutton_tentcent);
        this.ivTencent.setOnClickListener(this);
        this.ivSohu = (ImageButton) findViewById(R.id.imagebutton_sohu);
        this.ivSohu.setOnClickListener(this);
        this.ivNetease = (ImageButton) findViewById(R.id.imagebutton_netease);
        this.ivNetease.setOnClickListener(this);
        this.ivCamera = (ImageButton) findViewById(R.id.newweibo_camera);
        this.ivCamera.setOnClickListener(this);
    }

    /* access modifiers changed from: private */
    public void sendWeibo() {
        String status = this.text.getText().toString();
        if (status == null) {
            Toast.makeText(this, "请输入内容！", 1).show();
            return;
        }
        String result = StatusHttpUtils.updateStatus(this.weiboIds, status);
        if (result == null || !result.equals(SystemConfig.SUCCESS)) {
            this.success = false;
            return;
        }
        this.success = true;
        finish();
    }

    /* access modifiers changed from: private */
    public void sendWeioWithImage() {
        String status = this.text.getText().toString();
        try {
            File file = new File(String.valueOf(Environment.getDataDirectory().getPath()) + "/data/" + getPackageName() + CookieSpec.PATH_DELIM + "upload.jpg");
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            this.bitMap.compress(Bitmap.CompressFormat.PNG, 85, baos);
            byte[] photoBytes = baos.toByteArray();
            if (!file.exists()) {
                file.createNewFile();
            }
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(photoBytes);
            fos.flush();
            fos.close();
            String result = StatusHttpUtils.uploadStatus(this.weiboIds, status, file);
            if (result == null || !result.equals(SystemConfig.SUCCESS)) {
                this.success = false;
                return;
            }
            this.success = true;
            finish();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("发送微博中...请稍候");
        dialog.setIndeterminate(true);
        dialog.setCancelable(true);
        return dialog;
    }

    public class SendBlog extends AsyncTask<String, String, String> {
        public SendBlog() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            if (NewWeiboActivity.this.hasImage) {
                NewWeiboActivity.this.sendWeioWithImage();
                return "";
            }
            NewWeiboActivity.this.sendWeibo();
            return "";
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            NewWeiboActivity.this.removeDialog(0);
            if (NewWeiboActivity.this.success) {
                NewWeiboActivity.this.finish();
                Toast.makeText(NewWeiboActivity.this, "发表微博成功", 1).show();
            } else {
                Toast.makeText(NewWeiboActivity.this, "发表微博失败", 1).show();
            }
            super.onPostExecute((Object) result);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            NewWeiboActivity.this.showDialog(0);
            super.onPreExecute();
        }
    }

    private void doPickPhotoAction() {
        Context dialogContext = new ContextThemeWrapper(this, 16973836);
        ListAdapter adapter = new ArrayAdapter(dialogContext, 17367043, new String[]{getString(R.string.take_photo), getString(R.string.pick_photo)});
        AlertDialog.Builder builder = new AlertDialog.Builder(dialogContext);
        builder.setTitle((int) R.string.choice_photo);
        builder.setSingleChoiceItems(adapter, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                switch (which) {
                    case 0:
                        if (Environment.getExternalStorageState().equals("mounted")) {
                            NewWeiboActivity.this.doTakePhoto();
                            return;
                        }
                        return;
                    case 1:
                        NewWeiboActivity.this.doSelectImageFromLoacal();
                        return;
                    default:
                        return;
                }
            }
        });
        builder.setNegativeButton("返回", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    /* access modifiers changed from: protected */
    public void doTakePhoto() {
        try {
            startActivityForResult(new Intent("android.media.action.IMAGE_CAPTURE"), CAMERA_WITH_DATA);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void doSelectImageFromLoacal() {
        Intent localIntent = new Intent();
        localIntent.setType("image/*");
        localIntent.setAction("android.intent.action.GET_CONTENT");
        startActivityForResult(Intent.createChooser(localIntent, "选择图片"), PHOTO_PICKED_WITH_DATA);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1) {
            switch (requestCode) {
                case PHOTO_PICKED_WITH_DATA /*3021*/:
                    if (this.bitMap != null && !this.bitMap.isRecycled()) {
                        this.bitMap.recycle();
                    }
                    Uri selectedImageUri = data.getData();
                    if (selectedImageUri != null) {
                        try {
                            this.bitMap = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImageUri));
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        int scale = ImageThumbnail.reckonThumbnail(this.bitMap.getWidth(), this.bitMap.getHeight(), HttpStatus.SC_INTERNAL_SERVER_ERROR, 600);
                        this.bitMap = ImageThumbnail.PicZoom(this.bitMap, this.bitMap.getWidth() / scale, this.bitMap.getHeight() / scale);
                        this.imageView.setImageBitmap(this.bitMap);
                        this.imageView.setVisibility(0);
                        this.hasImage = true;
                        return;
                    }
                    return;
                case 3022:
                default:
                    return;
                case CAMERA_WITH_DATA /*3023*/:
                    this.bitMap = (Bitmap) data.getExtras().get("data");
                    if (this.bitMap != null) {
                        this.bitMap.recycle();
                    }
                    this.bitMap = (Bitmap) data.getExtras().get("data");
                    int scale2 = ImageThumbnail.reckonThumbnail(this.bitMap.getWidth(), this.bitMap.getHeight(), HttpStatus.SC_INTERNAL_SERVER_ERROR, 600);
                    this.bitMap = ImageThumbnail.PicZoom(this.bitMap, this.bitMap.getWidth() / scale2, this.bitMap.getHeight() / scale2);
                    this.imageView.setImageBitmap(this.bitMap);
                    this.imageView.setVisibility(0);
                    this.hasImage = true;
                    return;
            }
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        Cursor cursor = managedQuery(contentUri, new String[]{"_data"}, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow("_data");
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void clickImageViewListener() {
        AlertDialog.Builder bulider = new AlertDialog.Builder(this);
        bulider.setTitle("确定要删除该照片吗？");
        bulider.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (NewWeiboActivity.this.bitMap != null) {
                    NewWeiboActivity.this.bitMap.recycle();
                }
                NewWeiboActivity.this.bitMap = null;
                NewWeiboActivity.this.imageView.setVisibility(8);
                NewWeiboActivity.this.hasImage = false;
            }
        });
        bulider.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        bulider.show();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.bitMap != null && !this.bitMap.isRecycled()) {
            this.bitMap.recycle();
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.newweibo_back:
                finish();
                return;
            case R.id.newweibo_send:
                if (this.hasSina) {
                    this.weiboIds.add(SystemConfig.SINA_WEIBO);
                }
                if (this.hasTencent) {
                    this.weiboIds.add(SystemConfig.TENCENT_WEIBO);
                }
                if (this.hasSohu) {
                    this.weiboIds.add(SystemConfig.SOHU_WEIBO);
                }
                if (this.hasNetease) {
                    this.weiboIds.add(SystemConfig.NETEASE_WEIBO);
                }
                if (this.weiboIds.isEmpty()) {
                    Toast.makeText(this, "请选择要发送的第三方微博！", 1).show();
                    return;
                } else if (ServiceUtils.isConnectInternet(this)) {
                    new SendBlog().execute("");
                    return;
                } else {
                    Toast.makeText(this, "网络出现异常", 1).show();
                    return;
                }
            case R.id.newWeibo_content_layout:
            case R.id.thrid_newweibo_layout:
            case R.id.newWeibo_content:
            case R.id.textSize:
            default:
                return;
            case R.id.newWeibo_image:
                clickImageViewListener();
                return;
            case R.id.newweibo_camera:
                doPickPhotoAction();
                return;
            case R.id.clear_layout:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("提示");
                builder.setMessage("确定要清空所有内容吗?");
                builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        NewWeiboActivity.this.text.setText("");
                    }
                });
                builder.setNeutralButton("取消", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.show();
                return;
            case R.id.newweibo_sina:
                if (this.hasSina) {
                    this.hasSina = false;
                    this.ivSina.setBackgroundResource(R.drawable.checkbox_bg);
                    return;
                }
                this.hasSina = true;
                this.ivSina.setBackgroundResource(R.drawable.checkbox_selected_bg);
                return;
            case R.id.imagebutton_sina:
                if (this.hasSina) {
                    this.hasSina = false;
                    this.ivSina.setBackgroundResource(R.drawable.checkbox_bg);
                    return;
                }
                this.hasSina = true;
                this.ivSina.setBackgroundResource(R.drawable.checkbox_selected_bg);
                return;
            case R.id.newweibo_tencent:
                if (this.hasTencent) {
                    this.hasTencent = false;
                    this.ivTencent.setBackgroundResource(R.drawable.checkbox_bg);
                    return;
                }
                this.hasTencent = true;
                this.ivTencent.setBackgroundResource(R.drawable.checkbox_selected_bg);
                return;
            case R.id.imagebutton_tentcent:
                if (this.hasTencent) {
                    this.hasTencent = false;
                    this.ivTencent.setBackgroundResource(R.drawable.checkbox_bg);
                    return;
                }
                this.hasTencent = true;
                this.ivTencent.setBackgroundResource(R.drawable.checkbox_selected_bg);
                return;
            case R.id.newweibo_sohu:
                if (this.hasSohu) {
                    this.hasSohu = false;
                    this.ivSohu.setBackgroundResource(R.drawable.checkbox_bg);
                    return;
                }
                this.hasSohu = true;
                this.ivSohu.setBackgroundResource(R.drawable.checkbox_selected_bg);
                return;
            case R.id.imagebutton_sohu:
                if (this.hasSohu) {
                    this.hasSohu = false;
                    this.ivSohu.setBackgroundResource(R.drawable.checkbox_bg);
                    return;
                }
                this.hasSohu = true;
                this.ivSohu.setBackgroundResource(R.drawable.checkbox_selected_bg);
                return;
            case R.id.newweibo_netease:
                if (this.hasNetease) {
                    this.hasNetease = false;
                    this.ivNetease.setBackgroundResource(R.drawable.checkbox_bg);
                    return;
                }
                this.hasNetease = true;
                this.ivNetease.setBackgroundResource(R.drawable.checkbox_selected_bg);
                return;
            case R.id.imagebutton_netease:
                if (this.hasNetease) {
                    this.hasNetease = false;
                    this.ivNetease.setBackgroundResource(R.drawable.checkbox_bg);
                    return;
                }
                this.hasNetease = true;
                this.ivNetease.setBackgroundResource(R.drawable.checkbox_selected_bg);
                return;
        }
    }
}
