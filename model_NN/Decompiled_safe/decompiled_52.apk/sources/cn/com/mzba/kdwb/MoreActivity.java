package cn.com.mzba.kdwb;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

public class MoreActivity extends Activity implements View.OnClickListener {
    private RelativeLayout aboutLayout;
    private RelativeLayout accountLayout;
    private RelativeLayout feedbackLayout;
    private RelativeLayout settingLayout;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.more);
        initWidget();
    }

    private void initWidget() {
        this.settingLayout = (RelativeLayout) findViewById(R.id.blog_moreitem_setting);
        this.settingLayout.setOnClickListener(this);
        this.accountLayout = (RelativeLayout) findViewById(R.id.blog_moreitem_account);
        this.accountLayout.setOnClickListener(this);
        this.feedbackLayout = (RelativeLayout) findViewById(R.id.blog_moreitem_feedback);
        this.feedbackLayout.setOnClickListener(this);
        this.aboutLayout = (RelativeLayout) findViewById(R.id.blog_moreitem_about);
        this.aboutLayout.setOnClickListener(this);
    }

    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case R.id.blog_moreitem_setting /*2131361941*/:
                intent = new Intent();
                intent.setClass(this, SettingActivity.class);
                break;
            case R.id.blog_moreitem_account /*2131361942*/:
                intent = new Intent();
                intent.setClass(this, AccountBindActivity.class);
                break;
            case R.id.blog_moreitem_feedback /*2131361943*/:
                intent = new Intent();
                intent.setClass(this, SendMessageToMe.class);
                break;
            case R.id.blog_moreitem_about /*2131361944*/:
                intent = new Intent();
                intent.setClass(this, AboutActivity.class);
                break;
        }
        if (intent != null) {
            startActivity(intent);
        }
    }
}
