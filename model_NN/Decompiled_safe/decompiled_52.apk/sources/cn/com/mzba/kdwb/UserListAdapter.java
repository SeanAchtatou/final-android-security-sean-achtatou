package cn.com.mzba.kdwb;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import cn.com.mzba.db.UserInfo;
import java.util.List;

public class UserListAdapter extends BaseAdapter {
    private Activity activity;
    private List<UserInfo> userList;

    public UserListAdapter(Activity activity2, List<UserInfo> userList2) {
        this.activity = activity2;
        this.userList = userList2;
    }

    public int getCount() {
        return this.userList.size();
    }

    public Object getItem(int position) {
        return this.userList.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout layout = new LinearLayout(this.activity);
        layout.setOrientation(0);
        ImageView iconView = new ImageView(this.activity);
        iconView.setPadding(5, 5, 0, 5);
        iconView.setImageDrawable(this.userList.get(position).getUserIcon());
        layout.addView(iconView);
        TextView userText = new TextView(this.activity);
        userText.setPadding(5, 5, 5, 5);
        userText.setText(this.userList.get(position).getUserName());
        layout.addView(userText);
        return layout;
    }
}
