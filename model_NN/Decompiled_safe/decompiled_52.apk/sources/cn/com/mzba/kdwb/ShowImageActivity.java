package cn.com.mzba.kdwb;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.ZoomControls;
import cn.com.mzba.service.ServiceUtils;

public class ShowImageActivity extends Activity {
    /* access modifiers changed from: private */
    public Bitmap bmp;
    private Button btnBack;
    private Button btnSave;
    /* access modifiers changed from: private */
    public Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            ShowImageActivity.this.progressBar.setVisibility(8);
            ShowImageActivity.this.mZoomView.setImage(ShowImageActivity.this.bmp);
            ShowImageActivity.this.mZoomState = new ZoomState();
            ShowImageActivity.this.mZoomView.setZoomState(ShowImageActivity.this.mZoomState);
            ShowImageActivity.this.mZoomListener = new SimpleZoomListener();
            ShowImageActivity.this.mZoomListener.setZoomState(ShowImageActivity.this.mZoomState);
            ShowImageActivity.this.mZoomView.setOnTouchListener(ShowImageActivity.this.mZoomListener);
            ShowImageActivity.this.resetZoomState();
        }
    };
    /* access modifiers changed from: private */
    public SimpleZoomListener mZoomListener;
    /* access modifiers changed from: private */
    public ZoomState mZoomState;
    /* access modifiers changed from: private */
    public ImageZoomView mZoomView;
    /* access modifiers changed from: private */
    public String pic;
    /* access modifiers changed from: private */
    public ProgressBar progressBar;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.showimageview);
        this.progressBar = (ProgressBar) findViewById(R.id.progress_large);
        this.progressBar.setVisibility(0);
        this.mZoomView = (ImageZoomView) findViewById(R.id.zoomView);
        Intent intent = getIntent();
        if (intent != null) {
            this.pic = intent.getStringExtra("pic");
            final ProgressDialog dialog = new ProgressDialog(this);
            dialog.setMessage("加载图片中...");
            dialog.show();
            new Thread() {
                public void run() {
                    ShowImageActivity.this.bmp = ServiceUtils.downImage(ShowImageActivity.this.pic);
                    ShowImageActivity showImageActivity = ShowImageActivity.this;
                    final ProgressDialog progressDialog = dialog;
                    showImageActivity.runOnUiThread(new Runnable() {
                        public void run() {
                            ShowImageActivity.this.handler.sendEmptyMessage(0);
                            progressDialog.dismiss();
                        }
                    });
                }
            }.start();
        }
        this.btnBack = (Button) findViewById(R.id.showImage_back);
        this.btnSave = (Button) findViewById(R.id.showImage_save);
        this.btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShowImageActivity.this.finish();
            }
        });
        this.btnSave.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                new SaveImage().execute("");
            }
        });
        ZoomControls zoomCtrl = (ZoomControls) findViewById(R.id.zoomCtrl);
        zoomCtrl.setOnZoomInClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShowImageActivity.this.mZoomState.setZoom(ShowImageActivity.this.mZoomState.getZoom() + 0.25f);
                ShowImageActivity.this.mZoomState.notifyObservers();
            }
        });
        zoomCtrl.setOnZoomOutClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShowImageActivity.this.mZoomState.setZoom(ShowImageActivity.this.mZoomState.getZoom() - 0.25f);
                ShowImageActivity.this.mZoomState.notifyObservers();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.bmp != null) {
            this.bmp.recycle();
        }
    }

    /* access modifiers changed from: private */
    public void resetZoomState() {
        this.mZoomState.setPanX(0.5f);
        this.mZoomState.setPanY(0.5f);
        this.mZoomState.setZoom(1.0f);
        this.mZoomState.notifyObservers();
    }

    public void downLoadImage(String imagePath) {
        ServiceUtils.downLoadImage(imagePath);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("保存中...请稍候");
        dialog.setIndeterminate(true);
        dialog.setCancelable(true);
        return dialog;
    }

    public class SaveImage extends AsyncTask<String, String, String> {
        public SaveImage() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            ShowImageActivity.this.downLoadImage(ShowImageActivity.this.pic);
            return "";
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            ShowImageActivity.this.removeDialog(0);
            super.onPostExecute((Object) result);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            ShowImageActivity.this.showDialog(0);
            super.onPreExecute();
        }
    }
}
