package cn.com.mzba.kdwb;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import cn.com.mzba.db.UnReadInfo;
import cn.com.mzba.service.MyAction;
import cn.com.mzba.service.StatusHttpUtils;
import cn.com.mzba.service.SystemConfig;

public class AutoRemindStatusService extends Service {
    private int checkTime = Integer.parseInt(SystemConfig.checkTime);
    /* access modifiers changed from: private */
    public long sleepTime;

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        if (this.checkTime == 0) {
            this.sleepTime = 12000;
        } else if (this.checkTime == 1) {
            this.sleepTime = 120000;
        } else if (this.checkTime == 2) {
            this.sleepTime = 300000;
        }
        getUnreadStatus();
    }

    public void onCreate() {
        super.onCreate();
    }

    private void getUnreadStatus() {
        new Thread() {
            public void run() {
                while (true) {
                    try {
                        Log.i(AutoRemindStatusService.class.getCanonicalName(), "start service");
                        UnReadInfo unReadInfo = StatusHttpUtils.getUnReadInfo();
                        Intent intent = new Intent();
                        intent.setAction(MyAction.AUTOREMIND);
                        intent.putExtra("unread", unReadInfo);
                        AutoRemindStatusService.this.sendBroadcast(intent);
                        Thread.sleep(AutoRemindStatusService.this.sleepTime);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        return;
                    }
                }
            }
        }.start();
    }
}
