package cn.com.mzba.kdwb;

import java.util.Observable;

public class ZoomState extends Observable {
    private float mPanX;
    private float mPanY;
    private float mZoom;

    public float getPanX() {
        return this.mPanX;
    }

    public float getPanY() {
        return this.mPanY;
    }

    public float getZoom() {
        return this.mZoom;
    }

    public void setPanX(float panX) {
        if (panX != this.mPanX) {
            this.mPanX = panX;
            setChanged();
        }
    }

    public void setPanY(float panY) {
        if (panY != this.mPanY) {
            this.mPanY = panY;
            setChanged();
        }
    }

    public void setZoom(float zoom) {
        if (zoom != this.mZoom) {
            this.mZoom = zoom;
            setChanged();
        }
    }

    public float getZoomX(float aspectQuotient) {
        return Math.min(this.mZoom, this.mZoom * aspectQuotient);
    }

    public float getZoomY(float aspectQuotient) {
        return Math.min(this.mZoom, this.mZoom / aspectQuotient);
    }
}
