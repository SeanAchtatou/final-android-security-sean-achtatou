package cn.com.mzba.kdwb;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class AboutActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.about);
        ((Button) findViewById(R.id.about_back)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AboutActivity.this.finish();
            }
        });
    }
}
