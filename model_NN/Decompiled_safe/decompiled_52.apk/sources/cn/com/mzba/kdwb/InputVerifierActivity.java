package cn.com.mzba.kdwb;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import cn.com.mzba.db.UserInfo;
import cn.com.mzba.oauth.OAuth;
import cn.com.mzba.service.SystemConfig;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import oauth.signpost.exception.OAuthNotAuthorizedException;

public class InputVerifierActivity extends Activity {
    private Button btnOk;
    /* access modifiers changed from: private */
    public EditText text;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.inputverifier);
        this.text = (EditText) findViewById(R.id.verifier_text);
        this.btnOk = (Button) findViewById(R.id.inputverifier_ok);
        this.btnOk.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String verifier = InputVerifierActivity.this.text.getText().toString().trim();
                if (verifier == null || verifier.equals("")) {
                    Toast.makeText(InputVerifierActivity.this, "请输入Verifier,进行授权！", 1).show();
                    return;
                }
                OAuth.provider.setOAuth10a(true);
                try {
                    OAuth.provider.retrieveAccessToken(OAuth.consumer, verifier);
                } catch (OAuthMessageSignerException e) {
                    e.printStackTrace();
                } catch (OAuthNotAuthorizedException e2) {
                    e2.printStackTrace();
                } catch (OAuthExpectationFailedException e3) {
                    e3.printStackTrace();
                } catch (OAuthCommunicationException e4) {
                    e4.printStackTrace();
                }
                String userKey = OAuth.consumer.getToken();
                String userSecret = OAuth.consumer.getTokenSecret();
                Log.i(InputVerifierActivity.class.getCanonicalName(), "key:" + userKey + ",secret:" + userSecret);
                UserInfo userinfo = new UserInfo();
                userinfo.setToken(userKey);
                userinfo.setTokenSecret(userSecret);
                userinfo.setWeiboId(SystemConfig.NETEASE_WEIBO);
                Intent intentAuthorize = new Intent();
                intentAuthorize.setClass(InputVerifierActivity.this, AuthorizeActivity.class);
                intentAuthorize.putExtra("userinfo", userinfo);
                InputVerifierActivity.this.startActivity(intentAuthorize);
                InputVerifierActivity.this.finish();
            }
        });
    }
}
