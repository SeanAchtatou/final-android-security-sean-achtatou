package cn.com.mzba.kdwb;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class SimpleZoomListener implements View.OnTouchListener {
    private float mGap;
    private ZoomState mState;
    private float mX;
    private float mY;

    public enum ControlType {
        PAN,
        ZOOM
    }

    public void setZoomState(ZoomState state) {
        this.mState = state;
    }

    public boolean onTouch(View v, MotionEvent event) {
        int action = event.getAction();
        int pointCount = event.getPointerCount();
        if (pointCount == 1) {
            float x = event.getX();
            float y = event.getY();
            switch (action) {
                case 0:
                    this.mX = x;
                    this.mY = y;
                    break;
                case 2:
                    float dx = (x - this.mX) / ((float) v.getWidth());
                    float dy = (y - this.mY) / ((float) v.getHeight());
                    this.mState.setPanX(this.mState.getPanX() - dx);
                    this.mState.setPanY(this.mState.getPanY() - dy);
                    this.mState.notifyObservers();
                    this.mX = x;
                    this.mY = y;
                    break;
            }
        }
        if (pointCount != 2) {
            return true;
        }
        float x0 = event.getX(event.getPointerId(0));
        float y0 = event.getY(event.getPointerId(0));
        float x1 = event.getX(event.getPointerId(1));
        float y1 = event.getY(event.getPointerId(1));
        float gap = getGap(x0, x1, y0, y1);
        switch (action) {
            case 2:
                float dgap = (gap - this.mGap) / this.mGap;
                Log.d("Gap", String.valueOf((float) Math.pow(20.0d, (double) dgap)));
                this.mState.setZoom(this.mState.getZoom() * ((float) Math.pow(5.0d, (double) dgap)));
                this.mState.notifyObservers();
                this.mGap = gap;
                return true;
            case 5:
            case 261:
                this.mGap = gap;
                return true;
            case 6:
                this.mX = x1;
                this.mY = y1;
                return true;
            case 262:
                this.mX = x0;
                this.mY = y0;
                return true;
            default:
                return true;
        }
    }

    private float getGap(float x0, float x1, float y0, float y1) {
        return (float) Math.pow(Math.pow((double) (x0 - x1), 2.0d) + Math.pow((double) (y0 - y1), 2.0d), 0.5d);
    }
}
