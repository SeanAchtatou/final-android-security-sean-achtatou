package cn.com.mzba.kdwb;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import cn.com.mzba.db.UserInfo;
import cn.com.mzba.model.User;
import cn.com.mzba.service.AsyncImageLoader;
import cn.com.mzba.service.ImageCallback;
import cn.com.mzba.service.ServiceUtils;
import cn.com.mzba.service.SystemConfig;
import cn.com.mzba.service.UserHttpUtils;

public class UserInfoActivity extends Activity {
    private TextView address;
    private AsyncImageLoader asyncImageLoader;
    private Button attendtion;
    private Button btnDirect;
    private Button btnNewWeibo;
    private ImageButton btnRefresh;
    private Button btnRefresh2;
    private Button btnTell;
    private Button fans;
    private TextView message;
    private Button topic;
    private User user;
    private ImageView userIcon;
    /* access modifiers changed from: private */
    public String userId;
    private TextView userName;
    private ImageView userSex;
    private Button weibo;
    private String weiboId;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.userinfo);
        AutoScreen.AutoBackground(this, (RelativeLayout) findViewById(R.id.userinfo_layout), R.drawable.background, R.drawable.background2);
        this.btnNewWeibo = (Button) findViewById(R.id.userinfo_newWeibo);
        this.btnRefresh = (ImageButton) findViewById(R.id.userinfo_refresh);
        this.btnRefresh2 = (Button) findViewById(R.id.userinfo_refresh2);
        this.btnNewWeibo.setOnClickListener(new ButtonOnClickListener());
        this.btnTell = (Button) findViewById(R.id.tellher);
        this.btnDirect = (Button) findViewById(R.id.userinfo_direct);
        this.btnDirect.setOnClickListener(new ButtonOnClickListener());
        this.btnRefresh.setOnClickListener(new ButtonOnClickListener());
        this.btnRefresh2.setOnClickListener(new ButtonOnClickListener());
        this.btnTell.setOnClickListener(new ButtonOnClickListener());
        this.userIcon = (ImageView) findViewById(R.id.userinfo_userIcon);
        this.userSex = (ImageView) findViewById(R.id.userinfo_sex);
        this.userName = (TextView) findViewById(R.id.userinfo_userName);
        this.address = (TextView) findViewById(R.id.userinfo_address);
        this.message = (TextView) findViewById(R.id.userinfo_userMessage);
        this.attendtion = (Button) findViewById(R.id.userinfo_attention);
        this.attendtion.setOnClickListener(new ButtonOnClickListener());
        this.weibo = (Button) findViewById(R.id.userinfo_weibo);
        this.weibo.setOnClickListener(new ButtonOnClickListener());
        this.fans = (Button) findViewById(R.id.userinfo_fans);
        this.fans.setOnClickListener(new ButtonOnClickListener());
        this.topic = (Button) findViewById(R.id.userinfo_topic);
        this.topic.setOnClickListener(new ButtonOnClickListener());
        this.asyncImageLoader = new AsyncImageLoader();
        Intent intent = getIntent();
        this.userId = intent.getStringExtra(UserInfo.USERID);
        this.weiboId = intent.getStringExtra(UserInfo.WEIBOID);
        if (ServiceUtils.isConnectInternet(this)) {
            new LoadInfo().execute("");
            return;
        }
        Toast.makeText(this, "网络出现异常", 1).show();
    }

    public void loadInfo() {
        this.user = UserHttpUtils.getUserById(this.weiboId, this.userId);
    }

    public void initData() {
        Drawable drawable = this.asyncImageLoader.loadDrawable(this.user.getProfileImageUrl(), this.userIcon, new ImageCallback() {
            public void imageLoaded(Drawable imageDrawable, ImageView imageView, String imageUrl) {
                imageView.setImageDrawable(imageDrawable);
            }
        });
        if (drawable != null) {
            this.userIcon.setImageDrawable(drawable);
        }
        this.userName.setText(this.user.getScreenName());
        if (this.weiboId.equals(SystemConfig.SINA_WEIBO)) {
            String sex = this.user.getGender();
            if (sex.equals("m")) {
                this.userSex.setImageResource(R.drawable.icon_male);
                this.btnTell.setText("@他");
            } else if (sex.equals("f")) {
                this.userSex.setImageResource(R.drawable.icon_female);
                this.btnTell.setText("@她");
            } else {
                this.userSex.setImageResource(0);
            }
        } else if (this.weiboId.equals(SystemConfig.NETEASE_WEIBO)) {
            String sex2 = this.user.getGender();
            if (sex2.equals("1")) {
                this.userSex.setImageResource(R.drawable.icon_male);
                this.btnTell.setText("@他");
            } else if (sex2.equals("2")) {
                this.userSex.setImageResource(R.drawable.icon_female);
                this.btnTell.setText("@她");
            } else {
                this.userSex.setImageResource(0);
            }
        }
        this.address.setText(this.user.getLocation());
        this.message.setText(this.user.getDescription());
        this.attendtion.setText("关注(" + this.user.getFriendsCount() + ")");
        this.weibo.setText("微博(" + this.user.getStatusesCount() + ")");
        this.fans.setText("粉丝(" + this.user.getFollowersCount() + ")");
        this.topic.setText("话题(0)");
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("加载中...请稍候");
        dialog.setIndeterminate(true);
        dialog.setCancelable(true);
        return dialog;
    }

    public class LoadInfo extends AsyncTask<String, String, String> {
        public LoadInfo() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            UserInfoActivity.this.loadInfo();
            return "";
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            UserInfoActivity.this.removeDialog(0);
            UserInfoActivity.this.initData();
            super.onPostExecute((Object) result);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            UserInfoActivity.this.showDialog(0);
            super.onPreExecute();
        }
    }

    public class ButtonOnClickListener implements View.OnClickListener {
        public ButtonOnClickListener() {
        }

        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.userinfo_newWeibo:
                    UserInfoActivity.this.finish();
                    return;
                case R.id.userinfo_refresh:
                    Intent intent5 = new Intent();
                    intent5.setClass(UserInfoActivity.this, HomeActivity.class);
                    UserInfoActivity.this.startActivity(intent5);
                    UserInfoActivity.this.finish();
                    return;
                case R.id.userinfo_user_layout:
                case R.id.userinfo_userIcon:
                case R.id.userinfo_userName:
                case R.id.userinfo_sex:
                case R.id.userinfo_address:
                case R.id.userinfo_userMessage:
                case R.id.userinfo_bottom_layout:
                default:
                    return;
                case R.id.userinfo_attention:
                    Intent attentionIntent = new Intent();
                    attentionIntent.setClass(UserInfoActivity.this, AttentionOrFansActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("key", "attention");
                    bundle.putString("user_id", UserInfoActivity.this.userId);
                    attentionIntent.putExtras(bundle);
                    UserInfoActivity.this.startActivity(attentionIntent);
                    return;
                case R.id.userinfo_weibo:
                    Intent intent = new Intent();
                    Bundle bundle3 = new Bundle();
                    bundle3.putString("key", "weibo");
                    bundle3.putString("user_id", UserInfoActivity.this.userId);
                    intent.putExtras(bundle3);
                    intent.setClass(UserInfoActivity.this, WeiboActivity.class);
                    UserInfoActivity.this.startActivity(intent);
                    return;
                case R.id.userinfo_fans:
                    Intent fansIntent = new Intent();
                    fansIntent.setClass(UserInfoActivity.this, AttentionOrFansActivity.class);
                    Bundle bundle2 = new Bundle();
                    bundle2.putString("key", "fans");
                    bundle2.putString("user_id", UserInfoActivity.this.userId);
                    fansIntent.putExtras(bundle2);
                    UserInfoActivity.this.startActivity(fansIntent);
                    return;
                case R.id.userinfo_topic:
                    Intent intent4 = new Intent();
                    Bundle bundle4 = new Bundle();
                    bundle4.putString("user_id", UserInfoActivity.this.userId);
                    intent4.putExtras(bundle4);
                    intent4.setClass(UserInfoActivity.this, TopicActivity.class);
                    UserInfoActivity.this.startActivity(intent4);
                    return;
                case R.id.userinfo_refresh2:
                    if (ServiceUtils.isConnectInternet(UserInfoActivity.this)) {
                        new LoadInfo().execute("");
                        return;
                    }
                    Toast.makeText(UserInfoActivity.this, "网络出现异常", 1).show();
                    return;
                case R.id.tellher:
                    Intent intent6 = new Intent();
                    intent6.setClass(UserInfoActivity.this, NewWeiboActivity.class);
                    UserInfoActivity.this.startActivity(intent6);
                    UserInfoActivity.this.finish();
                    return;
                case R.id.userinfo_direct:
                    Intent intent7 = new Intent();
                    intent7.setClass(UserInfoActivity.this, NewDirectActivity.class);
                    Bundle extras = new Bundle();
                    extras.putString("user_id", UserInfoActivity.this.userId);
                    intent7.putExtras(extras);
                    UserInfoActivity.this.startActivity(intent7);
                    UserInfoActivity.this.finish();
                    return;
            }
        }
    }
}
