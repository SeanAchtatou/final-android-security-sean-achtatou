package cn.com.mzba.kdwb;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import cn.com.mzba.db.UserInfo;
import cn.com.mzba.service.CommentTopicHttpUtils;
import cn.com.mzba.service.ServiceUtils;
import cn.com.mzba.service.SystemConfig;

public class SendMessageToMe extends Activity {
    private Button btnBack;
    /* access modifiers changed from: private */
    public Button btnSend;
    private LinearLayout clearLayout;
    /* access modifiers changed from: private */
    public String sendResult;
    boolean success = false;
    /* access modifiers changed from: private */
    public EditText text;
    /* access modifiers changed from: private */
    public int textCount;
    /* access modifiers changed from: private */
    public TextView tvTextSize;
    /* access modifiers changed from: private */
    public String userId;
    /* access modifiers changed from: private */
    public String userName;
    /* access modifiers changed from: private */
    public String weiboId;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.newdirect);
        this.tvTextSize = (TextView) findViewById(R.id.direct_textSize);
        this.clearLayout = (LinearLayout) findViewById(R.id.direct_clear_layout);
        this.clearLayout.setOnClickListener(new ButtonOnClickListener());
        this.btnBack = (Button) findViewById(R.id.direct_back);
        this.btnBack.setOnClickListener(new ButtonOnClickListener());
        this.btnSend = (Button) findViewById(R.id.direct_send);
        this.btnSend.setOnClickListener(new ButtonOnClickListener());
        this.text = (EditText) findViewById(R.id.direct_content);
        this.text.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                SendMessageToMe.this.textCount = (SendMessageToMe.this.textCount + count) - before;
                if (SendMessageToMe.this.textCount <= 140) {
                    SendMessageToMe.this.btnSend.setClickable(true);
                    SendMessageToMe.this.tvTextSize.setText(new StringBuilder(String.valueOf(140 - SendMessageToMe.this.textCount)).toString());
                    SendMessageToMe.this.btnSend.setEnabled(true);
                    return;
                }
                SendMessageToMe.this.btnSend.setClickable(false);
                SendMessageToMe.this.tvTextSize.setText("0");
                SendMessageToMe.this.text.setFilters(new InputFilter[]{new InputFilter() {
                    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                        if (source.length() < 1) {
                            return dest.subSequence(dstart, dend);
                        }
                        return "";
                    }
                }});
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
            }
        });
        Intent intent = getIntent();
        this.userId = intent.getStringExtra(UserInfo.USERID);
        this.userName = intent.getStringExtra(UserInfo.USERNAME);
        this.weiboId = intent.getStringExtra(UserInfo.WEIBOID);
    }

    public class ButtonOnClickListener implements View.OnClickListener {
        public ButtonOnClickListener() {
        }

        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.direct_back:
                    SendMessageToMe.this.finish();
                    return;
                case R.id.direct_title:
                case R.id.direct_bottom_layout:
                case R.id.direct_content:
                default:
                    return;
                case R.id.direct_send:
                    if (ServiceUtils.isConnectInternet(SendMessageToMe.this)) {
                        new CommentBlog().execute("");
                        return;
                    }
                    Toast.makeText(SendMessageToMe.this, "网络出现异常", 1).show();
                    return;
                case R.id.direct_clear_layout:
                    AlertDialog.Builder builder = new AlertDialog.Builder(SendMessageToMe.this);
                    builder.setTitle("提示");
                    builder.setMessage("确定要清空所有内容吗?");
                    builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            SendMessageToMe.this.text.setText("");
                        }
                    });
                    builder.setNeutralButton("取消", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder.show();
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("发送微博中...请稍候");
        dialog.setIndeterminate(true);
        dialog.setCancelable(true);
        return dialog;
    }

    public class CommentBlog extends AsyncTask<String, String, String> {
        public CommentBlog() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            SendMessageToMe.this.sendResult = CommentTopicHttpUtils.sendDirect(SendMessageToMe.this.weiboId, SendMessageToMe.this.userId, SendMessageToMe.this.userName, SendMessageToMe.this.text.getText().toString());
            return "";
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            SendMessageToMe.this.removeDialog(0);
            if (SendMessageToMe.this.sendResult == null || !SendMessageToMe.this.sendResult.equals(SystemConfig.SUCCESS)) {
                Toast.makeText(SendMessageToMe.this, "发送微博失败", 1).show();
            } else {
                Toast.makeText(SendMessageToMe.this, "发送微博成功", 1).show();
            }
            super.onPostExecute((Object) result);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            SendMessageToMe.this.showDialog(0);
            super.onPreExecute();
        }
    }
}
