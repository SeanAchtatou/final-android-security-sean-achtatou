package cn.com.mzba.kdwb;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import cn.com.mzba.db.ConfigHelper;
import cn.com.mzba.service.SystemConfig;

public class AccountBindActivity extends Activity implements View.OnClickListener {
    private Button btnBack;
    private ImageButton btnHome;
    private ImageView ivNetease;
    private ImageView ivSina;
    private ImageView ivSohu;
    private ImageView ivTencent;
    private RelativeLayout neteaseLayout;
    private RelativeLayout sinaLayout;
    private RelativeLayout sohuLayout;
    private RelativeLayout tencentLayout;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.accountbind);
        this.sinaLayout = (RelativeLayout) findViewById(R.id.accountbind_sina);
        this.sinaLayout.setOnClickListener(this);
        this.tencentLayout = (RelativeLayout) findViewById(R.id.accountbind_tencent);
        this.tencentLayout.setOnClickListener(this);
        this.sohuLayout = (RelativeLayout) findViewById(R.id.accountbind_sohu);
        this.sohuLayout.setOnClickListener(this);
        this.neteaseLayout = (RelativeLayout) findViewById(R.id.accountbind_netease);
        this.neteaseLayout.setOnClickListener(this);
        this.btnBack = (Button) findViewById(R.id.accountbind_back);
        this.btnBack.setOnClickListener(this);
        this.btnHome = (ImageButton) findViewById(R.id.accountbind_index);
        this.btnHome.setOnClickListener(this);
        this.ivSina = (ImageView) findViewById(R.id.sina_imageview);
        this.ivTencent = (ImageView) findViewById(R.id.tencent_imageview);
        this.ivSohu = (ImageView) findViewById(R.id.sohu_imageview);
        this.ivNetease = (ImageView) findViewById(R.id.netease_imageview);
        if (ConfigHelper.USERINFO_SINA != null) {
            this.ivSina.setBackgroundResource(R.drawable.success);
        }
        if (ConfigHelper.USERINFO_TENCENT != null) {
            this.ivTencent.setBackgroundResource(R.drawable.success);
        }
        if (ConfigHelper.USERINFO_SOHU != null) {
            this.ivSohu.setBackgroundResource(R.drawable.success);
        }
        if (ConfigHelper.USERINFO_NETEASE != null) {
            this.ivNetease.setBackgroundResource(R.drawable.success);
        }
    }

    public void onClick(View v) {
        Class<AuthorizeActivity> cls = AuthorizeActivity.class;
        switch (v.getId()) {
            case R.id.accountbind_back /*2131361798*/:
                finish();
                return;
            case R.id.accountbind_index /*2131361800*/:
                startActivity(new Intent(this, HomeActivity.class));
                return;
            case R.id.accountbind_sina /*2131361801*/:
                if (ConfigHelper.USERINFO_SINA == null) {
                    Class<AuthorizeActivity> cls2 = AuthorizeActivity.class;
                    Intent intent = new Intent(this, cls);
                    intent.putExtra("thrid_weibo", SystemConfig.SINA_WEIBO);
                    startActivity(intent);
                    return;
                }
                Toast.makeText(this, "已绑定该微博", 1).show();
                return;
            case R.id.accountbind_tencent /*2131361805*/:
                if (ConfigHelper.USERINFO_TENCENT == null) {
                    Class<AuthorizeActivity> cls3 = AuthorizeActivity.class;
                    Intent intent2 = new Intent(this, cls);
                    intent2.putExtra("thrid_weibo", SystemConfig.TENCENT_WEIBO);
                    startActivity(intent2);
                    return;
                }
                Toast.makeText(this, "已绑定该微博", 1).show();
                return;
            case R.id.accountbind_sohu /*2131361809*/:
                if (ConfigHelper.USERINFO_SOHU == null) {
                    Class<AuthorizeActivity> cls4 = AuthorizeActivity.class;
                    Intent intent3 = new Intent(this, cls);
                    intent3.putExtra("thrid_weibo", SystemConfig.SOHU_WEIBO);
                    startActivity(intent3);
                    return;
                }
                Toast.makeText(this, "已绑定该微博", 1).show();
                return;
            case R.id.accountbind_netease /*2131361813*/:
                if (ConfigHelper.USERINFO_NETEASE != null) {
                    Toast.makeText(this, "已绑定该微博", 1).show();
                    return;
                } else if (SystemConfig.ISACCESS) {
                    Intent intentInputVerifier = new Intent();
                    intentInputVerifier.setClass(this, InputVerifierActivity.class);
                    startActivity(intentInputVerifier);
                    return;
                } else {
                    Class<AuthorizeActivity> cls5 = AuthorizeActivity.class;
                    Intent intent4 = new Intent(this, cls);
                    intent4.putExtra("thrid_weibo", SystemConfig.NETEASE_WEIBO);
                    startActivity(intent4);
                    return;
                }
            default:
                return;
        }
    }
}
