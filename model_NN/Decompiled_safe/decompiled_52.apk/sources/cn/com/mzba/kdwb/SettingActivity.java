package cn.com.mzba.kdwb;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.widget.Toast;
import cn.com.mzba.service.SystemConfig;

public class SettingActivity extends PreferenceActivity implements Preference.OnPreferenceChangeListener, Preference.OnPreferenceClickListener {
    private CheckBoxPreference autoLoadMode;
    private CheckBoxPreference autoRemind;
    private ListPreference checkTime;
    private Preference cleanHistory;
    private Preference remindset;
    private ListPreference textSize;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.setting);
        this.autoLoadMode = (CheckBoxPreference) findPreference("loadMore");
        this.checkTime = (ListPreference) findPreference("checkTime");
        this.textSize = (ListPreference) findPreference("textSize");
        this.cleanHistory = findPreference("cleanHistory");
        this.remindset = findPreference("remindset");
        this.autoRemind = (CheckBoxPreference) findPreference("autoRemind");
        this.autoLoadMode.setOnPreferenceChangeListener(this);
        this.checkTime.setOnPreferenceChangeListener(this);
        this.textSize.setOnPreferenceChangeListener(this);
        this.cleanHistory.setOnPreferenceClickListener(this);
        this.remindset.setOnPreferenceChangeListener(this);
        this.autoRemind.setOnPreferenceChangeListener(this);
    }

    public boolean onPreferenceChange(Preference preference, Object newValue) {
        if (preference == this.autoLoadMode) {
            Toast.makeText(this, "该设置需要重启应用方能生效！", 1).show();
        } else if (preference == this.autoRemind) {
            SystemConfig.autoRemind = new Boolean(newValue.toString()).booleanValue();
            if (SystemConfig.autoRemind) {
                Intent intent = new Intent();
                intent.setClass(this, AutoRemindStatusService.class);
                startService(intent);
            }
        } else if (preference != this.remindset) {
            if (preference == this.checkTime) {
                SystemConfig.checkTime = newValue.toString();
            } else if (preference == this.textSize) {
                SystemConfig.textSize = newValue.toString();
                if (HomeListActivity.adapter != null) {
                    HomeListActivity.adapter.notifyDataSetChanged();
                }
            }
        }
        return true;
    }

    public boolean onPreferenceClick(Preference preference) {
        if (preference != this.cleanHistory) {
            return true;
        }
        new AlertDialog.Builder(this).setTitle("清除历史记录").setMessage("是否真的要清除历史记录？").setPositiveButton("是", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        }).setNegativeButton("否", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).create().show();
        return true;
    }
}
