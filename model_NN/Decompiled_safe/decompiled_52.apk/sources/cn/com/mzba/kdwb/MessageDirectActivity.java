package cn.com.mzba.kdwb;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import cn.com.mzba.db.UserInfo;
import cn.com.mzba.model.Status;
import cn.com.mzba.model.StatusListAdapter;
import cn.com.mzba.service.CommentTopicHttpUtils;
import cn.com.mzba.service.ServiceUtils;
import cn.com.mzba.service.StatusHttpUtils;
import cn.com.mzba.service.SystemConfig;
import java.util.List;

public class MessageDirectActivity extends ListActivity implements View.OnClickListener {
    /* access modifiers changed from: private */
    public StatusListAdapter adapter;
    private Button btnNetease;
    private Button btnSina;
    private Button btnSohu;
    private Button btnTencent;
    /* access modifiers changed from: private */
    public int count = 20;
    /* access modifiers changed from: private */
    public int page = 1;
    /* access modifiers changed from: private */
    public String sinceId;
    /* access modifiers changed from: private */
    public String weiboId = SystemConfig.SINA_WEIBO;
    /* access modifiers changed from: private */
    public List<Status> weiboList;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.messagedirectlist);
        this.btnSina = (Button) findViewById(R.id.third_sina);
        this.btnSina.setOnClickListener(this);
        this.btnTencent = (Button) findViewById(R.id.third_tencent);
        this.btnTencent.setOnClickListener(this);
        this.btnNetease = (Button) findViewById(R.id.third_netease);
        this.btnNetease.setOnClickListener(this);
        this.btnSohu = (Button) findViewById(R.id.third_sohu);
        this.btnSohu.setOnClickListener(this);
        this.btnSina.setBackgroundResource(R.drawable.blog_thrid_left_d);
        if (ServiceUtils.isConnectInternet(this)) {
            new LoadWeiBoInfo().execute("");
            return;
        }
        Toast.makeText(this, "网络出现异常", 1).show();
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        if (position == 0) {
            refresh();
        } else if (position == getListAdapter().getCount() - 1) {
            loadMoreData();
        } else {
            onclick(this.weiboList.get(position - 1).getUser().getId(), this.weiboList.get(position - 1).getUser().getName(), this.weiboList.get(position).getId());
        }
    }

    private void refresh() {
        this.page = 1;
        showDialog(0);
        new Thread() {
            public void run() {
                MessageDirectActivity.this.loadWeiboInfo();
                MessageDirectActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        MessageDirectActivity.this.removeDialog(0);
                        new Thread() {
                            public void run() {
                                MessageDirectActivity.this.adapter.notifyDataSetChanged();
                            }
                        }.start();
                    }
                });
            }
        }.start();
    }

    private void loadMoreData() {
        showDialog(0);
        new Thread() {
            public void run() {
                MessageDirectActivity messageDirectActivity = MessageDirectActivity.this;
                messageDirectActivity.page = messageDirectActivity.page + 1;
                final List<Status> moreList = StatusHttpUtils.getDirectStatus(MessageDirectActivity.this.weiboId, MessageDirectActivity.this.page, MessageDirectActivity.this.count, MessageDirectActivity.this.sinceId);
                if (moreList == null || moreList.isEmpty()) {
                    MessageDirectActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            MessageDirectActivity.this.removeDialog(0);
                            Toast.makeText(MessageDirectActivity.this, "加载数据失败", 1).show();
                        }
                    });
                } else {
                    MessageDirectActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            MessageDirectActivity.this.adapter.addMoreDatas(moreList);
                            MessageDirectActivity.this.sinceId = ((Status) MessageDirectActivity.this.weiboList.get(0)).getId();
                            MessageDirectActivity.this.removeDialog(0);
                        }
                    });
                }
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public void loadWeiboInfo() {
        this.weiboList = StatusHttpUtils.getDirectStatus(this.weiboId, this.page, this.count, this.sinceId);
        if (this.weiboList != null && !this.weiboList.isEmpty()) {
            this.sinceId = this.weiboList.get(0).getId();
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        ProgressDialog dialog = new ProgressDialog(getParent());
        dialog.setMessage("加载中...请稍候");
        dialog.setIndeterminate(true);
        dialog.setCancelable(true);
        return dialog;
    }

    public class LoadWeiBoInfo extends AsyncTask<String, String, String> {
        public LoadWeiBoInfo() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            MessageDirectActivity.this.loadWeiboInfo();
            return "";
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            if (MessageDirectActivity.this.weiboList != null && !MessageDirectActivity.this.weiboList.isEmpty()) {
                MessageDirectActivity.this.adapter = new StatusListAdapter(MessageDirectActivity.this, MessageDirectActivity.this.weiboList);
                MessageDirectActivity.this.setListAdapter(MessageDirectActivity.this.adapter);
            }
            MessageDirectActivity.this.removeDialog(0);
            super.onPostExecute((Object) result);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            MessageDirectActivity.this.showDialog(0);
            super.onPreExecute();
        }
    }

    public void onclick(final String userId, final String userName, final String id) {
        Context dialogContext = new ContextThemeWrapper(getParent(), 16973836);
        ListAdapter adapter2 = new ArrayAdapter(dialogContext, 17367043, new String[]{getString(R.string.replay_direct), getString(R.string.delete_direct), getString(R.string.userinfo)});
        AlertDialog.Builder builder = new AlertDialog.Builder(dialogContext);
        builder.setTitle((int) R.string.choice_do);
        builder.setSingleChoiceItems(adapter2, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                switch (which) {
                    case 0:
                        Intent intent = new Intent();
                        intent.setClass(MessageDirectActivity.this, NewDirectActivity.class);
                        intent.putExtra(UserInfo.USERID, userId);
                        intent.putExtra(UserInfo.WEIBOID, MessageDirectActivity.this.weiboId);
                        intent.putExtra(UserInfo.USERNAME, userName);
                        MessageDirectActivity.this.startActivity(intent);
                        return;
                    case 1:
                        MessageDirectActivity.this.deleteDirect(id);
                        return;
                    case 2:
                        MessageDirectActivity.this.showUserInfo(userId);
                        return;
                    default:
                        return;
                }
            }
        });
        builder.setNegativeButton("返回", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    public void deleteDirect(final String id) {
        showDialog(0);
        new Thread() {
            public void run() {
                if (CommentTopicHttpUtils.deleteDirect(MessageDirectActivity.this.weiboId, id).equals(SystemConfig.SUCCESS)) {
                    MessageDirectActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            MessageDirectActivity.this.removeDialog(0);
                            MessageDirectActivity.this.adapter.notifyDataSetChanged();
                            Toast.makeText(MessageDirectActivity.this, "删除私信成功！", 1).show();
                        }
                    });
                } else {
                    MessageDirectActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            MessageDirectActivity.this.removeDialog(0);
                            Toast.makeText(MessageDirectActivity.this, "删除私信失败！", 1).show();
                        }
                    });
                }
            }
        }.start();
    }

    public void showUserInfo(String userId) {
        Intent intent = new Intent();
        intent.setClass(this, UserInfoActivity.class);
        Bundle extras = new Bundle();
        extras.putString(UserInfo.USERID, userId);
        extras.putString(UserInfo.WEIBOID, this.weiboId);
        intent.putExtras(extras);
        startActivity(intent);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.third_sina /*2131361992*/:
                if (this.weiboList != null) {
                    this.weiboList.clear();
                }
                if (this.adapter != null) {
                    this.adapter.notifyDataSetChanged();
                }
                this.weiboId = SystemConfig.SINA_WEIBO;
                new LoadWeiBoInfo().execute("");
                this.btnSina.setBackgroundResource(R.drawable.blog_thrid_left_d);
                this.btnTencent.setBackgroundResource(R.drawable.blog_thrid_center_n);
                this.btnSohu.setBackgroundResource(R.drawable.blog_thrid_center_n);
                this.btnNetease.setBackgroundResource(R.drawable.blog_thrid_right_n);
                return;
            case R.id.third_tencent /*2131361993*/:
                if (this.weiboList != null) {
                    this.weiboList.clear();
                }
                if (this.adapter != null) {
                    this.adapter.notifyDataSetChanged();
                }
                this.weiboId = SystemConfig.TENCENT_WEIBO;
                new LoadWeiBoInfo().execute("");
                this.btnSina.setBackgroundResource(R.drawable.blog_thrid_left_n);
                this.btnTencent.setBackgroundResource(R.drawable.blog_thrid_center_d);
                this.btnSohu.setBackgroundResource(R.drawable.blog_thrid_center_n);
                this.btnNetease.setBackgroundResource(R.drawable.blog_thrid_right_n);
                return;
            case R.id.third_sohu /*2131361994*/:
                if (this.weiboList != null) {
                    this.weiboList.clear();
                }
                if (this.adapter != null) {
                    this.adapter.notifyDataSetChanged();
                }
                this.weiboId = SystemConfig.SOHU_WEIBO;
                new LoadWeiBoInfo().execute("");
                this.btnSina.setBackgroundResource(R.drawable.blog_thrid_left_n);
                this.btnTencent.setBackgroundResource(R.drawable.blog_thrid_center_n);
                this.btnSohu.setBackgroundResource(R.drawable.blog_thrid_center_d);
                this.btnNetease.setBackgroundResource(R.drawable.blog_thrid_right_n);
                return;
            case R.id.third_netease /*2131361995*/:
                if (this.weiboList != null) {
                    this.weiboList.clear();
                }
                if (this.adapter != null) {
                    this.adapter.notifyDataSetChanged();
                }
                this.weiboId = SystemConfig.NETEASE_WEIBO;
                new LoadWeiBoInfo().execute("");
                this.btnSina.setBackgroundResource(R.drawable.blog_thrid_left_n);
                this.btnTencent.setBackgroundResource(R.drawable.blog_thrid_center_n);
                this.btnSohu.setBackgroundResource(R.drawable.blog_thrid_center_n);
                this.btnNetease.setBackgroundResource(R.drawable.blog_thrid_right_d);
                return;
            default:
                return;
        }
    }
}
