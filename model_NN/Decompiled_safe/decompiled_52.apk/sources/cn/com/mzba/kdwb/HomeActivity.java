package cn.com.mzba.kdwb;

import android.app.ActivityGroup;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import cn.com.mzba.service.SystemConfig;

public class HomeActivity extends ActivityGroup {
    private LinearLayout bodyLayout;
    /* access modifiers changed from: private */
    public int flag = 0;
    /* access modifiers changed from: private */
    public LinearLayout index;
    /* access modifiers changed from: private */
    public LinearLayout message;
    /* access modifiers changed from: private */
    public LinearLayout more;
    /* access modifiers changed from: private */
    public LinearLayout myinfo;
    private int type = 0;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.home);
        SharedPreferences prefs = getSharedPreferences(String.valueOf(getPackageName()) + "_preferences", 0);
        SystemConfig.textSize = prefs.getString("textSize", "15");
        SystemConfig.autoRemind = prefs.getBoolean("autoRemind", false);
        SystemConfig.checkTime = prefs.getString("checkTime", "2");
        SystemConfig.autoLoadMode = prefs.getBoolean("loadMore", false);
        String typeStr = getIntent().getStringExtra("message");
        if (typeStr != null && typeStr.equals("message")) {
            this.flag = 1;
            this.type = getIntent().getIntExtra("key", 0);
        }
        initLayout();
    }

    public void initLayout() {
        this.bodyLayout = (LinearLayout) findViewById(R.id.body_layout);
        this.index = (LinearLayout) findViewById(R.id.index);
        this.index.setOnTouchListener(new OnTouchClickListener());
        this.message = (LinearLayout) findViewById(R.id.message);
        this.message.setOnTouchListener(new OnTouchClickListener());
        this.myinfo = (LinearLayout) findViewById(R.id.myinfo);
        this.myinfo.setOnTouchListener(new OnTouchClickListener());
        this.more = (LinearLayout) findViewById(R.id.more);
        this.more.setOnTouchListener(new OnTouchClickListener());
        showView(this.flag);
        if (this.flag == 0) {
            this.index.setBackgroundResource(R.drawable.home_btn_bg_d);
            this.message.setBackgroundResource(0);
            this.myinfo.setBackgroundResource(0);
            this.more.setBackgroundResource(0);
        } else if (this.flag == 1) {
            this.index.setBackgroundResource(0);
            this.message.setBackgroundResource(R.drawable.home_btn_bg_d);
            this.myinfo.setBackgroundResource(0);
            this.more.setBackgroundResource(0);
        }
        this.index.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HomeActivity.this.flag = 0;
                HomeActivity.this.showView(HomeActivity.this.flag);
                HomeActivity.this.index.setBackgroundResource(R.drawable.home_btn_bg_d);
                HomeActivity.this.message.setBackgroundResource(0);
                HomeActivity.this.myinfo.setBackgroundResource(0);
                HomeActivity.this.more.setBackgroundResource(0);
            }
        });
        this.message.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HomeActivity.this.flag = 1;
                HomeActivity.this.showView(HomeActivity.this.flag);
                HomeActivity.this.index.setBackgroundResource(0);
                HomeActivity.this.message.setBackgroundResource(R.drawable.home_btn_bg_d);
                HomeActivity.this.myinfo.setBackgroundResource(0);
                HomeActivity.this.more.setBackgroundResource(0);
            }
        });
        this.myinfo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HomeActivity.this.flag = 2;
                HomeActivity.this.showView(HomeActivity.this.flag);
                HomeActivity.this.index.setBackgroundResource(0);
                HomeActivity.this.message.setBackgroundResource(0);
                HomeActivity.this.myinfo.setBackgroundResource(R.drawable.home_btn_bg_d);
                HomeActivity.this.more.setBackgroundResource(0);
            }
        });
        this.more.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HomeActivity.this.flag = 3;
                HomeActivity.this.showView(HomeActivity.this.flag);
                HomeActivity.this.index.setBackgroundResource(0);
                HomeActivity.this.message.setBackgroundResource(0);
                HomeActivity.this.myinfo.setBackgroundResource(0);
                HomeActivity.this.more.setBackgroundResource(R.drawable.home_btn_bg_d);
            }
        });
    }

    public class OnTouchClickListener implements View.OnTouchListener {
        public OnTouchClickListener() {
        }

        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == 1) {
                v.setBackgroundResource(0);
            } else if (event.getAction() == 0) {
                v.setBackgroundResource(R.drawable.home_btn_bg_s);
            }
            return false;
        }
    }

    public void showView(int flag2) {
        switch (flag2) {
            case 0:
                this.bodyLayout.removeAllViews();
                View view = getLocalActivityManager().startActivity("homelist", new Intent(this, HomeListActivity.class)).getDecorView();
                DisplayMetrics dm = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(dm);
                view.setLayoutParams(new ViewGroup.LayoutParams(-1, dm.heightPixels - 85));
                this.bodyLayout.addView(view);
                return;
            case 1:
                this.bodyLayout.removeAllViews();
                Intent intent2 = new Intent(this, MessageActivity.class);
                Bundle extras = new Bundle();
                extras.putInt("keyid", this.type);
                intent2.putExtras(extras);
                this.bodyLayout.addView(getLocalActivityManager().startActivity("message", intent2).getDecorView());
                return;
            case 2:
                this.bodyLayout.removeAllViews();
                this.bodyLayout.addView(getLocalActivityManager().startActivity("info", new Intent(this, InfoActivity.class)).getDecorView());
                return;
            case 3:
                this.bodyLayout.removeAllViews();
                this.bodyLayout.addView(getLocalActivityManager().startActivity("more", new Intent(this, MoreActivity.class)).getDecorView());
                return;
            default:
                return;
        }
    }
}
