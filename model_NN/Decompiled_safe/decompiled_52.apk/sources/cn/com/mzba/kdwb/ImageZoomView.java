package cn.com.mzba.kdwb;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import java.util.Observable;
import java.util.Observer;

public class ImageZoomView extends View implements Observer {
    private float mAspectQuotient;
    private Bitmap mBitmap;
    private final Paint mPaint = new Paint(2);
    private final Rect mRectDst = new Rect();
    private final Rect mRectSrc = new Rect();
    private ZoomState mState;

    public ImageZoomView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setZoomState(ZoomState state) {
        if (this.mState != null) {
            this.mState.deleteObserver(this);
        }
        this.mState = state;
        this.mState.addObserver(this);
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.mBitmap != null && this.mState != null) {
            int viewWidth = getWidth();
            int viewHeight = getHeight();
            int bitmapWidth = this.mBitmap.getWidth();
            int bitmapHeight = this.mBitmap.getHeight();
            float panX = this.mState.getPanX();
            float panY = this.mState.getPanY();
            float zoomX = (this.mState.getZoomX(this.mAspectQuotient) * ((float) viewWidth)) / ((float) bitmapWidth);
            float zoomY = (this.mState.getZoomY(this.mAspectQuotient) * ((float) viewHeight)) / ((float) bitmapHeight);
            this.mRectSrc.left = (int) ((((float) bitmapWidth) * panX) - (((float) viewWidth) / (zoomX * 2.0f)));
            this.mRectSrc.top = (int) ((((float) bitmapHeight) * panY) - (((float) viewHeight) / (zoomY * 2.0f)));
            this.mRectSrc.right = (int) (((float) this.mRectSrc.left) + (((float) viewWidth) / zoomX));
            this.mRectSrc.bottom = (int) (((float) this.mRectSrc.top) + (((float) viewHeight) / zoomY));
            this.mRectDst.left = getLeft();
            this.mRectDst.top = getTop();
            this.mRectDst.right = getRight();
            this.mRectDst.bottom = getBottom();
            if (this.mRectSrc.left < 0) {
                Rect rect = this.mRectDst;
                rect.left = (int) (((float) rect.left) + (((float) (-this.mRectSrc.left)) * zoomX));
                this.mRectSrc.left = 0;
            }
            if (this.mRectSrc.right > bitmapWidth) {
                Rect rect2 = this.mRectDst;
                rect2.right = (int) (((float) rect2.right) - (((float) (this.mRectSrc.right - bitmapWidth)) * zoomX));
                this.mRectSrc.right = bitmapWidth;
            }
            if (this.mRectSrc.top < 0) {
                Rect rect3 = this.mRectDst;
                rect3.top = (int) (((float) rect3.top) + (((float) (-this.mRectSrc.top)) * zoomY));
                this.mRectSrc.top = 0;
            }
            if (this.mRectSrc.bottom > bitmapHeight) {
                Rect rect4 = this.mRectDst;
                rect4.bottom = (int) (((float) rect4.bottom) - (((float) (this.mRectSrc.bottom - bitmapHeight)) * zoomY));
                this.mRectSrc.bottom = bitmapHeight;
            }
            canvas.drawBitmap(this.mBitmap, this.mRectSrc, this.mRectDst, this.mPaint);
        }
    }

    public void update(Observable observable, Object data) {
        invalidate();
    }

    private void calculateAspectQuotient() {
        if (this.mBitmap != null) {
            this.mAspectQuotient = (((float) this.mBitmap.getWidth()) / ((float) this.mBitmap.getHeight())) / (((float) getWidth()) / ((float) getHeight()));
        }
    }

    public void setImage(Bitmap bitmap) {
        this.mBitmap = bitmap;
        calculateAspectQuotient();
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        calculateAspectQuotient();
    }
}
