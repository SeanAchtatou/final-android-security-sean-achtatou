package cn.com.mzba.kdwb;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import cn.com.mzba.db.UserInfo;
import cn.com.mzba.model.Status;
import cn.com.mzba.model.User;
import cn.com.mzba.service.AsyncImageLoader;
import cn.com.mzba.service.CommentTopicHttpUtils;
import cn.com.mzba.service.DateUtil;
import cn.com.mzba.service.ImageCallback;
import cn.com.mzba.service.MyAction;
import cn.com.mzba.service.ServiceUtils;
import cn.com.mzba.service.StatusHttpUtils;
import cn.com.mzba.service.SystemConfig;
import cn.com.mzba.service.TextAutoLink;

public class DetailActivity extends Activity implements View.OnClickListener {
    private Button btnBack;
    private Button btnBottomComment;
    /* access modifiers changed from: private */
    public Button btnBottomFav;
    private Button btnBottomMore;
    private Button btnBottomRedirect;
    private Button btnBottomRefresh;
    private Button btnComment;
    private ImageButton btnIndex;
    private Button btnTransmit;
    /* access modifiers changed from: private */
    public ProgressDialog dialog;
    /* access modifiers changed from: private */
    public String id;
    private AsyncImageLoader imageLoader = new AsyncImageLoader();
    private boolean isFav;
    private boolean isRegister = false;
    private FinishBrocastReciever receiver;
    private TextView rtComments;
    private String rtId;
    private TextView rtRedirect;
    /* access modifiers changed from: private */
    public Status status;
    private User user;
    private String userId;
    private RelativeLayout userLayout;
    /* access modifiers changed from: private */
    public String weiboId;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.detailview);
        Intent intent = getIntent();
        this.id = intent.getStringExtra("id");
        if (this.id != null) {
            this.weiboId = intent.getStringExtra(UserInfo.WEIBOID);
            if (ServiceUtils.isConnectInternet(this)) {
                new LoadWeiBoInfo().execute("");
            }
        }
        this.userLayout = (RelativeLayout) findViewById(R.id.detail_user_layout);
        this.userLayout.setOnClickListener(this);
        this.btnIndex = (ImageButton) findViewById(R.id.detail_index);
        this.btnIndex.setOnClickListener(this);
        this.btnBack = (Button) findViewById(R.id.detail_back);
        this.btnBack.setOnClickListener(this);
        ScrollView scrollView = (ScrollView) findViewById(R.id.scrollView);
        scrollView.setVerticalScrollBarEnabled(false);
        scrollView.setHorizontalScrollBarEnabled(false);
        this.btnComment = (Button) findViewById(R.id.detail_comments);
        this.btnTransmit = (Button) findViewById(R.id.detail_redirect);
        this.btnComment.setOnClickListener(this);
        this.btnTransmit.setOnClickListener(this);
        this.btnBottomRefresh = (Button) findViewById(R.id.detail_bottom_refresh);
        this.btnBottomComment = (Button) findViewById(R.id.detail_bottom_comment);
        this.btnBottomRedirect = (Button) findViewById(R.id.detail_bottom_redirect);
        this.btnBottomFav = (Button) findViewById(R.id.detail_bottom_fav);
        this.btnBottomMore = (Button) findViewById(R.id.detail_bottom_more);
        this.btnBottomComment.setOnClickListener(this);
        this.btnBottomComment.setOnTouchListener(new OnTouchClickListener());
        this.btnBottomFav.setOnClickListener(this);
        this.btnBottomFav.setOnTouchListener(new OnTouchClickListener());
        this.btnBottomMore.setOnClickListener(this);
        this.btnBottomMore.setOnTouchListener(new OnTouchClickListener());
        this.btnBottomRedirect.setOnClickListener(this);
        this.btnBottomRedirect.setOnTouchListener(new OnTouchClickListener());
        this.btnBottomRefresh.setOnClickListener(this);
        this.btnBottomRefresh.setOnTouchListener(new OnTouchClickListener());
    }

    public class OnTouchClickListener implements View.OnTouchListener {
        public OnTouchClickListener() {
        }

        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == 1) {
                v.setBackgroundResource(0);
            } else if (event.getAction() == 0) {
                v.setBackgroundResource(R.drawable.detail_btn_bg_selected);
            }
            return false;
        }
    }

    public void initWidget() {
        this.user = this.status.getUser();
        if (this.user != null) {
            this.userId = this.user.getId();
            ((TextView) findViewById(R.id.detail_userName)).setText(this.user.getScreenName());
            ImageView imageUserIcon = (ImageView) findViewById(R.id.detail_userIcon);
            imageUserIcon.setImageDrawable(this.imageLoader.loadDrawable(this.user.getProfileImageUrl(), imageUserIcon, new ImageCallback() {
                public void imageLoaded(Drawable imageDrawable, ImageView imageView, String imageUrl) {
                    imageView.setImageDrawable(imageDrawable);
                }
            }));
            TextView txtText = (TextView) findViewById(R.id.detail_text);
            TextAutoLink.addURLSpan(this, this.status.getText(), txtText);
            txtText.setMovementMethod(LinkMovementMethod.getInstance());
            ((TextView) findViewById(R.id.detail_time)).setText(DateUtil.getCreateAt(this.status.getCreatedAt()));
            TextView txtSource = (TextView) findViewById(R.id.detail_source);
            txtSource.setText(Html.fromHtml(this.status.getSource()));
            txtSource.setMovementMethod(LinkMovementMethod.getInstance());
            ImageView imagePic = (ImageView) findViewById(R.id.detail_pic);
            String textPic = this.status.getThumbnailPic();
            String textPic2 = this.status.getOriginalPic();
            if (textPic == null || textPic.equals("null")) {
                imagePic.setVisibility(8);
            } else {
                imagePic.setVisibility(0);
                imagePic.setImageDrawable(this.imageLoader.loadDrawable(textPic, imagePic, new ImageCallback() {
                    public void imageLoaded(Drawable imageDrawable, ImageView imageView, String imageUrl) {
                        imageView.setImageDrawable(imageDrawable);
                    }
                }));
                imagePic.setTag(textPic2);
                final String str = textPic;
                imagePic.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        Object obj = v.getTag();
                        if (obj != null) {
                            String imagePath = obj.toString();
                            if (DetailActivity.this.weiboId.equals(SystemConfig.TENCENT_WEIBO)) {
                                imagePath = String.valueOf(str) + "/460";
                            }
                            Intent intent = new Intent();
                            intent.setClass(DetailActivity.this, ShowImageActivity.class);
                            intent.putExtra("pic", imagePath);
                            DetailActivity.this.startActivity(intent);
                        }
                    }
                });
            }
            TextView txtTransmit = (TextView) findViewById(R.id.detail_transmit);
            if (this.status.isRetweetedStatus()) {
                Status rtStatus = this.status.getStatus();
                this.rtId = rtStatus.getId();
                ((RelativeLayout) findViewById(R.id.detail_transmit_layout)).setBackgroundResource(R.drawable.detail_popup);
                TextAutoLink.addURLSpan(this, rtStatus.getText(), txtTransmit);
                ImageView imageTransmitPic = (ImageView) findViewById(R.id.detail_transmit_pic);
                String transmitPic = rtStatus.getThumbnailPic();
                String transmitPic2 = rtStatus.getOriginalPic();
                if (transmitPic != null) {
                    imageTransmitPic.setVisibility(0);
                    imageTransmitPic.setImageDrawable(this.imageLoader.loadDrawable(transmitPic, imageTransmitPic, new ImageCallback() {
                        public void imageLoaded(Drawable imageDrawable, ImageView imageView, String imageUrl) {
                            imageView.setImageDrawable(imageDrawable);
                        }
                    }));
                    imageTransmitPic.setTag(transmitPic2);
                    imageTransmitPic.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            Object obj = v.getTag();
                            if (obj != null) {
                                String imagePath = obj.toString();
                                Intent intent = new Intent();
                                intent.setClass(DetailActivity.this, ShowImageActivity.class);
                                intent.putExtra("pic", imagePath);
                                DetailActivity.this.startActivity(intent);
                            }
                        }
                    });
                } else {
                    imageTransmitPic.setVisibility(8);
                }
                this.rtComments = (TextView) findViewById(R.id.detail_rt_comments);
                this.rtRedirect = (TextView) findViewById(R.id.detail_rt_redirect);
                String rtCommentCount = rtStatus.getComments();
                if (rtCommentCount != null) {
                    this.rtComments.setVisibility(0);
                    this.rtComments.setText(rtCommentCount);
                    this.rtComments.setOnClickListener(this);
                } else {
                    this.rtComments.setVisibility(8);
                }
                String rtRedirectCount = rtStatus.getRt();
                if (rtRedirectCount != null) {
                    this.rtRedirect.setVisibility(0);
                    this.rtRedirect.setText(rtRedirectCount);
                    this.rtRedirect.setOnClickListener(this);
                } else {
                    this.rtRedirect.setVisibility(8);
                }
            }
            this.btnTransmit = (Button) findViewById(R.id.detail_redirect);
            this.btnComment = (Button) findViewById(R.id.detail_comments);
            String rt = this.status.getRt();
            if (rt != null && !rt.equals("null")) {
                this.btnTransmit.setText("转发(" + rt + ")");
            }
            String comment = this.status.getComments();
            if (comment != null && !comment.equals("null")) {
                this.btnComment.setText("评论(" + comment + ")");
            }
            this.isFav = this.status.isFavorited();
            if (this.isFav) {
                this.btnBottomFav.setCompoundDrawablesWithIntrinsicBounds(0, (int) R.drawable.detail_fav_normal, 0, 0);
                this.btnBottomFav.setText("取消收藏");
                return;
            }
            this.btnBottomFav.setCompoundDrawablesWithIntrinsicBounds(0, (int) R.drawable.detail_fav_highlight_normal, 0, 0);
            this.btnBottomFav.setText("收藏");
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id2) {
        ProgressDialog dialog2 = new ProgressDialog(this);
        dialog2.setMessage("加载中...请稍候");
        dialog2.setIndeterminate(true);
        dialog2.setCancelable(true);
        return dialog2;
    }

    public class LoadWeiBoInfo extends AsyncTask<String, String, String> {
        public LoadWeiBoInfo() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            DetailActivity.this.status = StatusHttpUtils.getStatusById(DetailActivity.this.weiboId, DetailActivity.this.id);
            return "";
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            if (DetailActivity.this.status != null) {
                Log.i(DetailActivity.class.getCanonicalName(), DetailActivity.this.status.toString());
                DetailActivity.this.initWidget();
            }
            DetailActivity.this.removeDialog(0);
            super.onPostExecute((Object) result);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            DetailActivity.this.showDialog(0);
            super.onPreExecute();
        }
    }

    public void commentsOrRt(String id2, String key) {
        Intent intent = new Intent();
        intent.setClass(this, NewCommentActivity.class);
        Bundle extras = new Bundle();
        extras.putString("id", id2);
        extras.putString("key", key);
        extras.putString(UserInfo.WEIBOID, this.weiboId);
        intent.putExtras(extras);
        startActivity(intent);
    }

    public void fav() {
        if (this.isFav) {
            this.dialog = new ProgressDialog(this);
            this.dialog.setTitle("收藏设置");
            this.dialog.setMessage("收藏微博中...请稍候");
            this.dialog.show();
            new Thread() {
                public void run() {
                    final String result = CommentTopicHttpUtils.deleteFavorite(DetailActivity.this.weiboId, DetailActivity.this.id);
                    if (result != null) {
                        DetailActivity.this.runOnUiThread(new Runnable() {
                            public void run() {
                                if (result.equals(SystemConfig.SUCCESS)) {
                                    DetailActivity.this.dialog.dismiss();
                                    DetailActivity.this.btnBottomFav.setCompoundDrawablesWithIntrinsicBounds(0, (int) R.drawable.detail_fav_highlight_normal, 0, 0);
                                    DetailActivity.this.btnBottomFav.setText("收藏");
                                    Toast.makeText(DetailActivity.this, "取消收藏微博成功", 1).show();
                                    return;
                                }
                                DetailActivity.this.dialog.dismiss();
                                Toast.makeText(DetailActivity.this, "取消收藏微博失败", 1).show();
                            }
                        });
                    } else {
                        DetailActivity.this.runOnUiThread(new Runnable() {
                            public void run() {
                                DetailActivity.this.dialog.dismiss();
                                Toast.makeText(DetailActivity.this, "取消收藏微博失败", 1).show();
                            }
                        });
                    }
                }
            }.start();
            return;
        }
        new Thread() {
            public void run() {
                final String result = CommentTopicHttpUtils.addFavorite(DetailActivity.this.weiboId, DetailActivity.this.id);
                if (result != null) {
                    DetailActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            if (result.equals(SystemConfig.SUCCESS)) {
                                DetailActivity.this.dialog.dismiss();
                                DetailActivity.this.btnBottomFav.setCompoundDrawablesWithIntrinsicBounds(0, (int) R.drawable.detail_fav_normal, 0, 0);
                                DetailActivity.this.btnBottomFav.setText("取消收藏");
                                Toast.makeText(DetailActivity.this, "收藏微博成功", 1).show();
                                return;
                            }
                            DetailActivity.this.dialog.dismiss();
                            Toast.makeText(DetailActivity.this, "收藏微博失败", 1).show();
                        }
                    });
                } else {
                    DetailActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            DetailActivity.this.dialog.dismiss();
                            Toast.makeText(DetailActivity.this, "收藏微博失败", 1).show();
                        }
                    });
                }
            }
        }.start();
    }

    public class FinishBrocastReciever extends BroadcastReceiver {
        public FinishBrocastReciever() {
        }

        public void onReceive(Context context, Intent intent) {
            DetailActivity.this.finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.receiver = new FinishBrocastReciever();
        IntentFilter filter = new IntentFilter();
        filter.addAction(MyAction.DELETESTATUS);
        registerReceiver(this.receiver, filter);
        this.isRegister = true;
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.isRegister) {
            unregisterReceiver(this.receiver);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.status = null;
        this.user = null;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.detail_back /*2131361854*/:
                finish();
                return;
            case R.id.detail_index /*2131361855*/:
                Intent intent = new Intent();
                intent.setClass(this, HomeActivity.class);
                startActivity(intent);
                finish();
                return;
            case R.id.detail_user_layout /*2131361856*/:
                Intent intent6 = new Intent();
                intent6.setClass(this, UserInfoActivity.class);
                intent6.putExtra(UserInfo.USERID, this.userId);
                intent6.putExtra(UserInfo.WEIBOID, this.weiboId);
                startActivity(intent6);
                return;
            case R.id.detail_userIcon /*2131361857*/:
            case R.id.detail_userName /*2131361858*/:
            case R.id.scrollView /*2131361859*/:
            case R.id.detail_bottomtoolbar /*2131361860*/:
            case R.id.detail_text /*2131361861*/:
            case R.id.detail_pic /*2131361862*/:
            case R.id.detail_transmit_layout /*2131361863*/:
            case R.id.detail_rtStatus_layout /*2131361864*/:
            case R.id.detail_transmit /*2131361865*/:
            case R.id.detail_transmit_pic /*2131361866*/:
            case R.id.rt_layout /*2131361867*/:
            case R.id.detail_source /*2131361872*/:
            case R.id.detail_time /*2131361873*/:
            default:
                return;
            case R.id.detail_rt_redirect /*2131361868*/:
                commentsOrRt(this.rtId, MyAction.REDIRECT);
                return;
            case R.id.detail_rt_comments /*2131361869*/:
                Intent intent3 = new Intent();
                intent3.setClass(this, CommentActivity.class);
                intent3.putExtra("id", this.rtId);
                intent3.putExtra(UserInfo.WEIBOID, this.weiboId);
                intent3.putExtra(UserInfo.USERID, this.userId);
                startActivity(intent3);
                return;
            case R.id.detail_redirect /*2131361870*/:
                commentsOrRt(this.id, MyAction.REDIRECT);
                return;
            case R.id.detail_comments /*2131361871*/:
                Intent intent2 = new Intent();
                intent2.setClass(this, CommentActivity.class);
                intent2.putExtra("id", this.id);
                intent2.putExtra(UserInfo.WEIBOID, this.weiboId);
                intent2.putExtra(UserInfo.USERID, this.userId);
                startActivity(intent2);
                return;
            case R.id.detail_bottom_refresh /*2131361874*/:
                if (ServiceUtils.isConnectInternet(this)) {
                    new LoadWeiBoInfo().execute("");
                    return;
                }
                Toast.makeText(this, "网络出现异常", 1).show();
                return;
            case R.id.detail_bottom_comment /*2131361875*/:
                commentsOrRt(this.id, "comment");
                return;
            case R.id.detail_bottom_redirect /*2131361876*/:
                commentsOrRt(this.id, MyAction.REDIRECT);
                return;
            case R.id.detail_bottom_fav /*2131361877*/:
                fav();
                return;
            case R.id.detail_bottom_more /*2131361878*/:
                Intent intent7 = new Intent();
                intent7.setClass(this, DialogActivity.class);
                intent7.putExtra("id", this.id);
                intent7.putExtra(UserInfo.USERID, this.userId);
                intent7.putExtra(UserInfo.WEIBOID, this.weiboId);
                startActivity(intent7);
                return;
        }
    }
}
