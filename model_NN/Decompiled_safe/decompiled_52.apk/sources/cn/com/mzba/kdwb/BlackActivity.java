package cn.com.mzba.kdwb;

import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import cn.com.mzba.db.UserInfo;
import cn.com.mzba.model.User;
import cn.com.mzba.service.AsyncImageLoader;
import cn.com.mzba.service.ImageCallback;
import cn.com.mzba.service.ServiceUtils;
import cn.com.mzba.service.SystemConfig;
import cn.com.mzba.service.UserHttpUtils;
import java.util.List;

public class BlackActivity extends ListActivity implements View.OnClickListener {
    /* access modifiers changed from: private */
    public BlackingAdapter adapter;
    private Button btnBack;
    private ImageButton btnHome;
    /* access modifiers changed from: private */
    public int count = 20;
    /* access modifiers changed from: private */
    public int page = 1;
    /* access modifiers changed from: private */
    public String userId;
    /* access modifiers changed from: private */
    public List<User> userList;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.blacklist);
        this.btnBack = (Button) findViewById(R.id.black_back);
        this.btnBack.setOnClickListener(this);
        this.btnHome = (ImageButton) findViewById(R.id.black_index);
        this.btnHome.setOnClickListener(this);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.userId = extras.getString("user_id");
            if (this.userId == null) {
                return;
            }
            if (ServiceUtils.isConnectInternet(this)) {
                new LoadWeiBoInfo().execute("");
                return;
            }
            Toast.makeText(this, "网络异常！", 1).show();
        }
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        if (position == 0) {
            refresh();
        } else if (position == getListAdapter().getCount() - 1) {
            loadMoreData();
        } else {
            Intent intent = new Intent();
            intent.setClass(this, UserInfoActivity.class);
            Bundle extras = new Bundle();
            extras.putString(UserInfo.USERID, this.userId);
            intent.putExtras(extras);
            startActivity(intent);
        }
    }

    private void refresh() {
        this.page = 1;
        showDialog(0);
        new Thread() {
            public void run() {
                BlackActivity.this.userList = UserHttpUtils.getBlocking(BlackActivity.this.page, BlackActivity.this.count);
                if (BlackActivity.this.userList != null && !BlackActivity.this.userList.isEmpty()) {
                    BlackActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            BlackActivity.this.adapter.notifyDataSetChanged();
                            BlackActivity.this.removeDialog(0);
                        }
                    });
                }
            }
        }.start();
    }

    private void loadMoreData() {
        showDialog(0);
        new Thread() {
            public void run() {
                BlackActivity blackActivity = BlackActivity.this;
                blackActivity.page = blackActivity.page + 1;
                final List<User> list = UserHttpUtils.getBlocking(BlackActivity.this.page, BlackActivity.this.count);
                if (list == null || list.isEmpty()) {
                    BlackActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            BlackActivity.this.removeDialog(0);
                            Toast.makeText(BlackActivity.this, "加载数据失败", 1).show();
                        }
                    });
                } else {
                    BlackActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            BlackActivity.this.userList.addAll(list);
                            BlackActivity.this.adapter.notifyDataSetChanged();
                            BlackActivity.this.removeDialog(0);
                        }
                    });
                }
            }
        }.start();
    }

    public class BlackingAdapter extends BaseAdapter {
        public BlackingAdapter() {
        }

        public int getCount() {
            return BlackActivity.this.userList.size() + 2;
        }

        public Object getItem(int position) {
            return BlackActivity.this.userList.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View view;
            if (position == 0) {
                return LayoutInflater.from(BlackActivity.this.getApplicationContext()).inflate((int) R.layout.listheader, (ViewGroup) null);
            }
            if (position == getCount() - 1) {
                return LayoutInflater.from(BlackActivity.this.getApplicationContext()).inflate((int) R.layout.listfooter, (ViewGroup) null);
            }
            if (convertView == null || convertView.getId() != R.id.topic_layout) {
                view = LayoutInflater.from(BlackActivity.this.getApplicationContext()).inflate((int) R.layout.blacklistitem, (ViewGroup) null);
                ImageView userIcon = (ImageView) view.findViewById(R.id.blacklistitem_userIcon);
                final Button button = (Button) view.findViewById(R.id.blacklistitem_button);
                ((TextView) view.findViewById(R.id.blacklistitem_userName)).setText(((User) BlackActivity.this.userList.get(position - 1)).getScreenName());
                userIcon.setImageDrawable(new AsyncImageLoader().loadDrawable(((User) BlackActivity.this.userList.get(position - 1)).getProfileImageUrl(), userIcon, new ImageCallback() {
                    public void imageLoaded(Drawable imageDrawable, ImageView imageView, String imageUrl) {
                        imageView.setImageDrawable(imageDrawable);
                    }
                }));
                if (((User) BlackActivity.this.userList.get(position - 1)).isBlack()) {
                    button.setText("移出黑名单");
                }
                button.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        BlackActivity.this.showDialog(0);
                        final Button button = button;
                        new Thread() {
                            public void run() {
                                if (UserHttpUtils.destroyBlocks(BlackActivity.this.userId).equals(SystemConfig.SUCCESS)) {
                                    BlackActivity access$0 = BlackActivity.this;
                                    final Button button = button;
                                    access$0.runOnUiThread(new Runnable() {
                                        public void run() {
                                            button.setText("加入黑名单");
                                            BlackActivity.this.adapter.notifyDataSetChanged();
                                            BlackActivity.this.removeDialog(0);
                                        }
                                    });
                                    return;
                                }
                                BlackActivity.this.runOnUiThread(new Runnable() {
                                    public void run() {
                                        Toast.makeText(BlackActivity.this, "移出黑名单失败！", 1).show();
                                        BlackActivity.this.removeDialog(0);
                                    }
                                });
                            }
                        }.start();
                    }
                });
            } else {
                view = convertView;
            }
            return view;
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("加载中...请稍候");
        dialog.setIndeterminate(true);
        dialog.setCancelable(true);
        return dialog;
    }

    public class LoadWeiBoInfo extends AsyncTask<String, String, String> {
        public LoadWeiBoInfo() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            BlackActivity.this.userList = UserHttpUtils.getBlocking(BlackActivity.this.page, BlackActivity.this.count);
            return "";
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            if (BlackActivity.this.userList != null && !BlackActivity.this.userList.isEmpty()) {
                BlackActivity.this.adapter = new BlackingAdapter();
                BlackActivity.this.setListAdapter(BlackActivity.this.adapter);
            }
            BlackActivity.this.removeDialog(0);
            super.onPostExecute((Object) result);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            BlackActivity.this.showDialog(0);
            super.onPreExecute();
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.black_back /*2131361828*/:
                finish();
                return;
            case R.id.black_textView /*2131361829*/:
            default:
                return;
            case R.id.black_index /*2131361830*/:
                startActivity(new Intent(this, HomeActivity.class));
                return;
        }
    }
}
