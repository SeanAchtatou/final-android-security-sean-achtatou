package cn.com.mzba.kdwb;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import cn.com.mzba.db.ConfigHelper;
import cn.com.mzba.db.UserInfo;
import cn.com.mzba.model.User;
import cn.com.mzba.service.AsyncImageLoader;
import cn.com.mzba.service.ImageCallback;
import cn.com.mzba.service.MyAction;
import cn.com.mzba.service.ServiceUtils;
import cn.com.mzba.service.SystemConfig;
import cn.com.mzba.service.UserHttpUtils;

public class InfoActivity extends Activity implements View.OnClickListener {
    private TextView address;
    private AsyncImageLoader asyncImageLoader;
    private Button attendtion;
    private Button btnNetease;
    private ImageButton btnNewWeibo;
    private ImageButton btnRefresh;
    private Button btnSina;
    private Button btnSohu;
    private Button btnTencent;
    private Button fans;
    private RelativeLayout favLayout;
    private TextView message;
    private Button topic;
    private TextView tvFav;
    /* access modifiers changed from: private */
    public User user;
    private ImageView userIcon;
    private String userId;
    private TextView userName;
    private ImageView userSex;
    private Button weibo;
    /* access modifiers changed from: private */
    public String weiboId = SystemConfig.SINA_WEIBO;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.info);
        this.btnNewWeibo = (ImageButton) findViewById(R.id.info_newWeibo);
        this.btnRefresh = (ImageButton) findViewById(R.id.info_refresh);
        this.btnNewWeibo.setOnClickListener(this);
        this.btnRefresh.setOnClickListener(this);
        this.tvFav = (TextView) findViewById(R.id.info_fav);
        this.favLayout = (RelativeLayout) findViewById(R.id.info_fav_layout);
        this.favLayout.setOnClickListener(this);
        this.userIcon = (ImageView) findViewById(R.id.info_userIcon);
        this.userSex = (ImageView) findViewById(R.id.info_sex);
        this.userName = (TextView) findViewById(R.id.info_userName);
        this.address = (TextView) findViewById(R.id.info_address);
        this.message = (TextView) findViewById(R.id.info_userMessage);
        this.attendtion = (Button) findViewById(R.id.info_attention);
        this.attendtion.setOnClickListener(this);
        this.weibo = (Button) findViewById(R.id.info_weibo);
        this.weibo.setOnClickListener(this);
        this.fans = (Button) findViewById(R.id.info_fans);
        this.fans.setOnClickListener(this);
        this.topic = (Button) findViewById(R.id.info_topic);
        this.topic.setOnClickListener(this);
        this.btnSina = (Button) findViewById(R.id.third_sina);
        this.btnSina.setOnClickListener(this);
        this.btnTencent = (Button) findViewById(R.id.third_tencent);
        this.btnTencent.setOnClickListener(this);
        this.btnNetease = (Button) findViewById(R.id.third_netease);
        this.btnNetease.setOnClickListener(this);
        this.btnSohu = (Button) findViewById(R.id.third_sohu);
        this.btnSohu.setOnClickListener(this);
        this.btnSina.setBackgroundResource(R.drawable.blog_thrid_left_d);
        this.asyncImageLoader = new AsyncImageLoader();
        if (ServiceUtils.isConnectInternet(this)) {
            new LoadInfo().execute("");
            return;
        }
        Toast.makeText(this, "网络出现异常", 1).show();
    }

    public void initData() {
        if (this.user == null) {
            Toast.makeText(this, "网络繁忙！", 1).show();
            return;
        }
        this.userId = this.user.getId();
        Drawable drawable = this.asyncImageLoader.loadDrawable(this.user.getProfileImageUrl(), this.userIcon, new ImageCallback() {
            public void imageLoaded(Drawable imageDrawable, ImageView imageView, String imageUrl) {
                imageView.setImageDrawable(imageDrawable);
            }
        });
        if (drawable != null) {
            this.userIcon.setImageDrawable(drawable);
        }
        this.userName.setText(this.user.getScreenName());
        if (this.weiboId.equals(SystemConfig.SINA_WEIBO)) {
            if (this.user.getGender().equals("m")) {
                this.userSex.setImageResource(R.drawable.icon_male);
            } else if (this.user.getGender().equals("f")) {
                this.userSex.setImageResource(R.drawable.icon_female);
            }
        }
        this.address.setText(this.user.getLocation());
        this.message.setText(this.user.getDescription());
        this.attendtion.setText("关注(" + this.user.getFriendsCount() + ")");
        this.weibo.setText("微博(" + this.user.getStatusesCount() + ")");
        this.fans.setText("粉丝(" + this.user.getFollowersCount() + ")");
        this.topic.setText("话题");
        this.tvFav.setText(this.user.getFavouritesCount());
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("加载中...请稍候");
        dialog.setIndeterminate(true);
        dialog.setCancelable(true);
        return dialog;
    }

    public class LoadInfo extends AsyncTask<String, String, String> {
        public LoadInfo() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            if (InfoActivity.this.weiboId.equals(SystemConfig.SINA_WEIBO) && ConfigHelper.USERINFO_SINA != null) {
                InfoActivity.this.user = UserHttpUtils.getUserById(InfoActivity.this.weiboId, ConfigHelper.USERINFO_SINA.getUserId());
                return "";
            } else if (InfoActivity.this.weiboId.equals(SystemConfig.TENCENT_WEIBO) && ConfigHelper.USERINFO_TENCENT != null) {
                InfoActivity.this.user = UserHttpUtils.getUserById(InfoActivity.this.weiboId, null);
                return "";
            } else if (InfoActivity.this.weiboId.equals(SystemConfig.SOHU_WEIBO) && ConfigHelper.USERINFO_SOHU != null) {
                InfoActivity.this.user = UserHttpUtils.getUserById(InfoActivity.this.weiboId, null);
                return "";
            } else if (!InfoActivity.this.weiboId.equals(SystemConfig.NETEASE_WEIBO) || ConfigHelper.USERINFO_NETEASE == null) {
                return "";
            } else {
                InfoActivity.this.user = UserHttpUtils.getUserById(InfoActivity.this.weiboId, null);
                return "";
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            InfoActivity.this.removeDialog(0);
            if (InfoActivity.this.user != null) {
                InfoActivity.this.initData();
            }
            super.onPostExecute((Object) result);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            InfoActivity.this.showDialog(0);
            super.onPreExecute();
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.info_newWeibo /*2131361908*/:
                Intent intent5 = new Intent();
                intent5.setClass(this, NewWeiboActivity.class);
                startActivity(intent5);
                return;
            case R.id.info_refresh /*2131361909*/:
                if (ServiceUtils.isConnectInternet(this)) {
                    new LoadInfo().execute("");
                    return;
                }
                Toast.makeText(this, "网络出现异常", 1).show();
                return;
            case R.id.info_attention /*2131361918*/:
                Intent attentionIntent = new Intent();
                attentionIntent.setClass(this, AttentionOrFansActivity.class);
                attentionIntent.putExtra("key", MyAction.FRIENDS);
                attentionIntent.putExtra(UserInfo.USERID, this.userId);
                attentionIntent.putExtra(UserInfo.WEIBOID, this.weiboId);
                startActivity(attentionIntent);
                return;
            case R.id.info_weibo /*2131361919*/:
                Intent intent = new Intent();
                intent.setClass(this, WeiboActivity.class);
                intent.putExtra(UserInfo.WEIBOID, this.weiboId);
                intent.putExtra("key", MyAction.USERSTATUS);
                intent.putExtra(UserInfo.USERID, this.userId);
                startActivity(intent);
                return;
            case R.id.info_fans /*2131361920*/:
                Intent fansIntent = new Intent();
                fansIntent.setClass(this, AttentionOrFansActivity.class);
                fansIntent.putExtra("key", MyAction.FOLLOWERS);
                fansIntent.putExtra(UserInfo.USERID, this.userId);
                fansIntent.putExtra(UserInfo.WEIBOID, this.weiboId);
                startActivity(fansIntent);
                return;
            case R.id.info_topic /*2131361921*/:
                Intent intent6 = new Intent();
                intent6.setClass(this, TopicActivity.class);
                intent6.putExtra(UserInfo.WEIBOID, this.weiboId);
                intent6.putExtra(UserInfo.USERID, this.userId);
                startActivity(intent6);
                return;
            case R.id.info_fav_layout /*2131361922*/:
                Intent intent4 = new Intent();
                intent4.setClass(this, WeiboActivity.class);
                intent4.putExtra("key", MyAction.FAVORITED);
                intent4.putExtra(UserInfo.WEIBOID, this.weiboId);
                startActivity(intent4);
                return;
            case R.id.third_sina /*2131361992*/:
                this.weiboId = SystemConfig.SINA_WEIBO;
                if (this.user != null) {
                    this.user = null;
                }
                new LoadInfo().execute("");
                this.btnSina.setBackgroundResource(R.drawable.blog_thrid_left_d);
                this.btnTencent.setBackgroundResource(R.drawable.blog_thrid_center_n);
                this.btnSohu.setBackgroundResource(R.drawable.blog_thrid_center_n);
                this.btnNetease.setBackgroundResource(R.drawable.blog_thrid_right_n);
                return;
            case R.id.third_tencent /*2131361993*/:
                this.weiboId = SystemConfig.TENCENT_WEIBO;
                if (this.user != null) {
                    this.user = null;
                }
                new LoadInfo().execute("");
                this.btnSina.setBackgroundResource(R.drawable.blog_thrid_left_n);
                this.btnTencent.setBackgroundResource(R.drawable.blog_thrid_center_d);
                this.btnSohu.setBackgroundResource(R.drawable.blog_thrid_center_n);
                this.btnNetease.setBackgroundResource(R.drawable.blog_thrid_right_n);
                return;
            case R.id.third_sohu /*2131361994*/:
                this.weiboId = SystemConfig.SOHU_WEIBO;
                if (this.user != null) {
                    this.user = null;
                }
                new LoadInfo().execute("");
                this.btnSina.setBackgroundResource(R.drawable.blog_thrid_left_n);
                this.btnTencent.setBackgroundResource(R.drawable.blog_thrid_center_n);
                this.btnSohu.setBackgroundResource(R.drawable.blog_thrid_center_d);
                this.btnNetease.setBackgroundResource(R.drawable.blog_thrid_right_n);
                return;
            case R.id.third_netease /*2131361995*/:
                this.weiboId = SystemConfig.NETEASE_WEIBO;
                if (this.user != null) {
                    this.user = null;
                }
                new LoadInfo().execute("");
                this.btnSina.setBackgroundResource(R.drawable.blog_thrid_left_n);
                this.btnTencent.setBackgroundResource(R.drawable.blog_thrid_center_n);
                this.btnSohu.setBackgroundResource(R.drawable.blog_thrid_center_n);
                this.btnNetease.setBackgroundResource(R.drawable.blog_thrid_right_d);
                return;
            default:
                return;
        }
    }
}
