package cn.com.mzba.kdwb;

import android.app.Activity;
import android.view.Display;
import android.view.View;

public class AutoScreen {
    public static int ScreenOrient(Activity activity) {
        int orient = activity.getRequestedOrientation();
        if (orient == 0 || orient == 1) {
            return orient;
        }
        Display display = activity.getWindowManager().getDefaultDisplay();
        return display.getWidth() < display.getHeight() ? 1 : 0;
    }

    public static void AutoBackground(Activity activity, View view, int background_v, int background_h) {
        if (ScreenOrient(activity) == 1) {
            view.setBackgroundResource(background_v);
        } else {
            view.setBackgroundResource(background_h);
        }
    }
}
