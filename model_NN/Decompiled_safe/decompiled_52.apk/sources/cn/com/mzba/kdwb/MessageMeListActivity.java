package cn.com.mzba.kdwb;

import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import cn.com.mzba.db.UserInfo;
import cn.com.mzba.model.Status;
import cn.com.mzba.model.StatusListAdapter;
import cn.com.mzba.service.ServiceUtils;
import cn.com.mzba.service.StatusHttpUtils;
import cn.com.mzba.service.SystemConfig;
import java.util.List;

public class MessageMeListActivity extends ListActivity implements View.OnClickListener {
    /* access modifiers changed from: private */
    public StatusListAdapter adapter;
    private Button btnNetease;
    private Button btnSina;
    private Button btnSohu;
    private Button btnTencent;
    /* access modifiers changed from: private */
    public int count = 20;
    /* access modifiers changed from: private */
    public int page = 1;
    /* access modifiers changed from: private */
    public String sinceId;
    /* access modifiers changed from: private */
    public String weiboId = SystemConfig.SINA_WEIBO;
    /* access modifiers changed from: private */
    public List<Status> weiboList;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.messagemelist);
        this.btnSina = (Button) findViewById(R.id.third_sina);
        this.btnSina.setOnClickListener(this);
        this.btnTencent = (Button) findViewById(R.id.third_tencent);
        this.btnTencent.setOnClickListener(this);
        this.btnNetease = (Button) findViewById(R.id.third_netease);
        this.btnNetease.setOnClickListener(this);
        this.btnSohu = (Button) findViewById(R.id.third_sohu);
        this.btnSohu.setOnClickListener(this);
        this.btnSina.setBackgroundResource(R.drawable.blog_thrid_left_d);
        if (ServiceUtils.isConnectInternet(this)) {
            new LoadWeiBoInfo().execute("");
            return;
        }
        Toast.makeText(this, "网络出现异常", 1).show();
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        if (position == 0) {
            this.page = 1;
            new LoadWeiBoInfo().execute("");
        } else if (position == getListAdapter().getCount() - 1) {
            showDialog(0);
            new Thread() {
                public void run() {
                    MessageMeListActivity messageMeListActivity = MessageMeListActivity.this;
                    messageMeListActivity.page = messageMeListActivity.page + 1;
                    final List<Status> list = StatusHttpUtils.getMentionsStatus(MessageMeListActivity.this.weiboId, MessageMeListActivity.this.page, MessageMeListActivity.this.count, MessageMeListActivity.this.sinceId);
                    if (list == null || list.isEmpty()) {
                        MessageMeListActivity.this.removeDialog(0);
                        Toast.makeText(MessageMeListActivity.this, "加载数据失败", 1).show();
                        return;
                    }
                    MessageMeListActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            MessageMeListActivity.this.adapter.addMoreDatas(list);
                            MessageMeListActivity.this.sinceId = ((Status) MessageMeListActivity.this.weiboList.get(0)).getId();
                            MessageMeListActivity.this.removeDialog(0);
                        }
                    });
                }
            }.start();
        } else {
            String statusId = this.weiboList.get(position - 1).getId();
            Intent intent = new Intent();
            intent.setClass(this, DetailActivity.class);
            intent.putExtra("id", statusId);
            intent.putExtra(UserInfo.WEIBOID, this.weiboId);
            startActivity(intent);
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        ProgressDialog dialog = new ProgressDialog(getParent());
        dialog.setMessage("加载中...请稍候");
        dialog.setIndeterminate(true);
        dialog.setCancelable(true);
        return dialog;
    }

    public class LoadWeiBoInfo extends AsyncTask<String, String, String> {
        public LoadWeiBoInfo() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            MessageMeListActivity.this.weiboList = StatusHttpUtils.getMentionsStatus(MessageMeListActivity.this.weiboId, MessageMeListActivity.this.page, MessageMeListActivity.this.count, MessageMeListActivity.this.sinceId);
            if (MessageMeListActivity.this.weiboList == null || MessageMeListActivity.this.weiboList.isEmpty()) {
                return "";
            }
            MessageMeListActivity.this.sinceId = ((Status) MessageMeListActivity.this.weiboList.get(0)).getId();
            return "";
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            if (MessageMeListActivity.this.weiboList != null && !MessageMeListActivity.this.weiboList.isEmpty()) {
                MessageMeListActivity.this.adapter = new StatusListAdapter(MessageMeListActivity.this, MessageMeListActivity.this.weiboList);
            }
            MessageMeListActivity.this.setListAdapter(MessageMeListActivity.this.adapter);
            MessageMeListActivity.this.removeDialog(0);
            super.onPostExecute((Object) result);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            MessageMeListActivity.this.showDialog(0);
            super.onPreExecute();
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.third_sina /*2131361992*/:
                if (this.weiboList != null) {
                    this.weiboList.clear();
                }
                if (this.adapter != null) {
                    this.adapter.notifyDataSetChanged();
                }
                this.weiboId = SystemConfig.SINA_WEIBO;
                new LoadWeiBoInfo().execute("");
                this.btnSina.setBackgroundResource(R.drawable.blog_thrid_left_d);
                this.btnTencent.setBackgroundResource(R.drawable.blog_thrid_center_n);
                this.btnSohu.setBackgroundResource(R.drawable.blog_thrid_center_n);
                this.btnNetease.setBackgroundResource(R.drawable.blog_thrid_right_n);
                return;
            case R.id.third_tencent /*2131361993*/:
                if (this.weiboList != null) {
                    this.weiboList.clear();
                }
                if (this.adapter != null) {
                    this.adapter.notifyDataSetChanged();
                }
                this.weiboId = SystemConfig.TENCENT_WEIBO;
                new LoadWeiBoInfo().execute("");
                this.btnSina.setBackgroundResource(R.drawable.blog_thrid_left_n);
                this.btnTencent.setBackgroundResource(R.drawable.blog_thrid_center_d);
                this.btnSohu.setBackgroundResource(R.drawable.blog_thrid_center_n);
                this.btnNetease.setBackgroundResource(R.drawable.blog_thrid_right_n);
                return;
            case R.id.third_sohu /*2131361994*/:
                if (this.weiboList != null) {
                    this.weiboList.clear();
                }
                if (this.adapter != null) {
                    this.adapter.notifyDataSetChanged();
                }
                this.weiboId = SystemConfig.SOHU_WEIBO;
                new LoadWeiBoInfo().execute("");
                this.btnSina.setBackgroundResource(R.drawable.blog_thrid_left_n);
                this.btnTencent.setBackgroundResource(R.drawable.blog_thrid_center_n);
                this.btnSohu.setBackgroundResource(R.drawable.blog_thrid_center_d);
                this.btnNetease.setBackgroundResource(R.drawable.blog_thrid_right_n);
                return;
            case R.id.third_netease /*2131361995*/:
                if (this.weiboList != null) {
                    this.weiboList.clear();
                }
                if (this.adapter != null) {
                    this.adapter.notifyDataSetChanged();
                }
                this.weiboId = SystemConfig.NETEASE_WEIBO;
                new LoadWeiBoInfo().execute("");
                this.btnSina.setBackgroundResource(R.drawable.blog_thrid_left_n);
                this.btnTencent.setBackgroundResource(R.drawable.blog_thrid_center_n);
                this.btnSohu.setBackgroundResource(R.drawable.blog_thrid_center_n);
                this.btnNetease.setBackgroundResource(R.drawable.blog_thrid_right_d);
                return;
            default:
                return;
        }
    }
}
