package cn.com.mzba.kdwb;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import cn.com.mzba.db.DataHelper;
import cn.com.mzba.db.UserInfo;
import cn.com.mzba.oauth.OAuth;
import cn.com.mzba.service.SystemConfig;

public class AuthorizeActivity extends Activity {
    public static final String CALLBACKURL = "app:AuthorizeActivity";
    public static DataHelper dataHelper;

    /* renamed from: oauth  reason: collision with root package name */
    public static OAuth f0oauth;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String thridweibo = getIntent().getStringExtra("thrid_weibo");
        if (thridweibo == null) {
            return;
        }
        if (thridweibo.equals(SystemConfig.SINA_WEIBO)) {
            f0oauth = new OAuth(SystemConfig.SINA_WEIBO, SystemConfig.CONSUMERKEY_SINA, SystemConfig.CONSUMERSECRET_SINA);
            f0oauth.requestAccessToken(this, CALLBACKURL, SystemConfig.requestTokenEndpointUrl_sina, SystemConfig.accessTokenEndpointUrl_sina, SystemConfig.authorizationWebsiteUrl_sina);
        } else if (thridweibo.equals(SystemConfig.TENCENT_WEIBO)) {
            f0oauth = new OAuth(SystemConfig.TENCENT_WEIBO, SystemConfig.CONSUMERKEY_TENCENT, SystemConfig.CONSUMERSECRET_TENCENT);
            f0oauth.requestAccessToken(this, CALLBACKURL, SystemConfig.requestTokenEndpointUrl_tencent, SystemConfig.accessTokenEndpointUrl_tencent, SystemConfig.authorizationWebsiteUrl_tencent);
        } else if (thridweibo.equals(SystemConfig.SOHU_WEIBO)) {
            f0oauth = new OAuth(SystemConfig.SOHU_WEIBO, SystemConfig.CONSUMERKEY_SOHU, SystemConfig.CONSUMERSECRET_SOHU);
            f0oauth.requestAccessToken(this, CALLBACKURL, SystemConfig.requestTokenEndpointUrl_sohu, SystemConfig.accessTokenEndpointUrl_sohu, SystemConfig.authorizationWebsiteUrl_sohu);
        } else if (thridweibo.equals(SystemConfig.NETEASE_WEIBO)) {
            f0oauth = new OAuth(SystemConfig.NETEASE_WEIBO, SystemConfig.CONSUMERKEY_NETEASE, SystemConfig.CONSUMERSECRET_NETEASE);
            f0oauth.requestAccessToken(this, CALLBACKURL, SystemConfig.requestTokenEndpointUrl_netease, SystemConfig.accessTokenEndpointUrl_netease, SystemConfig.authorizationWebsiteUrl_netease);
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        UserInfo userinfo = (UserInfo) intent.getSerializableExtra("userinfo");
        if (userinfo != null) {
            dataHelper = new DataHelper(this);
            dataHelper.addUserInfo(userinfo);
        }
        Intent intentLogin = new Intent();
        intentLogin.setClass(this, MainActivity.class);
        startActivity(intentLogin);
        finish();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(OAuth.reciver);
        dataHelper.close();
    }
}
