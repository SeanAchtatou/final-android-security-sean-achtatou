package cn.com.mzba.kdwb;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import cn.com.mzba.db.ConfigHelper;
import cn.com.mzba.db.DataHelper;
import cn.com.mzba.db.UserInfo;
import cn.com.mzba.service.MyAction;
import cn.com.mzba.service.StatusHttpUtils;
import cn.com.mzba.service.SystemConfig;

public class DialogActivity extends ListActivity {
    /* access modifiers changed from: private */
    public ProgressDialog dialog;
    /* access modifiers changed from: private */
    public int[] dialogInfo;
    private boolean hasUser;
    /* access modifiers changed from: private */
    public DataHelper helper;
    /* access modifiers changed from: private */
    public String id;
    private String userId;
    /* access modifiers changed from: private */
    public String weiboId;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        String thridWeiboUserId;
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.dialoglist);
        this.helper = new DataHelper(this);
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = -1;
        lp.gravity = 80;
        getWindow().setAttributes(lp);
        Intent intent = getIntent();
        this.userId = intent.getStringExtra(UserInfo.USERID);
        this.id = intent.getStringExtra("id");
        this.weiboId = intent.getStringExtra(UserInfo.WEIBOID);
        if (this.weiboId.equals(SystemConfig.SINA_WEIBO)) {
            String thridWeiboUserId2 = ConfigHelper.USERINFO_SINA.getUserId();
            if (thridWeiboUserId2 != null) {
                if (this.userId.equals(thridWeiboUserId2)) {
                    this.hasUser = true;
                } else {
                    this.hasUser = false;
                }
            }
        } else if (this.weiboId.equals(SystemConfig.TENCENT_WEIBO)) {
            String thridWeiboUserId3 = ConfigHelper.USERINFO_TENCENT.getUserId();
            if (thridWeiboUserId3 != null) {
                if (this.userId.equals(thridWeiboUserId3)) {
                    this.hasUser = true;
                } else {
                    this.hasUser = false;
                }
            }
        } else if (this.weiboId.equals(SystemConfig.SOHU_WEIBO)) {
            String thridWeiboUserId4 = ConfigHelper.USERINFO_SOHU.getUserId();
            if (thridWeiboUserId4 != null) {
                if (this.userId.equals(thridWeiboUserId4)) {
                    this.hasUser = true;
                } else {
                    this.hasUser = false;
                }
            }
        } else if (this.weiboId.equals(SystemConfig.NETEASE_WEIBO) && (thridWeiboUserId = ConfigHelper.USERINFO_NETEASE.getUserId()) != null) {
            if (this.userId.equals(thridWeiboUserId)) {
                this.hasUser = true;
            } else {
                this.hasUser = false;
            }
        }
        if (this.hasUser) {
            this.dialogInfo = new int[]{R.string.blog_share, R.string.blog_report, R.string.blog_delete, R.string.blog_cancel};
        } else {
            this.dialogInfo = new int[]{R.string.blog_share, R.string.blog_report, R.string.blog_cancel};
        }
        setListAdapter(new DialogAdapter());
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id2) {
        super.onListItemClick(l, v, position, id2);
        if (this.hasUser) {
            switch (position) {
                case 0:
                case 1:
                default:
                    return;
                case 2:
                    delStatusDialog();
                    return;
                case 3:
                    finish();
                    return;
            }
        } else {
            switch (position) {
                case 0:
                case 1:
                default:
                    return;
                case 2:
                    finish();
                    return;
            }
        }
    }

    private void delStatusDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("提示");
        builder.setMessage("确定要删除该微博吗?");
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                DialogActivity.this.delStatus();
            }
        });
        builder.setNeutralButton("取消", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    /* access modifiers changed from: private */
    public void delStatus() {
        this.dialog = new ProgressDialog(this);
        this.dialog.setTitle("提示");
        this.dialog.setMessage("删除微博中，请稍候...");
        this.dialog.show();
        new Thread() {
            public void run() {
                String result = StatusHttpUtils.deleteStatus(DialogActivity.this.weiboId, DialogActivity.this.id);
                if (result == null) {
                    return;
                }
                if (result.equals(SystemConfig.SUCCESS)) {
                    DialogActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            DialogActivity.this.helper.deleteStatus(DialogActivity.this.id);
                            DialogActivity.this.finish();
                            Intent intent = new Intent();
                            intent.setAction(MyAction.DELETESTATUS);
                            DialogActivity.this.sendBroadcast(intent);
                            DialogActivity.this.dialog.dismiss();
                            Toast.makeText(DialogActivity.this, "删除微博成功", 1).show();
                        }
                    });
                } else {
                    DialogActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            DialogActivity.this.dialog.dismiss();
                            Toast.makeText(DialogActivity.this, "删除微博失败", 1).show();
                        }
                    });
                }
            }
        }.start();
    }

    public class DialogAdapter extends BaseAdapter {
        public DialogAdapter() {
        }

        public int getCount() {
            return DialogActivity.this.dialogInfo.length;
        }

        public Object getItem(int position) {
            return Integer.valueOf(DialogActivity.this.dialogInfo[position]);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            LinearLayout layout = new LinearLayout(DialogActivity.this);
            layout.setOrientation(0);
            TextView textView = new TextView(DialogActivity.this);
            textView.setPadding(5, 20, 10, 20);
            textView.setTextSize(18.0f);
            textView.setTextColor((int) R.color.black);
            textView.setText(DialogActivity.this.getResources().getString(DialogActivity.this.dialogInfo[position]));
            layout.addView(textView);
            return layout;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.helper.close();
    }
}
