package cn.com.mzba.kdwb;

import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import cn.com.mzba.db.UserInfo;
import cn.com.mzba.model.Topic;
import cn.com.mzba.service.CommentTopicHttpUtils;
import cn.com.mzba.service.ServiceUtils;
import java.util.List;

public class TopicActivity extends ListActivity implements View.OnClickListener {
    /* access modifiers changed from: private */
    public TopicAdapter adapter;
    private ImageButton btnBack;
    private ImageButton btnHome;
    /* access modifiers changed from: private */
    public int count = 20;
    /* access modifiers changed from: private */
    public int page = 1;
    /* access modifiers changed from: private */
    public List<Topic> topicList;
    /* access modifiers changed from: private */
    public String userId;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.topic);
        this.btnBack = (ImageButton) findViewById(R.id.topic_back);
        this.btnBack.setOnClickListener(this);
        this.btnHome = (ImageButton) findViewById(R.id.topic_home);
        this.btnHome.setOnClickListener(this);
        this.userId = getIntent().getStringExtra(UserInfo.USERID);
        if (ServiceUtils.isConnectInternet(this)) {
            new LoadWeiBoInfo().execute("");
            return;
        }
        Toast.makeText(this, "网络异常！", 1).show();
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        if (position == 0) {
            refresh();
        } else if (position == getListAdapter().getCount() - 1) {
            loadMoreData();
        } else {
            Intent intent = new Intent();
            intent.setClass(this, TopicListActivity.class);
            intent.putExtra("topicId", this.topicList.get(position - 1).getId());
            intent.putExtra("topicName", this.topicList.get(position - 1).getHotword());
            startActivity(intent);
        }
    }

    private void refresh() {
        this.page = 1;
        showDialog(0);
        new Thread() {
            public void run() {
                TopicActivity.this.topicList = CommentTopicHttpUtils.getTopicByUserId(TopicActivity.this.userId, TopicActivity.this.page, TopicActivity.this.count);
                if (TopicActivity.this.topicList != null && !TopicActivity.this.topicList.isEmpty()) {
                    TopicActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            TopicActivity.this.adapter.notifyDataSetChanged();
                            TopicActivity.this.removeDialog(0);
                        }
                    });
                }
            }
        }.start();
    }

    private void loadMoreData() {
        showDialog(0);
        new Thread() {
            public void run() {
                TopicActivity topicActivity = TopicActivity.this;
                topicActivity.page = topicActivity.page + 1;
                final List<Topic> list = CommentTopicHttpUtils.getTopicByUserId(TopicActivity.this.userId, TopicActivity.this.page, TopicActivity.this.count);
                if (list == null || list.isEmpty()) {
                    TopicActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            TopicActivity.this.removeDialog(0);
                            Toast.makeText(TopicActivity.this, "加载数据失败", 1).show();
                        }
                    });
                } else {
                    TopicActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            TopicActivity.this.topicList.addAll(list);
                            TopicActivity.this.adapter.notifyDataSetChanged();
                            TopicActivity.this.removeDialog(0);
                        }
                    });
                }
            }
        }.start();
    }

    public class TopicAdapter extends BaseAdapter {
        public TopicAdapter() {
        }

        public int getCount() {
            return TopicActivity.this.topicList.size() + 2;
        }

        public Object getItem(int position) {
            return TopicActivity.this.topicList.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        /* Debug info: failed to restart local var, previous not found, register: 7 */
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;
            if (position == 0) {
                return LayoutInflater.from(TopicActivity.this.getApplicationContext()).inflate((int) R.layout.listheader, (ViewGroup) null);
            }
            if (position == getCount() - 1) {
                return LayoutInflater.from(TopicActivity.this.getApplicationContext()).inflate((int) R.layout.listfooter, (ViewGroup) null);
            }
            if (convertView == null || convertView.getId() != R.id.topic_layout) {
                view = LayoutInflater.from(TopicActivity.this.getApplicationContext()).inflate((int) R.layout.topicitem, (ViewGroup) null);
                ((TextView) view.findViewById(R.id.topic_textview)).setText(((Topic) TopicActivity.this.topicList.get(position - 1)).getHotword());
            } else {
                view = convertView;
            }
            return view;
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("加载中...请稍候");
        dialog.setIndeterminate(true);
        dialog.setCancelable(true);
        return dialog;
    }

    public class LoadWeiBoInfo extends AsyncTask<String, String, String> {
        public LoadWeiBoInfo() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            TopicActivity.this.topicList = CommentTopicHttpUtils.getTopicByUserId(TopicActivity.this.userId, TopicActivity.this.page, TopicActivity.this.count);
            return "";
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            if (TopicActivity.this.topicList != null && !TopicActivity.this.topicList.isEmpty()) {
                TopicActivity.this.adapter = new TopicAdapter();
                TopicActivity.this.setListAdapter(TopicActivity.this.adapter);
            }
            TopicActivity.this.removeDialog(0);
            super.onPostExecute((Object) result);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            TopicActivity.this.showDialog(0);
            super.onPreExecute();
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.topic_back:
                finish();
                return;
            case R.id.topic_name:
            default:
                return;
            case R.id.topic_home:
                startActivity(new Intent(this, HomeActivity.class));
                return;
        }
    }
}
