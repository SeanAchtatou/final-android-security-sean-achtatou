package cn.com.mzba.kdwb;

import android.app.Activity;
import android.content.Intent;
import android.hardware.Camera;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageButton;

public class PhotoActivity extends Activity implements SurfaceHolder.Callback, View.OnClickListener {
    private ImageButton backButton = null;
    private Camera camera = null;
    /* access modifiers changed from: private */
    public boolean isView = false;
    Camera.PictureCallback myjpegCallback = new Camera.PictureCallback() {
        public void onPictureTaken(byte[] data, Camera camera) {
            Intent intent = new Intent();
            Bundle bundle = new Bundle();
            bundle.putByteArray("image", data);
            intent.putExtras(bundle);
            PhotoActivity.this.setResult(-1, intent);
            PhotoActivity.this.isView = false;
            camera.stopPreview();
            camera.release();
            PhotoActivity.this.finish();
        }
    };
    private ImageButton photoButton = null;
    Camera.PictureCallback pictureCallback = new Camera.PictureCallback() {
        public void onPictureTaken(byte[] data, Camera camera) {
        }
    };
    Camera.ShutterCallback shutterCallback = new Camera.ShutterCallback() {
        public void onShutter() {
        }
    };
    private SurfaceHolder surfaceHolder = null;
    private SurfaceView surfaceView = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.photo);
        this.photoButton = (ImageButton) findViewById(R.id.photoButton);
        this.backButton = (ImageButton) findViewById(R.id.backButton);
        this.photoButton.setOnClickListener(this);
        this.backButton.setOnClickListener(this);
        this.surfaceView = (SurfaceView) findViewById(R.id.mySurfaceView);
        this.surfaceHolder = this.surfaceView.getHolder();
        this.surfaceHolder.addCallback(this);
        this.surfaceHolder.setType(3);
    }

    public void initCamera() {
        if (!this.isView) {
            this.camera = Camera.open();
        }
        if ((this.camera != null) && (!this.isView)) {
            try {
                Camera.Parameters myParameters = this.camera.getParameters();
                myParameters.setPictureFormat(256);
                myParameters.setPreviewSize(320, 445);
                this.camera.setParameters(myParameters);
                this.camera.setPreviewDisplay(this.surfaceHolder);
                this.camera.startPreview();
            } catch (Exception e) {
                e.printStackTrace();
            }
            this.isView = true;
        }
    }

    public void onClick(View v) {
        if (v == this.photoButton) {
            this.camera.takePicture(this.shutterCallback, this.pictureCallback, this.myjpegCallback);
        } else if (v == this.backButton) {
            this.isView = false;
            if (this.camera != null) {
                this.camera.stopPreview();
                this.camera.release();
                this.camera = null;
            }
            finish();
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    public void surfaceCreated(SurfaceHolder holder) {
        initCamera();
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        this.isView = false;
        if (this.camera != null) {
            this.camera.stopPreview();
            this.camera.release();
            this.camera = null;
        }
    }
}
