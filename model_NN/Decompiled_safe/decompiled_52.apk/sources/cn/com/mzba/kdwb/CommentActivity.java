package cn.com.mzba.kdwb;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import cn.com.mzba.db.UserInfo;
import cn.com.mzba.model.Comment;
import cn.com.mzba.model.CommentListAdapter;
import cn.com.mzba.service.CommentTopicHttpUtils;
import cn.com.mzba.service.ServiceUtils;
import cn.com.mzba.service.SystemConfig;
import java.util.List;

public class CommentActivity extends ListActivity {
    /* access modifiers changed from: private */
    public CommentListAdapter adapter;
    private Button btnBack;
    private Button btnComment;
    /* access modifiers changed from: private */
    public int count = 20;
    /* access modifiers changed from: private */
    public String id;
    /* access modifiers changed from: private */
    public List<Comment> list;
    /* access modifiers changed from: private */
    public int page = 1;
    /* access modifiers changed from: private */
    public ProgressDialog progress;
    /* access modifiers changed from: private */
    public String sinceId;
    /* access modifiers changed from: private */
    public String userId;
    /* access modifiers changed from: private */
    public String weiboId;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.commentlist);
        this.btnComment = (Button) findViewById(R.id.comment);
        this.btnComment.setOnClickListener(new ButtonOnClickListener());
        this.btnBack = (Button) findViewById(R.id.comment_back);
        this.btnBack.setOnClickListener(new ButtonOnClickListener());
        Intent intent = getIntent();
        this.id = intent.getStringExtra("id");
        this.weiboId = intent.getStringExtra(UserInfo.WEIBOID);
        this.userId = intent.getStringExtra(UserInfo.USERID);
        if (ServiceUtils.isConnectInternet(this)) {
            new CommentListLoader().execute("");
            return;
        }
        Toast.makeText(this, "网络出现异常", 1).show();
    }

    private void refresh() {
        this.page = 1;
        showDialog(0);
        new Thread() {
            public void run() {
                final List<Comment> list = CommentTopicHttpUtils.getCommentListById(CommentActivity.this.weiboId, CommentActivity.this.id, CommentActivity.this.page, CommentActivity.this.count, null);
                if (list == null || list.isEmpty()) {
                    CommentActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            CommentActivity.this.removeDialog(0);
                            Toast.makeText(CommentActivity.this, "加载数据失败", 1).show();
                        }
                    });
                } else {
                    CommentActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            CommentActivity.this.adapter.addMoreDatas(list);
                            CommentActivity.this.removeDialog(0);
                        }
                    });
                }
            }
        }.start();
    }

    private void loadMoreData() {
        showDialog(0);
        new Thread() {
            public void run() {
                CommentActivity commentActivity = CommentActivity.this;
                commentActivity.page = commentActivity.page + 1;
                final List<Comment> list = CommentTopicHttpUtils.getCommentListById(CommentActivity.this.weiboId, CommentActivity.this.id, CommentActivity.this.page, CommentActivity.this.count, CommentActivity.this.sinceId);
                if (list == null || list.isEmpty()) {
                    CommentActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            CommentActivity.this.removeDialog(0);
                            Toast.makeText(CommentActivity.this, "加载数据失败", 1).show();
                        }
                    });
                } else {
                    CommentActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            CommentActivity.this.adapter.addMoreDatas(list);
                            CommentActivity.this.removeDialog(0);
                        }
                    });
                }
            }
        }.start();
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id2) {
        super.onListItemClick(l, v, position, id2);
        if (position == 0) {
            refresh();
        } else if (position == getListAdapter().getCount() - 1) {
            loadMoreData();
        } else {
            doLongClick(this.list.get(position - 1).getId(), this.list.get(position - 1).getUser().getId());
        }
    }

    public class ButtonOnClickListener implements View.OnClickListener {
        public ButtonOnClickListener() {
        }

        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.comment_back /*2131361837*/:
                    CommentActivity.this.finish();
                    return;
                case R.id.comment /*2131361838*/:
                    Intent intent = new Intent();
                    intent.setClass(CommentActivity.this, NewCommentActivity.class);
                    Bundle extras = new Bundle();
                    extras.putString("id", CommentActivity.this.id);
                    extras.putString(UserInfo.WEIBOID, CommentActivity.this.weiboId);
                    extras.putString("key", "comment");
                    intent.putExtras(extras);
                    CommentActivity.this.startActivity(intent);
                    CommentActivity.this.finish();
                    return;
                default:
                    return;
            }
        }
    }

    private void doLongClick(final String commentid, final String commentUserId) {
        Context dialogContext = new ContextThemeWrapper(this, 16973836);
        ListAdapter adapter2 = new ArrayAdapter(dialogContext, 17367043, this.userId.equals(commentUserId) ? new String[]{getString(R.string.replay_comment), getString(R.string.delete_comment), getString(R.string.userinfo)} : new String[]{getString(R.string.replay_comment), getString(R.string.userinfo)});
        AlertDialog.Builder builder = new AlertDialog.Builder(dialogContext);
        builder.setTitle((int) R.string.choice_do);
        builder.setSingleChoiceItems(adapter2, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (CommentActivity.this.userId.equals(commentUserId)) {
                    switch (which) {
                        case 0:
                            CommentActivity.this.replayComment(commentid);
                            return;
                        case 1:
                            CommentActivity.this.deleteComment(commentid);
                            return;
                        case 2:
                            CommentActivity.this.showUserInfo(commentUserId);
                            return;
                        default:
                            return;
                    }
                } else {
                    switch (which) {
                        case 0:
                            CommentActivity.this.replayComment(commentid);
                            return;
                        case 1:
                            CommentActivity.this.showUserInfo(commentUserId);
                            return;
                        default:
                            return;
                    }
                }
            }
        });
        builder.setNegativeButton("返回", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    public void deleteComment(final String commentid) {
        this.progress = new ProgressDialog(this);
        this.progress.setMessage("删除评论中...请稍候");
        this.progress.setCancelable(true);
        this.progress.show();
        new Thread() {
            public void run() {
                String result = CommentTopicHttpUtils.deleteComment(CommentActivity.this.weiboId, commentid);
                if (result == null) {
                    return;
                }
                if (result.equals(SystemConfig.SUCCESS)) {
                    CommentActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            CommentActivity.this.adapter.notifyDataSetChanged();
                            CommentActivity.this.progress.dismiss();
                            Toast.makeText(CommentActivity.this, "删除评论成功", 1).show();
                        }
                    });
                } else {
                    CommentActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            CommentActivity.this.progress.dismiss();
                            Toast.makeText(CommentActivity.this, "删除评论失败", 1).show();
                        }
                    });
                }
            }
        }.start();
    }

    public void replayComment(String commentid) {
        Intent intent = new Intent();
        intent.setClass(this, NewCommentActivity.class);
        Bundle extras = new Bundle();
        extras.putString("key", "comment");
        extras.putString("id", this.id);
        extras.putString("cid", commentid);
        extras.putString(UserInfo.WEIBOID, this.weiboId);
        intent.putExtras(extras);
        startActivity(intent);
    }

    public void showUserInfo(String commentUserId) {
        Intent intent = new Intent();
        intent.setClass(this, UserInfoActivity.class);
        intent.putExtra(UserInfo.WEIBOID, this.weiboId);
        intent.putExtra(UserInfo.USERID, commentUserId);
        startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id2) {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("加载数据中...请稍候");
        dialog.setIndeterminate(true);
        dialog.setCancelable(true);
        return dialog;
    }

    public class CommentListLoader extends AsyncTask<String, String, String> {
        public CommentListLoader() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            CommentActivity.this.list = CommentTopicHttpUtils.getCommentListById(CommentActivity.this.weiboId, CommentActivity.this.id, CommentActivity.this.page, CommentActivity.this.count, null);
            if (CommentActivity.this.list == null || CommentActivity.this.list.isEmpty()) {
                return "";
            }
            CommentActivity.this.sinceId = ((Comment) CommentActivity.this.list.get(0)).getId();
            return "";
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            if (CommentActivity.this.list != null && !CommentActivity.this.list.isEmpty()) {
                CommentActivity.this.adapter = new CommentListAdapter(CommentActivity.this, CommentActivity.this.list);
                CommentActivity.this.setListAdapter(CommentActivity.this.adapter);
            }
            CommentActivity.this.removeDialog(0);
            super.onPostExecute((Object) result);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            CommentActivity.this.showDialog(0);
            super.onPreExecute();
        }
    }
}
