package cn.com.mzba.kdwb;

import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import cn.com.mzba.model.Status;
import cn.com.mzba.model.StatusListAdapter;
import cn.com.mzba.service.CommentTopicHttpUtils;
import cn.com.mzba.service.ServiceUtils;
import cn.com.mzba.service.SystemConfig;
import java.util.List;

public class TopicListActivity extends ListActivity implements View.OnClickListener {
    /* access modifiers changed from: private */
    public StatusListAdapter adapter;
    private ImageButton btnBack;
    private ImageButton btnHome;
    /* access modifiers changed from: private */
    public boolean isFollow = true;
    /* access modifiers changed from: private */
    public ImageView ivTopic;
    private LinearLayout newTopicLayout;
    /* access modifiers changed from: private */
    public List<Status> statusList;
    /* access modifiers changed from: private */
    public String topicId;
    private LinearLayout topicLayout;
    /* access modifiers changed from: private */
    public String topicName;
    /* access modifiers changed from: private */
    public TextView tvTopic;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.topiclist);
        this.btnBack = (ImageButton) findViewById(R.id.topiclist_back);
        this.btnBack.setOnClickListener(this);
        this.btnHome = (ImageButton) findViewById(R.id.topiclist_home);
        this.btnHome.setOnClickListener(this);
        this.topicLayout = (LinearLayout) findViewById(R.id.blog_topic_list_topic_layout);
        this.topicLayout.setOnClickListener(this);
        this.topicLayout.setOnTouchListener(new OnTouchClickListener());
        this.newTopicLayout = (LinearLayout) findViewById(R.id.blog_topic_list_new_layout);
        this.newTopicLayout.setOnClickListener(this);
        this.newTopicLayout.setOnTouchListener(new OnTouchClickListener());
        this.tvTopic = (TextView) findViewById(R.id.topiclist_topic_tv);
        this.ivTopic = (ImageView) findViewById(R.id.blog_topic_list_topic_iv);
        this.topicName = getIntent().getStringExtra("topicName");
        this.topicId = getIntent().getStringExtra("topicId");
        if (this.topicName == null) {
            return;
        }
        if (ServiceUtils.isConnectInternet(this)) {
            new LoadWeiBoInfo().execute("");
            return;
        }
        Toast.makeText(this, "网络异常！", 1).show();
    }

    public class OnTouchClickListener implements View.OnTouchListener {
        public OnTouchClickListener() {
        }

        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == 1) {
                v.setBackgroundResource(0);
            } else if (event.getAction() == 0) {
                v.setBackgroundResource(R.drawable.detail_btn_bg_selected);
            }
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        if (position == 0) {
            refresh();
        } else if (position == getListAdapter().getCount() - 1) {
            loadMoreData();
        } else {
            Intent intent = new Intent();
            intent.setClass(this, DetailActivity.class);
            Bundle extras = new Bundle();
            extras.putString("key", this.statusList.get(position - 1).getId());
            intent.putExtras(extras);
            startActivity(intent);
        }
    }

    private void refresh() {
        showDialog(0);
        new Thread() {
            public void run() {
                TopicListActivity.this.statusList = CommentTopicHttpUtils.getStaticByTopicName(TopicListActivity.this.topicName);
                if (TopicListActivity.this.statusList != null && !TopicListActivity.this.statusList.isEmpty()) {
                    TopicListActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            TopicListActivity.this.adapter.notifyDataSetChanged();
                            TopicListActivity.this.removeDialog(0);
                        }
                    });
                }
            }
        }.start();
    }

    private void loadMoreData() {
        Toast.makeText(this, "没有更多数据了！", 1).show();
    }

    private void followTopic() {
        showDialog(0);
        new Thread() {
            public void run() {
                if (CommentTopicHttpUtils.followTopic(TopicListActivity.this.topicName).equals(SystemConfig.SUCCESS)) {
                    TopicListActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            TopicListActivity.this.tvTopic.setText("取消此关注");
                            TopicListActivity.this.ivTopic.setBackgroundResource(R.drawable.detail_fav_normal);
                            TopicListActivity.this.isFollow = true;
                            TopicListActivity.this.removeDialog(0);
                        }
                    });
                } else {
                    TopicListActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(TopicListActivity.this, "关注该话题失败！", 1).show();
                            TopicListActivity.this.removeDialog(0);
                        }
                    });
                }
            }
        }.start();
    }

    private void destroyTopic() {
        showDialog(0);
        new Thread() {
            public void run() {
                if (CommentTopicHttpUtils.destroyTopic(TopicListActivity.this.topicId).equals(SystemConfig.SUCCESS)) {
                    TopicListActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            TopicListActivity.this.tvTopic.setText("关注该话题");
                            TopicListActivity.this.ivTopic.setBackgroundResource(R.drawable.detail_fav_highlight_normal);
                            TopicListActivity.this.isFollow = false;
                            TopicListActivity.this.removeDialog(0);
                        }
                    });
                } else {
                    TopicListActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(TopicListActivity.this, "取消关注失败！", 1).show();
                            TopicListActivity.this.removeDialog(0);
                        }
                    });
                }
            }
        }.start();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("加载中...请稍候");
        dialog.setIndeterminate(true);
        dialog.setCancelable(true);
        return dialog;
    }

    public class LoadWeiBoInfo extends AsyncTask<String, String, String> {
        public LoadWeiBoInfo() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            TopicListActivity.this.statusList = CommentTopicHttpUtils.getStaticByTopicName(TopicListActivity.this.topicName);
            return "";
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            if (TopicListActivity.this.statusList != null && !TopicListActivity.this.statusList.isEmpty()) {
                TopicListActivity.this.adapter = new StatusListAdapter(TopicListActivity.this, TopicListActivity.this.statusList);
                TopicListActivity.this.setListAdapter(TopicListActivity.this.adapter);
            }
            TopicListActivity.this.removeDialog(0);
            super.onPostExecute((Object) result);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            TopicListActivity.this.showDialog(0);
            super.onPreExecute();
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.topiclist_back:
                finish();
                return;
            case R.id.topiclist_name:
            case R.id.topiclist_bottom_layout:
            case R.id.blog_topic_list_topic_iv:
            case R.id.topiclist_topic_tv:
            default:
                return;
            case R.id.topiclist_home:
                startActivity(new Intent(this, HomeActivity.class));
                return;
            case R.id.blog_topic_list_topic_layout:
                if (this.isFollow) {
                    destroyTopic();
                    return;
                } else {
                    followTopic();
                    return;
                }
            case R.id.blog_topic_list_new_layout:
                startActivity(new Intent(this, NewWeiboActivity.class));
                return;
        }
    }
}
