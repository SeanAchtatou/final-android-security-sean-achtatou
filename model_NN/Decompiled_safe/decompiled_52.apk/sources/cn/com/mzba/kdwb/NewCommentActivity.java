package cn.com.mzba.kdwb;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import cn.com.mzba.db.UserInfo;
import cn.com.mzba.service.CommentTopicHttpUtils;
import cn.com.mzba.service.MyAction;
import cn.com.mzba.service.ServiceUtils;
import cn.com.mzba.service.StatusHttpUtils;
import cn.com.mzba.service.SystemConfig;
import java.util.ArrayList;
import java.util.List;

public class NewCommentActivity extends Activity {
    private Button btnBack;
    /* access modifiers changed from: private */
    public Button btnSend;
    private CheckBox cb;
    private String cid;
    private LinearLayout clearLayout;
    private String id;
    /* access modifiers changed from: private */
    public String key;
    /* access modifiers changed from: private */
    public String sendResult;
    /* access modifiers changed from: private */
    public EditText text;
    /* access modifiers changed from: private */
    public int textCount = 0;
    private TextView title;
    /* access modifiers changed from: private */
    public TextView tvTextSize;
    private String weiboId;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.commentweibo);
        this.tvTextSize = (TextView) findViewById(R.id.comment_textSize);
        this.clearLayout = (LinearLayout) findViewById(R.id.comment_clear_layout);
        this.clearLayout.setOnClickListener(new ButtonOnClickListener());
        this.btnBack = (Button) findViewById(R.id.commentweibo_back);
        this.btnBack.setOnClickListener(new ButtonOnClickListener());
        this.btnSend = (Button) findViewById(R.id.commentweibo_send);
        this.btnSend.setOnClickListener(new ButtonOnClickListener());
        this.text = (EditText) findViewById(R.id.commentweibo_content);
        this.cb = (CheckBox) findViewById(R.id.commentweibo_checkbox);
        this.title = (TextView) findViewById(R.id.commentweibo_title);
        this.text.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                NewCommentActivity.this.textCount = (NewCommentActivity.this.textCount + count) - before;
                if (NewCommentActivity.this.textCount <= 140) {
                    NewCommentActivity.this.btnSend.setClickable(true);
                    NewCommentActivity.this.tvTextSize.setText(new StringBuilder(String.valueOf(140 - NewCommentActivity.this.textCount)).toString());
                    NewCommentActivity.this.btnSend.setEnabled(true);
                    return;
                }
                NewCommentActivity.this.btnSend.setClickable(false);
                NewCommentActivity.this.tvTextSize.setText("0");
                NewCommentActivity.this.text.setFilters(new InputFilter[]{new InputFilter() {
                    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                        if (source.length() < 1) {
                            return dest.subSequence(dstart, dend);
                        }
                        return "";
                    }
                }});
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
            }
        });
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            this.id = bundle.getString("id");
            this.key = bundle.getString("key");
            this.cid = bundle.getString("cid");
            this.weiboId = bundle.getString(UserInfo.WEIBOID);
        }
        if (this.key.equals("comment")) {
            this.cb.setText("同时发一条微博");
            this.title.setText("评论微博");
        } else if (this.key.equals(MyAction.REDIRECT)) {
            this.cb.setText("同时作为评论发布");
            this.title.setText("转发微博");
        }
    }

    public class ButtonOnClickListener implements View.OnClickListener {
        public ButtonOnClickListener() {
        }

        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.commentweibo_back /*2131361844*/:
                    NewCommentActivity.this.finish();
                    return;
                case R.id.commentweibo_send /*2131361846*/:
                    if (ServiceUtils.isConnectInternet(NewCommentActivity.this)) {
                        new CommentBlog().execute("");
                        return;
                    }
                    Toast.makeText(NewCommentActivity.this, "网络出现异常", 1).show();
                    return;
                case R.id.clear_layout /*2131361962*/:
                    AlertDialog.Builder builder = new AlertDialog.Builder(NewCommentActivity.this);
                    builder.setTitle("提示");
                    builder.setMessage("确定要清空所有内容吗?");
                    builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            NewCommentActivity.this.text.setText("");
                        }
                    });
                    builder.setNeutralButton("取消", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder.show();
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void commentWeibo() {
        String status = this.text.getText().toString();
        this.sendResult = CommentTopicHttpUtils.comment(this.weiboId, this.id, this.cid, status);
        if (this.cb.isChecked()) {
            List<String> weiboIds = new ArrayList<>();
            weiboIds.add(this.weiboId);
            StatusHttpUtils.updateStatus(weiboIds, status);
        }
    }

    /* access modifiers changed from: private */
    public void redirectWeibo() {
        String status = this.text.getText().toString();
        boolean isComment = false;
        if (this.cb.isChecked()) {
            isComment = true;
        }
        this.sendResult = CommentTopicHttpUtils.repost(this.weiboId, this.id, status, isComment);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id2) {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("发送微博中...请稍候");
        dialog.setIndeterminate(true);
        dialog.setCancelable(true);
        return dialog;
    }

    public class CommentBlog extends AsyncTask<String, String, String> {
        public CommentBlog() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            if (NewCommentActivity.this.key.equals("comment")) {
                NewCommentActivity.this.commentWeibo();
                return "";
            } else if (!NewCommentActivity.this.key.equals(MyAction.REDIRECT)) {
                return "";
            } else {
                NewCommentActivity.this.redirectWeibo();
                return "";
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            NewCommentActivity.this.removeDialog(0);
            if (NewCommentActivity.this.sendResult != null) {
                if (NewCommentActivity.this.sendResult.equals(SystemConfig.SUCCESS)) {
                    Toast.makeText(NewCommentActivity.this, "发送微博成功", 1).show();
                } else {
                    Toast.makeText(NewCommentActivity.this, "发送微博失败", 1).show();
                }
            }
            super.onPostExecute((Object) result);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            NewCommentActivity.this.showDialog(0);
            super.onPreExecute();
        }
    }
}
