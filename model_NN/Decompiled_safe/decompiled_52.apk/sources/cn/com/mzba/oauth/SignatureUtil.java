package cn.com.mzba.oauth;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SignatureUtil {
    public static final String ENCODING = "UTF-8";

    public static String generateSignature(String method, String url, List<Parameter> params, String consumerSecret, String tokenSecret) throws Exception {
        String oauthKey;
        String signatureBase = generateSignatureBase(method, url, params);
        if (tokenSecret == null || tokenSecret.equals("")) {
            oauthKey = String.valueOf(encode(consumerSecret)) + "&";
        } else {
            oauthKey = String.valueOf(encode(consumerSecret)) + "&" + encode(tokenSecret);
        }
        return Base64.encode(HMAC_SHA1.HmacSHA1Encrypt(signatureBase, oauthKey));
    }

    public static String generateSignatureBase(String method, String url, List<Parameter> params) {
        StringBuilder urlBuilder = new StringBuilder();
        urlBuilder.append(String.valueOf(method.toUpperCase()) + "&");
        urlBuilder.append(String.valueOf(encode(url.toLowerCase())) + "&");
        Collections.sort(params);
        for (Parameter param : params) {
            String name = encode(param.getName());
            String value = encode(param.getValue());
            urlBuilder.append(name);
            urlBuilder.append("%3D");
            urlBuilder.append(value);
            urlBuilder.append("%26");
        }
        urlBuilder.delete(urlBuilder.length() - 3, urlBuilder.length());
        return urlBuilder.toString();
    }

    public static String encode(String s) {
        boolean z;
        boolean z2;
        if (s == null) {
            return "";
        }
        try {
            String encoded = URLEncoder.encode(s, "UTF-8");
            StringBuilder sBuilder = new StringBuilder();
            int i = 0;
            while (i < encoded.length()) {
                char c = encoded.charAt(i);
                if (c == '+') {
                    sBuilder.append("%20");
                } else if (c == '*') {
                    sBuilder.append("%2A");
                } else {
                    if (c == '%' && i + 1 < encoded.length()) {
                        if (i + 2 < encoded.length()) {
                            z = true;
                        } else {
                            z = false;
                        }
                        if (encoded.charAt(i + 1) == '7') {
                            z2 = true;
                        } else {
                            z2 = false;
                        }
                        if ((z && z2) && encoded.charAt(i + 2) == 'E') {
                            sBuilder.append("~");
                            i += 2;
                        }
                    }
                    sBuilder.append(c);
                }
                i++;
            }
            return sBuilder.toString();
        } catch (UnsupportedEncodingException e) {
            UnsupportedEncodingException e2 = e;
            throw new RuntimeException(e2.getMessage(), e2);
        }
    }

    public static String decode(String value) {
        int index;
        int nCount = 0;
        int i = 0;
        while (i < value.length()) {
            if (value.charAt(i) == '%') {
                i += 2;
            }
            nCount++;
            i++;
        }
        byte[] sb = new byte[nCount];
        int i2 = 0;
        int index2 = 0;
        while (i2 < value.length()) {
            if (value.charAt(i2) != '%') {
                index = index2 + 1;
                sb[index2] = (byte) value.charAt(i2);
            } else {
                StringBuilder sChar = new StringBuilder();
                sChar.append(value.charAt(i2 + 1));
                sChar.append(value.charAt(i2 + 2));
                index = index2 + 1;
                sb[index2] = Integer.valueOf(sChar.toString(), 16).byteValue();
                i2 += 2;
            }
            index2 = index;
            i2++;
        }
        try {
            return new String(sb, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String generateTimeStamp() {
        return String.valueOf(System.currentTimeMillis() / 1000);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v1, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v3, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v4, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String generateNonce(boolean r10) {
        /*
            java.util.Random r6 = new java.util.Random
            r6.<init>()
            r8 = 9876599(0x96b477, float:1.3840063E-38)
            int r8 = r6.nextInt(r8)
            r9 = 123400(0x1e208, float:1.7292E-40)
            int r8 = r8 + r9
            java.lang.String r7 = java.lang.String.valueOf(r8)
            if (r10 == 0) goto L_0x0036
            java.lang.String r8 = "MD5"
            java.security.MessageDigest r4 = java.security.MessageDigest.getInstance(r8)     // Catch:{ NoSuchAlgorithmException -> 0x0050 }
            byte[] r8 = r7.getBytes()     // Catch:{ NoSuchAlgorithmException -> 0x0050 }
            r4.update(r8)     // Catch:{ NoSuchAlgorithmException -> 0x0050 }
            byte[] r0 = r4.digest()     // Catch:{ NoSuchAlgorithmException -> 0x0050 }
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ NoSuchAlgorithmException -> 0x0050 }
            java.lang.String r8 = ""
            r1.<init>(r8)     // Catch:{ NoSuchAlgorithmException -> 0x0050 }
            r5 = 0
        L_0x002f:
            int r8 = r0.length     // Catch:{ NoSuchAlgorithmException -> 0x0050 }
            if (r5 < r8) goto L_0x0037
            java.lang.String r7 = r1.toString()     // Catch:{ NoSuchAlgorithmException -> 0x0050 }
        L_0x0036:
            return r7
        L_0x0037:
            byte r3 = r0[r5]     // Catch:{ NoSuchAlgorithmException -> 0x0050 }
            if (r3 >= 0) goto L_0x003d
            int r3 = r3 + 256
        L_0x003d:
            r8 = 16
            if (r3 >= r8) goto L_0x0046
            java.lang.String r8 = "0"
            r1.append(r8)     // Catch:{ NoSuchAlgorithmException -> 0x0050 }
        L_0x0046:
            java.lang.String r8 = java.lang.Integer.toHexString(r3)     // Catch:{ NoSuchAlgorithmException -> 0x0050 }
            r1.append(r8)     // Catch:{ NoSuchAlgorithmException -> 0x0050 }
            int r5 = r5 + 1
            goto L_0x002f
        L_0x0050:
            r8 = move-exception
            r2 = r8
            r2.printStackTrace()
            goto L_0x0036
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.com.mzba.oauth.SignatureUtil.generateNonce(boolean):java.lang.String");
    }

    public static List<Parameter> getQueryParameters(String queryString) {
        if (queryString.startsWith("?")) {
            queryString = queryString.substring(1);
        }
        List<Parameter> result = new ArrayList<>();
        if (queryString != null && !queryString.equals("")) {
            for (String s : queryString.split("&")) {
                if (s != null && !s.equals("") && s.indexOf(61) > -1) {
                    String[] temp = s.split("=");
                    result.add(new Parameter(temp[0], temp[1]));
                }
            }
        }
        return result;
    }
}
