package cn.com.mzba.oauth;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class HMAC_SHA1 {
    private static final String ENCODING = "US-ASCII";
    private static final String MAC_NAME = "HmacSHA1";

    public static byte[] HmacSHA1Encrypt(String encryptText, String encryptKey) throws NoSuchAlgorithmException, UnsupportedEncodingException, InvalidKeyException {
        Mac mac = Mac.getInstance(MAC_NAME);
        mac.init(new SecretKeySpec(encryptKey.getBytes("US-ASCII"), MAC_NAME));
        return mac.doFinal(encryptText.getBytes("US-ASCII"));
    }
}
