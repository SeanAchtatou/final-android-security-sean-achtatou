package cn.com.mzba.oauth;

import java.io.Serializable;

public class Parameter implements Serializable, Comparable<Parameter> {
    private static final long serialVersionUID = 1;
    private String name;
    private String value;

    public Parameter() {
    }

    public Parameter(String name2, String value2) {
        this.name = name2;
        this.value = value2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value2) {
        this.value = value2;
    }

    public String toString() {
        return "Parameter [name=" + this.name + ", value=" + this.value + "]";
    }

    public boolean equals(Object arg0) {
        if (arg0 == null) {
            return false;
        }
        if (this == arg0) {
            return true;
        }
        if (!(arg0 instanceof Parameter)) {
            return false;
        }
        Parameter param = (Parameter) arg0;
        return getName().equals(param.getName()) && getValue().equals(param.getValue());
    }

    public int compareTo(Parameter param) {
        int compared = this.name.compareTo(param.getName());
        if (compared == 0) {
            return this.value.compareTo(param.getValue());
        }
        return compared;
    }
}
