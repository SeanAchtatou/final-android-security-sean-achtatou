package cn.com.mzba.oauth;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import cn.com.mzba.db.SqliteHelper;
import cn.com.mzba.db.UserInfo;
import cn.com.mzba.kdwb.AuthorizeActivity;
import cn.com.mzba.kdwb.WebViewActivity;
import cn.com.mzba.model.Emotions;
import cn.com.mzba.service.MyAction;
import cn.com.mzba.service.ServiceUtils;
import cn.com.mzba.service.StatusHttpUtils;
import cn.com.mzba.service.SystemConfig;
import commonshttp.CommonsHttpOAuthConsumer;
import commonshttp.CommonsHttpOAuthProvider;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.OAuthProvider;
import oauth.signpost.basic.DefaultOAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import oauth.signpost.http.HttpParameters;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.ByteArrayRequestEntity;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

public class OAuth {
    private static final int CONNECTION_TIMEOUT = 5000;
    public static final String SIGNATURE_METHOD = "HMAC-SHA1";
    public static final String TAG = OAuth.class.getCanonicalName();
    public static OAuthConsumer consumer;
    public static OAuthProvider provider;
    public static VerfierRecivier reciver;
    public String consumerKey;
    public String consumerSecret;
    public String verifier;
    /* access modifiers changed from: private */
    public String weiboId;

    public OAuth(String weiboId2, String consumerKey2, String consumerSecret2) {
        this.consumerKey = consumerKey2;
        this.consumerSecret = consumerSecret2;
        this.weiboId = weiboId2;
    }

    public String requestAccessToken(Activity activity, String callbackurl, String requestToken, String accessToken, String authorization) {
        consumer = new CommonsHttpOAuthConsumer(this.consumerKey, this.consumerSecret);
        provider = new CommonsHttpOAuthProvider(requestToken, accessToken, authorization);
        String authUrl = "";
        try {
            if (this.weiboId.equals(SystemConfig.TENCENT_WEIBO)) {
                authUrl = provider.retrieveRequestTokenForTencent(consumer, callbackurl);
            } else {
                authUrl = provider.retrieveRequestToken(consumer, callbackurl);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.i(OAuth.class.getCanonicalName(), "token:" + consumer.getToken() + ",tokenSecret:" + consumer.getTokenSecret());
        Intent intent = new Intent();
        Bundle extras = new Bundle();
        extras.putString(Emotions.URL, authUrl);
        intent.setClass(activity.getApplicationContext(), WebViewActivity.class);
        intent.putExtras(extras);
        activity.startActivity(intent);
        IntentFilter filter = new IntentFilter();
        filter.addAction(MyAction.VERIFIER);
        reciver = new VerfierRecivier();
        activity.registerReceiver(reciver, filter);
        return authUrl;
    }

    public String httpGet(String url, String params) throws Exception {
        String result = null;
        if (params != null && !params.equals("")) {
            url = String.valueOf(url) + "?" + params;
        }
        Log.i(TAG, "url:" + url);
        HttpClient httpClient = new HttpClient();
        GetMethod httpGet = new GetMethod(url);
        httpGet.getParams().setParameter("http.socket.timeout", new Integer((int) CONNECTION_TIMEOUT));
        try {
            int statusCode = httpClient.executeMethod(httpGet);
            if (statusCode == 200) {
                result = new String(ServiceUtils.readStream(httpGet.getResponseBodyAsStream()));
            } else {
                Log.i(TAG, "code:" + statusCode);
            }
            httpGet.releaseConnection();
            return result;
        } catch (Exception e) {
            throw new Exception(e);
        } catch (Throwable th) {
            httpGet.releaseConnection();
            throw th;
        }
    }

    public String httpPost(String url, String params) throws Exception {
        String result = null;
        HttpClient httpClient = new HttpClient();
        PostMethod httpPost = new PostMethod(url);
        httpPost.addParameter("Connection", "Keep-Alive");
        httpPost.addParameter("Charset", "UTF-8");
        httpPost.addParameter("Content-Type", "application/x-www-form-urlencoded");
        httpPost.getParams().setParameter("http.socket.timeout", new Integer((int) CONNECTION_TIMEOUT));
        if (params != null && !params.equals("")) {
            httpPost.setRequestEntity(new ByteArrayRequestEntity(params.getBytes()));
        }
        try {
            int statusCode = httpClient.executeMethod(httpPost);
            if (statusCode == 200) {
                result = new String(ServiceUtils.readStream(httpPost.getResponseBodyAsStream()));
            } else {
                Log.i(TAG, "code:" + statusCode);
            }
            return result;
        } catch (Exception e) {
            throw new Exception(e);
        } catch (Throwable th) {
            throw th;
        }
    }

    public String getPostParameter(String httpMethod, String url, String token, String tokenSecret, List<Parameter> params) throws Exception {
        params.add(new Parameter(oauth.signpost.OAuth.OAUTH_CONSUMER_KEY, this.consumerKey));
        params.add(new Parameter(oauth.signpost.OAuth.OAUTH_SIGNATURE_METHOD, SIGNATURE_METHOD));
        params.add(new Parameter(oauth.signpost.OAuth.OAUTH_TIMESTAMP, SignatureUtil.generateTimeStamp()));
        params.add(new Parameter(oauth.signpost.OAuth.OAUTH_NONCE, SignatureUtil.generateNonce(false)));
        params.add(new Parameter(oauth.signpost.OAuth.OAUTH_VERSION, oauth.signpost.OAuth.VERSION_1_0));
        if (token != null) {
            params.add(new Parameter("oauth_token", token));
        }
        params.add(new Parameter(oauth.signpost.OAuth.OAUTH_SIGNATURE, SignatureUtil.generateSignature(httpMethod, url, params, this.consumerSecret, tokenSecret)));
        StringBuilder urlBuilder = new StringBuilder();
        for (Parameter param : params) {
            urlBuilder.append(param.getName());
            urlBuilder.append("=");
            urlBuilder.append(param.getValue());
            urlBuilder.append("&");
        }
        urlBuilder.deleteCharAt(urlBuilder.length() - 1);
        return urlBuilder.toString();
    }

    public HttpURLConnection signRequest(String token, String tokenSecret, HttpURLConnection conn) {
        consumer = new DefaultOAuthConsumer(this.consumerKey, this.consumerSecret);
        consumer.setTokenWithSecret(token, tokenSecret);
        try {
            consumer.sign(conn);
            conn.connect();
        } catch (OAuthMessageSignerException e) {
            e.printStackTrace();
        } catch (OAuthExpectationFailedException e2) {
            e2.printStackTrace();
        } catch (OAuthCommunicationException e3) {
            e3.printStackTrace();
        } catch (IOException e4) {
            e4.printStackTrace();
        }
        return conn;
    }

    public HttpResponse signRequest(String token, String tokenSecret, String url, List<BasicNameValuePair> params) {
        HttpPost post = new HttpPost(url);
        try {
            post.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        post.getParams().setBooleanParameter(HttpMethodParams.USE_EXPECT_CONTINUE, false);
        return signRequest(token, tokenSecret, post);
    }

    public HttpResponse signRequest(String token, String tokenSecret, HttpPost post) {
        consumer = new CommonsHttpOAuthConsumer(this.consumerKey, this.consumerSecret);
        consumer.setTokenWithSecret(token, tokenSecret);
        try {
            consumer.sign(post);
        } catch (OAuthMessageSignerException e) {
            e.printStackTrace();
        } catch (OAuthExpectationFailedException e2) {
            e2.printStackTrace();
        } catch (OAuthCommunicationException e3) {
            e3.printStackTrace();
        }
        try {
            return new DefaultHttpClient().execute(post);
        } catch (ClientProtocolException e4) {
            e4.printStackTrace();
            return null;
        } catch (IOException e5) {
            e5.printStackTrace();
            return null;
        }
    }

    public String uploadStatus(String token, String tokenSecret, File aFile, String status, String urlPath) {
        consumer = new DefaultOAuthConsumer(this.consumerKey, this.consumerSecret);
        consumer.setTokenWithSecret(token, tokenSecret);
        try {
            HttpURLConnection request = (HttpURLConnection) new URL(urlPath).openConnection();
            request.setDoOutput(true);
            request.setRequestMethod(SystemConfig.HTTP_POST);
            HttpParameters para = new HttpParameters();
            para.put(SqliteHelper.TABLE_STATUS, URLEncoder.encode(status, "utf-8").replaceAll("\\+", "%20"));
            String content = "--" + "---------------------------37531613912423" + "\r\nContent-Disposition: form-data; name=\"status\"\r\n\r\n";
            String pic = "\r\n--" + "---------------------------37531613912423" + "\r\nContent-Disposition: form-data; name=\"pic\"; filename=\"image.jpg\"\r\nContent-Type: image/jpeg\r\n\r\n";
            byte[] end_data = ("\r\n--" + "---------------------------37531613912423" + "--\r\n").getBytes();
            FileInputStream fileInputStream = new FileInputStream(aFile);
            byte[] file = new byte[((int) aFile.length())];
            fileInputStream.read(file);
            request.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + "---------------------------37531613912423");
            request.setRequestProperty("Content-Length", String.valueOf(((long) (content.getBytes().length + status.getBytes().length + pic.getBytes().length)) + aFile.length() + ((long) end_data.length)));
            consumer.setAdditionalParameters(para);
            consumer.sign(request);
            OutputStream ot = request.getOutputStream();
            ot.write(content.getBytes());
            ot.write(status.getBytes());
            ot.write(pic.getBytes());
            ot.write(file);
            ot.write(end_data);
            ot.flush();
            ot.close();
            request.connect();
            Log.i(OAuth.class.getCanonicalName(), "code:" + request.getResponseCode());
            if (200 == request.getResponseCode()) {
                return SystemConfig.SUCCESS;
            }
            return SystemConfig.ERROR;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e2) {
            e2.printStackTrace();
            return null;
        } catch (OAuthMessageSignerException e3) {
            e3.printStackTrace();
            return null;
        } catch (OAuthExpectationFailedException e4) {
            e4.printStackTrace();
            return null;
        } catch (OAuthCommunicationException e5) {
            e5.printStackTrace();
            return null;
        }
    }

    public String uploadStatusForNetease(String token, String tokenSecret, File aFile, String status, String urlPath) {
        consumer = new DefaultOAuthConsumer(this.consumerKey, this.consumerSecret);
        consumer.setTokenWithSecret(token, tokenSecret);
        try {
            HttpURLConnection request = (HttpURLConnection) new URL(urlPath).openConnection();
            request.setDoOutput(true);
            request.setRequestMethod(SystemConfig.HTTP_POST);
            HttpParameters para = new HttpParameters();
            para.put(SqliteHelper.TABLE_STATUS, URLEncoder.encode(status, "utf-8").replaceAll("\\+", "%20"));
            String content = "--" + "---------------------------37531613912423" + "\r\nContent-Disposition: form-data; name=\"status\"\r\n\r\n";
            String pic = "\r\n--" + "---------------------------37531613912423" + "\r\nContent-Disposition: form-data; name=\"pic\"; filename=\"image.jpg\"\r\nContent-Type: image/jpeg\r\n\r\n";
            byte[] end_data = ("\r\n--" + "---------------------------37531613912423" + "--\r\n").getBytes();
            FileInputStream fileInputStream = new FileInputStream(aFile);
            byte[] file = new byte[((int) aFile.length())];
            fileInputStream.read(file);
            request.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + "---------------------------37531613912423");
            request.setRequestProperty("Content-Length", String.valueOf(((long) (content.getBytes().length + status.getBytes().length + pic.getBytes().length)) + aFile.length() + ((long) end_data.length)));
            consumer.setAdditionalParameters(para);
            consumer.sign(request);
            OutputStream ot = request.getOutputStream();
            ot.write(content.getBytes());
            ot.write(status.getBytes());
            ot.write(pic.getBytes());
            ot.write(file);
            ot.write(end_data);
            ot.flush();
            ot.close();
            request.connect();
            if (200 != request.getResponseCode()) {
                return null;
            }
            InputStream is = request.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));
            StringBuilder builder = new StringBuilder();
            char[] temp = new char[4000];
            while (true) {
                int len = bufferedReader.read(temp);
                if (len == -1) {
                    break;
                }
                builder.append(temp, 0, len);
            }
            bufferedReader.close();
            is.close();
            String msg = builder.toString();
            request.disconnect();
            String imageUrl = new JSONObject(msg).getString("upload_image_url");
            if (imageUrl == null) {
                return null;
            }
            String status2 = String.valueOf(status) + imageUrl;
            ArrayList arrayList = new ArrayList();
            arrayList.add(SystemConfig.NETEASE_WEIBO);
            return StatusHttpUtils.updateStatus(arrayList, status2);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e2) {
            e2.printStackTrace();
            return null;
        } catch (OAuthMessageSignerException e3) {
            e3.printStackTrace();
            return null;
        } catch (OAuthExpectationFailedException e4) {
            e4.printStackTrace();
            return null;
        } catch (OAuthCommunicationException e5) {
            e5.printStackTrace();
            return null;
        } catch (JSONException e6) {
            e6.printStackTrace();
            return null;
        }
    }

    public class VerfierRecivier extends BroadcastReceiver {
        public VerfierRecivier() {
        }

        public void onReceive(Context context, Intent intent) {
            String userId;
            Class<VerfierRecivier> cls = VerfierRecivier.class;
            UserInfo userinfo = new UserInfo();
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                OAuth.this.verifier = bundle.getString("verifier");
                Class<VerfierRecivier> cls2 = VerfierRecivier.class;
                Log.i(cls.getCanonicalName(), "oauth:" + OAuth.this.verifier);
                try {
                    OAuth.provider.setOAuth10a(true);
                    if (OAuth.this.weiboId.equals(SystemConfig.TENCENT_WEIBO)) {
                        OAuth.provider.retrieveAccessTokenForTencent(OAuth.consumer, OAuth.this.verifier);
                    } else {
                        OAuth.provider.retrieveAccessToken(OAuth.consumer, OAuth.this.verifier);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String userKey = OAuth.consumer.getToken();
                String userSecret = OAuth.consumer.getTokenSecret();
                Class<VerfierRecivier> cls3 = VerfierRecivier.class;
                Log.i(cls.getCanonicalName(), "userKey:" + userKey);
                Class<VerfierRecivier> cls4 = VerfierRecivier.class;
                Log.i(cls.getCanonicalName(), "userSecret:" + userSecret);
                if (OAuth.this.weiboId.equals(SystemConfig.SINA_WEIBO) && (userId = OAuth.provider.getResponseParameters().get((Object) "user_id").first()) != null) {
                    userinfo.setUserId(userId);
                    Class<VerfierRecivier> cls5 = VerfierRecivier.class;
                    Log.i(cls.getCanonicalName(), "userId:" + userId);
                }
                userinfo.setToken(userKey);
                userinfo.setTokenSecret(userSecret);
                userinfo.setWeiboId(OAuth.this.weiboId);
                Intent intentAuthorize = new Intent();
                intentAuthorize.setClass(context, AuthorizeActivity.class);
                intentAuthorize.putExtra("userinfo", userinfo);
                context.startActivity(intentAuthorize);
            }
        }
    }
}
