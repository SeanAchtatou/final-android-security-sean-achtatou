package commonshttp;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import oauth.signpost.AbstractOAuthProvider;
import oauth.signpost.OAuth;
import oauth.signpost.http.HttpParameters;
import oauth.signpost.http.HttpRequest;
import oauth.signpost.http.HttpResponse;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

public class CommonsHttpOAuthProvider extends AbstractOAuthProvider {
    private static final long serialVersionUID = 1;
    private transient HttpClient httpClient;

    public CommonsHttpOAuthProvider(String requestTokenEndpointUrl, String accessTokenEndpointUrl, String authorizationWebsiteUrl) {
        super(requestTokenEndpointUrl, accessTokenEndpointUrl, authorizationWebsiteUrl);
        this.httpClient = new DefaultHttpClient();
    }

    public CommonsHttpOAuthProvider(String requestTokenEndpointUrl, String accessTokenEndpointUrl, String authorizationWebsiteUrl, HttpClient httpClient2) {
        super(requestTokenEndpointUrl, accessTokenEndpointUrl, authorizationWebsiteUrl);
        this.httpClient = httpClient2;
    }

    public void setHttpClient(HttpClient httpClient2) {
        this.httpClient = httpClient2;
    }

    /* access modifiers changed from: protected */
    public HttpRequest createRequest(String endpointUrl) throws Exception {
        return new HttpRequestAdapter(new HttpPost(endpointUrl));
    }

    /* access modifiers changed from: protected */
    public HttpRequest createRequestForTencent(String endpointUrl) throws Exception {
        HttpPost request = new HttpPost(endpointUrl);
        request.addHeader("Content-Type", "application/x-www-form-urlencoded");
        return new HttpRequestAdapter(request);
    }

    /* access modifiers changed from: protected */
    public HttpResponse sendRequest(HttpRequest request) throws Exception {
        return new HttpResponseAdapter(this.httpClient.execute((HttpUriRequest) request.unwrap()));
    }

    /* access modifiers changed from: protected */
    public HttpResponse sendRequestForTencent(HttpRequest request) throws Exception {
        HttpParameters httpParams = OAuth.oauthHeaderToParamsMap(request.getAllHeaders().get("Authorization"));
        List<NameValuePair> formParams = new ArrayList<>();
        for (String key : httpParams.keySet()) {
            formParams.add(new BasicNameValuePair(key, URLDecoder.decode(httpParams.getFirst(key), "UTF-8")));
        }
        UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formParams, "UTF-8");
        HttpPost postRequest = (HttpPost) request.unwrap();
        postRequest.setEntity(entity);
        return new HttpResponseAdapter(this.httpClient.execute(postRequest));
    }

    /* access modifiers changed from: protected */
    public void closeConnection(HttpRequest request, HttpResponse response) throws Exception {
        HttpEntity entity;
        if (response != null && (entity = ((org.apache.http.HttpResponse) response.unwrap()).getEntity()) != null) {
            try {
                entity.consumeContent();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
