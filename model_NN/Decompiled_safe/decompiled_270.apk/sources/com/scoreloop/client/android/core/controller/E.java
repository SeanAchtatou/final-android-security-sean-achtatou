package com.scoreloop.client.android.core.controller;

import android.os.Handler;
import android.os.Message;
import com.kidsfun.matching.smiles.SoundManager;

class E extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private ChallengeController f35a;

    public E(ChallengeController challengeController) {
        this.f35a = challengeController;
    }

    public void handleMessage(Message message) {
        ChallengeControllerObserver challengeControllerObserver = (ChallengeControllerObserver) message.obj;
        switch (message.what) {
            case SoundManager.EFFECT_FLAG_MENU:
                challengeControllerObserver.onCannotAcceptChallenge(this.f35a);
                return;
            case SoundManager.EFFECT_FLAG_CHOOSE:
                challengeControllerObserver.onCannotRejectChallenge(this.f35a);
                return;
            case 3:
                challengeControllerObserver.onInsufficientBalance(this.f35a);
                return;
            default:
                return;
        }
    }
}
