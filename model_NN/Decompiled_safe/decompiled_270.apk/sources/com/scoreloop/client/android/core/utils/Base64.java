package com.scoreloop.client.android.core.utils;

import com.kidsfun.matching.smiles.SoundManager;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.zip.GZIPOutputStream;

public class Base64 {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ boolean f130a = (!Base64.class.desiredAssertionStatus());
    private static final byte[] b = {45, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 95, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122};
    private static final byte[] c = {-9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -5, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, 0, -9, -9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, -9, -9, -9, -1, -9, -9, -9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, -9, -9, -9, -9, 37, -9, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9};
    private static final byte[] d = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
    private static final byte[] e = {-9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -5, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, 62, -9, -9, -9, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -9, -9, -9, -1, -9, -9, -9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -9, -9, -9, -9, -9, -9, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9};
    private static final byte[] f = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95};
    private static final byte[] g = {-9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -5, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, 62, -9, -9, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -9, -9, -9, -1, -9, -9, -9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -9, -9, -9, -9, 63, -9, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9};

    private Base64() {
    }

    public static String a(byte[] bArr) {
        String str = null;
        try {
            str = a(bArr, 0, bArr.length, 0);
        } catch (IOException e2) {
            if (!f130a) {
                throw new AssertionError(e2.getMessage());
            }
        }
        if (f130a || str != null) {
            return str;
        }
        throw new AssertionError();
    }

    public static String a(byte[] bArr, int i, int i2, int i3) {
        byte[] b2 = b(bArr, i, i2, i3);
        try {
            return new String(b2, "US-ASCII");
        } catch (UnsupportedEncodingException e2) {
            return new String(b2);
        }
    }

    private static byte[] a(byte[] bArr, int i, int i2, byte[] bArr2, int i3, int i4) {
        byte[] b2 = b(i4);
        int i5 = (i2 > 0 ? (bArr[i] << 24) >>> 8 : 0) | (i2 > 1 ? (bArr[i + 1] << 24) >>> 16 : 0) | (i2 > 2 ? (bArr[i + 2] << 24) >>> 24 : 0);
        switch (i2) {
            case SoundManager.EFFECT_FLAG_MENU:
                bArr2[i3] = b2[i5 >>> 18];
                bArr2[i3 + 1] = b2[(i5 >>> 12) & 63];
                bArr2[i3 + 2] = 61;
                bArr2[i3 + 3] = 61;
                return bArr2;
            case SoundManager.EFFECT_FLAG_CHOOSE:
                bArr2[i3] = b2[i5 >>> 18];
                bArr2[i3 + 1] = b2[(i5 >>> 12) & 63];
                bArr2[i3 + 2] = b2[(i5 >>> 6) & 63];
                bArr2[i3 + 3] = 61;
                return bArr2;
            case 3:
                bArr2[i3] = b2[i5 >>> 18];
                bArr2[i3 + 1] = b2[(i5 >>> 12) & 63];
                bArr2[i3 + 2] = b2[(i5 >>> 6) & 63];
                bArr2[i3 + 3] = b2[i5 & 63];
                return bArr2;
            default:
                return bArr2;
        }
    }

    /* access modifiers changed from: private */
    public static int b(byte[] bArr, int i, byte[] bArr2, int i2, int i3) {
        if (bArr == null) {
            throw new NullPointerException("Source array was null.");
        } else if (bArr2 == null) {
            throw new NullPointerException("Destination array was null.");
        } else if (i < 0 || i + 3 >= bArr.length) {
            throw new IllegalArgumentException(String.format("Source array with length %d cannot have offset of %d and still process four bytes.", Integer.valueOf(bArr.length), Integer.valueOf(i)));
        } else if (i2 < 0 || i2 + 2 >= bArr2.length) {
            throw new IllegalArgumentException(String.format("Destination array with length %d cannot have offset of %d and still store three bytes.", Integer.valueOf(bArr2.length), Integer.valueOf(i2)));
        } else {
            byte[] c2 = c(i3);
            if (bArr[i + 2] == 61) {
                bArr2[i2] = (byte) ((((c2[bArr[i + 1]] & 255) << 12) | ((c2[bArr[i]] & 255) << 18)) >>> 16);
                return 1;
            } else if (bArr[i + 3] == 61) {
                int i4 = ((c2[bArr[i + 2]] & 255) << 6) | ((c2[bArr[i]] & 255) << 18) | ((c2[bArr[i + 1]] & 255) << 12);
                bArr2[i2] = (byte) (i4 >>> 16);
                bArr2[i2 + 1] = (byte) (i4 >>> 8);
                return 2;
            } else {
                byte b2 = (c2[bArr[i + 3]] & 255) | ((c2[bArr[i]] & 255) << 18) | ((c2[bArr[i + 1]] & 255) << 12) | ((c2[bArr[i + 2]] & 255) << 6);
                bArr2[i2] = (byte) (b2 >> 16);
                bArr2[i2 + 1] = (byte) (b2 >> 8);
                bArr2[i2 + 2] = (byte) b2;
                return 3;
            }
        }
    }

    private static final byte[] b(int i) {
        return (i & 16) == 16 ? f : (i & 32) == 32 ? b : d;
    }

    public static byte[] b(byte[] bArr, int i, int i2, int i3) {
        int i4;
        int i5;
        a aVar;
        GZIPOutputStream gZIPOutputStream;
        ByteArrayOutputStream byteArrayOutputStream;
        a aVar2;
        GZIPOutputStream gZIPOutputStream2;
        if (bArr == null) {
            throw new NullPointerException("Cannot serialize a null array.");
        } else if (i < 0) {
            throw new IllegalArgumentException("Cannot have negative offset: " + i);
        } else if (i2 < 0) {
            throw new IllegalArgumentException("Cannot have length offset: " + i2);
        } else if (i + i2 > bArr.length) {
            throw new IllegalArgumentException(String.format("Cannot have offset of %d and length of %d with array of length %d", Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(bArr.length)));
        } else if ((i3 & 2) != 0) {
            try {
                ByteArrayOutputStream byteArrayOutputStream2 = new ByteArrayOutputStream();
                try {
                    aVar2 = new a(byteArrayOutputStream2, i3 | 1);
                    try {
                        gZIPOutputStream2 = new GZIPOutputStream(aVar2);
                    } catch (IOException e2) {
                        e = e2;
                        aVar = aVar2;
                        gZIPOutputStream = null;
                        byteArrayOutputStream = byteArrayOutputStream2;
                        try {
                            throw e;
                        } catch (Throwable th) {
                            th = th;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        aVar = aVar2;
                        gZIPOutputStream = null;
                        byteArrayOutputStream = byteArrayOutputStream2;
                        try {
                            gZIPOutputStream.close();
                        } catch (Exception e3) {
                        }
                        try {
                            aVar.close();
                        } catch (Exception e4) {
                        }
                        try {
                            byteArrayOutputStream.close();
                        } catch (Exception e5) {
                        }
                        throw th;
                    }
                } catch (IOException e6) {
                    e = e6;
                    aVar = null;
                    gZIPOutputStream = null;
                    byteArrayOutputStream = byteArrayOutputStream2;
                    throw e;
                } catch (Throwable th3) {
                    th = th3;
                    aVar = null;
                    gZIPOutputStream = null;
                    byteArrayOutputStream = byteArrayOutputStream2;
                    gZIPOutputStream.close();
                    aVar.close();
                    byteArrayOutputStream.close();
                    throw th;
                }
                try {
                    gZIPOutputStream2.write(bArr, i, i2);
                    gZIPOutputStream2.close();
                    try {
                        gZIPOutputStream2.close();
                    } catch (Exception e7) {
                    }
                    try {
                        aVar2.close();
                    } catch (Exception e8) {
                    }
                    try {
                        byteArrayOutputStream2.close();
                    } catch (Exception e9) {
                    }
                    return byteArrayOutputStream2.toByteArray();
                } catch (IOException e10) {
                    e = e10;
                    aVar = aVar2;
                    gZIPOutputStream = gZIPOutputStream2;
                    byteArrayOutputStream = byteArrayOutputStream2;
                    throw e;
                } catch (Throwable th4) {
                    th = th4;
                    aVar = aVar2;
                    gZIPOutputStream = gZIPOutputStream2;
                    byteArrayOutputStream = byteArrayOutputStream2;
                    gZIPOutputStream.close();
                    aVar.close();
                    byteArrayOutputStream.close();
                    throw th;
                }
            } catch (IOException e11) {
                e = e11;
                aVar = null;
                gZIPOutputStream = null;
                byteArrayOutputStream = null;
                throw e;
            } catch (Throwable th5) {
                th = th5;
                aVar = null;
                gZIPOutputStream = null;
                byteArrayOutputStream = null;
                gZIPOutputStream.close();
                aVar.close();
                byteArrayOutputStream.close();
                throw th;
            }
        } else {
            boolean z = (i3 & 8) != 0;
            int i6 = ((i2 / 3) * 4) + (i2 % 3 > 0 ? 4 : 0);
            if (z) {
                i6 += i6 / 76;
            }
            byte[] bArr2 = new byte[i6];
            int i7 = i2 - 2;
            int i8 = 0;
            int i9 = 0;
            int i10 = 0;
            while (i10 < i7) {
                a(bArr, i10 + i, 3, bArr2, i9, i3);
                int i11 = i8 + 4;
                if (!z || i11 < 76) {
                    i5 = i9;
                } else {
                    bArr2[i9 + 4] = 10;
                    i5 = i9 + 1;
                    i11 = 0;
                }
                i8 = i11;
                i9 = i5 + 4;
                i10 += 3;
            }
            if (i10 < i2) {
                a(bArr, i10 + i, i2 - i10, bArr2, i9, i3);
                i4 = i9 + 4;
            } else {
                i4 = i9;
            }
            if (i4 > bArr2.length - 1) {
                return bArr2;
            }
            byte[] bArr3 = new byte[i4];
            System.arraycopy(bArr2, 0, bArr3, 0, i4);
            return bArr3;
        }
    }

    /* access modifiers changed from: private */
    public static byte[] b(byte[] bArr, byte[] bArr2, int i, int i2) {
        a(bArr2, 0, i, bArr, 0, i2);
        return bArr;
    }

    /* access modifiers changed from: private */
    public static final byte[] c(int i) {
        return (i & 16) == 16 ? g : (i & 32) == 32 ? c : e;
    }
}
