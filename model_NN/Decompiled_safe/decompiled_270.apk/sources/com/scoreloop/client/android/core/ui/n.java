package com.scoreloop.client.android.core.ui;

import com.a.a.a;
import com.a.a.e;
import com.scoreloop.client.android.core.utils.Logger;
import java.io.IOException;

class n implements e {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FacebookAuthViewController f127a;

    private n(FacebookAuthViewController facebookAuthViewController) {
        this.f127a = facebookAuthViewController;
    }

    /* synthetic */ n(FacebookAuthViewController facebookAuthViewController, b bVar) {
        this(facebookAuthViewController);
    }

    public void a(a aVar) {
    }

    public void a(a aVar, Object obj) {
        String str;
        Logger.a("parsing FB response...");
        try {
            Logger.d("fbresponse", obj.toString());
            String[] split = obj.toString().split("SL123456789#");
            if (split.length > 1) {
                String[] split2 = split[1].split("#", 2);
                if (split2.length > 1) {
                    str = split2[0];
                    if (str.equalsIgnoreCase("ok")) {
                        Logger.a("FB response parsed OK");
                        this.f127a.a().didSucceed();
                        return;
                    }
                } else {
                    str = "";
                }
                String str2 = "GOT app's boundary but the message was != OK : " + str;
                Logger.a("facebook response parser", str2);
                this.f127a.a().didFail(new IllegalArgumentException(str2));
            } else {
                Logger.a("facebook response parser", "DID NOT FIND APP BOUNDARY IN THE RESPONSE");
                this.f127a.a().didFail(new IllegalArgumentException("DID NOT FIND APP BOUNDARY IN THE RESPONSE"));
            }
            boolean unused = this.f127a.d = true;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public void a(a aVar, Throwable th) {
        this.f127a.a().didFail(th);
    }

    public void b(a aVar) {
        throw new IllegalStateException();
    }
}
