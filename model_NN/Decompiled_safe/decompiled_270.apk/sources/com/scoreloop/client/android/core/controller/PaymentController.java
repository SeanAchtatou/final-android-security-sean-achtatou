package com.scoreloop.client.android.core.controller;

import com.kidsfun.matching.smiles.SoundManager;
import com.scoreloop.client.android.core.model.Payment;
import com.scoreloop.client.android.core.model.PaymentCredential;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.Response;

public class PaymentController extends RequestController {
    private PaymentCredential c;
    private Payment d;
    private boolean e;

    public PaymentController(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver);
    }

    private void i() {
        C0009h hVar = new C0009h(this, d(), b(), f(), this.c, null, this.d.a());
        h();
        a(hVar);
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) {
        boolean z;
        if (response.f() == 200) {
            Payment payment = new Payment(response.e().getJSONObject(Payment.c()));
            switch (O.f40a[((G) request).e().ordinal()]) {
                case SoundManager.EFFECT_FLAG_MENU:
                    if (this.d.b() != Payment.State.CREATED) {
                        throw new IllegalStateException("Payment state should be CREATED");
                    }
                    break;
                case SoundManager.EFFECT_FLAG_CHOOSE:
                    if (this.d.b() != Payment.State.BOOKED) {
                        throw new IllegalStateException("Payment state should be BOOKED");
                    }
                    break;
                default:
                    throw new IllegalStateException("Invalid request type");
            }
            this.d = payment;
            z = true;
        } else {
            z = false;
        }
        if (!this.e) {
            return true;
        }
        this.e = false;
        if (z) {
            i();
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void h() {
        this.e = false;
        super.h();
    }
}
