package com.scoreloop.client.android.core.controller;

/* renamed from: com.scoreloop.client.android.core.controller.f  reason: case insensitive filesystem */
class C0007f implements ChallengeControllerObserver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ScoreController f55a;

    private C0007f(ScoreController scoreController) {
        this.f55a = scoreController;
    }

    public void onCannotAcceptChallenge(ChallengeController challengeController) {
        throw new IllegalStateException();
    }

    public void onCannotRejectChallenge(ChallengeController challengeController) {
        throw new IllegalStateException();
    }

    public void onInsufficientBalance(ChallengeController challengeController) {
        throw new IllegalStateException();
    }

    public void requestControllerDidFail(RequestController requestController, Exception exc) {
        this.f55a.c().requestControllerDidFail(this.f55a, exc);
    }

    public void requestControllerDidReceiveResponse(RequestController requestController) {
        this.f55a.c().requestControllerDidReceiveResponse(this.f55a);
    }
}
