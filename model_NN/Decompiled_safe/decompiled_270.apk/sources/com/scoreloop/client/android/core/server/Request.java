package com.scoreloop.client.android.core.server;

import org.json.JSONObject;

public class Request {

    /* renamed from: a  reason: collision with root package name */
    private static int f94a = 0;
    static final /* synthetic */ boolean d = (!Request.class.desiredAssertionStatus());
    private final RequestCompletionCallback b;
    private String c;
    private JSONObject e;
    private Exception f;
    private JSONObject g;
    private final int h = f();
    private RequestMethod i = RequestMethod.GET;
    private Response j;
    private State k = State.IDLE;

    public enum State {
        CANCELLED,
        COMPLETED,
        ENQUEUED,
        EXECUTING,
        FAILED,
        IDLE;

        public static State[] a() {
            return (State[]) g.clone();
        }
    }

    public Request(RequestCompletionCallback requestCompletionCallback) {
        this.b = requestCompletionCallback;
    }

    public static int f() {
        int i2 = f94a;
        f94a = i2 + 1;
        return i2;
    }

    public JSONObject a() {
        return this.e;
    }

    public void a(Response response) {
        if (d || this.k == State.EXECUTING) {
            this.k = State.COMPLETED;
            this.j = response;
            this.f = null;
            return;
        }
        throw new AssertionError();
    }

    public void a(Exception exc) {
        if (d || this.k == State.EXECUTING) {
            this.k = State.FAILED;
            this.j = null;
            this.f = exc;
            return;
        }
        throw new AssertionError();
    }

    public void a(JSONObject jSONObject) {
        this.g = jSONObject;
    }

    public RequestMethod b() {
        return this.i;
    }

    public String c() {
        return this.c;
    }

    public RequestCompletionCallback g() {
        if (this.b != null) {
            return this.b;
        }
        throw new IllegalStateException();
    }

    public Exception h() {
        return this.f;
    }

    public JSONObject i() {
        return this.g;
    }

    public int j() {
        return this.h;
    }

    public Response k() {
        return this.j;
    }

    public synchronized State l() {
        return this.k;
    }

    public boolean m() {
        State l = l();
        return l == State.COMPLETED || l == State.CANCELLED || l == State.FAILED;
    }

    public void n() {
        if (d || this.k == State.EXECUTING || this.k == State.ENQUEUED) {
            this.k = State.CANCELLED;
            this.j = null;
            this.f = null;
            return;
        }
        throw new AssertionError();
    }

    public void o() {
        if (d || this.k == State.IDLE) {
            this.k = State.ENQUEUED;
            return;
        }
        throw new AssertionError();
    }

    public void p() {
        if (d || this.k == State.IDLE || this.k == State.ENQUEUED) {
            this.k = State.EXECUTING;
            return;
        }
        throw new AssertionError();
    }
}
