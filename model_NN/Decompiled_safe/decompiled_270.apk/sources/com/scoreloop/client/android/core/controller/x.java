package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.utils.Logger;

class x implements RequestControllerObserver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RequestController f67a;

    private x(RequestController requestController) {
        this.f67a = requestController;
    }

    /* synthetic */ x(RequestController requestController, M m) {
        this(requestController);
    }

    public void requestControllerDidFail(RequestController requestController, Exception exc) {
        Logger.a("RequestController", "Session authentication failed, failing _request");
        this.f67a.d.a(exc);
        this.f67a.d.g().a(this.f67a.d);
        U unused = this.f67a.f = (U) null;
    }

    public void requestControllerDidReceiveResponse(RequestController requestController) {
        U unused = this.f67a.f = (U) null;
    }
}
