package com.scoreloop.client.android.core.controller;

import android.os.Handler;
import android.os.Message;

/* renamed from: com.scoreloop.client.android.core.controller.t  reason: case insensitive filesystem */
class C0021t extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RequestController f65a;
    private final RequestControllerObserver b;
    private final Exception c;
    private final boolean d;

    public C0021t(RequestController requestController, RequestControllerObserver requestControllerObserver, boolean z, Exception exc) {
        this.f65a = requestController;
        this.b = requestControllerObserver;
        this.c = exc;
        this.d = z;
    }

    public void handleMessage(Message message) {
        if (this.d) {
            this.b.requestControllerDidFail(this.f65a, this.c);
        } else {
            this.b.requestControllerDidReceiveResponse(this.f65a);
        }
    }
}
