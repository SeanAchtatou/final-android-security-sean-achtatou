package com.scoreloop.client.android.core.controller;

import com.kidsfun.matching.smiles.SoundManager;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.utils.Logger;

class C implements RequestCompletionCallback {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RequestController f33a;

    private C(RequestController requestController) {
        this.f33a = requestController;
    }

    /* synthetic */ C(RequestController requestController, M m) {
        this(requestController);
    }

    public void a(Request request) {
        switch (M.f38a[request.l().ordinal()]) {
            case SoundManager.EFFECT_FLAG_MENU:
                Logger.a("RequestController", "RequestCallback.onRequestCompleted: request completed: " + request.toString());
                try {
                    if (this.f33a.a(request, request.k())) {
                        this.f33a.j();
                        return;
                    }
                    return;
                } catch (Exception e) {
                    this.f33a.c(e);
                    return;
                }
            case SoundManager.EFFECT_FLAG_CHOOSE:
                Logger.a("RequestController", "RequestCallback.onRequestCompleted: request failed: " + request.toString());
                this.f33a.c(request.h());
                return;
            case 3:
                Logger.a("RequestController", "RequestCallback.onRequestCompleted: request cancelled: " + request.toString());
                this.f33a.c(new RequestCancelledException());
                return;
            default:
                throw new IllegalStateException("onRequestCompleted called for not completed request");
        }
    }

    public void b(Request request) {
    }
}
