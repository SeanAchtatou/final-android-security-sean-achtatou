package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Session;

class P implements SocialProviderControllerObserver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MySpaceSocialProviderController f41a;

    P(MySpaceSocialProviderController mySpaceSocialProviderController) {
        this.f41a = mySpaceSocialProviderController;
    }

    public void didEnterInvalidCredentials() {
        this.f41a.d().didEnterInvalidCredentials();
    }

    public void didFail(Throwable th) {
        this.f41a.d().didFail(th);
    }

    public void didSucceed() {
        if (this.f41a.c == null) {
            UserController unused = this.f41a.c = new UserController(Session.getCurrentSession(), this.f41a);
        }
        this.f41a.c.setUser(Session.getCurrentSession().getUser());
        this.f41a.c.updateUser();
    }

    public void userDidCancel() {
        this.f41a.d().userDidCancel();
    }
}
