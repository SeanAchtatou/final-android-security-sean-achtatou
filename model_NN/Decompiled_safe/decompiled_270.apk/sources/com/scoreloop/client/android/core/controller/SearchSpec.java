package com.scoreloop.client.android.core.controller;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;

public class SearchSpec {

    /* renamed from: a  reason: collision with root package name */
    private final List f46a;
    private final int b;
    private final C0010i c;

    public SearchSpec() {
        this.b = 25;
        this.f46a = new ArrayList();
        this.c = null;
    }

    public SearchSpec(C0010i iVar) {
        this.b = 25;
        this.f46a = new ArrayList();
        this.c = iVar;
    }

    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("per_page", this.b);
        if (this.c != null) {
            jSONObject.put("order_by", this.c.a());
            jSONObject.put("order_as", this.c.b());
        }
        if (this.f46a != null) {
            JSONObject jSONObject2 = new JSONObject();
            for (C0017p pVar : this.f46a) {
                jSONObject2.put(pVar.a(), pVar.b());
            }
            jSONObject.put("conditions", jSONObject2);
        }
        JSONObject jSONObject3 = new JSONObject();
        jSONObject3.put("search", jSONObject);
        return jSONObject3;
    }

    public void a(C0017p pVar) {
        this.f46a.add(pVar);
    }
}
