package com.scoreloop.client.android.core.controller;

final class V implements UserControllerObserver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FacebookSocialProviderController f50a;

    private V(FacebookSocialProviderController facebookSocialProviderController) {
        this.f50a = facebookSocialProviderController;
    }

    public void onEmailAlreadyTaken(UserController userController) {
        this.f50a.e.a(false);
        this.f50a.d().didFail(new IllegalStateException());
    }

    public void onEmailInvalidFormat(UserController userController) {
        this.f50a.e.a(false);
        this.f50a.d().didFail(new IllegalStateException());
    }

    public void onUsernameAlreadyTaken(UserController userController) {
        this.f50a.e.a(false);
        this.f50a.d().didFail(new IllegalStateException());
    }

    public void requestControllerDidFail(RequestController requestController, Exception exc) {
        this.f50a.e.a(false);
        this.f50a.d().didFail(exc);
    }

    public void requestControllerDidReceiveResponse(RequestController requestController) {
        this.f50a.a();
    }
}
