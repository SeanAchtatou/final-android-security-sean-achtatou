package com.scoreloop.client.android.core.model;

import org.json.JSONObject;

public class Ranking {

    /* renamed from: a  reason: collision with root package name */
    private Integer f82a;
    private Integer b;

    public void a(JSONObject jSONObject) {
        this.f82a = Integer.valueOf(jSONObject.getInt("rank"));
        this.b = Integer.valueOf(jSONObject.getInt("total"));
        if (this.f82a.intValue() == 0) {
            this.f82a = null;
        }
    }

    public Integer getRank() {
        return this.f82a;
    }
}
