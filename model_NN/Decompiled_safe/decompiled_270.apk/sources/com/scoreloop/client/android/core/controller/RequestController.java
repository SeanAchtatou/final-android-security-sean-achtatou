package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.model.Game;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.Response;
import com.scoreloop.client.android.core.utils.Logger;
import org.json.JSONObject;

public abstract class RequestController {
    static final /* synthetic */ boolean b = (!RequestController.class.desiredAssertionStatus());

    /* renamed from: a  reason: collision with root package name */
    RequestControllerObserver f44a;
    private final C c;
    /* access modifiers changed from: private */
    public Request d;
    private final Session e;
    /* access modifiers changed from: private */
    public U f;

    RequestController(Session session, RequestControllerObserver requestControllerObserver) {
        if (requestControllerObserver == null) {
            throw new IllegalArgumentException("observer parameter cannot be null");
        }
        if (session == null) {
            this.e = Session.getCurrentSession();
        } else {
            this.e = session;
        }
        if (b || this.e != null) {
            this.f44a = requestControllerObserver;
            this.c = new C(this, null);
            return;
        }
        throw new AssertionError();
    }

    static Integer a(JSONObject jSONObject) {
        JSONObject jSONObject2;
        if (!jSONObject.has("error") || (jSONObject2 = jSONObject.getJSONObject("error")) == null) {
            return null;
        }
        return Integer.valueOf(jSONObject2.getInt("code"));
    }

    private void b(Request request) {
        this.d = request;
    }

    /* access modifiers changed from: private */
    public void c(Exception exc) {
        Logger.a("onRequestCompleted", "failed with exception: ", exc);
        if (this.f44a != null) {
            this.f44a.requestControllerDidFail(this, exc);
            return;
        }
        throw new IllegalStateException("could not invoke ControllerDidFail() because of the lack of the observer in the RequestController's instance !");
    }

    private Request i() {
        return this.d;
    }

    /* access modifiers changed from: private */
    public void j() {
        Logger.a("RequestController.invokeDidReceiveResponse", " observer = " + this.f44a.toString());
        if (this.f44a != null) {
            this.f44a.requestControllerDidReceiveResponse(this);
            return;
        }
        throw new IllegalStateException("could not invoke DidReceiveResponse() because of the lack of the observer in the RequestController's instance !");
    }

    /* access modifiers changed from: protected */
    public void a() {
        Logger.a("RequestController.invokeDelayedDidReceiveResponse", " observer = " + this.f44a.toString());
        if (this.f44a != null) {
            new C0021t(this, this.f44a, false, null).obtainMessage().sendToTarget();
            return;
        }
        throw new IllegalStateException("could not invoke DidReceiveResponse() because of the lack of the observer in the RequestController's instance !");
    }

    /* access modifiers changed from: package-private */
    public void a(Request request) {
        Session.State c2;
        if (!(!g() || (c2 = e().c()) == Session.State.AUTHENTICATED || c2 == Session.State.AUTHENTICATING)) {
            if (this.f == null) {
                this.f = new U(e(), new x(this, null));
            }
            this.f.i();
        }
        b(request);
        this.e.b().a(request);
    }

    /* access modifiers changed from: protected */
    public void a(Exception exc) {
        Logger.a("RequestController.invokeDelayedDidReceiveResponse", " observer = " + this.f44a.toString());
        if (this.f44a != null) {
            new C0021t(this, this.f44a, true, exc).obtainMessage().sendToTarget();
            return;
        }
        throw new IllegalStateException("could not invoke DidReceiveResponse() because of the lack of the observer in the RequestController's instance !");
    }

    /* access modifiers changed from: package-private */
    public abstract boolean a(Request request, Response response);

    /* access modifiers changed from: package-private */
    public final Game b() {
        return e().getGame();
    }

    /* access modifiers changed from: package-private */
    public void b(Exception exc) {
        Request i = i();
        if (i != null) {
            i.a(exc);
            c(exc);
        }
    }

    /* access modifiers changed from: package-private */
    public RequestControllerObserver c() {
        return this.f44a;
    }

    /* access modifiers changed from: package-private */
    public C d() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public final Session e() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public final User f() {
        return e().getUser();
    }

    /* access modifiers changed from: package-private */
    public abstract boolean g();

    /* access modifiers changed from: package-private */
    public void h() {
        if (this.d != null) {
            if (!this.d.m()) {
                this.e.b().b(this.d);
            }
            this.d = null;
        }
    }
}
