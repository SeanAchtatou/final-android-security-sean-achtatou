package com.scoreloop.client.android.core.model;

import com.scoreloop.client.android.core.controller.MySpaceSocialProviderController;

public class MySpaceSocialProvider extends SocialProvider {
    public static final String IDENTIFIER = "com.myspace.v1";

    public Class a() {
        return MySpaceSocialProviderController.class;
    }

    public String getIdentifier() {
        return IDENTIFIER;
    }

    public boolean isUserConnected(User user) {
        return user.i() != null;
    }
}
