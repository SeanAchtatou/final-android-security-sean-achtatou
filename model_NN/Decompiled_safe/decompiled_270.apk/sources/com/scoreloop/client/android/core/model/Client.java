package com.scoreloop.client.android.core.model;

import android.content.Context;
import com.scoreloop.client.android.core.ClientObserver;
import com.scoreloop.client.android.core.SessionObserver;
import com.scoreloop.client.android.core.server.Server;
import com.scoreloop.client.android.core.utils.Logger;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.BitSet;
import java.util.Properties;

public class Client implements SessionObserver {

    /* renamed from: a  reason: collision with root package name */
    private final BitSet f71a;
    private final ClientObserver b;
    private final Server c;
    private final Session d;

    public Client(Context context, String str, String str2, ClientObserver clientObserver) {
        this(context, str, str2, clientObserver, "https://api.scoreloop.com/bayeux");
    }

    Client(Context context, String str, String str2, ClientObserver clientObserver, String str3) {
        this.f71a = new BitSet();
        this.b = clientObserver;
        Game game = new Game(str, str2);
        game.setVersion("1.0");
        try {
            this.c = new Server(new URL(str3));
            this.c.a(game);
            this.d = new Session(this, this.c);
            this.d.a(game);
            Session.a(this.d);
            this.d.a().a(context);
            Properties a2 = a(context);
            if (a2 != null) {
                Money.a(a2.getProperty("currency.code"));
                Money.b(a2.getProperty("currency.symbol"));
                Money.c(a2.getProperty("currency.name.singular"));
                Money.d(a2.getProperty("currency.name.plural"));
            }
        } catch (MalformedURLException e) {
            throw new IllegalStateException(e);
        }
    }

    private Game a() {
        return this.d.getGame();
    }

    private Properties a(Context context) {
        try {
            InputStream open = context.getAssets().open("scoreloop.properties");
            try {
                Properties properties = new Properties();
                properties.load(open);
                return properties;
            } catch (IOException e) {
                Logger.c("Client", "Failed to load scoreloop.properties file");
                return null;
            }
        } catch (IOException e2) {
            Logger.c("Client", "No scoreloop.properties file found");
            return null;
        }
    }

    public Range getGameLevels() {
        return !a().hasLevels() ? new Range(0, 1) : new Range(a().getMinLevel().intValue(), a().getLevelCount().intValue());
    }

    public Range getGameModes() {
        return !a().hasModes() ? new Range(0, 1) : new Range(a().getMinMode().intValue(), a().getModeCount().intValue());
    }

    public Session getSession() {
        return this.d;
    }

    public void setGameLevels(Range range) {
        a().setMinLevel(Integer.valueOf(range.a()));
        a().setMaxLevel(Integer.valueOf(range.b()));
    }

    public void setGameModes(Range range) {
        a().setMinMode(Integer.valueOf(range.a()));
        a().setMaxMode(Integer.valueOf(range.b()));
    }
}
