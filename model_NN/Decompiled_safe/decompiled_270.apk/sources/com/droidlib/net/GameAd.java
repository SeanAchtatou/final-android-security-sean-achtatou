package com.droidlib.net;

import android.os.Environment;
import android.util.Log;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;

public abstract class GameAd {
    private static final String GAME_AD_IMG_FILE_EXT = ".p";
    private static final String GAME_AD_IMG_TEMP_FILE = "_ad.p";
    private static final String GAME_AD_INI_FILE = "gad2";
    private static final String GAME_AD_PATH = ".droidfun";
    private static final String GAME_IMG_URL_HEAD = "http://www.gamecenter.me/wbc_game_ad/kidsfun/";
    private static final String GAME_LIST_URL = "http://www.gamecenter.me/wbc_game_ad/kidsfun/game_list.txt";
    public static final int MORE_GAME_ID = 0;
    private static final String MORE_GAME_URL = "market://search?q=pub:\"KidsFun\"";
    public static final int NONE_GAME_ID = -1;
    private static String mClickGameIdList;
    private static String mGameAdPath;
    private static String mGameAdXml;
    private static String mInstallGameIdList;
    /* access modifiers changed from: private */
    public static int mLastShowGameId;
    /* access modifiers changed from: private */
    public static String mLastShowGameUrl;
    private static String mMoreGameUrl;

    public static void init(int curGameId) {
        mGameAdXml = new String("");
        mMoreGameUrl = new String("");
        mLastShowGameUrl = new String("");
        mInstallGameIdList = new String("");
        mClickGameIdList = new String("");
        mGameAdPath = new String("");
        createPath();
        loadParam();
        mMoreGameUrl = MORE_GAME_URL;
        if (!isGameIdInstalled(curGameId)) {
            addGameIdToInstallList(curGameId);
        }
        work();
        Log.v("GameAd", "init(): lastShowGameId=" + mLastShowGameId + ",clickId=" + mClickGameIdList);
    }

    public static String getMoreGameUrl() {
        return mMoreGameUrl;
    }

    private static void work() {
        new Thread() {
            public void run() {
                try {
                    sleep(1000);
                    if (GameAd.getGameAdList()) {
                        if (GameAd.mLastShowGameId == -1 || GameAd.isGameIdClicked(GameAd.mLastShowGameId)) {
                            GameAd.getAndSetNextGameAd();
                        } else {
                            GameAd.mLastShowGameUrl = GameAd.getGameUrlById(GameAd.mLastShowGameId);
                        }
                    }
                } catch (Exception e) {
                }
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public static boolean getGameAdList() {
        InputStream in = new MyHttp().doGet(GAME_LIST_URL, null);
        if (in == null) {
            Log.v("GameAd", "work(): in = null");
            return false;
        }
        try {
            byte[] buf = new byte[512];
            while (true) {
                int len = in.read(buf);
                if (len == -1) {
                    mGameAdXml = removeEnter(mGameAdXml);
                    Log.v("GameAd", "work():GameAdXml=" + mGameAdXml);
                    mMoreGameUrl = getGameUrlById(0);
                    return true;
                }
                mGameAdXml = String.valueOf(mGameAdXml) + new String(buf).substring(0, len);
            }
        } catch (Exception e) {
            Log.v("GameAd", "work(): exception:" + e.getMessage());
            return false;
        }
    }

    /* access modifiers changed from: private */
    public static void getAndSetNextGameAd() {
        int gameId = getNextGameId();
        Log.v("GameAd", "work():gameId=" + gameId);
        if (gameId == -1) {
            mLastShowGameId = -1;
            mClickGameIdList = "";
            saveParam();
            return;
        }
        String gameUrl = getGameUrlById(gameId);
        if (!gameUrl.equals("")) {
            mLastShowGameId = gameId;
            mLastShowGameUrl = gameUrl;
            if (checkImgFileExistsById(gameId)) {
                saveParam();
            } else if (downloadImgByGameId(gameId)) {
                saveParam();
            }
        }
    }

    /* access modifiers changed from: private */
    public static String getGameUrlById(int gameId) {
        String findKey = String.valueOf(Integer.toString(gameId)) + "#";
        int start = mGameAdXml.indexOf(findKey) + findKey.length();
        int end = mGameAdXml.indexOf("\n", start);
        if (end <= 0) {
            end = mGameAdXml.length();
        }
        String url = mGameAdXml.substring(start, end);
        Log.v("GameAd", "getGameUrlById(),gameId=" + gameId + ",url=" + url);
        return url;
    }

    private static String getGameImgFileNameById(int gameId) {
        return String.valueOf(Integer.toString(gameId)) + ".jpg";
    }

    public static String getLocalImgFullNameById(int gameId) {
        return String.valueOf(mGameAdPath) + Integer.toString(gameId) + GAME_AD_IMG_FILE_EXT;
    }

    private static boolean checkImgFileExistsById(int gameId) {
        try {
            if (!new File(getLocalImgFullNameById(gameId)).isFile()) {
                return false;
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private static int getNextGameId() {
        int id;
        int end = mGameAdXml.indexOf("\n");
        if (end <= 0) {
            return -1;
        }
        String line = mGameAdXml.substring(0, end);
        Log.v("GameAd", "getNextGameId():line=" + line);
        int start = 0;
        while (true) {
            int end2 = line.indexOf(",", start);
            if (end2 > 0) {
                String sGameId = line.substring(start, end2);
                Log.v("GameAd", "getNextGameId(): gameId=" + sGameId);
                if (!sGameId.equals("")) {
                    id = strToGameId(sGameId);
                    if (!isGameIdInstalled(id) && !isGameIdClicked(id)) {
                        break;
                    }
                    start = end2 + 1;
                } else {
                    id = -1;
                    break;
                }
            } else {
                id = -1;
                break;
            }
        }
        return id;
    }

    private static String removeEnter(String s) {
        return s.replace("\r\n", "");
    }

    /* JADX INFO: Multiple debug info for r7v6 boolean: [D('destName' java.lang.String), D('gameId' int)] */
    /* JADX INFO: Multiple debug info for r7v7 java.lang.String: [D('destName' java.lang.String), D('gameId' int)] */
    /* JADX INFO: Multiple debug info for r0v18 java.io.File: [D('save' java.io.FileOutputStream), D('f' java.io.File)] */
    /* JADX INFO: Multiple debug info for r0v19 java.io.File: [D('f2' java.io.File), D('f' java.io.File)] */
    /* JADX INFO: Multiple debug info for r4v1 int: [D('sb' java.lang.StringBuffer), D('len' int)] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00ae A[SYNTHETIC, Splitter:B:47:0x00ae] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00be A[SYNTHETIC, Splitter:B:55:0x00be] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean downloadImgByGameId(int r7) {
        /*
            r0 = -1
            if (r7 != r0) goto L_0x0005
            r7 = 0
        L_0x0004:
            return r7
        L_0x0005:
            java.lang.String r0 = getGameImgFileNameById(r7)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "http://www.gamecenter.me/wbc_game_ad/kidsfun/"
            r1.<init>(r2)
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r1 = r0.toString()
            com.droidlib.net.MyHttp r0 = new com.droidlib.net.MyHttp
            r0.<init>()
            r2 = 0
            java.io.InputStream r2 = r0.doGet(r1, r2)
            if (r2 != 0) goto L_0x0026
            r7 = 0
            goto L_0x0004
        L_0x0026:
            r0 = 0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r3 = com.droidlib.net.GameAd.mGameAdPath
            java.lang.String r3 = java.lang.String.valueOf(r3)
            r1.<init>(r3)
            java.lang.String r3 = "_ad.p"
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r1 = r1.toString()
            r3 = 1
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ FileNotFoundException -> 0x00db, Exception -> 0x00a5, all -> 0x00b9 }
            r4.<init>()     // Catch:{ FileNotFoundException -> 0x00db, Exception -> 0x00a5, all -> 0x00b9 }
            java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x00db, Exception -> 0x00a5, all -> 0x00b9 }
            r5.<init>(r1)     // Catch:{ FileNotFoundException -> 0x00db, Exception -> 0x00a5, all -> 0x00b9 }
            r0 = 8192(0x2000, float:1.14794E-41)
            byte[] r0 = new byte[r0]     // Catch:{ FileNotFoundException -> 0x0092, Exception -> 0x00d8, all -> 0x00ce }
        L_0x004b:
            int r4 = r2.read(r0)     // Catch:{ FileNotFoundException -> 0x0092, Exception -> 0x00d8, all -> 0x00ce }
            r6 = -1
            if (r4 != r6) goto L_0x008d
            r5.flush()     // Catch:{ FileNotFoundException -> 0x0092, Exception -> 0x00d8, all -> 0x00ce }
            java.lang.String r0 = "GameAd"
            java.lang.String r2 = "downloadImgFile()....ok"
            android.util.Log.v(r0, r2)     // Catch:{ FileNotFoundException -> 0x0092, Exception -> 0x00d8, all -> 0x00ce }
            if (r5 == 0) goto L_0x00cb
            r5.close()     // Catch:{ IOException -> 0x00c7 }
            r2 = r3
            r0 = r5
        L_0x0063:
            if (r2 == 0) goto L_0x008a
            java.lang.String r7 = getLocalImgFullNameById(r7)
            java.io.File r0 = new java.io.File
            r0.<init>(r7)
            boolean r3 = r0.isFile()
            if (r3 == 0) goto L_0x0077
            r0.delete()
        L_0x0077:
            java.io.File r0 = new java.io.File
            r0.<init>(r1)
            boolean r1 = r0.isFile()
            if (r1 == 0) goto L_0x008a
            java.io.File r1 = new java.io.File
            r1.<init>(r7)
            r0.renameTo(r1)
        L_0x008a:
            r7 = r2
            goto L_0x0004
        L_0x008d:
            r6 = 0
            r5.write(r0, r6, r4)     // Catch:{ FileNotFoundException -> 0x0092, Exception -> 0x00d8, all -> 0x00ce }
            goto L_0x004b
        L_0x0092:
            r0 = move-exception
            r3 = r5
        L_0x0094:
            r2 = 0
            r0.printStackTrace()     // Catch:{ all -> 0x00d3 }
            if (r3 == 0) goto L_0x00df
            r3.close()     // Catch:{ IOException -> 0x009f }
            r0 = r3
            goto L_0x0063
        L_0x009f:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r3
            goto L_0x0063
        L_0x00a5:
            r2 = move-exception
            r3 = r0
            r0 = r2
        L_0x00a8:
            r2 = 0
            r0.printStackTrace()     // Catch:{ all -> 0x00d3 }
            if (r3 == 0) goto L_0x00df
            r3.close()     // Catch:{ IOException -> 0x00b3 }
            r0 = r3
            goto L_0x0063
        L_0x00b3:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r3
            goto L_0x0063
        L_0x00b9:
            r7 = move-exception
            r1 = r7
            r7 = r3
        L_0x00bc:
            if (r0 == 0) goto L_0x00c1
            r0.close()     // Catch:{ IOException -> 0x00c2 }
        L_0x00c1:
            throw r1
        L_0x00c2:
            r7 = move-exception
            r7.printStackTrace()
            goto L_0x00c1
        L_0x00c7:
            r0 = move-exception
            r0.printStackTrace()
        L_0x00cb:
            r2 = r3
            r0 = r5
            goto L_0x0063
        L_0x00ce:
            r7 = move-exception
            r1 = r7
            r0 = r5
            r7 = r3
            goto L_0x00bc
        L_0x00d3:
            r7 = move-exception
            r1 = r7
            r0 = r3
            r7 = r2
            goto L_0x00bc
        L_0x00d8:
            r0 = move-exception
            r3 = r5
            goto L_0x00a8
        L_0x00db:
            r2 = move-exception
            r3 = r0
            r0 = r2
            goto L_0x0094
        L_0x00df:
            r0 = r3
            goto L_0x0063
        */
        throw new UnsupportedOperationException("Method not decompiled: com.droidlib.net.GameAd.downloadImgByGameId(int):boolean");
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void loadParam() {
        /*
            r4 = -1
            com.droidlib.net.GameAd.mLastShowGameId = r4
            java.lang.String r4 = ""
            com.droidlib.net.GameAd.mLastShowGameUrl = r4
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x0077, all -> 0x0095 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0077, all -> 0x0095 }
            java.lang.String r5 = com.droidlib.net.GameAd.mGameAdPath     // Catch:{ Exception -> 0x0077, all -> 0x0095 }
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ Exception -> 0x0077, all -> 0x0095 }
            r4.<init>(r5)     // Catch:{ Exception -> 0x0077, all -> 0x0095 }
            java.lang.String r5 = "gad2"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0077, all -> 0x0095 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0077, all -> 0x0095 }
            r2.<init>(r4)     // Catch:{ Exception -> 0x0077, all -> 0x0095 }
            java.io.BufferedReader r0 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0077, all -> 0x0095 }
            java.io.FileReader r4 = new java.io.FileReader     // Catch:{ Exception -> 0x0077, all -> 0x0095 }
            r4.<init>(r2)     // Catch:{ Exception -> 0x0077, all -> 0x0095 }
            r0.<init>(r4)     // Catch:{ Exception -> 0x0077, all -> 0x0095 }
            java.lang.String r3 = ""
            r1 = 0
        L_0x002e:
            java.lang.String r3 = r0.readLine()     // Catch:{ Exception -> 0x0077, all -> 0x0095 }
            if (r3 != 0) goto L_0x005a
        L_0x0034:
            r0.close()     // Catch:{ Exception -> 0x0077, all -> 0x0095 }
        L_0x0037:
            java.lang.String r4 = "GameAd"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "showId="
            r5.<init>(r6)
            int r6 = com.droidlib.net.GameAd.mLastShowGameId
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r6 = ",clickId="
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r6 = com.droidlib.net.GameAd.mClickGameIdList
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            android.util.Log.v(r4, r5)
            return
        L_0x005a:
            java.lang.String r3 = removeEnter(r3)     // Catch:{ Exception -> 0x0077, all -> 0x0095 }
            if (r1 != 0) goto L_0x0069
            int r4 = strToGameId(r3)     // Catch:{ Exception -> 0x0077, all -> 0x0095 }
            com.droidlib.net.GameAd.mLastShowGameId = r4     // Catch:{ Exception -> 0x0077, all -> 0x0095 }
        L_0x0066:
            int r1 = r1 + 1
            goto L_0x002e
        L_0x0069:
            r4 = 1
            if (r1 != r4) goto L_0x0079
            java.lang.String r4 = "\n"
            java.lang.String r5 = ""
            java.lang.String r4 = r3.replace(r4, r5)     // Catch:{ Exception -> 0x0077, all -> 0x0095 }
            com.droidlib.net.GameAd.mClickGameIdList = r4     // Catch:{ Exception -> 0x0077, all -> 0x0095 }
            goto L_0x0066
        L_0x0077:
            r4 = move-exception
            goto L_0x0037
        L_0x0079:
            r4 = 2
            if (r1 != r4) goto L_0x0034
            java.lang.String r4 = "\n"
            java.lang.String r5 = ""
            java.lang.String r4 = r3.replace(r4, r5)     // Catch:{ Exception -> 0x0077, all -> 0x0095 }
            com.droidlib.net.GameAd.mInstallGameIdList = r4     // Catch:{ Exception -> 0x0077, all -> 0x0095 }
            java.lang.String r4 = com.droidlib.net.GameAd.mInstallGameIdList     // Catch:{ Exception -> 0x0077, all -> 0x0095 }
            java.lang.String r5 = "market"
            int r4 = r4.indexOf(r5)     // Catch:{ Exception -> 0x0077, all -> 0x0095 }
            if (r4 < 0) goto L_0x0034
            java.lang.String r4 = ""
            com.droidlib.net.GameAd.mInstallGameIdList = r4     // Catch:{ Exception -> 0x0077, all -> 0x0095 }
            goto L_0x0034
        L_0x0095:
            r4 = move-exception
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.droidlib.net.GameAd.loadParam():void");
    }

    private static void saveParam() {
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(String.valueOf(mGameAdPath) + GAME_AD_INI_FILE)));
            bw.write(String.valueOf(Integer.toString(mLastShowGameId)) + "\n");
            bw.write(String.valueOf(mClickGameIdList.replace("\n", "")) + "\n");
            bw.write(String.valueOf(mInstallGameIdList.replace("\n", "")) + "\n");
            bw.close();
        } catch (Exception e) {
        }
    }

    public static void doGameAdClick() {
        if (mLastShowGameId != -1) {
            addGameIdToClickList(mLastShowGameId);
        }
        saveParam();
    }

    public static String getShowGameUrl() {
        return mLastShowGameUrl;
    }

    public static int getShowGameId() {
        return mLastShowGameId;
    }

    private static void createPath() {
        mGameAdPath = String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath()) + File.separator + GAME_AD_PATH + File.separator;
        Log.v("GameAd", "createPath(): mGameAdPath=" + mGameAdPath);
        File dir = new File(mGameAdPath);
        if (!dir.exists() && !dir.mkdirs()) {
            Log.v("GameAd", "dir.mkdirs() ....error!");
        }
    }

    public static boolean checkShowGameAd() {
        if (mLastShowGameId == -1) {
            return false;
        }
        if (mLastShowGameId == 0) {
            return false;
        }
        if (mLastShowGameUrl.equals("")) {
            Log.v("GameAd", "checkShowGameAd():getShowGameUrl() = false");
            return false;
        } else if (!checkImgFileExistsById(mLastShowGameId)) {
            return false;
        } else {
            if (isGameIdClicked(mLastShowGameId)) {
                Log.v("GameAd", "checkShowGameAd():mLastShowGameId =" + mLastShowGameId);
                return false;
            }
            Log.v("GameAd", "checkShowGameAd() OK!!, mLastShowGameId =" + mLastShowGameId + "clickId=" + mClickGameIdList);
            return true;
        }
    }

    private static int strToGameId(String s) {
        try {
            return Integer.parseInt(s);
        } catch (Exception e) {
            return -1;
        }
    }

    /* access modifiers changed from: private */
    public static boolean isGameIdClicked(int gameId) {
        if (gameId == -1) {
            return false;
        }
        return mClickGameIdList.indexOf(new StringBuilder(String.valueOf(Integer.toString(gameId))).append(",").toString()) >= 0;
    }

    private static boolean addGameIdToClickList(int gameId) {
        if (gameId == -1 || gameId == 0) {
            return false;
        }
        if (isGameIdClicked(gameId)) {
            return false;
        }
        mClickGameIdList = String.valueOf(mClickGameIdList) + Integer.toString(gameId) + ",";
        return true;
    }

    private static boolean isGameIdInstalled(int gameId) {
        if (gameId == -1 || gameId == 0) {
            return false;
        }
        return mInstallGameIdList.indexOf(new StringBuilder(String.valueOf(Integer.toString(gameId))).append(",").toString()) >= 0;
    }

    private static boolean addGameIdToInstallList(int gameId) {
        if (gameId == -1 || gameId == 0) {
            return false;
        }
        if (isGameIdInstalled(gameId)) {
            return false;
        }
        mInstallGameIdList = String.valueOf(mInstallGameIdList) + Integer.toString(gameId) + ",";
        return true;
    }
}
