package com.kidsfun.matching.smiles;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import com.droidlib.net.GameAd;
import com.scoreloop.android.coreui.HighscoresActivity;
import com.scoreloop.android.coreui.ScoreloopManager;

public class GMenu extends Activity {
    public static String SL_GAME_ID = "ebb4cd38-4552-4c6c-8b90-68b3f7e55469";
    public static String SL_GAME_SECRET = "k0wi2+cHxMQC2HbP03hZ4hiww+uB8KVMqwjpDE1ja1gnr5iF1ObzIA==";
    private final int REQUEST_CODE = 2;
    /* access modifiers changed from: private */
    public int mGameSaveLevel;
    /* access modifiers changed from: private */
    public int mGameSaveLife;
    /* access modifiers changed from: private */
    public boolean mGameSavePause;
    /* access modifiers changed from: private */
    public int mGameSaveScore;
    private ImageView mImgGameLogo = null;
    private boolean mIsMusic;
    private boolean mIsSound;
    private boolean mIsVibrate;
    private MediaPlayer mMusicMP;
    private MediaPlayer mSoundMP;
    private Window window;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        this.window = getWindow();
        this.window.setFlags(1024, 1024);
        int xmlId = R.layout.menu_480;
        if (((WindowManager) getSystemService("window")).getDefaultDisplay().getHeight() > 480) {
            xmlId = R.layout.menu_569;
        }
        setContentView(xmlId);
        this.mImgGameLogo = (ImageView) findViewById(R.id.img_logo);
        this.mGameSavePause = false;
        this.mGameSaveLevel = 0;
        this.mGameSaveScore = 0;
        SoundManager.init(getBaseContext());
        Bundle b = getIntent().getExtras();
        if (b != null) {
            Log.v("GMenu", "onCreate(), b != null");
            this.mGameSavePause = b.getBoolean("gamePause", false);
            this.mGameSaveScore = b.getInt("gameScore", 0);
            this.mGameSaveLevel = b.getInt("gameLevel", 0);
            if (this.mGameSaveScore > 0) {
                updateHighScore(this.mGameSaveScore);
            }
            Log.v("GMenu", "onCreate(), Life=" + Integer.toString(this.mGameSaveLife) + ", level=" + Integer.toString(this.mGameSaveLevel));
        } else {
            Log.v("GMenu", "onCreate(), saveInstanceState=null");
        }
        ScoreloopManager.init(this, SL_GAME_ID, SL_GAME_SECRET);
        ScoreloopManager.setNumberOfModes(2);
        handleMenuButton();
        startGameLogoMove();
        playMusic();
        GameAd.init(0);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.v("GMenu", "onActivityResult(), requestCode=" + Integer.toString(requestCode) + ", resultCode=" + resultCode);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        pauseMusic();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        resumeMusic();
        Button btnResume = (Button) findViewById(R.id.btn_resume);
        if (isResumeData()) {
            btnResume.setVisibility(0);
        } else {
            btnResume.setVisibility(8);
        }
        loadGameParam();
        SoundManager.setSoundOn(this.mIsSound);
        SoundManager.setVibrateOn(this.mIsVibrate);
        if (!this.mIsMusic) {
            stopMusic();
        } else {
            playMusic();
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    private void handleMenuButton() {
        ((Button) findViewById(R.id.btn_newgame)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundManager.play(1);
                GMenu.this.pauseMusic();
                Log.v("GMenu", "New Game...called, new itMain");
                Intent itMain = new Intent();
                itMain.setClass(GMenu.this, GameActivity.class);
                GMenu.this.startActivity(itMain);
            }
        });
        ((Button) findViewById(R.id.btn_resume)).setOnClickListener(new View.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
            public void onClick(View v) {
                SoundManager.play(1);
                Intent itMain = new Intent();
                itMain.setClass(GMenu.this, GameActivity.class);
                GMenu.this.mGameSavePause = true;
                if (GMenu.this.mGameSavePause) {
                    Log.v("GMenu", "Resume, mGameSavePause= true");
                    itMain.putExtra("gamePause", true);
                    itMain.putExtra("gameScore", GMenu.this.mGameSaveScore);
                    itMain.putExtra("gameLevel", GMenu.this.mGameSaveLevel);
                    Log.v("GMenu", "Resume, Life=" + Integer.toString(GMenu.this.mGameSaveLife) + ", level=" + Integer.toString(GMenu.this.mGameSaveLevel));
                } else {
                    Log.v("GMenu", "Resume, mGameSavePause=false!");
                }
                GMenu.this.stopMusic();
                GMenu.this.startActivity(itMain);
            }
        });
        ((Button) findViewById(R.id.btn_setting)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundManager.play(1);
                GMenu.this.showOptionDlg();
            }
        });
        ((Button) findViewById(R.id.btn_highscore)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundManager.play(1);
                GMenu.this.startActivity(new Intent(GMenu.this, HighscoresActivity.class));
            }
        });
        ((Button) findViewById(R.id.btn_more)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundManager.play(1);
                GMenu.this.showMoreApp();
            }
        });
        ((Button) findViewById(R.id.btn_exit)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundManager.play(1);
                GMenu.this.startActivity(new Intent(GMenu.this, AdSplash.class));
                GMenu.this.exitApp();
            }
        });
    }

    private void startGameLogoMove() {
        if (this.mImgGameLogo != null) {
            this.mImgGameLogo.startAnimation(AnimationUtils.loadAnimation(this, R.anim.logo_move));
        }
    }

    /* access modifiers changed from: package-private */
    public void showMoreApp() {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(GameAd.getMoreGameUrl())));
    }

    /* access modifiers changed from: private */
    public void exitApp() {
        saveGameParam();
        stopMusic();
        finish();
    }

    public void showOptionDlg() {
        Intent intent = new Intent();
        intent.setClass(this, OptionActivity.class);
        startActivity(intent);
    }

    public void showScoreList() {
        Intent intent = new Intent();
        intent.setClass(this, ScoreList.class);
        startActivity(intent);
    }

    public void loadGameParam() {
        SharedPreferences settings = getSharedPreferences(ConstInfo.PREFS_NAME, 0);
        this.mIsMusic = settings.getBoolean("isMusic", true);
        this.mIsSound = settings.getBoolean("isSound", true);
        this.mIsVibrate = settings.getBoolean("isVibrate", true);
    }

    public void saveGameParam() {
        SharedPreferences.Editor editor = getSharedPreferences(ConstInfo.PREFS_NAME, 0).edit();
        editor.putBoolean("isMusic", this.mIsMusic);
        editor.putBoolean("isSound", this.mIsSound);
        editor.putBoolean("isVibrate", this.mIsVibrate);
        editor.commit();
    }

    private boolean isResumeData() {
        return getSharedPreferences(ConstInfo.PREFS_NAME, 0).getInt("curScore", 0) != 0;
    }

    public void updateHighScore(int highScore) {
        Log.v("GMenu", "updateHighScore(),highScore=" + Integer.toString(highScore));
        if (highScore > 0) {
            SharedPreferences settings = getSharedPreferences(ConstInfo.PREFS_NAME, 0);
            int[] scoreArray = new int[10];
            for (int i = 0; i < 9; i++) {
                scoreArray[i] = settings.getInt("highScore" + Integer.toString(i), 0);
            }
            SharedPreferences.Editor editor = settings.edit();
            int i2 = 0;
            while (true) {
                if (i2 >= 9) {
                    break;
                } else if (highScore > scoreArray[i2]) {
                    Log.v("GMenu", "updateHighScore(), highScore>scoreArray[i], i=" + Integer.toString(i2));
                    for (int j = i2 + 1; j < 9; j++) {
                        editor.putInt("highScore" + Integer.toString(j), scoreArray[j - 1]);
                    }
                    editor.putInt("highScore" + Integer.toString(i2), highScore);
                } else {
                    i2++;
                }
            }
            editor.commit();
        }
    }

    public void playMusic() {
        if (this.mIsMusic) {
            stopMusic();
            if (this.mMusicMP == null) {
                this.mMusicMP = MediaPlayer.create(getBaseContext(), (int) R.raw.menu_music);
            }
            this.mMusicMP.start();
            this.mMusicMP.setLooping(true);
        }
    }

    /* access modifiers changed from: private */
    public void stopMusic() {
        if (this.mMusicMP != null) {
            if (this.mMusicMP.isPlaying()) {
                this.mMusicMP.stop();
            }
            this.mMusicMP.release();
            this.mMusicMP = null;
        }
    }

    /* access modifiers changed from: private */
    public void pauseMusic() {
        if (this.mMusicMP != null) {
            this.mMusicMP.pause();
        }
    }

    private void resumeMusic() {
        Log.v("GMenu", "resumeMusic()... mIsMusic=" + (this.mIsMusic ? "true" : "false"));
        if (!this.mIsMusic) {
            return;
        }
        if (this.mMusicMP != null) {
            this.mMusicMP.start();
            return;
        }
        this.mMusicMP = MediaPlayer.create(getBaseContext(), (int) R.raw.menu_music);
        this.mMusicMP.start();
        this.mMusicMP.setLooping(true);
    }
}
