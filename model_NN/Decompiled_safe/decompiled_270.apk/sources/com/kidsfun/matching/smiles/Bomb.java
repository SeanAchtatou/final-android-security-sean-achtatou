package com.kidsfun.matching.smiles;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import java.util.Vector;

public class Bomb {
    Bitmap[] bitmap;
    int height;
    private Vector<Point> vector = new Vector<>();
    int width;

    public Bomb(Bitmap[] mb, int w, int h) {
        this.bitmap = mb;
        this.width = w;
        this.height = h;
    }

    public void add(int x, int y, int type) {
        getVector().addElement(new Point(x, y, type));
    }

    private synchronized Vector<Point> getVector() {
        return this.vector;
    }

    public void move() {
        for (int i = 0; i < getVector().size(); i++) {
            if (getVector().get(i).translating > this.bitmap[getVector().get(i).type].getWidth() / 2) {
                getVector().remove(i);
            }
        }
        getVector().trimToSize();
    }

    public void paint(Canvas canvas, Paint paint) {
        canvas.save();
        canvas.clipRect(0, 0, this.width, this.height);
        for (int i = 0; i < getVector().size(); i++) {
            if (getVector().get(i) != null) {
                canvas.drawBitmap(this.bitmap[getVector().get(i).type], (Rect) null, new Rect(getVector().get(i).x + getVector().get(i).translating, getVector().get(i).y + getVector().get(i).translating, (getVector().get(i).x + this.bitmap[getVector().get(i).type].getWidth()) - getVector().get(i).translating, (getVector().get(i).y + this.bitmap[getVector().get(i).type].getHeight()) - getVector().get(i).translating), paint);
                getVector().get(i).translating += 6;
            }
        }
        canvas.restore();
    }

    class Point {
        int translating;
        int type;
        int x;
        int y;

        Point(int x2, int y2, int type2) {
            this.x = x2;
            this.y = y2;
            this.type = type2;
        }
    }
}
