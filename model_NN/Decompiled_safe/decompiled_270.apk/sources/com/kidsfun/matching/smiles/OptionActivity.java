package com.kidsfun.matching.smiles;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;

public class OptionActivity extends Activity {
    private CheckBox cbMusic;
    private CheckBox cbSound;
    private CheckBox cbVibrate;
    private Intent mIntent;
    /* access modifiers changed from: private */
    public SharedPreferences mSettings;
    private Window window;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        this.window = getWindow();
        this.window.setFlags(1024, 1024);
        setContentView((int) R.layout.option);
        setTitle("Game Option");
        this.mSettings = getSharedPreferences(ConstInfo.PREFS_NAME, 0);
        this.cbMusic = (CheckBox) findViewById(R.id.cb_music);
        this.cbMusic.setChecked(this.mSettings.getBoolean("isMusic", true));
        this.cbMusic.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SoundManager.play(1);
                OptionActivity.this.mSettings.edit().putBoolean("isMusic", isChecked).commit();
            }
        });
        this.cbSound = (CheckBox) findViewById(R.id.cb_sound);
        this.cbSound.setChecked(this.mSettings.getBoolean("isSound", true));
        this.cbSound.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SoundManager.play(1);
                OptionActivity.this.mSettings.edit().putBoolean("isSound", isChecked).commit();
            }
        });
        this.cbVibrate = (CheckBox) findViewById(R.id.cb_vibrate);
        this.cbVibrate.setChecked(this.mSettings.getBoolean("isVibrate", true));
        this.cbVibrate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SoundManager.play(1);
                OptionActivity.this.mSettings.edit().putBoolean("isVibrate", isChecked).commit();
            }
        });
        ((Button) findViewById(R.id.btn_menu)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SoundManager.play(1);
                OptionActivity.this.finish();
            }
        });
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }
}
