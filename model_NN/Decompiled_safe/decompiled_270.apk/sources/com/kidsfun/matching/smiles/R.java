package com.kidsfun.matching.smiles;

public final class R {

    public static final class anim {
        public static final int logo_move = 2130968576;
    }

    public static final class array {
        public static final int sl_game_modes = 2131099648;
    }

    public static final class attr {
    }

    public static final class color {
        public static final int sl_selector_color = 2131296256;
    }

    public static final class drawable {
        public static final int ad1 = 2130837504;
        public static final int ad2 = 2130837505;
        public static final int ad3 = 2130837506;
        public static final int ad4 = 2130837507;
        public static final int app0 = 2130837508;
        public static final int app1 = 2130837509;
        public static final int app2 = 2130837510;
        public static final int app3 = 2130837511;
        public static final int app4 = 2130837512;
        public static final int bg = 2130837513;
        public static final int bg_setting = 2130837514;
        public static final int bonus = 2130837515;
        public static final int bonusbar = 2130837516;
        public static final int bonusbar_fill = 2130837517;
        public static final int btn_exit = 2130837518;
        public static final int btn_gad_moregame = 2130837519;
        public static final int btn_leaderboard = 2130837520;
        public static final int btn_menu = 2130837521;
        public static final int btn_moregame = 2130837522;
        public static final int btn_newgame = 2130837523;
        public static final int btn_resume = 2130837524;
        public static final int btn_setting = 2130837525;
        public static final int button_bg = 2130837526;
        public static final int button_bg_down = 2130837527;
        public static final int checkbox_music = 2130837528;
        public static final int checkbox_sound = 2130837529;
        public static final int checkbox_vibrate = 2130837530;
        public static final int cursor1 = 2130837531;
        public static final int cursor2 = 2130837532;
        public static final int exit_down = 2130837533;
        public static final int exit_up = 2130837534;
        public static final int gad_bg_splash = 2130837535;
        public static final int gad_btn_close = 2130837536;
        public static final int gad_btn_download = 2130837537;
        public static final int gad_btn_moregame = 2130837538;
        public static final int gad_close_down = 2130837539;
        public static final int gad_close_up = 2130837540;
        public static final int gad_download_down = 2130837541;
        public static final int gad_download_up = 2130837542;
        public static final int gad_more_down = 2130837543;
        public static final int gad_more_up = 2130837544;
        public static final int game_logo = 2130837545;
        public static final int ic_cup = 2130837546;
        public static final int icon = 2130837547;
        public static final int img1 = 2130837548;
        public static final int img2 = 2130837549;
        public static final int img3 = 2130837550;
        public static final int img4 = 2130837551;
        public static final int leaderboards_down = 2130837552;
        public static final int leaderboards_up = 2130837553;
        public static final int menu_button = 2130837554;
        public static final int menu_down = 2130837555;
        public static final int menu_img = 2130837556;
        public static final int menu_up = 2130837557;
        public static final int moregame_down = 2130837558;
        public static final int moregame_up = 2130837559;
        public static final int music_checked = 2130837560;
        public static final int music_unchecked = 2130837561;
        public static final int n1 = 2130837562;
        public static final int n2 = 2130837563;
        public static final int n3 = 2130837564;
        public static final int n4 = 2130837565;
        public static final int n5 = 2130837566;
        public static final int n6 = 2130837567;
        public static final int n7 = 2130837568;
        public static final int n8 = 2130837569;
        public static final int n9 = 2130837570;
        public static final int newgame_down = 2130837571;
        public static final int newgame_up = 2130837572;
        public static final int num = 2130837573;
        public static final int num01 = 2130837574;
        public static final int pane = 2130837575;
        public static final int resume_down = 2130837576;
        public static final int resume_up = 2130837577;
        public static final int score = 2130837578;
        public static final int score_title = 2130837579;
        public static final int setting = 2130837580;
        public static final int setting_down = 2130837581;
        public static final int setting_up = 2130837582;
        public static final int sl_bg_btn = 2130837583;
        public static final int sl_bg_btn_pre = 2130837584;
        public static final int sl_bg_dropdown = 2130837585;
        public static final int sl_bg_dropdown_pre = 2130837586;
        public static final int sl_bg_h1 = 2130837587;
        public static final int sl_bg_list = 2130837588;
        public static final int sl_bg_list_pre = 2130837589;
        public static final int sl_divider = 2130837590;
        public static final int sl_divider_list = 2130837591;
        public static final int sl_logo = 2130837592;
        public static final int sl_menu_highscores = 2130837593;
        public static final int sl_menu_profile = 2130837594;
        public static final int sl_selector_btn = 2130837595;
        public static final int sl_selector_dropdown = 2130837596;
        public static final int sl_selector_list = 2130837597;
        public static final int sound_checked = 2130837598;
        public static final int sound_unchecked = 2130837599;
        public static final int star0 = 2130837600;
        public static final int star1 = 2130837601;
        public static final int star2 = 2130837602;
        public static final int star3 = 2130837603;
        public static final int star4 = 2130837604;
        public static final int star5 = 2130837605;
        public static final int star6 = 2130837606;
        public static final int star7 = 2130837607;
        public static final int vibrate_checked = 2130837608;
        public static final int vibrate_unchecked = 2130837609;
    }

    public static final class id {
        public static final int TextView02 = 2131361830;
        public static final int ad = 2131361797;
        public static final int ad1 = 2131361814;
        public static final int ad2 = 2131361809;
        public static final int ad_splash_layout = 2131361792;
        public static final int btn_back = 2131361798;
        public static final int btn_close = 2131361795;
        public static final int btn_download = 2131361793;
        public static final int btn_exit = 2131361808;
        public static final int btn_highscore = 2131361807;
        public static final int btn_menu = 2131361815;
        public static final int btn_more = 2131361794;
        public static final int btn_newgame = 2131361804;
        public static final int btn_profile = 2131361822;
        public static final int btn_resume = 2131361805;
        public static final int btn_setting = 2131361806;
        public static final int cb_music = 2131361811;
        public static final int cb_sound = 2131361812;
        public static final int cb_vibrate = 2131361813;
        public static final int email = 2131361829;
        public static final int game_mode_spinner = 2131361821;
        public static final int game_view = 2131361801;
        public static final int highscores_list_item = 2131361825;
        public static final int img_icon = 2131361817;
        public static final int img_logo = 2131361803;
        public static final int img_order = 2131361799;
        public static final int list_view = 2131361823;
        public static final int login = 2131361827;
        public static final int lv = 2131361816;
        public static final int lv_score = 2131361796;
        public static final int myscore_view = 2131361824;
        public static final int progress_indicator = 2131361820;
        public static final int rank = 2131361826;
        public static final int score = 2131361828;
        public static final int scorebar = 2131361802;
        public static final int scrollview2 = 2131361810;
        public static final int spinnerTarget = 2131361832;
        public static final int title_login = 2131361819;
        public static final int tv_name = 2131361818;
        public static final int txt_score = 2131361800;
        public static final int update_button = 2131361831;
    }

    public static final class layout {
        public static final int ad_splash = 2130903040;
        public static final int lv_score = 2130903041;
        public static final int lv_score_raw = 2130903042;
        public static final int main = 2130903043;
        public static final int menu_480 = 2130903044;
        public static final int menu_569 = 2130903045;
        public static final int option = 2130903046;
        public static final int popup_lv = 2130903047;
        public static final int popup_lv_item = 2130903048;
        public static final int sl_highscores = 2130903049;
        public static final int sl_highscores_list_item = 2130903050;
        public static final int sl_profile = 2130903051;
        public static final int sl_spinner_item = 2130903052;
    }

    public static final class raw {
        public static final int bg_01 = 2131034112;
        public static final int bg_02 = 2131034113;
        public static final int bg_03 = 2131034114;
        public static final int choose = 2131034115;
        public static final int click = 2131034116;
        public static final int click2 = 2131034117;
        public static final int drop1 = 2131034118;
        public static final int drop2 = 2131034119;
        public static final int menu_music = 2131034120;
        public static final int move = 2131034121;
        public static final int move_ok = 2131034122;
        public static final int music = 2131034123;
        public static final int nextlevel = 2131034124;
    }

    public static final class string {
        public static final int about = 2131165200;
        public static final int app_name = 2131165197;
        public static final int help = 2131165199;
        public static final int sl_email = 2131165188;
        public static final int sl_error_message_email_already_taken = 2131165195;
        public static final int sl_error_message_invalid_email_format = 2131165196;
        public static final int sl_error_message_name_already_taken = 2131165194;
        public static final int sl_error_message_network = 2131165193;
        public static final int sl_error_message_not_on_highscore_list = 2131165192;
        public static final int sl_highscores = 2131165185;
        public static final int sl_login = 2131165187;
        public static final int sl_next = 2131165190;
        public static final int sl_prev = 2131165189;
        public static final int sl_profile = 2131165184;
        public static final int sl_top = 2131165191;
        public static final int sl_update_profile = 2131165186;
        public static final int start = 2131165198;
    }

    public static final class style {
        public static final int sl_heading = 2131230720;
        public static final int sl_normal = 2131230722;
        public static final int sl_title_bar = 2131230721;
    }
}
