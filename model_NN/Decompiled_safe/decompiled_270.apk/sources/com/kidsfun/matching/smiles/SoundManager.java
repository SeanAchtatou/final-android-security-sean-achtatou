package com.kidsfun.matching.smiles;

import android.content.Context;
import android.media.SoundPool;
import android.os.Vibrator;
import android.util.Log;
import java.util.HashMap;
import java.util.Random;

public abstract class SoundManager {
    public static final int EFFECT_FLAG_CHOOSE = 2;
    public static final int EFFECT_FLAG_MENU = 1;
    private static Random mRand;
    private static HashMap<Integer, Integer> mSoundMap;
    private static boolean mSoundOn;
    private static SoundPool mSoundPool;
    private static boolean mVibrateOn;
    private static Vibrator mVibrator;

    public static void init(Context c) {
        mRand = new Random();
        mSoundPool = new SoundPool(30, 3, 100);
        mSoundMap = new HashMap<>();
        mSoundMap.put(1, Integer.valueOf(mSoundPool.load(c, R.raw.click, 1)));
        mSoundMap.put(2, Integer.valueOf(mSoundPool.load(c, R.raw.choose, 1)));
        if (mVibrator == null) {
            mVibrator = (Vibrator) c.getSystemService("vibrator");
        }
        mSoundOn = true;
    }

    public static void play(int soundId) {
        if (mSoundOn) {
            try {
                mSoundPool.play(mSoundMap.get(Integer.valueOf(soundId)).intValue(), 1.0f, 1.0f, 1, 0, 1.0f);
            } catch (Exception e) {
                Log.d("PlaySounds", e.toString());
            }
            playVibrate();
        }
    }

    public static void playVibrate() {
        if (mVibrateOn && mVibrator != null) {
            mVibrator.vibrate(25);
        }
    }

    public static void setSoundOn(boolean b) {
        mSoundOn = b;
    }

    public static boolean getSoundOn() {
        return mSoundOn;
    }

    public static void setVibrateOn(boolean b) {
        mVibrateOn = b;
    }

    public static boolean getVibrateOn() {
        return mVibrateOn;
    }
}
