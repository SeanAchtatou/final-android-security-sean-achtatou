package com.kidsfun.matching.smiles;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import com.droidlib.net.GameAd;
import java.lang.reflect.Array;
import java.util.Random;

public class GameView extends View implements Runnable {
    private static final int NUMBER_HEITH = 38;
    private static final int NUMBER_IMAGE_HEITH = 38;
    private static final int NUMBER_IMAGE_WIDTH = 264;
    private static final int NUMBER_WIDTH = 24;
    public static int SCREEN_H = 455;
    public static int SCREEN_W = 320;
    static final int ST_PLAYING = 2;
    static int gameState = 2;
    final int BODY_H = 8;
    final int BODY_W = 8;
    int beginDrawX;
    int beginDrawY;
    private Bitmap bg;
    private Bitmap[] block = new Bitmap[8];
    final int blockCount = 8;
    private Bitmap bmTop;
    public int[][] body = ((int[][]) Array.newInstance(Integer.TYPE, 8, 8));
    Bomb bomb;
    final int caseWidth = 40;
    int[][] clear = ((int[][]) Array.newInstance(Integer.TYPE, 8, 8));
    int clearFrame;
    final int clearFrameMax = 8;
    final int clearW = 30;
    Context context;
    int curScore;
    int currentX = -1;
    int currentY = -1;
    private Bitmap cursor1;
    private Bitmap cursor2;
    final int cursorW = 36;
    private int delay = 30;
    private int drawDelay = 50;
    private boolean focus_isShow = true;
    private int focus_show_cnt = 0;
    int gameScore;
    int iDisScore;
    private Bitmap imgBonus;
    private Bitmap imgBonusbar;
    private Bitmap imgBonusbar_fill;
    private Bitmap imgNum;
    private Bitmap imgPane;
    private Bitmap imgScore;
    boolean isClear;
    boolean isDown = true;
    boolean isExchange;
    boolean isReExchange;
    private boolean isResumeLoad;
    public boolean isRunning = true;
    boolean isSelected;
    private int[] mBkgFiles = {R.drawable.img1, R.drawable.img2, R.drawable.img3, R.drawable.img4};
    int mBonus = 1;
    Canvas mCanvas = new Canvas();
    private Typeface mFontFace;
    private int mFrameCounter = 0;
    private boolean mIsShowScoreAdd = false;
    private long mLastDrawTime = (System.currentTimeMillis() - ((long) this.drawDelay));
    int mMatchCnt;
    private GameActivity mParent;
    private int mRotateDegree = 0;
    private long mShowScoreBeginTime = 0;
    int moveFrame;
    final int moveFrameMax = 8;
    final int moveSpeed = 5;
    int nowAddScore;
    Paint paint = new Paint();
    Random random = new Random();
    ScoreBubble scoreBubble;
    int scoreSpace;
    int selectedX;
    int selectedY;
    int[][] tempMove = ((int[][]) Array.newInstance(Integer.TYPE, 8, 8));

    public GameView(Context context2, AttributeSet attrs) {
        super(context2, attrs);
        setFocusable(true);
    }

    public void setParent(GameActivity a2) {
        this.mParent = a2;
    }

    public void setScreen(int w, int h) {
        SCREEN_W = w;
        SCREEN_H = h;
        this.beginDrawX = (320 - 320) >> 1;
        this.beginDrawY = (480 - 320) >> 1;
        this.beginDrawY += 20;
        this.bomb = new Bomb(this.block, 320, 480);
        this.scoreBubble = new ScoreBubble(SCREEN_W, SCREEN_H);
    }

    public void init(boolean isNewGame) {
        this.isResumeLoad = true;
        if (!isNewGame) {
            this.isResumeLoad = false;
            this.mParent.loadResumeData();
        }
        loadFP();
        this.paint.setColor(-1);
        this.paint.setFlags(1);
        this.paint.setFakeBoldText(true);
        this.mMatchCnt = 0;
    }

    public Bitmap createImage(Drawable tile, int w, int h) {
        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        tile.setBounds(0, 0, w, h);
        tile.draw(canvas);
        return bitmap;
    }

    public void loadFP() {
        Resources r = getResources();
        changeBkg();
        this.bg = BitmapFactory.decodeResource(r, R.drawable.bg);
        this.cursor1 = BitmapFactory.decodeResource(r, R.drawable.cursor1);
        this.cursor2 = BitmapFactory.decodeResource(r, R.drawable.cursor2);
        this.imgNum = BitmapFactory.decodeResource(r, R.drawable.num);
        this.imgScore = BitmapFactory.decodeResource(r, R.drawable.score);
        this.imgBonus = BitmapFactory.decodeResource(r, R.drawable.bonus);
        this.imgBonusbar_fill = BitmapFactory.decodeResource(r, R.drawable.bonusbar_fill);
        this.imgBonusbar = BitmapFactory.decodeResource(r, R.drawable.bonusbar);
        this.imgPane = BitmapFactory.decodeResource(r, R.drawable.pane);
        this.scoreSpace = this.imgScore.getWidth();
        this.block[0] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(r, R.drawable.star0), 40, 40, true);
        this.block[1] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(r, R.drawable.star1), 40, 40, true);
        this.block[2] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(r, R.drawable.star2), 40, 40, true);
        this.block[3] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(r, R.drawable.star3), 40, 40, true);
        this.block[4] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(r, R.drawable.star4), 40, 40, true);
        this.block[5] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(r, R.drawable.star5), 40, 40, true);
        this.block[6] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(r, R.drawable.star6), 40, 40, true);
        this.block[7] = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(r, R.drawable.star7), 40, 40, true);
    }

    private void changeBkg() {
        recycleBmp(this.bmTop);
        int idx = this.mBonus % this.mBkgFiles.length;
        Log.v("GameView", "changeBkg(): idx=" + idx);
        this.bmTop = BitmapFactory.decodeResource(getResources(), this.mBkgFiles[idx]);
    }

    public void toState(int state) {
        gameState = state;
    }

    public void logic() {
        this.bomb.move();
        if (this.iDisScore < this.gameScore) {
            this.iDisScore += ((this.gameScore - this.iDisScore) / 2) + 1;
        }
        if (this.isClear) {
            this.clearFrame++;
            if (this.clearFrame >= 4) {
                this.clearFrame = 0;
                this.isClear = false;
                this.isDown = doDown();
            }
        }
        if (this.isDown || this.isExchange || this.isReExchange) {
            this.moveFrame++;
        }
        if (this.isDown && this.moveFrame >= 1) {
            this.moveFrame = 0;
            this.isDown = doDown();
            if (!this.isDown) {
                this.isClear = checkClear();
            }
        }
        if (this.isExchange && this.moveFrame >= 2) {
            this.moveFrame = 0;
            this.isExchange = false;
            Log.v("GameView", "logic(): new tempMove()");
            this.tempMove = (int[][]) Array.newInstance(Integer.TYPE, 8, 8);
            this.isClear = checkClear();
            if (!this.isClear) {
                this.isReExchange = true;
                doExchange();
            }
        }
        if (this.isReExchange && this.moveFrame >= 8) {
            this.tempMove = (int[][]) Array.newInstance(Integer.TYPE, 8, 8);
            this.moveFrame = 0;
            this.isReExchange = false;
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        switch (gameState) {
            case 2:
                paintPlaying(canvas);
                return;
            default:
                return;
        }
    }

    private void doDraw() {
        switch (gameState) {
            case 2:
                paintPlaying(this.mCanvas);
                drawProcessBar(this.mCanvas);
                return;
            default:
                return;
        }
    }

    private void paintSelectFocus(Canvas canvas) {
        if (!this.isSelected) {
            return;
        }
        if (this.focus_isShow) {
            int i = this.focus_show_cnt;
            this.focus_show_cnt = i + 1;
            if (i <= 2) {
                canvas.drawBitmap(this.isSelected ? this.cursor1 : this.cursor2, (float) (this.beginDrawX + (this.currentX * 40)), (float) (this.beginDrawY + (this.currentY * 40)), this.paint);
            } else {
                this.focus_isShow = false;
            }
        } else {
            int i2 = this.focus_show_cnt;
            this.focus_show_cnt = i2 - 1;
            if (i2 <= 0) {
                this.focus_show_cnt = 0;
                this.focus_isShow = true;
            }
        }
    }

    private int getAplhaByIndex(int idx) {
        switch (idx) {
            case SoundManager.EFFECT_FLAG_MENU /*1*/:
                return 200;
            case 2:
                return 190;
            case 3:
                return 180;
            case 4:
                return 170;
            case 5:
                return 150;
            case 6:
                return 140;
            case 7:
                return 120;
            case 8:
                return 100;
            default:
                return 225;
        }
    }

    private void drawNumber(Canvas canvas, int num) {
        int[] numList = {num % 10, (num / 10) % 10, (num / 100) % 10, (num / 1000) % 10, (num / 10000) % 10, (num / 100000) % 10, (num / 1000000) % 10, (num / 10000000) % 10};
        Rect sRect = new Rect();
        Rect dRect = new Rect();
        boolean isDrawZero = true;
        for (int i = numList.length - 1; i >= 0; i--) {
            int n = numList[i];
            if (n != 0 || isDrawZero) {
                isDrawZero = true;
                int x1 = n * NUMBER_WIDTH;
                sRect.set(x1, 0, x1 + NUMBER_WIDTH, 38);
                int i2 = 10 + 5;
                int left = ((8 - i) * 16) + 15;
                dRect.set(left, 5 + 5, left + 20, 5 + 45);
                this.paint.setAlpha(getAplhaByIndex(i));
                canvas.drawBitmap(this.imgNum, sRect, dRect, this.paint);
            }
        }
        this.paint.setAlpha(225);
    }

    private void paintScoreAndBonus(Canvas canvas) {
        canvas.drawBitmap(this.imgPane, 0.0f, 0.0f, this.paint);
        this.paint.setColor(-1);
        this.paint.setTextSize(16.0f);
        drawNumber(canvas, this.gameScore);
        canvas.drawBitmap(this.imgBonus, 228.0f, 41.0f, this.paint);
        canvas.drawText(String.valueOf(this.mBonus) + "x", 275.0f, 60.0f, this.paint);
    }

    private void paintPlaying(Canvas canvas) {
        canvas.clipRect(0, 0, SCREEN_W, SCREEN_H);
        canvas.drawBitmap(this.bmTop, 0.0f, 0.0f, this.paint);
        canvas.drawBitmap(this.bg, (float) this.beginDrawX, (float) (this.beginDrawY - 10), this.paint);
        paintScoreAndBonus(canvas);
        paintSelectFocus(canvas);
        drawProcessBar(canvas);
        for (int i = 0; i < this.tempMove.length; i++) {
            for (int j = 0; j < this.tempMove[i].length; j++) {
                switch (this.tempMove[i][j]) {
                    case SoundManager.EFFECT_FLAG_MENU /*1*/:
                        paintBlock(canvas, i, j, ((i - 1) * 40) + (this.moveFrame * 5), ((j - 1) * 40) + (this.moveFrame * 5));
                        break;
                    case 2:
                        Canvas canvas2 = canvas;
                        paintBlock(canvas2, i, j, i * 40, ((j - 1) * 40) + (this.moveFrame * 5));
                        break;
                    case 3:
                        paintBlock(canvas, i, j, ((i + 1) * 40) - (this.moveFrame * 5), ((j - 1) * 40) + (this.moveFrame * 5));
                        break;
                    case 4:
                        paintBlock(canvas, i, j, ((i - 1) * 40) + (this.moveFrame * 5), j * 40);
                        break;
                    case 5:
                    default:
                        if (this.clear[i][j] != 0) {
                            break;
                        } else {
                            paintBlock(canvas, i, j, i * 40, j * 40);
                            break;
                        }
                    case 6:
                        paintBlock(canvas, i, j, ((i + 1) * 40) - (this.moveFrame * 5), j * 40);
                        break;
                    case 7:
                        paintBlock(canvas, i, j, ((i - 1) * 40) + (this.moveFrame * 5), ((j + 1) * 40) - (this.moveFrame * 5));
                        break;
                    case 8:
                        Canvas canvas3 = canvas;
                        paintBlock(canvas3, i, j, i * 40, ((j + 1) * 40) - (this.moveFrame * 5));
                        break;
                    case 9:
                        paintBlock(canvas, i, j, ((i + 1) * 40) - (this.moveFrame * 5), ((j + 1) * 40) - (this.moveFrame * 5));
                        break;
                }
            }
        }
        this.bomb.paint(canvas, this.paint);
        this.scoreBubble.show(canvas, this.paint);
    }

    public int getRandomBlockId() {
        return Math.abs(this.random.nextInt() % 8) + 1;
    }

    private boolean doDown() {
        boolean isFull = false;
        for (int i = 0; i < this.body.length; i++) {
            int j = this.body[i].length - 1;
            while (true) {
                if (j < 0) {
                    break;
                }
                this.tempMove[i][j] = 0;
                if (this.body[i][j] == 0) {
                    isFull = true;
                    for (int k = j; k >= 0; k--) {
                        this.tempMove[i][k] = 2;
                        if (k != 0) {
                            this.body[i][k] = this.body[i][k - 1];
                        } else if (this.isResumeLoad) {
                            this.body[i][k] = getRandomBlockId();
                        }
                    }
                } else {
                    j--;
                }
            }
        }
        if (!this.isResumeLoad && !isFull) {
            this.isResumeLoad = true;
        }
        return isFull;
    }

    private void doExchange() {
        if (this.currentX - this.selectedX == -1) {
            if (this.currentY - this.selectedY == -1) {
                this.tempMove[this.currentX][this.currentY] = 9;
                this.tempMove[this.selectedX][this.selectedY] = 1;
            } else if (this.currentY - this.selectedY == 0) {
                this.tempMove[this.currentX][this.currentY] = 6;
                this.tempMove[this.selectedX][this.selectedY] = 4;
            } else if (this.currentY - this.selectedY == 1) {
                this.tempMove[this.currentX][this.currentY] = 3;
                this.tempMove[this.selectedX][this.selectedY] = 7;
            }
        } else if (this.currentX - this.selectedX == 0) {
            if (this.currentY - this.selectedY == -1) {
                this.tempMove[this.currentX][this.currentY] = 8;
                this.tempMove[this.selectedX][this.selectedY] = 2;
            } else if (this.currentY - this.selectedY == 1) {
                this.tempMove[this.currentX][this.currentY] = 2;
                this.tempMove[this.selectedX][this.selectedY] = 8;
            }
        } else if (this.currentX - this.selectedX == 1) {
            if (this.currentY - this.selectedY == -1) {
                this.tempMove[this.currentX][this.currentY] = 7;
                this.tempMove[this.selectedX][this.selectedY] = 3;
            } else if (this.currentY - this.selectedY == 0) {
                this.tempMove[this.currentX][this.currentY] = 4;
                this.tempMove[this.selectedX][this.selectedY] = 6;
            } else if (this.currentY - this.selectedY == 1) {
                this.tempMove[this.currentX][this.currentY] = 1;
                this.tempMove[this.selectedX][this.selectedY] = 9;
            }
        }
        int temp = this.body[this.selectedX][this.selectedY];
        this.body[this.selectedX][this.selectedY] = this.body[this.currentX][this.currentY];
        this.body[this.currentX][this.currentY] = temp;
    }

    private boolean checkClear() {
        for (int i = 0; i < this.body.length; i++) {
            for (int j = 0; j < this.body[i].length; j++) {
                if (j > 0 && j < this.body[i].length - 1 && this.body[i][j] == this.body[i][j - 1] && this.body[i][j] == this.body[i][j + 1]) {
                    int[] iArr = this.clear[i];
                    this.clear[i][j + 1] = 1;
                    this.clear[i][j - 1] = 1;
                    iArr[j] = 1;
                }
                if (i > 0 && i < this.body.length - 1 && this.body[i][j] == this.body[i - 1][j] && this.body[i][j] == this.body[i + 1][j]) {
                    int[] iArr2 = this.clear[i];
                    int[] iArr3 = this.clear[i - 1];
                    this.clear[i + 1][j] = 1;
                    iArr3[j] = 1;
                    iArr2[j] = 1;
                }
            }
        }
        boolean clearBlock = false;
        int cnt = 0;
        int left = 0;
        int top = 0;
        for (int i2 = 0; i2 < 8; i2++) {
            for (int j2 = 0; j2 < 8; j2++) {
                if (this.clear[i2][j2] == 1) {
                    clearBlock = true;
                    left = this.beginDrawX + (i2 * 40);
                    top = this.beginDrawY + (j2 * 40);
                    this.bomb.add(left, top, this.body[i2][j2] - 1);
                    int[] iArr4 = this.clear[i2];
                    this.body[i2][j2] = 0;
                    iArr4[j2] = 0;
                    cnt++;
                }
            }
        }
        if (clearBlock) {
            addScore(cnt, left, top);
            this.mParent.playVibrate();
        }
        return clearBlock;
    }

    private void addScore(int cnt, int x, int y) {
        if (cnt >= 3) {
            GameActivity gameActivity = this.mParent;
            this.mParent.mGameSound.getClass();
            gameActivity.playSP(3);
            this.mIsShowScoreAdd = true;
            this.mShowScoreBeginTime = System.currentTimeMillis();
            this.nowAddScore = (cnt - 2) * this.mBonus * 10;
            this.gameScore += this.nowAddScore;
            this.curScore += this.nowAddScore;
            this.mMatchCnt++;
            this.scoreBubble.Add(x, y, this.nowAddScore);
            if (this.curScore >= getCurLevelScore()) {
                this.curScore -= getCurLevelScore();
                this.mMatchCnt = 0;
                updateLevel();
                GameActivity gameActivity2 = this.mParent;
                this.mParent.mGameSound.getClass();
                gameActivity2.playSP(4);
            }
        }
    }

    private void updateLevel() {
        this.mBonus++;
        changeBkg();
        this.mParent.playMusic();
    }

    public int getCurLevelScore() {
        return this.mBonus * 10 * 50;
    }

    private void resetRotate() {
        this.mFrameCounter = 0;
        this.mRotateDegree = 0;
    }

    private int getCurRotateDegree() {
        int i = this.mFrameCounter + 1;
        this.mFrameCounter = i;
        if (i >= 1) {
            this.mFrameCounter = 0;
            this.mRotateDegree += 36;
            if (this.mRotateDegree >= 360) {
                this.mRotateDegree = 0;
            }
        }
        return this.mRotateDegree;
    }

    private void paintBlock(Canvas canvas, int r, int l, int x, int y) {
        int index = this.body[r][l] - 1;
        if (index >= 0 && index < this.block.length) {
            int i = this.beginDrawX + 320;
            int i2 = this.beginDrawY + 320;
            canvas.save();
            canvas.clipRect(this.beginDrawX + x, this.beginDrawY + y, this.beginDrawX + x + 40, this.beginDrawY + y + 40);
            if (this.isSelected && r == this.currentX && l == this.currentY) {
                canvas.rotate((float) getCurRotateDegree(), (float) (this.beginDrawX + x + 20), (float) (this.beginDrawY + y + 20));
            }
            canvas.drawBitmap(this.block[index], (float) (this.beginDrawX + x), (float) (this.beginDrawY + y), this.paint);
            canvas.restore();
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case GameAd.MORE_GAME_ID:
                if (!this.isDown && !this.isExchange && !this.isReExchange && this.moveFrame == 0 && !this.isClear) {
                    if (event.getX() > ((float) this.beginDrawX) && event.getX() < ((float) (this.beginDrawX + 320)) && event.getY() > ((float) this.beginDrawY) && event.getY() < ((float) (this.beginDrawY + 320))) {
                        if (!this.isSelected) {
                            this.isSelected = true;
                            this.focus_isShow = true;
                            this.focus_show_cnt = 0;
                            this.currentX = (int) ((event.getX() - ((float) this.beginDrawX)) / 40.0f);
                            this.currentY = (int) ((event.getY() - ((float) this.beginDrawY)) / 40.0f);
                            this.selectedX = this.currentX;
                            this.selectedY = this.currentY;
                            SoundManager.play(2);
                            resetRotate();
                            break;
                        } else {
                            int tempX = (int) ((event.getX() - ((float) this.beginDrawX)) / 40.0f);
                            int tempY = (int) ((event.getY() - ((float) this.beginDrawY)) / 40.0f);
                            boolean isSound = true;
                            if (tempX > this.selectedX) {
                                if (tempY > this.selectedY) {
                                    moveRightDown();
                                } else if (tempY == this.selectedY) {
                                    moveRight();
                                } else if (tempY < this.selectedY) {
                                    moveRightUp();
                                }
                            } else if (tempX == this.selectedX) {
                                if (tempY > this.selectedY) {
                                    moveDown();
                                } else if (tempY == this.selectedY) {
                                    this.isSelected = false;
                                    isSound = false;
                                } else if (tempY < this.selectedY) {
                                    moveUp();
                                }
                            } else if (tempX < this.selectedX) {
                                if (tempY > this.selectedY) {
                                    moveLeftDown();
                                } else if (tempY == this.selectedY) {
                                    moveLeft();
                                } else if (tempY < this.selectedY) {
                                    moveLeftUp();
                                }
                            }
                            if (isSound) {
                                GameActivity gameActivity = this.mParent;
                                this.mParent.mGameSound.getClass();
                                gameActivity.playSP(2);
                                break;
                            }
                        }
                    }
                } else {
                    return super.onTouchEvent(event);
                }
                break;
        }
        return super.onTouchEvent(event);
    }

    public boolean onKeyDown(int keyCode, KeyEvent msg) {
        if (this.isDown || this.isExchange || this.isReExchange || this.moveFrame != 0 || this.isClear) {
            return super.onKeyDown(keyCode, msg);
        }
        if (keyCode == 23) {
            if (this.isDown || this.isExchange || this.isReExchange) {
                return super.onKeyDown(keyCode, msg);
            }
            this.isSelected = !this.isSelected;
            this.selectedX = this.currentX;
            this.selectedY = this.currentY;
        } else if (this.isExchange || this.isReExchange) {
            return super.onKeyDown(keyCode, msg);
        } else {
            if (keyCode == 19) {
                moveUp();
            } else if (keyCode == 20) {
                moveDown();
            } else if (keyCode == 21) {
                moveLeft();
            } else if (keyCode == 22) {
                moveRight();
            }
        }
        return super.onKeyDown(keyCode, msg);
    }

    public synchronized void setExchange() {
        if (this.isSelected) {
            this.isExchange = true;
            this.isSelected = false;
            doExchange();
        }
    }

    private void moveRightDown() {
        if (this.currentX == 7 || this.currentY == 7) {
            this.isSelected = false;
        }
        int i = this.currentX + 1;
        this.currentX = i;
        this.currentX = i % 8;
        int i2 = this.currentY + 1;
        this.currentY = i2;
        this.currentY = i2 % 8;
        setExchange();
    }

    private void moveLeftDown() {
        if (this.currentX == 0 || this.currentY == 7) {
            this.isSelected = false;
        }
        int i = this.currentX - 1;
        this.currentX = i;
        this.currentX = (i + 8) % 8;
        int i2 = this.currentY + 1;
        this.currentY = i2;
        this.currentY = i2 % 8;
        setExchange();
    }

    private void moveRightUp() {
        if (this.currentY == 0 || this.currentX == 7) {
            this.isSelected = false;
        }
        int i = this.currentX + 1;
        this.currentX = i;
        this.currentX = i % 8;
        int i2 = this.currentY - 1;
        this.currentY = i2;
        this.currentY = (i2 + 8) % 8;
        setExchange();
    }

    private void moveLeftUp() {
        if (this.currentX == 0 || this.currentY == 0) {
            this.isSelected = false;
        }
        int i = this.currentX - 1;
        this.currentX = i;
        this.currentX = (i + 8) % 8;
        int i2 = this.currentY - 1;
        this.currentY = i2;
        this.currentY = (i2 + 8) % 8;
        setExchange();
    }

    private void moveUp() {
        if (this.currentY == 0) {
            this.isSelected = false;
        }
        int i = this.currentY - 1;
        this.currentY = i;
        this.currentY = (i + 8) % 8;
        setExchange();
    }

    private void moveDown() {
        if (this.currentY == 7) {
            this.isSelected = false;
        }
        int i = this.currentY + 1;
        this.currentY = i;
        this.currentY = i % 8;
        setExchange();
    }

    private void moveLeft() {
        if (this.currentX == 0) {
            this.isSelected = false;
        }
        int i = this.currentX - 1;
        this.currentX = i;
        this.currentX = (i + 8) % 8;
        setExchange();
    }

    private void moveRight() {
        if (this.currentX == 7) {
            this.isSelected = false;
        }
        int i = this.currentX + 1;
        this.currentX = i;
        this.currentX = i % 8;
        setExchange();
    }

    public void run() {
        logic();
        if (System.currentTimeMillis() - this.mLastDrawTime >= ((long) this.drawDelay)) {
            invalidate();
            this.mLastDrawTime = System.currentTimeMillis();
        }
        if (this.isRunning) {
            postDelayed(this, (long) this.delay);
        }
    }

    private void drawProcessBar(Canvas c) {
        BitmapDrawable d1 = new BitmapDrawable(this.imgBonusbar);
        d1.setBounds(6, 65, 308, 65 + 12);
        d1.draw(c);
        BitmapDrawable d2 = new BitmapDrawable(this.imgBonusbar_fill);
        d2.setBounds(6 + 1, 65, (int) ((306.0f * ((float) this.curScore)) / ((float) getCurLevelScore())), 65 + 12);
        d2.draw(c);
    }

    public void restoreData(int level, int score, int curScore2) {
        this.mBonus = level;
        this.gameScore = score;
    }

    public void freeObject() {
        recycleBmp(this.imgBonusbar_fill);
        recycleBmp(this.imgBonusbar);
        recycleBmp(this.bg);
        recycleBmp(this.bmTop);
        recycleBmp(this.imgScore);
        recycleBmp(this.imgBonus);
        for (Bitmap recycleBmp : this.block) {
            recycleBmp(recycleBmp);
        }
    }

    private void recycleBmp(Bitmap bmp) {
        if (bmp != null && !bmp.isRecycled()) {
            bmp.recycle();
        }
    }
}
