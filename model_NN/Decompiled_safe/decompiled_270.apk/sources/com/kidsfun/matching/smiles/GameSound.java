package com.kidsfun.matching.smiles;

import java.util.Random;

public class GameSound {
    public final int ST_CLICK = 6;
    public final int ST_DROP = 5;
    public final int ST_LEVEL = 4;
    public final int ST_MAX = 7;
    public final int ST_MOVE = 2;
    public final int ST_MOVE_OK = 3;
    public final int ST_MUSIC = 1;
    public boolean mMusicOn;
    private Random mRand = new Random();
    public boolean mSoundOn;
    public boolean mVibrateOn;
    private int[] musicId = {R.raw.bg_01};

    public GameSound(boolean bMusicOn, boolean bSoundOn, boolean bVibrateOn) {
        this.mMusicOn = bMusicOn;
        this.mSoundOn = bSoundOn;
        this.mVibrateOn = bVibrateOn;
    }

    public int getSoundResIdByType(int st) {
        if (st == 1) {
            return this.musicId[this.mRand.nextInt(this.musicId.length)];
        }
        if (st == 2) {
            return R.raw.move;
        }
        if (st == 3) {
            return R.raw.move_ok;
        }
        if (st == 4) {
            return R.raw.nextlevel;
        }
        if (st == 5) {
            return R.raw.drop1;
        }
        if (st == 6) {
            return R.raw.click;
        }
        return 0;
    }
}
