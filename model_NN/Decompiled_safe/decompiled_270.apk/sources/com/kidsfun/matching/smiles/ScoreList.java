package com.kidsfun.matching.smiles;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import java.util.ArrayList;
import java.util.HashMap;

public class ScoreList extends Activity {
    private final int[] mImgOrder = {R.drawable.n1, R.drawable.n2, R.drawable.n3, R.drawable.n4, R.drawable.n5, R.drawable.n6, R.drawable.n7, R.drawable.n8, R.drawable.n9};
    private ListView mList;
    private int[] mScoreArray = new int[10];
    private Window window;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        this.window = getWindow();
        this.window.setFlags(1024, 1024);
        setContentView((int) R.layout.lv_score);
        setTitle("High Score");
        loadHightScore();
        ArrayList<HashMap<String, Object>> users = new ArrayList<>();
        for (int i = 0; i < 9; i++) {
            HashMap<String, Object> user = new HashMap<>();
            int score = this.mScoreArray[i];
            user.put("img", Integer.valueOf(this.mImgOrder[i]));
            if (score == 0) {
                user.put("name", "");
            } else {
                user.put("name", Integer.toString(score));
            }
            users.add(user);
        }
        Log.v("main", "users.add");
        SimpleAdapter saImageItems = new SimpleAdapter(this, users, R.layout.lv_score_raw, new String[]{"img", "name"}, new int[]{R.id.img_order, R.id.txt_score});
        Log.v("main", "new SimpleAdapter");
        this.mList = (ListView) findViewById(R.id.lv_score);
        this.mList.setAdapter((ListAdapter) saImageItems);
        this.mList.setItemsCanFocus(true);
        this.mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
            }
        });
        ((Button) findViewById(R.id.btn_back)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ScoreList.this.finish();
            }
        });
    }

    private void loadHightScore() {
        SharedPreferences settings = getSharedPreferences(ConstInfo.PREFS_NAME, 0);
        for (int i = 0; i < 9; i++) {
            this.mScoreArray[i] = settings.getInt("highScore" + Integer.toString(i), 0);
        }
    }
}
