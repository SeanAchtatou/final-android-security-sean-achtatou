package com.kidsfun.matching.smiles;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import java.util.ArrayList;
import java.util.HashMap;

class MoreGame {
    /* access modifiers changed from: private */
    public AlertDialog mDlg;
    private Activity mParent;

    public MoreGame(Activity parent) {
        if (parent != null) {
            this.mParent = parent;
        }
    }

    public void show() {
        View entryDlg = LayoutInflater.from(this.mParent).inflate((int) R.layout.popup_lv, (ViewGroup) null);
        this.mDlg = new AlertDialog.Builder(this.mParent).setView(entryDlg).create();
        ListView lv = (ListView) entryDlg.findViewById(R.id.lv);
        ArrayList<HashMap<String, Object>> users = new ArrayList<>();
        HashMap<String, Object> rec1 = new HashMap<>();
        HashMap<String, Object> rec2 = new HashMap<>();
        HashMap<String, Object> rec3 = new HashMap<>();
        HashMap<String, Object> rec4 = new HashMap<>();
        HashMap<String, Object> rec5 = new HashMap<>();
        rec1.put("img", Integer.valueOf((int) R.drawable.app0));
        rec1.put("name", "Motris(Full)");
        users.add(rec1);
        rec2.put("img", Integer.valueOf((int) R.drawable.app1));
        rec2.put("name", "Zombie Wars");
        users.add(rec2);
        rec3.put("img", Integer.valueOf((int) R.drawable.app2));
        rec3.put("name", "Super Mario Memory");
        users.add(rec3);
        rec4.put("img", Integer.valueOf((int) R.drawable.app4));
        rec4.put("name", "More Game");
        users.add(rec4);
        rec5.put("img", Integer.valueOf((int) R.drawable.app4));
        rec5.put("name", "Back");
        users.add(rec5);
        lv.setAdapter((ListAdapter) new SimpleAdapter(this.mParent, users, R.layout.popup_lv_item, new String[]{"img", "name"}, new int[]{R.id.img_icon, R.id.tv_name}));
        lv.setBackgroundColor(-16777216);
        lv.setItemsCanFocus(true);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                int index = arg2;
                if (index >= 4 || index < 0) {
                    MoreGame.this.mDlg.dismiss();
                } else {
                    MoreGame.this.goApp(index);
                }
            }
        });
        this.mDlg.show();
    }

    public void goApp(int adId) {
        String url;
        new String();
        if (adId == 0) {
            url = "market://details?id=com.feasy.game.blocks.magical";
        } else if (1 == adId) {
            url = "market://details?id=com.feasy.game.hunt.zombie_ad";
        } else if (2 == adId) {
            url = "market://details?id=com.gmail.at.vrdream.android.memory.mario2";
        } else {
            url = "http://market.android.com/search?q=pub:\"Jazz Jackrabbit\"";
        }
        this.mParent.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
    }
}
