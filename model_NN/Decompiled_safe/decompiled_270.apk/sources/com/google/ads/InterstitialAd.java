package com.google.ads;

import android.app.Activity;
import com.google.ads.util.a;

public class InterstitialAd implements Ad {

    /* renamed from: a  reason: collision with root package name */
    private d f25a;

    public InterstitialAd(Activity activity, String adUnitId) {
        this(activity, adUnitId, false);
    }

    public InterstitialAd(Activity activity, String adUnitId, boolean shortTimeout) {
        this.f25a = new d(activity, this, null, adUnitId, shortTimeout);
    }

    public boolean isReady() {
        return this.f25a.o();
    }

    public void loadAd(AdRequest adRequest) {
        this.f25a.a(adRequest);
    }

    public void setAdListener(AdListener adListener) {
        this.f25a.a(adListener);
    }

    public void show() {
        if (isReady()) {
            this.f25a.y();
            this.f25a.v();
            AdActivity.launchAdActivity(this.f25a, new e("interstitial"));
            return;
        }
        a.c("Cannot show interstitial because it is not loaded and ready.");
    }

    public void stopLoading() {
        this.f25a.z();
    }
}
