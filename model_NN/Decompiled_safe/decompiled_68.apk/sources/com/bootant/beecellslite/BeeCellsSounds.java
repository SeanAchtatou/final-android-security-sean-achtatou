package com.bootant.beecellslite;

public class BeeCellsSounds {
    public static final int[] SOUND_FILES = {R.raw.snd_score, R.raw.snd_big_score, R.raw.snd_giant_score, R.raw.snd_no_score, R.raw.snd_drop, R.raw.snd_move, R.raw.snd_jump, R.raw.snd_no_way, R.raw.snd_game_over, R.raw.snd_command};
}
