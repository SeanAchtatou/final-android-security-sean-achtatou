package com.bootant.beecellslite;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.opengl.GLES10;
import android.opengl.GLSurfaceView;
import android.os.SystemClock;
import android.util.Log;
import android.view.MotionEvent;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL10Ext;
import javax.microedition.khronos.opengles.GL11;
import javax.microedition.khronos.opengles.GL11Ext;

/* compiled from: BeeCellsLite */
class BeeCellsLiteViewRenderer implements GLSurfaceView.Renderer {
    public static final String MARKET_URL = "market://details?id=com.bootant.beecells";
    static final String leaderboardId = "860386";
    public Context context;
    public int count = 0;
    public float delta_time = 0.033333335f;
    public float game_ratio = (((float) this.h_game) / ((float) this.w_game));
    public int h_game = 480;
    public int h_view = 480;
    SoundPoolManager soundMng;
    public float time_coeff = 8.333333E-5f;
    public long time_ms;
    public long touch_delay = 60;
    public int touch_state = 0;
    public long touch_time = 0;
    public int w_game = 320;
    public int w_view = 320;
    public float x_ratio = 1.0f;
    public int x_shift = 0;
    public float y_ratio = 1.0f;
    public int y_shift = 0;

    public native int LibGetRecordScore();

    public native boolean LibGetScoreSubmit();

    public native boolean LibGetShowAchievements();

    public native int LibGetSoundIdx();

    public native void LibInit(float f, float f2, float f3, float f4);

    public native int LibOpenUrl();

    public native void LibTouchBegin(int i, int i2, int i3, int i4);

    public native void LibTouchEnd(int i, int i2, int i3, int i4);

    public native void LibTouchMoved(int i, int i2, int i3, int i4);

    public native void LibUpdate(float f);

    BeeCellsLiteViewRenderer() {
    }

    public void Init(Context _context) {
        this.context = _context;
        this.soundMng = new SoundPoolManager();
        this.soundMng.initBeeCellsSounds(_context);
        this.time_ms = SystemClock.uptimeMillis();
    }

    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
    }

    public void onSurfaceChanged(GL10 gl, int w, int h) {
        if (((float) h) / ((float) w) >= this.game_ratio) {
            this.w_view = w;
            this.h_view = (int) (((float) w) * this.game_ratio);
            this.y_shift = (h - this.h_view) / 2;
        } else {
            this.w_view = (int) (((float) h) / this.game_ratio);
            this.h_view = h;
            this.x_shift = (w - this.w_view) / 2;
        }
        this.x_ratio = ((float) this.w_game) / ((float) this.w_view);
        this.y_ratio = ((float) this.h_game) / ((float) this.h_view);
        gl.glViewport(this.x_shift, this.y_shift, this.w_view, this.h_view);
        try {
            LibInit(0.0f, 0.0f, (float) this.w_game, (float) this.h_game);
        } catch (Exception e) {
            Log.e("LibInit", e.getMessage());
        }
    }

    public void printGLVerion(GL10 gl) {
        if (gl instanceof GL10Ext) {
            Log.e("GL", "GL10");
        }
        if (gl instanceof GL11) {
            Log.e("GL", "GL11");
        }
        if (gl instanceof GL11Ext) {
            Log.e("GL", "GL11Ext");
        }
        if (gl instanceof GLES10) {
            Log.e("GL", "GLES10");
        }
    }

    public void onDrawFrame(GL10 gl) {
        adjustDeltaTime();
        LibUpdate(this.delta_time);
        OpenBrowser();
        try {
            playSound(LibGetSoundIdx());
        } catch (Exception e) {
        }
    }

    public void playSound(int i) {
        if (i > -1) {
            try {
                this.soundMng.playSound(i);
            } catch (Exception e) {
            }
        }
    }

    public void processTouchEvent(MotionEvent event) {
        int actionCode = event.getAction() & 255;
        int pointer_count = event.getPointerCount();
        for (int i = 0; i < pointer_count; i++) {
            int x = 0;
            int y = 0;
            if (actionCode >= 0 && actionCode < 3) {
                x = (int) ((event.getX(i) - ((float) this.x_shift)) * this.x_ratio);
                y = (int) ((event.getY(i) - ((float) this.y_shift)) * this.y_ratio);
            }
            if (actionCode == 0) {
                LibTouchBegin(event.getPointerId(i), x, y, pointer_count);
                this.touch_state = 1;
            } else if (actionCode == 1) {
                LibTouchEnd(event.getPointerId(i), x, y, pointer_count);
                this.touch_state = 0;
                this.touch_time = SystemClock.uptimeMillis();
            } else if (actionCode == 2) {
                if (this.touch_state != 0) {
                    LibTouchMoved(event.getPointerId(i), x, y, pointer_count);
                    this.touch_state = 1;
                } else if (SystemClock.uptimeMillis() - this.touch_time > this.touch_delay) {
                    LibTouchBegin(event.getPointerId(i), x, y, pointer_count);
                    this.touch_state = 1;
                }
            }
        }
    }

    private void adjustDeltaTime() {
        if (this.count > 10) {
            long now_ms = SystemClock.uptimeMillis();
            this.delta_time = this.time_coeff * ((float) (now_ms - this.time_ms));
            this.count = 0;
            this.time_ms = now_ms;
        }
        this.count++;
    }

    private void OpenBrowser() {
        if (LibOpenUrl() > 0) {
            Intent i = new Intent("android.intent.action.VIEW");
            i.setData(Uri.parse(MARKET_URL));
            this.context.startActivity(i);
        }
    }
}
