package com.bootant.beecellslite;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int icon = 2130837504;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class raw {
        public static final int snd_big_score = 2130968576;
        public static final int snd_command = 2130968577;
        public static final int snd_drop = 2130968578;
        public static final int snd_game_over = 2130968579;
        public static final int snd_giant_score = 2130968580;
        public static final int snd_jump = 2130968581;
        public static final int snd_move = 2130968582;
        public static final int snd_no_score = 2130968583;
        public static final int snd_no_way = 2130968584;
        public static final int snd_score = 2130968585;
    }

    public static final class string {
        public static final int app_name = 2131034113;
        public static final int hello = 2131034112;
    }
}
