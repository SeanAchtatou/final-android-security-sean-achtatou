package com.bootant.beecellslite;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.opengl.GLSurfaceView;
import android.util.Log;
import android.view.MotionEvent;

public class BeeCellsLite extends Activity {
    public static final String FIELD_STRING = "FIELD_STRING";
    public static final String PLAY_SOUND = "PLAY_SOUND";
    public static final String RECORD_SCORE = "RECORD_SCORE";
    public static final String RESTORE_GAME = "RESTORE_GAME";
    public static final String RESTORE_VERSION = "RESTORE_VERSION";
    public static final String STATE_STRING = "STATE_STRING";
    static final String gameID = "334203";
    static final String gameKey = "PH8z7CDpNxiezOaqe0x4wA";
    static final String gameName = "MyOpenFeintSample";
    static final String gameSecret = "vcsswTJPl4h02TijmvXTuW8xNNMjvb7ojCJnVFs38";
    BeeCellsView beeCellsView;

    public native void LibClose();

    public native boolean LibGetColorBlindMode();

    public native int LibGetCurrentField();

    public native String LibGetFieldString();

    public native boolean LibGetPlaySound();

    public native int LibGetRecordScore();

    public native boolean LibGetRestoreGame();

    public native String LibGetStateString();

    public native void LibOpen();

    public native void LibSetApkFile(String str);

    public native void LibSetColorBlindMode(boolean z);

    public native void LibSetCurrentField(int i);

    public native void LibSetFieldString(String str);

    public native void LibSetGameCenterAvailable(boolean z);

    public native void LibSetMenu();

    public native void LibSetPath(String str);

    public native void LibSetPlaySound(boolean z);

    public native void LibSetRecordScore(int i);

    public native void LibSetRestoreGame(boolean z);

    public native void LibSetStateString(String str);

    static {
        System.loadLibrary("beecellslite");
    }

    public void onStart() {
        super.onStart();
        LibOpen();
        try {
            LibSetApkFile(getPackageManager().getPackageInfo("com.bootant.beecellslite", 0).applicationInfo.sourceDir);
            LibSetPath("assets");
        } catch (Exception e) {
            Log.e("Bee", e.getMessage());
        }
        this.beeCellsView = new BeeCellsView(this);
        setContentView(this.beeCellsView);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.beeCellsView.writeRecord();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.beeCellsView.readRecord();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        LibClose();
    }

    class BeeCellsView extends GLSurfaceView {
        BeeCellsLiteViewRenderer renderer = new BeeCellsLiteViewRenderer();

        BeeCellsView(Context context) {
            super(context);
            this.renderer.Init(context);
            setRenderer(this.renderer);
        }

        public boolean onTouchEvent(final MotionEvent event) {
            queueEvent(new Runnable() {
                public void run() {
                    BeeCellsView.this.renderer.processTouchEvent(event);
                }
            });
            return true;
        }

        /* access modifiers changed from: protected */
        public void writeRecord() {
            int record_score = BeeCellsLite.this.LibGetRecordScore();
            boolean restore_game = BeeCellsLite.this.LibGetRestoreGame();
            boolean play_sound = BeeCellsLite.this.LibGetPlaySound();
            SharedPreferences.Editor prefsEditor = BeeCellsLite.this.getPreferences(0).edit();
            prefsEditor.putInt(BeeCellsLite.RECORD_SCORE, record_score).commit();
            prefsEditor.putBoolean(BeeCellsLite.RESTORE_GAME, restore_game).commit();
            prefsEditor.putBoolean(BeeCellsLite.PLAY_SOUND, play_sound).commit();
            if (restore_game) {
                prefsEditor.putString(BeeCellsLite.FIELD_STRING, BeeCellsLite.this.LibGetFieldString()).commit();
                prefsEditor.putString(BeeCellsLite.STATE_STRING, BeeCellsLite.this.LibGetStateString()).commit();
            }
        }

        /* access modifiers changed from: protected */
        public void readRecord() {
            SharedPreferences prefs = BeeCellsLite.this.getPreferences(0);
            int restore_ver = prefs.getInt(BeeCellsLite.RESTORE_VERSION, 11);
            int record_score = prefs.getInt(BeeCellsLite.RECORD_SCORE, 0);
            boolean restore_game = prefs.getBoolean(BeeCellsLite.RESTORE_GAME, false);
            boolean play_sound = prefs.getBoolean(BeeCellsLite.PLAY_SOUND, false);
            BeeCellsLite.this.LibSetRecordScore(record_score);
            BeeCellsLite.this.LibSetPlaySound(play_sound);
            if (restore_ver == 11) {
                BeeCellsLite.this.LibSetRestoreGame(restore_game);
                if (restore_game) {
                    BeeCellsLite.this.LibSetFieldString(prefs.getString(BeeCellsLite.FIELD_STRING, ""));
                    BeeCellsLite.this.LibSetStateString(prefs.getString(BeeCellsLite.STATE_STRING, ""));
                }
            }
        }
    }
}
