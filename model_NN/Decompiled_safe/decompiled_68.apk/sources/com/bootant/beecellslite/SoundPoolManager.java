package com.bootant.beecellslite;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import java.util.HashMap;

public class SoundPoolManager {
    private AudioManager mAudioManager;
    private Context mContext;
    private SoundPool mSoundPool;
    private HashMap<Integer, Integer> mSoundPoolMap;
    private int mStreamVolume;

    public void initSounds(Context theContext) {
        this.mContext = theContext;
        this.mSoundPool = new SoundPool(4, 3, 0);
        this.mSoundPoolMap = new HashMap<>();
        this.mAudioManager = (AudioManager) this.mContext.getSystemService("audio");
        this.mStreamVolume = this.mAudioManager.getStreamVolume(3);
    }

    public void initBeeCellsSounds(Context theContext) {
        initSounds(theContext);
        for (int i = 0; i < BeeCellsSounds.SOUND_FILES.length; i++) {
            addSound(i, BeeCellsSounds.SOUND_FILES[i]);
        }
    }

    public void addSound(int index, int soundID) {
        this.mSoundPoolMap.put(Integer.valueOf(index), Integer.valueOf(this.mSoundPool.load(this.mContext, soundID, 1)));
    }

    public void playSound(int index) {
        this.mSoundPool.play(this.mSoundPoolMap.get(Integer.valueOf(index)).intValue(), (float) this.mStreamVolume, (float) this.mStreamVolume, 1, 0, 1.0f);
    }

    public void playLoopedSound(int index) {
        int streamVolume = this.mAudioManager.getStreamVolume(3);
        this.mSoundPool.play(this.mSoundPoolMap.get(Integer.valueOf(index)).intValue(), (float) streamVolume, (float) streamVolume, 1, -1, 1.0f);
    }
}
