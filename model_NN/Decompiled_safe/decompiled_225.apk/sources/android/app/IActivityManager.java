package android.app;

import android.app.ActivityManager;
import android.app.ApplicationErrorReport;
import android.content.ComponentName;
import android.content.ContentProviderNative;
import android.content.IContentProvider;
import android.content.IIntentReceiver;
import android.content.IIntentSender;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.ApplicationInfo;
import android.content.pm.ConfigurationInfo;
import android.content.pm.IPackageDataObserver;
import android.content.pm.ProviderInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Debug;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import android.os.RemoteException;
import java.util.List;

public interface IActivityManager extends IInterface {
    public static final int ACTIVITY_DESTROYED_TRANSACTION = 62;
    public static final int ACTIVITY_IDLE_TRANSACTION = 18;
    public static final int ACTIVITY_PAUSED_TRANSACTION = 19;
    public static final int ACTIVITY_STOPPED_TRANSACTION = 20;
    public static final int ATTACH_APPLICATION_TRANSACTION = 17;
    public static final int BACKUP_AGENT_CREATED_TRANSACTION = 91;
    public static final int BIND_SERVICE_TRANSACTION = 36;
    public static final int BROADCAST_INTENT_TRANSACTION = 14;
    public static final int BROADCAST_STICKY_CANT_HAVE_PERMISSION = -1;
    public static final int BROADCAST_SUCCESS = 0;
    public static final int CANCEL_INTENT_SENDER_TRANSACTION = 64;
    public static final int CHECK_PERMISSION_TRANSACTION = 53;
    public static final int CHECK_URI_PERMISSION_TRANSACTION = 54;
    public static final int CLEAR_APP_DATA_TRANSACTION = 78;
    public static final int CLOSE_SYSTEM_DIALOGS_TRANSACTION = 97;
    public static final int CRASH_APPLICATION_TRANSACTION = 114;
    public static final int ENTER_SAFE_MODE_TRANSACTION = 66;
    public static final int FINISH_ACTIVITY_TRANSACTION = 11;
    public static final int FINISH_HEAVY_WEIGHT_APP_TRANSACTION = 109;
    public static final int FINISH_INSTRUMENTATION_TRANSACTION = 45;
    public static final int FINISH_OTHER_INSTANCES_TRANSACTION = 39;
    public static final int FINISH_RECEIVER_TRANSACTION = 16;
    public static final int FINISH_SUB_ACTIVITY_TRANSACTION = 32;
    public static final int FORCE_STOP_PACKAGE_TRANSACTION = 79;
    public static final int GET_ACTIVITY_CLASS_FOR_TOKEN_TRANSACTION = 49;
    public static final int GET_CALLING_ACTIVITY_TRANSACTION = 22;
    public static final int GET_CALLING_PACKAGE_TRANSACTION = 21;
    public static final int GET_CONFIGURATION_TRANSACTION = 46;
    public static final int GET_CONTENT_PROVIDER_TRANSACTION = 29;
    public static final int GET_DEVICE_CONFIGURATION_TRANSACTION = 84;
    public static final int GET_INTENT_SENDER_TRANSACTION = 63;
    public static final int GET_MEMORY_INFO_TRANSACTION = 76;
    public static final int GET_PACKAGE_FOR_INTENT_SENDER_TRANSACTION = 65;
    public static final int GET_PACKAGE_FOR_TOKEN_TRANSACTION = 50;
    public static final int GET_PROCESSES_IN_ERROR_STATE_TRANSACTION = 77;
    public static final int GET_PROCESS_LIMIT_TRANSACTION = 52;
    public static final int GET_PROCESS_MEMORY_INFO_TRANSACTION = 98;
    public static final int GET_PROVIDER_MIME_TYPE_TRANSACTION = 115;
    public static final int GET_RECENT_TASKS_TRANSACTION = 60;
    public static final int GET_REQUESTED_ORIENTATION_TRANSACTION = 71;
    public static final int GET_RUNNING_APP_PROCESSES_TRANSACTION = 83;
    public static final int GET_RUNNING_EXTERNAL_APPLICATIONS_TRANSACTION = 108;
    public static final int GET_RUNNING_SERVICE_CONTROL_PANEL_TRANSACTION = 33;
    public static final int GET_SERVICES_TRANSACTION = 81;
    public static final int GET_TASKS_TRANSACTION = 23;
    public static final int GET_TASK_FOR_ACTIVITY_TRANSACTION = 27;
    public static final int GOING_TO_SLEEP_TRANSACTION = 40;
    public static final int GRANT_URI_PERMISSION_FROM_OWNER_TRANSACTION = 117;
    public static final int GRANT_URI_PERMISSION_TRANSACTION = 55;
    public static final int HANDLE_APPLICATION_CRASH_TRANSACTION = 2;
    public static final int HANDLE_APPLICATION_STRICT_MODE_VIOLATION_TRANSACTION = 110;
    public static final int HANDLE_APPLICATION_WTF_TRANSACTION = 102;
    public static final int INTENT_SENDER_ACTIVITY = 2;
    public static final int INTENT_SENDER_ACTIVITY_RESULT = 3;
    public static final int INTENT_SENDER_BROADCAST = 1;
    public static final int INTENT_SENDER_SERVICE = 4;
    public static final int IS_IMMERSIVE_TRANSACTION = 111;
    public static final int IS_TOP_ACTIVITY_IMMERSIVE_TRANSACTION = 113;
    public static final int IS_USER_A_MONKEY_TRANSACTION = 104;
    public static final int KILL_APPLICATION_PROCESS_TRANSACTION = 99;
    public static final int KILL_APPLICATION_WITH_UID_TRANSACTION = 96;
    public static final int KILL_BACKGROUND_PROCESSES_TRANSACTION = 103;
    public static final int KILL_PIDS_TRANSACTION = 80;
    public static final int MOVE_ACTIVITY_TASK_TO_BACK_TRANSACTION = 75;
    public static final int MOVE_TASK_BACKWARDS_TRANSACTION = 26;
    public static final int MOVE_TASK_TO_BACK_TRANSACTION = 25;
    public static final int MOVE_TASK_TO_FRONT_TRANSACTION = 24;
    public static final int NEW_URI_PERMISSION_OWNER_TRANSACTION = 116;
    public static final int NOTE_WAKEUP_ALARM_TRANSACTION = 68;
    public static final int OPEN_CONTENT_URI_TRANSACTION = 5;
    public static final int OVERRIDE_PENDING_TRANSITION_TRANSACTION = 101;
    public static final int PEEK_SERVICE_TRANSACTION = 85;
    public static final int PROFILE_CONTROL_TRANSACTION = 86;
    public static final int PUBLISH_CONTENT_PROVIDERS_TRANSACTION = 30;
    public static final int PUBLISH_SERVICE_TRANSACTION = 38;
    public static final int REGISTER_ACTIVITY_WATCHER_TRANSACTION = 93;
    public static final int REGISTER_RECEIVER_TRANSACTION = 12;
    public static final int REMOVE_CONTENT_PROVIDER_TRANSACTION = 69;
    public static final int REPORT_THUMBNAIL_TRANSACTION = 28;
    public static final int RESUME_APP_SWITCHES_TRANSACTION = 89;
    public static final int REVOKE_URI_PERMISSION_FROM_OWNER_TRANSACTION = 118;
    public static final int REVOKE_URI_PERMISSION_TRANSACTION = 56;
    public static final int SERVICE_DONE_EXECUTING_TRANSACTION = 61;
    public static final int SET_ACTIVITY_CONTROLLER_TRANSACTION = 57;
    public static final int SET_ALWAYS_FINISH_TRANSACTION = 43;
    public static final int SET_DEBUG_APP_TRANSACTION = 42;
    public static final int SET_IMMERSIVE_TRANSACTION = 112;
    public static final int SET_PROCESS_FOREGROUND_TRANSACTION = 73;
    public static final int SET_PROCESS_LIMIT_TRANSACTION = 51;
    public static final int SET_REQUESTED_ORIENTATION_TRANSACTION = 70;
    public static final int SET_SERVICE_FOREGROUND_TRANSACTION = 74;
    public static final int SHOW_WAITING_FOR_DEBUGGER_TRANSACTION = 58;
    public static final int SHUTDOWN_TRANSACTION = 87;
    public static final int SIGNAL_PERSISTENT_PROCESSES_TRANSACTION = 59;
    public static final int START_ACTIVITY_AND_WAIT_TRANSACTION = 105;
    public static final int START_ACTIVITY_INTENT_SENDER_TRANSACTION = 100;
    public static final int START_ACTIVITY_IN_PACKAGE_TRANSACTION = 95;
    public static final int START_ACTIVITY_TRANSACTION = 3;
    public static final int START_ACTIVITY_WITH_CONFIG_TRANSACTION = 107;
    public static final int START_BACKUP_AGENT_TRANSACTION = 90;
    public static final int START_CANCELED = -6;
    public static final int START_CLASS_NOT_FOUND = -2;
    public static final int START_DELIVERED_TO_TOP = 3;
    public static final int START_FORWARD_AND_REQUEST_CONFLICT = -3;
    public static final int START_INSTRUMENTATION_TRANSACTION = 44;
    public static final int START_INTENT_NOT_RESOLVED = -1;
    public static final int START_NEXT_MATCHING_ACTIVITY_TRANSACTION = 67;
    public static final int START_NOT_ACTIVITY = -5;
    public static final int START_PERMISSION_DENIED = -4;
    public static final int START_RETURN_INTENT_TO_CALLER = 1;
    public static final int START_RUNNING_TRANSACTION = 1;
    public static final int START_SERVICE_TRANSACTION = 34;
    public static final int START_SUCCESS = 0;
    public static final int START_SWITCHES_CANCELED = 4;
    public static final int START_TASK_TO_FRONT = 2;
    public static final int STOP_APP_SWITCHES_TRANSACTION = 88;
    public static final int STOP_SERVICE_TOKEN_TRANSACTION = 48;
    public static final int STOP_SERVICE_TRANSACTION = 35;
    public static final int UNBIND_BACKUP_AGENT_TRANSACTION = 92;
    public static final int UNBIND_FINISHED_TRANSACTION = 72;
    public static final int UNBIND_SERVICE_TRANSACTION = 37;
    public static final int UNBROADCAST_INTENT_TRANSACTION = 15;
    public static final int UNHANDLED_BACK_TRANSACTION = 4;
    public static final int UNREGISTER_ACTIVITY_WATCHER_TRANSACTION = 94;
    public static final int UNREGISTER_RECEIVER_TRANSACTION = 13;
    public static final int UPDATE_CONFIGURATION_TRANSACTION = 47;
    public static final int WAKING_UP_TRANSACTION = 41;
    public static final int WILL_ACTIVITY_BE_VISIBLE_TRANSACTION = 106;
    public static final String descriptor = "android.app.IActivityManager";

    void activityDestroyed(IBinder iBinder) throws RemoteException;

    void activityIdle(IBinder iBinder, Configuration configuration) throws RemoteException;

    void activityPaused(IBinder iBinder, Bundle bundle) throws RemoteException;

    void activityStopped(IBinder iBinder, Bitmap bitmap, CharSequence charSequence) throws RemoteException;

    void attachApplication(IApplicationThread iApplicationThread) throws RemoteException;

    void backupAgentCreated(String str, IBinder iBinder) throws RemoteException;

    boolean bindBackupAgent(ApplicationInfo applicationInfo, int i) throws RemoteException;

    int bindService(IApplicationThread iApplicationThread, IBinder iBinder, Intent intent, String str, IServiceConnection iServiceConnection, int i) throws RemoteException;

    int broadcastIntent(IApplicationThread iApplicationThread, Intent intent, String str, IIntentReceiver iIntentReceiver, int i, String str2, Bundle bundle, String str3, boolean z, boolean z2) throws RemoteException;

    void cancelIntentSender(IIntentSender iIntentSender) throws RemoteException;

    int checkPermission(String str, int i, int i2) throws RemoteException;

    int checkUriPermission(Uri uri, int i, int i2, int i3) throws RemoteException;

    boolean clearApplicationUserData(String str, IPackageDataObserver iPackageDataObserver) throws RemoteException;

    void closeSystemDialogs(String str) throws RemoteException;

    void crashApplication(int i, int i2, String str, String str2) throws RemoteException;

    void enterSafeMode() throws RemoteException;

    boolean finishActivity(IBinder iBinder, int i, Intent intent) throws RemoteException;

    void finishHeavyWeightApp() throws RemoteException;

    void finishInstrumentation(IApplicationThread iApplicationThread, int i, Bundle bundle) throws RemoteException;

    void finishOtherInstances(IBinder iBinder, ComponentName componentName) throws RemoteException;

    void finishReceiver(IBinder iBinder, int i, String str, Bundle bundle, boolean z) throws RemoteException;

    void finishSubActivity(IBinder iBinder, String str, int i) throws RemoteException;

    void forceStopPackage(String str) throws RemoteException;

    ComponentName getActivityClassForToken(IBinder iBinder) throws RemoteException;

    ComponentName getCallingActivity(IBinder iBinder) throws RemoteException;

    String getCallingPackage(IBinder iBinder) throws RemoteException;

    Configuration getConfiguration() throws RemoteException;

    ContentProviderHolder getContentProvider(IApplicationThread iApplicationThread, String str) throws RemoteException;

    ConfigurationInfo getDeviceConfigurationInfo() throws RemoteException;

    IIntentSender getIntentSender(int i, String str, IBinder iBinder, String str2, int i2, Intent intent, String str3, int i3) throws RemoteException;

    void getMemoryInfo(ActivityManager.MemoryInfo memoryInfo) throws RemoteException;

    String getPackageForIntentSender(IIntentSender iIntentSender) throws RemoteException;

    String getPackageForToken(IBinder iBinder) throws RemoteException;

    int getProcessLimit() throws RemoteException;

    Debug.MemoryInfo[] getProcessMemoryInfo(int[] iArr) throws RemoteException;

    List<ActivityManager.ProcessErrorStateInfo> getProcessesInErrorState() throws RemoteException;

    String getProviderMimeType(Uri uri) throws RemoteException;

    List<ActivityManager.RecentTaskInfo> getRecentTasks(int i, int i2) throws RemoteException;

    int getRequestedOrientation(IBinder iBinder) throws RemoteException;

    List<ActivityManager.RunningAppProcessInfo> getRunningAppProcesses() throws RemoteException;

    List<ApplicationInfo> getRunningExternalApplications() throws RemoteException;

    PendingIntent getRunningServiceControlPanel(ComponentName componentName) throws RemoteException;

    List getServices(int i, int i2) throws RemoteException;

    int getTaskForActivity(IBinder iBinder, boolean z) throws RemoteException;

    List getTasks(int i, int i2, IThumbnailReceiver iThumbnailReceiver) throws RemoteException;

    void goingToSleep() throws RemoteException;

    void grantUriPermission(IApplicationThread iApplicationThread, String str, Uri uri, int i) throws RemoteException;

    void grantUriPermissionFromOwner(IBinder iBinder, int i, String str, Uri uri, int i2) throws RemoteException;

    void handleApplicationCrash(IBinder iBinder, ApplicationErrorReport.CrashInfo crashInfo) throws RemoteException;

    boolean handleApplicationWtf(IBinder iBinder, String str, ApplicationErrorReport.CrashInfo crashInfo) throws RemoteException;

    boolean isUserAMonkey() throws RemoteException;

    void killApplicationProcess(String str, int i) throws RemoteException;

    void killApplicationWithUid(String str, int i) throws RemoteException;

    void killBackgroundProcesses(String str) throws RemoteException;

    boolean killPids(int[] iArr, String str) throws RemoteException;

    boolean moveActivityTaskToBack(IBinder iBinder, boolean z) throws RemoteException;

    void moveTaskBackwards(int i) throws RemoteException;

    void moveTaskToBack(int i) throws RemoteException;

    void moveTaskToFront(int i) throws RemoteException;

    IBinder newUriPermissionOwner(String str) throws RemoteException;

    void noteWakeupAlarm(IIntentSender iIntentSender) throws RemoteException;

    ParcelFileDescriptor openContentUri(Uri uri) throws RemoteException;

    void overridePendingTransition(IBinder iBinder, String str, int i, int i2) throws RemoteException;

    IBinder peekService(Intent intent, String str) throws RemoteException;

    boolean profileControl(String str, boolean z, String str2, ParcelFileDescriptor parcelFileDescriptor) throws RemoteException;

    void publishContentProviders(IApplicationThread iApplicationThread, List<ContentProviderHolder> list) throws RemoteException;

    void publishService(IBinder iBinder, Intent intent, IBinder iBinder2) throws RemoteException;

    void registerActivityWatcher(IActivityWatcher iActivityWatcher) throws RemoteException;

    Intent registerReceiver(IApplicationThread iApplicationThread, IIntentReceiver iIntentReceiver, IntentFilter intentFilter, String str) throws RemoteException;

    void removeContentProvider(IApplicationThread iApplicationThread, String str) throws RemoteException;

    void reportThumbnail(IBinder iBinder, Bitmap bitmap, CharSequence charSequence) throws RemoteException;

    void resumeAppSwitches() throws RemoteException;

    void revokeUriPermission(IApplicationThread iApplicationThread, Uri uri, int i) throws RemoteException;

    void revokeUriPermissionFromOwner(IBinder iBinder, Uri uri, int i) throws RemoteException;

    void serviceDoneExecuting(IBinder iBinder, int i, int i2, int i3) throws RemoteException;

    void setActivityController(IActivityController iActivityController) throws RemoteException;

    void setAlwaysFinish(boolean z) throws RemoteException;

    void setDebugApp(String str, boolean z, boolean z2) throws RemoteException;

    void setProcessForeground(IBinder iBinder, int i, boolean z) throws RemoteException;

    void setProcessLimit(int i) throws RemoteException;

    void setRequestedOrientation(IBinder iBinder, int i) throws RemoteException;

    void setServiceForeground(ComponentName componentName, IBinder iBinder, int i, Notification notification, boolean z) throws RemoteException;

    void showWaitingForDebugger(IApplicationThread iApplicationThread, boolean z) throws RemoteException;

    boolean shutdown(int i) throws RemoteException;

    void signalPersistentProcesses(int i) throws RemoteException;

    int startActivity(IApplicationThread iApplicationThread, Intent intent, String str, Uri[] uriArr, int i, IBinder iBinder, String str2, int i2, boolean z, boolean z2) throws RemoteException;

    WaitResult startActivityAndWait(IApplicationThread iApplicationThread, Intent intent, String str, Uri[] uriArr, int i, IBinder iBinder, String str2, int i2, boolean z, boolean z2) throws RemoteException;

    int startActivityInPackage(int i, Intent intent, String str, IBinder iBinder, String str2, int i2, boolean z) throws RemoteException;

    int startActivityIntentSender(IApplicationThread iApplicationThread, IntentSender intentSender, Intent intent, String str, IBinder iBinder, String str2, int i, int i2, int i3) throws RemoteException;

    int startActivityWithConfig(IApplicationThread iApplicationThread, Intent intent, String str, Uri[] uriArr, int i, IBinder iBinder, String str2, int i2, boolean z, boolean z2, Configuration configuration) throws RemoteException;

    boolean startInstrumentation(ComponentName componentName, String str, int i, Bundle bundle, IInstrumentationWatcher iInstrumentationWatcher) throws RemoteException;

    boolean startNextMatchingActivity(IBinder iBinder, Intent intent) throws RemoteException;

    void startRunning(String str, String str2, String str3, String str4) throws RemoteException;

    ComponentName startService(IApplicationThread iApplicationThread, Intent intent, String str) throws RemoteException;

    void stopAppSwitches() throws RemoteException;

    int stopService(IApplicationThread iApplicationThread, Intent intent, String str) throws RemoteException;

    boolean stopServiceToken(ComponentName componentName, IBinder iBinder, int i) throws RemoteException;

    boolean testIsSystemReady();

    void unbindBackupAgent(ApplicationInfo applicationInfo) throws RemoteException;

    void unbindFinished(IBinder iBinder, Intent intent, boolean z) throws RemoteException;

    boolean unbindService(IServiceConnection iServiceConnection) throws RemoteException;

    void unbroadcastIntent(IApplicationThread iApplicationThread, Intent intent) throws RemoteException;

    void unhandledBack() throws RemoteException;

    void unregisterActivityWatcher(IActivityWatcher iActivityWatcher) throws RemoteException;

    void unregisterReceiver(IIntentReceiver iIntentReceiver) throws RemoteException;

    void updateConfiguration(Configuration configuration) throws RemoteException;

    void wakingUp() throws RemoteException;

    boolean willActivityBeVisible(IBinder iBinder) throws RemoteException;

    public static class ContentProviderHolder implements Parcelable {
        public static final Parcelable.Creator<ContentProviderHolder> CREATOR = new Parcelable.Creator<ContentProviderHolder>() {
            public ContentProviderHolder createFromParcel(Parcel source) {
                return new ContentProviderHolder(source);
            }

            public ContentProviderHolder[] newArray(int size) {
                return new ContentProviderHolder[size];
            }
        };
        public final ProviderInfo info;
        public boolean noReleaseNeeded;
        public IContentProvider provider;

        public ContentProviderHolder(ProviderInfo _info) {
            this.info = _info;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel dest, int flags) {
            int i;
            this.info.writeToParcel(dest, 0);
            if (this.provider != null) {
                dest.writeStrongBinder(this.provider.asBinder());
            } else {
                dest.writeStrongBinder(null);
            }
            if (this.noReleaseNeeded) {
                i = 1;
            } else {
                i = 0;
            }
            dest.writeInt(i);
        }

        private ContentProviderHolder(Parcel source) {
            this.info = (ProviderInfo) ProviderInfo.CREATOR.createFromParcel(source);
            this.provider = ContentProviderNative.asInterface(source.readStrongBinder());
            this.noReleaseNeeded = source.readInt() != 0;
        }
    }

    public static class WaitResult implements Parcelable {
        public static final Parcelable.Creator<WaitResult> CREATOR = new Parcelable.Creator<WaitResult>() {
            public WaitResult createFromParcel(Parcel source) {
                return new WaitResult(source);
            }

            public WaitResult[] newArray(int size) {
                return new WaitResult[size];
            }
        };
        public int result;
        public long thisTime;
        public boolean timeout;
        public long totalTime;
        public ComponentName who;

        public WaitResult() {
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.result);
            dest.writeInt(this.timeout ? 1 : 0);
            ComponentName.writeToParcel(this.who, dest);
            dest.writeLong(this.thisTime);
            dest.writeLong(this.totalTime);
        }

        private WaitResult(Parcel source) {
            this.result = source.readInt();
            this.timeout = source.readInt() != 0;
            this.who = ComponentName.readFromParcel(source);
            this.thisTime = source.readLong();
            this.totalTime = source.readLong();
        }
    }
}
