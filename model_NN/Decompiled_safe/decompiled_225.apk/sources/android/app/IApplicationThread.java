package android.app;

import android.content.ComponentName;
import android.content.IIntentReceiver;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.ProviderInfo;
import android.content.pm.ServiceInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Debug;
import android.os.IBinder;
import android.os.IInterface;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import java.io.FileDescriptor;
import java.util.List;
import java.util.Map;

public interface IApplicationThread extends IInterface {
    public static final int BACKUP_MODE_FULL = 1;
    public static final int BACKUP_MODE_INCREMENTAL = 0;
    public static final int BACKUP_MODE_RESTORE = 2;
    public static final int BIND_APPLICATION_TRANSACTION = 13;
    public static final int DEBUG_OFF = 0;
    public static final int DEBUG_ON = 1;
    public static final int DEBUG_WAIT = 2;
    public static final int DISPATCH_PACKAGE_BROADCAST_TRANSACTION = 34;
    public static final int DUMP_SERVICE_TRANSACTION = 22;
    public static final int EXTERNAL_STORAGE_UNAVAILABLE = 1;
    public static final int GET_MEMORY_INFO_TRANSACTION = 32;
    public static final int PACKAGE_REMOVED = 0;
    public static final int PROCESS_IN_BACKGROUND_TRANSACTION = 19;
    public static final int PROFILER_CONTROL_TRANSACTION = 28;
    public static final int REQUEST_THUMBNAIL_TRANSACTION = 15;
    public static final int SCHEDULE_ACTIVITY_CONFIGURATION_CHANGED_TRANSACTION = 25;
    public static final int SCHEDULE_BIND_SERVICE_TRANSACTION = 20;
    public static final int SCHEDULE_CONFIGURATION_CHANGED_TRANSACTION = 16;
    public static final int SCHEDULE_CRASH_TRANSACTION = 35;
    public static final int SCHEDULE_CREATE_BACKUP_AGENT_TRANSACTION = 30;
    public static final int SCHEDULE_CREATE_SERVICE_TRANSACTION = 11;
    public static final int SCHEDULE_DESTROY_BACKUP_AGENT_TRANSACTION = 31;
    public static final int SCHEDULE_EXIT_TRANSACTION = 14;
    public static final int SCHEDULE_FINISH_ACTIVITY_TRANSACTION = 9;
    public static final int SCHEDULE_LAUNCH_ACTIVITY_TRANSACTION = 7;
    public static final int SCHEDULE_LOW_MEMORY_TRANSACTION = 24;
    public static final int SCHEDULE_NEW_INTENT_TRANSACTION = 8;
    public static final int SCHEDULE_PAUSE_ACTIVITY_TRANSACTION = 1;
    public static final int SCHEDULE_RECEIVER_TRANSACTION = 10;
    public static final int SCHEDULE_REGISTERED_RECEIVER_TRANSACTION = 23;
    public static final int SCHEDULE_RELAUNCH_ACTIVITY_TRANSACTION = 26;
    public static final int SCHEDULE_RESUME_ACTIVITY_TRANSACTION = 5;
    public static final int SCHEDULE_SEND_RESULT_TRANSACTION = 6;
    public static final int SCHEDULE_SERVICE_ARGS_TRANSACTION = 17;
    public static final int SCHEDULE_STOP_ACTIVITY_TRANSACTION = 3;
    public static final int SCHEDULE_STOP_SERVICE_TRANSACTION = 12;
    public static final int SCHEDULE_SUICIDE_TRANSACTION = 33;
    public static final int SCHEDULE_UNBIND_SERVICE_TRANSACTION = 21;
    public static final int SCHEDULE_WINDOW_VISIBILITY_TRANSACTION = 4;
    public static final int SET_SCHEDULING_GROUP_TRANSACTION = 29;
    public static final int UPDATE_TIME_ZONE_TRANSACTION = 18;
    public static final String descriptor = "android.app.IApplicationThread";

    void bindApplication(String str, ApplicationInfo applicationInfo, List<ProviderInfo> list, ComponentName componentName, String str2, Bundle bundle, IInstrumentationWatcher iInstrumentationWatcher, int i, boolean z, Configuration configuration, Map<String, IBinder> map) throws RemoteException;

    void dispatchPackageBroadcast(int i, String[] strArr) throws RemoteException;

    void dumpService(FileDescriptor fileDescriptor, IBinder iBinder, String[] strArr) throws RemoteException;

    void getMemoryInfo(Debug.MemoryInfo memoryInfo) throws RemoteException;

    void processInBackground() throws RemoteException;

    void profilerControl(boolean z, String str, ParcelFileDescriptor parcelFileDescriptor) throws RemoteException;

    void requestThumbnail(IBinder iBinder) throws RemoteException;

    void scheduleActivityConfigurationChanged(IBinder iBinder) throws RemoteException;

    void scheduleBindService(IBinder iBinder, Intent intent, boolean z) throws RemoteException;

    void scheduleConfigurationChanged(Configuration configuration) throws RemoteException;

    void scheduleCrash(String str) throws RemoteException;

    void scheduleCreateBackupAgent(ApplicationInfo applicationInfo, int i) throws RemoteException;

    void scheduleCreateService(IBinder iBinder, ServiceInfo serviceInfo) throws RemoteException;

    void scheduleDestroyActivity(IBinder iBinder, boolean z, int i) throws RemoteException;

    void scheduleDestroyBackupAgent(ApplicationInfo applicationInfo) throws RemoteException;

    void scheduleExit() throws RemoteException;

    void scheduleLaunchActivity(Intent intent, IBinder iBinder, int i, ActivityInfo activityInfo, Bundle bundle, List<ResultInfo> list, List<Intent> list2, boolean z, boolean z2) throws RemoteException;

    void scheduleLowMemory() throws RemoteException;

    void scheduleNewIntent(List<Intent> list, IBinder iBinder) throws RemoteException;

    void schedulePauseActivity(IBinder iBinder, boolean z, boolean z2, int i) throws RemoteException;

    void scheduleReceiver(Intent intent, ActivityInfo activityInfo, int i, String str, Bundle bundle, boolean z) throws RemoteException;

    void scheduleRegisteredReceiver(IIntentReceiver iIntentReceiver, Intent intent, int i, String str, Bundle bundle, boolean z, boolean z2) throws RemoteException;

    void scheduleRelaunchActivity(IBinder iBinder, List<ResultInfo> list, List<Intent> list2, int i, boolean z, Configuration configuration) throws RemoteException;

    void scheduleResumeActivity(IBinder iBinder, boolean z) throws RemoteException;

    void scheduleSendResult(IBinder iBinder, List<ResultInfo> list) throws RemoteException;

    void scheduleServiceArgs(IBinder iBinder, int i, int i2, Intent intent) throws RemoteException;

    void scheduleStopActivity(IBinder iBinder, boolean z, int i) throws RemoteException;

    void scheduleStopService(IBinder iBinder) throws RemoteException;

    void scheduleSuicide() throws RemoteException;

    void scheduleUnbindService(IBinder iBinder, Intent intent) throws RemoteException;

    void scheduleWindowVisibility(IBinder iBinder, boolean z) throws RemoteException;

    void setSchedulingGroup(int i) throws RemoteException;

    void updateTimeZone() throws RemoteException;
}
