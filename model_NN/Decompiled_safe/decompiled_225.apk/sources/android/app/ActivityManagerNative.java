package android.app;

import android.app.ActivityManager;
import android.app.ApplicationErrorReport;
import android.app.IActivityController;
import android.app.IActivityManager;
import android.app.IActivityWatcher;
import android.app.IInstrumentationWatcher;
import android.app.IServiceConnection;
import android.app.IThumbnailReceiver;
import android.content.ComponentName;
import android.content.IIntentReceiver;
import android.content.IIntentSender;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.ApplicationInfo;
import android.content.pm.ConfigurationInfo;
import android.content.pm.IPackageDataObserver;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.Debug;
import android.os.IBinder;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.text.TextUtils;
import java.util.List;

public abstract class ActivityManagerNative extends Binder implements IActivityManager {
    private static IActivityManager gDefault;
    static boolean sSystemReady = false;

    public static IActivityManager asInterface(IBinder obj) {
        if (obj == null) {
            return null;
        }
        IActivityManager in = (IActivityManager) obj.queryLocalInterface(IActivityManager.descriptor);
        if (in != null) {
            return in;
        }
        return new ActivityManagerProxy(obj);
    }

    public static IActivityManager getDefault() {
        if (gDefault != null) {
            return gDefault;
        }
        gDefault = asInterface(ServiceManager.getService("activity"));
        return gDefault;
    }

    public static boolean isSystemReady() {
        if (!sSystemReady) {
            sSystemReady = getDefault().testIsSystemReady();
        }
        return sSystemReady;
    }

    public static void broadcastStickyIntent(Intent intent, String permission) {
        try {
            getDefault().broadcastIntent(null, intent, null, null, -1, null, null, null, false, true);
        } catch (RemoteException e) {
        }
    }

    public static void noteWakeupAlarm(PendingIntent ps) {
    }

    public ActivityManagerNative() {
        attachInterface(this, IActivityManager.descriptor);
    }

    public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
        boolean sticky;
        boolean debug;
        boolean debug2;
        boolean debug3;
        switch (code) {
            case 1:
                data.enforceInterface(IActivityManager.descriptor);
                startRunning(data.readString(), data.readString(), data.readString(), data.readString());
                reply.writeNoException();
                return true;
            case 2:
                data.enforceInterface(IActivityManager.descriptor);
                handleApplicationCrash(data.readStrongBinder(), new ApplicationErrorReport.CrashInfo(data));
                reply.writeNoException();
                return true;
            case 3:
                data.enforceInterface(IActivityManager.descriptor);
                IApplicationThread app = ApplicationThreadNative.asInterface(data.readStrongBinder());
                Intent intent = (Intent) Intent.CREATOR.createFromParcel(data);
                String resolvedType = data.readString();
                Uri[] grantedUriPermissions = (Uri[]) data.createTypedArray(Uri.CREATOR);
                int grantedMode = data.readInt();
                IBinder resultTo = data.readStrongBinder();
                String resultWho = data.readString();
                int requestCode = data.readInt();
                boolean onlyIfNeeded = data.readInt() != 0;
                if (data.readInt() != 0) {
                    debug3 = true;
                } else {
                    debug3 = false;
                }
                int result = startActivity(app, intent, resolvedType, grantedUriPermissions, grantedMode, resultTo, resultWho, requestCode, onlyIfNeeded, debug3);
                reply.writeNoException();
                reply.writeInt(result);
                return true;
            case 4:
                data.enforceInterface(IActivityManager.descriptor);
                unhandledBack();
                reply.writeNoException();
                return true;
            case 5:
                data.enforceInterface(IActivityManager.descriptor);
                ParcelFileDescriptor pfd = openContentUri(Uri.parse(data.readString()));
                reply.writeNoException();
                if (pfd != null) {
                    reply.writeInt(1);
                    pfd.writeToParcel(reply, 1);
                } else {
                    reply.writeInt(0);
                }
                return true;
            case 6:
            case IApplicationThread.SCHEDULE_LAUNCH_ACTIVITY_TRANSACTION /*7*/:
            case 8:
            case IApplicationThread.SCHEDULE_FINISH_ACTIVITY_TRANSACTION /*9*/:
            case 10:
            case 31:
            case 82:
            case IActivityManager.IS_IMMERSIVE_TRANSACTION /*111*/:
            case IActivityManager.SET_IMMERSIVE_TRANSACTION /*112*/:
            case IActivityManager.IS_TOP_ACTIVITY_IMMERSIVE_TRANSACTION /*113*/:
            default:
                return super.onTransact(code, data, reply, flags);
            case 11:
                data.enforceInterface(IActivityManager.descriptor);
                IBinder token = data.readStrongBinder();
                Intent resultData = null;
                int resultCode = data.readInt();
                if (data.readInt() != 0) {
                    resultData = (Intent) Intent.CREATOR.createFromParcel(data);
                }
                boolean res = finishActivity(token, resultCode, resultData);
                reply.writeNoException();
                reply.writeInt(res ? 1 : 0);
                return true;
            case 12:
                data.enforceInterface(IActivityManager.descriptor);
                IBinder b = data.readStrongBinder();
                IApplicationThread app2 = b != null ? ApplicationThreadNative.asInterface(b) : null;
                IBinder b2 = data.readStrongBinder();
                Intent intent2 = registerReceiver(app2, b2 != null ? IIntentReceiver.Stub.asInterface(b2) : null, (IntentFilter) IntentFilter.CREATOR.createFromParcel(data), data.readString());
                reply.writeNoException();
                if (intent2 != null) {
                    reply.writeInt(1);
                    intent2.writeToParcel(reply, 0);
                } else {
                    reply.writeInt(0);
                }
                return true;
            case 13:
                data.enforceInterface(IActivityManager.descriptor);
                IBinder b3 = data.readStrongBinder();
                if (b3 == null) {
                    return true;
                }
                unregisterReceiver(IIntentReceiver.Stub.asInterface(b3));
                reply.writeNoException();
                return true;
            case 14:
                data.enforceInterface(IActivityManager.descriptor);
                IBinder b4 = data.readStrongBinder();
                IApplicationThread app3 = b4 != null ? ApplicationThreadNative.asInterface(b4) : null;
                Intent intent3 = (Intent) Intent.CREATOR.createFromParcel(data);
                String resolvedType2 = data.readString();
                IBinder b5 = data.readStrongBinder();
                IIntentReceiver resultTo2 = b5 != null ? IIntentReceiver.Stub.asInterface(b5) : null;
                int resultCode2 = data.readInt();
                String resultData2 = data.readString();
                Bundle resultExtras = data.readBundle();
                String perm = data.readString();
                boolean serialized = data.readInt() != 0;
                if (data.readInt() != 0) {
                    sticky = true;
                } else {
                    sticky = false;
                }
                int res2 = broadcastIntent(app3, intent3, resolvedType2, resultTo2, resultCode2, resultData2, resultExtras, perm, serialized, sticky);
                reply.writeNoException();
                reply.writeInt(res2);
                return true;
            case 15:
                data.enforceInterface(IActivityManager.descriptor);
                IBinder b6 = data.readStrongBinder();
                unbroadcastIntent(b6 != null ? ApplicationThreadNative.asInterface(b6) : null, (Intent) Intent.CREATOR.createFromParcel(data));
                reply.writeNoException();
                return true;
            case 16:
                data.enforceInterface(IActivityManager.descriptor);
                IBinder who = data.readStrongBinder();
                int resultCode3 = data.readInt();
                String resultData3 = data.readString();
                Bundle resultExtras2 = data.readBundle();
                boolean resultAbort = data.readInt() != 0;
                if (who != null) {
                    finishReceiver(who, resultCode3, resultData3, resultExtras2, resultAbort);
                }
                reply.writeNoException();
                return true;
            case 17:
                data.enforceInterface(IActivityManager.descriptor);
                IApplicationThread app4 = ApplicationThreadNative.asInterface(data.readStrongBinder());
                if (app4 != null) {
                    attachApplication(app4);
                }
                reply.writeNoException();
                return true;
            case 18:
                data.enforceInterface(IActivityManager.descriptor);
                IBinder token2 = data.readStrongBinder();
                Configuration config = null;
                if (data.readInt() != 0) {
                    config = (Configuration) Configuration.CREATOR.createFromParcel(data);
                }
                if (token2 != null) {
                    activityIdle(token2, config);
                }
                reply.writeNoException();
                return true;
            case 19:
                data.enforceInterface(IActivityManager.descriptor);
                activityPaused(data.readStrongBinder(), data.readBundle());
                reply.writeNoException();
                return true;
            case 20:
                data.enforceInterface(IActivityManager.descriptor);
                activityStopped(data.readStrongBinder(), data.readInt() != 0 ? (Bitmap) Bitmap.CREATOR.createFromParcel(data) : null, (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(data));
                reply.writeNoException();
                return true;
            case 21:
                data.enforceInterface(IActivityManager.descriptor);
                IBinder token3 = data.readStrongBinder();
                String res3 = token3 != null ? getCallingPackage(token3) : null;
                reply.writeNoException();
                reply.writeString(res3);
                return true;
            case 22:
                data.enforceInterface(IActivityManager.descriptor);
                ComponentName cn = getCallingActivity(data.readStrongBinder());
                reply.writeNoException();
                ComponentName.writeToParcel(cn, reply);
                return true;
            case 23:
                data.enforceInterface(IActivityManager.descriptor);
                int maxNum = data.readInt();
                int fl = data.readInt();
                IBinder receiverBinder = data.readStrongBinder();
                List list = getTasks(maxNum, fl, receiverBinder != null ? IThumbnailReceiver.Stub.asInterface(receiverBinder) : null);
                reply.writeNoException();
                int N = list != null ? list.size() : -1;
                reply.writeInt(N);
                for (int i = 0; i < N; i++) {
                    ((ActivityManager.RunningTaskInfo) list.get(i)).writeToParcel(reply, 0);
                }
                return true;
            case 24:
                data.enforceInterface(IActivityManager.descriptor);
                moveTaskToFront(data.readInt());
                reply.writeNoException();
                return true;
            case 25:
                data.enforceInterface(IActivityManager.descriptor);
                moveTaskToBack(data.readInt());
                reply.writeNoException();
                return true;
            case 26:
                data.enforceInterface(IActivityManager.descriptor);
                moveTaskBackwards(data.readInt());
                reply.writeNoException();
                return true;
            case IActivityManager.GET_TASK_FOR_ACTIVITY_TRANSACTION /*27*/:
                data.enforceInterface(IActivityManager.descriptor);
                IBinder token4 = data.readStrongBinder();
                int res4 = token4 != null ? getTaskForActivity(token4, data.readInt() != 0) : -1;
                reply.writeNoException();
                reply.writeInt(res4);
                return true;
            case 28:
                data.enforceInterface(IActivityManager.descriptor);
                reportThumbnail(data.readStrongBinder(), data.readInt() != 0 ? (Bitmap) Bitmap.CREATOR.createFromParcel(data) : null, (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(data));
                reply.writeNoException();
                return true;
            case 29:
                data.enforceInterface(IActivityManager.descriptor);
                IActivityManager.ContentProviderHolder cph = getContentProvider(ApplicationThreadNative.asInterface(data.readStrongBinder()), data.readString());
                reply.writeNoException();
                if (cph != null) {
                    reply.writeInt(1);
                    cph.writeToParcel(reply, 0);
                } else {
                    reply.writeInt(0);
                }
                return true;
            case 30:
                data.enforceInterface(IActivityManager.descriptor);
                publishContentProviders(ApplicationThreadNative.asInterface(data.readStrongBinder()), data.createTypedArrayList(IActivityManager.ContentProviderHolder.CREATOR));
                reply.writeNoException();
                return true;
            case 32:
                data.enforceInterface(IActivityManager.descriptor);
                finishSubActivity(data.readStrongBinder(), data.readString(), data.readInt());
                reply.writeNoException();
                return true;
            case 33:
                data.enforceInterface(IActivityManager.descriptor);
                PendingIntent pi = getRunningServiceControlPanel((ComponentName) ComponentName.CREATOR.createFromParcel(data));
                reply.writeNoException();
                PendingIntent.writePendingIntentOrNullToParcel(pi, reply);
                return true;
            case 34:
                data.enforceInterface(IActivityManager.descriptor);
                ComponentName cn2 = startService(ApplicationThreadNative.asInterface(data.readStrongBinder()), (Intent) Intent.CREATOR.createFromParcel(data), data.readString());
                reply.writeNoException();
                ComponentName.writeToParcel(cn2, reply);
                return true;
            case 35:
                data.enforceInterface(IActivityManager.descriptor);
                int res5 = stopService(ApplicationThreadNative.asInterface(data.readStrongBinder()), (Intent) Intent.CREATOR.createFromParcel(data), data.readString());
                reply.writeNoException();
                reply.writeInt(res5);
                return true;
            case IActivityManager.BIND_SERVICE_TRANSACTION /*36*/:
                data.enforceInterface(IActivityManager.descriptor);
                int res6 = bindService(ApplicationThreadNative.asInterface(data.readStrongBinder()), data.readStrongBinder(), (Intent) Intent.CREATOR.createFromParcel(data), data.readString(), IServiceConnection.Stub.asInterface(data.readStrongBinder()), data.readInt());
                reply.writeNoException();
                reply.writeInt(res6);
                return true;
            case IActivityManager.UNBIND_SERVICE_TRANSACTION /*37*/:
                data.enforceInterface(IActivityManager.descriptor);
                boolean res7 = unbindService(IServiceConnection.Stub.asInterface(data.readStrongBinder()));
                reply.writeNoException();
                reply.writeInt(res7 ? 1 : 0);
                return true;
            case IActivityManager.PUBLISH_SERVICE_TRANSACTION /*38*/:
                data.enforceInterface(IActivityManager.descriptor);
                publishService(data.readStrongBinder(), (Intent) Intent.CREATOR.createFromParcel(data), data.readStrongBinder());
                reply.writeNoException();
                return true;
            case IActivityManager.FINISH_OTHER_INSTANCES_TRANSACTION /*39*/:
                data.enforceInterface(IActivityManager.descriptor);
                finishOtherInstances(data.readStrongBinder(), ComponentName.readFromParcel(data));
                reply.writeNoException();
                return true;
            case IActivityManager.GOING_TO_SLEEP_TRANSACTION /*40*/:
                data.enforceInterface(IActivityManager.descriptor);
                goingToSleep();
                reply.writeNoException();
                return true;
            case IActivityManager.WAKING_UP_TRANSACTION /*41*/:
                data.enforceInterface(IActivityManager.descriptor);
                wakingUp();
                reply.writeNoException();
                return true;
            case IActivityManager.SET_DEBUG_APP_TRANSACTION /*42*/:
                data.enforceInterface(IActivityManager.descriptor);
                setDebugApp(data.readString(), data.readInt() != 0, data.readInt() != 0);
                reply.writeNoException();
                return true;
            case IActivityManager.SET_ALWAYS_FINISH_TRANSACTION /*43*/:
                data.enforceInterface(IActivityManager.descriptor);
                setAlwaysFinish(data.readInt() != 0);
                reply.writeNoException();
                return true;
            case IActivityManager.START_INSTRUMENTATION_TRANSACTION /*44*/:
                data.enforceInterface(IActivityManager.descriptor);
                boolean res8 = startInstrumentation(ComponentName.readFromParcel(data), data.readString(), data.readInt(), data.readBundle(), IInstrumentationWatcher.Stub.asInterface(data.readStrongBinder()));
                reply.writeNoException();
                reply.writeInt(res8 ? 1 : 0);
                return true;
            case IActivityManager.FINISH_INSTRUMENTATION_TRANSACTION /*45*/:
                data.enforceInterface(IActivityManager.descriptor);
                finishInstrumentation(ApplicationThreadNative.asInterface(data.readStrongBinder()), data.readInt(), data.readBundle());
                reply.writeNoException();
                return true;
            case IActivityManager.GET_CONFIGURATION_TRANSACTION /*46*/:
                data.enforceInterface(IActivityManager.descriptor);
                Configuration config2 = getConfiguration();
                reply.writeNoException();
                config2.writeToParcel(reply, 0);
                return true;
            case IActivityManager.UPDATE_CONFIGURATION_TRANSACTION /*47*/:
                data.enforceInterface(IActivityManager.descriptor);
                updateConfiguration((Configuration) Configuration.CREATOR.createFromParcel(data));
                reply.writeNoException();
                return true;
            case IActivityManager.STOP_SERVICE_TOKEN_TRANSACTION /*48*/:
                data.enforceInterface(IActivityManager.descriptor);
                boolean res9 = stopServiceToken(ComponentName.readFromParcel(data), data.readStrongBinder(), data.readInt());
                reply.writeNoException();
                reply.writeInt(res9 ? 1 : 0);
                return true;
            case IActivityManager.GET_ACTIVITY_CLASS_FOR_TOKEN_TRANSACTION /*49*/:
                data.enforceInterface(IActivityManager.descriptor);
                ComponentName cn3 = getActivityClassForToken(data.readStrongBinder());
                reply.writeNoException();
                ComponentName.writeToParcel(cn3, reply);
                return true;
            case IActivityManager.GET_PACKAGE_FOR_TOKEN_TRANSACTION /*50*/:
                data.enforceInterface(IActivityManager.descriptor);
                IBinder token5 = data.readStrongBinder();
                reply.writeNoException();
                reply.writeString(getPackageForToken(token5));
                return true;
            case IActivityManager.SET_PROCESS_LIMIT_TRANSACTION /*51*/:
                data.enforceInterface(IActivityManager.descriptor);
                setProcessLimit(data.readInt());
                reply.writeNoException();
                return true;
            case IActivityManager.GET_PROCESS_LIMIT_TRANSACTION /*52*/:
                data.enforceInterface(IActivityManager.descriptor);
                int limit = getProcessLimit();
                reply.writeNoException();
                reply.writeInt(limit);
                return true;
            case IActivityManager.CHECK_PERMISSION_TRANSACTION /*53*/:
                data.enforceInterface(IActivityManager.descriptor);
                int res10 = checkPermission(data.readString(), data.readInt(), data.readInt());
                reply.writeNoException();
                reply.writeInt(res10);
                return true;
            case IActivityManager.CHECK_URI_PERMISSION_TRANSACTION /*54*/:
                data.enforceInterface(IActivityManager.descriptor);
                int res11 = checkUriPermission((Uri) Uri.CREATOR.createFromParcel(data), data.readInt(), data.readInt(), data.readInt());
                reply.writeNoException();
                reply.writeInt(res11);
                return true;
            case IActivityManager.GRANT_URI_PERMISSION_TRANSACTION /*55*/:
                data.enforceInterface(IActivityManager.descriptor);
                grantUriPermission(ApplicationThreadNative.asInterface(data.readStrongBinder()), data.readString(), (Uri) Uri.CREATOR.createFromParcel(data), data.readInt());
                reply.writeNoException();
                return true;
            case IActivityManager.REVOKE_URI_PERMISSION_TRANSACTION /*56*/:
                data.enforceInterface(IActivityManager.descriptor);
                revokeUriPermission(ApplicationThreadNative.asInterface(data.readStrongBinder()), (Uri) Uri.CREATOR.createFromParcel(data), data.readInt());
                reply.writeNoException();
                return true;
            case IActivityManager.SET_ACTIVITY_CONTROLLER_TRANSACTION /*57*/:
                data.enforceInterface(IActivityManager.descriptor);
                IActivityController asInterface = IActivityController.Stub.asInterface(data.readStrongBinder());
                return true;
            case IActivityManager.SHOW_WAITING_FOR_DEBUGGER_TRANSACTION /*58*/:
                data.enforceInterface(IActivityManager.descriptor);
                showWaitingForDebugger(ApplicationThreadNative.asInterface(data.readStrongBinder()), data.readInt() != 0);
                reply.writeNoException();
                return true;
            case IActivityManager.SIGNAL_PERSISTENT_PROCESSES_TRANSACTION /*59*/:
                data.enforceInterface(IActivityManager.descriptor);
                signalPersistentProcesses(data.readInt());
                reply.writeNoException();
                return true;
            case IActivityManager.GET_RECENT_TASKS_TRANSACTION /*60*/:
                data.enforceInterface(IActivityManager.descriptor);
                List<ActivityManager.RecentTaskInfo> list2 = getRecentTasks(data.readInt(), data.readInt());
                reply.writeNoException();
                reply.writeTypedList(list2);
                return true;
            case IActivityManager.SERVICE_DONE_EXECUTING_TRANSACTION /*61*/:
                data.enforceInterface(IActivityManager.descriptor);
                serviceDoneExecuting(data.readStrongBinder(), data.readInt(), data.readInt(), data.readInt());
                reply.writeNoException();
                return true;
            case IActivityManager.ACTIVITY_DESTROYED_TRANSACTION /*62*/:
                data.enforceInterface(IActivityManager.descriptor);
                activityDestroyed(data.readStrongBinder());
                reply.writeNoException();
                return true;
            case IActivityManager.GET_INTENT_SENDER_TRANSACTION /*63*/:
                data.enforceInterface(IActivityManager.descriptor);
                IIntentSender res12 = getIntentSender(data.readInt(), data.readString(), data.readStrongBinder(), data.readString(), data.readInt(), data.readInt() != 0 ? (Intent) Intent.CREATOR.createFromParcel(data) : null, data.readString(), data.readInt());
                reply.writeNoException();
                reply.writeStrongBinder(res12 != null ? res12.asBinder() : null);
                return true;
            case IActivityManager.CANCEL_INTENT_SENDER_TRANSACTION /*64*/:
                data.enforceInterface(IActivityManager.descriptor);
                cancelIntentSender(IIntentSender.Stub.asInterface(data.readStrongBinder()));
                reply.writeNoException();
                return true;
            case IActivityManager.GET_PACKAGE_FOR_INTENT_SENDER_TRANSACTION /*65*/:
                data.enforceInterface(IActivityManager.descriptor);
                String res13 = getPackageForIntentSender(IIntentSender.Stub.asInterface(data.readStrongBinder()));
                reply.writeNoException();
                reply.writeString(res13);
                return true;
            case IActivityManager.ENTER_SAFE_MODE_TRANSACTION /*66*/:
                data.enforceInterface(IActivityManager.descriptor);
                enterSafeMode();
                reply.writeNoException();
                return true;
            case IActivityManager.START_NEXT_MATCHING_ACTIVITY_TRANSACTION /*67*/:
                data.enforceInterface(IActivityManager.descriptor);
                boolean result2 = startNextMatchingActivity(data.readStrongBinder(), (Intent) Intent.CREATOR.createFromParcel(data));
                reply.writeNoException();
                reply.writeInt(result2 ? 1 : 0);
                return true;
            case IActivityManager.NOTE_WAKEUP_ALARM_TRANSACTION /*68*/:
                data.enforceInterface(IActivityManager.descriptor);
                noteWakeupAlarm(IIntentSender.Stub.asInterface(data.readStrongBinder()));
                reply.writeNoException();
                return true;
            case IActivityManager.REMOVE_CONTENT_PROVIDER_TRANSACTION /*69*/:
                data.enforceInterface(IActivityManager.descriptor);
                removeContentProvider(ApplicationThreadNative.asInterface(data.readStrongBinder()), data.readString());
                reply.writeNoException();
                return true;
            case IActivityManager.SET_REQUESTED_ORIENTATION_TRANSACTION /*70*/:
                data.enforceInterface(IActivityManager.descriptor);
                setRequestedOrientation(data.readStrongBinder(), data.readInt());
                reply.writeNoException();
                return true;
            case IActivityManager.GET_REQUESTED_ORIENTATION_TRANSACTION /*71*/:
                data.enforceInterface(IActivityManager.descriptor);
                int req = getRequestedOrientation(data.readStrongBinder());
                reply.writeNoException();
                reply.writeInt(req);
                return true;
            case IActivityManager.UNBIND_FINISHED_TRANSACTION /*72*/:
                data.enforceInterface(IActivityManager.descriptor);
                unbindFinished(data.readStrongBinder(), (Intent) Intent.CREATOR.createFromParcel(data), data.readInt() != 0);
                reply.writeNoException();
                return true;
            case IActivityManager.SET_PROCESS_FOREGROUND_TRANSACTION /*73*/:
                data.enforceInterface(IActivityManager.descriptor);
                setProcessForeground(data.readStrongBinder(), data.readInt(), data.readInt() != 0);
                reply.writeNoException();
                return true;
            case IActivityManager.SET_SERVICE_FOREGROUND_TRANSACTION /*74*/:
                data.enforceInterface(IActivityManager.descriptor);
                ComponentName className = ComponentName.readFromParcel(data);
                IBinder token6 = data.readStrongBinder();
                int id = data.readInt();
                Notification notification = null;
                if (data.readInt() != 0) {
                    notification = (Notification) Notification.CREATOR.createFromParcel(data);
                }
                setServiceForeground(className, token6, id, notification, data.readInt() != 0);
                reply.writeNoException();
                return true;
            case IActivityManager.MOVE_ACTIVITY_TASK_TO_BACK_TRANSACTION /*75*/:
                data.enforceInterface(IActivityManager.descriptor);
                boolean res14 = moveActivityTaskToBack(data.readStrongBinder(), data.readInt() != 0);
                reply.writeNoException();
                reply.writeInt(res14 ? 1 : 0);
                return true;
            case IActivityManager.GET_MEMORY_INFO_TRANSACTION /*76*/:
                data.enforceInterface(IActivityManager.descriptor);
                ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
                getMemoryInfo(mi);
                reply.writeNoException();
                mi.writeToParcel(reply, 0);
                return true;
            case IActivityManager.GET_PROCESSES_IN_ERROR_STATE_TRANSACTION /*77*/:
                data.enforceInterface(IActivityManager.descriptor);
                List<ActivityManager.ProcessErrorStateInfo> list3 = getProcessesInErrorState();
                reply.writeNoException();
                reply.writeTypedList(list3);
                return true;
            case IActivityManager.CLEAR_APP_DATA_TRANSACTION /*78*/:
                data.enforceInterface(IActivityManager.descriptor);
                boolean res15 = clearApplicationUserData(data.readString(), IPackageDataObserver.Stub.asInterface(data.readStrongBinder()));
                reply.writeNoException();
                reply.writeInt(res15 ? 1 : 0);
                return true;
            case IActivityManager.FORCE_STOP_PACKAGE_TRANSACTION /*79*/:
                data.enforceInterface(IActivityManager.descriptor);
                forceStopPackage(data.readString());
                reply.writeNoException();
                return true;
            case IActivityManager.KILL_PIDS_TRANSACTION /*80*/:
                data.enforceInterface(IActivityManager.descriptor);
                boolean res16 = killPids(data.createIntArray(), data.readString());
                reply.writeNoException();
                reply.writeInt(res16 ? 1 : 0);
                return true;
            case IActivityManager.GET_SERVICES_TRANSACTION /*81*/:
                data.enforceInterface(IActivityManager.descriptor);
                List list4 = getServices(data.readInt(), data.readInt());
                reply.writeNoException();
                int N2 = list4 != null ? list4.size() : -1;
                reply.writeInt(N2);
                for (int i2 = 0; i2 < N2; i2++) {
                    ((ActivityManager.RunningServiceInfo) list4.get(i2)).writeToParcel(reply, 0);
                }
                return true;
            case IActivityManager.GET_RUNNING_APP_PROCESSES_TRANSACTION /*83*/:
                data.enforceInterface(IActivityManager.descriptor);
                List<ActivityManager.RunningAppProcessInfo> list5 = getRunningAppProcesses();
                reply.writeNoException();
                reply.writeTypedList(list5);
                return true;
            case IActivityManager.GET_DEVICE_CONFIGURATION_TRANSACTION /*84*/:
                data.enforceInterface(IActivityManager.descriptor);
                ConfigurationInfo config3 = getDeviceConfigurationInfo();
                reply.writeNoException();
                config3.writeToParcel(reply, 0);
                return true;
            case IActivityManager.PEEK_SERVICE_TRANSACTION /*85*/:
                data.enforceInterface(IActivityManager.descriptor);
                IBinder binder = peekService((Intent) Intent.CREATOR.createFromParcel(data), data.readString());
                reply.writeNoException();
                reply.writeStrongBinder(binder);
                return true;
            case IActivityManager.PROFILE_CONTROL_TRANSACTION /*86*/:
                data.enforceInterface(IActivityManager.descriptor);
                boolean res17 = profileControl(data.readString(), data.readInt() != 0, data.readString(), data.readInt() != 0 ? data.readFileDescriptor() : null);
                reply.writeNoException();
                reply.writeInt(res17 ? 1 : 0);
                return true;
            case IActivityManager.SHUTDOWN_TRANSACTION /*87*/:
                data.enforceInterface(IActivityManager.descriptor);
                boolean res18 = shutdown(data.readInt());
                reply.writeNoException();
                reply.writeInt(res18 ? 1 : 0);
                return true;
            case IActivityManager.STOP_APP_SWITCHES_TRANSACTION /*88*/:
                data.enforceInterface(IActivityManager.descriptor);
                stopAppSwitches();
                reply.writeNoException();
                return true;
            case IActivityManager.RESUME_APP_SWITCHES_TRANSACTION /*89*/:
                data.enforceInterface(IActivityManager.descriptor);
                resumeAppSwitches();
                reply.writeNoException();
                return true;
            case IActivityManager.START_BACKUP_AGENT_TRANSACTION /*90*/:
                data.enforceInterface(IActivityManager.descriptor);
                boolean success = bindBackupAgent((ApplicationInfo) ApplicationInfo.CREATOR.createFromParcel(data), data.readInt());
                reply.writeNoException();
                reply.writeInt(success ? 1 : 0);
                return true;
            case 91:
                data.enforceInterface(IActivityManager.descriptor);
                backupAgentCreated(data.readString(), data.readStrongBinder());
                reply.writeNoException();
                return true;
            case IActivityManager.UNBIND_BACKUP_AGENT_TRANSACTION /*92*/:
                data.enforceInterface(IActivityManager.descriptor);
                unbindBackupAgent((ApplicationInfo) ApplicationInfo.CREATOR.createFromParcel(data));
                reply.writeNoException();
                return true;
            case IActivityManager.REGISTER_ACTIVITY_WATCHER_TRANSACTION /*93*/:
                data.enforceInterface(IActivityManager.descriptor);
                registerActivityWatcher(IActivityWatcher.Stub.asInterface(data.readStrongBinder()));
                return true;
            case IActivityManager.UNREGISTER_ACTIVITY_WATCHER_TRANSACTION /*94*/:
                data.enforceInterface(IActivityManager.descriptor);
                unregisterActivityWatcher(IActivityWatcher.Stub.asInterface(data.readStrongBinder()));
                return true;
            case IActivityManager.START_ACTIVITY_IN_PACKAGE_TRANSACTION /*95*/:
                data.enforceInterface(IActivityManager.descriptor);
                int result3 = startActivityInPackage(data.readInt(), (Intent) Intent.CREATOR.createFromParcel(data), data.readString(), data.readStrongBinder(), data.readString(), data.readInt(), data.readInt() != 0);
                reply.writeNoException();
                reply.writeInt(result3);
                return true;
            case IActivityManager.KILL_APPLICATION_WITH_UID_TRANSACTION /*96*/:
                data.enforceInterface(IActivityManager.descriptor);
                killApplicationWithUid(data.readString(), data.readInt());
                reply.writeNoException();
                return true;
            case IActivityManager.CLOSE_SYSTEM_DIALOGS_TRANSACTION /*97*/:
                data.enforceInterface(IActivityManager.descriptor);
                closeSystemDialogs(data.readString());
                reply.writeNoException();
                return true;
            case IActivityManager.GET_PROCESS_MEMORY_INFO_TRANSACTION /*98*/:
                data.enforceInterface(IActivityManager.descriptor);
                Debug.MemoryInfo[] res19 = getProcessMemoryInfo(data.createIntArray());
                reply.writeNoException();
                reply.writeTypedArray(res19, 1);
                return true;
            case IActivityManager.KILL_APPLICATION_PROCESS_TRANSACTION /*99*/:
                data.enforceInterface(IActivityManager.descriptor);
                killApplicationProcess(data.readString(), data.readInt());
                reply.writeNoException();
                return true;
            case 100:
                data.enforceInterface(IActivityManager.descriptor);
                IApplicationThread app5 = ApplicationThreadNative.asInterface(data.readStrongBinder());
                IntentSender intent4 = (IntentSender) IntentSender.CREATOR.createFromParcel(data);
                Intent fillInIntent = null;
                if (data.readInt() != 0) {
                    fillInIntent = (Intent) Intent.CREATOR.createFromParcel(data);
                }
                int result4 = startActivityIntentSender(app5, intent4, fillInIntent, data.readString(), data.readStrongBinder(), data.readString(), data.readInt(), data.readInt(), data.readInt());
                reply.writeNoException();
                reply.writeInt(result4);
                return true;
            case IActivityManager.OVERRIDE_PENDING_TRANSITION_TRANSACTION /*101*/:
                data.enforceInterface(IActivityManager.descriptor);
                overridePendingTransition(data.readStrongBinder(), data.readString(), data.readInt(), data.readInt());
                reply.writeNoException();
                return true;
            case IActivityManager.HANDLE_APPLICATION_WTF_TRANSACTION /*102*/:
                data.enforceInterface(IActivityManager.descriptor);
                boolean res20 = handleApplicationWtf(data.readStrongBinder(), data.readString(), new ApplicationErrorReport.CrashInfo(data));
                reply.writeNoException();
                reply.writeInt(res20 ? 1 : 0);
                return true;
            case IActivityManager.KILL_BACKGROUND_PROCESSES_TRANSACTION /*103*/:
                data.enforceInterface(IActivityManager.descriptor);
                killBackgroundProcesses(data.readString());
                reply.writeNoException();
                return true;
            case IActivityManager.IS_USER_A_MONKEY_TRANSACTION /*104*/:
                data.enforceInterface(IActivityManager.descriptor);
                boolean areThey = isUserAMonkey();
                reply.writeNoException();
                reply.writeInt(areThey ? 1 : 0);
                return true;
            case IActivityManager.START_ACTIVITY_AND_WAIT_TRANSACTION /*105*/:
                data.enforceInterface(IActivityManager.descriptor);
                IApplicationThread app6 = ApplicationThreadNative.asInterface(data.readStrongBinder());
                Intent intent5 = (Intent) Intent.CREATOR.createFromParcel(data);
                String resolvedType3 = data.readString();
                Uri[] grantedUriPermissions2 = (Uri[]) data.createTypedArray(Uri.CREATOR);
                int grantedMode2 = data.readInt();
                IBinder resultTo3 = data.readStrongBinder();
                String resultWho2 = data.readString();
                int requestCode2 = data.readInt();
                boolean onlyIfNeeded2 = data.readInt() != 0;
                if (data.readInt() != 0) {
                    debug2 = true;
                } else {
                    debug2 = false;
                }
                IActivityManager.WaitResult result5 = startActivityAndWait(app6, intent5, resolvedType3, grantedUriPermissions2, grantedMode2, resultTo3, resultWho2, requestCode2, onlyIfNeeded2, debug2);
                reply.writeNoException();
                result5.writeToParcel(reply, 0);
                return true;
            case IActivityManager.WILL_ACTIVITY_BE_VISIBLE_TRANSACTION /*106*/:
                data.enforceInterface(IActivityManager.descriptor);
                boolean res21 = willActivityBeVisible(data.readStrongBinder());
                reply.writeNoException();
                reply.writeInt(res21 ? 1 : 0);
                return true;
            case IActivityManager.START_ACTIVITY_WITH_CONFIG_TRANSACTION /*107*/:
                data.enforceInterface(IActivityManager.descriptor);
                IApplicationThread app7 = ApplicationThreadNative.asInterface(data.readStrongBinder());
                Intent intent6 = (Intent) Intent.CREATOR.createFromParcel(data);
                String resolvedType4 = data.readString();
                Uri[] grantedUriPermissions3 = (Uri[]) data.createTypedArray(Uri.CREATOR);
                int grantedMode3 = data.readInt();
                IBinder resultTo4 = data.readStrongBinder();
                String resultWho3 = data.readString();
                int requestCode3 = data.readInt();
                boolean onlyIfNeeded3 = data.readInt() != 0;
                if (data.readInt() != 0) {
                    debug = true;
                } else {
                    debug = false;
                }
                int result6 = startActivityWithConfig(app7, intent6, resolvedType4, grantedUriPermissions3, grantedMode3, resultTo4, resultWho3, requestCode3, onlyIfNeeded3, debug, (Configuration) Configuration.CREATOR.createFromParcel(data));
                reply.writeNoException();
                reply.writeInt(result6);
                return true;
            case IActivityManager.GET_RUNNING_EXTERNAL_APPLICATIONS_TRANSACTION /*108*/:
                data.enforceInterface(IActivityManager.descriptor);
                List<ApplicationInfo> list6 = getRunningExternalApplications();
                reply.writeNoException();
                reply.writeTypedList(list6);
                return true;
            case IActivityManager.FINISH_HEAVY_WEIGHT_APP_TRANSACTION /*109*/:
                data.enforceInterface(IActivityManager.descriptor);
                finishHeavyWeightApp();
                reply.writeNoException();
                return true;
            case IActivityManager.HANDLE_APPLICATION_STRICT_MODE_VIOLATION_TRANSACTION /*110*/:
                data.enforceInterface(IActivityManager.descriptor);
                IBinder readStrongBinder = data.readStrongBinder();
                int readInt = data.readInt();
                reply.writeNoException();
                return true;
            case IActivityManager.CRASH_APPLICATION_TRANSACTION /*114*/:
                data.enforceInterface(IActivityManager.descriptor);
                crashApplication(data.readInt(), data.readInt(), data.readString(), data.readString());
                reply.writeNoException();
                return true;
            case IActivityManager.GET_PROVIDER_MIME_TYPE_TRANSACTION /*115*/:
                data.enforceInterface(IActivityManager.descriptor);
                String type = getProviderMimeType((Uri) Uri.CREATOR.createFromParcel(data));
                reply.writeNoException();
                reply.writeString(type);
                return true;
            case IActivityManager.NEW_URI_PERMISSION_OWNER_TRANSACTION /*116*/:
                data.enforceInterface(IActivityManager.descriptor);
                IBinder perm2 = newUriPermissionOwner(data.readString());
                reply.writeNoException();
                reply.writeStrongBinder(perm2);
                return true;
            case IActivityManager.GRANT_URI_PERMISSION_FROM_OWNER_TRANSACTION /*117*/:
                data.enforceInterface(IActivityManager.descriptor);
                grantUriPermissionFromOwner(data.readStrongBinder(), data.readInt(), data.readString(), (Uri) Uri.CREATOR.createFromParcel(data), data.readInt());
                reply.writeNoException();
                return true;
            case IActivityManager.REVOKE_URI_PERMISSION_FROM_OWNER_TRANSACTION /*118*/:
                data.enforceInterface(IActivityManager.descriptor);
                IBinder owner = data.readStrongBinder();
                if (data.readInt() != 0) {
                    Uri.CREATOR.createFromParcel(data);
                }
                revokeUriPermissionFromOwner(owner, null, data.readInt());
                reply.writeNoException();
                return true;
        }
    }

    public IBinder asBinder() {
        return this;
    }
}
