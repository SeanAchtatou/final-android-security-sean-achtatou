package android.content;

import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ContentProviderNative */
final class ContentProviderProxy implements IContentProvider {
    private IBinder mRemote;

    public ContentProviderProxy(IBinder remote) {
        this.mRemote = remote;
    }

    public IBinder asBinder() {
        return this.mRemote;
    }

    public Cursor query(Uri url, String[] projection, String selection, String[] selectionArgs, String sortOrder) throws RemoteException {
        return null;
    }

    public String getType(Uri url) throws RemoteException {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();
        data.writeInterfaceToken(IContentProvider.descriptor);
        url.writeToParcel(data, 0);
        this.mRemote.transact(2, data, reply, 0);
        DatabaseUtils.readExceptionFromParcel(reply);
        String out = reply.readString();
        data.recycle();
        reply.recycle();
        return out;
    }

    public Uri insert(Uri url, ContentValues values) throws RemoteException {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();
        data.writeInterfaceToken(IContentProvider.descriptor);
        url.writeToParcel(data, 0);
        values.writeToParcel(data, 0);
        this.mRemote.transact(3, data, reply, 0);
        DatabaseUtils.readExceptionFromParcel(reply);
        Uri out = (Uri) Uri.CREATOR.createFromParcel(reply);
        data.recycle();
        reply.recycle();
        return out;
    }

    public int bulkInsert(Uri url, ContentValues[] values) throws RemoteException {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();
        data.writeInterfaceToken(IContentProvider.descriptor);
        url.writeToParcel(data, 0);
        data.writeTypedArray(values, 0);
        this.mRemote.transact(13, data, reply, 0);
        DatabaseUtils.readExceptionFromParcel(reply);
        int count = reply.readInt();
        data.recycle();
        reply.recycle();
        return count;
    }

    public ContentProviderResult[] applyBatch(ArrayList<ContentProviderOperation> operations) throws RemoteException, OperationApplicationException {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();
        data.writeInterfaceToken(IContentProvider.descriptor);
        data.writeInt(operations.size());
        Iterator i$ = operations.iterator();
        while (i$.hasNext()) {
            i$.next().writeToParcel(data, 0);
        }
        this.mRemote.transact(20, data, reply, 0);
        DatabaseUtils.readExceptionWithOperationApplicationExceptionFromParcel(reply);
        ContentProviderResult[] results = (ContentProviderResult[]) reply.createTypedArray(ContentProviderResult.CREATOR);
        data.recycle();
        reply.recycle();
        return results;
    }

    public int delete(Uri url, String selection, String[] selectionArgs) throws RemoteException {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();
        data.writeInterfaceToken(IContentProvider.descriptor);
        url.writeToParcel(data, 0);
        data.writeString(selection);
        data.writeStringArray(selectionArgs);
        this.mRemote.transact(4, data, reply, 0);
        DatabaseUtils.readExceptionFromParcel(reply);
        int count = reply.readInt();
        data.recycle();
        reply.recycle();
        return count;
    }

    public int update(Uri url, ContentValues values, String selection, String[] selectionArgs) throws RemoteException {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();
        data.writeInterfaceToken(IContentProvider.descriptor);
        url.writeToParcel(data, 0);
        values.writeToParcel(data, 0);
        data.writeString(selection);
        data.writeStringArray(selectionArgs);
        this.mRemote.transact(10, data, reply, 0);
        DatabaseUtils.readExceptionFromParcel(reply);
        int count = reply.readInt();
        data.recycle();
        reply.recycle();
        return count;
    }

    public ParcelFileDescriptor openFile(Uri url, String mode) throws RemoteException, FileNotFoundException {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();
        data.writeInterfaceToken(IContentProvider.descriptor);
        url.writeToParcel(data, 0);
        data.writeString(mode);
        this.mRemote.transact(14, data, reply, 0);
        DatabaseUtils.readExceptionWithFileNotFoundExceptionFromParcel(reply);
        ParcelFileDescriptor fd = reply.readInt() != 0 ? reply.readFileDescriptor() : null;
        data.recycle();
        reply.recycle();
        return fd;
    }

    /* Debug info: failed to restart local var, previous not found, register: 7 */
    public AssetFileDescriptor openAssetFile(Uri url, String mode) throws RemoteException, FileNotFoundException {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();
        data.writeInterfaceToken(IContentProvider.descriptor);
        url.writeToParcel(data, 0);
        data.writeString(mode);
        this.mRemote.transact(15, data, reply, 0);
        DatabaseUtils.readExceptionWithFileNotFoundExceptionFromParcel(reply);
        AssetFileDescriptor fd = reply.readInt() != 0 ? (AssetFileDescriptor) AssetFileDescriptor.CREATOR.createFromParcel(reply) : null;
        data.recycle();
        reply.recycle();
        return fd;
    }

    public Bundle call(String method, String request, Bundle args) throws RemoteException {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();
        data.writeInterfaceToken(IContentProvider.descriptor);
        data.writeString(method);
        data.writeString(request);
        data.writeBundle(args);
        this.mRemote.transact(21, data, reply, 0);
        DatabaseUtils.readExceptionFromParcel(reply);
        Bundle bundle = reply.readBundle();
        data.recycle();
        reply.recycle();
        return bundle;
    }
}
