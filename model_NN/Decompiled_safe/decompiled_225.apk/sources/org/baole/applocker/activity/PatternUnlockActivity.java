package org.baole.applocker.activity;

import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;
import com.anttek.widget.LockPatternUtils;
import com.anttek.widget.LockPatternView;
import java.util.List;
import org.baole.albs.R;
import org.baole.applocker.model.App;
import org.baole.applocker.model.Configuration;
import org.baole.applocker.service.ActivityWatcher;

public class PatternUnlockActivity extends UnlockActivity {
    App mApp;
    /* access modifiers changed from: private */
    public Configuration mConf;
    /* access modifiers changed from: private */
    public LockPatternView mLockPatternView;
    protected boolean mShouldBeClear = false;
    protected int mTryCount = 0;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.pattern_unlock_activity);
        this.mApp = (App) getIntent().getParcelableExtra(ActivityWatcher.APP);
        this.mConf = Configuration.getInstance(getApplicationContext());
        ((TextView) findViewById(R.id.text_hint)).setText(this.mConf.getPatternHint());
        this.mLockPatternView = (LockPatternView) findViewById(R.id.text_view1);
        this.mLockPatternView.setDisplayMode(LockPatternView.DisplayMode.Correct);
        this.mLockPatternView.setInStealthMode(this.mConf.mInvislblePattern);
        this.mLockPatternView.setOnPatternListener(new LockPatternView.OnPatternListener() {
            public void onPatternStart() {
                PatternUnlockActivity.this.mShouldBeClear = false;
            }

            public void onPatternDetected(List<LockPatternView.Cell> pattern) {
                if (LockPatternUtils.patternToString(pattern).equals(PatternUnlockActivity.this.mConf.mDefaultPattern.mExtra2)) {
                    PatternUnlockActivity.this.unlock(PatternUnlockActivity.this.mApp);
                    PatternUnlockActivity.this.finish();
                    return;
                }
                PatternUnlockActivity.this.mTryCount++;
                PatternUnlockActivity.this.checkLaunchLauncher(PatternUnlockActivity.this.mTryCount);
                PatternUnlockActivity.this.mShouldBeClear = true;
                PatternUnlockActivity.this.mLockPatternView.setDisplayMode(LockPatternView.DisplayMode.Wrong);
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        if (PatternUnlockActivity.this.mShouldBeClear) {
                            PatternUnlockActivity.this.mLockPatternView.clearPattern();
                        }
                    }
                }, 1000);
            }

            public void onPatternCleared() {
            }

            public void onPatternCellAdded(List<LockPatternView.Cell> list) {
                PatternUnlockActivity.this.mShouldBeClear = false;
            }
        });
    }
}
