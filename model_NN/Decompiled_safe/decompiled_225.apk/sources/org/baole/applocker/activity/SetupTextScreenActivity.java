package org.baole.applocker.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;
import org.baole.albs.R;
import org.baole.applocker.model.App;
import org.baole.applocker.model.Configuration;
import org.baole.applocker.service.ActivityWatcher;

public class SetupTextScreenActivity extends Activity implements View.OnClickListener {
    public static final String CONTENT = "_c";
    public static final String IS_RUNNING = "_ir";
    private App mApp;
    private CheckBox mCheckRunning;
    private EditText mEditContent;
    private boolean mIsEditable;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mIsEditable = true;
        setContentView((int) R.layout.setup_text_screen_activity);
        this.mApp = (App) getIntent().getParcelableExtra(ActivityWatcher.APP);
        this.mEditContent = (EditText) findViewById(R.id.edit_content);
        this.mCheckRunning = (CheckBox) findViewById(R.id.check_running);
        this.mEditContent.setEnabled(this.mIsEditable);
        String text = Configuration.getInstance(getApplicationContext()).mLatestTextTemplate;
        if (TextUtils.isEmpty(text)) {
            text = getString(R.string.text_template);
        }
        this.mEditContent.setText(text);
        this.mCheckRunning.setChecked(this.mApp.getExtra1() == 1);
        findViewById(R.id.button_preview).setOnClickListener(this);
        findViewById(R.id.button_save).setOnClickListener(this);
        findViewById(R.id.button_cancel).setOnClickListener(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_cancel:
                setResult(0);
                finish();
                return;
            case R.id.button_preview:
                this.mApp.setExtra2(this.mEditContent.getText().toString());
                this.mApp.setExtra1(this.mCheckRunning.isChecked() ? 1 : 0);
                Intent intent = new Intent(this, TextUnlockActivity.class);
                intent.putExtra(UnlockActivity.PREVIEW_MODE, true);
                intent.putExtra(ActivityWatcher.APP, this.mApp);
                startActivity(intent);
                Toast.makeText(this, (int) R.string.exit_preview_mode, 0).show();
                return;
            case R.id.button_save:
                String text = this.mEditContent.getText().toString();
                Intent data = new Intent();
                data.putExtra("_ir", this.mCheckRunning.isChecked());
                data.putExtra("_c", text);
                setResult(-1, data);
                Configuration.getInstance(getApplicationContext()).setLatestTextTemplate(text);
                finish();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        AppListActivity.mShouldCheckLockIntent = false;
    }
}
