package org.baole.applocker.activity;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.baole.albs.R;
import org.baole.applocker.db.DbHelper;
import org.baole.applocker.model.App;
import org.baole.applocker.model.Configuration;
import org.baole.applocker.model.LockMethod;
import org.baole.applocker.utils.L;

public class SetupActivity extends Activity {
    protected static final int SCAN_APPS_TASK = 1;
    protected static final int SCAN_APPS_TASK_DONE = 2;
    protected static final int TASK_DONE = 5;
    protected static final int VERIFY_TASK = 3;
    protected static final int VERIFY_TASK_DONE = 4;
    Button mButtonClose = null;
    Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    new ScanAppTask(SetupActivity.this.getApplicationContext()).execute(new Void[0]);
                    return;
                case 2:
                    Message m = new Message();
                    m.what = 3;
                    sendMessage(m);
                    return;
                case 3:
                    new VerifyDBTask(SetupActivity.this.getApplicationContext()).execute(new Void[0]);
                    return;
                case 4:
                    Message m2 = new Message();
                    m2.what = 5;
                    sendMessage(m2);
                    return;
                case 5:
                    SetupActivity.this.setResult(-1);
                    SetupActivity.this.mScanText.setText(SetupActivity.this.getString(R.string.app_locked_count, new Object[]{Integer.valueOf(SetupActivity.this.mPreLockCount)}));
                    SetupActivity.this.mButtonClose.setVisibility(0);
                    return;
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public int mPreLockCount = 0;
    /* access modifiers changed from: private */
    public ProgressBar mProgress;
    /* access modifiers changed from: private */
    public TextView mScanText;

    static /* synthetic */ int access$008(SetupActivity x0) {
        int i = x0.mPreLockCount;
        x0.mPreLockCount = i + 1;
        return i;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.setup_activity);
        this.mScanText = (TextView) findViewById(R.id.scan_apps);
        this.mScanText.setVisibility(8);
        this.mProgress = (ProgressBar) findViewById(R.id.progressBar1);
        this.mButtonClose = (Button) findViewById(R.id.button_close);
        this.mButtonClose.setVisibility(8);
        this.mButtonClose.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SetupActivity.this.finish();
            }
        });
        Message msg = new Message();
        msg.what = 1;
        this.mHandler.sendMessage(msg);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        AppListActivity.mShouldCheckLockIntent = false;
    }

    public void onBackPressed() {
    }

    class VerifyDBTask extends AsyncTask<Void, Object, Void> {
        private int mAppCount;
        private Context mContext;
        private DbHelper mHelper;

        public VerifyDBTask(Context c) {
            this.mContext = c;
            this.mHelper = DbHelper.getInstance(c);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            SetupActivity.this.mProgress.setVisibility(0);
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... params) {
            ArrayList<App> apps = this.mHelper.queryApps(null, null);
            this.mAppCount = apps.size();
            for (int i = 0; i < this.mAppCount; i++) {
                App app = apps.get(i);
                if (app.shouldBeRemoved(this.mContext)) {
                    this.mHelper.removeApp(app.getPackage());
                }
            }
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void result) {
            super.onPostExecute((Object) result);
            SetupActivity.this.mProgress.setVisibility(8);
            Message msg = new Message();
            msg.what = 4;
            SetupActivity.this.mHandler.sendMessage(msg);
        }
    }

    class ScanAppTask extends AsyncTask<Void, Integer, Integer> {
        private int mAppCount;
        private Context mContext;
        private DbHelper mHelper;
        private List<String> mPkgs;

        public ScanAppTask(Context c) {
            this.mContext = c;
            this.mHelper = DbHelper.getInstance(c);
            this.mPkgs = new ArrayList(Arrays.asList(c.getResources().getStringArray(R.array.pre_lock_packages)));
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            int unused = SetupActivity.this.mPreLockCount = 0;
            SetupActivity.this.mProgress.setVisibility(0);
            SetupActivity.this.mScanText.setVisibility(0);
        }

        /* JADX INFO: finally extract failed */
        /* access modifiers changed from: protected */
        public Integer doInBackground(Void... params) {
            PackageManager pm = SetupActivity.this.getPackageManager();
            List<PackageInfo> pkgs = pm.getInstalledPackages(8192);
            this.mAppCount = pkgs.size();
            int index = 0;
            ArrayList<App> caches = this.mHelper.queryApps("_use_lock=?", new String[]{"1"});
            this.mHelper.getDb().beginTransaction();
            try {
                for (PackageInfo pkg : pkgs) {
                    String packageName = pkg.packageName;
                    String versionName = pkg.versionName;
                    String name = packageName;
                    App app = new App();
                    app.setPackage(packageName);
                    app.setVersion(versionName);
                    if (pkg.applicationInfo != null) {
                        Drawable drawable = pkg.applicationInfo.loadIcon(pm);
                        if (drawable instanceof BitmapDrawable) {
                            app.setIcon(((BitmapDrawable) drawable).getBitmap());
                        }
                        name = pkg.applicationInfo.loadLabel(pm).toString();
                        app.setExtra1((long) pkg.applicationInfo.flags);
                    }
                    app.setName(name);
                    refineLockedApp(caches, app);
                    this.mHelper.insertOrUpdateApp(app, true, true);
                    index++;
                    publishProgress(Integer.valueOf(index));
                }
                this.mHelper.getDb().setTransactionSuccessful();
                this.mHelper.getDb().endTransaction();
                return null;
            } catch (Throwable th) {
                this.mHelper.getDb().endTransaction();
                throw th;
            }
        }

        private void refineLockedApp(ArrayList<App> caches, App app) {
            Iterator i$ = caches.iterator();
            while (i$.hasNext()) {
                App a = i$.next();
                if (a.getPackage().equals(app.getPackage())) {
                    app.setExtra1(a.getExtra1());
                    app.setExtra2(a.getExtra2());
                    app.setExtra3(a.getExtra3());
                    app.setLockMethod(a.getLockMethod());
                    app.setUseDefaultLockMethod(a.isUseDefaultLockMethod());
                    app.setUseLock(a.isUseLock());
                    return;
                }
            }
            if ((this.mPkgs.contains(app.getPackage()) || SetupActivity.this.getPackageName().equals(app.getPackage())) && !app.isUseLock()) {
                app.setLockMethod(LockMethod.USE_DEFAULT);
                app.setUseDefaultLockMethod(true);
                app.setUseLock(true);
                L.e("scan app: %s", app.toString());
                SetupActivity.access$008(SetupActivity.this);
            }
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(Integer... values) {
            SetupActivity.this.mScanText.setText(this.mContext.getString(R.string.scan_apps, String.format("%d of %d", values[0], Integer.valueOf(this.mAppCount))));
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Integer result) {
            super.onPostExecute((Object) result);
            SetupActivity.this.mProgress.setVisibility(8);
            SetupActivity.this.mScanText.setText(this.mContext.getString(R.string.scan_apps, "done"));
            Configuration.getInstance(this.mContext.getApplicationContext()).setPrefScanValue("1");
            Message msg = new Message();
            msg.what = 2;
            SetupActivity.this.mHandler.sendMessage(msg);
        }
    }
}
