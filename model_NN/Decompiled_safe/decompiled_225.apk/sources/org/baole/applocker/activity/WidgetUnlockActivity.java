package org.baole.applocker.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;
import java.util.ArrayList;
import org.baole.albs.R;
import org.baole.applocker.db.DbHelper;
import org.baole.applocker.model.App;
import org.baole.applocker.model.Configuration;
import org.baole.applocker.service.ActivityWatcher;
import org.baole.applocker.widget.AppLockerWidget;

public class WidgetUnlockActivity extends Activity {
    public static final String ACTION_CONFIG = "org.baole.applocker.ACTION_WIDGET_CONFIG";
    private static final int LOCK_INTENT = 102;
    private Configuration mConf;
    private DbHelper mHelper;
    private boolean mShouldCheckLockIntent = true;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        ArrayList<App> apps;
        super.onCreate(savedInstanceState);
        this.mConf = Configuration.getInstance(getApplicationContext());
        if (!"org.baole.applocker.ACTION_WIDGET_CONFIG".equals(getIntent().getAction())) {
            return;
        }
        if (this.mConf.mEnable) {
            this.mHelper = DbHelper.getInstance(getApplicationContext());
            if (this.mShouldCheckLockIntent) {
                String pkg = getPackageName();
                if (this.mConf.mLockAll) {
                    apps = this.mHelper.queryApps("_package=? ", new String[]{pkg});
                } else {
                    apps = this.mHelper.queryApps("_package=? AND _use_lock=?", new String[]{pkg, "1"});
                }
                Intent intent = null;
                if (apps != null && apps.size() > 0) {
                    intent = ActivityWatcher.lockIntent(this, apps.get(0), true);
                }
                if (intent != null) {
                    startActivityForResult(intent, 102);
                } else {
                    toggleEnable();
                }
            }
            this.mShouldCheckLockIntent = true;
            return;
        }
        toggleEnable();
    }

    private void toggleEnable() {
        this.mConf.setEnable(!this.mConf.mEnable);
        Toast.makeText(this, this.mConf.mEnable ? R.string.app_on : R.string.app_off, 0).show();
        AppLockerWidget.updateRemoteView(this);
        finish();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        this.mShouldCheckLockIntent = false;
        if (resultCode == -1 && requestCode == 102) {
            toggleEnable();
        }
    }
}
