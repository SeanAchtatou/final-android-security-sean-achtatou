package org.baole.applocker.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import org.baole.albs.R;
import org.baole.applocker.model.App;
import org.baole.applocker.model.Configuration;
import org.baole.applocker.service.ActivityWatcher;

public class PasswordUnlockActivity extends UnlockActivity {
    App mApp;
    /* access modifiers changed from: private */
    public Configuration mConf;
    private EditText mEditPassWord;
    /* access modifiers changed from: private */
    public int mTryCount = 0;

    static /* synthetic */ int access$108(PasswordUnlockActivity x0) {
        int i = x0.mTryCount;
        x0.mTryCount = i + 1;
        return i;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.password_unlock_activity);
        this.mApp = (App) getIntent().getParcelableExtra(ActivityWatcher.APP);
        this.mConf = Configuration.getInstance(getApplicationContext());
        ((TextView) findViewById(R.id.text_hint)).setText(this.mConf.getPassHint());
        this.mEditPassWord = (EditText) findViewById(R.id.edit_password);
        this.mEditPassWord.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s != null && PasswordUnlockActivity.this.mConf.mDefaultPassword.mExtra2 != null) {
                    String t = s.toString();
                    if (t.equals(PasswordUnlockActivity.this.mConf.mDefaultPassword.mExtra2)) {
                        PasswordUnlockActivity.this.unlock(PasswordUnlockActivity.this.mApp);
                        PasswordUnlockActivity.this.finish();
                    } else if (t.length() > PasswordUnlockActivity.this.mConf.mDefaultPassword.mExtra2.length()) {
                        PasswordUnlockActivity.access$108(PasswordUnlockActivity.this);
                        PasswordUnlockActivity.this.checkLaunchLauncher(PasswordUnlockActivity.this.mTryCount);
                    }
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
            }
        });
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_ok:
                if (this.mEditPassWord.getText().toString().equals(this.mConf.mDefaultPassword.mExtra2)) {
                    unlock(this.mApp);
                    finish();
                    return;
                }
                this.mTryCount++;
                checkLaunchLauncher(this.mTryCount);
                return;
            default:
                super.onClick(v);
                return;
        }
    }
}
