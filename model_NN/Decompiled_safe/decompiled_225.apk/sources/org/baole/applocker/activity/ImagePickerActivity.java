package org.baole.applocker.activity;

import android.app.Activity;
import android.app.IActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import org.baole.albs.R;
import org.baole.applocker.activity.adapter.ArrayAdapter;
import org.baole.applocker.model.Configuration;
import org.baole.applocker.utils.BitmapUtil;

public class ImagePickerActivity extends Activity implements View.OnClickListener {
    private static final String IMAGE_SRC = "images";
    public static String SELECTED_IMAGE = "_i";
    public static String SOURCE_TYPE = "type";
    public static int SOURCE_TYPE_BUILD_IN = 2;
    public static int SOURCE_TYPE_ICONFINDER = 3;
    /* access modifiers changed from: private */
    public ArrayAdapter<String> mAdapter;
    /* access modifiers changed from: private */
    public Button mButtonSearch;
    /* access modifiers changed from: private */
    public ArrayList<String> mData;
    /* access modifiers changed from: private */
    public EditText mEditSearchTerm;
    private Gallery mGallery;
    /* access modifiers changed from: private */
    public ProgressBar mProgress;
    /* access modifiers changed from: private */
    public int mSourceType;
    private TextView mTitle;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.image_picker_activity);
        this.mTitle = (TextView) findViewById(R.id.textview_title);
        this.mGallery = (Gallery) findViewById(R.id.gallery1);
        this.mData = new ArrayList<>();
        this.mAdapter = new ImageAdapter(this, this.mData);
        this.mGallery.setAdapter((SpinnerAdapter) this.mAdapter);
        this.mEditSearchTerm = (EditText) findViewById(R.id.edittext_search);
        this.mButtonSearch = (Button) findViewById(R.id.button_search);
        this.mProgress = (ProgressBar) findViewById(R.id.progressBar1);
        this.mButtonSearch.setOnClickListener(this);
        this.mEditSearchTerm.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == 0) {
                    switch (keyCode) {
                        case 23:
                        case IActivityManager.ENTER_SAFE_MODE_TRANSACTION:
                            if (!TextUtils.isEmpty(ImagePickerActivity.this.mEditSearchTerm.getText().toString())) {
                                new LoadIconTask().execute(new Boolean[0]);
                            }
                            return true;
                    }
                }
                return false;
            }
        });
        this.mEditSearchTerm.setText(Configuration.getInstance(getApplicationContext()).mSearchTerm);
        this.mSourceType = getIntent().getIntExtra(SOURCE_TYPE, SOURCE_TYPE_BUILD_IN);
        if (this.mSourceType == SOURCE_TYPE_BUILD_IN) {
            this.mTitle.setText((int) R.string.buildin_image_screen);
            findViewById(R.id.pane_search).setVisibility(8);
            new LoadIconTask().execute(new Boolean[0]);
        }
        this.mGallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int pos, long arg3) {
                Intent data = new Intent();
                data.putExtra(ImagePickerActivity.SELECTED_IMAGE, ImagePickerActivity.IMAGE_SRC + File.separator + ((String) ImagePickerActivity.this.mData.get(pos)));
                ImagePickerActivity.this.setResult(-1, data);
                ImagePickerActivity.this.finish();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        AppListActivity.mShouldCheckLockIntent = false;
    }

    class LoadIconTask extends AsyncTask<Boolean, Void, Void> {
        LoadIconTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            ImagePickerActivity.this.mButtonSearch.setEnabled(false);
            ImagePickerActivity.this.mEditSearchTerm.setEnabled(false);
            ImagePickerActivity.this.mProgress.setVisibility(0);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void result) {
            super.onPostExecute((Object) result);
            ImagePickerActivity.this.mButtonSearch.setEnabled(true);
            ImagePickerActivity.this.mEditSearchTerm.setEnabled(true);
            ImagePickerActivity.this.mProgress.setVisibility(8);
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Boolean... params) {
            if (ImagePickerActivity.this.mSourceType == ImagePickerActivity.SOURCE_TYPE_BUILD_IN) {
                loadBuildIn();
                return null;
            } else if (ImagePickerActivity.this.mSourceType != ImagePickerActivity.SOURCE_TYPE_ICONFINDER) {
                return null;
            } else {
                loadIconFinder();
                return null;
            }
        }

        private void loadIconFinder() {
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(Void... values) {
            super.onProgressUpdate((Object[]) values);
            ImagePickerActivity.this.mAdapter.notifyDataSetChanged();
        }

        private void loadBuildIn() {
            try {
                for (String item : ImagePickerActivity.this.getApplicationContext().getAssets().list(ImagePickerActivity.IMAGE_SRC)) {
                    if (item != null && item.startsWith("image")) {
                        ImagePickerActivity.this.mData.add(item);
                        publishProgress(new Void[0]);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public class ImageAdapter extends ArrayAdapter<String> {
        private LayoutInflater mInflater;

        public ImageAdapter(Context context, ArrayList<String> objects) {
            super(context, objects);
            this.mInflater = LayoutInflater.from(context);
        }

        public View getView(int position, View v, ViewGroup parent) {
            if (v == null) {
                v = this.mInflater.inflate((int) R.layout.image_item, (ViewGroup) null);
            }
            if (ImagePickerActivity.this.mSourceType == ImagePickerActivity.SOURCE_TYPE_BUILD_IN) {
                try {
                    ((ImageView) v).setImageBitmap(BitmapUtil.fromAssets(ImagePickerActivity.this.getAssets(), ImagePickerActivity.IMAGE_SRC + File.separator + ((String) getItem(position))));
                } catch (Throwable th) {
                }
            }
            return v;
        }
    }

    public void onClick(View v) {
        if (v == this.mButtonSearch && !TextUtils.isEmpty(this.mEditSearchTerm.getText().toString())) {
            new LoadIconTask().execute(new Boolean[0]);
        }
    }
}
