package org.baole.applocker.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import org.baole.albs.R;
import org.baole.applocker.model.App;
import org.baole.applocker.model.Configuration;
import org.baole.applocker.model.LockInfo;
import org.baole.applocker.utils.DialogUtil;
import org.baole.applocker.utils.EmailUtil;

public class UnlockActivity extends Activity implements View.OnClickListener {
    private static final int PASSWORD_RECOVERY = 1;
    public static final String PREVIEW_MODE = "_pm";
    private static final int RECOVERY_FAILED = 3;
    private static final int RECOVERY_OK = 2;
    Configuration mConf = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mConf = Configuration.getInstance(getApplicationContext());
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_cancel:
                activateLauncher();
                return;
            case R.id.button_ok:
            default:
                return;
            case R.id.button_code_recovery:
                requestCodeRecovery();
                return;
        }
    }

    public void unlock(App app) {
        this.mConf.mHasUnLock = true;
        this.mConf.mCurrentUnLockPkg = app.getPackage();
        this.mConf.mLastUnlockPkg = app.getPackage();
        this.mConf.mLastUnlock = Long.valueOf(System.currentTimeMillis());
        LockInfo li = new LockInfo();
        li.mHasUnlocked = true;
        li.mPkg = app.getPackage();
        li.mTime = this.mConf.mLastUnlock.longValue();
        this.mConf.mUnlockedCaches.put(li.mPkg, li);
        setResult(-1);
    }

    public void onBackPressed() {
    }

    /* access modifiers changed from: protected */
    public void checkLaunchLauncher(int tryCount) {
        if (tryCount >= this.mConf.getWrongToClose()) {
            activateLauncher();
        }
    }

    /* access modifiers changed from: protected */
    public void activateLauncher() {
        this.mConf.mHasUnLock = false;
        Intent home = new Intent();
        home.setAction("android.intent.action.MAIN");
        home.addCategory("android.intent.category.HOME");
        home.setFlags(270532608);
        startActivity(home);
        finish();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void requestCodeRecovery() {
        if (TextUtils.isEmpty(this.mConf.getRecoveryEmail())) {
            showDialog(1);
            return;
        }
        String msg = EmailUtil.buildRecoveryMessage(getApplicationContext());
        new SendEmailTask().execute(this.mConf.getRecoveryEmail(), msg);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                return DialogUtil.createInfoDialog(this, (int) R.string.code_recovery, (int) R.string.code_recovery_message);
            case 2:
                return DialogUtil.createInfoDialog(this, getString(R.string.code_recovery), getString(R.string.code_recovery_ok, new Object[]{this.mConf.getRecoveryEmail()}));
            case 3:
                return DialogUtil.createInfoDialog(this, (int) R.string.code_recovery, (int) R.string.code_recovery_fail);
            default:
                return null;
        }
    }

    class SendEmailTask extends AsyncTask<String, Void, String> {
        ProgressDialog dialog;

        SendEmailTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            this.dialog = ProgressDialog.show(UnlockActivity.this, "", UnlockActivity.this.getString(R.string.sending), true);
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            return EmailUtil.sendEmail(params[0], params[1]);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            this.dialog.dismiss();
            if ("0".equals(result)) {
                UnlockActivity.this.showDialog(2);
            } else {
                UnlockActivity.this.showDialog(3);
            }
        }
    }
}
