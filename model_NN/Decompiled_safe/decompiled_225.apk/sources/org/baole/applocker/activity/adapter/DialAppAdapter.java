package org.baole.applocker.activity.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import org.baole.albs.R;
import org.baole.applocker.model.App;

public class DialAppAdapter extends ArrayAdapter<App> {
    protected LayoutInflater mInflater;

    public static class ItemItemHolder {
        public ImageView mIcon;
        public TextView mName;
    }

    public DialAppAdapter(Context context, ArrayList<App> data) {
        super(context, data);
        this.mInflater = LayoutInflater.from(context);
    }

    public View getView(int position, View v, ViewGroup parent) {
        ItemItemHolder holder;
        if (v == null) {
            v = this.mInflater.inflate((int) R.layout.dial_app_item, (ViewGroup) null);
            holder = new ItemItemHolder();
            holder.mIcon = (ImageView) v.findViewById(R.id.item_icon);
            holder.mName = (TextView) v.findViewById(R.id.item_name);
            v.setTag(holder);
        } else {
            holder = (ItemItemHolder) v.getTag();
        }
        App data = (App) getItem(position);
        holder.mName.setText(data.getName());
        Bitmap icon = data.getIcon();
        if (icon == null) {
            holder.mIcon.setImageResource(R.drawable.icon);
        } else {
            holder.mIcon.setImageBitmap(icon);
        }
        return v;
    }
}
