package org.baole.applocker.activity;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import org.baole.albs.R;
import org.baole.applocker.model.App;
import org.baole.applocker.model.Configuration;
import org.baole.applocker.service.ActivityWatcher;
import org.baole.applocker.utils.BitmapUtil;

public class ImageUnlockActivity extends UnlockActivity implements View.OnClickListener {
    App mApp;
    long mClickCount = 0;
    private Configuration mConf;
    private ImageView mImageView1;
    private Boolean mPreview;
    private int mTryCount = 0;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.image_screen_unlock_activity);
        this.mPreview = Boolean.valueOf(getIntent().getBooleanExtra(UnlockActivity.PREVIEW_MODE, false));
        this.mApp = (App) getIntent().getParcelableExtra(ActivityWatcher.APP);
        this.mConf = Configuration.getInstance(getApplicationContext());
        if (!this.mPreview.booleanValue()) {
            findViewById(R.id.text_view2).setOnClickListener(this);
            findViewById(R.id.image_view).setOnClickListener(this);
        }
        this.mImageView1 = (ImageView) findViewById(R.id.image_view);
        setSelectedImage(this.mApp);
    }

    private void setSelectedImage(App app) {
        try {
            if (TextUtils.isEmpty(app.getExtra2())) {
                app.setExtra1(1);
                app.setExtra2("images/image_3.jpg");
            }
            if (app.getExtra1() == 1) {
                this.mImageView1.setImageBitmap(BitmapUtil.fromAssets(getAssets(), app.getExtra2()));
            } else if (app.getExtra1() == 2) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 2;
                this.mImageView1.setImageBitmap(BitmapFactory.decodeFile(app.getExtra2(), options));
            }
        } catch (Throwable th) {
            Toast.makeText(this, "Can not load image!", 0).show();
        }
    }

    public void onBackPressed() {
        if (this.mPreview.booleanValue()) {
            finish();
        } else {
            super.onBackPressed();
        }
    }

    public void onClick(View v) {
        if (v.getId() == R.id.text_view2) {
            this.mClickCount++;
            if (this.mClickCount >= this.mConf.mDefaultBlackScreen.mExtra1) {
                unlock(this.mApp);
                finish();
            }
        } else if (v.getId() == R.id.imageView1) {
            this.mClickCount = 0;
            this.mTryCount++;
            checkLaunchLauncher(this.mTryCount);
        }
    }
}
