package org.baole.applocker.activity;

import android.content.Intent;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.widget.Toast;
import java.util.ArrayList;
import org.baole.albs.R;
import org.baole.applocker.db.DbHelper;
import org.baole.applocker.model.App;
import org.baole.applocker.model.Configuration;
import org.baole.applocker.service.ActivityWatcher;
import org.baole.applocker.service.AppLockerService;
import org.baole.applocker.widget.AppLockerWidget;

public class Settings extends PreferenceActivity implements Preference.OnPreferenceClickListener {
    public static final String ACTION_CONFIG = "org.baole.applocker.ACTION_WIDGET_CONFIG";
    private static final int LOCK_INTENT = 102;
    private static final int SETUP_PATTERN = 100;
    private Configuration mConf;
    private CheckBoxPreference mEnablePref;
    private DbHelper mHelper;
    private Preference mPatternPref;
    private boolean mShouldCheckLockIntent = true;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        boolean z;
        ArrayList<App> apps;
        Intent intent;
        super.onCreate(savedInstanceState);
        this.mConf = Configuration.getInstance(getApplicationContext());
        addPreferencesFromResource(R.xml.preferences);
        if ("org.baole.applocker.ACTION_WIDGET_CONFIG".equals(getIntent().getAction())) {
            if (this.mConf.mEnable) {
                this.mHelper = DbHelper.getInstance(getApplicationContext());
                if (this.mConf.mEnable && this.mShouldCheckLockIntent) {
                    String pkg = getPackageName();
                    if (this.mConf.mLockAll) {
                        apps = this.mHelper.queryApps("_package=? ", new String[]{pkg});
                    } else {
                        apps = this.mHelper.queryApps("_package=? AND _use_lock=?", new String[]{pkg, "1"});
                    }
                    if (!(apps == null || apps.size() <= 0 || (intent = ActivityWatcher.lockIntent(this, apps.get(0), true)) == null)) {
                        startActivityForResult(intent, 102);
                    }
                }
                this.mShouldCheckLockIntent = true;
            } else {
                Configuration configuration = this.mConf;
                if (!this.mConf.mEnable) {
                    z = true;
                } else {
                    z = false;
                }
                configuration.setEnable(z);
                AppLockerWidget.updateRemoteView(this);
                Toast.makeText(this, (int) R.string.app_on, 0).show();
                finish();
            }
        }
        this.mPatternPref = findPreference(Configuration.KEY_DEFAULT_PATTERN);
        if (this.mPatternPref != null) {
            this.mPatternPref.setOnPreferenceClickListener(this);
        }
        this.mEnablePref = (CheckBoxPreference) findPreference(Configuration.KEY_ENABLE);
        if (this.mEnablePref != null) {
            this.mEnablePref.setOnPreferenceClickListener(this);
        }
    }

    public boolean onPreferenceClick(Preference preference) {
        if (preference != null) {
            if (preference.equals(this.mPatternPref)) {
                startActivityForResult(new Intent(this, SetupPatternActivity.class), 100);
            } else if (preference.equals(this.mEnablePref)) {
                this.mConf.mEnable = this.mEnablePref.isChecked();
                AppLockerWidget.updateRemoteView(this);
                if (this.mConf.mEnable) {
                    AppLockerService.startActivityWatcherService(getApplicationContext());
                    Toast.makeText(this, (int) R.string.app_on, 0);
                } else {
                    AppLockerService.stopActivityWatcherService(getApplicationContext());
                    Toast.makeText(this, (int) R.string.app_off, 1);
                }
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        this.mShouldCheckLockIntent = false;
        if (resultCode == -1 && requestCode == 100) {
            this.mConf.setDefaultPattern(data.getStringExtra(SetupPatternActivity.PATTERN));
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.mConf.readPref();
        this.mConf.writeDb();
        AppListActivity.mShouldCheckLockIntent = false;
    }
}
