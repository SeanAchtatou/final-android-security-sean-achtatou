package org.baole.applocker.activity.adapter;

import android.app.IApplicationThread;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import org.baole.albs.R;
import org.baole.applocker.db.DbHelper;
import org.baole.applocker.model.App;
import org.baole.applocker.model.Configuration;
import org.baole.applocker.model.LockMethod;

public class AppListAdapter extends ArrayAdapter<App> {
    /* access modifiers changed from: private */
    public Configuration mConf = null;
    /* access modifiers changed from: private */
    public DbHelper mHelper;
    protected LayoutInflater mInflater;

    public static class ItemItemHolder {
        public ImageView mIcon;
        public TextView mLockMethod;
        public CheckBox mLocked;
        public TextView mName;
        public TextView mPackage;
    }

    public AppListAdapter(Context context, ArrayList<App> data) {
        super(context, data);
        this.mInflater = LayoutInflater.from(context);
        this.mConf = Configuration.getInstance(context.getApplicationContext());
        this.mHelper = DbHelper.getInstance(context);
    }

    public View getView(int position, View v, ViewGroup parent) {
        ItemItemHolder holder;
        if (v == null) {
            v = this.mInflater.inflate((int) R.layout.item_item, (ViewGroup) null);
            holder = new ItemItemHolder();
            holder.mIcon = (ImageView) v.findViewById(R.id.item_icon);
            holder.mName = (TextView) v.findViewById(R.id.item_name);
            holder.mPackage = (TextView) v.findViewById(R.id.item_package);
            holder.mLockMethod = (TextView) v.findViewById(R.id.item_lock_method);
            holder.mLocked = (CheckBox) v.findViewById(R.id.item_lock);
            holder.mLocked.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    App app = (App) v.getTag();
                    app.setUseLock(((CheckBox) v).isChecked());
                    if (AppListAdapter.this.mConf.mResetToDefault) {
                        app.setLockMethod(LockMethod.USE_DEFAULT);
                    }
                    AppListAdapter.this.mHelper.insertOrUpdateApp(app, new Boolean[0]);
                    AppListAdapter.this.notifyDataSetChanged();
                }
            });
            v.setTag(holder);
        } else {
            holder = (ItemItemHolder) v.getTag();
        }
        App data = (App) getItem(position);
        holder.mLocked.setTag(data);
        holder.mName.setText(data.getName());
        holder.mPackage.setText(data.getPackage());
        holder.mLocked.setChecked(data.isUseLock());
        if (!data.isUseLock() || data.getLockMethod() == LockMethod.NONE) {
            holder.mLockMethod.setVisibility(8);
        } else {
            holder.mLockMethod.setVisibility(0);
            holder.mLockMethod.setText(getLockMethodText(data.isUseLock(), data.getLockMethod()));
        }
        Bitmap icon = data.getIcon();
        if (icon == null) {
            holder.mIcon.setImageResource(R.drawable.icon);
        } else {
            holder.mIcon.setImageBitmap(icon);
        }
        return v;
    }

    /* access modifiers changed from: package-private */
    public String getLockMethodText(boolean isLocked, LockMethod m) {
        if (!isLocked) {
            return null;
        }
        switch (AnonymousClass2.$SwitchMap$org$baole$applocker$model$LockMethod[m.ordinal()]) {
            case 1:
                return getContext().getString(R.string.lock_method_pin);
            case 2:
                return getContext().getString(R.string.lock_method_password);
            case 3:
                return getContext().getString(R.string.lock_method_pattern);
            case 4:
                return getContext().getString(R.string.lock_method_dialog);
            case 5:
                return getContext().getString(R.string.lock_method_dial);
            case 6:
                return getContext().getString(R.string.lock_method_default);
            case IApplicationThread.SCHEDULE_LAUNCH_ACTIVITY_TRANSACTION:
                return getContext().getString(R.string.lock_method_text_screen);
            case 8:
                return getContext().getString(R.string.lock_method_image_screen);
            case IApplicationThread.SCHEDULE_FINISH_ACTIVITY_TRANSACTION:
                return getContext().getString(R.string.lock_method_fc);
            default:
                return null;
        }
    }

    /* renamed from: org.baole.applocker.activity.adapter.AppListAdapter$2  reason: invalid class name */
    static /* synthetic */ class AnonymousClass2 {
        static final /* synthetic */ int[] $SwitchMap$org$baole$applocker$model$LockMethod = new int[LockMethod.values().length];

        static {
            try {
                $SwitchMap$org$baole$applocker$model$LockMethod[LockMethod.PIN.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$org$baole$applocker$model$LockMethod[LockMethod.PASSWORD.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$org$baole$applocker$model$LockMethod[LockMethod.PATTERN.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$org$baole$applocker$model$LockMethod[LockMethod.DIALOG.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$org$baole$applocker$model$LockMethod[LockMethod.DIAL.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$org$baole$applocker$model$LockMethod[LockMethod.USE_DEFAULT.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$org$baole$applocker$model$LockMethod[LockMethod.TEXT_SCREEN.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$org$baole$applocker$model$LockMethod[LockMethod.IMAGE_SCREEN.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                $SwitchMap$org$baole$applocker$model$LockMethod[LockMethod.FORCE_CLOSE.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
        }
    }
}
