package org.baole.applocker.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.anttek.widget.LockPatternUtils;
import com.anttek.widget.LockPatternView;
import java.util.List;
import org.baole.albs.R;

public class SetupPatternActivity extends Activity implements LockPatternView.OnPatternListener, View.OnClickListener {
    public static String PATTERN = "_patetrn";
    private View mButtonCancel;
    private View mButtonConfirm;
    private View mButtonRetry;
    /* access modifiers changed from: private */
    public LockPatternView mLockPatternView;
    private String mPattern;
    protected boolean mShouldBeClear;
    private int mState = 0;
    private TextView mTextTitle;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mState = 0;
        setContentView((int) R.layout.setup_pattern_activity);
        this.mLockPatternView = (LockPatternView) findViewById(R.id.pattern_view);
        this.mLockPatternView.setDisplayMode(LockPatternView.DisplayMode.Correct);
        this.mLockPatternView.setOnPatternListener(this);
        this.mButtonCancel = findViewById(R.id.button_cancel);
        this.mButtonRetry = findViewById(R.id.button_retry);
        this.mButtonConfirm = findViewById(R.id.button_confirm);
        this.mTextTitle = (TextView) findViewById(R.id.textview_title);
        this.mButtonCancel.setOnClickListener(this);
        this.mButtonRetry.setOnClickListener(this);
        this.mButtonConfirm.setOnClickListener(this);
        refineStateButtons();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        AppListActivity.mShouldCheckLockIntent = false;
    }

    public void onPatternStart() {
        this.mShouldBeClear = false;
    }

    public void onPatternDetected(List<LockPatternView.Cell> pattern) {
        if (pattern == null || pattern.size() < 4) {
            if (this.mState == 0) {
                Toast.makeText(this, (int) R.string.draw_unlock_pattern_warning, 1).show();
            } else {
                this.mState = 0;
                Toast.makeText(this, (int) R.string.draw_unlock_pattern_confirm_wrong, 1).show();
            }
            this.mLockPatternView.setDisplayMode(LockPatternView.DisplayMode.Wrong);
            clearPattern();
        } else if (this.mState == 0) {
            this.mState = 1;
            this.mPattern = LockPatternUtils.patternToString(pattern);
            clearPattern();
        } else {
            if (this.mPattern.equals(LockPatternUtils.patternToString(pattern))) {
                this.mState = 2;
                clearPattern();
            } else {
                this.mState = 0;
                this.mPattern = "";
                Toast.makeText(this, (int) R.string.draw_unlock_pattern_confirm_wrong, 1).show();
                this.mLockPatternView.setDisplayMode(LockPatternView.DisplayMode.Wrong);
                this.mShouldBeClear = true;
                clearPattern();
            }
        }
        refineStateButtons();
    }

    private void clearPattern() {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                if (SetupPatternActivity.this.mShouldBeClear) {
                    SetupPatternActivity.this.mLockPatternView.clearPattern();
                }
            }
        }, 1000);
    }

    private void refineStateButtons() {
        switch (this.mState) {
            case 0:
                this.mButtonRetry.setVisibility(8);
                this.mButtonConfirm.setVisibility(8);
                this.mTextTitle.setText((int) R.string.draw_unlock_pattern);
                return;
            case 1:
                this.mButtonRetry.setVisibility(0);
                this.mButtonConfirm.setVisibility(8);
                this.mTextTitle.setText((int) R.string.draw_unlock_pattern_confirm);
                return;
            case 2:
                this.mButtonRetry.setVisibility(0);
                this.mButtonConfirm.setVisibility(0);
                this.mTextTitle.setText((int) R.string.draw_unlock_pattern_finish);
                return;
            default:
                return;
        }
    }

    public void onPatternCleared() {
    }

    public void onPatternCellAdded(List<LockPatternView.Cell> list) {
        this.mShouldBeClear = false;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_cancel:
                finish();
                return;
            case R.id.button_retry:
                this.mState = 0;
                this.mLockPatternView.clearPattern();
                refineStateButtons();
                return;
            case R.id.button_confirm:
                Intent data = new Intent();
                data.putExtra(PATTERN, this.mPattern);
                setResult(-1, data);
                finish();
                return;
            default:
                return;
        }
    }
}
