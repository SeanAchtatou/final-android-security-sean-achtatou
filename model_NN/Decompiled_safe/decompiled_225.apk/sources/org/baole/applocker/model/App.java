package org.baole.applocker.model;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

public class App implements Parcelable {
    public static final Parcelable.Creator<App> CREATOR = new Parcelable.Creator<App>() {
        public App createFromParcel(Parcel in) {
            return new App(in);
        }

        public App[] newArray(int size) {
            return new App[size];
        }
    };
    public static final int FALSE = 0;
    public static final int IMAGE_SRC_BUILDIN = 1;
    public static final int IMAGE_SRC_GALLERY = 2;
    public static final int TEXT_NOT_RUNNING = 2;
    public static final int TEXT_RUNNING = 1;
    public static final int TRUE = 1;
    private long mExtra1;
    private String mExtra2;
    private String mExtra3;
    private Bitmap mIcon;
    private long mId;
    private LockMethod mLockMethod;
    private String mName;
    private String mPackage;
    private boolean mUseDefaultLockMethod;
    private boolean mUseLock;
    private String mVersion;

    public App() {
        this.mLockMethod = LockMethod.NONE;
        this.mUseDefaultLockMethod = true;
        this.mUseLock = false;
    }

    public App clone() {
        App app = new App();
        app.mName = this.mName;
        app.mIcon = this.mIcon;
        app.mPackage = this.mPackage;
        app.mVersion = this.mVersion;
        app.mLockMethod = this.mLockMethod;
        app.mUseDefaultLockMethod = this.mUseDefaultLockMethod;
        app.mUseLock = this.mUseLock;
        app.mId = this.mId;
        app.mExtra1 = this.mExtra1;
        app.mExtra2 = this.mExtra2;
        app.mExtra3 = this.mExtra3;
        return app;
    }

    public final boolean isUseDefaultLockMethod() {
        return this.mUseDefaultLockMethod;
    }

    public final void setUseDefaultLockMethod(boolean useDefaultLockMethod) {
        this.mUseDefaultLockMethod = useDefaultLockMethod;
    }

    public final LockMethod getLockMethod() {
        return this.mLockMethod;
    }

    public final void setLockMethod(LockMethod lockMethod) {
        this.mLockMethod = lockMethod;
    }

    public final long getExtra1() {
        return this.mExtra1;
    }

    public final void setExtra1(long extra1) {
        this.mExtra1 = extra1;
    }

    public final String getExtra2() {
        return this.mExtra2;
    }

    public final void setExtra2(String extra2) {
        this.mExtra2 = extra2;
    }

    public final String getExtra3() {
        return this.mExtra3;
    }

    public final void setExtra3(String extra3) {
        this.mExtra3 = extra3;
    }

    public final long getId() {
        return this.mId;
    }

    public final void setId(long id) {
        this.mId = id;
    }

    public final String getName() {
        return this.mName;
    }

    public final void setName(String name) {
        this.mName = name;
    }

    public final Bitmap getIcon() {
        return this.mIcon;
    }

    public final void setIcon(Bitmap icon) {
        this.mIcon = icon;
    }

    public final String getPackage() {
        return this.mPackage;
    }

    public final void setPackage(String package1) {
        this.mPackage = package1;
    }

    public void writeToParcel(Parcel par, int flags) {
        int i;
        par.writeLong(this.mId);
        par.writeInt(this.mLockMethod.getType());
        par.writeString(this.mPackage);
        par.writeString(this.mVersion);
        par.writeString(this.mName);
        par.writeInt(this.mUseDefaultLockMethod ? 1 : 0);
        if (this.mUseLock) {
            i = 1;
        } else {
            i = 0;
        }
        par.writeInt(i);
        par.writeLong(this.mExtra1);
        par.writeString(this.mExtra2);
        par.writeString(this.mExtra3);
        par.writeParcelable(this.mIcon, 0);
    }

    protected App(Parcel in) {
        boolean z;
        this.mId = in.readLong();
        this.mLockMethod = LockMethod.getMethod(in.readInt());
        this.mPackage = in.readString();
        this.mVersion = in.readString();
        this.mName = in.readString();
        this.mUseDefaultLockMethod = in.readInt() == 1;
        if (in.readInt() == 1) {
            z = true;
        } else {
            z = false;
        }
        this.mUseLock = z;
        this.mExtra1 = in.readLong();
        this.mExtra2 = in.readString();
        this.mExtra3 = in.readString();
        this.mIcon = (Bitmap) in.readParcelable(Bitmap.class.getClassLoader());
    }

    public final boolean isUseLock() {
        return this.mUseLock;
    }

    public final void setUseLock(boolean useLock) {
        this.mUseLock = useLock;
    }

    public final String getVersion() {
        return this.mVersion;
    }

    public final void setVersion(String version) {
        this.mVersion = version;
    }

    public int describeContents() {
        return 0;
    }

    public String toString() {
        return "App [mId=" + this.mId + ", mName=" + this.mName + ", mIcon=" + this.mIcon + ", mPackage=" + this.mPackage + ", mVersion=" + this.mVersion + ", mUseDefaultLockMethod=" + this.mUseDefaultLockMethod + ", mUseLock=" + this.mUseLock + ", mLockMethod=" + this.mLockMethod + ", mExtra1=" + this.mExtra1 + ", mExtra2=" + this.mExtra2 + ", mExtra3=" + this.mExtra3 + "]";
    }

    public boolean shouldBeRemoved(Context c) {
        try {
            if (c.getPackageManager().getApplicationInfo(this.mPackage, 0) != null) {
                return false;
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return true;
    }
}
