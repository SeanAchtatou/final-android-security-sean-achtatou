package org.baole.applocker.simplified;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.anttek.quickactions.BetterPopupWindow;
import com.markupartist.android.widget.ActionBar;
import java.util.ArrayList;
import java.util.Iterator;
import org.baole.albs.R;
import org.baole.applocker.activity.HowtoActivity;
import org.baole.applocker.activity.Settings;
import org.baole.applocker.activity.SetupActivity;
import org.baole.applocker.activity.SetupImageScreenActivity;
import org.baole.applocker.activity.adapter.AppListAdapter;
import org.baole.applocker.db.DbHelper;
import org.baole.applocker.model.App;
import org.baole.applocker.model.Configuration;
import org.baole.applocker.model.LockMethod;
import org.baole.applocker.service.ActivityWatcher;
import org.baole.applocker.service.AppLockerService;
import org.baole.applocker.utils.DialogUtil;
import org.baole.applocker.utils.L;

public class AppListActivity extends Activity implements View.OnClickListener, AdapterView.OnItemClickListener {
    public static final int ACTIVITY_ITEM = 200;
    protected static final int ACTIVITY_SETUP = 104;
    private static final int ACTIVITY_SETUP_IMAGE = 105;
    private static final int ACTIVITY_SETUP_TEXT = 103;
    private static final int DIALOG_CHANGLOGS = 100;
    private static final int LOCK_INTENT = 102;
    protected AppListAdapter mAdapter;
    /* access modifiers changed from: private */
    public int mAppFilter = R.id.action_all_apps;
    /* access modifiers changed from: private */
    public Configuration mConf;
    protected int mCurCheckPosition = 0;
    protected ArrayList<App> mData;
    private LockMethod mDefaultLockMethod = LockMethod.USE_DEFAULT;
    private EditText mEditTextFilter;
    private ImageView mFilterAppView;
    private View mFilterBar;
    private DbHelper mHelper;
    /* access modifiers changed from: private */
    public boolean mIsShowFilter;
    protected ListView mListView;
    private View mOnOffView;
    protected ArrayList<App> mOrgData;
    private boolean mPause = false;
    protected FilterPopupWindow mSettingPopupWindow;
    private boolean mShouldCheckLockIntent = true;
    private TextView mStatus;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mHelper = DbHelper.getInstance(this);
        this.mConf = Configuration.getInstance(getApplicationContext());
        try {
            this.mDefaultLockMethod = LockMethod.getMethod(Integer.parseInt(getString(R.string.default_lock_method)));
        } catch (NumberFormatException e) {
            this.mDefaultLockMethod = LockMethod.USE_DEFAULT;
        }
        setContentView((int) R.layout.applist_activity);
        setupActionBar();
        AppLockerService.startActivityWatcherService(getApplicationContext());
        if (getLastNonConfigurationInstance() != null) {
            this.mShouldCheckLockIntent = false;
        }
        this.mOrgData = this.mHelper.queryApps(null, null);
        this.mData = new ArrayList<>();
        this.mAppFilter = R.id.action_all_apps;
        this.mIsShowFilter = false;
        this.mFilterBar = findViewById(R.id.filter_bar);
        refreshFilterBar();
        createHeaderView();
        this.mAdapter = new AppListAdapter(this, this.mData);
        this.mListView = (ListView) findViewById(16908298);
        this.mListView.setEmptyView(findViewById(R.id.empty));
        this.mListView.setOnItemClickListener(this);
        this.mListView.setAdapter((ListAdapter) this.mAdapter);
        refineAppFilter();
        registerForContextMenu(this.mListView);
        checkImport();
        String v0 = this.mConf.getLastVersion("0");
        String v1 = getString(R.string.app_version);
        if (!v0.equals(v1)) {
            this.mConf.setLastVersion(v1);
            showDialog(100);
            startActivity(new Intent(this, HowtoActivity.class));
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    public Object onRetainNonConfigurationInstance() {
        return new Object();
    }

    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 100:
                return DialogUtil.createChangLogDialog(this, R.string.updates_title, R.array.updates);
            default:
                return null;
        }
    }

    private void checkImport() {
        if (this.mConf.getPrefScanValue("0").compareTo("1") < 0) {
            startActivityForResult(new Intent(this, SetupActivity.class), 104);
        }
    }

    private void changeAppAttributes(App app, LockMethod lm) {
        app.setUseLock(lm != LockMethod.NONE);
        app.setLockMethod(lm);
        this.mHelper.insertOrUpdateApp(app, new Boolean[0]);
        this.mAdapter.notifyDataSetChanged();
    }

    private void configureLocker(int pos, boolean useLock, LockMethod lm, Class clz, int activitySetupText) {
        if (pos >= 0 && pos < this.mAdapter.getCount()) {
            App a = ((App) this.mAdapter.getItem(pos)).clone();
            a.setUseLock(useLock);
            if (useLock) {
                a.setLockMethod(lm);
            }
            Intent intent = new Intent(this, clz);
            intent.putExtra(ActivityWatcher.APP, a);
            startActivityForResult(intent, activitySetupText);
        }
    }

    private void refineAppFilter() {
        this.mData.clear();
        switch (this.mAppFilter) {
            case R.id.action_third_party_apps:
                Iterator i$ = this.mOrgData.iterator();
                while (i$.hasNext()) {
                    App app = i$.next();
                    if ((app.getExtra1() & 1) == 0) {
                        this.mData.add(app);
                    }
                }
                this.mFilterAppView.setImageResource(R.drawable.ic_filter_3rd);
                break;
            case R.id.action_system_apps:
                Iterator i$2 = this.mOrgData.iterator();
                while (i$2.hasNext()) {
                    App app2 = i$2.next();
                    if ((app2.getExtra1() & 1) != 0) {
                        this.mData.add(app2);
                    }
                }
                this.mFilterAppView.setImageResource(R.drawable.ic_filter_sys);
                break;
            case R.id.action_locked_apps:
                Iterator i$3 = this.mOrgData.iterator();
                while (i$3.hasNext()) {
                    App app3 = i$3.next();
                    if (app3.isUseLock()) {
                        this.mData.add(app3);
                    }
                }
                this.mFilterAppView.setImageResource(R.drawable.ic_filter_lock);
                break;
            case R.id.action_unlocked_apps:
                Iterator i$4 = this.mOrgData.iterator();
                while (i$4.hasNext()) {
                    App app4 = i$4.next();
                    if (!app4.isUseLock()) {
                        this.mData.add(app4);
                    }
                }
                this.mFilterAppView.setImageResource(R.drawable.ic_filter_unlock);
                break;
            default:
                this.mData.addAll(this.mOrgData);
                this.mFilterAppView.setImageResource(R.drawable.ic_filter_all);
                break;
        }
        this.mAdapter.notifyDataSetChanged();
        this.mFilterAppView.invalidate();
        this.mFilterAppView.setSelected(!this.mFilterAppView.isSelected());
    }

    /* access modifiers changed from: private */
    public void refreshFilterBar() {
        if (this.mIsShowFilter) {
            this.mFilterBar.setVisibility(0);
        } else {
            this.mFilterBar.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public void doFilter(CharSequence s) {
        if (this.mAdapter != null) {
            this.mAdapter.getFilter().filter(s);
        }
    }

    public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long arg3) {
        L.e("testing onClick() @ %d", Integer.valueOf(position));
        if (position != -1) {
            this.mCurCheckPosition = position;
            App app = (App) this.mAdapter.getItem(this.mCurCheckPosition);
            if (app.getLockMethod() == LockMethod.NONE) {
                changeAppAttributes(app, this.mDefaultLockMethod);
            } else {
                changeAppAttributes(app, LockMethod.NONE);
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        this.mShouldCheckLockIntent = false;
        if (resultCode == -1) {
            switch (requestCode) {
                case 103:
                    App app = (App) this.mAdapter.getItem(this.mCurCheckPosition);
                    app.setUseLock(true);
                    app.setLockMethod(LockMethod.TEXT_SCREEN);
                    app.setExtra1(data.getBooleanExtra("_ir", true) ? 1 : 0);
                    app.setExtra2(data.getStringExtra("_c"));
                    this.mHelper.insertOrUpdateApp(app, new Boolean[0]);
                    this.mAdapter.notifyDataSetChanged();
                    break;
                case 104:
                    this.mOrgData = this.mHelper.queryApps(null, null);
                    if (this.mOrgData != null) {
                        refineAppFilter();
                        break;
                    }
                    break;
                case 105:
                    App app2 = (App) this.mAdapter.getItem(this.mCurCheckPosition);
                    app2.setUseLock(true);
                    app2.setLockMethod(LockMethod.IMAGE_SCREEN);
                    app2.setExtra1(data.getLongExtra(SetupImageScreenActivity.IMAGE_SRC_TYPE, 0));
                    app2.setExtra2(data.getStringExtra(SetupImageScreenActivity.IMAGE_SRC_PATH));
                    this.mHelper.insertOrUpdateApp(app2, new Boolean[0]);
                    this.mAdapter.notifyDataSetChanged();
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
        this.mShouldCheckLockIntent = true;
    }

    /* access modifiers changed from: protected */
    public void createHeaderView() {
        this.mStatus = (TextView) findViewById(R.id.textview_status);
        this.mStatus.setGravity(1);
        findViewById(R.id.button_clear).setOnClickListener(this);
        this.mEditTextFilter = (EditText) findViewById(R.id.edittext_search);
        this.mEditTextFilter.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                AppListActivity.this.doFilter(s);
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        ArrayList<App> apps;
        Intent intent;
        super.onResume();
        L.e("activate this code, check=%s, unlock %s, curpkg %s", Boolean.valueOf(this.mShouldCheckLockIntent), Boolean.valueOf(this.mConf.mHasUnLock), this.mConf.mCurrentUnLockPkg);
        this.mOnOffView.setSelected(this.mConf.mEnable);
        setLockerStatus();
        if (this.mConf.mHasUnLock && getPackageName().equals(this.mConf.mCurrentUnLockPkg)) {
            this.mConf.mCurrentUnLockPkg = "";
        } else if (this.mConf.mEnable && this.mShouldCheckLockIntent) {
            String pkg = getPackageName();
            if (this.mConf.mLockAll) {
                apps = this.mHelper.queryApps("_package=? ", new String[]{pkg});
            } else {
                apps = this.mHelper.queryApps("_package=? AND _use_lock=?", new String[]{pkg, "1"});
            }
            if (!(apps == null || apps.size() <= 0 || (intent = ActivityWatcher.lockIntent(this, apps.get(0), true)) == null)) {
                startActivityForResult(intent, 102);
            }
        }
        this.mShouldCheckLockIntent = true;
        this.mPause = false;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.mPause = true;
    }

    private void setLockerStatus() {
        String validity;
        switch (this.mConf.mValidity) {
            case -1:
                validity = getString(R.string.screen_off);
                break;
            case 0:
                validity = getString(R.string.app_background);
                break;
            default:
                validity = getString(R.string.n_minutes, new Object[]{Integer.valueOf(this.mConf.mValidity)});
                break;
        }
        this.mStatus.setText(getString(R.string.locker_status, new Object[]{validity}));
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_clear:
                this.mEditTextFilter.setText("");
                this.mIsShowFilter = false;
                refreshFilterBar();
                return;
            case R.id.action_all_apps:
            case R.id.action_third_party_apps:
            case R.id.action_system_apps:
            case R.id.action_locked_apps:
            case R.id.action_unlocked_apps:
                this.mAppFilter = v.getId();
                refineAppFilter();
                this.mSettingPopupWindow.dismiss();
                return;
            default:
                return;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_option_menu, menu);
        menu.findItem(R.id.donate).setVisible(false);
        return super.onCreateOptionsMenu(menu);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_rescan_apps:
                startActivityForResult(new Intent(this, SetupActivity.class), 104);
                break;
            case R.id.preferneces:
                this.mConf.writePref();
                startSafelyActivity(new Intent(this, Settings.class), 1);
                return true;
            case R.id.donate:
                startSafelyActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + getString(R.string.pro_package))), -1);
                return true;
            case R.id.howto:
                startActivity(new Intent(this, HowtoActivity.class));
                return true;
            case R.id.about:
                Intent market = new Intent("android.intent.action.VIEW");
                market.setData(Uri.parse("market://search?q=pub:AntTek"));
                startActivity(market);
                return true;
            case R.id.rateit:
                startSafelyActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + getPackageName())), -1);
                return true;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void startSafelyActivity(Intent intent, int requestCode) {
        try {
            startActivityForResult(intent, requestCode);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void setupActionBar() {
        ActionBar actionBar = (ActionBar) findViewById(R.id.actionbar);
        actionBar.setTitle((int) R.string.app_name);
        this.mOnOffView = actionBar.addAction(new ActionBar.Action() {
            public void performAction(View view) {
                AppListActivity.this.mConf.mEnable = !AppListActivity.this.mConf.mEnable;
                AppListActivity.this.mConf.setEnable(AppListActivity.this.mConf.mEnable);
                view.setSelected(AppListActivity.this.mConf.mEnable);
                Toast.makeText(AppListActivity.this, AppListActivity.this.mConf.mEnable ? R.string.app_on : R.string.app_off, 0).show();
            }

            public int getDrawable() {
                return R.drawable.ic_onoff;
            }
        });
        this.mOnOffView.setSelected(this.mConf.mEnable);
        this.mFilterAppView = (ImageView) actionBar.addAction(new ActionBar.Action() {
            public int getDrawable() {
                switch (AppListActivity.this.mAppFilter) {
                    case R.id.action_third_party_apps:
                        return R.drawable.ic_filter_3rd;
                    case R.id.action_system_apps:
                        return R.drawable.ic_filter_sys;
                    case R.id.action_locked_apps:
                        return R.drawable.ic_filter_lock;
                    case R.id.action_unlocked_apps:
                        return R.drawable.ic_filter_unlock;
                    default:
                        return R.drawable.ic_filter_all;
                }
            }

            public void performAction(View view) {
                AppListActivity.this.mSettingPopupWindow = new FilterPopupWindow(view);
                AppListActivity.this.mSettingPopupWindow.showLikePopDownMenu();
            }
        });
        actionBar.addAction(new ActionBar.Action() {
            public int getDrawable() {
                return R.drawable.search;
            }

            public void performAction(View view) {
                boolean unused = AppListActivity.this.mIsShowFilter = true;
                AppListActivity.this.refreshFilterBar();
            }
        });
    }

    private class FilterPopupWindow extends BetterPopupWindow {
        public FilterPopupWindow(View anchor) {
            super(anchor);
        }

        /* access modifiers changed from: protected */
        public void onCreate() {
            ViewGroup root = (ViewGroup) ((LayoutInflater) this.anchor.getContext().getSystemService("layout_inflater")).inflate((int) R.layout.popup_filters, (ViewGroup) null);
            root.findViewById(R.id.action_all_apps).setOnClickListener(AppListActivity.this);
            root.findViewById(R.id.action_third_party_apps).setOnClickListener(AppListActivity.this);
            root.findViewById(R.id.action_system_apps).setOnClickListener(AppListActivity.this);
            root.findViewById(R.id.action_locked_apps).setOnClickListener(AppListActivity.this);
            root.findViewById(R.id.action_unlocked_apps).setOnClickListener(AppListActivity.this);
            setContentView(root);
        }
    }
}
