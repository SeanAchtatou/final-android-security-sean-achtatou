package org.baole.applocker.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import java.util.Random;
import org.baole.albs.R;

public class TictictoeView extends View {
    public static final long FPS_MS = 500;
    private static final int MARGIN = 4;
    private static final int MSG_BLINK = 1;
    /* access modifiers changed from: private */
    public boolean mBlinkDisplayOff;
    /* access modifiers changed from: private */
    public final Rect mBlinkRect = new Rect();
    private Paint mBmpPaint;
    private Bitmap mBmpPlayer1;
    private Bitmap mBmpPlayer2;
    private ICellListener mCellListener;
    private State mCurrentPlayer = State.UNKNOWN;
    private final State[] mData = new State[9];
    private Drawable mDrawableBg;
    private final Rect mDstRect = new Rect();
    /* access modifiers changed from: private */
    public final Handler mHandler = new Handler(new MyHandler());
    private Paint mLinePaint;
    private int mOffetX;
    private int mOffetY;
    /* access modifiers changed from: private */
    public int mSelectedCell = -1;
    /* access modifiers changed from: private */
    public State mSelectedValue = State.EMPTY;
    private final Rect mSrcRect = new Rect();
    private int mSxy;
    private int mWinCol = -1;
    private int mWinDiag = -1;
    private Paint mWinPaint;
    private int mWinRow = -1;
    private State mWinner = State.EMPTY;

    public interface ICellListener {
        void onCellSelected();
    }

    public enum State {
        UNKNOWN(-3),
        WIN(-2),
        EMPTY(0),
        PLAYER1(1),
        PLAYER2(2);
        
        private int mValue;

        private State(int value) {
            this.mValue = value;
        }

        public int getValue() {
            return this.mValue;
        }

        public static State fromInt(int i) {
            for (State s : values()) {
                if (s.getValue() == i) {
                    return s;
                }
            }
            return EMPTY;
        }
    }

    public TictictoeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        requestFocus();
        this.mDrawableBg = getResources().getDrawable(R.drawable.lib_bg);
        setBackgroundDrawable(this.mDrawableBg);
        this.mBmpPlayer1 = getResBitmap(R.drawable.lib_cross);
        this.mBmpPlayer2 = getResBitmap(R.drawable.lib_circle);
        if (this.mBmpPlayer1 != null) {
            this.mSrcRect.set(0, 0, this.mBmpPlayer1.getWidth() - 1, this.mBmpPlayer1.getHeight() - 1);
        }
        this.mBmpPaint = new Paint(1);
        this.mLinePaint = new Paint();
        this.mLinePaint.setColor(-1);
        this.mLinePaint.setStrokeWidth(5.0f);
        this.mLinePaint.setStyle(Paint.Style.STROKE);
        this.mWinPaint = new Paint(1);
        this.mWinPaint.setColor(-65536);
        this.mWinPaint.setStrokeWidth(10.0f);
        this.mWinPaint.setStyle(Paint.Style.STROKE);
        for (int i = 0; i < this.mData.length; i++) {
            this.mData[i] = State.EMPTY;
        }
        if (isInEditMode()) {
            Random rnd = new Random();
            for (int i2 = 0; i2 < this.mData.length; i2++) {
                this.mData[i2] = State.fromInt(rnd.nextInt(3));
            }
        }
    }

    public State[] getData() {
        return this.mData;
    }

    public void setCell(int cellIndex, State value) {
        this.mData[cellIndex] = value;
        invalidate();
    }

    public void setCellListener(ICellListener cellListener) {
        this.mCellListener = cellListener;
    }

    public int getSelection() {
        if (this.mSelectedValue == this.mCurrentPlayer) {
            return this.mSelectedCell;
        }
        return -1;
    }

    public State getCurrentPlayer() {
        return this.mCurrentPlayer;
    }

    public void setCurrentPlayer(State player) {
        this.mCurrentPlayer = player;
        this.mSelectedCell = -1;
    }

    public State getWinner() {
        return this.mWinner;
    }

    public void setWinner(State winner) {
        this.mWinner = winner;
    }

    public void setFinished(int col, int row, int diagonal) {
        this.mWinCol = col;
        this.mWinRow = row;
        this.mWinDiag = diagonal;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0091  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00be  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0078 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onDraw(android.graphics.Canvas r22) {
        /*
            r21 = this;
            super.onDraw(r22)
            r0 = r21
            int r0 = r0.mSxy
            r15 = r0
            int r14 = r15 * 3
            r0 = r21
            int r0 = r0.mOffetX
            r18 = r0
            r0 = r21
            int r0 = r0.mOffetY
            r20 = r0
            r11 = 0
            r13 = r15
        L_0x0018:
            r5 = 2
            if (r11 >= r5) goto L_0x0051
            r0 = r18
            float r0 = (float) r0
            r6 = r0
            int r5 = r20 + r13
            float r7 = (float) r5
            int r5 = r18 + r14
            r8 = 1
            int r5 = r5 - r8
            float r8 = (float) r5
            int r5 = r20 + r13
            float r9 = (float) r5
            r0 = r21
            android.graphics.Paint r0 = r0.mLinePaint
            r10 = r0
            r5 = r22
            r5.drawLine(r6, r7, r8, r9, r10)
            int r5 = r18 + r13
            float r6 = (float) r5
            r0 = r20
            float r0 = (float) r0
            r7 = r0
            int r5 = r18 + r13
            float r8 = (float) r5
            int r5 = r20 + r14
            r9 = 1
            int r5 = r5 - r9
            float r9 = (float) r5
            r0 = r21
            android.graphics.Paint r0 = r0.mLinePaint
            r10 = r0
            r5 = r22
            r5.drawLine(r6, r7, r8, r9, r10)
            int r11 = r11 + 1
            int r13 = r13 + r15
            goto L_0x0018
        L_0x0051:
            r12 = 0
            r13 = 0
            r19 = r20
        L_0x0055:
            r5 = 3
            if (r12 >= r5) goto L_0x00e9
            r11 = 0
            r17 = r18
        L_0x005b:
            r5 = 3
            if (r11 >= r5) goto L_0x00e3
            r0 = r21
            android.graphics.Rect r0 = r0.mDstRect
            r5 = r0
            int r6 = r17 + 4
            int r7 = r19 + 4
            r5.offsetTo(r6, r7)
            r0 = r21
            int r0 = r0.mSelectedCell
            r5 = r0
            if (r5 != r13) goto L_0x00b6
            r0 = r21
            boolean r0 = r0.mBlinkDisplayOff
            r5 = r0
            if (r5 == 0) goto L_0x007f
        L_0x0078:
            int r11 = r11 + 1
            int r13 = r13 + 1
            int r17 = r17 + r15
            goto L_0x005b
        L_0x007f:
            r0 = r21
            org.baole.applocker.view.TictictoeView$State r0 = r0.mSelectedValue
            r16 = r0
        L_0x0085:
            int[] r5 = org.baole.applocker.view.TictictoeView.AnonymousClass1.$SwitchMap$org$baole$applocker$view$TictictoeView$State
            int r6 = r16.ordinal()
            r5 = r5[r6]
            switch(r5) {
                case 1: goto L_0x0091;
                case 2: goto L_0x00be;
                default: goto L_0x0090;
            }
        L_0x0090:
            goto L_0x0078
        L_0x0091:
            r0 = r21
            android.graphics.Bitmap r0 = r0.mBmpPlayer1
            r5 = r0
            if (r5 == 0) goto L_0x0078
            r0 = r21
            android.graphics.Bitmap r0 = r0.mBmpPlayer1
            r5 = r0
            r0 = r21
            android.graphics.Rect r0 = r0.mSrcRect
            r6 = r0
            r0 = r21
            android.graphics.Rect r0 = r0.mDstRect
            r7 = r0
            r0 = r21
            android.graphics.Paint r0 = r0.mBmpPaint
            r8 = r0
            r0 = r22
            r1 = r5
            r2 = r6
            r3 = r7
            r4 = r8
            r0.drawBitmap(r1, r2, r3, r4)
            goto L_0x0078
        L_0x00b6:
            r0 = r21
            org.baole.applocker.view.TictictoeView$State[] r0 = r0.mData
            r5 = r0
            r16 = r5[r13]
            goto L_0x0085
        L_0x00be:
            r0 = r21
            android.graphics.Bitmap r0 = r0.mBmpPlayer2
            r5 = r0
            if (r5 == 0) goto L_0x0078
            r0 = r21
            android.graphics.Bitmap r0 = r0.mBmpPlayer2
            r5 = r0
            r0 = r21
            android.graphics.Rect r0 = r0.mSrcRect
            r6 = r0
            r0 = r21
            android.graphics.Rect r0 = r0.mDstRect
            r7 = r0
            r0 = r21
            android.graphics.Paint r0 = r0.mBmpPaint
            r8 = r0
            r0 = r22
            r1 = r5
            r2 = r6
            r3 = r7
            r4 = r8
            r0.drawBitmap(r1, r2, r3, r4)
            goto L_0x0078
        L_0x00e3:
            int r12 = r12 + 1
            int r19 = r19 + r15
            goto L_0x0055
        L_0x00e9:
            r0 = r21
            int r0 = r0.mWinRow
            r5 = r0
            if (r5 < 0) goto L_0x0119
            r0 = r21
            int r0 = r0.mWinRow
            r5 = r0
            int r5 = r5 * r15
            int r5 = r5 + r20
            int r6 = r15 / 2
            int r19 = r5 + r6
            int r5 = r18 + 4
            float r6 = (float) r5
            r0 = r19
            float r0 = (float) r0
            r7 = r0
            int r5 = r18 + r14
            r8 = 1
            int r5 = r5 - r8
            r8 = 4
            int r5 = r5 - r8
            float r8 = (float) r5
            r0 = r19
            float r0 = (float) r0
            r9 = r0
            r0 = r21
            android.graphics.Paint r0 = r0.mWinPaint
            r10 = r0
            r5 = r22
            r5.drawLine(r6, r7, r8, r9, r10)
        L_0x0118:
            return
        L_0x0119:
            r0 = r21
            int r0 = r0.mWinCol
            r5 = r0
            if (r5 < 0) goto L_0x0149
            r0 = r21
            int r0 = r0.mWinCol
            r5 = r0
            int r5 = r5 * r15
            int r5 = r5 + r18
            int r6 = r15 / 2
            int r17 = r5 + r6
            r0 = r17
            float r0 = (float) r0
            r6 = r0
            int r5 = r20 + 4
            float r7 = (float) r5
            r0 = r17
            float r0 = (float) r0
            r8 = r0
            int r5 = r20 + r14
            r9 = 1
            int r5 = r5 - r9
            r9 = 4
            int r5 = r5 - r9
            float r9 = (float) r5
            r0 = r21
            android.graphics.Paint r0 = r0.mWinPaint
            r10 = r0
            r5 = r22
            r5.drawLine(r6, r7, r8, r9, r10)
            goto L_0x0118
        L_0x0149:
            r0 = r21
            int r0 = r0.mWinDiag
            r5 = r0
            if (r5 != 0) goto L_0x016f
            int r5 = r18 + 4
            float r6 = (float) r5
            int r5 = r20 + 4
            float r7 = (float) r5
            int r5 = r18 + r14
            r8 = 1
            int r5 = r5 - r8
            r8 = 4
            int r5 = r5 - r8
            float r8 = (float) r5
            int r5 = r20 + r14
            r9 = 1
            int r5 = r5 - r9
            r9 = 4
            int r5 = r5 - r9
            float r9 = (float) r5
            r0 = r21
            android.graphics.Paint r0 = r0.mWinPaint
            r10 = r0
            r5 = r22
            r5.drawLine(r6, r7, r8, r9, r10)
            goto L_0x0118
        L_0x016f:
            r0 = r21
            int r0 = r0.mWinDiag
            r5 = r0
            r6 = 1
            if (r5 != r6) goto L_0x0118
            int r5 = r18 + 4
            float r6 = (float) r5
            int r5 = r20 + r14
            r7 = 1
            int r5 = r5 - r7
            r7 = 4
            int r5 = r5 - r7
            float r7 = (float) r5
            int r5 = r18 + r14
            r8 = 1
            int r5 = r5 - r8
            r8 = 4
            int r5 = r5 - r8
            float r8 = (float) r5
            int r5 = r20 + 4
            float r9 = (float) r5
            r0 = r21
            android.graphics.Paint r0 = r0.mWinPaint
            r10 = r0
            r5 = r22
            r5.drawLine(r6, r7, r8, r9, r10)
            goto L_0x0118
        */
        throw new UnsupportedOperationException("Method not decompiled: org.baole.applocker.view.TictictoeView.onDraw(android.graphics.Canvas):void");
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int w = View.MeasureSpec.getSize(widthMeasureSpec);
        int h = View.MeasureSpec.getSize(heightMeasureSpec);
        int d = w == 0 ? h : h == 0 ? w : w < h ? w : h;
        setMeasuredDimension(d, d);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        int size;
        super.onSizeChanged(w, h, oldw, oldh);
        int sx = (w - 8) / 3;
        int sy = (h - 8) / 3;
        if (sx < sy) {
            size = sx;
        } else {
            size = sy;
        }
        this.mSxy = size;
        this.mOffetX = (w - (size * 3)) / 2;
        this.mOffetY = (h - (size * 3)) / 2;
        this.mDstRect.set(4, 4, size - 4, size - 4);
    }

    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction();
        if (action == 0) {
            return true;
        }
        if (action != 1) {
            return false;
        }
        int x = (int) event.getX();
        int y = (int) event.getY();
        int sxy = this.mSxy;
        int x2 = (x - 4) / sxy;
        int y2 = (y - 4) / sxy;
        if (isEnabled() && x2 >= 0 && x2 < 3) {
            if ((y2 >= 0) && (y2 < 3)) {
                int cell = x2 + (y2 * 3);
                State state = (cell == this.mSelectedCell ? this.mSelectedValue : this.mData[cell]) == State.EMPTY ? this.mCurrentPlayer : State.EMPTY;
                stopBlink();
                this.mSelectedCell = cell;
                this.mSelectedValue = state;
                this.mBlinkDisplayOff = false;
                this.mBlinkRect.set((x2 * sxy) + 4, (y2 * sxy) + 4, ((x2 + 1) * sxy) + 4, ((y2 + 1) * sxy) + 4);
                if (state != State.EMPTY) {
                    this.mHandler.sendEmptyMessageDelayed(1, 500);
                }
                if (this.mCellListener != null) {
                    this.mCellListener.onCellSelected();
                }
            }
        }
        return true;
    }

    public void stopBlink() {
        boolean hadSelection;
        if (this.mSelectedCell == -1 || this.mSelectedValue == State.EMPTY) {
            hadSelection = false;
        } else {
            hadSelection = true;
        }
        this.mSelectedCell = -1;
        this.mSelectedValue = State.EMPTY;
        if (!this.mBlinkRect.isEmpty()) {
            invalidate(this.mBlinkRect);
        }
        this.mBlinkDisplayOff = false;
        this.mBlinkRect.setEmpty();
        this.mHandler.removeMessages(1);
        if (hadSelection && this.mCellListener != null) {
            this.mCellListener.onCellSelected();
        }
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        Bundle b = new Bundle();
        b.putParcelable("gv_super_state", super.onSaveInstanceState());
        b.putBoolean("gv_en", isEnabled());
        int[] data = new int[this.mData.length];
        for (int i = 0; i < data.length; i++) {
            data[i] = this.mData[i].getValue();
        }
        b.putIntArray("gv_data", data);
        b.putInt("gv_sel_cell", this.mSelectedCell);
        b.putInt("gv_sel_val", this.mSelectedValue.getValue());
        b.putInt("gv_curr_play", this.mCurrentPlayer.getValue());
        b.putInt("gv_winner", this.mWinner.getValue());
        b.putInt("gv_win_col", this.mWinCol);
        b.putInt("gv_win_row", this.mWinRow);
        b.putInt("gv_win_diag", this.mWinDiag);
        b.putBoolean("gv_blink_off", this.mBlinkDisplayOff);
        b.putParcelable("gv_blink_rect", this.mBlinkRect);
        return b;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable state) {
        if (!(state instanceof Bundle)) {
            super.onRestoreInstanceState(state);
            return;
        }
        Bundle b = (Bundle) state;
        Parcelable superState = b.getParcelable("gv_super_state");
        setEnabled(b.getBoolean("gv_en", true));
        int[] data = b.getIntArray("gv_data");
        if (data != null && data.length == this.mData.length) {
            for (int i = 0; i < data.length; i++) {
                this.mData[i] = State.fromInt(data[i]);
            }
        }
        this.mSelectedCell = b.getInt("gv_sel_cell", -1);
        this.mSelectedValue = State.fromInt(b.getInt("gv_sel_val", State.EMPTY.getValue()));
        this.mCurrentPlayer = State.fromInt(b.getInt("gv_curr_play", State.EMPTY.getValue()));
        this.mWinner = State.fromInt(b.getInt("gv_winner", State.EMPTY.getValue()));
        this.mWinCol = b.getInt("gv_win_col", -1);
        this.mWinRow = b.getInt("gv_win_row", -1);
        this.mWinDiag = b.getInt("gv_win_diag", -1);
        this.mBlinkDisplayOff = b.getBoolean("gv_blink_off", false);
        Rect r = (Rect) b.getParcelable("gv_blink_rect");
        if (r != null) {
            this.mBlinkRect.set(r);
        }
        this.mHandler.sendEmptyMessage(1);
        super.onRestoreInstanceState(superState);
    }

    private class MyHandler implements Handler.Callback {
        private MyHandler() {
        }

        public boolean handleMessage(Message msg) {
            if (msg.what != 1) {
                return false;
            }
            if (!(TictictoeView.this.mSelectedCell < 0 || TictictoeView.this.mSelectedValue == State.EMPTY || TictictoeView.this.mBlinkRect.top == 0)) {
                boolean unused = TictictoeView.this.mBlinkDisplayOff = !TictictoeView.this.mBlinkDisplayOff;
                TictictoeView.this.invalidate(TictictoeView.this.mBlinkRect);
                if (!TictictoeView.this.mHandler.hasMessages(1)) {
                    TictictoeView.this.mHandler.sendEmptyMessageDelayed(1, 500);
                }
            }
            return true;
        }
    }

    private Bitmap getResBitmap(int bmpResId) {
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inDither = false;
        Resources res = getResources();
        Bitmap bmp = BitmapFactory.decodeResource(res, bmpResId, opts);
        if (bmp != null || !isInEditMode()) {
            return bmp;
        }
        Drawable d = res.getDrawable(bmpResId);
        int w = d.getIntrinsicWidth();
        int h = d.getIntrinsicHeight();
        Bitmap bmp2 = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bmp2);
        d.setBounds(0, 0, w - 1, h - 1);
        d.draw(c);
        return bmp2;
    }
}
