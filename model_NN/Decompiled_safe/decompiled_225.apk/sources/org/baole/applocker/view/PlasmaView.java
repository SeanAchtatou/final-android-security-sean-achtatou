package org.baole.applocker.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class PlasmaView extends View {
    boolean done = false;
    private Paint mPaint = new Paint();

    static double displace(double num, double width, double height) {
        return (Math.random() - 0.5d) * (num / (width + height)) * 3.0d;
    }

    public PlasmaView(Context context) {
        super(context);
    }

    public PlasmaView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        Paint paint = this.mPaint;
        paint.setAntiAlias(true);
        paint.setDither(true);
        drawPlasma(getContext(), canvas, paint, canvas.getWidth(), canvas.getHeight());
    }

    static int computeColor(double c) {
        double Red;
        double Green;
        double c2;
        if (c < 0.5d) {
            Red = 2.0d * c;
        } else {
            Red = (1.0d - c) * 2.0d;
        }
        if (c >= 0.3d && c < 0.8d) {
            Green = (c - 0.3d) * 2.0d;
        } else if (c < 0.3d) {
            Green = (0.3d - c) * 2.0d;
        } else {
            Green = (1.3d - c) * 2.0d;
        }
        if (c >= 0.5d) {
            c2 = (c - 0.5d) * 2.0d;
        } else {
            c2 = (0.5d - c) * 2.0d;
        }
        return Color.rgb((int) (Red * 255.0d), (int) (Green * 255.0d), (int) (c2 * 255.0d));
    }

    /* JADX INFO: Multiple debug info for r20v1 float: [D('c1' float), D('ctx' android.content.Context)] */
    static void drawPlasma(Context ctx, Canvas c, Paint p, int width, int height) {
        float c1 = (float) Math.random();
        float c4 = (float) Math.random();
        p.setStyle(Paint.Style.STROKE);
        DivideGrid(c, p, 0.0d, 0.0d, (double) width, (double) height, (double) c1, (double) ((float) Math.random()), (double) ((float) Math.random()), (double) c4);
    }

    /* JADX INFO: Multiple debug info for r66v13 double: [D('width' double), D('color' double)] */
    /* JADX INFO: Multiple debug info for r66v14 int: [D('color' double), D('colorValue' int)] */
    static void DivideGrid(Canvas c, Paint p, double x, double y, double width, double height, double c1, double c2, double c3, double c4) {
        double newWidth = width / 2.0d;
        double newHeight = height / 2.0d;
        if (width > 2.0d || height > 2.0d) {
            double Middle = ((((c1 + c2) + c3) + c4) / 4.0d) + displace(newWidth + newHeight, width, height);
            double Edge1 = (c1 + c2) / 2.0d;
            double Edge2 = (c2 + c3) / 2.0d;
            double Edge3 = (c3 + c4) / 2.0d;
            double Edge4 = (c4 + c1) / 2.0d;
            if (Middle < 0.0d) {
                Middle = 0.0d;
            } else if (Middle > 1.0d) {
                Middle = 1.0d;
            }
            DivideGrid(c, p, x, y, newWidth, newHeight, c1, Edge1, Middle, Edge4);
            DivideGrid(c, p, x + newWidth, y, newWidth, newHeight, Edge1, c2, Edge2, Middle);
            DivideGrid(c, p, x + newWidth, y + newHeight, newWidth, newHeight, Middle, Edge2, c3, Edge3);
            DivideGrid(c, p, x, y + newHeight, newWidth, newHeight, Edge4, Middle, Edge3, c4);
            return;
        }
        p.setStrokeWidth(2.0f);
        p.setColor(computeColor((((c1 + c2) + c3) + c4) / 4.0d));
        c.drawPoint((float) x, (float) y, p);
    }
}
