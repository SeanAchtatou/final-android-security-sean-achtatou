package org.baole.applocker.service;

import android.app.ActivityManager;
import android.app.IActivityWatcher;
import android.app.IApplicationThread;
import android.content.Context;
import android.content.Intent;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.baole.applocker.activity.DialogUnlockActivity;
import org.baole.applocker.activity.FCUnlockActivity;
import org.baole.applocker.activity.ImageUnlockActivity;
import org.baole.applocker.activity.PINUnlockActivity;
import org.baole.applocker.activity.PasswordUnlockActivity;
import org.baole.applocker.activity.PatternUnlockActivity;
import org.baole.applocker.activity.TextUnlockActivity;
import org.baole.applocker.db.DbHelper;
import org.baole.applocker.model.App;
import org.baole.applocker.model.Configuration;
import org.baole.applocker.model.LockInfo;
import org.baole.applocker.model.LockMethod;
import org.baole.applocker.utils.L;

public class ActivityWatcher extends IActivityWatcher.Stub {
    public static final String APP = "_app";
    public static final String LOCK_METHOD = "_lock_method";
    private static ActivityWatcher mInstance = null;
    private ActivityManager mAM;
    private Configuration mConf;
    Context mContext;
    private DbHelper mHelper;
    private String mThisPackage = "";

    public static ActivityWatcher getInstance(Context context) {
        if (mInstance == null) {
            synchronized (ActivityWatcher.class) {
                mInstance = new ActivityWatcher(context);
            }
        }
        return mInstance;
    }

    private ActivityWatcher(Context context) {
        this.mContext = context;
        this.mAM = (ActivityManager) context.getSystemService("activity");
        this.mConf = Configuration.getInstance(context.getApplicationContext());
        this.mHelper = DbHelper.getInstance(this.mContext);
        this.mThisPackage = context.getPackageName();
    }

    public void activityResuming(int activityId) throws RemoteException {
        ActivityManager.RecentTaskInfo task = getLatestTask();
        inspectLastActivity(task, "resuming");
        if (this.mConf.mEnable && task != null) {
            String pkg = task.baseIntent.getComponent().getPackageName();
            String act = task.baseIntent.getComponent().getClassName();
            if (task.id < 2) {
                unlocking(pkg);
            } else {
                locking(pkg, act);
            }
        }
    }

    private void unlocking(String pkg) {
        L.e("unlocking %s", pkg);
        if (!pkg.equals(this.mThisPackage)) {
            if (pkg.equals(this.mConf.mCurrentUnLockPkg)) {
                this.mConf.mUnlockedCaches.get(pkg).mTime = System.currentTimeMillis();
            } else {
                if (!pkg.startsWith(this.mConf.mCurrentUnLockPkg) || !pkg.startsWith(this.mThisPackage)) {
                    this.mConf.mLastResumingPkg = "";
                }
                this.mConf.mCurrentUnLockPkg = "";
            }
            this.mConf.mLastResumingPkg = "";
        }
    }

    public void locking(String pkg, String act) {
        ArrayList<App> apps;
        L.e("locking %s/%s", pkg, act);
        if (this.mConf.mEnable) {
            if (pkg == null || this.mThisPackage.equals(pkg)) {
                L.e("locking 1", new Object[0]);
                assignLastResumingPackage(pkg);
                return;
            }
            if (this.mConf.mHasUnLock) {
                if (this.mConf.mValidity > 0) {
                    if (!this.mConf.mUnLockOneAll) {
                        LockInfo li = this.mConf.mUnlockedCaches.get(pkg);
                        if (li != null && System.currentTimeMillis() < li.mTime + (60000 * ((long) this.mConf.mValidity))) {
                            L.e("locking 3", new Object[0]);
                            assignLastResumingPackage(pkg);
                            return;
                        }
                    } else if (System.currentTimeMillis() < this.mConf.mLastUnlock.longValue() + (60000 * ((long) this.mConf.mValidity))) {
                        L.e("locking 2", new Object[0]);
                        assignLastResumingPackage(pkg);
                        return;
                    }
                } else if (this.mConf.mValidity < 0) {
                    L.e("locking 4", new Object[0]);
                    assignLastResumingPackage(pkg);
                    return;
                }
            }
            if (!this.mConf.mHasUnLock || !pkg.equals(this.mConf.mCurrentUnLockPkg) || !pkg.equals(this.mConf.mLastResumingPkg)) {
                assignLastResumingPackage(pkg);
                if (this.mConf.mLockAll) {
                    apps = this.mHelper.queryApps("_package=? ", new String[]{pkg});
                } else {
                    apps = this.mHelper.queryApps("_package=? AND _use_lock=?", new String[]{pkg, "1"});
                }
                Iterator i$ = apps.iterator();
                while (i$.hasNext()) {
                    App app = i$.next();
                    if (app.getPackage().equals(pkg)) {
                        Intent intent = lockIntent(this.mContext, app, false);
                        if (intent != null) {
                            this.mContext.startActivity(intent);
                            L.e("locked %s", pkg, act);
                            return;
                        }
                        L.e("locking 6", new Object[0]);
                        return;
                    }
                }
                return;
            }
            L.e("locking 5", new Object[0]);
        }
    }

    private void assignLastResumingPackage(String pkg) {
        if (!pkg.startsWith(this.mThisPackage)) {
            this.mConf.mLastResumingPkg = pkg;
        }
    }

    public void closingSystemDialogs(String reason) {
        ActivityManager.RecentTaskInfo task = getLatestTask();
        inspectLastActivity(task, "closing");
        if (task != null) {
            String pkg = task.baseIntent.getComponent().getPackageName();
            if (pkg.equals(this.mConf.mCurrentUnLockPkg)) {
                this.mConf.mUnlockedCaches.get(pkg).mTime = System.currentTimeMillis();
            }
        }
    }

    public static Intent lockIntent(Context context, App app, boolean master) {
        Configuration mConf2 = Configuration.getInstance(context.getApplicationContext());
        LockMethod lockMethod = app.getLockMethod();
        if (lockMethod == LockMethod.USE_DEFAULT || (mConf2.mLockAll && lockMethod == LockMethod.NONE)) {
            lockMethod = mConf2.mDefaultLockMethod;
        }
        Intent unlock = null;
        switch (AnonymousClass1.$SwitchMap$org$baole$applocker$model$LockMethod[lockMethod.ordinal()]) {
            case 1:
                unlock = new Intent(context, PINUnlockActivity.class);
                break;
            case 2:
                unlock = new Intent(context, PasswordUnlockActivity.class);
                break;
            case 3:
                unlock = new Intent(context, PatternUnlockActivity.class);
                break;
            case 4:
                unlock = new Intent(context, DialogUnlockActivity.class);
                break;
            case 5:
                unlock = new Intent(context, DialogUnlockActivity.class);
                break;
            case 6:
                unlock = new Intent(context, TextUnlockActivity.class);
                break;
            case IApplicationThread.SCHEDULE_LAUNCH_ACTIVITY_TRANSACTION:
                unlock = new Intent(context, ImageUnlockActivity.class);
                break;
            case 8:
                unlock = new Intent(context, FCUnlockActivity.class);
                break;
        }
        if (unlock != null) {
            unlock.putExtra("_lock_method", lockMethod.getType());
            unlock.putExtra(APP, app);
            if (!master) {
                unlock.addFlags(268435456);
                unlock.addFlags(2097152);
                unlock.addFlags(536870912);
            }
        }
        return unlock;
    }

    /* renamed from: org.baole.applocker.service.ActivityWatcher$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$baole$applocker$model$LockMethod = new int[LockMethod.values().length];

        static {
            try {
                $SwitchMap$org$baole$applocker$model$LockMethod[LockMethod.PIN.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$org$baole$applocker$model$LockMethod[LockMethod.PASSWORD.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$org$baole$applocker$model$LockMethod[LockMethod.PATTERN.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$org$baole$applocker$model$LockMethod[LockMethod.DIALOG.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$org$baole$applocker$model$LockMethod[LockMethod.DIAL.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$org$baole$applocker$model$LockMethod[LockMethod.TEXT_SCREEN.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$org$baole$applocker$model$LockMethod[LockMethod.IMAGE_SCREEN.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$org$baole$applocker$model$LockMethod[LockMethod.FORCE_CLOSE.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
        }
    }

    private ActivityManager.RecentTaskInfo getLatestTask() {
        List<ActivityManager.RecentTaskInfo> tasks = this.mAM.getRecentTasks(2, 1);
        for (ActivityManager.RecentTaskInfo t : tasks) {
            L.d("inspect recent task: %s | %s | id=%d", t.baseIntent.getComponent().getPackageName(), t.baseIntent.getComponent().getClassName(), Integer.valueOf(t.id));
        }
        ActivityManager.RecentTaskInfo t2 = tasks.get(1);
        if (t2.id < 2) {
            String pkg = t2.baseIntent.getComponent().getPackageName();
            if (!pkg.equals(this.mThisPackage)) {
                if (pkg.equals(this.mConf.mCurrentUnLockPkg)) {
                    this.mConf.mUnlockedCaches.get(pkg).mTime = System.currentTimeMillis();
                } else {
                    if (!pkg.startsWith(this.mConf.mCurrentUnLockPkg) || !pkg.startsWith(this.mThisPackage)) {
                        this.mConf.mLastResumingPkg = "";
                    }
                    this.mConf.mCurrentUnLockPkg = "";
                }
                this.mConf.mLastResumingPkg = "";
            }
        }
        Iterator i$ = tasks.iterator();
        if (i$.hasNext()) {
            return i$.next();
        }
        return null;
    }

    private void inspectLastActivity(ActivityManager.RecentTaskInfo t, String tag) {
        L.e("inspect-%s: %s | %s | id=%d", tag, t.baseIntent.getComponent().getPackageName(), t.baseIntent.getComponent().getClassName(), Integer.valueOf(t.id));
    }
}
