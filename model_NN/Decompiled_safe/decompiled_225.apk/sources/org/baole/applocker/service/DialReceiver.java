package org.baole.applocker.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import org.baole.applocker.activity.DialUnlockActivity;
import org.baole.applocker.model.Configuration;

public class DialReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        Configuration conf = Configuration.getInstance(context.getApplicationContext());
        String number = intent.getStringExtra("android.intent.extra.PHONE_NUMBER");
        if (number != null && number.equals(conf.mDefaultDial.mExtra2)) {
            setResultData(null);
            Intent confirmIntent = new Intent(context, DialUnlockActivity.class);
            confirmIntent.addFlags(268435456);
            confirmIntent.putExtra("android.intent.extra.PHONE_NUMBER", number);
            context.startActivity(confirmIntent);
        }
    }
}
