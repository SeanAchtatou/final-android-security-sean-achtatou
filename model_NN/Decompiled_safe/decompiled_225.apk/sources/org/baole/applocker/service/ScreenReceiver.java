package org.baole.applocker.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import org.baole.applocker.model.Configuration;
import org.baole.applocker.utils.L;

public class ScreenReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.USER_PRESENT") || intent.getAction().equals("android.intent.action.SCREEN_ON")) {
            Configuration conf = Configuration.getInstance(context.getApplicationContext());
            if (conf.mValidity < 0) {
                conf.mHasUnLock = false;
            }
            L.e("ScreenReceiver.onReceive", new Object[0]);
        }
        L.e("ScreenReceiver.onReceive - outter", new Object[0]);
    }
}
