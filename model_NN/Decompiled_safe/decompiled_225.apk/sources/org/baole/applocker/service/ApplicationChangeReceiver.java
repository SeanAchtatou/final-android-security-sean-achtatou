package org.baole.applocker.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import org.baole.applocker.db.DbHelper;
import org.baole.applocker.model.App;

public class ApplicationChangeReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if ("android.intent.action.PACKAGE_ADDED".equals(intent.getAction())) {
            App item = getApp(context, intent.getDataString().substring(8));
            if (item != null) {
                DbHelper.getInstance(context).insertOrUpdateApp(item, false, true);
            }
        } else if ("android.intent.action.PACKAGE_REMOVED".equals(intent.getAction())) {
            String packageName = intent.getDataString().substring(8);
            if (!hasPackage(context, packageName)) {
                DbHelper.getInstance(context).removeApp(packageName);
            }
        }
    }

    private App getApp(Context context, String packageName) {
        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo pkg = pm.getPackageInfo(packageName, 8192);
            String versionName = pkg.versionName;
            String name = packageName;
            App app = new App();
            app.setPackage(packageName);
            app.setVersion(versionName);
            if (pkg.applicationInfo != null) {
                Drawable drawable = pkg.applicationInfo.loadIcon(pm);
                if (drawable instanceof BitmapDrawable) {
                    app.setIcon(((BitmapDrawable) drawable).getBitmap());
                }
                name = pkg.applicationInfo.loadLabel(pm).toString();
                app.setExtra1((long) pkg.applicationInfo.flags);
            }
            app.setName(name);
            return app;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean hasPackage(Context c, String packageName) {
        try {
            if (c.getPackageManager().getApplicationInfo(packageName, 0) != null) {
                return true;
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }
}
