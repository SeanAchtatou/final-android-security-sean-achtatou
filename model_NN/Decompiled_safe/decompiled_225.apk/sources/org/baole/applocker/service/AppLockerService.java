package org.baole.applocker.service;

import android.app.ActivityManagerNative;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.os.RemoteException;
import android.widget.Toast;
import java.util.StringTokenizer;
import org.baole.applocker.utils.L;

public class AppLockerService extends Service implements LogcatInterceptor {
    private static LogcatProcessor mLogcatThread;
    ActivityWatcher mWatcher;
    String thispackage = "";

    public IBinder onBind(Intent arg0) {
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003a, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003b, code lost:
        r2.printStackTrace();
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int onStartCommand(android.content.Intent r4, int r5, int r6) {
        /*
            r3 = this;
            android.content.Context r1 = r3.getApplicationContext()
            r3.registerActivityWatcher(r1)
            android.content.Context r1 = r3.getApplicationContext()
            org.baole.applocker.service.ActivityWatcher r1 = org.baole.applocker.service.ActivityWatcher.getInstance(r1)
            r3.mWatcher = r1
            java.lang.String r1 = r3.getPackageName()
            r3.thispackage = r1
            java.lang.Class<org.baole.applocker.service.LogcatProcessor> r1 = org.baole.applocker.service.LogcatProcessor.class
            monitor-enter(r1)
            org.baole.applocker.service.LogcatProcessor r2 = org.baole.applocker.service.AppLockerService.mLogcatThread     // Catch:{ all -> 0x0040 }
            if (r2 == 0) goto L_0x0026
            org.baole.applocker.service.LogcatProcessor r2 = org.baole.applocker.service.AppLockerService.mLogcatThread     // Catch:{ all -> 0x0040 }
            boolean r2 = r2.isAlive()     // Catch:{ all -> 0x0040 }
            if (r2 != 0) goto L_0x0037
        L_0x0026:
            org.baole.applocker.service.LogcatProcessor r2 = new org.baole.applocker.service.LogcatProcessor     // Catch:{ Throwable -> 0x003a }
            r2.<init>()     // Catch:{ Throwable -> 0x003a }
            org.baole.applocker.service.AppLockerService.mLogcatThread = r2     // Catch:{ Throwable -> 0x003a }
            org.baole.applocker.service.LogcatProcessor r2 = org.baole.applocker.service.AppLockerService.mLogcatThread     // Catch:{ Throwable -> 0x003a }
            r2.setInterceptor(r3)     // Catch:{ Throwable -> 0x003a }
            org.baole.applocker.service.LogcatProcessor r2 = org.baole.applocker.service.AppLockerService.mLogcatThread     // Catch:{ Throwable -> 0x003a }
            r2.start()     // Catch:{ Throwable -> 0x003a }
        L_0x0037:
            monitor-exit(r1)     // Catch:{ all -> 0x0040 }
            r1 = 1
            return r1
        L_0x003a:
            r2 = move-exception
            r0 = r2
            r0.printStackTrace()     // Catch:{ all -> 0x0040 }
            goto L_0x0037
        L_0x0040:
            r2 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0040 }
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: org.baole.applocker.service.AppLockerService.onStartCommand(android.content.Intent, int, int):int");
    }

    public static void startActivityWatcherService(Context context) {
        try {
            context.startService(new Intent(context, AppLockerService.class));
        } catch (SecurityException e) {
            SecurityException e2 = e;
            Toast.makeText(context, "Can not start AppLockerService" + e2.getMessage(), 0).show();
            e2.printStackTrace();
        } catch (Throwable th) {
            Throwable e3 = th;
            Toast.makeText(context, "Can not start AppLockerService" + e3.getMessage(), 0).show();
            e3.printStackTrace();
        }
        try {
            IntentFilter filter = new IntentFilter("android.intent.action.SCREEN_ON");
            filter.addAction("android.intent.action.SCREEN_OFF");
            context.registerReceiver(new ScreenReceiver(), filter);
        } catch (Throwable th2) {
            th2.printStackTrace();
        }
    }

    public static void stopActivityWatcherService(Context context) {
        try {
            context.stopService(new Intent(context, AppLockerService.class));
        } catch (SecurityException e) {
            SecurityException e2 = e;
            Toast.makeText(context, "Can not start AppLockerService" + e2.getMessage(), 0).show();
            e2.printStackTrace();
        } catch (Throwable th) {
            Throwable e3 = th;
            Toast.makeText(context, "Can not start AppLockerService" + e3.getMessage(), 0).show();
            e3.printStackTrace();
        }
    }

    public void registerActivityWatcher(Context context) {
        try {
            ActivityManagerNative.getDefault().registerActivityWatcher(ActivityWatcher.getInstance(context));
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void onError(String msg, Throwable e) {
        e.printStackTrace();
    }

    public void onNewline(String line) {
        StringTokenizer tokenizer;
        if (line != null && line.contains("ActivityManager") && (tokenizer = new StringTokenizer(line)) != null) {
            while (tokenizer.hasMoreTokens()) {
                try {
                    String token = tokenizer.nextToken();
                    if (token != null && token.startsWith("cmp=")) {
                        String token2 = token.substring(4);
                        int ind = token2.indexOf("/");
                        String pkg = token2.substring(0, ind);
                        String act = pkg + token2.substring(ind + 1);
                        if (!pkg.startsWith(this.thispackage)) {
                            L.e("inspect package %s | %s", pkg, act);
                            this.mWatcher.locking(pkg, act);
                        }
                    }
                } catch (Throwable th) {
                    th.printStackTrace();
                    return;
                }
            }
        }
    }
}
