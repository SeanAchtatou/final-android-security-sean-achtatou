package org.baole.applocker.service;

public class LogcatProcessor extends Thread {
    private static final int BUFFER_SIZE = 1024;
    public static final String[] LOGCAT_CMD = {"logcat", "ActivityManager:I"};
    LogcatInterceptor mInterceptor = null;
    private int mLines = 0;
    protected Process mLogcatProc = null;

    public void setInterceptor(LogcatInterceptor interceptor) {
        this.mInterceptor = interceptor;
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x005c A[SYNTHETIC, Splitter:B:17:0x005c] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0095 A[SYNTHETIC, Splitter:B:30:0x0095] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r10 = this;
            r9 = 0
            java.lang.Runtime r6 = java.lang.Runtime.getRuntime()     // Catch:{ IOException -> 0x0063 }
            java.lang.String[] r7 = org.baole.applocker.service.LogcatProcessor.LOGCAT_CMD     // Catch:{ IOException -> 0x0063 }
            java.lang.Process r6 = r6.exec(r7)     // Catch:{ IOException -> 0x0063 }
            r10.mLogcatProc = r6     // Catch:{ IOException -> 0x0063 }
            r4 = 0
            java.lang.Process r6 = r10.mLogcatProc
            java.io.InputStream r2 = r6.getInputStream()
            int r6 = r2.available()     // Catch:{ IOException -> 0x0082 }
            long r6 = (long) r6     // Catch:{ IOException -> 0x0082 }
            r2.skip(r6)     // Catch:{ IOException -> 0x0082 }
        L_0x001c:
            java.io.BufferedReader r5 = new java.io.BufferedReader     // Catch:{ IOException -> 0x00a5 }
            java.io.InputStreamReader r6 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x00a5 }
            r6.<init>(r2)     // Catch:{ IOException -> 0x00a5 }
            r7 = 1024(0x400, float:1.435E-42)
            r5.<init>(r6, r7)     // Catch:{ IOException -> 0x00a5 }
        L_0x0028:
            java.lang.String r3 = r5.readLine()     // Catch:{ IOException -> 0x003a, all -> 0x00a2 }
            if (r3 == 0) goto L_0x0088
            org.baole.applocker.service.LogcatInterceptor r6 = r10.mInterceptor     // Catch:{ IOException -> 0x003a, all -> 0x00a2 }
            r6.onNewline(r3)     // Catch:{ IOException -> 0x003a, all -> 0x00a2 }
            int r6 = r10.mLines     // Catch:{ IOException -> 0x003a, all -> 0x00a2 }
            int r6 = r6 + 1
            r10.mLines = r6     // Catch:{ IOException -> 0x003a, all -> 0x00a2 }
            goto L_0x0028
        L_0x003a:
            r6 = move-exception
            r0 = r6
            r4 = r5
        L_0x003d:
            org.baole.applocker.service.LogcatInterceptor r6 = r10.mInterceptor     // Catch:{ all -> 0x0092 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x0092 }
            r7.<init>()     // Catch:{ all -> 0x0092 }
            java.lang.String r8 = "Error reading from process "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ all -> 0x0092 }
            java.lang.String[] r8 = org.baole.applocker.service.LogcatProcessor.LOGCAT_CMD     // Catch:{ all -> 0x0092 }
            r9 = 0
            r8 = r8[r9]     // Catch:{ all -> 0x0092 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ all -> 0x0092 }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x0092 }
            r6.onError(r7, r0)     // Catch:{ all -> 0x0092 }
            if (r4 == 0) goto L_0x005f
            r4.close()     // Catch:{ IOException -> 0x009e }
        L_0x005f:
            r10.stopCatter()
        L_0x0062:
            return
        L_0x0063:
            r6 = move-exception
            r0 = r6
            org.baole.applocker.service.LogcatInterceptor r6 = r10.mInterceptor
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "Can't start "
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String[] r8 = org.baole.applocker.service.LogcatProcessor.LOGCAT_CMD
            r8 = r8[r9]
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r7 = r7.toString()
            r6.onError(r7, r0)
            goto L_0x0062
        L_0x0082:
            r6 = move-exception
            r1 = r6
            r1.printStackTrace()
            goto L_0x001c
        L_0x0088:
            if (r5 == 0) goto L_0x008d
            r5.close()     // Catch:{ IOException -> 0x009c }
        L_0x008d:
            r10.stopCatter()
            r4 = r5
            goto L_0x0062
        L_0x0092:
            r6 = move-exception
        L_0x0093:
            if (r4 == 0) goto L_0x0098
            r4.close()     // Catch:{ IOException -> 0x00a0 }
        L_0x0098:
            r10.stopCatter()
            throw r6
        L_0x009c:
            r6 = move-exception
            goto L_0x008d
        L_0x009e:
            r6 = move-exception
            goto L_0x005f
        L_0x00a0:
            r7 = move-exception
            goto L_0x0098
        L_0x00a2:
            r6 = move-exception
            r4 = r5
            goto L_0x0093
        L_0x00a5:
            r6 = move-exception
            r0 = r6
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: org.baole.applocker.service.LogcatProcessor.run():void");
    }

    public void stopCatter() {
        if (this.mLogcatProc != null) {
            this.mLogcatProc.destroy();
            this.mLogcatProc = null;
        }
    }

    public int getLineCount() {
        return this.mLines;
    }
}
