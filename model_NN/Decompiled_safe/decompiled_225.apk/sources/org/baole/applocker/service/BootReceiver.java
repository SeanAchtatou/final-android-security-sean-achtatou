package org.baole.applocker.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BootReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        AppLockerService.startActivityWatcherService(context);
    }
}
