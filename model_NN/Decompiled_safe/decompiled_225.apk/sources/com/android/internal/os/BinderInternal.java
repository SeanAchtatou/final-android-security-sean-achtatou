package com.android.internal.os;

import android.os.IBinder;
import android.os.SystemClock;
import java.lang.ref.WeakReference;

public class BinderInternal {
    static WeakReference<GcWatcher> mGcWatcher = new WeakReference<>(new GcWatcher());
    static long mLastGcTime;

    public static final native void disableBackgroundScheduling(boolean z);

    public static final native IBinder getContextObject();

    static final native void handleGc();

    public static final native void joinThreadPool();

    static final class GcWatcher {
        GcWatcher() {
        }

        /* access modifiers changed from: protected */
        public void finalize() throws Throwable {
            BinderInternal.handleGc();
            BinderInternal.mLastGcTime = SystemClock.uptimeMillis();
            BinderInternal.mGcWatcher = new WeakReference<>(new GcWatcher());
        }
    }

    public static long getLastGcTime() {
        return mLastGcTime;
    }

    public static void forceGc(String reason) {
        Runtime.getRuntime().gc();
    }

    static void forceBinderGc() {
        forceGc("Binder");
    }
}
