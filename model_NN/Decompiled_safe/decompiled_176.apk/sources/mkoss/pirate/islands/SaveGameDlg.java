package mkoss.pirate.islands;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;

public class SaveGameDlg extends View {
    gButton cancelButton;
    gButton noButton;
    Paint paint;
    gButton yesButton;

    public SaveGameDlg(Context c) {
        super(c);
        this.paint = null;
        this.paint = new Paint();
        initUI();
    }

    public void initUI() {
        this.yesButton = new gButton();
        this.yesButton.setText("Yes");
        this.yesButton.setId(Vars.getInstance().SAVE_YES);
        Vars.getInstance().addButton(this.yesButton);
        this.noButton = new gButton();
        this.noButton.setText("No");
        this.noButton.setId(Vars.getInstance().SAVE_NO);
        Vars.getInstance().addButton(this.noButton);
        this.cancelButton = new gButton();
        this.cancelButton.setText("Cancel");
        this.cancelButton.setId(Vars.getInstance().SAVE_CANCEL);
        Vars.getInstance().addButton(this.cancelButton);
    }

    public void layoutUI() {
        int btnw = (int) (((double) getWidth()) * 0.45d);
        int btnh = Vars.getInstance().getScaleFactor() * 60;
        this.yesButton.setSize(btnw, btnh);
        this.yesButton.setPosition((int) (((double) getWidth()) * 0.05d), getHeight() - (this.yesButton.getHeight() * 3));
        this.noButton.setSize(btnw, btnh);
        this.noButton.setPosition(((int) (((double) getWidth()) * 0.05d)) + (getWidth() / 2), getHeight() - (this.yesButton.getHeight() * 3));
        this.cancelButton.setSize(btnw, btnh);
        this.cancelButton.setPosition((getWidth() / 2) - (this.cancelButton.getWidth() / 2), (int) (((double) getHeight()) - (1.5d * ((double) this.yesButton.getHeight()))));
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        layoutUI();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        this.paint.setColor(Color.rgb(0, 0, 20));
        this.paint.setAntiAlias(true);
        this.paint.setStyle(Paint.Style.FILL);
        canvas.drawRect(0.0f, 0.0f, (float) getWidth(), (float) getHeight(), this.paint);
        ImageLoader.getInstance().getImage(ImageLoader.getInstance().MENU).setBounds(0, 0, getWidth(), getHeight());
        ImageLoader.getInstance().getImage(ImageLoader.getInstance().MENU).draw(canvas);
        int text_y = getHeight() / 3;
        this.paint.setTextSize(35.0f);
        this.paint.setColor(-16777216);
        canvas.drawText("Save current game?", (float) ((getWidth() / 2) - (((int) this.paint.measureText("Save current game?")) / 2)), (float) text_y, this.paint);
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            Vars.getInstance().getButton(i).draw(canvas, this.paint);
        }
    }

    public void mouseDown(int x, int y) {
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseDown(x, y);
            if (pressed != 0) {
                postInvalidate();
                return;
            }
        }
    }

    public void mouseUp(int x, int y) {
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseUp(x, y);
            if (pressed != 0) {
                handleCommand(Vars.getInstance().getButton(i).getId());
                postInvalidate();
                return;
            }
        }
    }

    public void mouseDrag(int x, int y) {
        int pressed = 0;
        for (int i = 0; i < Vars.getInstance().getButtonList().size(); i++) {
            pressed |= Vars.getInstance().getButton(i).mouseDrag(x, y);
        }
        if (pressed != 0) {
            postInvalidate();
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        int x = (int) event.getX();
        int y = (int) event.getY();
        if (event.getAction() == 0) {
            mouseDown(x, y);
        } else if (2 == event.getAction()) {
            mouseDrag(x, y);
        } else if (1 == event.getAction()) {
            mouseUp(x, y);
        }
        postInvalidate();
        return true;
    }

    public void handleCommand(int id) {
        if (id == Vars.getInstance().SAVE_YES) {
            FileWR.getInstance().saveGame();
            IslandsActivity.getInstance().showMenu();
        } else if (id == Vars.getInstance().SAVE_NO) {
            IslandsActivity.getInstance().showMenu();
        } else if (id == Vars.getInstance().SAVE_CANCEL) {
            IslandsActivity.getInstance().showGame();
        }
    }
}
