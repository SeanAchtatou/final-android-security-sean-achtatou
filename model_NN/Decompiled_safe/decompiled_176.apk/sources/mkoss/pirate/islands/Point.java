package mkoss.pirate.islands;

public class Point {
    public double X;
    public double Y;

    public Point(int x, int y) {
        this.X = (double) x;
        this.Y = (double) y;
    }
}
