package mkoss.pirate.islands;

public class CMap {
    static CMap instance = null;
    int height = 1000;
    int mouseStartX = 0;
    int mouseStartY = 0;
    int screen_height = 200;
    int screen_width = 200;
    int vpx = 0;
    int vpy = 0;
    int width = 1000;

    public void startScroll(int x, int y) {
        this.mouseStartX = x;
        this.mouseStartY = y;
    }

    public void scroll(int x, int y) {
        this.mouseStartX = x;
        this.mouseStartY = y;
        SetVp(this.vpx - ((int) (((double) (x - this.mouseStartX)) * 1.5d)), this.vpy - ((int) (((double) (y - this.mouseStartY)) * 1.5d)));
    }

    public void setMapSize(int w, int h) {
        this.width = w;
        this.height = h;
    }

    public int GetVpx() {
        return this.vpx;
    }

    public int GetVpy() {
        return this.vpy;
    }

    public void SetVp(int x, int y) {
        this.vpx = x;
        this.vpy = y;
        AdjustVP();
    }

    public void AdjustVP() {
        if (this.vpx < 0) {
            this.vpx = 0;
        }
        if (this.vpy < 0) {
            this.vpy = 0;
        }
        if (this.vpx + this.screen_width > this.width) {
            this.vpx = this.width - this.screen_width;
        }
        if (this.vpy + this.screen_height > this.height) {
            this.vpy = this.height - this.screen_height;
        }
    }

    public void centerOnPoint(int x, int y) {
        this.vpx = x - (this.screen_width / 2);
        this.vpy = y - (this.screen_height / 2);
        AdjustVP();
    }

    public void SetScreenSize(int w, int h) {
        this.screen_width = w;
        this.screen_height = h;
    }

    public int GetWidth() {
        return this.width;
    }

    public int GetHeight() {
        return this.height;
    }

    public int getScreenWidth() {
        return this.screen_width;
    }

    public int getScreenHeight() {
        return this.screen_height;
    }

    public void generate(int planet_count) {
        String[] starNames = {"Andromeda", "Antlia", "Apus", "Aquarius", "Aquila", "Ara", "Aries", "Auriga", "Bootes", "Caelum", "Camelopardalis", "Cancer", "Canes Venatici", "Canis Major", "Canis Minor", "Capricornus", "Carina", "Cassiopeia", "Centaurus", "Cepheus", "Cetus", "Chamaeleon", "Circinus", "Columba", "Coma Berenices", "Corona Australis", "Corona Borealis", "Corvus", "Crater", "Crux", "Cygnus", "Delphinus", "Dorado", "Draco", "Equuleus", "Eridanus", "Fornax", "Gemini", "Grus", "Hercules", "Horologium", "Hydra", "Hydrus", "Indus", "Lacerta", "Leo", "Leo Minor", "Lepus", "Libra", "Lupus", "Lynx", "Lyra", "Mensa", "Microscopium", "Monoceros", "Musca", "Norma", "Octans", "Ophiuchus", "Orion", "Pavo", "Pegasus", "Perseus", "Phoenix", "Pictor", "Pisces", "Piscis Austrinus", "Puppis", "Pyxis", "Reticulum", "Sagitta", "Sagittarius", "Scorpius", "Sculptor", "Scutum", "Serpens", "Sextans", "Taurus", "Telescopium", "Triangulum", "Triangulum Australe", "Tucana", "Ursa Major", "Ursa Minor", "Vega", "Vela", "Virgo", "Volans", "Vulpecula"};
        Vars vars = Vars.getInstance();
        vars.getPlanetList().clear();
        int planet_size = Vars.getInstance().getScaleFactor() * 60;
        for (int i = 0; i < planet_count; i++) {
            Planet p = new Planet();
            double nx = (double) vars.GetNextInt(getInstance().GetWidth() - 80);
            double ny = (double) (vars.GetNextInt(getInstance().GetHeight()) - 130);
            p.SetPlanetType(vars.GetNextInt(5));
            p.setSize(planet_size, planet_size);
            boolean done = false;
            int mapw = getInstance().GetWidth();
            int maph = getInstance().GetHeight();
            while (!done) {
                done = true;
                nx = (double) (vars.GetNextInt(mapw - 80) + 20);
                ny = (double) (vars.GetNextInt(maph - 130) + 20);
                int j = 0;
                while (true) {
                    if (j >= i) {
                        break;
                    }
                    Planet p1 = vars.getPlanet(j);
                    if (Math.sqrt(((p1.getPx() - nx) * (p1.getPx() - nx)) + ((p1.getPy() - ny) * (p1.getPy() - ny))) < ((double) (planet_size * 2))) {
                        done = false;
                        break;
                    }
                    j++;
                }
            }
            p.SetPos(nx, ny);
            p.setProduction(vars.GetNextInt(10) + 5);
            if (i < starNames.length) {
                p.setName(starNames[i]);
            } else {
                p.setName("Island " + i);
            }
            vars.getPlanetList().add(p);
        }
    }

    public static CMap getInstance() {
        if (instance == null) {
            instance = new CMap();
        }
        return instance;
    }
}
