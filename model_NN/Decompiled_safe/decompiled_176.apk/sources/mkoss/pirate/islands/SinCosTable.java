package mkoss.pirate.islands;

public class SinCosTable {
    static SinCosTable instance = null;
    double[] _cos = new double[360];
    double[] _sin = new double[360];

    public SinCosTable() {
        instance = this;
        for (int i = 0; i < 360; i++) {
            double rad_angle = (((double) i) * 3.141592653589793d) / 180.0d;
            this._sin[i] = Math.sin(rad_angle);
            this._cos[i] = Math.cos(rad_angle);
        }
    }

    public static SinCosTable getInstance() {
        if (instance == null) {
            instance = new SinCosTable();
        }
        return instance;
    }

    public double Sin(int angle) {
        while (angle < 0) {
            angle += 360;
        }
        while (angle >= 360) {
            angle -= 360;
        }
        return this._sin[angle];
    }

    public double Cos(int angle) {
        while (angle < 0) {
            angle += 360;
        }
        while (angle >= 360) {
            angle -= 360;
        }
        return this._cos[angle];
    }
}
