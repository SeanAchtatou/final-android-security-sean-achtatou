package mkoss.pirate.islands;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import mkoss.pirate.islands.Player;

public class FileWR {
    static FileWR instance = null;
    int file_pos = 0;

    public static final byte[] intToByteArray(int value) {
        return new byte[]{(byte) (value >>> 24), (byte) (value >>> 16), (byte) (value >>> 8), (byte) value};
    }

    public static final int byteArrayToInt(byte[] b) {
        return (b[0] << 24) + ((b[1] & 255) << 16) + ((b[2] & 255) << 8) + (b[3] & 255);
    }

    public void writeInt(FileOutputStream fos, int val) {
        try {
            fos.write(intToByteArray(val));
        } catch (Exception e) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, ?[OBJECT, ARRAY], java.io.FileNotFoundException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, ?[OBJECT, ARRAY], java.io.IOException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public void saveGame() {
        FileOutputStream fos = null;
        try {
            fos = IslandsActivity.getInstance().getApplicationContext().openFileOutput("solarwars_save", 0);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileWR.class.getName()).log(Level.SEVERE, (String) null, (Throwable) ex);
        }
        try {
            Vars v = Vars.getInstance();
            writeInt(fos, CMap.getInstance().GetVpx());
            writeInt(fos, CMap.getInstance().GetVpy());
            writeInt(fos, CMap.getInstance().GetWidth());
            writeInt(fos, CMap.getInstance().GetHeight());
            writeInt(fos, CMap.getInstance().getScreenWidth());
            writeInt(fos, CMap.getInstance().getScreenHeight());
            int pcount = v.getPlayerList().size();
            writeInt(fos, pcount);
            for (int i = 0; i < pcount; i++) {
                writeInt(fos, v.getPlayer(i).getName().length());
                fos.write(v.getPlayer(i).getName().getBytes());
                writeInt(fos, v.getPlayer(i).getId());
                if (v.getPlayer(i).getPlayerType() == Player.PlayerType.Human) {
                    writeInt(fos, 1);
                }
                if (v.getPlayer(i).getPlayerType() == Player.PlayerType.AI) {
                    writeInt(fos, 2);
                }
                writeInt(fos, v.getPlayer(i).getPlanetConquered());
                writeInt(fos, v.getPlayer(i).getShipsBuild());
            }
            writeInt(fos, v.getPlanetList().size());
            for (int p = 0; p < v.getPlanetList().size(); p++) {
                writeInt(fos, v.getPlanet(p).GetPlanetType());
                writeInt(fos, v.getPlanet(p).getName().length());
                fos.write(v.getPlanet(p).getName().getBytes());
                writeInt(fos, v.getPlanet(p).getShips());
                writeInt(fos, v.getPlanet(p).getProduction());
                writeInt(fos, (int) v.getPlanet(p).getPx());
                writeInt(fos, (int) v.getPlanet(p).getPy());
                writeInt(fos, (int) v.getPlanet(p).getWidth());
                writeInt(fos, (int) v.getPlanet(p).getHeight());
                writeInt(fos, (int) v.getPlanet(p).getDestPx());
                writeInt(fos, (int) v.getPlanet(p).getDestPy());
                writeInt(fos, v.getPlanet(p).getOwnerID());
            }
            writeInt(fos, v.getFleetList().size());
            for (int f = 0; f < v.getFleetList().size(); f++) {
                writeInt(fos, v.getFleet(f).getShips());
                writeInt(fos, v.getFleet(f).getSpeed());
                writeInt(fos, (int) v.getFleet(f).getPx());
                writeInt(fos, (int) v.getFleet(f).getPy());
                writeInt(fos, (int) v.getFleet(f).getWidth());
                writeInt(fos, (int) v.getFleet(f).getHeight());
                writeInt(fos, (int) v.getFleet(f).getDestPx());
                writeInt(fos, (int) v.getFleet(f).getDestPy());
                writeInt(fos, v.getFleet(f).getOwnerID());
            }
            fos.close();
        } catch (IOException e) {
            Logger.getLogger(FileWR.class.getName()).log(Level.SEVERE, (String) null, (Throwable) e);
        }
    }

    public int getInt(byte[] buffer) {
        byte[] int_buf = {buffer[this.file_pos], buffer[this.file_pos + 1], buffer[this.file_pos + 2], buffer[this.file_pos + 3]};
        this.file_pos += 4;
        return byteArrayToInt(int_buf);
    }

    public String getString(byte[] buffer, int len) {
        byte[] tmp = new byte[len];
        for (int i = 0; i < len; i++) {
            tmp[i] = buffer[this.file_pos + i];
        }
        this.file_pos += len;
        return new String(tmp);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, ?[OBJECT, ARRAY], java.io.FileNotFoundException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, ?[OBJECT, ARRAY], java.io.IOException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public boolean loadGame() {
        try {
            FileInputStream fis = IslandsActivity.getInstance().getApplicationContext().openFileInput("solarwars_save");
            try {
                byte[] buffer = new byte[fis.available()];
                fis.read(buffer);
                this.file_pos = 0;
                CMap.getInstance().SetVp(getInt(buffer), getInt(buffer));
                CMap.getInstance().setMapSize(getInt(buffer), getInt(buffer));
                CMap.getInstance().SetScreenSize(getInt(buffer), getInt(buffer));
                Vars.getInstance().getPlayerList().clear();
                int player_count = getInt(buffer);
                for (int i = 0; i < player_count; i++) {
                    int len = getInt(buffer);
                    Player p = new Player();
                    p.setName(getString(buffer, len));
                    p.setId(getInt(buffer));
                    int type = getInt(buffer);
                    if (type == 1) {
                        p.setPlayerType(Player.PlayerType.Human);
                    }
                    if (type == 2) {
                        p.setPlayerType(Player.PlayerType.AI);
                    }
                    p.setPlanetConquered(getInt(buffer));
                    p.ship_build = getInt(buffer);
                    Vars.getInstance().getPlayerList().add(p);
                }
                Vars.getInstance().getPlanetList().clear();
                int planet_count = getInt(buffer);
                for (int p2 = 0; p2 < planet_count; p2++) {
                    Planet planet = new Planet();
                    planet.SetPlanetType(getInt(buffer));
                    planet.setName(getString(buffer, getInt(buffer)));
                    planet.setShips(getInt(buffer));
                    planet.setProduction(getInt(buffer));
                    planet.SetPos((double) getInt(buffer), (double) getInt(buffer));
                    planet.setSize(getInt(buffer), getInt(buffer));
                    planet.setDestPos((double) getInt(buffer), (double) getInt(buffer));
                    planet.setOwnerID(getInt(buffer));
                    Vars.getInstance().getPlanetList().add(planet);
                }
                Vars.getInstance().getFleetList().clear();
                int fleet_count = getInt(buffer);
                for (int f = 0; f < fleet_count; f++) {
                    Fleet fleet = new Fleet();
                    fleet.setShips(getInt(buffer));
                    fleet.setSpeed(getInt(buffer));
                    fleet.SetPos((double) getInt(buffer), (double) getInt(buffer));
                    fleet.setSize(getInt(buffer), getInt(buffer));
                    fleet.setDestPos((double) getInt(buffer), (double) getInt(buffer));
                    fleet.setOwnerID(getInt(buffer));
                    Vars.getInstance().getFleetList().add(fleet);
                }
                fis.close();
                return true;
            } catch (IOException e) {
                IOException ex = e;
                Logger.getLogger(FileWR.class.getName()).log(Level.SEVERE, (String) null, (Throwable) ex);
                return false;
            }
        } catch (FileNotFoundException ex2) {
            Logger.getLogger(FileWR.class.getName()).log(Level.SEVERE, (String) null, (Throwable) ex2);
            return false;
        }
    }

    public static FileWR getInstance() {
        if (instance == null) {
            instance = new FileWR();
        }
        return instance;
    }
}
