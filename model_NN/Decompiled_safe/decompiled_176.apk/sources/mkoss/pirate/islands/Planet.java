package mkoss.pirate.islands;

public class Planet extends GameObject {
    String name;
    int planetType;
    int production = 10;
    int ships = 10;

    public String getName() {
        return this.name;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void SetPlanetType(int t) {
        this.planetType = t;
    }

    public int GetPlanetType() {
        return this.planetType;
    }

    public int getProduction() {
        return this.production;
    }

    Planet() {
        setOwnerID(-1);
        this.planetType = 0;
    }

    public void setProduction(int t) {
        this.production = t;
    }

    public int getShips() {
        return this.ships;
    }

    public void setShips(int s) {
        this.ships = s;
    }

    public void AdvanceTime() {
    }
}
