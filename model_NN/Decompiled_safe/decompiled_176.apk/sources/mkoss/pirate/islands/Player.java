package mkoss.pirate.islands;

public class Player {
    int id;
    String name = "Player name";
    int planet_conquered = 0;
    public PlayerType playerType;
    int ship_build = 0;

    public enum PlayerType {
        Human,
        AI
    }

    public int getShipsBuild() {
        return this.ship_build;
    }

    public int getPlanetConquered() {
        return this.planet_conquered;
    }

    public void setPlanetConquered(int p) {
        this.planet_conquered = p;
    }

    public void setId(int i) {
        this.id = i;
    }

    public int getId() {
        return this.id;
    }

    public void setName(String n) {
        this.name = n;
    }

    public String getName() {
        return this.name;
    }

    public void setPlayerType(PlayerType pt) {
        this.playerType = pt;
    }

    public PlayerType getPlayerType() {
        return this.playerType;
    }
}
