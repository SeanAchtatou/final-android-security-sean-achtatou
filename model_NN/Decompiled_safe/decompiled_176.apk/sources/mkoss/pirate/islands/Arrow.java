package mkoss.pirate.islands;

import android.graphics.Canvas;
import android.graphics.Paint;

class Arrow {
    int angle;
    Point[] drawing_points = new Point[4];
    int height;
    Point[] points = new Point[4];
    int width;
    int xoff;
    int yoff;

    public Arrow() {
        this.points[0] = new Point(1 * 4, 0);
        this.points[1] = new Point(1 * 8, 1 * 14);
        this.points[2] = new Point(1 * 4, 1 * 10);
        this.points[3] = new Point(0, 1 * 14);
        for (int p = 0; p < this.drawing_points.length; p++) {
            this.drawing_points[p] = new Point(0, 0);
        }
        this.angle = 0;
        this.width = 0;
        this.height = 0;
        for (int i = 0; i < this.drawing_points.length; i++) {
            if (this.points[i].X > ((double) this.width)) {
                this.width = (int) this.points[i].X;
            }
            if (this.points[i].Y > ((double) this.height)) {
                this.height = (int) this.points[i].Y;
            }
        }
    }

    public void SetAngle(int a) {
        this.angle = a;
    }

    public void SetDrawingOffset(int x, int y) {
        this.xoff = x;
        this.yoff = y;
    }

    public void Rotate() {
        int xc = this.width / 2;
        int yc = this.height / 2;
        for (int i = 0; i < this.drawing_points.length; i++) {
            this.drawing_points[i].X = this.points[i].X - ((double) xc);
            this.drawing_points[i].Y = this.points[i].Y - ((double) yc);
            double x = this.drawing_points[i].X;
            double y = this.drawing_points[i].Y;
            this.drawing_points[i].X = (double) ((int) ((SinCosTable.getInstance().Cos(this.angle) * x) - (SinCosTable.getInstance().Sin(this.angle) * y)));
            this.drawing_points[i].Y = (double) ((int) ((SinCosTable.getInstance().Sin(this.angle) * x) + (SinCosTable.getInstance().Cos(this.angle) * y)));
            this.drawing_points[i].X += (double) xc;
            this.drawing_points[i].Y += (double) yc;
        }
    }

    public int GetSize() {
        if (this.width > this.height) {
            return this.width;
        }
        return this.height;
    }

    public void MoveDrawingOffset() {
        for (int i = 0; i < this.drawing_points.length; i++) {
            this.drawing_points[i].X += (double) this.xoff;
            this.drawing_points[i].Y += (double) this.yoff;
            this.drawing_points[i].X -= (double) (this.width / 2);
            this.drawing_points[i].Y -= (double) (this.height / 2);
        }
    }

    public void draw(Canvas g, Paint p) {
        Rotate();
        MoveDrawingOffset();
        for (int i = 0; i < this.drawing_points.length - 1; i++) {
            g.drawLine((float) this.drawing_points[i].X, (float) this.drawing_points[i].Y, (float) this.drawing_points[i + 1].X, (float) this.drawing_points[i + 1].Y, p);
        }
        g.drawLine((float) this.drawing_points[0].X, (float) this.drawing_points[0].Y, (float) this.drawing_points[this.drawing_points.length - 1].X, (float) this.drawing_points[this.drawing_points.length - 1].Y, p);
    }
}
