package mkoss.pirate.islands;

public class GameObject {
    double dest_px;
    double dest_py;
    double height;
    int owner_id = -1;
    double px;
    double py;
    double width;

    public void setOwnerID(int id) {
        this.owner_id = id;
    }

    public int getOwnerID() {
        return this.owner_id;
    }

    public double getPx() {
        return this.px;
    }

    public double getPy() {
        return this.py;
    }

    public void SetPos(double x, double y) {
        this.px = x;
        this.py = y;
    }

    public double getWidth() {
        return this.width;
    }

    public double getHeight() {
        return this.height;
    }

    public void setSize(int w, int h) {
        this.width = (double) w;
        this.height = (double) h;
    }

    public double getDestPx() {
        return this.dest_px;
    }

    public double getDestPy() {
        return this.dest_py;
    }

    public void setDestPos(double x, double y) {
        this.dest_px = x;
        this.dest_py = y;
    }
}
