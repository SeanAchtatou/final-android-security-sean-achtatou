package mkoss.pirate.islands;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;

public class gSlider {
    int currentValue;
    boolean enabled;
    boolean hasKeyboardFocus;
    int height;
    int id;
    String image;
    ImageLoader imgInst;
    boolean is_pressed;
    int maxValue;
    int minValue;
    boolean mouseDown;
    int px;
    int py;
    int sliderPos;
    int sliderWidth;
    String text;
    boolean visible;
    int width;

    public void setMaxValue(int mv) {
        this.maxValue = mv;
        UpdateValueBtn();
    }

    public int getMaxValue() {
        return this.maxValue;
    }

    public void setMinValue(int mv) {
        this.minValue = mv;
    }

    public int getMinValue() {
        return this.minValue;
    }

    public void setValue(int mv) {
        this.currentValue = mv;
        UpdateValueBtn();
    }

    public int getValue() {
        return this.currentValue;
    }

    public void setEnabled(boolean e) {
        this.enabled = e;
    }

    public void setKeyboardFocus(boolean f) {
        this.hasKeyboardFocus = f;
    }

    public boolean getKeyboardFocus() {
        return this.hasKeyboardFocus;
    }

    public void setImage(String img) {
        this.image = img;
    }

    public gSlider() {
        this.imgInst = null;
        this.image = null;
        this.enabled = true;
        this.is_pressed = false;
        this.mouseDown = false;
        this.text = "";
        this.id = -1;
        this.imgInst = ImageLoader.getInstance();
    }

    public void setText(String t) {
        this.text = t;
    }

    public String getText() {
        return this.text;
    }

    public void setId(int i) {
        this.id = i;
    }

    public int getId() {
        return this.id;
    }

    public void show() {
        this.visible = true;
    }

    public void hide() {
        this.visible = false;
    }

    public void setPosition(int x, int y) {
        this.px = x;
        this.py = y;
    }

    public void setSize(int w, int h) {
        this.width = w;
        this.height = h;
    }

    public int getPx() {
        return this.px;
    }

    public int getPy() {
        return this.py;
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }

    public void drawImage(Canvas canvas, int image2, int x, int y, int w, int h) {
        if (ImageLoader.getInstance().getImage(image2) != null) {
            ImageLoader.getInstance().getImage(image2).setBounds(x, y, x + w, y + h);
            ImageLoader.getInstance().getImage(image2).draw(canvas);
        }
    }

    public void draw(Canvas g, Paint p) {
        if (this.visible) {
            UpdateValueBtn();
            drawImage(g, ImageLoader.getInstance().PUSH_OFF_8, this.px, this.py, this.width, this.height);
            drawImage(g, ImageLoader.getInstance().PUSH_OFF_1, this.px, this.py, this.width, 10);
            drawImage(g, ImageLoader.getInstance().PUSH_OFF_5, this.px, (this.py + this.height) - 10, this.width, 10);
            drawImage(g, ImageLoader.getInstance().PUSH_OFF_7, this.px, this.py, 10, this.height);
            drawImage(g, ImageLoader.getInstance().PUSH_OFF_3, (this.px - 10) + this.width, this.py, 10, this.height);
            drawImage(g, ImageLoader.getInstance().PUSH_OFF_0, this.px, this.py, 10, 10);
            drawImage(g, ImageLoader.getInstance().PUSH_OFF_2, (this.px + this.width) - 10, this.py, 10, 10);
            drawImage(g, ImageLoader.getInstance().PUSH_OFF_4, (this.px + this.width) - 10, (this.py + this.height) - 10, 10, 10);
            drawImage(g, ImageLoader.getInstance().PUSH_OFF_6, this.px, (this.py - 10) + this.height, 10, 10);
            if (this.image == null) {
                p.setTextSize(24.0f);
                p.setColor(-1);
                g.drawText(this.text, ((float) (this.px + (this.width / 2))) - (p.measureText(this.text) / 2.0f), ((float) (this.py + (this.height / 2))) + (p.getTextSize() / 2.0f), p);
            }
            p.setColor(-7829368);
            g.drawRect((float) (this.sliderPos + (this.sliderWidth / 2)), (float) (this.py + 8), (float) (this.sliderWidth + (this.sliderWidth / 2) + this.sliderPos), (float) ((this.py + this.height) - 8), p);
            p.setColor(-1);
            p.setTypeface(Typeface.SANS_SERIF);
            p.setTextSize(24.0f);
            String val = Integer.toString(this.currentValue);
            g.drawText(val, ((float) (this.px + (this.width / 2))) - (p.measureText(val) / 2.0f), ((float) (this.py + (this.height / 2))) + (p.getTextSize() / 2.0f), p);
        }
    }

    public int mouseDrag(int x, int y) {
        if (!this.visible || !this.enabled) {
            return 0;
        }
        if (x <= this.px || y <= this.py || x >= this.px + this.width || y >= this.py + this.height) {
            this.is_pressed = false;
            return 0;
        }
        this.is_pressed = true;
        this.currentValue = (int) (((double) (this.maxValue + 1)) * (((double) (x - this.px)) / ((double) this.width)));
        UpdateValueBtn();
        return 1;
    }

    public int mouseUp(int x, int y) {
        if (!this.visible || !this.enabled) {
            return 0;
        }
        int val = 0;
        if (x > this.px && y > this.py && x < this.px + this.width && y < this.py + this.height && this.is_pressed && this.mouseDown) {
            val = 1;
        }
        this.is_pressed = false;
        this.mouseDown = false;
        UpdateValueBtn();
        return val;
    }

    public int mouseDown(int x, int y) {
        if (!this.visible || !this.enabled) {
            return 0;
        }
        if (x <= this.px || y <= this.py || x >= this.px + this.width || y >= this.py + this.height) {
            return 0;
        }
        this.is_pressed = true;
        this.mouseDown = true;
        UpdateValueBtn();
        return 1;
    }

    public void UpdateValueBtn() {
        double diff = (double) ((this.maxValue - this.minValue) + 1);
        double w = ((double) this.width) / diff;
        double pos = (((double) (this.currentValue - this.minValue)) * w) - ((((double) this.minValue) / diff) * w);
        if (w < 20.0d) {
            w = 20.0d;
        }
        this.sliderWidth = (int) w;
        this.sliderPos = (int) (((double) this.px) + pos);
        if (this.sliderPos < this.px) {
            this.sliderPos = this.px;
        }
        if (this.sliderPos > this.width || this.currentValue == this.maxValue) {
            this.sliderPos = this.width - this.sliderWidth;
        }
    }
}
