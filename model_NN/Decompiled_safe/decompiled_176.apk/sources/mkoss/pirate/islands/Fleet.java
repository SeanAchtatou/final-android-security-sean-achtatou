package mkoss.pirate.islands;

public class Fleet extends GameObject {
    int ships;
    int speed;

    public Fleet() {
        this.speed = 40;
        this.speed = Vars.getInstance().getScaleFactor() * 40;
    }

    public void setShips(int s) {
        this.ships = s;
    }

    public int getShips() {
        return this.ships;
    }

    public void setSpeed(int s) {
        this.speed = s;
    }

    public int getSpeed() {
        return this.speed;
    }
}
