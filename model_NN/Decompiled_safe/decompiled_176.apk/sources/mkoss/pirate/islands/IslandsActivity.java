package mkoss.pirate.islands;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import mkoss.pirate.islands.Vars;

public class IslandsActivity extends Activity {
    static IslandsActivity instance = null;
    EndGameCanvas endGameCanvas = null;
    GameCanvas gameCanvas = null;
    LinearLayout gameLayout;
    private Button menuBtn;
    MenuCanvas menuCanvas = null;
    private Button mobilsoftBtn;
    SaveGameDlg saveGameCanvas = null;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        instance = this;
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        Vars.getInstance().screenSize(dm.widthPixels, dm.heightPixels);
        ImageLoader.getInstance().LoadImages();
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        this.menuCanvas = new MenuCanvas(getApplicationContext());
        this.endGameCanvas = new EndGameCanvas(getApplicationContext());
        this.saveGameCanvas = new SaveGameDlg(getApplicationContext());
        showMenu();
    }

    public void showMenu() {
        Vars.getInstance().gameState = Vars.GameState.Menu;
        Vars.getInstance().showUI(true);
        setContentView(this.menuCanvas);
    }

    public void showGame() {
        if (this.gameCanvas == null) {
            this.gameLayout = new LinearLayout(this);
            this.gameLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
            this.gameLayout.setOrientation(1);
            this.gameCanvas = new GameCanvas(this);
            AdView adView = new AdView(this, AdSize.BANNER, "a14c2a4abf04ba2");
            adView.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
            adView.loadAd(new AdRequest());
            this.gameCanvas.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
            this.gameLayout.addView(adView);
            this.gameLayout.addView(this.gameCanvas);
        }
        Vars.getInstance().gameState = Vars.GameState.Game;
        Vars.getInstance().showUI(true);
        setContentView(this.gameLayout);
    }

    public void showAbout() {
        Vars.getInstance().gameState = Vars.GameState.About;
        Vars.getInstance().showUI(false);
        setContentView((int) R.layout.main);
        this.menuBtn = (Button) findViewById(R.id.menubtn);
        this.menuBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                IslandsActivity.this.showMenu();
            }
        });
        this.mobilsoftBtn = (Button) findViewById(R.id.gotomobilsoft);
        this.mobilsoftBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                IslandsActivity.getInstance().startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.mobilsoft.pl")));
            }
        });
    }

    public void showSaveGameDlg() {
        Vars.getInstance().gameState = Vars.GameState.SaveGameDlg;
        Vars.getInstance().showUI(true);
        setContentView(this.saveGameCanvas);
    }

    public void showEndGame() {
        Vars.getInstance().gameState = Vars.GameState.EndGame;
        this.endGameCanvas.initCanvas();
        Vars.getInstance().showUI(true);
        setContentView(this.endGameCanvas);
    }

    public static IslandsActivity getInstance() {
        return instance;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4 && (Vars.getInstance().gameState == Vars.GameState.Game || Vars.getInstance().gameState == Vars.GameState.About || Vars.getInstance().gameState == Vars.GameState.SaveGameDlg || Vars.getInstance().gameState == Vars.GameState.EndGame)) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        System.out.println("keyCode = " + keyCode);
        if (keyCode != 4) {
            return super.onKeyUp(keyCode, event);
        }
        if (Vars.getInstance().gameState == Vars.GameState.Game) {
            askForSaveAndShowMenu();
        } else if (Vars.getInstance().gameState == Vars.GameState.SaveGameDlg || Vars.getInstance().gameState == Vars.GameState.EndGame) {
            showMenu();
        }
        return true;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 2000, 0, "Save game").setIcon((int) R.drawable.menu_save);
        menu.add(0, 2001, 0, "Load game").setIcon((int) R.drawable.menu_load);
        menu.add(1, 2002, 1, "Cancel");
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 2000:
                saveGame();
                return true;
            case 2001:
                loadGame();
                return true;
            case 2002:
                return true;
            default:
                return false;
        }
    }

    public void saveGame() {
        if (Vars.getInstance().gameState == Vars.GameState.Game) {
            FileWR.getInstance().saveGame();
            showInfoDialog("Game saved", "Save game");
            this.gameCanvas.postInvalidate();
        }
    }

    public void loadGame() {
        if (Vars.getInstance().gameState == Vars.GameState.Game && FileWR.getInstance().loadGame()) {
            showInfoDialog("Game loaded", "Load game");
            this.gameCanvas.postInvalidate();
        }
        if (Vars.getInstance().gameState == Vars.GameState.Menu && FileWR.getInstance().loadGame()) {
            getInstance().showGame();
        }
    }

    public void showInfoDialog(String msg, String title) {
        AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
        alertbox.setTitle(title);
        alertbox.setMessage(msg);
        alertbox.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
            }
        });
        alertbox.show();
    }

    public void askForSaveAndShowMenu() {
        AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
        alertbox.setTitle("Save game");
        alertbox.setMessage("Do you want save current game?");
        alertbox.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                FileWR.getInstance().saveGame();
                IslandsActivity.getInstance().showMenu();
            }
        });
        alertbox.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                IslandsActivity.getInstance().showMenu();
            }
        });
        alertbox.show();
    }
}
