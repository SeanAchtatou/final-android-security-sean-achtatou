package org.acra;

import android.app.Activity;
import android.app.NotificationManager;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import org.acra.ErrorReporter;

public class CrashReportDialog extends Activity {
    private static final int CRASH_DIALOG_LEFT_ICON = 17301543;
    String mReportFileName = null;
    /* access modifiers changed from: private */
    public EditText userComment = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mReportFileName = getIntent().getStringExtra("REPORT_FILE_NAME");
        if (this.mReportFileName == null) {
            finish();
        }
        requestWindowFeature(3);
        final Bundle crashResources = ACRA.getCrashResources();
        LinearLayout root = new LinearLayout(this);
        root.setOrientation(1);
        root.setPadding(10, 10, 10, 10);
        root.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        ScrollView scroll = new ScrollView(this);
        root.addView(scroll, new LinearLayout.LayoutParams(-1, -1, 1.0f));
        TextView text = new TextView(this);
        text.setText(getText(crashResources.getInt("RES_DIALOG_TEXT")));
        scroll.addView(text, -1, -1);
        int commentPromptId = crashResources.getInt("RES_DIALOG_COMMENT_PROMPT");
        if (commentPromptId != 0) {
            TextView label = new TextView(this);
            label.setText(getText(commentPromptId));
            label.setPadding(label.getPaddingLeft(), 10, label.getPaddingRight(), label.getPaddingBottom());
            root.addView(label, new LinearLayout.LayoutParams(-1, -2));
            this.userComment = new EditText(this);
            this.userComment.setLines(2);
            root.addView(this.userComment, new LinearLayout.LayoutParams(-1, -2));
        }
        LinearLayout buttons = new LinearLayout(this);
        buttons.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        buttons.setPadding(buttons.getPaddingLeft(), 10, buttons.getPaddingRight(), buttons.getPaddingBottom());
        Button yes = new Button(this);
        yes.setText(17039379);
        yes.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ErrorReporter err = ErrorReporter.getInstance();
                if (CrashReportDialog.this.userComment != null) {
                    err.setUserComment(CrashReportDialog.this.userComment.getText().toString());
                }
                err.getClass();
                ErrorReporter.ReportsSenderWorker worker = new ErrorReporter.ReportsSenderWorker();
                worker.setCommentReportFileName(CrashReportDialog.this.mReportFileName);
                worker.start();
                int toastId = crashResources.getInt("RES_DIALOG_OK_TOAST");
                if (toastId != 0) {
                    Toast.makeText(CrashReportDialog.this.getApplicationContext(), toastId, 1).show();
                }
                CrashReportDialog.this.finish();
            }
        });
        buttons.addView(yes, new LinearLayout.LayoutParams(-1, -2, 1.0f));
        Button no = new Button(this);
        no.setText(17039369);
        no.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ErrorReporter.getInstance().deletePendingReports();
                CrashReportDialog.this.finish();
            }
        });
        buttons.addView(no, new LinearLayout.LayoutParams(-1, -2, 1.0f));
        root.addView(buttons, new LinearLayout.LayoutParams(-1, -2));
        setContentView(root);
        int resTitle = crashResources.getInt("RES_DIALOG_TITLE");
        if (resTitle != 0) {
            setTitle(resTitle);
        }
        int resLeftIcon = crashResources.getInt("RES_DIALOG_ICON");
        if (resLeftIcon != 0) {
            getWindow().setFeatureDrawableResource(3, resLeftIcon);
        } else {
            getWindow().setFeatureDrawableResource(3, CRASH_DIALOG_LEFT_ICON);
        }
        cancelNotification();
    }

    /* access modifiers changed from: protected */
    public void cancelNotification() {
        ((NotificationManager) getSystemService("notification")).cancel(666);
    }
}
