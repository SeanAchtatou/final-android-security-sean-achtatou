package twitter4j;

import com.adwhirl.util.AdWhirlUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import twitter4j.internal.http.HttpResponse;
import twitter4j.internal.json.DataObjectFactoryUtil;
import twitter4j.internal.org.json.JSONArray;
import twitter4j.internal.org.json.JSONException;
import twitter4j.internal.org.json.JSONObject;
import twitter4j.internal.util.ParseUtil;

final class TrendsJSONImpl implements Trends, Serializable {
    private static final long serialVersionUID = -7151479143843312309L;
    private Date asOf;
    private Location location;
    private Date trendAt;
    private Trend[] trends;

    public int compareTo(Object x0) {
        return compareTo((Trends) x0);
    }

    public int compareTo(Trends that) {
        return this.trendAt.compareTo(that.getTrendAt());
    }

    TrendsJSONImpl(HttpResponse res) throws TwitterException {
        String jsonStr = res.asString();
        init(res.asString());
        DataObjectFactoryUtil.clearThreadLocalMap();
        DataObjectFactoryUtil.registerJSONObject(this, jsonStr);
    }

    TrendsJSONImpl(String jsonStr) throws TwitterException {
        init(jsonStr);
    }

    /* access modifiers changed from: package-private */
    public void init(String jsonStr) throws TwitterException {
        JSONObject json;
        try {
            if (jsonStr.startsWith("[")) {
                JSONArray array = new JSONArray(jsonStr);
                if (array.length() > 0) {
                    json = array.getJSONObject(0);
                } else {
                    throw new TwitterException("No trends found on the specified woeid");
                }
            } else {
                json = new JSONObject(jsonStr);
            }
            this.asOf = parseTrendsDate(json.getString("as_of"));
            this.location = extractLocation(json);
            JSONArray array2 = json.getJSONArray("trends");
            this.trendAt = this.asOf;
            this.trends = jsonArrayToTrendArray(array2);
        } catch (JSONException e) {
            JSONException jsone = e;
            throw new TwitterException(jsone.getMessage(), jsone);
        }
    }

    TrendsJSONImpl(Date asOf2, Location location2, Date trendAt2, Trend[] trends2) {
        this.asOf = asOf2;
        this.location = location2;
        this.trendAt = trendAt2;
        this.trends = trends2;
    }

    static List<Trends> createTrendsList(HttpResponse res) throws TwitterException {
        JSONObject json = res.asJSONObject();
        try {
            Date asOf2 = parseTrendsDate(json.getString("as_of"));
            JSONObject trendsJson = json.getJSONObject("trends");
            Location location2 = extractLocation(json);
            List<Trends> trends2 = new ArrayList<>(trendsJson.length());
            Iterator ite = trendsJson.keys();
            while (ite.hasNext()) {
                String key = (String) ite.next();
                Trend[] trendsArray = jsonArrayToTrendArray(trendsJson.getJSONArray(key));
                if (key.length() == 19) {
                    trends2.add(new TrendsJSONImpl(asOf2, location2, ParseUtil.getDate(key, "yyyy-MM-dd HH:mm:ss"), trendsArray));
                } else if (key.length() == 16) {
                    trends2.add(new TrendsJSONImpl(asOf2, location2, ParseUtil.getDate(key, "yyyy-MM-dd HH:mm"), trendsArray));
                } else if (key.length() == 10) {
                    trends2.add(new TrendsJSONImpl(asOf2, location2, ParseUtil.getDate(key, "yyyy-MM-dd"), trendsArray));
                }
            }
            Collections.sort(trends2);
            return trends2;
        } catch (JSONException e) {
            JSONException jsone = e;
            throw new TwitterException(new StringBuffer().append(jsone.getMessage()).append(":").append(res.asString()).toString(), jsone);
        }
    }

    private static Location extractLocation(JSONObject json) throws TwitterException {
        Location location2;
        if (json.isNull("locations")) {
            return null;
        }
        try {
            ResponseList<Location> locations = LocationJSONImpl.createLocationList(json.getJSONArray("locations"));
            if (locations.size() != 0) {
                location2 = (Location) locations.get(0);
            } else {
                location2 = null;
            }
            return location2;
        } catch (JSONException e) {
            throw new AssertionError("locations can't be null");
        }
    }

    private static Date parseTrendsDate(String asOfStr) throws TwitterException {
        switch (asOfStr.length()) {
            case 10:
                return new Date(Long.parseLong(asOfStr) * 1000);
            case AdWhirlUtil.NETWORK_TYPE_ZESTADZ:
                return ParseUtil.getDate(asOfStr, "yyyy-mm-dd'T'HH:mm:ss'Z'");
            default:
                return ParseUtil.getDate(asOfStr, "EEE, d MMM yyyy HH:mm:ss z");
        }
    }

    private static Trend[] jsonArrayToTrendArray(JSONArray array) throws JSONException {
        Trend[] trends2 = new Trend[array.length()];
        for (int i = 0; i < array.length(); i++) {
            trends2[i] = new TrendJSONImpl(array.getJSONObject(i));
        }
        return trends2;
    }

    public Trend[] getTrends() {
        return this.trends;
    }

    public Location getLocation() {
        return this.location;
    }

    public Date getAsOf() {
        return this.asOf;
    }

    public Date getTrendAt() {
        return this.trendAt;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Trends)) {
            return false;
        }
        Trends trends1 = (Trends) o;
        if (this.asOf == null ? trends1.getAsOf() != null : !this.asOf.equals(trends1.getAsOf())) {
            return false;
        }
        if (this.trendAt == null ? trends1.getTrendAt() != null : !this.trendAt.equals(trends1.getTrendAt())) {
            return false;
        }
        return Arrays.equals(this.trends, trends1.getTrends());
    }

    public int hashCode() {
        int result;
        int i;
        int i2;
        if (this.asOf != null) {
            result = this.asOf.hashCode();
        } else {
            result = 0;
        }
        int i3 = result * 31;
        if (this.trendAt != null) {
            i = this.trendAt.hashCode();
        } else {
            i = 0;
        }
        int i4 = (i3 + i) * 31;
        if (this.trends != null) {
            i2 = Arrays.hashCode(this.trends);
        } else {
            i2 = 0;
        }
        return i4 + i2;
    }

    public String toString() {
        return new StringBuffer().append("TrendsJSONImpl{asOf=").append(this.asOf).append(", trendAt=").append(this.trendAt).append(", trends=").append(this.trends == null ? null : Arrays.asList(this.trends)).append('}').toString();
    }
}
