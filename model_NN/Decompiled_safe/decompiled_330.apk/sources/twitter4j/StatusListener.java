package twitter4j;

public interface StatusListener extends StreamListener {
    void onDeletionNotice(StatusDeletionNotice statusDeletionNotice);

    void onScrubGeo(int i, long j);

    void onStatus(Status status);

    void onTrackLimitationNotice(int i);
}
