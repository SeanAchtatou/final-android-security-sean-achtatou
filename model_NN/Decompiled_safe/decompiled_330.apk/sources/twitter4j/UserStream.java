package twitter4j;

import java.io.IOException;

public interface UserStream extends StreamImplementation {
    void close() throws IOException;

    void next(UserStreamListener userStreamListener) throws TwitterException;
}
