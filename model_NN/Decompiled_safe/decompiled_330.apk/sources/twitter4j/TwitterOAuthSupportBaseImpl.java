package twitter4j;

import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationContext;
import twitter4j.http.AccessToken;
import twitter4j.http.Authorization;
import twitter4j.http.AuthorizationFactory;
import twitter4j.http.BasicAuthorization;
import twitter4j.http.NullAuthorization;
import twitter4j.http.OAuthAuthorization;
import twitter4j.http.OAuthSupport;
import twitter4j.http.RequestToken;
import twitter4j.internal.http.XAuthAuthorization;

class TwitterOAuthSupportBaseImpl extends TwitterOAuthSupportBase {
    private static final long serialVersionUID = 2166151122833272805L;
    protected transient int id = 0;
    protected transient String screenName = null;

    TwitterOAuthSupportBaseImpl(Configuration conf) {
        super(conf);
    }

    TwitterOAuthSupportBaseImpl() {
        super(ConfigurationContext.getInstance());
    }

    TwitterOAuthSupportBaseImpl(String screenName2, String password) {
        super(ConfigurationContext.getInstance(), screenName2, password);
    }

    TwitterOAuthSupportBaseImpl(Configuration conf, String screenName2, String password) {
        super(conf, screenName2, password);
    }

    TwitterOAuthSupportBaseImpl(Configuration conf, Authorization auth) {
        super(conf, auth);
    }

    public RequestToken getOAuthRequestToken() throws TwitterException {
        return getOAuthRequestToken(null);
    }

    public RequestToken getOAuthRequestToken(String callbackUrl) throws TwitterException {
        return getOAuth().getOAuthRequestToken(callbackUrl);
    }

    public synchronized AccessToken getOAuthAccessToken() throws TwitterException {
        AccessToken oauthAccessToken;
        Authorization auth = getAuthorization();
        if (auth instanceof BasicAuthorization) {
            BasicAuthorization basicAuth = (BasicAuthorization) auth;
            Authorization auth2 = AuthorizationFactory.getInstance(this.conf, true);
            if (auth2 instanceof OAuthAuthorization) {
                this.auth = auth2;
                oauthAccessToken = ((OAuthAuthorization) auth2).getOAuthAccessToken(basicAuth.getUserId(), basicAuth.getPassword());
            } else {
                throw new IllegalStateException("consumer key / secret combination not supplied.");
            }
        } else if (auth instanceof XAuthAuthorization) {
            XAuthAuthorization xauth = (XAuthAuthorization) auth;
            this.auth = xauth;
            oauthAccessToken = new OAuthAuthorization(this.conf, xauth.getConsumerKey(), xauth.getConsumerSecret()).getOAuthAccessToken(xauth.getUserId(), xauth.getPassword());
        } else {
            oauthAccessToken = getOAuth().getOAuthAccessToken();
        }
        this.screenName = oauthAccessToken.getScreenName();
        this.id = oauthAccessToken.getUserId();
        return oauthAccessToken;
    }

    public synchronized AccessToken getOAuthAccessToken(String oauthVerifier) throws TwitterException {
        AccessToken oauthAccessToken;
        oauthAccessToken = getOAuth().getOAuthAccessToken(oauthVerifier);
        this.screenName = oauthAccessToken.getScreenName();
        return oauthAccessToken;
    }

    public synchronized AccessToken getOAuthAccessToken(RequestToken requestToken) throws TwitterException {
        AccessToken oauthAccessToken;
        oauthAccessToken = getOAuth().getOAuthAccessToken(requestToken);
        this.screenName = oauthAccessToken.getScreenName();
        return oauthAccessToken;
    }

    public synchronized AccessToken getOAuthAccessToken(RequestToken requestToken, String oauthVerifier) throws TwitterException {
        return getOAuth().getOAuthAccessToken(requestToken, oauthVerifier);
    }

    public synchronized void setOAuthAccessToken(AccessToken accessToken) {
        getOAuth().setOAuthAccessToken(accessToken);
    }

    public synchronized AccessToken getOAuthAccessToken(String token, String tokenSecret) throws TwitterException {
        return getOAuth().getOAuthAccessToken(new RequestToken(token, tokenSecret));
    }

    public synchronized AccessToken getOAuthAccessToken(String token, String tokenSecret, String pin) throws TwitterException {
        return getOAuthAccessToken(new RequestToken(token, tokenSecret), pin);
    }

    public void setOAuthAccessToken(String token, String tokenSecret) {
        getOAuth().setOAuthAccessToken(new AccessToken(token, tokenSecret));
    }

    public boolean isOAuthEnabled() {
        return (this.auth instanceof OAuthAuthorization) && this.auth.isEnabled();
    }

    private OAuthSupport getOAuth() {
        if (this.auth instanceof OAuthSupport) {
            return (OAuthSupport) this.auth;
        }
        throw new IllegalStateException("OAuth consumer key/secret combination not supplied");
    }

    public synchronized void setOAuthConsumer(String consumerKey, String consumerSecret) {
        if (consumerKey == null) {
            throw new NullPointerException("consumer key is null");
        } else if (consumerSecret == null) {
            throw new NullPointerException("consumer secret is null");
        } else if (this.auth instanceof NullAuthorization) {
            this.auth = new OAuthAuthorization(this.conf, consumerKey, consumerSecret);
        } else if (this.auth instanceof BasicAuthorization) {
            XAuthAuthorization xauth = new XAuthAuthorization((BasicAuthorization) this.auth);
            xauth.setOAuthConsumer(consumerKey, consumerSecret);
            this.auth = xauth;
        } else if (this.auth instanceof OAuthAuthorization) {
            throw new IllegalStateException("consumer key/secret pair already set.");
        }
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        TwitterOAuthSupportBaseImpl that = (TwitterOAuthSupportBaseImpl) o;
        if (this.id != that.id) {
            return false;
        }
        return this.screenName == null ? that.screenName == null : this.screenName.equals(that.screenName);
    }

    public int hashCode() {
        return (((super.hashCode() * 31) + (this.screenName != null ? this.screenName.hashCode() : 0)) * 31) + this.id;
    }
}
