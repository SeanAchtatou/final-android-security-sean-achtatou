package twitter4j;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import twitter4j.ProfileImage;
import twitter4j.api.AccountMethods;
import twitter4j.api.BlockMethods;
import twitter4j.api.DirectMessageMethods;
import twitter4j.api.FavoriteMethods;
import twitter4j.api.FriendsFollowersMethods;
import twitter4j.api.FriendshipMethods;
import twitter4j.api.GeoMethods;
import twitter4j.api.HelpMethods;
import twitter4j.api.LegalResources;
import twitter4j.api.ListMembersMethods;
import twitter4j.api.ListMethods;
import twitter4j.api.ListSubscribersMethods;
import twitter4j.api.LocalTrendsMethods;
import twitter4j.api.NewTwitterMethods;
import twitter4j.api.NotificationMethods;
import twitter4j.api.SavedSearchesMethods;
import twitter4j.api.SearchMethods;
import twitter4j.api.SpamReportingMethods;
import twitter4j.api.StatusMethods;
import twitter4j.api.TimelineMethods;
import twitter4j.api.TrendsMethods;
import twitter4j.api.UserMethods;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationContext;
import twitter4j.http.AccessToken;
import twitter4j.http.Authorization;
import twitter4j.http.AuthorizationFactory;
import twitter4j.http.BasicAuthorization;
import twitter4j.http.RequestToken;
import twitter4j.internal.http.HttpParameter;
import twitter4j.internal.http.HttpResponse;
import twitter4j.internal.http.HttpResponseEvent;
import twitter4j.internal.org.json.JSONException;
import twitter4j.internal.util.StringUtil;

public class Twitter extends TwitterOAuthSupportBaseImpl implements Serializable, SearchMethods, TrendsMethods, TimelineMethods, StatusMethods, UserMethods, ListMethods, ListMembersMethods, ListSubscribersMethods, DirectMessageMethods, FriendshipMethods, FriendsFollowersMethods, AccountMethods, FavoriteMethods, NotificationMethods, BlockMethods, SpamReportingMethods, SavedSearchesMethods, LocalTrendsMethods, GeoMethods, LegalResources, NewTwitterMethods, HelpMethods {
    private static final long serialVersionUID = -1486360080128882436L;
    private final HttpParameter INCLUDE_ENTITIES;
    private final HttpParameter INCLUDE_RTS;

    public boolean equals(Object x0) {
        return super.equals(x0);
    }

    public Configuration getConfiguration() {
        return super.getConfiguration();
    }

    public AccessToken getOAuthAccessToken() throws TwitterException {
        return super.getOAuthAccessToken();
    }

    public AccessToken getOAuthAccessToken(String x0) throws TwitterException {
        return super.getOAuthAccessToken(x0);
    }

    public AccessToken getOAuthAccessToken(String x0, String x1) throws TwitterException {
        return super.getOAuthAccessToken(x0, x1);
    }

    public AccessToken getOAuthAccessToken(String x0, String x1, String x2) throws TwitterException {
        return super.getOAuthAccessToken(x0, x1, x2);
    }

    public AccessToken getOAuthAccessToken(RequestToken x0) throws TwitterException {
        return super.getOAuthAccessToken(x0);
    }

    public AccessToken getOAuthAccessToken(RequestToken x0, String x1) throws TwitterException {
        return super.getOAuthAccessToken(x0, x1);
    }

    public RequestToken getOAuthRequestToken() throws TwitterException {
        return super.getOAuthRequestToken();
    }

    public RequestToken getOAuthRequestToken(String x0) throws TwitterException {
        return super.getOAuthRequestToken(x0);
    }

    public int hashCode() {
        return super.hashCode();
    }

    public void httpResponseReceived(HttpResponseEvent x0) {
        super.httpResponseReceived(x0);
    }

    public boolean isOAuthEnabled() {
        return super.isOAuthEnabled();
    }

    public void setOAuthAccessToken(String x0, String x1) {
        super.setOAuthAccessToken(x0, x1);
    }

    public void setOAuthAccessToken(AccessToken x0) {
        super.setOAuthAccessToken(x0);
    }

    public void setOAuthConsumer(String x0, String x1) {
        super.setOAuthConsumer(x0, x1);
    }

    public void setRateLimitStatusListener(RateLimitStatusListener x0) {
        super.setRateLimitStatusListener(x0);
    }

    public void shutdown() {
        super.shutdown();
    }

    Twitter(Configuration conf) {
        this(conf, AuthorizationFactory.getInstance(conf, true));
    }

    public Twitter() {
        super(ConfigurationContext.getInstance());
        this.INCLUDE_ENTITIES = new HttpParameter("include_entities", ConfigurationContext.getInstance().isIncludeEntitiesEnabled());
        this.INCLUDE_RTS = new HttpParameter("include_rts", this.conf.isIncludeRTsEnabled());
    }

    public Twitter(String screenName, String password) {
        super(ConfigurationContext.getInstance(), screenName, password);
        this.INCLUDE_ENTITIES = new HttpParameter("include_entities", ConfigurationContext.getInstance().isIncludeEntitiesEnabled());
        this.INCLUDE_RTS = new HttpParameter("include_rts", this.conf.isIncludeRTsEnabled());
    }

    Twitter(Configuration conf, String screenName, String password) {
        super(conf, screenName, password);
        this.INCLUDE_ENTITIES = new HttpParameter("include_entities", conf.isIncludeEntitiesEnabled());
        this.INCLUDE_RTS = new HttpParameter("include_rts", conf.isIncludeRTsEnabled());
    }

    Twitter(Configuration conf, Authorization auth) {
        super(conf, auth);
        this.INCLUDE_ENTITIES = new HttpParameter("include_entities", conf.isIncludeEntitiesEnabled());
        this.INCLUDE_RTS = new HttpParameter("include_rts", conf.isIncludeRTsEnabled());
    }

    private HttpParameter[] mergeParameters(HttpParameter[] params1, HttpParameter[] params2) {
        if (params1 != null && params2 != null) {
            HttpParameter[] params = new HttpParameter[(params1.length + params2.length)];
            System.arraycopy(params1, 0, params, 0, params1.length);
            System.arraycopy(params2, 0, params, params1.length, params2.length);
            return params;
        } else if (params1 == null && params2 == null) {
            return new HttpParameter[0];
        } else {
            if (params1 != null) {
                return params1;
            }
            return params2;
        }
    }

    private HttpParameter[] mergeParameters(HttpParameter[] params1, HttpParameter params2) {
        if (params1 != null && params2 != null) {
            HttpParameter[] params = new HttpParameter[(params1.length + 1)];
            System.arraycopy(params1, 0, params, 0, params1.length);
            params[params.length - 1] = params2;
            return params;
        } else if (params1 == null && params2 == null) {
            return new HttpParameter[0];
        } else {
            if (params1 != null) {
                return params1;
            }
            return new HttpParameter[]{params2};
        }
    }

    public String getScreenName() throws TwitterException, IllegalStateException {
        if (!this.auth.isEnabled()) {
            throw new IllegalStateException("Neither user ID/password combination nor OAuth consumer key/secret combination supplied");
        }
        if (this.screenName == null) {
            if (this.auth instanceof BasicAuthorization) {
                this.screenName = ((BasicAuthorization) this.auth).getUserId();
                if (-1 != this.screenName.indexOf("@")) {
                    this.screenName = null;
                }
            }
            if (this.screenName == null) {
                verifyCredentials();
            }
        }
        return this.screenName;
    }

    public int getId() throws TwitterException, IllegalStateException {
        if (!this.auth.isEnabled()) {
            throw new IllegalStateException("Neither user ID/password combination nor OAuth consumer key/secret combination supplied");
        }
        if (this.id == 0) {
            verifyCredentials();
        }
        return this.id;
    }

    public QueryResult search(Query query) throws TwitterException {
        try {
            return new QueryResultJSONImpl(this.http.get(new StringBuffer().append(this.conf.getSearchBaseURL()).append("search.json").toString(), query.asHttpParameterArray(), null));
        } catch (TwitterException e) {
            TwitterException te = e;
            if (404 == te.getStatusCode()) {
                return new QueryResultJSONImpl(query);
            }
            throw te;
        }
    }

    public Trends getTrends() throws TwitterException {
        return new TrendsJSONImpl(this.http.get(new StringBuffer().append(this.conf.getSearchBaseURL()).append("trends.json").toString()));
    }

    public Trends getCurrentTrends() throws TwitterException {
        return TrendsJSONImpl.createTrendsList(this.http.get(new StringBuffer().append(this.conf.getSearchBaseURL()).append("trends/current.json").toString())).get(0);
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public Trends getCurrentTrends(boolean excludeHashTags) throws TwitterException {
        return TrendsJSONImpl.createTrendsList(this.http.get(new StringBuffer().append(this.conf.getSearchBaseURL()).append("trends/current.json").append(excludeHashTags ? "?exclude=hashtags" : "").toString())).get(0);
    }

    public List<Trends> getDailyTrends() throws TwitterException {
        return TrendsJSONImpl.createTrendsList(this.http.get(new StringBuffer().append(this.conf.getSearchBaseURL()).append("trends/daily.json").toString()));
    }

    public List<Trends> getDailyTrends(Date date, boolean excludeHashTags) throws TwitterException {
        return TrendsJSONImpl.createTrendsList(this.http.get(new StringBuffer().append(this.conf.getSearchBaseURL()).append("trends/daily.json?date=").append(toDateStr(date)).append(excludeHashTags ? "&exclude=hashtags" : "").toString()));
    }

    private String toDateStr(Date date) {
        if (date == null) {
            date = new Date();
        }
        return new SimpleDateFormat("yyyy-MM-dd").format(date);
    }

    public List<Trends> getWeeklyTrends() throws TwitterException {
        return TrendsJSONImpl.createTrendsList(this.http.get(new StringBuffer().append(this.conf.getSearchBaseURL()).append("trends/weekly.json").toString()));
    }

    public List<Trends> getWeeklyTrends(Date date, boolean excludeHashTags) throws TwitterException {
        return TrendsJSONImpl.createTrendsList(this.http.get(new StringBuffer().append(this.conf.getSearchBaseURL()).append("trends/weekly.json?date=").append(toDateStr(date)).append(excludeHashTags ? "&exclude=hashtags" : "").toString()));
    }

    public ResponseList<Status> getPublicTimeline() throws TwitterException {
        return StatusJSONImpl.createStatusList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("statuses/public_timeline.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).append("&include_rts=").append(this.conf.isIncludeRTsEnabled()).toString(), this.auth));
    }

    public ResponseList<Status> getHomeTimeline() throws TwitterException {
        ensureAuthorizationEnabled();
        return StatusJSONImpl.createStatusList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("statuses/home_timeline.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).toString(), this.auth));
    }

    public ResponseList<Status> getHomeTimeline(Paging paging) throws TwitterException {
        ensureAuthorizationEnabled();
        return StatusJSONImpl.createStatusList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("statuses/home_timeline.json").toString(), mergeParameters(paging.asPostParameterArray(), this.INCLUDE_ENTITIES), this.auth));
    }

    public ResponseList<Status> getFriendsTimeline() throws TwitterException {
        ensureAuthorizationEnabled();
        return StatusJSONImpl.createStatusList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("statuses/friends_timeline.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).append("&include_rts=").append(this.conf.isIncludeRTsEnabled()).toString(), this.auth));
    }

    public ResponseList<Status> getFriendsTimeline(Paging paging) throws TwitterException {
        ensureAuthorizationEnabled();
        return StatusJSONImpl.createStatusList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("statuses/friends_timeline.json").toString(), mergeParameters(new HttpParameter[]{this.INCLUDE_RTS, this.INCLUDE_ENTITIES}, paging.asPostParameterArray()), this.auth));
    }

    public ResponseList<Status> getUserTimeline(String screenName, Paging paging) throws TwitterException {
        return StatusJSONImpl.createStatusList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("statuses/user_timeline.json").toString(), mergeParameters(new HttpParameter[]{new HttpParameter("screen_name", screenName), this.INCLUDE_RTS, this.INCLUDE_ENTITIES}, paging.asPostParameterArray()), this.auth));
    }

    public ResponseList<Status> getUserTimeline(int userId, Paging paging) throws TwitterException {
        return StatusJSONImpl.createStatusList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("statuses/user_timeline.json").toString(), mergeParameters(new HttpParameter[]{new HttpParameter("user_id", userId), this.INCLUDE_RTS, this.INCLUDE_ENTITIES}, paging.asPostParameterArray()), this.auth));
    }

    public ResponseList<Status> getUserTimeline(String screenName) throws TwitterException {
        return getUserTimeline(screenName, new Paging());
    }

    public ResponseList<Status> getUserTimeline(int userId) throws TwitterException {
        return getUserTimeline(userId, new Paging());
    }

    public ResponseList<Status> getUserTimeline() throws TwitterException {
        return getUserTimeline(new Paging());
    }

    public ResponseList<Status> getUserTimeline(Paging paging) throws TwitterException {
        ensureAuthorizationEnabled();
        return StatusJSONImpl.createStatusList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("statuses/user_timeline.json").toString(), mergeParameters(new HttpParameter[]{this.INCLUDE_RTS, this.INCLUDE_ENTITIES}, paging.asPostParameterArray()), this.auth));
    }

    public ResponseList<Status> getMentions() throws TwitterException {
        ensureAuthorizationEnabled();
        return StatusJSONImpl.createStatusList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("statuses/mentions.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).append("&include_rts=").append(this.conf.isIncludeRTsEnabled()).toString(), this.auth));
    }

    public ResponseList<Status> getMentions(Paging paging) throws TwitterException {
        ensureAuthorizationEnabled();
        return StatusJSONImpl.createStatusList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("statuses/mentions.json").toString(), mergeParameters(new HttpParameter[]{this.INCLUDE_RTS, this.INCLUDE_ENTITIES}, paging.asPostParameterArray()), this.auth));
    }

    public ResponseList<Status> getRetweetedByMe() throws TwitterException {
        ensureAuthorizationEnabled();
        return StatusJSONImpl.createStatusList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("statuses/retweeted_by_me.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).toString(), this.auth));
    }

    public ResponseList<Status> getRetweetedByMe(Paging paging) throws TwitterException {
        ensureAuthorizationEnabled();
        return StatusJSONImpl.createStatusList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("statuses/retweeted_by_me.json").toString(), mergeParameters(paging.asPostParameterArray(), this.INCLUDE_ENTITIES), this.auth));
    }

    public ResponseList<Status> getRetweetedToMe() throws TwitterException {
        ensureAuthorizationEnabled();
        return StatusJSONImpl.createStatusList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("statuses/retweeted_to_me.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).toString(), this.auth));
    }

    public ResponseList<Status> getRetweetedToMe(Paging paging) throws TwitterException {
        ensureAuthorizationEnabled();
        return StatusJSONImpl.createStatusList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("statuses/retweeted_to_me.json").toString(), mergeParameters(paging.asPostParameterArray(), this.INCLUDE_ENTITIES), this.auth));
    }

    public ResponseList<Status> getRetweetsOfMe() throws TwitterException {
        ensureAuthorizationEnabled();
        return StatusJSONImpl.createStatusList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("statuses/retweets_of_me.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).toString(), this.auth));
    }

    public ResponseList<Status> getRetweetsOfMe(Paging paging) throws TwitterException {
        ensureAuthorizationEnabled();
        return StatusJSONImpl.createStatusList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("statuses/retweets_of_me.json").toString(), mergeParameters(paging.asPostParameterArray(), this.INCLUDE_ENTITIES), this.auth));
    }

    public ResponseList<Status> getRetweetedToUser(String screenName, Paging paging) throws TwitterException {
        return StatusJSONImpl.createStatusList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("statuses/retweeted_to_user.json").toString(), mergeParameters(paging.asPostParameterArray(), new HttpParameter[]{new HttpParameter("screen_name", screenName), this.INCLUDE_ENTITIES}), this.auth));
    }

    public ResponseList<Status> getRetweetedToUser(int userId, Paging paging) throws TwitterException {
        return StatusJSONImpl.createStatusList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("statuses/retweeted_to_user.json").toString(), mergeParameters(paging.asPostParameterArray(), new HttpParameter[]{new HttpParameter("user_id", userId), this.INCLUDE_ENTITIES}), this.auth));
    }

    public ResponseList<Status> getRetweetedByUser(String screenName, Paging paging) throws TwitterException {
        return StatusJSONImpl.createStatusList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("statuses/retweeted_by_user.json").toString(), mergeParameters(paging.asPostParameterArray(), new HttpParameter[]{new HttpParameter("screen_name", screenName), this.INCLUDE_ENTITIES}), this.auth));
    }

    public ResponseList<Status> getRetweetedByUser(int userId, Paging paging) throws TwitterException {
        return StatusJSONImpl.createStatusList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("statuses/retweeted_by_user.json").toString(), mergeParameters(paging.asPostParameterArray(), new HttpParameter[]{new HttpParameter("user_id", userId), this.INCLUDE_ENTITIES}), this.auth));
    }

    public ResponseList<User> getRetweetedBy(long statusId) throws TwitterException {
        ensureAuthorizationEnabled();
        return UserJSONImpl.createUserList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("statuses/").append(statusId).append("/retweeted_by.json?count=100&include_entities").append(this.conf.isIncludeEntitiesEnabled()).toString(), this.auth));
    }

    public ResponseList<User> getRetweetedBy(long statusId, Paging paging) throws TwitterException {
        ensureAuthorizationEnabled();
        return UserJSONImpl.createUserList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("statuses/").append(statusId).append("/retweeted_by.json").toString(), mergeParameters(paging.asPostParameterArray(), this.INCLUDE_ENTITIES), this.auth));
    }

    public IDs getRetweetedByIDs(long statusId) throws TwitterException {
        ensureAuthorizationEnabled();
        return new IDsJSONImpl(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("statuses/").append(statusId).append("/retweeted_by/ids.json?count=100&include_entities").append(this.conf.isIncludeEntitiesEnabled()).toString(), this.auth));
    }

    public IDs getRetweetedByIDs(long statusId, Paging paging) throws TwitterException {
        ensureAuthorizationEnabled();
        return new IDsJSONImpl(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("statuses/").append(statusId).append("/retweeted_by/ids.json").toString(), mergeParameters(paging.asPostParameterArray(), this.INCLUDE_ENTITIES), this.auth));
    }

    public Status showStatus(long id) throws TwitterException {
        return new StatusJSONImpl(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("statuses/show/").append(id).append(".json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).toString(), this.auth));
    }

    public Status updateStatus(String status) throws TwitterException {
        ensureAuthorizationEnabled();
        return new StatusJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append("statuses/update.json").toString(), new HttpParameter[]{new HttpParameter("status", status), this.INCLUDE_ENTITIES}, this.auth));
    }

    public Status updateStatus(String status, GeoLocation location) throws TwitterException {
        ensureAuthorizationEnabled();
        return new StatusJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append("statuses/update.json").toString(), new HttpParameter[]{new HttpParameter("status", status), new HttpParameter("lat", location.getLatitude()), new HttpParameter("long", location.getLongitude()), this.INCLUDE_ENTITIES}, this.auth));
    }

    public Status updateStatus(String status, long inReplyToStatusId) throws TwitterException {
        ensureAuthorizationEnabled();
        return new StatusJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append("statuses/update.json").toString(), new HttpParameter[]{new HttpParameter("status", status), new HttpParameter("in_reply_to_status_id", inReplyToStatusId), this.INCLUDE_ENTITIES}, this.auth));
    }

    public Status updateStatus(String status, long inReplyToStatusId, GeoLocation location) throws TwitterException {
        ensureAuthorizationEnabled();
        return new StatusJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append("statuses/update.json").toString(), new HttpParameter[]{new HttpParameter("status", status), new HttpParameter("lat", location.getLatitude()), new HttpParameter("long", location.getLongitude()), new HttpParameter("in_reply_to_status_id", inReplyToStatusId), this.INCLUDE_ENTITIES}, this.auth));
    }

    public Status updateStatus(StatusUpdate latestStatus) throws TwitterException {
        ensureAuthorizationEnabled();
        return new StatusJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append("statuses/update.json").toString(), mergeParameters(latestStatus.asHttpParameterArray(), this.INCLUDE_ENTITIES), this.auth));
    }

    public Status destroyStatus(long statusId) throws TwitterException {
        ensureAuthorizationEnabled();
        return new StatusJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append("statuses/destroy/").append(statusId).append(".json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).toString(), this.auth));
    }

    public Status retweetStatus(long statusId) throws TwitterException {
        ensureAuthorizationEnabled();
        return new StatusJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append("statuses/retweet/").append(statusId).append(".json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).toString(), this.auth));
    }

    public ResponseList<Status> getRetweets(long statusId) throws TwitterException {
        ensureAuthorizationEnabled();
        return StatusJSONImpl.createStatusList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("statuses/retweets/").append(statusId).append(".json?count=100&include_entities=").append(this.conf.isIncludeEntitiesEnabled()).toString(), this.auth));
    }

    public User showUser(String screenName) throws TwitterException {
        return new UserJSONImpl(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("users/show.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).append("&screen_name=").append(screenName).toString(), this.auth));
    }

    public User showUser(int userId) throws TwitterException {
        return new UserJSONImpl(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("users/show.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).append("&user_id=").append(userId).toString(), this.auth));
    }

    public ResponseList<User> lookupUsers(String[] screenNames) throws TwitterException {
        ensureAuthorizationEnabled();
        return UserJSONImpl.createUserList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("users/lookup.json").toString(), new HttpParameter[]{new HttpParameter("screen_name", toCommaSeparatedString(screenNames)), this.INCLUDE_ENTITIES}, this.auth));
    }

    public ResponseList<User> lookupUsers(int[] ids) throws TwitterException {
        ensureAuthorizationEnabled();
        return UserJSONImpl.createUserList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("users/lookup.json").toString(), new HttpParameter[]{new HttpParameter("user_id", toCommaSeparatedString(ids)), this.INCLUDE_ENTITIES}, this.auth));
    }

    public ResponseList<User> searchUsers(String query, int page) throws TwitterException {
        ensureAuthorizationEnabled();
        return UserJSONImpl.createUserList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("users/search.json").toString(), new HttpParameter[]{new HttpParameter("q", query), new HttpParameter("per_page", 20), new HttpParameter("page", page), this.INCLUDE_ENTITIES}, this.auth));
    }

    public ResponseList<Category> getSuggestedUserCategories() throws TwitterException {
        return CategoryJSONImpl.createCategoriesList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("users/suggestions.json").toString(), this.auth));
    }

    public ResponseList<User> getUserSuggestions(String categorySlug) throws TwitterException {
        HttpResponse res = this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("users/suggestions/").append(categorySlug).append(".json").toString(), this.auth);
        try {
            return UserJSONImpl.createUserList(res.asJSONObject().getJSONArray("users"), res);
        } catch (JSONException e) {
            throw new TwitterException(e);
        }
    }

    public ResponseList<User> getMemberSuggestions(String categorySlug) throws TwitterException {
        HttpResponse res = this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("users/suggestions/").append(categorySlug).append("/members.json").toString(), this.auth);
        return UserJSONImpl.createUserList(res.asJSONArray(), res);
    }

    public ProfileImage getProfileImage(String screenName, ProfileImage.ImageSize size) throws TwitterException {
        return new ProfileImageImpl(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("users/profile_image/").append(screenName).append(".json?size=").append(size.getName()).toString(), this.auth));
    }

    public PagableResponseList<User> getFriendsStatuses() throws TwitterException {
        return getFriendsStatuses(-1L);
    }

    public PagableResponseList<User> getFriendsStatuses(long cursor) throws TwitterException {
        return UserJSONImpl.createPagableUserList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("statuses/friends.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).append("&cursor=").append(cursor).toString(), this.auth));
    }

    public PagableResponseList<User> getFriendsStatuses(String screenName) throws TwitterException {
        return getFriendsStatuses(screenName, -1);
    }

    public PagableResponseList<User> getFriendsStatuses(int userId) throws TwitterException {
        return getFriendsStatuses(userId, -1);
    }

    public PagableResponseList<User> getFriendsStatuses(String screenName, long cursor) throws TwitterException {
        return UserJSONImpl.createPagableUserList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("statuses/friends.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).append("&screen_name=").append(screenName).append("&cursor=").append(cursor).toString(), this.auth));
    }

    public PagableResponseList<User> getFriendsStatuses(int userId, long cursor) throws TwitterException {
        return UserJSONImpl.createPagableUserList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("statuses/friends.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).append("&user_id=").append(userId).append("&cursor=").append(cursor).toString(), null, this.auth));
    }

    public PagableResponseList<User> getFollowersStatuses() throws TwitterException {
        return getFollowersStatuses(-1L);
    }

    public PagableResponseList<User> getFollowersStatuses(long cursor) throws TwitterException {
        return UserJSONImpl.createPagableUserList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("statuses/followers.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).append("&cursor=").append(cursor).toString(), this.auth));
    }

    public PagableResponseList<User> getFollowersStatuses(String screenName) throws TwitterException {
        return getFollowersStatuses(screenName, -1);
    }

    public PagableResponseList<User> getFollowersStatuses(int userId) throws TwitterException {
        return getFollowersStatuses(userId, -1);
    }

    public PagableResponseList<User> getFollowersStatuses(String screenName, long cursor) throws TwitterException {
        return UserJSONImpl.createPagableUserList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("statuses/followers.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).append("&screen_name=").append(screenName).append("&cursor=").append(cursor).toString(), this.auth));
    }

    public PagableResponseList<User> getFollowersStatuses(int userId, long cursor) throws TwitterException {
        return UserJSONImpl.createPagableUserList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("statuses/followers.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).append("&user_id=").append(userId).append("&cursor=").append(cursor).toString(), this.auth));
    }

    public UserList createUserList(String listName, boolean isPublicList, String description) throws TwitterException {
        ensureAuthorizationEnabled();
        List<HttpParameter> httpParams = new ArrayList<>();
        httpParams.add(new HttpParameter("name", listName));
        httpParams.add(new HttpParameter("mode", isPublicList ? "public" : "private"));
        if (description != null) {
            httpParams.add(new HttpParameter("description", description));
        }
        return new UserListJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append(getScreenName()).append("/lists.json").toString(), (HttpParameter[]) httpParams.toArray(new HttpParameter[httpParams.size()]), this.auth));
    }

    public UserList updateUserList(int listId, String newListName, boolean isPublicList, String newDescription) throws TwitterException {
        ensureAuthorizationEnabled();
        List<HttpParameter> httpParams = new ArrayList<>();
        if (newListName != null) {
            httpParams.add(new HttpParameter("name", newListName));
        }
        httpParams.add(new HttpParameter("mode", isPublicList ? "public" : "private"));
        if (newDescription != null) {
            httpParams.add(new HttpParameter("description", newDescription));
        }
        return new UserListJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append(getScreenName()).append("/lists/").append(listId).append(".json").toString(), (HttpParameter[]) httpParams.toArray(new HttpParameter[httpParams.size()]), this.auth));
    }

    public PagableResponseList<UserList> getUserLists(String listOwnerScreenName, long cursor) throws TwitterException {
        ensureAuthorizationEnabled();
        return UserListJSONImpl.createPagableUserListList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append(listOwnerScreenName).append("/lists.json?cursor=").append(cursor).toString(), this.auth));
    }

    public UserList showUserList(String listOwnerScreenName, int id) throws TwitterException {
        ensureAuthorizationEnabled();
        return new UserListJSONImpl(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append(listOwnerScreenName).append("/lists/").append(id).append(".json").toString(), this.auth));
    }

    public UserList destroyUserList(int listId) throws TwitterException {
        ensureAuthorizationEnabled();
        return new UserListJSONImpl(this.http.delete(new StringBuffer().append(this.conf.getRestBaseURL()).append(getScreenName()).append("/lists/").append(listId).append(".json").toString(), this.auth));
    }

    public ResponseList<Status> getUserListStatuses(String listOwnerScreenName, int id, Paging paging) throws TwitterException {
        return StatusJSONImpl.createStatusList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append(listOwnerScreenName).append("/lists/").append(id).append("/statuses.json").toString(), mergeParameters(paging.asPostParameterArray(Paging.SMCP, "per_page"), this.INCLUDE_ENTITIES), this.auth));
    }

    public ResponseList<Status> getUserListStatuses(int listOwnerId, int id, Paging paging) throws TwitterException {
        return StatusJSONImpl.createStatusList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append(listOwnerId).append("/lists/").append(id).append("/statuses.json").toString(), mergeParameters(paging.asPostParameterArray(Paging.SMCP, "per_page"), this.INCLUDE_ENTITIES), this.auth));
    }

    public PagableResponseList<UserList> getUserListMemberships(String listMemberScreenName, long cursor) throws TwitterException {
        ensureAuthorizationEnabled();
        return UserListJSONImpl.createPagableUserListList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append(listMemberScreenName).append("/lists/memberships.json?cursor=").append(cursor).toString(), this.auth));
    }

    public PagableResponseList<UserList> getUserListSubscriptions(String listOwnerScreenName, long cursor) throws TwitterException {
        ensureAuthorizationEnabled();
        return UserListJSONImpl.createPagableUserListList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append(listOwnerScreenName).append("/lists/subscriptions.json?cursor=").append(cursor).toString(), this.auth));
    }

    public ResponseList<UserList> getAllUserLists(String screenName) throws TwitterException {
        return UserListJSONImpl.createUserListList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("lists/all.json?screen_name=").append(screenName).toString(), this.auth));
    }

    public ResponseList<UserList> getAllUserLists(int userId) throws TwitterException {
        return UserListJSONImpl.createUserListList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("lists/all.json?user_id=").append(userId).toString(), this.auth));
    }

    public PagableResponseList<User> getUserListMembers(String listOwnerScreenName, int listId, long cursor) throws TwitterException {
        ensureAuthorizationEnabled();
        return UserJSONImpl.createPagableUserList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append(listOwnerScreenName).append("/").append(listId).append("/members.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).append("&cursor=").append(cursor).toString(), this.auth));
    }

    public PagableResponseList<User> getUserListMembers(int listOwnerId, int listId, long cursor) throws TwitterException {
        ensureAuthorizationEnabled();
        return UserJSONImpl.createPagableUserList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append(listOwnerId).append("/").append(listId).append("/members.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).append("&cursor=").append(cursor).toString(), this.auth));
    }

    public UserList addUserListMember(int listId, int userId) throws TwitterException {
        ensureAuthorizationEnabled();
        return new UserListJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append(getScreenName()).append("/").append(listId).append("/members.json?id=").append(userId).toString(), this.auth));
    }

    public UserList addUserListMembers(int listId, int[] userIds) throws TwitterException {
        ensureAuthorizationEnabled();
        return new UserListJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append(getScreenName()).append("/").append(listId).append("/members/create_all.json?user_id=").append(toCommaSeparatedString(userIds)).toString(), this.auth));
    }

    public UserList addUserListMembers(int listId, String[] screenNames) throws TwitterException {
        ensureAuthorizationEnabled();
        return new UserListJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append(getScreenName()).append("/").append(listId).append("/members/create_all.json?screen_name=").append(toCommaSeparatedString(screenNames)).toString(), this.auth));
    }

    public UserList deleteUserListMember(int listId, int userId) throws TwitterException {
        ensureAuthorizationEnabled();
        return new UserListJSONImpl(this.http.delete(new StringBuffer().append(this.conf.getRestBaseURL()).append(getScreenName()).append("/").append(listId).append("/members.json?id=").append(userId).toString(), this.auth));
    }

    public User checkUserListMembership(String listOwnerScreenName, int listId, int userId) throws TwitterException {
        ensureAuthorizationEnabled();
        return new UserJSONImpl(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append(listOwnerScreenName).append("/").append(listId).append("/members/").append(userId).append(".json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).toString(), this.auth));
    }

    public PagableResponseList<User> getUserListSubscribers(String listOwnerScreenName, int listId, long cursor) throws TwitterException {
        ensureAuthorizationEnabled();
        return UserJSONImpl.createPagableUserList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append(listOwnerScreenName).append("/").append(listId).append("/subscribers.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).append("&cursor=").append(cursor).toString(), this.auth));
    }

    public UserList subscribeUserList(String listOwnerScreenName, int listId) throws TwitterException {
        ensureAuthorizationEnabled();
        return new UserListJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append(listOwnerScreenName).append("/").append(listId).append("/subscribers.json").toString(), this.auth));
    }

    public UserList unsubscribeUserList(String listOwnerScreenName, int listId) throws TwitterException {
        ensureAuthorizationEnabled();
        return new UserListJSONImpl(this.http.delete(new StringBuffer().append(this.conf.getRestBaseURL()).append(listOwnerScreenName).append("/").append(listId).append("/subscribers.json?id=").append(verifyCredentials().getId()).toString(), this.auth));
    }

    public User checkUserListSubscription(String listOwnerScreenName, int listId, int userId) throws TwitterException {
        ensureAuthorizationEnabled();
        return new UserJSONImpl(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append(listOwnerScreenName).append("/").append(listId).append("/subscribers/").append(userId).append(".json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).toString(), this.auth));
    }

    public ResponseList<DirectMessage> getDirectMessages() throws TwitterException {
        ensureAuthorizationEnabled();
        return DirectMessageJSONImpl.createDirectMessageList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("direct_messages.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).toString(), this.auth));
    }

    public ResponseList<DirectMessage> getDirectMessages(Paging paging) throws TwitterException {
        ensureAuthorizationEnabled();
        return DirectMessageJSONImpl.createDirectMessageList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("direct_messages.json").toString(), mergeParameters(paging.asPostParameterArray(), this.INCLUDE_ENTITIES), this.auth));
    }

    public ResponseList<DirectMessage> getSentDirectMessages() throws TwitterException {
        ensureAuthorizationEnabled();
        return DirectMessageJSONImpl.createDirectMessageList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("direct_messages/sent.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).toString(), this.auth));
    }

    public ResponseList<DirectMessage> getSentDirectMessages(Paging paging) throws TwitterException {
        ensureAuthorizationEnabled();
        return DirectMessageJSONImpl.createDirectMessageList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("direct_messages/sent.json").toString(), mergeParameters(paging.asPostParameterArray(), this.INCLUDE_ENTITIES), this.auth));
    }

    public DirectMessage sendDirectMessage(String screenName, String text) throws TwitterException {
        ensureAuthorizationEnabled();
        return new DirectMessageJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append("direct_messages/new.json").toString(), new HttpParameter[]{new HttpParameter("screen_name", screenName), new HttpParameter("text", text), this.INCLUDE_ENTITIES}, this.auth));
    }

    public DirectMessage sendDirectMessage(int userId, String text) throws TwitterException {
        ensureAuthorizationEnabled();
        return new DirectMessageJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append("direct_messages/new.json").toString(), new HttpParameter[]{new HttpParameter("user_id", userId), new HttpParameter("text", text), this.INCLUDE_ENTITIES}, this.auth));
    }

    public DirectMessage destroyDirectMessage(long id) throws TwitterException {
        ensureAuthorizationEnabled();
        return new DirectMessageJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append("direct_messages/destroy/").append(id).append(".json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).toString(), this.auth));
    }

    public DirectMessage showDirectMessage(long id) throws TwitterException {
        ensureAuthorizationEnabled();
        return new DirectMessageJSONImpl(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("direct_messages/show/").append(id).append(".json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).toString(), this.auth));
    }

    public User createFriendship(String screenName) throws TwitterException {
        ensureAuthorizationEnabled();
        return new UserJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append("friendships/create.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).append("&screen_name=").append(screenName).toString(), this.auth));
    }

    public User createFriendship(int userId) throws TwitterException {
        ensureAuthorizationEnabled();
        return new UserJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append("friendships/create.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).append("&user_id=").append(userId).toString(), this.auth));
    }

    public User createFriendship(String screenName, boolean follow) throws TwitterException {
        ensureAuthorizationEnabled();
        return new UserJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append("friendships/create.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).append("&screen_name=").append(screenName).append("&follow=").append(follow).toString(), this.auth));
    }

    public User createFriendship(int userId, boolean follow) throws TwitterException {
        ensureAuthorizationEnabled();
        return new UserJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append("friendships/create.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).append("&user_id=").append(userId).append("&follow=").append(follow).toString(), this.auth));
    }

    public User destroyFriendship(String screenName) throws TwitterException {
        ensureAuthorizationEnabled();
        return new UserJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append("friendships/destroy.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).append("&screen_name=").append(screenName).toString(), this.auth));
    }

    public User destroyFriendship(int userId) throws TwitterException {
        ensureAuthorizationEnabled();
        return new UserJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append("friendships/destroy.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).append("&user_id=").append(userId).toString(), this.auth));
    }

    public boolean existsFriendship(String userA, String userB) throws TwitterException {
        return -1 != this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("friendships/exists.json").toString(), HttpParameter.getParameterArray("user_a", userA, "user_b", userB), this.auth).asString().indexOf("true");
    }

    public Relationship showFriendship(String sourceScreenName, String targetScreenName) throws TwitterException {
        return new RelationshipJSONImpl(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("friendships/show.json").toString(), HttpParameter.getParameterArray("source_screen_name", sourceScreenName, "target_screen_name", targetScreenName), this.auth));
    }

    public Relationship showFriendship(int sourceId, int targetId) throws TwitterException {
        return new RelationshipJSONImpl(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("friendships/show.json").toString(), HttpParameter.getParameterArray("source_id", sourceId, "target_id", targetId), this.auth));
    }

    public IDs getIncomingFriendships(long cursor) throws TwitterException {
        ensureAuthorizationEnabled();
        return new IDsJSONImpl(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("friendships/incoming.json?cursor=").append(cursor).toString(), this.auth));
    }

    public IDs getOutgoingFriendships(long cursor) throws TwitterException {
        ensureAuthorizationEnabled();
        return new IDsJSONImpl(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("friendships/outgoing.json?cursor=").append(cursor).toString(), this.auth));
    }

    public ResponseList<Friendship> lookupFriendships(String[] screenNames) throws TwitterException {
        ensureAuthorizationEnabled();
        return FriendshipJSONImpl.createFriendshipList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("friendships/lookup.json?screen_name=").append(StringUtil.join(screenNames)).toString(), this.auth));
    }

    public ResponseList<Friendship> lookupFriendships(int[] ids) throws TwitterException {
        ensureAuthorizationEnabled();
        return FriendshipJSONImpl.createFriendshipList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("friendships/lookup.json?user_id=").append(StringUtil.join(ids)).toString(), this.auth));
    }

    public Relationship updateFriendship(String screenName, boolean enableDeviceNotification, boolean retweets) throws TwitterException {
        ensureAuthorizationEnabled();
        return new RelationshipJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append("friendships/update.json").toString(), new HttpParameter[]{new HttpParameter("screen_name", screenName), new HttpParameter("device", enableDeviceNotification), new HttpParameter("retweets", enableDeviceNotification)}, this.auth));
    }

    public Relationship updateFriendship(int userId, boolean enableDeviceNotification, boolean retweets) throws TwitterException {
        ensureAuthorizationEnabled();
        return new RelationshipJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append("friendships/update.json").toString(), new HttpParameter[]{new HttpParameter("user_id", userId), new HttpParameter("device", enableDeviceNotification), new HttpParameter("retweets", enableDeviceNotification)}, this.auth));
    }

    public IDs getFriendsIDs() throws TwitterException {
        return getFriendsIDs(-1L);
    }

    public IDs getFriendsIDs(long cursor) throws TwitterException {
        return new IDsJSONImpl(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("friends/ids.json?cursor=").append(cursor).toString(), this.auth));
    }

    public IDs getFriendsIDs(int userId) throws TwitterException {
        return getFriendsIDs(userId, -1);
    }

    public IDs getFriendsIDs(int userId, long cursor) throws TwitterException {
        return new IDsJSONImpl(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("friends/ids.json?user_id=").append(userId).append("&cursor=").append(cursor).toString(), this.auth));
    }

    public IDs getFriendsIDs(String screenName) throws TwitterException {
        return getFriendsIDs(screenName, -1);
    }

    public IDs getFriendsIDs(String screenName, long cursor) throws TwitterException {
        return new IDsJSONImpl(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("friends/ids.json?screen_name=").append(screenName).append("&cursor=").append(cursor).toString(), this.auth));
    }

    public IDs getFollowersIDs() throws TwitterException {
        return getFollowersIDs(-1L);
    }

    public IDs getFollowersIDs(long cursor) throws TwitterException {
        return new IDsJSONImpl(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("followers/ids.json?cursor=").append(cursor).toString(), this.auth));
    }

    public IDs getFollowersIDs(int userId) throws TwitterException {
        return getFollowersIDs(userId, -1);
    }

    public IDs getFollowersIDs(int userId, long cursor) throws TwitterException {
        return new IDsJSONImpl(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("followers/ids.json?user_id=").append(userId).append("&cursor=").append(cursor).toString(), this.auth));
    }

    public IDs getFollowersIDs(String screenName) throws TwitterException {
        return getFollowersIDs(screenName, -1);
    }

    public IDs getFollowersIDs(String screenName, long cursor) throws TwitterException {
        return new IDsJSONImpl(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("followers/ids.json?screen_name=").append(screenName).append("&cursor=").append(cursor).toString(), this.auth));
    }

    public User verifyCredentials() throws TwitterException {
        ensureAuthorizationEnabled();
        User user = new UserJSONImpl(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("account/verify_credentials.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).toString(), this.auth));
        this.screenName = user.getScreenName();
        this.id = user.getId();
        return user;
    }

    public RateLimitStatus getRateLimitStatus() throws TwitterException {
        return new RateLimitStatusJSONImpl(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("account/rate_limit_status.json").toString(), this.auth));
    }

    public User updateProfile(String name, String url, String location, String description) throws TwitterException {
        ensureAuthorizationEnabled();
        List<HttpParameter> profile = new ArrayList<>(4);
        addParameterToList(profile, "name", name);
        addParameterToList(profile, "url", url);
        addParameterToList(profile, "location", location);
        addParameterToList(profile, "description", description);
        profile.add(this.INCLUDE_ENTITIES);
        return new UserJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append("account/update_profile.json").toString(), (HttpParameter[]) profile.toArray(new HttpParameter[profile.size()]), this.auth));
    }

    public AccountTotals getAccountTotals() throws TwitterException {
        ensureAuthorizationEnabled();
        return new AccountTotalsJSONImpl(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("account/totals.json").toString(), this.auth));
    }

    public AccountSettings getAccountSettings() throws TwitterException {
        ensureAuthorizationEnabled();
        return new AccountSettingsJSONImpl(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("account/settings.json").toString(), this.auth));
    }

    public User updateProfileColors(String profileBackgroundColor, String profileTextColor, String profileLinkColor, String profileSidebarFillColor, String profileSidebarBorderColor) throws TwitterException {
        ensureAuthorizationEnabled();
        List<HttpParameter> colors = new ArrayList<>(6);
        addParameterToList(colors, "profile_background_color", profileBackgroundColor);
        addParameterToList(colors, "profile_text_color", profileTextColor);
        addParameterToList(colors, "profile_link_color", profileLinkColor);
        addParameterToList(colors, "profile_sidebar_fill_color", profileSidebarFillColor);
        addParameterToList(colors, "profile_sidebar_border_color", profileSidebarBorderColor);
        colors.add(this.INCLUDE_ENTITIES);
        return new UserJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append("account/update_profile_colors.json").toString(), (HttpParameter[]) colors.toArray(new HttpParameter[colors.size()]), this.auth));
    }

    private void addParameterToList(List<HttpParameter> colors, String paramName, String color) {
        if (color != null) {
            colors.add(new HttpParameter(paramName, color));
        }
    }

    public User updateProfileImage(File image) throws TwitterException {
        checkFileValidity(image);
        ensureAuthorizationEnabled();
        return new UserJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append("account/update_profile_image.json").toString(), new HttpParameter[]{new HttpParameter("image", image), this.INCLUDE_ENTITIES}, this.auth));
    }

    public User updateProfileImage(InputStream image) throws TwitterException {
        ensureAuthorizationEnabled();
        return new UserJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append("account/update_profile_image.json").toString(), new HttpParameter[]{new HttpParameter("image", "image", image), this.INCLUDE_ENTITIES}, this.auth));
    }

    public User updateProfileBackgroundImage(File image, boolean tile) throws TwitterException {
        ensureAuthorizationEnabled();
        checkFileValidity(image);
        return new UserJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append("account/update_profile_background_image.json").toString(), new HttpParameter[]{new HttpParameter("image", image), new HttpParameter("tile", tile), this.INCLUDE_ENTITIES}, this.auth));
    }

    public User updateProfileBackgroundImage(InputStream image, boolean tile) throws TwitterException {
        ensureAuthorizationEnabled();
        return new UserJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append("account/update_profile_background_image.json").toString(), new HttpParameter[]{new HttpParameter("image", "image", image), new HttpParameter("tile", tile), this.INCLUDE_ENTITIES}, this.auth));
    }

    private void checkFileValidity(File image) throws TwitterException {
        if (!image.exists()) {
            throw new TwitterException(new FileNotFoundException(new StringBuffer().append(image).append(" is not found.").toString()));
        } else if (!image.isFile()) {
            throw new TwitterException(new IOException(new StringBuffer().append(image).append(" is not a file.").toString()));
        }
    }

    public ResponseList<Status> getFavorites() throws TwitterException {
        ensureAuthorizationEnabled();
        return StatusJSONImpl.createStatusList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("favorites.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).toString(), this.auth));
    }

    public ResponseList<Status> getFavorites(int page) throws TwitterException {
        ensureAuthorizationEnabled();
        return StatusJSONImpl.createStatusList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("favorites.json").toString(), new HttpParameter[]{new HttpParameter("page", page), this.INCLUDE_ENTITIES}, this.auth));
    }

    public ResponseList<Status> getFavorites(String id) throws TwitterException {
        ensureAuthorizationEnabled();
        return StatusJSONImpl.createStatusList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("favorites/").append(id).append(".json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).toString(), this.auth));
    }

    public ResponseList<Status> getFavorites(String id, int page) throws TwitterException {
        ensureAuthorizationEnabled();
        return StatusJSONImpl.createStatusList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("favorites/").append(id).append(".json").toString(), mergeParameters(HttpParameter.getParameterArray("page", page), this.INCLUDE_ENTITIES), this.auth));
    }

    public Status createFavorite(long id) throws TwitterException {
        ensureAuthorizationEnabled();
        return new StatusJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append("favorites/create/").append(id).append(".json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).toString(), this.auth));
    }

    public Status destroyFavorite(long id) throws TwitterException {
        ensureAuthorizationEnabled();
        return new StatusJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append("favorites/destroy/").append(id).append(".json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).toString(), this.auth));
    }

    public User enableNotification(String screenName) throws TwitterException {
        ensureAuthorizationEnabled();
        return new UserJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append("notifications/follow.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).append("&screen_name=").append(screenName).toString(), this.auth));
    }

    public User enableNotification(int userId) throws TwitterException {
        ensureAuthorizationEnabled();
        return new UserJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append("notifications/follow.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).append("&userId=").append(userId).toString(), this.auth));
    }

    public User disableNotification(String screenName) throws TwitterException {
        ensureAuthorizationEnabled();
        return new UserJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append("notifications/leave.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).append("&screen_name=").append(screenName).toString(), this.auth));
    }

    public User disableNotification(int userId) throws TwitterException {
        ensureAuthorizationEnabled();
        return new UserJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append("notifications/leave.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).append("&user_id=").append(userId).toString(), this.auth));
    }

    public User createBlock(String screenName) throws TwitterException {
        ensureAuthorizationEnabled();
        return new UserJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append("blocks/create.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).append("&screen_name=").append(screenName).toString(), this.auth));
    }

    public User createBlock(int userId) throws TwitterException {
        ensureAuthorizationEnabled();
        return new UserJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append("blocks/create.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).append("&user_id=").append(userId).toString(), this.auth));
    }

    public User destroyBlock(String screen_name) throws TwitterException {
        ensureAuthorizationEnabled();
        return new UserJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append("blocks/destroy.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).append("&screen_name=").append(screen_name).toString(), this.auth));
    }

    public User destroyBlock(int userId) throws TwitterException {
        ensureAuthorizationEnabled();
        return new UserJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append("blocks/destroy.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).append("&user_id=").append(userId).toString(), this.auth));
    }

    public boolean existsBlock(String screenName) throws TwitterException {
        ensureAuthorizationEnabled();
        try {
            if (-1 == this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("blocks/exists.json?screen_name=").append(screenName).toString(), this.auth).asString().indexOf("You are not blocking this user.")) {
                return true;
            }
            return false;
        } catch (TwitterException e) {
            TwitterException te = e;
            if (te.getStatusCode() == 404) {
                return false;
            }
            throw te;
        }
    }

    public boolean existsBlock(int userId) throws TwitterException {
        ensureAuthorizationEnabled();
        try {
            if (-1 == this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("blocks/exists.json?user_id=").append(userId).toString(), this.auth).asString().indexOf("<error>You are not blocking this user.</error>")) {
                return true;
            }
            return false;
        } catch (TwitterException e) {
            TwitterException te = e;
            if (te.getStatusCode() == 404) {
                return false;
            }
            throw te;
        }
    }

    public ResponseList<User> getBlockingUsers() throws TwitterException {
        ensureAuthorizationEnabled();
        return UserJSONImpl.createUserList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("blocks/blocking.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).toString(), this.auth));
    }

    public ResponseList<User> getBlockingUsers(int page) throws TwitterException {
        ensureAuthorizationEnabled();
        return UserJSONImpl.createUserList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("blocks/blocking.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).append("&page=").append(page).toString(), this.auth));
    }

    public IDs getBlockingUsersIDs() throws TwitterException {
        ensureAuthorizationEnabled();
        return new IDsJSONImpl(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("blocks/blocking/ids.json").toString(), this.auth));
    }

    public User reportSpam(int userId) throws TwitterException {
        ensureAuthorizationEnabled();
        return new UserJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append("report_spam.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).append("&user_id=").append(userId).toString(), this.auth));
    }

    public User reportSpam(String screenName) throws TwitterException {
        ensureAuthorizationEnabled();
        return new UserJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append("report_spam.json?include_entities=").append(this.conf.isIncludeEntitiesEnabled()).append("&screen_name=").append(screenName).toString(), this.auth));
    }

    public List<SavedSearch> getSavedSearches() throws TwitterException {
        ensureAuthorizationEnabled();
        return SavedSearchJSONImpl.createSavedSearchList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("saved_searches.json").toString(), this.auth));
    }

    public SavedSearch showSavedSearch(int id) throws TwitterException {
        ensureAuthorizationEnabled();
        return new SavedSearchJSONImpl(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("saved_searches/show/").append(id).append(".json").toString(), this.auth));
    }

    public SavedSearch createSavedSearch(String query) throws TwitterException {
        ensureAuthorizationEnabled();
        return new SavedSearchJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append("saved_searches/create.json").toString(), new HttpParameter[]{new HttpParameter("query", query)}, this.auth));
    }

    public SavedSearch destroySavedSearch(int id) throws TwitterException {
        ensureAuthorizationEnabled();
        return new SavedSearchJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append("saved_searches/destroy/").append(id).append(".json").toString(), this.auth));
    }

    public ResponseList<Location> getAvailableTrends() throws TwitterException {
        return LocationJSONImpl.createLocationList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("trends/available.json").toString(), this.auth));
    }

    public ResponseList<Location> getAvailableTrends(GeoLocation location) throws TwitterException {
        return LocationJSONImpl.createLocationList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("trends/available.json").toString(), new HttpParameter[]{new HttpParameter("lat", location.getLatitude()), new HttpParameter("long", location.getLongitude())}, this.auth));
    }

    public Trends getLocationTrends(int woeid) throws TwitterException {
        return new TrendsJSONImpl(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("trends/").append(woeid).append(".json").toString(), this.auth));
    }

    public ResponseList<Place> searchPlaces(GeoQuery query) throws TwitterException {
        try {
            return PlaceJSONImpl.createPlaceList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("geo/search.json").toString(), query.asHttpParameterArray(), this.auth));
        } catch (TwitterException e) {
            TwitterException te = e;
            if (te.getStatusCode() == 404) {
                return new ResponseListImpl(0, null);
            }
            throw te;
        }
    }

    public SimilarPlaces getSimilarPlaces(GeoLocation location, String name, String containedWithin, String streetAddress) throws TwitterException {
        List<HttpParameter> params = new ArrayList<>(3);
        params.add(new HttpParameter("lat", location.getLatitude()));
        params.add(new HttpParameter("long", location.getLongitude()));
        params.add(new HttpParameter("name", name));
        if (containedWithin != null) {
            params.add(new HttpParameter("contained_within", containedWithin));
        }
        if (streetAddress != null) {
            params.add(new HttpParameter("attribute:street_address", streetAddress));
        }
        return SimilarPlacesImpl.createSimilarPlaces(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("geo/similar_places.json").toString(), (HttpParameter[]) params.toArray(new HttpParameter[params.size()]), this.auth));
    }

    public ResponseList<Place> getNearbyPlaces(GeoQuery query) throws TwitterException {
        try {
            return PlaceJSONImpl.createPlaceList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("geo/nearby_places.json").toString(), query.asHttpParameterArray(), this.auth));
        } catch (TwitterException e) {
            TwitterException te = e;
            if (te.getStatusCode() == 404) {
                return new ResponseListImpl(0, null);
            }
            throw te;
        }
    }

    public ResponseList<Place> reverseGeoCode(GeoQuery query) throws TwitterException {
        try {
            return PlaceJSONImpl.createPlaceList(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("geo/reverse_geocode.json").toString(), query.asHttpParameterArray(), this.auth));
        } catch (TwitterException e) {
            TwitterException te = e;
            if (te.getStatusCode() == 404) {
                return new ResponseListImpl(0, null);
            }
            throw te;
        }
    }

    public Place getGeoDetails(String id) throws TwitterException {
        return new PlaceJSONImpl(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("geo/id/").append(id).append(".json").toString(), this.auth));
    }

    public Place createPlace(String name, String containedWithin, String token, GeoLocation location, String streetAddress) throws TwitterException {
        ensureAuthorizationEnabled();
        List<HttpParameter> params = new ArrayList<>(3);
        params.add(new HttpParameter("name", name));
        params.add(new HttpParameter("contained_within", containedWithin));
        params.add(new HttpParameter("token", token));
        params.add(new HttpParameter("lat", location.getLatitude()));
        params.add(new HttpParameter("long", location.getLongitude()));
        if (streetAddress != null) {
            params.add(new HttpParameter("attribute:street_address", streetAddress));
        }
        return new PlaceJSONImpl(this.http.post(new StringBuffer().append(this.conf.getRestBaseURL()).append("geo/place.json").toString(), (HttpParameter[]) params.toArray(new HttpParameter[params.size()]), this.auth));
    }

    public String getTermsOfService() throws TwitterException {
        try {
            return this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("legal/tos.json").toString(), this.auth).asJSONObject().getString("tos");
        } catch (JSONException e) {
            throw new TwitterException(e);
        }
    }

    public String getPrivacyPolicy() throws TwitterException {
        try {
            return this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("legal/privacy.json").toString(), this.auth).asJSONObject().getString("privacy");
        } catch (JSONException e) {
            throw new TwitterException(e);
        }
    }

    public RelatedResults getRelatedResults(long statusId) throws TwitterException {
        ensureAuthorizationEnabled();
        return new RelatedResultsJSONImpl(this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("related_results/show/").append(Long.toString(statusId)).append(".json").toString(), this.auth));
    }

    public boolean test() throws TwitterException {
        return -1 != this.http.get(new StringBuffer().append(this.conf.getRestBaseURL()).append("help/test.json").toString()).asString().indexOf("ok");
    }

    private static String toCommaSeparatedString(String[] strArray) {
        StringBuffer buf = new StringBuffer(strArray.length * 8);
        for (String value : strArray) {
            if (buf.length() != 0) {
                buf.append(",");
            }
            buf.append(value);
        }
        return buf.toString();
    }

    private static String toCommaSeparatedString(int[] strArray) {
        StringBuffer buf = new StringBuffer(strArray.length * 8);
        for (int value : strArray) {
            if (buf.length() != 0) {
                buf.append(",");
            }
            buf.append(value);
        }
        return buf.toString();
    }

    public String toString() {
        return new StringBuffer().append("Twitter{auth='").append(this.auth).append('\'').append('}').toString();
    }
}
