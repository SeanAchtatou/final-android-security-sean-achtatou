package twitter4j;

import java.io.Serializable;
import twitter4j.internal.org.json.JSONObject;
import twitter4j.internal.util.ParseUtil;

final class TrendJSONImpl implements Trend, Serializable {
    private static final long serialVersionUID = 1925956704460743946L;
    private String name;
    private String query = null;
    private String url = null;

    TrendJSONImpl(JSONObject json) {
        this.name = ParseUtil.getRawString("name", json);
        this.url = ParseUtil.getRawString("url", json);
        this.query = ParseUtil.getRawString("query", json);
    }

    public String getName() {
        return this.name;
    }

    public String getUrl() {
        return this.url;
    }

    public String getQuery() {
        return this.query;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Trend)) {
            return false;
        }
        Trend trend = (Trend) o;
        if (!this.name.equals(trend.getName())) {
            return false;
        }
        if (this.query == null ? trend.getQuery() != null : !this.query.equals(trend.getQuery())) {
            return false;
        }
        return this.url == null ? trend.getUrl() == null : this.url.equals(trend.getUrl());
    }

    public int hashCode() {
        int i;
        int i2;
        int hashCode = this.name.hashCode() * 31;
        if (this.url != null) {
            i = this.url.hashCode();
        } else {
            i = 0;
        }
        int i3 = (hashCode + i) * 31;
        if (this.query != null) {
            i2 = this.query.hashCode();
        } else {
            i2 = 0;
        }
        return i3 + i2;
    }

    public String toString() {
        return new StringBuffer().append("TrendJSONImpl{name='").append(this.name).append('\'').append(", url='").append(this.url).append('\'').append(", query='").append(this.query).append('\'').append('}').toString();
    }
}
