package twitter4j;

import java.io.Serializable;
import twitter4j.conf.Configuration;
import twitter4j.http.Authorization;
import twitter4j.http.BasicAuthorization;
import twitter4j.http.NullAuthorization;
import twitter4j.http.OAuthAuthorization;

abstract class TwitterBase implements Serializable {
    private static final long serialVersionUID = -3812176145960812140L;
    protected Authorization auth;
    protected final Configuration conf;

    TwitterBase(Configuration conf2) {
        this.conf = conf2;
        initBasicAuthorization(conf2.getUser(), conf2.getPassword());
    }

    TwitterBase(Configuration conf2, String userId, String password) {
        this.conf = conf2;
        initBasicAuthorization(userId, password);
    }

    private void initBasicAuthorization(String screenName, String password) {
        if (!(screenName == null || password == null)) {
            this.auth = new BasicAuthorization(screenName, password);
        }
        if (this.auth == null) {
            this.auth = NullAuthorization.getInstance();
        }
    }

    TwitterBase(Configuration conf2, Authorization auth2) {
        this.conf = conf2;
        this.auth = auth2;
    }

    public final boolean isBasicAuthEnabled() {
        return (this.auth instanceof BasicAuthorization) && this.auth.isEnabled();
    }

    /* access modifiers changed from: protected */
    public final void ensureAuthorizationEnabled() {
        if (!this.auth.isEnabled()) {
            throw new IllegalStateException("Authentication credentials are missing. See http://twitter4j.org/configuration.html for the detail.");
        }
    }

    /* access modifiers changed from: protected */
    public final void ensureOAuthEnabled() {
        if (!(this.auth instanceof OAuthAuthorization)) {
            throw new IllegalStateException("OAuth required. Authentication credentials are missing. See http://twitter4j.org/configuration.html for the detail.");
        }
    }

    public final Authorization getAuthorization() {
        return this.auth;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TwitterBase)) {
            return false;
        }
        return this.auth.equals(((TwitterBase) o).auth);
    }

    public Configuration getConfiguration() {
        return this.conf;
    }

    public int hashCode() {
        if (this.auth != null) {
            return this.auth.hashCode();
        }
        return 0;
    }

    public String toString() {
        return new StringBuffer().append("TwitterBase{auth=").append(this.auth).append('}').toString();
    }
}
