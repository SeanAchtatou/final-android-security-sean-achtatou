package twitter4j;

import java.io.Serializable;
import java.util.Date;

public interface Tweet extends Comparable<Tweet>, Serializable {
    Annotations getAnnotations();

    Date getCreatedAt();

    String getFromUser();

    int getFromUserId();

    GeoLocation getGeoLocation();

    long getId();

    String getIsoLanguageCode();

    String getLocation();

    String getProfileImageUrl();

    String getSource();

    String getText();

    String getToUser();

    int getToUserId();
}
