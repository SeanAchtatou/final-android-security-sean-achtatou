package twitter4j;

import java.io.Serializable;
import java.util.Date;

public interface RateLimitStatus extends Serializable {
    int getHourlyLimit();

    int getRemainingHits();

    Date getResetTime();

    int getResetTimeInSeconds();

    int getSecondsUntilReset();
}
