package twitter4j;

import java.io.File;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Date;
import twitter4j.ProfileImage;
import twitter4j.api.AccountMethodsAsync;
import twitter4j.api.BlockMethodsAsync;
import twitter4j.api.DirectMessageMethodsAsync;
import twitter4j.api.FavoriteMethodsAsync;
import twitter4j.api.FriendsFollowersMethodsAsync;
import twitter4j.api.FriendshipMethodsAsync;
import twitter4j.api.GeoMethodsAsync;
import twitter4j.api.HelpMethodsAsync;
import twitter4j.api.LegalResourcesAsync;
import twitter4j.api.ListMembersMethodsAsync;
import twitter4j.api.ListMethodsAsync;
import twitter4j.api.ListSubscribersMethodsAsync;
import twitter4j.api.LocalTrendsMethodsAsync;
import twitter4j.api.NewTwitterMethodsAsync;
import twitter4j.api.NotificationMethodsAsync;
import twitter4j.api.SavedSearchesMethodsAsync;
import twitter4j.api.SearchMethodsAsync;
import twitter4j.api.SpamReportingMethodsAsync;
import twitter4j.api.StatusMethodsAsync;
import twitter4j.api.TimelineMethodsAsync;
import twitter4j.api.TrendsMethodsAsync;
import twitter4j.api.UserMethodsAsync;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationContext;
import twitter4j.http.AccessToken;
import twitter4j.http.Authorization;
import twitter4j.http.RequestToken;
import twitter4j.internal.async.Dispatcher;
import twitter4j.internal.async.DispatcherFactory;
import twitter4j.internal.http.HttpResponseEvent;

public class AsyncTwitter extends TwitterOAuthSupportBase implements Serializable, SearchMethodsAsync, TrendsMethodsAsync, TimelineMethodsAsync, StatusMethodsAsync, UserMethodsAsync, ListMethodsAsync, ListMembersMethodsAsync, ListSubscribersMethodsAsync, DirectMessageMethodsAsync, FriendshipMethodsAsync, FriendsFollowersMethodsAsync, AccountMethodsAsync, FavoriteMethodsAsync, NotificationMethodsAsync, BlockMethodsAsync, SpamReportingMethodsAsync, SavedSearchesMethodsAsync, LocalTrendsMethodsAsync, GeoMethodsAsync, LegalResourcesAsync, NewTwitterMethodsAsync, HelpMethodsAsync {
    static Class class$twitter4j$AsyncTwitter = null;
    static Class class$twitter4j$TwitterStream = null;
    private static transient Dispatcher dispatcher = null;
    private static final long serialVersionUID = -2008667933225051907L;
    private final TwitterListener listener;
    private final Twitter twitter;

    static Twitter access$000(AsyncTwitter x0) {
        return x0.twitter;
    }

    public boolean equals(Object x0) {
        return super.equals(x0);
    }

    public Configuration getConfiguration() {
        return super.getConfiguration();
    }

    public int hashCode() {
        return super.hashCode();
    }

    public void httpResponseReceived(HttpResponseEvent x0) {
        super.httpResponseReceived(x0);
    }

    public boolean isOAuthEnabled() {
        return super.isOAuthEnabled();
    }

    public void setRateLimitStatusListener(RateLimitStatusListener x0) {
        super.setRateLimitStatusListener(x0);
    }

    public String toString() {
        return super.toString();
    }

    public AsyncTwitter(String screenName, String password, TwitterListener listener2) {
        super(ConfigurationContext.getInstance(), screenName, password);
        this.twitter = new TwitterFactory(ConfigurationContext.getInstance()).getInstance(screenName, password);
        this.listener = listener2;
    }

    AsyncTwitter(Configuration conf, Authorization auth, TwitterListener listener2) {
        super(conf, auth);
        this.twitter = new TwitterFactory(conf).getInstance(auth);
        this.listener = listener2;
    }

    public String getScreenName() throws TwitterException, IllegalStateException {
        return this.twitter.getScreenName();
    }

    public int getId() throws TwitterException, IllegalStateException {
        return this.twitter.getId();
    }

    public void search(Query query) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.SEARCH, this.listener, query) {
            private final AsyncTwitter this$0;
            private final Query val$query;

            {
                this.this$0 = r1;
                this.val$query = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.searched(AsyncTwitter.access$000(this.this$0).search(this.val$query));
            }
        });
    }

    public void getTrends() {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.TRENDS, this.listener) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotTrends(AsyncTwitter.access$000(this.this$0).getTrends());
            }
        });
    }

    public void getCurrentTrends() {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.CURRENT_TRENDS, this.listener) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotCurrentTrends(AsyncTwitter.access$000(this.this$0).getCurrentTrends());
            }
        });
    }

    public void getCurrentTrends(boolean excludeHashTags) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.CURRENT_TRENDS, this.listener, excludeHashTags) {
            private final AsyncTwitter this$0;
            private final boolean val$excludeHashTags;

            {
                this.this$0 = r1;
                this.val$excludeHashTags = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotCurrentTrends(AsyncTwitter.access$000(this.this$0).getCurrentTrends(this.val$excludeHashTags));
            }
        });
    }

    public void getDailyTrends() {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.DAILY_TRENDS, this.listener) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotDailyTrends(AsyncTwitter.access$000(this.this$0).getDailyTrends());
            }
        });
    }

    public void getDailyTrends(Date date, boolean excludeHashTags) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.DAILY_TRENDS, this.listener, date, excludeHashTags) {
            private final AsyncTwitter this$0;
            private final Date val$date;
            private final boolean val$excludeHashTags;

            {
                this.this$0 = r1;
                this.val$date = r4;
                this.val$excludeHashTags = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotDailyTrends(AsyncTwitter.access$000(this.this$0).getDailyTrends(this.val$date, this.val$excludeHashTags));
            }
        });
    }

    public void getWeeklyTrends() {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.WEEKLY_TRENDS, this.listener) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotWeeklyTrends(AsyncTwitter.access$000(this.this$0).getWeeklyTrends());
            }
        });
    }

    public void getWeeklyTrends(Date date, boolean excludeHashTags) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.WEEKLY_TRENDS, this.listener, date, excludeHashTags) {
            private final AsyncTwitter this$0;
            private final Date val$date;
            private final boolean val$excludeHashTags;

            {
                this.this$0 = r1;
                this.val$date = r4;
                this.val$excludeHashTags = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotWeeklyTrends(AsyncTwitter.access$000(this.this$0).getWeeklyTrends(this.val$date, this.val$excludeHashTags));
            }
        });
    }

    public void getPublicTimeline() {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.PUBLIC_TIMELINE, this.listener) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotPublicTimeline(AsyncTwitter.access$000(this.this$0).getPublicTimeline());
            }
        });
    }

    public void getHomeTimeline() {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.HOME_TIMELINE, this.listener) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotHomeTimeline(AsyncTwitter.access$000(this.this$0).getHomeTimeline());
            }
        });
    }

    public void getHomeTimeline(Paging paging) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.HOME_TIMELINE, this.listener, paging) {
            private final AsyncTwitter this$0;
            private final Paging val$paging;

            {
                this.this$0 = r1;
                this.val$paging = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotHomeTimeline(AsyncTwitter.access$000(this.this$0).getHomeTimeline(this.val$paging));
            }
        });
    }

    public void getFriendsTimeline() {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.FRIENDS_TIMELINE, this.listener) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotFriendsTimeline(AsyncTwitter.access$000(this.this$0).getFriendsTimeline());
            }
        });
    }

    public void getFriendsTimeline(Paging paging) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.FRIENDS_TIMELINE, this.listener, paging) {
            private final AsyncTwitter this$0;
            private final Paging val$paging;

            {
                this.this$0 = r1;
                this.val$paging = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotFriendsTimeline(AsyncTwitter.access$000(this.this$0).getFriendsTimeline(this.val$paging));
            }
        });
    }

    public void getUserTimeline(String screenName, Paging paging) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.USER_TIMELINE, this.listener, screenName, paging) {
            private final AsyncTwitter this$0;
            private final Paging val$paging;
            private final String val$screenName;

            {
                this.this$0 = r1;
                this.val$screenName = r4;
                this.val$paging = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotUserTimeline(AsyncTwitter.access$000(this.this$0).getUserTimeline(this.val$screenName, this.val$paging));
            }
        });
    }

    public void getUserTimeline(int userId, Paging paging) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.USER_TIMELINE, this.listener, userId, paging) {
            private final AsyncTwitter this$0;
            private final Paging val$paging;
            private final int val$userId;

            {
                this.this$0 = r1;
                this.val$userId = r4;
                this.val$paging = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotUserTimeline(AsyncTwitter.access$000(this.this$0).getUserTimeline(this.val$userId, this.val$paging));
            }
        });
    }

    public void getUserTimeline(Paging paging) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.USER_TIMELINE, this.listener, paging) {
            private final AsyncTwitter this$0;
            private final Paging val$paging;

            {
                this.this$0 = r1;
                this.val$paging = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotUserTimeline(AsyncTwitter.access$000(this.this$0).getUserTimeline(this.val$paging));
            }
        });
    }

    public void getUserTimeline(String screenName) {
        getUserTimeline(screenName, new Paging());
    }

    public void getUserTimeline(int userId) {
        getUserTimeline(userId, new Paging());
    }

    public void getUserTimeline() {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.USER_TIMELINE, this.listener) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotUserTimeline(AsyncTwitter.access$000(this.this$0).getUserTimeline());
            }
        });
    }

    public void getMentions() {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.MENTIONS, this.listener) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotMentions(AsyncTwitter.access$000(this.this$0).getMentions());
            }
        });
    }

    public void getMentions(Paging paging) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.MENTIONS, this.listener, paging) {
            private final AsyncTwitter this$0;
            private final Paging val$paging;

            {
                this.this$0 = r1;
                this.val$paging = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotMentions(AsyncTwitter.access$000(this.this$0).getMentions(this.val$paging));
            }
        });
    }

    public void getRetweetedByMe() {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.RETWEETED_BY_ME, this.listener) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotRetweetedByMe(AsyncTwitter.access$000(this.this$0).getRetweetedByMe());
            }
        });
    }

    public void getRetweetedByMe(Paging paging) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.RETWEETED_BY_ME, this.listener, paging) {
            private final AsyncTwitter this$0;
            private final Paging val$paging;

            {
                this.this$0 = r1;
                this.val$paging = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotRetweetedByMe(AsyncTwitter.access$000(this.this$0).getRetweetedByMe(this.val$paging));
            }
        });
    }

    public void getRetweetedToMe() {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.RETWEETED_TO_ME, this.listener) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotRetweetedToMe(AsyncTwitter.access$000(this.this$0).getRetweetedToMe());
            }
        });
    }

    public void getRetweetedToMe(Paging paging) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.RETWEETED_TO_ME, this.listener, paging) {
            private final AsyncTwitter this$0;
            private final Paging val$paging;

            {
                this.this$0 = r1;
                this.val$paging = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotRetweetedToMe(AsyncTwitter.access$000(this.this$0).getRetweetedToMe(this.val$paging));
            }
        });
    }

    public void getRetweetsOfMe() {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.RETWEETS_OF_ME, this.listener) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotRetweetsOfMe(AsyncTwitter.access$000(this.this$0).getRetweetsOfMe());
            }
        });
    }

    public void getRetweetsOfMe(Paging paging) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.RETWEETS_OF_ME, this.listener, paging) {
            private final AsyncTwitter this$0;
            private final Paging val$paging;

            {
                this.this$0 = r1;
                this.val$paging = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotRetweetsOfMe(AsyncTwitter.access$000(this.this$0).getRetweetsOfMe(this.val$paging));
            }
        });
    }

    public void getRetweetedByUser(String screenName, Paging paging) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.RETWEETED_BY_USER, this.listener, screenName, paging) {
            private final AsyncTwitter this$0;
            private final Paging val$paging;
            private final String val$screenName;

            {
                this.this$0 = r1;
                this.val$screenName = r4;
                this.val$paging = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotRetweetedByUser(AsyncTwitter.access$000(this.this$0).getRetweetedByUser(this.val$screenName, this.val$paging));
            }
        });
    }

    public void getRetweetedByUser(int userId, Paging paging) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.RETWEETED_BY_USER, this.listener, userId, paging) {
            private final AsyncTwitter this$0;
            private final Paging val$paging;
            private final int val$userId;

            {
                this.this$0 = r1;
                this.val$userId = r4;
                this.val$paging = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotRetweetedByUser(AsyncTwitter.access$000(this.this$0).getRetweetedByUser(this.val$userId, this.val$paging));
            }
        });
    }

    public void getRetweetedToUser(String screenName, Paging paging) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.RETWEETED_TO_USER, this.listener, screenName, paging) {
            private final AsyncTwitter this$0;
            private final Paging val$paging;
            private final String val$screenName;

            {
                this.this$0 = r1;
                this.val$screenName = r4;
                this.val$paging = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotRetweetedToUser(AsyncTwitter.access$000(this.this$0).getRetweetedToUser(this.val$screenName, this.val$paging));
            }
        });
    }

    public void getRetweetedToUser(int userId, Paging paging) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.RETWEETED_TO_USER, this.listener, userId, paging) {
            private final AsyncTwitter this$0;
            private final Paging val$paging;
            private final int val$userId;

            {
                this.this$0 = r1;
                this.val$userId = r4;
                this.val$paging = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotRetweetedToUser(AsyncTwitter.access$000(this.this$0).getRetweetedToUser(this.val$userId, this.val$paging));
            }
        });
    }

    public void showStatus(long id) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.SHOW_STATUS, this.listener, id) {
            private final AsyncTwitter this$0;
            private final long val$id;

            {
                this.this$0 = r1;
                this.val$id = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotShowStatus(AsyncTwitter.access$000(this.this$0).showStatus(this.val$id));
            }
        });
    }

    public void updateStatus(String status) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.UPDATE_STATUS, this.listener, status) {
            private final AsyncTwitter this$0;
            private final String val$status;

            {
                this.this$0 = r1;
                this.val$status = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.updatedStatus(AsyncTwitter.access$000(this.this$0).updateStatus(this.val$status));
            }
        });
    }

    public void updateStatus(String status, GeoLocation location) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.UPDATE_STATUS, this.listener, status, location) {
            private final AsyncTwitter this$0;
            private final GeoLocation val$location;
            private final String val$status;

            {
                this.this$0 = r1;
                this.val$status = r4;
                this.val$location = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.updatedStatus(AsyncTwitter.access$000(this.this$0).updateStatus(this.val$status, this.val$location));
            }
        });
    }

    public void updateStatus(String status, long inReplyToStatusId) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.UPDATE_STATUS, this.listener, status, inReplyToStatusId) {
            private final AsyncTwitter this$0;
            private final long val$inReplyToStatusId;
            private final String val$status;

            {
                this.this$0 = r1;
                this.val$status = r4;
                this.val$inReplyToStatusId = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.updatedStatus(AsyncTwitter.access$000(this.this$0).updateStatus(this.val$status, this.val$inReplyToStatusId));
            }
        });
    }

    public void updateStatus(String status, long inReplyToStatusId, GeoLocation location) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.UPDATE_STATUS, this.listener, status, inReplyToStatusId, location) {
            private final AsyncTwitter this$0;
            private final long val$inReplyToStatusId;
            private final GeoLocation val$location;
            private final String val$status;

            {
                this.this$0 = r1;
                this.val$status = r4;
                this.val$inReplyToStatusId = r5;
                this.val$location = r7;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.updatedStatus(AsyncTwitter.access$000(this.this$0).updateStatus(this.val$status, this.val$inReplyToStatusId, this.val$location));
            }
        });
    }

    public void updateStatus(StatusUpdate latestStatus) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.UPDATE_STATUS, this.listener, latestStatus) {
            private final AsyncTwitter this$0;
            private final StatusUpdate val$latestStatus;

            {
                this.this$0 = r1;
                this.val$latestStatus = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.updatedStatus(AsyncTwitter.access$000(this.this$0).updateStatus(this.val$latestStatus));
            }
        });
    }

    public void destroyStatus(long statusId) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.DESTROY_STATUS, this.listener, statusId) {
            private final AsyncTwitter this$0;
            private final long val$statusId;

            {
                this.this$0 = r1;
                this.val$statusId = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.destroyedStatus(AsyncTwitter.access$000(this.this$0).destroyStatus(this.val$statusId));
            }
        });
    }

    public void retweetStatus(long statusId) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.RETWEET_STATUS, this.listener, statusId) {
            private final AsyncTwitter this$0;
            private final long val$statusId;

            {
                this.this$0 = r1;
                this.val$statusId = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.retweetedStatus(AsyncTwitter.access$000(this.this$0).retweetStatus(this.val$statusId));
            }
        });
    }

    public void getRetweets(long statusId) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.RETWEETS, this.listener, statusId) {
            private final AsyncTwitter this$0;
            private final long val$statusId;

            {
                this.this$0 = r1;
                this.val$statusId = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotRetweets(AsyncTwitter.access$000(this.this$0).getRetweets(this.val$statusId));
            }
        });
    }

    public void getRetweetedBy(long statusId) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.RETWEETED_BY, this.listener, statusId) {
            private final AsyncTwitter this$0;
            private final long val$statusId;

            {
                this.this$0 = r1;
                this.val$statusId = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotRetweetedBy(AsyncTwitter.access$000(this.this$0).getRetweetedBy(this.val$statusId));
            }
        });
    }

    public void getRetweetedBy(long statusId, Paging paging) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.RETWEETED_BY, this.listener, statusId, paging) {
            private final AsyncTwitter this$0;
            private final Paging val$paging;
            private final long val$statusId;

            {
                this.this$0 = r1;
                this.val$statusId = r4;
                this.val$paging = r6;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotRetweetedBy(AsyncTwitter.access$000(this.this$0).getRetweetedBy(this.val$statusId, this.val$paging));
            }
        });
    }

    public void getRetweetedByIDs(long statusId) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.RETWEETED_BY_IDS, this.listener, statusId) {
            private final AsyncTwitter this$0;
            private final long val$statusId;

            {
                this.this$0 = r1;
                this.val$statusId = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotRetweetedByIDs(AsyncTwitter.access$000(this.this$0).getRetweetedByIDs(this.val$statusId));
            }
        });
    }

    public void getRetweetedByIDs(long statusId, Paging paging) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.RETWEETED_BY_IDS, this.listener, statusId, paging) {
            private final AsyncTwitter this$0;
            private final Paging val$paging;
            private final long val$statusId;

            {
                this.this$0 = r1;
                this.val$statusId = r4;
                this.val$paging = r6;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotRetweetedByIDs(AsyncTwitter.access$000(this.this$0).getRetweetedByIDs(this.val$statusId, this.val$paging));
            }
        });
    }

    public void showUser(String screenName) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.SHOW_USER, this.listener, screenName) {
            private final AsyncTwitter this$0;
            private final String val$screenName;

            {
                this.this$0 = r1;
                this.val$screenName = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotUserDetail(AsyncTwitter.access$000(this.this$0).showUser(this.val$screenName));
            }
        });
    }

    public void showUser(int userId) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.SHOW_USER, this.listener, userId) {
            private final AsyncTwitter this$0;
            private final int val$userId;

            {
                this.this$0 = r1;
                this.val$userId = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotUserDetail(AsyncTwitter.access$000(this.this$0).showUser(this.val$userId));
            }
        });
    }

    public void lookupUsers(String[] screenNames) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.LOOKUP_USERS, this.listener, screenNames) {
            private final AsyncTwitter this$0;
            private final String[] val$screenNames;

            {
                this.this$0 = r1;
                this.val$screenNames = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.lookedupUsers(AsyncTwitter.access$000(this.this$0).lookupUsers(this.val$screenNames));
            }
        });
    }

    public void lookupUsers(int[] ids) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.LOOKUP_USERS, this.listener, ids) {
            private final AsyncTwitter this$0;
            private final int[] val$ids;

            {
                this.this$0 = r1;
                this.val$ids = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.lookedupUsers(AsyncTwitter.access$000(this.this$0).lookupUsers(this.val$ids));
            }
        });
    }

    public void searchUsers(String query, int page) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.SEARCH_USERS, this.listener, query, page) {
            private final AsyncTwitter this$0;
            private final int val$page;
            private final String val$query;

            {
                this.this$0 = r1;
                this.val$query = r4;
                this.val$page = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.searchedUser(AsyncTwitter.access$000(this.this$0).searchUsers(this.val$query, this.val$page));
            }
        });
    }

    public void getSuggestedUserCategories() {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.SUGGESTED_USER_CATEGORIES, this.listener) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotSuggestedUserCategories(AsyncTwitter.access$000(this.this$0).getSuggestedUserCategories());
            }
        });
    }

    public void getUserSuggestions(String categorySlug) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.USER_SUGGESTIONS, this.listener, categorySlug) {
            private final AsyncTwitter this$0;
            private final String val$categorySlug;

            {
                this.this$0 = r1;
                this.val$categorySlug = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotUserSuggestions(AsyncTwitter.access$000(this.this$0).getUserSuggestions(this.val$categorySlug));
            }
        });
    }

    public void getMemberSuggestions(String categorySlug) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.MEMBER_SUGGESTIONS, this.listener, categorySlug) {
            private final AsyncTwitter this$0;
            private final String val$categorySlug;

            {
                this.this$0 = r1;
                this.val$categorySlug = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotMemberSuggestions(AsyncTwitter.access$000(this.this$0).getMemberSuggestions(this.val$categorySlug));
            }
        });
    }

    public void getProfileImage(String screenName, ProfileImage.ImageSize size) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.PROFILE_IMAGE, this.listener, screenName, size) {
            private final AsyncTwitter this$0;
            private final String val$screenName;
            private final ProfileImage.ImageSize val$size;

            {
                this.this$0 = r1;
                this.val$screenName = r4;
                this.val$size = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotProfileImage(AsyncTwitter.access$000(this.this$0).getProfileImage(this.val$screenName, this.val$size));
            }
        });
    }

    public void getAccountTotals() {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.ACCOUNT_TOTALS, this.listener) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotAccountTotals(AsyncTwitter.access$000(this.this$0).getAccountTotals());
            }
        });
    }

    public void getAccountSettings() {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.ACCOUNT_SETTINGS, this.listener) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotAccountSettings(AsyncTwitter.access$000(this.this$0).getAccountSettings());
            }
        });
    }

    public void getFriendsStatuses() {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.FRIENDS_STATUSES, this.listener) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotFriendsStatuses(AsyncTwitter.access$000(this.this$0).getFriendsStatuses());
            }
        });
    }

    public void getFriendsStatuses(long cursor) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.FRIENDS_STATUSES, this.listener, cursor) {
            private final AsyncTwitter this$0;
            private final long val$cursor;

            {
                this.this$0 = r1;
                this.val$cursor = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotFriendsStatuses(AsyncTwitter.access$000(this.this$0).getFriendsStatuses(this.val$cursor));
            }
        });
    }

    public void getFriendsStatuses(String screenName) {
        getFriendsStatuses(screenName, -1);
    }

    public void getFriendsStatuses(int userId) {
        getFriendsStatuses(userId, -1);
    }

    public void getFriendsStatuses(String screenName, long cursor) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.FRIENDS_STATUSES, this.listener, screenName, cursor) {
            private final AsyncTwitter this$0;
            private final long val$cursor;
            private final String val$screenName;

            {
                this.this$0 = r1;
                this.val$screenName = r4;
                this.val$cursor = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotFriendsStatuses(AsyncTwitter.access$000(this.this$0).getFriendsStatuses(this.val$screenName, this.val$cursor));
            }
        });
    }

    public void getFriendsStatuses(int userId, long cursor) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.FRIENDS_STATUSES, this.listener, userId, cursor) {
            private final AsyncTwitter this$0;
            private final long val$cursor;
            private final int val$userId;

            {
                this.this$0 = r1;
                this.val$userId = r4;
                this.val$cursor = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotFriendsStatuses(AsyncTwitter.access$000(this.this$0).getFriendsStatuses(this.val$userId, this.val$cursor));
            }
        });
    }

    public void getFollowersStatuses() {
        getFollowersStatuses(-1L);
    }

    public void getFollowersStatuses(long cursor) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.FOLLOWERS_STATUSES, this.listener, cursor) {
            private final AsyncTwitter this$0;
            private final long val$cursor;

            {
                this.this$0 = r1;
                this.val$cursor = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotFollowersStatuses(AsyncTwitter.access$000(this.this$0).getFollowersStatuses(this.val$cursor));
            }
        });
    }

    public void getFollowersStatuses(String screenName) {
        getFollowersStatuses(screenName, -1);
    }

    public void getFollowersStatuses(int userId) {
        getFollowersStatuses(userId, -1);
    }

    public void getFollowersStatuses(String screenName, long cursor) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.FOLLOWERS_STATUSES, this.listener, screenName, cursor) {
            private final AsyncTwitter this$0;
            private final long val$cursor;
            private final String val$screenName;

            {
                this.this$0 = r1;
                this.val$screenName = r4;
                this.val$cursor = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotFollowersStatuses(AsyncTwitter.access$000(this.this$0).getFollowersStatuses(this.val$screenName, this.val$cursor));
            }
        });
    }

    public void getFollowersStatuses(int userId, long cursor) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.FOLLOWERS_STATUSES, this.listener, userId, cursor) {
            private final AsyncTwitter this$0;
            private final long val$cursor;
            private final int val$userId;

            {
                this.this$0 = r1;
                this.val$userId = r4;
                this.val$cursor = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotFollowersStatuses(AsyncTwitter.access$000(this.this$0).getFollowersStatuses(this.val$userId, this.val$cursor));
            }
        });
    }

    public void createUserList(String listName, boolean isPublicList, String description) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.CREATE_USER_LIST, this.listener, listName, isPublicList, description) {
            private final AsyncTwitter this$0;
            private final String val$description;
            private final boolean val$isPublicList;
            private final String val$listName;

            {
                this.this$0 = r1;
                this.val$listName = r4;
                this.val$isPublicList = r5;
                this.val$description = r6;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.createdUserList(AsyncTwitter.access$000(this.this$0).createUserList(this.val$listName, this.val$isPublicList, this.val$description));
            }
        });
    }

    public void updateUserList(int listId, String newListName, boolean isPublicList, String newDescription) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.UPDATE_USER_LIST, this.listener, listId, newListName, isPublicList, newDescription) {
            private final AsyncTwitter this$0;
            private final boolean val$isPublicList;
            private final int val$listId;
            private final String val$newDescription;
            private final String val$newListName;

            {
                this.this$0 = r1;
                this.val$listId = r4;
                this.val$newListName = r5;
                this.val$isPublicList = r6;
                this.val$newDescription = r7;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.updatedUserList(AsyncTwitter.access$000(this.this$0).updateUserList(this.val$listId, this.val$newListName, this.val$isPublicList, this.val$newDescription));
            }
        });
    }

    public void getUserLists(String listOwnerScreenName, long cursor) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.USER_LISTS, this.listener, listOwnerScreenName, cursor) {
            private final AsyncTwitter this$0;
            private final long val$cursor;
            private final String val$listOwnerScreenName;

            {
                this.this$0 = r1;
                this.val$listOwnerScreenName = r4;
                this.val$cursor = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotUserLists(AsyncTwitter.access$000(this.this$0).getUserLists(this.val$listOwnerScreenName, this.val$cursor));
            }
        });
    }

    public void showUserList(String listOwnerScreenName, int id) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.UPDATE_USER_LIST, this.listener, listOwnerScreenName, id) {
            private final AsyncTwitter this$0;
            private final int val$id;
            private final String val$listOwnerScreenName;

            {
                this.this$0 = r1;
                this.val$listOwnerScreenName = r4;
                this.val$id = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotShowUserList(AsyncTwitter.access$000(this.this$0).showUserList(this.val$listOwnerScreenName, this.val$id));
            }
        });
    }

    public void destroyUserList(int listId) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.DESTROY_USER_LIST, this.listener, listId) {
            private final AsyncTwitter this$0;
            private final int val$listId;

            {
                this.this$0 = r1;
                this.val$listId = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.destroyedUserList(AsyncTwitter.access$000(this.this$0).destroyUserList(this.val$listId));
            }
        });
    }

    public void getUserListStatuses(String listOwnerScreenName, int id, Paging paging) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.USER_LIST_STATUSES, this.listener, listOwnerScreenName, id, paging) {
            private final AsyncTwitter this$0;
            private final int val$id;
            private final String val$listOwnerScreenName;
            private final Paging val$paging;

            {
                this.this$0 = r1;
                this.val$listOwnerScreenName = r4;
                this.val$id = r5;
                this.val$paging = r6;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotUserListStatuses(AsyncTwitter.access$000(this.this$0).getUserListStatuses(this.val$listOwnerScreenName, this.val$id, this.val$paging));
            }
        });
    }

    public void getUserListStatuses(int listOwnerId, int id, Paging paging) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.USER_LIST_STATUSES, this.listener, listOwnerId, id, paging) {
            private final AsyncTwitter this$0;
            private final int val$id;
            private final int val$listOwnerId;
            private final Paging val$paging;

            {
                this.this$0 = r1;
                this.val$listOwnerId = r4;
                this.val$id = r5;
                this.val$paging = r6;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotUserListStatuses(AsyncTwitter.access$000(this.this$0).getUserListStatuses(this.val$listOwnerId, this.val$id, this.val$paging));
            }
        });
    }

    public void getUserListMemberships(String listMemberScreenName, long cursor) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.USER_LIST_MEMBERSHIPS, this.listener, listMemberScreenName, cursor) {
            private final AsyncTwitter this$0;
            private final long val$cursor;
            private final String val$listMemberScreenName;

            {
                this.this$0 = r1;
                this.val$listMemberScreenName = r4;
                this.val$cursor = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotUserListMemberships(AsyncTwitter.access$000(this.this$0).getUserListMemberships(this.val$listMemberScreenName, this.val$cursor));
            }
        });
    }

    public void getUserListSubscriptions(String listOwnerScreenName, long cursor) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.USER_LIST_SUBSCRIPTIONS, this.listener, listOwnerScreenName, cursor) {
            private final AsyncTwitter this$0;
            private final long val$cursor;
            private final String val$listOwnerScreenName;

            {
                this.this$0 = r1;
                this.val$listOwnerScreenName = r4;
                this.val$cursor = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotUserListSubscriptions(AsyncTwitter.access$000(this.this$0).getUserListSubscriptions(this.val$listOwnerScreenName, this.val$cursor));
            }
        });
    }

    public void getAllSubscribingUserLists(String screenName) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.ALL_USER_LISTS, this.listener, screenName) {
            private final AsyncTwitter this$0;
            private final String val$screenName;

            {
                this.this$0 = r1;
                this.val$screenName = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotAllUserLists(AsyncTwitter.access$000(this.this$0).getAllUserLists(this.val$screenName));
            }
        });
    }

    public void getAllSubscribingUserLists(int userId) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.ALL_USER_LISTS, this.listener, userId) {
            private final AsyncTwitter this$0;
            private final int val$userId;

            {
                this.this$0 = r1;
                this.val$userId = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotAllUserLists(AsyncTwitter.access$000(this.this$0).getAllUserLists(this.val$userId));
            }
        });
    }

    public void getUserListMembers(String listOwnerScreenName, int listId, long cursor) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.LIST_MEMBERS, this.listener, listOwnerScreenName, listId, cursor) {
            private final AsyncTwitter this$0;
            private final long val$cursor;
            private final int val$listId;
            private final String val$listOwnerScreenName;

            {
                this.this$0 = r1;
                this.val$listOwnerScreenName = r4;
                this.val$listId = r5;
                this.val$cursor = r6;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotUserListMembers(AsyncTwitter.access$000(this.this$0).getUserListMembers(this.val$listOwnerScreenName, this.val$listId, this.val$cursor));
            }
        });
    }

    public void getUserListMembers(int listOwnerId, int listId, long cursor) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.LIST_MEMBERS, this.listener, listOwnerId, listId, cursor) {
            private final AsyncTwitter this$0;
            private final long val$cursor;
            private final int val$listId;
            private final int val$listOwnerId;

            {
                this.this$0 = r1;
                this.val$listOwnerId = r4;
                this.val$listId = r5;
                this.val$cursor = r6;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotUserListMembers(AsyncTwitter.access$000(this.this$0).getUserListMembers(this.val$listOwnerId, this.val$listId, this.val$cursor));
            }
        });
    }

    public void addUserListMember(int listId, int userId) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.ADD_LIST_MEMBER, this.listener, listId, userId) {
            private final AsyncTwitter this$0;
            private final int val$listId;
            private final int val$userId;

            {
                this.this$0 = r1;
                this.val$listId = r4;
                this.val$userId = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.addedUserListMember(AsyncTwitter.access$000(this.this$0).addUserListMember(this.val$listId, this.val$userId));
            }
        });
    }

    public void addUserListMembers(int listId, int[] userIds) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.ADD_LIST_MEMBERS, this.listener, listId, userIds) {
            private final AsyncTwitter this$0;
            private final int val$listId;
            private final int[] val$userIds;

            {
                this.this$0 = r1;
                this.val$listId = r4;
                this.val$userIds = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.addedUserListMembers(AsyncTwitter.access$000(this.this$0).addUserListMembers(this.val$listId, this.val$userIds));
            }
        });
    }

    public void addUserListMembers(int listId, String[] screenNames) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.ADD_LIST_MEMBERS, this.listener, listId, screenNames) {
            private final AsyncTwitter this$0;
            private final int val$listId;
            private final String[] val$screenNames;

            {
                this.this$0 = r1;
                this.val$listId = r4;
                this.val$screenNames = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.addedUserListMembers(AsyncTwitter.access$000(this.this$0).addUserListMembers(this.val$listId, this.val$screenNames));
            }
        });
    }

    public void deleteUserListMember(int listId, int userId) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.DELETE_LIST_MEMBER, this.listener, listId, userId) {
            private final AsyncTwitter this$0;
            private final int val$listId;
            private final int val$userId;

            {
                this.this$0 = r1;
                this.val$listId = r4;
                this.val$userId = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.deletedUserListMember(AsyncTwitter.access$000(this.this$0).deleteUserListMember(this.val$listId, this.val$userId));
            }
        });
    }

    public void checkUserListMembership(String listOwnerScreenName, int listId, int userId) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.CHECK_LIST_MEMBERSHIP, this.listener, listOwnerScreenName, listId, userId) {
            private final AsyncTwitter this$0;
            private final int val$listId;
            private final String val$listOwnerScreenName;
            private final int val$userId;

            {
                this.this$0 = r1;
                this.val$listOwnerScreenName = r4;
                this.val$listId = r5;
                this.val$userId = r6;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.checkedUserListMembership(AsyncTwitter.access$000(this.this$0).checkUserListMembership(this.val$listOwnerScreenName, this.val$listId, this.val$userId));
            }
        });
    }

    public void getUserListSubscribers(String listOwnerScreenName, int listId, long cursor) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.LIST_SUBSCRIBERS, this.listener, listOwnerScreenName, listId, cursor) {
            private final AsyncTwitter this$0;
            private final long val$cursor;
            private final int val$listId;
            private final String val$listOwnerScreenName;

            {
                this.this$0 = r1;
                this.val$listOwnerScreenName = r4;
                this.val$listId = r5;
                this.val$cursor = r6;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotUserListSubscribers(AsyncTwitter.access$000(this.this$0).getUserListSubscribers(this.val$listOwnerScreenName, this.val$listId, this.val$cursor));
            }
        });
    }

    public void subscribeUserList(String listOwnerScreenName, int listId) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.SUBSCRIBE_LIST, this.listener, listOwnerScreenName, listId) {
            private final AsyncTwitter this$0;
            private final int val$listId;
            private final String val$listOwnerScreenName;

            {
                this.this$0 = r1;
                this.val$listOwnerScreenName = r4;
                this.val$listId = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.subscribedUserList(AsyncTwitter.access$000(this.this$0).subscribeUserList(this.val$listOwnerScreenName, this.val$listId));
            }
        });
    }

    public void unsubscribeUserList(String listOwnerScreenName, int listId) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.UNSUBSCRIBE_LIST, this.listener, listOwnerScreenName, listId) {
            private final AsyncTwitter this$0;
            private final int val$listId;
            private final String val$listOwnerScreenName;

            {
                this.this$0 = r1;
                this.val$listOwnerScreenName = r4;
                this.val$listId = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.unsubscribedUserList(AsyncTwitter.access$000(this.this$0).unsubscribeUserList(this.val$listOwnerScreenName, this.val$listId));
            }
        });
    }

    public void checkUserListSubscription(String listOwnerScreenName, int listId, int userId) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.CHECK_LIST_SUBSCRIPTION, this.listener, listOwnerScreenName, listId, userId) {
            private final AsyncTwitter this$0;
            private final int val$listId;
            private final String val$listOwnerScreenName;
            private final int val$userId;

            {
                this.this$0 = r1;
                this.val$listOwnerScreenName = r4;
                this.val$listId = r5;
                this.val$userId = r6;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.checkedUserListSubscription(AsyncTwitter.access$000(this.this$0).checkUserListSubscription(this.val$listOwnerScreenName, this.val$listId, this.val$userId));
            }
        });
    }

    public void getDirectMessages() {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.DIRECT_MESSAGES, this.listener) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotDirectMessages(AsyncTwitter.access$000(this.this$0).getDirectMessages());
            }
        });
    }

    public void getDirectMessages(Paging paging) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.DIRECT_MESSAGES, this.listener, paging) {
            private final AsyncTwitter this$0;
            private final Paging val$paging;

            {
                this.this$0 = r1;
                this.val$paging = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotDirectMessages(AsyncTwitter.access$000(this.this$0).getDirectMessages(this.val$paging));
            }
        });
    }

    public void getSentDirectMessages() {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.SENT_DIRECT_MESSAGES, this.listener) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotSentDirectMessages(AsyncTwitter.access$000(this.this$0).getSentDirectMessages());
            }
        });
    }

    public void getSentDirectMessages(Paging paging) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.SENT_DIRECT_MESSAGES, this.listener, paging) {
            private final AsyncTwitter this$0;
            private final Paging val$paging;

            {
                this.this$0 = r1;
                this.val$paging = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotSentDirectMessages(AsyncTwitter.access$000(this.this$0).getSentDirectMessages(this.val$paging));
            }
        });
    }

    public void sendDirectMessage(String screenName, String text) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.SEND_DIRECT_MESSAGE, this.listener, screenName, text) {
            private final AsyncTwitter this$0;
            private final String val$screenName;
            private final String val$text;

            {
                this.this$0 = r1;
                this.val$screenName = r4;
                this.val$text = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.sentDirectMessage(AsyncTwitter.access$000(this.this$0).sendDirectMessage(this.val$screenName, this.val$text));
            }
        });
    }

    public void sendDirectMessage(int userId, String text) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.SEND_DIRECT_MESSAGE, this.listener, userId, text) {
            private final AsyncTwitter this$0;
            private final String val$text;
            private final int val$userId;

            {
                this.this$0 = r1;
                this.val$userId = r4;
                this.val$text = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.sentDirectMessage(AsyncTwitter.access$000(this.this$0).sendDirectMessage(this.val$userId, this.val$text));
            }
        });
    }

    public void destroyDirectMessage(long id) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.DESTROY_DIRECT_MESSAGE, this.listener, id) {
            private final AsyncTwitter this$0;
            private final long val$id;

            {
                this.this$0 = r1;
                this.val$id = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.destroyedDirectMessage(AsyncTwitter.access$000(this.this$0).destroyDirectMessage(this.val$id));
            }
        });
    }

    public void showDirectMessage(long id) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.DIRECT_MESSAGE, this.listener, id) {
            private final AsyncTwitter this$0;
            private final long val$id;

            {
                this.this$0 = r1;
                this.val$id = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotDirectMessage(AsyncTwitter.access$000(this.this$0).showDirectMessage(this.val$id));
            }
        });
    }

    public void createFriendship(String screenName) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.CREATE_FRIENDSHIP, this.listener, screenName) {
            private final AsyncTwitter this$0;
            private final String val$screenName;

            {
                this.this$0 = r1;
                this.val$screenName = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.createdFriendship(AsyncTwitter.access$000(this.this$0).createFriendship(this.val$screenName));
            }
        });
    }

    public void createFriendship(int userId) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.CREATE_FRIENDSHIP, this.listener, userId) {
            private final AsyncTwitter this$0;
            private final int val$userId;

            {
                this.this$0 = r1;
                this.val$userId = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.createdFriendship(AsyncTwitter.access$000(this.this$0).createFriendship(this.val$userId));
            }
        });
    }

    public void createFriendship(String screenName, boolean follow) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.CREATE_FRIENDSHIP, this.listener, screenName, follow) {
            private final AsyncTwitter this$0;
            private final boolean val$follow;
            private final String val$screenName;

            {
                this.this$0 = r1;
                this.val$screenName = r4;
                this.val$follow = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.createdFriendship(AsyncTwitter.access$000(this.this$0).createFriendship(this.val$screenName, this.val$follow));
            }
        });
    }

    public void createFriendship(int userId, boolean follow) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.CREATE_FRIENDSHIP, this.listener, userId, follow) {
            private final AsyncTwitter this$0;
            private final boolean val$follow;
            private final int val$userId;

            {
                this.this$0 = r1;
                this.val$userId = r4;
                this.val$follow = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.createdFriendship(AsyncTwitter.access$000(this.this$0).createFriendship(this.val$userId, this.val$follow));
            }
        });
    }

    public void destroyFriendship(String screenName) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.DESTROY_FRIENDSHIP, this.listener, screenName) {
            private final AsyncTwitter this$0;
            private final String val$screenName;

            {
                this.this$0 = r1;
                this.val$screenName = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.destroyedFriendship(AsyncTwitter.access$000(this.this$0).destroyFriendship(this.val$screenName));
            }
        });
    }

    public void destroyFriendship(int userId) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.DESTROY_FRIENDSHIP, this.listener, userId) {
            private final AsyncTwitter this$0;
            private final int val$userId;

            {
                this.this$0 = r1;
                this.val$userId = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.destroyedFriendship(AsyncTwitter.access$000(this.this$0).destroyFriendship(this.val$userId));
            }
        });
    }

    public void existsFriendship(String userA, String userB) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.EXISTS_FRIENDSHIP, this.listener, userA, userB) {
            private final AsyncTwitter this$0;
            private final String val$userA;
            private final String val$userB;

            {
                this.this$0 = r1;
                this.val$userA = r4;
                this.val$userB = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotExistsFriendship(AsyncTwitter.access$000(this.this$0).existsFriendship(this.val$userA, this.val$userB));
            }
        });
    }

    public void showFriendship(String sourceScreenName, String targetScreenName) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.SHOW_FRIENDSHIP, this.listener, sourceScreenName, targetScreenName) {
            private final AsyncTwitter this$0;
            private final String val$sourceScreenName;
            private final String val$targetScreenName;

            {
                this.this$0 = r1;
                this.val$sourceScreenName = r4;
                this.val$targetScreenName = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotShowFriendship(AsyncTwitter.access$000(this.this$0).showFriendship(this.val$sourceScreenName, this.val$targetScreenName));
            }
        });
    }

    public void showFriendship(int sourceId, int targetId) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.SHOW_FRIENDSHIP, this.listener, sourceId, targetId) {
            private final AsyncTwitter this$0;
            private final int val$sourceId;
            private final int val$targetId;

            {
                this.this$0 = r1;
                this.val$sourceId = r4;
                this.val$targetId = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotShowFriendship(AsyncTwitter.access$000(this.this$0).showFriendship(this.val$sourceId, this.val$targetId));
            }
        });
    }

    public void getIncomingFriendships(long cursor) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.INCOMING_FRIENDSHIPS, this.listener, cursor) {
            private final AsyncTwitter this$0;
            private final long val$cursor;

            {
                this.this$0 = r1;
                this.val$cursor = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotIncomingFriendships(AsyncTwitter.access$000(this.this$0).getIncomingFriendships(this.val$cursor));
            }
        });
    }

    public void getOutgoingFriendships(long cursor) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.OUTGOING_FRIENDSHIPS, this.listener, cursor) {
            private final AsyncTwitter this$0;
            private final long val$cursor;

            {
                this.this$0 = r1;
                this.val$cursor = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotOutgoingFriendships(AsyncTwitter.access$000(this.this$0).getOutgoingFriendships(this.val$cursor));
            }
        });
    }

    public void lookupFriendships(String[] screenNames) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.LOOKUP_FRIENDSHIPS, this.listener, screenNames) {
            private final AsyncTwitter this$0;
            private final String[] val$screenNames;

            {
                this.this$0 = r1;
                this.val$screenNames = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.lookedUpFriendships(AsyncTwitter.access$000(this.this$0).lookupFriendships(this.val$screenNames));
            }
        });
    }

    public void lookupFriendships(int[] ids) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.LOOKUP_FRIENDSHIPS, this.listener, ids) {
            private final AsyncTwitter this$0;
            private final int[] val$ids;

            {
                this.this$0 = r1;
                this.val$ids = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.lookedUpFriendships(AsyncTwitter.access$000(this.this$0).lookupFriendships(this.val$ids));
            }
        });
    }

    public void updateFriendship(String screenName, boolean enableDeviceNotification, boolean retweet) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.UPDATE_FRIENDSHIP, this.listener, screenName, enableDeviceNotification, retweet) {
            private final AsyncTwitter this$0;
            private final boolean val$enableDeviceNotification;
            private final boolean val$retweet;
            private final String val$screenName;

            {
                this.this$0 = r1;
                this.val$screenName = r4;
                this.val$enableDeviceNotification = r5;
                this.val$retweet = r6;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.updatedFriendship(AsyncTwitter.access$000(this.this$0).updateFriendship(this.val$screenName, this.val$enableDeviceNotification, this.val$retweet));
            }
        });
    }

    public void updateFriendship(int userId, boolean enableDeviceNotification, boolean retweet) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.UPDATE_FRIENDSHIP, this.listener, userId, enableDeviceNotification, retweet) {
            private final AsyncTwitter this$0;
            private final boolean val$enableDeviceNotification;
            private final boolean val$retweet;
            private final int val$userId;

            {
                this.this$0 = r1;
                this.val$userId = r4;
                this.val$enableDeviceNotification = r5;
                this.val$retweet = r6;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.updatedFriendship(AsyncTwitter.access$000(this.this$0).updateFriendship(this.val$userId, this.val$enableDeviceNotification, this.val$retweet));
            }
        });
    }

    public void getFriendsIDs() {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.FRIENDS_IDS, this.listener) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotFriendsIDs(AsyncTwitter.access$000(this.this$0).getFriendsIDs());
            }
        });
    }

    public void getFriendsIDs(long cursor) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.FRIENDS_IDS, this.listener, cursor) {
            private final AsyncTwitter this$0;
            private final long val$cursor;

            {
                this.this$0 = r1;
                this.val$cursor = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotFriendsIDs(AsyncTwitter.access$000(this.this$0).getFriendsIDs(this.val$cursor));
            }
        });
    }

    public void getFriendsIDs(int userId) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.FRIENDS_IDS, this.listener, userId) {
            private final AsyncTwitter this$0;
            private final int val$userId;

            {
                this.this$0 = r1;
                this.val$userId = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotFriendsIDs(AsyncTwitter.access$000(this.this$0).getFriendsIDs(this.val$userId));
            }
        });
    }

    public void getFriendsIDs(int userId, long cursor) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.FRIENDS_IDS, this.listener, userId, cursor) {
            private final AsyncTwitter this$0;
            private final long val$cursor;
            private final int val$userId;

            {
                this.this$0 = r1;
                this.val$userId = r4;
                this.val$cursor = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotFriendsIDs(AsyncTwitter.access$000(this.this$0).getFriendsIDs(this.val$userId, this.val$cursor));
            }
        });
    }

    public void getFriendsIDs(String screenName) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.FRIENDS_IDS, this.listener, screenName) {
            private final AsyncTwitter this$0;
            private final String val$screenName;

            {
                this.this$0 = r1;
                this.val$screenName = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotFriendsIDs(AsyncTwitter.access$000(this.this$0).getFriendsIDs(this.val$screenName));
            }
        });
    }

    public void getFriendsIDs(String screenName, long cursor) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.FRIENDS_IDS, this.listener, screenName, cursor) {
            private final AsyncTwitter this$0;
            private final long val$cursor;
            private final String val$screenName;

            {
                this.this$0 = r1;
                this.val$screenName = r4;
                this.val$cursor = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotFriendsIDs(AsyncTwitter.access$000(this.this$0).getFriendsIDs(this.val$screenName, this.val$cursor));
            }
        });
    }

    public void getFollowersIDs() {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.FOLLOWERS_IDS, this.listener) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotFollowersIDs(AsyncTwitter.access$000(this.this$0).getFollowersIDs());
            }
        });
    }

    public void getFollowersIDs(long cursor) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.FOLLOWERS_IDS, this.listener, cursor) {
            private final AsyncTwitter this$0;
            private final long val$cursor;

            {
                this.this$0 = r1;
                this.val$cursor = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotFollowersIDs(AsyncTwitter.access$000(this.this$0).getFollowersIDs(this.val$cursor));
            }
        });
    }

    public void getFollowersIDs(int userId) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.FOLLOWERS_IDS, this.listener, userId) {
            private final AsyncTwitter this$0;
            private final int val$userId;

            {
                this.this$0 = r1;
                this.val$userId = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotFollowersIDs(AsyncTwitter.access$000(this.this$0).getFollowersIDs(this.val$userId));
            }
        });
    }

    public void getFollowersIDs(int userId, long cursor) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.FOLLOWERS_IDS, this.listener, userId, cursor) {
            private final AsyncTwitter this$0;
            private final long val$cursor;
            private final int val$userId;

            {
                this.this$0 = r1;
                this.val$userId = r4;
                this.val$cursor = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotFollowersIDs(AsyncTwitter.access$000(this.this$0).getFollowersIDs(this.val$userId, this.val$cursor));
            }
        });
    }

    public void getFollowersIDs(String screenName) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.FOLLOWERS_IDS, this.listener, screenName) {
            private final AsyncTwitter this$0;
            private final String val$screenName;

            {
                this.this$0 = r1;
                this.val$screenName = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotFollowersIDs(AsyncTwitter.access$000(this.this$0).getFollowersIDs(this.val$screenName));
            }
        });
    }

    public void getFollowersIDs(String screenName, long cursor) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.FOLLOWERS_IDS, this.listener, screenName, cursor) {
            private final AsyncTwitter this$0;
            private final long val$cursor;
            private final String val$screenName;

            {
                this.this$0 = r1;
                this.val$screenName = r4;
                this.val$cursor = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotFollowersIDs(AsyncTwitter.access$000(this.this$0).getFollowersIDs(this.val$screenName, this.val$cursor));
            }
        });
    }

    public void verifyCredentials() {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.VERIFY_CREDENTIALS, this.listener) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.verifiedCredentials(AsyncTwitter.access$000(this.this$0).verifyCredentials());
            }
        });
    }

    public void updateProfile(String name, String url, String location, String description) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.UPDATE_PROFILE, this.listener, name, url, location, description) {
            private final AsyncTwitter this$0;
            private final String val$description;
            private final String val$location;
            private final String val$name;
            private final String val$url;

            {
                this.this$0 = r1;
                this.val$name = r4;
                this.val$url = r5;
                this.val$location = r6;
                this.val$description = r7;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.updatedProfile(AsyncTwitter.access$000(this.this$0).updateProfile(this.val$name, this.val$url, this.val$location, this.val$description));
            }
        });
    }

    public void getRateLimitStatus() {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.RATE_LIMIT_STATUS, this.listener) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotRateLimitStatus(AsyncTwitter.access$000(this.this$0).getRateLimitStatus());
            }
        });
    }

    public void updateProfileColors(String profileBackgroundColor, String profileTextColor, String profileLinkColor, String profileSidebarFillColor, String profileSidebarBorderColor) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.UPDATE_PROFILE_COLORS, this.listener, profileBackgroundColor, profileTextColor, profileLinkColor, profileSidebarFillColor, profileSidebarBorderColor) {
            private final AsyncTwitter this$0;
            private final String val$profileBackgroundColor;
            private final String val$profileLinkColor;
            private final String val$profileSidebarBorderColor;
            private final String val$profileSidebarFillColor;
            private final String val$profileTextColor;

            {
                this.this$0 = r1;
                this.val$profileBackgroundColor = r4;
                this.val$profileTextColor = r5;
                this.val$profileLinkColor = r6;
                this.val$profileSidebarFillColor = r7;
                this.val$profileSidebarBorderColor = r8;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.updatedProfileColors(AsyncTwitter.access$000(this.this$0).updateProfileColors(this.val$profileBackgroundColor, this.val$profileTextColor, this.val$profileLinkColor, this.val$profileSidebarFillColor, this.val$profileSidebarBorderColor));
            }
        });
    }

    public void updateProfileImage(File image) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.UPDATE_PROFILE_IMAGE, this.listener, image) {
            private final AsyncTwitter this$0;
            private final File val$image;

            {
                this.this$0 = r1;
                this.val$image = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.updatedProfileImage(AsyncTwitter.access$000(this.this$0).updateProfileImage(this.val$image));
            }
        });
    }

    public void updateProfileImage(InputStream image) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.UPDATE_PROFILE_IMAGE, this.listener, image) {
            private final AsyncTwitter this$0;
            private final InputStream val$image;

            {
                this.this$0 = r1;
                this.val$image = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.updatedProfileImage(AsyncTwitter.access$000(this.this$0).updateProfileImage(this.val$image));
            }
        });
    }

    public void updateProfileBackgroundImage(File image, boolean tile) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.UPDATE_PROFILE_BACKGROUND_IMAGE, this.listener, image, tile) {
            private final AsyncTwitter this$0;
            private final File val$image;
            private final boolean val$tile;

            {
                this.this$0 = r1;
                this.val$image = r4;
                this.val$tile = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.updatedProfileBackgroundImage(AsyncTwitter.access$000(this.this$0).updateProfileBackgroundImage(this.val$image, this.val$tile));
            }
        });
    }

    public void updateProfileBackgroundImage(InputStream image, boolean tile) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.UPDATE_PROFILE_BACKGROUND_IMAGE, this.listener, image, tile) {
            private final AsyncTwitter this$0;
            private final InputStream val$image;
            private final boolean val$tile;

            {
                this.this$0 = r1;
                this.val$image = r4;
                this.val$tile = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.updatedProfileBackgroundImage(AsyncTwitter.access$000(this.this$0).updateProfileBackgroundImage(this.val$image, this.val$tile));
            }
        });
    }

    public void getFavorites() {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.FAVORITES, this.listener) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotFavorites(AsyncTwitter.access$000(this.this$0).getFavorites());
            }
        });
    }

    public void getFavorites(int page) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.FAVORITES, this.listener, page) {
            private final AsyncTwitter this$0;
            private final int val$page;

            {
                this.this$0 = r1;
                this.val$page = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotFavorites(AsyncTwitter.access$000(this.this$0).getFavorites(this.val$page));
            }
        });
    }

    public void getFavorites(String id) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.FAVORITES, this.listener, id) {
            private final AsyncTwitter this$0;
            private final String val$id;

            {
                this.this$0 = r1;
                this.val$id = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotFavorites(AsyncTwitter.access$000(this.this$0).getFavorites(this.val$id));
            }
        });
    }

    public void getFavorites(String id, int page) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.FAVORITES, this.listener, id, page) {
            private final AsyncTwitter this$0;
            private final String val$id;
            private final int val$page;

            {
                this.this$0 = r1;
                this.val$id = r4;
                this.val$page = r5;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotFavorites(AsyncTwitter.access$000(this.this$0).getFavorites(this.val$id, this.val$page));
            }
        });
    }

    public void createFavorite(long id) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.CREATE_FAVORITE, this.listener, id) {
            private final AsyncTwitter this$0;
            private final long val$id;

            {
                this.this$0 = r1;
                this.val$id = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.createdFavorite(AsyncTwitter.access$000(this.this$0).createFavorite(this.val$id));
            }
        });
    }

    public void destroyFavorite(long id) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.DESTROY_FAVORITE, this.listener, id) {
            private final AsyncTwitter this$0;
            private final long val$id;

            {
                this.this$0 = r1;
                this.val$id = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.destroyedFavorite(AsyncTwitter.access$000(this.this$0).destroyFavorite(this.val$id));
            }
        });
    }

    public void enableNotification(String screenName) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.ENABLE_NOTIFICATION, this.listener, screenName) {
            private final AsyncTwitter this$0;
            private final String val$screenName;

            {
                this.this$0 = r1;
                this.val$screenName = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.enabledNotification(AsyncTwitter.access$000(this.this$0).enableNotification(this.val$screenName));
            }
        });
    }

    public void enableNotification(int userId) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.ENABLE_NOTIFICATION, this.listener, userId) {
            private final AsyncTwitter this$0;
            private final int val$userId;

            {
                this.this$0 = r1;
                this.val$userId = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.enabledNotification(AsyncTwitter.access$000(this.this$0).enableNotification(this.val$userId));
            }
        });
    }

    public void disableNotification(String screenName) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.DISABLE_NOTIFICATION, this.listener, screenName) {
            private final AsyncTwitter this$0;
            private final String val$screenName;

            {
                this.this$0 = r1;
                this.val$screenName = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.disabledNotification(AsyncTwitter.access$000(this.this$0).disableNotification(this.val$screenName));
            }
        });
    }

    public void disableNotification(int userId) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.DISABLE_NOTIFICATION, this.listener, userId) {
            private final AsyncTwitter this$0;
            private final int val$userId;

            {
                this.this$0 = r1;
                this.val$userId = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.disabledNotification(AsyncTwitter.access$000(this.this$0).disableNotification(this.val$userId));
            }
        });
    }

    public void createBlock(String screenName) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.CREATE_BLOCK, this.listener, screenName) {
            private final AsyncTwitter this$0;
            private final String val$screenName;

            {
                this.this$0 = r1;
                this.val$screenName = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.createdBlock(AsyncTwitter.access$000(this.this$0).createBlock(this.val$screenName));
            }
        });
    }

    public void createBlock(int userId) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.CREATE_BLOCK, this.listener, userId) {
            private final AsyncTwitter this$0;
            private final int val$userId;

            {
                this.this$0 = r1;
                this.val$userId = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.createdBlock(AsyncTwitter.access$000(this.this$0).createBlock(this.val$userId));
            }
        });
    }

    public void destroyBlock(String screenName) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.DESTROY_BLOCK, this.listener, screenName) {
            private final AsyncTwitter this$0;
            private final String val$screenName;

            {
                this.this$0 = r1;
                this.val$screenName = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.destroyedBlock(AsyncTwitter.access$000(this.this$0).destroyBlock(this.val$screenName));
            }
        });
    }

    public void destroyBlock(int userId) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.DESTROY_BLOCK, this.listener, userId) {
            private final AsyncTwitter this$0;
            private final int val$userId;

            {
                this.this$0 = r1;
                this.val$userId = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.destroyedBlock(AsyncTwitter.access$000(this.this$0).destroyBlock(this.val$userId));
            }
        });
    }

    public void existsBlock(String screenName) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.EXISTS_BLOCK, this.listener, screenName) {
            private final AsyncTwitter this$0;
            private final String val$screenName;

            {
                this.this$0 = r1;
                this.val$screenName = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotExistsBlock(AsyncTwitter.access$000(this.this$0).existsBlock(this.val$screenName));
            }
        });
    }

    public void existsBlock(int userId) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.EXISTS_BLOCK, this.listener, userId) {
            private final AsyncTwitter this$0;
            private final int val$userId;

            {
                this.this$0 = r1;
                this.val$userId = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotExistsBlock(AsyncTwitter.access$000(this.this$0).existsBlock(this.val$userId));
            }
        });
    }

    public void getBlockingUsers() {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.BLOCKING_USERS, this.listener) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotBlockingUsers(AsyncTwitter.access$000(this.this$0).getBlockingUsers());
            }
        });
    }

    public void getBlockingUsers(int page) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.BLOCKING_USERS, this.listener, page) {
            private final AsyncTwitter this$0;
            private final int val$page;

            {
                this.this$0 = r1;
                this.val$page = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotBlockingUsers(AsyncTwitter.access$000(this.this$0).getBlockingUsers(this.val$page));
            }
        });
    }

    public void getBlockingUsersIDs() {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.BLOCKING_USERS_IDS, this.listener) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotBlockingUsersIDs(AsyncTwitter.access$000(this.this$0).getBlockingUsersIDs());
            }
        });
    }

    public void reportSpam(int userId) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.REPORT_SPAM, this.listener, userId) {
            private final AsyncTwitter this$0;
            private final int val$userId;

            {
                this.this$0 = r1;
                this.val$userId = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.reportedSpam(AsyncTwitter.access$000(this.this$0).reportSpam(this.val$userId));
            }
        });
    }

    public void reportSpam(String screenName) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.REPORT_SPAM, this.listener, screenName) {
            private final AsyncTwitter this$0;
            private final String val$screenName;

            {
                this.this$0 = r1;
                this.val$screenName = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.reportedSpam(AsyncTwitter.access$000(this.this$0).reportSpam(this.val$screenName));
            }
        });
    }

    public void getAvailableTrends() {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.AVAILABLE_TRENDS, this.listener) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotAvailableTrends(AsyncTwitter.access$000(this.this$0).getAvailableTrends());
            }
        });
    }

    public void getAvailableTrends(GeoLocation location) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.AVAILABLE_TRENDS, this.listener, location) {
            private final AsyncTwitter this$0;
            private final GeoLocation val$location;

            {
                this.this$0 = r1;
                this.val$location = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotAvailableTrends(AsyncTwitter.access$000(this.this$0).getAvailableTrends(this.val$location));
            }
        });
    }

    public void getLocationTrends(int woeid) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.LOCATION_TRENDS, this.listener, woeid) {
            private final AsyncTwitter this$0;
            private final int val$woeid;

            {
                this.this$0 = r1;
                this.val$woeid = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotLocationTrends(AsyncTwitter.access$000(this.this$0).getLocationTrends(this.val$woeid));
            }
        });
    }

    public void searchPlaces(GeoQuery query) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.SEARCH_PLACES, this.listener, query) {
            private final AsyncTwitter this$0;
            private final GeoQuery val$query;

            {
                this.this$0 = r1;
                this.val$query = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.searchedPlaces(AsyncTwitter.access$000(this.this$0).searchPlaces(this.val$query));
            }
        });
    }

    public void getSimilarPlaces(GeoLocation location, String name, String containedWithin, String streetAddress) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.SIMILAR_PLACES, this.listener, location, name, containedWithin, streetAddress) {
            private final AsyncTwitter this$0;
            private final String val$containedWithin;
            private final GeoLocation val$location;
            private final String val$name;
            private final String val$streetAddress;

            {
                this.this$0 = r1;
                this.val$location = r4;
                this.val$name = r5;
                this.val$containedWithin = r6;
                this.val$streetAddress = r7;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotSimilarPlaces(AsyncTwitter.access$000(this.this$0).getSimilarPlaces(this.val$location, this.val$name, this.val$containedWithin, this.val$streetAddress));
            }
        });
    }

    public void getNearbyPlaces(GeoQuery query) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.NEAR_BY_PLACES, this.listener, query) {
            private final AsyncTwitter this$0;
            private final GeoQuery val$query;

            {
                this.this$0 = r1;
                this.val$query = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotNearByPlaces(AsyncTwitter.access$000(this.this$0).getNearbyPlaces(this.val$query));
            }
        });
    }

    public void reverseGeoCode(GeoQuery query) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.REVERSE_GEO_CODE, this.listener, query) {
            private final AsyncTwitter this$0;
            private final GeoQuery val$query;

            {
                this.this$0 = r1;
                this.val$query = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotReverseGeoCode(AsyncTwitter.access$000(this.this$0).reverseGeoCode(this.val$query));
            }
        });
    }

    public void getGeoDetails(String id) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.GEO_DETAILS, this.listener, id) {
            private final AsyncTwitter this$0;
            private final String val$id;

            {
                this.this$0 = r1;
                this.val$id = r4;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotGeoDetails(AsyncTwitter.access$000(this.this$0).getGeoDetails(this.val$id));
            }
        });
    }

    public void createPlace(String name, String containedWithin, String token, GeoLocation location, String streetAddress) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.CREATE_PLACE, this.listener, name, containedWithin, token, location, streetAddress) {
            private final AsyncTwitter this$0;
            private final String val$containedWithin;
            private final GeoLocation val$location;
            private final String val$name;
            private final String val$streetAddress;
            private final String val$token;

            {
                this.this$0 = r1;
                this.val$name = r4;
                this.val$containedWithin = r5;
                this.val$token = r6;
                this.val$location = r7;
                this.val$streetAddress = r8;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.createdPlace(AsyncTwitter.access$000(this.this$0).createPlace(this.val$name, this.val$containedWithin, this.val$token, this.val$location, this.val$streetAddress));
            }
        });
    }

    public void getTermsOfService() {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.TERMS_OF_SERVICE, this.listener) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotTermsOfService(AsyncTwitter.access$000(this.this$0).getTermsOfService());
            }
        });
    }

    public void getPrivacyPolicy() {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.PRIVACY_POLICY, this.listener) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotPrivacyPolicy(AsyncTwitter.access$000(this.this$0).getPrivacyPolicy());
            }
        });
    }

    public void getRelatedResults(long statusId) {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.RELATED_RESULTS, this.listener, statusId) {
            private final AsyncTwitter this$0;
            private final long val$statusId;

            {
                this.this$0 = r1;
                this.val$statusId = r4;
            }

            /* access modifiers changed from: package-private */
            public void invoke(TwitterListener listener) throws TwitterException {
                listener.gotRelatedResults(AsyncTwitter.access$000(this.this$0).getRelatedResults(this.val$statusId));
            }
        });
    }

    public void test() {
        getDispatcher().invokeLater(new AsyncTask(this, TwitterMethod.TEST, this.listener) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener) throws TwitterException {
                listener.tested(AsyncTwitter.access$000(this.this$0).test());
            }
        });
    }

    public void shutdown() {
        Class cls;
        super.shutdown();
        if (class$twitter4j$AsyncTwitter == null) {
            cls = class$("twitter4j.AsyncTwitter");
            class$twitter4j$AsyncTwitter = cls;
        } else {
            cls = class$twitter4j$AsyncTwitter;
        }
        synchronized (cls) {
            if (dispatcher != null) {
                dispatcher.shutdown();
                dispatcher = null;
            }
        }
    }

    static Class class$(String x0) {
        try {
            return Class.forName(x0);
        } catch (ClassNotFoundException x1) {
            throw new NoClassDefFoundError().initCause(x1);
        }
    }

    private Dispatcher getDispatcher() {
        Class cls;
        if (dispatcher == null) {
            if (class$twitter4j$TwitterStream == null) {
                cls = class$("twitter4j.TwitterStream");
                class$twitter4j$TwitterStream = cls;
            } else {
                cls = class$twitter4j$TwitterStream;
            }
            synchronized (cls) {
                if (dispatcher == null) {
                    dispatcher = new DispatcherFactory(this.conf).getInstance();
                }
            }
        }
        return dispatcher;
    }

    public void setOAuthConsumer(String consumerKey, String consumerSecret) {
        this.twitter.setOAuthConsumer(consumerKey, consumerSecret);
    }

    public RequestToken getOAuthRequestToken() throws TwitterException {
        return this.twitter.getOAuthRequestToken();
    }

    public RequestToken getOAuthRequestToken(String callbackUrl) throws TwitterException {
        return this.twitter.getOAuthRequestToken(callbackUrl);
    }

    public AccessToken getOAuthAccessToken() throws TwitterException {
        return this.twitter.getOAuthAccessToken();
    }

    public AccessToken getOAuthAccessToken(String oauthVerifier) throws TwitterException {
        return this.twitter.getOAuthAccessToken(oauthVerifier);
    }

    public AccessToken getOAuthAccessToken(RequestToken requestToken) throws TwitterException {
        return this.twitter.getOAuthAccessToken(requestToken);
    }

    public AccessToken getOAuthAccessToken(RequestToken requestToken, String oauthVerifier) throws TwitterException {
        return this.twitter.getOAuthAccessToken(requestToken, oauthVerifier);
    }

    public void setOAuthAccessToken(AccessToken accessToken) {
        this.twitter.setOAuthAccessToken(accessToken);
    }

    public AccessToken getOAuthAccessToken(String token, String tokenSecret) throws TwitterException {
        return this.twitter.getOAuthAccessToken(token, tokenSecret);
    }

    public AccessToken getOAuthAccessToken(String token, String tokenSecret, String pin) throws TwitterException {
        return this.twitter.getOAuthAccessToken(new RequestToken(token, tokenSecret), pin);
    }

    public void setOAuthAccessToken(String token, String tokenSecret) {
        this.twitter.setOAuthAccessToken(token, tokenSecret);
    }

    abstract class AsyncTask implements Runnable {
        TwitterListener listener;
        TwitterMethod method;
        private final AsyncTwitter this$0;

        /* access modifiers changed from: package-private */
        public abstract void invoke(TwitterListener twitterListener) throws TwitterException;

        AsyncTask(AsyncTwitter asyncTwitter, TwitterMethod method2, TwitterListener listener2) {
            this.this$0 = asyncTwitter;
            this.method = method2;
            this.listener = listener2;
        }

        public void run() {
            try {
                invoke(this.listener);
            } catch (TwitterException e) {
                TwitterException te = e;
                if (this.listener != null) {
                    this.listener.onException(te, this.method);
                }
            }
        }
    }
}
