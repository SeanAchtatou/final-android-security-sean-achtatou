package twitter4j.internal.logging;

import org.apache.commons.logging.Log;

final class CommonsLoggingLogger extends Logger {
    private final Log LOGGER;

    CommonsLoggingLogger(Log logger) {
        this.LOGGER = logger;
    }

    public boolean isDebugEnabled() {
        return this.LOGGER.isDebugEnabled();
    }

    public boolean isInfoEnabled() {
        return this.LOGGER.isInfoEnabled();
    }

    public boolean isWarnEnabled() {
        return this.LOGGER.isWarnEnabled();
    }

    public void debug(String message) {
        this.LOGGER.debug(message);
    }

    public void debug(String message, String message2) {
        this.LOGGER.debug(new StringBuffer().append(message).append(message2).toString());
    }

    public void info(String message) {
        this.LOGGER.info(message);
    }

    public void info(String message, String message2) {
        this.LOGGER.info(new StringBuffer().append(message).append(message2).toString());
    }

    public void warn(String message) {
        this.LOGGER.warn(message);
    }

    public void warn(String message, String message2) {
        this.LOGGER.warn(new StringBuffer().append(message).append(message2).toString());
    }
}
