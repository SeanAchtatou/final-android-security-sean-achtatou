package twitter4j;

import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public interface ProfileImage extends TwitterResponse, Serializable {
    public static final ImageSize BIGGER = new ImageSize("bigger", null);
    public static final ImageSize MINI = new ImageSize("mini", null);
    public static final ImageSize NORMAL = new ImageSize("normal", null);

    /* renamed from: twitter4j.ProfileImage$1  reason: invalid class name */
    static class AnonymousClass1 {
    }

    String getURL();

    public static class ImageSize implements Serializable {
        private static final Map<String, ImageSize> instances = new HashMap();
        private static final long serialVersionUID = 3363026523372848987L;
        private final String name;

        ImageSize(String x0, AnonymousClass1 x1) {
            this(x0);
        }

        private ImageSize() {
            throw new AssertionError();
        }

        private ImageSize(String name2) {
            this.name = name2;
            instances.put(name2, this);
        }

        public String getName() {
            return this.name;
        }

        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            return this.name.equals(((ImageSize) o).name);
        }

        public int hashCode() {
            return this.name.hashCode();
        }

        public String toString() {
            return this.name;
        }

        private static ImageSize getInstance(String name2) {
            return instances.get(name2);
        }

        private Object readResolve() throws ObjectStreamException {
            return getInstance(this.name);
        }
    }
}
