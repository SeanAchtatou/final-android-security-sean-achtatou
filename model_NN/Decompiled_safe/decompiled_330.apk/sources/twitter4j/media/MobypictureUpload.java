package twitter4j.media;

import twitter4j.TwitterException;
import twitter4j.conf.Configuration;
import twitter4j.http.OAuthAuthorization;
import twitter4j.internal.http.HttpParameter;
import twitter4j.internal.org.json.JSONException;
import twitter4j.internal.org.json.JSONObject;

class MobypictureUpload extends AbstractImageUploadImpl {
    public MobypictureUpload(Configuration conf, String apiKey, OAuthAuthorization oauth) {
        super(conf, apiKey, oauth);
    }

    /* access modifiers changed from: protected */
    public String postUpload() throws TwitterException {
        if (this.httpResponse.getStatusCode() != 200) {
            throw new TwitterException("Mobypic image upload returned invalid status code", this.httpResponse);
        }
        String response = this.httpResponse.asString();
        try {
            JSONObject json = new JSONObject(response);
            if (!json.isNull("media")) {
                return json.getJSONObject("media").getString("mediaurl");
            }
            throw new TwitterException("Unknown Mobypic response", this.httpResponse);
        } catch (JSONException e) {
            throw new TwitterException(new StringBuffer().append("Invalid Mobypic response: ").append(response).toString(), e);
        }
    }

    /* access modifiers changed from: protected */
    public void preUpload() throws TwitterException {
        this.uploadUrl = "https://api.mobypicture.com/2.0/upload.json";
        String verifyCredentialsAuthorizationHeader = generateVerifyCredentialsAuthorizationHeader(AbstractImageUploadImpl.TWITTER_VERIFY_CREDENTIALS_JSON);
        this.headers.put("X-Auth-Service-Provider", AbstractImageUploadImpl.TWITTER_VERIFY_CREDENTIALS_JSON);
        this.headers.put("X-Verify-Credentials-Authorization", verifyCredentialsAuthorizationHeader);
        if (this.apiKey == null) {
            throw new IllegalStateException("No API Key for Mobypic specified. put media.providerAPIKey in twitter4j.properties.");
        }
        HttpParameter[] params = {new HttpParameter("key", this.apiKey), this.image};
        if (this.message != null) {
            params = appendHttpParameters(new HttpParameter[]{this.message}, params);
        }
        this.postParameter = params;
    }
}
