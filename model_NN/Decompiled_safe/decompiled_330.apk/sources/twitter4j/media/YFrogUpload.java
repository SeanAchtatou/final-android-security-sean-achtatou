package twitter4j.media;

import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.Configuration;
import twitter4j.http.OAuthAuthorization;
import twitter4j.internal.http.HttpParameter;

class YFrogUpload extends AbstractImageUploadImpl {
    public YFrogUpload(Configuration conf, OAuthAuthorization oauth) {
        super(conf, oauth);
    }

    /* access modifiers changed from: protected */
    public String postUpload() throws TwitterException {
        if (this.httpResponse.getStatusCode() != 200) {
            throw new TwitterException("YFrog image upload returned invalid status code", this.httpResponse);
        }
        String response = this.httpResponse.asString();
        if (-1 != response.indexOf("<rsp stat=\"fail\">")) {
            throw new TwitterException(new StringBuffer().append("YFrog image upload failed with this error message: ").append(response.substring(response.indexOf("msg") + 5, response.lastIndexOf("\""))).toString(), this.httpResponse);
        } else if (-1 != response.indexOf("<rsp stat=\"ok\">")) {
            return response.substring(response.indexOf("<mediaurl>") + "<mediaurl>".length(), response.indexOf("</mediaurl>"));
        } else {
            throw new TwitterException("Unknown YFrog response", this.httpResponse);
        }
    }

    /* access modifiers changed from: protected */
    public void preUpload() throws TwitterException {
        this.uploadUrl = "https://yfrog.com/api/upload";
        String signedVerifyCredentialsURL = generateVerifyCredentialsAuthorizationURL(AbstractImageUploadImpl.TWITTER_VERIFY_CREDENTIALS_XML);
        HttpParameter[] params = {new HttpParameter("auth", "oauth"), new HttpParameter("username", new TwitterFactory().getInstance(this.oauth).verifyCredentials().getScreenName()), new HttpParameter("verify_url", signedVerifyCredentialsURL), this.image};
        if (this.message != null) {
            params = appendHttpParameters(new HttpParameter[]{this.message}, params);
        }
        this.postParameter = params;
    }
}
