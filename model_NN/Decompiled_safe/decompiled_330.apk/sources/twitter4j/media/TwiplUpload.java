package twitter4j.media;

import twitter4j.TwitterException;
import twitter4j.conf.Configuration;
import twitter4j.http.OAuthAuthorization;
import twitter4j.internal.http.HttpParameter;

class TwiplUpload extends AbstractImageUploadImpl {
    public TwiplUpload(Configuration conf, String apiKey, OAuthAuthorization oauth) {
        super(conf, apiKey, oauth);
    }

    /* access modifiers changed from: protected */
    public String postUpload() throws TwitterException {
        int i;
        int j;
        int j2;
        if (this.httpResponse.getStatusCode() != 200) {
            throw new TwitterException("Twipl image upload returned invalid status code", this.httpResponse);
        }
        String response = this.httpResponse.asString();
        if (-1 != response.indexOf("<response status=\"ok\">")) {
            int i2 = response.indexOf("<mediaurl>");
            if (!(i2 == -1 || (j2 = response.indexOf("</mediaurl>", "<mediaurl>".length() + i2)) == -1)) {
                return response.substring("<mediaurl>".length() + i2, j2);
            }
        } else if (!(-1 == response.indexOf("<rsp status=\"fail\">") || (i = response.indexOf("msg=\"")) == -1 || (j = response.indexOf("\"", "msg=\"".length() + i)) == -1)) {
            throw new TwitterException(new StringBuffer().append("Invalid Twitgoo response: ").append(response.substring("msg=\"".length() + i, j)).toString());
        }
        throw new TwitterException("Unknown Twipl response", this.httpResponse);
    }

    /* access modifiers changed from: protected */
    public void preUpload() throws TwitterException {
        this.uploadUrl = "http://api.twipl.net/2/upload.xml";
        this.headers.put("X-OAUTH-AUTHORIZATION ", generateVerifyCredentialsAuthorizationHeader(AbstractImageUploadImpl.TWITTER_VERIFY_CREDENTIALS_XML));
        this.headers.put("X-OAUTH-SP-URL ", AbstractImageUploadImpl.TWITTER_VERIFY_CREDENTIALS_XML);
        if (this.apiKey == null) {
            throw new IllegalStateException("No API Key for Twipl specified. put media.providerAPIKey in twitter4j.properties.");
        }
        HttpParameter[] params = {new HttpParameter("key", this.apiKey), new HttpParameter("media1", this.image.getFile().getName(), this.image.getFileBody())};
        if (this.message != null) {
            params = appendHttpParameters(new HttpParameter[]{this.message}, params);
        }
        this.postParameter = params;
    }
}
