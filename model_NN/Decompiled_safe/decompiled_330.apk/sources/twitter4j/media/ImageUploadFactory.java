package twitter4j.media;

import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationContext;
import twitter4j.http.Authorization;
import twitter4j.http.AuthorizationFactory;
import twitter4j.http.OAuthAuthorization;

public class ImageUploadFactory {
    private final String apiKey;
    private final Configuration conf;
    private final MediaProvider defaultMediaProvider;

    public ImageUploadFactory() {
        this(ConfigurationContext.getInstance());
    }

    public ImageUploadFactory(Configuration conf2) {
        String mediaProvider = conf2.getMediaProvider().toLowerCase();
        if ("imgly".equals(mediaProvider) || "img_ly".equals(mediaProvider)) {
            this.defaultMediaProvider = MediaProvider.IMG_LY;
        } else if ("plixi".equals(mediaProvider)) {
            this.defaultMediaProvider = MediaProvider.PLIXI;
        } else if ("twipple".equals(mediaProvider)) {
            this.defaultMediaProvider = MediaProvider.TWIPPLE;
        } else if ("twitgoo".equals(mediaProvider)) {
            this.defaultMediaProvider = MediaProvider.TWITGOO;
        } else if ("twitpic".equals(mediaProvider)) {
            this.defaultMediaProvider = MediaProvider.TWITPIC;
        } else if ("yfrog".equals(mediaProvider)) {
            this.defaultMediaProvider = MediaProvider.YFROG;
        } else if ("mobypicture".equals(mediaProvider)) {
            this.defaultMediaProvider = MediaProvider.MOBYPICTURE;
        } else if ("twipl".equals(mediaProvider)) {
            this.defaultMediaProvider = MediaProvider.TWIPL;
        } else if ("posterous".equals(mediaProvider)) {
            this.defaultMediaProvider = MediaProvider.POSTEROUS;
        } else {
            throw new IllegalArgumentException(new StringBuffer().append("unsupported media provider:").append(mediaProvider).toString());
        }
        this.conf = conf2;
        this.apiKey = conf2.getMediaProviderAPIKey();
    }

    public ImageUpload getInstance() {
        return getInstance(this.defaultMediaProvider);
    }

    public ImageUpload getInstance(Authorization authorization) {
        return getInstance(this.defaultMediaProvider, authorization);
    }

    public ImageUpload getInstance(MediaProvider mediaProvider) {
        return getInstance(mediaProvider, AuthorizationFactory.getInstance(this.conf, true));
    }

    public ImageUpload getInstance(MediaProvider mediaProvider, Authorization authorization) {
        if (!(authorization instanceof OAuthAuthorization)) {
            throw new IllegalArgumentException("OAuth authorization is required.");
        }
        OAuthAuthorization oauth = (OAuthAuthorization) authorization;
        if (mediaProvider == MediaProvider.IMG_LY) {
            return new ImgLyUpload(this.conf, oauth);
        }
        if (mediaProvider == MediaProvider.PLIXI) {
            return new PlixiUpload(this.conf, this.apiKey, oauth);
        }
        if (mediaProvider == MediaProvider.TWIPPLE) {
            return new TwippleUpload(this.conf, oauth);
        }
        if (mediaProvider == MediaProvider.TWITGOO) {
            return new TwitgooUpload(this.conf, oauth);
        }
        if (mediaProvider == MediaProvider.TWITPIC) {
            return new TwitpicUpload(this.conf, this.apiKey, oauth);
        }
        if (mediaProvider == MediaProvider.YFROG) {
            return new YFrogUpload(this.conf, oauth);
        }
        if (mediaProvider == MediaProvider.MOBYPICTURE) {
            return new MobypictureUpload(this.conf, this.apiKey, oauth);
        }
        if (mediaProvider == MediaProvider.TWIPL) {
            return new TwiplUpload(this.conf, this.apiKey, oauth);
        }
        if (mediaProvider == MediaProvider.POSTEROUS) {
            return new PosterousUpload(this.conf, oauth);
        }
        throw new AssertionError("Unknown provider");
    }
}
