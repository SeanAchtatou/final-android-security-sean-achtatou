package twitter4j.media;

import twitter4j.conf.Configuration;
import twitter4j.http.Authorization;

public final class ImageUploaderFactory {
    ImageUploadFactory delegate;

    public ImageUploaderFactory() {
        this.delegate = new ImageUploadFactory();
    }

    public ImageUploaderFactory(Configuration conf) {
        this.delegate = new ImageUploadFactory(conf);
    }

    public ImageUpload getInstance() {
        return this.delegate.getInstance();
    }

    public ImageUpload getInstance(Authorization authorization) {
        return this.delegate.getInstance(authorization);
    }

    public ImageUpload getInstance(MediaProvider mediaProvider) {
        return this.delegate.getInstance(mediaProvider);
    }

    public ImageUpload getInstance(MediaProvider mediaProvider, Authorization authorization) {
        return this.delegate.getInstance(mediaProvider, authorization);
    }
}
