package twitter4j.media;

import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class MediaProvider implements Serializable {
    public static MediaProvider IMG_LY = new MediaProvider("IMG_LY");
    public static MediaProvider MOBYPICTURE = new MediaProvider("MOBYPICTURE");
    public static MediaProvider PLIXI = new MediaProvider("PLIXI");
    public static MediaProvider POSTEROUS = new MediaProvider("POSTEROUS");
    public static MediaProvider TWIPL = new MediaProvider("TWIPL");
    public static MediaProvider TWIPPLE = new MediaProvider("TWIPPLE");
    public static MediaProvider TWITGOO = new MediaProvider("TWITGOO");
    public static MediaProvider TWITPIC = new MediaProvider("TWITPIC");
    public static MediaProvider YFROG = new MediaProvider("YFROG");
    private static final Map<String, MediaProvider> instances = new HashMap();
    private static final long serialVersionUID = -258215809702057490L;
    private final String name;

    private MediaProvider() {
        throw new AssertionError();
    }

    MediaProvider(String name2) {
        this.name = name2;
        instances.put(name2, this);
    }

    public String getName() {
        return this.name;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        return this.name.equals(((MediaProvider) o).name);
    }

    public int hashCode() {
        return this.name.hashCode();
    }

    public String toString() {
        return this.name;
    }

    private static MediaProvider getInstance(String name2) {
        return instances.get(name2);
    }

    private Object readResolve() throws ObjectStreamException {
        return getInstance(this.name);
    }
}
