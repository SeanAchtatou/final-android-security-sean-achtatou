package twitter4j.media;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import twitter4j.TwitterException;
import twitter4j.conf.Configuration;
import twitter4j.http.OAuthAuthorization;
import twitter4j.internal.http.HttpClientWrapper;
import twitter4j.internal.http.HttpParameter;
import twitter4j.internal.http.HttpResponse;
import twitter4j.internal.logging.Logger;

abstract class AbstractImageUploadImpl implements ImageUpload {
    public static final String TWITTER_VERIFY_CREDENTIALS_JSON = "https://api.twitter.com/1/account/verify_credentials.json";
    public static final String TWITTER_VERIFY_CREDENTIALS_XML = "https://api.twitter.com/1/account/verify_credentials.xml";
    static Class class$twitter4j$media$AbstractImageUploadImpl;
    protected static final Logger logger;
    protected String apiKey;
    protected HttpParameter[] appendParameter;
    private HttpClientWrapper client;
    protected Configuration conf;
    protected Map<String, String> headers;
    protected HttpResponse httpResponse;
    protected HttpParameter image;
    protected HttpParameter message;
    protected OAuthAuthorization oauth;
    protected HttpParameter[] postParameter;
    protected String uploadUrl;

    /* access modifiers changed from: protected */
    public abstract String postUpload() throws TwitterException;

    /* access modifiers changed from: protected */
    public abstract void preUpload() throws TwitterException;

    static {
        Class cls;
        if (class$twitter4j$media$AbstractImageUploadImpl == null) {
            cls = class$("twitter4j.media.AbstractImageUploadImpl");
            class$twitter4j$media$AbstractImageUploadImpl = cls;
        } else {
            cls = class$twitter4j$media$AbstractImageUploadImpl;
        }
        logger = Logger.getLogger(cls);
    }

    static Class class$(String x0) {
        try {
            return Class.forName(x0);
        } catch (ClassNotFoundException x1) {
            throw new NoClassDefFoundError().initCause(x1);
        }
    }

    AbstractImageUploadImpl(Configuration conf2, OAuthAuthorization oauth2) {
        this.conf = null;
        this.apiKey = null;
        this.oauth = null;
        this.uploadUrl = null;
        this.postParameter = null;
        this.appendParameter = null;
        this.image = null;
        this.message = null;
        this.headers = new HashMap();
        this.httpResponse = null;
        this.oauth = oauth2;
        this.conf = conf2;
        this.client = new HttpClientWrapper(conf2);
    }

    public AbstractImageUploadImpl(Configuration conf2, String apiKey2, OAuthAuthorization oauth2) {
        this(conf2, oauth2);
        this.apiKey = apiKey2;
    }

    public String upload(String imageFileName, InputStream imageBody) throws TwitterException {
        this.image = new HttpParameter("media", imageFileName, imageBody);
        return upload();
    }

    public String upload(String imageFileName, InputStream imageBody, String message2) throws TwitterException {
        this.image = new HttpParameter("media", imageFileName, imageBody);
        this.message = new HttpParameter("message", message2);
        return upload();
    }

    public String upload(File file, String message2) throws TwitterException {
        this.image = new HttpParameter("media", file);
        this.message = new HttpParameter("message", message2);
        return upload();
    }

    public String upload(File file) throws TwitterException {
        this.image = new HttpParameter("media", file);
        return upload();
    }

    public String upload() throws TwitterException {
        if (this.conf.getMediaProviderParameters() != null) {
            Set set = this.conf.getMediaProviderParameters().keySet();
            HttpParameter[] params = new HttpParameter[set.size()];
            int pos = 0;
            for (Object k : set) {
                params[pos] = new HttpParameter((String) k, this.conf.getMediaProviderParameters().getProperty((String) k));
                pos++;
            }
            this.appendParameter = params;
        }
        preUpload();
        if (this.postParameter == null) {
            throw new AssertionError("Incomplete implementation. postParameter is not set.");
        } else if (this.uploadUrl == null) {
            throw new AssertionError("Incomplete implementation. uploadUrl is not set.");
        } else {
            if (this.conf.getMediaProviderParameters() != null && this.appendParameter.length > 0) {
                this.postParameter = appendHttpParameters(this.postParameter, this.appendParameter);
            }
            this.httpResponse = this.client.post(this.uploadUrl, this.postParameter, this.headers);
            String mediaUrl = postUpload();
            logger.debug(new StringBuffer().append("uploaded url [").append(mediaUrl).append("]").toString());
            return mediaUrl;
        }
    }

    /* access modifiers changed from: protected */
    public HttpParameter[] appendHttpParameters(HttpParameter[] src, HttpParameter[] dst) {
        int srcLen = src.length;
        int dstLen = dst.length;
        HttpParameter[] ret = new HttpParameter[(srcLen + dstLen)];
        System.arraycopy(src, 0, ret, 0, srcLen);
        System.arraycopy(dst, 0, ret, srcLen, dstLen);
        return ret;
    }

    /* access modifiers changed from: protected */
    public String generateVerifyCredentialsAuthorizationHeader(String verifyCredentialsUrl) {
        return new StringBuffer().append("OAuth realm=\"http://api.twitter.com/\",").append(OAuthAuthorization.encodeParameters(this.oauth.generateOAuthSignatureHttpParams("GET", verifyCredentialsUrl), ",", true)).toString();
    }

    /* access modifiers changed from: protected */
    public String generateVerifyCredentialsAuthorizationURL(String verifyCredentialsUrl) {
        return new StringBuffer().append(verifyCredentialsUrl).append("?").append(OAuthAuthorization.encodeParameters(this.oauth.generateOAuthSignatureHttpParams("GET", verifyCredentialsUrl))).toString();
    }
}
