package twitter4j.media;

import twitter4j.TwitterException;
import twitter4j.conf.Configuration;
import twitter4j.http.OAuthAuthorization;
import twitter4j.internal.http.HttpParameter;

class TwippleUpload extends AbstractImageUploadImpl {
    public TwippleUpload(Configuration conf, OAuthAuthorization oauth) {
        super(conf, oauth);
    }

    /* access modifiers changed from: protected */
    public String postUpload() throws TwitterException {
        if (this.httpResponse.getStatusCode() != 200) {
            throw new TwitterException("Twipple image upload returned invalid status code", this.httpResponse);
        }
        String response = this.httpResponse.asString();
        if (-1 != response.indexOf("<rsp stat=\"fail\">")) {
            throw new TwitterException(new StringBuffer().append("Twipple image upload failed with this error message: ").append(response.substring(response.indexOf("msg") + 5, response.lastIndexOf("\""))).toString(), this.httpResponse);
        } else if (-1 != response.indexOf("<rsp stat=\"ok\">")) {
            return response.substring(response.indexOf("<mediaurl>") + "<mediaurl>".length(), response.indexOf("</mediaurl>"));
        } else {
            throw new TwitterException("Unknown Twipple response", this.httpResponse);
        }
    }

    /* access modifiers changed from: protected */
    public void preUpload() throws TwitterException {
        this.uploadUrl = "http://p.twipple.jp/api/upload";
        this.postParameter = new HttpParameter[]{new HttpParameter("verify_url", generateVerifyCredentialsAuthorizationURL(AbstractImageUploadImpl.TWITTER_VERIFY_CREDENTIALS_XML)), this.image};
    }
}
