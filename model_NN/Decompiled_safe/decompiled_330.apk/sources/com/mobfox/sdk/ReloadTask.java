package com.mobfox.sdk;

import java.util.TimerTask;

class ReloadTask extends TimerTask {
    private final MobFoxView mobFoxWebView;

    public ReloadTask(MobFoxView mobFoxWebView2) {
        this.mobFoxWebView = mobFoxWebView2;
    }

    public void run() {
        this.mobFoxWebView.loadNextAd();
    }
}
