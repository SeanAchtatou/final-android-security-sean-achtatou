package com.mobfox.sdk;

import com.lyrebird.colorreplacer.MyMotionEvent;

public class Base64 {
    private static char[] map1 = new char[64];
    private static byte[] map2 = new byte[128];

    static {
        int i;
        int i2 = 0;
        char c = 'A';
        while (true) {
            i = i2;
            if (c > 'Z') {
                break;
            }
            i2 = i + 1;
            map1[i] = c;
            c = (char) (c + 1);
        }
        char c2 = 'a';
        while (c2 <= 'z') {
            map1[i] = c2;
            c2 = (char) (c2 + 1);
            i++;
        }
        char c3 = '0';
        while (c3 <= '9') {
            map1[i] = c3;
            c3 = (char) (c3 + 1);
            i++;
        }
        int i3 = i + 1;
        map1[i] = '+';
        int i4 = i3 + 1;
        map1[i3] = '/';
        for (int i5 = 0; i5 < map2.length; i5++) {
            map2[i5] = -1;
        }
        for (int i6 = 0; i6 < 64; i6++) {
            map2[map1[i6]] = (byte) i6;
        }
    }

    public static String encodeString(String s) {
        return new String(encode(s.getBytes()));
    }

    public static char[] encode(byte[] in) {
        return encode(in, in.length);
    }

    /* JADX INFO: Multiple debug info for r0v11 int: [D('i0' int), D('o1' int)] */
    /* JADX INFO: Multiple debug info for r1v6 int: [D('o2' int), D('i1' int)] */
    /* JADX INFO: Multiple debug info for r2v3 int: [D('i2' int), D('o3' int)] */
    public static char[] encode(byte[] in, int iLen) {
        int ip;
        int i1;
        int ip2;
        int oDataLen = ((iLen * 4) + 2) / 3;
        char[] out = new char[(((iLen + 2) / 3) * 4)];
        int op = 0;
        int op2 = 0;
        while (op < iLen) {
            int ip3 = op + 1;
            int i0 = in[op] & MyMotionEvent.ACTION_MASK;
            if (ip3 < iLen) {
                int ip4 = ip3 + 1;
                i1 = in[ip3] & MyMotionEvent.ACTION_MASK;
                ip = ip4;
            } else {
                ip = ip3;
                i1 = 0;
            }
            if (ip < iLen) {
                int i = in[ip] & MyMotionEvent.ACTION_MASK;
                ip++;
                ip2 = i;
            } else {
                ip2 = 0;
            }
            int o0 = i0 >>> 2;
            int o1 = ((i0 & 3) << 4) | (i1 >>> 4);
            int i12 = ((i1 & 15) << 2) | (ip2 >>> 6);
            int o3 = ip2 & 63;
            int op3 = op2 + 1;
            out[op2] = map1[o0];
            int op4 = op3 + 1;
            out[op3] = map1[o1];
            out[op4] = op4 < oDataLen ? map1[i12] : '=';
            int op5 = op4 + 1;
            out[op5] = op5 < oDataLen ? map1[o3] : '=';
            op2 = op5 + 1;
            op = ip;
        }
        return out;
    }

    public static String decodeString(String s) {
        return new String(decode(s));
    }

    public static byte[] decode(String s) {
        return decode(s.toCharArray());
    }

    /* JADX INFO: Multiple debug info for r1v5 byte: [D('b1' int), D('i1' int)] */
    /* JADX INFO: Multiple debug info for r2v3 byte: [D('i2' int), D('b2' int)] */
    /* JADX INFO: Multiple debug info for r3v4 byte: [D('i3' int), D('b3' int)] */
    /* JADX INFO: Multiple debug info for r0v18 int: [D('b0' int), D('o0' int)] */
    /* JADX INFO: Multiple debug info for r1v9 int: [D('b1' int), D('o1' int)] */
    /* JADX INFO: Multiple debug info for r2v6 byte: [D('o2' int), D('b2' int)] */
    /* JADX INFO: Multiple debug info for r3v5 int: [D('b3' int), D('op' int)] */
    public static byte[] decode(char[] in) {
        int ip;
        char c;
        int ip2;
        char c2;
        int o1;
        int op;
        int iLen = in.length;
        if (iLen % 4 != 0) {
            throw new IllegalArgumentException("Length of Base64 encoded input string is not a multiple of 4.");
        }
        int iLen2 = iLen;
        while (iLen2 > 0 && in[iLen2 - 1] == '=') {
            iLen2--;
        }
        int oLen = (iLen2 * 3) / 4;
        byte[] out = new byte[oLen];
        int op2 = 0;
        for (int ip3 = 0; ip3 < iLen2; ip3 = ip2) {
            int ip4 = ip3 + 1;
            char c3 = in[ip3];
            int ip5 = ip4 + 1;
            char c4 = in[ip4];
            if (ip5 < iLen2) {
                int ip6 = ip5 + 1;
                c = in[ip5];
                ip = ip6;
            } else {
                ip = ip5;
                c = 'A';
            }
            if (ip < iLen2) {
                int ip7 = ip + 1;
                c2 = in[ip];
                ip2 = ip7;
            } else {
                ip2 = ip;
                c2 = 'A';
            }
            if (c3 > 127 || c4 > 127 || c > 127 || c2 > 127) {
                throw new IllegalArgumentException("Illegal character in Base64 encoded data.");
            }
            byte b = map2[c3];
            byte b2 = map2[c4];
            byte b3 = map2[c];
            byte b4 = map2[c2];
            if (b < 0 || b2 < 0 || b3 < 0 || b4 < 0) {
                throw new IllegalArgumentException("Illegal character in Base64 encoded data. " + ((int) c3));
            }
            int o0 = (b << 2) | (b2 >>> 4);
            int o12 = ((b2 & 15) << 4) | (b3 >>> 2);
            int b22 = ((b3 & 3) << 6) | b4;
            int op3 = op2 + 1;
            out[op2] = (byte) o0;
            if (op3 < oLen) {
                out[op3] = (byte) o12;
                o1 = op3 + 1;
            } else {
                o1 = op3;
            }
            if (o1 < oLen) {
                op = o1 + 1;
                out[o1] = (byte) b22;
            } else {
                op = o1;
            }
            op2 = op;
        }
        return out;
    }

    private Base64() {
    }
}
