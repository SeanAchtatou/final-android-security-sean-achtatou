package com.bugsense.trace;

import android.os.AsyncTask;

public abstract class ActivityAsyncTask<Connect, Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {
    private volatile boolean mPostProcessingDone = false;
    private Result mResult;
    protected volatile Connect mWrapped;

    public ActivityAsyncTask(Connect connect) {
        connectTo(connect);
    }

    public void connectTo(Connect connect) {
        if (this.mWrapped == null || connect == null) {
            this.mWrapped = connect;
            if (this.mWrapped == null) {
                return;
            }
            if (getStatus() == AsyncTask.Status.RUNNING) {
                onPreExecute();
            } else if (getStatus() == AsyncTask.Status.FINISHED && !this.mPostProcessingDone) {
                this.mPostProcessingDone = true;
                processPostExecute(this.mResult);
                this.mResult = null;
            }
        } else {
            throw new IllegalStateException();
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Result result) {
        super.onPostExecute(result);
        if (this.mWrapped != null) {
            this.mPostProcessingDone = true;
            processPostExecute(result);
            return;
        }
        this.mResult = result;
    }

    public boolean postProcessingDone() {
        return this.mPostProcessingDone;
    }

    /* access modifiers changed from: protected */
    public abstract void processPostExecute(Result result);
}
