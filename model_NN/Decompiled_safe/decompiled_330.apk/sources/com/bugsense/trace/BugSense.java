package com.bugsense.trace;

import android.util.Log;
import com.mobfox.sdk.Const;
import java.io.BufferedReader;
import java.io.StringReader;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Date;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.json.JSONObject;

public class BugSense {
    public static String MD5(String str) throws Exception {
        MessageDigest instance = MessageDigest.getInstance("MD5");
        instance.update(str.getBytes(), 0, str.length());
        return new BigInteger(1, instance.digest()).toString(16);
    }

    public static String createJSON(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String[] strArr, Date date) throws Exception {
        JSONObject jSONObject = new JSONObject();
        JSONObject jSONObject2 = new JSONObject();
        JSONObject jSONObject3 = new JSONObject();
        JSONObject jSONObject4 = new JSONObject();
        JSONObject jSONObject5 = new JSONObject();
        jSONObject2.put("remote_ip", "");
        jSONObject.put("request", jSONObject2);
        BufferedReader bufferedReader = new BufferedReader(new StringReader(str5));
        if (date == null) {
            jSONObject3.put("occured_at", bufferedReader.readLine());
        } else {
            jSONObject3.put("occured_at", date);
        }
        jSONObject3.put("message", bufferedReader.readLine());
        String readLine = bufferedReader.readLine();
        jSONObject3.put("where", readLine.substring(readLine.lastIndexOf("(") + 1, readLine.lastIndexOf(")")));
        jSONObject3.put("klass", getClass(str5));
        jSONObject3.put("backtrace", str5);
        jSONObject.put("exception", jSONObject3);
        bufferedReader.close();
        jSONObject4.put("phone", str3);
        jSONObject4.put("appver", str2);
        jSONObject4.put("appname", str);
        jSONObject4.put("osver", str4);
        jSONObject4.put("wifi_on", str6);
        jSONObject4.put("mobile_net_on", str7);
        jSONObject4.put("gps_on", str8);
        jSONObject4.put("screen:width", strArr[0]);
        jSONObject4.put("screen:height", strArr[1]);
        jSONObject4.put("screen:orientation", strArr[2]);
        jSONObject4.put("screen_dpi(x:y)", strArr[3] + ":" + strArr[4]);
        jSONObject.put("application_environment", jSONObject4);
        jSONObject5.put("version", "bugsense-version-0.6");
        jSONObject5.put("name", "bugsense-android");
        jSONObject.put("client", jSONObject5);
        return jSONObject.toString();
    }

    public static String getClass(String str) {
        int indexOf = str.indexOf(":");
        return (indexOf == -1 || indexOf + 1 >= str.length()) ? "" : str.substring(0, indexOf);
    }

    public static void submitError(int i, Date date, String str) throws Exception {
        try {
            Log.d(G.TAG, "Transmitting stack trace: " + str);
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            HttpParams params = defaultHttpClient.getParams();
            HttpProtocolParams.setUseExpectContinue(params, false);
            if (i != 0) {
                HttpConnectionParams.setConnectionTimeout(params, i);
                HttpConnectionParams.setSoTimeout(params, i);
            }
            HttpPost httpPost = new HttpPost(G.URL);
            httpPost.addHeader("X-BugSense-Api-Key", G.API_KEY);
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BasicNameValuePair("data", createJSON(G.APP_PACKAGE, G.APP_VERSION, G.PHONE_MODEL, G.ANDROID_VERSION, str, BugSenseHandler.isWifiOn(), BugSenseHandler.isMobileNetworkOn(), BugSenseHandler.isGPSOn(), BugSenseHandler.ScreenProperties(), date)));
            arrayList.add(new BasicNameValuePair("hash", MD5(str)));
            httpPost.setEntity(new UrlEncodedFormEntity(arrayList, Const.ENCODING));
            if (defaultHttpClient.execute(httpPost).getEntity() == null) {
                throw new Exception("no internet connection maybe");
            }
        } catch (Exception e) {
            Log.e(G.TAG, "Error sending exception stacktrace", e);
            throw e;
        }
    }
}
