package com.lyrebird.colorreplacer;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;

public class MarketDialog extends Dialog {
    Context mContext;

    public MarketDialog(Context context) {
        super(context);
        this.mContext = context;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((LinearLayout) LayoutInflater.from(this.mContext).inflate((int) R.layout.market_dialog_layout, (ViewGroup) null));
    }
}
