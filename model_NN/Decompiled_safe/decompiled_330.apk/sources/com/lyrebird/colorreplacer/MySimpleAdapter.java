package com.lyrebird.colorreplacer;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import java.util.HashMap;
import java.util.List;

public class MySimpleAdapter extends SimpleAdapter {
    public MySimpleAdapter(Context context, List<HashMap<String, Integer>> items, int resource, String[] from, int[] to) {
        super(context, items, resource, from, to);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ((ImageView) convertView.findViewById(R.id.spinnerTarget)).setImageResource(((Integer) ((HashMap) getItem(position)).get("id")).intValue());
        return convertView;
    }
}
