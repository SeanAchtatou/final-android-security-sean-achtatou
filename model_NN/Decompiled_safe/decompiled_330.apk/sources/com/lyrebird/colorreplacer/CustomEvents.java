package com.lyrebird.colorreplacer;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import com.adwhirl.AdWhirlLayout;
import com.mobfox.sdk.BannerListener;
import com.mobfox.sdk.MobFoxView;
import com.mobfox.sdk.Mode;
import com.mobfox.sdk.RequestException;

public class CustomEvents implements AdWhirlLayout.AdWhirlInterface {
    private Activity mActivity;
    /* access modifiers changed from: private */
    public AdWhirlLayout mAdWhirlLayout;
    private Context mContext;

    public CustomEvents(AdWhirlLayout mAdWhirlLayout2, Activity mActivity2, Context mContext2) {
        this.mAdWhirlLayout = mAdWhirlLayout2;
        this.mActivity = mActivity2;
        this.mContext = mContext2;
    }

    public void adWhirlGeneric() {
    }

    public void mobfoxBanner() {
        MobFoxView mBanner = new MobFoxView(this.mActivity, "128a3455eeb67662deac07c59e113b38", Mode.LIVE, true, true);
        mBanner.setInternalBrowser(true);
        mBanner.setBannerListener(new BannerListener() {
            public void noAdFound() {
                Log.v("Mobfox Banner Listener", "No Ad Found.");
                CustomEvents.this.mAdWhirlLayout.rollover();
            }

            public void bannerLoadSucceeded() {
                Log.v("Mobfox Banner Listener", "Ad Received.");
                CustomEvents.this.mAdWhirlLayout.adWhirlManager.resetRollover();
                CustomEvents.this.mAdWhirlLayout.rotateThreadedDelayed();
            }

            public void bannerLoadFailed(RequestException arg0) {
                Log.v("Mobfox Banner Listener", "Ad Failed.");
                CustomEvents.this.mAdWhirlLayout.rollover();
            }
        });
        this.mAdWhirlLayout.handler.post(new AdWhirlLayout.ViewAdRunnable(this.mAdWhirlLayout, mBanner));
    }
}
