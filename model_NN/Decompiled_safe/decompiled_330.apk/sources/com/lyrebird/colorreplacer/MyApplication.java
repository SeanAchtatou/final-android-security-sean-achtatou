package com.lyrebird.colorreplacer;

import android.app.Application;
import org.acra.ACRA;
import org.acra.annotation.ReportsCrashes;

@ReportsCrashes(formKey = "", formUri = "http://www.bugsense.com/api/acra?api_key=9850ed2a")
public class MyApplication extends Application {
    public void onCreate() {
        ACRA.init(this);
        super.onCreate();
    }
}
