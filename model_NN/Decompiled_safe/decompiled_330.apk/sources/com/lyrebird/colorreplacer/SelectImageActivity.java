package com.lyrebird.colorreplacer;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.graphics.Point;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Debug;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.adwhirl.AdWhirlLayout;
import com.adwhirl.AdWhirlManager;
import com.adwhirl.AdWhirlTargeting;
import com.bugsense.trace.BugSenseHandler;
import com.flurry.android.FlurryAgent;
import com.zemariamm.appirater.AppiraterBase;
import java.io.File;

public class SelectImageActivity extends AppiraterBase {
    public static final int sizeDivider = 120000;
    int SELECT_IMAGE = 41;
    int TAKE_PICTURE = 42;
    Cursor cursor;
    String selectedImagePath;
    File sessionDirectory;
    String sessionPath = "";

    public void onCreate(Bundle savedInstanceState) {
        Log.e("oncreate", "true");
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.select_image_layout);
        BugSenseHandler.setup(this, "1298f5c8");
        Utility.logFreeMemory();
        RelativeLayout layout = (RelativeLayout) findViewById(R.id.main_layout);
        AdWhirlManager.setConfigExpireTimeout(300000);
        AdWhirlTargeting.setAge(23);
        AdWhirlTargeting.setGender(AdWhirlTargeting.Gender.FEMALE);
        AdWhirlTargeting.setKeywords("online games gaming");
        AdWhirlTargeting.setPostalCode("94123");
        AdWhirlTargeting.setTestMode(false);
        RelativeLayout.LayoutParams layoutParamsBottom = new RelativeLayout.LayoutParams(-1, -2);
        layoutParamsBottom.addRule(10, -1);
        AdWhirlLayout adWhirlLayout = new AdWhirlLayout(this, "8535feca24884c8bb8e28878fb9972a7");
        adWhirlLayout.setAdWhirlInterface(new CustomEvents(adWhirlLayout, this, getApplicationContext()));
        layout.setGravity(1);
        layout.addView(adWhirlLayout, layoutParamsBottom);
        layout.invalidate();
    }

    public void onDestroy() {
        Log.e("onDestroy", "true");
        if (this.cursor != null) {
            this.cursor.close();
        }
        super.onDestroy();
    }

    public void onResume() {
        Log.e("onResume", "true");
        super.onResume();
    }

    public void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, "BV3I7HPIX9RV7ITTV8TW");
    }

    public void onStop() {
        FlurryAgent.onEndSession(this);
        Log.e("onStop", "true");
        super.onStop();
    }

    public void onPause() {
        Log.e("onPause", "true");
        super.onPause();
    }

    public void myClickHandler(View view) {
        switch (view.getId()) {
            case R.id.select_image_button:
                Log.e("select image", "true");
                startActivityForResult(new Intent("android.intent.action.PICK", MediaStore.Images.Media.INTERNAL_CONTENT_URI), this.SELECT_IMAGE);
                return;
            case R.id.take_photo:
                takePhoto();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public boolean isPackageLite() {
        return getPackageName().toLowerCase().contains("lite");
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Point p;
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == this.SELECT_IMAGE && resultCode == -1) {
            this.selectedImagePath = getPath(data.getData());
            if (this.selectedImagePath == null || !checkFileExtension(this.selectedImagePath)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("This app is working only with image files, please select an image file.").setCancelable(false).setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
                builder.create().show();
            } else {
                Point p2 = BitmapResizer.decodeFileSize(new File(this.selectedImagePath), (int) (getFreeMemory() / 120000));
                if (p2 != null) {
                    if (p2.x == -1) {
                        startShaderActivity(2);
                    } else {
                        alertDialogBuilder();
                    }
                }
            }
        }
        if (requestCode == this.TAKE_PICTURE && resultCode == -1) {
            this.selectedImagePath = getImageUri().getPath();
            if (this.selectedImagePath != null && (p = BitmapResizer.decodeFileSize(new File(this.selectedImagePath), (int) (getFreeMemory() / 120000))) != null) {
                if (p.x == -1) {
                    startShaderActivity(2);
                    return;
                }
                Log.e("Before DeleyedAlertBuildertask", "DeleyedAlertBuildertask");
                new DeleyedAlertBuildertask(this, null).execute((Object[]) null);
                Log.e("After DeleyedAlertBuildertask", "DeleyedAlertBuildertask");
            }
        }
    }

    private class DeleyedAlertBuildertask extends AsyncTask<Void, Void, Void> {
        private DeleyedAlertBuildertask() {
        }

        /* synthetic */ DeleyedAlertBuildertask(SelectImageActivity selectImageActivity, DeleyedAlertBuildertask deleyedAlertBuildertask) {
            this();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void unused) {
            SelectImageActivity.this.alertDialogBuilder();
            Log.e("DeleyedAlertBuildertask", "DeleyedAlertBuildertask");
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... arg0) {
            try {
                synchronized (this) {
                    wait(500);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Log.e("During DeleyedAlertBuildertask", "DeleyedAlertBuildertask");
            return null;
        }
    }

    /* JADX INFO: finally extract failed */
    private void galaxyS() {
        Cursor myCursor = managedQuery(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, new String[]{"_id", "image_id", "kind", "_data"}, "kind=1", null, "_id DESC");
        try {
            myCursor.moveToFirst();
            long imageId = myCursor.getLong(myCursor.getColumnIndexOrThrow("image_id"));
            long thumbnailImageId = myCursor.getLong(myCursor.getColumnIndexOrThrow("_id"));
            String thumbnailPath = myCursor.getString(myCursor.getColumnIndexOrThrow("_data"));
            myCursor.close();
            Cursor myCursor2 = managedQuery(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{"_id", "_data"}, null, null, "_id DESC");
            try {
                myCursor2.moveToFirst();
                String largeImagePath = myCursor2.getString(myCursor2.getColumnIndexOrThrow("_data"));
                myCursor2.close();
                Uri withAppendedPath = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, String.valueOf(imageId));
                Uri withAppendedPath2 = Uri.withAppendedPath(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, String.valueOf(thumbnailImageId));
            } catch (Throwable th) {
                myCursor2.close();
                throw th;
            }
        } catch (Throwable th2) {
            myCursor.close();
            throw th2;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void startShaderActivity(int sizeOption) {
        Log.e("sizeOption", String.valueOf(sizeOption));
        Log.e("sixxxxxxxxxxxxxxxxxxxxxxxxxxxzeOption", String.valueOf(sizeOption));
        if (sizeOption > 4 || sizeOption < 0) {
            sizeOption = 2;
        }
        int maxSize = (int) (getFreeMemory() / 120000);
        Intent shaderIntent = new Intent(getApplicationContext(), PaintActivity.class);
        shaderIntent.putExtra("selectedImagePath", this.selectedImagePath);
        shaderIntent.putExtra("isSession", false);
        shaderIntent.putExtra("MAX_SIZE", maxSize);
        shaderIntent.putExtra("SIZE_OPTION", sizeOption);
        Utility.logFreeMemory();
        startActivity(shaderIntent);
        finish();
    }

    private String[] sizeStringBuilder() {
        String[] sizeStringArray = new String[3];
        Point p = BitmapResizer.decodeFileSize(new File(this.selectedImagePath), (int) (getFreeMemory() / ((long) sizeDivider)));
        int x = p.x;
        int y = p.y;
        if (isPackageLite()) {
            sizeStringArray[2] = "Maximum Pro " + String.valueOf(x) + "x" + String.valueOf(y);
            int x2 = (int) (((double) x) / 1.5d);
            int y2 = (int) (((double) y) / 1.5d);
            sizeStringArray[1] = "High Pro            " + String.valueOf(x2) + "x" + String.valueOf(y2);
            sizeStringArray[0] = "Normal              " + String.valueOf((int) (((double) x2) / 1.6d)) + "x" + String.valueOf((int) (((double) y2) / 1.6d));
        } else {
            sizeStringArray[2] = "Maximum     " + String.valueOf(x) + "x" + String.valueOf(y);
            int x3 = (int) (((double) x) / 1.5d);
            int y3 = (int) (((double) y) / 1.5d);
            sizeStringArray[1] = "High                " + String.valueOf(x3) + "x" + String.valueOf(y3);
            sizeStringArray[0] = "Normal              " + String.valueOf((int) (((double) x3) / 1.6d)) + "x" + String.valueOf((int) (((double) y3) / 1.6d));
        }
        return sizeStringArray;
    }

    /* access modifiers changed from: private */
    public void alertDialogBuilder() {
        final CharSequence[] items = sizeStringBuilder();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Open large picture  as :");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                Toast.makeText(SelectImageActivity.this.getApplicationContext(), items[item], 0).show();
                if (!SelectImageActivity.this.isPackageLite()) {
                    SelectImageActivity.this.startShaderActivity(item);
                } else {
                    SelectImageActivity.this.startShaderActivity(0);
                }
            }
        });
        builder.create().show();
    }

    private void setAppState() {
    }

    private long getFreeMemory() {
        return Runtime.getRuntime().maxMemory() - Debug.getNativeHeapAllocatedSize();
    }

    public String getPath(Uri uri) throws CursorIndexOutOfBoundsException {
        this.cursor = managedQuery(uri, new String[]{"_data"}, null, null, null);
        try {
            if (this.cursor == null) {
                return null;
            }
            int column_index = this.cursor.getColumnIndexOrThrow("_data");
            this.cursor.moveToFirst();
            return this.cursor.getString(column_index);
        } catch (CursorIndexOutOfBoundsException e) {
            return null;
        }
    }

    private String getFileExtension(String str) {
        if (str == null) {
            str = "";
        }
        int dotPos = str.lastIndexOf(".");
        if (dotPos > 0) {
            return str.substring(dotPos);
        }
        return "";
    }

    private boolean checkFileExtension(String str) {
        String extension = getFileExtension(str);
        extension.toLowerCase();
        if (extension.contains("jpg") || extension.contains("png") || extension.contains("jpeg") || extension.contains("tiff") || extension.contains("gif") || extension.contains("jiff") || extension.contains("jiff")) {
            return true;
        }
        return false;
    }

    public void takePhoto() {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        Uri imageUri = getImageUri();
        intent.putExtra("output", imageUri);
        Log.e("is imageUri null xx", String.valueOf(imageUri == null));
        startActivityForResult(intent, this.TAKE_PICTURE);
    }

    private Uri getImageUri() {
        return Uri.fromFile(new File(Environment.getExternalStorageDirectory(), "Pic.jpg"));
    }
}
