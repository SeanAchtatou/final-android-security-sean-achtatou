package com.lyrebird.colorreplacer;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.SweepGradient;
import android.os.Bundle;
import android.view.View;
import twitter4j.internal.http.HttpResponseCode;

public class ColorPickerDialog extends Dialog {
    static double hueVal = 0.0d;
    private int mInitialColor;
    /* access modifiers changed from: private */
    public OnColorChangedListener mListener;

    public interface OnColorChangedListener {
        void colorChanged(int i, double d);
    }

    private static class ColorPickerView extends View {
        private static final int CENTER_RADIUS = 32;
        private static final int CENTER_X = 100;
        private static final int CENTER_Y = 100;
        private static final float PI = 3.1415925f;
        private Paint mCenterPaint;
        private final int[] mColors;
        private boolean mHighlightCenter;
        private OnColorChangedListener mListener;
        private Paint mPaint;
        private boolean mTrackingCenter;
        Paint paint = new Paint();

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.graphics.SweepGradient.<init>(float, float, int[], float[]):void}
         arg types: [int, int, int[], ?[OBJECT, ARRAY]]
         candidates:
          ClspMth{android.graphics.SweepGradient.<init>(float, float, long, long):void}
          ClspMth{android.graphics.SweepGradient.<init>(float, float, int, int):void}
          ClspMth{android.graphics.SweepGradient.<init>(float, float, long[], float[]):void}
          ClspMth{android.graphics.SweepGradient.<init>(float, float, int[], float[]):void} */
        ColorPickerView(Context c, OnColorChangedListener l, int color) {
            super(c);
            this.mListener = l;
            this.mColors = new int[]{-65536, -256, -16711936, -16711681, -16776961, -65281, -65536};
            Shader s = new SweepGradient(0.0f, 0.0f, this.mColors, (float[]) null);
            this.mPaint = new Paint(1);
            this.mPaint.setShader(s);
            this.mPaint.setStyle(Paint.Style.STROKE);
            this.mPaint.setStrokeWidth(32.0f);
            this.mCenterPaint = new Paint(1);
            this.mCenterPaint.setColor(color);
            this.mCenterPaint.setStrokeWidth(5.0f);
        }

        /* access modifiers changed from: protected */
        public void onDraw(Canvas canvas) {
            float r = 100.0f - (this.mPaint.getStrokeWidth() * 0.5f);
            canvas.translate(100.0f, 100.0f);
            canvas.drawOval(new RectF(-r, -r, r, r), this.mPaint);
            canvas.drawCircle(0.0f, 0.0f, 32.0f, this.mCenterPaint);
            if (this.mTrackingCenter) {
                int c = this.mCenterPaint.getColor();
                this.mCenterPaint.setStyle(Paint.Style.STROKE);
                if (this.mHighlightCenter) {
                    this.mCenterPaint.setAlpha(MyMotionEvent.ACTION_MASK);
                } else {
                    this.mCenterPaint.setAlpha(128);
                }
                canvas.drawCircle(0.0f, 0.0f, this.mCenterPaint.getStrokeWidth() + 32.0f, this.mCenterPaint);
                this.mCenterPaint.setStyle(Paint.Style.FILL);
                this.mCenterPaint.setColor(c);
            }
        }

        /* access modifiers changed from: protected */
        public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            setMeasuredDimension(HttpResponseCode.OK, HttpResponseCode.OK);
        }

        private int hsb2Rgb(double hue, double sat, double brite) {
            double red;
            double green;
            double blue;
            if (sat != 0.0d) {
                if (hue == 360.0d) {
                    hue = 0.0d;
                }
                int slice = (int) (hue / 60.0d);
                double hue_frac = (hue / 60.0d) - ((double) slice);
                double aa = brite * (1.0d - sat);
                double bb = brite * (1.0d - (sat * hue_frac));
                double cc = brite * (1.0d - ((1.0d - hue_frac) * sat));
                switch (slice) {
                    case 0:
                        red = brite;
                        green = cc;
                        blue = aa;
                        break;
                    case 1:
                        red = bb;
                        green = brite;
                        blue = aa;
                        break;
                    case 2:
                        red = aa;
                        green = brite;
                        blue = cc;
                        break;
                    case 3:
                        red = aa;
                        green = bb;
                        blue = brite;
                        break;
                    case 4:
                        red = cc;
                        green = aa;
                        blue = brite;
                        break;
                    case 5:
                        red = brite;
                        green = aa;
                        blue = bb;
                        break;
                    default:
                        red = 0.0d;
                        green = 0.0d;
                        blue = 0.0d;
                        break;
                }
            } else {
                red = brite;
                green = brite;
                blue = brite;
            }
            return -16777216 | (((int) (255.0d * red)) << 16) | (((int) (255.0d * green)) << 8) | ((int) (255.0d * blue));
        }

        private int floatToByte(float x) {
            return Math.round(x);
        }

        private int pinToByte(int n) {
            if (n < 0) {
                return 0;
            }
            if (n > 255) {
                return MyMotionEvent.ACTION_MASK;
            }
            return n;
        }

        private int ave(int s, int d, float p) {
            return Math.round(((float) (d - s)) * p) + s;
        }

        private int interpColor(int[] colors, float unit) {
            if (unit <= 0.0f) {
                return colors[0];
            }
            if (unit >= 1.0f) {
                return colors[colors.length - 1];
            }
            float p = unit * ((float) (colors.length - 1));
            int i = (int) p;
            float p2 = p - ((float) i);
            int c0 = colors[i];
            int c1 = colors[i + 1];
            return Color.argb(ave(Color.alpha(c0), Color.alpha(c1), p2), ave(Color.red(c0), Color.red(c1), p2), ave(Color.green(c0), Color.green(c1), p2), ave(Color.blue(c0), Color.blue(c1), p2));
        }

        private int rotateColor(int color, float rad) {
            int r = Color.red(color);
            int g = Color.green(color);
            int b = Color.blue(color);
            ColorMatrix cm = new ColorMatrix();
            ColorMatrix tmp = new ColorMatrix();
            cm.setRGB2YUV();
            tmp.setRotate(0, (180.0f * rad) / 3.1415927f);
            cm.postConcat(tmp);
            tmp.setYUV2RGB();
            cm.postConcat(tmp);
            float[] a = cm.getArray();
            return Color.argb(Color.alpha(color), pinToByte(floatToByte((a[0] * ((float) r)) + (a[1] * ((float) g)) + (a[2] * ((float) b)))), pinToByte(floatToByte((a[5] * ((float) r)) + (a[6] * ((float) g)) + (a[7] * ((float) b)))), pinToByte(floatToByte((a[10] * ((float) r)) + (a[11] * ((float) g)) + (a[12] * ((float) b)))));
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        /* JADX WARNING: Removed duplicated region for block: B:12:0x003b  */
        /* JADX WARNING: Removed duplicated region for block: B:15:0x0045  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean onTouchEvent(android.view.MotionEvent r15) {
            /*
                r14 = this;
                r12 = 0
                r1 = 1120403456(0x42c80000, float:100.0)
                r3 = 4607182418800017408(0x3ff0000000000000, double:1.0)
                r13 = 1
                float r0 = r15.getX()
                float r10 = r0 - r1
                float r0 = r15.getY()
                float r11 = r0 - r1
                float r0 = r10 * r10
                float r1 = r11 * r11
                float r0 = r0 + r1
                double r0 = (double) r0
                double r0 = java.lang.Math.sqrt(r0)
                r5 = 4629700416936869888(0x4040000000000000, double:32.0)
                int r0 = (r0 > r5 ? 1 : (r0 == r5 ? 0 : -1))
                if (r0 > 0) goto L_0x002b
                r8 = r13
            L_0x0023:
                int r0 = r15.getAction()
                switch(r0) {
                    case 0: goto L_0x002d;
                    case 1: goto L_0x0078;
                    case 2: goto L_0x0037;
                    default: goto L_0x002a;
                }
            L_0x002a:
                return r13
            L_0x002b:
                r8 = r12
                goto L_0x0023
            L_0x002d:
                r14.mTrackingCenter = r8
                if (r8 == 0) goto L_0x0037
                r14.mHighlightCenter = r13
                r14.invalidate()
                goto L_0x002a
            L_0x0037:
                boolean r0 = r14.mTrackingCenter
                if (r0 == 0) goto L_0x0045
                boolean r0 = r14.mHighlightCenter
                if (r0 == r8) goto L_0x002a
                r14.mHighlightCenter = r8
                r14.invalidate()
                goto L_0x002a
            L_0x0045:
                double r0 = (double) r11
                double r5 = (double) r10
                double r0 = java.lang.Math.atan2(r0, r5)
                float r7 = (float) r0
                r0 = 1086918618(0x40c90fda, float:6.283185)
                float r9 = r7 / r0
                r0 = 0
                int r0 = (r9 > r0 ? 1 : (r9 == r0 ? 0 : -1))
                if (r0 >= 0) goto L_0x0059
                r0 = 1065353216(0x3f800000, float:1.0)
                float r9 = r9 + r0
            L_0x0059:
                r0 = 1135869952(0x43b40000, float:360.0)
                float r9 = r9 * r0
                double r0 = (double) r9
                com.lyrebird.colorreplacer.ColorPickerDialog.hueVal = r0
                java.lang.String r0 = "unit"
                java.lang.String r1 = java.lang.String.valueOf(r9)
                android.util.Log.e(r0, r1)
                android.graphics.Paint r12 = r14.mCenterPaint
                double r1 = (double) r9
                r0 = r14
                r5 = r3
                int r0 = r0.hsb2Rgb(r1, r3, r5)
                r12.setColor(r0)
                r14.invalidate()
                goto L_0x002a
            L_0x0078:
                boolean r0 = r14.mTrackingCenter
                if (r0 == 0) goto L_0x002a
                if (r8 == 0) goto L_0x008b
                com.lyrebird.colorreplacer.ColorPickerDialog$OnColorChangedListener r0 = r14.mListener
                android.graphics.Paint r1 = r14.mCenterPaint
                int r1 = r1.getColor()
                double r2 = com.lyrebird.colorreplacer.ColorPickerDialog.hueVal
                r0.colorChanged(r1, r2)
            L_0x008b:
                r14.mTrackingCenter = r12
                r14.invalidate()
                goto L_0x002a
            */
            throw new UnsupportedOperationException("Method not decompiled: com.lyrebird.colorreplacer.ColorPickerDialog.ColorPickerView.onTouchEvent(android.view.MotionEvent):boolean");
        }
    }

    public ColorPickerDialog(Context context, OnColorChangedListener listener, int initialColor) {
        super(context);
        this.mListener = listener;
        this.mInitialColor = initialColor;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new ColorPickerView(getContext(), new OnColorChangedListener() {
            public void colorChanged(int color, double hueVal) {
                ColorPickerDialog.this.mListener.colorChanged(color, hueVal);
                ColorPickerDialog.this.dismiss();
            }
        }, this.mInitialColor));
        setTitle("Pick a Color");
    }
}
