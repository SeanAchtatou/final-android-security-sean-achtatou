package com.lyrebird.colorreplacer;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.SpinnerAdapter;

class MyAdapter extends BaseAdapter implements SpinnerAdapter {
    Context myContext;
    int[] resList = {R.drawable.brush1, R.drawable.brush2, R.drawable.brush3};

    public int getCount() {
        return this.resList.length;
    }

    public MyAdapter(Context context) {
        this.myContext = context;
    }

    public Object getItem(int position) {
        return Integer.valueOf(this.resList[position]);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View view, ViewGroup parent) {
        ImageView im = new ImageView(this.myContext);
        im.setBackgroundResource(this.resList[position]);
        return im;
    }
}
