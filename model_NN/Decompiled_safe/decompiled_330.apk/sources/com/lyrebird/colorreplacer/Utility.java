package com.lyrebird.colorreplacer;

import android.os.Debug;
import android.os.Environment;
import android.util.Log;

public class Utility {
    public static void logFreeMemory() {
        Log.e("free memory", String.valueOf(getFreeMemory()));
    }

    public static long getFreeMemory() {
        return Runtime.getRuntime().maxMemory() - Debug.getNativeHeapAllocatedSize();
    }

    public static boolean isSDCardAvialable() {
        String state = Environment.getExternalStorageState();
        if ("mounted".equals(state)) {
            return true;
        }
        if ("mounted_ro".equals(state)) {
            return false;
        }
        return false;
    }
}
