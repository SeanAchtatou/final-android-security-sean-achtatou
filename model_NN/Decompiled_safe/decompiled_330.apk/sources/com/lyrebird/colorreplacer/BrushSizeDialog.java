package com.lyrebird.colorreplacer;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import twitter4j.internal.http.HttpResponseCode;

public class BrushSizeDialog extends Dialog {
    final float MAX_SIZE = 50.0f;
    final float MIN_SIZE = 12.0f;
    float currentSize = 20.0f;
    Context mContext;
    private float mInitialSize = 20.0f;
    /* access modifiers changed from: private */
    public SizePickerView mySizePickerView;
    private SeekBar seekBar;

    public interface OnBrushSizeChangeListener {
        void onBrushSizeChange(float f);
    }

    public interface mClickListener {
        void onClick();
    }

    public float getSize() {
        return this.mySizePickerView.getSize();
    }

    public BrushSizeDialog(Context context, View.OnClickListener clickListener, float size) {
        super(context);
    }

    public BrushSizeDialog(Context context, float size) {
        super(context);
        this.mContext = context;
        this.mInitialSize = size;
    }

    public float getSeekBarValue() {
        return (float) this.seekBar.getProgress();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LinearLayout mLayout = (LinearLayout) LayoutInflater.from(this.mContext).inflate((int) R.layout.brush_size_dialog_layout, (ViewGroup) null);
        setContentView(mLayout);
        SeekBar.OnSeekBarChangeListener mListener = new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                BrushSizeDialog.this.currentSize = BrushSizeDialog.this.getSeekBarValue();
                BrushSizeDialog.this.mySizePickerView.updateSize(BrushSizeDialog.this.currentSize);
                BrushSizeDialog.this.mySizePickerView.invalidate();
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        };
        this.seekBar = (SeekBar) findViewById(R.id.brush_size_seekbar);
        this.seekBar.setOnSeekBarChangeListener(mListener);
        this.mySizePickerView = new SizePickerView(getContext(), this.currentSize);
        this.seekBar.setProgress((int) this.mInitialSize);
        mLayout.addView(this.mySizePickerView);
        setContentView(mLayout);
    }

    public static class SizePickerView extends View {
        private BrushSizeDialog mCallback;
        private Paint mPaint;
        private float mSize = 20.0f;

        SizePickerView(Context c, float s) {
            super(c);
        }

        public void updateSize(float size) {
            this.mSize = size;
        }

        public float getSize() {
            return this.mSize;
        }

        public BrushSizeDialog getCallback() {
            return this.mCallback;
        }

        /* access modifiers changed from: protected */
        public void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            this.mPaint = new Paint(1);
            this.mPaint.setStyle(Paint.Style.FILL);
            this.mPaint.setColor(-256);
            this.mPaint.setColor(-18161);
            canvas.drawCircle(100.0f, 50.0f, this.mSize / 2.0f, this.mPaint);
        }

        /* access modifiers changed from: protected */
        public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            setMeasuredDimension(HttpResponseCode.OK, 100);
        }

        public void setCallback(BrushSizeDialog c) {
            this.mCallback = c;
        }
    }
}
