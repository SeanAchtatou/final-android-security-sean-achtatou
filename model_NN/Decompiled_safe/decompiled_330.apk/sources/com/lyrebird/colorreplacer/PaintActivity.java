package com.lyrebird.colorreplacer;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.EmbossMaskFilter;
import android.graphics.MaskFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.Shader;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Debug;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.adwhirl.AdWhirlLayout;
import com.adwhirl.AdWhirlManager;
import com.adwhirl.AdWhirlTargeting;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.flurry.android.FlurryAgent;
import com.lyrebird.colorreplacer.ColorPickerDialog;
import com.mobfox.sdk.Const;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Properties;
import org.acra.ErrorReporter;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.conf.PropertyConfiguration;
import twitter4j.http.AccessToken;
import twitter4j.http.RequestToken;
import twitter4j.media.ImageUploadFactory;
import twitter4j.media.MediaProvider;

public class PaintActivity extends Activity implements AdWhirlLayout.AdWhirlInterface, ColorPickerDialog.OnColorChangedListener {
    private static final int COLOR_ID = 3;
    private static final int COLOR_MENU_ID = 1;
    private static final int PREVIEW_ID = 2;
    private static final int ROTATE_ID = 4;
    final int BLUR_STATE = 2;
    final int COLOR_STATE = 0;
    final int EMBOSS_STATE = 3;
    final int ERASE_STATE = 1;
    int MAX_SIZE = 0;
    /* access modifiers changed from: private */
    public float MAX_ZOOM = 5.0f;
    /* access modifiers changed from: private */
    public float MIN_ZOOM = 1.0f;
    final int OPAQUE_STATE = 4;
    int PAINT_STATE = 0;
    int SAVED_STATE = 0;
    /* access modifiers changed from: private */
    public float SCALE = 1.2f;
    AccessToken accessToken = null;
    Activity activity;
    MyAdWhirlLayout adWhirlLayout;
    RelativeLayout addAdLayout;
    Context alertContext = this;
    SharedPreferences app_preferences;
    int bottomHeightOffset = 0;
    double[] briteArray;
    Button brushButton;
    float brushSize = 40.0f;
    BrushSizeDialog brushSizeDialog;
    ColorPickerDialog.OnColorChangedListener colorListener;
    Shader currentShader;
    Object data = null;
    Button eraseButton;
    Facebook facebook;
    final String facebookAppId = "102911443145956";
    boolean facebookAutorization = false;
    ActionItem firstActionItem;
    RelativeLayout footer;
    ActionItem fourthActionItem;
    Button fullScreenButton;
    Button grayButton;
    Shader grayShader;
    double hueValue = 350.0d;
    int initialYPosition = 60;
    boolean isAdLoaded = false;
    boolean isInClearState = false;
    Boolean isMasked = false;
    boolean isfsbClicked = false;
    RelativeLayout layout;
    private AsyncFacebookRunner mAsyncRunner;
    /* access modifiers changed from: private */
    public MaskFilter mBlur;
    Context mContext;
    /* access modifiers changed from: private */
    public MaskFilter mEmboss;
    /* access modifiers changed from: private */
    public Paint mPaint;
    MarketDialog marketDialog;
    MenuDialog menuDialog;
    MyView myView;
    Paint notBlackClearPaint;
    int oldColor = 0;
    private float oldZoom = 1.0f;
    Button paintButton;
    Button panButton;
    public boolean panZoom = false;
    int[] pixels;
    QuickAction qa;
    RequestToken requestToken = null;
    Matrix restoreMatrix = new Matrix();
    double[] satArray;
    int screenHeight;
    int screenWidth;
    ActionItem secondActionItem;
    String selectedImagePath;
    Bitmap shaderSourceBtm;
    int sizeOption = 0;
    View sliderDrawer;
    ActionItem thirdActionItem;
    int thumbSize = 100;
    RelativeLayout toolbar;
    int topHeightOffset = 0;
    final String twitPicApiKey = "8e80d7e0a4bba7466faf4583240a75bb";
    Twitter twitter;
    final String twitterKey = "GnGP83Qqw9uE6WDKKmDag";
    final String twitterSecret = "G27yAKHnGyspLvYCwmcPnyxFVb3tF7snvupMHREEfo";
    String twitterUploadFile = null;

    public void onCreate(Bundle savedInstanceState) {
        this.mContext = getApplicationContext();
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        float density = getResources().getDisplayMetrics().density;
        if (density > 0.0f) {
            this.thumbSize = (int) (((float) this.thumbSize) * density);
        }
        Log.e("density", String.valueOf(density));
        this.screenHeight = getResources().getDisplayMetrics().heightPixels;
        this.bottomHeightOffset = this.screenHeight;
        this.screenWidth = getResources().getDisplayMetrics().widthPixels;
        super.onCreate(savedInstanceState);
        this.data = getLastNonConfigurationInstance();
        this.activity = this;
        this.colorListener = this;
        this.app_preferences = PreferenceManager.getDefaultSharedPreferences(this.mContext);
        this.SCALE = this.app_preferences.getFloat("scale_preference", 1.2f);
        this.mPaint = new Paint();
        this.mPaint.setAntiAlias(true);
        this.mPaint.setDither(true);
        this.mPaint.setStyle(Paint.Style.STROKE);
        this.mPaint.setStrokeJoin(Paint.Join.ROUND);
        this.mPaint.setStrokeCap(Paint.Cap.ROUND);
        this.mPaint.setStrokeWidth(this.brushSize);
        this.mEmboss = new EmbossMaskFilter(new float[]{1.0f, 1.0f, 1.0f}, 0.4f, 6.0f, 3.5f);
        this.mBlur = new BlurMaskFilter(8.0f, BlurMaskFilter.Blur.NORMAL);
        this.facebook = new Facebook("102911443145956");
        Bundle extras = getIntent().getExtras();
        this.selectedImagePath = extras.getString("selectedImagePath");
        this.MAX_SIZE = extras.getInt("MAX_SIZE");
        this.sizeOption = extras.getInt("SIZE_OPTION");
        this.layout = new RelativeLayout(this.mContext);
        this.layout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.layout.setBackgroundColor(-16777216);
        this.sliderDrawer = getLayoutInflater().inflate((int) R.layout.slider_layout, (ViewGroup) null);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(-2, -2);
        lp.addRule(12);
        this.myView = new MyView(this.mContext);
        this.myView.setId(112);
        this.layout.addView(this.myView);
        this.layout.addView(this.sliderDrawer, lp);
        setContentView(this.layout);
        setupUI();
        this.toolbar = (RelativeLayout) findViewById(R.id.toolbar);
        this.footer = (RelativeLayout) findViewById(R.id.footer);
        if (this.app_preferences.getBoolean("isFirstRun", true)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("You can ZOOM IN and ZOOM OUT by pinch zoom( By sweeping two fingers different direction)").setCancelable(false).setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            builder.create().show();
            SharedPreferences.Editor editor = this.app_preferences.edit();
            editor.putBoolean("isFirstRun", false);
            editor.commit();
        }
        if (!isPackageLite()) {
            isPackageColorMe();
        }
        this.adWhirlLayout = new MyAdWhirlLayout(this, "8535feca24884c8bb8e28878fb9972a7");
        this.adWhirlLayout.setId(114);
        AdWhirlManager.setConfigExpireTimeout(300000);
        AdWhirlTargeting.setAge(23);
        AdWhirlTargeting.setGender(AdWhirlTargeting.Gender.FEMALE);
        AdWhirlTargeting.setKeywords("color splash effect");
        AdWhirlTargeting.setPostalCode("94123");
        AdWhirlTargeting.setTestMode(false);
        new RelativeLayout.LayoutParams(-1, -2);
        this.adWhirlLayout.setAdWhirlInterface(new CustomEvents(this.adWhirlLayout, this, getApplicationContext()));
        RelativeLayout.LayoutParams adLayoutparams = new RelativeLayout.LayoutParams(-2, -2);
        if (getResources().getConfiguration().orientation != 2) {
            adLayoutparams.addRule(3, this.toolbar.getId());
        }
        adLayoutparams.addRule(14);
        this.addAdLayout = (RelativeLayout) findViewById(R.id.add_ad_layout);
        if (!(this.addAdLayout == null || this.adWhirlLayout == null || adLayoutparams == null)) {
            this.addAdLayout.addView(this.adWhirlLayout, 0, adLayoutparams);
        }
        this.panButton = (Button) findViewById(R.id.pan_button);
        this.paintButton = (Button) findViewById(R.id.paint_button);
        this.eraseButton = (Button) findViewById(R.id.erase_button);
        this.grayButton = (Button) findViewById(R.id.gray_button);
        Log.e("geImageAllocationSize", String.valueOf(geImageAllocationSize()));
        logFreeMemory();
    }

    public void adWhirlMeasureChanged(int height) {
        float[] values = new float[9];
        this.myView.matrix.getValues(values);
        Log.e("before ad loaded", String.valueOf(this.topHeightOffset));
        this.topHeightOffset += height;
        setMinZoom();
        Log.e("after ad loaded", String.valueOf(this.topHeightOffset));
        this.myView.matrix.setValues(values);
        this.myView.invalidate();
    }

    /* access modifiers changed from: private */
    public void logFreeMemory() {
        Log.e("free memory", String.valueOf(getFreeMemory()));
    }

    private long geImageAllocationSize() {
        return (long) (this.myView.sourceHeight * this.myView.sourceHeight * 4);
    }

    private long getFreeMemory() {
        return Runtime.getRuntime().maxMemory() - Debug.getNativeHeapAllocatedSize();
    }

    /* access modifiers changed from: private */
    public void addFullScreenButton() {
        RelativeLayout.LayoutParams fsButtonLayout = new RelativeLayout.LayoutParams(-2, -2);
        fsButtonLayout.addRule(11);
        if (this.fullScreenButton == null) {
            this.fullScreenButton = new Button(this.mContext);
            this.fullScreenButton.setId(113);
            this.fullScreenButton.setBackgroundResource(R.drawable.up);
            fsButtonLayout.addRule(11);
            this.fullScreenButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    if (PaintActivity.this.toolbar.getVisibility() == 0) {
                        PaintActivity.this.toolbar.setVisibility(4);
                        PaintActivity.this.footer.setVisibility(4);
                        PaintActivity.this.paintButton.setVisibility(4);
                        RelativeLayout.LayoutParams adLayoutparams = new RelativeLayout.LayoutParams(-2, -2);
                        adLayoutparams.addRule(14);
                        PaintActivity.this.adWhirlLayout.setLayoutParams(adLayoutparams);
                        RelativeLayout.LayoutParams fsblp = new RelativeLayout.LayoutParams(-2, -2);
                        fsblp.addRule(3, PaintActivity.this.adWhirlLayout.getId());
                        fsblp.addRule(11);
                        PaintActivity.this.fullScreenButton.setLayoutParams(fsblp);
                        if (PaintActivity.this.getResources().getConfiguration().orientation != 2) {
                            PaintActivity.this.topHeightOffset -= PaintActivity.this.toolbar.getHeight();
                            PaintActivity.this.bottomHeightOffset += PaintActivity.this.footer.getHeight();
                            PaintActivity.this.isfsbClicked = true;
                            return;
                        }
                        return;
                    }
                    PaintActivity.this.toolbar.setVisibility(0);
                    PaintActivity.this.footer.setVisibility(0);
                    PaintActivity.this.paintButton.setVisibility(0);
                    RelativeLayout.LayoutParams adLayoutparams2 = new RelativeLayout.LayoutParams(-2, -2);
                    if (PaintActivity.this.getResources().getConfiguration().orientation != 2) {
                        adLayoutparams2.addRule(3, PaintActivity.this.toolbar.getId());
                    }
                    adLayoutparams2.addRule(14);
                    PaintActivity.this.adWhirlLayout.setLayoutParams(adLayoutparams2);
                    RelativeLayout.LayoutParams layoutElse = new RelativeLayout.LayoutParams(-2, -2);
                    adLayoutparams2.addRule(14);
                    layoutElse.addRule(11);
                    PaintActivity.this.fullScreenButton.setLayoutParams(layoutElse);
                    if (PaintActivity.this.isfsbClicked) {
                        PaintActivity.this.topHeightOffset += PaintActivity.this.toolbar.getHeight();
                        PaintActivity.this.bottomHeightOffset -= PaintActivity.this.footer.getHeight();
                        PaintActivity.this.isfsbClicked = false;
                    }
                }
            });
            this.addAdLayout.addView(this.fullScreenButton, fsButtonLayout);
        }
    }

    private byte[] getByteArray() {
        Bitmap screenshot = Bitmap.createBitmap(this.myView.sourceWidth, this.myView.sourceHeight, Bitmap.Config.ARGB_8888);
        Canvas cvs = new Canvas(screenshot);
        Paint paint = new Paint();
        Paint paint2 = new Paint();
        paint.setStyle(Paint.Style.FILL);
        cvs.drawBitmap(this.myView.graySourceBtm, 0.0f, 0.0f, paint);
        cvs.drawBitmap(this.myView.colorSourceBtm, 0.0f, 0.0f, paint2);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        screenshot.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        screenshot.recycle();
        return b;
    }

    /* access modifiers changed from: private */
    public String getTwitterPic() throws FileNotFoundException {
        Bitmap screenshot = Bitmap.createBitmap(this.myView.sourceWidth, this.myView.sourceHeight, Bitmap.Config.ARGB_8888);
        Canvas cvs = new Canvas(screenshot);
        Paint paint = new Paint();
        Paint paint2 = new Paint();
        paint.setStyle(Paint.Style.FILL);
        cvs.drawBitmap(this.myView.graySourceBtm, 0.0f, 0.0f, paint);
        cvs.drawBitmap(this.myView.colorSourceBtm, 0.0f, 0.0f, paint2);
        this.twitterUploadFile = String.valueOf(String.valueOf(String.valueOf(Environment.getExternalStorageDirectory().toString()) + getString(R.string.Directory) + System.currentTimeMillis())) + ".png";
        try {
            FileOutputStream out = new FileOutputStream(this.twitterUploadFile);
            screenshot.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        screenshot.recycle();
        return this.twitterUploadFile;
    }

    public void onResume() {
        super.onResume();
        if (this.facebookAutorization) {
            this.mAsyncRunner = new AsyncFacebookRunner(this.facebook);
            facebookWallPost();
            this.facebookAutorization = false;
        }
    }

    public void onPause() {
        super.onPause();
    }

    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data2) {
        super.onActivityResult(requestCode, resultCode, data2);
        try {
            this.facebook.authorizeCallback(requestCode, resultCode, data2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void facebookWallPost() {
        Bundle params = new Bundle();
        params.putString("message", "Color Replacer Android app");
        params.putByteArray("image", getByteArray());
        this.mAsyncRunner.request("me/photos", params, "POST", new AsyncFacebookRunner.RequestListener() {
            public void onMalformedURLException(MalformedURLException e) {
            }

            public void onIOException(IOException e) {
            }

            public void onFileNotFoundException(FileNotFoundException e) {
            }

            public void onFacebookError(FacebookError e) {
            }

            public void onComplete(String response) {
            }

            public void onComplete(String response, Object state) {
            }

            public void onIOException(IOException e, Object state) {
            }

            public void onFileNotFoundException(FileNotFoundException e, Object state) {
            }

            public void onMalformedURLException(MalformedURLException e, Object state) {
            }

            public void onFacebookError(FacebookError e, Object state) {
            }
        }, null);
    }

    private class TwiterTask extends AsyncTask<String, Void, Void> {
        private TwiterTask() {
        }

        /* synthetic */ TwiterTask(PaintActivity paintActivity, TwiterTask twiterTask) {
            this();
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(String... params) {
            if (PaintActivity.this.accessToken == null && params[0].length() > 0) {
                try {
                    PaintActivity.this.accessToken = PaintActivity.this.twitter.getOAuthAccessToken(PaintActivity.this.requestToken, params[0]);
                    SharedPreferences.Editor editor = PaintActivity.this.app_preferences.edit();
                    editor.putString("token", PaintActivity.this.accessToken.getToken());
                    editor.putString("secret", PaintActivity.this.accessToken.getTokenSecret());
                    editor.commit();
                } catch (TwitterException e) {
                    e.printStackTrace();
                }
            }
            try {
                PaintActivity.this.twitter.setOAuthAccessToken(PaintActivity.this.accessToken);
                Properties props = new Properties();
                MediaProvider mProvider = MediaProvider.TWITPIC;
                props.put(PropertyConfiguration.MEDIA_PROVIDER, mProvider);
                props.put(PropertyConfiguration.OAUTH_ACCESS_TOKEN, PaintActivity.this.accessToken.getToken());
                props.put(PropertyConfiguration.OAUTH_ACCESS_TOKEN_SECRET, PaintActivity.this.accessToken.getTokenSecret());
                props.put(PropertyConfiguration.OAUTH_CONSUMER_KEY, "GnGP83Qqw9uE6WDKKmDag");
                props.put(PropertyConfiguration.OAUTH_CONSUMER_SECRET, "G27yAKHnGyspLvYCwmcPnyxFVb3tF7snvupMHREEfo");
                props.put(PropertyConfiguration.MEDIA_PROVIDER_API_KEY, "8e80d7e0a4bba7466faf4583240a75bb");
                Status updateStatus = PaintActivity.this.twitter.updateStatus(new ImageUploadFactory(new PropertyConfiguration(props)).getInstance(mProvider).upload(new File(PaintActivity.this.getTwitterPic()), "Color Splash Photos"));
                return null;
            } catch (Exception e2) {
                e2.printStackTrace();
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void unused) {
        }
    }

    private void tweetAlert() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this.alertContext);
        alert.setTitle("PIN");
        alert.setMessage("Please enter PIN ");
        final EditText input = new EditText(this.alertContext);
        alert.setView(input);
        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String value = input.getText().toString().trim();
                new TwiterTask(PaintActivity.this, null).execute(value);
                dialog.cancel();
            }
        });
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.cancel();
            }
        });
        alert.show();
    }

    class AuthorizeListener implements Facebook.DialogListener {
        AuthorizeListener() {
        }

        public void onComplete(Bundle values) {
        }

        public void onFacebookError(FacebookError e) {
        }

        public void onError(DialogError e) {
        }

        public void onCancel() {
        }
    }

    public void onDestroy() {
        this.myView.graySourceBtm.recycle();
        this.myView.colorSourceBtm.recycle();
        this.shaderSourceBtm.recycle();
        if (this.menuDialog != null) {
            this.menuDialog.dismiss();
        }
        if (this.marketDialog != null) {
            this.marketDialog.dismiss();
        }
        this.menuDialog = null;
        this.marketDialog = null;
        this.myView.graySourceBtm = null;
        this.myView.colorSourceBtm = null;
        this.myView.rotateBitmap = null;
        this.myView.mShader = null;
        this.myView.rotateShader = null;
        this.shaderSourceBtm = null;
        this.mPaint.setShader(null);
        this.myView = null;
        this.briteArray = null;
        this.satArray = null;
        this.pixels = null;
        System.gc();
        super.onDestroy();
    }

    public void onStop() {
        super.onStop();
        this.menuDialog = null;
        this.marketDialog = null;
        FlurryAgent.onEndSession(this);
    }

    public void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, "BV3I7HPIX9RV7ITTV8TW");
    }

    public Object onRetainNonConfigurationInstance() {
        return null;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || isPackageLite() || !Utility.isSDCardAvialable()) {
            return super.onKeyDown(keyCode, event);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this.alertContext);
        builder.setMessage("Would you like to save image ?").setCancelable(true).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                new SavePictureTask(PaintActivity.this, null).execute(true);
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        }).setNeutralButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                PaintActivity.this.finish();
            }
        });
        builder.create().show();
        return true;
    }

    public void myClickHandler(View view) {
        switch (view.getId()) {
            case R.id.brush_ok_button:
                this.brushSize = this.brushSizeDialog.getSize();
                this.brushSizeDialog.dismiss();
                this.mPaint.setStrokeWidth(this.brushSize);
                return;
            case R.id.brush_cancel_button:
                this.brushSizeDialog.dismiss();
                return;
            case R.id.market_button:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.lyrebird.splashofcolor")));
                return;
            case R.id.market_dismiss_button:
                this.marketDialog.dismiss();
                return;
            case R.id.save_image:
                new SavePictureTask(this, null).execute((Object[]) null);
                this.menuDialog.dismiss();
                return;
            case R.id.post_facebook:
                if (this.facebook.getAccessToken() == null) {
                    this.facebookAutorization = true;
                    this.facebook.authorize(this, new String[]{"offline_access", "publish_stream"}, new AuthorizeListener());
                } else {
                    this.mAsyncRunner = new AsyncFacebookRunner(this.facebook);
                    facebookWallPost();
                }
                if (this.menuDialog != null) {
                    this.menuDialog.dismiss();
                    return;
                }
                return;
            case R.id.menu_button:
                this.menuDialog = new MenuDialog(this);
                this.menuDialog.setContentView((int) R.layout.menu_dialog);
                this.menuDialog.setCancelable(true);
                this.menuDialog.show();
                return;
            case R.id.help_button:
                startActivity(new Intent(this.mContext, HelpActivity.class));
                return;
            case R.id.mask_radiogroup:
                Button maskRadioGroup = (Button) findViewById(R.id.mask_radiogroup);
                if (this.isMasked.booleanValue()) {
                    this.isMasked = false;
                    maskRadioGroup.setBackgroundResource(R.drawable.mask_deactive);
                } else {
                    this.isMasked = true;
                    maskRadioGroup.setBackgroundResource(R.drawable.mask_active);
                }
                this.myView.invalidate();
                return;
            case R.id.brush_button:
                this.qa = new QuickAction(view);
                this.qa.addActionItem(this.firstActionItem);
                this.qa.addActionItem(this.secondActionItem);
                this.qa.addActionItem(this.thirdActionItem);
                this.qa.addActionItem(this.fourthActionItem);
                this.qa.show();
                return;
            case R.id.brush_size_button:
                this.brushSizeDialog = new BrushSizeDialog(this.alertContext, this.mPaint.getStrokeWidth());
                this.brushSizeDialog.show();
                return;
            case R.id.color_button:
                new ColorPickerDialog(this.activity, this.colorListener, this.oldColor).show();
                return;
            case R.id.pan_button:
                checkBoxButtonStateChanger(1);
                return;
            case R.id.gray_button:
                checkBoxButtonStateChanger(4);
                return;
            case R.id.paint_button:
                checkBoxButtonStateChanger(2);
                return;
            case R.id.erase_button:
                checkBoxButtonStateChanger(3);
                return;
            default:
                return;
        }
    }

    public void showMarketDialog() {
        this.marketDialog = new MarketDialog(this.alertContext);
        this.marketDialog.setContentView((int) R.layout.market_dialog_layout);
        this.marketDialog.setCancelable(true);
        this.marketDialog.show();
    }

    public boolean isPackageLite() {
        return getPackageName().toLowerCase().contains("lite");
    }

    public boolean isPackageColorMe() {
        return getPackageName().toLowerCase().contains("colorme");
    }

    private void checkBoxButtonStateChanger(int buttonState) {
        this.mPaint.setXfermode(null);
        this.mPaint.setColorFilter(null);
        this.currentShader = this.myView.mShader;
        this.isInClearState = false;
        switch (buttonState) {
            case 1:
                this.panZoom = true;
                this.panButton.setBackgroundResource(R.drawable.pan_active);
                this.paintButton.setBackgroundResource(R.drawable.paint);
                this.eraseButton.setBackgroundResource(R.drawable.erase);
                this.grayButton.setBackgroundResource(R.drawable.gray);
                break;
            case 2:
                this.panZoom = false;
                if (this.PAINT_STATE == 1) {
                    this.PAINT_STATE = this.SAVED_STATE;
                }
                this.panButton.setBackgroundResource(R.drawable.pan);
                this.paintButton.setBackgroundResource(R.drawable.paint_active);
                this.eraseButton.setBackgroundResource(R.drawable.erase);
                this.grayButton.setBackgroundResource(R.drawable.gray);
                break;
            case 3:
                this.isInClearState = true;
                this.panZoom = false;
                this.mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
                if (this.PAINT_STATE != 1) {
                    this.SAVED_STATE = this.PAINT_STATE;
                }
                this.PAINT_STATE = 1;
                this.panButton.setBackgroundResource(R.drawable.pan);
                this.paintButton.setBackgroundResource(R.drawable.paint);
                this.eraseButton.setBackgroundResource(R.drawable.erase_active);
                this.grayButton.setBackgroundResource(R.drawable.gray);
                break;
            case 4:
                this.panZoom = false;
                ColorMatrix cm = new ColorMatrix();
                cm.setSaturation(0.0f);
                this.mPaint.setColorFilter(new ColorMatrixColorFilter(cm));
                this.currentShader = this.grayShader;
                this.panButton.setBackgroundResource(R.drawable.pan);
                this.paintButton.setBackgroundResource(R.drawable.paint);
                this.eraseButton.setBackgroundResource(R.drawable.erase);
                this.grayButton.setBackgroundResource(R.drawable.gray_active);
                break;
        }
        this.mPaint.setShader(null);
        this.mPaint.setShader(this.currentShader);
    }

    private void setupUI() {
        this.brushButton = (Button) findViewById(R.id.brush_button);
        this.firstActionItem = new ActionItem();
        this.firstActionItem.setTitle("");
        this.firstActionItem.setIcon(getResources().getDrawable(R.drawable.brush1));
        this.firstActionItem.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PaintActivity.this.mPaint.setAlpha(MyMotionEvent.ACTION_MASK);
                PaintActivity.this.mPaint.setMaskFilter(null);
                PaintActivity.this.PAINT_STATE = 0;
                PaintActivity.this.qa.dismiss();
            }
        });
        this.secondActionItem = new ActionItem();
        this.secondActionItem.setTitle("");
        this.secondActionItem.setIcon(getResources().getDrawable(R.drawable.brush2));
        this.secondActionItem.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PaintActivity.this.mPaint.setAlpha(MyMotionEvent.ACTION_MASK);
                PaintActivity.this.mPaint.setMaskFilter(PaintActivity.this.mBlur);
                PaintActivity.this.PAINT_STATE = 2;
                PaintActivity.this.qa.dismiss();
            }
        });
        this.thirdActionItem = new ActionItem();
        this.thirdActionItem.setTitle("");
        this.thirdActionItem.setIcon(getResources().getDrawable(R.drawable.brush3));
        this.thirdActionItem.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PaintActivity.this.mPaint.setAlpha(MyMotionEvent.ACTION_MASK);
                PaintActivity.this.mPaint.setMaskFilter(PaintActivity.this.mEmboss);
                PaintActivity.this.PAINT_STATE = 3;
                PaintActivity.this.qa.dismiss();
            }
        });
        this.fourthActionItem = new ActionItem();
        this.fourthActionItem.setTitle("");
        this.fourthActionItem.setIcon(getResources().getDrawable(R.drawable.brush4));
        this.fourthActionItem.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PaintActivity.this.mPaint.setAlpha(68);
                PaintActivity.this.mPaint.setMaskFilter(null);
                PaintActivity.this.PAINT_STATE = 4;
                PaintActivity.this.qa.dismiss();
            }
        });
    }

    private class SavePictureTask extends AsyncTask<Object, Object, Object> {
        boolean finish;

        private SavePictureTask() {
            this.finish = false;
        }

        /* synthetic */ SavePictureTask(PaintActivity paintActivity, SavePictureTask savePictureTask) {
            this();
        }

        /* access modifiers changed from: protected */
        public Object doInBackground(Object... arg0) {
            if (arg0 != null && ((Boolean) arg0[0]).booleanValue()) {
                this.finish = true;
            }
            Bitmap screenshot = Bitmap.createBitmap(PaintActivity.this.myView.sourceWidth, PaintActivity.this.myView.sourceHeight, Bitmap.Config.ARGB_8888);
            Canvas cvs = new Canvas(screenshot);
            Paint paint = new Paint();
            Paint paint2 = new Paint();
            paint.setStyle(Paint.Style.FILL);
            cvs.drawBitmap(PaintActivity.this.myView.graySourceBtm, 0.0f, 0.0f, paint);
            cvs.drawBitmap(PaintActivity.this.myView.colorSourceBtm, 0.0f, 0.0f, paint2);
            try {
                PaintActivity.this.twitterUploadFile = String.valueOf(System.currentTimeMillis());
                FileOutputStream out = new FileOutputStream(String.valueOf(Environment.getExternalStorageDirectory().toString()) + PaintActivity.this.getString(R.string.Directory) + PaintActivity.this.twitterUploadFile + ".png");
                screenshot.compress(Bitmap.CompressFormat.PNG, 90, out);
                screenshot.recycle();
                out.flush();
                out.close();
                return null;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Object result) {
            super.onPostExecute(result);
            Toast msg = Toast.makeText(PaintActivity.this.mContext, "Image has been saved to the /SD/colorme/ folder.", 1);
            msg.setGravity(17, msg.getXOffset() / 2, msg.getYOffset() / 2);
            msg.show();
            PaintActivity.this.sendBroadcast(new Intent("android.intent.action.MEDIA_MOUNTED", Uri.parse("file://" + Environment.getExternalStorageDirectory())));
            if (this.finish) {
                PaintActivity.this.finish();
            }
        }
    }

    private void setMinZoom() {
        this.MIN_ZOOM = Math.min(((float) this.screenWidth) / ((float) this.myView.sourceWidth), ((float) (this.bottomHeightOffset - this.topHeightOffset)) / ((float) this.myView.sourceHeight));
        Log.e("bottomHeightOffset", String.valueOf(this.bottomHeightOffset));
        Log.e("screenHeight", String.valueOf(this.screenHeight));
    }

    public class MyView extends View {
        static final int DRAG = 1;
        static final int NONE = 0;
        private static final float TOUCH_TOLERANCE = 4.0f;
        static final int ZOOM = 2;
        public Bitmap colorSourceBtm;
        int direction = 0;
        private Paint grayPaint = new Paint();
        /* access modifiers changed from: private */
        public Bitmap graySourceBtm;
        boolean isPointerDown = false;
        boolean isYPositionSet = false;
        PointF leftTopCorner = new PointF();
        public Paint mBitmapPaint = new Paint();
        private Canvas mCanvas;
        private Path mPath;
        BitmapShader mShader;
        private float mX;
        private float mY;
        Bitmap maskBitmap;
        Paint maskPaint;
        public Matrix matrix = new Matrix();
        PointF mid = new PointF();
        PointF midPoint = new PointF();
        int mode = 0;
        int oldDirection = 1;
        float oldDist = 1.0f;
        PointF oldPointerOne = new PointF();
        PointF oldPointerTwo = new PointF();
        PointF pointerOne = new PointF();
        PointF pointerTwo = new PointF();
        PointF rigtBottomCorner = new PointF();
        Bitmap rotateBitmap;
        BitmapShader rotateShader;
        public Matrix savedMatrix = new Matrix();
        /* access modifiers changed from: private */
        public int sourceHeight;
        /* access modifiers changed from: private */
        public int sourceWidth;
        PointF start = new PointF();
        Paint surroundPaint = new Paint();
        float[] values = new float[9];
        float[] zoomLimitMatrixValues = new float[9];

        private Bitmap decodeFile() {
            float IMAGE_MAX_SIZE;
            Display display = PaintActivity.this.getWindowManager().getDefaultDisplay();
            float displayWidth = ((float) display.getWidth()) * PaintActivity.this.SCALE;
            float displayHeight = ((float) display.getHeight()) * PaintActivity.this.SCALE;
            if (getResources().getConfiguration().orientation == 1) {
                IMAGE_MAX_SIZE = displayWidth;
            } else {
                IMAGE_MAX_SIZE = displayHeight;
            }
            if (PaintActivity.this.MAX_SIZE > 0) {
                IMAGE_MAX_SIZE = (float) (PaintActivity.this.MAX_SIZE * 2);
            }
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(PaintActivity.this.selectedImagePath, o);
            int scale = 1;
            if (((float) o.outHeight) > IMAGE_MAX_SIZE || ((float) o.outWidth) > IMAGE_MAX_SIZE) {
                scale = (int) Math.pow(2.0d, (double) ((int) Math.round(Math.log(((double) IMAGE_MAX_SIZE) / ((double) Math.max(o.outHeight, o.outWidth))) / Math.log(0.5d))));
            }
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            ErrorReporter.getInstance().putCustomData("selectedImagePath in decodeFile", PaintActivity.this.selectedImagePath);
            Bitmap b = BitmapFactory.decodeFile(PaintActivity.this.selectedImagePath, o2);
            if (PaintActivity.this.sizeOption == 1) {
                return Bitmap.createScaledBitmap(b, (int) (((double) b.getWidth()) / 1.5d), (int) (((double) b.getHeight()) / 1.5d), false);
            }
            if (PaintActivity.this.sizeOption == 0) {
                return Bitmap.createScaledBitmap(b, (int) (((double) b.getWidth()) / 2.4d), (int) (((double) b.getHeight()) / 2.4d), false);
            }
            return b;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
         arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
         candidates:
          ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
          ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
        public MyView(Context context) {
            super(context);
            String o1 = "";
            try {
                o1 = new ExifInterface(PaintActivity.this.selectedImagePath).getAttribute("Orientation");
                if (o1 != null) {
                    Log.e("exif", o1);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            o1 = o1 == null ? "" : o1;
            if (o1.contentEquals("6")) {
                Bitmap localBitmap = decodeFile();
                Matrix localMatrix = new Matrix();
                localMatrix.postRotate(90.0f);
                this.graySourceBtm = Bitmap.createBitmap(localBitmap, 0, 0, localBitmap.getWidth(), localBitmap.getHeight(), localMatrix, false);
                localBitmap.recycle();
            } else if (o1.contentEquals("8")) {
                Bitmap localBitmap2 = decodeFile();
                Matrix localMatrix2 = new Matrix();
                localMatrix2.postRotate(270.0f);
                this.graySourceBtm = Bitmap.createBitmap(localBitmap2, 0, 0, localBitmap2.getWidth(), localBitmap2.getHeight(), localMatrix2, false);
                localBitmap2.recycle();
            } else if (o1.contentEquals(Const.PROTOCOL_VERSION)) {
                Bitmap localBitmap3 = decodeFile();
                Matrix localMatrix3 = new Matrix();
                localMatrix3.postRotate(180.0f);
                this.graySourceBtm = Bitmap.createBitmap(localBitmap3, 0, 0, localBitmap3.getWidth(), localBitmap3.getHeight(), localMatrix3, false);
                localBitmap3.recycle();
            } else {
                this.graySourceBtm = decodeFile();
            }
            this.sourceHeight = this.graySourceBtm.getHeight();
            this.sourceWidth = this.graySourceBtm.getWidth();
            this.colorSourceBtm = Bitmap.createBitmap(this.sourceWidth, this.sourceHeight, Bitmap.Config.ARGB_8888);
            PaintActivity.this.shaderSourceBtm = Bitmap.createBitmap(this.sourceWidth, this.sourceHeight, Bitmap.Config.RGB_565);
            PaintActivity.this.pixels = new int[(this.sourceWidth * this.sourceHeight)];
            PaintActivity.this.briteArray = new double[(this.sourceWidth * this.sourceHeight)];
            PaintActivity.this.satArray = new double[(this.sourceWidth * this.sourceHeight)];
            this.graySourceBtm.getPixels(PaintActivity.this.pixels, 0, this.sourceWidth, 0, 0, this.sourceWidth, this.sourceHeight);
            PaintActivity.this.setHueArrays();
            this.mCanvas = new Canvas(this.colorSourceBtm);
            this.mPath = new Path();
            this.mBitmapPaint = new Paint(4);
            this.mShader = new BitmapShader(this.graySourceBtm, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
            PaintActivity.this.grayShader = new BitmapShader(this.graySourceBtm, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
            this.maskBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.mask_pattern2);
            BitmapShader bitmapShader = new BitmapShader(this.maskBitmap, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
            new Matrix().preRotate(90.0f);
            this.maskPaint = new Paint();
            this.maskPaint.setShader(bitmapShader);
            this.maskPaint.setAlpha(168);
            PaintActivity.this.currentShader = this.mShader;
            PaintActivity.this.mPaint.setShader(PaintActivity.this.currentShader);
            this.surroundPaint.setColor(-7829368);
            PaintActivity.this.notBlackClearPaint = new Paint();
            PaintActivity.this.notBlackClearPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST));
            PaintActivity.this.notBlackClearPaint.setColor(0);
            float scale = Math.min(((float) PaintActivity.this.screenWidth) / ((float) this.sourceWidth), ((float) (PaintActivity.this.bottomHeightOffset - PaintActivity.this.topHeightOffset)) / ((float) this.sourceHeight));
            Log.e("screenWidth", String.valueOf(PaintActivity.this.screenWidth));
            Log.e("screenHeight", String.valueOf(PaintActivity.this.screenHeight));
            this.matrix.postScale(scale, scale, (float) (PaintActivity.this.screenWidth / 2), (float) (PaintActivity.this.screenHeight / 2));
            this.matrix.getValues(this.values);
            PaintActivity.this.MIN_ZOOM = scale;
            Log.e("sourceWidth", String.valueOf(this.sourceWidth));
            Log.e("MIN_ZOOM", String.valueOf(PaintActivity.this.MIN_ZOOM));
        }

        public int getId() {
            return super.getId();
        }

        public void setInitialYPosition() {
            float[] values2 = new float[9];
            this.matrix.getValues(values2);
            if (getResources().getConfiguration().orientation == 1) {
                if (PaintActivity.this.toolbar.getMeasuredHeight() > 0) {
                    PaintActivity.this.initialYPosition = PaintActivity.this.toolbar.getMeasuredHeight();
                    this.isYPositionSet = true;
                }
                int footerHeight = 0;
                if (PaintActivity.this.footer.getMeasuredHeight() > 0) {
                    footerHeight = PaintActivity.this.footer.getMeasuredHeight();
                }
                values2[5] = (float) PaintActivity.this.initialYPosition;
                PaintActivity.this.topHeightOffset += PaintActivity.this.initialYPosition;
                PaintActivity.this.bottomHeightOffset -= footerHeight;
            } else {
                if (PaintActivity.this.toolbar.getMeasuredWidth() > 0) {
                    PaintActivity.this.initialYPosition = PaintActivity.this.toolbar.getMeasuredWidth();
                    this.isYPositionSet = true;
                }
                values2[2] = (float) PaintActivity.this.initialYPosition;
            }
            PaintActivity.this.addFullScreenButton();
            this.matrix.setValues(values2);
        }

        /* access modifiers changed from: protected */
        public void onSizeChanged(int w, int h, int oldw, int oldh) {
            super.onSizeChanged(w, h, oldw, oldh);
            Log.e("w", String.valueOf(getWidth()));
            Log.e("h", String.valueOf(getHeight()));
            Log.e("sizeChanged", "true");
        }

        /* access modifiers changed from: protected */
        public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            Log.e("onMeasure", "true");
        }

        public void setBrushSize() {
            float width = 40.0f;
            if (this.values[0] > 0.0f) {
                width = PaintActivity.this.brushSize / this.values[0];
            }
            PaintActivity.this.mPaint.setStrokeWidth(width);
        }

        public void onDraw(Canvas canvas) {
            if (!this.isYPositionSet) {
                setInitialYPosition();
            }
            checkSreenBounds();
            this.matrix.getValues(this.values);
            canvas.setMatrix(this.matrix);
            canvas.drawBitmap(this.graySourceBtm, 0.0f, 0.0f, this.grayPaint);
            if (PaintActivity.this.isMasked.booleanValue()) {
                canvas.saveLayer(0.0f, 0.0f, (float) this.sourceWidth, (float) this.sourceHeight, null, 31);
            }
            if (PaintActivity.this.isInClearState) {
                int saveLayer = canvas.saveLayer(0.0f, 0.0f, (float) this.sourceWidth, (float) this.sourceHeight, null, 31);
                Log.e("clear2", "clear2");
            }
            canvas.drawBitmap(this.colorSourceBtm, 0.0f, 0.0f, this.mBitmapPaint);
            canvas.drawPath(this.mPath, PaintActivity.this.mPaint);
            if (PaintActivity.this.isInClearState) {
                Log.e("clear2", "clear2");
                canvas.drawPaint(PaintActivity.this.notBlackClearPaint);
            }
            if (PaintActivity.this.isMasked.booleanValue()) {
                this.maskPaint.setXfermode(null);
                this.maskPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
                canvas.drawPaint(this.maskPaint);
            }
            canvas.drawRect((float) (-this.sourceWidth), (float) (-this.sourceHeight), 0.0f, (float) (this.sourceHeight * 2), this.surroundPaint);
            canvas.drawRect((float) this.sourceWidth, (float) (-this.sourceHeight), (float) (this.sourceWidth * 2), (float) (this.sourceHeight * 2), this.surroundPaint);
            canvas.drawRect(0.0f, (float) (-this.sourceHeight), (float) this.sourceWidth, 0.0f, this.surroundPaint);
            canvas.drawRect(0.0f, (float) this.sourceHeight, (float) this.sourceWidth, (float) (this.sourceHeight * 2), this.surroundPaint);
        }

        public void checkSreenBounds() {
            int imageWidth = this.sourceWidth;
            int imageHeight = this.sourceHeight;
            this.matrix.getValues(this.values);
            if (this.values[0] < PaintActivity.this.MIN_ZOOM) {
                this.matrix.postScale(PaintActivity.this.MIN_ZOOM / this.values[0], PaintActivity.this.MIN_ZOOM / this.values[0], this.mid.x, this.mid.y);
                this.matrix.getValues(this.values);
            }
            if (this.values[0] > PaintActivity.this.MAX_ZOOM) {
                this.matrix.postScale(PaintActivity.this.MAX_ZOOM / this.values[0], PaintActivity.this.MAX_ZOOM / this.values[0], (float) (PaintActivity.this.screenWidth / 2), (float) (PaintActivity.this.screenHeight / 2));
                this.matrix.getValues(this.values);
            }
            this.leftTopCorner.x = this.values[2];
            this.leftTopCorner.y = this.values[5];
            this.rigtBottomCorner.x = this.values[2] + (((float) imageWidth) * this.values[0]);
            this.rigtBottomCorner.y = this.values[5] + (((float) imageHeight) * this.values[0]);
            this.midPoint.x = (this.leftTopCorner.x + this.rigtBottomCorner.x) / 2.0f;
            this.midPoint.y = (this.leftTopCorner.y + this.rigtBottomCorner.y) / 2.0f;
            if (((float) imageHeight) * this.values[0] > ((float) (PaintActivity.this.bottomHeightOffset - PaintActivity.this.topHeightOffset))) {
                if (this.leftTopCorner.y > ((float) PaintActivity.this.topHeightOffset)) {
                    this.values[5] = (float) PaintActivity.this.topHeightOffset;
                }
                if (this.rigtBottomCorner.y < ((float) PaintActivity.this.bottomHeightOffset)) {
                    this.values[5] = ((float) PaintActivity.this.bottomHeightOffset) - (((float) imageHeight) * this.values[0]);
                }
            } else if (this.midPoint.y != ((float) (PaintActivity.this.screenHeight / 2))) {
                this.values[5] = (((float) (PaintActivity.this.bottomHeightOffset + PaintActivity.this.topHeightOffset)) - (((float) imageHeight) * this.values[0])) / 2.0f;
            }
            if (((float) imageWidth) * this.values[0] >= ((float) PaintActivity.this.screenWidth)) {
                if (this.leftTopCorner.x > 0.0f) {
                    this.values[2] = 0.0f;
                }
                if (this.rigtBottomCorner.x < ((float) PaintActivity.this.screenWidth)) {
                    this.values[2] = ((float) PaintActivity.this.screenWidth) - (((float) imageWidth) * this.values[0]);
                }
            } else if (this.midPoint.x != ((float) (PaintActivity.this.screenWidth / 2))) {
                this.values[2] = (((float) PaintActivity.this.screenWidth) - (((float) imageWidth) * this.values[0])) / 2.0f;
            }
            this.matrix.setValues(this.values);
        }

        private void touch_start(float x, float y) {
            float x2 = (x - this.values[2]) / this.values[0];
            float y2 = (y - this.values[5]) / this.values[0];
            this.mPath.reset();
            this.mPath.moveTo(x2, y2);
            this.mX = x2;
            this.mY = y2;
        }

        private void touch_move(float x, float y) {
            float x2 = (x - this.values[2]) / this.values[0];
            float y2 = (y - this.values[5]) / this.values[0];
            float dx = Math.abs(x2 - this.mX);
            float dy = Math.abs(y2 - this.mY);
            if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
                this.mPath.quadTo(this.mX, this.mY, (this.mX + x2) / 2.0f, (this.mY + y2) / 2.0f);
                this.mX = x2;
                this.mY = y2;
            }
        }

        private void touch_up() {
            this.mPath.lineTo(this.mX, this.mY);
            this.mCanvas.drawPath(this.mPath, PaintActivity.this.mPaint);
            this.mPath.reset();
            PaintActivity.this.logFreeMemory();
        }

        public boolean onTouchEvent(MotionEvent event) {
            float x = event.getX();
            float y = event.getY();
            if (Build.VERSION.SDK_INT >= 7) {
                switch (event.getAction() & MyMotionEvent.ACTION_MASK) {
                    case 0:
                        if (PaintActivity.this.panZoom) {
                            this.savedMatrix.set(this.matrix);
                            this.start.set(event.getX(), event.getY());
                            this.mode = 1;
                        } else {
                            touch_start(x, y);
                        }
                        invalidate();
                        return true;
                    case 1:
                        if (PaintActivity.this.panZoom) {
                            this.mode = 0;
                        } else if (!this.isPointerDown) {
                            touch_up();
                        } else {
                            this.isPointerDown = false;
                        }
                        invalidate();
                        return true;
                    case 2:
                        if (PaintActivity.this.panZoom) {
                            if (this.mode == 1) {
                                this.matrix.set(this.savedMatrix);
                                this.matrix.postTranslate(event.getX() - this.start.x, event.getY() - this.start.y);
                            } else if (this.mode == 2) {
                                float newDist = MyMotionEvent.spacing(event);
                                if (newDist > 10.0f) {
                                    this.matrix.set(this.savedMatrix);
                                    float scale = newDist / this.oldDist;
                                    this.matrix.postScale(scale, scale, this.mid.x, this.mid.y);
                                }
                            }
                        } else if (this.isPointerDown) {
                            try {
                                this.pointerOne.x = event.getX(0);
                                this.pointerOne.y = event.getY(0);
                                this.pointerTwo.x = event.getX(1);
                                this.pointerTwo.y = event.getY(1);
                            } catch (Exception e) {
                            }
                            float fx = this.pointerOne.x - this.oldPointerOne.x;
                            float fy = this.pointerOne.y - this.oldPointerOne.y;
                            float fd = (float) Math.sqrt((double) ((fx * fx) + (fy * fy)));
                            PointF pointF = new PointF(fx / fd, fy / fd);
                            float fx2 = this.pointerTwo.x - this.oldPointerTwo.x;
                            float fy2 = this.pointerTwo.y - this.oldPointerTwo.y;
                            float fd2 = (float) Math.sqrt((double) ((fx2 * fx2) + (fy2 * fy2)));
                            PointF pointF2 = new PointF(fx2 / fd2, fy2 / fd2);
                            float angle = (float) Math.toDegrees(Math.acos((double) ((pointF.x * pointF2.x) + (pointF.y * pointF2.y))));
                            float newDist2 = MyMotionEvent.spacing(event);
                            if (newDist2 <= 10.0f || angle <= 90.0f) {
                                if (angle <= 90.0f) {
                                    this.matrix.set(this.savedMatrix);
                                    this.matrix.postTranslate(event.getX() - this.start.x, event.getY() - this.start.y);
                                }
                            } else if (newDist2 > 10.0f) {
                                this.matrix.set(this.savedMatrix);
                                float scale2 = newDist2 / this.oldDist;
                                this.matrix.postScale(scale2, scale2, this.mid.x, this.mid.y);
                            }
                        } else {
                            touch_move(x, y);
                        }
                        invalidate();
                        return true;
                    case 3:
                    case 4:
                    default:
                        return true;
                    case 5:
                        if (PaintActivity.this.panZoom) {
                            try {
                                this.oldDist = MyMotionEvent.spacing(event);
                            } catch (Exception e2) {
                            }
                            if (this.oldDist <= 10.0f) {
                                return true;
                            }
                            this.savedMatrix.set(this.matrix);
                            MyMotionEvent.midPoint(this.mid, event);
                            this.mode = 2;
                            return true;
                        }
                        try {
                            this.oldDist = MyMotionEvent.spacing(event);
                            this.start.set(event.getX(), event.getY());
                            this.oldPointerOne.set(event.getX(0), event.getY(0));
                            this.oldPointerTwo.set(event.getX(1), event.getY(1));
                        } catch (Exception e3) {
                        }
                        this.isPointerDown = true;
                        if (this.oldDist <= 10.0f) {
                            return true;
                        }
                        this.savedMatrix.set(this.matrix);
                        MyMotionEvent.midPoint(this.mid, event);
                        return true;
                }
            } else {
                switch (event.getAction()) {
                    case 0:
                        if (PaintActivity.this.panZoom) {
                            this.savedMatrix.set(this.matrix);
                            this.start.set(event.getX(), event.getY());
                            this.mode = 1;
                        } else {
                            touch_start(x, y);
                        }
                        invalidate();
                        return true;
                    case 1:
                        if (PaintActivity.this.panZoom) {
                            this.mode = 0;
                        } else {
                            touch_up();
                        }
                        invalidate();
                        return true;
                    case 2:
                        if (!PaintActivity.this.panZoom) {
                            touch_move(x, y);
                        } else if (this.mode == 1) {
                            this.matrix.set(this.savedMatrix);
                            this.matrix.postTranslate(event.getX() - this.start.x, event.getY() - this.start.y);
                        }
                        invalidate();
                        return true;
                    default:
                        return true;
                }
            }
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 2, 0, "Preview").setShortcut('4', 'g');
        menu.add(0, 3, 0, "COLOR").setShortcut('4', 'g');
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 2:
                if (this.sliderDrawer.getVisibility() == 0) {
                    this.restoreMatrix.set(this.myView.matrix);
                    RectF photoRect = new RectF();
                    RectF canvasRect = new RectF();
                    Display display = getWindowManager().getDefaultDisplay();
                    photoRect.set(0.0f, 0.0f, (float) this.myView.sourceWidth, (float) this.myView.sourceHeight);
                    canvasRect.set(0.0f, 0.0f, (float) display.getWidth(), (float) display.getHeight());
                    this.myView.matrix.setRectToRect(photoRect, canvasRect, Matrix.ScaleToFit.START);
                    this.sliderDrawer.setVisibility(4);
                } else {
                    this.myView.matrix.set(this.restoreMatrix);
                    this.sliderDrawer.setVisibility(0);
                }
                this.myView.invalidate();
                return true;
            case 3:
                new ColorPickerDialog(this.activity, this.colorListener, this.oldColor).show();
                return true;
            case 4:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void adWhirlGeneric() {
    }

    public void colorChanged(int color, double hueVal) {
        this.oldColor = color;
        setPix(hueVal);
        Log.e("shaderSourceBtm.getHeight()", String.valueOf(this.shaderSourceBtm.getHeight()));
        Log.e("shaderSourceBtm.geteifht()", String.valueOf(this.shaderSourceBtm.getWidth()));
        this.shaderSourceBtm.setPixels(this.pixels, 0, this.shaderSourceBtm.getWidth(), 0, 0, this.shaderSourceBtm.getWidth(), this.shaderSourceBtm.getHeight());
        this.myView.mShader = new BitmapShader(this.shaderSourceBtm, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        if (this.currentShader != this.grayShader) {
            this.currentShader = this.myView.mShader;
        }
        this.mPaint.setShader(null);
        this.mPaint.setShader(this.currentShader);
    }

    /* access modifiers changed from: private */
    public void setHueArrays() {
        for (int i = 0; i < this.pixels.length; i++) {
            rgb2Hsb(this.pixels[i], i);
        }
    }

    private double rgb2Hsb(int RGB, int index) {
        double red = ((double) ((RGB >> 16) & MyMotionEvent.ACTION_MASK)) / 255.0d;
        double green = ((double) ((RGB >> 8) & MyMotionEvent.ACTION_MASK)) / 255.0d;
        double blue = ((double) (RGB & MyMotionEvent.ACTION_MASK)) / 255.0d;
        double dmax = Math.max(Math.max(red, green), blue);
        double range = dmax - Math.min(Math.min(red, green), blue);
        double brite = dmax;
        double sat = 0.0d;
        double hue = 0.0d;
        if (dmax != 0.0d) {
            sat = range / dmax;
        }
        if (sat != 0.0d) {
            if (red == dmax) {
                hue = (green - blue) / range;
            } else if (green == dmax) {
                hue = 2.0d + ((blue - red) / range);
            } else if (blue == dmax) {
                hue = 4.0d + ((green - red) / range);
            }
            hue *= 60.0d;
            if (hue < 0.0d) {
                hue += 360.0d;
            }
        }
        if (index != -1) {
            this.briteArray[index] = brite;
            this.satArray[index] = sat;
        }
        return hue;
    }

    public void setPix(double hueVal) {
        for (int i = 0; i < this.pixels.length; i++) {
            this.pixels[i] = hsb2Rgb(hueVal, this.satArray[i], this.briteArray[i]);
        }
    }

    private int hsb2Rgb(double hue, double sat, double brite) {
        double red;
        double green;
        double blue;
        if (sat != 0.0d) {
            if (hue == 360.0d) {
                hue = 0.0d;
            }
            int slice = (int) (hue / 60.0d);
            double hue_frac = (hue / 60.0d) - ((double) slice);
            double aa = brite * (1.0d - sat);
            double bb = brite * (1.0d - (sat * hue_frac));
            double cc = brite * (1.0d - ((1.0d - hue_frac) * sat));
            switch (slice) {
                case 0:
                    red = brite;
                    green = cc;
                    blue = aa;
                    break;
                case 1:
                    red = bb;
                    green = brite;
                    blue = aa;
                    break;
                case 2:
                    red = aa;
                    green = brite;
                    blue = cc;
                    break;
                case 3:
                    red = aa;
                    green = bb;
                    blue = brite;
                    break;
                case 4:
                    red = cc;
                    green = aa;
                    blue = brite;
                    break;
                case 5:
                    red = brite;
                    green = aa;
                    blue = bb;
                    break;
                default:
                    red = 0.0d;
                    green = 0.0d;
                    blue = 0.0d;
                    break;
            }
        } else {
            red = brite;
            green = brite;
            blue = brite;
        }
        return (((int) (255.0d * red)) << 16) | (((int) (255.0d * green)) << 8) | ((int) (255.0d * blue));
    }
}
