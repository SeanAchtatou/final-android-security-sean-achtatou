package com.lyrebird.colorreplacer;

import android.app.Activity;
import android.util.Log;
import com.adwhirl.AdWhirlLayout;

public class MyAdWhirlLayout extends AdWhirlLayout {
    PaintActivity activity;

    public MyAdWhirlLayout(Activity context, String keyAdWhirl) {
        super(context, keyAdWhirl);
        this.activity = (PaintActivity) context;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        this.activity.adWhirlMeasureChanged(0);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        Log.e("adwhirl onSizeChanged", String.valueOf(h));
        this.activity.adWhirlMeasureChanged(h - oldh);
    }
}
