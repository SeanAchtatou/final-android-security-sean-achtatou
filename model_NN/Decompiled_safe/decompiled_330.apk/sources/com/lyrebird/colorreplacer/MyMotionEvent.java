package com.lyrebird.colorreplacer;

import android.graphics.PointF;
import android.util.FloatMath;
import android.view.MotionEvent;

public class MyMotionEvent {
    public static final int ACTION_MASK = 255;
    public static final int ACTION_POINTER_DOWN = 5;

    public static float spacing(MotionEvent event) {
        try {
            float x = event.getX(0) - event.getX(1);
            float y = event.getY(0) - event.getY(1);
            return FloatMath.sqrt((x * x) + (y * y));
        } catch (Exception e) {
            return 9.0f;
        }
    }

    public static void midPoint(PointF point, MotionEvent event) {
        point.set((event.getX(0) + event.getX(1)) / 2.0f, (event.getY(0) + event.getY(1)) / 2.0f);
    }
}
