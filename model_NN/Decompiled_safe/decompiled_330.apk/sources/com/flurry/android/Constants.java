package com.flurry.android;

public interface Constants {
    public static final int FEMALE = 0;
    public static final int MALE = 1;
    public static final int MODE_LANDSCAPE = 2;
    public static final int MODE_PORTRAIT = 1;
    public static final int UNKNOWN = -1;
}
