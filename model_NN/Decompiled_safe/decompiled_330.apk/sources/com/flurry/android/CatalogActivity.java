package com.flurry.android;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import com.google.ads.AdActivity;
import java.util.ArrayList;
import java.util.List;

public class CatalogActivity extends Activity implements View.OnClickListener {
    private WebView a;
    private p b;
    private List c = new ArrayList();
    /* access modifiers changed from: private */
    public n d;
    /* access modifiers changed from: private */
    public y e;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        setTheme(16973839);
        super.onCreate(bundle);
        this.d = FlurryAgent.b();
        Long valueOf = Long.valueOf(getIntent().getExtras().getLong(AdActivity.ORIENTATION_PARAM));
        if (valueOf != null) {
            this.e = this.d.b(valueOf.longValue());
        }
        v vVar = new v(this, this);
        vVar.setId(1);
        vVar.setBackgroundColor(-16777216);
        this.a = new WebView(this);
        this.a.setId(2);
        this.a.setScrollBarStyle(0);
        this.a.setBackgroundColor(-1);
        this.a.setWebViewClient(new g(this));
        this.a.getSettings().setJavaScriptEnabled(true);
        this.b = new p(this, this);
        this.b.setId(3);
        RelativeLayout relativeLayout = new RelativeLayout(this);
        relativeLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(10, vVar.getId());
        relativeLayout.addView(vVar, layoutParams);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams2.addRule(3, vVar.getId());
        layoutParams2.addRule(2, this.b.getId());
        relativeLayout.addView(this.a, layoutParams2);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams3.addRule(12, vVar.getId());
        relativeLayout.addView(this.b, layoutParams3);
        this.a.loadUrl(getIntent().getExtras().getString(AdActivity.URL_PARAM));
        setContentView(relativeLayout);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.d.f();
        super.onDestroy();
    }

    public void onClick(View view) {
        if (view instanceof r) {
            q qVar = new q();
            qVar.a = this.e;
            qVar.b = this.a.getUrl();
            qVar.c = new ArrayList(this.b.b());
            this.c.add(qVar);
            if (this.c.size() > 5) {
                this.c.remove(0);
            }
            q qVar2 = new q();
            r rVar = (r) view;
            String b2 = rVar.b(this.d.h());
            this.e = rVar.a();
            qVar2.a = rVar.a();
            qVar2.a.a(new i((byte) 4, this.d.i()));
            qVar2.b = b2;
            qVar2.c = this.b.a(view.getContext());
            a(qVar2);
        } else if (view.getId() == 10000) {
            finish();
        } else if (view.getId() == 10002) {
            this.b.a();
        } else if (this.c.isEmpty()) {
            finish();
        } else {
            a((q) this.c.remove(this.c.size() - 1));
        }
    }

    private void a(q qVar) {
        z.a("FlurryAgent", "Loading url: " + qVar.b);
        try {
            this.a.loadUrl(qVar.b);
            this.b.a(qVar.c);
        } catch (Exception e2) {
            z.a("FlurryAgent", "Error loading url: " + qVar.b);
        }
    }
}
