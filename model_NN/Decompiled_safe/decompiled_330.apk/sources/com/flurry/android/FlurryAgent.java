package com.flurry.android;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.SystemClock;
import android.view.View;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;

public final class FlurryAgent implements LocationListener {
    private static volatile String a = null;
    private static volatile String b = null;
    private static volatile String c = "http://ad.flurry.com/getCanvas.do";
    private static volatile String d = null;
    private static volatile String e = "http://ad.flurry.com/getAndroidApp.do";
    /* access modifiers changed from: private */
    public static final FlurryAgent f = new FlurryAgent();
    private static long g = 10000;
    private static boolean h = true;
    private static boolean i = false;
    private static boolean j = true;
    private static Criteria k = null;
    private static volatile String kInsecureReportUrl = "http://data.flurry.com/aap.do";
    private static volatile String kSecureReportUrl = "https://data.flurry.com/aap.do";
    private static boolean l = false;
    private static AppCircle m = new AppCircle();
    private boolean A;
    private long B;
    private List C = new ArrayList();
    private long D;
    private long E;
    private long F;
    private String G = "";
    private String H = "";
    private byte I = -1;
    private String J;
    private byte K = -1;
    private Long L;
    private int M;
    private Location N;
    private Map O;
    private List P;
    private boolean Q;
    private int R;
    private List S;
    private int T;
    private n U;
    private final Handler n;
    private File o = null;
    private boolean p = false;
    private boolean q = false;
    private long r;
    private Map s = new WeakHashMap();
    private String t;
    private String u;
    private String v;
    private boolean w = true;
    private List x;
    private LocationManager y;
    private String z;

    static /* synthetic */ void a(FlurryAgent flurryAgent, Context context) {
        boolean z2;
        try {
            synchronized (flurryAgent) {
                z2 = !flurryAgent.p && SystemClock.elapsedRealtime() - flurryAgent.r > g && flurryAgent.C.size() > 0;
            }
            if (z2) {
                flurryAgent.c(false);
            }
        } catch (Throwable th) {
            z.b("FlurryAgent", "", th);
        }
    }

    static /* synthetic */ void a(FlurryAgent flurryAgent, Context context, boolean z2) {
        Location location = null;
        if (z2) {
            try {
                location = flurryAgent.b(context);
            } catch (Throwable th) {
                z.b("FlurryAgent", "", th);
                return;
            }
        }
        synchronized (flurryAgent) {
            flurryAgent.N = location;
        }
        flurryAgent.c(true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.FlurryAgent.a(android.content.Context, boolean):void
     arg types: [?[OBJECT, ARRAY], int]
     candidates:
      com.flurry.android.FlurryAgent.a(android.content.Context, java.lang.String):void
      com.flurry.android.FlurryAgent.a(com.flurry.android.FlurryAgent, android.content.Context):void
      com.flurry.android.FlurryAgent.a(byte[], java.lang.String):boolean
      com.flurry.android.FlurryAgent.a(android.content.Context, boolean):void */
    /* access modifiers changed from: package-private */
    public final void a(Throwable th) {
        onError("uncaught", th.getMessage(), th.getClass().toString());
        this.s.clear();
        a((Context) null, true);
    }

    private FlurryAgent() {
        HandlerThread handlerThread = new HandlerThread("FlurryAgent");
        handlerThread.start();
        this.n = new Handler(handlerThread.getLooper());
    }

    public static AppCircle getAppCircle() {
        return m;
    }

    static View a(Context context, String str, int i2) {
        if (!l) {
            return null;
        }
        try {
            return f.U.a(context, str, i2);
        } catch (Throwable th) {
            z.b("FlurryAgent", "", th);
            return null;
        }
    }

    static void a(Context context, String str) {
        if (l) {
            f.U.a(context, str);
        }
    }

    static Offer a(String str) {
        if (!l) {
            return null;
        }
        return f.U.a(str);
    }

    static void a(boolean z2) {
        if (l) {
            f.U.a(z2);
        }
    }

    static boolean a() {
        return f.U.g();
    }

    public static void enableAppCircle() {
        l = true;
    }

    public static void setDefaultNoAdsMessage(String str) {
        if (l) {
            n.b = str == null ? "" : str;
        }
    }

    public static void addUserCookie(String str, String str2) {
        if (l) {
            f.U.a(str, str2);
        }
    }

    public static void clearUserCookies() {
        if (l) {
            f.U.j();
        }
    }

    public static void onStartSession(Context context, String str) {
        if (context == null) {
            throw new NullPointerException("Null context");
        } else if (str == null || str.length() == 0) {
            throw new IllegalArgumentException("Api key not specified");
        } else {
            try {
                f.b(context, str);
            } catch (Throwable th) {
                z.b("FlurryAgent", "", th);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.FlurryAgent.a(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.flurry.android.FlurryAgent.a(android.content.Context, java.lang.String):void
      com.flurry.android.FlurryAgent.a(com.flurry.android.FlurryAgent, android.content.Context):void
      com.flurry.android.FlurryAgent.a(byte[], java.lang.String):boolean
      com.flurry.android.FlurryAgent.a(android.content.Context, boolean):void */
    public static void onEndSession(Context context) {
        if (context == null) {
            throw new NullPointerException("Null context");
        }
        try {
            f.a(context, false);
        } catch (Throwable th) {
            z.b("FlurryAgent", "", th);
        }
    }

    public static void onPageView() {
        try {
            f.f();
        } catch (Throwable th) {
            z.b("FlurryAgent", "", th);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.FlurryAgent.a(java.lang.String, java.util.Map, boolean):void
     arg types: [java.lang.String, ?[OBJECT, ARRAY], int]
     candidates:
      com.flurry.android.FlurryAgent.a(android.content.Context, java.lang.String, int):android.view.View
      com.flurry.android.FlurryAgent.a(com.flurry.android.FlurryAgent, android.content.Context, boolean):void
      com.flurry.android.FlurryAgent.a(java.lang.String, java.lang.String, java.lang.String):void
      com.flurry.android.FlurryAgent.a(java.lang.String, java.util.Map, boolean):void */
    public static void logEvent(String str) {
        try {
            f.a(str, (Map) null, false);
        } catch (Throwable th) {
            z.b("FlurryAgent", "Failed to log event: " + str, th);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.FlurryAgent.a(java.lang.String, java.util.Map, boolean):void
     arg types: [java.lang.String, java.util.Map, int]
     candidates:
      com.flurry.android.FlurryAgent.a(android.content.Context, java.lang.String, int):android.view.View
      com.flurry.android.FlurryAgent.a(com.flurry.android.FlurryAgent, android.content.Context, boolean):void
      com.flurry.android.FlurryAgent.a(java.lang.String, java.lang.String, java.lang.String):void
      com.flurry.android.FlurryAgent.a(java.lang.String, java.util.Map, boolean):void */
    public static void logEvent(String str, Map map) {
        try {
            f.a(str, map, false);
        } catch (Throwable th) {
            z.b("FlurryAgent", "Failed to log event: " + str, th);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.FlurryAgent.a(java.lang.String, java.util.Map, boolean):void
     arg types: [java.lang.String, ?[OBJECT, ARRAY], int]
     candidates:
      com.flurry.android.FlurryAgent.a(android.content.Context, java.lang.String, int):android.view.View
      com.flurry.android.FlurryAgent.a(com.flurry.android.FlurryAgent, android.content.Context, boolean):void
      com.flurry.android.FlurryAgent.a(java.lang.String, java.lang.String, java.lang.String):void
      com.flurry.android.FlurryAgent.a(java.lang.String, java.util.Map, boolean):void */
    public static void logEvent(String str, boolean z2) {
        try {
            f.a(str, (Map) null, false);
        } catch (Throwable th) {
            z.b("FlurryAgent", "Failed to log event: " + str, th);
        }
    }

    public static void logEvent(String str, Map map, boolean z2) {
        try {
            f.a(str, map, z2);
        } catch (Throwable th) {
            z.b("FlurryAgent", "Failed to log event: " + str, th);
        }
    }

    public static void endTimedEvent(String str) {
        try {
            f.b(str);
        } catch (Throwable th) {
            z.b("FlurryAgent", "Failed to signify the end of event: " + str, th);
        }
    }

    public static void onError(String str, String str2, String str3) {
        try {
            f.a(str, str2, str3);
        } catch (Throwable th) {
            z.b("FlurryAgent", "", th);
        }
    }

    public static void setReportUrl(String str) {
        a = str;
    }

    public static void setCanvasUrl(String str) {
        b = str;
    }

    public static void setGetAppUrl(String str) {
        d = str;
    }

    public static void setVersionName(String str) {
        synchronized (f) {
            f.v = str;
        }
    }

    public static void setReportLocation(boolean z2) {
        synchronized (f) {
            f.w = z2;
        }
    }

    public static void setLocationCriteria(Criteria criteria) {
        synchronized (f) {
            k = criteria;
        }
    }

    public static void setAge(int i2) {
        if (i2 > 0 && i2 < 110) {
            Date date = new Date(new Date(System.currentTimeMillis() - (((long) i2) * 31449600000L)).getYear(), 1, 1);
            f.L = Long.valueOf(date.getTime());
        }
    }

    public static void setGender(byte b2) {
        switch (b2) {
            case 0:
            case 1:
                f.K = b2;
                return;
            default:
                f.K = -1;
                return;
        }
    }

    public static int getAgentVersion() {
        return 108;
    }

    public static boolean getForbidPlaintextFallback() {
        return false;
    }

    public static void setLogEnabled(boolean z2) {
        synchronized (f) {
            if (z2) {
                z.b();
            } else {
                z.a();
            }
        }
    }

    public static void setContinueSessionMillis(long j2) {
        synchronized (f) {
            g = j2;
        }
    }

    public static void setLogEvents(boolean z2) {
        synchronized (f) {
            h = z2;
        }
    }

    public static void setUserId(String str) {
        synchronized (f) {
            f.J = h.a(str);
        }
    }

    public static void setCaptureUncaughtExceptions(boolean z2) {
        synchronized (f) {
            if (f.p) {
                z.b("FlurryAgent", "Cannot setCaptureUncaughtExceptions after onSessionStart");
            } else {
                j = z2;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.FlurryAgent.a(java.lang.String, java.util.Map, boolean):void
     arg types: [java.lang.String, ?[OBJECT, ARRAY], int]
     candidates:
      com.flurry.android.FlurryAgent.a(android.content.Context, java.lang.String, int):android.view.View
      com.flurry.android.FlurryAgent.a(com.flurry.android.FlurryAgent, android.content.Context, boolean):void
      com.flurry.android.FlurryAgent.a(java.lang.String, java.lang.String, java.lang.String):void
      com.flurry.android.FlurryAgent.a(java.lang.String, java.util.Map, boolean):void */
    public static void onEvent(String str) {
        try {
            f.a(str, (Map) null, false);
        } catch (Throwable th) {
            z.b("FlurryAgent", "", th);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.FlurryAgent.a(java.lang.String, java.util.Map, boolean):void
     arg types: [java.lang.String, java.util.Map, int]
     candidates:
      com.flurry.android.FlurryAgent.a(android.content.Context, java.lang.String, int):android.view.View
      com.flurry.android.FlurryAgent.a(com.flurry.android.FlurryAgent, android.content.Context, boolean):void
      com.flurry.android.FlurryAgent.a(java.lang.String, java.lang.String, java.lang.String):void
      com.flurry.android.FlurryAgent.a(java.lang.String, java.util.Map, boolean):void */
    public static void onEvent(String str, Map map) {
        try {
            f.a(str, map, false);
        } catch (Throwable th) {
            z.b("FlurryAgent", "", th);
        }
    }

    static n b() {
        return f.U;
    }

    /* JADX WARNING: Removed duplicated region for block: B:47:0x018c A[Catch:{ Throwable -> 0x022d }] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x01f6 A[Catch:{ Throwable -> 0x022d }] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0208 A[Catch:{ Throwable -> 0x022d }] */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x026c A[Catch:{ Throwable -> 0x022d }] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:36:0x0172=Splitter:B:36:0x0172, B:38:0x0175=Splitter:B:38:0x0175, B:44:0x0188=Splitter:B:44:0x0188, B:77:0x0229=Splitter:B:77:0x0229} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void b(android.content.Context r10, java.lang.String r11) {
        /*
            r9 = this;
            r8 = 0
            r7 = 1
            monitor-enter(r9)
            java.lang.String r0 = r9.t     // Catch:{ all -> 0x0224 }
            if (r0 == 0) goto L_0x0033
            java.lang.String r0 = r9.t     // Catch:{ all -> 0x0224 }
            boolean r0 = r0.equals(r11)     // Catch:{ all -> 0x0224 }
            if (r0 != 0) goto L_0x0033
            java.lang.String r0 = "FlurryAgent"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0224 }
            r1.<init>()     // Catch:{ all -> 0x0224 }
            java.lang.String r2 = "onStartSession called with different api keys: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0224 }
            java.lang.String r2 = r9.t     // Catch:{ all -> 0x0224 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0224 }
            java.lang.String r2 = " and "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0224 }
            java.lang.StringBuilder r1 = r1.append(r11)     // Catch:{ all -> 0x0224 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0224 }
            com.flurry.android.z.b(r0, r1)     // Catch:{ all -> 0x0224 }
        L_0x0033:
            java.util.Map r0 = r9.s     // Catch:{ all -> 0x0224 }
            java.lang.Object r0 = r0.put(r10, r10)     // Catch:{ all -> 0x0224 }
            android.content.Context r0 = (android.content.Context) r0     // Catch:{ all -> 0x0224 }
            if (r0 == 0) goto L_0x0044
            java.lang.String r0 = "FlurryAgent"
            java.lang.String r1 = "onStartSession called with duplicate context, use a specific Activity or Service as context instead of using a global context"
            com.flurry.android.z.d(r0, r1)     // Catch:{ all -> 0x0224 }
        L_0x0044:
            boolean r0 = r9.p     // Catch:{ all -> 0x0224 }
            if (r0 != 0) goto L_0x0214
            r9.t = r11     // Catch:{ all -> 0x0224 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0224 }
            r0.<init>()     // Catch:{ all -> 0x0224 }
            java.lang.String r1 = ".flurryagent."
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0224 }
            java.lang.String r1 = r9.t     // Catch:{ all -> 0x0224 }
            int r1 = r1.hashCode()     // Catch:{ all -> 0x0224 }
            r2 = 16
            java.lang.String r1 = java.lang.Integer.toString(r1, r2)     // Catch:{ all -> 0x0224 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0224 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0224 }
            java.io.File r0 = r10.getFileStreamPath(r0)     // Catch:{ all -> 0x0224 }
            r9.o = r0     // Catch:{ all -> 0x0224 }
            boolean r0 = com.flurry.android.FlurryAgent.j     // Catch:{ all -> 0x0224 }
            if (r0 == 0) goto L_0x007b
            com.flurry.android.f r0 = new com.flurry.android.f     // Catch:{ all -> 0x0224 }
            r0.<init>()     // Catch:{ all -> 0x0224 }
            java.lang.Thread.setDefaultUncaughtExceptionHandler(r0)     // Catch:{ all -> 0x0224 }
        L_0x007b:
            android.content.Context r0 = r10.getApplicationContext()     // Catch:{ all -> 0x0224 }
            r1 = 1
            r9.p = r1     // Catch:{ all -> 0x0224 }
            java.lang.String r1 = r9.v     // Catch:{ all -> 0x0224 }
            if (r1 != 0) goto L_0x008c
            java.lang.String r1 = a(r0)     // Catch:{ all -> 0x0224 }
            r9.v = r1     // Catch:{ all -> 0x0224 }
        L_0x008c:
            java.lang.String r1 = r0.getPackageName()     // Catch:{ all -> 0x0224 }
            java.lang.String r2 = r9.u     // Catch:{ all -> 0x0224 }
            if (r2 == 0) goto L_0x00c0
            java.lang.String r2 = r9.u     // Catch:{ all -> 0x0224 }
            boolean r2 = r2.equals(r1)     // Catch:{ all -> 0x0224 }
            if (r2 != 0) goto L_0x00c0
            java.lang.String r2 = "FlurryAgent"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0224 }
            r3.<init>()     // Catch:{ all -> 0x0224 }
            java.lang.String r4 = "onStartSession called from different application packages: "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0224 }
            java.lang.String r4 = r9.u     // Catch:{ all -> 0x0224 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0224 }
            java.lang.String r4 = " and "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0224 }
            java.lang.StringBuilder r3 = r3.append(r1)     // Catch:{ all -> 0x0224 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0224 }
            com.flurry.android.z.b(r2, r3)     // Catch:{ all -> 0x0224 }
        L_0x00c0:
            r9.u = r1     // Catch:{ all -> 0x0224 }
            long r1 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x0224 }
            long r3 = r9.r     // Catch:{ all -> 0x0224 }
            long r3 = r1 - r3
            long r5 = com.flurry.android.FlurryAgent.g     // Catch:{ all -> 0x0224 }
            int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r3 <= 0) goto L_0x026f
            java.lang.String r3 = "FlurryAgent"
            java.lang.String r4 = "Starting new session"
            com.flurry.android.z.a(r3, r4)     // Catch:{ all -> 0x0224 }
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0224 }
            r9.D = r3     // Catch:{ all -> 0x0224 }
            r9.E = r1     // Catch:{ all -> 0x0224 }
            r1 = -1
            r9.F = r1     // Catch:{ all -> 0x0224 }
            java.lang.String r1 = ""
            r9.J = r1     // Catch:{ all -> 0x0224 }
            r1 = 0
            r9.M = r1     // Catch:{ all -> 0x0224 }
            r1 = 0
            r9.N = r1     // Catch:{ all -> 0x0224 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0224 }
            r1.<init>()     // Catch:{ all -> 0x0224 }
            java.util.Locale r2 = java.util.Locale.getDefault()     // Catch:{ all -> 0x0224 }
            java.lang.String r2 = r2.getLanguage()     // Catch:{ all -> 0x0224 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0224 }
            java.lang.String r2 = "_"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0224 }
            java.util.Locale r2 = java.util.Locale.getDefault()     // Catch:{ all -> 0x0224 }
            java.lang.String r2 = r2.getCountry()     // Catch:{ all -> 0x0224 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0224 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0224 }
            r9.G = r1     // Catch:{ all -> 0x0224 }
            java.util.HashMap r1 = new java.util.HashMap     // Catch:{ all -> 0x0224 }
            r1.<init>()     // Catch:{ all -> 0x0224 }
            r9.O = r1     // Catch:{ all -> 0x0224 }
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x0224 }
            r1.<init>()     // Catch:{ all -> 0x0224 }
            r9.P = r1     // Catch:{ all -> 0x0224 }
            r1 = 1
            r9.Q = r1     // Catch:{ all -> 0x0224 }
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x0224 }
            r1.<init>()     // Catch:{ all -> 0x0224 }
            r9.S = r1     // Catch:{ all -> 0x0224 }
            r1 = 0
            r9.R = r1     // Catch:{ all -> 0x0224 }
            r1 = 0
            r9.T = r1     // Catch:{ all -> 0x0224 }
            java.lang.String r1 = "FlurryAgent"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0224 }
            r2.<init>()     // Catch:{ all -> 0x0224 }
            java.lang.String r3 = "Got locale: "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0224 }
            java.lang.String r3 = r9.G     // Catch:{ all -> 0x0224 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0224 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0224 }
            com.flurry.android.z.a(r1, r2)     // Catch:{ all -> 0x0224 }
            boolean r1 = r9.q     // Catch:{ all -> 0x0224 }
            if (r1 != 0) goto L_0x01cd
            java.io.File r1 = r9.o     // Catch:{ all -> 0x0224 }
            boolean r1 = r1.exists()     // Catch:{ all -> 0x0224 }
            if (r1 == 0) goto L_0x0188
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ Throwable -> 0x0216, all -> 0x0227 }
            java.io.File r2 = r9.o     // Catch:{ Throwable -> 0x0216, all -> 0x0227 }
            r1.<init>(r2)     // Catch:{ Throwable -> 0x0216, all -> 0x0227 }
            java.io.DataInputStream r2 = new java.io.DataInputStream     // Catch:{ Throwable -> 0x0216, all -> 0x0227 }
            r2.<init>(r1)     // Catch:{ Throwable -> 0x0216, all -> 0x0227 }
            int r1 = r2.readUnsignedShort()     // Catch:{ Throwable -> 0x0286 }
            r3 = 46586(0xb5fa, float:6.5281E-41)
            if (r1 != r3) goto L_0x0172
            r9.b(r2)     // Catch:{ Throwable -> 0x0286 }
        L_0x0172:
            com.flurry.android.h.a(r2)     // Catch:{ all -> 0x0224 }
        L_0x0175:
            boolean r1 = r9.q     // Catch:{ Throwable -> 0x022d }
            if (r1 != 0) goto L_0x0188
            java.io.File r1 = r9.o     // Catch:{ Throwable -> 0x022d }
            boolean r1 = r1.delete()     // Catch:{ Throwable -> 0x022d }
            if (r1 != 0) goto L_0x0188
            java.lang.String r1 = "FlurryAgent"
            java.lang.String r2 = "Cannot delete persistence file"
            com.flurry.android.z.b(r1, r2)     // Catch:{ Throwable -> 0x022d }
        L_0x0188:
            boolean r1 = r9.q     // Catch:{ all -> 0x0224 }
            if (r1 != 0) goto L_0x01cd
            r1 = 0
            r9.A = r1     // Catch:{ all -> 0x0224 }
            long r1 = r9.D     // Catch:{ all -> 0x0224 }
            r9.B = r1     // Catch:{ all -> 0x0224 }
            android.content.ContentResolver r1 = r0.getContentResolver()     // Catch:{ all -> 0x0224 }
            java.lang.String r2 = "android_id"
            java.lang.String r1 = android.provider.Settings.System.getString(r1, r2)     // Catch:{ all -> 0x0224 }
            if (r1 == 0) goto L_0x0237
            int r2 = r1.length()     // Catch:{ all -> 0x0224 }
            if (r2 <= 0) goto L_0x0237
            java.lang.String r2 = "null"
            boolean r2 = r1.equals(r2)     // Catch:{ all -> 0x0224 }
            if (r2 != 0) goto L_0x0237
            java.lang.String r2 = "9774d56d682e549c"
            boolean r2 = r1.equals(r2)     // Catch:{ all -> 0x0224 }
            if (r2 != 0) goto L_0x0237
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0224 }
            r2.<init>()     // Catch:{ all -> 0x0224 }
            java.lang.String r3 = "AND"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0224 }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ all -> 0x0224 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0224 }
        L_0x01c8:
            r9.z = r1     // Catch:{ all -> 0x0224 }
            r1 = 1
            r9.q = r1     // Catch:{ all -> 0x0224 }
        L_0x01cd:
            boolean r1 = r9.w     // Catch:{ all -> 0x0224 }
            android.os.Handler r2 = r9.n     // Catch:{ all -> 0x0224 }
            com.flurry.android.d r3 = new com.flurry.android.d     // Catch:{ all -> 0x0224 }
            r3.<init>(r9, r0, r1)     // Catch:{ all -> 0x0224 }
            r2.post(r3)     // Catch:{ all -> 0x0224 }
            com.flurry.android.a r0 = new com.flurry.android.a     // Catch:{ all -> 0x0224 }
            r0.<init>()     // Catch:{ all -> 0x0224 }
            java.lang.String r1 = r9.t     // Catch:{ all -> 0x0224 }
            r0.a = r1     // Catch:{ all -> 0x0224 }
            java.lang.String r1 = r9.z     // Catch:{ all -> 0x0224 }
            r0.b = r1     // Catch:{ all -> 0x0224 }
            long r1 = r9.B     // Catch:{ all -> 0x0224 }
            r0.c = r1     // Catch:{ all -> 0x0224 }
            long r1 = r9.D     // Catch:{ all -> 0x0224 }
            r0.d = r1     // Catch:{ all -> 0x0224 }
            long r1 = r9.E     // Catch:{ all -> 0x0224 }
            r0.e = r1     // Catch:{ all -> 0x0224 }
            java.lang.String r1 = com.flurry.android.FlurryAgent.b     // Catch:{ all -> 0x0224 }
            if (r1 == 0) goto L_0x026c
            java.lang.String r1 = com.flurry.android.FlurryAgent.b     // Catch:{ all -> 0x0224 }
        L_0x01f8:
            r0.f = r1     // Catch:{ all -> 0x0224 }
            java.lang.String r1 = c()     // Catch:{ all -> 0x0224 }
            r0.g = r1     // Catch:{ all -> 0x0224 }
            android.os.Handler r1 = r9.n     // Catch:{ all -> 0x0224 }
            r0.h = r1     // Catch:{ all -> 0x0224 }
            boolean r1 = com.flurry.android.FlurryAgent.l     // Catch:{ all -> 0x0224 }
            if (r1 == 0) goto L_0x0214
            com.flurry.android.n r1 = new com.flurry.android.n     // Catch:{ all -> 0x0224 }
            r1.<init>(r10, r0)     // Catch:{ all -> 0x0224 }
            r9.U = r1     // Catch:{ all -> 0x0224 }
            com.flurry.android.n r0 = r9.U     // Catch:{ all -> 0x0224 }
            r0.a()     // Catch:{ all -> 0x0224 }
        L_0x0214:
            monitor-exit(r9)
            return
        L_0x0216:
            r1 = move-exception
            r2 = r8
        L_0x0218:
            java.lang.String r3 = "FlurryAgent"
            java.lang.String r4 = ""
            com.flurry.android.z.b(r3, r4, r1)     // Catch:{ all -> 0x0283 }
            com.flurry.android.h.a(r2)     // Catch:{ all -> 0x0224 }
            goto L_0x0175
        L_0x0224:
            r0 = move-exception
            monitor-exit(r9)
            throw r0
        L_0x0227:
            r0 = move-exception
            r1 = r8
        L_0x0229:
            com.flurry.android.h.a(r1)     // Catch:{ all -> 0x0224 }
            throw r0     // Catch:{ all -> 0x0224 }
        L_0x022d:
            r1 = move-exception
            java.lang.String r2 = "FlurryAgent"
            java.lang.String r3 = ""
            com.flurry.android.z.b(r2, r3, r1)     // Catch:{ all -> 0x0224 }
            goto L_0x0188
        L_0x0237:
            double r1 = java.lang.Math.random()     // Catch:{ all -> 0x0224 }
            long r1 = java.lang.Double.doubleToLongBits(r1)     // Catch:{ all -> 0x0224 }
            r3 = 37
            long r5 = java.lang.System.nanoTime()     // Catch:{ all -> 0x0224 }
            java.lang.String r7 = r9.t     // Catch:{ all -> 0x0224 }
            int r7 = r7.hashCode()     // Catch:{ all -> 0x0224 }
            int r7 = r7 * 37
            long r7 = (long) r7     // Catch:{ all -> 0x0224 }
            long r5 = r5 + r7
            long r3 = r3 * r5
            long r1 = r1 + r3
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0224 }
            r3.<init>()     // Catch:{ all -> 0x0224 }
            java.lang.String r4 = "ID"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0224 }
            r4 = 16
            java.lang.String r1 = java.lang.Long.toString(r1, r4)     // Catch:{ all -> 0x0224 }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ all -> 0x0224 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0224 }
            goto L_0x01c8
        L_0x026c:
            java.lang.String r1 = com.flurry.android.FlurryAgent.c     // Catch:{ all -> 0x0224 }
            goto L_0x01f8
        L_0x026f:
            java.lang.String r0 = "FlurryAgent"
            java.lang.String r1 = "Continuing previous session"
            com.flurry.android.z.a(r0, r1)     // Catch:{ all -> 0x0224 }
            java.util.List r0 = r9.C     // Catch:{ all -> 0x0224 }
            java.util.List r1 = r9.C     // Catch:{ all -> 0x0224 }
            int r1 = r1.size()     // Catch:{ all -> 0x0224 }
            int r1 = r1 - r7
            r0.remove(r1)     // Catch:{ all -> 0x0224 }
            goto L_0x0214
        L_0x0283:
            r0 = move-exception
            r1 = r2
            goto L_0x0229
        L_0x0286:
            r1 = move-exception
            goto L_0x0218
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.FlurryAgent.b(android.content.Context, java.lang.String):void");
    }

    private synchronized void a(Context context, boolean z2) {
        if (context != null) {
            if (((Context) this.s.remove(context)) == null) {
                z.d("FlurryAgent", "onEndSession called without context from corresponding onStartSession");
            }
        }
        if (this.p && this.s.isEmpty()) {
            z.a("FlurryAgent", "Ending session");
            h();
            Context applicationContext = context == null ? null : context.getApplicationContext();
            if (context != null) {
                String packageName = applicationContext.getPackageName();
                if (!this.u.equals(packageName)) {
                    z.b("FlurryAgent", "onEndSession called from different application package, expected: " + this.u + " actual: " + packageName);
                }
            }
            this.p = false;
            long elapsedRealtime = SystemClock.elapsedRealtime();
            this.r = elapsedRealtime;
            this.F = elapsedRealtime - this.E;
            e();
            g();
            if (!z2) {
                this.n.postDelayed(new b(this, applicationContext), g);
            }
            if (l) {
                this.U.b();
            }
        }
    }

    private void e() {
        DataOutputStream dataOutputStream;
        IOException e2;
        Throwable th;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            DataOutputStream dataOutputStream2 = new DataOutputStream(byteArrayOutputStream);
            try {
                dataOutputStream2.writeShort(1);
                dataOutputStream2.writeUTF(this.v);
                dataOutputStream2.writeLong(this.D);
                dataOutputStream2.writeLong(this.F);
                dataOutputStream2.writeLong(0);
                dataOutputStream2.writeUTF(this.G);
                dataOutputStream2.writeUTF(this.H);
                dataOutputStream2.writeByte(this.I);
                dataOutputStream2.writeUTF(this.J);
                if (this.N == null) {
                    dataOutputStream2.writeBoolean(false);
                } else {
                    dataOutputStream2.writeBoolean(true);
                    dataOutputStream2.writeDouble(this.N.getLatitude());
                    dataOutputStream2.writeDouble(this.N.getLongitude());
                    dataOutputStream2.writeFloat(this.N.getAccuracy());
                }
                dataOutputStream2.writeInt(this.T);
                dataOutputStream2.writeByte(-1);
                dataOutputStream2.writeByte(-1);
                dataOutputStream2.writeByte(this.K);
                if (this.L == null) {
                    dataOutputStream2.writeBoolean(false);
                } else {
                    dataOutputStream2.writeBoolean(true);
                    dataOutputStream2.writeLong(this.L.longValue());
                }
                dataOutputStream2.writeShort(this.O.size());
                for (Map.Entry entry : this.O.entrySet()) {
                    dataOutputStream2.writeUTF((String) entry.getKey());
                    dataOutputStream2.writeInt(((j) entry.getValue()).a);
                }
                dataOutputStream2.writeShort(this.P.size());
                for (m b2 : this.P) {
                    dataOutputStream2.write(b2.b());
                }
                dataOutputStream2.writeBoolean(this.Q);
                dataOutputStream2.writeInt(this.M);
                dataOutputStream2.writeShort(this.S.size());
                for (u uVar : this.S) {
                    dataOutputStream2.writeLong(uVar.a);
                    dataOutputStream2.writeUTF(uVar.b);
                    dataOutputStream2.writeUTF(uVar.c);
                    dataOutputStream2.writeUTF(uVar.d);
                }
                if (l) {
                    List<y> e3 = this.U.e();
                    dataOutputStream2.writeShort(e3.size());
                    for (y a2 : e3) {
                        a2.a(dataOutputStream2);
                    }
                } else {
                    dataOutputStream2.writeShort(0);
                }
                this.C.add(byteArrayOutputStream.toByteArray());
                h.a(dataOutputStream2);
            } catch (IOException e4) {
                e2 = e4;
                dataOutputStream = dataOutputStream2;
            } catch (Throwable th2) {
                th = th2;
                dataOutputStream = dataOutputStream2;
                h.a(dataOutputStream);
                throw th;
            }
        } catch (IOException e5) {
            IOException iOException = e5;
            dataOutputStream = null;
            e2 = iOException;
            try {
                z.b("FlurryAgent", "", e2);
                h.a(dataOutputStream);
            } catch (Throwable th3) {
                th = th3;
                h.a(dataOutputStream);
                throw th;
            }
        } catch (Throwable th4) {
            Throwable th5 = th4;
            dataOutputStream = null;
            th = th5;
            h.a(dataOutputStream);
            throw th;
        }
    }

    private synchronized void f() {
        this.T++;
    }

    private synchronized void a(String str, Map map, boolean z2) {
        Map map2;
        if (this.P == null) {
            z.b("FlurryAgent", "onEvent called before onStartSession.  Event: " + str);
        } else {
            long elapsedRealtime = SystemClock.elapsedRealtime() - this.E;
            String a2 = h.a(str);
            if (a2.length() != 0) {
                j jVar = (j) this.O.get(a2);
                if (jVar != null) {
                    jVar.a++;
                } else if (this.O.size() < 100) {
                    j jVar2 = new j();
                    jVar2.a = 1;
                    this.O.put(a2, jVar2);
                } else if (z.a("FlurryAgent")) {
                    z.a("FlurryAgent", "MaxEventIds exceeded: " + a2);
                }
                if (!h || this.P.size() >= 100 || this.R >= 8000) {
                    this.Q = false;
                } else {
                    if (map == null) {
                        map2 = Collections.emptyMap();
                    } else {
                        map2 = map;
                    }
                    if (map2.size() <= 10) {
                        m mVar = new m(a2, map2, elapsedRealtime, z2);
                        if (mVar.b().length + this.R <= 8000) {
                            this.P.add(mVar);
                            this.R = mVar.b().length + this.R;
                        } else {
                            this.R = 8000;
                            this.Q = false;
                        }
                    } else if (z.a("FlurryAgent")) {
                        z.a("FlurryAgent", "MaxEventParams exceeded: " + map2.size());
                    }
                }
            }
        }
    }

    private synchronized void b(String str) {
        Iterator it = this.P.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            m mVar = (m) it.next();
            if (mVar.a(str)) {
                mVar.a();
                break;
            }
        }
    }

    private synchronized void a(String str, String str2, String str3) {
        if (this.S == null) {
            z.b("FlurryAgent", "onError called before onStartSession.  Error: " + str);
        } else {
            this.M++;
            if (this.S.size() < 10) {
                u uVar = new u();
                uVar.a = System.currentTimeMillis();
                uVar.b = h.a(str);
                uVar.c = h.a(str2);
                uVar.d = h.a(str3);
                this.S.add(uVar);
            }
        }
    }

    private synchronized byte[] b(boolean z2) {
        DataOutputStream dataOutputStream;
        byte[] bArr;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            DataOutputStream dataOutputStream2 = new DataOutputStream(byteArrayOutputStream);
            try {
                dataOutputStream2.writeShort(15);
                if (!l || !z2) {
                    dataOutputStream2.writeShort(0);
                } else {
                    dataOutputStream2.writeShort(1);
                }
                if (l) {
                    dataOutputStream2.writeLong(this.U.c());
                    Set<Long> d2 = this.U.d();
                    dataOutputStream2.writeShort(d2.size());
                    for (Long longValue : d2) {
                        long longValue2 = longValue.longValue();
                        dataOutputStream2.writeByte(1);
                        dataOutputStream2.writeLong(longValue2);
                    }
                } else {
                    dataOutputStream2.writeLong(0);
                    dataOutputStream2.writeShort(0);
                }
                dataOutputStream2.writeShort(3);
                dataOutputStream2.writeShort(108);
                dataOutputStream2.writeLong(System.currentTimeMillis());
                dataOutputStream2.writeUTF(this.t);
                dataOutputStream2.writeUTF(this.v);
                dataOutputStream2.writeShort(0);
                dataOutputStream2.writeUTF(this.z);
                dataOutputStream2.writeLong(this.B);
                dataOutputStream2.writeLong(this.D);
                dataOutputStream2.writeShort(6);
                dataOutputStream2.writeUTF("device.model");
                dataOutputStream2.writeUTF(Build.MODEL);
                dataOutputStream2.writeUTF("build.brand");
                dataOutputStream2.writeUTF(Build.BRAND);
                dataOutputStream2.writeUTF("build.id");
                dataOutputStream2.writeUTF(Build.ID);
                dataOutputStream2.writeUTF("version.release");
                dataOutputStream2.writeUTF(Build.VERSION.RELEASE);
                dataOutputStream2.writeUTF("build.device");
                dataOutputStream2.writeUTF(Build.DEVICE);
                dataOutputStream2.writeUTF("build.product");
                dataOutputStream2.writeUTF(Build.PRODUCT);
                int size = this.C.size();
                dataOutputStream2.writeShort(size);
                for (int i2 = 0; i2 < size; i2++) {
                    dataOutputStream2.write((byte[]) this.C.get(i2));
                }
                this.x = new ArrayList(this.C);
                dataOutputStream2.close();
                bArr = byteArrayOutputStream.toByteArray();
                h.a(dataOutputStream2);
            } catch (IOException e2) {
                e = e2;
                dataOutputStream = dataOutputStream2;
            } catch (Throwable th) {
                th = th;
                dataOutputStream = dataOutputStream2;
                h.a(dataOutputStream);
                throw th;
            }
        } catch (IOException e3) {
            e = e3;
            dataOutputStream = null;
            try {
                z.b("FlurryAgent", "", e);
                h.a(dataOutputStream);
                bArr = null;
                return bArr;
            } catch (Throwable th2) {
                th = th2;
                h.a(dataOutputStream);
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            dataOutputStream = null;
            h.a(dataOutputStream);
            throw th;
        }
        return bArr;
    }

    static String c() {
        return d != null ? d : e;
    }

    private boolean a(byte[] bArr) {
        boolean z2;
        String str = a != null ? a : kInsecureReportUrl;
        if (str == null) {
            return false;
        }
        try {
            z2 = a(bArr, str);
        } catch (Exception e2) {
            z.a("FlurryAgent", "Sending report exception");
            e2.printStackTrace();
            z2 = false;
        }
        if (z2 || a != null) {
        }
        return z2;
    }

    private boolean a(byte[] bArr, String str) {
        if ("local".equals(str)) {
            return true;
        }
        z.a("FlurryAgent", "Sending report to: " + str);
        boolean z2 = false;
        ByteArrayEntity byteArrayEntity = new ByteArrayEntity(bArr);
        byteArrayEntity.setContentType("application/octet-stream");
        HttpPost httpPost = new HttpPost(str);
        httpPost.setEntity(byteArrayEntity);
        HttpResponse execute = new DefaultHttpClient().execute(httpPost);
        int statusCode = execute.getStatusLine().getStatusCode();
        synchronized (this) {
            if (statusCode == 200) {
                z.a("FlurryAgent", "Report successful");
                this.A = true;
                this.C.removeAll(this.x);
                HttpEntity entity = execute.getEntity();
                z.a("FlurryAgent", "Report response: " + entity);
                if (entity == null || entity.getContentLength() == 0) {
                    z2 = true;
                } else {
                    try {
                        a(new DataInputStream(entity.getContent()));
                        entity.consumeContent();
                        z2 = true;
                    } catch (Throwable th) {
                        entity.consumeContent();
                        throw th;
                    }
                }
            } else {
                z.a("FlurryAgent", "Report failed");
            }
        }
        this.x = null;
        return z2;
    }

    /* JADX WARN: Type inference failed for: r0v6, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(java.io.DataInputStream r13) {
        /*
            r12 = this;
            r11 = 0
            java.util.HashMap r1 = new java.util.HashMap
            r1.<init>()
            java.util.HashMap r2 = new java.util.HashMap
            r2.<init>()
            java.util.HashMap r3 = new java.util.HashMap
            r3.<init>()
            java.util.HashMap r4 = new java.util.HashMap
            r4.<init>()
            java.util.HashMap r5 = new java.util.HashMap
            r5.<init>()
        L_0x001a:
            int r6 = r13.readUnsignedShort()
            int r0 = r13.readInt()
            switch(r6) {
                case 258: goto L_0x004d;
                case 259: goto L_0x0051;
                case 260: goto L_0x0025;
                case 261: goto L_0x0025;
                case 262: goto L_0x0070;
                case 263: goto L_0x0088;
                case 264: goto L_0x0040;
                case 265: goto L_0x0025;
                case 266: goto L_0x00a0;
                case 267: goto L_0x0025;
                case 268: goto L_0x00d6;
                case 269: goto L_0x00f8;
                case 270: goto L_0x009c;
                case 271: goto L_0x00b8;
                case 272: goto L_0x00f3;
                case 273: goto L_0x00fd;
                default: goto L_0x0025;
            }
        L_0x0025:
            java.lang.String r7 = "FlurryAgent"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "Unknown chunkType: "
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.StringBuilder r8 = r8.append(r6)
            java.lang.String r8 = r8.toString()
            com.flurry.android.z.a(r7, r8)
            r13.skipBytes(r0)
        L_0x0040:
            r0 = 264(0x108, float:3.7E-43)
            if (r6 != r0) goto L_0x001a
            boolean r0 = com.flurry.android.FlurryAgent.l
            if (r0 == 0) goto L_0x004c
            r0 = r12
            r0.a(r1, r2, r3, r4, r5)
        L_0x004c:
            return
        L_0x004d:
            r13.readInt()
            goto L_0x0040
        L_0x0051:
            byte r0 = r13.readByte()
            int r7 = r13.readUnsignedShort()
            com.flurry.android.o[] r8 = new com.flurry.android.o[r7]
            r9 = r11
        L_0x005c:
            if (r9 >= r7) goto L_0x0068
            com.flurry.android.o r10 = new com.flurry.android.o
            r10.<init>(r13)
            r8[r9] = r10
            int r9 = r9 + 1
            goto L_0x005c
        L_0x0068:
            java.lang.Byte r0 = java.lang.Byte.valueOf(r0)
            r1.put(r0, r8)
            goto L_0x0040
        L_0x0070:
            int r0 = r13.readUnsignedShort()
            r7 = r11
        L_0x0075:
            if (r7 >= r0) goto L_0x0040
            com.flurry.android.AdImage r8 = new com.flurry.android.AdImage
            r8.<init>(r13)
            long r9 = r8.a
            java.lang.Long r9 = java.lang.Long.valueOf(r9)
            r2.put(r9, r8)
            int r7 = r7 + 1
            goto L_0x0075
        L_0x0088:
            int r0 = r13.readInt()
            r7 = r11
        L_0x008d:
            if (r7 >= r0) goto L_0x0040
            com.flurry.android.e r8 = new com.flurry.android.e
            r8.<init>(r13)
            java.lang.String r9 = r8.a
            r3.put(r9, r8)
            int r7 = r7 + 1
            goto L_0x008d
        L_0x009c:
            r13.skipBytes(r0)
            goto L_0x0040
        L_0x00a0:
            byte r0 = r13.readByte()
            r7 = r11
        L_0x00a5:
            if (r7 >= r0) goto L_0x0040
            com.flurry.android.c r8 = new com.flurry.android.c
            r8.<init>(r13)
            byte r9 = r8.a
            java.lang.Byte r9 = java.lang.Byte.valueOf(r9)
            r4.put(r9, r8)
            int r7 = r7 + 1
            goto L_0x00a5
        L_0x00b8:
            byte r7 = r13.readByte()
            r8 = r11
        L_0x00bd:
            if (r8 >= r7) goto L_0x0040
            byte r0 = r13.readByte()
            java.lang.Byte r0 = java.lang.Byte.valueOf(r0)
            java.lang.Object r0 = r4.get(r0)
            com.flurry.android.c r0 = (com.flurry.android.c) r0
            if (r0 == 0) goto L_0x00d2
            r0.a(r13)
        L_0x00d2:
            int r0 = r8 + 1
            r8 = r0
            goto L_0x00bd
        L_0x00d6:
            int r0 = r13.readInt()
            r7 = r11
        L_0x00db:
            if (r7 >= r0) goto L_0x0040
            long r8 = r13.readLong()
            short r10 = r13.readShort()
            java.lang.Short r10 = java.lang.Short.valueOf(r10)
            java.lang.Long r8 = java.lang.Long.valueOf(r8)
            r5.put(r10, r8)
            int r7 = r7 + 1
            goto L_0x00db
        L_0x00f3:
            r13.skipBytes(r0)
            goto L_0x0040
        L_0x00f8:
            r13.skipBytes(r0)
            goto L_0x0040
        L_0x00fd:
            r13.skipBytes(r0)
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.FlurryAgent.a(java.io.DataInputStream):void");
    }

    private void a(Map map, Map map2, Map map3, Map map4, Map map5) {
        AdImage a2;
        this.U.a(map2);
        for (o[] oVarArr : map.values()) {
            for (o oVar : (o[]) r1.next()) {
                if (!(oVar == null || (a2 = this.U.a(oVar.f.longValue())) == null)) {
                    oVar.h = a2;
                }
            }
        }
        HashMap hashMap = new HashMap();
        for (Map.Entry entry : map3.entrySet()) {
            e eVar = (e) entry.getValue();
            o[] oVarArr2 = (o[]) map.get(Byte.valueOf(eVar.b));
            if (oVarArr2 != null) {
                hashMap.put(entry.getKey(), oVarArr2);
            }
            c cVar = (c) map4.get(Byte.valueOf(eVar.c));
            if (cVar != null) {
                eVar.d = cVar;
            }
        }
        this.U.a(hashMap, map3, map4, map5);
    }

    private void c(boolean z2) {
        try {
            byte[] b2 = b(z2);
            if (b2 != null && a(b2)) {
                z.a("FlurryAgent", "Done sending agent report");
                g();
            }
        } catch (IOException e2) {
            z.a("FlurryAgent", "", e2);
        } catch (Throwable th) {
            z.b("FlurryAgent", "", th);
        }
    }

    private synchronized void b(DataInputStream dataInputStream) {
        int readUnsignedShort = dataInputStream.readUnsignedShort();
        if (readUnsignedShort > 2) {
            throw new IOException("Unknown agent file version: " + readUnsignedShort);
        } else if (readUnsignedShort >= 2) {
            String readUTF = dataInputStream.readUTF();
            if (readUTF.equals(this.t)) {
                this.z = dataInputStream.readUTF();
                this.A = dataInputStream.readBoolean();
                this.B = dataInputStream.readLong();
                while (true) {
                    int readUnsignedShort2 = dataInputStream.readUnsignedShort();
                    if (readUnsignedShort2 == 0) {
                        break;
                    }
                    byte[] bArr = new byte[readUnsignedShort2];
                    dataInputStream.readFully(bArr);
                    this.C.add(0, bArr);
                }
                this.q = true;
            } else {
                z.a("FlurryAgent", "Api keys do not match, old: " + readUTF + ", new: " + this.t);
            }
        }
    }

    private synchronized void g() {
        DataOutputStream dataOutputStream;
        try {
            File parentFile = this.o.getParentFile();
            if (parentFile.mkdirs() || parentFile.exists()) {
                dataOutputStream = new DataOutputStream(new FileOutputStream(this.o));
                try {
                    dataOutputStream.writeShort(46586);
                    dataOutputStream.writeShort(2);
                    dataOutputStream.writeUTF(this.t);
                    dataOutputStream.writeUTF(this.z);
                    dataOutputStream.writeBoolean(this.A);
                    dataOutputStream.writeLong(this.B);
                    for (int size = this.C.size() - 1; size >= 0; size--) {
                        byte[] bArr = (byte[]) this.C.get(size);
                        int length = bArr.length;
                        if (length + 2 + dataOutputStream.size() > 50000) {
                            break;
                        }
                        dataOutputStream.writeShort(length);
                        dataOutputStream.write(bArr);
                    }
                    dataOutputStream.writeShort(0);
                    h.a(dataOutputStream);
                } catch (Throwable th) {
                    th = th;
                    try {
                        z.b("FlurryAgent", "", th);
                        h.a(dataOutputStream);
                    } catch (Throwable th2) {
                        th = th2;
                        h.a(dataOutputStream);
                        throw th;
                    }
                }
            } else {
                z.b("FlurryAgent", "Unable to create persistent dir: " + parentFile);
                h.a((Closeable) null);
            }
        } catch (Throwable th3) {
            th = th3;
            dataOutputStream = null;
            h.a(dataOutputStream);
            throw th;
        }
    }

    private static String a(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            if (packageInfo.versionName != null) {
                return packageInfo.versionName;
            }
            if (packageInfo.versionCode != 0) {
                return Integer.toString(packageInfo.versionCode);
            }
            return "Unknown";
        } catch (Throwable th) {
            z.b("FlurryAgent", "", th);
        }
    }

    private Location b(Context context) {
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0 || context.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
            LocationManager locationManager = (LocationManager) context.getSystemService("location");
            synchronized (this) {
                if (this.y == null) {
                    this.y = locationManager;
                } else {
                    locationManager = this.y;
                }
            }
            Criteria criteria = k;
            if (criteria == null) {
                criteria = new Criteria();
            }
            String bestProvider = locationManager.getBestProvider(criteria, true);
            if (bestProvider != null) {
                locationManager.requestLocationUpdates(bestProvider, 0, 0.0f, this, Looper.getMainLooper());
                return locationManager.getLastKnownLocation(bestProvider);
            }
        }
        return null;
    }

    private synchronized void h() {
        if (this.y != null) {
            this.y.removeUpdates(this);
        }
    }

    public final synchronized void onLocationChanged(Location location) {
        try {
            this.N = location;
            h();
        } catch (Throwable th) {
            z.b("FlurryAgent", "", th);
        }
        return;
    }

    public final void onProviderDisabled(String str) {
    }

    public final void onProviderEnabled(String str) {
    }

    public final void onStatusChanged(String str, int i2, Bundle bundle) {
    }
}
