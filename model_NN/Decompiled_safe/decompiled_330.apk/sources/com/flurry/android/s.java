package com.flurry.android;

import android.content.Context;
import java.io.Closeable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

final class s {
    private Context a;
    private a b;
    private long c;
    private x d = new x(20);
    private Map e = new HashMap();
    private Map f = new HashMap();
    private Map g = new HashMap();
    private Map h = new HashMap();

    public s(Context context, a aVar) {
        this.a = context;
        this.b = aVar;
    }

    /* access modifiers changed from: package-private */
    public final o[] a(String str) {
        o[] oVarArr = (o[]) this.e.get(str);
        if (oVarArr == null) {
            return (o[]) this.e.get("");
        }
        return oVarArr;
    }

    /* access modifiers changed from: package-private */
    public final Set a() {
        return this.d.b();
    }

    /* access modifiers changed from: package-private */
    public final AdImage a(long j) {
        return (AdImage) this.d.a(Long.valueOf(j));
    }

    /* access modifiers changed from: package-private */
    public final AdImage a(short s) {
        Long l = (Long) this.h.get((short) 1);
        if (l == null) {
            return null;
        }
        return (AdImage) this.d.a(l);
    }

    /* access modifiers changed from: package-private */
    public final e b(String str) {
        e eVar = (e) this.f.get(str);
        if (eVar == null) {
            return (e) this.f.get("");
        }
        return eVar;
    }

    /* access modifiers changed from: package-private */
    public final boolean b() {
        return this.e != null && !this.e.isEmpty();
    }

    /* access modifiers changed from: package-private */
    public final void a(Map map) {
        this.c = System.currentTimeMillis();
        for (Map.Entry entry : map.entrySet()) {
            if (entry.getValue() != null) {
                this.d.a(entry.getKey(), entry.getValue());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(Map map, Map map2, Map map3, Map map4) {
        if (map != null) {
            this.e = a(this.e, map, true);
        }
        if (map2 != null) {
            this.f = a(this.f, map2, true);
        }
        if (map3 != null) {
            this.g = a(this.g, map3, true);
        }
        if (this.h != null) {
            this.h = a(this.h, map4, false);
        }
    }

    private static Map a(Map map, Map map2, boolean z) {
        HashMap hashMap = z ? new HashMap() : map;
        for (Map.Entry entry : map2.entrySet()) {
            Object value = entry.getValue();
            if (value == null) {
                value = map.get(entry.getKey());
            }
            hashMap.put(entry.getKey(), value);
        }
        return hashMap;
    }

    /* access modifiers changed from: package-private */
    public final long c() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x002e  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0043 A[Catch:{ all -> 0x0057 }] */
    /* JADX WARNING: Removed duplicated region for block: B:33:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void d() {
        /*
            r7 = this;
            r0 = 0
            android.content.Context r1 = r7.a
            java.lang.String r2 = r7.g()
            java.io.File r1 = r1.getFileStreamPath(r2)
            boolean r2 = r1.exists()
            if (r2 == 0) goto L_0x0031
            r2 = 0
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ Throwable -> 0x0032, all -> 0x004e }
            r3.<init>(r1)     // Catch:{ Throwable -> 0x0032, all -> 0x004e }
            java.io.DataInputStream r4 = new java.io.DataInputStream     // Catch:{ Throwable -> 0x0032, all -> 0x004e }
            r4.<init>(r3)     // Catch:{ Throwable -> 0x0032, all -> 0x004e }
            int r2 = r4.readUnsignedShort()     // Catch:{ Throwable -> 0x005a, all -> 0x0054 }
            r3 = 46587(0xb5fb, float:6.5282E-41)
            if (r2 != r3) goto L_0x0029
            r7.a(r4)     // Catch:{ Throwable -> 0x005a, all -> 0x0054 }
            r0 = 1
        L_0x0029:
            com.flurry.android.h.a(r4)
        L_0x002c:
            if (r0 == 0) goto L_0x0031
            r7.f()
        L_0x0031:
            return
        L_0x0032:
            r3 = move-exception
            r6 = r3
            r3 = r2
            r2 = r6
        L_0x0036:
            java.lang.String r4 = "FlurryAgent"
            java.lang.String r5 = "Discarding cache"
            com.flurry.android.z.b(r4, r5, r2)     // Catch:{ all -> 0x0057 }
            boolean r1 = r1.delete()     // Catch:{ all -> 0x0057 }
            if (r1 != 0) goto L_0x004a
            java.lang.String r1 = "FlurryAgent"
            java.lang.String r2 = "Cannot delete cached ads"
            com.flurry.android.z.b(r1, r2)     // Catch:{ all -> 0x0057 }
        L_0x004a:
            com.flurry.android.h.a(r3)
            goto L_0x002c
        L_0x004e:
            r0 = move-exception
            r1 = r2
        L_0x0050:
            com.flurry.android.h.a(r1)
            throw r0
        L_0x0054:
            r0 = move-exception
            r1 = r4
            goto L_0x0050
        L_0x0057:
            r0 = move-exception
            r1 = r3
            goto L_0x0050
        L_0x005a:
            r2 = move-exception
            r3 = r4
            goto L_0x0036
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.s.d():void");
    }

    private void f() {
        Iterator it = this.g.values().iterator();
        while (it.hasNext()) {
            it.next();
        }
        for (o[] oVarArr : this.e.values()) {
            if (oVarArr != null) {
                for (o oVar : oVarArr) {
                    oVar.h = (AdImage) this.d.a(Long.valueOf(oVar.f.longValue()));
                }
            }
        }
        for (e eVar : this.f.values()) {
            if (eVar.d == null) {
                eVar.d = (c) this.g.get(Byte.valueOf(eVar.c));
            }
            if (eVar.d == null) {
                z.d("FlurryAgent", "No ad theme found for " + ((int) eVar.c));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void e() {
        DataOutputStream dataOutputStream;
        try {
            File fileStreamPath = this.a.getFileStreamPath(g());
            File parentFile = fileStreamPath.getParentFile();
            if (parentFile.mkdirs() || parentFile.exists()) {
                DataOutputStream dataOutputStream2 = new DataOutputStream(new FileOutputStream(fileStreamPath));
                try {
                    dataOutputStream2.writeShort(46587);
                    a(dataOutputStream2);
                    h.a(dataOutputStream2);
                } catch (Throwable th) {
                    Throwable th2 = th;
                    dataOutputStream = dataOutputStream2;
                    th = th2;
                    h.a(dataOutputStream);
                    throw th;
                }
            } else {
                z.b("FlurryAgent", "Unable to create persistent dir: " + parentFile);
                h.a((Closeable) null);
            }
        } catch (Throwable th3) {
            th = th3;
            dataOutputStream = null;
            h.a(dataOutputStream);
            throw th;
        }
    }

    private void a(DataInputStream dataInputStream) {
        if (dataInputStream.readUnsignedShort() == 1) {
            this.c = dataInputStream.readLong();
            int readUnsignedShort = dataInputStream.readUnsignedShort();
            this.d = new x(20);
            for (int i = 0; i < readUnsignedShort; i++) {
                long readLong = dataInputStream.readLong();
                AdImage adImage = new AdImage();
                adImage.load(dataInputStream);
                this.d.a(Long.valueOf(readLong), adImage);
            }
            int readUnsignedShort2 = dataInputStream.readUnsignedShort();
            this.f = new HashMap(readUnsignedShort2);
            for (int i2 = 0; i2 < readUnsignedShort2; i2++) {
                this.f.put(dataInputStream.readUTF(), new e(dataInputStream));
            }
            int readUnsignedShort3 = dataInputStream.readUnsignedShort();
            this.e = new HashMap(readUnsignedShort3);
            for (int i3 = 0; i3 < readUnsignedShort3; i3++) {
                String readUTF = dataInputStream.readUTF();
                int readUnsignedShort4 = dataInputStream.readUnsignedShort();
                o[] oVarArr = new o[readUnsignedShort4];
                for (int i4 = 0; i4 < readUnsignedShort4; i4++) {
                    o oVar = new o();
                    oVar.a(dataInputStream);
                    oVarArr[i4] = oVar;
                }
                this.e.put(readUTF, oVarArr);
            }
            int readUnsignedShort5 = dataInputStream.readUnsignedShort();
            this.g = new HashMap();
            for (int i5 = 0; i5 < readUnsignedShort5; i5++) {
                byte readByte = dataInputStream.readByte();
                c cVar = new c();
                cVar.b(dataInputStream);
                this.g.put(Byte.valueOf(readByte), cVar);
            }
            int readUnsignedShort6 = dataInputStream.readUnsignedShort();
            this.h = new HashMap(readUnsignedShort6);
            for (int i6 = 0; i6 < readUnsignedShort6; i6++) {
                this.h.put(Short.valueOf(dataInputStream.readShort()), Long.valueOf(dataInputStream.readLong()));
            }
        }
    }

    private void a(DataOutputStream dataOutputStream) {
        boolean z;
        dataOutputStream.writeShort(1);
        dataOutputStream.writeLong(this.c);
        Collection<Map.Entry> a2 = this.d.a();
        dataOutputStream.writeShort(a2.size());
        for (Map.Entry entry : a2) {
            dataOutputStream.writeLong(((Long) entry.getKey()).longValue());
            ((AdImage) entry.getValue()).persist(dataOutputStream);
        }
        dataOutputStream.writeShort(this.f.size());
        for (Map.Entry entry2 : this.f.entrySet()) {
            dataOutputStream.writeUTF((String) entry2.getKey());
            e eVar = (e) entry2.getValue();
            dataOutputStream.writeUTF(eVar.a);
            dataOutputStream.writeByte(eVar.b);
            dataOutputStream.writeByte(eVar.c);
        }
        dataOutputStream.writeShort(this.e.size());
        for (Map.Entry entry3 : this.e.entrySet()) {
            dataOutputStream.writeUTF((String) entry3.getKey());
            o[] oVarArr = (o[]) entry3.getValue();
            int length = oVarArr == null ? 0 : oVarArr.length;
            dataOutputStream.writeShort(length);
            for (int i = 0; i < length; i++) {
                o oVar = oVarArr[i];
                dataOutputStream.writeLong(oVar.a);
                dataOutputStream.writeLong(oVar.b);
                dataOutputStream.writeUTF(oVar.d);
                dataOutputStream.writeUTF(oVar.c);
                dataOutputStream.writeLong(oVar.e);
                dataOutputStream.writeLong(oVar.f.longValue());
                dataOutputStream.writeByte(oVar.g.length);
                dataOutputStream.write(oVar.g);
                if (oVar.h != null) {
                    z = true;
                } else {
                    z = false;
                }
                dataOutputStream.writeBoolean(z);
                if (z) {
                    oVar.h.persist(dataOutputStream);
                }
            }
        }
        dataOutputStream.writeShort(this.g.size());
        for (Map.Entry entry4 : this.g.entrySet()) {
            dataOutputStream.writeByte(((Byte) entry4.getKey()).byteValue());
            ((c) entry4.getValue()).a(dataOutputStream);
        }
        dataOutputStream.writeShort(this.h.size());
        for (Map.Entry entry5 : this.h.entrySet()) {
            dataOutputStream.writeShort(((Short) entry5.getKey()).shortValue());
            dataOutputStream.writeLong(((Long) entry5.getValue()).longValue());
        }
    }

    private String g() {
        return ".flurryappcircle." + Integer.toString(this.b.a.hashCode(), 16);
    }
}
