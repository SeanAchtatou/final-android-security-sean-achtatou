package com.flurry.android;

public final class Offer {
    private String a;
    private String b;
    private AdImage c;

    public Offer(String str, String str2, AdImage adImage) {
        this.a = str;
        this.b = str2;
        this.c = adImage;
    }

    public final String getName() {
        return this.a;
    }

    public final String getUrl() {
        return this.b;
    }

    public final AdImage getImage() {
        return this.c;
    }
}
