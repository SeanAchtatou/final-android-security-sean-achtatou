package com.flurry.android;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.SystemClock;
import android.view.View;
import com.google.ads.AdActivity;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

final class n implements View.OnClickListener {
    static String a = "FlurryAgent";
    static String b = "";
    private static volatile String c = "market://";
    private static volatile String d = "market://details?id=";
    private static volatile String e = "market://search?q=pname:";
    private Context f;
    private String g;
    private String h;
    private String i;
    private String j;
    private long k;
    private long l;
    private long m;
    private s n;
    private List o = new ArrayList();
    private Map p = new HashMap();
    private Handler q;
    private boolean r;
    private transient Map s = new HashMap();

    static /* synthetic */ void a(n nVar, Context context, String str) {
        if (str.startsWith(c)) {
            try {
                context.startActivity(new Intent("android.intent.action.VIEW").setData(Uri.parse(str)));
            } catch (Exception e2) {
                z.c(a, "Cannot launch Marketplace url " + str, e2);
            }
        } else {
            z.d(a, "Unexpected android market url scheme: " + str);
        }
    }

    static {
        new Random().nextInt();
    }

    public n(Context context, a aVar) {
        this.f = context;
        this.g = aVar.f;
        this.h = aVar.g;
        this.i = aVar.a;
        this.j = aVar.b;
        this.k = aVar.c;
        this.l = aVar.d;
        this.m = aVar.e;
        this.q = aVar.h;
        context.getResources().getDisplayMetrics();
        this.n = new s(this.f, aVar);
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        if (!this.n.b()) {
            this.n.d();
        }
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        this.n.e();
    }

    /* access modifiers changed from: package-private */
    public final void a(Map map) {
        this.n.a(map);
    }

    /* access modifiers changed from: package-private */
    public final void a(Map map, Map map2, Map map3, Map map4) {
        this.n.a(map, map2, map3, map4);
    }

    /* access modifiers changed from: package-private */
    public final long c() {
        return this.n.c();
    }

    /* access modifiers changed from: package-private */
    public final Set d() {
        return this.n.a();
    }

    /* access modifiers changed from: package-private */
    public final AdImage a(long j2) {
        return this.n.a(j2);
    }

    /* access modifiers changed from: package-private */
    public final List e() {
        return this.o;
    }

    /* access modifiers changed from: package-private */
    public final y b(long j2) {
        return (y) this.s.get(Long.valueOf(j2));
    }

    /* access modifiers changed from: package-private */
    public final void f() {
        this.s.clear();
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(Context context, String str) {
        a(context, str, (Long) null);
    }

    private synchronized void a(Context context, String str, Long l2) {
        try {
            List a2 = a(Arrays.asList(str), (Long) null);
            if (a2 != null && !a2.isEmpty()) {
                y yVar = new y(str, (byte) 2, SystemClock.elapsedRealtime() - this.m);
                yVar.b = (o) a2.get(0);
                a(yVar);
                b(context, yVar, this.g + a(yVar, Long.valueOf(System.currentTimeMillis())));
            }
        } catch (Exception e2) {
            z.d(a, "Failed to launch promotional canvas for hook: " + str, e2);
        }
        return;
    }

    /* access modifiers changed from: package-private */
    public final void a(boolean z) {
        this.r = z;
    }

    /* access modifiers changed from: package-private */
    public final boolean g() {
        return this.r;
    }

    /* access modifiers changed from: package-private */
    public final String h() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(Context context, y yVar, String str) {
        this.q.post(new ab(this, str, context, yVar));
    }

    /* access modifiers changed from: private */
    public static String c(String str) {
        try {
            if (!str.startsWith(c)) {
                HttpResponse execute = new DefaultHttpClient().execute(new HttpGet(str));
                int statusCode = execute.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    return EntityUtils.toString(execute.getEntity());
                }
                z.c(a, "Cannot process with responseCode " + statusCode);
            }
            return str;
        } catch (Exception e2) {
            z.c(a, "Failed on url: " + str, e2);
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized Offer a(String str) {
        Offer offer;
        List a2 = a(Arrays.asList(str), (Long) null);
        if (a2 == null || a2.isEmpty()) {
            offer = null;
        } else {
            o oVar = (o) a2.get(0);
            y yVar = new y(str, (byte) 2, i());
            yVar.b = oVar;
            offer = new Offer(oVar.d, FlurryAgent.c() + a(yVar, Long.valueOf(yVar.a())), oVar.h);
        }
        return offer;
    }

    /* access modifiers changed from: package-private */
    public final synchronized List a(Context context, List list, Long l2, int i2, boolean z) {
        List emptyList;
        if (!this.n.b() || list == null) {
            emptyList = Collections.emptyList();
        } else {
            List a2 = a(list, l2);
            int min = Math.min(list.size(), a2.size());
            ArrayList arrayList = new ArrayList();
            for (int i3 = 0; i3 < min; i3++) {
                String str = (String) list.get(i3);
                e b2 = this.n.b(str);
                if (b2 != null) {
                    y yVar = new y((String) list.get(i3), (byte) 1, i());
                    a(yVar);
                    yVar.b = (o) a2.get(i3);
                    yVar.a(new i((byte) 2, i()));
                    r rVar = new r(context, this, yVar, b2, i2, z);
                    rVar.a(a(yVar, (Long) null));
                    yVar.a(new i((byte) 3, i()));
                    arrayList.add(rVar);
                } else {
                    z.d(a, "Cannot find hook: " + str);
                }
            }
            emptyList = arrayList;
        }
        return emptyList;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v7, resolved type: android.widget.TextView} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v9, resolved type: com.flurry.android.r} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v10, resolved type: android.widget.TextView} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v11, resolved type: android.widget.TextView} */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized android.view.View a(android.content.Context r7, java.lang.String r8, int r9) {
        /*
            r6 = this;
            monitor-enter(r6)
            r0 = 1
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ all -> 0x003e }
            r1 = 0
            r0[r1] = r8     // Catch:{ all -> 0x003e }
            java.util.List r2 = java.util.Arrays.asList(r0)     // Catch:{ all -> 0x003e }
            r3 = 0
            r5 = 0
            r0 = r6
            r1 = r7
            r4 = r9
            java.util.List r0 = r0.a(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x003e }
            boolean r1 = r0.isEmpty()     // Catch:{ all -> 0x003e }
            if (r1 != 0) goto L_0x0026
            r1 = 0
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x003e }
            com.flurry.android.r r0 = (com.flurry.android.r) r0     // Catch:{ all -> 0x003e }
            r0.setOnClickListener(r6)     // Catch:{ all -> 0x003e }
        L_0x0024:
            monitor-exit(r6)
            return r0
        L_0x0026:
            java.lang.String r0 = com.flurry.android.n.a     // Catch:{ all -> 0x003e }
            java.lang.String r1 = "No ads from server"
            com.flurry.android.z.a(r0, r1)     // Catch:{ all -> 0x003e }
            android.widget.TextView r0 = new android.widget.TextView     // Catch:{ all -> 0x003e }
            r0.<init>(r7)     // Catch:{ all -> 0x003e }
            java.lang.String r1 = com.flurry.android.n.b     // Catch:{ all -> 0x003e }
            r0.setText(r1)     // Catch:{ all -> 0x003e }
            r1 = 1
            r2 = 1101004800(0x41a00000, float:20.0)
            r0.setTextSize(r1, r2)     // Catch:{ all -> 0x003e }
            goto L_0x0024
        L_0x003e:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.n.a(android.content.Context, java.lang.String, int):android.view.View");
    }

    private void a(y yVar) {
        if (this.o.size() < 32767) {
            this.o.add(yVar);
            this.s.put(Long.valueOf(yVar.a()), yVar);
        }
    }

    private List a(List list, Long l2) {
        if (!this.n.b() && list != null && !list.isEmpty()) {
            return Collections.emptyList();
        }
        o[] a2 = this.n.a((String) list.get(0));
        if (a2 == null || a2.length <= 0) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList(Arrays.asList(a2));
        if (l2 != null) {
            Iterator it = arrayList.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (((o) it.next()).a == l2.longValue()) {
                        it.remove();
                        break;
                    }
                } else {
                    break;
                }
            }
        }
        Collections.shuffle(arrayList);
        return arrayList.subList(0, Math.min(arrayList.size(), list.size()));
    }

    /* access modifiers changed from: package-private */
    public final long i() {
        return SystemClock.elapsedRealtime() - this.m;
    }

    public final synchronized void onClick(View view) {
        r rVar = (r) view;
        y a2 = rVar.a();
        a2.a(new i((byte) 4, i()));
        if (this.r) {
            b(view.getContext(), a2, rVar.b(this.g));
        } else {
            a(view.getContext(), a2, rVar.b(this.h));
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(String str, String str2) {
        this.p.put(str, str2);
    }

    /* access modifiers changed from: package-private */
    public final synchronized void j() {
        this.p.clear();
    }

    private static void b(Context context, y yVar, String str) {
        Intent intent = new Intent("com.flurry.android.ACTION_CATALOG");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.putExtra(AdActivity.URL_PARAM, str);
        if (yVar != null) {
            intent.putExtra(AdActivity.ORIENTATION_PARAM, yVar.a());
        }
        context.startActivity(intent);
    }

    private String a(y yVar, Long l2) {
        o oVar = yVar.b;
        StringBuilder append = new StringBuilder().append("?apik=").append(this.i).append("&cid=").append(oVar.e).append("&adid=").append(oVar.a).append("&pid=").append(this.j).append("&iid=").append(this.k).append("&sid=").append(this.l).append("&its=").append(yVar.a()).append("&hid=").append(h.b(yVar.a)).append("&ac=").append(a(oVar.g));
        if (this.p != null && !this.p.isEmpty()) {
            for (Map.Entry entry : this.p.entrySet()) {
                append.append("&").append("c_" + h.b((String) entry.getKey())).append("=").append(h.b((String) entry.getValue()));
            }
        }
        append.append("&ats=");
        if (l2 != null) {
            append.append(l2);
        }
        return append.toString();
    }

    private static String a(byte[] bArr) {
        StringBuilder sb = new StringBuilder();
        for (int i2 = 0; i2 < bArr.length; i2++) {
            int i3 = (bArr[i2] >> 4) & 15;
            if (i3 < 10) {
                sb.append((char) (i3 + 48));
            } else {
                sb.append((char) ((i3 + 65) - 10));
            }
            byte b2 = bArr[i2] & 15;
            if (b2 < 10) {
                sb.append((char) (b2 + 48));
            } else {
                sb.append((char) ((b2 + 65) - 10));
            }
        }
        return sb.toString();
    }

    public final String toString() {
        return this.n.toString();
    }

    /* access modifiers changed from: package-private */
    public final AdImage k() {
        return this.n.a((short) 1);
    }
}
