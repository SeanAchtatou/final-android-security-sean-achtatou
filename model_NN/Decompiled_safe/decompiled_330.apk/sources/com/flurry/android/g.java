package com.flurry.android;

import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.mobfox.sdk.Const;

final class g extends WebViewClient {
    private /* synthetic */ CatalogActivity a;

    /* synthetic */ g(CatalogActivity catalogActivity) {
        this(catalogActivity, (byte) 0);
    }

    private g(CatalogActivity catalogActivity, byte b) {
        this.a = catalogActivity;
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (str == null) {
            return false;
        }
        if (this.a.e != null) {
            this.a.e.a(new i((byte) 6, this.a.d.i()));
        }
        this.a.d.a(webView.getContext(), this.a.e, str);
        return true;
    }

    public final void onReceivedError(WebView webView, int i, String str, String str2) {
        z.c("FlurryAgent", "Failed to load url: " + str2);
        webView.loadData("Cannot find Android Market information. <p>Please check your network", "text/html", Const.ENCODING);
    }

    public final void onPageFinished(WebView webView, String str) {
        try {
            y a2 = this.a.e;
            i iVar = new i((byte) 5, this.a.d.i());
            long j = this.a.e.c;
            a2.d.add(iVar);
            a2.c = j;
        } catch (Exception e) {
        }
    }
}
