package com.flurry.android;

import java.lang.Thread;

final class f implements Thread.UncaughtExceptionHandler {
    private Thread.UncaughtExceptionHandler a = Thread.getDefaultUncaughtExceptionHandler();

    f() {
    }

    public final void uncaughtException(Thread thread, Throwable th) {
        try {
            FlurryAgent.f.a(th);
        } catch (Throwable th2) {
            z.b("FlurryAgent", "", th2);
        }
        if (this.a != null) {
            this.a.uncaughtException(thread, th);
        }
    }
}
