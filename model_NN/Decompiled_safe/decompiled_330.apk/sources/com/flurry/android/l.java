package com.flurry.android;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.widget.ImageView;
import android.widget.LinearLayout;

final class l extends LinearLayout {
    public l(CatalogActivity catalogActivity, Context context) {
        super(context);
        setBackgroundColor(-1);
        AdImage k = catalogActivity.d.k();
        if (k != null) {
            ImageView imageView = new ImageView(context);
            imageView.setId(10000);
            byte[] bArr = k.d;
            imageView.setImageBitmap(BitmapFactory.decodeByteArray(bArr, 0, bArr.length));
            h.a(context, imageView, h.a(context, k.b), h.a(context, k.c));
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
            layoutParams.setMargins(0, 0, 0, -3);
            setGravity(3);
            addView(imageView, layoutParams);
        }
    }
}
