package com.zemariamm.appirater;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class AppiraterBase extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkAppirater();
    }

    /* access modifiers changed from: protected */
    public boolean shoulAppiraterdRun() {
        return true;
    }

    public void processNever() {
        Log.d("Hooligans Appirater", "Never");
    }

    public void processRate() {
        Log.d("Hooligans Appirater", "Rate");
    }

    public void processRemindMe() {
        Log.d("Hooligans Appirater", "Remind Me");
    }

    /* access modifiers changed from: protected */
    public void checkAppirater() {
        if (AppirateUtils.shouldAppirater(this) && shoulAppiraterdRun()) {
            AppirateUtils.appiraterDialog(this, this);
        }
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        checkAppirater();
    }
}
