package com.adwhirl.adapters;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;
import com.adwhirl.AdWhirlLayout;
import com.adwhirl.AdWhirlTargeting;
import com.adwhirl.obj.Ration;
import com.adwhirl.util.AdWhirlUtil;
import com.millennialmedia.android.MMAdView;
import com.millennialmedia.android.MMAdViewSDK;
import java.util.Hashtable;

public class MillennialAdapter extends AdWhirlAdapter implements MMAdView.MMAdListener {
    public MillennialAdapter(AdWhirlLayout adWhirlLayout, Ration ration) {
        super(adWhirlLayout, ration);
    }

    public void handle() {
        String keywords;
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) this.adWhirlLayoutReference.get();
        if (adWhirlLayout != null) {
            Hashtable<String, String> map = new Hashtable<>();
            AdWhirlTargeting.Gender gender = AdWhirlTargeting.getGender();
            if (gender == AdWhirlTargeting.Gender.MALE) {
                map.put(MMAdView.KEY_GENDER, "male");
            } else if (gender == AdWhirlTargeting.Gender.FEMALE) {
                map.put(MMAdView.KEY_GENDER, "female");
            }
            int age = AdWhirlTargeting.getAge();
            if (age != -1) {
                map.put(MMAdView.KEY_AGE, String.valueOf(age));
            }
            String postalCode = AdWhirlTargeting.getPostalCode();
            if (!TextUtils.isEmpty(postalCode)) {
                map.put(MMAdView.KEY_ZIP_CODE, postalCode);
            }
            if (AdWhirlTargeting.getKeywordSet() != null) {
                keywords = TextUtils.join(",", AdWhirlTargeting.getKeywordSet());
            } else {
                keywords = AdWhirlTargeting.getKeywords();
            }
            if (!TextUtils.isEmpty(keywords)) {
                map.put(MMAdView.KEY_KEYWORDS, keywords);
            }
            map.put("vendor", "adwhirl");
            MMAdView adView = new MMAdView((Activity) adWhirlLayout.getContext(), this.ration.key, MMAdView.BANNER_AD_TOP, -1, map);
            adView.setId(MMAdViewSDK.DEFAULT_VIEWID);
            adView.setListener(this);
            adView.callForAd();
            if (adWhirlLayout.extra.locationOn == 1 && adWhirlLayout.adWhirlManager.location != null) {
                adView.updateUserLocation(adWhirlLayout.adWhirlManager.location);
            }
            adView.setHorizontalScrollBarEnabled(false);
            adView.setVerticalScrollBarEnabled(false);
        }
    }

    public void MMAdReturned(MMAdView adView) {
        Log.d(AdWhirlUtil.ADWHIRL, "Millennial success");
        adView.setListener(null);
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) this.adWhirlLayoutReference.get();
        if (adWhirlLayout != null) {
            adWhirlLayout.adWhirlManager.resetRollover();
            adWhirlLayout.handler.post(new AdWhirlLayout.ViewAdRunnable(adWhirlLayout, adView));
            adWhirlLayout.rotateThreadedDelayed();
        }
    }

    public void MMAdFailed(MMAdView adView) {
        Log.d(AdWhirlUtil.ADWHIRL, "Millennial failure");
        adView.setListener(null);
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) this.adWhirlLayoutReference.get();
        if (adWhirlLayout != null) {
            adWhirlLayout.rollover();
        }
    }

    public void MMAdClickedToNewBrowser(MMAdView adview) {
        Log.d(AdWhirlUtil.ADWHIRL, "Millennial Ad clicked, new browser launched");
    }

    public void MMAdClickedToOverlay(MMAdView adview) {
        Log.d(AdWhirlUtil.ADWHIRL, "Millennial Ad Clicked to overlay");
    }

    public void MMAdOverlayLaunched(MMAdView adview) {
        Log.d(AdWhirlUtil.ADWHIRL, "Millennial Ad Overlay Launched");
    }

    public void MMAdRequestIsCaching(MMAdView adView) {
    }
}
