package org.ksoap2.servlet;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Hashtable;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.kxml2.io.KXmlParser;
import org.kxml2.io.KXmlSerializer;

public class SoapServlet extends HttpServlet {
    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER12);
    Hashtable instanceMap = new Hashtable();

    /* access modifiers changed from: protected */
    public Object getInstance(HttpServletRequest request) {
        if (request.getPathInfo() == null) {
            return this;
        }
        Object result = this.instanceMap.get(request.getPathInfo());
        return result != null ? result : this;
    }

    public void publishClass(Class service, String namespace) {
        Method[] methods = service.getMethods();
        for (int i = 0; i < methods.length; i++) {
            if (Modifier.isPublic(methods[i].getModifiers())) {
                Class[] types = methods[i].getParameterTypes();
                PropertyInfo[] info = new PropertyInfo[types.length];
                for (int j = 0; j < types.length; j++) {
                    info[j] = new PropertyInfo();
                    info[j].type = types[j];
                }
                publishMethod(service, namespace, methods[i].getName(), info);
            }
        }
    }

    public void publishInstance(String path, Object instance) {
        this.instanceMap.put(path, instance);
    }

    public void publishMethod(Class service, String namespace, String name, PropertyInfo[] parameters) {
        SoapObject template = new SoapObject(namespace, name);
        for (PropertyInfo addProperty : parameters) {
            template.addProperty(addProperty, (Object) null);
        }
        this.envelope.addTemplate(template);
    }

    public void publishMethod(Class service, String namespace, String name, String[] parameterNames) {
        Method[] methods = service.getMethods();
        int i = 0;
        while (i < methods.length) {
            if (!methods[i].getName().equals(name) || methods[i].getParameterTypes().length != parameterNames.length) {
                i++;
            } else {
                Class[] types = methods[i].getParameterTypes();
                PropertyInfo[] info = new PropertyInfo[types.length];
                for (int j = 0; j < types.length; j++) {
                    info[j] = new PropertyInfo();
                    info[j].name = parameterNames[j];
                    info[j].type = types[j];
                }
                publishMethod(service, namespace, name, info);
                return;
            }
        }
        throw new RuntimeException("Method not found!");
    }

    public SoapSerializationEnvelope getEnvelope() {
        return this.envelope;
    }

    public void setEnvelope(SoapSerializationEnvelope envelope2) {
        this.envelope = envelope2;
    }

    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        try {
            Object service = getInstance(req);
            KXmlParser kXmlParser = new KXmlParser();
            byte[] inputRequest = new byte[req.getInputStream().available()];
            req.getInputStream().read(inputRequest);
            System.out.println("Request: " + new String(inputRequest));
            kXmlParser.setInput(new ByteArrayInputStream(inputRequest), null);
            kXmlParser.setFeature("http://xmlpull.org/v1/doc/features.html#process-namespaces", true);
            this.envelope.parse(kXmlParser);
            SoapObject result = invoke(service, (SoapObject) this.envelope.bodyIn);
            System.out.println("result: " + result);
            this.envelope.bodyOut = result;
            res.setContentType("text/xml; charset=utf-8");
            res.setHeader("Connection", "close");
            StringWriter sw = new StringWriter();
            KXmlSerializer kXmlSerializer = new KXmlSerializer();
            kXmlSerializer.setOutput(sw);
            try {
                this.envelope.write(kXmlSerializer);
            } catch (Exception e) {
                e.printStackTrace();
            }
            kXmlSerializer.flush();
            System.out.println("result xml: " + sw);
            PrintWriter writer = res.getWriter();
            writer.write(sw.toString());
            writer.close();
        } catch (SoapFault e2) {
            SoapFault f = e2;
            f.printStackTrace();
            this.envelope.bodyOut = f;
            res.setStatus(500);
            res.setContentType("text/xml; charset=utf-8");
            res.setHeader("Connection", "close");
            StringWriter sw2 = new StringWriter();
            KXmlSerializer kXmlSerializer2 = new KXmlSerializer();
            kXmlSerializer2.setOutput(sw2);
            try {
                this.envelope.write(kXmlSerializer2);
            } catch (Exception e3) {
                e3.printStackTrace();
            }
            kXmlSerializer2.flush();
            System.out.println("result xml: " + sw2);
            PrintWriter writer2 = res.getWriter();
            writer2.write(sw2.toString());
            writer2.close();
        } catch (Throwable th) {
            res.setContentType("text/xml; charset=utf-8");
            res.setHeader("Connection", "close");
            StringWriter sw3 = new StringWriter();
            KXmlSerializer kXmlSerializer3 = new KXmlSerializer();
            kXmlSerializer3.setOutput(sw3);
            try {
                this.envelope.write(kXmlSerializer3);
            } catch (Exception e4) {
                e4.printStackTrace();
            }
            kXmlSerializer3.flush();
            System.out.println("result xml: " + sw3);
            PrintWriter writer3 = res.getWriter();
            writer3.write(sw3.toString());
            writer3.close();
            throw th;
        }
        res.flushBuffer();
    }

    /* access modifiers changed from: protected */
    public SoapObject invoke(Object service, SoapObject soapReq) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        String name = soapReq.getName();
        Class[] types = new Class[soapReq.getPropertyCount()];
        Object[] args = new Object[soapReq.getPropertyCount()];
        PropertyInfo arg = new PropertyInfo();
        Hashtable properties = new Hashtable();
        for (int i = 0; i < types.length; i++) {
            soapReq.getPropertyInfo(i, properties, arg);
            types[i] = (Class) arg.type;
            args[i] = soapReq.getProperty(i);
        }
        Object result = service.getClass().getMethod(name, types).invoke(service, args);
        System.out.println("result:" + result);
        SoapObject response = new SoapObject(soapReq.getNamespace(), String.valueOf(name) + "Response");
        if (result != null) {
            response.addProperty("return", result);
        }
        return response;
    }
}
