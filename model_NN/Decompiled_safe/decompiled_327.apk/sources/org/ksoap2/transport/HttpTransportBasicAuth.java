package org.ksoap2.transport;

import java.io.IOException;
import org.kobjects.base64.Base64;

public class HttpTransportBasicAuth extends HttpTransport {
    private String password;
    private String username;

    public HttpTransportBasicAuth(String url, String username2, String password2) {
        super(url);
        this.username = username2;
        this.password = password2;
    }

    /* access modifiers changed from: protected */
    public ServiceConnection getServiceConnection() throws IOException {
        ServiceConnectionMidp midpConnection = new ServiceConnectionMidp(this.url);
        addBasicAuthentication(midpConnection);
        return midpConnection;
    }

    /* access modifiers changed from: protected */
    public void addBasicAuthentication(ServiceConnection midpConnection) throws IOException {
        if (this.username != null && this.password != null) {
            StringBuffer buf = new StringBuffer(this.username);
            buf.append(':').append(this.password);
            byte[] raw = buf.toString().getBytes();
            buf.setLength(0);
            buf.append("Basic ");
            Base64.encode(raw, 0, raw.length, buf);
            midpConnection.setRequestProperty("Authorization", buf.toString());
        }
    }
}
