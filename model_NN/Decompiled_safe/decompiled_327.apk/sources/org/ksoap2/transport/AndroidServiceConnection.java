package org.ksoap2.transport;

import java.io.IOException;

public class AndroidServiceConnection extends ServiceConnectionSE {
    public AndroidServiceConnection(String url) throws IOException {
        super(url);
    }

    public AndroidServiceConnection(String url, int timeout) throws IOException {
        super(url, timeout);
    }
}
