package org.ksoap2.transport;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;

public class ServiceConnectionMidp implements ServiceConnection {
    private HttpConnection connection;

    public ServiceConnectionMidp(String url) throws IOException {
        this.connection = Connector.open(url, 3, true);
    }

    public void disconnect() throws IOException {
        this.connection.close();
    }

    public void setRequestProperty(String string, String soapAction) throws IOException {
        this.connection.setRequestProperty(string, soapAction);
    }

    public void setRequestMethod(String post) throws IOException {
        this.connection.setRequestMethod(post);
    }

    public OutputStream openOutputStream() throws IOException {
        return this.connection.openOutputStream();
    }

    public InputStream openInputStream() throws IOException {
        return this.connection.openInputStream();
    }

    public void connect() throws IOException {
        throw new RuntimeException("ServiceConnectionMidp.connect is not available.");
    }

    public InputStream getErrorStream() {
        throw new RuntimeException("ServiceConnectionMidp.getErrorStream is not available.");
    }
}
