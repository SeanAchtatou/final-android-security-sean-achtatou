package org.ksoap2.transport;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.xmlpull.v1.XmlPullParserException;

public class HttpTransport extends Transport {
    private boolean connected = false;
    ServiceConnection connection;
    InputStream is;
    OutputStream os;

    public HttpTransport(String url) {
        super(url);
    }

    /* Debug info: failed to restart local var, previous not found, register: 9 */
    public void call(String soapAction, SoapEnvelope envelope) throws IOException, XmlPullParserException {
        boolean z;
        if (soapAction == null) {
            soapAction = "\"\"";
        }
        byte[] requestData = createRequestData(envelope);
        this.requestDump = this.debug ? new String(requestData) : null;
        this.responseDump = null;
        try {
            this.connected = true;
            this.connection = getServiceConnection();
            this.connection.setRequestProperty("SOAPAction", soapAction);
            this.connection.setRequestProperty("Content-Type", "text/xml");
            this.connection.setRequestProperty("Content-Length", new StringBuilder().append(requestData.length).toString());
            this.connection.setRequestProperty("User-Agent", "kSOAP/2.0");
            this.connection.setRequestMethod("POST");
            this.os = this.connection.openOutputStream();
            this.os.write(requestData, 0, requestData.length);
            this.os.close();
            byte[] requestData2 = null;
            this.is = this.connection.openInputStream();
            if (this.debug) {
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                byte[] buf = new byte[256];
                while (true) {
                    int rd = this.is.read(buf, 0, 256);
                    if (rd == -1) {
                        break;
                    }
                    bos.write(buf, 0, rd);
                }
                bos.flush();
                byte[] buf2 = bos.toByteArray();
                this.responseDump = new String(buf2);
                this.is.close();
                this.is = new ByteArrayInputStream(buf2);
            }
            parseResponse(envelope, this.is);
            if (!z) {
                throw new InterruptedIOException();
            }
            reset();
            if (envelope.bodyIn instanceof SoapFault) {
                throw ((SoapFault) envelope.bodyIn);
            }
        } finally {
            if (!this.connected) {
                throw new InterruptedIOException();
            }
            reset();
        }
    }

    public void reset() {
        this.connected = false;
        if (this.is != null) {
            try {
                this.is.close();
            } catch (Throwable th) {
            }
            this.is = null;
        }
        if (this.connection != null) {
            try {
                this.connection.disconnect();
            } catch (Throwable th2) {
            }
            this.connection = null;
        }
    }

    /* access modifiers changed from: protected */
    public ServiceConnection getServiceConnection() throws IOException {
        return new ServiceConnectionMidp(this.url);
    }
}
