package org.ksoap2.transport;

import java.io.IOException;

public class AndroidHttpTransport extends HttpTransportSE {
    public AndroidHttpTransport(String url) {
        super(url);
    }

    public AndroidHttpTransport(String url, int timeout) {
        super(url, timeout);
    }

    /* access modifiers changed from: protected */
    public ServiceConnection getServiceConnection() throws IOException {
        return new AndroidServiceConnection(this.url, this.timeout);
    }
}
