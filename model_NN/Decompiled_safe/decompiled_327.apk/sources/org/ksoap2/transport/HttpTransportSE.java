package org.ksoap2.transport;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.SocketTimeoutException;
import org.ksoap2.SoapEnvelope;
import org.xmlpull.v1.XmlPullParserException;

public class HttpTransportSE extends Transport {
    ServiceConnection connection;

    public HttpTransportSE(String url) {
        super(url);
    }

    public HttpTransportSE(String url, int timeout) {
        super(url, timeout);
    }

    public void call(String soapAction, SoapEnvelope envelope) throws IOException, XmlPullParserException, SocketTimeoutException {
        String str;
        InputStream is;
        if (soapAction == null) {
            soapAction = "\"\"";
        }
        byte[] requestData = createRequestData(envelope);
        if (this.debug) {
            str = new String(requestData);
        } else {
            str = null;
        }
        this.requestDump = str;
        this.responseDump = null;
        this.connection = getServiceConnection();
        try {
            this.connection.setRequestProperty("User-Agent", "kSOAP/2.0");
            this.connection.setRequestProperty("SOAPAction", soapAction);
            this.connection.setRequestProperty("Content-Type", "text/xml");
            this.connection.setRequestProperty("Connection", "close");
            this.connection.setRequestProperty("Content-Length", new StringBuilder().append(requestData.length).toString());
            this.connection.setRequestMethod("POST");
            this.connection.connect();
            OutputStream os = this.connection.openOutputStream();
            os.write(requestData, 0, requestData.length);
            os.flush();
            os.close();
            byte[] requestData2 = null;
            this.connection.connect();
            is = this.connection.openInputStream();
        } catch (IOException e) {
            IOException e2 = e;
            is = this.connection.getErrorStream();
            if (is == null) {
                this.connection.disconnect();
                throw e2;
            }
        } catch (Exception e3) {
            throw new SocketTimeoutException();
        }
        if (this.debug) {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] buf = new byte[256];
            while (true) {
                int rd = is.read(buf, 0, 256);
                if (rd == -1) {
                    break;
                }
                bos.write(buf, 0, rd);
            }
            bos.flush();
            byte[] buf2 = bos.toByteArray();
            this.responseDump = new String(buf2);
            is.close();
            is = new ByteArrayInputStream(buf2);
        }
        parseResponse(envelope, is);
    }

    /* access modifiers changed from: protected */
    public ServiceConnection getServiceConnection() throws IOException {
        return new ServiceConnectionSE(this.url, this.timeout);
    }
}
