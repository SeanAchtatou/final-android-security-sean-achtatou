package org.ksoap2.transport;

import java.io.IOException;

public class HttpsTransportSE extends HttpTransportSE {
    static final String PROTOCOL = "https";
    private HttpsServiceConnectionSE conn = null;
    private final String file;
    private final String host;
    private final int port;
    private final int timeout;

    public HttpsTransportSE(String host2, int port2, String file2, int timeout2) {
        super("https://" + host2 + ":" + port2 + file2);
        this.host = host2;
        this.port = port2;
        this.file = file2;
        this.timeout = timeout2;
    }

    public HttpsServiceConnectionSE getConnection() {
        return this.conn;
    }

    /* access modifiers changed from: protected */
    public ServiceConnection getServiceConnection() throws IOException {
        this.conn = new HttpsServiceConnectionSE(this.host, this.port, this.file, this.timeout);
        return this.conn;
    }
}
