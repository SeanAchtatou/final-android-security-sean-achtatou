package org.ksoap2.transport;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import org.ksoap2.SoapEnvelope;
import org.kxml2.io.KXmlParser;
import org.kxml2.io.KXmlSerializer;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

public abstract class Transport {
    public boolean debug;
    public String requestDump;
    public String responseDump;
    protected int timeout = 20000;
    protected String url;
    private String xmlVersionTag = "";

    public abstract void call(String str, SoapEnvelope soapEnvelope) throws IOException, XmlPullParserException;

    public Transport() {
    }

    public Transport(String url2) {
        this.url = url2;
    }

    public Transport(String url2, int timeout2) {
        this.url = url2;
        this.timeout = timeout2;
    }

    /* access modifiers changed from: protected */
    public void parseResponse(SoapEnvelope envelope, InputStream is) throws XmlPullParserException, IOException {
        XmlPullParser xp = new KXmlParser();
        xp.setFeature("http://xmlpull.org/v1/doc/features.html#process-namespaces", true);
        xp.setInput(is, null);
        envelope.parse(xp);
    }

    /* access modifiers changed from: protected */
    public byte[] createRequestData(SoapEnvelope envelope) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bos.write(this.xmlVersionTag.getBytes());
        XmlSerializer xw = new KXmlSerializer();
        xw.setOutput(bos, null);
        envelope.write(xw);
        xw.flush();
        bos.write(13);
        bos.write(10);
        bos.flush();
        return bos.toByteArray();
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public void setTimeout(int timeout2) {
        this.timeout = timeout2;
    }

    public void setXmlVersionTag(String tag) {
        this.xmlVersionTag = tag;
    }

    public void reset() {
    }
}
