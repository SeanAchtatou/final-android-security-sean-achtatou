package org.ksoap2.serialization;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Vector;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

public class SoapSerializationEnvelope extends SoapEnvelope {
    private static final String ANY_TYPE_LABEL = "anyType";
    private static final String ARRAY_MAPPING_NAME = "Array";
    private static final String ARRAY_TYPE_LABEL = "arrayType";
    static final Marshal DEFAULT_MARSHAL = new DM();
    private static final String HREF_LABEL = "href";
    private static final String ID_LABEL = "id";
    private static final String ITEM_LABEL = "item";
    private static final String NIL_LABEL = "nil";
    private static final String NULL_LABEL = "null";
    protected static final int QNAME_MARSHAL = 3;
    protected static final int QNAME_NAMESPACE = 0;
    protected static final int QNAME_TYPE = 1;
    private static final String ROOT_LABEL = "root";
    private static final String TYPE_LABEL = "type";
    protected boolean addAdornments = true;
    protected Hashtable classToQName = new Hashtable();
    public boolean dotNet;
    Hashtable idMap = new Hashtable();
    public boolean implicitTypes;
    Vector multiRef;
    public Hashtable properties = new Hashtable();
    protected Hashtable qNameToClass = new Hashtable();

    public SoapSerializationEnvelope(int version) {
        super(version);
        addMapping(this.enc, ARRAY_MAPPING_NAME, PropertyInfo.VECTOR_CLASS);
        DEFAULT_MARSHAL.register(this);
    }

    public boolean isAddAdornments() {
        return this.addAdornments;
    }

    public void setAddAdornments(boolean addAdornments2) {
        this.addAdornments = addAdornments2;
    }

    public void parseBody(XmlPullParser parser) throws IOException, XmlPullParserException {
        this.bodyIn = null;
        parser.nextTag();
        if (parser.getEventType() != 2 || !parser.getNamespace().equals(this.env) || !parser.getName().equals("Fault")) {
            while (parser.getEventType() == 2) {
                String rootAttr = parser.getAttributeValue(this.enc, ROOT_LABEL);
                Object o = read(parser, null, -1, parser.getNamespace(), parser.getName(), PropertyInfo.OBJECT_TYPE);
                if ("1".equals(rootAttr) || this.bodyIn == null) {
                    this.bodyIn = o;
                }
                parser.nextTag();
            }
            return;
        }
        SoapFault fault = new SoapFault();
        fault.parse(parser);
        this.bodyIn = fault;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.ksoap2.serialization.SoapSerializationEnvelope.readSerializable(org.xmlpull.v1.XmlPullParser, org.ksoap2.serialization.KvmSerializable):void
     arg types: [org.xmlpull.v1.XmlPullParser, org.ksoap2.serialization.SoapObject]
     candidates:
      org.ksoap2.serialization.SoapSerializationEnvelope.readSerializable(org.xmlpull.v1.XmlPullParser, org.ksoap2.serialization.SoapObject):void
      org.ksoap2.serialization.SoapSerializationEnvelope.readSerializable(org.xmlpull.v1.XmlPullParser, org.ksoap2.serialization.KvmSerializable):void */
    /* access modifiers changed from: protected */
    public void readSerializable(XmlPullParser parser, SoapObject obj) throws IOException, XmlPullParserException {
        for (int counter = 0; counter < parser.getAttributeCount(); counter++) {
            obj.addAttribute(parser.getAttributeName(counter), parser.getAttributeValue(counter));
        }
        readSerializable(parser, (KvmSerializable) obj);
    }

    /* access modifiers changed from: protected */
    public void readSerializable(XmlPullParser parser, KvmSerializable obj) throws IOException, XmlPullParserException {
        int testIndex = -1;
        int propertyCount = obj.getPropertyCount();
        PropertyInfo info = new PropertyInfo();
        while (parser.nextTag() != 3) {
            String name = parser.getName();
            int countdown = propertyCount;
            if (!this.implicitTypes || !(obj instanceof SoapObject)) {
                while (true) {
                    int countdown2 = countdown;
                    countdown = countdown2 - 1;
                    if (countdown2 == 0) {
                        throw new RuntimeException("Unknown Property: " + name);
                    }
                    testIndex++;
                    if (testIndex >= propertyCount) {
                        testIndex = 0;
                    }
                    obj.getPropertyInfo(testIndex, this.properties, info);
                    if ((info.namespace != null || !name.equals(info.name)) && (!(info.name == null && testIndex == 0) && (!name.equals(info.name) || !parser.getNamespace().equals(info.namespace)))) {
                    }
                }
                obj.setProperty(testIndex, read(parser, obj, testIndex, null, null, info));
            } else {
                ((SoapObject) obj).addProperty(parser.getName(), read(parser, obj, obj.getPropertyCount(), ((SoapObject) obj).getNamespace(), name, PropertyInfo.OBJECT_TYPE));
            }
        }
        parser.require(3, null, null);
    }

    /* access modifiers changed from: protected */
    public Object readUnknown(XmlPullParser parser, String typeNamespace, String typeName) throws IOException, XmlPullParserException {
        SoapObject soapObject;
        String name = parser.getName();
        String namespace = parser.getNamespace();
        Vector attributeInfoVector = new Vector();
        for (int attributeCount = 0; attributeCount < parser.getAttributeCount(); attributeCount++) {
            AttributeInfo attributeInfo = new AttributeInfo();
            attributeInfo.setName(parser.getAttributeName(attributeCount));
            attributeInfo.setValue(parser.getAttributeValue(attributeCount));
            attributeInfo.setNamespace(parser.getAttributeNamespace(attributeCount));
            attributeInfo.setType(parser.getAttributeType(attributeCount));
            attributeInfoVector.addElement(attributeInfo);
        }
        parser.next();
        SoapObject result = null;
        String text = null;
        if (parser.getEventType() == 4) {
            text = parser.getText();
            SoapPrimitive soapPrimitive = new SoapPrimitive(typeNamespace, typeName, text);
            result = soapPrimitive;
            for (int i = 0; i < attributeInfoVector.size(); i++) {
                soapPrimitive.addAttribute((AttributeInfo) attributeInfoVector.elementAt(i));
            }
            parser.next();
        } else if (parser.getEventType() == 3) {
            SoapObject soapObject2 = new SoapObject(typeNamespace, typeName);
            for (int i2 = 0; i2 < attributeInfoVector.size(); i2++) {
                soapObject2.addAttribute((AttributeInfo) attributeInfoVector.elementAt(i2));
            }
            result = soapObject2;
        }
        if (parser.getEventType() != 2) {
            soapObject = result;
        } else if (text == null || text.trim().length() == 0) {
            SoapObject result2 = new SoapObject(typeNamespace, typeName);
            for (int i3 = 0; i3 < attributeInfoVector.size(); i3++) {
                result2.addAttribute((AttributeInfo) attributeInfoVector.elementAt(i3));
            }
            while (parser.getEventType() != 3) {
                result2.addProperty(parser.getName(), read(parser, result2, result2.getPropertyCount(), null, null, PropertyInfo.OBJECT_TYPE));
                parser.nextTag();
            }
            soapObject = result2;
        } else {
            throw new RuntimeException("Malformed input: Mixed content");
        }
        parser.require(3, namespace, name);
        return soapObject;
    }

    private int getIndex(String value, int start, int dflt) {
        if (value == null) {
            return dflt;
        }
        return value.length() - start < 3 ? dflt : Integer.parseInt(value.substring(start + 1, value.length() - 1));
    }

    /* access modifiers changed from: protected */
    public void readVector(XmlPullParser parser, Vector v, PropertyInfo elementType) throws IOException, XmlPullParserException {
        String prefix;
        String namespace = null;
        String name = null;
        int size = v.size();
        boolean dynamic = true;
        String type = parser.getAttributeValue(this.enc, ARRAY_TYPE_LABEL);
        if (type != null) {
            int cut0 = type.indexOf(58);
            int cut1 = type.indexOf("[", cut0);
            name = type.substring(cut0 + 1, cut1);
            if (cut0 == -1) {
                prefix = "";
            } else {
                prefix = type.substring(0, cut0);
            }
            namespace = parser.getNamespace(prefix);
            size = getIndex(type, cut1, -1);
            if (size != -1) {
                v.setSize(size);
                dynamic = false;
            }
        }
        if (elementType == null) {
            elementType = PropertyInfo.OBJECT_TYPE;
        }
        parser.nextTag();
        int position = getIndex(parser.getAttributeValue(this.enc, "offset"), 0, 0);
        while (parser.getEventType() != 3) {
            int position2 = getIndex(parser.getAttributeValue(this.enc, "position"), 0, position);
            if (dynamic && position2 >= size) {
                size = position2 + 1;
                v.setSize(size);
            }
            v.setElementAt(read(parser, v, position2, namespace, name, elementType), position2);
            position = position2 + 1;
            parser.nextTag();
        }
        parser.require(3, null, null);
    }

    public Object read(XmlPullParser parser, Object owner, int index, String namespace, String name, PropertyInfo expected) throws IOException, XmlPullParserException {
        Object obj;
        Object obj2;
        String elementName = parser.getName();
        String href = parser.getAttributeValue(null, HREF_LABEL);
        if (href == null) {
            String nullAttr = parser.getAttributeValue(this.xsi, NIL_LABEL);
            String id = parser.getAttributeValue(null, ID_LABEL);
            if (nullAttr == null) {
                nullAttr = parser.getAttributeValue(this.xsi, NULL_LABEL);
            }
            if (nullAttr == null || !SoapEnvelope.stringToBoolean(nullAttr)) {
                String type = parser.getAttributeValue(this.xsi, TYPE_LABEL);
                if (type != null) {
                    int cut = type.indexOf(58);
                    name = type.substring(cut + 1);
                    namespace = parser.getNamespace(cut == -1 ? "" : type.substring(0, cut));
                } else if (name == null && namespace == null) {
                    if (parser.getAttributeValue(this.enc, ARRAY_TYPE_LABEL) != null) {
                        namespace = this.enc;
                        name = ARRAY_MAPPING_NAME;
                    } else {
                        Object[] names = getInfo(expected.type, null);
                        namespace = (String) names[0];
                        name = (String) names[1];
                    }
                }
                if (type == null) {
                    this.implicitTypes = true;
                }
                obj = readInstance(parser, namespace, name, expected);
                if (obj == null) {
                    obj = readUnknown(parser, namespace, name);
                }
            } else {
                obj = null;
                parser.nextTag();
                parser.require(3, null, elementName);
            }
            if (id != null) {
                Object hlp = this.idMap.get(id);
                if (hlp instanceof FwdRef) {
                    FwdRef f = (FwdRef) hlp;
                    do {
                        if (f.obj instanceof KvmSerializable) {
                            ((KvmSerializable) f.obj).setProperty(f.index, obj);
                        } else {
                            ((Vector) f.obj).setElementAt(obj, f.index);
                        }
                        f = f.next;
                    } while (f != null);
                } else if (hlp != null) {
                    throw new RuntimeException("double ID");
                }
                this.idMap.put(id, obj);
            }
            obj2 = obj;
        } else if (owner == null) {
            throw new RuntimeException("href at root level?!?");
        } else {
            String href2 = href.substring(1);
            Object obj3 = this.idMap.get(href2);
            if (obj3 == null || (obj3 instanceof FwdRef)) {
                FwdRef f2 = new FwdRef();
                f2.next = (FwdRef) obj3;
                f2.obj = owner;
                f2.index = index;
                this.idMap.put(href2, f2);
                obj3 = null;
            }
            parser.nextTag();
            parser.require(3, null, elementName);
            obj2 = obj3;
        }
        parser.require(3, null, elementName);
        return obj2;
    }

    public Object readInstance(XmlPullParser parser, String namespace, String name, PropertyInfo expected) throws IOException, XmlPullParserException {
        Object obj;
        Object obj2 = this.qNameToClass.get(new SoapPrimitive(namespace, name, null));
        if (obj2 == null) {
            return null;
        }
        if (obj2 instanceof Marshal) {
            return ((Marshal) obj2).readInstance(parser, namespace, name, expected);
        }
        if (obj2 instanceof SoapObject) {
            obj = ((SoapObject) obj2).newInstance();
        } else if (obj2 == SoapObject.class) {
            obj = new SoapObject(namespace, name);
        } else {
            try {
                obj = ((Class) obj2).newInstance();
            } catch (Exception e) {
                throw new RuntimeException(e.toString());
            }
        }
        if (obj instanceof SoapObject) {
            readSerializable(parser, (SoapObject) obj);
        } else if (obj instanceof KvmSerializable) {
            readSerializable(parser, (KvmSerializable) obj);
        } else if (obj instanceof Vector) {
            readVector(parser, (Vector) obj, expected.elementType);
        } else {
            throw new RuntimeException("no deserializer for " + obj.getClass());
        }
        return obj;
    }

    /* JADX INFO: Multiple debug info for r10v1 'type'  java.lang.Object: [D('type' java.lang.Class), D('type' java.lang.Object)] */
    public Object[] getInfo(Object type, Object instance) {
        Object[] tmp;
        if (type == null) {
            if ((instance instanceof SoapObject) || (instance instanceof SoapPrimitive)) {
                type = instance;
            } else {
                type = instance.getClass();
            }
        }
        if (type instanceof SoapObject) {
            SoapObject so = (SoapObject) type;
            Object[] objArr = new Object[4];
            objArr[0] = so.getNamespace();
            objArr[1] = so.getName();
            return objArr;
        } else if (type instanceof SoapPrimitive) {
            SoapPrimitive sp = (SoapPrimitive) type;
            Object[] objArr2 = new Object[4];
            objArr2[0] = sp.getNamespace();
            objArr2[1] = sp.getName();
            objArr2[3] = DEFAULT_MARSHAL;
            return objArr2;
        } else if ((type instanceof Class) && type != PropertyInfo.OBJECT_CLASS && (tmp = (Object[]) this.classToQName.get(((Class) type).getName())) != null) {
            return tmp;
        } else {
            Object[] objArr3 = new Object[4];
            objArr3[0] = this.xsd;
            objArr3[1] = ANY_TYPE_LABEL;
            return objArr3;
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v1, resolved type: java.lang.Class} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v2, resolved type: java.lang.Class} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v5, resolved type: java.lang.Class} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void addMapping(java.lang.String r5, java.lang.String r6, java.lang.Class r7, org.ksoap2.serialization.Marshal r8) {
        /*
            r4 = this;
            java.util.Hashtable r0 = r4.qNameToClass
            org.ksoap2.serialization.SoapPrimitive r1 = new org.ksoap2.serialization.SoapPrimitive
            r2 = 0
            r1.<init>(r5, r6, r2)
            if (r8 != 0) goto L_0x0024
            r2 = r7
        L_0x000b:
            r0.put(r1, r2)
            java.util.Hashtable r0 = r4.classToQName
            java.lang.String r1 = r7.getName()
            r2 = 4
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r3 = 0
            r2[r3] = r5
            r3 = 1
            r2[r3] = r6
            r3 = 3
            r2[r3] = r8
            r0.put(r1, r2)
            return
        L_0x0024:
            r2 = r8
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: org.ksoap2.serialization.SoapSerializationEnvelope.addMapping(java.lang.String, java.lang.String, java.lang.Class, org.ksoap2.serialization.Marshal):void");
    }

    public void addMapping(String namespace, String name, Class clazz) {
        addMapping(namespace, name, clazz, null);
    }

    public void addTemplate(SoapObject so) {
        this.qNameToClass.put(new SoapPrimitive(so.namespace, so.name, null), so);
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public Object getResponse() throws SoapFault {
        if (this.bodyIn instanceof SoapFault) {
            throw ((SoapFault) this.bodyIn);
        }
        KvmSerializable ks = (KvmSerializable) this.bodyIn;
        if (ks.getPropertyCount() == 0) {
            return null;
        }
        return ks.getProperty(0);
    }

    public Object getResult() {
        KvmSerializable ks = (KvmSerializable) this.bodyIn;
        if (ks.getPropertyCount() == 0) {
            return null;
        }
        return ks.getProperty(0);
    }

    /* Debug info: failed to restart local var, previous not found, register: 7 */
    public void writeBody(XmlSerializer writer) throws IOException {
        this.multiRef = new Vector();
        this.multiRef.addElement(this.bodyOut);
        Object[] qName = getInfo(null, this.bodyOut);
        writer.startTag(this.dotNet ? "" : (String) qName[0], (String) qName[1]);
        if (this.dotNet) {
            writer.attribute(null, "xmlns", (String) qName[0]);
        }
        if (this.addAdornments) {
            writer.attribute(null, ID_LABEL, qName[2] == null ? "o0" : (String) qName[2]);
            writer.attribute(this.enc, ROOT_LABEL, "1");
        }
        writeElement(writer, this.bodyOut, null, qName[3]);
        writer.endTag(this.dotNet ? "" : (String) qName[0], (String) qName[1]);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.ksoap2.serialization.SoapSerializationEnvelope.writeObjectBody(org.xmlpull.v1.XmlSerializer, org.ksoap2.serialization.KvmSerializable):void
     arg types: [org.xmlpull.v1.XmlSerializer, org.ksoap2.serialization.SoapObject]
     candidates:
      org.ksoap2.serialization.SoapSerializationEnvelope.writeObjectBody(org.xmlpull.v1.XmlSerializer, org.ksoap2.serialization.SoapObject):void
      org.ksoap2.serialization.SoapSerializationEnvelope.writeObjectBody(org.xmlpull.v1.XmlSerializer, org.ksoap2.serialization.KvmSerializable):void */
    public void writeObjectBody(XmlSerializer writer, SoapObject obj) throws IOException {
        SoapObject soapObject = obj;
        for (int counter = 0; counter < soapObject.getAttributeCount(); counter++) {
            AttributeInfo attributeInfo = new AttributeInfo();
            soapObject.getAttributeInfo(counter, attributeInfo);
            writer.attribute(attributeInfo.getNamespace(), attributeInfo.getName(), attributeInfo.getValue().toString());
        }
        writeObjectBody(writer, (KvmSerializable) obj);
    }

    public void writeObjectBody(XmlSerializer writer, KvmSerializable obj) throws IOException {
        PropertyInfo info = new PropertyInfo();
        int cnt = obj.getPropertyCount();
        for (int i = 0; i < cnt; i++) {
            obj.getPropertyInfo(i, this.properties, info);
            if ((info.flags & 1) == 0) {
                writer.startTag(info.namespace, info.name);
                writeProperty(writer, obj.getProperty(i), info);
                writer.endTag(info.namespace, info.name);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void writeProperty(XmlSerializer writer, Object obj, PropertyInfo type) throws IOException {
        String str;
        if (obj == null) {
            String str2 = this.xsi;
            if (this.version >= 120) {
                str = NIL_LABEL;
            } else {
                str = NULL_LABEL;
            }
            writer.attribute(str2, str, "true");
            return;
        }
        Object[] qName = getInfo(null, obj);
        if (type.multiRef || qName[2] != null) {
            int i = this.multiRef.indexOf(obj);
            if (i == -1) {
                i = this.multiRef.size();
                this.multiRef.addElement(obj);
            }
            writer.attribute(null, HREF_LABEL, qName[2] == null ? "#o" + i : "#" + qName[2]);
            return;
        }
        if (!this.implicitTypes || obj.getClass() != type.type) {
            writer.attribute(this.xsi, TYPE_LABEL, String.valueOf(writer.getPrefix((String) qName[0], true)) + ":" + qName[1]);
        }
        writeElement(writer, obj, type, qName[3]);
    }

    private void writeElement(XmlSerializer writer, Object element, PropertyInfo type, Object marshal) throws IOException {
        if (marshal != null) {
            ((Marshal) marshal).writeInstance(writer, element);
        } else if (element instanceof SoapObject) {
            writeObjectBody(writer, (SoapObject) element);
        } else if (element instanceof KvmSerializable) {
            writeObjectBody(writer, (KvmSerializable) element);
        } else if (element instanceof Vector) {
            writeVectorBody(writer, (Vector) element, type.elementType);
        } else {
            throw new RuntimeException("Cannot serialize: " + element);
        }
    }

    /* access modifiers changed from: protected */
    public void writeVectorBody(XmlSerializer writer, Vector vector, PropertyInfo elementType) throws IOException {
        String itemsTagName = ITEM_LABEL;
        String itemsNamespace = null;
        if (elementType == null) {
            elementType = PropertyInfo.OBJECT_TYPE;
        } else if ((elementType instanceof PropertyInfo) && elementType.name != null) {
            itemsTagName = elementType.name;
            itemsNamespace = elementType.namespace;
        }
        int cnt = vector.size();
        Object[] arrType = getInfo(elementType.type, null);
        writer.attribute(this.enc, ARRAY_TYPE_LABEL, String.valueOf(writer.getPrefix((String) arrType[0], false)) + ":" + arrType[1] + "[" + cnt + "]");
        boolean skipped = false;
        for (int i = 0; i < cnt; i++) {
            if (vector.elementAt(i) == null) {
                skipped = true;
            } else {
                writer.startTag(itemsNamespace, itemsTagName);
                if (skipped) {
                    writer.attribute(this.enc, "position", "[" + i + "]");
                    skipped = false;
                }
                writeProperty(writer, vector.elementAt(i), elementType);
                writer.endTag(itemsNamespace, itemsTagName);
            }
        }
    }
}
