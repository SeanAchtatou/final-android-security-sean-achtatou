package org.ksoap2.serialization;

import java.util.Hashtable;
import java.util.Vector;

public class SoapObject extends AttributeContainer implements KvmSerializable {
    protected String name;
    protected String namespace;
    protected Vector properties = new Vector();

    public SoapObject(String namespace2, String name2) {
        this.namespace = namespace2;
        this.name = name2;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof SoapObject)) {
            return false;
        }
        SoapObject otherSoapObject = (SoapObject) obj;
        if (!this.name.equals(otherSoapObject.name) || !this.namespace.equals(otherSoapObject.namespace)) {
            return false;
        }
        int numProperties = this.properties.size();
        if (numProperties != otherSoapObject.properties.size()) {
            return false;
        }
        for (int propIndex = 0; propIndex < numProperties; propIndex++) {
            PropertyInfo thisProp = (PropertyInfo) this.properties.elementAt(propIndex);
            Object thisPropValue = thisProp.getValue();
            if (!otherSoapObject.hasProperty(thisProp.getName())) {
                return false;
            }
            if (!thisPropValue.equals(otherSoapObject.getProperty(thisProp.getName()))) {
                return false;
            }
        }
        return attributesAreEqual(otherSoapObject);
    }

    public String getName() {
        return this.name;
    }

    public String getNamespace() {
        return this.namespace;
    }

    public Object getProperty(int index) {
        return ((PropertyInfo) this.properties.elementAt(index)).getValue();
    }

    public Object getProperty(String name2) {
        Integer index = propertyIndex(name2);
        if (index != null) {
            return getProperty(index.intValue());
        }
        throw new RuntimeException("illegal property: " + name2);
    }

    public boolean hasProperty(String name2) {
        if (propertyIndex(name2) != null) {
            return true;
        }
        return false;
    }

    public Object safeGetProperty(String name2) {
        Integer i = propertyIndex(name2);
        if (i != null) {
            return getProperty(i.intValue());
        }
        return new NullSoapObject();
    }

    public Object safeGetProperty(String name2, Object defaultThing) {
        Integer i = propertyIndex(name2);
        if (i != null) {
            return getProperty(i.intValue());
        }
        return defaultThing;
    }

    private Integer propertyIndex(String name2) {
        for (int i = 0; i < this.properties.size(); i++) {
            if (name2.equals(((PropertyInfo) this.properties.elementAt(i)).getName())) {
                return new Integer(i);
            }
        }
        return null;
    }

    public int getPropertyCount() {
        return this.properties.size();
    }

    public void getPropertyInfo(int index, Hashtable properties2, PropertyInfo propertyInfo) {
        getPropertyInfo(index, propertyInfo);
    }

    public void getPropertyInfo(int index, PropertyInfo propertyInfo) {
        PropertyInfo p = (PropertyInfo) this.properties.elementAt(index);
        propertyInfo.name = p.name;
        propertyInfo.namespace = p.namespace;
        propertyInfo.flags = p.flags;
        propertyInfo.type = p.type;
        propertyInfo.elementType = p.elementType;
    }

    public SoapObject newInstance() {
        SoapObject o = new SoapObject(this.namespace, this.name);
        for (int propIndex = 0; propIndex < this.properties.size(); propIndex++) {
            o.addProperty((PropertyInfo) this.properties.elementAt(propIndex));
        }
        for (int attribIndex = 0; attribIndex < getAttributeCount(); attribIndex++) {
            AttributeInfo newAI = new AttributeInfo();
            getAttributeInfo(attribIndex, newAI);
            o.addAttribute(newAI);
        }
        return o;
    }

    public void setProperty(int index, Object value) {
        ((PropertyInfo) this.properties.elementAt(index)).setValue(value);
    }

    public SoapObject addProperty(String name2, Object value) {
        PropertyInfo propertyInfo = new PropertyInfo();
        propertyInfo.name = name2;
        propertyInfo.type = value == null ? PropertyInfo.OBJECT_CLASS : value.getClass();
        propertyInfo.value = value;
        return addProperty(propertyInfo);
    }

    public SoapObject addProperty(PropertyInfo propertyInfo, Object value) {
        propertyInfo.setValue(value);
        addProperty(propertyInfo);
        return this;
    }

    public SoapObject addProperty(PropertyInfo propertyInfo) {
        this.properties.addElement(propertyInfo);
        return this;
    }

    public String toString() {
        StringBuffer buf = new StringBuffer(this.name + "{");
        for (int i = 0; i < getPropertyCount(); i++) {
            buf.append(((PropertyInfo) this.properties.elementAt(i)).getName() + "=" + getProperty(i) + "; ");
        }
        buf.append("}");
        return buf.toString();
    }
}
