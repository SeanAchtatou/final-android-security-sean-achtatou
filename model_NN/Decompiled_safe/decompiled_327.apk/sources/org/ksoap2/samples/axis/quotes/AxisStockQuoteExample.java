package org.ksoap2.samples.axis.quotes;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalFloat;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

public class AxisStockQuoteExample {
    public AxisStockQuoteExample() {
        SoapObject requestObject = new SoapObject("x", "getQuote");
        requestObject.addProperty("symbol", "XXX");
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.setOutputSoapObject(requestObject);
        new MarshalFloat().register(envelope);
        HttpTransportSE transportSE = new HttpTransportSE("http://localhost:8080/axis/StockQuoteService.jws");
        transportSE.debug = true;
        try {
            transportSE.call("getQuote", envelope);
            System.out.println(envelope.getResponse());
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(transportSE.responseDump);
        }
    }

    public static void main(String[] args) {
        new AxisStockQuoteExample();
    }
}
