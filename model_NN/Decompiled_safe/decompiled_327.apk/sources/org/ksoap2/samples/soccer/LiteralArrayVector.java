package org.ksoap2.samples.soccer;

import java.util.Hashtable;
import java.util.Vector;
import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapSerializationEnvelope;

public abstract class LiteralArrayVector extends Vector implements KvmSerializable {
    /* access modifiers changed from: protected */
    public abstract Class getElementClass();

    public void register(SoapSerializationEnvelope envelope, String namespace, String name) {
        envelope.addMapping(namespace, name, getClass());
        registerElementClass(envelope, namespace);
    }

    private void registerElementClass(SoapSerializationEnvelope envelope, String namespace) {
        Class elementClass = getElementClass();
        try {
            if (elementClass.newInstance() instanceof KvmSerializable) {
                envelope.addMapping(namespace, "", elementClass);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getPropertyInfo(int index, Hashtable properties, PropertyInfo info) {
        info.name = getItemDescriptor();
        info.type = getElementClass();
    }

    public Object getProperty(int index) {
        return this;
    }

    public int getPropertyCount() {
        return 1;
    }

    public void setProperty(int index, Object value) {
        addElement(value);
    }

    /* access modifiers changed from: protected */
    public String getItemDescriptor() {
        return "item";
    }
}
