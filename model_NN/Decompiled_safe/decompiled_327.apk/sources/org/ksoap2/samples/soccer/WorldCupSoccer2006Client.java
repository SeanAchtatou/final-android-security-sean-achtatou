package org.ksoap2.samples.soccer;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

public class WorldCupSoccer2006Client {
    private static final String METHOD_NAME = "StadiumNames";
    private static final String NAMESPACE = "http://www.dataaccess.nl/wk2006";
    private static final String SOAP_ACTION = "";
    private static final String URL = "http://www.dataaccess.nl/wk2006/footballpoolwebservice.wso";

    WorldCupSoccer2006Client() {
        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.setOutputSoapObject(request);
        addClassMappings(envelope);
        try {
            new HttpTransportSE(URL).call(SOAP_ACTION, envelope);
            StadiumNamesResult response = (StadiumNamesResult) envelope.getResponse();
            for (int i = 0; i < response.size(); i++) {
                System.out.println(response.elementAt(i));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addClassMappings(SoapSerializationEnvelope envelope) {
        createWrappingResultTemplate(envelope);
        new StadiumNamesResult().register(envelope, NAMESPACE, "StadiumNamesResult");
    }

    private void createWrappingResultTemplate(SoapSerializationEnvelope envelope) {
        PropertyInfo info = new PropertyInfo();
        info.name = "StadiumNamesResult";
        info.type = new StadiumNamesResult().getClass();
        SoapObject template = new SoapObject(NAMESPACE, "StadiumNamesResponse");
        template.addProperty(info, "not important what this is");
        envelope.addTemplate(template);
    }

    public static void main(String[] args) {
        new WorldCupSoccer2006Client();
    }
}
