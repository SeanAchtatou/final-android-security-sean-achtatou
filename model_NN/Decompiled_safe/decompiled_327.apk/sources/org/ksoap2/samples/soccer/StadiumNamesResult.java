package org.ksoap2.samples.soccer;

import org.ksoap2.serialization.PropertyInfo;

public class StadiumNamesResult extends LiteralArrayVector {
    /* access modifiers changed from: protected */
    public String getItemDescriptor() {
        return "string";
    }

    /* access modifiers changed from: protected */
    public Class getElementClass() {
        return PropertyInfo.STRING_CLASS;
    }
}
