package org.ksoap2.samples.amazon.search.messages;

import java.util.Hashtable;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;

public class ItemSearchResponse extends BaseObject {
    private BookItems bookItems;
    private String operationRequest;

    public Object getProperty(int index) {
        if (index == 0) {
            return this.bookItems;
        }
        return this.operationRequest;
    }

    public int getPropertyCount() {
        return 2;
    }

    public void getPropertyInfo(int index, Hashtable properties, PropertyInfo info) {
        switch (index) {
            case 0:
                info.name = "Items";
                info.type = new BookItems().getClass();
                return;
            case 1:
                info.name = "OperationRequest";
                info.type = new SoapObject("http://webservices.amazon.com/AWSECommerceService/2006-05-17", "OperationRequest").getClass();
                return;
            default:
                return;
        }
    }

    public void setProperty(int index, Object value) {
        if (index == 0) {
            this.bookItems = (BookItems) value;
        } else {
            this.operationRequest = value.toString();
        }
    }

    public void register(SoapSerializationEnvelope envelope) {
        envelope.addMapping("http://webservices.amazon.com/AWSECommerceService/2006-05-17", "ItemSearchResponse", getClass());
        new BookItems().register(envelope);
        new BookAttributes().register(envelope);
    }
}
