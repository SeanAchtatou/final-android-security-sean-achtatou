package org.ksoap2.samples.amazon.search.messages;

import java.util.Hashtable;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;

public class BookItems extends LiteralArrayVector {
    private String request;

    /* access modifiers changed from: protected */
    public String getItemDescriptor() {
        return "Item";
    }

    public Object getProperty(int index) {
        throw new RuntimeException("BookItems.getProperty is not implemented yet");
    }

    public int getPropertyCount() {
        return 4;
    }

    public void getPropertyInfo(int index, Hashtable properties, PropertyInfo info) {
        info.type = new SoapObject("http://webservices.amazon.com/AWSECommerceService/2006-05-17", "").getClass();
        switch (index) {
            case 0:
                info.name = "Request";
                return;
            case 1:
                info.name = "TotalResults";
                return;
            case 2:
                info.name = "TotalPages";
                return;
            case 3:
                super.getPropertyInfo(index, properties, info);
                return;
            default:
                return;
        }
    }

    public void setProperty(int index, Object value) {
        switch (index) {
            case 0:
                this.request = value.toString();
                return;
            case 1:
            case 2:
            default:
                return;
            case 3:
                super.setProperty(index, value);
                return;
        }
    }

    /* access modifiers changed from: protected */
    public Class getElementClass() {
        return new Book().getClass();
    }

    public void register(SoapSerializationEnvelope envelope) {
        super.register(envelope, "http://webservices.amazon.com/AWSECommerceService/2006-05-17", "Items");
    }

    public synchronized String toString() {
        StringBuffer buffer;
        buffer = new StringBuffer();
        buffer.append("Request: ");
        buffer.append(this.request);
        buffer.append("\n");
        for (int i = 0; i < size(); i++) {
            buffer.append("\n=== BOOK ===\n");
            buffer.append(elementAt(i).toString());
        }
        return buffer.toString();
    }
}
