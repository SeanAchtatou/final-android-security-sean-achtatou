package org.ksoap2.samples.amazon.search.messages;

import java.util.Hashtable;
import org.ksoap2.serialization.PropertyInfo;

public class Book extends BaseObject {
    private String asin;
    private String detailPageUrl;
    private BookAttributes itemAttributes;

    public Object getProperty(int index) {
        throw new RuntimeException("Book.getProperty is not implemented yet");
    }

    public int getPropertyCount() {
        return 3;
    }

    public void getPropertyInfo(int index, Hashtable properties, PropertyInfo info) {
        info.type = PropertyInfo.STRING_CLASS;
        switch (index) {
            case 0:
                info.name = "ASIN";
                return;
            case 1:
                info.name = "DetailPageURL";
                return;
            case 2:
                info.name = "ItemAttributes";
                info.type = new BookAttributes().getClass();
                return;
            default:
                return;
        }
    }

    public void setProperty(int index, Object value) {
        switch (index) {
            case 0:
                this.asin = value.toString();
                return;
            case 1:
                this.detailPageUrl = value.toString();
                return;
            case 2:
                this.itemAttributes = (BookAttributes) value;
                return;
            default:
                return;
        }
    }

    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("ASIN: ");
        buffer.append(this.asin);
        buffer.append("\n");
        buffer.append("Detail page URL: ");
        buffer.append(this.detailPageUrl);
        buffer.append("\n");
        buffer.append(this.itemAttributes.toString());
        return buffer.toString();
    }
}
