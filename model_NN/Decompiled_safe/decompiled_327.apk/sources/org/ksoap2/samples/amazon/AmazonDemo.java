package org.ksoap2.samples.amazon;

import java.util.Vector;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.Image;
import javax.microedition.lcdui.List;
import javax.microedition.lcdui.StringItem;
import javax.microedition.lcdui.TextField;
import javax.microedition.midlet.MIDlet;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransport;

public class AmazonDemo extends MIDlet implements CommandListener, Runnable {
    static Command backCommand = new Command("Back", 2, 1);
    static Command detailCommand = new Command("Details", 1, 1);
    static Command getCommand = new Command("Get", 1, 1);
    static Command newCommand = new Command("New", 1, 1);
    Display display;
    Form mainForm = new Form("Amazon Sample");
    List resultList;
    Vector resultVector;
    StringItem statusItem = new StringItem("Status", "idle");
    TextField symbolField = new TextField("Keyword", "pattern", 64, 0);
    TextField tagField = new TextField("Developer-Tag", "", 64, 0);

    public AmazonDemo() {
        this.mainForm.append(this.tagField);
        this.mainForm.append(this.symbolField);
        this.mainForm.append(this.statusItem);
        this.mainForm.addCommand(getCommand);
        this.mainForm.setCommandListener(this);
    }

    public void startApp() {
        this.display = Display.getDisplay(this);
        this.display.setCurrent(this.mainForm);
    }

    public void pauseApp() {
    }

    public void destroyApp(boolean unconditional) {
    }

    public void run() {
        try {
            String symbol = this.symbolField.getString();
            this.statusItem.setText("building request");
            SoapObject rpc = new SoapObject("urn:PI/DevCentral/SoapService", "KeywordSearchRequest");
            SoapObject ro = new SoapObject("urn:PI/DevCentral/SoapService", "KeywordRequest");
            ro.addProperty("keyword", symbol.trim().toLowerCase());
            ro.addProperty("tag", "webservices-20");
            ro.addProperty("type", "lite");
            ro.addProperty("mode", "book");
            ro.addProperty("page", "1");
            ro.addProperty("devtag", this.tagField.getString());
            rpc.addProperty("KeywordSearchRequest", ro);
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.bodyOut = rpc;
            HttpTransport ht = new HttpTransport("http://soap.amazon.com/onca/soap3");
            ht.debug = true;
            this.statusItem.setText("submitting request");
            try {
                ht.call(null, envelope);
                this.statusItem.setText("analyzing results...");
                System.err.println(ht.responseDump);
                this.resultVector = (Vector) ((SoapObject) envelope.getResponse()).getProperty("Details");
                this.resultList = new List("Result", 3);
                this.resultList.addCommand(newCommand);
                this.resultList.addCommand(detailCommand);
                this.resultList.setCommandListener(this);
                for (int i = 0; i < this.resultVector.size(); i++) {
                    this.resultList.append((String) ((SoapObject) this.resultVector.elementAt(i)).getProperty("ProductName"), (Image) null);
                }
                this.display.setCurrent(this.resultList);
            } catch (SoapFault e) {
                this.statusItem.setText("Error (perhaps keyword not found): " + e.faultstring);
            }
        } catch (Exception e2) {
            Exception e3 = e2;
            e3.printStackTrace();
            this.statusItem.setText("Error: " + e3.toString());
        }
    }

    public void commandAction(Command c, Displayable d) {
        if (c == getCommand) {
            new Thread(this).start();
        } else if (c == newCommand) {
            this.display.setCurrent(this.mainForm);
            this.statusItem.setText("idle");
        } else if (c == backCommand) {
            this.display.setCurrent(this.resultList);
        } else {
            int sel = this.resultList.getSelectedIndex();
            SoapObject details = (SoapObject) this.resultVector.elementAt(sel);
            Form detailForm = new Form("Details: " + this.resultList.getString(sel));
            detailForm.setCommandListener(this);
            detailForm.addCommand(backCommand);
            PropertyInfo pi = new PropertyInfo();
            for (int i = 0; i < details.getPropertyCount(); i++) {
                details.getPropertyInfo(i, null, pi);
                if (pi.name.toLowerCase().indexOf("url") == -1) {
                    detailForm.append(new StringItem(pi.name, new StringBuilder().append(details.getProperty(i)).toString()));
                }
            }
            this.display.setCurrent(detailForm);
        }
    }
}
