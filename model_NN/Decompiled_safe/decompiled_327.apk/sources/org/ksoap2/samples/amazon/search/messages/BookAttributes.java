package org.ksoap2.samples.amazon.search.messages;

import java.util.Hashtable;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapSerializationEnvelope;

public class BookAttributes extends BaseObject {
    private String author = "";
    private String creator;
    private String manufacturer;
    private String productGroup;
    private String title;

    public Object getProperty(int index) {
        throw new RuntimeException("BookAttributes.getProperty is not implemented yet");
    }

    public int getPropertyCount() {
        return 5;
    }

    public void getPropertyInfo(int index, Hashtable properties, PropertyInfo info) {
        info.type = PropertyInfo.STRING_CLASS;
        switch (index) {
            case 0:
                info.name = "Author";
                return;
            case 1:
                info.name = "Manufacturer";
                return;
            case 2:
                info.name = "ProductGroup";
                return;
            case 3:
                info.name = "Title";
                return;
            case 4:
                info.name = "Creator";
                return;
            default:
                return;
        }
    }

    public void setProperty(int index, Object value) {
        switch (index) {
            case 0:
                this.author = String.valueOf(this.author) + value.toString() + ";";
                return;
            case 1:
                this.manufacturer = value.toString();
                return;
            case 2:
                this.productGroup = value.toString();
                return;
            case 3:
                this.title = value.toString();
                return;
            case 4:
                this.creator = value.toString();
                return;
            default:
                return;
        }
    }

    public void register(SoapSerializationEnvelope envelope) {
        envelope.addMapping("http://webservices.amazon.com/AWSECommerceService/2006-05-17", "ItemAttributes", getClass());
    }

    public String toString() {
        StringBuffer buffer = new StringBuffer("*** Attributes ***\n");
        buffer.append("Author: ");
        buffer.append(this.author);
        buffer.append("\n");
        buffer.append("Manufacturer: ");
        buffer.append(this.manufacturer);
        buffer.append("\n");
        buffer.append("Product Group: ");
        buffer.append(this.productGroup);
        buffer.append("\n");
        buffer.append("Title: ");
        buffer.append(this.title);
        buffer.append("\n");
        if (this.creator != null) {
            buffer.append("Creator: ");
            buffer.append(this.creator);
            buffer.append("\n");
        }
        return buffer.toString();
    }
}
