package org.ksoap2.samples.amazon.search.messages;

import java.util.Hashtable;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapSerializationEnvelope;

public class Request extends BaseObject {
    public String author;
    public String searchIndex;

    public Object getProperty(int index) {
        if (index == 0) {
            return this.author;
        }
        return this.searchIndex;
    }

    public int getPropertyCount() {
        return 2;
    }

    public void getPropertyInfo(int index, Hashtable properties, PropertyInfo info) {
        info.type = PropertyInfo.STRING_CLASS;
        if (index == 0) {
            info.name = "Author";
        } else {
            info.name = "SearchIndex";
        }
    }

    public void setProperty(int index, Object value) {
        throw new RuntimeException("Request.setProperty is not implemented yet");
    }

    public void register(SoapSerializationEnvelope envelope) {
        envelope.addMapping("http://webservices.amazon.com/AWSECommerceService/2006-05-17", "ItemSearchRequest", getClass());
    }
}
