package org.ksoap2.samples.quotes;

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.StringItem;
import javax.microedition.lcdui.TextField;
import javax.microedition.midlet.MIDlet;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransport;

public class StockQuoteDemo extends MIDlet implements CommandListener, Runnable {
    Command getCommand = new Command("Get", 1, 1);
    Form mainForm = new Form("StockQuotes");
    StringItem resultItem = new StringItem("", "");
    TextField symbolField = new TextField("Symbol", "IBM", 5, 0);

    public StockQuoteDemo() {
        this.mainForm.append(this.symbolField);
        this.mainForm.append(this.resultItem);
        this.mainForm.addCommand(this.getCommand);
        this.mainForm.setCommandListener(this);
    }

    public void startApp() {
        Display.getDisplay(this).setCurrent(this.mainForm);
    }

    public void pauseApp() {
    }

    public void destroyApp(boolean unconditional) {
    }

    public void run() {
        try {
            String symbol = this.symbolField.getString();
            SoapObject rpc = new SoapObject("urn:xmethods-delayed-quotes", "getQuote");
            rpc.addProperty("symbol", symbol);
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(100);
            envelope.bodyOut = rpc;
            this.resultItem.setLabel(symbol);
            new HttpTransport("http://services.xmethods.net/soap").call("urn:xmethods-delayed-quotes#getQuote", envelope);
            this.resultItem.setText(new StringBuilder().append(envelope.getResponse()).toString());
        } catch (Exception e) {
            Exception e2 = e;
            e2.printStackTrace();
            this.resultItem.setLabel("Error:");
            this.resultItem.setText(e2.toString());
        }
    }

    public void commandAction(Command c, Displayable d) {
        new Thread(this).start();
    }

    public static void main(String[] argv) {
        new StockQuoteDemo().startApp();
    }
}
