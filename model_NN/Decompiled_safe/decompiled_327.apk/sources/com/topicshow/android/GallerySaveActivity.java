package com.topicshow.android;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import com.photogrid.android.R;
import com.topicshow.data.TableFunction.GalleryFunc;
import com.topicshow.data.TableFunction.StatusFunc;
import com.topicshow.data.adapter.DBAdapter;
import com.topicshow.data.table.StatusData;
import com.topicshow.utils.FileUploadTask;
import com.topicshow.utils.ImageData;
import com.topicshow.utils.MessageWS;
import com.topicshow.utils.ProgressDialogThread;
import com.topicshow.utils.SimpleDialog;
import java.io.IOException;
import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParserException;

public class GallerySaveActivity extends CommonActivity {
    public static FileUploadTask task = null;
    protected Class<?> ACTIVITY = GalleryHistoryActivity.class;
    /* access modifiers changed from: private */
    public String Category = "";
    /* access modifiers changed from: private */
    public String canvasid = "-1";
    /* access modifiers changed from: private */
    public ArrayList<ImageData> dataset;
    /* access modifiers changed from: private */
    public long gallery_added = -1;
    /* access modifiers changed from: private */
    public String gallery_id = "";
    public boolean isClicked = false;
    /* access modifiers changed from: private */
    public String userid = "";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.gallerysavelayout);
        Spinner category = (Spinner) findViewById(R.id.category);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.image_category, 17367048);
        adapter.setDropDownViewResource(R.layout.dropdownlayout);
        category.setAdapter((SpinnerAdapter) adapter);
        Intent intent = getIntent();
        ArrayList<String> ids = intent.getStringArrayListExtra("image_ids");
        ArrayList<String> titles = intent.getStringArrayListExtra("titles");
        ArrayList<String> descriptions = intent.getStringArrayListExtra("descriptions");
        ArrayList<Integer> rotations = intent.getIntegerArrayListExtra("rotations");
        this.userid = intent.getStringExtra("userid");
        this.canvasid = intent.getStringExtra("canvasid");
        if (task == null || task.isCompleted) {
            task = null;
        } else {
            task.cancel(true);
            task = null;
        }
        if (task == null) {
            task = new FileUploadTask(this, getString(R.string.FORM_IMAGE_UPLOAD), this.userid, this.canvasid);
            try {
                this.dataset = setupImages(ids, titles, descriptions, rotations);
                this.gallery_id = String.valueOf(this.dataset.get(0).getImageID());
                this.gallery_added = this.dataset.get(0).getImageAdded();
                task.execute(this.dataset);
            } catch (Exception e) {
                Log.w("========", e.getLocalizedMessage());
            }
        }
        category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                Button btn_category = (Button) GallerySaveActivity.this.findViewById(R.id.btn_category);
                if (!GallerySaveActivity.this.isClicked || pos <= 0) {
                    GallerySaveActivity.this.Category = "";
                    btn_category.setText("Choose a category");
                    return;
                }
                GallerySaveActivity.this.Category = parent.getItemAtPosition(pos).toString();
                btn_category.setText(GallerySaveActivity.this.Category);
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        ((Button) findViewById(R.id.btn_category)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                GallerySaveActivity.this.isClicked = true;
                Button button = (Button) v;
                ((Spinner) GallerySaveActivity.this.findViewById(R.id.category)).performClick();
            }
        });
        ((Button) findViewById(R.id.btn_next)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ProgressDialogThread progressDialogThread = new ProgressDialogThread(GallerySaveActivity.this, "", "Sending ... ");
                Thread thread = new Thread(progressDialogThread);
                progressDialogThread.setViewState(v, false);
                if (!GallerySaveActivity.task.isCompleted) {
                    SimpleDialog.showAlert("Alert ...", "Please wait while the photos are uploading.", v.getContext());
                    progressDialogThread.setViewState(v, true);
                    return;
                }
                MessageWS messageWS = new MessageWS(GallerySaveActivity.this);
                final DBAdapter dBAdapter = new DBAdapter(v.getContext());
                String validation_string = "";
                String privacy = "EVERYONE";
                try {
                    RadioButton everyone = (RadioButton) GallerySaveActivity.this.findViewById(R.id.privacy_everyone);
                    RadioButton friends = (RadioButton) GallerySaveActivity.this.findViewById(R.id.privacy_friend);
                    RadioButton onlyme = (RadioButton) GallerySaveActivity.this.findViewById(R.id.privacy_onlyme);
                    final EditText title = (EditText) GallerySaveActivity.this.findViewById(R.id.txt_title);
                    final EditText description = (EditText) GallerySaveActivity.this.findViewById(R.id.txt_description);
                    if (title.getText().toString().trim().equals("")) {
                        validation_string = String.valueOf(validation_string) + "Title\n";
                    }
                    if (GallerySaveActivity.this.Category == "") {
                        validation_string = String.valueOf(validation_string) + "Category\n";
                    }
                    if (everyone.isChecked()) {
                        privacy = "Public";
                    } else if (friends.isChecked()) {
                        privacy = "FacebookFriends";
                    } else if (onlyme.isChecked()) {
                        privacy = "Private";
                    } else {
                        validation_string = String.valueOf(validation_string) + "Privacy Setting\n";
                    }
                    if (validation_string != "") {
                        SimpleDialog.showAlert("Please enter:", validation_string, v.getContext());
                    } else {
                        progressDialogThread.showDialog();
                        thread.start();
                        if (!GallerySaveActivity.task.hasFailure) {
                            if (messageWS.createGallery(GallerySaveActivity.this.userid, GallerySaveActivity.this.canvasid, GallerySaveActivity.this.Category, title.getText().toString(), description.getText().toString(), privacy)) {
                                dBAdapter.open();
                                long insertGallery = GalleryFunc.insertGallery(dBAdapter, title.getText().toString(), description.getText().toString(), GallerySaveActivity.this.Category, privacy, GallerySaveActivity.this.gallery_id, GallerySaveActivity.this.gallery_added, StatusFunc.GetStatusID(dBAdapter, StatusData.STATUS_INDEX.SUCCESS), GallerySaveActivity.this.userid);
                                dBAdapter.close();
                                Intent intent = new Intent();
                                intent.setClass(v.getContext(), GallerySaveActivity.this.ACTIVITY);
                                intent.putExtra("facebookID", GallerySaveActivity.this.userid);
                                GallerySaveActivity.this.startActivity(intent);
                                progressDialogThread.isRunning = false;
                                progressDialogThread.setViewState(v, true);
                            } else {
                                progressDialogThread.isRunning = false;
                                progressDialogThread.setViewState(v, true);
                            }
                        } else if (GallerySaveActivity.task.hasFailure) {
                            final String temp_privacy = privacy;
                            progressDialogThread.isRunning = false;
                            progressDialogThread.setViewState(v, true);
                            final MessageWS messageWS2 = messageWS;
                            SimpleDialog.showWithTwoButton("Warning", "Not all photos successfully uploaded. Please click on 'Resend' button to resend the photo. Otherwise click 'Finish'.", "Resend", "Finish", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    GallerySaveActivity.task = new FileUploadTask(GallerySaveActivity.this, GallerySaveActivity.this.getString(R.string.FORM_IMAGE_UPLOAD), GallerySaveActivity.this.userid, GallerySaveActivity.this.canvasid);
                                    GallerySaveActivity.task.execute(GallerySaveActivity.this.dataset);
                                }
                            }, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    try {
                                        if (messageWS2.createGallery(GallerySaveActivity.this.userid, GallerySaveActivity.this.canvasid, GallerySaveActivity.this.Category, title.getText().toString(), description.getText().toString(), temp_privacy)) {
                                            dBAdapter.open();
                                            long insertGallery = GalleryFunc.insertGallery(dBAdapter, title.getText().toString(), description.getText().toString(), GallerySaveActivity.this.Category, temp_privacy, GallerySaveActivity.this.gallery_id, GallerySaveActivity.this.gallery_added, StatusFunc.GetStatusID(dBAdapter, StatusData.STATUS_INDEX.SUCCESS), GallerySaveActivity.this.userid);
                                            dBAdapter.close();
                                            Intent intent = new Intent();
                                            intent.setClass(GallerySaveActivity.this, GallerySaveActivity.this.ACTIVITY);
                                            intent.putExtra("facebookID", GallerySaveActivity.this.userid);
                                            GallerySaveActivity.this.startActivity(intent);
                                        }
                                    } catch (SQLException | IOException | Exception | XmlPullParserException e) {
                                    }
                                }
                            }, v.getContext());
                        }
                    }
                } catch (IOException e) {
                } catch (XmlPullParserException e2) {
                } catch (Exception e3) {
                } finally {
                    progressDialogThread.isRunning = false;
                    progressDialogThread.setViewState(v, true);
                }
            }
        });
    }

    private void enabledButtonState(boolean state) {
        if (!state) {
            ((Button) findViewById(R.id.btn_next)).setEnabled(false);
        } else {
            ((Button) findViewById(R.id.btn_next)).setEnabled(true);
        }
    }

    private void setButtonText(String text) {
        ((Button) findViewById(R.id.btn_next)).setText(text);
    }

    private ArrayList<ImageData> setupImages(ArrayList<String> ids, ArrayList<String> titles, ArrayList<String> descriptions, ArrayList<Integer> rotations) {
        Log.w("========", "aaa");
        ArrayList<ImageData> dataset2 = new ArrayList<>();
        Log.w("========", "bbb");
        String whereClause = "";
        for (int i = 0; i < ids.size(); i++) {
            if (i == ids.size() - 1 || ids.size() <= 1) {
                whereClause = String.valueOf(whereClause) + "_id = " + ids.get(i) + " ";
            } else {
                whereClause = String.valueOf(whereClause) + "_id = " + ids.get(i) + " OR ";
            }
        }
        Cursor cursor = managedQuery(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, whereClause, null, null);
        if (cursor.moveToFirst()) {
            try {
                ArrayList<ImageData> temporary = new ArrayList<>();
                do {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    Uri uri = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, String.valueOf(cursor.getString(cursor.getColumnIndex("_id"))));
                    int rotation = 0;
                    int index = ids.indexOf(Long.toString(cursor.getLong(cursor.getColumnIndex("_id"))));
                    if (index == -1) {
                        rotation = 0;
                    }
                    Log.w("========", new StringBuilder(String.valueOf(index)).toString());
                    Log.w("========", titles.get(index));
                    Log.w("========", descriptions.get(index));
                    Log.w("========", "Rotation size " + rotation);
                    ImageData data = new ImageData();
                    data.SetURI(uri);
                    data.SetImageID(cursor.getLong(cursor.getColumnIndex("_id")));
                    data.SetFilename(String.valueOf(cursor.getString(cursor.getColumnIndex("_id"))));
                    data.SetThumbnail(MediaStore.Images.Thumbnails.getThumbnail(getContentResolver(), cursor.getLong(cursor.getColumnIndex("_id")), 3, options));
                    data.SetTitle(titles.get(index));
                    data.SetDescription(descriptions.get(index));
                    data.SetRotation(rotations.get(index));
                    data.SetImageAdded(cursor.getLong(cursor.getColumnIndex("date_added")));
                    Log.w("========", "set start " + rotation);
                    temporary.add(data);
                    Log.w("========", "set end " + rotation);
                } while (cursor.moveToNext());
                cursor.close();
                for (int i2 = 0; i2 < ids.size(); i2++) {
                    String id = ids.get(i2);
                    int j = 0;
                    while (true) {
                        if (j >= temporary.size()) {
                            break;
                        }
                        ImageData data2 = temporary.get(j);
                        if (data2.getImageID() == Long.valueOf(id).longValue()) {
                            dataset2.add(data2);
                            break;
                        }
                        j++;
                    }
                }
            } catch (Exception e) {
                Exception e2 = e;
                Log.w("eeeeeeeeeee", e2.getMessage());
                Log.w("Error: ", e2.getMessage());
            }
        }
        return dataset2;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (Integer.valueOf(Build.VERSION.SDK).intValue() < 7 && keyCode == 4) {
            event.getRepeatCount();
        }
        return super.onKeyDown(keyCode, event);
    }

    public void onBackPressed() {
    }
}
