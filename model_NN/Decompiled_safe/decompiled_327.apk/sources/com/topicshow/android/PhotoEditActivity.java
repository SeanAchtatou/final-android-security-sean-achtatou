package com.topicshow.android;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.photogrid.android.R;
import com.topcishow.android.image.ImageAdapter;
import com.topicshow.data.TableFunction.UserFunc;
import com.topicshow.data.adapter.DBAdapter;
import com.topicshow.utils.ImageManupulator;
import com.topicshow.utils.MessageWS;
import com.topicshow.utils.ProgressDialogThread;
import java.util.ArrayList;
import org.json.JSONObject;

public class PhotoEditActivity extends CommonActivity {
    protected Class<?> ACTIVITY = GallerySaveActivity.class;
    public View.OnClickListener accessFacebook = new View.OnClickListener() {
        public void onClick(final View v) {
            PhotoEditActivity.this.titles.set(PhotoEditActivity.this.current_position, ((TextView) PhotoEditActivity.this.findViewById(R.id.txt_title)).getText().toString());
            PhotoEditActivity.this.descriptions.set(PhotoEditActivity.this.current_position, ((TextView) PhotoEditActivity.this.findViewById(R.id.txt_description)).getText().toString());
            final ProgressDialogThread progress = new ProgressDialogThread(PhotoEditActivity.this, "", "Accessing Facebook ...");
            Thread progress_thread = new Thread(progress);
            progress.showDialog();
            progress.setViewState(v, false);
            progress_thread.start();
            try {
                PhotoEditActivity.this.facebook = new Facebook(PhotoEditActivity.this.getString(R.string.FacebookAppID));
                SharedPreferences setting = PhotoEditActivity.this.getSharedPreferences("USER_PREF", 0);
                String token = setting.getString("facebook_access_token", null);
                Long expire = Long.valueOf(setting.getLong("facebook_access_expired", 0));
                PhotoEditActivity.this.facebook.setAccessToken(token);
                PhotoEditActivity.this.facebook.setAccessExpires(expire.longValue());
                if (PhotoEditActivity.this.facebook.isSessionValid()) {
                    PhotoEditActivity.this.nextActivity(PhotoEditActivity.this.facebook);
                    progress.isRunning = false;
                    progress.setViewState(v, true);
                } else {
                    PhotoEditActivity.this.facebook.authorize((Activity) v.getContext(), new String[]{"offline_access", "user_photos", "publish_stream"}, new Facebook.DialogListener() {
                        public void onComplete(Bundle values) {
                            SharedPreferences.Editor editor = PhotoEditActivity.this.getSharedPreferences("USER_PREF", 0).edit();
                            editor.putString("facebook_access_token", PhotoEditActivity.this.facebook.getAccessToken());
                            editor.putLong("facebook_access_expired", PhotoEditActivity.this.facebook.getAccessExpires());
                            editor.commit();
                            try {
                                progress.isRunning = false;
                                progress.setViewState(v, true);
                                PhotoEditActivity.this.nextActivity(PhotoEditActivity.this.facebook);
                            } catch (Exception e) {
                            } finally {
                                progress.isRunning = false;
                                progress.setViewState(v, true);
                            }
                        }

                        public void onFacebookError(FacebookError error) {
                            progress.isRunning = false;
                            progress.setViewState(v, true);
                        }

                        public void onError(DialogError e) {
                            progress.isRunning = false;
                            progress.setViewState(v, true);
                        }

                        public void onCancel() {
                            progress.isRunning = false;
                            progress.setViewState(v, true);
                        }
                    });
                }
            } catch (Exception e) {
            } finally {
                progress.isRunning = false;
                progress.setViewState(v, true);
            }
        }
    };
    public View.OnClickListener click_gallery_move = new View.OnClickListener() {
        /* Debug info: failed to restart local var, previous not found, register: 12 */
        public void onClick(View v) {
            Log.w("===========", String.valueOf(String.valueOf(v.getId())) + String.valueOf((int) R.id.rotateCW));
            Gallery gallery_control = (Gallery) PhotoEditActivity.this.findViewById(R.id.gallery1);
            int previous = PhotoEditActivity.this.current_position;
            try {
                switch (v.getId()) {
                    case R.id.previousbutton:
                        if (PhotoEditActivity.this.current_position > 0) {
                            PhotoEditActivity.this.current_position--;
                            for (int i = 0; i < gallery_control.getChildCount(); i++) {
                                View view = gallery_control.getChildAt(i);
                                view.setBackgroundColor(view.getContext().getResources().getColor(R.color.thumbnail_medium_unselected));
                            }
                            gallery_control.setSelection(PhotoEditActivity.this.current_position, true);
                            gallery_control.getSelectedView().setBackgroundColor(gallery_control.getResources().getColor(R.color.thumbnail_medium_selected));
                            PhotoEditActivity.this.updatePhoto((Long) ((ImageView) gallery_control.getSelectedView()).getTag(), PhotoEditActivity.this.rotations.get(PhotoEditActivity.this.current_position).intValue());
                            PhotoEditActivity.this.updateDetails(gallery_control.getSelectedView(), previous, PhotoEditActivity.this.current_position);
                            return;
                        }
                        return;
                    case R.id.gallery1:
                    case R.id.linearLayout3:
                    case R.id.tableRow3:
                    case R.id.linearLayout5:
                    case R.id.lbl_cw:
                    default:
                        return;
                    case R.id.nextbutton:
                        if (PhotoEditActivity.this.current_position < PhotoEditActivity.this.image_ids.size() - 1) {
                            PhotoEditActivity.this.current_position++;
                            for (int i2 = 0; i2 < gallery_control.getChildCount(); i2++) {
                                View view2 = gallery_control.getChildAt(i2);
                                view2.setBackgroundColor(view2.getContext().getResources().getColor(R.color.thumbnail_medium_unselected));
                            }
                            gallery_control.setSelection(PhotoEditActivity.this.current_position, true);
                            gallery_control.getSelectedView().setBackgroundColor(gallery_control.getResources().getColor(R.color.thumbnail_medium_selected));
                            PhotoEditActivity.this.updatePhoto((Long) ((ImageView) gallery_control.getSelectedView()).getTag(), PhotoEditActivity.this.rotations.get(PhotoEditActivity.this.current_position).intValue());
                            PhotoEditActivity.this.updateDetails(gallery_control.getSelectedView(), previous, PhotoEditActivity.this.current_position);
                            return;
                        }
                        return;
                    case R.id.rotateCW:
                        int rotation2 = PhotoEditActivity.this.rotations.get(PhotoEditActivity.this.current_position).intValue() + 90;
                        PhotoEditActivity.this.rotations.set(PhotoEditActivity.this.current_position, Integer.valueOf(rotation2));
                        PhotoEditActivity.this.updatePhoto((Long) ((ImageView) gallery_control.getSelectedView()).getTag(), rotation2);
                        return;
                    case R.id.rotateCCW:
                        int rotation1 = PhotoEditActivity.this.rotations.get(PhotoEditActivity.this.current_position).intValue() - 90;
                        PhotoEditActivity.this.rotations.set(PhotoEditActivity.this.current_position, Integer.valueOf(rotation1));
                        PhotoEditActivity.this.updatePhoto((Long) ((ImageView) gallery_control.getSelectedView()).getTag(), rotation1);
                        return;
                }
            } catch (Exception e) {
            }
        }
    };
    public AdapterView.OnItemClickListener click_thumbnail = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
            for (int i = 0; i < parent.getChildCount(); i++) {
                View view = parent.getChildAt(i);
                view.setBackgroundColor(view.getContext().getResources().getColor(R.color.thumbnail_medium_unselected));
            }
            v.setBackgroundColor(v.getResources().getColor(R.color.thumbnail_medium_selected));
            PhotoEditActivity.this.updatePhoto((Long) ((ImageView) v).getTag(), PhotoEditActivity.this.rotations.get(position).intValue());
            PhotoEditActivity.this.updateDetails(v, PhotoEditActivity.this.current_position, position);
        }
    };
    protected int current_position = 0;
    protected ArrayList<String> descriptions;
    protected Facebook facebook;
    protected ArrayList<String> image_ids;
    protected ArrayList<Integer> rotations;
    protected ArrayList<String> titles;
    protected final String user_preference = "USER_PREF";

    public void onCreate(Bundle savestated) {
        super.onCreate(savestated);
        setContentView((int) R.layout.photoeditlayout);
        this.image_ids = getIntent().getStringArrayListExtra("image_ids");
        this.titles = new ArrayList<>(this.image_ids.size());
        this.descriptions = new ArrayList<>(this.image_ids.size());
        this.rotations = new ArrayList<>(this.image_ids.size());
        for (int i = 0; i < this.image_ids.size(); i++) {
            this.titles.add("");
            this.descriptions.add("");
            this.rotations.add(0);
        }
        Gallery gallery_control = (Gallery) findViewById(R.id.gallery1);
        gallery_control.setAdapter((SpinnerAdapter) new ImageAdapter(this, null, this.image_ids));
        ((Button) findViewById(R.id.btnPreview)).setOnClickListener(this.accessFacebook);
        ((ImageButton) findViewById(R.id.previousbutton)).setOnClickListener(this.click_gallery_move);
        ((ImageButton) findViewById(R.id.nextbutton)).setOnClickListener(this.click_gallery_move);
        ((ImageButton) findViewById(R.id.rotateCW)).setOnClickListener(this.click_gallery_move);
        ((ImageButton) findViewById(R.id.rotateCCW)).setOnClickListener(this.click_gallery_move);
        gallery_control.setOnItemClickListener(this.click_thumbnail);
        gallery_control.setSelection(this.current_position, true);
        gallery_control.performItemClick(gallery_control.getSelectedView(), 0, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* access modifiers changed from: private */
    public void updatePhoto(Long image_id, int rotation) {
        Cursor cursor = managedQuery(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{"_data"}, "_id = " + String.valueOf(image_id), null, null);
        if (cursor.moveToFirst()) {
            Bitmap bMap = ImageManupulator.ImageResizing(getContentResolver(), Uri.parse("file://" + cursor.getString(cursor.getColumnIndex("_data"))), 600, 400);
            cursor.close();
            Matrix mat = new Matrix();
            mat.postRotate((float) rotation);
            ((ImageView) findViewById(R.id.image)).setImageBitmap(Bitmap.createBitmap(bMap, 0, 0, bMap.getWidth(), bMap.getHeight(), mat, true));
        }
    }

    /* access modifiers changed from: private */
    public void updateDetails(View view, int current_position2, int new_position) {
        TextView text_title = (TextView) findViewById(R.id.txt_title);
        TextView text_description = (TextView) findViewById(R.id.txt_description);
        this.titles.set(current_position2, text_title.getText().toString());
        this.descriptions.set(current_position2, text_description.getText().toString());
        String title = this.titles.get(new_position);
        String description = this.descriptions.get(new_position);
        if (title == null) {
            text_title.setText("");
        }
        if (description == null) {
            text_description.setText("");
        }
        text_title.setText(title);
        text_description.setText(description);
        this.current_position = new_position;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (this.facebook != null) {
            this.facebook.authorizeCallback(requestCode, resultCode, data);
        }
    }

    public void nextActivity(Facebook facebook2) throws Exception {
        JSONObject user = new JSONObject(facebook2.request("me"));
        String id = (String) user.get("id");
        String name = (String) user.get("name");
        String firstname = (String) user.get("first_name");
        String lastname = (String) user.get("last_name");
        MessageWS messageWS = new MessageWS(this);
        DBAdapter dBAdapter = new DBAdapter(this);
        dBAdapter.open();
        Cursor user_cursor = UserFunc.FetchUserByFacebookID(dBAdapter, id);
        long index = -1;
        if (user_cursor != null && user_cursor.getCount() <= 0) {
            index = UserFunc.insertUser(dBAdapter, id, firstname, lastname);
        } else if (user_cursor != null) {
            index = UserFunc.GetUserIDFromCursor(user_cursor, 0).longValue();
        }
        user_cursor.close();
        dBAdapter.close();
        if (index > -1) {
            String result = messageWS.createAccount(id, firstname, lastname);
            if (!result.equals("-1")) {
                Intent intent = new Intent();
                intent.setClass(getApplication(), this.ACTIVITY);
                intent.putStringArrayListExtra("image_ids", this.image_ids);
                intent.putStringArrayListExtra("titles", this.titles);
                intent.putStringArrayListExtra("descriptions", this.descriptions);
                intent.putIntegerArrayListExtra("rotations", this.rotations);
                intent.putExtra("canvasid", result);
                intent.putExtra("userid", id);
                intent.putExtra("name", name);
                intent.putExtra("lastname", lastname);
                intent.putExtra("firstname", firstname);
                startActivity(intent);
            }
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (Integer.valueOf(Build.VERSION.SDK).intValue() < 7 && keyCode == 4) {
            event.getRepeatCount();
        }
        return super.onKeyDown(keyCode, event);
    }

    public void onBackPressed() {
    }
}
