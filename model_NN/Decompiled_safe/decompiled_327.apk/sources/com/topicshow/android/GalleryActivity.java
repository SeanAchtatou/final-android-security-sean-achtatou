package com.topicshow.android;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.LinearLayout;
import com.facebook.android.Facebook;
import com.photogrid.android.R;
import com.topicshow.controls.ThumbnailGridView;
import com.topicshow.utils.SimpleDialog;
import com.topicshow.utils.ThumbnailTaskLoader;

public class GalleryActivity extends CommonActivity {
    protected Class<?> ACTIVITY = PhotoEditActivity.class;
    private Facebook facebook;
    protected ThumbnailGridView grid = null;
    private View.OnClickListener nextOnClick = new View.OnClickListener() {
        public void onClick(View arg0) {
            if (GalleryActivity.this.grid.thumbnail_creator != null && !GalleryActivity.this.grid.thumbnail_creator.isCancelled()) {
                GalleryActivity.this.grid.thumbnail_creator.cancel(true);
            }
            if (GalleryActivity.this.grid.thumbnail_creator != null) {
                GalleryActivity.this.grid.thumbnail_creator.closeDialog();
            }
            GalleryActivity.this.grid.thumbnail_creator = new ThumbnailTaskLoader(GalleryActivity.this, GalleryActivity.this.grid, true);
            Button next = (Button) GalleryActivity.this.findViewById(R.id.btn_next);
            next.setVisibility(0);
            ((Button) GalleryActivity.this.findViewById(R.id.btn_previous)).setVisibility(0);
            GalleryActivity.this.grid.next();
            if (!GalleryActivity.this.grid.hasMore() || GalleryActivity.this.grid.page >= GalleryActivity.this.grid.total_page) {
                next.setVisibility(4);
            }
        }
    };
    private View.OnClickListener previousOnClick = new View.OnClickListener() {
        public void onClick(View arg0) {
            if (GalleryActivity.this.grid.thumbnail_creator != null && !GalleryActivity.this.grid.thumbnail_creator.isCancelled()) {
                GalleryActivity.this.grid.thumbnail_creator.cancel(true);
            }
            if (GalleryActivity.this.grid.thumbnail_creator != null) {
                GalleryActivity.this.grid.thumbnail_creator.closeDialog();
            }
            GalleryActivity.this.grid.thumbnail_creator = new ThumbnailTaskLoader(GalleryActivity.this, GalleryActivity.this.grid, false);
            Button next = (Button) GalleryActivity.this.findViewById(R.id.btn_next);
            next.setVisibility(0);
            Button previous = (Button) GalleryActivity.this.findViewById(R.id.btn_previous);
            previous.setVisibility(0);
            GalleryActivity.this.grid.previous();
            if (!GalleryActivity.this.grid.hasPrevious) {
                previous.setVisibility(4);
            }
            if (!GalleryActivity.this.grid.hasMore()) {
                next.setVisibility(4);
            }
        }
    };
    private AbsListView.OnScrollListener scrollChanged = new AbsListView.OnScrollListener() {
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            if (firstVisibleItem + visibleItemCount >= totalItemCount && totalItemCount != 0 && GalleryActivity.this.grid.hasMore() && GalleryActivity.this.grid.page < GalleryActivity.this.grid.total_page) {
                ((Button) GalleryActivity.this.findViewById(R.id.btn_next)).setVisibility(0);
            }
        }

        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }
    };
    private final String user_preference = "USER_PREF";

    public void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView((int) R.layout.gallerylayout);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(-1, -1);
            params.weight = 1.0f;
            Log.w("==============", "sfsfsadfsdaf");
            this.grid = new ThumbnailGridView(this, getBaseContext());
            this.grid.setOnScrollListener(this.scrollChanged);
            this.grid.setWillNotDraw(false);
            this.grid.setId(R.id.imageView1);
            ((LinearLayout) findViewById(R.id.thumbnail_grid)).addView(this.grid, params);
            ((Button) findViewById(R.id.btn_previous)).setOnClickListener(this.previousOnClick);
            ((Button) findViewById(R.id.btn_next)).setOnClickListener(this.nextOnClick);
            ((Button) findViewById(R.id.btnPreview)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Log.w("=============", "GA");
                    if (GalleryActivity.this.grid.saved.size() < 1) {
                        SimpleDialog.showAlert("Alert", "Please select at least one photo", v.getContext());
                    } else if (GalleryActivity.this.grid.saved.size() > 16) {
                        SimpleDialog.showAlert("Alert", "Limit of 16 Photos for each " + GalleryActivity.this.getString(R.string.app_name), v.getContext());
                    } else {
                        Intent intent = new Intent();
                        intent.putStringArrayListExtra("image_ids", GalleryActivity.this.grid.saved);
                        intent.setClass(GalleryActivity.this.getApplication(), GalleryActivity.this.ACTIVITY);
                        GalleryActivity.this.startActivity(intent);
                    }
                }
            });
        } catch (Exception e) {
        }
    }
}
