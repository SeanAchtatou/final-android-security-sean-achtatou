package com.topicshow.android;

import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.photogrid.android.R;
import com.topicshow.data.TableFunction.GalleryFunc;
import com.topicshow.data.adapter.DBAdapter;
import com.topicshow.utils.HistoryAdapter;

public class GalleryHistoryActivity extends CommonActivity {
    protected Class<?> ACTIVITY = GalleryActivity.class;
    private View.OnClickListener btnListener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.setFlags(67108864);
            intent.setClass(v.getContext(), GalleryHistoryActivity.this.ACTIVITY);
            GalleryHistoryActivity.this.startActivity(intent);
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.galleryhistorylayout);
        String facebookID = getIntent().getStringExtra("facebookID");
        DBAdapter adapter = new DBAdapter(this);
        try {
            adapter.open();
            Cursor cursor_gallery = GalleryFunc.FetchGalleriesByFacebookID(adapter, facebookID);
            GridView listview = (GridView) findViewById(R.id.list_gallery_item);
            if (cursor_gallery != null) {
                listview.setAdapter((ListAdapter) new HistoryAdapter(this, cursor_gallery));
            }
            adapter.close();
        } catch (Exception e) {
        }
        ((Button) findViewById(R.id.btnCreate)).setOnClickListener(this.btnListener);
        AdView adView = new AdView(this, AdSize.BANNER, getString(R.string.publish_id));
        AdRequest request = new AdRequest();
        ((LinearLayout) findViewById(R.id.google_ads)).addView(adView);
        adView.loadAd(request);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (Integer.valueOf(Build.VERSION.SDK).intValue() < 7 && keyCode == 4) {
            event.getRepeatCount();
        }
        return super.onKeyDown(keyCode, event);
    }

    public void onBackPressed() {
    }
}
