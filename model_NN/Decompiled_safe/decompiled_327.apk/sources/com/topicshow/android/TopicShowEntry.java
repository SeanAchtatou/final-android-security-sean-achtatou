package com.topicshow.android;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import com.photogrid.android.R;

public class TopicShowEntry extends CommonActivity {
    protected Class<?> ACTIVITY = GalleryActivity.class;
    private View.OnClickListener btnListener = new View.OnClickListener() {
        public void onClick(View v) {
            ((Button) v).setText("Loading Photos ...");
            Intent intent = new Intent();
            intent.setClass(v.getContext(), TopicShowEntry.this.ACTIVITY);
            TopicShowEntry.this.startActivity(intent);
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        ((Button) findViewById(R.id.btnview_created)).setOnClickListener(this.btnListener);
    }

    public void onResume() {
        super.onResume();
        ((Button) findViewById(R.id.btnview_created)).setText(getResources().getText(R.string.mainpage_btn_1));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }
}
