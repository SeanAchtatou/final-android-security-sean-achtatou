package com.topicshow.android;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.photogrid.android.R;
import java.io.IOException;
import java.net.MalformedURLException;

public class CommonActivity extends Activity {
    protected Class<?> HomeActivity = TopicShowEntry.class;
    /* access modifiers changed from: private */
    public Facebook facebook;

    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.menu_item_login);
        if (checkFacebookAuthorized()) {
            item.setTitle("Logout from facebook");
            return true;
        }
        item.setTitle("Login to facebook");
        return true;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.common_menu, menu);
        return true;
    }

    public boolean checkFacebookAuthorized() {
        this.facebook = new Facebook(getString(R.string.FacebookAppID));
        SharedPreferences setting = getSharedPreferences("USER_PREF", 0);
        String token = setting.getString("facebook_access_token", null);
        Long expire = Long.valueOf(setting.getLong("facebook_access_expired", 0));
        this.facebook.setAccessToken(token);
        this.facebook.setAccessExpires(expire.longValue());
        if (this.facebook.isSessionValid()) {
            return true;
        }
        return false;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getTitle().equals("Logout from facebook")) {
            try {
                if (GallerySaveActivity.task != null && !GallerySaveActivity.task.isCancelled()) {
                    GallerySaveActivity.task.cancel(true);
                    GallerySaveActivity.task = null;
                }
                System.gc();
                if (this.facebook.logout(this).equals("true")) {
                    SharedPreferences.Editor editor = getSharedPreferences("USER_PREF", 0).edit();
                    editor.putString("facebook_access_token", null);
                    editor.putLong("facebook_access_expired", this.facebook.getAccessExpires());
                    editor.commit();
                }
                Intent intent = new Intent();
                intent.setFlags(67108864);
                intent.setClass(this, this.HomeActivity);
                startActivity(intent);
                finish();
            } catch (IOException | MalformedURLException e) {
            }
            return true;
        } else if (item.getTitle().equals("Login to facebook")) {
            this.facebook.authorize(this, new String[]{"offline_access", "user_photos", "publish_stream"}, new Facebook.DialogListener() {
                public void onComplete(Bundle values) {
                    SharedPreferences.Editor editor = CommonActivity.this.getSharedPreferences("USER_PREF", 0).edit();
                    editor.putString("facebook_access_token", CommonActivity.this.facebook.getAccessToken());
                    editor.putLong("facebook_access_expired", CommonActivity.this.facebook.getAccessExpires());
                    editor.commit();
                }

                public void onFacebookError(FacebookError error) {
                }

                public void onError(DialogError e) {
                }

                public void onCancel() {
                }
            });
            return true;
        } else {
            switch (item.getItemId()) {
                case R.id.menu_item_home:
                    if (GallerySaveActivity.task != null && !GallerySaveActivity.task.isCancelled()) {
                        GallerySaveActivity.task.cancel(true);
                        GallerySaveActivity.task = null;
                    }
                    Intent intent2 = new Intent();
                    intent2.setFlags(67108864);
                    intent2.setClass(this, this.HomeActivity);
                    startActivity(intent2);
                    return true;
                case R.id.menu_item_help:
                    Intent intent1 = new Intent();
                    intent1.setClass(this, HelpActivity.class);
                    startActivity(intent1);
                    return true;
                case R.id.menu_item_exit:
                    finish();
                    break;
            }
            return super.onOptionsItemSelected(item);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (this.facebook != null) {
            this.facebook.authorizeCallback(requestCode, resultCode, data);
        }
    }
}
