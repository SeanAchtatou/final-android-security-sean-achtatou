package com.topicshow.android;

import android.app.Activity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.photogrid.android.R;

public class HelpActivity extends Activity {
    private View.OnClickListener btnListener = new View.OnClickListener() {
        public void onClick(View v) {
            HelpActivity.this.finish();
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.help);
        ((Button) findViewById(R.id.btn_back)).setOnClickListener(this.btnListener);
        ((TextView) findViewById(R.id.tv_text)).setMovementMethod(new ScrollingMovementMethod());
        AdView adView = new AdView(this, AdSize.BANNER, getString(R.string.publish_id));
        AdRequest request = new AdRequest();
        ((LinearLayout) findViewById(R.id.google_ads)).addView(adView);
        adView.loadAd(request);
    }
}
