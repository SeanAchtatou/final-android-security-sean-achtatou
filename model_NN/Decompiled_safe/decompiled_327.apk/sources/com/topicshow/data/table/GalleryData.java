package com.topicshow.data.table;

public class GalleryData {
    public static final String CATEGORY = "CATEGORY";
    public static final String DATECREATED = "DATECREATED";
    public static final String DESCRIPTION = "DESCRIPTION";
    public static final String ID = "_ID";
    public static final String PHONEIMAGEADDED = "PHONEIMAGEADDED";
    public static final String PHONEIMAGEID = "PHONEIMAGEID";
    public static final String PRIVACY = "PRIVACY";
    public static final String STATUSID = "STATUSID";
    public static final String TABLE_NAME = "Gallery";
    public static final String TITLE = "TITLE";
    public static final String USERID = "USERID";

    public static String GetCreationString() {
        return "create table Gallery ( _ID integer primary key autoincrement, TITLE text not null, DESCRIPTION text, CATEGORY text not null, PRIVACY text not null, PHONEIMAGEID text not null, DATECREATED text not null, PHONEIMAGEADDED INTEGER not null,USERID INTEGER,STATUSID INTEGER, FOREIGN KEY ( STATUSID) REFERENCES STATUS ( _ID ));";
    }
}
