package com.topicshow.data.table;

import android.content.ContentValues;

public class StatusData {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$topicshow$data$table$StatusData$STATUS_INDEX = null;
    public static final String ID = "_ID";
    public static final String STATUS = "STATUS";
    public static final String TABLE_NAME = "STATUS";

    public enum STATUS_INDEX {
        SUCCESS,
        FAILURE
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$topicshow$data$table$StatusData$STATUS_INDEX() {
        int[] iArr = $SWITCH_TABLE$com$topicshow$data$table$StatusData$STATUS_INDEX;
        if (iArr == null) {
            iArr = new int[STATUS_INDEX.values().length];
            try {
                iArr[STATUS_INDEX.FAILURE.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[STATUS_INDEX.SUCCESS.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            $SWITCH_TABLE$com$topicshow$data$table$StatusData$STATUS_INDEX = iArr;
        }
        return iArr;
    }

    public static String GetCreationString() {
        return "create table STATUS ( _ID integer primary key autoincrement, STATUS text not null)";
    }

    public static ContentValues[] GetDefaultValues() {
        ContentValues[] values = new ContentValues[STATUS_INDEX.values().length];
        values[0] = new ContentValues();
        values[1] = new ContentValues();
        values[0].put("STATUS", GetStatus(STATUS_INDEX.SUCCESS));
        values[1].put("STATUS", GetStatus(STATUS_INDEX.FAILURE));
        return values;
    }

    public static String GetStatus(STATUS_INDEX index) {
        switch ($SWITCH_TABLE$com$topicshow$data$table$StatusData$STATUS_INDEX()[index.ordinal()]) {
            case 1:
                return "SUCCESS";
            case 2:
                return "FAILURE";
            default:
                return "SUCCESS";
        }
    }
}
