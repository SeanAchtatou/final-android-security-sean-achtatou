package com.topicshow.data.table;

public class UserData {
    public static final String DATECREATED = "DATECREATED";
    public static final String FACEBOOKID = "FACEBOOKID";
    public static final String FIRSTNAME = "FIRSTNAME";
    public static final String ID = "_ID";
    public static final String LASTNAME = "LASTNAME";
    public static final String TABLE_NAME = "User";

    public static String GetCreationString() {
        return "create table User ( _ID integer primary key autoincrement, FACEBOOKID text not null, FIRSTNAME text not null, LASTNAME text not null, DATECREATED text not null)";
    }
}
