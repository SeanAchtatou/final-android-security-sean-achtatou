package com.topicshow.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.topicshow.data.table.GalleryData;
import com.topicshow.data.table.StatusData;
import com.topicshow.data.table.UserData;

public class DatabaseHelper extends SQLiteOpenHelper {
    public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(UserData.GetCreationString());
        db.execSQL(StatusData.GetCreationString());
        db.execSQL(GalleryData.GetCreationString());
        ContentValues[] content_values = StatusData.GetDefaultValues();
        for (ContentValues insert : content_values) {
            db.insert("STATUS", null, insert);
        }
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}
