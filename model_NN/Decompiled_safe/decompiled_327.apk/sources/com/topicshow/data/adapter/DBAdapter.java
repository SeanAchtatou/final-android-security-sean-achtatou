package com.topicshow.data.adapter;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import com.topicshow.data.DatabaseHelper;

public class DBAdapter {
    public static final int DATABASE_VERSION = 9;
    public final Context CONTEXT;
    public final String DATABSE_NAME = "TOPICSHOW";
    public DatabaseHelper DBHelper;
    public SQLiteDatabase db;

    public DBAdapter(Context context) {
        this.CONTEXT = context;
        this.DBHelper = new DatabaseHelper(context, "TOPICSHOW", null, 9);
    }

    public DBAdapter open() throws SQLException {
        this.db = this.DBHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        this.db.close();
    }

    public boolean hasOpen() {
        return this.db.isOpen();
    }
}
