package com.topicshow.data.TableFunction;

import android.database.Cursor;
import com.topicshow.data.adapter.DBAdapter;
import com.topicshow.data.table.StatusData;

public class StatusFunc {
    public static int GetStatusID(DBAdapter adapter, StatusData.STATUS_INDEX index) {
        if (!adapter.hasOpen()) {
            adapter.open();
        }
        Cursor cursor = adapter.db.query("STATUS", null, "STATUS = '" + StatusData.GetStatus(index) + "'", null, null, null, null);
        if (cursor.getCount() < 1) {
            return -1;
        }
        int column_index = cursor.getColumnIndex("_ID");
        cursor.moveToFirst();
        return cursor.getInt(column_index);
    }

    public static String GetStatus(DBAdapter adapter, int status_id) {
        if (!adapter.hasOpen()) {
            adapter.open();
        }
        Cursor cursor = adapter.db.query("STATUS", null, "_ID = " + status_id, null, null, null, null);
        if (cursor.getCount() < 1) {
            return null;
        }
        int column_index = cursor.getColumnIndex("STATUS");
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
}
