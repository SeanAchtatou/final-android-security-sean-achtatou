package com.topicshow.data.TableFunction;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import com.topicshow.data.adapter.DBAdapter;
import com.topicshow.data.table.GalleryData;
import java.text.SimpleDateFormat;
import java.util.Date;

public class GalleryFunc {
    /* JADX INFO: Multiple debug info for r14v1 android.database.Cursor: [D('user_id' java.lang.String), D('cursor' android.database.Cursor)] */
    public static long insertGallery(DBAdapter adapter, String title, String description, String category, String privacy, String phone_image_id, long phone_image_added, int status_id, String user_id) throws SQLException {
        if (!adapter.hasOpen()) {
            adapter.open();
        }
        Cursor cursor = UserFunc.FetchUserByFacebookID(adapter, user_id);
        if (cursor.getCount() < 1 || !cursor.moveToFirst()) {
            return -1;
        }
        long value = cursor.getLong(cursor.getColumnIndex("_ID"));
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        ContentValues values = new ContentValues();
        values.put(GalleryData.TITLE, title);
        values.put(GalleryData.DESCRIPTION, description);
        values.put(GalleryData.CATEGORY, category);
        values.put(GalleryData.PRIVACY, privacy);
        values.put(GalleryData.STATUSID, Integer.valueOf(status_id));
        values.put(GalleryData.PHONEIMAGEID, phone_image_id);
        values.put(GalleryData.PHONEIMAGEADDED, Long.valueOf(phone_image_added));
        values.put("DATECREATED", dateFormat.format(date));
        values.put(GalleryData.USERID, Long.valueOf(value));
        return adapter.db.insert(GalleryData.TABLE_NAME, null, values);
    }

    public static Cursor FetchGalleriesByFacebookID(DBAdapter adapter, String facebookID) throws SQLException {
        if (!adapter.hasOpen()) {
            adapter.open();
        }
        Cursor cursor = UserFunc.FetchUserByFacebookID(adapter, facebookID);
        if (cursor.getCount() < 1 || !cursor.moveToFirst()) {
            return null;
        }
        long value = cursor.getLong(cursor.getColumnIndex("_ID"));
        cursor.close();
        return adapter.db.query(GalleryData.TABLE_NAME, null, "USERID=" + value, null, null, null, "DATECREATED DESC");
    }
}
