package com.topicshow.data.TableFunction;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import com.topicshow.data.adapter.DBAdapter;
import com.topicshow.data.table.UserData;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UserFunc {
    public static long insertUser(DBAdapter adapter, String facebookID, String firstname, String lastname) throws SQLException {
        if (!adapter.hasOpen()) {
            adapter.open();
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        ContentValues values = new ContentValues();
        values.put(UserData.FACEBOOKID, facebookID);
        values.put(UserData.FIRSTNAME, firstname);
        values.put(UserData.LASTNAME, lastname);
        values.put("DATECREATED", dateFormat.format(date));
        return adapter.db.insert(UserData.TABLE_NAME, null, values);
    }

    public static Cursor FetchUserByFacebookID(DBAdapter adapter, String facebookID) throws SQLException {
        if (!adapter.hasOpen()) {
            adapter.open();
        }
        return adapter.db.query(UserData.TABLE_NAME, null, "FACEBOOKID = '" + facebookID + "'", null, null, null, null);
    }

    public static Long GetUserIDFromCursor(Cursor cursor, int position) {
        if (cursor.moveToPosition(position)) {
            return Long.valueOf(cursor.getLong(cursor.getColumnIndex("_ID")));
        }
        return -1L;
    }
}
