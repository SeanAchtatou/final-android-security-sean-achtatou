package com.topicshow.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.widget.GridView;
import android.widget.ListAdapter;
import com.topicshow.controls.ThumbnailGridView;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;

public class ThumbnailTaskLoader extends AsyncTask<Cursor, Integer, Cursor> {
    public static final String _FILEHEADER = "PHOTOGALLERY_";
    private ListAdapter adapter;
    public boolean canValidate = true;
    private Context context;
    public ProgressDialog dialog;
    private boolean isForward = true;
    private GridView view;

    public ThumbnailTaskLoader(Context c, ThumbnailGridView v, boolean forward) {
        this.view = v;
        this.context = c;
        this.isForward = forward;
        this.dialog = new ProgressDialog(c);
        this.dialog.setMessage("Loading Photos ...");
        this.dialog.show();
    }

    public void loadAdapter(ListAdapter adapter2) {
        this.adapter = adapter2;
    }

    public void closeDialog() {
        if (this.dialog.isShowing()) {
            this.dialog.dismiss();
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
    }

    /* access modifiers changed from: protected */
    public Cursor doInBackground(Cursor... cursors) {
        Cursor cursor = cursors[0];
        try {
            File app_directory = this.context.getDir("PhotoGallery", 0);
            String[] filenames = app_directory.list();
            if (cursor.moveToFirst()) {
                do {
                    long image_id = cursor.getLong(cursor.getColumnIndex("_id"));
                    Cursor thumbnail_cursor = MediaStore.Images.Thumbnails.queryMiniThumbnail(this.context.getContentResolver(), image_id, 3, new String[]{"image_id"});
                    if (thumbnail_cursor != null && thumbnail_cursor.getCount() <= 0 && !Arrays.asList(filenames).contains(_FILEHEADER + image_id)) {
                        generateInternalFile(app_directory, image_id);
                    }
                    thumbnail_cursor.close();
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
        }
        return cursor;
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(Integer... progress) {
        super.onProgressUpdate((Object[]) progress);
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Cursor cursor) {
        if (cursor.isClosed()) {
            cursor.close();
        }
        try {
            ((ThumbnailGridView) this.view).setAdapter(this.adapter);
            if (this.isForward) {
                ((ThumbnailGridView) this.view).page++;
            } else if (((ThumbnailGridView) this.view).page > 1) {
                ((ThumbnailGridView) this.view).page--;
            }
            if (this.canValidate) {
                this.view.invalidateViews();
            }
        } catch (Exception e) {
        }
        closeDialog();
    }

    private void generateInternalFile(File app_directory, long image_id) throws IOException {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 128;
        Bitmap bitmap = MediaStore.Images.Thumbnails.getThumbnail(this.context.getContentResolver(), image_id, 3, options);
        File newfile = new File(String.valueOf(app_directory.getAbsolutePath()) + File.separator + _FILEHEADER + image_id);
        if (newfile.createNewFile()) {
            FileOutputStream stream = new FileOutputStream(newfile);
            bitmap.compress(Bitmap.CompressFormat.PNG, 10, stream);
            stream.flush();
            stream.close();
        }
        bitmap.recycle();
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        this.dialog.dismiss();
    }
}
