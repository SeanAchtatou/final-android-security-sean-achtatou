package com.topicshow.utils;

import android.graphics.Bitmap;
import android.net.Uri;

public class ImageData {
    private String description = "";
    private String filename;
    private long phone_image_added;
    private long phone_image_id;
    private Integer rotation = 0;
    private boolean server_saved = false;
    private Bitmap thumbnail;
    private String title = "";
    private Uri url;

    public void SetRotation(Integer value) {
        this.rotation = value;
    }

    public void SetTitle(String value) {
        this.title = value;
    }

    public void SetDescription(String value) {
        this.description = value;
    }

    public void SetURI(Uri value) {
        this.url = value;
    }

    public void SetFilename(String value) {
        this.filename = value;
    }

    public void SetImageID(long value) {
        this.phone_image_id = value;
    }

    public void SetImageAdded(long value) {
        this.phone_image_added = value;
    }

    public void SetThumbnail(Bitmap value) {
        this.thumbnail = value;
    }

    public void SetServerSaved(boolean value) {
        this.server_saved = value;
    }

    public Uri getURI() {
        return this.url;
    }

    public Bitmap getThumbnail() {
        return this.thumbnail;
    }

    public long getImageID() {
        return this.phone_image_id;
    }

    public long getImageAdded() {
        return this.phone_image_added;
    }

    public String getFilename() {
        return this.filename;
    }

    public boolean IsServerSaved() {
        return this.server_saved;
    }

    public String getTitle() {
        return this.title;
    }

    public String getDescription() {
        return this.description;
    }

    public Integer getRotation() {
        return this.rotation;
    }
}
