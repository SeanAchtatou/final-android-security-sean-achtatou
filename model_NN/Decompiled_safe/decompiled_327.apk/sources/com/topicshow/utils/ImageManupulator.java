package com.topicshow.utils;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import java.io.InputStream;

public class ImageManupulator {
    /* JADX INFO: Multiple debug info for r5v1 java.io.InputStream: [D('uri' android.net.Uri), D('stream2' java.io.InputStream)] */
    /* JADX INFO: Multiple debug info for r4v4 android.graphics.Bitmap: [D('b' android.graphics.Bitmap), D('resolver' android.content.ContentResolver)] */
    public static Bitmap ImageResizing(ContentResolver resolver, Uri uri, int ResizeWidth, int ResizeHeight) {
        int ResizeHeight2;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        try {
            InputStream stream = resolver.openInputStream(uri);
            BitmapFactory.decodeStream(stream, null, options);
            stream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (options.outHeight > ResizeWidth || options.outWidth > ResizeHeight) {
            ResizeHeight2 = (int) Math.pow(2.0d, (double) ((int) Math.round(Math.log(((double) ResizeWidth) / ((double) Math.max(options.outHeight, options.outWidth))) / Math.log(0.5d))));
        } else {
            ResizeHeight2 = 1;
        }
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = ResizeHeight2;
        o2.inJustDecodeBounds = false;
        try {
            InputStream stream2 = resolver.openInputStream(uri);
            Bitmap b = BitmapFactory.decodeStream(stream2, null, o2);
            stream2.close();
            return b;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
