package com.topicshow.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.View;

public class ProgressDialogThread implements Runnable {
    public Context context = null;
    public ProgressDialog dialog = null;
    public Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            if (msg.what == 0) {
                ProgressDialogThread.this.dialog.dismiss();
            }
        }
    };
    public boolean isRunning = true;
    public String message = "";
    public String title = "";

    public ProgressDialogThread(Context context2, String title2, String message2) {
        this.title = title2;
        this.message = message2;
        this.context = context2;
    }

    public void showDialog() {
        this.dialog = ProgressDialog.show(this.context, this.title, this.message);
        this.dialog.setCancelable(false);
    }

    public void run() {
        do {
        } while (this.isRunning);
        this.handler.sendEmptyMessage(0);
    }

    public void setViewState(final View v, final boolean state) {
        v.getHandler().post(new Runnable() {
            public void run() {
                v.setEnabled(state);
            }
        });
    }

    public void closeDialog() {
        this.isRunning = false;
        this.handler.sendEmptyMessage(0);
    }
}
