package com.topicshow.utils;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import com.photogrid.android.R;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

public class FileUploadTask extends AsyncTask<ArrayList<ImageData>, Integer, Long> {
    private String canvasid = "";
    public boolean hasFailure = false;
    private ArrayList<ImageData> images = null;
    public boolean isCompleted = false;
    private ContentResolver resolver;
    private String target = "";
    private String userid = "";
    private Context view = null;

    /* access modifiers changed from: protected */
    public /* bridge */ /* synthetic */ Object doInBackground(Object... objArr) {
        return doInBackground((ArrayList<ImageData>[]) ((ArrayList[]) objArr));
    }

    public FileUploadTask(Context v, String targetURL, String userid2, String canvasid2) {
        this.view = v;
        this.target = targetURL;
        this.userid = userid2;
        this.canvasid = canvasid2;
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
        this.resolver = this.view.getContentResolver();
    }

    /* access modifiers changed from: protected */
    public Long doInBackground(ArrayList<ImageData>... arg) {
        int length = arg[0].size();
        this.images = arg[0];
        publishProgress(1, Integer.valueOf(length));
        for (int i = 0; i < length; i++) {
            try {
                if (!arg[0].get(i).IsServerSaved()) {
                    arg[0].get(i).SetServerSaved(send(this.target, arg[0].get(i).getURI(), arg[0].get(i).getFilename(), arg[0].get(i).getTitle(), arg[0].get(i).getDescription(), arg[0].get(i).getRotation()));
                }
            } catch (OutOfMemoryError e) {
                e.printStackTrace();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            publishProgress(Integer.valueOf(i + 1), Integer.valueOf(length));
        }
        return Long.valueOf((long) arg[0].size());
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(Integer... progress) {
        super.onProgressUpdate((Object[]) progress);
        ((TextView) ((Activity) this.view).findViewById(R.id.lbl_loadingTitle)).setText("Uploading Photos ...");
        if (this.images != null && progress[0].intValue() >= 0) {
            ((ImageView) ((Activity) this.view).findViewById(R.id.iview_loadingImage)).setImageBitmap(this.images.get(progress[0].intValue() - 1).getThumbnail());
        }
        ((TextView) ((Activity) this.view).findViewById(R.id.lbl_loadingDetail)).setText("Total File Uploaded : " + (progress[0].intValue() - 1) + " out of " + progress[1]);
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Long result) {
        super.onPostExecute((Object) result);
        ((TextView) ((Activity) this.view).findViewById(R.id.lbl_loadingDetail)).setText("Upload Completed");
        for (int i = 0; i < this.images.size(); i++) {
            if (!this.images.get(i).IsServerSaved()) {
                this.hasFailure = true;
            }
        }
        this.isCompleted = true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private boolean send(String targetURL, Uri url, String filename, String title, String description, Integer rotation) throws Exception {
        Exception ex;
        Bitmap temporary = ImageManupulator.ImageResizing(this.resolver, url, 600, 400);
        Matrix mat = new Matrix();
        mat.postRotate((float) rotation.intValue());
        Bitmap newBitmap = Bitmap.createBitmap(temporary, 0, 0, temporary.getWidth(), temporary.getHeight(), mat, true);
        if (newBitmap == null) {
            return false;
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        newBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(targetURL);
        try {
            InputStreamBody body = new InputStreamBody(new ByteArrayInputStream(b), String.valueOf(filename) + ".jpg");
            MultipartEntity multipartEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            multipartEntity.addPart("user-file", body);
            if (!this.userid.equals("") && this.userid != null) {
                multipartEntity.addPart("FacebookUID", new StringBody(this.userid));
            }
            if (!this.canvasid.equals("") && this.canvasid != null) {
                multipartEntity.addPart("canvasID", new StringBody(this.canvasid));
            }
            multipartEntity.addPart("title", new StringBody(title));
            multipartEntity.addPart("description", new StringBody(description));
            httpPost.setEntity(multipartEntity);
            HttpResponse response = defaultHttpClient.execute(httpPost);
            ByteArrayOutputStream outstream = new ByteArrayOutputStream();
            response.getEntity().writeTo(outstream);
            String str = new String(outstream.toByteArray());
            try {
                Log.w("============", String.valueOf(str) + " ========= ");
                temporary.recycle();
                newBitmap.recycle();
                baos.flush();
                baos.close();
                return Boolean.parseBoolean(str);
            } catch (Exception e) {
                ex = e;
                Log.w("============", ex.getMessage());
                return false;
            }
        } catch (Exception e2) {
            ex = e2;
            Log.w("============", ex.getMessage());
            return false;
        }
    }
}
