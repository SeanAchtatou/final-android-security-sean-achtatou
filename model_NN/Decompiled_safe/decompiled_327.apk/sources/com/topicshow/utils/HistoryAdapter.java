package com.topicshow.utils;

import android.content.Context;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.provider.MediaStore;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.photogrid.android.R;
import com.topicshow.data.TableFunction.StatusFunc;
import com.topicshow.data.adapter.DBAdapter;
import com.topicshow.data.table.GalleryData;
import com.topicshow.data.table.StatusData;

public class HistoryAdapter extends BaseAdapter {
    private Context context;
    private Cursor cursor;

    public HistoryAdapter(Context context2, Cursor cursor2) {
        this.context = context2;
        this.cursor = cursor2;
    }

    public Cursor getCursor() {
        return this.cursor;
    }

    public int getCount() {
        return this.cursor.getCount();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        System.gc();
        Cursor phone_cursor = SetupPhoneIfNotExisted();
        LinearLayout linearLayout = new LinearLayout(this.context);
        LinearLayout linearLayout2 = new LinearLayout(this.context);
        linearLayout.setId(1);
        linearLayout2.setId(2);
        linearLayout.setLayoutParams(new AbsListView.LayoutParams(new ViewGroup.LayoutParams(-1, -2)));
        linearLayout.setOrientation(0);
        linearLayout.setPadding(5, 5, 5, 5);
        linearLayout2.setWeightSum(1.0f);
        linearLayout2.setOrientation(1);
        ImageView image = new ImageView(this.context);
        TextView textView = new TextView(this.context);
        TextView textView2 = new TextView(this.context);
        image.setId(3);
        textView.setId(4);
        textView2.setId(5);
        textView.setTextColor(parent.getResources().getColor(R.color.text));
        image.setPadding(parent.getResources().getDimensionPixelSize(R.dimen.thumbnail_medium_padding), parent.getResources().getDimensionPixelSize(R.dimen.thumbnail_medium_padding), parent.getResources().getDimensionPixelSize(R.dimen.thumbnail_medium_padding), parent.getResources().getDimensionPixelSize(R.dimen.thumbnail_medium_padding));
        image.setBackgroundColor(parent.getResources().getColor(R.color.thumbnail_medium_unselected));
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(new ViewGroup.LayoutParams(70, 70));
        layoutParams.setMargins(0, 0, 5, 0);
        image.setLayoutParams(layoutParams);
        linearLayout.addView(image);
        linearLayout.addView(linearLayout2);
        linearLayout2.addView(textView);
        linearLayout2.addView(textView2);
        if (this.cursor.moveToPosition(position)) {
            int title_index = this.cursor.getColumnIndex(GalleryData.TITLE);
            int status_id = this.cursor.getColumnIndex(GalleryData.STATUSID);
            int image_index = this.cursor.getColumnIndex(GalleryData.PHONEIMAGEID);
            int image_added_index = this.cursor.getColumnIndex(GalleryData.PHONEIMAGEADDED);
            long phone_image_id = Long.parseLong(this.cursor.getString(image_index));
            String titleField = this.cursor.getString(title_index);
            int statusID = this.cursor.getInt(status_id);
            if (CheckImageStillExisted(phone_cursor, phone_image_id, this.cursor.getLong(image_added_index))) {
                image.setImageBitmap(MediaStore.Images.Thumbnails.getThumbnail(this.context.getContentResolver(), phone_image_id, 3, new BitmapFactory.Options()));
            } else {
                image.setImageResource(R.drawable.icon);
            }
            DBAdapter adapter = new DBAdapter(this.context);
            adapter.open();
            String statusField = StatusFunc.GetStatus(adapter, statusID);
            adapter.close();
            if (statusField.equals(StatusData.GetStatus(StatusData.STATUS_INDEX.SUCCESS))) {
                textView2.setText("Posted to Facebook");
                textView2.setTextColor(-16711936);
            } else {
                textView2.setText("Not posted to Facebook - Please try again");
                textView2.setTextColor(-65536);
            }
            textView.setText(titleField);
            if (phone_cursor != null && !phone_cursor.isClosed()) {
                phone_cursor.close();
            }
        }
        return linearLayout;
    }

    private Cursor SetupPhoneIfNotExisted() {
        int i = -1;
        String whereClause = "";
        if (this.cursor.moveToFirst()) {
            do {
                long phone_image_id = Long.parseLong(this.cursor.getString(this.cursor.getColumnIndex(GalleryData.PHONEIMAGEID)));
                if (i == -1) {
                    whereClause = String.valueOf(whereClause) + " _id = " + phone_image_id;
                } else {
                    whereClause = String.valueOf(whereClause) + " OR _id = " + phone_image_id;
                }
                i = 1;
            } while (this.cursor.moveToNext());
        }
        if (whereClause == "") {
            return null;
        }
        return this.context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{"_id", "date_added"}, whereClause, null, null);
    }

    private boolean CheckImageStillExisted(Cursor phone_cursor, long id, long dateAdded) {
        if (phone_cursor.moveToFirst()) {
            do {
                long phone_image_id = phone_cursor.getLong(phone_cursor.getColumnIndex("_id"));
                long phone_image_added = phone_cursor.getLong(phone_cursor.getColumnIndex("date_added"));
                if (phone_image_id == id && phone_image_added == dateAdded) {
                    return true;
                }
            } while (phone_cursor.moveToNext());
        }
        return false;
    }
}
