package com.topicshow.utils;

import android.content.Context;
import android.util.Log;
import com.photogrid.android.R;
import java.io.IOException;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

public class MessageWS {
    private String NAMESPACE = "http://tempuri.org/";
    private String URL = "http://photogallery.topicshow.com/Service/ServiceForAndroid.asmx";
    private Context resource;

    public MessageWS(Context resource2) {
        this.resource = resource2;
        Log.w("================*", "access resource");
        this.URL = resource2.getString(R.string.WS_URL);
        this.NAMESPACE = resource2.getString(R.string.WS_NAMESPACE);
    }

    public MessageWS(Context resource2, String namespace, String url) {
        this.resource = resource2;
        this.URL = url;
        this.NAMESPACE = namespace;
    }

    public String createAccount(String userid, String firstname, String lastname) throws IOException, XmlPullParserException, Exception {
        String method = this.resource.getString(R.string.WS_METHOD_CreateAccount);
        String soap_action = String.valueOf(this.NAMESPACE) + method;
        Log.w("================*", this.resource.getString(R.string.WS_METHOD_CreateAccount));
        Log.w("================*", this.NAMESPACE);
        Log.w("================*", this.URL);
        SoapObject request = new SoapObject(this.NAMESPACE, method);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        PropertyInfo pUserID = new PropertyInfo();
        pUserID.setName("facebookUID");
        pUserID.setValue(userid);
        PropertyInfo pFirstname = new PropertyInfo();
        pFirstname.setName("firstname");
        pFirstname.setValue(firstname);
        PropertyInfo pLastname = new PropertyInfo();
        pLastname.setName("lastname");
        pLastname.setValue(lastname);
        request.addProperty(pUserID);
        request.addProperty(pFirstname);
        request.addProperty(pLastname);
        try {
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            new HttpTransportSE(this.URL).call(soap_action, envelope);
            String result = ((SoapPrimitive) envelope.getResponse()).toString();
            Log.w("====================*", "Response From create account " + result);
            return result;
        } catch (Exception e) {
            Exception e2 = e;
            Log.w("=======================*", "AAA" + e2.getMessage());
            e2.printStackTrace();
            return "-1";
        }
    }

    public String createAccount(String userid, String firstname, String lastname, String template) throws IOException, XmlPullParserException, Exception {
        String method = this.resource.getString(R.string.WS_METHOD_CreateAccount);
        String soap_action = String.valueOf(this.NAMESPACE) + method;
        Log.w("================*", this.resource.getString(R.string.WS_METHOD_CreateAccount));
        Log.w("================*", this.NAMESPACE);
        Log.w("================*", this.URL);
        SoapObject request = new SoapObject(this.NAMESPACE, method);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        PropertyInfo pUserID = new PropertyInfo();
        pUserID.setName("facebookUID");
        pUserID.setValue(userid);
        PropertyInfo pFirstname = new PropertyInfo();
        pFirstname.setName("firstname");
        pFirstname.setValue(firstname);
        PropertyInfo pLastname = new PropertyInfo();
        pLastname.setName("lastname");
        pLastname.setValue(lastname);
        PropertyInfo pTemplate = new PropertyInfo();
        pTemplate.setName("template");
        pTemplate.setValue(template);
        request.addProperty(pUserID);
        request.addProperty(pFirstname);
        request.addProperty(pLastname);
        request.addProperty(pTemplate);
        try {
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            new HttpTransportSE(this.URL).call(soap_action, envelope);
            String result = ((SoapPrimitive) envelope.getResponse()).toString();
            Log.w("====================*", "Response From create account " + result);
            return result;
        } catch (Exception e) {
            Exception e2 = e;
            Log.w("=======================*", "AAA" + e2.getMessage());
            e2.printStackTrace();
            return "-1";
        }
    }

    public boolean createGallery(String facebookUID, String canvasID, String Category, String title, String description, String Privacy) throws IOException, XmlPullParserException, Exception {
        String method = this.resource.getString(R.string.WS_METHOD_CreateGallery);
        String soap_action = String.valueOf(this.NAMESPACE) + method;
        SoapObject soapObject = new SoapObject(this.NAMESPACE, method);
        SoapSerializationEnvelope soapSerializationEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        PropertyInfo pFacebookUID = new PropertyInfo();
        pFacebookUID.setName("facebookUID");
        pFacebookUID.setValue(facebookUID);
        PropertyInfo pCanvasID = new PropertyInfo();
        pCanvasID.setName("canvasID");
        pCanvasID.setValue(Integer.valueOf(Integer.parseInt(canvasID)));
        PropertyInfo ptitle = new PropertyInfo();
        ptitle.setName("title");
        ptitle.setValue(title);
        PropertyInfo pCategory = new PropertyInfo();
        pCategory.setName("category");
        pCategory.setValue(Category);
        PropertyInfo pDescription = new PropertyInfo();
        pDescription.setName("description");
        pDescription.setValue(description);
        PropertyInfo pPrivacy = new PropertyInfo();
        pPrivacy.setName("privacy");
        pPrivacy.setValue(Privacy);
        soapObject.addProperty(pFacebookUID);
        soapObject.addProperty(pCanvasID);
        soapObject.addProperty(pCategory);
        soapObject.addProperty(ptitle);
        soapObject.addProperty(pDescription);
        soapObject.addProperty(pPrivacy);
        try {
            soapSerializationEnvelope.dotNet = true;
            soapSerializationEnvelope.setOutputSoapObject(soapObject);
            new HttpTransportSE(this.URL).call(soap_action, soapSerializationEnvelope);
            if (((SoapPrimitive) soapSerializationEnvelope.getResponse()).toString().equals("SUCCESS")) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }
}
