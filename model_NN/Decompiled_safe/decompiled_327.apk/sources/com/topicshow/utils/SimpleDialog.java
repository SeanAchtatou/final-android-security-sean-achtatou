package com.topicshow.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class SimpleDialog {
    public static void showAlert(String title, String message, Context c) {
        AlertDialog dg = new AlertDialog.Builder(c).create();
        dg.setTitle(title);
        dg.setMessage(message);
        dg.setButton(-3, "OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dg.show();
    }

    public static void showWithTwoButton(String title, String message, String Button1, String button2, DialogInterface.OnClickListener listener1, DialogInterface.OnClickListener listener2, Context c) {
        AlertDialog dg = new AlertDialog.Builder(c).create();
        dg.setTitle(title);
        dg.setMessage(message);
        dg.setButton(-2, "Resend", listener1);
        dg.setButton(-3, "OK", listener2);
        dg.show();
    }
}
