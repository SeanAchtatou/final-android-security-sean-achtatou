package com.topicshow.controls;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;
import android.util.TypedValue;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListAdapter;
import com.photogrid.android.R;
import com.topcishow.android.image.ImageCursorAdapter;
import com.topicshow.utils.SimpleDialog;
import com.topicshow.utils.ThumbnailTaskLoader;
import java.util.ArrayList;

public class ThumbnailGridView extends GridView {
    public final int LIMIT = 24;
    public Activity activity = null;
    public int counter = 0;
    public long first;
    public boolean hasPrevious = false;
    public long last;
    public int page = 1;
    public ArrayList<String> saved = null;
    public ThumbnailTaskLoader thumbnail_creator;
    public int total_page = 20;
    public String whereClause = null;

    public ThumbnailGridView(Activity activity2, Context context) {
        super(context);
        this.activity = activity2;
        this.saved = new ArrayList<>();
        super.setColumnWidth((int) TypedValue.applyDimension(1, 70.0f, context.getResources().getDisplayMetrics()));
        super.setVerticalSpacing((int) TypedValue.applyDimension(1, 5.0f, context.getResources().getDisplayMetrics()));
        setNumColumns(-1);
        Cursor image_cursor = this.activity.managedQuery(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{"_id"}, this.whereClause, null, "_id DESC LIMIT " + 24);
        if (this.thumbnail_creator != null && !this.thumbnail_creator.isCancelled()) {
            this.thumbnail_creator.cancel(true);
        }
        this.thumbnail_creator = new ThumbnailTaskLoader(activity2, this, false);
        preAdapter(new ImageCursorAdapter(activity2, image_cursor, this.saved, true));
        loadBoundaryID(image_cursor);
        super.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                Cursor image_cursor = ((ImageCursorAdapter) ThumbnailGridView.this.getAdapter()).getCursor();
                image_cursor.moveToPosition(position);
                long _id = image_cursor.getLong(image_cursor.getColumnIndex("_id"));
                v.setPadding(v.getContext().getResources().getDimensionPixelOffset(R.dimen.thumbnail_medium_padding), v.getContext().getResources().getDimensionPixelOffset(R.dimen.thumbnail_medium_padding), v.getContext().getResources().getDimensionPixelOffset(R.dimen.thumbnail_medium_padding), v.getContext().getResources().getDimensionPixelOffset(R.dimen.thumbnail_medium_padding));
                boolean hasID = false;
                if (ThumbnailGridView.this.saved.contains(String.valueOf(_id))) {
                    hasID = true;
                }
                v.setBackgroundColor(v.getResources().getColor(R.color.thumbnail_medium_unselected));
                if (hasID) {
                    ThumbnailGridView.this.saved.remove(String.valueOf(_id));
                } else if (ThumbnailGridView.this.saved.size() >= 16) {
                    SimpleDialog.showAlert("Warning", "Maximum selected photos reached.", ThumbnailGridView.this.activity);
                } else {
                    ThumbnailGridView.this.saved.add(String.valueOf(_id));
                    v.setBackgroundColor(v.getResources().getColor(R.color.thumbnail_medium_selected));
                }
            }
        });
    }

    public void next() {
        if (hasMore() && this.page < this.total_page) {
            this.hasPrevious = true;
            this.whereClause = "_id < " + this.last;
            ImageCursorAdapter adapter = (ImageCursorAdapter) getAdapter();
            Cursor image_cursor = this.activity.managedQuery(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{"_id"}, this.whereClause, null, "_id DESC LIMIT " + 24);
            adapter.changeCursor(image_cursor);
            loadBoundaryID(image_cursor);
            preAdapter(adapter);
        }
    }

    public void previous() {
        long first2 = 0;
        long last2 = 0;
        Cursor tempCursor = this.activity.managedQuery(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{"_id"}, "_id > " + this.first, null, "_id LIMIT " + String.valueOf(25));
        if (this.hasPrevious) {
            if (tempCursor.getCount() >= 25) {
                if (tempCursor.moveToLast()) {
                    tempCursor.moveToPosition(tempCursor.getPosition() - 1);
                    first2 = tempCursor.getLong(tempCursor.getColumnIndex("_id"));
                    this.hasPrevious = true;
                }
            } else if (tempCursor.moveToLast()) {
                first2 = tempCursor.getLong(tempCursor.getColumnIndex("_id"));
                this.hasPrevious = false;
            }
            if (tempCursor.moveToFirst()) {
                last2 = tempCursor.getLong(tempCursor.getColumnIndex("_id"));
            }
            this.whereClause = "_id <= " + first2 + " AND " + "_id" + " >= " + last2;
            tempCursor.close();
            Cursor image_cursor = this.activity.managedQuery(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{"_id"}, this.whereClause, null, "_id DESC LIMIT " + 24);
            ImageCursorAdapter adapter = (ImageCursorAdapter) getAdapter();
            loadBoundaryID(image_cursor);
            adapter.changeCursor(image_cursor);
            preAdapter(adapter);
        }
    }

    public boolean hasMore() {
        Cursor tempCursor = this.activity.managedQuery(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{"_id"}, "_id < " + this.last, null, "_id DESC LIMIT 1");
        if (tempCursor.getCount() >= 1) {
            tempCursor.close();
            return true;
        }
        tempCursor.close();
        return false;
    }

    private void loadBoundaryID(Cursor image_cursor) {
        if (image_cursor.moveToFirst()) {
            this.first = image_cursor.getLong(image_cursor.getColumnIndex("_id"));
        }
        if (image_cursor.moveToLast()) {
            this.last = image_cursor.getLong(image_cursor.getColumnIndex("_id"));
        }
    }

    public void preAdapter(ListAdapter adapter) {
        Cursor cursor = this.activity.managedQuery(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{"_id"}, this.whereClause, null, "_id DESC LIMIT " + String.valueOf(24));
        this.thumbnail_creator.loadAdapter(adapter);
        this.thumbnail_creator.execute(cursor);
    }

    public void invalidate() {
        super.invalidate();
    }
}
