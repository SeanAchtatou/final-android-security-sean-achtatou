package com.topcishow.io;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.impl.client.DefaultHttpClient;

public class HttpFileUploader {
    public static void send(String targetURL, InputStream stream, String filename, String username, String password) {
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(targetURL);
        try {
            InputStreamBody body = new InputStreamBody(stream, filename);
            MultipartEntity mEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            mEntity.addPart("user-file", body);
            post.setEntity(mEntity);
            client.execute(post);
        } catch (Exception e) {
        }
    }

    /* JADX INFO: Multiple debug info for r5v5 java.lang.StringBuffer: [D('contentType' java.lang.String), D('requestBody' java.lang.StringBuffer)] */
    /* JADX INFO: Multiple debug info for r3v5 java.net.HttpURLConnection: [D('targetURL' java.lang.String), D('conn' java.net.HttpURLConnection)] */
    /* JADX INFO: Multiple debug info for r4v12 int: [D('dataOS' java.io.DataOutputStream), D('responseCode' int)] */
    public static void senda(String targetURL, InputStream stream, String filename, String username, String password) throws Exception {
        Exception ex;
        try {
            String str = "Content-Disposition: form-data; name=userfile; filename=" + filename + ".jpg";
            StringBuffer requestBody = new StringBuffer();
            requestBody.append(10);
            requestBody.append(10);
            requestBody.append("--" + "==============");
            requestBody.append(10);
            requestBody.append("Content-Disposition: form-data; name=test;");
            requestBody.append(10);
            requestBody.append("Content-Type: text/plain");
            requestBody.append(10);
            requestBody.append(10);
            requestBody.append("basdfsdafsadfsad");
            requestBody.append(10);
            requestBody.append(10);
            requestBody.append("--" + "==============" + "--");
            URL url = new URL(targetURL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            try {
                conn.setRequestMethod("POST");
                conn.setRequestProperty("MIME-Version:", "1.0");
                conn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + "==============");
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setUseCaches(false);
                DataOutputStream dataOS = new DataOutputStream(conn.getOutputStream());
                dataOS.writeBytes(requestBody.toString());
                dataOS.flush();
                dataOS.close();
                int responseCode = conn.getResponseCode();
                if (responseCode != 200) {
                    throw new Exception(String.format("Received the response code %d from the URL %s", Integer.valueOf(responseCode), url));
                }
            } catch (Exception e) {
                ex = e;
                throw new Exception(ex.getMessage());
            }
        } catch (Exception e2) {
            ex = e2;
            throw new Exception(ex.getMessage());
        }
    }

    public static byte[] getBytesFromFile(File file) throws IOException {
        int numRead;
        InputStream is = new FileInputStream(file);
        byte[] bytes = new byte[((int) file.length())];
        int offset = 0;
        while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
            offset += numRead;
        }
        if (offset < bytes.length) {
            throw new IOException("Could not completely read file " + file.getName());
        }
        is.close();
        return bytes;
    }

    public static byte[] getByteFromStream(InputStream is) throws Exception {
        try {
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            byte[] data = new byte[16384];
            while (true) {
                int nRead = is.read(data, 0, data.length);
                if (nRead == -1) {
                    buffer.flush();
                    return buffer.toByteArray();
                }
                buffer.write(data, 0, nRead);
            }
        } catch (Exception e) {
            throw new Exception(String.valueOf(e.getMessage()) + "  ");
        }
    }
}
