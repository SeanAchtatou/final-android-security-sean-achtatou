package com.topcishow.android.image;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.photogrid.android.R;
import com.topicshow.utils.ThumbnailTaskLoader;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

public class ImageAdapter extends BaseAdapter {
    private Context context;
    private Cursor cursor;
    private ArrayList<String> saved;

    public ImageAdapter(Context context2, Cursor cursor2, ArrayList<String> saved2) {
        this.context = context2;
        this.cursor = cursor2;
        this.saved = saved2;
    }

    public Cursor getCursor() {
        return this.cursor;
    }

    public int getCount() {
        if (this.cursor == null) {
            return this.saved.size();
        }
        return this.cursor.getCount();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (this.cursor == null) {
            return getViewByArray(position, convertView, parent);
        }
        System.gc();
        if (!this.cursor.moveToPosition(position)) {
            return convertView;
        }
        ImageView iview = new ImageView(this.context);
        long id = this.cursor.getLong(this.cursor.getColumnIndex("_id"));
        Uri uri = getThumbnailIfExist(id);
        if (uri != null) {
            iview.setImageURI(uri);
        }
        iview.setScaleType(ImageView.ScaleType.CENTER_CROP);
        iview.setLayoutParams(new AbsListView.LayoutParams(iview.getContext().getResources().getDimensionPixelOffset(R.dimen.thumbnail_medium_size), iview.getContext().getResources().getDimensionPixelOffset(R.dimen.thumbnail_medium_size)));
        iview.setBackgroundColor(iview.getContext().getResources().getColor(R.color.thumbnail_medium_unselected));
        if (this.saved.contains(String.valueOf(id))) {
            iview.setBackgroundColor(iview.getContext().getResources().getColor(R.color.thumbnail_medium_selected));
        }
        iview.setPadding(iview.getContext().getResources().getDimensionPixelOffset(R.dimen.thumbnail_medium_padding), iview.getContext().getResources().getDimensionPixelOffset(R.dimen.thumbnail_medium_padding), iview.getContext().getResources().getDimensionPixelOffset(R.dimen.thumbnail_medium_padding), iview.getContext().getResources().getDimensionPixelOffset(R.dimen.thumbnail_medium_padding));
        return iview;
    }

    private ImageView getViewByArray(int position, View convertView, ViewGroup parent) {
        String image_id = this.saved.get(position);
        ImageView iview = new ImageView(this.context);
        Uri uri = getThumbnailIfExist(Long.parseLong(image_id));
        if (uri != null) {
            iview.setImageURI(uri);
            iview.setTag(Long.valueOf(image_id));
        }
        iview.setScaleType(ImageView.ScaleType.CENTER_CROP);
        iview.setBackgroundColor(iview.getContext().getResources().getColor(R.color.thumbnail_medium_unselected));
        iview.setPadding(iview.getContext().getResources().getDimensionPixelOffset(R.dimen.thumbnail_medium_padding), iview.getContext().getResources().getDimensionPixelOffset(R.dimen.thumbnail_medium_padding), iview.getContext().getResources().getDimensionPixelOffset(R.dimen.thumbnail_medium_padding), iview.getContext().getResources().getDimensionPixelOffset(R.dimen.thumbnail_medium_padding));
        return iview;
    }

    private Uri getThumbnailIfExist(long image_id) {
        Cursor thumbnail_cursor = MediaStore.Images.Thumbnails.queryMiniThumbnail(this.context.getContentResolver(), image_id, 3, new String[]{"_id"});
        File directory = this.context.getDir("PhotoGallery", 0);
        String[] filenames = directory.list();
        if (thumbnail_cursor == null || thumbnail_cursor.getCount() <= 0) {
            if (Arrays.asList(filenames).contains(ThumbnailTaskLoader._FILEHEADER + image_id)) {
                thumbnail_cursor.close();
                return Uri.parse(String.valueOf(directory.getAbsolutePath()) + File.separator + ThumbnailTaskLoader._FILEHEADER + image_id);
            }
        } else if (thumbnail_cursor.moveToFirst()) {
            String str_uri = thumbnail_cursor.getString(thumbnail_cursor.getColumnIndex("_id"));
            thumbnail_cursor.close();
            return Uri.parse(str_uri);
        }
        thumbnail_cursor.close();
        return null;
    }
}
