package com.topcishow.android.image;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import com.photogrid.android.R;
import com.topicshow.utils.ThumbnailTaskLoader;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

public class ImageCursorAdapter extends CursorAdapter {
    private Context context;
    private ArrayList<String> saved;

    public ImageCursorAdapter(Context context2, Cursor c, ArrayList<String> saved2, Boolean autoRequery) {
        super(context2, c, autoRequery.booleanValue());
        this.context = context2;
        this.saved = saved2;
    }

    public void bindView(View view, Context context2, Cursor cursor) {
        ImageView iview = (ImageView) view;
        long id = cursor.getLong(cursor.getColumnIndex("_id"));
        Uri uri = getThumbnailIfExist(id);
        if (uri != null) {
            iview.setImageURI(uri);
        }
        iview.setScaleType(ImageView.ScaleType.CENTER_CROP);
        iview.setLayoutParams(new AbsListView.LayoutParams(iview.getContext().getResources().getDimensionPixelOffset(R.dimen.thumbnail_medium_size), iview.getContext().getResources().getDimensionPixelOffset(R.dimen.thumbnail_medium_size)));
        iview.setBackgroundColor(iview.getContext().getResources().getColor(R.color.thumbnail_medium_unselected));
        if (this.saved.contains(String.valueOf(id))) {
            iview.setBackgroundColor(iview.getContext().getResources().getColor(R.color.thumbnail_medium_selected));
        }
        iview.setPadding(iview.getContext().getResources().getDimensionPixelOffset(R.dimen.thumbnail_medium_padding), iview.getContext().getResources().getDimensionPixelOffset(R.dimen.thumbnail_medium_padding), iview.getContext().getResources().getDimensionPixelOffset(R.dimen.thumbnail_medium_padding), iview.getContext().getResources().getDimensionPixelOffset(R.dimen.thumbnail_medium_padding));
    }

    public View newView(Context context2, Cursor cursor, ViewGroup parent) {
        ImageView iview = new ImageView(context2);
        long id = cursor.getLong(cursor.getColumnIndex("_id"));
        Uri uri = getThumbnailIfExist(id);
        if (uri != null) {
            iview.setImageURI(uri);
        }
        iview.setScaleType(ImageView.ScaleType.CENTER_CROP);
        iview.setLayoutParams(new AbsListView.LayoutParams(iview.getContext().getResources().getDimensionPixelOffset(R.dimen.thumbnail_medium_size), iview.getContext().getResources().getDimensionPixelOffset(R.dimen.thumbnail_medium_size)));
        iview.setBackgroundColor(iview.getContext().getResources().getColor(R.color.thumbnail_medium_unselected));
        if (this.saved.contains(String.valueOf(id))) {
            iview.setBackgroundColor(iview.getContext().getResources().getColor(R.color.thumbnail_medium_selected));
        }
        iview.setPadding(iview.getContext().getResources().getDimensionPixelOffset(R.dimen.thumbnail_medium_padding), iview.getContext().getResources().getDimensionPixelOffset(R.dimen.thumbnail_medium_padding), iview.getContext().getResources().getDimensionPixelOffset(R.dimen.thumbnail_medium_padding), iview.getContext().getResources().getDimensionPixelOffset(R.dimen.thumbnail_medium_padding));
        return iview;
    }

    private Uri getThumbnailIfExist(long image_id) {
        Cursor thumbnail_cursor = MediaStore.Images.Thumbnails.queryMiniThumbnail(this.context.getContentResolver(), image_id, 3, new String[]{"_id"});
        File directory = this.context.getDir("PhotoGallery", 0);
        String[] filenames = directory.list();
        if (thumbnail_cursor == null || thumbnail_cursor.getCount() <= 0) {
            if (Arrays.asList(filenames).contains(ThumbnailTaskLoader._FILEHEADER + image_id)) {
                thumbnail_cursor.close();
                return Uri.parse(String.valueOf(directory.getAbsolutePath()) + File.separator + ThumbnailTaskLoader._FILEHEADER + image_id);
            }
        } else if (thumbnail_cursor.moveToFirst()) {
            String str_uri = thumbnail_cursor.getString(thumbnail_cursor.getColumnIndex("_id"));
            thumbnail_cursor.close();
            return Uri.parse(str_uri);
        }
        thumbnail_cursor.close();
        return null;
    }
}
