package com.topcishow.android.image;

import android.content.Context;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.provider.MediaStore;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.photogrid.android.R;

public class ThumbnailAdapter extends BaseAdapter {
    private Context context;
    private Cursor cursor;

    public ThumbnailAdapter(Context context2, Cursor cursor2) {
        this.context = context2;
        this.cursor = cursor2;
    }

    public Cursor getCursor() {
        return this.cursor;
    }

    public int getCount() {
        return this.cursor.getCount();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        System.gc();
        if (convertView != null) {
            return convertView;
        }
        if (!this.cursor.moveToPosition(position)) {
            return null;
        }
        ImageView iview = new ImageView(this.context);
        int i = this.cursor.getInt(this.cursor.getColumnIndexOrThrow("_id"));
        iview.setImageBitmap(MediaStore.Images.Thumbnails.getThumbnail(this.context.getContentResolver(), this.cursor.getLong(this.cursor.getColumnIndex("image_id")), 3, new BitmapFactory.Options()));
        iview.setScaleType(ImageView.ScaleType.CENTER_CROP);
        iview.setLayoutParams(new AbsListView.LayoutParams(iview.getContext().getResources().getDimensionPixelOffset(R.dimen.thumbnail_medium_size), iview.getContext().getResources().getDimensionPixelOffset(R.dimen.thumbnail_medium_size)));
        iview.setBackgroundColor(iview.getContext().getResources().getColor(R.color.thumbnail_medium_unselected));
        iview.setPadding(iview.getContext().getResources().getDimensionPixelOffset(R.dimen.thumbnail_medium_padding), iview.getContext().getResources().getDimensionPixelOffset(R.dimen.thumbnail_medium_padding), iview.getContext().getResources().getDimensionPixelOffset(R.dimen.thumbnail_medium_padding), iview.getContext().getResources().getDimensionPixelOffset(R.dimen.thumbnail_medium_padding));
        return iview;
    }
}
