package com.facebook.android;

public final class R {

    public static final class array {
        public static final int image_category = 2131165184;
    }

    public static final class attr {
    }

    public static final class color {
        public static final int clicked = 2130968580;
        public static final int main_background = 2130968578;
        public static final int main_label = 2130968579;
        public static final int selector = 2130968583;
        public static final int text = 2130968582;
        public static final int thumbnail_medium_selected = 2130968576;
        public static final int thumbnail_medium_unselected = 2130968577;
        public static final int unclicked = 2130968581;
    }

    public static final class dimen {
        public static final int thumbnail_medium_padding = 2131034112;
        public static final int thumbnail_medium_size = 2131034113;
    }

    public static final class drawable {
        public static final int anticlockwise = 2130837504;
        public static final int button = 2130837505;
        public static final int clockwise = 2130837506;
        public static final int facebook_icon = 2130837507;
        public static final int helpimage = 2130837508;
        public static final int homeicon = 2130837509;
        public static final int homeimage = 2130837510;
        public static final int homelogo = 2130837511;
        public static final int icon = 2130837512;
        public static final int nextbutton = 2130837513;
        public static final int no1 = 2130837514;
        public static final int no2 = 2130837515;
        public static final int powerby = 2130837516;
        public static final int previousbutton = 2130837517;
    }

    public static final class id {
        public static final int btnCreate = 2131361795;
        public static final int btnPreview = 2131361805;
        public static final int btn_back = 2131361826;
        public static final int btn_category = 2131361815;
        public static final int btn_next = 2131361804;
        public static final int btn_previous = 2131361803;
        public static final int btnview_created = 2131361831;
        public static final int category = 2131361823;
        public static final int gallery1 = 2131361833;
        public static final int google_ads = 2131361794;
        public static final int image = 2131361842;
        public static final int imageView1 = 2131361824;
        public static final int img_gallery_item_image = 2131361797;
        public static final int iview_homeimage = 2131361829;
        public static final int iview_loadingImage = 2131361808;
        public static final int iview_logo1 = 2131361827;
        public static final int iview_logo2 = 2131361828;
        public static final int lbl_Description = 2131361816;
        public static final int lbl_category = 2131361814;
        public static final int lbl_cw = 2131361838;
        public static final int lbl_gallery_item_status = 2131361800;
        public static final int lbl_gallery_item_title = 2131361799;
        public static final int lbl_loadingDetail = 2131361810;
        public static final int lbl_loadingTitle = 2131361809;
        public static final int lbl_privacy = 2131361818;
        public static final int lbl_textlabel = 2131361830;
        public static final int linearLayout1 = 2131361796;
        public static final int linearLayout2 = 2131361798;
        public static final int linearLayout3 = 2131361835;
        public static final int linearLayout5 = 2131361837;
        public static final int linearLayout8 = 2131361841;
        public static final int list_gallery_item = 2131361793;
        public static final int menu_item_exit = 2131361846;
        public static final int menu_item_help = 2131361845;
        public static final int menu_item_home = 2131361844;
        public static final int menu_item_login = 2131361843;
        public static final int nextbutton = 2131361834;
        public static final int previousbutton = 2131361832;
        public static final int privacy_everyone = 2131361820;
        public static final int privacy_friend = 2131361821;
        public static final int privacy_onlyme = 2131361822;
        public static final int radioGroup1 = 2131361819;
        public static final int rotateCCW = 2131361840;
        public static final int rotateCW = 2131361839;
        public static final int tableLayout1 = 2131361806;
        public static final int tableLayout2 = 2131361811;
        public static final int tableRow1 = 2131361812;
        public static final int tableRow3 = 2131361836;
        public static final int tbl_progress = 2131361807;
        public static final int textView1 = 2131361792;
        public static final int thumbnail_grid = 2131361802;
        public static final int tv_text = 2131361825;
        public static final int txt_description = 2131361817;
        public static final int txt_title = 2131361813;
        public static final int vstub_panel = 2131361801;
    }

    public static final class layout {
        public static final int dropdownlayout = 2130903040;
        public static final int galleryhistorylayout = 2130903041;
        public static final int galleryitemlayout = 2130903042;
        public static final int gallerylayout = 2130903043;
        public static final int gallerysavelayout = 2130903044;
        public static final int help = 2130903045;
        public static final int loading_dialog = 2130903046;
        public static final int main = 2130903047;
        public static final int photoeditlayout = 2130903048;
    }

    public static final class menu {
        public static final int common_menu = 2131296256;
        public static final int home_menu = 2131296257;
    }

    public static final class string {
        public static final int FORM_IMAGE_UPLOAD = 2131099655;
        public static final int FacebookAppID = 2131099650;
        public static final int INTENT_Category = 2131099656;
        public static final int WS_METHOD_CreateAccount = 2131099652;
        public static final int WS_METHOD_CreateGallery = 2131099653;
        public static final int WS_NAMESPACE = 2131099654;
        public static final int WS_URL = 2131099651;
        public static final int abouttitle = 2131099660;
        public static final int app_name = 2131099649;
        public static final int default_title_1 = 2131099648;
        public static final int hello = 2131099664;
        public static final int help = 2131099663;
        public static final int historytitle = 2131099661;
        public static final int hometext = 2131099658;
        public static final int mainpage_btn_1 = 2131099659;
        public static final int publish_id = 2131099662;
        public static final int select_category = 2131099657;
    }

    public static final class style {
        public static final int Buttons = 2131230720;
        public static final int StandardSpinnerDropDownItem = 2131230722;
        public static final int application_style = 2131230721;
        public static final int background_style = 2131230723;
        public static final int form_edit = 2131230729;
        public static final int form_label = 2131230728;
        public static final int label = 2131230730;
        public static final int label_panel = 2131230726;
        public static final int label_panel_heading = 2131230727;
        public static final int thumbnail_medium = 2131230724;
        public static final int thumbnail_small = 2131230725;
    }
}
