package net.wessendorf.j2me;

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.TextBox;
import javax.microedition.lcdui.TextField;
import javax.microedition.midlet.MIDlet;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransport;

public class SoapDemo extends MIDlet implements CommandListener {
    private Display display;
    Command getCommand = new Command("send", 1, 1);
    Form mainForm = new Form("Hello World WebService");
    TextField nameField = new TextField("Your name", "", 456, 0);

    public SoapDemo() {
        this.mainForm.append(this.nameField);
        this.mainForm.addCommand(this.getCommand);
        this.mainForm.setCommandListener(this);
    }

    public void startApp() {
        this.display = Display.getDisplay(this);
        this.display.setCurrent(this.mainForm);
    }

    public void pauseApp() {
    }

    public void destroyApp(boolean unconditional) {
    }

    public void commandAction(Command c, Displayable s) {
        if (c == this.getCommand) {
            final TextBox t = new TextBox("", "", 256, 0);
            new Thread() {
                public void run() {
                    try {
                        SoapObject client = new SoapObject("", "getObject");
                        client.addProperty("name", SoapDemo.this.nameField.getString());
                        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                        envelope.bodyOut = client;
                        new HttpTransport("http://localhost:8080/axis/services/AxisService").call("", envelope);
                        new SoapObject("http://ws.wessendorf.net", "CustomObject");
                        t.setString(((SoapObject) envelope.getResponse()).getProperty(0).toString());
                    } catch (SoapFault e) {
                        SoapFault sf = e;
                        t.setString("Code: " + sf.faultcode + "\nString: " + sf.faultstring);
                    } catch (Exception e2) {
                        Exception e3 = e2;
                        e3.printStackTrace();
                        t.setString(e3.getMessage());
                    }
                }
            }.start();
            this.display.setCurrent(t);
            return;
        }
        destroyApp(false);
        notifyDestroyed();
    }
}
