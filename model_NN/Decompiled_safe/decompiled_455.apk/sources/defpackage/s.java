package defpackage;

import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.ads.AdActivity;
import com.google.ads.util.AdUtil;
import com.google.ads.util.d;
import java.util.HashMap;
import java.util.Map;

/* renamed from: s  reason: default package */
public final class s extends WebViewClient {
    private h a;
    private Map b;
    private boolean c;
    private boolean d;
    private boolean e = false;
    private boolean f = false;

    public s(h hVar, Map map, boolean z, boolean z2) {
        this.a = hVar;
        this.b = map;
        this.c = z;
        this.d = z2;
    }

    public final void a() {
        this.e = true;
    }

    public final void b() {
        this.f = true;
    }

    public final void onPageFinished(WebView webView, String str) {
        if (this.e) {
            k f2 = this.a.f();
            if (f2 != null) {
                f2.a();
            } else {
                d.a("adLoader was null while trying to setFinishedLoadingHtml().");
            }
            this.e = false;
        }
        if (this.f) {
            l.a(webView);
            this.f = false;
        }
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        d.a("shouldOverrideUrlLoading(\"" + str + "\")");
        Uri parse = Uri.parse(str);
        HashMap b2 = AdUtil.b(parse);
        if (b2 == null) {
            d.e("An error occurred while parsing the url parameters.");
            return true;
        }
        String str2 = (String) b2.get("ai");
        if (str2 != null) {
            this.a.k().a(str2);
        }
        if (l.a(parse)) {
            l.a(this.a, this.b, parse, webView);
            return true;
        } else if (this.d) {
            if (AdUtil.a(parse)) {
                return super.shouldOverrideUrlLoading(webView, str);
            }
            HashMap hashMap = new HashMap();
            hashMap.put("u", str);
            AdActivity.a(this.a, new i("intent", hashMap));
            return true;
        } else if (this.c) {
            String str3 = (!this.a.u() || !AdUtil.a(parse)) ? "intent" : "webapp";
            HashMap hashMap2 = new HashMap();
            hashMap2.put("u", parse.toString());
            AdActivity.a(this.a, new i(str3, hashMap2));
            return true;
        } else {
            d.e("URL is not a GMSG and can't handle URL: " + str);
            return true;
        }
    }
}
