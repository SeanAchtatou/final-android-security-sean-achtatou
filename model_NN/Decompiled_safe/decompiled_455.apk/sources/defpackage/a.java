package defpackage;

import android.webkit.WebView;
import com.google.ads.g;

/* renamed from: a  reason: default package */
final class a implements Runnable {
    final /* synthetic */ k a;
    private final h b;
    private final WebView c;
    private final j d;
    private final g e;
    private final boolean f;

    public a(k kVar, h hVar, WebView webView, j jVar, g gVar, boolean z) {
        this.a = kVar;
        this.b = hVar;
        this.c = webView;
        this.d = jVar;
        this.e = gVar;
        this.f = z;
    }

    public final void run() {
        this.c.stopLoading();
        this.c.destroy();
        this.d.a();
        if (this.f) {
            g h = this.b.h();
            h.stopLoading();
            h.setVisibility(8);
        }
        this.b.a(this.e);
    }
}
