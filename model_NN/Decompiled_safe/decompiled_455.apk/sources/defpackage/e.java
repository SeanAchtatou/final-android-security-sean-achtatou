package defpackage;

import java.util.LinkedList;

/* renamed from: e  reason: default package */
final class e implements Runnable {
    final /* synthetic */ k a;
    private final h b;
    private final LinkedList c;
    private final int d;

    public e(k kVar, h hVar, LinkedList linkedList, int i) {
        this.a = kVar;
        this.b = hVar;
        this.c = linkedList;
        this.d = i;
    }

    public final void run() {
        this.b.a(this.c);
        this.b.a(this.d);
        this.b.o();
    }
}
