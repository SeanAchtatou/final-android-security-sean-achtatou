package defpackage;

import android.net.Uri;
import android.webkit.WebView;
import com.google.ads.util.d;
import java.util.HashMap;
import java.util.Locale;

/* renamed from: r  reason: default package */
public final class r implements t {
    public final void a(h hVar, HashMap hashMap, WebView webView) {
        Uri parse;
        String host;
        String str;
        String str2 = (String) hashMap.get("u");
        if (str2 == null) {
            d.e("Could not get URL from click gmsg.");
            return;
        }
        f k = hVar.k();
        if (!(k == null || (host = (parse = Uri.parse(str2)).getHost()) == null || !host.toLowerCase(Locale.US).endsWith(".admob.com"))) {
            String path = parse.getPath();
            if (path != null) {
                String[] split = path.split("/");
                if (split.length >= 4) {
                    str = split[2] + "/" + split[3];
                    k.b(str);
                }
            }
            str = null;
            k.b(str);
        }
        new Thread(new u(str2, webView.getContext().getApplicationContext())).start();
    }
}
