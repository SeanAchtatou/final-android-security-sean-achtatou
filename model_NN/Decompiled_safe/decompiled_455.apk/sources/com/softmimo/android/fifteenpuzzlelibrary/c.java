package com.softmimo.android.fifteenpuzzlelibrary;

import android.content.SharedPreferences;
import android.view.View;
import android.widget.Button;
import com.softmimo.android.fifteenpuzzlefreeversion.R;

class c implements View.OnClickListener {
    final /* synthetic */ FifteenPuzzlePreferences a;

    c(FifteenPuzzlePreferences fifteenPuzzlePreferences) {
        this.a = fifteenPuzzlePreferences;
    }

    public void onClick(View view) {
        SharedPreferences.Editor edit = this.a.getSharedPreferences("FifteenPuzzlePrefsFile", 0).edit();
        edit.clear();
        edit.commit();
        ((Button) this.a.findViewById(R.id.clearrecord)).setEnabled(false);
    }
}
