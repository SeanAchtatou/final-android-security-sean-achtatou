package com.softmimo.android.fifteenpuzzlelibrary;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import com.softmimo.android.fifteenpuzzlefreeversion.R;

public class FifteenPuzzleView extends View {
    public static int A = R.drawable.forteen;
    public static int B = R.drawable.fifteen;
    public static int C;
    public static boolean E = false;
    public static boolean F = false;
    public static int G = 480;
    public static int H = 854;
    public static float I = 1.5f;
    public static String J = "      ";
    public static boolean b = true;
    public static boolean c = true;
    public static int n = R.drawable.one;
    public static int o = R.drawable.two;
    public static int p = R.drawable.three;
    public static int q = R.drawable.four;
    public static int r = R.drawable.five;
    public static int s = R.drawable.six;
    public static int t = R.drawable.seven;
    public static int u = R.drawable.eight;
    public static int v = R.drawable.nine;
    public static int w = R.drawable.ten;
    public static int x = R.drawable.eleven;
    public static int y = R.drawable.twelve;
    public static int z = R.drawable.thirteen;
    int D = 0;
    private Context K;
    private Bitmap L;
    private Canvas M;
    /* access modifiers changed from: private */
    public h N;
    private Button O;
    /* access modifiers changed from: private */
    public Button P;
    /* access modifiers changed from: private */
    public boolean Q = false;
    /* access modifiers changed from: private */
    public int R = 0;
    /* access modifiers changed from: private */
    public Handler S = new Handler();
    /* access modifiers changed from: private */
    public long T = 0;
    /* access modifiers changed from: private */
    public int U = 0;
    private int V = 5999;
    private boolean W = false;
    private Runnable X = new g(this);
    Rect a = new Rect();
    int d = 40;
    int e = 10;
    int f;
    int g;
    int h;
    int i;
    int j = 5;
    int k = 5;
    int l = 100;
    int m = 150;

    public FifteenPuzzleView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.K = context;
        a();
        i();
        this.N = new h(1, this.k - 1, this.j - 1, this.D);
        setBackgroundColor(Color.rgb(4, 92, 116));
        b();
    }

    /* access modifiers changed from: private */
    public void i() {
        this.T = SystemClock.uptimeMillis();
        this.U = 0;
        this.S.removeCallbacks(this.X);
        this.S.postDelayed(this.X, 100);
    }

    private void j() {
        this.S.removeCallbacks(this.X);
        f();
        g();
        a(false);
    }

    public int a(int i2) {
        for (int i3 = 0; i3 < this.k - 1; i3++) {
            if (i2 >= this.d + (this.l * i3) && i2 <= this.d + ((i3 + 1) * this.l)) {
                return i3;
            }
        }
        return -1;
    }

    public void a() {
        int i2 = !E ? (int) (150.0f * I) : (int) (210.0d * ((double) I));
        int i3 = G <= H ? G : H;
        int i4 = G <= H ? H : G;
        this.l = (int) (((double) i3) / 4.5d);
        this.m = (int) (((double) (i4 - i2)) / 4.5d);
        this.d = this.l / 4;
        this.e = this.m / 6;
    }

    public void a(int i2, int i3, int i4) {
        int i5 = (this.l * i2) + this.d + (this.l / 2);
        int i6 = (this.m * i3) + this.e + (this.m / 2);
        Paint paint = new Paint();
        int min = (Math.min(this.l, this.m) / 2) - 2;
        if (i4 > 0 && i4 < 16) {
            paint.setColor(-16777216);
            this.M.drawText(Integer.toString(i4), (float) i5, (float) i6, paint);
            int i7 = i4 == 1 ? n : i4 == 2 ? o : i4 == 3 ? p : i4 == 4 ? q : 0;
            switch (i4) {
                case 1:
                    i7 = n;
                    break;
                case 2:
                    i7 = o;
                    break;
                case 3:
                    i7 = p;
                    break;
                case 4:
                    i7 = q;
                    break;
                case 5:
                    i7 = r;
                    break;
                case 6:
                    i7 = s;
                    break;
                case 7:
                    i7 = t;
                    break;
                case 8:
                    i7 = u;
                    break;
                case 9:
                    i7 = v;
                    break;
                case 10:
                    i7 = w;
                    break;
                case 11:
                    i7 = x;
                    break;
                case 12:
                    i7 = y;
                    break;
                case 13:
                    i7 = z;
                    break;
                case 14:
                    i7 = A;
                    break;
                case 15:
                    i7 = B;
                    break;
            }
            Drawable drawable = this.K.getResources().getDrawable(i7);
            drawable.setBounds((i5 - min) - 2, (i6 - min) - 2, i5 + min + 2, min + i6 + 2);
            drawable.draw(this.M);
        }
        this.a.set(i5, i6, i5 + 60, i6 + 60);
        invalidate(this.a);
    }

    public void a(int i2, int i3, int i4, int i5) {
        try {
            Paint paint = new Paint();
            paint.setColor(-65536);
            this.M.drawLine((float) this.f, (float) this.g, (float) (this.f + ((this.l * 1) / 5)), (float) this.g, paint);
            int i6 = 5 - 1;
            this.M.drawLine((float) (this.f + ((this.l * 4) / 5)), (float) this.g, (float) (this.f + this.l), (float) this.g, paint);
            this.M.drawLine((float) this.f, (float) this.g, (float) this.f, (float) (this.g + ((this.m * 1) / 5)), paint);
            int i7 = 5 - 1;
            this.M.drawLine((float) this.f, (float) (this.g + ((this.m * 4) / 5)), (float) this.f, (float) (this.g + this.m), paint);
            this.M.drawLine((float) (this.f + this.l), (float) this.g, (float) (this.f + this.l), (float) (this.g + ((this.m * 1) / 5)), paint);
            int i8 = 5 - 1;
            this.M.drawLine((float) (this.f + this.l), (float) (this.g + ((this.m * 4) / 5)), (float) (this.f + this.l), (float) (this.g + this.m), paint);
            this.M.drawLine((float) this.f, (float) (this.g + this.m), (float) (this.f + ((this.l * 1) / 5)), (float) (this.g + this.m), paint);
            int i9 = 5 - 1;
            this.M.drawLine((float) (this.f + ((this.l * 4) / 5)), (float) (this.g + this.m), (float) (this.f + this.l), (float) (this.g + this.m), paint);
            this.a.set(this.f - 5, this.g - 5, this.f + this.l + 5, this.g + this.m + 5);
            invalidate(this.a);
        } catch (Exception e2) {
            Log.v("mov", "moveFoucs Error:" + e2);
        }
    }

    public void a(int i2, int i3, int i4, Button button, Button button2) {
        C = i2;
        if (C == 0) {
            C = 9999;
        }
        this.V = i3;
        if (this.V == 0) {
            this.V = 5999;
        }
        this.O = button;
        this.P = button2;
        this.R = i4;
        this.N.a(this.R);
        a(true);
        c();
    }

    public void a(boolean z2) {
        if (z2) {
            try {
                h();
            } catch (Exception e2) {
                return;
            }
        }
        this.P.setTextColor(-16777216);
        this.P.setOnClickListener(new f(this));
    }

    public int b(int i2) {
        for (int i3 = 0; i3 < this.j - 1; i3++) {
            if (i2 > this.e + (this.m * i3) && i2 < this.e + ((i3 + 1) * this.m)) {
                return i3;
            }
        }
        return -1;
    }

    public void b() {
        Bitmap createBitmap = Bitmap.createBitmap((this.l * (this.k - 1)) + (this.d * 2), (this.m * (this.j - 1)) + (this.e * 2), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        if (this.L != null) {
            canvas.drawBitmap(this.L, 0.0f, 0.0f, (Paint) null);
        }
        this.L = createBitmap;
        this.M = canvas;
        Paint paint = new Paint();
        Rect rect = new Rect(this.d + 1, this.e + 1, this.d + ((this.k - 1) * this.l), this.e + ((this.j - 1) * this.m));
        paint.setColor(-1);
        this.M.drawRect(rect, paint);
        for (int i2 = 0; i2 < this.j - 1; i2++) {
            for (int i3 = 0; i3 < this.k - 1; i3++) {
                a(i3, i2, this.N.a()[i3][i2]);
            }
        }
        paint.setColor(-16777216);
        for (int i4 = 1; i4 <= this.j; i4++) {
            this.M.drawLine((float) this.d, (float) (((i4 - 1) * this.m) + this.e), (float) ((this.l * (this.k - 1)) + this.d), (float) (((i4 - 1) * this.m) + this.e), paint);
        }
        for (int i5 = 1; i5 <= this.k; i5++) {
            this.M.drawLine((float) (((i5 - 1) * this.l) + this.d), (float) this.e, (float) (((i5 - 1) * this.l) + this.d), (float) ((this.m * (this.j - 1)) + this.e), paint);
        }
        if (this.j % 2 == 0) {
            this.f = this.d + (this.l * ((this.k / 2) - 1));
            this.h = this.k / 2;
        } else {
            this.f = (this.d + ((this.l * (this.k - 1)) / 2)) - (this.l / 2);
            this.h = (this.k + 1) / 2;
        }
        if (this.k % 2 == 0) {
            this.g = this.e + (this.m * ((this.j / 2) - 1));
            this.i = this.j / 2;
        } else {
            this.g = (this.e + ((this.m * (this.j - 1)) / 2)) - (this.m / 2);
            this.i = (this.j + 1) / 2;
        }
        a(this.f - (this.l / 2), (this.g - (this.m / 2)) - 1, this.l * 3, this.m * 2);
    }

    public void c() {
        invalidate();
        Paint paint = new Paint();
        paint.setColor(Color.rgb(4, 92, 116));
        this.M.drawRect(0.0f, 0.0f, (float) (this.l * (this.k - 1)), (float) (this.m * (this.j - 1)), paint);
        Rect rect = new Rect(this.d + 1, this.e + 1, this.d + ((this.k - 1) * this.l), this.e + ((this.j - 1) * this.m));
        paint.setColor(-1);
        this.M.drawRect(rect, paint);
        for (int i2 = 0; i2 < this.j - 1; i2++) {
            for (int i3 = 0; i3 < this.k - 1; i3++) {
                a(i3, i2, this.N.a()[i3][i2]);
            }
        }
        paint.setColor(-16777216);
        for (int i4 = 1; i4 <= this.j; i4++) {
            this.M.drawLine((float) this.d, (float) (((i4 - 1) * this.m) + this.e), (float) ((this.l * (this.k - 1)) + this.d), (float) (((i4 - 1) * this.m) + this.e), paint);
        }
        for (int i5 = 1; i5 <= this.k; i5++) {
            this.M.drawLine((float) (((i5 - 1) * this.l) + this.d), (float) this.e, (float) (((i5 - 1) * this.l) + this.d), (float) ((this.m * (this.j - 1)) + this.e), paint);
        }
        this.a.set(0, 0, this.l * (this.k - 1), this.m * (this.j - 1));
        invalidate(this.a);
        this.f = this.d + (this.N.b() * this.l);
        this.g = this.e + (this.N.c() * this.m);
        a((this.d + (this.N.b() * this.l)) - (this.l / 2), ((this.e + (this.N.c() * this.m)) - (this.m / 2)) - 1, this.l * 3, this.m * 2);
    }

    public void d() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.K);
        if (this.W) {
            builder.setTitle("NEW RECORD!");
        } else {
            builder.setTitle("Congratulations!");
        }
        builder.setMessage("You solve the puzzle by " + this.D + " steps in " + this.U + " seconds!");
        builder.setNeutralButton("OK", new b(this));
        builder.show();
    }

    public void e() {
        this.Q = true;
        this.P.setText("--> Start New Game <--" + J);
        this.P.setTextColor(-65281);
        this.D = 0;
        this.W = false;
        this.P.setOnClickListener(new e(this));
    }

    public void f() {
        if (this.D != 0) {
            int i2 = C;
            if (this.D < C) {
                this.W = true;
            }
            C = Math.min(C, this.D);
            if (C != i2 && C != 0 && C != 9999) {
                SharedPreferences.Editor edit = this.K.getSharedPreferences("FifteenPuzzlePrefsFile", 0).edit();
                switch (this.R) {
                    case 1:
                        edit.putInt("easybeststeps", C);
                        break;
                    case 2:
                        edit.putInt("normalbeststeps", C);
                        break;
                    case 3:
                        edit.putInt("hardbeststeps", C);
                        break;
                }
                edit.commit();
            }
        }
    }

    public void g() {
        if (this.U != 0) {
            int i2 = this.V;
            if (this.U < this.V) {
                this.W = true;
            }
            this.V = Math.min(this.V, this.U);
            if (this.V != i2 && this.V != 0 && this.V != 9999) {
                SharedPreferences.Editor edit = this.K.getSharedPreferences("FifteenPuzzlePrefsFile", 0).edit();
                switch (this.R) {
                    case 1:
                        edit.putInt("easybesttime", this.V);
                        break;
                    case 2:
                        edit.putInt("normalbesttime", this.V);
                        break;
                    case 3:
                        edit.putInt("hardbesttime", this.V);
                        break;
                }
                edit.commit();
            }
        }
    }

    public void h() {
        if (C == 0) {
            C = 9999;
        }
        if (this.V == 0) {
            this.V = 6039;
        }
        int i2 = this.V;
        int i3 = i2 / 60;
        int i4 = i2 % 60;
        if (i4 < 10) {
            this.O.setText("Best Steps: " + Integer.toString(C) + "    Best Time: " + i3 + ":0" + i4 + J);
        } else {
            this.O.setText("Best Steps: " + Integer.toString(C) + "    Best Time: " + i3 + ":" + i4 + J);
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.L != null) {
            canvas.drawBitmap(this.L, 0.0f, 0.0f, (Paint) null);
        }
        super.onDraw(canvas);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.Q) {
            return super.onTouchEvent(motionEvent);
        }
        int a2 = a((int) motionEvent.getX());
        int b2 = b((int) motionEvent.getY());
        if (-1 == a2 || -1 == b2) {
            return false;
        }
        if (this.N.b(a2, b2)) {
            c();
            if (this.N.c(a2, b2)) {
                this.D++;
                j();
                d();
                return false;
            }
            this.D++;
            a(false);
        }
        return super.onTouchEvent(motionEvent);
    }
}
