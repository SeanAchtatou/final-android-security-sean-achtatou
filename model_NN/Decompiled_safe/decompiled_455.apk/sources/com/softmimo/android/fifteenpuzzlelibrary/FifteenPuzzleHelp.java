package com.softmimo.android.fifteenpuzzlelibrary;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.softmimo.android.fifteenpuzzlefreeversion.R;

public class FifteenPuzzleHelp extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        ImageView imageView = new ImageView(this);
        ImageView imageView2 = new ImageView(this);
        TextView textView = new TextView(this);
        TextView textView2 = new TextView(this);
        ScrollView scrollView = new ScrollView(this);
        imageView.setImageDrawable(getResources().getDrawable(R.drawable.fifteenpuzzle));
        imageView2.setImageDrawable(getResources().getDrawable(R.drawable.goal));
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(1);
        linearLayout.addView(imageView);
        Button button = new Button(this);
        if (FifteenPuzzleView.E) {
            String packageName = getPackageName();
            if (FifteenPuzzleView.F) {
                button.setText("--> Please provide feedback and support. <--");
            } else {
                button.setText("--> Please click here to check our products. <--");
            }
            button.setTextColor(-16776961);
            button.setOnClickListener(new i(this, packageName));
            linearLayout.addView(button);
        }
        linearLayout.addView(textView);
        linearLayout.addView(imageView2);
        linearLayout.addView(textView2);
        scrollView.addView(linearLayout);
        scrollView.setBackgroundDrawable(getResources().getDrawable(R.drawable.background));
        textView.setTextColor(-16777216);
        textView2.setTextColor(-16777216);
        textView.setTextSize(18.0f);
        textView.append("The 15 puzzle is also called Gem Puzzle, Boss Puzzle, Game of Fifteen, Mystic Square and many others. It is a sliding puzzle that consists of a frame of numbered square tiles in random order with one tile missing. The size of the frame is 4×4. The object of the puzzle is to place the tiles in order (see below diagram) by making sliding moves that use the empty space.");
        textView2.append("Version: " + Double.toString(ListView.a));
        textView2.append("\n \n");
        textView2.append("Difficulty and Level: User can select game difficulty from easy, normal and hard. There are hundreds of different puzzles to play.");
        textView2.append("\n \n");
        textView2.append("Use Splash Screen: User can trun on or off for starting the program with splash screen (not available for free version). ");
        textView2.append("\n \n");
        textView2.append("The system requirement for this game is: \n");
        textView2.append("   ---OS: Android v1.5 or above\n");
        textView2.append("   ---Physical Resolution: 320x480(HVGA), 480x800(WVGA800), 480x854(WVGA854) and higher resolutions\n");
        textView2.append("   ---Perfect for Android smart phone and tablet\n");
        textView2.append("Game is provided by Frank Android Software.\n");
        TextView textView3 = new TextView(this);
        textView3.setTextColor(-16777216);
        textView3.setTextSize(18.0f);
        textView3.setText(Html.fromHtml("<b>Website:</b><a href=\"http://www.softmimo.com/\">http://www.softmimo.com/</a> .<br><b>Email:</b><a href=\"mailto:support@softmimo.com\">support@softmimo.com</a> .<br><br>"));
        textView3.setMovementMethod(LinkMovementMethod.getInstance());
        linearLayout.addView(textView3);
        setContentView(scrollView);
    }
}
