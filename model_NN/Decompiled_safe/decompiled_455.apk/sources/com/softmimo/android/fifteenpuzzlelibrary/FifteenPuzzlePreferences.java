package com.softmimo.android.fifteenpuzzlelibrary;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.widget.Button;
import com.softmimo.android.fifteenpuzzlefreeversion.R;

public class FifteenPuzzlePreferences extends PreferenceActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        addPreferencesFromResource(R.xml.preferences);
        setContentView((int) R.layout.preferencelayout);
        setTheme((int) R.style.Theme_DarkText);
        ((Button) findViewById(R.id.clearrecord)).setOnClickListener(new c(this));
        ((Button) findViewById(R.id.ok)).setOnClickListener(new d(this));
    }
}
