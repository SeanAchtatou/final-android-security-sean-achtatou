package com.softmimo.android.fifteenpuzzlelibrary;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import com.softmimo.android.fifteenpuzzlefreeversion.R;

public class ListView extends Activity implements View.OnClickListener {
    public static double a = 1.0d;
    private Button b;
    private Button c;
    private Button d;
    private Button e;
    private Intent f;

    private String a() {
        try {
            return getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public void onClick(View view) {
        if (view == this.b) {
            this.f = new Intent(this, FifteenPuzzle.class);
            startActivity(this.f);
        }
        if (view == this.e) {
            this.f = new Intent(this, FifteenPuzzlePreferences.class);
            startActivity(this.f);
        }
        if (view == this.d) {
            this.f = new Intent(this, FifteenPuzzleHelp.class);
            startActivity(this.f);
        }
        if (view == this.c) {
            finish();
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.list_view);
        this.b = (Button) findViewById(R.id.start);
        this.c = (Button) findViewById(R.id.exit);
        this.d = (Button) findViewById(R.id.help);
        this.e = (Button) findViewById(R.id.options);
        this.b.setOnClickListener(this);
        this.c.setOnClickListener(this);
        this.d.setOnClickListener(this);
        this.e.setOnClickListener(this);
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        FifteenPuzzleView.G = defaultDisplay.getWidth();
        FifteenPuzzleView.H = defaultDisplay.getHeight();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        defaultDisplay.getMetrics(displayMetrics);
        FifteenPuzzleView.I = displayMetrics.density;
        PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String a2 = a();
        if (a2 != null) {
            a = Double.parseDouble(a2);
        }
    }
}
