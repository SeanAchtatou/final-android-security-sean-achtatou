package com.softmimo.android.fifteenpuzzlelibrary;

import android.os.SystemClock;

class g implements Runnable {
    final /* synthetic */ FifteenPuzzleView a;

    g(FifteenPuzzleView fifteenPuzzleView) {
        this.a = fifteenPuzzleView;
    }

    public void run() {
        this.a.U = ((int) (SystemClock.uptimeMillis() - this.a.T)) / 1000;
        int b = this.a.U;
        int i = b / 60;
        int i2 = b % 60;
        if (i2 < 10) {
            this.a.P.setText("Current Steps: " + Integer.toString(this.a.D) + "    Time Used: " + i + ":0" + i2 + FifteenPuzzleView.J);
        } else {
            this.a.P.setText("Current Steps: " + Integer.toString(this.a.D) + "    Time Used: " + i + ":" + i2 + FifteenPuzzleView.J);
        }
        this.a.S.postAtTime(this, ((long) ((i2 + (i * 60) + 1) * 1000)) + this.a.T);
    }
}
