package com.softmimo.android.fifteenpuzzlefreeversion;

import android.os.Handler;
import android.os.Message;

class b extends Handler {
    final /* synthetic */ WelcomeView a;

    b(WelcomeView welcomeView) {
        this.a = welcomeView;
    }

    public void handleMessage(Message message) {
        super.handleMessage(message);
        this.a.a.setAlpha(this.a.c);
        this.a.a.invalidate();
        this.a.b.setAlpha(this.a.c);
        this.a.b.invalidate();
    }
}
