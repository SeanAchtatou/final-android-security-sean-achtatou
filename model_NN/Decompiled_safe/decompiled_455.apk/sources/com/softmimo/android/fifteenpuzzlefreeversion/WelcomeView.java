package com.softmimo.android.fifteenpuzzlefreeversion;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.widget.ImageView;
import com.softmimo.android.fifteenpuzzlelibrary.FifteenPuzzleView;
import com.softmimo.android.fifteenpuzzlelibrary.ListView;

public class WelcomeView extends Activity {
    ImageView a;
    ImageView b;
    int c = 255;
    int d = 0;
    private Handler e = new Handler();

    public void a() {
        this.c -= 5;
        if (this.c <= 0) {
            this.d = 2;
            startActivity(new Intent(this, ListView.class));
            finish();
        }
        this.e.sendMessage(this.e.obtainMessage());
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        FifteenPuzzleView.E = true;
        setContentView((int) R.layout.welcome);
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean("checkbox_splashscreen", true)) {
            this.a = (ImageView) findViewById(R.id.logo);
            this.a.setAlpha(this.c);
            this.b = (ImageView) findViewById(R.id.logofrank);
            this.b.setAlpha(this.c);
            new Thread(new a(this)).start();
            this.e = new b(this);
            return;
        }
        startActivity(new Intent(this, ListView.class));
        finish();
    }
}
