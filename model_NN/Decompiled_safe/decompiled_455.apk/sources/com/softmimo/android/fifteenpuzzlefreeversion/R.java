package com.softmimo.android.fifteenpuzzlefreeversion;

/* This class is generated by JADX */
public final class R {

    public static final class attr {
        public static final int testing = 2130771968;
        public static final int backgroundColor = 2130771969;
        public static final int textColor = 2130771970;
        public static final int keywords = 2130771971;
        public static final int refreshInterval = 2130771972;
        public static final int isGoneWithoutAd = 2130771973;
    }

    public static final class drawable {
        public static final int background = 2130837504;
        public static final int best = 2130837505;
        public static final int bt_blue = 2130837506;
        public static final int bt_blue_image = 2130837507;
        public static final int bt_red_image = 2130837508;
        public static final int buttonbg = 2130837509;
        public static final int eight = 2130837510;
        public static final int eleven = 2130837511;
        public static final int exit = 2130837512;
        public static final int fifteen = 2130837513;
        public static final int fifteenpuzzle = 2130837514;
        public static final int five = 2130837515;
        public static final int forteen = 2130837516;
        public static final int four = 2130837517;
        public static final int frankandroidsoftware = 2130837518;
        public static final int goal = 2130837519;
        public static final int help = 2130837520;
        public static final int icon = 2130837521;
        public static final int nine = 2130837522;
        public static final int one = 2130837523;
        public static final int options = 2130837524;
        public static final int play = 2130837525;
        public static final int seven = 2130837526;
        public static final int six = 2130837527;
        public static final int softmimoandroid = 2130837528;
        public static final int ten = 2130837529;
        public static final int thirteen = 2130837530;
        public static final int three = 2130837531;
        public static final int time = 2130837532;
        public static final int twelve = 2130837533;
        public static final int two = 2130837534;
    }

    public static final class layout {
        public static final int ads = 2130903040;
        public static final int free_main = 2130903041;
        public static final int help = 2130903042;
        public static final int list_view = 2130903043;
        public static final int main = 2130903044;
        public static final int preferencelayout = 2130903045;
        public static final int welcome = 2130903046;
    }

    public static final class anim {
        public static final int zoomin = 2130968576;
        public static final int zoomout = 2130968577;
    }

    public static final class xml {
        public static final int preferences = 2131034112;
    }

    public static final class string {
        public static final int app_name = 2131099648;
        public static final int eula_title = 2131099649;
        public static final int eula_accept = 2131099650;
        public static final int eula_refuse = 2131099651;
        public static final int hello = 2131099652;
        public static final int starttext = 2131099653;
        public static final int helptext = 2131099654;
        public static final int highestScore = 2131099655;
        public static final int currentScore = 2131099656;
        public static final int exittext = 2131099657;
        public static final int upgradelink = 2131099658;
        public static final int dialog_based_preferences = 2131099659;
        public static final int optionstext = 2131099660;
        public static final int optionsok = 2131099661;
        public static final int list_difficulty = 2131099662;
        public static final int title_list_difficulty = 2131099663;
        public static final int summary_list_difficulty = 2131099664;
        public static final int dialog_title_list_difficulty = 2131099665;
        public static final int list_color = 2131099666;
        public static final int title_list_color = 2131099667;
        public static final int summary_list_color = 2131099668;
        public static final int dialog_title_list_color = 2131099669;
        public static final int checkbox_autocheckupgrade = 2131099670;
        public static final int title_toggle_autocheckupgrade = 2131099671;
        public static final int summary_toggle_autocheckupgrade = 2131099672;
    }

    public static final class style {
        public static final int Theme_DarkText = 2131165184;
    }

    public static final class array {
        public static final int entries_list_difficulty = 2131230720;
        public static final int entryvalues_list_difficulty = 2131230721;
        public static final int entries_list_color = 2131230722;
        public static final int entryvalues_list_color = 2131230723;
    }

    public static final class id {
        public static final int ad = 2131296256;
        public static final int adview = 2131296257;
        public static final int highestScore = 2131296258;
        public static final int currentScore = 2131296259;
        public static final int tv = 2131296260;
        public static final int LinearLayout01 = 2131296261;
        public static final int rl = 2131296262;
        public static final int ib = 2131296263;
        public static final int start = 2131296264;
        public static final int options = 2131296265;
        public static final int help = 2131296266;
        public static final int exit = 2131296267;
        public static final int linearLayout = 2131296268;
        public static final int clearrecord = 2131296269;
        public static final int ok = 2131296270;
        public static final int logo = 2131296271;
        public static final int logofrank = 2131296272;
        public static final int splashscreen = 2131296273;
    }
}
