package com.google.ads.util;

import java.io.UnsupportedEncodingException;

public class e {
    static final /* synthetic */ boolean a = (!e.class.desiredAssertionStatus());

    private e() {
    }

    public static String a(byte[] bArr) {
        try {
            int length = bArr.length;
            a aVar = new a();
            int i = (length / 3) * 4;
            if (!aVar.b) {
                switch (length % 3) {
                    case 1:
                        i += 2;
                        break;
                    case 2:
                        i += 3;
                        break;
                }
            } else if (length % 3 > 0) {
                i += 4;
            }
            if (aVar.c && length > 0) {
                i += (((length - 1) / 57) + 1) * (aVar.d ? 2 : 1);
            }
            aVar.f = new byte[i];
            aVar.a(bArr, length);
            if (a || aVar.g == i) {
                return new String(aVar.f, "US-ASCII");
            }
            throw new AssertionError();
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }
}
