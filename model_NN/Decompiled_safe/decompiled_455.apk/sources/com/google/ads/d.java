package com.google.ads;

public class d {
    public static final d a = new d(320, 50, "320x50_mb");
    public static final d b = new d(300, 250, "300x250_as");
    public static final d c = new d(468, 60, "468x60_as");
    public static final d d = new d(728, 90, "728x90_as");
    private int e;
    private int f;
    private String g;

    private d(int i, int i2, String str) {
        this.e = i;
        this.f = i2;
        this.g = str;
    }

    public int a() {
        return this.e;
    }

    public int b() {
        return this.f;
    }

    public String toString() {
        return this.g;
    }
}
