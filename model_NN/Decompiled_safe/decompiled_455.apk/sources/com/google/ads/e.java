package com.google.ads;

public enum e {
    MALE("m"),
    FEMALE("f");
    
    private String c;

    private e(String str) {
        this.c = str;
    }

    public final String toString() {
        return this.c;
    }
}
