   j        Activity   .ActivityWrapper   Activity   btnBack   .ButtonWrapper   Button   btnStart   .ButtonWrapper   Button
   ImageView1   .ImageViewWrapper	   ImageView
   ImageView2   .ImageViewWrapper	   ImageView   Label1   .LabelWrapper   Label   lblTitle   .LabelWrapper   Label   pnlad   .PanelWrapper   Panel   	   hole1.png	   star1.png     �?@  �     csType   Dbasic.Designer.MetaActivity   type   .ActivityWrapper   drawable   csType&   Dbasic.Designer.Drawable.ColorDrawable   type   .drawable.ColorDrawable   alpha�      color����   cornerRadius       orientation
   LEFT_RIGHT       enabled	   eventName   Activity
   fullScreen   includeTitle    javaType   .ActivityWrapper   name   Activity   parent       tag       title   Activity
   titleColor����   visible   variant0   leftd      topd      widthd      heightd          :kids   0   csType   Dbasic.Designer.MetaButton   type   .ButtonWrapper   drawable   csType(   Dbasic.Designer.Drawable.DefaultDrawable   type   .drawable.DefaultDrawable       enabled	   eventName   btnStart   fontsize  `A
   hAlignment   CENTER_HORIZONTAL   javaType   .ButtonWrapper   name   btnStart   parent   Activity   pressed    style   NORMAL   tag       text   start game>>	   textColor�      typeface   DEFAULT
   vAlignment   CENTER_VERTICAL   visible   variant0   left      top<      width     height<              1   csType   Dbasic.Designer.MetaImageView   type   .ImageViewWrapper   drawable   csType'   Dbasic.Designer.Drawable.BitmapDrawable   type   .drawable.BitmapDrawable   file	   hole1.png   gravityw          enabled	   eventName
   ImageView2   javaType   .ImageViewWrapper   name
   ImageView2   parent   Activity   pressed    tag       visible   variant0   leftx      top     width�      height�              2   csType   Dbasic.Designer.MetaImageView   type   .ImageViewWrapper   drawable   csType'   Dbasic.Designer.Drawable.BitmapDrawable   type   .drawable.BitmapDrawable   file	   star1.png   gravityw          enabled	   eventName
   ImageView1   javaType   .ImageViewWrapper   name
   ImageView1   parent   Activity   pressed    tag       visible   variant0   left      top�      width�      height�              3   csType   Dbasic.Designer.MetaLabel   type   .LabelWrapper   drawable   csType1   Dbasic.Designer.Drawable.ColorWithCornersDrawable   type   .drawable.ColorDrawable   alpha       color�      cornerRadius       orientation
   LEFT_RIGHT       enabled	   eventName   lblTitle   fontsize   B
   hAlignment   CENTER_HORIZONTAL   javaType   .LabelWrapper   name   lblTitle   parent   Activity   style   BOLD   tag       text   Bank Bank Star!	   textColor��""   typeface   SERIF
   vAlignment   CENTER_VERTICAL   visible   variant0   left       top|     width@     heightF              4   csType   Dbasic.Designer.MetaPanel   type   .PanelWrapper   drawable   csType1   Dbasic.Designer.Drawable.ColorWithCornersDrawable   type   .drawable.ColorDrawable   alpha       color�      cornerRadius       orientation
   LEFT_RIGHT       enabled	   eventName   pnlad   javaType   .PanelWrapper   name   pnlad   parent   Activity   tag       visible   variant0   left       top       width@     height2              5   csType   Dbasic.Designer.MetaButton   type   .ButtonWrapper   drawable   csType(   Dbasic.Designer.Drawable.DefaultDrawable   type   .drawable.DefaultDrawable       enabled	   eventName   btnBack   fontsize  `A
   hAlignment   CENTER_HORIZONTAL   javaType   .ButtonWrapper   name   btnBack   parent   Activity   pressed    style   NORMAL   tag       text   << back	   textColor�      typeface   DEFAULT
   vAlignment   CENTER_VERTICAL   visible   variant0   left      topJ     width     height<              6   csType   Dbasic.Designer.MetaLabel   type   .LabelWrapper   drawable   csType1   Dbasic.Designer.Drawable.ColorWithCornersDrawable   type   .drawable.ColorDrawable   alpha       color�      cornerRadius       orientation
   LEFT_RIGHT       enabled	   eventName   Label1   fontsize  `A
   hAlignment   LEFT   javaType   .LabelWrapper   name   Label1   parent   Activity   style   NORMAL   tag       text  1 - Click and drag the star

2 - The target is the hole for the star to escape from

3 - You MUST hit one of the walls at least once, and at most 4 times before hitting the hole!

4 - The more you hit the walls, the more points you get, but remember... NO MORE THAN 4 BANKS!	   textColor�      typeface   SERIF
   vAlignment   CENTER_VERTICAL   visible   variant0   left      topx      width     height�                       