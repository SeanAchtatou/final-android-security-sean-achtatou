package com.madelephantstudios.starbank;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import anywheresoftware.b4a.B4AActivity;
import anywheresoftware.b4a.B4AMenuItem;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.Msgbox;
import anywheresoftware.b4a.keywords.Common;
import anywheresoftware.b4a.keywords.constants.TypefaceWrapper;
import anywheresoftware.b4a.objects.ActivityWrapper;
import anywheresoftware.b4a.objects.AnimationWrapper;
import anywheresoftware.b4a.objects.ImageViewWrapper;
import anywheresoftware.b4a.objects.LabelWrapper;
import anywheresoftware.b4a.objects.PanelWrapper;
import anywheresoftware.b4a.objects.Timer;
import anywheresoftware.b4a.objects.ViewWrapper;
import anywheresoftware.b4a.objects.streams.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;

public class activitystartup extends Activity implements B4AActivity {
    public static int _starsize = 0;
    public static Timer _tmrs = null;
    public static Timer _tmrs2 = null;
    static boolean afterFirstLayout = false;
    private static final boolean fullScreen = true;
    private static final boolean includeTitle = false;
    static boolean isFirst = true;
    static activitystartup mostCurrent;
    public static WeakReference<Activity> previousOne;
    public static BA processBA;
    private static boolean processGlobalsRun = false;
    public Common __c = null;
    ActivityWrapper _activity;
    public activitygame _activitygame = null;
    public activityhow _activityhow = null;
    public AnimationWrapper _anim = null;
    public AnimationWrapper[] _animstars = null;
    public LabelWrapper _label1 = null;
    public main _main = null;
    public PanelWrapper _panel1 = null;
    public PanelWrapper[] _panelanim = null;
    public ImageViewWrapper _star1 = null;
    public TypefaceWrapper _tf = null;
    BA activityBA;
    BALayout layout;
    ArrayList<B4AMenuItem> menuItems;
    private Boolean onKeySubExist = null;
    private Boolean onKeyUpSubExist = null;

    public void onCreate(Bundle bundle) {
        Activity activity;
        super.onCreate(bundle);
        if (isFirst) {
            processBA = new BA(getApplicationContext(), null, null, "com.madelephantstudios.starbank", "activitystartup");
            processBA.loadHtSubs(getClass());
            BALayout.setDeviceScale(getApplicationContext().getResources().getDisplayMetrics().density);
        } else if (!(previousOne == null || (activity = previousOne.get()) == null || activity == this)) {
            Common.Log("Killing previous instance (activitystartup).");
            activity.finish();
        }
        getWindow().requestFeature(1);
        getWindow().setFlags(1024, 1024);
        mostCurrent = this;
        processBA.activityBA = null;
        this.layout = new BALayout(this);
        setContentView(this.layout);
        afterFirstLayout = false;
        BA.handler.postDelayed(new WaitForLayout(), 5);
    }

    private static class WaitForLayout implements Runnable {
        private WaitForLayout() {
        }

        public void run() {
            if (!activitystartup.afterFirstLayout) {
                if (activitystartup.mostCurrent.layout.getWidth() == 0) {
                    BA.handler.postDelayed(this, 5);
                    return;
                }
                activitystartup.mostCurrent.layout.getLayoutParams().height = activitystartup.mostCurrent.layout.getHeight();
                activitystartup.mostCurrent.layout.getLayoutParams().width = activitystartup.mostCurrent.layout.getWidth();
                activitystartup.afterFirstLayout = true;
                activitystartup.mostCurrent.afterFirstLayout();
            }
        }
    }

    /* access modifiers changed from: private */
    public void afterFirstLayout() {
        this.activityBA = new BA(this, this.layout, processBA, "com.madelephantstudios.starbank", "activitystartup");
        processBA.activityBA = new WeakReference<>(this.activityBA);
        this._activity = new ActivityWrapper(this.activityBA, "activity");
        Msgbox.isDismissing = false;
        initializeProcessGlobals();
        initializeGlobals();
        ViewWrapper.lastId = 0;
        Common.Log("** Activity (activitystartup) Create, isFirst = " + isFirst + " **");
        processBA.raiseEvent2(null, true, "activity_create", false, Boolean.valueOf(isFirst));
        isFirst = false;
        if (mostCurrent != null && mostCurrent == this) {
            processBA.setActivityPaused(false);
            Common.Log("** Activity (activitystartup) Resume **");
            processBA.raiseEvent(null, "activity_resume", new Object[0]);
        }
    }

    public void addMenuItem(B4AMenuItem b4AMenuItem) {
        if (this.menuItems == null) {
            this.menuItems = new ArrayList<>();
        }
        this.menuItems.add(b4AMenuItem);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        if (this.menuItems == null) {
            return false;
        }
        Iterator<B4AMenuItem> it = this.menuItems.iterator();
        while (it.hasNext()) {
            B4AMenuItem next = it.next();
            MenuItem add = menu.add(next.title);
            if (next.drawable != null) {
                add.setIcon(next.drawable);
            }
            add.setOnMenuItemClickListener(new B4AMenuItemsClickListener(next.eventName.toLowerCase(BA.cul)));
        }
        return true;
    }

    private class B4AMenuItemsClickListener implements MenuItem.OnMenuItemClickListener {
        private final String eventName;

        public B4AMenuItemsClickListener(String str) {
            this.eventName = str;
        }

        public boolean onMenuItemClick(MenuItem menuItem) {
            activitystartup.processBA.raiseEvent(menuItem.getTitle(), this.eventName + "_click", new Object[0]);
            return true;
        }
    }

    public static Class<?> getObject() {
        return activitystartup.class;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (this.onKeySubExist == null) {
            this.onKeySubExist = Boolean.valueOf(processBA.subExists("activity_keypress"));
        }
        if (this.onKeySubExist.booleanValue()) {
            Boolean bool = (Boolean) processBA.raiseEvent2(this._activity, false, "activity_keypress", false, Integer.valueOf(i));
            if (bool == null || bool.booleanValue()) {
                return true;
            }
        }
        return super.onKeyDown(i, keyEvent);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (this.onKeyUpSubExist == null) {
            this.onKeyUpSubExist = Boolean.valueOf(processBA.subExists("activity_keyup"));
        }
        if (this.onKeyUpSubExist.booleanValue()) {
            Boolean bool = (Boolean) processBA.raiseEvent2(this._activity, false, "activity_keyup", false, Integer.valueOf(i));
            if (bool == null || bool.booleanValue()) {
                return true;
            }
        }
        return super.onKeyUp(i, keyEvent);
    }

    public void onNewIntent(Intent intent) {
        setIntent(intent);
    }

    public void onPause() {
        super.onPause();
        if (this._activity != null) {
            Msgbox.dismiss(true);
            Common.Log("** Activity (activitystartup) Pause, UserClosed = " + this.activityBA.activity.isFinishing() + " **");
            processBA.raiseEvent2(this._activity, true, "activity_pause", false, Boolean.valueOf(this.activityBA.activity.isFinishing()));
            processBA.setActivityPaused(true);
            mostCurrent = null;
            if (!this.activityBA.activity.isFinishing()) {
                previousOne = new WeakReference<>(this);
            }
            Msgbox.isDismissing = false;
        }
    }

    public void onDestroy() {
        super.onDestroy();
        previousOne = null;
    }

    public void onResume() {
        super.onResume();
        mostCurrent = this;
        Msgbox.isDismissing = false;
        if (this.activityBA != null) {
            BA.handler.post(new ResumeMessage(mostCurrent));
        }
    }

    private static class ResumeMessage implements Runnable {
        private final WeakReference<Activity> activity;

        public ResumeMessage(Activity activity2) {
            this.activity = new WeakReference<>(activity2);
        }

        public void run() {
            if (activitystartup.mostCurrent != null && activitystartup.mostCurrent == this.activity.get()) {
                activitystartup.processBA.setActivityPaused(false);
                Common.Log("** Activity (activitystartup) Resume **");
                activitystartup.processBA.raiseEvent(activitystartup.mostCurrent._activity, "activity_resume", null);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        processBA.onActivityResult(i, i2, intent);
    }

    private static void initializeGlobals() {
        processBA.raiseEvent2(null, true, "globals", false, null);
    }

    public static String _activity_create(boolean z) throws Exception {
        mostCurrent._activity.LoadLayout("layoutStartup", mostCurrent.activityBA);
        mostCurrent._panel1.setHeight(Common.PerYToCurrent(100.0f, mostCurrent.activityBA));
        TypefaceWrapper typefaceWrapper = mostCurrent._tf;
        TypefaceWrapper typefaceWrapper2 = Common.Typeface;
        typefaceWrapper.setObject(TypefaceWrapper.LoadFromAssets("MountainsofChristmas.ttf"));
        mostCurrent._label1.setTypeface((Typeface) mostCurrent._tf.getObject());
        mostCurrent._label1.setText("Bank Bank Star!");
        _starsize = Common.DipToCurrent(20);
        mostCurrent._star1.setLeft((int) (((double) mostCurrent._panel1.getLeft()) - (((double) mostCurrent._star1.getWidth()) / 2.0d)));
        double length = (double) (mostCurrent._animstars.length - 1);
        int i = 0;
        while (true) {
            int i2 = i;
            if (((double) i2) > length) {
                break;
            }
            mostCurrent._animstars[i2].InitializeTranslate(mostCurrent.activityBA, "anim", Common.Density, Common.Density, (float) Common.Rnd(-50, 51), (float) Common.Rnd(-50, 51));
            mostCurrent._animstars[i2].setDuration((long) Common.Rnd(400, 1000));
            mostCurrent._animstars[i2].setRepeatCount(0);
            AnimationWrapper animationWrapper = mostCurrent._animstars[i2];
            AnimationWrapper animationWrapper2 = mostCurrent._animstars[i2];
            animationWrapper.setRepeatMode(1);
            mostCurrent._panelanim[i2].Initialize(mostCurrent.activityBA, "");
            PanelWrapper panelWrapper = mostCurrent._panelanim[i2];
            File file = Common.File;
            panelWrapper.SetBackgroundImage((Bitmap) Common.LoadBitmap(File.getDirAssets(), "star1.png").getObject());
            mostCurrent._activity.AddView((View) mostCurrent._panelanim[i2].getObject(), mostCurrent._star1.getLeft(), mostCurrent._star1.getTop(), (int) (((double) mostCurrent._star1.getWidth()) / 8.0d), (int) (((double) mostCurrent._star1.getHeight()) / 8.0d));
            mostCurrent._panelanim[i2].setTag(Integer.valueOf(i2));
            mostCurrent._panelanim[i2].BringToFront();
            mostCurrent._animstars[i2].Start((View) mostCurrent._panelanim[i2].getObject());
            i = (int) (((double) i2) + 1.0d);
        }
        mostCurrent._star1.BringToFront();
        if (z) {
            _tmrs.Initialize(processBA, "tmrs", 10);
        }
        if (z) {
            _tmrs2.Initialize(processBA, "tmrs2", 1000);
        }
        _tmrs.setEnabled(true);
        return "";
    }

    public static String _activity_pause(boolean z) throws Exception {
        return "";
    }

    public static String _activity_resume() throws Exception {
        return "";
    }

    public static String _anim_animationend() throws Exception {
        double length = (double) (mostCurrent._panelanim.length - 1);
        int i = 0;
        while (true) {
            int i2 = i;
            if (((double) i2) > length) {
                return "";
            }
            if (Common.Sender(mostCurrent.activityBA).equals(mostCurrent._animstars[i2].getObject())) {
                mostCurrent._panelanim[i2].setTop((int) (((double) mostCurrent._star1.getTop()) + (((double) mostCurrent._star1.getWidth()) / 2.0d)));
                mostCurrent._panelanim[i2].setLeft((int) (((double) mostCurrent._star1.getLeft()) + (((double) mostCurrent._star1.getHeight()) / 2.0d)));
                mostCurrent._animstars[i2].setDuration((long) Common.Rnd(400, 1000));
                mostCurrent._animstars[i2].Start((View) mostCurrent._panelanim[i2].getObject());
            }
            i = (int) (((double) i2) + 1.0d);
        }
    }

    public static void initializeProcessGlobals() {
    }

    public static String _globals() throws Exception {
        mostCurrent._label1 = new LabelWrapper();
        mostCurrent._anim = new AnimationWrapper();
        mostCurrent._panel1 = new PanelWrapper();
        mostCurrent._tf = new TypefaceWrapper();
        mostCurrent._star1 = new ImageViewWrapper();
        mostCurrent._animstars = new AnimationWrapper[10];
        int length = mostCurrent._animstars.length;
        for (int i = 0; i < length; i++) {
            mostCurrent._animstars[i] = new AnimationWrapper();
        }
        mostCurrent._panelanim = new PanelWrapper[10];
        int length2 = mostCurrent._panelanim.length;
        for (int i2 = 0; i2 < length2; i2++) {
            mostCurrent._panelanim[i2] = new PanelWrapper();
        }
        _starsize = 0;
        return "";
    }

    public static String _process_globals() throws Exception {
        _tmrs = new Timer();
        _tmrs2 = new Timer();
        return "";
    }

    public static String _tmrs_tick() throws Exception {
        mostCurrent._panel1.setLeft(mostCurrent._panel1.getLeft() + 3);
        mostCurrent._star1.setLeft((int) (((double) mostCurrent._panel1.getLeft()) - (((double) mostCurrent._star1.getWidth()) / 2.0d)));
        if (mostCurrent._panel1.getLeft() < Common.PerXToCurrent(100.0f, mostCurrent.activityBA) + mostCurrent._star1.getWidth()) {
            return "";
        }
        _tmrs.setEnabled(false);
        _tmrs2.setEnabled(true);
        return "";
    }

    public static String _tmrs2_tick() throws Exception {
        _tmrs2.setEnabled(false);
        mostCurrent._activity.Finish();
        return "";
    }
}
