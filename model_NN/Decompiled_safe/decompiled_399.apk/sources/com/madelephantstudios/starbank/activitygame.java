package com.madelephantstudios.starbank;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import anywheresoftware.b4a.B4AActivity;
import anywheresoftware.b4a.B4AMenuItem;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.BALayout;
import anywheresoftware.b4a.Msgbox;
import anywheresoftware.b4a.admobwrapper.AdViewWrapper;
import anywheresoftware.b4a.keywords.Common;
import anywheresoftware.b4a.keywords.constants.Colors;
import anywheresoftware.b4a.keywords.constants.TypefaceWrapper;
import anywheresoftware.b4a.objects.ActivityWrapper;
import anywheresoftware.b4a.objects.AnimationWrapper;
import anywheresoftware.b4a.objects.ButtonWrapper;
import anywheresoftware.b4a.objects.ImageViewWrapper;
import anywheresoftware.b4a.objects.LabelWrapper;
import anywheresoftware.b4a.objects.PanelWrapper;
import anywheresoftware.b4a.objects.Timer;
import anywheresoftware.b4a.objects.ViewWrapper;
import anywheresoftware.b4a.objects.streams.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;

public class activitygame extends Activity implements B4AActivity {
    public static int _banks = 0;
    public static int _bordersize = 0;
    public static double _cx = 0.0d;
    public static double _cy = 0.0d;
    public static int _holesize = 0;
    public static int _hscore = 0;
    public static int _lifes = 0;
    public static int _maxbanks = 0;
    public static int _score = 0;
    public static int _starsize = 0;
    public static Timer _tmr = null;
    public static int _xend = 0;
    public static int _xstart = 0;
    public static double _xvel = 0.0d;
    public static int _yend = 0;
    public static int _ystart = 0;
    public static double _yvel = 0.0d;
    static boolean afterFirstLayout = false;
    private static final boolean fullScreen = true;
    private static final boolean includeTitle = false;
    static boolean isFirst = true;
    static activitygame mostCurrent;
    public static WeakReference<Activity> previousOne;
    public static BA processBA;
    private static boolean processGlobalsRun = false;
    public Common __c = null;
    ActivityWrapper _activity;
    public activityhow _activityhow = null;
    public activitystartup _activitystartup = null;
    public AdViewWrapper _ad = null;
    public AnimationWrapper[] _animstars = null;
    public PanelWrapper _blackhole = null;
    public ButtonWrapper _btnpause = null;
    public LabelWrapper _lblhighscore = null;
    public LabelWrapper _lblscore = null;
    public ImageViewWrapper _life1 = null;
    public ImageViewWrapper _life2 = null;
    public ImageViewWrapper _life3 = null;
    public main _main = null;
    public PanelWrapper[] _panelanim = null;
    public PanelWrapper _playarea = null;
    public PanelWrapper _pnlad = null;
    public PanelWrapper _star = null;
    public TypefaceWrapper _tf = null;
    BA activityBA;
    BALayout layout;
    ArrayList<B4AMenuItem> menuItems;
    private Boolean onKeySubExist = null;
    private Boolean onKeyUpSubExist = null;

    public void onCreate(Bundle bundle) {
        Activity activity;
        super.onCreate(bundle);
        if (isFirst) {
            processBA = new BA(getApplicationContext(), null, null, "com.madelephantstudios.starbank", "activitygame");
            processBA.loadHtSubs(getClass());
            BALayout.setDeviceScale(getApplicationContext().getResources().getDisplayMetrics().density);
        } else if (!(previousOne == null || (activity = previousOne.get()) == null || activity == this)) {
            Common.Log("Killing previous instance (activitygame).");
            activity.finish();
        }
        getWindow().requestFeature(1);
        getWindow().setFlags(1024, 1024);
        mostCurrent = this;
        processBA.activityBA = null;
        this.layout = new BALayout(this);
        setContentView(this.layout);
        afterFirstLayout = false;
        BA.handler.postDelayed(new WaitForLayout(), 5);
    }

    private static class WaitForLayout implements Runnable {
        private WaitForLayout() {
        }

        public void run() {
            if (!activitygame.afterFirstLayout) {
                if (activitygame.mostCurrent.layout.getWidth() == 0) {
                    BA.handler.postDelayed(this, 5);
                    return;
                }
                activitygame.mostCurrent.layout.getLayoutParams().height = activitygame.mostCurrent.layout.getHeight();
                activitygame.mostCurrent.layout.getLayoutParams().width = activitygame.mostCurrent.layout.getWidth();
                activitygame.afterFirstLayout = true;
                activitygame.mostCurrent.afterFirstLayout();
            }
        }
    }

    /* access modifiers changed from: private */
    public void afterFirstLayout() {
        this.activityBA = new BA(this, this.layout, processBA, "com.madelephantstudios.starbank", "activitygame");
        processBA.activityBA = new WeakReference<>(this.activityBA);
        this._activity = new ActivityWrapper(this.activityBA, "activity");
        Msgbox.isDismissing = false;
        initializeProcessGlobals();
        initializeGlobals();
        ViewWrapper.lastId = 0;
        Common.Log("** Activity (activitygame) Create, isFirst = " + isFirst + " **");
        processBA.raiseEvent2(null, true, "activity_create", false, Boolean.valueOf(isFirst));
        isFirst = false;
        if (mostCurrent != null && mostCurrent == this) {
            processBA.setActivityPaused(false);
            Common.Log("** Activity (activitygame) Resume **");
            processBA.raiseEvent(null, "activity_resume", new Object[0]);
        }
    }

    public void addMenuItem(B4AMenuItem b4AMenuItem) {
        if (this.menuItems == null) {
            this.menuItems = new ArrayList<>();
        }
        this.menuItems.add(b4AMenuItem);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        if (this.menuItems == null) {
            return false;
        }
        Iterator<B4AMenuItem> it = this.menuItems.iterator();
        while (it.hasNext()) {
            B4AMenuItem next = it.next();
            MenuItem add = menu.add(next.title);
            if (next.drawable != null) {
                add.setIcon(next.drawable);
            }
            add.setOnMenuItemClickListener(new B4AMenuItemsClickListener(next.eventName.toLowerCase(BA.cul)));
        }
        return true;
    }

    private class B4AMenuItemsClickListener implements MenuItem.OnMenuItemClickListener {
        private final String eventName;

        public B4AMenuItemsClickListener(String str) {
            this.eventName = str;
        }

        public boolean onMenuItemClick(MenuItem menuItem) {
            activitygame.processBA.raiseEvent(menuItem.getTitle(), this.eventName + "_click", new Object[0]);
            return true;
        }
    }

    public static Class<?> getObject() {
        return activitygame.class;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (this.onKeySubExist == null) {
            this.onKeySubExist = Boolean.valueOf(processBA.subExists("activity_keypress"));
        }
        if (this.onKeySubExist.booleanValue()) {
            Boolean bool = (Boolean) processBA.raiseEvent2(this._activity, false, "activity_keypress", false, Integer.valueOf(i));
            if (bool == null || bool.booleanValue()) {
                return true;
            }
        }
        return super.onKeyDown(i, keyEvent);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (this.onKeyUpSubExist == null) {
            this.onKeyUpSubExist = Boolean.valueOf(processBA.subExists("activity_keyup"));
        }
        if (this.onKeyUpSubExist.booleanValue()) {
            Boolean bool = (Boolean) processBA.raiseEvent2(this._activity, false, "activity_keyup", false, Integer.valueOf(i));
            if (bool == null || bool.booleanValue()) {
                return true;
            }
        }
        return super.onKeyUp(i, keyEvent);
    }

    public void onNewIntent(Intent intent) {
        setIntent(intent);
    }

    public void onPause() {
        super.onPause();
        if (this._activity != null) {
            Msgbox.dismiss(true);
            Common.Log("** Activity (activitygame) Pause, UserClosed = " + this.activityBA.activity.isFinishing() + " **");
            processBA.raiseEvent2(this._activity, true, "activity_pause", false, Boolean.valueOf(this.activityBA.activity.isFinishing()));
            processBA.setActivityPaused(true);
            mostCurrent = null;
            if (!this.activityBA.activity.isFinishing()) {
                previousOne = new WeakReference<>(this);
            }
            Msgbox.isDismissing = false;
        }
    }

    public void onDestroy() {
        super.onDestroy();
        previousOne = null;
    }

    public void onResume() {
        super.onResume();
        mostCurrent = this;
        Msgbox.isDismissing = false;
        if (this.activityBA != null) {
            BA.handler.post(new ResumeMessage(mostCurrent));
        }
    }

    private static class ResumeMessage implements Runnable {
        private final WeakReference<Activity> activity;

        public ResumeMessage(Activity activity2) {
            this.activity = new WeakReference<>(activity2);
        }

        public void run() {
            if (activitygame.mostCurrent != null && activitygame.mostCurrent == this.activity.get()) {
                activitygame.processBA.setActivityPaused(false);
                Common.Log("** Activity (activitygame) Resume **");
                activitygame.processBA.raiseEvent(activitygame.mostCurrent._activity, "activity_resume", null);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        processBA.onActivityResult(i, i2, intent);
    }

    private static void initializeGlobals() {
        processBA.raiseEvent2(null, true, "globals", false, null);
    }

    public static String _activity_create(boolean z) throws Exception {
        try {
            File file = Common.File;
            File file2 = Common.File;
            _hscore = (int) Double.parseDouble(File.ReadString(File.getDirInternal(), "starbank.dat"));
        } catch (Exception e) {
            processBA.setLastException(e);
            _hscore = 0;
        }
        _score = 0;
        TypefaceWrapper typefaceWrapper = mostCurrent._tf;
        TypefaceWrapper typefaceWrapper2 = Common.Typeface;
        typefaceWrapper.setObject(TypefaceWrapper.LoadFromAssets("MountainsofChristmas.ttf"));
        mostCurrent._activity.LoadLayout("layoutGame", mostCurrent.activityBA);
        _loadad();
        _starsize = Common.DipToCurrent(20);
        _holesize = Common.DipToCurrent(72);
        _bordersize = Common.DipToCurrent(5);
        _maxbanks = 4;
        _banks = 0;
        _lifes = 3;
        mostCurrent._playarea.Initialize(mostCurrent.activityBA, "playarea");
        mostCurrent._activity.AddView((View) mostCurrent._playarea.getObject(), _bordersize, Common.DipToCurrent(100), Common.PerXToCurrent(100.0f, mostCurrent.activityBA) - (_bordersize * 2), (Common.PerYToCurrent(100.0f, mostCurrent.activityBA) - Common.DipToCurrent(100)) - _bordersize);
        PanelWrapper panelWrapper = mostCurrent._playarea;
        Colors colors = Common.Colors;
        panelWrapper.setColor(-1);
        if (z) {
            _tmr.Initialize(processBA, "tmr", 30);
        }
        mostCurrent._blackhole.Initialize(mostCurrent.activityBA, "");
        mostCurrent._playarea.AddView((View) mostCurrent._blackhole.getObject(), Common.Rnd(_holesize + _bordersize, (Common.PerXToCurrent(100.0f, mostCurrent.activityBA) - _holesize) - _bordersize), Common.Rnd(_holesize + _bordersize, (mostCurrent._playarea.getHeight() - _holesize) - _bordersize), _holesize, _holesize);
        PanelWrapper panelWrapper2 = mostCurrent._blackhole;
        File file3 = Common.File;
        panelWrapper2.SetBackgroundImage((Bitmap) Common.LoadBitmap(File.getDirAssets(), "hole1.png").getObject());
        mostCurrent._star.Initialize(mostCurrent.activityBA, "");
        mostCurrent._playarea.AddView((View) mostCurrent._star.getObject(), Common.Rnd(_starsize + _bordersize, (Common.PerXToCurrent(100.0f, mostCurrent.activityBA) - _starsize) - _bordersize), Common.Rnd(_starsize + _bordersize, (mostCurrent._playarea.getHeight() - _starsize) - _bordersize), _starsize, _starsize);
        PanelWrapper panelWrapper3 = mostCurrent._star;
        File file4 = Common.File;
        panelWrapper3.SetBackgroundImage((Bitmap) Common.LoadBitmap(File.getDirAssets(), "star1.png").getObject());
        mostCurrent._activity.setTitle("playarea height:" + BA.NumberToString(mostCurrent._playarea.getHeight()) + ", top:" + BA.NumberToString(mostCurrent._playarea.getTop()));
        double length = (double) (mostCurrent._animstars.length - 1);
        for (int i = 0; ((double) i) <= length; i = (int) (((double) i) + 1.0d)) {
            mostCurrent._animstars[i].InitializeTranslate(mostCurrent.activityBA, "anim", Common.Density, Common.Density, (float) Common.Rnd(-50, 51), (float) Common.Rnd(-50, 51));
            mostCurrent._animstars[i].setDuration((long) Common.Rnd(400, 1000));
            mostCurrent._animstars[i].setRepeatCount(0);
            AnimationWrapper animationWrapper = mostCurrent._animstars[i];
            AnimationWrapper animationWrapper2 = mostCurrent._animstars[i];
            animationWrapper.setRepeatMode(1);
            mostCurrent._panelanim[i].Initialize(mostCurrent.activityBA, "");
            PanelWrapper panelWrapper4 = mostCurrent._panelanim[i];
            File file5 = Common.File;
            panelWrapper4.SetBackgroundImage((Bitmap) Common.LoadBitmap(File.getDirAssets(), "star1.png").getObject());
            mostCurrent._playarea.AddView((View) mostCurrent._panelanim[i].getObject(), mostCurrent._star.getLeft(), mostCurrent._star.getTop(), (int) (((double) mostCurrent._star.getWidth()) / 4.0d), (int) (((double) mostCurrent._star.getHeight()) / 4.0d));
            mostCurrent._panelanim[i].setTag(Integer.valueOf(i));
            mostCurrent._animstars[i].Start((View) mostCurrent._panelanim[i].getObject());
        }
        mostCurrent._star.BringToFront();
        mostCurrent._lblscore.setText("Score: " + BA.NumberToString(_score));
        mostCurrent._lblscore.setTypeface((Typeface) mostCurrent._tf.getObject());
        mostCurrent._lblscore.setTextSize(mostCurrent._lblscore.getTextSize() + 4.0f);
        LabelWrapper labelWrapper = mostCurrent._lblscore;
        Colors colors2 = Common.Colors;
        labelWrapper.setTextColor(Colors.Yellow);
        mostCurrent._lblhighscore.setTypeface((Typeface) mostCurrent._tf.getObject());
        mostCurrent._lblhighscore.setTextSize(mostCurrent._lblscore.getTextSize());
        LabelWrapper labelWrapper2 = mostCurrent._lblhighscore;
        Colors colors3 = Common.Colors;
        labelWrapper2.setTextColor(Colors.Yellow);
        mostCurrent._lblhighscore.setText("High Score: " + BA.NumberToString(_hscore));
        mostCurrent._btnpause.setTypeface((Typeface) mostCurrent._tf.getObject());
        return "";
    }

    public static String _activity_pause(boolean z) throws Exception {
        _tmr.setEnabled(false);
        double length = (double) (mostCurrent._animstars.length - 1);
        for (int i = 0; ((double) i) <= length; i = (int) (((double) i) + 1.0d)) {
            AnimationWrapper animationWrapper = mostCurrent._animstars[i];
            AnimationWrapper.Stop((View) mostCurrent._panelanim[i].getObject());
        }
        mostCurrent._activity.Finish();
        return "";
    }

    public static String _activity_resume() throws Exception {
        mostCurrent._ad.LoadAd();
        return "";
    }

    public static String _anim_animationend() throws Exception {
        double length = (double) (mostCurrent._panelanim.length - 1);
        int i = 0;
        while (true) {
            int i2 = i;
            if (((double) i2) > length) {
                return "";
            }
            if (Common.Sender(mostCurrent.activityBA).equals(mostCurrent._animstars[i2].getObject())) {
                mostCurrent._panelanim[i2].setTop((int) (((double) mostCurrent._star.getTop()) + (((double) _starsize) / 2.0d)));
                mostCurrent._panelanim[i2].setLeft((int) (((double) mostCurrent._star.getLeft()) + (((double) _starsize) / 2.0d)));
                mostCurrent._animstars[i2].setDuration((long) Common.Rnd(400, 1000));
                mostCurrent._animstars[i2].Start((View) mostCurrent._panelanim[i2].getObject());
            }
            i = (int) (((double) i2) + 1.0d);
        }
    }

    public static String _btnpause_click() throws Exception {
        return "";
    }

    public static String _checkcollision() throws Exception {
        if (((double) mostCurrent._star.getLeft()) - _xvel <= 0.0d || (((double) mostCurrent._star.getLeft()) - _xvel) + ((double) mostCurrent._star.getWidth()) >= ((double) mostCurrent._playarea.getWidth())) {
            if (mostCurrent._star.getLeft() < Common.PerXToCurrent(50.0f, mostCurrent.activityBA)) {
                mostCurrent._star.setLeft(0);
            } else {
                mostCurrent._star.setLeft(mostCurrent._playarea.getWidth() - _starsize);
            }
            _xvel *= -1.0d;
            _banks++;
        }
        if (((double) mostCurrent._star.getTop()) - _yvel > 0.0d && (((double) mostCurrent._star.getTop()) - _yvel) + ((double) mostCurrent._star.getHeight()) < ((double) mostCurrent._playarea.getHeight())) {
            return "";
        }
        _yvel *= -1.0d;
        _banks++;
        return "";
    }

    public static boolean _checkgoal() throws Exception {
        if (((double) _getdist((int) (((double) mostCurrent._star.getLeft()) + (((double) _starsize) / 2.0d)), (int) (((double) mostCurrent._star.getTop()) + (((double) _starsize) / 2.0d)), (int) (((double) mostCurrent._blackhole.getLeft()) + (((double) _holesize) / 2.0d)), (int) (((double) mostCurrent._blackhole.getTop()) + (((double) _holesize) / 2.0d)))) <= ((double) _holesize) / 3.0d) {
            return true;
        }
        return false;
    }

    public static int _getdist(int i, int i2, int i3, int i4) throws Exception {
        return (int) Common.Sqrt(Common.Power((double) (i - i3), 2.0d) + Common.Power((double) (i2 - i4), 2.0d));
    }

    public static void initializeProcessGlobals() {
    }

    public static String _globals() throws Exception {
        mostCurrent._playarea = new PanelWrapper();
        mostCurrent._star = new PanelWrapper();
        mostCurrent._blackhole = new PanelWrapper();
        _starsize = 0;
        _holesize = 0;
        _bordersize = 0;
        _xstart = 0;
        _ystart = 0;
        _xend = 0;
        _yend = 0;
        _xvel = 0.0d;
        _yvel = 0.0d;
        _maxbanks = 0;
        _banks = 0;
        mostCurrent._animstars = new AnimationWrapper[10];
        int length = mostCurrent._animstars.length;
        for (int i = 0; i < length; i++) {
            mostCurrent._animstars[i] = new AnimationWrapper();
        }
        mostCurrent._panelanim = new PanelWrapper[10];
        int length2 = mostCurrent._panelanim.length;
        for (int i2 = 0; i2 < length2; i2++) {
            mostCurrent._panelanim[i2] = new PanelWrapper();
        }
        mostCurrent._btnpause = new ButtonWrapper();
        mostCurrent._lblhighscore = new LabelWrapper();
        mostCurrent._lblscore = new LabelWrapper();
        mostCurrent._pnlad = new PanelWrapper();
        _score = 0;
        mostCurrent._tf = new TypefaceWrapper();
        mostCurrent._life1 = new ImageViewWrapper();
        mostCurrent._life2 = new ImageViewWrapper();
        mostCurrent._life3 = new ImageViewWrapper();
        _lifes = 0;
        _cx = 0.0d;
        _cy = 0.0d;
        _hscore = 0;
        mostCurrent._ad = new AdViewWrapper();
        return "";
    }

    public static String _loadad() throws Exception {
        AdViewWrapper adViewWrapper = mostCurrent._ad;
        BA ba = mostCurrent.activityBA;
        main main = mostCurrent._main;
        adViewWrapper.Initialize(ba, "ad", main._admob);
        mostCurrent._pnlad.AddView((View) mostCurrent._ad.getObject(), Common.DipToCurrent(0), 0, Common.DipToCurrent(320), Common.DipToCurrent(50));
        mostCurrent._ad.LoadAd();
        return "";
    }

    public static String _movepanel() throws Exception {
        _xvel = (((double) (_xstart - _xend)) / 10.0d) * 1.5d;
        _yvel = (((double) (_ystart - _yend)) / 10.0d) * 1.5d;
        _cx = (double) mostCurrent._star.getLeft();
        _cy = (double) mostCurrent._star.getTop();
        if (_xvel == 0.0d && _yvel == 0.0d) {
            _tmr.setEnabled(false);
            return "";
        }
        _tmr.setEnabled(true);
        return "";
    }

    public static String _playarea_touch(int i, float f, float f2) throws Exception {
        if (_tmr.getEnabled()) {
            return "";
        }
        ActivityWrapper activityWrapper = mostCurrent._activity;
        if (i == 0) {
            _xstart = (int) f;
            _ystart = (int) f2;
        }
        ActivityWrapper activityWrapper2 = mostCurrent._activity;
        if (i == 1) {
            _xend = (int) f;
            _yend = (int) f2;
            _movepanel();
        }
        return "";
    }

    public static String _process_globals() throws Exception {
        _tmr = new Timer();
        return "";
    }

    public static String _randomisestar() throws Exception {
        mostCurrent._star.SetLayout(Common.Rnd(_starsize + _bordersize, (Common.PerXToCurrent(100.0f, mostCurrent.activityBA) - _starsize) - _bordersize), Common.Rnd(_starsize + _bordersize, (mostCurrent._playarea.getHeight() - _starsize) - _bordersize), _starsize, _starsize);
        mostCurrent._blackhole.SetLayout(Common.Rnd(_holesize - _bordersize, (Common.PerXToCurrent(100.0f, mostCurrent.activityBA) - _holesize) - _bordersize), Common.Rnd(_holesize + _bordersize, (mostCurrent._playarea.getHeight() - _holesize) - _bordersize), _holesize, _holesize);
        for (int _getdist = _getdist((int) (((double) mostCurrent._star.getLeft()) + (((double) _starsize) / 2.0d)), (int) (((double) mostCurrent._star.getTop()) + (((double) _starsize) / 2.0d)), (int) (((double) mostCurrent._blackhole.getLeft()) + (((double) _holesize) / 2.0d)), (int) (((double) mostCurrent._blackhole.getTop()) + (((double) _holesize) / 2.0d))); _getdist < _holesize; _getdist = _getdist((int) (((double) mostCurrent._star.getLeft()) + (((double) _starsize) / 2.0d)), (int) (((double) mostCurrent._star.getTop()) + (((double) _starsize) / 2.0d)), (int) (((double) mostCurrent._blackhole.getLeft()) + (((double) _holesize) / 2.0d)), (int) (((double) mostCurrent._blackhole.getTop()) + (((double) _holesize) / 2.0d)))) {
            mostCurrent._blackhole.SetLayout(Common.Rnd(_holesize - _bordersize, (Common.PerXToCurrent(100.0f, mostCurrent.activityBA) - _holesize) - _bordersize), Common.Rnd(_holesize + _bordersize, (mostCurrent._playarea.getHeight() - _holesize) - _bordersize), _holesize, _holesize);
        }
        return "";
    }

    public static String _tmr_tick() throws Exception {
        _checkcollision();
        _cx -= _xvel;
        _cy -= _yvel;
        mostCurrent._star.setTop((int) _cy);
        mostCurrent._star.setLeft((int) _cx);
        if (_checkgoal()) {
            if (_banks == 0) {
                _lifes--;
                _updatelifes();
                if (_lifes <= 0) {
                    Common.ToastMessageShow("Star not banked! -  Game Over!!!", false);
                    main main = mostCurrent._main;
                    main._lastscore = BA.NumberToString(_score);
                    mostCurrent._activity.Finish();
                    return "";
                }
                Common.ToastMessageShow("Star not banked!", false);
                _tmr.setEnabled(false);
                _randomisestar();
                _banks = 0;
                return "";
            }
            _score += _banks;
            if (_score > _hscore) {
                _updatehighscore();
            }
            mostCurrent._lblscore.setText("Score: " + BA.NumberToString(_score));
            _tmr.setEnabled(false);
            _randomisestar();
            _banks = 0;
            return "";
        } else if (_banks <= _maxbanks) {
            return "";
        } else {
            _lifes--;
            if (_lifes <= 0) {
                Common.ToastMessageShow("Overbanked! -  Game Over!!!", false);
                main main2 = mostCurrent._main;
                main._lastscore = BA.NumberToString(_score);
                mostCurrent._activity.Finish();
                return "";
            }
            _updatelifes();
            Common.ToastMessageShow("Overbanked!", false);
            _tmr.setEnabled(false);
            _randomisestar();
            _banks = 0;
            return "";
        }
    }

    public static String _updatehighscore() throws Exception {
        try {
            _hscore = _score;
            File file = Common.File;
            File file2 = Common.File;
            File.WriteString(File.getDirInternal(), "starbank.dat", BA.NumberToString(_hscore) + "");
            mostCurrent._lblhighscore.setText("High Score: " + BA.NumberToString(_hscore));
        } catch (Exception e) {
            processBA.setLastException(e);
        }
        return "";
    }

    public static String _updatelifes() throws Exception {
        if (_lifes == 3) {
            mostCurrent._life1.setVisible(true);
            mostCurrent._life2.setVisible(true);
            mostCurrent._life3.setVisible(true);
            return "";
        } else if (_lifes == 2) {
            mostCurrent._life1.setVisible(true);
            mostCurrent._life2.setVisible(true);
            mostCurrent._life3.setVisible(false);
            return "";
        } else {
            mostCurrent._life1.setVisible(true);
            mostCurrent._life2.setVisible(false);
            mostCurrent._life3.setVisible(false);
            return "";
        }
    }
}
