package anywheresoftware.b4a;

import java.util.HashMap;
import java.util.Iterator;
import java.util.WeakHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class B4AThreadPool {
    private WeakHashMap<Object, HashMap<Integer, Future<?>>> futures = new WeakHashMap<>();
    private ThreadPoolExecutor pool = new ThreadPoolExecutor(0, 20, 60, TimeUnit.SECONDS, new SynchronousQueue());

    public B4AThreadPool() {
        this.pool.setThreadFactory(new MyThreadFactory(null));
    }

    private static class MyThreadFactory implements ThreadFactory {
        private final ThreadFactory defaultFactory;

        private MyThreadFactory() {
            this.defaultFactory = Executors.defaultThreadFactory();
        }

        /* synthetic */ MyThreadFactory(MyThreadFactory myThreadFactory) {
            this();
        }

        public Thread newThread(Runnable r) {
            Thread t = this.defaultFactory.newThread(r);
            t.setDaemon(true);
            return t;
        }
    }

    public Future<?> submit(Runnable task, Object container, int taskId) {
        Future<?> f = this.pool.submit(task);
        HashMap<Integer, Future<?>> map = this.futures.get(container);
        if (map == null) {
            map = new HashMap<>();
            this.futures.put(container, map);
        }
        Iterator<Future<?>> it = map.values().iterator();
        while (it.hasNext()) {
            if (it.next().isDone()) {
                it.remove();
            }
        }
        map.put(Integer.valueOf(taskId), f);
        return f;
    }

    public boolean isRunning(Object container, int taskId) {
        HashMap<Integer, Future<?>> map = this.futures.get(container);
        if (map == null) {
            return false;
        }
        Future<?> f = map.get(Integer.valueOf(taskId));
        if (f == null) {
            return false;
        }
        return !f.isDone();
    }

    public void markTaskAsFinished(Object container, int taskId) {
        HashMap<Integer, Future<?>> map = this.futures.get(container);
        if (map != null) {
            map.remove(Integer.valueOf(taskId));
        }
    }
}
