package anywheresoftware.b4a.objects;

import android.graphics.RectF;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import anywheresoftware.b4a.AbsObjectWrapper;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.keywords.Common;

@BA.ActivityObject
@BA.ShortName("Animation")
public class AnimationWrapper extends AbsObjectWrapper<Animation> {
    public static final int REPEAT_RESTART = 1;
    public static final int REPEAT_REVERSE = 2;

    private interface MyAnimation {
        void applyTransformation(float f, Transformation transformation);

        boolean getFillBefore();
    }

    public void InitializeAlpha(BA ba, String EventName, float FromAlpha, float ToAlpha) {
        init(ba, EventName, new AlphaAnimation(FromAlpha, ToAlpha));
    }

    public void InitializeScaleCenter(BA ba, String EventName, float FromX, float FromY, float ToX, float ToY, View View) {
        ViewGroup.LayoutParams lp = View.getLayoutParams();
        init(ba, EventName, new MyScaleAnimation(FromX, ToX, FromY, ToY, (float) (lp.width / 2), (float) (lp.height / 2)));
    }

    public void InitializeScale(BA ba, String EventName, float FromX, float FromY, float ToX, float ToY) {
        init(ba, EventName, new MyScaleAnimation(FromX, ToX, FromY, ToY, Common.Density, Common.Density));
    }

    public void InitializeRotate(BA ba, String EventName, float FromDegrees, float ToDegrees) {
        init(ba, EventName, new RotateAnimation(FromDegrees, ToDegrees));
    }

    public void InitializeRotateCenter(BA ba, String EventName, float FromDegrees, float ToDegrees, View View) {
        ViewGroup.LayoutParams lp = View.getLayoutParams();
        init(ba, EventName, new RotateAnimation(FromDegrees, ToDegrees, (float) (lp.width / 2), (float) (lp.height / 2)));
    }

    public void InitializeTranslate(BA ba, String EventName, float FromDX, float FromDY, float ToDX, float ToDY) {
        Animation a = new MyTranslateAnimation(FromDX, ToDX, FromDY, ToDY);
        a.setFillEnabled(true);
        init(ba, EventName, a);
    }

    private void init(final BA ba, String EventName, final Animation a) {
        setObject(a);
        final String eventName = String.valueOf(EventName.toLowerCase(BA.cul)) + "_animationend";
        if (ba.subExists(eventName)) {
            a.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationEnd(Animation animation) {
                    ba.raiseEvent(a, eventName, new Object[0]);
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationStart(Animation animation) {
                }
            });
        }
    }

    public void Start(View View) {
        View.startAnimation((Animation) getObject());
    }

    public static void Stop(View View) {
        View.clearAnimation();
    }

    public int getRepeatCount() {
        return ((Animation) getObject()).getRepeatCount();
    }

    public void setRepeatCount(int value) {
        ((Animation) getObject()).setRepeatCount(value);
    }

    public int getRepeatMode() {
        return ((Animation) getObject()).getRepeatMode();
    }

    public void setRepeatMode(int mode) {
        if (mode == 2) {
            ((Animation) getObject()).setFillEnabled(false);
        }
        ((Animation) getObject()).setRepeatMode(mode);
    }

    public long getDuration() {
        return ((Animation) getObject()).getDuration();
    }

    public void setDuration(long value) {
        ((Animation) getObject()).setDuration(value);
    }

    @BA.Hide
    public static class MyScaleAnimation extends ScaleAnimation implements MyAnimation {
        private AnimationBugFix a = new AnimationBugFix(this);

        public MyScaleAnimation(float fromX, float toX, float fromY, float toY, float pivotX, float pivotY) {
            super(fromX, toX, fromY, toY, pivotX, pivotY);
        }

        public void getInvalidateRegion(int left, int top, int right, int bottom, RectF invalidate, Transformation transformation) {
            this.a.getInvalidateRegion(left, top, right, bottom, invalidate, transformation);
        }

        public void initializeInvalidateRegion(int left, int top, int right, int bottom) {
            this.a.initializeInvalidateRegion(left, top, right, bottom);
        }

        public void applyTransformation(float interpolatedTime, Transformation t) {
            super.applyTransformation(interpolatedTime, t);
        }

        public void reset() {
            this.a.reset();
            super.reset();
        }
    }

    @BA.Hide
    public static class MyTranslateAnimation extends TranslateAnimation implements MyAnimation {
        private AnimationBugFix a = new AnimationBugFix(this);

        public MyTranslateAnimation(float fromXDelta, float toXDelta, float fromYDelta, float toYDelta) {
            super(fromXDelta, toXDelta, fromYDelta, toYDelta);
        }

        public void getInvalidateRegion(int left, int top, int right, int bottom, RectF invalidate, Transformation transformation) {
            this.a.getInvalidateRegion(left, top, right, bottom, invalidate, transformation);
        }

        public void initializeInvalidateRegion(int left, int top, int right, int bottom) {
            this.a.initializeInvalidateRegion(left, top, right, bottom);
        }

        public void applyTransformation(float interpolatedTime, Transformation t) {
            super.applyTransformation(interpolatedTime, t);
        }

        public void reset() {
            this.a.reset();
            super.reset();
        }
    }

    private static class AnimationBugFix {
        RectF mPreviousRegion = new RectF();
        Transformation mPreviousTransformation = new Transformation();
        RectF mRegion = new RectF();
        Transformation mTransformation = new Transformation();
        private MyAnimation parent;

        public AnimationBugFix(MyAnimation parent2) {
            this.parent = parent2;
        }

        public void getInvalidateRegion(int left, int top, int right, int bottom, RectF invalidate, Transformation transformation) {
            RectF tempRegion = this.mRegion;
            RectF previousRegion = this.mPreviousRegion;
            invalidate.set((float) left, (float) top, (float) right, (float) bottom);
            transformation.getMatrix().mapRect(invalidate);
            invalidate.inset(-5.0f, -5.0f);
            tempRegion.set(invalidate);
            invalidate.union(previousRegion);
            previousRegion.set(tempRegion);
            Transformation tempTransformation = this.mTransformation;
            Transformation previousTransformation = this.mPreviousTransformation;
            tempTransformation.set(transformation);
            transformation.set(previousTransformation);
            previousTransformation.set(tempTransformation);
        }

        public void initializeInvalidateRegion(int left, int top, int right, int bottom) {
            RectF region = this.mPreviousRegion;
            region.set((float) left, (float) top, (float) right, (float) bottom);
            region.inset(-1.0f, -1.0f);
            if (this.parent.getFillBefore()) {
                this.parent.applyTransformation(Common.Density, this.mPreviousTransformation);
            }
        }

        public void reset() {
            this.mPreviousRegion.setEmpty();
            this.mPreviousTransformation.clear();
        }
    }
}
