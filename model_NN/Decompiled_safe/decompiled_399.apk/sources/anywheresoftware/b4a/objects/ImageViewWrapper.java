package anywheresoftware.b4a.objects;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.DynamicBuilder;
import java.util.HashMap;

@BA.ShortName("ImageView")
@BA.ActivityObject
public class ImageViewWrapper extends ViewWrapper<ImageView> {
    @BA.Hide
    public void innerInitialize(BA ba, String eventName, boolean keepOldObject) {
        if (!keepOldObject) {
            setObject(new ImageView(ba.context));
        }
        super.innerInitialize(ba, eventName, true);
    }

    public int getGravity() {
        Drawable d = ((ImageView) getObject()).getBackground();
        if (d == null) {
            return 0;
        }
        if (d instanceof BitmapDrawable) {
            return ((BitmapDrawable) d).getGravity();
        }
        if (d instanceof ColorDrawable) {
            return d.getLevel();
        }
        return 0;
    }

    public void setGravity(int value) {
        Drawable d = ((ImageView) getObject()).getBackground();
        if (d == null || !(d instanceof BitmapDrawable)) {
            anywheresoftware.b4a.objects.drawable.BitmapDrawable bd = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
            bd.Initialize(null);
            ((ImageView) getObject()).setBackgroundDrawable((Drawable) bd.getObject());
            d = (Drawable) bd.getObject();
        }
        ((BitmapDrawable) d).setGravity(value);
    }

    public Bitmap getBitmap() {
        Drawable d = ((ImageView) getObject()).getBackground();
        if (d == null || !(d instanceof BitmapDrawable)) {
            return null;
        }
        return ((BitmapDrawable) d).getBitmap();
    }

    public void setBitmap(Bitmap value) {
        int gravity = getGravity();
        anywheresoftware.b4a.objects.drawable.BitmapDrawable bd = new anywheresoftware.b4a.objects.drawable.BitmapDrawable();
        bd.Initialize(value);
        bd.setGravity(gravity);
        setBackground((Drawable) bd.getObject());
    }

    public void SetBackgroundImage(Bitmap Bitmap) {
        setBitmap(Bitmap);
    }

    @BA.Hide
    public static View build(Object prev, HashMap<String, Object> props, boolean designer, Object tag) throws Exception {
        if (prev == null) {
            prev = new ImageView((Context) tag);
        }
        ImageView iv = (ImageView) ViewWrapper.build(prev, props, designer);
        HashMap<String, Object> drawProps = (HashMap) props.get("drawable");
        Drawable d = (Drawable) DynamicBuilder.build(prev, drawProps, designer, null);
        if (d == null) {
            ColorDrawable cd = new ColorDrawable(-1);
            Integer gravity = (Integer) drawProps.get("gravity");
            if (gravity == null) {
                gravity = 0;
            }
            iv.setBackgroundDrawable(cd);
            cd.setLevel(gravity.intValue());
        } else {
            iv.setBackgroundDrawable(d);
        }
        return iv;
    }
}
