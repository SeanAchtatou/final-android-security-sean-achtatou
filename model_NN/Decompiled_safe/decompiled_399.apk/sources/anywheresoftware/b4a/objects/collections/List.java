package anywheresoftware.b4a.objects.collections;

import anywheresoftware.b4a.AbsObjectWrapper;
import anywheresoftware.b4a.BA;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

@BA.ShortName("List")
public class List extends AbsObjectWrapper<java.util.List<Object>> {
    public void Initialize() {
        setObject(new ArrayList());
    }

    public void Initialize2(List Array) {
        setObject((java.util.List) Array.getObject());
    }

    public void Clear() {
        ((java.util.List) getObject()).clear();
    }

    public void Add(Object item) {
        ((java.util.List) getObject()).add(item);
    }

    public void AddAll(List List) {
        ((java.util.List) getObject()).addAll((Collection) List.getObject());
    }

    public void AddAllAt(int Index, List List) {
        ((java.util.List) getObject()).addAll(Index, (Collection) List.getObject());
    }

    public void RemoveAt(int Index) {
        ((java.util.List) getObject()).remove(Index);
    }

    public void InsertAt(int Index, Object Item) {
        ((java.util.List) getObject()).add(Index, Item);
    }

    public Object Get(int Index) {
        return ((java.util.List) getObject()).get(Index);
    }

    public void Set(int Index, Object Item) {
        ((java.util.List) getObject()).set(Index, Item);
    }

    public int getSize() {
        return ((java.util.List) getObject()).size();
    }

    public int IndexOf(Object Item) {
        return ((java.util.List) getObject()).indexOf(Item);
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public void Sort(boolean Ascending) {
        if (Ascending) {
            Collections.sort((java.util.List) getObject());
        } else {
            Collections.sort((java.util.List) getObject(), new Comparator<Comparable>() {
                public int compare(Comparable o1, Comparable o2) {
                    return o2.compareTo(o1);
                }
            });
        }
    }

    public void SortCaseInsensitive(boolean Ascending) {
        if (Ascending) {
            Collections.sort((java.util.List) getObject(), new Comparator<Comparable>() {
                public int compare(Comparable o1, Comparable o2) {
                    return o1.toString().compareToIgnoreCase(o2.toString());
                }
            });
        } else {
            Collections.sort((java.util.List) getObject(), new Comparator<Comparable>() {
                public int compare(Comparable o1, Comparable o2) {
                    return o2.toString().compareToIgnoreCase(o1.toString());
                }
            });
        }
    }
}
