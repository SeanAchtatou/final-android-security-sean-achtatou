package com.tencent.game.smartcard.component;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.activity.AppDetailActivityV5;

/* compiled from: ProGuard */
class a extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f2721a;
    final /* synthetic */ SimpleAppModel b;
    final /* synthetic */ STInfoV2 c;
    final /* synthetic */ NormalSmartCardAppHorizontalNodeWithRank d;

    a(NormalSmartCardAppHorizontalNodeWithRank normalSmartCardAppHorizontalNodeWithRank, int i, SimpleAppModel simpleAppModel, STInfoV2 sTInfoV2) {
        this.d = normalSmartCardAppHorizontalNodeWithRank;
        this.f2721a = i;
        this.b = simpleAppModel;
        this.c = sTInfoV2;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.d.getContext(), AppDetailActivityV5.class);
        intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.f2721a);
        intent.putExtra("simpleModeInfo", this.b);
        this.d.getContext().startActivity(intent);
    }

    public STInfoV2 getStInfo() {
        if (this.c != null) {
            this.c.actionId = 200;
            this.c.updateStatusToDetail(this.b);
        }
        return this.c;
    }
}
