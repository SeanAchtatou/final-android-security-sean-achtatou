package com.tencent.game.smartcard.view;

import android.os.Bundle;
import android.view.View;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.STPageInfo;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
public class a implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ NormalSmartCardRankAggregationSixItem f2724a;

    public a(NormalSmartCardRankAggregationSixItem normalSmartCardRankAggregationSixItem) {
        this.f2724a = normalSmartCardRankAggregationSixItem;
    }

    public void onClick(View view) {
        STPageInfo sTPageInfo;
        int i;
        String str;
        Bundle bundle = new Bundle();
        if (this.f2724a.f1693a instanceof BaseActivity) {
            STPageInfo n = ((BaseActivity) this.f2724a.f1693a).n();
            if (n != null) {
                i = n.f2060a;
                sTPageInfo = n;
            } else {
                i = 0;
                sTPageInfo = n;
            }
        } else {
            sTPageInfo = null;
            i = 0;
        }
        bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, i);
        b.b(this.f2724a.f1693a, this.f2724a.d.o, bundle);
        if (sTPageInfo != null) {
            String str2 = sTPageInfo.b;
            if (this.f2724a.d.u >= 0) {
                str = (this.f2724a.d.u < 9 ? "0" : Constants.STR_EMPTY) + String.valueOf(this.f2724a.d.u + 1);
            } else {
                str = str2;
            }
            STInfoV2 sTInfoV2 = new STInfoV2(sTPageInfo.f2060a, ((com.tencent.game.smartcard.b.b) this.f2724a.d).f + "_000", sTPageInfo.c, com.tencent.assistantv2.st.page.a.b(str, STConst.ST_STATUS_DEFAULT), 200);
            sTInfoV2.status = "06";
            if (sTInfoV2 != null) {
                l.a(sTInfoV2);
            }
        }
    }
}
