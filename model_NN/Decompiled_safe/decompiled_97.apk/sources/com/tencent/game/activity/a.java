package com.tencent.game.activity;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.protocol.jce.TagGroup;
import com.tencent.assistantv2.st.model.STCommonInfo;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class a extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GameCategoryDetailActivity f2609a;

    a(GameCategoryDetailActivity gameCategoryDetailActivity) {
        this.f2609a = gameCategoryDetailActivity;
    }

    public void onTMAClick(View view) {
        TagGroup tagGroup = (TagGroup) view.getTag();
        if (tagGroup != null) {
            long unused = this.f2609a.A = tagGroup.a();
            this.f2609a.w.a(this.f2609a.f(), this.f2609a.y, this.f2609a.A);
            if (this.f2609a.v != null) {
                this.f2609a.v.j();
                this.f2609a.v.k();
                this.f2609a.v.a(this.f2609a.A);
            }
        }
    }

    public STInfoV2 getStInfo(View view) {
        int i;
        try {
            int intValue = ((Integer) view.getTag(R.id.category_detail_btn_index)).intValue();
            TagGroup tagGroup = (TagGroup) view.getTag();
            if (tagGroup != null) {
                long unused = this.f2609a.A = tagGroup.a();
            }
            i = intValue;
        } catch (Exception e) {
            Exception exc = e;
            i = 0;
            exc.printStackTrace();
        }
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f2609a, 200);
        buildSTInfo.updateContentId(STCommonInfo.ContentIdType.CATEGORY, this.f2609a.y + "_" + this.f2609a.A);
        buildSTInfo.slotId = com.tencent.assistantv2.st.page.a.a("05", i);
        return buildSTInfo;
    }
}
