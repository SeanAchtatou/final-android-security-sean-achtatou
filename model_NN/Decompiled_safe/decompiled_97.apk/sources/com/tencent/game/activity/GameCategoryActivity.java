package com.tencent.game.activity;

import android.os.Bundle;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.adapter.AppCategoryListAdapter;
import com.tencent.assistant.component.invalidater.ViewPageScrollListener;
import com.tencent.assistant.protocol.jce.AppCategory;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.game.component.GameCategoryListPage;
import com.tencent.game.d.a.a;
import com.tencent.game.d.j;
import java.util.List;

/* compiled from: ProGuard */
public class GameCategoryActivity extends BaseActivity implements a {
    private SecondNavigationTitleViewV5 n;
    private j u;
    private GameCategoryListPage v;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.act_game_category);
        w();
        x();
    }

    private void w() {
        this.n = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.n.i();
        this.n.d(false);
        this.n.a(this);
        this.n.b(getResources().getString(R.string.game_category_title));
        this.n.d(v());
        this.v = (GameCategoryListPage) findViewById(R.id.clp_act_list);
    }

    private void x() {
        this.u = new j();
        this.u.register(this);
        this.u.b();
        this.v.a(this.u);
    }

    private void y() {
        this.v.a(t());
        this.v.d();
        this.v.setVisibility(0);
        this.v.a(new ViewPageScrollListener());
        List<ColorCardItem> c = this.u.c(t());
        List<AppCategory> a2 = this.u.a(t());
        List<AppCategory> b = this.u.b(t());
        AppCategoryListAdapter appCategoryListAdapter = new AppCategoryListAdapter(this, this.v, u(), null, null, this.u.a());
        appCategoryListAdapter.a(c, a2, b);
        this.v.a(appCategoryListAdapter);
        this.v.c();
    }

    /* access modifiers changed from: protected */
    public long t() {
        return -2;
    }

    /* access modifiers changed from: protected */
    public AppCategoryListAdapter.CategoryType u() {
        return AppCategoryListAdapter.CategoryType.CATEGORYTYPEGAME;
    }

    /* access modifiers changed from: protected */
    public int v() {
        return 0;
    }

    public int f() {
        return STConst.ST_PAGE_GAME_CATEGORY;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.n.l();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.n.m();
    }

    public void a(int i, int i2, List<ColorCardItem> list, List<AppCategory> list2, List<AppCategory> list3) {
        y();
    }
}
