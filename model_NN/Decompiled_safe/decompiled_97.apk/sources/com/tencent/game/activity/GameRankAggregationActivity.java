package com.tencent.game.activity;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.BaseAdapter;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.component.invalidater.ViewPageScrollListener;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.game.adapter.GameRankAggregationListAdapter;
import com.tencent.game.component.GameRankAggregationListView;
import com.tencent.game.component.m;
import com.tencent.game.d.a.d;
import com.tencent.game.d.am;
import com.tencent.game.smartcard.b.b;
import java.util.List;

/* compiled from: ProGuard */
public class GameRankAggregationActivity extends BaseActivity implements m, d {
    private SecondNavigationTitleViewV5 n;
    private am u;
    private int v;
    /* access modifiers changed from: private */
    public GameRankAggregationListView w;
    private LoadingView x;
    private NormalErrorRecommendPage y;
    private View.OnClickListener z = new m(this);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.act_game_rank_aggregation);
        w();
        x();
    }

    private void w() {
        this.n = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.n.i();
        this.n.d(false);
        this.n.a(this);
        this.n.b(getResources().getString(R.string.ranking));
        this.n.d(t());
        this.w = (GameRankAggregationListView) findViewById(R.id.applist);
        this.w.setVisibility(8);
        this.w.setDivider(null);
        this.w.setSelector(new ColorDrawable(0));
        this.w.setCacheColorHint(17170445);
        this.w.a((m) this);
        this.x = (LoadingView) findViewById(R.id.loading_view);
        this.x.setVisibility(0);
        this.y = (NormalErrorRecommendPage) findViewById(R.id.error_page);
        this.y.setButtonClickListener(this.z);
    }

    private void x() {
        this.u = new am();
        this.u.register(this);
        this.u.b();
    }

    private void y() {
        this.w.r();
        a(new ViewPageScrollListener());
        a(new GameRankAggregationListAdapter(this, this.v, this.u.a()));
        this.w.s();
    }

    /* access modifiers changed from: protected */
    public int t() {
        return 0;
    }

    public int f() {
        if (this.v != 1 && this.v == 2) {
            return STConst.ST_PAGE_GAME_RANK_SIX;
        }
        return STConst.ST_PAGE_GAME_RANK_THREE;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.n.l();
        this.w.r();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.n.m();
        this.w.q();
    }

    public void a(int i, int i2, List<b> list, int i3) {
        this.v = i3;
        y();
    }

    public void b(int i) {
        this.w.setVisibility(8);
        this.y.setVisibility(0);
        this.x.setVisibility(8);
        this.y.setErrorType(i);
    }

    public void u() {
        this.w.setVisibility(0);
        this.y.setVisibility(8);
        this.x.setVisibility(8);
    }

    public void v() {
        this.x.setVisibility(0);
        this.w.setVisibility(8);
        this.y.setVisibility(8);
    }

    public void a(ViewPageScrollListener viewPageScrollListener) {
        this.w.a(viewPageScrollListener);
    }

    public void a(BaseAdapter baseAdapter) {
        this.w.setAdapter(baseAdapter);
    }
}
