package com.tencent.game.activity;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class g extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ GameDesktopShortActivity f2620a;
    private String b;
    private String c;

    g(GameDesktopShortActivity gameDesktopShortActivity, String str, String str2) {
        this.f2620a = gameDesktopShortActivity;
        this.b = str;
        this.c = str2;
    }

    public void onTMAClick(View view) {
        b.a(this.f2620a, this.b);
        if (!this.f2620a.isFinishing()) {
            this.f2620a.finish();
        }
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f2620a, 200);
        if (buildSTInfo != null) {
            buildSTInfo.slotId = this.c;
        }
        return buildSTInfo;
    }
}
