package com.tencent.game.d;

import com.tencent.assistant.protocol.jce.GftGetNavigationResponse;
import com.tencent.assistant.protocol.jce.SubNavigationNode;
import com.tencent.assistant.utils.XLog;
import com.tencent.game.activity.GameRankTabType;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class y {

    /* renamed from: a  reason: collision with root package name */
    public long f2716a;
    public List<z> b = new ArrayList(4);
    public int c;

    private y() {
        this.b.add(new z("新游", GameRankTabType.NEWGAME.ordinal(), 0, null, -2, 11, 20, (byte) 0));
        this.b.add(new z("单机", GameRankTabType.ONEPC.ordinal(), 0, null, -2, 8, 20, (byte) 0));
        this.b.add(new z("网游", GameRankTabType.NETGAME.ordinal(), 0, null, -2, 9, 20, (byte) 0));
        this.b.add(new z("人气", GameRankTabType.RENQI.ordinal(), 0, null, -2, 10, 20, (byte) 0));
        this.c = 2;
    }

    public static y a(GftGetNavigationResponse gftGetNavigationResponse) {
        y yVar = null;
        try {
            yVar = b(gftGetNavigationResponse);
        } catch (Exception e) {
            XLog.e("GameGetNavigationEngine", "parse tab container fail,type:.ex:" + e);
            e.printStackTrace();
        }
        if (yVar == null) {
            return new y();
        }
        return yVar;
    }

    public static y b(GftGetNavigationResponse gftGetNavigationResponse) {
        if (gftGetNavigationResponse == null || gftGetNavigationResponse.b() == null || gftGetNavigationResponse.b().size() <= 0) {
            return null;
        }
        y yVar = new y();
        yVar.c = 2;
        yVar.f2716a = gftGetNavigationResponse.a();
        ArrayList<SubNavigationNode> b2 = gftGetNavigationResponse.b();
        ArrayList arrayList = new ArrayList(gftGetNavigationResponse.b().size());
        Iterator<SubNavigationNode> it = b2.iterator();
        while (it.hasNext()) {
            SubNavigationNode next = it.next();
            arrayList.add(new z(next.f1571a, next.b, next.c, next.d, next.e, next.f, next.g, next.h));
        }
        yVar.b = arrayList;
        return yVar;
    }
}
