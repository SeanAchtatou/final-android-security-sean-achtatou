package com.tencent.nucleus.manager.floatingwindow;

import android.view.animation.Animation;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.manager.t;

/* compiled from: ProGuard */
class g implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ f f2903a;

    g(f fVar) {
        this.f2903a = fVar;
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.floatingwindow.FloatWindowBigView.a(com.tencent.nucleus.manager.floatingwindow.FloatWindowBigView, boolean):boolean
     arg types: [com.tencent.nucleus.manager.floatingwindow.FloatWindowBigView, int]
     candidates:
      com.tencent.nucleus.manager.floatingwindow.FloatWindowBigView.a(com.tencent.nucleus.manager.floatingwindow.FloatWindowBigView, int):int
      com.tencent.nucleus.manager.floatingwindow.FloatWindowBigView.a(com.tencent.nucleus.manager.floatingwindow.FloatWindowBigView, boolean):boolean */
    public void onAnimationEnd(Animation animation) {
        boolean unused = this.f2903a.f2902a.z = true;
        this.f2903a.f2902a.q.setVisibility(0);
        try {
            this.f2903a.f2902a.B.setBackgroundResource(R.drawable.floating_big_window_accelerate_circle_selector);
        } catch (Throwable th) {
            t.a().b();
        }
    }
}
