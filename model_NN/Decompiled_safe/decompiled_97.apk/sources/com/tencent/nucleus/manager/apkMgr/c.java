package com.tencent.nucleus.manager.apkMgr;

import android.os.Message;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class c extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ LocalApkInfo f2766a;
    final /* synthetic */ STInfoV2 b;
    final /* synthetic */ a c;

    c(a aVar, LocalApkInfo localApkInfo, STInfoV2 sTInfoV2) {
        this.c = aVar;
        this.f2766a = localApkInfo;
        this.b = sTInfoV2;
    }

    public void onTMAClick(View view) {
        this.c.f2764a.sendMessage(Message.obtain(this.c.f2764a, 110001, this.f2766a));
    }

    public STInfoV2 getStInfo() {
        this.b.actionId = 305;
        this.b.status = STConst.ST_STATUS_DEFAULT;
        return this.b;
    }
}
