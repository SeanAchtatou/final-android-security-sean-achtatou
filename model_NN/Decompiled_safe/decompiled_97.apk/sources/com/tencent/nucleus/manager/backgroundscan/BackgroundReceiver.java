package com.tencent.nucleus.manager.backgroundscan;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
public class BackgroundReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        XLog.i("BackgroundScan", "<receiver> received user present action");
        if ("android.intent.action.USER_PRESENT".equals(intent.getAction())) {
            d.b().c();
        }
    }
}
