package com.tencent.nucleus.manager.about;

import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
class ab extends ae {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ HelperFeedbackActivity f2735a;

    ab(HelperFeedbackActivity helperFeedbackActivity) {
        this.f2735a = helperFeedbackActivity;
    }

    public void a(int i, int i2) {
        String unused = this.f2735a.B = (String) null;
        if (i == 0 && i2 == 0) {
            this.f2735a.v.a().setText(Constants.STR_EMPTY);
            this.f2735a.v.a().setSelection(0);
            Toast.makeText(this.f2735a, (int) R.string.feedback_send_success, 0).show();
            this.f2735a.finish();
            return;
        }
        Toast.makeText(this.f2735a, (int) R.string.feedback_send_failed, 0).show();
    }
}
