package com.tencent.nucleus.manager.spaceclean;

import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/* compiled from: ProGuard */
public class n {

    /* renamed from: a  reason: collision with root package name */
    public int f3037a = 0;
    public String b = Constants.STR_EMPTY;
    public long c = 0;
    public long d = 0;
    public String e = Constants.STR_EMPTY;
    public ArrayList<SubRubbishInfo> f = new ArrayList<>();
    public boolean g = false;
    public int h = Integer.MAX_VALUE;
    private Comparator<SubRubbishInfo> i = new o(this);

    public void a(SubRubbishInfo subRubbishInfo) {
        if (subRubbishInfo != null) {
            this.f.add(subRubbishInfo);
        }
    }

    public void a(ArrayList<SubRubbishInfo> arrayList) {
        if (arrayList != null) {
            this.f = arrayList;
        }
    }

    public synchronized void a() {
        Collections.sort(this.f, this.i);
    }
}
