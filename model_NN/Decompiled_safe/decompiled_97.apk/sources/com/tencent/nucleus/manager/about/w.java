package com.tencent.nucleus.manager.about;

import android.text.Editable;
import android.text.TextWatcher;

/* compiled from: ProGuard */
class w implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ HelperFeedbackActivity f2759a;

    w(HelperFeedbackActivity helperFeedbackActivity) {
        this.f2759a = helperFeedbackActivity;
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void afterTextChanged(Editable editable) {
        if (editable.length() >= 300) {
            this.f2759a.v.b();
        } else {
            this.f2759a.v.c();
        }
    }
}
