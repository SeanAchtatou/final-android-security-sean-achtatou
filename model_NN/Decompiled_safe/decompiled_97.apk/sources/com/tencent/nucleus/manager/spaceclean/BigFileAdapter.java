package com.tencent.nucleus.manager.spaceclean;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.utils.bm;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

/* compiled from: ProGuard */
public class BigFileAdapter extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private ArrayList<n> f3003a = new ArrayList<>();
    private Context b;
    private boolean c = true;
    private Handler d = null;
    private b e = null;

    public BigFileAdapter(Context context) {
        this.b = context;
        RubbishItemView.f3007a = true;
    }

    public void a(ArrayList<n> arrayList) {
        if (arrayList != null && !arrayList.isEmpty()) {
            this.f3003a.clear();
            this.f3003a.addAll(arrayList);
            Collections.sort(this.f3003a, new a(this));
            Iterator<n> it = this.f3003a.iterator();
            while (it.hasNext()) {
                it.next().a();
            }
            notifyDataSetChanged();
        }
    }

    public int getCount() {
        if (this.f3003a != null) {
            return this.f3003a.size();
        }
        return 0;
    }

    public Object getItem(int i) {
        if (this.f3003a != null) {
            return this.f3003a.get(i);
        }
        return null;
    }

    public long getItemId(int i) {
        return 0;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        RubbishItemView rubbishItemView;
        n nVar = null;
        if (this.f3003a != null && i <= this.f3003a.size() - 1) {
            nVar = this.f3003a.get(i);
        }
        if (nVar != null) {
            if (this.c && i == 0) {
                nVar.g = true;
                this.c = false;
            }
            STInfoV2 a2 = a(i);
            if (view == null) {
                RubbishItemView rubbishItemView2 = new RubbishItemView(this.b, nVar, a2, true, this.b.getResources().getDimension(R.dimen.big_file_rubbish_detail_item_height));
                rubbishItemView2.a(this.d);
                rubbishItemView = rubbishItemView2;
            } else {
                ((RubbishItemView) view).a(nVar, a2, this.b.getResources().getDimension(R.dimen.big_file_rubbish_detail_item_height));
                rubbishItemView = view;
            }
            if (i == 0) {
                ((RubbishItemView) rubbishItemView).a(0);
                return rubbishItemView;
            }
            ((RubbishItemView) rubbishItemView).a(8);
            return rubbishItemView;
        } else if (view != null) {
            return view;
        } else {
            RubbishItemView rubbishItemView3 = new RubbishItemView(this.b);
            rubbishItemView3.a(this.d);
            rubbishItemView3.a(true);
            return rubbishItemView3;
        }
    }

    public void a(Handler handler) {
        this.d = handler;
    }

    private STInfoV2 a(int i) {
        if (this.e == null) {
            this.e = new b();
        }
        String b2 = b(i);
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.b, 100);
        buildSTInfo.slotId = b2;
        this.e.exposure(buildSTInfo);
        return buildSTInfo;
    }

    private String b(int i) {
        if (i == 0) {
            return "00_" + bm.a(i + 1);
        }
        if (i == 1) {
            return "01_" + bm.a(i + 1);
        }
        return null;
    }

    public boolean hasStableIds() {
        return true;
    }
}
