package com.tencent.nucleus.manager.apkuninstall;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.localres.model.LocalApkInfo;

/* compiled from: ProGuard */
class h extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ i f2790a;
    final /* synthetic */ LocalApkInfo b;
    final /* synthetic */ PreInstalledAppListAdapter c;

    h(PreInstalledAppListAdapter preInstalledAppListAdapter, i iVar, LocalApkInfo localApkInfo) {
        this.c = preInstalledAppListAdapter;
        this.f2790a = iVar;
        this.b = localApkInfo;
    }

    public void onRightBtnClick() {
        this.c.b(this.f2790a, this.b);
    }

    public void onLeftBtnClick() {
    }

    public void onCancell() {
    }
}
