package com.tencent.nucleus.manager.apkuninstall;

import android.widget.AbsListView;
import com.tencent.assistant.component.invalidater.CommonViewInvalidater;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class e extends CommonViewInvalidater implements AbsListView.OnScrollListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PreInstallAppListView f2787a;

    public e(PreInstallAppListView preInstallAppListView) {
        this.f2787a = preInstallAppListView;
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
    }

    public void onScrollStateChanged(AbsListView absListView, int i) {
        boolean z = true;
        if (!(i == 0 || i == 1 || (i == 2 && this.f2787a.l()))) {
            z = false;
        }
        this.canHandleMsg = z;
        if (canHandleMessage()) {
            handleQueueMsg();
        }
        if (i == 2) {
            XLog.d("donald", "---yVelocity = " + this.f2787a.k());
        }
    }

    /* access modifiers changed from: protected */
    public boolean canHandleMessage() {
        return this.canHandleMsg;
    }
}
