package com.tencent.nucleus.manager.floatingwindow;

import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.m;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
class q implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ n f2913a;

    q(n nVar) {
        this.f2913a = nVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
     arg types: [java.lang.String, boolean]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(int, byte[]):void
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean */
    public void run() {
        if (AstApp.i().l() && AstApp.m() != null) {
            try {
                Dialog dialog = new Dialog(AstApp.m(), R.style.dialog);
                Window window = dialog.getWindow();
                window.setLayout(-1, -2);
                WindowManager.LayoutParams attributes = window.getAttributes();
                attributes.gravity = 17;
                attributes.width = (int) (((float) window.getWindowManager().getDefaultDisplay().getWidth()) * 0.867f);
                window.setAttributes(attributes);
                View inflate = LayoutInflater.from(AstApp.m()).inflate((int) R.layout.float_window_create_dialog_layout, (ViewGroup) null);
                ((Button) inflate.findViewById(R.id.positive_btn)).setOnClickListener(new r(this, dialog));
                dialog.setContentView(inflate, new WindowManager.LayoutParams(-1, -2));
                if (AstApp.i().l()) {
                    dialog.show();
                    m.a().b("key_has_show_float_window_create_tips", (Object) true);
                }
            } catch (Throwable th) {
                XLog.e("floatingwindow", "showFloatWindowCreateTips throws exception. maybe low memory.");
                t.a().b();
            }
        }
    }
}
