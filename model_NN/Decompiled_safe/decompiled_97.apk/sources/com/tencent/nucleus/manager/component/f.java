package com.tencent.nucleus.manager.component;

import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ah;

/* compiled from: ProGuard */
public class f {

    /* renamed from: a  reason: collision with root package name */
    m f2865a;
    k b;
    AnimationExpandableListView c;

    public f(m mVar, k kVar, AnimationExpandableListView animationExpandableListView) {
        this.f2865a = mVar;
        this.b = kVar;
        this.c = animationExpandableListView;
    }

    public void a(l lVar) {
        this.c.a(-1, new g(this, lVar));
    }

    public void b(l lVar) {
        this.f2865a.e();
        c(lVar);
    }

    /* access modifiers changed from: private */
    public void c(l lVar) {
        Object h = this.f2865a.h();
        n g = this.f2865a.g();
        if (h == null || g == null) {
            XLog.d("ManagerGeneralController", "deleteAllSelectItemInner---finish");
            if (lVar != null) {
                lVar.a();
            }
            ah.a().post(new h(this));
            return;
        }
        XLog.d("ManagerGeneralController", "deleteAllSelectItemInner---position = " + g.b + "group.pos = " + g.f2870a);
        lVar.a(h);
        ah.a().post(new i(this, g, lVar, h));
    }
}
