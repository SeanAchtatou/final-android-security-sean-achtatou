package com.tencent.nucleus.manager.about;

import android.view.View;

/* compiled from: ProGuard */
class k implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FeedbackInputView f2749a;

    k(FeedbackInputView feedbackInputView) {
        this.f2749a = feedbackInputView;
    }

    public void onClick(View view) {
        this.f2749a.b.setFocusable(true);
        this.f2749a.b.setCursorVisible(true);
        this.f2749a.b.setFocusableInTouchMode(true);
    }
}
