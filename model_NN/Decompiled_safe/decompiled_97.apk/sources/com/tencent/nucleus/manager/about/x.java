package com.tencent.nucleus.manager.about;

import android.view.KeyEvent;
import android.view.View;

/* compiled from: ProGuard */
class x implements View.OnKeyListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ HelperFeedbackActivity f2760a;

    x(HelperFeedbackActivity helperFeedbackActivity) {
        this.f2760a = helperFeedbackActivity;
    }

    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (i != 66 || 1 != keyEvent.getAction()) {
            return false;
        }
        this.f2760a.t();
        return false;
    }
}
