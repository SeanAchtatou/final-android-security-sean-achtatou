package com.tencent.nucleus.manager.floatingwindow;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.manager.t;
import com.tencent.nucleus.manager.floatingwindow.Rotate3dAnimation;

/* compiled from: ProGuard */
public class FloatWindowResultView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    public static int f2885a;
    public static int b;
    private View c;
    private TextView d;
    private TextView e;
    private View f;
    private ImageView g;
    private int h;

    public FloatWindowResultView(Context context) {
        super(context);
        try {
            LayoutInflater.from(context).inflate((int) R.layout.float_window_result_layout, this);
            f2885a = (int) context.getResources().getDimension(R.dimen.floating_window_result_view_width);
            b = (int) context.getResources().getDimension(R.dimen.floating_window_result_view_layout_height);
            this.h = (int) context.getResources().getDimension(R.dimen.floating_window_result_rocket_width);
            this.c = findViewById(R.id.ll_content);
            this.d = (TextView) findViewById(R.id.tv_mem_free_tips);
            this.e = (TextView) findViewById(R.id.tv_speedup_tips);
            this.f = findViewById(R.id.flash_line);
            this.g = (ImageView) findViewById(R.id.iv_rocket);
            a();
        } catch (Throwable th) {
            t.a().b();
        }
    }

    private void a() {
        this.f.startAnimation(b());
        this.c.startAnimation(c());
        this.g.startAnimation(d());
    }

    private Animation b() {
        AnimationSet animationSet = new AnimationSet(true);
        ScaleAnimation scaleAnimation = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setFillAfter(true);
        scaleAnimation.setDuration(300);
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setDuration(300);
        AlphaAnimation alphaAnimation2 = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation2.setFillAfter(true);
        alphaAnimation2.setStartOffset(300);
        alphaAnimation2.setDuration(400);
        animationSet.addAnimation(scaleAnimation);
        animationSet.addAnimation(alphaAnimation);
        animationSet.addAnimation(alphaAnimation2);
        animationSet.setFillAfter(true);
        return animationSet;
    }

    private Animation c() {
        ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 1.0f, 0.0f, 1.0f, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setFillAfter(true);
        scaleAnimation.setStartOffset(300);
        scaleAnimation.setDuration(400);
        return scaleAnimation;
    }

    private Animation d() {
        AnimationSet animationSet = new AnimationSet(true);
        Rotate3dAnimation rotate3dAnimation = new Rotate3dAnimation(-90.0f, 20.0f, ((float) this.h) / 2.0f, ((float) this.h) / 2.0f, 0.0f, false);
        rotate3dAnimation.a(Rotate3dAnimation.RotateAxis.Y);
        rotate3dAnimation.setStartOffset(700);
        rotate3dAnimation.setDuration(400);
        rotate3dAnimation.setFillAfter(true);
        Rotate3dAnimation rotate3dAnimation2 = new Rotate3dAnimation(20.0f, 0.0f, ((float) this.h) / 2.0f, ((float) this.h) / 2.0f, 0.0f, false);
        rotate3dAnimation2.a(Rotate3dAnimation.RotateAxis.Y);
        rotate3dAnimation2.setStartOffset(1100);
        rotate3dAnimation2.setDuration(100);
        rotate3dAnimation2.setFillAfter(true);
        Rotate3dAnimation rotate3dAnimation3 = new Rotate3dAnimation(0.0f, -20.0f, ((float) this.h) / 2.0f, ((float) this.h) / 2.0f, 0.0f, false);
        rotate3dAnimation3.a(Rotate3dAnimation.RotateAxis.Y);
        rotate3dAnimation3.setStartOffset(1200);
        rotate3dAnimation3.setDuration(100);
        rotate3dAnimation3.setFillAfter(true);
        animationSet.addAnimation(rotate3dAnimation);
        animationSet.addAnimation(rotate3dAnimation2);
        animationSet.addAnimation(rotate3dAnimation3);
        animationSet.setFillAfter(true);
        return animationSet;
    }

    private Animation b(Animation.AnimationListener animationListener) {
        AnimationSet animationSet = new AnimationSet(true);
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setDuration(300);
        ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 0.0f, 1.0f, 0.0f, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setStartOffset(300);
        scaleAnimation.setFillAfter(true);
        scaleAnimation.setDuration(300);
        AlphaAnimation alphaAnimation2 = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation2.setStartOffset(300);
        alphaAnimation2.setFillAfter(true);
        alphaAnimation2.setDuration(400);
        animationSet.addAnimation(alphaAnimation);
        animationSet.addAnimation(scaleAnimation);
        animationSet.addAnimation(alphaAnimation2);
        animationSet.setAnimationListener(animationListener);
        animationSet.setFillAfter(true);
        return animationSet;
    }

    private Animation e() {
        ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 1.0f, 1.0f, 0.0f, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setFillAfter(true);
        scaleAnimation.setDuration(400);
        return scaleAnimation;
    }

    public void a(String str) {
        if (this.e != null && str != null) {
            this.e.setText(str);
        }
    }

    public void a(CharSequence charSequence) {
        if (this.d != null) {
            if (charSequence != null) {
                this.d.setText(charSequence);
                this.d.setVisibility(0);
                return;
            }
            this.d.setVisibility(8);
        }
    }

    public void a(Animation.AnimationListener animationListener) {
        if (this.f != null && this.c != null && this.g != null) {
            this.f.startAnimation(b(animationListener));
            this.c.startAnimation(e());
            this.g.startAnimation(e());
        }
    }
}
