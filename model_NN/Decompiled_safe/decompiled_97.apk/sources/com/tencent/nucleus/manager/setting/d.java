package com.tencent.nucleus.manager.setting;

import android.content.SharedPreferences;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.model.ItemElement;
import com.tencent.nucleus.socialcontact.tagpage.MiniVideoSetDialog;
import com.tencent.nucleus.socialcontact.tagpage.i;

/* compiled from: ProGuard */
class d implements i {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ItemElement f2998a;
    final /* synthetic */ SettingActivity b;

    d(SettingActivity settingActivity, ItemElement itemElement) {
        this.b = settingActivity;
        this.f2998a = itemElement;
    }

    public void a(MiniVideoSetDialog.ItemIndex itemIndex) {
        SharedPreferences.Editor edit = SettingActivity.t().edit();
        switch (h.f3002a[itemIndex.ordinal()]) {
            case 1:
                XLog.i("SettingActivity", "*** 数据网络和Wifi ***");
                edit.putInt("item_index", MiniVideoSetDialog.ItemIndex.MOBILE_WIFI.ordinal());
                edit.commit();
                this.f2998a.b = this.b.a(MiniVideoSetDialog.ItemIndex.MOBILE_WIFI);
                break;
            case 2:
                XLog.i("SettingActivity", "*** 仅Wifi ***");
                edit.putInt("item_index", MiniVideoSetDialog.ItemIndex.ONLY_WIFI.ordinal());
                edit.commit();
                this.f2998a.b = this.b.a(MiniVideoSetDialog.ItemIndex.ONLY_WIFI);
                break;
            case 3:
                XLog.i("SettingActivity", "*** 关闭 ***");
                edit.putInt("item_index", MiniVideoSetDialog.ItemIndex.CLOSE.ordinal());
                edit.commit();
                this.f2998a.b = this.b.a(MiniVideoSetDialog.ItemIndex.CLOSE);
                break;
        }
        this.b.w.notifyDataSetChanged();
    }
}
