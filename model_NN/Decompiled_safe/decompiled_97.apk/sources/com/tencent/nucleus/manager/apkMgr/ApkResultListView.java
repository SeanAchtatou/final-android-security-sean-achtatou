package com.tencent.nucleus.manager.apkMgr;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.ListViewScrollListener;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.ah;
import com.tencent.nucleus.manager.component.AnimationExpandableListView;
import java.util.ArrayList;
import java.util.Map;

/* compiled from: ProGuard */
public class ApkResultListView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    public a f2763a;
    private Context b;
    private LayoutInflater c;
    /* access modifiers changed from: private */
    public View d;
    /* access modifiers changed from: private */
    public AnimationExpandableListView e;
    /* access modifiers changed from: private */
    public RelativeLayout f;
    /* access modifiers changed from: private */
    public TextView g;
    /* access modifiers changed from: private */
    public TextView h;
    /* access modifiers changed from: private */
    public int i;
    /* access modifiers changed from: private */
    public int j;
    /* access modifiers changed from: private */
    public int k;
    private ListViewScrollListener l;

    public ApkResultListView(Context context) {
        this(context, null);
    }

    public ApkResultListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f = null;
        this.g = null;
        this.h = null;
        this.i = 0;
        this.j = -1;
        this.l = new m(this);
        this.b = context;
        this.c = (LayoutInflater) this.b.getSystemService("layout_inflater");
        d();
    }

    public AnimationExpandableListView a() {
        return this.e;
    }

    public a b() {
        return this.f2763a;
    }

    private void d() {
        this.d = this.c.inflate((int) R.layout.apkresult_component_view, this);
        this.e = (AnimationExpandableListView) this.d.findViewById(R.id.list_view);
        this.e.setGroupIndicator(null);
        this.e.setDivider(null);
        this.e.setChildDivider(null);
        this.e.setSelector(R.drawable.transparent_selector);
        this.e.setOnScrollListener(this.l);
        this.e.setOnGroupClickListener(new l(this));
        this.f2763a = new a(this.b, this.e);
        this.e.setAdapter(this.f2763a);
        this.i = this.f2763a.getGroupCount();
        this.g = (TextView) this.d.findViewById(R.id.group_title);
        this.h = (TextView) this.d.findViewById(R.id.select_all);
    }

    /* access modifiers changed from: private */
    public int e() {
        int i2 = this.k;
        int pointToPosition = this.e.pointToPosition(0, this.k);
        if (pointToPosition == -1 || ExpandableListView.getPackedPositionGroup(this.e.getExpandableListPosition(pointToPosition)) == this.j) {
            return i2;
        }
        View expandChildAt = this.e.getExpandChildAt(pointToPosition - this.e.getFirstVisiblePosition());
        if (expandChildAt != null) {
            return expandChildAt.getTop();
        }
        return 100;
    }

    /* access modifiers changed from: private */
    public void f() {
        for (int i2 = 0; i2 < this.f2763a.getGroupCount(); i2++) {
            this.e.expandGroup(i2);
        }
    }

    public void a(Map<Integer, ArrayList<LocalApkInfo>> map, boolean z, boolean z2) {
        ah.a().post(new p(this, map, z2));
    }

    public void c() {
        ah.a().post(new q(this));
    }

    public void a(Handler handler) {
        this.f2763a.f2764a = handler;
    }

    public void a(ExpandableListView.OnChildClickListener onChildClickListener) {
        this.e.setOnChildClickListener(onChildClickListener);
    }

    public String a(int i2, int i3) {
        if (this.f2763a != null) {
            return this.f2763a.b(i2, i3);
        }
        return STConst.ST_DEFAULT_SLOT;
    }
}
