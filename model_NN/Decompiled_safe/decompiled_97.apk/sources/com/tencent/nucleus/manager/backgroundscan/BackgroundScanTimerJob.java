package com.tencent.nucleus.manager.backgroundscan;

import android.content.Context;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.Global;
import com.tencent.assistant.m;
import com.tencent.assistant.module.timer.BaseTimePointJob;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.t;
import com.tencent.nucleus.manager.uninstallwatch.e;
import java.security.SecureRandom;
import java.util.Calendar;
import java.util.Random;

/* compiled from: ProGuard */
public class BackgroundScanTimerJob extends BaseTimePointJob {

    /* renamed from: a  reason: collision with root package name */
    private static BackgroundScanTimerJob f2821a = null;
    private static double[][] c = {new double[]{1.0d, 5.0d}, new double[]{5.0d, 7.0d}, new double[]{10.0d, 11.5d}, new double[]{16.0d, 17.5d}};
    private int[] b;
    private Object d = new Object();

    public BackgroundScanTimerJob() {
        j();
    }

    public static synchronized BackgroundScanTimerJob h() {
        BackgroundScanTimerJob backgroundScanTimerJob;
        synchronized (BackgroundScanTimerJob.class) {
            if (f2821a == null) {
                f2821a = new BackgroundScanTimerJob();
            }
            backgroundScanTimerJob = f2821a;
        }
        return backgroundScanTimerJob;
    }

    public int[] g() {
        int[] iArr;
        synchronized (this.d) {
            iArr = this.b;
        }
        return iArr;
    }

    private void j() {
        XLog.i("BackgroundScan", "<timer> loadTimes");
        synchronized (this.d) {
            if (this.b == null || this.b.length <= 0) {
                this.b = new int[4];
                Calendar instance = Calendar.getInstance();
                instance.set(11, 1);
                instance.set(12, 0);
                instance.set(13, 0);
                this.b[0] = a(instance.getTimeInMillis() + b(14400000));
                instance.set(11, 5);
                this.b[1] = a(instance.getTimeInMillis() + b(7200000));
                instance.set(11, 10);
                this.b[2] = a(instance.getTimeInMillis() + b(5400000));
                instance.set(11, 16);
                this.b[3] = a(instance.getTimeInMillis() + b(5400000));
            }
        }
    }

    public static int a(long j) {
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(j);
        return instance.get(14) + (instance.get(11) * 10000000) + (instance.get(12) * 100000) + (instance.get(13) * 1000);
    }

    public static long b(long j) {
        try {
            return Math.abs(SecureRandom.getInstance("SHA1PRNG", "Crypto").nextLong() % j);
        } catch (Exception e) {
            return Math.abs(new Random(i()).nextLong() % j);
        }
    }

    public static long i() {
        long j;
        try {
            String phoneGuid = Global.getPhoneGuid();
            if (!TextUtils.isEmpty(phoneGuid)) {
                j = Long.parseLong(phoneGuid);
            } else {
                j = 0;
            }
        } catch (Exception e) {
            j = 0;
        }
        if (j == 0) {
            try {
                String g = t.g();
                if (!TextUtils.isEmpty(g)) {
                    j = Long.parseLong(g);
                }
            } catch (Exception e2) {
            }
        }
        return j + System.currentTimeMillis();
    }

    private boolean k() {
        XLog.i("BackgroundScan", "<timer> isNeed2Run");
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(System.currentTimeMillis());
        double d2 = ((double) instance.get(11)) + (((double) instance.get(12)) / 60.0d);
        for (double[] dArr : c) {
            if (d2 >= dArr[0] && d2 <= dArr[1]) {
                return true;
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.uninstallwatch.e.a(android.content.Context, boolean):void
     arg types: [com.qq.AppService.AstApp, int]
     candidates:
      com.tencent.nucleus.manager.uninstallwatch.e.a(com.tencent.nucleus.manager.uninstallwatch.e, android.content.Context):void
      com.tencent.nucleus.manager.uninstallwatch.e.a(com.tencent.nucleus.manager.uninstallwatch.e, boolean):boolean
      com.tencent.nucleus.manager.uninstallwatch.e.a(android.content.Context, boolean):void */
    /* access modifiers changed from: protected */
    public void d() {
        boolean s = m.a().s();
        XLog.i("BackgroundScan", "<timer> doWork, switch = " + s);
        if (s) {
            if (k()) {
                XLog.d("BackgroundScan", "<timer> Time to scan");
                BackgroundScanManager.a().e();
            } else {
                XLog.e("BackgroundScan", "<timer> Time not in time edges !!");
            }
        }
        e.a().a((Context) AstApp.i(), false);
        o.a().b();
    }
}
