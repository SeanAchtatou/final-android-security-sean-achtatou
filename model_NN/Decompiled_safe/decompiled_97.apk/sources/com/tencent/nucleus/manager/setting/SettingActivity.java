package com.tencent.nucleus.manager.setting;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Message;
import android.view.Menu;
import android.view.View;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.Global;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.activity.debug.DActivity;
import com.tencent.assistant.activity.debug.ServerAdressSettingActivity;
import com.tencent.assistant.adapter.af;
import com.tencent.assistant.adapter.y;
import com.tencent.assistant.component.txscrollview.TXExpandableListView;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.module.nac.d;
import com.tencent.assistant.protocol.jce.AppSecretUserProfile;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.t;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.model.ItemElement;
import com.tencent.nucleus.manager.component.SwitchButton;
import com.tencent.nucleus.socialcontact.login.a;
import com.tencent.nucleus.socialcontact.login.j;
import com.tencent.nucleus.socialcontact.login.l;
import com.tencent.nucleus.socialcontact.tagpage.MiniVideoSetDialog;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* compiled from: ProGuard */
public class SettingActivity extends BaseActivity implements UIEventListener {
    private static int[] y = {EventDispatcherEnum.UI_EVENT_LOGIN_CANCEL, EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, EventDispatcherEnum.UI_EVENT_LOGIN_FAIL, EventDispatcherEnum.UI_EVENT_SETTING_LOGIN_CANCEL, EventDispatcherEnum.UI_EVENT_GET_USERINFO_SUCCESS, EventDispatcherEnum.UI_EVENT_GET_USERINFO_FAIL, EventDispatcherEnum.UI_EVENT_SETUSERPROFILE_STATUS_CHANGED_SUCC, EventDispatcherEnum.UI_EVENT_SETUSERPROFILE_STATUS_CHANGED_FAIL, EventDispatcherEnum.UI_EVENT_SELFUPDATE_UPDATE, EventDispatcherEnum.UI_EVENT_SERVER_ENVIRONMENT_CHANGE};
    private Context n;
    private SecondNavigationTitleViewV5 u;
    private TXExpandableListView v;
    /* access modifiers changed from: private */
    public y w;
    private boolean x = true;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_setting);
        this.n = this;
        this.w = new y(this.n);
        w();
        y();
        u();
        if (j.a().j()) {
            a.a().b();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.w != null && !this.x && !this.w.c) {
            this.w.notifyDataSetChanged();
        }
        this.x = false;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        v();
    }

    private void u() {
        for (int addUIEventListener : y) {
            AstApp.i().k().addUIEventListener(addUIEventListener, this);
        }
    }

    private void v() {
        for (int removeUIEventListener : y) {
            AstApp.i().k().removeUIEventListener(removeUIEventListener, this);
        }
    }

    private void w() {
        if (!t.z()) {
            ArrayList arrayList = new ArrayList();
            arrayList.add(new ItemElement(this.n.getString(R.string.setting_float_window_item_title), null, 1, 16, 0));
            this.w.a("floatwindow_setting", (int) R.string.setting_group_float_window, -1, arrayList);
        }
        ArrayList arrayList2 = new ArrayList();
        int i = t().getInt("item_index", -1);
        MiniVideoSetDialog.ItemIndex itemIndex = MiniVideoSetDialog.ItemIndex.ONLY_WIFI;
        if (i >= 0 && i <= 2) {
            itemIndex = MiniVideoSetDialog.ItemIndex.values()[i];
        }
        arrayList2.add(new ItemElement(this.n.getString(R.string.setting_group_mini_video_title), a(itemIndex), 0, 11, 0));
        this.w.a("mini_video_setting", (int) R.string.setting_group_mini_video, -1, arrayList2);
        ArrayList arrayList3 = new ArrayList();
        arrayList3.add(new ItemElement(this.n.getString(R.string.setting_thumbnail_item_title), this.n.getString(R.string.setting_thumbnail_item_des), 1, 0, 0));
        arrayList3.add(new ItemElement(this.n.getString(R.string.setting_auto_install_item_title), null, 1, 4, 0));
        arrayList3.add(new ItemElement(this.n.getString(R.string.setting_wise_udate_download_item_title), this.n.getString(R.string.setting_wise_udate_download_item_des), 1, 1, 0));
        arrayList3.add(new ItemElement(this.n.getString(R.string.setting_download_location_item_title), this.n.getString(R.string.setting_download_location_item_desc), 0, 17, 0));
        this.w.a("download_setting", (int) R.string.setting_group_download, -1, arrayList3);
        ArrayList arrayList4 = new ArrayList();
        arrayList4.add(new ItemElement(this.n.getString(R.string.setting_root_install_item_title), this.n.getString(R.string.setting_root_install_item_des), 1, 3, 0));
        arrayList4.add(new ItemElement(this.n.getString(R.string.setting_auto_del_package_item_title), null, 1, 5, 0));
        this.w.a("install_setting", (int) R.string.setting_group_install, -1, arrayList4);
        ArrayList arrayList5 = new ArrayList();
        arrayList5.add(new ItemElement(this.n.getString(R.string.setting_message_tip_item_title), this.n.getString(R.string.setting_message_tip_item_des), 0, 7, 0));
        arrayList5.add(new ItemElement(this.n.getString(R.string.setting_privacy_protection_item_title), this.n.getString(R.string.setting_privacy_protection_item_des), 1, 8, 0));
        if (Global.isDev()) {
            arrayList5.add(new ItemElement(this.n.getString(R.string.setting_server_item_title) + ":" + Global.getServerAddressName(), null, 0, 9, 0));
            arrayList5.add(new ItemElement(this.n.getString(R.string.setting_debug_item_title), null, 0, 10, 0));
        }
        this.w.a("other_setting", (int) R.string.setting_group_other, -1, arrayList5);
    }

    private void x() {
        this.w.a();
        w();
    }

    private void y() {
        this.u = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.u.a(this);
        this.u.b(getString(R.string.setting_title));
        this.u.d();
        this.u.c(new b(this));
        this.v = (TXExpandableListView) findViewById(R.id.list_view);
        this.v.setAdapter(this.w);
        this.v.setDivider(null);
        for (int i = 0; i < this.w.getGroupCount(); i++) {
            this.v.expandGroup(i);
        }
        this.v.setSelector(R.drawable.transparent_selector);
        this.v.setOnGroupClickListener(new c(this));
    }

    public void a(ItemElement itemElement, View view, int i) {
        boolean z = true;
        switch (itemElement.d) {
            case 7:
                Intent intent = new Intent(this.n, ChildSettingActivity.class);
                List<ItemElement> z2 = z();
                Bundle bundle = new Bundle();
                bundle.putSerializable("child_setting_page_key", (Serializable) z2);
                intent.putExtras(bundle);
                intent.putExtra("child_setting_title", itemElement.f2018a);
                this.n.startActivity(intent);
                return;
            case 8:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            default:
                af afVar = (af) view.getTag();
                SwitchButton switchButton = afVar.g;
                if (afVar.g.b()) {
                    z = false;
                }
                switchButton.b(z);
                ah.a().postDelayed(new e(this, itemElement, view, i, afVar), (long) afVar.g.a());
                return;
            case 9:
                a((Context) this);
                return;
            case 10:
                this.n.startActivity(new Intent(this.n, DActivity.class));
                return;
            case 11:
                int i2 = t().getInt("item_index", -1);
                MiniVideoSetDialog.ItemIndex itemIndex = MiniVideoSetDialog.ItemIndex.ONLY_WIFI;
                if (i2 >= 0 && i2 <= 2) {
                    itemIndex = MiniVideoSetDialog.ItemIndex.values()[i2];
                }
                MiniVideoSetDialog miniVideoSetDialog = new MiniVideoSetDialog(this.n, R.style.dialog, itemIndex);
                miniVideoSetDialog.a(new d(this, itemElement));
                miniVideoSetDialog.show();
                return;
            case 17:
                HashMap hashMap = new HashMap();
                hashMap.put("B1", Global.getPhoneGuidAndGen());
                hashMap.put("B2", Global.getQUAForBeacon());
                XLog.d("beacon", "beacon report >> DownloadLocationClick. " + hashMap.toString());
                com.tencent.beacon.event.a.a("DownloadLocationClick", true, -1, -1, hashMap, true);
                return;
        }
    }

    public String a(MiniVideoSetDialog.ItemIndex itemIndex) {
        switch (h.f3002a[itemIndex.ordinal()]) {
            case 1:
                return getResources().getString(R.string.setting_group_mini_video_item_des_all);
            case 2:
                return getResources().getString(R.string.setting_group_mini_video_item_des_only_wifi);
            case 3:
                return getResources().getString(R.string.setting_group_mini_video_item_des_close);
            default:
                return getResources().getString(R.string.setting_group_mini_video_item_des_only_wifi);
        }
    }

    private void a(Context context) {
        startActivity(new Intent(context, ServerAdressSettingActivity.class));
    }

    private List<ItemElement> z() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new ItemElement(this.n.getString(R.string.setting_appupdate_message_push_item_title), this.n.getString(R.string.setting_appupdate_message_push_item_des), 0, 11, 0));
        arrayList.add(new ItemElement(this.n.getString(R.string.setting_recommend_message_push_item_title), this.n.getString(R.string.setting_recommend_message_push_item_des), 0, 12, 0));
        arrayList.add(new ItemElement(this.n.getString(R.string.setting_phone_manager_push_item_title), this.n.getString(R.string.setting_phone_manager_push_item_des), 0, 13, 0));
        arrayList.add(new ItemElement(this.n.getString(R.string.setting_personal_message_item_title), this.n.getString(R.string.setting_personal_message_item_des), 0, 14, 0));
        arrayList.add(new ItemElement(this.n.getString(R.string.setting_freewifi_protection_item_title), this.n.getString(R.string.setting_freewifi_protection_item_des), 0, 15, 0));
        return arrayList;
    }

    public int f() {
        return STConst.ST_PAGE_SETTING;
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case EventDispatcherEnum.UI_EVENT_SELFUPDATE_UPDATE:
            case EventDispatcherEnum.UI_EVENT_GET_USERINFO_SUCCESS:
            case EventDispatcherEnum.UI_EVENT_GET_USERINFO_FAIL:
            case EventDispatcherEnum.UI_EVENT_LOGIN_FAIL:
            case EventDispatcherEnum.UI_EVENT_LOGIN_CANCEL:
            case EventDispatcherEnum.UI_EVENT_SETTING_LOGIN_CANCEL:
            case EventDispatcherEnum.UI_EVENT_SETUSERPROFILE_STATUS_CHANGED_FAIL:
                ah.a().post(new f(this));
                XLog.i("SettingActivity", "UI_EVENT_LOGIN_FAIL");
                return;
            case EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS:
                XLog.i("SettingActivity", "UI_EVENT_LOGIN_SUCCESS");
                return;
            case EventDispatcherEnum.UI_EVENT_SETUSERPROFILE_STATUS_CHANGED_SUCC:
                l.a((AppSecretUserProfile) message.obj, false);
                ah.a().post(new g(this));
                XLog.i("SettingActivity", "UI_EVENT_SETUSERPROFILE_STATUS_CHANGED_SUCC");
                return;
            case EventDispatcherEnum.UI_EVENT_SERVER_ENVIRONMENT_CHANGE:
                d.a().b();
                x();
                this.w.notifyDataSetChanged();
                return;
            default:
                return;
        }
    }

    public boolean onMenuOpened(int i, Menu menu) {
        return true;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    public static SharedPreferences t() {
        return AstApp.i().getSharedPreferences("settings", 0);
    }
}
