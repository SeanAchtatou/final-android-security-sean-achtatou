package com.tencent.nucleus.manager.spaceclean;

import android.os.Environment;
import android.text.TextUtils;
import com.tencent.assistant.db.table.z;
import com.tencent.assistant.m;
import com.tencent.assistant.protocol.jce.RubbishRule;
import com.tencent.assistant.protocol.jce.RubbishRuleList;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.an;
import com.tencent.assistantv2.st.business.BaseSTManagerV2;
import com.tencent.tmsecurelite.commom.DataEntity;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;

/* compiled from: ProGuard */
public class ac extends BaseSTManagerV2 {

    /* renamed from: a  reason: collision with root package name */
    private static ac f3016a = null;
    private Object c = new Object();
    private ArrayList<RubbishRule> d = new ArrayList<>();
    private List<String> e = new ArrayList();

    private ac() {
    }

    public static synchronized ac a() {
        ac acVar;
        synchronized (ac.class) {
            if (f3016a == null) {
                f3016a = new ac();
            }
            acVar = f3016a;
        }
        return acVar;
    }

    public void b() {
        if (e()) {
            synchronized (this.c) {
                this.d.clear();
            }
        }
    }

    public void a(int i, DataEntity dataEntity) {
        if (e() && i == 1) {
            try {
                boolean z = dataEntity.getBoolean("rubbish.suggest");
                String string = dataEntity.getString("app.name");
                String string2 = dataEntity.getString("app.pkg");
                String string3 = dataEntity.getString("rubbish.desc");
                JSONArray jSONArray = dataEntity.getJSONArray("rubbish.path.array");
                if (jSONArray.length() > 0) {
                    for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                        String obj = jSONArray.get(i2).toString();
                        if (!TextUtils.isEmpty(obj)) {
                            File file = new File(obj);
                            if (!file.exists()) {
                                continue;
                            } else {
                                if (file.isFile()) {
                                    obj = file.getParent();
                                }
                                Iterator<String> it = f().iterator();
                                while (true) {
                                    if (!it.hasNext()) {
                                        break;
                                    }
                                    String next = it.next();
                                    if (obj.startsWith(next)) {
                                        obj = obj.substring(next.length());
                                        break;
                                    }
                                }
                                synchronized (this.c) {
                                    this.d.add(new RubbishRule(string2, obj, string, z ? 1 : 0, string3, 1));
                                    if (this.d.size() >= 300) {
                                        c();
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public void c() {
        if (e()) {
            if (this.d.size() > 0) {
                XLog.i("rubbishRule", "doWhenRubbishFinished size = " + this.d.size());
                ArrayList<RubbishRule> arrayList = this.d;
                RubbishRuleList rubbishRuleList = new RubbishRuleList();
                rubbishRuleList.f1474a = arrayList;
                rubbishRuleList.b = arrayList.size();
                z.a().a(getSTType(), an.a(rubbishRuleList));
            }
            synchronized (this.c) {
                this.d.clear();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    private boolean e() {
        return m.a().a("key_rubbish_rule_collect_sample_switch", false);
    }

    private synchronized List<String> f() {
        if (this.e.size() <= 0) {
            String absolutePath = Environment.getExternalStorageDirectory().getAbsolutePath();
            if (!absolutePath.endsWith(File.separator)) {
                absolutePath = absolutePath + File.separator;
            }
            this.e.add(absolutePath);
            this.e.add("/mnt/sdcard/");
            this.e.add("/sdcard/");
            this.e.add("/storage/sdcard0/");
            this.e.add("/storage/sdcard1/");
            this.e.add("/storage/emulated/0/");
            this.e.add("/storage/emulated/1/");
        }
        return this.e;
    }

    public byte getSTType() {
        return 25;
    }

    public void flush() {
    }
}
