package com.tencent.nucleus.manager.component;

import android.view.animation.Animation;

/* compiled from: ProGuard */
class ae implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TxManagerCommContainView f2851a;

    ae(TxManagerCommContainView txManagerCommContainView) {
        this.f2851a = txManagerCommContainView;
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        this.f2851a.j.setVisibility(0);
        int childCount = this.f2851a.j.getChildCount();
        if (childCount != 0) {
            for (int i = 0; i < childCount; i++) {
                this.f2851a.j.getChildAt(i).setVisibility(0);
            }
        }
    }
}
