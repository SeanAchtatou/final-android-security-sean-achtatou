package com.tencent.nucleus.manager.spaceclean;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXRefreshListView;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class BigFileListView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private Context f3006a;
    private LayoutInflater b;
    private BigFileAdapter c;
    private View d;
    private TXRefreshListView e;

    public BigFileListView(Context context) {
        this(context, null);
    }

    public BigFileListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3006a = context;
        this.b = (LayoutInflater) this.f3006a.getSystemService("layout_inflater");
        a();
    }

    private void a() {
        this.d = this.b.inflate((int) R.layout.big_file_component_view, this);
        this.e = (TXRefreshListView) this.d.findViewById(R.id.list_view);
        this.e.setDivider(null);
        this.e.setSelector(this.f3006a.getResources().getDrawable(R.drawable.transparent_selector));
        this.c = new BigFileAdapter(this.f3006a);
        this.e.setAdapter(this.c);
    }

    public void a(ArrayList<n> arrayList) {
        this.c.a(arrayList);
    }

    public void a(Handler handler) {
        this.c.a(handler);
    }
}
