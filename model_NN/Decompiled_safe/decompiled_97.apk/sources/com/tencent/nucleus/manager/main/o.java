package com.tencent.nucleus.manager.main;

import android.view.animation.Transformation;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class o implements b {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AssistantTabActivity f2957a;

    o(AssistantTabActivity assistantTabActivity) {
        this.f2957a = assistantTabActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.manager.main.AssistantTabActivity.e(com.tencent.nucleus.manager.main.AssistantTabActivity, boolean):void
     arg types: [com.tencent.nucleus.manager.main.AssistantTabActivity, int]
     candidates:
      com.tencent.nucleus.manager.main.AssistantTabActivity.e(com.tencent.nucleus.manager.main.AssistantTabActivity, int):int
      com.tencent.nucleus.manager.main.AssistantTabActivity.e(com.tencent.nucleus.manager.main.AssistantTabActivity, boolean):void */
    public void a(float f, float f2, float f3, float f4) {
        if (f3 >= 0.0f && this.f2957a.Q.getCurrentView().getId() == R.id.layout_tips_result && f4 > f3) {
            this.f2957a.d(false);
        }
    }

    public void b(float f, float f2, float f3, float f4) {
        boolean z = true;
        if (this.f2957a.aP.b() == 0.0f && this.f2957a.w.getScrollY() <= 0) {
            this.f2957a.w.smoothScrollTo(0, 0);
        }
        if (this.f2957a.aP.b() != 0.0f) {
            this.f2957a.w.smoothScrollTo(0, 0);
        }
        if (this.f2957a.aP.b() == 0.0f && this.f2957a.Q.getCurrentView().getId() == R.id.tv_tips && f3 > f4) {
            this.f2957a.bi.postDelayed(new p(this), 10);
        }
        if (this.f2957a.aP.b() == 1.0f) {
            this.f2957a.w.b(0.0f);
            boolean z2 = (!this.f2957a.aI) & (!this.f2957a.bb);
            if (this.f2957a.bg) {
                z = false;
            }
            if (z2 && z) {
                this.f2957a.bi.postDelayed(new q(this), 1000);
            }
        }
        if (this.f2957a.aP.b() == this.f2957a.aR) {
            this.f2957a.u();
            this.f2957a.w.b(this.f2957a.aS);
        }
        if (this.f2957a.bb) {
            boolean unused = this.f2957a.bb = false;
            this.f2957a.bi.postDelayed(new r(this), 200);
        }
    }

    public void a(float f, Transformation transformation) {
        this.f2957a.w.smoothScrollTo(0, 0);
    }
}
