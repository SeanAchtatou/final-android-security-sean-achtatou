package com.tencent.nucleus.manager.apkuninstall;

import android.content.Context;
import android.os.Handler;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.LocalPkgSizeTextView;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.m;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.bm;
import com.tencent.assistant.utils.bo;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.component.MovingProgressBar;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class UserInstalledAppListAdapter extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private Context f2782a;
    private List<LocalApkInfo> b = new ArrayList();
    private LayoutInflater c;
    private IViewInvalidater d;
    private boolean e = true;
    private int f = 0;
    /* access modifiers changed from: private */
    public Handler g;
    private boolean h;

    public UserInstalledAppListAdapter(Context context) {
        this.f2782a = context;
        this.c = LayoutInflater.from(context);
    }

    public void a(List<LocalApkInfo> list, int i) {
        this.b.clear();
        this.b.addAll(list);
        this.f = i;
        notifyDataSetChanged();
    }

    public int getCount() {
        if (this.b != null) {
            return this.b.size();
        }
        return 0;
    }

    public Object getItem(int i) {
        if (this.b != null) {
            return this.b.get(i);
        }
        return null;
    }

    public long getItemId(int i) {
        return 0;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        l lVar;
        if (view == null || view.getTag() == null) {
            view = this.c.inflate((int) R.layout.installed_app_list_item, (ViewGroup) null);
            lVar = new l(null);
            lVar.f2794a = view.findViewById(R.id.container_layout);
            lVar.c = (TextView) view.findViewById(R.id.soft_name_txt);
            lVar.b = (TXImageView) view.findViewById(R.id.soft_icon_img);
            lVar.d = (LocalPkgSizeTextView) view.findViewById(R.id.soft_size_txt);
            lVar.e = (TextView) view.findViewById(R.id.tv_check);
            lVar.f = (TextView) view.findViewById(R.id.last_used_time_txt);
            lVar.g = (TextView) view.findViewById(R.id.popbar_title);
            lVar.j = view.findViewById(R.id.rl_appdetail);
            lVar.h = view.findViewById(R.id.uninstall_progress);
            lVar.i = (MovingProgressBar) view.findViewById(R.id.app_uninstall_progress_bar);
            lVar.i.a(AstApp.i().getResources().getDimensionPixelSize(R.dimen.app_detail_pic_gap));
            lVar.i.b(AstApp.i().getResources().getDimensionPixelSize(R.dimen.install_progress_bar_width));
            lVar.k = view.findViewById(R.id.top_margin);
            lVar.l = view.findViewById(R.id.bottom_margin);
            lVar.m = view.findViewById(R.id.last_line);
            view.setTag(lVar);
        } else {
            lVar = (l) view.getTag();
        }
        LocalApkInfo localApkInfo = this.b.get(i);
        STInfoV2 a2 = a(this.f, i);
        if (localApkInfo != null) {
            a(localApkInfo, lVar, i, a2);
        }
        return view;
    }

    private void a(LocalApkInfo localApkInfo, l lVar, int i, STInfoV2 sTInfoV2) {
        lVar.c.setText(localApkInfo.mAppName);
        lVar.b.updateImageView(localApkInfo.mPackageName, R.drawable.pic_defaule, TXImageView.TXImageViewType.INSTALL_APK_ICON);
        if (this.h) {
            lVar.e.setEnabled(false);
            lVar.f2794a.setEnabled(false);
            if (localApkInfo.mIsSelect) {
                lVar.h.setVisibility(0);
                lVar.j.setVisibility(8);
                lVar.i.a();
            } else {
                lVar.h.setVisibility(8);
                lVar.j.setVisibility(0);
                lVar.i.b();
            }
        } else {
            lVar.e.setEnabled(true);
            lVar.f2794a.setEnabled(true);
            lVar.h.setVisibility(8);
            lVar.j.setVisibility(0);
            lVar.i.b();
        }
        lVar.e.setSelected(localApkInfo.mIsSelect);
        if (this.f == 1) {
            lVar.d.setTextColor(this.f2782a.getResources().getColor(R.color.appadmin_highlight_text));
        } else {
            lVar.d.setTextColor(this.f2782a.getResources().getColor(R.color.appadmin_card_size_text));
        }
        lVar.d.updateTextView(localApkInfo.mPackageName, localApkInfo.occupySize);
        lVar.d.setInvalidater(this.d);
        if (i == 0) {
            lVar.g.setText(this.f2782a.getString(R.string.popbar_title_installed_count, Integer.valueOf(getCount())));
            lVar.g.setVisibility(0);
        } else {
            lVar.g.setVisibility(8);
        }
        if (this.f == 3) {
            a(lVar.f, localApkInfo.mInstallDate);
        } else {
            a(lVar.f, localApkInfo.mLastLaunchTime, localApkInfo.mFakeLastLaunchTime);
        }
        if (this.e) {
            ((RelativeLayout.LayoutParams) lVar.e.getLayoutParams()).rightMargin = by.a(this.f2782a, 16.0f);
            ((LinearLayout.LayoutParams) lVar.m.getLayoutParams()).rightMargin = by.a(this.f2782a, 16.0f);
        } else {
            ((RelativeLayout.LayoutParams) lVar.e.getLayoutParams()).rightMargin = by.a(this.f2782a, 46.0f);
            ((LinearLayout.LayoutParams) lVar.m.getLayoutParams()).rightMargin = by.a(this.f2782a, 30.0f);
        }
        lVar.e.setOnClickListener(new j(this, lVar, localApkInfo, sTInfoV2));
        lVar.f2794a.setOnClickListener(new k(this, lVar, localApkInfo, sTInfoV2));
        a(lVar.f2794a, lVar.m, lVar.k, lVar.l, i, this.b.size());
    }

    private STInfoV2 a(int i, int i2) {
        String b2 = b(i, i2);
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f2782a, 100);
        buildSTInfo.slotId = b2;
        buildSTInfo.scene = STConst.ST_PAGE_USER_APP_UNINSTALL;
        b.getInstance().exposure(buildSTInfo);
        return buildSTInfo;
    }

    private String b(int i, int i2) {
        if (i == 0) {
            return "03_" + bm.a(i2 + 1);
        }
        if (i == 1) {
            return "04_" + bm.a(i2 + 1);
        }
        if (i == 2) {
            return "05_" + bm.a(i2 + 1);
        }
        return "06_" + bm.a(i2 + 1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(java.lang.String, long):long */
    private void a(TextView textView, long j, long j2) {
        int f2;
        long a2 = m.a().a("key_first_load_installed_app_time", 0L);
        boolean z = a2 != 0 && (System.currentTimeMillis() - a2) - 259200000 >= 0;
        if (j > 0 || z) {
            if (j > 0) {
                f2 = bo.f(j);
            } else {
                f2 = bo.f(j2);
            }
            if (f2 == 0) {
                textView.setVisibility(0);
                textView.setText(this.f2782a.getResources().getString(R.string.today));
                return;
            }
            String format = String.format(this.f2782a.getResources().getString(R.string.last_used_time), Integer.valueOf(f2));
            if (this.f == 0) {
                SpannableString spannableString = new SpannableString(format);
                spannableString.setSpan(new ForegroundColorSpan(this.f2782a.getResources().getColor(R.color.appadmin_highlight_text)), 0, format.indexOf("天") + 1, 33);
                textView.setText(spannableString);
            } else {
                textView.setText(format);
            }
            textView.setVisibility(0);
            return;
        }
        textView.setVisibility(8);
    }

    private void a(TextView textView, long j) {
        if (j == 0) {
            textView.setVisibility(8);
            return;
        }
        int f2 = bo.f(j);
        if (f2 == 0) {
            textView.setVisibility(0);
            textView.setText(this.f2782a.getResources().getString(R.string.today_install));
            return;
        }
        String format = String.format(this.f2782a.getResources().getString(R.string.install_time), Integer.valueOf(f2));
        SpannableString spannableString = new SpannableString(format);
        spannableString.setSpan(new ForegroundColorSpan(this.f2782a.getResources().getColor(R.color.appadmin_highlight_text)), 0, format.indexOf("天") + 1, 33);
        textView.setText(spannableString);
        textView.setVisibility(0);
    }

    public void a(View view, View view2, View view3, View view4, int i, int i2) {
        if (i2 == 1) {
            view2.setVisibility(8);
            view4.setVisibility(0);
            view3.setVisibility(0);
        } else if (i == 0) {
            view2.setVisibility(0);
            view4.setVisibility(8);
            view3.setVisibility(0);
        } else if (i == i2 - 1) {
            view2.setVisibility(8);
            view4.setVisibility(0);
            view3.setVisibility(8);
        } else {
            view2.setVisibility(0);
            view4.setVisibility(8);
            view3.setVisibility(8);
        }
        view.setBackgroundResource(R.drawable.helper_cardbg_all_selector_new);
    }

    public int a(int i) {
        for (int i2 = 0; i2 < getCount(); i2++) {
            String str = this.b.get(i2).mSortKey;
            if (!TextUtils.isEmpty(str)) {
                char charAt = str.toUpperCase().charAt(0);
                if (charAt == i) {
                    return i2;
                }
                if (i == 35 && charAt >= '0' && charAt <= '9') {
                    return i2;
                }
            }
        }
        return -1;
    }

    public void a(IViewInvalidater iViewInvalidater) {
        this.d = iViewInvalidater;
    }

    public void a(boolean z) {
        this.e = z;
    }

    public void a(Handler handler) {
        this.g = handler;
    }

    public void a(boolean z, boolean z2) {
        this.h = z;
        if (z2) {
            notifyDataSetChanged();
        }
    }
}
