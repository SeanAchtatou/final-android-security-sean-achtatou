package com.tencent.nucleus.manager.about;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.m;
import com.tencent.assistant.protocol.jce.Feedback;
import com.tencent.assistant.utils.t;
import com.tencent.assistantv2.model.ItemElement;
import com.tencent.pangu.manager.SelfUpdateManager;
import com.tencent.pangu.module.a.l;
import com.tencent.pangu.module.ax;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class AboutAdapter extends BaseAdapter implements l, l {

    /* renamed from: a  reason: collision with root package name */
    private Context f2729a;
    private LayoutInflater b;
    private List<ItemElement> c = new ArrayList();

    public AboutAdapter(Context context) {
        this.f2729a = context;
        this.b = LayoutInflater.from(context);
        ax.a().register(this);
        n.a().register(this);
    }

    public void a(List<ItemElement> list) {
        if (list != null && list.size() > 0) {
            this.c.addAll(list);
        }
    }

    public int getCount() {
        if (this.c == null || this.c.size() <= 0) {
            return 0;
        }
        return this.c.size();
    }

    public Object getItem(int i) {
        if (i < 0 || i > this.c.size() - 1) {
            return null;
        }
        return this.c.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.manager.SelfUpdateManager.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.tencent.pangu.manager.SelfUpdateManager.a(com.tencent.pangu.manager.SelfUpdateManager, com.tencent.assistant.localres.model.LocalApkInfo):com.tencent.assistant.localres.model.LocalApkInfo
      com.tencent.pangu.manager.SelfUpdateManager.a(com.tencent.assistant.localres.model.LocalApkInfo, com.tencent.pangu.manager.SelfUpdateManager$SelfUpdateInfo):com.tencent.assistant.model.SimpleAppModel
      com.tencent.pangu.manager.SelfUpdateManager.a(boolean, boolean):void */
    public View getView(int i, View view, ViewGroup viewGroup) {
        h hVar;
        if (view == null) {
            view = this.b.inflate((int) R.layout.about_item_layout, (ViewGroup) null);
            hVar = new h(this);
            hVar.f2746a = view.findViewById(R.id.item_layout);
            hVar.b = view.findViewById(R.id.top_margin);
            hVar.c = (TextView) view.findViewById(R.id.item_title);
            hVar.d = (TextView) view.findViewById(R.id.item_description);
            hVar.e = (TextView) view.findViewById(R.id.item_image);
            hVar.f = view.findViewById(R.id.item_line);
            hVar.g = view.findViewById(R.id.bottom_margin);
            view.setTag(hVar);
        } else {
            hVar = (h) view.getTag();
        }
        ItemElement itemElement = (ItemElement) getItem(i);
        if (itemElement == null) {
            return null;
        }
        hVar.c.setText(itemElement.f2018a);
        switch (itemElement.c) {
            case 1:
                hVar.d.setVisibility(0);
                hVar.d.setTextColor(this.f2729a.getResources().getColor(R.color.about_description_txt_color));
                if (m.a().a("update_newest_versioncode", 0) > t.o()) {
                    if (!m.a().a("update_newest_versionname", (String) null).equals("null")) {
                        hVar.e.setVisibility(0);
                        hVar.d.setText(m.a().a("update_newest_versionname", (String) null));
                        hVar.d.setTextColor(this.f2729a.getResources().getColor(R.color.about_last_version_color));
                        break;
                    } else {
                        hVar.e.setVisibility(8);
                        hVar.d.setText(this.f2729a.getString(R.string.about_version_state_checking));
                        SelfUpdateManager.a().a(false, true);
                        break;
                    }
                } else {
                    hVar.e.setVisibility(8);
                    hVar.d.setText(this.f2729a.getString(R.string.about_version_state_last));
                    break;
                }
            case 2:
                int b2 = n.a().b();
                if (b2 <= 0) {
                    hVar.e.setVisibility(8);
                    hVar.d.setVisibility(8);
                    break;
                } else {
                    hVar.e.setVisibility(0);
                    hVar.d.setVisibility(0);
                    hVar.d.setText(String.valueOf(b2));
                    break;
                }
            case 4:
                hVar.e.setVisibility(8);
                hVar.d.setVisibility(8);
                break;
        }
        hVar.f2746a.setBackgroundResource(R.drawable.common_download_item_selector);
        if (i == getCount() - 1) {
            hVar.f.setVisibility(8);
            hVar.b.setVisibility(8);
            hVar.g.setVisibility(0);
            return view;
        } else if (i == 0) {
            hVar.f.setVisibility(0);
            hVar.b.setVisibility(0);
            hVar.g.setVisibility(8);
            return view;
        } else {
            hVar.f.setVisibility(0);
            hVar.b.setVisibility(8);
            hVar.g.setVisibility(8);
            return view;
        }
    }

    public void a() {
        notifyDataSetChanged();
    }

    public void b() {
    }

    public void c() {
        ax.a().unregister(this);
        n.a().unregister(this);
    }

    public void onCheckSelfUpdateFinish(int i, int i2, SelfUpdateManager.SelfUpdateInfo selfUpdateInfo) {
        m.a().b("update_newest_versionname", selfUpdateInfo.f);
        notifyDataSetChanged();
    }

    public void a(int i) {
        notifyDataSetChanged();
    }

    public void a(int i, boolean z, boolean z2, ArrayList<Feedback> arrayList) {
    }
}
