package com.tencent.nucleus.manager.about;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.b;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.protocol.jce.PostFeedbackResponse;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;

/* compiled from: ProGuard */
public class af extends BaseEngine<ad> {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public int f2737a = -1;
    /* access modifiers changed from: private */
    public Object b = new Object();

    public void a(String str) {
        TemporaryThreadManager.get().start(new ag(this, str));
    }

    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, JceStruct jceStruct, JceStruct jceStruct2) {
        PostFeedbackResponse postFeedbackResponse = (PostFeedbackResponse) jceStruct2;
        notifyDataChangedInMainThread(new ah(this, postFeedbackResponse));
        XLog.d("Config", "******* post feedback level =" + postFeedbackResponse.b);
        if (postFeedbackResponse.b == 1) {
            b.a().a("DEV");
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        notifyDataChangedInMainThread(new ai(this, i2));
    }
}
