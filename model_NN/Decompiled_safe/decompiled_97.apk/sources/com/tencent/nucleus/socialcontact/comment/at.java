package com.tencent.nucleus.socialcontact.comment;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.protocol.jce.GetReplyListResponse;

/* compiled from: ProGuard */
class at implements CallbackHelper.Caller<y> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f3101a;
    final /* synthetic */ boolean b;
    final /* synthetic */ GetReplyListResponse c;
    final /* synthetic */ ar d;

    at(ar arVar, int i, boolean z, GetReplyListResponse getReplyListResponse) {
        this.d = arVar;
        this.f3101a = i;
        this.b = z;
        this.c = getReplyListResponse;
    }

    /* renamed from: a */
    public void call(y yVar) {
        yVar.a(this.f3101a, 0, this.b, this.d.h, this.c.e, this.d.d, this.c.b);
    }
}
