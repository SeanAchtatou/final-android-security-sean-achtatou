package com.tencent.nucleus.socialcontact.comment;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class ae extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CommentReplyListActivity f3087a;

    ae(CommentReplyListActivity commentReplyListActivity) {
        this.f3087a = commentReplyListActivity;
    }

    public STInfoV2 getStInfo(View view) {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3087a, 200);
        buildSTInfo.scene = STConst.ST_PAGE_COMMENT_REPLY;
        buildSTInfo.slotId = this.f3087a.Z + "001";
        return buildSTInfo;
    }

    public void onTMAClick(View view) {
        int unused = this.f3087a.V = 0;
        if (this.f3087a.U.j()) {
            this.f3087a.y();
            this.f3087a.T.setPadding(by.a(this.f3087a, 8.0f), by.a(this.f3087a, 5.0f), by.a(this.f3087a, 8.0f), by.a(this.f3087a, 20.0f));
            return;
        }
        this.f3087a.B();
    }
}
