package com.tencent.nucleus.socialcontact.login;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.module.callback.i;
import com.tencent.assistant.protocol.jce.GetUserInfoRequest;
import com.tencent.assistant.protocol.jce.GetUserInfoResponse;

/* compiled from: ProGuard */
class b implements CallbackHelper.Caller<i> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f3156a;
    final /* synthetic */ JceStruct b;
    final /* synthetic */ GetUserInfoResponse c;
    final /* synthetic */ a d;

    b(a aVar, int i, JceStruct jceStruct, GetUserInfoResponse getUserInfoResponse) {
        this.d = aVar;
        this.f3156a = i;
        this.b = jceStruct;
        this.c = getUserInfoResponse;
    }

    /* renamed from: a */
    public void call(i iVar) {
        iVar.a(this.f3156a, (GetUserInfoRequest) this.b, this.c);
    }
}
