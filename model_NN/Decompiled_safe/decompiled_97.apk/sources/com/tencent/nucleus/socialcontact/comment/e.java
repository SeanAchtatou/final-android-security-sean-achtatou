package com.tencent.nucleus.socialcontact.comment;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.protocol.jce.PraiseCommentRequest;
import com.tencent.assistant.protocol.jce.PraiseCommentResponse;

/* compiled from: ProGuard */
class e implements CallbackHelper.Caller<j> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f3123a;
    final /* synthetic */ PraiseCommentResponse b;
    final /* synthetic */ PraiseCommentRequest c;
    final /* synthetic */ d d;

    e(d dVar, int i, PraiseCommentResponse praiseCommentResponse, PraiseCommentRequest praiseCommentRequest) {
        this.d = dVar;
        this.f3123a = i;
        this.b = praiseCommentResponse;
        this.c = praiseCommentRequest;
    }

    /* renamed from: a */
    public void call(j jVar) {
        jVar.a(this.f3123a, this.b.f1442a, this.c.f1441a, this.b.b, this.b.c);
    }
}
