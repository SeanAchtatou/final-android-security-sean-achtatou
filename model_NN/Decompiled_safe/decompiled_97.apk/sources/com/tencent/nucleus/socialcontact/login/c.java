package com.tencent.nucleus.socialcontact.login;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.module.callback.i;
import com.tencent.assistant.protocol.jce.GetUserInfoRequest;
import com.tencent.assistant.protocol.jce.GetUserInfoResponse;

/* compiled from: ProGuard */
class c implements CallbackHelper.Caller<i> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f3157a;
    final /* synthetic */ int b;
    final /* synthetic */ JceStruct c;
    final /* synthetic */ JceStruct d;
    final /* synthetic */ a e;

    c(a aVar, int i, int i2, JceStruct jceStruct, JceStruct jceStruct2) {
        this.e = aVar;
        this.f3157a = i;
        this.b = i2;
        this.c = jceStruct;
        this.d = jceStruct2;
    }

    /* renamed from: a */
    public void call(i iVar) {
        iVar.a(this.f3157a, this.b, (GetUserInfoRequest) this.c, (GetUserInfoResponse) this.d);
    }
}
