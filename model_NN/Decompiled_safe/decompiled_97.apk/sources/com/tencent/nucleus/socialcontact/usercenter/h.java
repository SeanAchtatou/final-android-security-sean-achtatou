package com.tencent.nucleus.socialcontact.usercenter;

import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.module.BaseEngine;
import com.tencent.assistant.module.callback.ActionCallback;
import com.tencent.assistant.protocol.jce.GetUserActivityRequest;
import com.tencent.assistant.protocol.jce.GetUserActivityResponse;
import com.tencent.assistant.protocol.scu.RequestResponePair;
import com.tencent.assistant.utils.XLog;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class h extends BaseEngine<ActionCallback> {

    /* renamed from: a  reason: collision with root package name */
    private int f3231a = 0;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.nucleus.socialcontact.usercenter.j.a(java.util.List<? extends com.qq.taf.jce.JceStruct>, boolean):void
     arg types: [java.util.ArrayList, int]
     candidates:
      com.tencent.nucleus.socialcontact.usercenter.j.a(com.qq.taf.jce.JceStruct, int):com.tencent.nucleus.socialcontact.usercenter.a
      com.tencent.nucleus.socialcontact.usercenter.j.a(int, int):void
      com.tencent.nucleus.socialcontact.usercenter.j.a(com.tencent.assistant.protocol.jce.UserActivityItem, com.tencent.assistant.protocol.jce.UserActivityItem):boolean
      com.tencent.nucleus.socialcontact.usercenter.j.a(java.util.List<? extends com.qq.taf.jce.JceStruct>, boolean):void */
    /* access modifiers changed from: protected */
    public void onRequestSuccessed(int i, List<RequestResponePair> list) {
        XLog.i("xjp", "retSeq = " + this.f3231a + ", sep = " + i);
        if (this.f3231a != i || list == null || list.size() <= 0) {
            XLog.i("xjp", "[UserCenterEngine] ---> onRequestSuccessed (error)");
            return;
        }
        XLog.i("xjp", "[UserCenterEngine] ---> onRequestSuccessed, (responses.size() = " + list.size() + ")");
        ArrayList arrayList = new ArrayList(1);
        for (RequestResponePair next : list) {
            if (next.response instanceof GetUserActivityResponse) {
                arrayList.clear();
                arrayList.add((GetUserActivityResponse) next.response);
                j.a((List<? extends JceStruct>) arrayList, true);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onRequestFailed(int i, int i2, List<RequestResponePair> list) {
        XLog.i("xjp", "[UserCenterEngine] ---> onRequestFailed, errorCode = " + i2);
    }

    public int a() {
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(new GetUserActivityRequest());
        this.f3231a = send(arrayList);
        return this.f3231a;
    }
}
