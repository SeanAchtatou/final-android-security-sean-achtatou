package com.tencent.nucleus.socialcontact.tagpage;

import android.view.SurfaceHolder;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
class ay implements SurfaceHolder.Callback {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ as f3196a;

    private ay(as asVar) {
        this.f3196a = asVar;
    }

    /* synthetic */ ay(as asVar, at atVar) {
        this(asVar);
    }

    public void a(String str) {
        String unused = this.f3196a.l = str;
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        TemporaryThreadManager.get().start(new az(this, surfaceHolder));
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
    }
}
