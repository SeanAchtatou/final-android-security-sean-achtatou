package com.tencent.nucleus.socialcontact.comment;

import android.content.Context;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.protocol.jce.CommentTagInfo;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.bm;
import com.tencent.assistantv2.component.FlowLayout;
import com.tencent.assistantv2.component.t;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import java.util.List;

/* compiled from: ProGuard */
public class CommentHeaderTagView extends RelativeLayout {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f3076a;
    private LayoutInflater b;
    private TextView c;
    private TextView d;
    /* access modifiers changed from: private */
    public FlowLayout e;
    private ImageView f;
    private RelativeLayout g;
    /* access modifiers changed from: private */
    public boolean h = false;
    /* access modifiers changed from: private */
    public t i;
    private int j = EventDispatcherEnum.UI_EVENT_APP_DETAIL_DOWNLOAD_CLICK;

    public CommentHeaderTagView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3076a = context;
        a();
    }

    public CommentHeaderTagView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f3076a = context;
        a();
    }

    public CommentHeaderTagView(Context context) {
        super(context);
        this.f3076a = context;
        a();
    }

    private void a() {
        this.b = LayoutInflater.from(this.f3076a);
        View inflate = this.b.inflate((int) R.layout.comment_detail_header_taglist_layout, this);
        this.c = (TextView) inflate.findViewById(R.id.comment_average_numbers);
        this.d = (TextView) inflate.findViewById(R.id.comment_average_text_totol_custom);
        this.g = (RelativeLayout) inflate.findViewById(R.id.comment_taglayout);
        this.e = (FlowLayout) inflate.findViewById(R.id.comment_taglist);
        this.f = (ImageView) inflate.findViewById(R.id.expand_more_img);
    }

    public void a(String str) {
        this.c.setVisibility(0);
        this.c.setText(str);
    }

    public void b(String str) {
        this.d.setVisibility(0);
        this.d.setText(str);
    }

    public void a(int i2) {
        this.g.setVisibility(i2);
    }

    public void a(t tVar) {
        this.i = tVar;
        this.e.a(tVar);
    }

    public void a(CommentTagInfo commentTagInfo) {
        if (this.i != null) {
            this.i.a(null, commentTagInfo);
        }
    }

    public void b(int i2) {
        this.f.setVisibility(i2);
    }

    public void a(CommentTagInfo commentTagInfo, List<CommentTagInfo> list) {
        String str;
        if (list != null && list.size() > 0) {
            ImageView imageView = (ImageView) findViewById(R.id.expand_more_img);
            this.e.removeAllViews();
            RelativeLayout relativeLayout = new RelativeLayout(this.f3076a);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            TextView textView = new TextView(this.f3076a);
            textView.setId(R.id.commenttag_txview_id);
            textView.setBackgroundResource(R.drawable.comment_tag_score_normal_background);
            textView.setTextColor(Color.parseColor("#6e6e6e"));
            textView.setTextSize(0, (float) this.f3076a.getResources().getDimensionPixelSize(R.dimen.app_detail_comment_tag_text_size));
            textView.setText(this.f3076a.getResources().getString(R.string.comment_detail_tag_all));
            textView.setOnClickListener(new q(this));
            relativeLayout.addView(textView, layoutParams);
            if (commentTagInfo == null || TextUtils.isEmpty(commentTagInfo.f1204a)) {
                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams2.addRule(7, textView.getId());
                layoutParams2.addRule(8, textView.getId());
                ImageView imageView2 = new ImageView(this.f3076a);
                try {
                    imageView2.setImageResource(R.drawable.pinglun_icon_on);
                    imageView2.setId(R.id.commenttag_selectimg_id);
                } catch (Throwable th) {
                    th.printStackTrace();
                }
                relativeLayout.addView(imageView2, layoutParams2);
            }
            ViewGroup.MarginLayoutParams marginLayoutParams = new ViewGroup.MarginLayoutParams(-2, -2);
            marginLayoutParams.leftMargin = 0;
            marginLayoutParams.rightMargin = 16;
            marginLayoutParams.topMargin = 0;
            marginLayoutParams.bottomMargin = 16;
            this.e.addView(relativeLayout, marginLayoutParams);
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < list.size()) {
                    CommentTagInfo commentTagInfo2 = list.get(i3);
                    String a2 = commentTagInfo2.a();
                    byte b2 = commentTagInfo2.b();
                    RelativeLayout relativeLayout2 = new RelativeLayout(this.f3076a);
                    RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
                    TextView textView2 = new TextView(this.f3076a);
                    textView2.setId(R.id.commenttag_txview_id);
                    if (b2 == 2) {
                        textView2.setBackgroundResource(R.drawable.comment_tag_color_good_background);
                        textView2.setTextColor(Color.parseColor("#bb9359"));
                        str = "11_";
                    } else if (b2 == 3) {
                        textView2.setBackgroundResource(R.drawable.comment_tag_color_bad_background);
                        textView2.setTextColor(Color.parseColor("#8891b7"));
                        str = "11_";
                    } else {
                        textView2.setBackgroundResource(R.drawable.comment_tag_score_normal_background);
                        textView2.setTextColor(Color.parseColor("#6e6e6e"));
                        str = "10_";
                    }
                    String str2 = a2 + " (" + bm.a(commentTagInfo2.e) + ")";
                    SpannableString spannableString = new SpannableString(str2);
                    spannableString.setSpan(new AbsoluteSizeSpan(13, true), 0, a2.length(), 33);
                    spannableString.setSpan(new AbsoluteSizeSpan(11, true), a2.length(), str2.length(), 33);
                    textView2.setTextSize(0, (float) this.f3076a.getResources().getDimensionPixelSize(R.dimen.app_detail_comment_tag_text_size));
                    textView2.setText(spannableString);
                    textView2.setTag(R.id.tma_st_slot_tag, bm.a(i3 + 1));
                    textView2.setOnClickListener(new r(this, str, commentTagInfo2));
                    relativeLayout2.addView(textView2, layoutParams3);
                    if (!(commentTagInfo == null || commentTagInfo.f1204a == null || !commentTagInfo.f1204a.equals(commentTagInfo2.f1204a))) {
                        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
                        layoutParams4.addRule(7, textView2.getId());
                        layoutParams4.addRule(8, textView2.getId());
                        ImageView imageView3 = new ImageView(this.f3076a);
                        imageView3.setImageResource(R.drawable.pinglun_icon_on);
                        imageView3.setId(R.id.commenttag_selectimg_id);
                        relativeLayout2.addView(imageView3, layoutParams4);
                    }
                    ViewGroup.MarginLayoutParams marginLayoutParams2 = new ViewGroup.MarginLayoutParams(-2, -2);
                    marginLayoutParams2.leftMargin = 0;
                    marginLayoutParams2.rightMargin = 16;
                    marginLayoutParams2.topMargin = 0;
                    marginLayoutParams2.bottomMargin = 16;
                    relativeLayout2.setTag(commentTagInfo2);
                    this.e.addView(relativeLayout2, marginLayoutParams2);
                    l.a(new STInfoV2(STConst.ST_PAGE_APP_DETAIL_COMMENT, str + bm.a(i3 + 1), 0, STConst.ST_DEFAULT_SLOT, 100));
                    i2 = i3 + 1;
                } else {
                    imageView.setOnClickListener(new s(this));
                    return;
                }
            }
        }
    }

    public void c(int i2) {
        this.e.a(i2);
    }
}
