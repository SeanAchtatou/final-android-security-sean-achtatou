package com.tencent.nucleus.socialcontact.comment;

import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class bi implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PopViewDialog f3115a;

    bi(PopViewDialog popViewDialog) {
        this.f3115a = popViewDialog;
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void afterTextChanged(Editable editable) {
        String obj = editable.toString();
        this.f3115a.e.setText(String.format(this.f3115a.b.getString(R.string.comment_txt_tips), Integer.valueOf(obj.length())));
        if (obj.length() > 100) {
            this.f3115a.e.setVisibility(0);
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(this.f3115a.e.getText().toString());
            ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(-65536);
            ForegroundColorSpan foregroundColorSpan2 = new ForegroundColorSpan(this.f3115a.b.getResources().getColor(R.color.comment_over_txt_tips));
            spannableStringBuilder.setSpan(foregroundColorSpan, 0, this.f3115a.e.getText().toString().indexOf("/"), 33);
            spannableStringBuilder.setSpan(foregroundColorSpan2, this.f3115a.e.getText().toString().indexOf("/") + 1, this.f3115a.e.getText().toString().length(), 18);
            this.f3115a.e.setText(spannableStringBuilder);
        } else if (obj.length() > 0) {
            this.f3115a.e.setVisibility(0);
            this.f3115a.e.setTextColor(this.f3115a.b.getResources().getColor(R.color.comment_over_txt_tips));
            String str = null;
            CharSequence text = this.f3115a.g.getText();
            if (text != null) {
                str = text.toString();
            }
            String string = this.f3115a.b.getString(R.string.comment_expendcount_error);
            if (this.f3115a.g.getVisibility() == 0 && !TextUtils.isEmpty(str) && str.equals(string)) {
                this.f3115a.g.setVisibility(8);
            }
        } else {
            this.f3115a.e.setVisibility(0);
        }
    }
}
