package com.tencent.connector.a;

import com.qq.AppService.s;
import com.qq.l.p;
import com.tencent.assistant.st.a;
import com.tencent.wcs.c.b;

/* compiled from: ProGuard */
class d extends Thread {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f2383a;

    d(c cVar) {
        this.f2383a = cVar;
    }

    public void run() {
        if (this.f2383a.e != null) {
            boolean z = false;
            long currentTimeMillis = System.currentTimeMillis();
            synchronized (this.f2383a.e) {
                try {
                    this.f2383a.e.wait((long) this.f2383a.d);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    z = true;
                }
            }
            long currentTimeMillis2 = System.currentTimeMillis();
            if (!z && currentTimeMillis2 - currentTimeMillis > ((long) (this.f2383a.d - 100))) {
                a.a().b((byte) 7);
                p.m().c(p.m().p().b(), 2, 1);
                s.a();
                if (this.f2383a.b != null) {
                    this.f2383a.b.sendMessage(this.f2383a.b.obtainMessage(20, this.f2383a.c));
                }
                b.a("WaitPCRequestTask waiting timeout !!!");
            }
        }
    }
}
