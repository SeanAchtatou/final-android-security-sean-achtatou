package com.tencent.connector;

import android.media.MediaPlayer;

/* compiled from: ProGuard */
class b implements MediaPlayer.OnCompletionListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CaptureActivity f2384a;

    b(CaptureActivity captureActivity) {
        this.f2384a = captureActivity;
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        this.f2384a.i.release();
        MediaPlayer unused = this.f2384a.i = (MediaPlayer) null;
    }
}
