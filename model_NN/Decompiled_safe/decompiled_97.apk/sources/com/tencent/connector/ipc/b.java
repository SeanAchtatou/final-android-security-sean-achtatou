package com.tencent.connector.ipc;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import com.qq.AppService.AppService;
import com.qq.AppService.AstApp;
import com.qq.AppService.w;
import com.qq.AppService.x;

/* compiled from: ProGuard */
class b implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f2418a;

    b(a aVar) {
        this.f2418a = aVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.connector.ipc.a.a(com.tencent.connector.ipc.a, boolean):boolean
     arg types: [com.tencent.connector.ipc.a, int]
     candidates:
      com.tencent.connector.ipc.a.a(com.tencent.connector.ipc.a, com.qq.AppService.w):com.qq.AppService.w
      com.tencent.connector.ipc.a.a(com.tencent.connector.ipc.a, boolean):boolean */
    public void onServiceDisconnected(ComponentName componentName) {
        boolean unused = this.f2418a.e = false;
        w unused2 = this.f2418a.d = (w) null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.connector.ipc.a.a(com.tencent.connector.ipc.a, boolean):boolean
     arg types: [com.tencent.connector.ipc.a, int]
     candidates:
      com.tencent.connector.ipc.a.a(com.tencent.connector.ipc.a, com.qq.AppService.w):com.qq.AppService.w
      com.tencent.connector.ipc.a.a(com.tencent.connector.ipc.a, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        w unused = this.f2418a.d = x.a(iBinder);
        if (!(this.f2418a.d == null || this.f2418a.i == null)) {
            try {
                this.f2418a.d.a(this.f2418a.i);
                boolean unused2 = this.f2418a.e = true;
                if (this.f2418a.g != null) {
                    this.f2418a.g.a();
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        if (!this.f2418a.c()) {
            try {
                AstApp i = AstApp.i();
                Intent intent = new Intent();
                intent.setClass(i, AppService.class);
                intent.putExtra("isWifi", true);
                i.startService(intent);
            } catch (Throwable th) {
            }
        }
    }
}
