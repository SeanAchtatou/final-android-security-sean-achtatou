package com.tencent.connector;

import android.view.View;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class n implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UsbDebugModeAlertActivity f2424a;

    n(UsbDebugModeAlertActivity usbDebugModeAlertActivity) {
        this.f2424a = usbDebugModeAlertActivity;
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancel /*2131165503*/:
                this.f2424a.finish();
                return;
            case R.id.ok /*2131165504*/:
                this.f2424a.b();
                return;
            default:
                return;
        }
    }
}
