package com.tencent.smtt.sdk;

import com.tencent.smtt.export.external.interfaces.IX5WebBackForwardList;

public class WebBackForwardList {
    private android.webkit.WebBackForwardList mSysWebBackForwardList = null;
    private IX5WebBackForwardList mWebBackForwardListImpl = null;

    static WebBackForwardList wrap(IX5WebBackForwardList x5List) {
        if (x5List == null) {
            return null;
        }
        WebBackForwardList ret = new WebBackForwardList();
        ret.mWebBackForwardListImpl = x5List;
        return ret;
    }

    static WebBackForwardList wrap(android.webkit.WebBackForwardList sysList) {
        if (sysList == null) {
            return null;
        }
        WebBackForwardList ret = new WebBackForwardList();
        ret.mSysWebBackForwardList = sysList;
        return ret;
    }

    public WebHistoryItem getCurrentItem() {
        if (this.mWebBackForwardListImpl != null) {
            return WebHistoryItem.wrap(this.mWebBackForwardListImpl.getCurrentItem());
        }
        return WebHistoryItem.wrap(this.mSysWebBackForwardList.getCurrentItem());
    }

    public int getCurrentIndex() {
        if (this.mWebBackForwardListImpl != null) {
            return this.mWebBackForwardListImpl.getCurrentIndex();
        }
        return this.mSysWebBackForwardList.getCurrentIndex();
    }

    public WebHistoryItem getItemAtIndex(int index) {
        if (this.mWebBackForwardListImpl != null) {
            return WebHistoryItem.wrap(this.mWebBackForwardListImpl.getItemAtIndex(index));
        }
        return WebHistoryItem.wrap(this.mSysWebBackForwardList.getItemAtIndex(index));
    }

    public int getSize() {
        if (this.mWebBackForwardListImpl != null) {
            return this.mWebBackForwardListImpl.getSize();
        }
        return this.mSysWebBackForwardList.getSize();
    }
}
