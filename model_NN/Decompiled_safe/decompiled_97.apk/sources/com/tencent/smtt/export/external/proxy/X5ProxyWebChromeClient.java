package com.tencent.smtt.export.external.proxy;

import com.tencent.smtt.export.external.WebViewWizardBase;
import com.tencent.smtt.export.external.interfaces.IX5WebChromeClient;

public abstract class X5ProxyWebChromeClient extends ProxyWebChromeClient {
    public X5ProxyWebChromeClient(WebViewWizardBase wizard) {
        this.mWebChromeClient = (IX5WebChromeClient) wizard.newInstance(wizard.isDynamicMode(), "com.tencent.smtt.webkit.WebChromeClient");
    }
}
