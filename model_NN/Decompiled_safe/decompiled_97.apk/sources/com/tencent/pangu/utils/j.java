package com.tencent.pangu.utils;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.manager.au;

/* compiled from: ProGuard */
final class j extends AppConst.TwoBtnDialogInfo {
    j() {
    }

    public void onLeftBtnClick() {
        l.a(new STInfoV2(STConst.ST_PAGE_GUIDE_RESULT_HUANJI_DIALOG, "03_002", 0, STConst.ST_DEFAULT_SLOT, 200));
        au.i = true;
    }

    public void onRightBtnClick() {
        e.b();
        l.a(new STInfoV2(STConst.ST_PAGE_GUIDE_RESULT_HUANJI_DIALOG, "03_001", 0, STConst.ST_DEFAULT_SLOT, 200));
        au.i = true;
    }

    public void onCancell() {
        l.a(new STInfoV2(STConst.ST_PAGE_GUIDE_RESULT_HUANJI_DIALOG, "03_002", 0, STConst.ST_DEFAULT_SLOT, 200));
        au.i = true;
    }
}
