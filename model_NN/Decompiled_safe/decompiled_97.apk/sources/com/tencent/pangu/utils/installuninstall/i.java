package com.tencent.pangu.utils.installuninstall;

import com.tencent.pangu.manager.DownloadProxy;

/* compiled from: ProGuard */
class i extends o {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ InstallUninstallDialogManager f4004a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    i(InstallUninstallDialogManager installUninstallDialogManager) {
        super(installUninstallDialogManager);
        this.f4004a = installUninstallDialogManager;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean
     arg types: [com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, int]
     candidates:
      com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT):com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT
      com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_TYPE, com.tencent.pangu.utils.installuninstall.InstallUninstallTaskBean):boolean
      com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.manager.DownloadProxy.b(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.pangu.manager.DownloadProxy.b(com.tencent.assistant.localres.model.LocalApkInfo, java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>):java.lang.String
      com.tencent.pangu.manager.DownloadProxy.b(java.lang.String, boolean):void */
    public void onLeftBtnClick() {
        boolean unused = this.f4004a.c = false;
        this.f4004a.c();
        DownloadProxy.a().b(this.b.downloadTicket, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean
     arg types: [com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, int]
     candidates:
      com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT):com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT
      com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_TYPE, com.tencent.pangu.utils.installuninstall.InstallUninstallTaskBean):boolean
      com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean */
    public void onRightBtnClick() {
        boolean unused = this.f4004a.c = false;
        this.f4004a.c();
        this.f4004a.b(this.b.downloadTicket);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean
     arg types: [com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, int]
     candidates:
      com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT):com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_RESULT
      com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager$DIALOG_DEALWITH_TYPE, com.tencent.pangu.utils.installuninstall.InstallUninstallTaskBean):boolean
      com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager.a(com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager, boolean):boolean */
    public void onCancell() {
        boolean unused = this.f4004a.c = false;
        this.f4004a.c();
    }
}
