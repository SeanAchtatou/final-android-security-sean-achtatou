package com.tencent.pangu.utils.installuninstall;

import android.util.Pair;

/* compiled from: ProGuard */
class ah extends Thread {

    /* renamed from: a  reason: collision with root package name */
    boolean f3995a = true;
    InstallUninstallTaskBean b;
    InstallUninstallDialogManager c;
    final /* synthetic */ ac d;

    public ah(ac acVar) {
        this.d = acVar;
        setName("Thread_SystemInstall");
        this.c = new InstallUninstallDialogManager();
        this.c.a(false);
    }

    public void run() {
        while (this.f3995a) {
            if (this.b == null) {
                if (this.d.e()) {
                    this.d.g();
                } else {
                    this.b = this.d.f();
                    if (this.b == null) {
                    }
                }
            }
            try {
                if (this.b.action == 1) {
                    Pair a2 = this.d.a(this.c, this.b);
                    if (a2 == null || !((Boolean) a2.first).booleanValue()) {
                        this.b = null;
                    } else {
                        InstallUninstallUtil.a(this.b.packageName, this.b.versionCode, this.b.filePath);
                        this.b = null;
                    }
                }
            } catch (Throwable th) {
            }
        }
    }
}
