package com.tencent.pangu.utils;

import android.animation.Animator;

/* compiled from: ProGuard */
class b implements Animator.AnimatorListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f3976a;

    b(a aVar) {
        this.f3976a = aVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.utils.a.a(com.tencent.pangu.utils.a, boolean):boolean
     arg types: [com.tencent.pangu.utils.a, int]
     candidates:
      com.tencent.pangu.utils.a.a(com.tencent.pangu.utils.a, double):float
      com.tencent.pangu.utils.a.a(com.tencent.pangu.utils.a, float):void
      com.tencent.pangu.utils.a.a(float, float):void
      com.tencent.pangu.utils.a.a(com.tencent.pangu.utils.a, boolean):boolean */
    public void onAnimationStart(Animator animator) {
        this.f3976a.o.removeMessages(1);
        boolean unused = this.f3976a.m = false;
    }

    public void onAnimationRepeat(Animator animator) {
    }

    public void onAnimationEnd(Animator animator) {
        if (!this.f3976a.m) {
            this.f3976a.o.sendEmptyMessage(1);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.utils.a.a(com.tencent.pangu.utils.a, boolean):boolean
     arg types: [com.tencent.pangu.utils.a, int]
     candidates:
      com.tencent.pangu.utils.a.a(com.tencent.pangu.utils.a, double):float
      com.tencent.pangu.utils.a.a(com.tencent.pangu.utils.a, float):void
      com.tencent.pangu.utils.a.a(float, float):void
      com.tencent.pangu.utils.a.a(com.tencent.pangu.utils.a, boolean):boolean */
    public void onAnimationCancel(Animator animator) {
        boolean unused = this.f3976a.m = true;
    }
}
