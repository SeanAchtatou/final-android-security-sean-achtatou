package com.tencent.pangu.utils;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Handler;
import android.text.TextUtils;
import android.view.animation.AccelerateInterpolator;
import com.tencent.assistant.utils.XLog;
import java.lang.reflect.Method;
import java.util.Random;

@SuppressLint({"NewApi"})
/* compiled from: ProGuard */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private long f3975a = 500;
    private long b = this.f3975a;
    private ObjectAnimator c;
    private float d;
    /* access modifiers changed from: private */
    public float e;
    /* access modifiers changed from: private */
    public float f;
    private Object g;
    private Random h = new Random();
    private String i;
    private Method j;
    private long k;
    private int l = Build.VERSION.SDK_INT;
    /* access modifiers changed from: private */
    public boolean m = false;
    private float n = 0.99f;
    /* access modifiers changed from: private */
    public Handler o = new c(this);

    static /* synthetic */ float a(a aVar, double d2) {
        float f2 = (float) (((double) aVar.e) + d2);
        aVar.e = f2;
        return f2;
    }

    public a(Object obj, String str) {
        String str2;
        this.g = obj;
        this.i = str;
        try {
            if (!TextUtils.isEmpty(str)) {
                if (str.length() > 1) {
                    str2 = "set" + str.substring(0, 1).toUpperCase() + str.substring(1);
                } else {
                    str2 = "set" + str.substring(0, 1).toUpperCase();
                }
                this.j = obj.getClass().getDeclaredMethod(str2, Float.TYPE);
            }
        } catch (NoSuchMethodException e2) {
            XLog.e("ljh", "Exception，找不到方法   " + e2.toString());
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void b(float f2) {
        if (this.j != null && this.g != null) {
            try {
                this.j.invoke(this.g, Float.valueOf(f2));
            } catch (Exception e2) {
                XLog.e("ljh", "反射不倒方法！ name = set" + this.i);
                e2.printStackTrace();
            }
        }
    }

    private void b() {
        this.o.sendEmptyMessage(1);
    }

    private void c() {
        if (this.c != null) {
            this.c.cancel();
        }
        this.c = ObjectAnimator.ofFloat(this.g, this.i, this.d, this.e);
        if (this.c != null) {
            this.c.setDuration(this.b);
            this.c.setInterpolator(new AccelerateInterpolator());
            this.c.addListener(new b(this));
            try {
                this.c.start();
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    public void a(float f2, float f3) {
        float f4;
        if (this.d > f2) {
            b(f2);
            this.d = f2;
        } else if (f2 >= this.n * f3) {
            a();
            b(f2);
        } else {
            this.f = f2;
            b(this.d);
            float f5 = f2 - this.d;
            if (f5 >= 0.0f) {
                if (((double) f5) > 0.5d) {
                    f4 = (float) (((double) f2) - 0.5d);
                } else {
                    f4 = (f5 * 0.8f) + this.d;
                }
                this.e = f4;
                this.b = Math.abs((long) (((double) (System.currentTimeMillis() - this.k)) * 1.5d));
                this.k = System.currentTimeMillis();
                if (this.b == 0 || this.b > 10000) {
                    this.b = this.f3975a;
                }
                if (this.g != null) {
                    if (this.l > 10) {
                        c();
                    } else {
                        b();
                    }
                }
                this.d = f4;
            }
        }
    }

    public void a() {
        this.d = this.e;
        this.o.removeMessages(1);
        if (this.c != null) {
            this.c.cancel();
        }
    }

    /* access modifiers changed from: private */
    public long d() {
        return (long) (90.0f + (this.h.nextFloat() * 50.0f));
    }

    public void a(float f2) {
        this.d = f2;
    }
}
