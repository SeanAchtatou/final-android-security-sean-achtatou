package com.tencent.pangu.utils.installuninstall;

import com.qq.AppService.AstApp;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.m;
import com.tencent.pangu.download.DownloadInfo;
import java.util.ArrayList;

/* compiled from: ProGuard */
class s implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ArrayList f4012a;
    final /* synthetic */ boolean b;
    final /* synthetic */ p c;

    s(p pVar, ArrayList arrayList, boolean z) {
        this.c = pVar;
        this.f4012a = arrayList;
        this.b = z;
    }

    public void run() {
        int i = 0;
        if (this.f4012a != null && !this.f4012a.isEmpty()) {
            if (this.b && !m.a().k()) {
                return;
            }
            if (this.c.a(1) == -2 && AstApp.i().l()) {
                ArrayList arrayList = new ArrayList();
                for (int i2 = 0; i2 < this.f4012a.size(); i2++) {
                    DownloadInfo downloadInfo = (DownloadInfo) this.f4012a.get(i2);
                    InstallUninstallTaskBean installUninstallTaskBean = new InstallUninstallTaskBean(0, 1, downloadInfo.getFilePath(), downloadInfo.packageName, downloadInfo.versionCode, downloadInfo.name, downloadInfo.signatrue, downloadInfo.downloadTicket, downloadInfo.fileSize);
                    installUninstallTaskBean.isAutoInstall = this.b;
                    arrayList.add(installUninstallTaskBean);
                    this.c.f4009a.sendMessage(this.c.f4009a.obtainMessage(EventDispatcherEnum.UI_EVENT_ADD_APP_INSTALL_TASK, installUninstallTaskBean));
                }
                this.c.a(arrayList);
            } else if (this.f4012a != null && !this.f4012a.isEmpty()) {
                while (true) {
                    int i3 = i;
                    if (i3 < this.f4012a.size()) {
                        DownloadInfo downloadInfo2 = (DownloadInfo) this.f4012a.get(i3);
                        this.c.a(downloadInfo2.downloadTicket, downloadInfo2.packageName, downloadInfo2.name, downloadInfo2.getFilePath(), downloadInfo2.versionCode, downloadInfo2.signatrue, downloadInfo2.downloadTicket, downloadInfo2.fileSize, this.b, downloadInfo2);
                        i = i3 + 1;
                    } else {
                        return;
                    }
                }
            }
        }
    }
}
