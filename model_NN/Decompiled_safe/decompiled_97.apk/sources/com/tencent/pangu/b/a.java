package com.tencent.pangu.b;

import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: ProGuard */
public class a implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    private static a f3468a;
    private static AstApp b;
    private static ConcurrentHashMap<String, ArrayList<b>> c = new ConcurrentHashMap<>();
    private ConcurrentHashMap<String, SimpleDownloadInfo.DownloadState> d = new ConcurrentHashMap<>();

    public static synchronized a a() {
        a aVar;
        synchronized (a.class) {
            if (f3468a == null) {
                f3468a = new a();
            }
            aVar = f3468a;
        }
        return aVar;
    }

    public a() {
        b = AstApp.i();
    }

    public void b() {
        b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING, this);
        b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_PAUSE, this);
        b.k().addUIEventListener(1007, this);
        b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC, this);
        b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_QUEUING, this);
        b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        b.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_ROOT_SILENT_INSTALL_START, this);
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:37:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void handleUIEvent(android.os.Message r6) {
        /*
            r5 = this;
            r4 = 1
            r3 = 0
            java.lang.String r0 = ""
            r1 = 0
            java.lang.Object r0 = r6.obj
            boolean r0 = r0 instanceof java.lang.String
            if (r0 == 0) goto L_0x0092
            java.lang.Object r0 = r6.obj
            java.lang.String r0 = (java.lang.String) r0
            boolean r2 = android.text.TextUtils.isEmpty(r0)
            if (r2 != 0) goto L_0x0092
            com.tencent.pangu.manager.DownloadProxy r1 = com.tencent.pangu.manager.DownloadProxy.a()
            com.tencent.pangu.download.DownloadInfo r0 = r1.d(r0)
        L_0x001d:
            int r1 = r6.what
            switch(r1) {
                case 1003: goto L_0x003f;
                case 1005: goto L_0x0051;
                case 1006: goto L_0x0051;
                case 1007: goto L_0x0023;
                case 1008: goto L_0x003f;
                case 1009: goto L_0x005d;
                case 1025: goto L_0x0080;
                default: goto L_0x0022;
            }
        L_0x0022:
            return
        L_0x0023:
            if (r0 == 0) goto L_0x0022
            boolean r1 = com.tencent.assistant.net.c.a()
            if (r1 != 0) goto L_0x0035
            if (r0 == 0) goto L_0x0035
            int r1 = r0.errorCode
            if (r1 != 0) goto L_0x0035
            r1 = -15
            r0.errorCode = r1
        L_0x0035:
            boolean r1 = r5.a(r0, r4)
            if (r1 == 0) goto L_0x0022
            r5.a(r0)
            goto L_0x0022
        L_0x003f:
            if (r0 == 0) goto L_0x0022
            int r1 = r0.errorCode
            if (r1 == 0) goto L_0x0047
            r0.errorCode = r3
        L_0x0047:
            boolean r1 = r5.a(r0, r4)
            if (r1 == 0) goto L_0x0022
            r5.a(r0)
            goto L_0x0022
        L_0x0051:
            if (r0 == 0) goto L_0x0022
            boolean r1 = r5.a(r0, r4)
            if (r1 == 0) goto L_0x0022
            r5.a(r0)
            goto L_0x0022
        L_0x005d:
            java.lang.Object r0 = r6.obj
            boolean r0 = r0 instanceof com.tencent.pangu.download.DownloadInfo
            if (r0 == 0) goto L_0x0022
            java.lang.Object r0 = r6.obj
            com.tencent.pangu.download.DownloadInfo r0 = (com.tencent.pangu.download.DownloadInfo) r0
            boolean r1 = r5.a(r0, r3)
            if (r1 == 0) goto L_0x0022
            com.tencent.pangu.download.DownloadInfo r1 = r0.clone()
            com.tencent.pangu.download.SimpleDownloadInfo$DownloadState r2 = com.tencent.pangu.download.SimpleDownloadInfo.DownloadState.DELETED
            r1.downloadState = r2
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.tencent.pangu.download.SimpleDownloadInfo$DownloadState> r2 = r5.d
            java.lang.String r0 = r0.downloadTicket
            r2.remove(r0)
            r5.a(r1)
            goto L_0x0022
        L_0x0080:
            boolean r1 = r5.a(r0, r3)
            if (r1 == 0) goto L_0x0022
            com.tencent.pangu.download.DownloadInfo r1 = r0.clone()
            com.tencent.pangu.download.SimpleDownloadInfo$DownloadState r2 = com.tencent.pangu.download.SimpleDownloadInfo.DownloadState.INSTALLING
            r0.downloadState = r2
            r5.a(r1)
            goto L_0x0022
        L_0x0092:
            r0 = r1
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.pangu.b.a.handleUIEvent(android.os.Message):void");
    }

    private boolean a(DownloadInfo downloadInfo, boolean z) {
        if (downloadInfo == null || TextUtils.isEmpty(downloadInfo.hostAppId) || TextUtils.isEmpty(downloadInfo.hostPackageName) || TextUtils.isEmpty(downloadInfo.taskId)) {
            return false;
        }
        if (!z || b(downloadInfo)) {
            return true;
        }
        return false;
    }

    private boolean b(DownloadInfo downloadInfo) {
        if (!this.d.containsKey(downloadInfo.downloadTicket)) {
            this.d.put(downloadInfo.downloadTicket, downloadInfo.downloadState);
            return true;
        } else if (this.d.get(downloadInfo.downloadTicket) == downloadInfo.downloadState) {
            return false;
        } else {
            this.d.put(downloadInfo.downloadTicket, downloadInfo.downloadState);
            return true;
        }
    }

    public void a(DownloadInfo downloadInfo) {
        ArrayList arrayList;
        if (downloadInfo != null) {
            a(String.valueOf(downloadInfo.apkId), downloadInfo.packageName, downloadInfo.versionCode, downloadInfo.grayVersionCode, downloadInfo.hostAppId, downloadInfo.hostPackageName, downloadInfo.hostVersionCode, downloadInfo.taskId);
            String valueOf = String.valueOf(downloadInfo.apkId);
            if (c.containsKey(valueOf) && (arrayList = c.get(valueOf)) != null && !arrayList.isEmpty()) {
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    b bVar = (b) it.next();
                    HashMap hashMap = new HashMap();
                    hashMap.put("hostPackageName", bVar.b);
                    hashMap.put("hostVersion", bVar.c);
                    hashMap.put("sngAppId", bVar.f3470a);
                    hashMap.put("taskId", bVar.d);
                    hashMap.put("taskApkId", String.valueOf(downloadInfo.apkId));
                    hashMap.put("taskAppId", String.valueOf(downloadInfo.appId));
                    hashMap.put("taskPackageName", downloadInfo.packageName);
                    hashMap.put("taskVersion", String.valueOf(downloadInfo.versionCode));
                    hashMap.put("via", downloadInfo.via);
                    hashMap.put("state", String.valueOf(com.tencent.pangu.b.a.a.a(downloadInfo)));
                    hashMap.put("uin", downloadInfo.uin);
                    hashMap.put("uinType", downloadInfo.uinType);
                    hashMap.put("errorCode", String.valueOf(downloadInfo.errorCode));
                    hashMap.put("errorMsg", "a");
                    com.tencent.e.a.b.a.a(b, hashMap);
                }
            }
        }
    }

    public void a(String str, String str2, int i, int i2, String str3, String str4, String str5, String str6) {
        ArrayList arrayList;
        DownloadInfo downloadInfo = null;
        if (!TextUtils.isEmpty(str)) {
            downloadInfo = DownloadProxy.a().d(str);
        }
        if (downloadInfo == null) {
            downloadInfo = DownloadProxy.a().a(str2, i, i2);
        }
        if (downloadInfo != null) {
            downloadInfo.hostAppId = str3;
            downloadInfo.hostPackageName = str4;
            downloadInfo.hostVersionCode = str5;
            downloadInfo.taskId = str6;
            ArrayList arrayList2 = c.get(str);
            if (arrayList2 == null) {
                ArrayList arrayList3 = new ArrayList();
                c.put(str, arrayList3);
                arrayList = arrayList3;
            } else {
                arrayList = arrayList2;
            }
            if (!arrayList.isEmpty()) {
                ArrayList arrayList4 = new ArrayList();
                if (!TextUtils.isEmpty(str3)) {
                    Iterator it = arrayList.iterator();
                    while (it.hasNext()) {
                        b bVar = (b) it.next();
                        if (!str3.equalsIgnoreCase(bVar.f3470a)) {
                            arrayList4.add(new b(bVar.f3470a, bVar.b, bVar.c, bVar.d));
                        }
                    }
                    arrayList4.add(new b(str3, str4, str5, str6));
                }
                if (!arrayList4.isEmpty()) {
                    arrayList.clear();
                    arrayList.addAll(arrayList4);
                    return;
                }
                return;
            }
            arrayList.add(new b(str3, str4, str5, str6));
        }
    }
}
