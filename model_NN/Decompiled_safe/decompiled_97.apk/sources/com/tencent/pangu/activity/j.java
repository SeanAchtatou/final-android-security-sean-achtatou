package com.tencent.pangu.activity;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.widget.ImageView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.utils.a;
import com.tencent.pangu.component.ShareAppDialog;

/* compiled from: ProGuard */
class j implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppDetailActivityV5 f3384a;

    j(AppDetailActivityV5 appDetailActivityV5) {
        this.f3384a = appDetailActivityV5;
    }

    public void onCancel(DialogInterface dialogInterface) {
        if (dialogInterface != null) {
            Bitmap c = ((ShareAppDialog) dialogInterface).c();
            ImageView imageView = new ImageView(this.f3384a);
            imageView.setImageBitmap(c);
            a.a(this.f3384a.F, this.f3384a.H.findViewById(R.id.show_more), imageView);
        }
    }
}
