package com.tencent.pangu.activity;

import android.text.TextUtils;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.utils.XLog;
import com.tencent.pangu.activity.SearchActivity;
import com.tencent.pangu.model.a.f;
import com.tencent.pangu.model.a.g;
import com.tencent.pangu.model.b;
import com.tencent.pangu.module.a.k;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
class ck extends k {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchActivity f3362a;

    private ck(SearchActivity searchActivity) {
        this.f3362a = searchActivity;
    }

    /* synthetic */ ck(SearchActivity searchActivity, bu buVar) {
        this(searchActivity);
    }

    public void a(g gVar, int i) {
        XLog.d("SearchActivity", "onNotifyUISearchHotWordsFinished:(errorcode)" + i);
        f fVar = null;
        if (gVar != null) {
            fVar = (f) gVar.a(Integer.valueOf(this.f3362a.O));
        }
        if (i == 0 && fVar != null) {
            XLog.d("SearchActivity", "onNotifyUISearchHotWordsFinished:" + this.f3362a.O + ":" + fVar.b());
            this.f3362a.w.a(fVar);
        }
        if (this.f3362a.H() == SearchActivity.Layer.Search) {
            XLog.d("SearchActivity", "onNotifyUISearchHotWordsFinished:(is Layer.Search)");
            if (i != 0) {
                XLog.d("SearchActivity", "onNotifyUISearchHotWordsFinished:(is Layer.Search): not ok");
                if (-800 == i) {
                    XLog.d("SearchActivity", "onNotifyUISearchHotWordsFinished:(is Layer.Search): unable");
                    this.f3362a.d(30);
                    return;
                }
                XLog.d("SearchActivity", "onNotifyUISearchHotWordsFinished:(is Layer.Search): others");
                this.f3362a.d(20);
            } else if (fVar == null) {
                this.f3362a.G();
            } else if (TextUtils.isEmpty(this.f3362a.v.a().getText())) {
                this.f3362a.C();
            }
        }
    }

    public void a(ArrayList<SimpleAppModel> arrayList, ArrayList<String> arrayList2, int i) {
        if (i != 0) {
            this.f3362a.c(8);
        } else if (arrayList.size() == 0 && arrayList2.size() == 0) {
            this.f3362a.c(8);
        } else {
            if (this.f3362a.f() != 200703) {
                this.f3362a.a(this.f3362a.n(), (String) null, (String) null);
            }
            this.f3362a.A.a(arrayList, arrayList2, this.f3362a.v.a().getText().toString());
            this.f3362a.D();
        }
    }

    public void a(int i, int i2, boolean z, int i3, ArrayList<String> arrayList, boolean z2, List<b> list, long j, String str, int i4) {
    }
}
