package com.tencent.pangu.activity;

import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/* compiled from: ProGuard */
class ca implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchActivity f3352a;

    ca(SearchActivity searchActivity) {
        this.f3352a = searchActivity;
    }

    public void run() {
        if (this.f3352a.v != null) {
            EditText a2 = this.f3352a.v.a();
            InputMethodManager inputMethodManager = (InputMethodManager) this.f3352a.getSystemService("input_method");
            if (inputMethodManager != null) {
                inputMethodManager.showSoftInput(a2, 1);
            }
        }
    }
}
