package com.tencent.pangu.activity;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class by extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchActivity f3349a;

    by(SearchActivity searchActivity) {
        this.f3349a = searchActivity;
    }

    public void onTMAClick(View view) {
        if (this.f3349a.J) {
            this.f3349a.v.a().setText((CharSequence) null);
            this.f3349a.v.a().setSelection(0);
            this.f3349a.B();
            return;
        }
        this.f3349a.finish();
    }

    public STInfoV2 getStInfo(View view) {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3349a, 200);
        buildSTInfo.slotId = a.a("03", "003");
        return buildSTInfo;
    }
}
