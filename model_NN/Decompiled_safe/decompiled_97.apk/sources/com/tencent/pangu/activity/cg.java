package com.tencent.pangu.activity;

import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class cg extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SearchActivity f3358a;

    cg(SearchActivity searchActivity) {
        this.f3358a = searchActivity;
    }

    public void onTMAClick(View view) {
        if (!TextUtils.isEmpty(this.f3358a.P) && !TextUtils.isEmpty(this.f3358a.Q)) {
            this.f3358a.v.b(this.f3358a.Q);
            String unused = this.f3358a.P = (String) null;
            String unused2 = this.f3358a.Q = (String) null;
            int unused3 = this.f3358a.N = 200705;
        } else if (this.f3358a.z != null) {
            int unused4 = this.f3358a.N = this.f3358a.z.b();
        } else {
            int unused5 = this.f3358a.N = (int) STConst.ST_PAGE_SEARCH;
        }
        String b = this.f3358a.v.b();
        if (TextUtils.isEmpty(b) || TextUtils.isEmpty(b.trim())) {
            Toast.makeText(this.f3358a, (int) R.string.must_input_keyword, 0).show();
            return;
        }
        this.f3358a.K.removeMessages(0);
        this.f3358a.G.a(this.f3358a.I);
        this.f3358a.z();
    }

    public STInfoV2 getStInfo() {
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.f3358a, 200);
        buildSTInfo.slotId = a.a("03", "004");
        return buildSTInfo;
    }
}
