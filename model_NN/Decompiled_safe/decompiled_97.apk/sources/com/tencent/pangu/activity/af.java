package com.tencent.pangu.activity;

import com.tencent.pangu.adapter.DownloadInfoMultiAdapter;

/* compiled from: ProGuard */
class af implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f3307a;
    final /* synthetic */ DownloadActivity b;

    af(DownloadActivity downloadActivity, int i) {
        this.b = downloadActivity;
        this.f3307a = i;
    }

    public void run() {
        this.b.x.a(((long) this.f3307a) == this.b.U ? DownloadInfoMultiAdapter.CreatingTaskStatusEnum.NONE : DownloadInfoMultiAdapter.CreatingTaskStatusEnum.CREATING);
        this.b.x.notifyDataSetChanged();
    }
}
