package com.tencent.pangu.adapter;

import android.text.TextUtils;
import com.tencent.assistant.component.ApkMetaInfoLoader;

/* compiled from: ProGuard */
class v implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ApkMetaInfoLoader.MetaInfo f3463a;
    final /* synthetic */ u b;

    v(u uVar, ApkMetaInfoLoader.MetaInfo metaInfo) {
        this.b = uVar;
        this.f3463a = metaInfo;
    }

    public void run() {
        if (this.b.b.getTag() != null && this.b.b.getTag().equals(this.b.f3462a.m) && this.f3463a != null) {
            if (this.f3463a.icon != null) {
                this.b.b.setImageDrawable(this.f3463a.icon);
            }
            if (!TextUtils.isEmpty(this.f3463a.name)) {
                this.b.c.setText(this.f3463a.name);
            }
        }
    }
}
