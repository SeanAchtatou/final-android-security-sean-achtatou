package com.tencent.pangu.adapter;

import android.content.Context;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.AppIconView;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.invalidater.ListViewScrollListener;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.model.AppGroupInfo;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.utils.by;
import com.tencent.assistant.utils.t;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.component.fps.FPSTextView;
import com.tencent.assistantv2.manager.a;
import com.tencent.assistantv2.st.b.b;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.download.DownloadInfo;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/* compiled from: ProGuard */
public class al extends BaseExpandableListAdapter implements UIEventListener {
    private static int h = 0;
    private static int i = (h + 1);

    /* renamed from: a  reason: collision with root package name */
    public AstApp f3414a = AstApp.i();
    private LinkedHashMap<AppGroupInfo, ArrayList<SimpleAppModel>> b;
    private LinkedHashMap<AppGroupInfo, ArrayList<SimpleAppModel>> c;
    private LinkedHashMap<AppGroupInfo, ArrayList<SimpleAppModel>> d;
    /* access modifiers changed from: private */
    public Context e;
    private LayoutInflater f;
    private LayoutInflater g;
    /* access modifiers changed from: private */
    public View j;
    private HashMap<String, Integer> k = new HashMap<>();
    private ArrayList<AppGroupInfo> l;
    private int m = 2000;
    private long n = -100;
    private IViewInvalidater o;
    private b p = new b();

    public al() {
    }

    public al(Context context, View view, int i2) {
        this.e = context;
        this.j = view;
        this.l = new ArrayList<>();
        this.f = LayoutInflater.from(this.e);
        this.g = LayoutInflater.from(this.e);
        this.j = view;
    }

    public void a(Map<AppGroupInfo, ArrayList<SimpleAppModel>> map, long j2) {
        if (map != null && map.size() > 0) {
            if (this.d == null) {
                this.d = new LinkedHashMap<>();
            } else if (!this.d.isEmpty()) {
                this.d.clear();
            }
            if (this.l == null) {
                this.l = new ArrayList<>();
            } else if (!this.l.isEmpty()) {
                this.l.clear();
                this.l = new ArrayList<>();
            }
            LinkedHashMap<AppGroupInfo, ArrayList<SimpleAppModel>> a2 = a(map);
            this.d.putAll(a2);
            for (AppGroupInfo add : a2.keySet()) {
                this.l.add(add);
            }
            a(this.d);
            a(a.a().b().c());
        }
    }

    public void b(Map<AppGroupInfo, ArrayList<SimpleAppModel>> map, long j2) {
        if (map != null && map.size() > 0) {
            if (this.d == null) {
                this.d = new LinkedHashMap<>();
            }
            LinkedHashMap<AppGroupInfo, ArrayList<SimpleAppModel>> a2 = a(map);
            this.d.putAll(a2);
            for (AppGroupInfo add : a2.keySet()) {
                this.l.add(add);
            }
            a(this.d);
            a(a.a().b().c());
        }
    }

    private LinkedHashMap<AppGroupInfo, ArrayList<SimpleAppModel>> a(Map<AppGroupInfo, ArrayList<SimpleAppModel>> map) {
        LinkedHashMap<AppGroupInfo, ArrayList<SimpleAppModel>> linkedHashMap = new LinkedHashMap<>();
        if (map != null && !map.isEmpty()) {
            for (AppGroupInfo next : map.keySet()) {
                ArrayList arrayList = map.get(next);
                ArrayList arrayList2 = new ArrayList();
                ArrayList arrayList3 = new ArrayList();
                ArrayList arrayList4 = new ArrayList();
                if (arrayList != null && !arrayList.isEmpty()) {
                    if (arrayList.size() > 5) {
                        Iterator it = arrayList.iterator();
                        while (it.hasNext()) {
                            SimpleAppModel simpleAppModel = (SimpleAppModel) it.next();
                            if (simpleAppModel.u() == AppConst.AppState.INSTALLED) {
                                arrayList3.add(simpleAppModel);
                            } else {
                                arrayList4.add(simpleAppModel);
                            }
                        }
                        if (arrayList4.size() >= 5) {
                            arrayList2.addAll(arrayList4.subList(0, 5));
                        } else if (arrayList4.size() != 0) {
                            arrayList2.addAll(arrayList4);
                            if (arrayList2.size() < 5) {
                                arrayList2.addAll(arrayList2.size(), arrayList3.subList(0, 5 - arrayList2.size()));
                            }
                        }
                    } else if (arrayList.size() >= 3) {
                        Iterator it2 = arrayList.iterator();
                        while (it2.hasNext()) {
                            SimpleAppModel simpleAppModel2 = (SimpleAppModel) it2.next();
                            if (simpleAppModel2.u() == AppConst.AppState.INSTALLED) {
                                arrayList3.add(simpleAppModel2);
                            } else {
                                arrayList4.add(simpleAppModel2);
                            }
                        }
                        if (arrayList4.size() > 0) {
                            arrayList2.addAll(arrayList4);
                            arrayList2.addAll(arrayList3);
                        }
                    }
                    if (a.a().b().c()) {
                        if (arrayList2.size() > 0) {
                            linkedHashMap.put(next, arrayList2);
                        }
                    } else if (arrayList2.size() > 0) {
                        linkedHashMap.put(next, arrayList2);
                    } else {
                        linkedHashMap.put(next, arrayList3);
                    }
                }
            }
        }
        return linkedHashMap;
    }

    private void a(LinkedHashMap<AppGroupInfo, ArrayList<SimpleAppModel>> linkedHashMap) {
        this.c = new LinkedHashMap<>();
        if (linkedHashMap != null && !linkedHashMap.isEmpty()) {
            for (AppGroupInfo next : linkedHashMap.keySet()) {
                ArrayList arrayList = new ArrayList();
                ArrayList arrayList2 = linkedHashMap.get(next);
                if (!(arrayList2 == null || arrayList2.size() == 0)) {
                    Iterator it = arrayList2.iterator();
                    while (it.hasNext()) {
                        SimpleAppModel simpleAppModel = (SimpleAppModel) it.next();
                        if (ApkResourceManager.getInstance().getLocalApkInfo(simpleAppModel.c) == null || ApkResourceManager.getInstance().getLocalApkInfo(simpleAppModel.c).mVersionCode < simpleAppModel.g) {
                            arrayList.add(simpleAppModel);
                        }
                    }
                    if (arrayList.size() > 0) {
                        this.c.put(next, arrayList);
                    }
                }
            }
        }
    }

    public void a(int i2, long j2) {
        this.m = i2;
        this.n = j2;
    }

    public Object getChild(int i2, int i3) {
        if (this.b == null || this.b.size() <= 0) {
            return null;
        }
        return this.b.get(this.l.get(i2)).get(i3);
    }

    public long getChildId(int i2, int i3) {
        return (long) i3;
    }

    public View getGroupView(int i2, boolean z, View view, ViewGroup viewGroup) {
        as asVar;
        if (view == null) {
            view = this.f.inflate((int) R.layout.rank_customizelist_group_item, (ViewGroup) null);
            asVar = new as(this);
            asVar.f3421a = (TextView) view.findViewById(R.id.group_title);
            view.setTag(asVar);
            if (t.b <= 320) {
                asVar.f3421a.setBackgroundResource(R.drawable.common_cardbg_middle_selector);
            }
        } else {
            asVar = (as) view.getTag();
        }
        asVar.f3421a.setText(this.l.get(i2).a());
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) asVar.f3421a.getLayoutParams();
        if (layoutParams != null) {
            if (i2 == 0) {
                layoutParams.topMargin = 0;
            } else {
                layoutParams.topMargin = by.a(this.e, 8.0f);
            }
            asVar.f3421a.setLayoutParams(layoutParams);
        }
        return view;
    }

    public View getChildView(int i2, int i3, boolean z, View view, ViewGroup viewGroup) {
        SimpleAppModel simpleAppModel;
        ArrayList arrayList = this.b.get(this.l.get(i2));
        if (arrayList == null || i3 > arrayList.size() - 1) {
            simpleAppModel = null;
        } else {
            simpleAppModel = (SimpleAppModel) arrayList.get(i3);
        }
        int i4 = 0;
        if (view != null) {
            i4 = ((Integer) view.getTag(R.id.card_type)).intValue();
        }
        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.e, simpleAppModel, b(i2, i3), 100, null);
        buildSTInfo.setCategoryId(this.n);
        if (this.p != null) {
            this.p.exposure(buildSTInfo);
        }
        if (h == a(i2, i3) && h == i4) {
            return a(view, simpleAppModel, i2, i3, z, buildSTInfo);
        }
        if (h == a(i2, i3) && i == i4) {
            return a(null, simpleAppModel, i2, i3, z, buildSTInfo);
        }
        if (i == a(i2, i3) && i == i4) {
            return a(view, simpleAppModel, i2, i3, buildSTInfo);
        }
        if (i == a(i2, i3) && h == i4) {
            return a((View) null, simpleAppModel, i2, i3, buildSTInfo);
        }
        return view;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.widget.RelativeLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private View a(View view, SimpleAppModel simpleAppModel, int i2, int i3, boolean z, STInfoV2 sTInfoV2) {
        at atVar;
        if (view == null) {
            atVar = new at(this, null);
            view = this.g.inflate((int) R.layout.rank_item_normal, (ViewGroup) new RelativeLayout(this.e), true);
            atVar.f3422a = view;
            atVar.b = (TXAppIconView) view.findViewById(R.id.app_icon_img);
            atVar.b.setInvalidater(this.o);
            atVar.c = (FPSTextView) view.findViewById(R.id.app_name_txt);
            atVar.d = (DownloadButton) view.findViewById(R.id.state_app_btn);
            atVar.e = (ListItemInfoView) view.findViewById(R.id.download_info);
            atVar.f = (ImageView) view.findViewById(R.id.last_line);
            view.setTag(atVar);
        } else {
            atVar = (at) view.getTag();
        }
        view.setTag(R.id.card_type, Integer.valueOf(h));
        atVar.f3422a.setTag(R.id.tma_st_slot_tag, b(i2, i3));
        atVar.f3422a.setOnClickListener(new am(this, simpleAppModel, sTInfoV2));
        a(atVar, simpleAppModel, i2, i3, sTInfoV2);
        if (z) {
            atVar.f3422a.setBackgroundResource(R.drawable.common_cardbg_bottom_selector);
            atVar.f.setVisibility(8);
        } else {
            atVar.f3422a.setBackgroundResource(R.drawable.common_cardbg_middle_selector);
            atVar.f.setVisibility(0);
        }
        return view;
    }

    private View a(View view, SimpleAppModel simpleAppModel, int i2, int i3, STInfoV2 sTInfoV2) {
        ar arVar;
        if (view == null) {
            arVar = new ar(this, null);
            view = this.g.inflate((int) R.layout.competitive_card, (ViewGroup) null);
            arVar.f3420a = (TextView) view.findViewById(R.id.title);
            arVar.b = (TXImageView) view.findViewById(R.id.pic);
            arVar.b.setInvalidater(this.o);
            arVar.c = (ImageView) view.findViewById(R.id.vedio);
            arVar.d = (AppIconView) view.findViewById(R.id.icon);
            arVar.d.setInvalidater(this.o);
            arVar.e = (DownloadButton) view.findViewById(R.id.download_soft_btn);
            arVar.f = (ListItemInfoView) view.findViewById(R.id.download_info);
            arVar.g = (TextView) view.findViewById(R.id.name);
            arVar.h = (TextView) view.findViewById(R.id.description);
            view.setTag(arVar);
        } else {
            arVar = (ar) view.getTag();
        }
        view.setTag(R.id.card_type, Integer.valueOf(i));
        view.setTag(R.id.tma_st_slot_tag, b(i2, i3));
        view.setOnClickListener(new an(this, simpleAppModel, sTInfoV2));
        a(arVar, simpleAppModel, i2, i3, sTInfoV2);
        return view;
    }

    public int getChildrenCount(int i2) {
        if (this.b == null || this.l == null || i2 >= this.l.size()) {
            return 0;
        }
        ArrayList arrayList = this.b.get(this.l.get(i2));
        if (arrayList != null) {
            return arrayList.size();
        }
        return 0;
    }

    public Object getGroup(int i2) {
        if (this.l == null || this.l.size() <= 0 || i2 >= this.l.size() || i2 < 0) {
            return null;
        }
        return this.l.get(i2);
    }

    public int getGroupCount() {
        if (this.b == null || this.b.size() <= 0) {
            return 0;
        }
        return this.b.size();
    }

    public long getGroupId(int i2) {
        return (long) i2;
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean isChildSelectable(int i2, int i3) {
        return false;
    }

    public void a(ListViewScrollListener listViewScrollListener) {
        this.o = listViewScrollListener;
    }

    private String b(int i2, int i3) {
        return com.tencent.assistantv2.st.page.a.a("07", i2, i3);
    }

    private void a(at atVar, SimpleAppModel simpleAppModel, int i2, int i3, STInfoV2 sTInfoV2) {
        if (simpleAppModel != null && atVar != null) {
            atVar.c.setText(simpleAppModel.d);
            com.tencent.assistant.adapter.a.a(this.e, simpleAppModel, atVar.c, false);
            atVar.b.updateImageView(simpleAppModel.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            atVar.d.a(simpleAppModel);
            atVar.d.setTag(R.id.group_position, Integer.valueOf(i2));
            atVar.e.a(ListItemInfoView.InfoType.DOWNTIMES_SIZE);
            atVar.e.a(simpleAppModel);
            atVar.d.a(sTInfoV2, new ao(this));
        }
    }

    private void a(ar arVar, SimpleAppModel simpleAppModel, int i2, int i3, STInfoV2 sTInfoV2) {
        if (simpleAppModel != null && arVar != null) {
            arVar.f3420a.setText(simpleAppModel.V);
            if (!TextUtils.isEmpty(simpleAppModel.Y)) {
                arVar.b.updateImageView(simpleAppModel.Y, -1, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            }
            if (simpleAppModel.Z == null || simpleAppModel.Z.length() == 0) {
                arVar.c.setVisibility(8);
            } else {
                arVar.c.setVisibility(0);
                arVar.b.setOnClickListener(new ap(this, simpleAppModel));
            }
            arVar.d.setSimpleAppModel(simpleAppModel, com.tencent.assistantv2.st.page.a.a(sTInfoV2), -100);
            arVar.d.setTag(simpleAppModel.q());
            arVar.e.a(simpleAppModel);
            arVar.e.setTag(R.id.group_position, Integer.valueOf(i2));
            arVar.f.a(simpleAppModel);
            arVar.g.setText(simpleAppModel.d);
            if (TextUtils.isEmpty(simpleAppModel.X)) {
                arVar.h.setVisibility(8);
            } else {
                arVar.h.setVisibility(0);
                arVar.h.setText(simpleAppModel.X);
            }
            arVar.e.a(sTInfoV2, new aq(this));
        }
    }

    public void handleUIEvent(Message message) {
        DownloadButton downloadButton = (DownloadButton) this.j.findViewById(R.id.state_app_btn);
        if (downloadButton != null) {
            int intValue = ((Integer) downloadButton.getTag(R.id.group_position)).intValue();
            if (this.l != null && intValue < this.l.size()) {
                ArrayList arrayList = this.b.get(this.l.get(intValue));
                switch (message.what) {
                    case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE:
                        DownloadInfo downloadInfo = null;
                        if (!(message.obj instanceof DownloadInfo) || ((downloadInfo = (DownloadInfo) message.obj) != null && !TextUtils.isEmpty(downloadInfo.downloadTicket))) {
                            Iterator it = arrayList.iterator();
                            while (it.hasNext()) {
                                SimpleAppModel simpleAppModel = (SimpleAppModel) it.next();
                                if (downloadInfo != null && simpleAppModel.q().equals(downloadInfo.downloadTicket)) {
                                    notifyDataSetChanged();
                                }
                            }
                            notifyDataSetChanged();
                            return;
                        }
                        return;
                    case 1016:
                        k.g(arrayList);
                        notifyDataSetChanged();
                        return;
                    default:
                        return;
                }
            }
        }
    }

    public int a(int i2, int i3) {
        SimpleAppModel.CARD_TYPE card_type;
        ArrayList arrayList;
        SimpleAppModel simpleAppModel;
        SimpleAppModel.CARD_TYPE card_type2 = SimpleAppModel.CARD_TYPE.NORMAL;
        if (this.b == null || this.l == null || i2 >= this.l.size() || (arrayList = this.b.get(this.l.get(i2))) == null || i3 >= arrayList.size() || (simpleAppModel = (SimpleAppModel) arrayList.get(i3)) == null) {
            card_type = card_type2;
        } else {
            card_type = simpleAppModel.U;
        }
        if (SimpleAppModel.CARD_TYPE.NORMAL == card_type) {
            return h;
        }
        if (SimpleAppModel.CARD_TYPE.QUALITY == card_type) {
            return i;
        }
        return h;
    }

    public void a() {
        this.f3414a.k().addUIEventListener(1002, this);
        this.f3414a.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING, this);
        this.f3414a.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_PAUSE, this);
        this.f3414a.k().addUIEventListener(1007, this);
        this.f3414a.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC, this);
        this.f3414a.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_QUEUING, this);
        this.f3414a.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        this.f3414a.k().addUIEventListener(1010, this);
        b(a.a().b().c());
    }

    private void a(boolean z) {
        if (this.d != null && this.d.size() != 0) {
            if (!z) {
                this.b = this.d;
                this.l = new ArrayList<>();
                for (AppGroupInfo add : this.b.keySet()) {
                    this.l.add(add);
                }
            } else {
                this.c = a((Map<AppGroupInfo, ArrayList<SimpleAppModel>>) this.c);
                this.b = this.c;
                this.l = new ArrayList<>();
                for (AppGroupInfo add2 : this.c.keySet()) {
                    this.l.add(add2);
                }
            }
            notifyDataSetChanged();
        }
    }

    private void b(boolean z) {
        a(this.d);
        a(z);
    }

    public void b() {
        this.k.clear();
        this.f3414a.k().removeUIEventListener(1002, this);
        this.f3414a.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING, this);
        this.f3414a.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_PAUSE, this);
        this.f3414a.k().removeUIEventListener(1007, this);
        this.f3414a.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC, this);
        this.f3414a.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_QUEUING, this);
        this.f3414a.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        this.f3414a.k().removeUIEventListener(1010, this);
    }
}
