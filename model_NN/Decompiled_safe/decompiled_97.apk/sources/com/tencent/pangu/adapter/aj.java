package com.tencent.pangu.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.MovingProgressBar;
import com.tencent.assistantv2.component.fps.FPSProgressBar;
import com.tencent.pangu.component.DownloadNumView;

/* compiled from: ProGuard */
public class aj extends ab {
    public View f;
    public RelativeLayout g;
    public TXImageView h;
    public DownloadButton i;
    public TextView j;
    public FPSProgressBar k;
    TextView l;
    TextView m;
    DownloadNumView n;
    public String o;
    public ImageView p;
    public MovingProgressBar q;
}
