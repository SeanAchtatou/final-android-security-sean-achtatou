package com.tencent.pangu.adapter;

import com.tencent.pangu.download.DownloadInfoWrapper;
import java.util.Comparator;

/* compiled from: ProGuard */
public class ag implements Comparator<DownloadInfoWrapper> {
    /* renamed from: a */
    public int compare(DownloadInfoWrapper downloadInfoWrapper, DownloadInfoWrapper downloadInfoWrapper2) {
        boolean c = downloadInfoWrapper.c();
        boolean c2 = downloadInfoWrapper2.c();
        if ((!c && !c2) || c == c2) {
            return (int) (downloadInfoWrapper2.b() - downloadInfoWrapper.b());
        }
        if (c) {
            return -1;
        }
        return 1;
    }
}
