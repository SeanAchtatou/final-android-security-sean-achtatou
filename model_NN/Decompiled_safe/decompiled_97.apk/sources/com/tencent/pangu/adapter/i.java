package com.tencent.pangu.adapter;

import com.tencent.assistant.AppConst;
import com.tencent.assistant.component.SecondNavigationTitleView;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.a;
import java.util.ArrayList;

/* compiled from: ProGuard */
class i extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ArrayList f3450a;
    final /* synthetic */ DownloadInfoMultiAdapter b;

    i(DownloadInfoMultiAdapter downloadInfoMultiAdapter, ArrayList arrayList) {
        this.b = downloadInfoMultiAdapter;
        this.f3450a = arrayList;
    }

    public void onLeftBtnClick() {
        STInfoV2 sTInfoV2 = new STInfoV2(STConst.ST_PAGE_DOWNLOAD, SecondNavigationTitleView.TMA_ST_NAVBAR_HOME_TAG, 0, STConst.ST_DEFAULT_SLOT, 200);
        sTInfoV2.status = "02";
        l.a(sTInfoV2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.download.a.a(java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>, boolean):void
     arg types: [java.util.ArrayList, int]
     candidates:
      com.tencent.pangu.download.a.a(com.tencent.pangu.download.a, java.util.List):java.util.List
      com.tencent.pangu.download.a.a(long, long):boolean
      com.tencent.pangu.download.a.a(com.tencent.pangu.download.a, boolean):boolean
      com.tencent.pangu.download.a.a(java.lang.String, int):boolean
      com.tencent.pangu.download.a.a(java.util.List<com.tencent.assistant.model.SimpleAppModel>, com.tencent.assistantv2.st.model.StatInfo):int
      com.tencent.pangu.download.a.a(com.tencent.assistant.model.SimpleAppModel, com.tencent.assistantv2.st.model.StatInfo):void
      com.tencent.pangu.download.a.a(com.tencent.pangu.download.DownloadInfo, com.tencent.pangu.download.SimpleDownloadInfo$UIType):void
      com.tencent.pangu.download.a.a(java.lang.String, com.tencent.cloud.a.f):void
      com.tencent.pangu.download.a.a(java.lang.String, java.lang.String):void
      com.tencent.pangu.download.a.a(com.tencent.pangu.download.DownloadInfo, boolean):boolean
      com.tencent.pangu.download.a.a(java.lang.String, boolean):boolean
      com.tencent.pangu.download.a.a(java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>, boolean):void */
    public void onRightBtnClick() {
        a.a((ArrayList<DownloadInfo>) this.f3450a, false);
        STInfoV2 sTInfoV2 = new STInfoV2(STConst.ST_PAGE_DOWNLOAD, SecondNavigationTitleView.TMA_ST_NAVBAR_HOME_TAG, 0, STConst.ST_DEFAULT_SLOT, 200);
        sTInfoV2.status = "01";
        l.a(sTInfoV2);
    }

    public void onCancell() {
        STInfoV2 sTInfoV2 = new STInfoV2(STConst.ST_PAGE_DOWNLOAD, SecondNavigationTitleView.TMA_ST_NAVBAR_HOME_TAG, 0, STConst.ST_DEFAULT_SLOT, 200);
        sTInfoV2.status = "02";
        l.a(sTInfoV2);
    }
}
