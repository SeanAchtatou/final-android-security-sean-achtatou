package com.tencent.pangu.model;

import android.text.TextUtils;
import com.tencent.assistant.utils.FileUtil;

/* compiled from: ProGuard */
public class d extends AbstractDownloadInfo {
    public String w;

    public static final d a(String str, String str2, String str3) {
        d dVar = new d();
        if (TextUtils.isEmpty(str)) {
            dVar.m = System.currentTimeMillis() + "_" + str2;
        } else {
            dVar.m = a(str, str2);
        }
        dVar.l = str3;
        dVar.o = System.currentTimeMillis();
        return dVar;
    }

    public static String a(String str, String str2) {
        return str + "_" + str2;
    }

    public int a() {
        return 100;
    }

    public String b() {
        return FileUtil.getDynamicFileDir();
    }

    public String g() {
        if (TextUtils.isEmpty(this.k)) {
            return "downloadfile_" + this.o;
        }
        return this.k;
    }

    public String toString() {
        return "FileDownInfo{, downUrl='" + this.l + '\'' + ", downId='" + this.m + '\'' + ", fileSize=" + this.n + ", createTime=" + this.o + ", finishTime=" + this.p + ", downloadingPath='" + this.q + '\'' + ", savePath='" + this.r + '\'' + ", downState=" + this.s + ", errorCode=" + this.t + ", DownResponse=" + this.u + '}';
    }
}
