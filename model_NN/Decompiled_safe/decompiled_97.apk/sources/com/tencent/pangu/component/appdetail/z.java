package com.tencent.pangu.component.appdetail;

import com.tencent.pangu.adapter.c;

/* compiled from: ProGuard */
class z implements bd {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppdetailViewPager f3626a;

    z(AppdetailViewPager appdetailViewPager) {
        this.f3626a = appdetailViewPager;
    }

    public boolean a() {
        bc bcVar = (bc) ((c) this.f3626a.getAdapter()).a(this.f3626a.getCurrentItem());
        if (bcVar == null) {
            return false;
        }
        bd c = bcVar.c();
        if (c != null) {
            return c.a();
        }
        return false;
    }

    public void a(int i) {
        bd c;
        bc bcVar = (bc) ((c) this.f3626a.getAdapter()).a(this.f3626a.getCurrentItem());
        if (bcVar != null && (c = bcVar.c()) != null) {
            c.a(i);
        }
    }

    public void fling(int i) {
        bd c;
        bc bcVar = (bc) ((c) this.f3626a.getAdapter()).a(this.f3626a.getCurrentItem());
        if (bcVar != null && (c = bcVar.c()) != null) {
            c.fling(i);
        }
    }

    public void a(boolean z) {
        bd c;
        bc bcVar = (bc) ((c) this.f3626a.getAdapter()).a(this.f3626a.getCurrentItem());
        if (bcVar != null && (c = bcVar.c()) != null) {
            c.a(z);
        }
    }
}
