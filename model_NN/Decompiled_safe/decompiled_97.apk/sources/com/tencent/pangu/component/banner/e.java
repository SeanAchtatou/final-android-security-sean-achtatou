package com.tencent.pangu.component.banner;

import android.text.TextUtils;
import com.tencent.assistant.protocol.jce.Banner;
import com.tencent.assistant.protocol.jce.ColorCardItem;
import com.tencent.assistantv2.component.banner.floatheader.a;
import com.tencent.smtt.sdk.WebView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class e {
    public static List<f> a(List<ColorCardItem> list, int i, int i2) {
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (ColorCardItem next : list) {
            if (next.e == i) {
                Banner banner = new Banner();
                banner.c = next.f1199a;
                banner.b = next.c;
                banner.e = next.b;
                if (!TextUtils.isEmpty(next.b())) {
                    banner.d = next.b();
                }
                banner.f1175a = 1;
                a aVar = new a(banner);
                if (next.getTag() != null) {
                    aVar.f3641a.setTag(next.getTag());
                }
                aVar.a((int) WebView.NIGHT_MODE_COLOR);
                arrayList.add(aVar);
                if (arrayList.size() > 0 && i2 > 0 && arrayList.size() >= i2) {
                    return arrayList;
                }
            }
        }
        return arrayList;
    }

    public static List<f> b(List<ColorCardItem> list, int i, int i2) {
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (ColorCardItem colorCardItem : new ArrayList(list)) {
            if (colorCardItem.e == i && ("分类".equals(colorCardItem.c) || "排行".equals(colorCardItem.c))) {
                Banner banner = new Banner();
                banner.c = colorCardItem.f1199a;
                banner.b = colorCardItem.c;
                banner.e = colorCardItem.b;
                if (!TextUtils.isEmpty(colorCardItem.b())) {
                    banner.d = colorCardItem.b();
                }
                banner.f1175a = 1;
                a aVar = new a(banner);
                aVar.a((int) WebView.NIGHT_MODE_COLOR);
                arrayList.add(aVar);
                if (arrayList.size() > 0 && i2 > 0 && arrayList.size() >= i2) {
                    return arrayList;
                }
            }
        }
        return arrayList;
    }

    public static boolean a(List<f> list) {
        int i;
        if (list == null || list.size() < 2) {
            return false;
        }
        Iterator<f> it = list.iterator();
        int i2 = 0;
        while (true) {
            if (!it.hasNext()) {
                i = i2;
                break;
            }
            f next = it.next();
            if (next == null || next.e() == null || (!"分类".equals(next.e().b) && !"排行".equals(next.e().b))) {
                i = i2;
            } else {
                i = i2 + 1;
            }
            if (i >= 2) {
                break;
            }
            i2 = i;
        }
        if (i >= 2) {
            return true;
        }
        return false;
    }
}
