package com.tencent.pangu.component;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.adapter.StartPopWindowGridViewAdapterV2;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class PopUpContentView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private View f3495a;
    private GridView b;
    /* access modifiers changed from: private */
    public StartPopWindowGridViewAdapterV2 c;
    /* access modifiers changed from: private */
    public ArrayList<SimpleAppModel> d = new ArrayList<>();
    /* access modifiers changed from: private */
    public x e;
    private ImageView f;
    private TXImageView g;
    private TextView h;
    private int i;
    /* access modifiers changed from: private */
    public Context j;

    public PopUpContentView(Context context, x xVar, int i2) {
        super(context);
        this.j = context;
        if (xVar != null) {
            this.e = xVar;
        }
        this.i = i2;
        f();
    }

    private void f() {
        this.f3495a = LayoutInflater.from(this.j).inflate((int) R.layout.start_pop_new_window_content_view, this);
        this.g = (TXImageView) this.f3495a.findViewById(R.id.header_view);
        this.h = (TextView) this.f3495a.findViewById(R.id.title);
        this.b = (GridView) this.f3495a.findViewById(R.id.grid_view);
        this.b.setSelector(new ColorDrawable(0));
        this.c = new StartPopWindowGridViewAdapterV2(this.j, this.i);
        this.f = (ImageView) this.f3495a.findViewById(R.id.cancel_btn);
        this.b.setAdapter((ListAdapter) this.c);
        this.f.bringToFront();
        this.f.setOnClickListener(new u(this));
    }

    public void a(String str, String str2, boolean z, ArrayList<SimpleAppModel> arrayList) {
        if (!TextUtils.isEmpty(str2)) {
            this.h.setText(str2);
        }
        if (str != null) {
            this.g.updateImageView(str, R.drawable.banner_test, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
        }
        this.d = arrayList;
        int i2 = 3;
        if (this.d.size() > 6) {
            this.c.a(0);
        } else {
            this.c.a(1);
            i2 = 2;
        }
        this.b.setNumColumns(i2);
        if (this.d != null) {
            this.c.a(this.d, this.d.size(), z);
        }
        this.b.setAdapter((ListAdapter) this.c);
        this.b.setOnItemClickListener(new v(this));
    }

    public int a() {
        return this.c.b();
    }

    public long b() {
        return this.c.c();
    }

    public ArrayList<w> c() {
        ArrayList<w> arrayList = new ArrayList<>();
        ArrayList<Boolean> d2 = this.c.d();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= d2.size()) {
                return arrayList;
            }
            if (d2.get(i3).booleanValue()) {
                w wVar = new w(this);
                wVar.c = g();
                wVar.f3744a = this.d.get(i3);
                wVar.b = i3;
                arrayList.add(wVar);
            }
            i2 = i3 + 1;
        }
    }

    public void d() {
        XLog.i("PopWindowReprot", "pageid:205041" + this.i + " position:" + this.i);
        STInfoV2 sTInfoV2 = new STInfoV2(g(), "01", 2000, STConst.ST_DEFAULT_SLOT, 100);
        sTInfoV2.status = this.c.a() == 0 ? "01" : "02";
        l.a(sTInfoV2);
    }

    public void e() {
        this.c.e();
    }

    private int g() {
        return STConst.ST_PAGE_POP_UP_NECESSRAY_CONTENT_VEIW + this.i;
    }
}
