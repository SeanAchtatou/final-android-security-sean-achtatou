package com.tencent.pangu.component.topbanner;

import com.tencent.assistant.db.table.ag;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.pangu.model.g;

/* compiled from: ProGuard */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private static b f3718a;
    /* access modifiers changed from: private */
    public ag b;
    /* access modifiers changed from: private */
    public g c;

    public static synchronized b a() {
        b bVar;
        synchronized (b.class) {
            if (f3718a == null) {
                f3718a = new b();
            }
            bVar = f3718a;
        }
        return bVar;
    }

    public b() {
        this.b = null;
        this.b = new ag();
    }

    public void a(g gVar) {
        TemporaryThreadManager.get().start(new c(this, gVar));
    }

    public void b(g gVar) {
        TemporaryThreadManager.get().start(new d(this, gVar));
    }

    public void c(g gVar) {
        a(gVar);
    }

    public boolean d(g gVar) {
        this.c = gVar;
        if (!a(gVar.f3888a) || !b(gVar.f3888a)) {
            return false;
        }
        return true;
    }

    private boolean a(int i) {
        long currentTimeMillis = System.currentTimeMillis();
        if (this.c == null || this.c.f3888a != i || currentTimeMillis >= this.c.c * 1000) {
            return false;
        }
        return true;
    }

    private boolean b(int i) {
        if (this.c == null || this.c.f3888a != i || this.c.k >= this.c.j) {
            return false;
        }
        return true;
    }

    public int e(g gVar) {
        if (this.c == null || this.c.f3888a != gVar.f3888a) {
            this.c = this.b.b(gVar.f3888a);
        }
        if (this.c.m == 0 || this.c.m == gVar.m) {
            return this.c.k;
        }
        TemporaryThreadManager.get().startDelayed(new e(this), 1500);
        return 0;
    }

    public void f(g gVar) {
        gVar.k = 0;
        TemporaryThreadManager.get().start(new f(this, gVar));
    }

    public void g(g gVar) {
        b(gVar);
        this.c = null;
    }
}
