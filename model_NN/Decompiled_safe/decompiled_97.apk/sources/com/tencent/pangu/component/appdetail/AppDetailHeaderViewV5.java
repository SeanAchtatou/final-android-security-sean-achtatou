package com.tencent.pangu.component.appdetail;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.adapter.a;
import com.tencent.assistant.component.AppIconView;
import com.tencent.assistant.component.RatingView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.utils.bm;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
public class AppDetailHeaderViewV5 extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private Context f3526a;
    private LayoutInflater b;
    private AstApp c = AstApp.i();
    private AppIconView d;
    private TextView e;
    private TextView f;
    private TextView g;
    private TextView h;
    private RatingView i;
    private TextView j;
    private AppdetailFlagView k;
    private SimpleAppModel l;
    private STInfoV2 m = null;

    public AppDetailHeaderViewV5(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3526a = context;
        this.b = LayoutInflater.from(context);
        c();
    }

    public AppDetailHeaderViewV5(Context context) {
        super(context);
        this.f3526a = context;
        this.b = LayoutInflater.from(context);
        c();
    }

    private void c() {
        View inflate = this.b.inflate((int) R.layout.appdetail_floating_layout_v5, this);
        this.d = (AppIconView) inflate.findViewById(R.id.softapp_icon_img);
        this.d.getIconImageView().setAdjustViewBounds(true);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.d.getIconImageView().getLayoutParams();
        layoutParams.width = -1;
        layoutParams.height = -1;
        this.d.getIconImageView().setLayoutParams(layoutParams);
        this.e = (TextView) inflate.findViewById(R.id.app_name);
        this.i = (RatingView) inflate.findViewById(R.id.comment_score);
        this.f = (TextView) inflate.findViewById(R.id.download_times_txt);
        this.g = (TextView) inflate.findViewById(R.id.version_num);
        this.h = (TextView) inflate.findViewById(R.id.developer_name);
        this.j = (TextView) inflate.findViewById(R.id.friends_num);
        this.d.setIconClickListener(new e(this));
        this.k = (AppdetailFlagView) inflate.findViewById(R.id.flag_view);
    }

    private void a(String str, String str2, String str3) {
        if (this.l != null) {
            this.d.setSimpleAppModel(this.l, this.m, -100);
            this.e.setText(this.l.d);
            a.a(this.f3526a, this.l, this.e, true);
            this.i.setRating(this.l.q);
            this.f.setText(bm.a(this.l.p, 0));
            this.k.a(this.l);
            this.g.setText(String.format(getResources().getString(R.string.version_num), str));
            this.h.setText(str2);
        }
    }

    private boolean a(SimpleAppModel simpleAppModel) {
        if (simpleAppModel != null && !TextUtils.isEmpty(simpleAppModel.d) && !TextUtils.isEmpty(simpleAppModel.i) && simpleAppModel.p >= 0) {
            return true;
        }
        return false;
    }

    public void a(SimpleAppModel simpleAppModel, STInfoV2 sTInfoV2, String str, String str2, String str3) {
        if (a(simpleAppModel)) {
            this.l = simpleAppModel;
            this.m = sTInfoV2;
            a(str, str2, str3);
        }
    }

    public void a() {
        if (this.l != null) {
            this.d.setSimpleAppModel(this.l, this.m, -100);
        }
    }

    public void b() {
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            return true;
        }
        return super.onTouchEvent(motionEvent);
    }

    public void a(String str) {
        this.j.setText(str);
    }
}
