package com.tencent.pangu.component;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;
import com.tencent.assistant.utils.bm;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.utils.a;

@SuppressLint({"NewApi"})
/* compiled from: ProGuard */
public class DownloadNumView extends TextView {

    /* renamed from: a  reason: collision with root package name */
    private float f3491a;
    private a b;
    private String c = Constants.STR_EMPTY;
    private String d = Constants.STR_EMPTY;

    public float getNumber() {
        return this.f3491a;
    }

    private void a() {
        if (this.b == null) {
            this.b = new a(this, "number");
        }
    }

    public DownloadNumView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    public DownloadNumView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a();
    }

    public DownloadNumView(Context context) {
        super(context);
        a();
    }

    private void a(float f) {
        if (this.b != null) {
            this.b.a((f / 1024.0f) / 1024.0f);
        }
    }

    public void showWithAnimation(float f, float f2, String str, String str2) {
        float f3 = (f / 1024.0f) / 1024.0f;
        float f4 = (f2 / 1024.0f) / 1024.0f;
        this.c = str;
        this.d = str2;
        if (this.b != null) {
            this.b.a(f3, f4);
        } else {
            a();
        }
    }

    public void setNumber(float f) {
        String str = this.c + bm.a((double) f, 2) + this.d;
        if (!TextUtils.isEmpty(str)) {
            try {
                setText(str);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
    }

    public void cancelAnimation() {
        if (this.b != null) {
            this.b.a();
        }
    }

    public void mySetText(float f, String str) {
        cancelAnimation();
        a(f);
        super.setText(str);
    }
}
