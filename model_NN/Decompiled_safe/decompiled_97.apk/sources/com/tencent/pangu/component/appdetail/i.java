package com.tencent.pangu.component.appdetail;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.manager.t;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.pangu.activity.AppDetailActivityV5;

/* compiled from: ProGuard */
class i extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppDetailViewV5 f3596a;

    i(AppDetailViewV5 appDetailViewV5) {
        this.f3596a = appDetailViewV5;
    }

    public void onTMAClick(View view) {
        try {
            if (this.f3596a.c.a()) {
                this.f3596a.c.a(false);
                this.f3596a.d.setImageResource(R.drawable.icon_close);
                return;
            }
            this.f3596a.c.a(true);
            this.f3596a.d.setImageResource(R.drawable.icon_open);
        } catch (OutOfMemoryError e) {
            t.a().b();
        } catch (Throwable th) {
        }
    }

    public STInfoV2 getStInfo() {
        if (!(this.f3596a.j instanceof AppDetailActivityV5)) {
            return null;
        }
        STInfoV2 t = ((AppDetailActivityV5) this.f3596a.j).t();
        t.slotId = a.a("07", "009");
        if (this.f3596a.c.a()) {
            t.status = "01";
        } else {
            t.status = "02";
        }
        t.actionId = 200;
        return t;
    }
}
