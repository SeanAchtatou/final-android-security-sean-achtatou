package com.tencent.pangu.component;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Html;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.m;
import com.tencent.assistant.utils.FunctionUtils;
import com.tencent.assistant.utils.at;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;
import com.tencent.pangu.manager.SelfUpdateManager;

/* compiled from: ProGuard */
public class SelfForceUpdateView extends RelativeLayout implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    public AstApp f3501a;
    public SelfUpdateManager.SelfUpdateInfo b;
    public DownloadInfo c;
    private View d;
    private TextView e;
    private Button f;
    private Button g;
    private View h;
    private TextView i;
    private TextView j;
    private ProgressBar k;
    private bk l;
    private TextView m;
    private TextView n;
    private TXImageView o;

    public SelfForceUpdateView(Context context) {
        this(context, null);
    }

    public SelfForceUpdateView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        e();
    }

    private void e() {
        this.f3501a = AstApp.i();
        f();
    }

    private void f() {
        this.d = LayoutInflater.from(getContext()).inflate((int) R.layout.dialog_selfupdate_force, this);
        this.o = (TXImageView) this.d.findViewById(R.id.self_update_banner);
        this.e = (TextView) this.d.findViewById(R.id.msg_versionfeature);
        this.f = (Button) this.d.findViewById(R.id.btn_ignore);
        this.g = (Button) this.d.findViewById(R.id.btn_update);
        this.m = (TextView) this.d.findViewById(R.id.title_tip_1);
        this.n = (TextView) this.d.findViewById(R.id.title_tip_2);
        this.h = this.d.findViewById(R.id.downloadlayout);
        this.i = (TextView) this.d.findViewById(R.id.update_download_txt_speed);
        this.j = (TextView) this.d.findViewById(R.id.update_download_txt_per);
        this.k = (ProgressBar) this.d.findViewById(R.id.update_download_progressbar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    public void a() {
        this.b = SelfUpdateManager.a().d();
        if (this.b != null) {
            if (!TextUtils.isEmpty(this.b.h)) {
                this.e.setText(String.format(getResources().getString(R.string.dialog_update_feature), this.b.h));
            }
            g();
            if (!TextUtils.isEmpty(this.b.q)) {
                this.n.setText(Html.fromHtml(this.b.q));
            } else {
                this.n.setText(Constants.STR_EMPTY);
            }
            this.o.setImageBitmap(FunctionUtils.a(BitmapFactory.decodeResource(this.f3501a.getResources(), R.drawable.self_update_banner)));
            this.f.setOnClickListener(new bb(this));
            this.g.setOnClickListener(new bc(this));
            if (m.a().a("update_force_downloading", false)) {
                this.h.setVisibility(0);
                new Handler(Looper.getMainLooper()).postDelayed(new bd(this), 300);
                return;
            }
            this.h.setVisibility(8);
            return;
        }
        this.l.u();
    }

    private void g() {
        if (SelfUpdateManager.a().a(this.b.e)) {
            this.m.setVisibility(0);
            this.m.setText((int) R.string.selfupdate_apk_file_ready);
            this.g.setText((int) R.string.selfupdate_update_right_now);
            this.g.setTextColor(getResources().getColor(R.color.appdetail_btn_text_color_to_install));
            this.g.setBackgroundResource(R.drawable.appdetail_bar_btn_to_install_selector_v5);
            return;
        }
        this.m.setVisibility(8);
        this.g.setText(Constants.STR_EMPTY);
        this.g.append(new SpannableString(getResources().getString(R.string.download)));
        SpannableString spannableString = new SpannableString(" (" + at.b(this.b.a()) + ")");
        spannableString.setSpan(new AbsoluteSizeSpan(15, true), 0, spannableString.length(), 17);
        this.g.append(spannableString);
        this.g.setBackgroundResource(R.drawable.selfupdate_btn_blue_selector_v5);
    }

    public void a(bk bkVar) {
        this.l = bkVar;
    }

    public void b() {
        h();
        g();
        this.c = SelfUpdateManager.a().i();
        if (this.c != null && !SelfUpdateManager.a().a(this.b.e)) {
            this.k.setProgress(SimpleDownloadInfo.getPercent(DownloadProxy.a().c(this.c.downloadTicket)));
            this.i.setText(this.c.response.c);
            this.j.setText(this.c.response.f3766a + "/" + this.c.response.b);
        } else if (this.c != null && SelfUpdateManager.a().a(this.b.e)) {
            this.k.setProgress(100);
            this.i.setText(Constants.STR_EMPTY);
            this.j.setText(this.c.response.f3766a + "/" + this.c.response.b);
        }
    }

    public void c() {
        i();
    }

    private void h() {
        this.f3501a.k().addUIEventListener(1010, this);
        this.f3501a.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING_START, this);
        this.f3501a.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING, this);
        this.f3501a.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_QUEUING, this);
        this.f3501a.k().addUIEventListener(1007, this);
        this.f3501a.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_PAUSE, this);
        this.f3501a.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC, this);
    }

    private void i() {
        this.f3501a.k().removeUIEventListener(1010, this);
        this.f3501a.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING_START, this);
        this.f3501a.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING, this);
        this.f3501a.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_QUEUING, this);
        this.f3501a.k().removeUIEventListener(1007, this);
        this.f3501a.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_PAUSE, this);
        this.f3501a.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC, this);
    }

    /* access modifiers changed from: protected */
    public void d() {
        j();
        k();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
     arg types: [java.lang.String, boolean]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(int, byte[]):void
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean */
    private void j() {
        this.h.setVisibility(0);
        m.a().b("update_force_downloading", (Object) true);
    }

    /* access modifiers changed from: private */
    public void k() {
        SelfUpdateManager.a().a(SelfUpdateManager.SelfUpdateType.FORCE);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
     arg types: [java.lang.String, boolean]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(int, byte[]):void
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean */
    public void handleUIEvent(Message message) {
        if (this.b != null) {
            this.c = SelfUpdateManager.a().i();
            if (this.c != null) {
                this.c = DownloadProxy.a().d(this.c.downloadTicket);
            }
            if (this.c != null && this.c.packageName.equals(AstApp.i().getPackageName())) {
                switch (message.what) {
                    case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING:
                    case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING_START:
                        this.k.setSecondaryProgress(0);
                        this.k.setProgress(SimpleDownloadInfo.getPercent(DownloadProxy.a().c(this.c.downloadTicket)));
                        String str = this.c.response.c;
                        this.i.setText(String.format(getContext().getResources().getString(R.string.down_page_downloading), str));
                        this.j.setText(at.a(this.c.response.f3766a) + "/" + at.a(this.c.response.b));
                        this.g.setEnabled(false);
                        this.g.setTextColor(getResources().getColor(R.color.appadmin_disabled_text));
                        this.g.setBackgroundResource(R.drawable.common_btn_big_disabled);
                        this.g.setText((int) R.string.plugin_down_dialog_downloading);
                        return;
                    case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_PAUSE:
                        this.k.setSecondaryProgress(0);
                        if (this.c != null) {
                            this.k.setProgress(SimpleDownloadInfo.getPercent(DownloadProxy.a().c(this.c.downloadTicket)));
                        }
                        this.i.setText(String.format(getContext().getResources().getString(R.string.down_page_pause), new Object[0]));
                        this.j.setText(at.a(this.c.response.f3766a) + "/" + at.a(this.c.response.b));
                        this.g.setEnabled(true);
                        this.g.setText((int) R.string.continuing);
                        this.g.setBackgroundResource(R.drawable.selfupdate_btn_blue_selector_v5);
                        this.g.setOnClickListener(new bg(this));
                        return;
                    case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC:
                        this.k.setSecondaryProgress(0);
                        this.h.setVisibility(8);
                        this.g.setEnabled(true);
                        g();
                        this.g.setOnClickListener(new bh(this));
                        return;
                    case 1007:
                        m.a().b("update_force_downloading", (Object) true);
                        this.k.setProgress(0);
                        this.k.setSecondaryProgress(100);
                        this.g.setEnabled(true);
                        g();
                        this.g.setOnClickListener(new bf(this));
                        return;
                    case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_QUEUING:
                    case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE:
                    default:
                        return;
                    case 1010:
                        this.k.setSecondaryProgress(0);
                        this.k.setProgress(100);
                        this.h.setOnClickListener(new be(this));
                        if (!SelfUpdateManager.a().b()) {
                            SelfUpdateManager.a().c();
                            return;
                        }
                        return;
                }
            }
        }
    }
}
