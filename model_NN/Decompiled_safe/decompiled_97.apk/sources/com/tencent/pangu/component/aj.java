package com.tencent.pangu.component;

import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.a;
import com.tencent.assistant.protocol.jce.TagGroup;
import java.util.HashMap;
import java.util.List;

/* compiled from: ProGuard */
class aj implements a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RankFriendsListView f3518a;

    aj(RankFriendsListView rankFriendsListView) {
        this.f3518a = rankFriendsListView;
    }

    public void a(int i, int i2, boolean z, List<SimpleAppModel> list, List<TagGroup> list2) {
        if (this.f3518a.x != null) {
            ViewInvalidateMessage viewInvalidateMessage = new ViewInvalidateMessage(1, null, this.f3518a.M);
            viewInvalidateMessage.arg1 = i2;
            viewInvalidateMessage.arg2 = i;
            HashMap hashMap = new HashMap();
            hashMap.put("isFirstPage", Boolean.valueOf(z));
            hashMap.put("key_data", list);
            viewInvalidateMessage.params = hashMap;
            this.f3518a.x.sendMessage(viewInvalidateMessage);
        }
    }
}
