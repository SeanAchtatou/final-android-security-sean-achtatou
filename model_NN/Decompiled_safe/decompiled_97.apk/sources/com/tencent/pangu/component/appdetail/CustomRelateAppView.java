package com.tencent.pangu.component.appdetail;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.RatingView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.bm;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.activity.AppDetailActivityV5;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class CustomRelateAppView extends LinearLayout implements ak {

    /* renamed from: a  reason: collision with root package name */
    public View f3540a;
    public AstApp b;
    public int c = 0;
    public String d = Constants.STR_EMPTY;
    /* access modifiers changed from: private */
    public Context e;
    private LinearLayout[] f = new LinearLayout[3];
    private TXImageView[] g = new TXImageView[3];
    private TextView[] h = new TextView[3];
    private RatingView[] i = new RatingView[3];
    /* access modifiers changed from: private */
    public List<SimpleAppModel> j = new ArrayList();
    private boolean k = false;
    private View.OnClickListener l = new ae(this);

    public CustomRelateAppView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.e = context;
        b();
    }

    public CustomRelateAppView(Context context) {
        super(context);
        this.e = context;
        b();
    }

    private void b() {
        this.b = AstApp.i();
        View inflate = LayoutInflater.from(this.e).inflate((int) R.layout.appdetail_custom_relate_layout, this);
        this.f3540a = this;
        this.f[0] = (LinearLayout) inflate.findViewById(R.id.ralate_app_layout1);
        this.f[1] = (LinearLayout) inflate.findViewById(R.id.ralate_app_layout2);
        this.f[2] = (LinearLayout) inflate.findViewById(R.id.ralate_app_layout3);
        for (LinearLayout onClickListener : this.f) {
            onClickListener.setOnClickListener(this.l);
        }
        this.g[0] = (TXImageView) inflate.findViewById(R.id.soft_icon_img1);
        this.g[1] = (TXImageView) inflate.findViewById(R.id.soft_icon_img2);
        this.g[2] = (TXImageView) inflate.findViewById(R.id.soft_icon_img3);
        this.h[0] = (TextView) inflate.findViewById(R.id.soft_name_txt1);
        this.h[1] = (TextView) inflate.findViewById(R.id.soft_name_txt2);
        this.h[2] = (TextView) inflate.findViewById(R.id.soft_name_txt3);
        this.i[0] = (RatingView) inflate.findViewById(R.id.app_ratingview1);
        this.i[1] = (RatingView) inflate.findViewById(R.id.app_ratingview2);
        this.i[2] = (RatingView) inflate.findViewById(R.id.app_ratingview3);
    }

    /* access modifiers changed from: private */
    public int a(int i2) {
        switch (i2) {
            case R.id.ralate_app_layout1 /*2131165636*/:
            default:
                return 0;
            case R.id.ralate_app_layout2 /*2131165640*/:
                return 1;
            case R.id.ralate_app_layout3 /*2131165644*/:
                return 2;
        }
    }

    /* access modifiers changed from: private */
    public String a(int i2, int i3) {
        int i4 = (i2 * 3) + i3 + 1;
        return this.d + "_" + bm.a(i4) + "|" + (i4 % 4);
    }

    /* access modifiers changed from: private */
    public void a(SimpleAppModel simpleAppModel) {
        if (simpleAppModel != null && !this.k) {
            this.k = true;
            Intent intent = new Intent(this.e, AppDetailActivityV5.class);
            int i2 = STConst.ST_PAGE_APP_DETAIL;
            if (this.e instanceof BaseActivity) {
                i2 = ((BaseActivity) this.e).f();
                intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, i2);
            }
            intent.putExtra("simpleModeInfo", simpleAppModel);
            intent.putExtra("statInfo", new StatInfo(simpleAppModel.b, i2, 0, null, 0));
            this.e.startActivity(intent);
            this.k = false;
        }
    }

    public void a() {
        int i2;
        int i3 = 2000;
        if (this.e instanceof BaseActivity) {
            BaseActivity baseActivity = (BaseActivity) this.e;
            i2 = baseActivity.f();
            i3 = baseActivity.m();
        } else {
            i2 = 2000;
        }
        l.a(new STInfoV2(i2, this.d, i3, STConst.ST_DEFAULT_SLOT, 100));
    }
}
