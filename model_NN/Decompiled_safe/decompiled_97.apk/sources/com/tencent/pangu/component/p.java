package com.tencent.pangu.component;

import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.page.a;
import com.tencent.pangu.link.b;
import com.tencent.pangu.mediadownload.o;

/* compiled from: ProGuard */
class p extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ o f3686a;
    final /* synthetic */ FileDownloadButton b;

    p(FileDownloadButton fileDownloadButton, o oVar) {
        this.b = fileDownloadButton;
        this.f3686a = oVar;
    }

    public void onLeftBtnClick() {
        this.b.a(STConst.ST_PAGE_DOWNLOAD_FILE_CANT_OPEN, AstApp.m() != null ? AstApp.m().f() : 2000, a.a("03", 1), 200);
    }

    public void onRightBtnClick() {
        b.a(this.b.getContext(), "tmast://search?selflink=true&key=" + this.f3686a.b);
        this.b.a(STConst.ST_PAGE_DOWNLOAD_FILE_CANT_OPEN, AstApp.m() != null ? AstApp.m().f() : 2000, a.a("03", 0), 200);
    }

    public void onCancell() {
    }
}
