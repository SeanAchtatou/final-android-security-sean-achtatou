package com.tencent.pangu.component.appdetail;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.pangu.activity.AppDetailActivityV5;

/* compiled from: ProGuard */
public class DetailGameNewsView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    protected LayoutInflater f3543a;
    /* access modifiers changed from: private */
    public Context b;
    private TextView c;
    private View d;
    private View e;
    private LinearLayout f;

    public DetailGameNewsView(Context context) {
        super(context);
        a(context);
    }

    public DetailGameNewsView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    private void a(Context context) {
        this.b = context;
        this.f3543a = (LayoutInflater) context.getSystemService("layout_inflater");
        View inflate = this.f3543a.inflate((int) R.layout.app_detail_relate_news_view, this);
        this.d = inflate.findViewById(R.id.header_layout);
        this.c = (TextView) inflate.findViewById(R.id.title);
        this.e = inflate.findViewById(R.id.tips_btn);
        this.f = (LinearLayout) inflate.findViewById(R.id.content_layout);
    }

    public void a(ao aoVar) {
        if (aoVar == null || aoVar.c == null || aoVar.c.length <= 0) {
            setVisibility(8);
            return;
        }
        if (!TextUtils.isEmpty(aoVar.f3573a)) {
            this.c.setText(aoVar.f3573a);
        }
        if (aoVar.d) {
            this.e.setVisibility(0);
        } else {
            this.e.setVisibility(8);
        }
        this.f.removeAllViews();
        for (int i = 0; i < aoVar.c.length; i++) {
            ap apVar = aoVar.c[i];
            if (i >= aoVar.g || apVar == null) {
                break;
            }
            View a2 = a(apVar, aoVar.e, i);
            if (a2 != null) {
                this.f.addView(a2, new LinearLayout.LayoutParams(-1, by.b(95.0f)));
            }
        }
        this.d.setOnClickListener(new al(this, aoVar));
    }

    private View a(ap apVar, int i, int i2) {
        if (apVar == null) {
            return null;
        }
        View inflate = this.f3543a.inflate((int) R.layout.app_detail_relate_news_item_view, (ViewGroup) null);
        TextView textView = (TextView) inflate.findViewById(R.id.news_content);
        ImageView imageView = (ImageView) inflate.findViewById(R.id.play_video);
        ImageView imageView2 = (ImageView) inflate.findViewById(R.id.news_divider);
        ((TXImageView) inflate.findViewById(R.id.news_icon)).updateImageView(apVar.f3574a, R.drawable.app_treasure_box_above_icon, TXImageView.TXImageViewType.NETWORK_IMAGE_MIDDLE);
        ((TextView) inflate.findViewById(R.id.news_title)).setText(apVar.b);
        if (apVar.g >= 5) {
            textView.setText(apVar.g + "人已赞");
            textView.setVisibility(0);
        } else {
            textView.setVisibility(8);
        }
        if (apVar.d) {
            imageView.setVisibility(0);
            imageView.setOnClickListener(new am(this, i2, apVar));
        } else {
            imageView.setVisibility(8);
        }
        inflate.setOnClickListener(new an(this, apVar, i, i2));
        if (this.b instanceof AppDetailActivityV5) {
            STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this.b, 100);
            buildSTInfo.slotId = a.a(i == 1 ? "17" : "09", i2 + 1);
            l.a(buildSTInfo);
        }
        return inflate;
    }
}
