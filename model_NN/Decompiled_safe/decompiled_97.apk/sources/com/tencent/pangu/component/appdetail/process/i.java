package com.tencent.pangu.component.appdetail.process;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.a.a;
import com.tencent.assistant.component.dialog.DialogUtils;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.manager.b;
import com.tencent.assistant.manager.n;
import com.tencent.assistant.manager.u;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.l;
import com.tencent.assistant.module.k;
import com.tencent.assistant.net.c;
import com.tencent.assistant.protocol.jce.VerifyInfo;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.at;
import com.tencent.assistant.utils.e;
import com.tencent.assistant.utils.r;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.connect.common.Constants;
import com.tencent.nucleus.socialcontact.comment.PopViewDialog;
import com.tencent.nucleus.socialcontact.login.j;
import com.tencent.pangu.activity.AppDetailActivityV5;
import com.tencent.pangu.component.appdetail.process.AppdetailActionUIListener;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;
import com.tencent.pangu.manager.ak;

/* compiled from: ProGuard */
public class i extends e implements UIEventListener, b {
    private Bundle A;
    private String B;
    private String C;
    private boolean D = false;
    private boolean E = false;
    private boolean F = false;
    private boolean G = false;
    private Context n = null;
    private AstApp o;
    private SimpleAppModel p = null;
    private long q;
    private PopViewDialog r = null;
    private byte s;
    private int t = 0;
    private int u = 0;
    private int v = 0;
    private int w = 1;
    private n x = null;
    private Dialog y = null;
    private int z = -1;

    public i(a aVar) {
        super(aVar);
    }

    public void a(SimpleAppModel simpleAppModel, long j, boolean z2, PopViewDialog popViewDialog, StatInfo statInfo, Bundle bundle, boolean z3, Context context) {
        this.x = n.b();
        this.A = bundle;
        String string = bundle.getString(a.g);
        String string2 = bundle.getString(a.s);
        this.C = bundle.getString(a.y);
        this.B = bundle.getString(a.z);
        if (bundle.getString(a.D) != null) {
            this.F = true;
        }
        this.D = z3;
        this.o = AstApp.i();
        this.n = context;
        this.p = simpleAppModel;
        this.q = j;
        this.f3608a = z2;
        this.r = popViewDialog;
        if (!TextUtils.isEmpty(string)) {
            try {
                this.s = Byte.valueOf(string).byteValue();
                this.u = this.s;
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        } else {
            this.s = simpleAppModel.P;
        }
        if (!TextUtils.isEmpty(string2)) {
            try {
                this.t = Integer.valueOf(string2).intValue();
                this.v = this.t;
            } catch (NumberFormatException e2) {
                e2.printStackTrace();
            }
            simpleAppModel.Q = (byte) this.t;
        } else {
            if (this.p.d()) {
                this.p.Q = 1;
            }
            if (this.p.e()) {
                this.p.Q = 2;
            }
        }
        AppConst.AppState a2 = a(simpleAppModel);
        if (AppConst.AppState.SDKUNSUPPORT == a2) {
            a(context.getString(R.string.unsupported));
            b(context.getResources().getColor(R.color.apk_size));
            return;
        }
        com.tencent.assistant.manager.a.a().a(this.p.q(), this);
        if (o() || p()) {
            this.x.c();
            this.o.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_QQ_AUTH_FAIL, this);
            this.o.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_QQ_AUTH_SUCCESS, this);
            this.o.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_QQ_WRITE_AUTH_SUCCESS, this);
            this.o.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_QQ_WRITE_AUTH_FAIL, this);
            this.o.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_QQ_AUTH_CANCEL, this);
            l();
        }
        this.o.k().addUIEventListener(1016, this);
        a(a2);
        k();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.component.appdetail.process.e.a(boolean, com.tencent.pangu.component.appdetail.process.AppdetailActionUIListener$AuthType):void
     arg types: [int, com.tencent.pangu.component.appdetail.process.AppdetailActionUIListener$AuthType]
     candidates:
      com.tencent.pangu.component.appdetail.process.i.a(int, com.tencent.assistant.model.l):void
      com.tencent.pangu.component.appdetail.process.i.a(com.tencent.pangu.download.DownloadInfo, com.tencent.assistant.AppConst$AppState):void
      com.tencent.pangu.component.appdetail.process.i.a(boolean, int):void
      com.tencent.pangu.component.appdetail.process.e.a(int, int):void
      com.tencent.pangu.component.appdetail.process.e.a(android.content.Context, com.tencent.assistant.model.SimpleAppModel):void
      com.tencent.pangu.component.appdetail.process.e.a(java.lang.String, int):void
      com.tencent.pangu.component.appdetail.process.e.a(java.lang.String, java.lang.String):void
      com.tencent.pangu.component.appdetail.process.e.a(boolean, com.tencent.pangu.component.appdetail.process.AppdetailActionUIListener$AuthType):void */
    private void k() {
        if (o() || p()) {
            byte a2 = u.a(this.q);
            if (j()) {
                b(false);
                if (s.a(a2, (byte) 1)) {
                    a(false, AppdetailActionUIListener.AuthType.f3604a);
                }
            } else if ((this.s & 1) != 0) {
                b(true);
                if (s.a(a2, (byte) 0)) {
                    c(false);
                } else {
                    c(true);
                }
                if (s.a(a2, (byte) 1)) {
                    d(false);
                } else {
                    d(true);
                }
            } else {
                b(false);
                if (s.a(a2, (byte) 1)) {
                    a(false, AppdetailActionUIListener.AuthType.f3604a);
                }
            }
        } else {
            b(false);
        }
    }

    private void l() {
        if (!q() || s.a(u.a(this.q), (byte) 1) || TextUtils.isEmpty(this.C) || TextUtils.isEmpty(this.B)) {
            return;
        }
        if ("game_openId".equals(this.B)) {
            a(false);
            this.z = a(this.C, (byte) 1, (byte) 5, false);
        } else if ("qqNumber".equals(this.B)) {
            a(false);
            this.z = a(this.C, (byte) 1, (byte) 3, false);
        }
    }

    private void m() {
        if (this.y == null) {
            j jVar = new j(this);
            if (p()) {
                jVar.loadingText = this.n.getResources().getString(R.string.vie_number_doing);
            } else if (o()) {
                jVar.loadingText = this.n.getResources().getString(R.string.wx_auth_doing);
            } else {
                return;
            }
            this.y = DialogUtils.showLoadingDialog(jVar);
        } else if (this.y.getOwnerActivity() != null && !this.y.getOwnerActivity().isFinishing()) {
            this.y.show();
        }
    }

    private void n() {
        if (this.y != null) {
            this.y.dismiss();
        }
    }

    public int a(String str, byte b, byte b2, boolean z2) {
        m();
        if (str == null) {
            str = Constants.STR_EMPTY;
        }
        l lVar = new l();
        VerifyInfo verifyInfo = new VerifyInfo();
        verifyInfo.f1607a = this.p.f938a;
        verifyInfo.b = this.p.b;
        verifyInfo.c = this.p.c;
        verifyInfo.g = str;
        verifyInfo.f = Constants.STR_EMPTY;
        verifyInfo.j = n.e;
        verifyInfo.i = b;
        verifyInfo.h = b2;
        verifyInfo.e = t();
        verifyInfo.d = this.p.g;
        verifyInfo.l = u();
        verifyInfo.m = 2;
        lVar.f948a = verifyInfo;
        lVar.d = z2;
        return this.x.a(lVar);
    }

    public void e() {
        this.o.k().removeUIEventListener(1016, this);
        this.o.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_QQ_AUTH_FAIL, this);
        this.o.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_QQ_AUTH_SUCCESS, this);
        this.o.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_QQ_AUTH_CANCEL, this);
        this.o.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_QQ_WRITE_AUTH_SUCCESS, this);
        this.o.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_QQ_WRITE_AUTH_FAIL, this);
        this.o.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
    }

    private AppConst.AppState a(SimpleAppModel simpleAppModel) {
        AppConst.AppState b = b(simpleAppModel);
        return b != AppConst.AppState.ILLEGAL ? b : k.d(simpleAppModel);
    }

    private AppConst.AppState b(SimpleAppModel simpleAppModel) {
        if (simpleAppModel == null) {
            return AppConst.AppState.ILLEGAL;
        }
        if (simpleAppModel.h > r.d()) {
            return AppConst.AppState.SDKUNSUPPORT;
        }
        AppConst.AppState appState = AppConst.AppState.ILLEGAL;
        DownloadInfo a2 = DownloadProxy.a().a(simpleAppModel.c, simpleAppModel.g, simpleAppModel.ad);
        if (a2 != null) {
            appState = k.b(a2);
        }
        if (appState != AppConst.AppState.ILLEGAL) {
            return appState;
        }
        if (!simpleAppModel.ab) {
            return AppConst.AppState.ILLEGAL;
        }
        if (e.a(simpleAppModel.c, simpleAppModel.g, simpleAppModel.ad)) {
            return AppConst.AppState.INSTALLED;
        }
        return AppConst.AppState.UPDATE;
    }

    private void a(AppConst.AppState appState) {
        if (o() || p()) {
            b(appState);
        } else {
            d(appState);
        }
    }

    public boolean j() {
        if (q() && !TextUtils.isEmpty(this.C) && !TextUtils.isEmpty(this.B) && !"code".equals(this.B)) {
            return true;
        }
        return false;
    }

    private boolean o() {
        if (this.p == null || this.E) {
            return false;
        }
        if (!(this.p.d() && (this.p.P & 2) > 0)) {
            return false;
        }
        if (this.D && (this.u & 2) != 0 && (this.v & 1) != 0) {
            return true;
        }
        AppConst.AppState d = k.d(this.p);
        if (d != AppConst.AppState.INSTALLED && d != AppConst.AppState.DOWNLOAD && d != AppConst.AppState.UPDATE) {
            this.E = true;
            return false;
        } else if (!e.a(this.p.c, this.p.g, this.p.ad)) {
            return true;
        } else {
            return false;
        }
    }

    private boolean p() {
        if (this.p == null || this.E) {
            return false;
        }
        if (!(this.p.e() && (this.p.P & 2) > 0)) {
            return false;
        }
        if (this.D && (this.u & 2) != 0 && (this.v & 2) != 0) {
            return true;
        }
        AppConst.AppState d = k.d(this.p);
        if (d != AppConst.AppState.INSTALLED && d != AppConst.AppState.DOWNLOAD && d != AppConst.AppState.UPDATE) {
            this.E = true;
            return false;
        } else if (!e.a(this.p.c, this.p.g, this.p.ad)) {
            return true;
        } else {
            return false;
        }
    }

    private boolean q() {
        return this.D && (this.u & 2) > 0 && (this.v & 3) > 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.component.appdetail.process.i.a(boolean, int):void
     arg types: [int, ?]
     candidates:
      com.tencent.pangu.component.appdetail.process.i.a(int, com.tencent.assistant.model.l):void
      com.tencent.pangu.component.appdetail.process.i.a(com.tencent.pangu.download.DownloadInfo, com.tencent.assistant.AppConst$AppState):void
      com.tencent.pangu.component.appdetail.process.e.a(int, int):void
      com.tencent.pangu.component.appdetail.process.e.a(android.content.Context, com.tencent.assistant.model.SimpleAppModel):void
      com.tencent.pangu.component.appdetail.process.e.a(java.lang.String, int):void
      com.tencent.pangu.component.appdetail.process.e.a(java.lang.String, java.lang.String):void
      com.tencent.pangu.component.appdetail.process.e.a(boolean, com.tencent.pangu.component.appdetail.process.AppdetailActionUIListener$AuthType):void
      com.tencent.pangu.component.appdetail.process.i.a(boolean, int):void */
    private void b(AppConst.AppState appState) {
        boolean z2;
        boolean z3;
        byte a2 = u.a(this.q);
        if ((this.s & 1) != 0) {
            if ((s.a(a2, (byte) 0) || s.a(a2, (byte) 2)) && (s.a(a2, (byte) 1) || s.a(a2, (byte) 3))) {
                z2 = false;
                z3 = true;
            }
            z2 = false;
            z3 = false;
        } else {
            if (s.a(a2, (byte) 1) || s.a(a2, (byte) 3)) {
                z2 = true;
                z3 = false;
            }
            z2 = false;
            z3 = false;
        }
        if (o()) {
            a(0, 100);
            a(true, (int) R.drawable.appdetail_bar_btn_downloaded_selector_v5);
            b(this.n.getResources().getColor(R.color.white));
            if (z2) {
                a(this.n.getString(R.string.auth_btn_no_number));
            } else {
                a(this.n.getString(R.string.jionbeta));
            }
            if (f()) {
                if (z3 || z2) {
                    Toast.makeText(this.n, (int) R.string.auth_fail_list_over, 0).show();
                } else {
                    b(this.n.getString(R.string.start_wx_auth));
                }
                a(false);
            }
        } else if (p()) {
            if (z2) {
                a(this.n.getString(R.string.vie_number_end), (int) R.drawable.icon_qq_disable);
            } else {
                a(this.n.getString(R.string.qq_vie_number), (int) R.drawable.icon_qq);
            }
            if (f()) {
                if (z2 || z3) {
                    Toast.makeText(this.n, (int) R.string.vie_number_fail_no_number, 0).show();
                } else {
                    b(this.p.d + this.n.getResources().getString(R.string.start_vie_number));
                }
                a(false);
            }
        } else {
            if (this.w != 2 || ApkResourceManager.getInstance().getLocalApkInfo(this.p.c) == null) {
            }
            if (3 == this.w) {
                a(this.n.getResources().getString(R.string.appdetail_appbar_send_topic));
                return;
            }
            if (appState == null || appState == AppConst.AppState.ILLEGAL) {
                appState = a(this.p);
            }
            switch (l.f3613a[appState.ordinal()]) {
                case 1:
                    a(this.n.getResources().getString(R.string.install));
                    return;
                case 2:
                    a(this.n, this.p);
                    return;
                case 3:
                    if (ak.a().c(this.p.c)) {
                        ak.a();
                        if (!TextUtils.isEmpty(ak.h())) {
                            ak.a();
                            if (ak.h().equals(this.p.c)) {
                                a(this.n.getResources().getString(R.string.qube_apk_using));
                                return;
                            }
                        }
                        a(this.n.getResources().getString(R.string.qube_apk_use));
                        return;
                    }
                    a(this.n.getResources().getString(R.string.open));
                    return;
                case 4:
                case 5:
                    a(this.n.getResources().getString(R.string.continuing));
                    return;
                case 6:
                    r();
                    return;
                case 7:
                    a(this.n.getResources().getString(R.string.queuing));
                    return;
                case 8:
                    if (this.p.c()) {
                        a(this.n.getResources().getString(R.string.jionfirstrelease) + " " + at.a(this.p.k));
                        return;
                    } else if (this.p.h()) {
                        a(this.n.getResources().getString(R.string.jionbeta) + " " + at.a(this.p.k));
                        return;
                    } else {
                        a(this.n, this.p);
                        return;
                    }
                case 9:
                    if (this.p.a() && !this.p.h() && !this.p.i()) {
                        a(this.n.getResources().getString(R.string.slim_update), at.a(this.p.k), at.a(this.p.v));
                        return;
                    } else if (!this.p.a() || (!this.p.h() && !this.p.i())) {
                        a(this.n.getResources().getString(R.string.update) + " " + at.a(this.p.k));
                        return;
                    } else {
                        a(this.n.getResources().getString(R.string.jionbeta));
                        return;
                    }
                case 10:
                    a(this.n.getResources().getString(R.string.installing));
                    return;
                case 11:
                    a(this.n.getResources().getString(R.string.uninstalling));
                    return;
                default:
                    a(this.n, this.p);
                    return;
            }
        }
    }

    private void c(AppConst.AppState appState) {
        DownloadInfo a2 = DownloadProxy.a().a(this.p);
        int i = 0;
        if (a2 != null) {
            if (c(a2, appState)) {
                i = a2.response.f;
            } else {
                i = SimpleDownloadInfo.getPercent(a2);
            }
        }
        if (appState == AppConst.AppState.INSTALLED) {
            b(this.n.getResources().getColor(R.color.white));
        } else if (appState == AppConst.AppState.DOWNLOADING || appState == AppConst.AppState.PAUSED) {
            b(this.n.getResources().getColor(R.color.appdetail_tag_text_color_blue_n));
        } else if (appState == AppConst.AppState.DOWNLOADED) {
            b(this.n.getResources().getColor(R.color.white));
        } else if (appState == AppConst.AppState.FAIL) {
        } else {
            if ((appState == AppConst.AppState.DOWNLOAD || appState == AppConst.AppState.UPDATE) && o()) {
                b(this.n.getResources().getColor(R.color.white));
            } else if (appState == AppConst.AppState.QUEUING && i <= 0) {
                b(this.n.getResources().getColor(R.color.appdetail_tag_text_color_blue_n));
            } else if (appState == AppConst.AppState.QUEUING && i > 0) {
            } else {
                if (appState == AppConst.AppState.INSTALLING) {
                    b(this.n.getResources().getColor(R.color.state_disable));
                } else {
                    b(this.n.getResources().getColor(17170443));
                }
            }
        }
    }

    private void d(AppConst.AppState appState) {
        String str = null;
        if (this.p != null) {
            str = this.p.q();
        }
        if (!TextUtils.isEmpty(str)) {
            if (appState == null || appState == AppConst.AppState.ILLEGAL) {
                appState = a(this.p);
            }
            b(DownloadProxy.a().a(this.p), appState);
            return;
        }
        a(this.n.getResources().getString(R.string.illegal_data));
    }

    private void r() {
        a(DownloadProxy.a().a(this.p), a(this.p));
    }

    private void a(DownloadInfo downloadInfo, AppConst.AppState appState) {
        double d = 0.0d;
        if (downloadInfo != null) {
            if (c(downloadInfo, appState)) {
                d = (double) downloadInfo.response.f;
            } else {
                d = SimpleDownloadInfo.getPercentFloat(downloadInfo);
            }
        }
        a(String.format(this.n.getResources().getString(R.string.downloading_percent), String.format("%.1f", Double.valueOf(d))));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.component.appdetail.process.i.a(boolean, int):void
     arg types: [int, ?]
     candidates:
      com.tencent.pangu.component.appdetail.process.i.a(int, com.tencent.assistant.model.l):void
      com.tencent.pangu.component.appdetail.process.i.a(com.tencent.pangu.download.DownloadInfo, com.tencent.assistant.AppConst$AppState):void
      com.tencent.pangu.component.appdetail.process.e.a(int, int):void
      com.tencent.pangu.component.appdetail.process.e.a(android.content.Context, com.tencent.assistant.model.SimpleAppModel):void
      com.tencent.pangu.component.appdetail.process.e.a(java.lang.String, int):void
      com.tencent.pangu.component.appdetail.process.e.a(java.lang.String, java.lang.String):void
      com.tencent.pangu.component.appdetail.process.e.a(boolean, com.tencent.pangu.component.appdetail.process.AppdetailActionUIListener$AuthType):void
      com.tencent.pangu.component.appdetail.process.i.a(boolean, int):void */
    private void b(DownloadInfo downloadInfo, AppConst.AppState appState) {
        if (!(this.w == 2 && ApkResourceManager.getInstance().getLocalApkInfo(this.p.c) != null && appState == AppConst.AppState.INSTALLED)) {
        }
        if (appState == AppConst.AppState.DOWNLOADING || appState == AppConst.AppState.FAIL || appState == AppConst.AppState.PAUSED || appState == AppConst.AppState.QUEUING) {
            if (!(downloadInfo == null || downloadInfo.response == null)) {
                d(0);
                if (3 == this.w) {
                    a(true, (int) R.drawable.btn_green_selector);
                } else {
                    a(false, (int) R.drawable.btn_green_selector);
                }
                if (c(downloadInfo, appState)) {
                    a(downloadInfo.response.f, 0);
                } else {
                    a(SimpleDownloadInfo.getPercent(downloadInfo), 0);
                }
            }
        } else if (appState == AppConst.AppState.INSTALLED) {
            a(0, 100);
            d(8);
            if (this.w == 1) {
                a(true, (int) R.drawable.appdetail_bar_btn_installed_selector_v5_shixin);
            } else {
                a(true, (int) R.drawable.btn_green_selector);
            }
        } else if (appState == AppConst.AppState.DOWNLOADED) {
            d(8);
            a(0, 0);
            a(true, (int) R.drawable.appdetail_bar_btn_downloaded_selector_v5);
        } else if (appState == AppConst.AppState.INSTALLING) {
            a(0, 100);
            a(true, (int) R.drawable.common_btn_big_disabled);
        } else if ((appState == AppConst.AppState.DOWNLOAD || appState == AppConst.AppState.UPDATE) && o()) {
            a(0, 100);
            a(true, (int) R.drawable.appdetail_bar_btn_downloaded_selector_v5);
        } else {
            a(0, 100);
            a(true, (int) R.drawable.btn_green_selector);
        }
        b(appState);
        if ((appState == AppConst.AppState.DOWNLOADING || appState == AppConst.AppState.PAUSED) && SimpleDownloadInfo.getPercent(downloadInfo) >= 68) {
            b(this.n.getResources().getColor(17170443));
        } else {
            c(appState);
        }
    }

    private boolean c(DownloadInfo downloadInfo, AppConst.AppState appState) {
        return (appState == AppConst.AppState.DOWNLOADING || (downloadInfo.response.b > 0 && (appState == AppConst.AppState.PAUSED || appState == AppConst.AppState.FAIL))) && downloadInfo.response.f > SimpleDownloadInfo.getPercent(downloadInfo);
    }

    private void a(boolean z2, int i) {
        if (z2) {
            c(0);
            a(this.n.getResources().getDrawable(i));
            return;
        }
        c(8);
    }

    public void a(boolean z2, AppConst.AppState appState, long j, String str, int i, int i2, boolean z3) {
        this.f3608a = z2;
        if (i > 0) {
            this.b = str;
            this.c = i;
            this.e = i2;
            this.d = j;
            this.f = z3;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.component.appdetail.process.i.a(boolean, int):void
     arg types: [int, ?]
     candidates:
      com.tencent.pangu.component.appdetail.process.i.a(int, com.tencent.assistant.model.l):void
      com.tencent.pangu.component.appdetail.process.i.a(com.tencent.pangu.download.DownloadInfo, com.tencent.assistant.AppConst$AppState):void
      com.tencent.pangu.component.appdetail.process.e.a(int, int):void
      com.tencent.pangu.component.appdetail.process.e.a(android.content.Context, com.tencent.assistant.model.SimpleAppModel):void
      com.tencent.pangu.component.appdetail.process.e.a(java.lang.String, int):void
      com.tencent.pangu.component.appdetail.process.e.a(java.lang.String, java.lang.String):void
      com.tencent.pangu.component.appdetail.process.e.a(boolean, com.tencent.pangu.component.appdetail.process.AppdetailActionUIListener$AuthType):void
      com.tencent.pangu.component.appdetail.process.i.a(boolean, int):void */
    public void a(int i) {
        AppConst.AppState a2 = a(this.p);
        if ((a2 == null || AppConst.AppState.SDKUNSUPPORT != a2) && i <= 3 && i >= 1) {
            this.w = i;
            if (this.p != null && !o() && !p()) {
                if (i == 2) {
                    if (ApkResourceManager.getInstance().getLocalApkInfo(this.p.c) != null) {
                    }
                } else if (1 == i) {
                    if (a2 == AppConst.AppState.INSTALLED) {
                        a(0, 100);
                        a(true, (int) R.drawable.appdetail_bar_btn_installed_selector_v5_shixin);
                    } else {
                        b(DownloadProxy.a().a(this.p), a2);
                    }
                } else if (3 == i) {
                    a(true, (int) R.drawable.btn_green_selector);
                }
                b(a2);
                c(a2);
            }
        }
    }

    public void handleUIEvent(Message message) {
        int e;
        switch (message.what) {
            case 1016:
                if (this.p != null) {
                    d(a(this.p));
                    break;
                }
                break;
            case EventDispatcherEnum.UI_EVENT_QQ_AUTH_SUCCESS:
                if (this.z == message.arg2) {
                    this.z = -1;
                    this.z = a(((Bundle) message.obj).getString("qqNumber"), (byte) 1, (byte) 3, true);
                    break;
                } else {
                    return;
                }
            case EventDispatcherEnum.UI_EVENT_QQ_AUTH_FAIL:
                if (this.z == message.arg2) {
                    this.z = -1;
                    this.z = s();
                    Toast.makeText(this.n, (int) R.string.qq_auth_fail, 0).show();
                    break;
                } else {
                    return;
                }
            case EventDispatcherEnum.UI_EVENT_QQ_AUTH_CANCEL:
                if (this.z == message.arg2 && this.n != null) {
                    this.z = -1;
                    Toast.makeText(this.n, (int) R.string.qq_auth_fail_cancel, 0).show();
                    break;
                } else {
                    return;
                }
                break;
            case EventDispatcherEnum.UI_EVENT_QQ_WRITE_AUTH_SUCCESS:
                if (this.z == message.arg2) {
                    this.z = -1;
                    n();
                    a(message.arg1, (l) message.obj);
                    break;
                } else {
                    return;
                }
            case EventDispatcherEnum.UI_EVENT_QQ_WRITE_AUTH_FAIL:
                if (this.z == message.arg2) {
                    this.z = -1;
                    if (((l) message.obj).f948a.i == 1) {
                        n();
                        if (p()) {
                            Toast.makeText(this.n, (int) R.string.vie_number_fail, 0).show();
                            break;
                        }
                    }
                } else {
                    return;
                }
                break;
            case EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS:
                XLog.i("xjp", "[AppdetailQQProcess] : 登录成功");
                Bundle bundle = (Bundle) message.obj;
                if (bundle == null || !bundle.containsKey(AppConst.KEY_FROM_TYPE)) {
                    e = com.tencent.nucleus.socialcontact.login.l.e();
                    com.tencent.nucleus.socialcontact.login.l.a(0);
                } else {
                    e = bundle.getInt(AppConst.KEY_FROM_TYPE);
                    com.tencent.nucleus.socialcontact.login.l.a(0);
                }
                if (11 != e && 12 != e) {
                    if (19 == e && this.r != null) {
                        this.r.e();
                        break;
                    }
                } else {
                    com.tencent.pangu.link.b.a(this.n, Uri.parse("tmast://webview?url=http://mq.wsq.qq.com/direct?route=sId/t/new&pkgName=" + this.p.c));
                    break;
                }
                break;
        }
        a(a(this.p));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.component.appdetail.process.e.a(boolean, com.tencent.pangu.component.appdetail.process.AppdetailActionUIListener$AuthType):void
     arg types: [int, com.tencent.pangu.component.appdetail.process.AppdetailActionUIListener$AuthType]
     candidates:
      com.tencent.pangu.component.appdetail.process.i.a(int, com.tencent.assistant.model.l):void
      com.tencent.pangu.component.appdetail.process.i.a(com.tencent.pangu.download.DownloadInfo, com.tencent.assistant.AppConst$AppState):void
      com.tencent.pangu.component.appdetail.process.i.a(boolean, int):void
      com.tencent.pangu.component.appdetail.process.e.a(int, int):void
      com.tencent.pangu.component.appdetail.process.e.a(android.content.Context, com.tencent.assistant.model.SimpleAppModel):void
      com.tencent.pangu.component.appdetail.process.e.a(java.lang.String, int):void
      com.tencent.pangu.component.appdetail.process.e.a(java.lang.String, java.lang.String):void
      com.tencent.pangu.component.appdetail.process.e.a(boolean, com.tencent.pangu.component.appdetail.process.AppdetailActionUIListener$AuthType):void */
    private void a(int i, l lVar) {
        if (lVar.f948a.i == 1) {
            switch (i) {
                case 0:
                    e(lVar.d);
                    this.E = true;
                    this.G = true;
                    return;
                case 1:
                default:
                    if (o()) {
                        Toast.makeText(this.n, (int) R.string.qq_auth_fail, 0).show();
                        return;
                    } else if (p()) {
                        Toast.makeText(this.n, (int) R.string.vie_number_fail, 0).show();
                        return;
                    } else {
                        return;
                    }
                case 2:
                    Toast.makeText(this.n, this.n.getResources().getString(R.string.wx_qq_auth_fail_code_invalid), 0).show();
                    return;
                case 3:
                    n.b(this.q, (byte) 2);
                    if ((this.s & 1) != 0) {
                        d(false);
                    } else {
                        a(false, AppdetailActionUIListener.AuthType.f3604a);
                        a(e((int) R.string.vie_number_end), (int) R.drawable.icon_qq_disable);
                    }
                    Toast.makeText(this.n, (int) R.string.vie_number_fail_no_number, 0).show();
                    return;
                case 4:
                    Toast.makeText(this.n, (int) R.string.vie_number_fail_no_need, 0).show();
                    this.s = 0;
                    this.t = 0;
                    this.E = true;
                    k();
                    a(a(this.p));
                    return;
                case 5:
                    n.b(this.q, (byte) 8);
                    if ((this.s & 1) != 0) {
                        d(false);
                    } else {
                        a(false, AppdetailActionUIListener.AuthType.f3604a);
                        a(e((int) R.string.vie_number_end), (int) R.drawable.icon_qq_disable);
                    }
                    Toast.makeText(this.n, (int) R.string.vie_number_fail_today_no_number, 0).show();
                    return;
            }
        }
    }

    private int s() {
        l lVar = new l();
        VerifyInfo verifyInfo = new VerifyInfo();
        verifyInfo.f1607a = this.p.f938a;
        verifyInfo.b = this.p.b;
        verifyInfo.k = Constants.STR_EMPTY;
        verifyInfo.c = this.p.c;
        verifyInfo.g = Constants.STR_EMPTY;
        verifyInfo.f = Constants.STR_EMPTY;
        verifyInfo.j = n.e;
        verifyInfo.i = 0;
        verifyInfo.h = 2;
        verifyInfo.e = t();
        verifyInfo.d = this.p.g;
        verifyInfo.l = u();
        lVar.f948a = verifyInfo;
        return this.x.a(lVar);
    }

    private void e(boolean z2) {
        b(false);
        boolean p2 = p();
        if (p2) {
            Message obtainMessage = this.o.j().obtainMessage();
            obtainMessage.what = EventDispatcherEnum.UI_EVENT_VIEBUMBER_SUCC;
            obtainMessage.obj = Long.valueOf(this.p.f938a);
            this.o.j().sendMessage(obtainMessage);
        }
        AppConst.AppState a2 = a(this.p);
        if (p2) {
            if (a2 == AppConst.AppState.DOWNLOADED) {
                b(this.n.getString(R.string.vie_number_suc_install));
                return;
            } else if (a2 == AppConst.AppState.INSTALLED) {
                b(this.n.getString(R.string.vie_number_suc_open));
                return;
            }
        } else if (o()) {
            if (a2 == AppConst.AppState.DOWNLOADED) {
                b(this.n.getString(R.string.auth_suc_install));
                return;
            } else if (a2 == AppConst.AppState.INSTALLED) {
                b(this.n.getString(R.string.auth_suc_open));
                return;
            }
        }
        if (c.e()) {
            if (AppConst.AppState.SDKUNSUPPORT != a2) {
                if (this.i != null) {
                    if (p2) {
                        this.i.b();
                    } else if (o()) {
                        this.i.a();
                    }
                }
                h();
                DownloadInfo a3 = DownloadProxy.a().a(this.p);
                if (a3 != null && a3.needReCreateInfo(this.p)) {
                    DownloadProxy.a().b(a3.downloadTicket);
                    a3 = null;
                }
                StatInfo a4 = com.tencent.assistantv2.st.page.a.a(i());
                a4.d = g();
                if (a3 == null) {
                    a3 = DownloadInfo.createDownloadInfo(this.p, a4);
                    a2 = a(this.p);
                } else {
                    a3.updateDownloadInfoStatInfo(a4);
                }
                if (this.A != null) {
                    a3.initCallParamsFromActionBundle(this.A);
                }
                if (!TextUtils.isEmpty(this.p.q())) {
                    switch (l.f3613a[a2.ordinal()]) {
                        case 2:
                        case 5:
                            com.tencent.pangu.download.a.a().a(a3);
                            AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_APP_DETAIL_DOWNLOAD_CLICK));
                            a(a3, a2);
                            return;
                        case 3:
                        case 6:
                        case 7:
                        default:
                            return;
                        case 4:
                            com.tencent.pangu.download.a.a().b(a3);
                            AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_APP_DETAIL_DOWNLOAD_CLICK));
                            a(a3, a2);
                            return;
                        case 8:
                        case 9:
                            com.tencent.pangu.download.a.a().a(a3);
                            AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_APP_DETAIL_DOWNLOAD_CLICK));
                            a(a3, a2);
                            return;
                    }
                }
            }
        } else if (p2) {
            b(this.n.getResources().getString(R.string.vie_number_suc_click_download));
        } else if (o()) {
            b(this.n.getResources().getString(R.string.auth_suc_click_download));
        }
    }

    public void onAppStateChange(String str, AppConst.AppState appState) {
        if (!o() && !p()) {
            b(DownloadProxy.a().d(str), appState);
        }
    }

    public void b() {
        this.o.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_LOGIN_SUCCESS, this);
    }

    public void c() {
        AppConst.AppState a2 = a(this.p);
        com.tencent.assistant.manager.a.a().a(this.p.q(), this);
        a(a2);
    }

    private int t() {
        PackageInfo d = e.d("com.tencent.mobileqq", 0);
        if (d != null) {
            return d.versionCode;
        }
        return 0;
    }

    private String e(int i) {
        if (this.n == null) {
            return Constants.STR_EMPTY;
        }
        return this.n.getResources().getString(i);
    }

    private byte u() {
        if (this.p == null) {
            return 0;
        }
        return this.p.Q;
    }

    public void a() {
        if (o() || p()) {
            this.z = this.x.a((Activity) this.n);
            return;
        }
        if (!(this.w == 2 && ApkResourceManager.getInstance().getLocalApkInfo(this.p.c) == null)) {
        }
        AppConst.AppState a2 = a(this.p);
        if (AppConst.AppState.SDKUNSUPPORT == a2) {
            Toast.makeText(this.n, this.n.getResources().getString(R.string.canot_support_sofrware), 0).show();
            return;
        }
        DownloadInfo a3 = DownloadProxy.a().a(this.p);
        if (a3 != null && a3.needReCreateInfo(this.p)) {
            DownloadProxy.a().b(a3.downloadTicket);
            a3 = null;
        }
        StatInfo a4 = com.tencent.assistantv2.st.page.a.a(i());
        a4.d = g();
        if (a3 == null) {
            a3 = DownloadInfo.createDownloadInfo(this.p, a4);
        } else {
            a3.updateDownloadInfoStatInfo(a4);
        }
        if (this.A != null) {
            a3.initCallParamsFromActionBundle(this.A);
        }
        if (TextUtils.isEmpty(this.p.q())) {
            return;
        }
        if (3 != this.w) {
            switch (l.f3613a[a2.ordinal()]) {
                case 1:
                    com.tencent.pangu.download.a.a().d(a3);
                    a(this.n.getResources().getString(R.string.install));
                    return;
                case 2:
                case 5:
                    com.tencent.pangu.download.a.a().a(a3);
                    a(a3, a2);
                    return;
                case 3:
                    com.tencent.pangu.download.a.a().c(a3);
                    if (!ak.a().c(this.p.c)) {
                        a(this.n.getResources().getString(R.string.open));
                    }
                    if (!this.F && this.D) {
                        ((Activity) this.n).finish();
                        return;
                    }
                    return;
                case 4:
                    com.tencent.pangu.download.a.a().b(a3);
                    a(a3, a2);
                    return;
                case 6:
                    com.tencent.pangu.download.a.a().b(a3.downloadTicket);
                    a(this.n.getResources().getString(R.string.continuing));
                    return;
                case 7:
                    com.tencent.pangu.download.a.a().b(a3.downloadTicket);
                    a(this.n.getResources().getString(R.string.pause));
                    return;
                case 8:
                case 9:
                    com.tencent.pangu.download.a.a().a(a3);
                    a(a3, a2);
                    if (this.i != null && this.G) {
                        if (this.p.e()) {
                            this.i.a(4);
                            return;
                        } else if (this.p.d()) {
                            this.i.a(3);
                            return;
                        } else {
                            return;
                        }
                    } else {
                        return;
                    }
                case 10:
                    Toast.makeText(this.n, (int) R.string.tips_slicent_install, 0).show();
                    return;
                case 11:
                    Toast.makeText(this.n, (int) R.string.tips_slicent_uninstall, 0).show();
                    return;
                default:
                    return;
            }
        } else if (j.a().l()) {
            v();
        } else if (j.a().k()) {
            com.tencent.pangu.link.b.a(this.n, Uri.parse("tmast://webview?url=http://mq.wsq.qq.com/direct?route=sId/t/new&pkgName=" + this.p.c));
        } else {
            Bundle bundle = new Bundle();
            bundle.putInt(AppConst.KEY_LOGIN_TYPE, 5);
            bundle.putInt(AppConst.KEY_FROM_TYPE, 11);
            com.tencent.nucleus.socialcontact.login.l.a(11);
            j.a().a(AppConst.IdentityType.MOBILEQ, bundle);
        }
    }

    private void v() {
        k kVar = new k(this);
        Resources resources = this.n.getResources();
        kVar.titleRes = resources.getString(R.string.login_prompt);
        kVar.contentRes = resources.getString(R.string.login_prompt_content);
        kVar.lBtnTxtRes = resources.getString(R.string.cancel);
        kVar.rBtnTxtRes = resources.getString(R.string.login_prompt_switch_account);
        DialogUtils.show2BtnDialog(kVar);
    }

    public void a(View view) {
        int id = view.getId();
        if (id == R.id.btn_pause_download) {
            a();
        } else if (id == R.id.btn_delete_download) {
            DownloadInfo a2 = DownloadProxy.a().a(this.p);
            if (a2 != null) {
                DownloadProxy.a().b(a2.downloadTicket);
            }
            b((DownloadInfo) null, a(this.p));
        } else if (id == R.id.appdetail_progress_btn_for_cmd) {
            if (ApkResourceManager.getInstance().getLocalApkInfo(this.p.c) != null || (ak.a().c(this.p.c) && ak.a().d(this.p.c))) {
                this.r.a(this.f3608a, this.p.f938a, this.q, this.d, this.b, this.c, this.e, (AppDetailActivityV5) this.n, this.p.d, this.f);
                try {
                    this.r.d();
                } catch (Throwable th) {
                }
            } else {
                Toast.makeText(this.n, (int) R.string.comment_txt_tips_need_install, 1).show();
                ((View) view.getParent()).setVisibility(8);
            }
        } else if (id != R.id.appdetail_progress_btn_for_appbar) {
        } else {
            if (j.a().l()) {
                v();
            } else if (j.a().k()) {
                com.tencent.pangu.link.b.a(this.n, Uri.parse("tmast://webview?url=http://mq.wsq.qq.com/direct?route=sId/t/new&pkgName=" + this.p.c));
            } else {
                Bundle bundle = new Bundle();
                bundle.putInt(AppConst.KEY_LOGIN_TYPE, 5);
                bundle.putInt(AppConst.KEY_FROM_TYPE, 11);
                com.tencent.nucleus.socialcontact.login.l.a(11);
                j.a().a(AppConst.IdentityType.MOBILEQ, bundle);
            }
        }
    }

    public int g() {
        byte u2 = u();
        if (u2 == 2) {
            return 21;
        }
        return u2;
    }
}
