package com.tencent.pangu.manager;

import com.tencent.pangu.download.DownloadInfo;
import java.io.File;
import java.io.FilenameFilter;

/* compiled from: ProGuard */
class ar implements FilenameFilter {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadInfo f3800a;
    final /* synthetic */ ak b;

    ar(ak akVar, DownloadInfo downloadInfo) {
        this.b = akVar;
        this.f3800a = downloadInfo;
    }

    public boolean accept(File file, String str) {
        if (!str.startsWith(this.f3800a.packageName) || !str.endsWith(".apk")) {
            return false;
        }
        return true;
    }
}
