package com.tencent.pangu.manager;

import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.protocol.jce.AdviceApp;

/* compiled from: ProGuard */
class ay extends AppConst.TwoBtnDialogInfo {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bj f3807a;
    final /* synthetic */ au b;

    ay(au auVar, bj bjVar) {
        this.b = auVar;
        this.f3807a = bjVar;
    }

    public void onCancell() {
        int i = 2000;
        if (AstApp.m() != null) {
            i = AstApp.m().f();
        }
        AdviceApp b2 = this.f3807a.b();
        this.b.a(this.pageId, i, "03_002", 200, b2 != null ? b2.i() : null);
    }

    public void onLeftBtnClick() {
        int i = 2000;
        if (AstApp.m() != null) {
            i = AstApp.m().f();
        }
        AdviceApp b2 = this.f3807a.b();
        this.b.a(this.pageId, i, "03_002", 200, b2 != null ? b2.i() : null);
    }

    public void onRightBtnClick() {
        this.b.a(this.pageId);
        this.b.a(this.f3807a.e, this.f3807a.f);
        int i = 2000;
        if (AstApp.m() != null) {
            i = AstApp.m().f();
        }
        AdviceApp b2 = this.f3807a.b();
        this.b.a(this.pageId, i, "03_001", 200, b2 != null ? b2.i() : null);
    }
}
