package com.tencent.pangu.manager;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import com.qq.AppService.AstApp;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class ag extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Dialog f3790a;
    final /* synthetic */ Activity b;
    final /* synthetic */ ac c;

    ag(ac acVar, Dialog dialog, Activity activity) {
        this.c = acVar;
        this.f3790a = dialog;
        this.b = activity;
    }

    public void onTMAClick(View view) {
        this.f3790a.dismiss();
        this.b.finish();
        AstApp.i().f();
    }

    public STInfoV2 getStInfo() {
        STInfoV2 a2 = this.c.c(this.b);
        a2.slotId = "03_002";
        a2.actionId = 200;
        return a2;
    }
}
