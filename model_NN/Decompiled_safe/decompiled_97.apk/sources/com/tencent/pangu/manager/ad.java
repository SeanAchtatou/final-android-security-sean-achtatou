package com.tencent.pangu.manager;

import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.utils.XLog;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
class ad implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ac f3787a;

    ad(ac acVar) {
        this.f3787a = acVar;
    }

    public void run() {
        ArrayList arrayList = (ArrayList) ApkResourceManager.getInstance().getLocalApkInfos();
        ArrayList<LocalApkInfo> lastInstalledApkInfo = ApkResourceManager.getInstance().getLastInstalledApkInfo();
        if (lastInstalledApkInfo != null && !lastInstalledApkInfo.isEmpty() && arrayList != null && !arrayList.isEmpty()) {
            Iterator<LocalApkInfo> it = lastInstalledApkInfo.iterator();
            while (it.hasNext()) {
                LocalApkInfo next = it.next();
                if (!arrayList.contains(next)) {
                    XLog.d("AreadyRemind", "uninstall app" + next.mPackageName);
                    XLog.d("AreadyRemind", " uninstall state" + next.mInstall);
                    this.f3787a.e.add(next);
                }
            }
        }
    }
}
