package com.tencent.pangu.manager;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.db.helper.ExternalDbHelper;
import com.tencent.assistant.db.helper.SQLiteDatabaseWrapper;
import com.tencent.assistant.db.table.v;
import com.tencent.assistant.db.table.w;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.ad;
import com.tencent.assistant.net.c;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ay;
import com.tencent.assistant.utils.e;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;
import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/* compiled from: ProGuard */
public class ak {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f3794a = false;
    /* access modifiers changed from: private */
    public static int b = 0;
    private static ak c = null;
    /* access modifiers changed from: private */
    public static Handler d = new al(AstApp.i().getMainLooper());
    /* access modifiers changed from: private */
    public boolean e = false;

    public static synchronized ak a() {
        ak akVar;
        synchronized (ak.class) {
            if (c == null) {
                c = new ak();
            }
            akVar = c;
        }
        return akVar;
    }

    public static boolean b() {
        return f3794a;
    }

    public static void a(boolean z) {
        f3794a = z;
    }

    public static SimpleDownloadInfo.DownloadState c() {
        List<DownloadInfo> e2 = DownloadProxy.a().e("com.tencent.qlauncher");
        if (e2 == null || e2.size() <= 0) {
            return null;
        }
        return e2.get(0).downloadState;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0098 A[SYNTHETIC, Splitter:B:30:0x0098] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.tencent.pangu.manager.at a(java.lang.String r9) {
        /*
            r8 = this;
            r6 = 0
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>()
            com.qq.AppService.AstApp r0 = com.qq.AppService.AstApp.i()     // Catch:{ Throwable -> 0x0084, all -> 0x0095 }
            android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ Throwable -> 0x0084, all -> 0x0095 }
            java.lang.String r1 = "content://com.tencent.qlauncher.theme.db.ThemeDBProvider/guest_qqdownloader"
            android.net.Uri r1 = android.net.Uri.parse(r1)     // Catch:{ Throwable -> 0x0084, all -> 0x0095 }
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Throwable -> 0x0084, all -> 0x0095 }
            if (r0 == 0) goto L_0x0060
            boolean r1 = r0.moveToFirst()     // Catch:{ Throwable -> 0x00a4 }
            if (r1 == 0) goto L_0x0060
        L_0x0024:
            java.io.PrintStream r1 = java.lang.System.out     // Catch:{ Throwable -> 0x00a4 }
            r2 = 1
            java.lang.String r2 = r0.getColumnName(r2)     // Catch:{ Throwable -> 0x00a4 }
            r1.println(r2)     // Catch:{ Throwable -> 0x00a4 }
            com.tencent.pangu.manager.at r1 = new com.tencent.pangu.manager.at     // Catch:{ Throwable -> 0x00a4 }
            r1.<init>(r8)     // Catch:{ Throwable -> 0x00a4 }
            r2 = 0
            int r2 = r0.getInt(r2)     // Catch:{ Throwable -> 0x00a4 }
            r1.f3802a = r2     // Catch:{ Throwable -> 0x00a4 }
            r2 = 1
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Throwable -> 0x00a4 }
            r1.b = r2     // Catch:{ Throwable -> 0x00a4 }
            r2 = 2
            int r2 = r0.getInt(r2)     // Catch:{ Throwable -> 0x00a4 }
            r1.c = r2     // Catch:{ Throwable -> 0x00a4 }
            r2 = 3
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Throwable -> 0x00a4 }
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)     // Catch:{ Throwable -> 0x00a4 }
            boolean r2 = r2.booleanValue()     // Catch:{ Throwable -> 0x00a4 }
            r1.d = r2     // Catch:{ Throwable -> 0x00a4 }
            r7.add(r1)     // Catch:{ Throwable -> 0x00a4 }
            boolean r1 = r0.moveToNext()     // Catch:{ Throwable -> 0x00a4 }
            if (r1 != 0) goto L_0x0024
        L_0x0060:
            if (r0 == 0) goto L_0x0065
            r0.close()     // Catch:{ Exception -> 0x0093 }
        L_0x0065:
            int r0 = r7.size()
            if (r0 <= 0) goto L_0x009c
            java.util.Iterator r1 = r7.iterator()
        L_0x006f:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x009c
            java.lang.Object r0 = r1.next()
            com.tencent.pangu.manager.at r0 = (com.tencent.pangu.manager.at) r0
            java.lang.String r2 = r0.b
            boolean r2 = r2.equals(r9)
            if (r2 == 0) goto L_0x006f
        L_0x0083:
            return r0
        L_0x0084:
            r0 = move-exception
            r0 = r6
        L_0x0086:
            java.lang.String r1 = "QubeManager"
            java.lang.String r2 = "<Qube> getInstalledThemeByPackage read qlauncher provider error"
            com.tencent.assistant.utils.XLog.e(r1, r2)     // Catch:{ all -> 0x00a0 }
            if (r0 == 0) goto L_0x0065
            r0.close()     // Catch:{ Exception -> 0x0093 }
            goto L_0x0065
        L_0x0093:
            r0 = move-exception
            goto L_0x0065
        L_0x0095:
            r0 = move-exception
        L_0x0096:
            if (r6 == 0) goto L_0x009b
            r6.close()     // Catch:{ Exception -> 0x009e }
        L_0x009b:
            throw r0
        L_0x009c:
            r0 = r6
            goto L_0x0083
        L_0x009e:
            r1 = move-exception
            goto L_0x009b
        L_0x00a0:
            r1 = move-exception
            r6 = r0
            r0 = r1
            goto L_0x0096
        L_0x00a4:
            r1 = move-exception
            goto L_0x0086
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.pangu.manager.ak.a(java.lang.String):com.tencent.pangu.manager.at");
    }

    public static boolean d() {
        boolean z = false;
        try {
            PackageInfo packageInfo = AstApp.i().getApplicationContext().getPackageManager().getPackageInfo("com.tencent.qlauncher", 0);
            if (packageInfo != null) {
                z = g(packageInfo.versionName);
            }
        } catch (Exception e2) {
            Log.e("QubeManager", "<Qube> isQLauncherAvaliable 1 =", e2);
        }
        Log.d("QubeManager", "isQLauncherAvaliable = " + z);
        return z;
    }

    public static boolean a(DownloadInfo downloadInfo) {
        boolean z = false;
        if (downloadInfo != null) {
            z = g(downloadInfo.versionName);
        }
        Log.d("QubeManager", "isQLauncherAvaliable 2 = " + z);
        return z;
    }

    private static boolean g(String str) {
        int i;
        int i2;
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        String[] strArr = {Constants.STR_EMPTY, Constants.STR_EMPTY, Constants.STR_EMPTY};
        String[] split = str.split("\\.");
        int i3 = 0;
        while (i3 < split.length && i3 < 3) {
            strArr[i3] = split[i3];
            i3++;
        }
        int[] iArr = {3, 6, 617};
        int i4 = 0;
        while (i4 < iArr.length) {
            try {
                i2 = Integer.parseInt(strArr[i4]);
            } catch (NumberFormatException e2) {
                i2 = 0;
            }
            try {
                if (i2 > iArr[i4]) {
                    return true;
                }
                i4++;
            } catch (Exception e3) {
                Log.e("QubeManager", "<Qube> isQLauncherAvaliable", e3);
                return false;
            }
        }
        for (int i5 = 0; i5 < iArr.length; i5++) {
            try {
                i = Integer.parseInt(strArr[i5]);
            } catch (NumberFormatException e4) {
                i = 0;
            }
            if (i != iArr[i5]) {
                return false;
            }
        }
        return true;
    }

    public static boolean e() {
        PackageInfo packageInfo;
        boolean z;
        try {
            packageInfo = AstApp.i().getApplicationContext().getPackageManager().getPackageInfo("com.tencent.qlauncher", 0);
        } catch (Exception e2) {
            packageInfo = null;
        }
        StringBuilder append = new StringBuilder().append("<Qube> isQubeInstalled -> ");
        if (packageInfo != null) {
            z = true;
        } else {
            z = false;
        }
        Log.i("QubeManager", append.append(z).toString());
        if (packageInfo != null) {
            return true;
        }
        return false;
    }

    private static int i(DownloadInfo downloadInfo) {
        PackageInfo packageInfo;
        int i = 0;
        try {
            packageInfo = AstApp.i().getApplicationContext().getPackageManager().getPackageInfo("com.tencent.qlauncher", 0);
            Log.d("QubeManager", "<Qube> qlauncher versionCode = " + packageInfo.versionCode + " ,minQLauncherVersionCode = " + downloadInfo.minQLauncherVersionCode + ", maxQLauncherVersionCode = " + downloadInfo.maxQLauncherVersionCode);
        } catch (Exception e2) {
            packageInfo = null;
        }
        if (packageInfo == null || downloadInfo == null) {
            i = 3;
        } else if (downloadInfo.minQLauncherVersionCode != 0 && packageInfo.versionCode < downloadInfo.minQLauncherVersionCode) {
            i = 1;
        } else if (downloadInfo.maxQLauncherVersionCode != 0 && packageInfo.versionCode > downloadInfo.maxQLauncherVersionCode) {
            i = 2;
        }
        Log.d("QubeManager", "<Qube> isThemeAvaliable = " + i);
        return i;
    }

    private ak() {
    }

    public boolean b(String str) {
        return str.equals("com.tencent.qlauncher");
    }

    public boolean c(String str) {
        return !TextUtils.isEmpty(str) && str.startsWith("com.tencent.qlauncher.theme");
    }

    public boolean b(DownloadInfo downloadInfo) {
        if (downloadInfo != null && !TextUtils.isEmpty(downloadInfo.packageName)) {
            return c(downloadInfo.packageName);
        }
        Log.d("QubeManager", "<Qube> isQubeTheme = false");
        return false;
    }

    public boolean d(String str) {
        boolean z;
        List<at> g = g();
        boolean z2 = false;
        if (g != null && g.size() > 0) {
            Iterator<at> it = g.iterator();
            while (true) {
                z = z2;
                if (!it.hasNext()) {
                    break;
                } else if (it.next().b.equals(str)) {
                    z2 = true;
                } else {
                    z2 = z;
                }
            }
            z2 = z;
        }
        Log.d("QubeManager", "<Qube> isThemeInstalled :" + z2 + ", package = " + str);
        return z2;
    }

    public void a(int i) {
        String str;
        String str2;
        SQLiteDatabase sQLiteDatabase = null;
        Log.i("QubeManager", "<Qube> setAppUpdateNum update num to " + i);
        try {
            sQLiteDatabase = ExternalDbHelper.get(AstApp.i()).getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("key", "can_update_app_sum");
            contentValues.put("value", Integer.valueOf(i));
            sQLiteDatabase.replace("qube_app_update_info", null, contentValues);
            Context applicationContext = AstApp.i().getApplicationContext();
            applicationContext.getContentResolver().notifyChange(w.f754a, null);
            applicationContext.sendBroadcast(new Intent("com.tencent.android.qqdownloader.action.CAN_UPDATE_APP_SUM_CHANGED"));
            if (sQLiteDatabase != null) {
                try {
                    sQLiteDatabase.close();
                    return;
                } catch (Exception e2) {
                    e = e2;
                    str2 = "QubeManager";
                    str = "setAppUpdateNum";
                }
            } else {
                return;
            }
            Log.e(str2, str, e);
        } catch (Throwable th) {
            if (sQLiteDatabase != null) {
                try {
                    sQLiteDatabase.close();
                } catch (Exception e3) {
                    Log.e("QubeManager", "setAppUpdateNum", e3);
                }
            }
            throw th;
        }
    }

    public void a(DownloadInfo downloadInfo, boolean z, int i) {
        AppConst.TwoBtnDialogInfo b2 = b(downloadInfo, z, i);
        Message message = new Message();
        message.what = 1;
        message.obj = b2;
        d.sendMessage(message);
        if (i == 1) {
            if (z) {
                a(20430101, b, STConst.ST_DEFAULT_SLOT, 100);
            } else {
                a(20430102, b, STConst.ST_DEFAULT_SLOT, 100);
            }
        } else if (z) {
            a(20430103, b, STConst.ST_DEFAULT_SLOT, 100);
        } else {
            a(20430104, b, STConst.ST_DEFAULT_SLOT, 100);
        }
    }

    private AppConst.TwoBtnDialogInfo b(DownloadInfo downloadInfo, boolean z, int i) {
        am amVar = new am(this, i, downloadInfo, z);
        Context applicationContext = AstApp.i().getApplicationContext();
        amVar.lBtnTxtRes = applicationContext.getString(R.string.cancel);
        amVar.rBtnTxtRes = z ? applicationContext.getString(R.string.download) : applicationContext.getString(R.string.update);
        amVar.titleRes = applicationContext.getString(R.string.qube_tips_title);
        String str = downloadInfo.name;
        amVar.contentRes = z ? String.format(applicationContext.getString(R.string.qube_is_not_installed), str) : String.format(applicationContext.getString(R.string.qube_version_is_low), str);
        amVar.blockCaller = true;
        return amVar;
    }

    public void a(int i, DownloadInfo downloadInfo) {
        AppConst.OneBtnDialogInfo b2 = b(i, downloadInfo);
        Message message = new Message();
        message.what = 2;
        message.obj = b2;
        d.sendMessage(message);
    }

    private AppConst.OneBtnDialogInfo b(int i, DownloadInfo downloadInfo) {
        an anVar = new an(this, downloadInfo);
        Context applicationContext = AstApp.i().getApplicationContext();
        anVar.btnTxtRes = applicationContext.getString(R.string.qube_download_ok);
        anVar.titleRes = applicationContext.getString(R.string.qube_tips_title);
        anVar.contentRes = applicationContext.getString(i);
        anVar.blockCaller = true;
        return anVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0063, code lost:
        if (r5.isDownloadFileExist() != false) goto L_0x001c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void c(com.tencent.pangu.download.DownloadInfo r5) {
        /*
            r4 = this;
            r0 = 1013(0x3f5, float:1.42E-42)
            java.lang.String r1 = "QubeManager"
            java.lang.String r2 = "<Qube> updateDownloadBtnUI"
            android.util.Log.i(r1, r2)
            if (r5 != 0) goto L_0x0013
            java.lang.String r0 = "QubeManager"
            java.lang.String r1 = "updateDownloadBtnUI error, info = null"
            android.util.Log.e(r0, r1)
        L_0x0012:
            return
        L_0x0013:
            r1 = 0
            java.lang.String r2 = r5.packageName
            boolean r2 = r4.d(r2)
            if (r2 == 0) goto L_0x0038
        L_0x001c:
            if (r0 == 0) goto L_0x0012
            com.qq.AppService.AstApp r1 = com.qq.AppService.AstApp.i()
            com.tencent.assistant.event.EventDispatcher r1 = r1.j()
            com.qq.AppService.AstApp r2 = com.qq.AppService.AstApp.i()
            com.tencent.assistant.event.EventDispatcher r2 = r2.j()
            java.lang.String r3 = r5.downloadTicket
            android.os.Message r0 = r2.obtainMessage(r0, r3)
            r1.sendMessage(r0)
            goto L_0x0012
        L_0x0038:
            com.tencent.pangu.download.SimpleDownloadInfo$DownloadState r2 = r5.downloadState
            int[] r3 = com.tencent.pangu.manager.as.f3801a
            int r2 = r2.ordinal()
            r2 = r3[r2]
            switch(r2) {
                case 1: goto L_0x0047;
                case 2: goto L_0x0047;
                case 3: goto L_0x004a;
                case 4: goto L_0x004d;
                case 5: goto L_0x004d;
                case 6: goto L_0x0050;
                case 7: goto L_0x0053;
                case 8: goto L_0x0056;
                case 9: goto L_0x0059;
                case 10: goto L_0x0059;
                case 11: goto L_0x0059;
                default: goto L_0x0045;
            }
        L_0x0045:
            r0 = r1
            goto L_0x001c
        L_0x0047:
            r0 = 1001(0x3e9, float:1.403E-42)
            goto L_0x001c
        L_0x004a:
            r0 = 1003(0x3eb, float:1.406E-42)
            goto L_0x001c
        L_0x004d:
            r0 = 1005(0x3ed, float:1.408E-42)
            goto L_0x001c
        L_0x0050:
            r0 = 1007(0x3ef, float:1.411E-42)
            goto L_0x001c
        L_0x0053:
            r0 = 1008(0x3f0, float:1.413E-42)
            goto L_0x001c
        L_0x0056:
            r0 = 1012(0x3f4, float:1.418E-42)
            goto L_0x001c
        L_0x0059:
            boolean r2 = r5.isDownloaded()
            if (r2 == 0) goto L_0x0045
            boolean r2 = r5.isDownloadFileExist()
            if (r2 == 0) goto L_0x0045
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.pangu.manager.ak.c(com.tencent.pangu.download.DownloadInfo):void");
    }

    /* access modifiers changed from: private */
    public void j(DownloadInfo downloadInfo) {
        Log.i("QubeManager", "<Qube> downloadLastestQubeWithTheme");
        this.e = false;
        TemporaryThreadManager.get().start(new ao(this, downloadInfo));
    }

    /* access modifiers changed from: private */
    public void a(CountDownLatch countDownLatch) {
        Log.i("QubeManager", "<Qube> downloadLastestQubeOnly");
        if (!c.a()) {
            if (countDownLatch != null) {
                this.e = false;
                countDownLatch.countDown();
            }
            d.sendEmptyMessage(3);
            return;
        }
        SimpleAppModel simpleAppModel = new SimpleAppModel();
        simpleAppModel.c = "com.tencent.qlauncher";
        simpleAppModel.ac = "000116083139363434323633";
        ad adVar = new ad();
        adVar.register(new ap(this, countDownLatch));
        adVar.a(simpleAppModel);
    }

    /* access modifiers changed from: private */
    public int k(DownloadInfo downloadInfo) {
        PackageInfo packageInfo;
        LocalApkInfo c2;
        try {
            packageInfo = AstApp.i().getApplicationContext().getPackageManager().getPackageInfo("com.tencent.qlauncher", 0);
        } catch (Exception e2) {
            packageInfo = null;
        }
        if (packageInfo == null) {
            List<DownloadInfo> e3 = DownloadProxy.a().e("com.tencent.qlauncher");
            if (e3 != null && e3.size() > 0) {
                DownloadInfo downloadInfo2 = e3.get(0);
                LocalApkInfo c3 = e.c(downloadInfo2.downloadingPath);
                if (c3 != null && c3.mVersionCode >= downloadInfo.versionCode && a(downloadInfo2)) {
                    return 0;
                }
            }
            if (a(downloadInfo)) {
                return 1;
            }
        } else if (!d()) {
            List<DownloadInfo> e4 = DownloadProxy.a().e("com.tencent.qlauncher");
            if (e4 != null && e4.size() > 0) {
                DownloadInfo downloadInfo3 = e4.get(0);
                if (downloadInfo3.isSuccApkFileExist() && (c2 = e.c(downloadInfo3.downloadingPath)) != null && a(downloadInfo3) && c2.mVersionCode >= downloadInfo.versionCode) {
                    return 0;
                }
            }
            if (packageInfo.versionCode < downloadInfo.versionCode && a(downloadInfo)) {
                return 1;
            }
        }
        return 2;
    }

    public void d(DownloadInfo downloadInfo) {
        if (downloadInfo != null) {
            c(downloadInfo);
            Log.d("QubeManager", "<Qube> startQubeDownload package = " + downloadInfo.packageName);
            if (!FileUtil.isSDCardExistAndCanWrite()) {
                a((int) R.string.qube_no_sdcard_or_data_exception, downloadInfo);
            } else if (!e()) {
                SimpleDownloadInfo.DownloadState c2 = c();
                if (SimpleDownloadInfo.DownloadState.DOWNLOADING == c2 || SimpleDownloadInfo.DownloadState.QUEUING == c2 || SimpleDownloadInfo.DownloadState.INSTALLING == c2) {
                    Log.d("QubeManager", "<Qube> qlauncher is in downloading, theme package = " + downloadInfo.packageName + ", theme version = " + downloadInfo.versionCode);
                    DownloadProxy.a().c(downloadInfo);
                    return;
                }
                Log.d("QubeManager", "<Qube> qlauncher is not installed or in downloading, theme package = " + downloadInfo.packageName + ", theme version = " + downloadInfo.versionCode);
                a(downloadInfo, true, 1);
            } else if (!d()) {
                Log.d("QubeManager", "<Qube> qlauncher is unavaliable, need to update");
                a(downloadInfo, false, 1);
            } else if (i(downloadInfo) == 0) {
                Log.d("QubeManager", "<Qube> qlauncher is installed and theme is avaliable, theme package = " + downloadInfo.packageName + ", theme version = " + downloadInfo.versionCode);
                DownloadProxy.a().c(downloadInfo);
            } else {
                Log.d("QubeManager", "<Qube> qlauncher is installed and theme is unavaliable, theme package = " + downloadInfo.packageName + ", theme version = " + downloadInfo.versionCode);
                a(downloadInfo, false, 1);
            }
        }
    }

    public void e(DownloadInfo downloadInfo) {
        if (downloadInfo == null) {
            Log.e("QubeManager", "<Qube> openQubeTheme failed, package = null");
            return;
        }
        Log.i("QubeManager", "<Qube> openQubeTheme package = " + downloadInfo.packageName);
        if (!e()) {
            SimpleDownloadInfo.DownloadState c2 = c();
            if (SimpleDownloadInfo.DownloadState.DOWNLOADING == c2 || SimpleDownloadInfo.DownloadState.QUEUING == c2 || SimpleDownloadInfo.DownloadState.INSTALLING == c2) {
                a((int) R.string.qube_wait_qube_install, downloadInfo);
            } else {
                a(downloadInfo, true, 2);
            }
        } else if (!d()) {
            Log.d("QubeManager", "<Qube> qlauncher is unavaliable, need to update");
            a(downloadInfo, false, 2);
        } else if (i(downloadInfo) == 0) {
            f(downloadInfo);
            i(downloadInfo.packageName);
        } else {
            a(downloadInfo, false, 2);
        }
    }

    public void e(String str) {
        DownloadInfo downloadInfo;
        if (!TextUtils.isEmpty(str)) {
            if (e()) {
                List<DownloadInfo> e2 = DownloadProxy.a().e(str);
                if (!(e2 == null || e2.size() <= 0 || (downloadInfo = e2.get(0)) == null)) {
                    f(downloadInfo);
                }
                i(str);
                return;
            }
            h(str);
        }
    }

    private void h(String str) {
        Log.i("QubeManager", "<Qube> downloadThemeByPackage");
        SimpleAppModel simpleAppModel = new SimpleAppModel();
        simpleAppModel.c = str;
        ad adVar = new ad();
        adVar.register(new aq(this));
        adVar.a(simpleAppModel);
    }

    private void i(String str) {
        try {
            Context applicationContext = AstApp.i().getApplicationContext();
            Intent intent = new Intent();
            intent.setClassName("com.tencent.qlauncher", "com.tencent.qlauncher.home.Launcher");
            intent.putExtra("theme_packageName", str);
            intent.setFlags(268435456);
            applicationContext.startActivity(intent);
            List<DownloadInfo> e2 = DownloadProxy.a().e(str);
            if (e2 != null && e2.size() > 0) {
                for (DownloadInfo next : e2) {
                    next.downloadState = SimpleDownloadInfo.DownloadState.INSTALLED;
                    DownloadProxy.a().d(next);
                }
            }
        } catch (Exception e3) {
            Log.e("QubeManager", "openQubeThemeInner", e3);
        }
    }

    public void f(DownloadInfo downloadInfo) {
        File file;
        if (downloadInfo != null) {
            try {
                if (TextUtils.isEmpty(downloadInfo.downloadingPath)) {
                    File[] listFiles = new File(FileUtil.getAPKDir()).listFiles(new ar(this, downloadInfo));
                    if (listFiles != null && listFiles.length > 0) {
                        if (listFiles.length == 1) {
                            file = listFiles[0];
                        } else {
                            for (int i = 0; i < listFiles.length - 1; i++) {
                                if (listFiles[i].lastModified() > listFiles[i + 1].lastModified()) {
                                    File file2 = listFiles[i];
                                    listFiles[i] = listFiles[i + 1];
                                    listFiles[i + 1] = file2;
                                }
                            }
                            file = listFiles[listFiles.length - 1];
                        }
                    } else {
                        return;
                    }
                } else {
                    file = new File(downloadInfo.downloadingPath);
                }
                if (!file.exists()) {
                    Log.e("QubeManager", "<Qube> downloadQubeThemeComplete download file not exist !!!");
                    return;
                }
                String k = k();
                if (k != null) {
                    if (!new File(k).exists()) {
                        new File(k).mkdirs();
                    }
                    String str = k + File.separator + file.getName();
                    File file3 = new File(str);
                    if (!file3.exists()) {
                        file3.createNewFile();
                        if (FileUtil.copy(file.getAbsolutePath(), str)) {
                            XLog.i("QubeManager", "Theme copy succeed!");
                        } else {
                            XLog.i("QubeManager", "Theme copy failed!");
                            if (!file3.exists()) {
                                XLog.i("QubeManager", "Theme copy failed ---> file is not exist!");
                            }
                        }
                    }
                    f(downloadInfo.packageName);
                    XLog.i("QubeManager", "Qube theme download succeed");
                    XLog.i("QubeManager", "downloadInfo.filePath = " + downloadInfo.filePath);
                    HashMap hashMap = new HashMap();
                    hashMap.put("lastModifiedTime", String.valueOf(new File(downloadInfo.filePath).lastModified()));
                    hashMap.put("packageName", downloadInfo.packageName);
                    hashMap.put("versionCode", String.valueOf(downloadInfo.versionCode));
                    hashMap.put("themeFilePath", downloadInfo.filePath);
                    ay.b(hashMap);
                }
            } catch (Exception e2) {
                Log.e("QubeManager", "<Qube> downloadQubeThemeComplete", e2);
            }
        }
    }

    public void f(String str) {
        if (!TextUtils.isEmpty(str)) {
            Log.i("QubeManager", "<Qube> notifyThemeDownloadComplete notice qlauncher that: theme has download complete");
            try {
                Context applicationContext = AstApp.i().getApplicationContext();
                Intent intent = new Intent();
                intent.setAction("com.android.qlauncher.action.THEME_DOWNLOAD_COMPLETE");
                intent.putExtra("theme_packageName", str);
                intent.putExtra("theme_downloadState", "success");
                applicationContext.sendBroadcast(intent);
            } catch (Exception e2) {
                Log.e("QubeManager", "notifyThemeDownloadComplete", e2);
            }
        }
    }

    public void f() {
        SQLiteDatabaseWrapper sQLiteDatabaseWrapper = null;
        try {
            sQLiteDatabaseWrapper = ExternalDbHelper.get(AstApp.i()).getWritableDatabaseWrapper();
            sQLiteDatabaseWrapper.delete("qube_theme_download_status", "state=?", new String[]{SimpleDownloadInfo.DownloadState.SUCC.name()});
        } catch (Throwable th) {
            if (sQLiteDatabaseWrapper != null) {
                sQLiteDatabaseWrapper.close();
            }
        }
    }

    public void g(DownloadInfo downloadInfo) {
        String str;
        String str2;
        SQLiteDatabase sQLiteDatabase = null;
        if (downloadInfo != null && downloadInfo.packageName != null) {
            Log.d("QubeManager", "<Qube> updateThemeDownloadState, package = " + downloadInfo.packageName + ", downloadState = " + downloadInfo.downloadState);
            try {
                SQLiteDatabase writableDatabase = ExternalDbHelper.get(AstApp.i()).getWritableDatabase();
                ContentValues contentValues = new ContentValues();
                contentValues.put("package", downloadInfo.packageName);
                contentValues.put("state", downloadInfo.downloadState.name());
                if (downloadInfo.downloadState == SimpleDownloadInfo.DownloadState.SUCC || downloadInfo.downloadState == SimpleDownloadInfo.DownloadState.COMPLETE) {
                    contentValues.put("state", SimpleDownloadInfo.DownloadState.INSTALLED.name());
                }
                writableDatabase.replace("qube_theme_download_status", null, contentValues);
                AstApp.i().getContentResolver().notifyChange(v.f753a, null);
                if (writableDatabase != null) {
                    try {
                        writableDatabase.close();
                        return;
                    } catch (Exception e2) {
                        e = e2;
                        str = "QubeManager";
                        str2 = "notifyThemeDownloadComplete";
                    }
                } else {
                    return;
                }
            } catch (Throwable th) {
                if (sQLiteDatabase != null) {
                    try {
                        sQLiteDatabase.close();
                    } catch (Exception e3) {
                        Log.e("QubeManager", "notifyThemeDownloadComplete", e3);
                    }
                }
                throw th;
            }
        } else {
            return;
        }
        Log.e(str, str2, e);
    }

    public void h(DownloadInfo downloadInfo) {
        if (downloadInfo != null && downloadInfo.packageName != null) {
            Log.d("QubeManager", "<Qube> deleteThemeDownloadState, package = " + downloadInfo.packageName + ", downloadState = " + downloadInfo.downloadState);
            SQLiteDatabaseWrapper sQLiteDatabaseWrapper = null;
            try {
                sQLiteDatabaseWrapper = ExternalDbHelper.get(AstApp.i()).getWritableDatabaseWrapper();
                sQLiteDatabaseWrapper.delete("qube_theme_download_status", "package=?", new String[]{downloadInfo.packageName});
                if (sQLiteDatabaseWrapper == null) {
                    return;
                }
            } catch (Throwable th) {
                if (sQLiteDatabaseWrapper != null) {
                    sQLiteDatabaseWrapper.close();
                }
                throw th;
            }
            sQLiteDatabaseWrapper.close();
        }
    }

    private String k() {
        if (FileUtil.isSDCardExistAndCanWrite()) {
            return Environment.getExternalStorageDirectory().getPath() + "/tencent/qlauncher_themes";
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x008b A[SYNTHETIC, Splitter:B:28:0x008b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<com.tencent.pangu.manager.at> g() {
        /*
            r8 = this;
            r6 = 0
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>()
            com.qq.AppService.AstApp r0 = com.qq.AppService.AstApp.i()     // Catch:{ Throwable -> 0x0077, all -> 0x0088 }
            android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ Throwable -> 0x0077, all -> 0x0088 }
            java.lang.String r1 = "content://com.tencent.qlauncher.theme.db.ThemeDBProvider/guest_qqdownloader"
            android.net.Uri r1 = android.net.Uri.parse(r1)     // Catch:{ Throwable -> 0x0077, all -> 0x0088 }
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Throwable -> 0x0077, all -> 0x0088 }
            if (r0 == 0) goto L_0x0071
            boolean r1 = r0.moveToFirst()     // Catch:{ Throwable -> 0x0095 }
            if (r1 == 0) goto L_0x0071
            int r1 = r0.getColumnCount()     // Catch:{ Throwable -> 0x0095 }
        L_0x0028:
            com.tencent.pangu.manager.at r2 = new com.tencent.pangu.manager.at     // Catch:{ Throwable -> 0x0095 }
            r2.<init>(r8)     // Catch:{ Throwable -> 0x0095 }
            r3 = 0
            int r3 = r0.getInt(r3)     // Catch:{ Throwable -> 0x0095 }
            r2.f3802a = r3     // Catch:{ Throwable -> 0x0095 }
            r3 = 1
            java.lang.String r3 = r0.getString(r3)     // Catch:{ Throwable -> 0x0095 }
            r2.b = r3     // Catch:{ Throwable -> 0x0095 }
            r3 = 2
            int r3 = r0.getInt(r3)     // Catch:{ Throwable -> 0x0095 }
            r2.c = r3     // Catch:{ Throwable -> 0x0095 }
            r3 = 3
            java.lang.String r3 = r0.getString(r3)     // Catch:{ Throwable -> 0x0095 }
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r3)     // Catch:{ Throwable -> 0x0095 }
            boolean r3 = r3.booleanValue()     // Catch:{ Throwable -> 0x0095 }
            r2.d = r3     // Catch:{ Throwable -> 0x0095 }
            r3 = 4
            if (r1 <= r3) goto L_0x0068
            java.lang.String r3 = "theme_used"
            int r3 = r0.getColumnIndex(r3)     // Catch:{ Throwable -> 0x0095 }
            java.lang.String r3 = r0.getString(r3)     // Catch:{ Throwable -> 0x0095 }
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r3)     // Catch:{ Throwable -> 0x0095 }
            boolean r3 = r3.booleanValue()     // Catch:{ Throwable -> 0x0095 }
            r2.e = r3     // Catch:{ Throwable -> 0x0095 }
        L_0x0068:
            r7.add(r2)     // Catch:{ Throwable -> 0x0095 }
            boolean r2 = r0.moveToNext()     // Catch:{ Throwable -> 0x0095 }
            if (r2 != 0) goto L_0x0028
        L_0x0071:
            if (r0 == 0) goto L_0x0076
            r0.close()     // Catch:{ Exception -> 0x0086 }
        L_0x0076:
            return r7
        L_0x0077:
            r0 = move-exception
            r0 = r6
        L_0x0079:
            java.lang.String r1 = "QubeManager"
            java.lang.String r2 = "<Qube> getAllInstalledThemes read qlauncher provider error"
            android.util.Log.e(r1, r2)     // Catch:{ all -> 0x0091 }
            if (r0 == 0) goto L_0x0076
            r0.close()     // Catch:{ Exception -> 0x0086 }
            goto L_0x0076
        L_0x0086:
            r0 = move-exception
            goto L_0x0076
        L_0x0088:
            r0 = move-exception
        L_0x0089:
            if (r6 == 0) goto L_0x008e
            r6.close()     // Catch:{ Exception -> 0x008f }
        L_0x008e:
            throw r0
        L_0x008f:
            r1 = move-exception
            goto L_0x008e
        L_0x0091:
            r1 = move-exception
            r6 = r0
            r0 = r1
            goto L_0x0089
        L_0x0095:
            r1 = move-exception
            goto L_0x0079
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.pangu.manager.ak.g():java.util.List");
    }

    public static final String h() {
        List<at> g = a().g();
        if (g == null) {
            return null;
        }
        for (at next : g) {
            if (next.e) {
                return next.b;
            }
        }
        return null;
    }

    public void b(int i) {
        b = i;
    }

    /* access modifiers changed from: private */
    public void a(int i, int i2, String str, int i3) {
        Log.i("QubeManager", "<Qube> report pageId = " + i + ", prePageId = " + i2 + ", slotId = " + str + ", action = " + i3 + ", extraDataType = 0, para = null");
        l.a(new STInfoV2(i, str, i2, STConst.ST_DEFAULT_SLOT, i3));
    }
}
