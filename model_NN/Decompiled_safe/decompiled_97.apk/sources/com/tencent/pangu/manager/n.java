package com.tencent.pangu.manager;

import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;
import com.tencent.pangu.download.a;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

/* compiled from: ProGuard */
class n implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadProxy f3837a;

    n(DownloadProxy downloadProxy) {
        this.f3837a = downloadProxy;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.download.SimpleDownloadInfo$DownloadType, boolean):java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>
     arg types: [com.tencent.pangu.download.SimpleDownloadInfo$DownloadType, int]
     candidates:
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.manager.DownloadProxy, android.app.Dialog):android.app.Dialog
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.manager.DownloadProxy, com.tencent.assistant.net.APN):com.tencent.assistant.net.APN
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.manager.DownloadProxy, com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager):com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.assistant.localres.model.LocalApkInfo, java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>):java.lang.String
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.download.DownloadInfo, com.tencent.pangu.download.SimpleDownloadInfo$DownloadState):void
      com.tencent.pangu.manager.DownloadProxy.a(java.lang.String, int):com.tencent.pangu.download.DownloadInfo
      com.tencent.pangu.manager.DownloadProxy.a(java.lang.String, boolean):void
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.download.SimpleDownloadInfo$DownloadType, boolean):java.util.ArrayList<com.tencent.pangu.download.DownloadInfo> */
    public void run() {
        ArrayList<DownloadInfo> a2 = this.f3837a.a(SimpleDownloadInfo.DownloadType.APK, true);
        Collections.sort(a2);
        Iterator<DownloadInfo> it = a2.iterator();
        while (it.hasNext()) {
            DownloadInfo next = it.next();
            if (next.downloadState == SimpleDownloadInfo.DownloadState.WAITTING_FOR_WIFI || (next.downloadState == SimpleDownloadInfo.DownloadState.FAIL && next.errorCode != -10)) {
                a.a().a(next);
            }
        }
    }
}
