package com.tencent.pangu.manager;

import com.qq.AppService.AstApp;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.callback.h;
import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.AppSimpleDetail;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;
import java.util.List;

/* compiled from: ProGuard */
class bi implements h {

    /* renamed from: a  reason: collision with root package name */
    String f3818a = null;
    final /* synthetic */ au b;
    private boolean c = true;

    bi(au auVar) {
        this.b = auVar;
    }

    public void a(boolean z) {
        this.c = z;
    }

    public void a(String str) {
        this.f3818a = str;
    }

    private boolean a(DownloadInfo downloadInfo) {
        SimpleDownloadInfo.DownloadState downloadState;
        if (au.j) {
            return this.b.a(downloadInfo.name, this.b.u);
        }
        try {
            if (this.b.d.getPackageInfo(this.b.u.g, 0) != null) {
                XLog.e("RecommendDownloadManager", "<install> 推荐的应用<" + downloadInfo.packageName + "> 已安装，不再弹出安装推荐");
                return false;
            }
        } catch (Exception e) {
        }
        List<DownloadInfo> e2 = DownloadProxy.a().e(this.b.u.g);
        if (e2 == null || e2.size() <= 0 || (SimpleDownloadInfo.DownloadState.DOWNLOADING != (downloadState = e2.get(0).downloadState) && SimpleDownloadInfo.DownloadState.QUEUING != downloadState && SimpleDownloadInfo.DownloadState.INSTALLING != downloadState)) {
            return this.b.a(downloadInfo.name, this.b.u);
        }
        XLog.e("RecommendDownloadManager", "<install> 推荐的应用<" + downloadInfo.packageName + "> 正在下载中，不再弹出安装推荐");
        return false;
    }

    public void onGetAppInfoSuccess(int i, int i2, AppSimpleDetail appSimpleDetail) {
        DownloadInfo downloadInfo;
        if (this.b.u != null && this.f3818a != null) {
            SimpleAppModel a2 = k.a(appSimpleDetail);
            DownloadInfo a3 = DownloadProxy.a().a(a2);
            if (a3 == null) {
                downloadInfo = DownloadInfo.createDownloadInfo(a2, new StatInfo(a2.b, AstApp.m() != null ? AstApp.m().f() : STConst.ST_PAGE_DOWNLOAD, 0, null, 0));
            } else {
                downloadInfo = a3;
            }
            XLog.d("RecommendDownloadManager", "<install> 服务器拉取到的DownloadInfo = " + downloadInfo.toString());
            boolean a4 = this.b.u.a(downloadInfo.appId, downloadInfo.packageName, downloadInfo.categoryId);
            XLog.d("RecommendDownloadManager", "<install> " + this.f3818a + (a4 ? "<成功命中>安装推荐逻辑" : "<未命中>安装推荐逻辑"));
            boolean z = false;
            if (a4 && !au.k) {
                z = a(downloadInfo);
            }
            if (!this.c || z) {
                XLog.d("RecommendDownloadManager", "<install> 不尝试引导打开, isNeedGuideOpen = " + this.c + ", isShowRecommendTip = " + z);
            } else {
                this.b.b(downloadInfo);
            }
        }
    }

    public void onGetAppInfoFail(int i, int i2) {
        DownloadInfo a2 = this.b.e(this.f3818a);
        XLog.e("RecommendDownloadManager", "<install> 服务器拉刚才已安装应用信息<失败> !!, local donwloadinfo = " + a2);
        if (a2 != null) {
            this.b.b(a2);
        }
    }
}
