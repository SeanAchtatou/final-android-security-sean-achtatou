package com.tencent.pangu.a;

import android.content.Context;
import android.view.ViewStub;
import com.tencent.assistant.protocol.jce.NpcCfg;

/* compiled from: ProGuard */
public class b {

    /* renamed from: a  reason: collision with root package name */
    a f3290a = null;

    public void a(Context context, ViewStub viewStub, boolean z) {
        NpcCfg b;
        if (z || (b = c.a().b()) == null) {
            this.f3290a = new h(context, viewStub);
            return;
        }
        this.f3290a = new d(context, viewStub);
        ((d) this.f3290a).a(b);
    }

    public void a(boolean z) {
        if (this.f3290a == null) {
            return;
        }
        if (this.f3290a instanceof h) {
            this.f3290a.a(z);
        } else {
            this.f3290a.a(false);
        }
    }

    public a a() {
        return this.f3290a;
    }
}
