package com.tencent.pangu.download;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.event.EventDispatcher;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.module.update.j;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.r;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.cloud.a.f;
import com.tencent.connect.common.Constants;
import com.tencent.downloadsdk.DownloadManager;
import com.tencent.mm.sdk.constants.ConstantsAPI;
import com.tencent.pangu.download.SimpleDownloadInfo;
import com.tencent.pangu.link.b;
import com.tencent.pangu.manager.DownloadProxy;
import com.tencent.pangu.manager.ak;
import com.tencent.pangu.utils.installuninstall.p;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: ProGuard */
public class a implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    static a f3754a;
    private static AstApp c = AstApp.i();
    /* access modifiers changed from: private */
    public static EventDispatcher d = c.j();
    private static ConcurrentHashMap<String, String> e = new ConcurrentHashMap<>();
    public ConcurrentHashMap<String, j> b;
    private Handler f;
    /* access modifiers changed from: private */
    public boolean g;

    public static synchronized a a() {
        a aVar;
        synchronized (a.class) {
            if (f3754a == null) {
                f3754a = new a();
            }
            aVar = f3754a;
        }
        return aVar;
    }

    private a() {
        this.f = null;
        this.b = new ConcurrentHashMap<>();
        this.f = k.a();
        c.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC, this);
        c.k().addUIEventListener(1010, this);
        c.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_INSTALL, this);
        c.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UNINSTALL, this);
    }

    public boolean a(String str) {
        DownloadInfo d2 = DownloadProxy.a().d(str);
        if (d2 == null) {
            return false;
        }
        DownloadManager.a().d(d2.getDownloadSubType(), str);
        if (d2.fileType == SimpleDownloadInfo.DownloadType.APK) {
            d2.sllUpdate = 0;
        }
        d2.downloadState = SimpleDownloadInfo.DownloadState.FAIL;
        DownloadProxy.a().d(d2);
        if (d2.fileType == SimpleDownloadInfo.DownloadType.APK && d2.isUpdate == 1) {
            d.sendMessage(d.obtainMessage(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_UPDATE_START));
        }
        DownloadProxy.a().c(d2);
        return true;
    }

    public boolean a(String str, boolean z) {
        DownloadInfo d2;
        if (!TextUtils.isEmpty(str) && (d2 = DownloadProxy.a().d(str)) != null) {
            if (ak.a().b(d2)) {
                ak.a().e(d2);
                return true;
            }
            String filePath = d2.getFilePath();
            if (!TextUtils.isEmpty(filePath)) {
                if (!new File(filePath).exists()) {
                    d2.downloadState = SimpleDownloadInfo.DownloadState.FAIL;
                    DownloadProxy.a().d(d2);
                    d.sendMessage(d.obtainMessage(1007, d2.downloadTicket));
                } else {
                    p.a().a(d2, z);
                    return true;
                }
            }
        }
        return false;
    }

    public boolean a(DownloadInfo downloadInfo, boolean z) {
        if (downloadInfo != null) {
            if (ak.a().b(downloadInfo)) {
                ak.a().e(downloadInfo);
            } else {
                if (downloadInfo.checkCurrentDownloadSucc() == 0) {
                    d.sendMessage(d.obtainMessage(1007, downloadInfo.downloadTicket));
                }
                p.a().a(downloadInfo, z);
            }
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.download.a.a(com.tencent.assistant.model.SimpleAppModel, com.tencent.assistantv2.st.model.StatInfo, boolean):void
     arg types: [com.tencent.assistant.model.SimpleAppModel, com.tencent.assistantv2.st.model.StatInfo, int]
     candidates:
      com.tencent.pangu.download.a.a(com.tencent.pangu.download.a, java.util.List, com.tencent.assistantv2.st.model.StatInfo):void
      com.tencent.pangu.download.a.a(com.tencent.assistant.model.SimpleAppModel, com.tencent.assistantv2.st.model.StatInfo, boolean):void */
    public void a(SimpleAppModel simpleAppModel, StatInfo statInfo) {
        a(simpleAppModel, statInfo, false);
    }

    public void a(SimpleAppModel simpleAppModel, StatInfo statInfo, boolean z) {
        this.f.post(new b(this, simpleAppModel, statInfo, z));
    }

    public void a(DownloadInfo downloadInfo) {
        if (a(downloadInfo.downloadTicket, 1)) {
            a(downloadInfo, SimpleDownloadInfo.UIType.NORMAL);
        }
    }

    public void a(DownloadInfo downloadInfo, SimpleDownloadInfo.UIType uIType) {
        this.f.post(new c(this, downloadInfo, uIType));
    }

    public void b(String str) {
        if (a(str, 2)) {
            this.f.post(new d(this, str));
        }
    }

    public void b(DownloadInfo downloadInfo) {
        if (a(downloadInfo.downloadTicket, 1)) {
            b(downloadInfo, SimpleDownloadInfo.UIType.NORMAL);
        }
    }

    public void b(DownloadInfo downloadInfo, SimpleDownloadInfo.UIType uIType) {
        this.f.post(new e(this, downloadInfo, uIType));
    }

    public void c(DownloadInfo downloadInfo) {
        if (downloadInfo != null && !TextUtils.isEmpty(downloadInfo.packageName)) {
            if (ak.a().b(downloadInfo)) {
                ak.a().e(downloadInfo);
            } else if (downloadInfo == null || downloadInfo.applinkInfo == null || !downloadInfo.applinkInfo.b()) {
                d(downloadInfo.packageName);
            }
        }
    }

    public void c(String str) {
        if (ak.a().c(str)) {
            ak.a().e(str);
            return;
        }
        DownloadInfo d2 = DownloadProxy.a().d(str);
        if (d2 == null || d2.applinkInfo == null || !d2.applinkInfo.b()) {
            d(str);
        }
    }

    public void a(String str, f fVar) {
        if (ak.a().c(str)) {
            ak.a().e(str);
        } else if (fVar == null || !fVar.b()) {
            d(str);
        }
    }

    public void a(String str, String str2) {
        if (ak.a().c(str)) {
            ak.a().e(str);
            return;
        }
        if (!TextUtils.isEmpty(str2)) {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str2));
            if (b.a(AstApp.i(), intent)) {
                intent.setFlags(268435456);
                AstApp.i().startActivity(intent);
                return;
            }
        }
        d(str);
    }

    private void d(String str) {
        Bundle bundle = new Bundle();
        bundle.putString(ConstantsAPI.Token.WX_TOKEN_PLATFORMID_KEY, "myapp_m");
        bundle.putInt("dest_view", 7798785);
        if (!r.a(str, bundle)) {
            ah.a().post(new f(this));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.download.a.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.pangu.download.a.a(com.tencent.pangu.download.a, java.util.List):java.util.List
      com.tencent.pangu.download.a.a(java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>, boolean):void
      com.tencent.pangu.download.a.a(long, long):boolean
      com.tencent.pangu.download.a.a(com.tencent.pangu.download.a, boolean):boolean
      com.tencent.pangu.download.a.a(java.lang.String, int):boolean
      com.tencent.pangu.download.a.a(java.util.List<com.tencent.assistant.model.SimpleAppModel>, com.tencent.assistantv2.st.model.StatInfo):int
      com.tencent.pangu.download.a.a(com.tencent.assistant.model.SimpleAppModel, com.tencent.assistantv2.st.model.StatInfo):void
      com.tencent.pangu.download.a.a(com.tencent.pangu.download.DownloadInfo, com.tencent.pangu.download.SimpleDownloadInfo$UIType):void
      com.tencent.pangu.download.a.a(java.lang.String, com.tencent.cloud.a.f):void
      com.tencent.pangu.download.a.a(java.lang.String, java.lang.String):void
      com.tencent.pangu.download.a.a(com.tencent.pangu.download.DownloadInfo, boolean):boolean
      com.tencent.pangu.download.a.a(java.lang.String, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.download.a.a(com.tencent.pangu.download.DownloadInfo, boolean):boolean
     arg types: [com.tencent.pangu.download.DownloadInfo, int]
     candidates:
      com.tencent.pangu.download.a.a(com.tencent.pangu.download.a, java.util.List):java.util.List
      com.tencent.pangu.download.a.a(java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>, boolean):void
      com.tencent.pangu.download.a.a(long, long):boolean
      com.tencent.pangu.download.a.a(com.tencent.pangu.download.a, boolean):boolean
      com.tencent.pangu.download.a.a(java.lang.String, int):boolean
      com.tencent.pangu.download.a.a(java.util.List<com.tencent.assistant.model.SimpleAppModel>, com.tencent.assistantv2.st.model.StatInfo):int
      com.tencent.pangu.download.a.a(com.tencent.assistant.model.SimpleAppModel, com.tencent.assistantv2.st.model.StatInfo):void
      com.tencent.pangu.download.a.a(com.tencent.pangu.download.DownloadInfo, com.tencent.pangu.download.SimpleDownloadInfo$UIType):void
      com.tencent.pangu.download.a.a(java.lang.String, com.tencent.cloud.a.f):void
      com.tencent.pangu.download.a.a(java.lang.String, java.lang.String):void
      com.tencent.pangu.download.a.a(java.lang.String, boolean):boolean
      com.tencent.pangu.download.a.a(com.tencent.pangu.download.DownloadInfo, boolean):boolean */
    public void d(DownloadInfo downloadInfo) {
        if (downloadInfo != null && downloadInfo.fileType == SimpleDownloadInfo.DownloadType.APK) {
            if (DownloadProxy.a().d(downloadInfo.downloadTicket) != null) {
                a(downloadInfo.downloadTicket, false);
            } else {
                a(downloadInfo, false);
            }
        }
    }

    public void handleUIEvent(Message message) {
        String str = Constants.STR_EMPTY;
        if (message.obj instanceof String) {
            str = (String) message.obj;
        }
        if (!TextUtils.isEmpty(str)) {
            switch (message.what) {
                case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC:
                    DownloadInfo d2 = DownloadProxy.a().d(str);
                    if (ak.a().b(d2)) {
                        d2.downloadState = SimpleDownloadInfo.DownloadState.INSTALLED;
                        DownloadProxy.a().d(d2);
                        ak.a().c(d2);
                        TemporaryThreadManager.get().start(new h(this, d2));
                        return;
                    } else if (d2 == null) {
                        return;
                    } else {
                        if ((d2.isUiTypeWiseAppUpdateDownload() && (TextUtils.isEmpty(d2.packageName) || !d2.packageName.equals(AstApp.i().getPackageName()))) || d2.isUiTypeWiseBookingDownload() || !d2.isUiTypeWiseDownload() || d2.isUiTypeWiseSubscribtionDownloadAutoInstall()) {
                            if (!d2.autoInstall || !d2.isUiTypeWiseAppUpdateDownload() || !e(d2)) {
                                a().a(d2.downloadTicket, d2.autoInstall);
                                return;
                            } else {
                                XLog.d("WiseDownload", "ingore by user, :" + d2.name + " " + d2.packageName);
                                return;
                            }
                        } else {
                            return;
                        }
                    }
                case 1007:
                case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_QUEUING:
                case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE:
                default:
                    return;
                case 1010:
                    TemporaryThreadManager.get().start(new g(this, str));
                    return;
                case EventDispatcherEnum.UI_EVENT_APP_INSTALL:
                    String str2 = (String) message.obj;
                    try {
                        if (e.containsValue(str2) && (r2 = e.keySet().iterator()) != null) {
                            for (String str3 : e.keySet()) {
                                String str4 = e.get(str3);
                                if (str2.equals(e.get(str4))) {
                                    e.remove(str4);
                                    return;
                                }
                            }
                            return;
                        }
                        return;
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        return;
                    }
            }
        }
    }

    private boolean e(DownloadInfo downloadInfo) {
        Set<com.tencent.assistant.module.update.r> e2 = j.b().e();
        if (e2 == null) {
            return false;
        }
        com.tencent.assistant.module.update.r rVar = new com.tencent.assistant.module.update.r();
        rVar.a(downloadInfo.packageName, downloadInfo.versionName, downloadInfo.versionCode, false);
        if (e2.contains(rVar)) {
            return true;
        }
        return false;
    }

    public int a(List<SimpleAppModel> list, StatInfo statInfo) {
        if (list == null || list.isEmpty()) {
            return -1;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(list);
        if (this.g) {
            return 1;
        }
        this.g = true;
        TemporaryThreadManager.get().start(new i(this, arrayList, statInfo));
        return 0;
    }

    /* access modifiers changed from: private */
    public int c() {
        return 0;
    }

    /* access modifiers changed from: private */
    public List<SimpleAppModel> a(List<SimpleAppModel> list) {
        DownloadInfo downloadInfo;
        ArrayList arrayList = new ArrayList();
        ApkResourceManager instance = ApkResourceManager.getInstance();
        j b2 = j.b();
        DownloadProxy a2 = DownloadProxy.a();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= list.size()) {
                return arrayList;
            }
            SimpleAppModel simpleAppModel = list.get(i2);
            DownloadInfo a3 = a2.a(simpleAppModel);
            if (a3 != null) {
                AppConst.AppState b3 = k.b(a3);
                if (AppConst.AppState.PAUSED == b3) {
                    if (a3 == null || !a3.needReCreateInfo(simpleAppModel)) {
                        downloadInfo = a3;
                    } else {
                        DownloadProxy.a().b(a3.downloadTicket);
                        downloadInfo = DownloadInfo.createDownloadInfo(simpleAppModel, null);
                    }
                    b(downloadInfo);
                } else if (AppConst.AppState.FAIL == b3) {
                    if (a3 != null && a3.needReCreateInfo(simpleAppModel)) {
                        DownloadProxy.a().b(a3.downloadTicket);
                        a3 = DownloadInfo.createDownloadInfo(simpleAppModel, null);
                    }
                    a(a3);
                } else if (AppConst.AppState.DOWNLOADED == b3) {
                    d(a3);
                }
            } else if (instance.getLocalApkInfo(simpleAppModel.c) == null || b2.b(simpleAppModel.c)) {
                arrayList.add(simpleAppModel);
            }
            i = i2 + 1;
        }
    }

    /* access modifiers changed from: private */
    public void b(List<SimpleAppModel> list, StatInfo statInfo) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < list.size()) {
                SimpleAppModel simpleAppModel = list.get(i2);
                if (simpleAppModel != null) {
                    StatInfo statInfo2 = new StatInfo(statInfo);
                    statInfo2.recommendId = simpleAppModel.y;
                    a(simpleAppModel, statInfo2);
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public static void a(ArrayList<DownloadInfo> arrayList, boolean z) {
        if (arrayList != null && !arrayList.isEmpty()) {
            p.a().a(arrayList, z);
        }
    }

    private boolean a(String str, int i) {
        j jVar = new j(this, str, i);
        j jVar2 = null;
        if (this.b.containsKey(str)) {
            jVar2 = this.b.get(str);
        }
        if (jVar2 == null || !jVar2.equals(jVar)) {
            this.b.put(str, jVar);
            return true;
        }
        try {
            if (a(jVar.a(), this.b.get(str).a())) {
                this.b.remove(str);
                return true;
            }
        } catch (Exception e2) {
        }
        return false;
    }

    private boolean a(long j, long j2) {
        return j - j2 >= 500;
    }
}
