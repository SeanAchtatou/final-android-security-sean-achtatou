package com.tencent.pangu.graphic;

import android.graphics.Canvas;
import android.graphics.drawable.AnimationDrawable;
import com.tencent.assistant.utils.ah;

/* compiled from: ProGuard */
public class a extends AnimationDrawable {

    /* renamed from: a  reason: collision with root package name */
    private int f3768a = 0;
    private boolean b = false;
    private int c = -1;
    private int d = 0;

    public void a(int i) {
        this.f3768a = i;
    }

    public void b(int i) {
        this.c = i;
    }

    public void draw(Canvas canvas) {
        if (!this.b) {
            super.draw(canvas);
        }
    }

    public boolean selectDrawable(int i) {
        boolean selectDrawable = super.selectDrawable(i);
        if (i != 0 && i == getNumberOfFrames() - 1 && this.c > 0) {
            this.d++;
            if (this.d > this.c - 1) {
                a();
            }
        }
        return selectDrawable;
    }

    private void a() {
        ah.a().postDelayed(new b(this), 0);
    }
}
