package com.tencent.pangu.module;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.utils.XLog;
import com.tencent.pangu.module.a.f;
import java.util.ArrayList;

/* compiled from: ProGuard */
class ag implements CallbackHelper.Caller<f> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ boolean f3898a;
    final /* synthetic */ int b;
    final /* synthetic */ ArrayList c;
    final /* synthetic */ ad d;

    ag(ad adVar, boolean z, int i, ArrayList arrayList) {
        this.d = adVar;
        this.f3898a = z;
        this.b = i;
        this.c = arrayList;
    }

    /* renamed from: a */
    public void call(f fVar) {
        XLog.d("GetHomeEngine", "to caller has next:" + this.f3898a + ",pagerContext:" + this.d.j);
        fVar.a(this.b, 0, this.f3898a, this.d.j, false, null, null, 0, this.c, this.d.g);
    }
}
