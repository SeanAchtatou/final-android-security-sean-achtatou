package com.tencent.pangu.module;

import com.tencent.assistant.module.callback.CallbackHelper;
import com.tencent.assistant.utils.XLog;
import com.tencent.pangu.module.a.f;
import java.util.ArrayList;

/* compiled from: ProGuard */
class ah implements CallbackHelper.Caller<f> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ar f3899a;
    final /* synthetic */ int b;
    final /* synthetic */ boolean c;
    final /* synthetic */ ArrayList d;
    final /* synthetic */ ad e;

    ah(ad adVar, ar arVar, int i, boolean z, ArrayList arrayList) {
        this.e = adVar;
        this.f3899a = arVar;
        this.b = i;
        this.c = z;
        this.d = arrayList;
    }

    /* renamed from: a */
    public void call(f fVar) {
        XLog.d("GetHomeEngine", "to caller has next:" + this.f3899a.c + ",pagerContext:" + this.f3899a);
        fVar.a(this.b, 0, this.f3899a.c, this.f3899a, this.c, this.e.c, this.e.e, this.e.f, this.d, this.e.g);
    }
}
