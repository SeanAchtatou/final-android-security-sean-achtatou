package com.tencent.pangu.module.wisedownload;

import com.tencent.assistant.module.update.t;
import com.tencent.assistant.protocol.jce.AutoDownloadInfo;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;
import com.tencent.pangu.manager.SelfUpdateManager;
import com.tencent.pangu.module.wisedownload.condition.c;
import com.tencent.pangu.module.wisedownload.condition.k;
import java.io.File;
import java.util.List;

/* compiled from: ProGuard */
public class g extends b {
    public g() {
        a();
    }

    public void a() {
        this.d = new k(this);
    }

    /* access modifiers changed from: protected */
    public void c() {
        this.b = new c(this);
    }

    public boolean e() {
        return this.e;
    }

    public boolean b() {
        List<AutoDownloadInfo> g;
        if (k() || (g = t.a().g()) == null || g.isEmpty()) {
            return false;
        }
        AutoDownloadInfo autoDownloadInfo = g.get(0);
        DownloadInfo a2 = DownloadProxy.a().a(autoDownloadInfo.f1166a, autoDownloadInfo.d);
        if (a2 == null || a2.downloadState != SimpleDownloadInfo.DownloadState.SUCC) {
            return true;
        }
        return !new File(a2.getFilePath()).exists();
    }

    private boolean k() {
        if (SelfUpdateManager.a().d() == null) {
            return false;
        }
        if (SelfUpdateManager.a().d().z == 1) {
            return true;
        }
        return false;
    }

    public void a(p pVar) {
        boolean z;
        boolean z2 = false;
        if (pVar != null) {
            boolean e = e();
            if (this.d == null || !(this.d instanceof k)) {
                z = false;
            } else {
                k kVar = (k) this.d;
                z = kVar.b();
                z2 = kVar.h();
            }
            pVar.a(e, z, z2);
        }
    }
}
