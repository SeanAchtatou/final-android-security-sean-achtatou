package com.tencent.pangu.module;

import java.util.Arrays;

/* compiled from: ProGuard */
public class ar {

    /* renamed from: a  reason: collision with root package name */
    public byte[] f3909a = new byte[0];
    public int b;
    public boolean c;

    public void a(int i) {
        this.b += i;
    }

    public String toString() {
        return "PagerContext{pageContext=" + Arrays.toString(this.f3909a) + ", smartCardIndex=" + this.b + ", hasNext=" + this.c + '}';
    }
}
