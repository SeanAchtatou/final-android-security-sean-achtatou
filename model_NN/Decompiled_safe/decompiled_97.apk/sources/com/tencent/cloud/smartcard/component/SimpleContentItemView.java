package com.tencent.cloud.smartcard.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.protocol.jce.ContentAggregationSimpleItem;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
public class SimpleContentItemView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private TXImageView f2324a;
    private TextView b;
    private TXImageView c;
    private TextView d;
    private View e;

    public SimpleContentItemView(Context context) {
        this(context, null);
    }

    public SimpleContentItemView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    private void a() {
        LayoutInflater.from(getContext()).inflate((int) R.layout.smartcard_collection_item_layout, this);
        this.f2324a = (TXImageView) findViewById(R.id.img);
        this.b = (TextView) findViewById(R.id.title);
        this.c = (TXImageView) findViewById(R.id.icon);
        this.d = (TextView) findViewById(R.id.app_name);
        this.e = findViewById(R.id.cut_line);
        setBackgroundResource(R.drawable.smartcard_vertical_item_selector);
        int a2 = by.a(getContext(), 8.0f);
        setPadding(a2, 0, a2, 0);
    }

    public void a(STInfoV2 sTInfoV2, ContentAggregationSimpleItem contentAggregationSimpleItem, boolean z) {
        this.f2324a.updateImageView(contentAggregationSimpleItem.f1210a, -1, TXImageView.TXImageViewType.NETWORK_IMAGE_MIDDLE);
        this.b.setText(contentAggregationSimpleItem.b);
        this.c.updateImageView(contentAggregationSimpleItem.c, -1, TXImageView.TXImageViewType.NETWORK_IMAGE_MIDDLE);
        this.d.setText(contentAggregationSimpleItem.d);
        if (z) {
            this.e.setVisibility(0);
        } else {
            this.e.setVisibility(4);
        }
        setOnClickListener(new c(this, contentAggregationSimpleItem, sTInfoV2));
    }
}
