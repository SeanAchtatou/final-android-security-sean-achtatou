package com.tencent.cloud.smartcard.b;

import android.text.TextUtils;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.ContentAggregationComplexItem;

/* compiled from: ProGuard */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public ContentAggregationComplexItem f2322a;
    public SimpleAppModel b;

    public a(ContentAggregationComplexItem contentAggregationComplexItem) {
        this.f2322a = contentAggregationComplexItem;
        if (!(contentAggregationComplexItem.e == null || contentAggregationComplexItem.e.f == null || contentAggregationComplexItem.e.f.p <= 0)) {
            this.b = k.a(contentAggregationComplexItem.e);
        }
        if (this.b != null && !TextUtils.isEmpty(contentAggregationComplexItem.d)) {
            this.b.d = contentAggregationComplexItem.d;
        }
        contentAggregationComplexItem.e = null;
    }
}
