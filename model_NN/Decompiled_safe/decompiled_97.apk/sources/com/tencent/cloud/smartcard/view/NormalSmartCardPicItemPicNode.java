package com.tencent.cloud.smartcard.view;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.protocol.jce.SmartCardPicNode;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
public class NormalSmartCardPicItemPicNode extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private TXImageView f2328a;
    private TextView b;
    private RelativeLayout c;
    private TextView d;
    private TextView e;
    private TextView f;

    public NormalSmartCardPicItemPicNode(Context context) {
        this(context, null);
    }

    public NormalSmartCardPicItemPicNode(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    private void a() {
        View inflate = LayoutInflater.from(getContext()).inflate((int) R.layout.smartcard_pic_frame_pic_node_layout, this);
        this.f2328a = (TXImageView) inflate.findViewById(R.id.smart_pic_node1_pic);
        this.c = (RelativeLayout) inflate.findViewById(R.id.smart_pic1_cover_layout);
        this.b = (TextView) inflate.findViewById(R.id.smart_pic_node1_txt);
        this.f = (TextView) inflate.findViewById(R.id.smartcard_pic_node1_cover_text);
        this.d = (TextView) inflate.findViewById(R.id.smartcard_pic_node1_cover_text_left);
        this.e = (TextView) inflate.findViewById(R.id.smartcard_pic_node1_cover_text_right);
    }

    public void a(SmartCardPicNode smartCardPicNode, STInfoV2 sTInfoV2, int i, boolean z, boolean z2) {
        if (i > 0) {
            this.f2328a.getLayoutParams().height = by.a(getContext(), (float) i);
        }
        this.f2328a.updateImageView(smartCardPicNode.f1513a, -1, TXImageView.TXImageViewType.NETWORK_IMAGE_MIDDLE);
        this.f2328a.setOnClickListener(new b(this, smartCardPicNode, sTInfoV2));
        if (!TextUtils.isEmpty(smartCardPicNode.e)) {
            this.b.setText(Html.fromHtml(smartCardPicNode.e));
            this.b.setVisibility(0);
            this.b.setSingleLine(z);
            if (!z) {
                this.b.setMaxLines(2);
            }
        } else {
            this.b.setVisibility(8);
        }
        if (!TextUtils.isEmpty(smartCardPicNode.c) || !TextUtils.isEmpty(smartCardPicNode.c)) {
            this.c.setVisibility(0);
            if (TextUtils.isEmpty(smartCardPicNode.d)) {
                this.d.setVisibility(8);
                this.e.setVisibility(8);
                this.f.setVisibility(0);
                this.f.setText(smartCardPicNode.c);
                this.f.setSingleLine(z2);
                if (!z2) {
                    this.f.setMaxLines(2);
                    return;
                }
                return;
            }
            this.d.setVisibility(0);
            this.d.setText(smartCardPicNode.c);
            this.e.setVisibility(0);
            this.e.setText(smartCardPicNode.d);
            this.f.setVisibility(8);
            return;
        }
        this.c.setVisibility(8);
    }
}
