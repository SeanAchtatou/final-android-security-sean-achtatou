package com.tencent.cloud.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.tencent.assistant.component.categorydetail.FloatTagHeader;
import com.tencent.assistant.component.categorydetail.SmoothShrinkListener;
import com.tencent.assistant.component.txscrollview.ITXRefreshListViewListener;
import com.tencent.assistant.component.txscrollview.TXGetMoreListView;
import com.tencent.assistant.component.txscrollview.TXRefreshScrollViewBase;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.module.callback.b;
import com.tencent.assistant.protocol.jce.TagGroup;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.adapter.smartlist.SmartListAdapter;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.cloud.c.e;
import java.util.List;

/* compiled from: ProGuard */
public class CategoryDetailListView extends TXGetMoreListView implements ITXRefreshListViewListener {
    private float A;
    private float B;
    private float C;
    private float D;
    private final int E = by.a(getContext(), 10.0f);
    private boolean F = false;
    /* access modifiers changed from: private */
    public int G = 0;
    /* access modifiers changed from: private */
    public int H = 0;
    private b I = new d(this);
    private SmoothShrinkListener J = new e(this);
    private e u = null;
    /* access modifiers changed from: private */
    public SmartListAdapter v = null;
    private f w;
    private int x = 1;
    /* access modifiers changed from: private */
    public FloatTagHeader y;
    /* access modifiers changed from: private */
    public long z = 0;

    public CategoryDetailListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        s();
    }

    public ListView getListView() {
        return (ListView) this.s;
    }

    /* access modifiers changed from: private */
    public void s() {
        ListAdapter adapter = ((ListView) this.s).getAdapter();
        if (adapter instanceof HeaderViewListAdapter) {
            this.v = (SmartListAdapter) ((HeaderViewListAdapter) adapter).getWrappedAdapter();
        } else {
            this.v = (SmartListAdapter) ((ListView) this.s).getAdapter();
        }
        if (this.v == null) {
        }
    }

    public void a(View.OnClickListener onClickListener) {
        this.y.setTagSelectedListener(onClickListener);
    }

    public void b(View.OnClickListener onClickListener) {
        this.y.setTagClickListener(onClickListener);
    }

    public void a(f fVar) {
        this.w = fVar;
    }

    public void a(e eVar) {
        this.u = eVar;
        this.u.a(this.I);
        setRefreshListViewListener(this);
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                this.A = motionEvent.getRawX();
                this.B = motionEvent.getRawY();
                break;
            case 1:
                this.C = motionEvent.getRawX();
                this.D = motionEvent.getRawY();
                if (this.F) {
                    b(true);
                    break;
                }
                break;
            case 2:
                this.C = motionEvent.getRawX();
                this.D = motionEvent.getRawY();
                break;
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    /* access modifiers changed from: private */
    public void a(int i, int i2, boolean z2) {
        if (i2 == 0) {
            if (this.v.getCount() == 0) {
                this.w.b(10);
                return;
            }
            this.w.f();
            this.e.setVisibility(0);
            this.v.notifyDataSetChanged();
            onRefreshComplete(this.u.b(), true);
        } else if (!z2) {
            this.e.setVisibility(0);
            onRefreshComplete(this.u.b(), false);
            this.w.c();
        } else if (-800 == i2) {
            this.w.b(30);
        } else if (this.x <= 0) {
            this.w.b(20);
        } else {
            this.x--;
            this.u.a(this.z);
        }
    }

    /* access modifiers changed from: private */
    public void b(int i, int i2, boolean z2) {
        if (i2 == 0) {
            if (this.v.getCount() == 0) {
                this.w.a(10);
                return;
            }
            this.w.b();
            this.e.setVisibility(0);
            this.v.notifyDataSetChanged();
            onRefreshComplete(this.u.b());
        } else if (!z2) {
            this.e.setVisibility(0);
            onRefreshComplete(this.u.b());
            this.w.c();
        } else if (-800 == i2) {
            this.w.a(30);
        } else if (this.x <= 0) {
            this.w.a(20);
        } else {
            this.x--;
            this.u.c();
        }
    }

    public void a(long j) {
        if (this.v == null) {
            s();
        }
        if (!this.y.isTagExisted() || this.v == null) {
            this.w.d();
        } else {
            this.f691a = TXRefreshScrollViewBase.RefreshState.RESET;
            this.e.setVisibility(8);
            reset();
            this.v.a();
            this.w.e();
        }
        this.z = j;
        this.u.a(j);
    }

    public void onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState scrollState) {
        this.u.a();
    }

    public void g() {
        if (this.v != null) {
            this.v.h();
        }
    }

    public void q() {
        if (this.v != null) {
            this.v.i();
            this.v.notifyDataSetChanged();
        }
    }

    public void recycleData() {
        super.recycleData();
        this.u.b(this.I);
        if (this.v != null) {
            this.v.j();
        }
    }

    public void a(FloatTagHeader floatTagHeader, boolean z2) {
        this.y = floatTagHeader;
        if (z2) {
            r();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.cloud.component.CategoryDetailListView.a(boolean, int):void
     arg types: [int, int]
     candidates:
      com.tencent.cloud.component.CategoryDetailListView.a(com.tencent.cloud.component.CategoryDetailListView, java.util.List):void
      com.tencent.cloud.component.CategoryDetailListView.a(com.tencent.assistant.component.categorydetail.FloatTagHeader, boolean):void
      com.tencent.assistant.component.txscrollview.TXRefreshListView.a(android.content.Context, com.tencent.assistant.component.txscrollview.TXScrollViewBase$ScrollMode):com.tencent.assistant.component.txscrollview.TXLoadingLayoutBase
      com.tencent.assistant.component.txscrollview.TXRefreshScrollViewBase.a(android.content.Context, com.tencent.assistant.component.txscrollview.TXScrollViewBase$ScrollMode):com.tencent.assistant.component.txscrollview.TXLoadingLayoutBase
      com.tencent.assistant.component.txscrollview.TXRefreshScrollViewBase.a(int, int):void
      com.tencent.assistant.component.txscrollview.TXScrollViewBase.a(android.content.Context, android.view.View):void
      com.tencent.assistant.component.txscrollview.TXScrollViewBase.a(int, int):void
      com.tencent.cloud.component.CategoryDetailListView.a(boolean, int):void */
    public void b(boolean z2) {
        if (!z2) {
            a(true, 0);
        } else if (this.D - this.B >= 0.0f) {
            a(false, 0);
        } else {
            a(true, -((int) (this.D - this.B)));
        }
    }

    public void a(boolean z2, int i) {
        if (this.y != null && this.y.getEmptyHeaderHeight() > 0) {
            this.G = getListView().getFirstVisiblePosition();
            View childAt = getListView().getChildAt(this.G);
            if (childAt != null) {
                this.H = childAt.getTop();
                this.y.shrinkEmptyHeader(z2, i, this.J);
            }
        }
        this.F = false;
    }

    public void r() {
        if (this.y != null && this.y.getEmptyHeader() != null) {
            if (getAdapter() != null) {
                removeHeaderView(this.y.getEmptyHeader());
            }
            addHeaderView(this.y.getEmptyHeader());
        }
    }

    /* access modifiers changed from: private */
    public void a(List<TagGroup> list) {
        if (list != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < list.size()) {
                    if (list.get(i2) != null) {
                        STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(getContext(), 100);
                        buildSTInfo.slotId = a.a("05", i2 + 1);
                        l.a(buildSTInfo);
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }
}
