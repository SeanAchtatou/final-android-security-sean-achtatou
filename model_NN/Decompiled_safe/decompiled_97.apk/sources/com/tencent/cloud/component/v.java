package com.tencent.cloud.component;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.module.k;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.cloud.activity.UpdateIgnoreListActivity;

/* compiled from: ProGuard */
class v extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UpdateListView f2294a;

    v(UpdateListView updateListView) {
        this.f2294a = updateListView;
    }

    public void onTMAClick(View view) {
        if (k.b(this.f2294a.f2269a.b) != 0) {
            this.f2294a.b.startActivity(new Intent(this.f2294a.b, UpdateIgnoreListActivity.class));
        }
    }

    public STInfoV2 getStInfo() {
        STInfoV2 e = this.f2294a.e();
        if (e != null) {
            e.slotId = a.a("06", "001");
        }
        return e;
    }
}
