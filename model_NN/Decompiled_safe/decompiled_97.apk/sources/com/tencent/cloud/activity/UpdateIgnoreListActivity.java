package com.tencent.cloud.activity;

import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.widget.RelativeLayout;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.LoadingView;
import com.tencent.assistant.component.NormalErrorRecommendPage;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.module.k;
import com.tencent.assistant.protocol.jce.StatUpdateManageAction;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.cloud.component.UpdateIgnoreListView;
import com.tencent.pangu.manager.ak;

/* compiled from: ProGuard */
public class UpdateIgnoreListActivity extends BaseActivity implements UIEventListener {
    private StatUpdateManageAction A = null;
    private String B = null;
    private Context n = this;
    private AstApp u;
    private SecondNavigationTitleViewV5 v;
    private RelativeLayout w;
    private NormalErrorRecommendPage x;
    private LoadingView y;
    private UpdateIgnoreListView z;

    public int f() {
        return STConst.ST_PAGE_UPDATE_IGNORE;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_updateignorelist);
        u();
        this.u.k().addUIEventListener(1016, this);
        this.u.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UPDATE_LIST_CHANGED, this);
        this.u.k().addUIEventListener(1019, this);
        this.u.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UPDATE_IGNORE_LIST_DELETE, this);
        this.u.k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD, this);
        this.u.k().addUIEventListener(1013, this);
    }

    private void u() {
        this.A = new StatUpdateManageAction();
        this.u = AstApp.i();
        this.v = (SecondNavigationTitleViewV5) findViewById(R.id.title_view);
        this.w = (RelativeLayout) findViewById(R.id.layout_container);
        this.x = (NormalErrorRecommendPage) findViewById(R.id.error_page);
        this.y = (LoadingView) findViewById(R.id.loading_view);
        this.v.d(false);
        this.v.a(this);
        this.v.i();
        this.v.d();
        this.x.setErrorType(10);
        if (k.b(false) > 0) {
            this.y.setVisibility(8);
        }
        this.z = new UpdateIgnoreListView(this.n, this.A);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.B = extras.getString(STConst.ST_PUSH_TO_UPDATE_KEY);
        }
        this.z.a(this.B);
        this.w.addView(this.z);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        t();
        this.z.a();
        this.v.l();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.z.f2268a.b();
        super.onPause();
        this.v.m();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.u.k().removeUIEventListener(1016, this);
        this.u.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UPDATE_LIST_CHANGED, this);
        this.u.k().removeUIEventListener(1019, this);
        this.u.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_UPDATE_IGNORE_LIST_DELETE, this);
        this.u.k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD, this);
        this.u.k().removeUIEventListener(1013, this);
        this.z.f2268a.c();
        super.onDestroy();
    }

    public void handleUIEvent(Message message) {
        switch (message.what) {
            case 1013:
            case EventDispatcherEnum.UI_EVENT_APP_UPDATE_LIST_CHANGED:
            case 1019:
            case EventDispatcherEnum.UI_EVENT_APP_UPDATE_IGNORE_LIST_DELETE:
                break;
            case EventDispatcherEnum.UI_EVENT_APP_STATE_UNINSTALL:
            case EventDispatcherEnum.UI_EVENT_CHECKUPDATE_FAIL:
            default:
                return;
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD:
            case 1016:
                this.z.f2268a.a();
                break;
        }
        this.z.b();
        t();
        ak.a().a(k.i());
    }

    public void t() {
        int b = k.b(false);
        if (b > 0) {
            this.v.b(getResources().getString(R.string.app_update_ignore));
            this.v.c("(" + b + ")");
            return;
        }
        finish();
    }
}
