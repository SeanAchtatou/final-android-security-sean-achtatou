package com.tencent.cloud.activity.debug;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/* compiled from: ProGuard */
public class QReaderPluginDebugActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private ViewGroup f2180a;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        LineBreakLayout lineBreakLayout = new LineBreakLayout(this);
        setContentView(lineBreakLayout, new ViewGroup.LayoutParams(-1, -1));
        this.f2180a = lineBreakLayout;
        a("下载电子书", new a(this));
        a("下载免费全本电子书", new b(this));
        a("批量下载电子书", new c(this));
        a("阅读电子书", new d(this));
        a("删除电子书", new e(this));
        a("书籍详情页", new f(this));
        a("充值（2）", new g(this));
        a("充值（3）", new h(this));
        a("触发QQ阅读客户端下载", new i(this));
    }

    private void a(String str, View.OnClickListener onClickListener) {
        Button button = new Button(this);
        button.setText(str);
        button.setOnClickListener(onClickListener);
        this.f2180a.addView(button);
    }
}
