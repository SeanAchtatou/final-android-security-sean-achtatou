package com.tencent.cloud.activity;

import android.view.View;
import android.widget.AbsListView;
import com.tencent.assistant.component.invalidater.TXRefreshGetMoreListViewScrollListener;
import com.tencent.assistantv2.adapter.smartlist.SmartListAdapter;

/* compiled from: ProGuard */
class k extends TXRefreshGetMoreListViewScrollListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ i f2194a;

    private k(i iVar) {
        this.f2194a = iVar;
    }

    /* synthetic */ k(i iVar, j jVar) {
        this(iVar);
    }

    public void onScroll(View view, int i, int i2, int i3) {
        super.onScroll(view, i, i2, i3);
        if (this.f2194a.Z != null) {
            SmartListAdapter.BannerType unused = this.f2194a.ad = this.f2194a.Z.p();
            if (this.f2194a.ad != SmartListAdapter.BannerType.None && this.f2194a.ad != SmartListAdapter.BannerType.HomePage && this.f2194a.ae != null) {
                this.f2194a.ae.a((AbsListView) view, i, i2, i3);
            }
        }
    }

    public void onScrollStateChanged(AbsListView absListView, int i) {
        super.onScrollStateChanged(absListView, i);
        if (this.f2194a.ae != null) {
            this.f2194a.ae.b(absListView, i);
        }
    }
}
