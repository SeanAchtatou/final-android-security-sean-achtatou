package com.tencent.cloud.d;

import com.tencent.assistant.protocol.jce.EBookInfo;
import com.tencent.cloud.model.SimpleEbookModel;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class q {
    public static SimpleEbookModel a(EBookInfo eBookInfo) {
        if (eBookInfo == null) {
            return null;
        }
        SimpleEbookModel simpleEbookModel = new SimpleEbookModel();
        try {
            simpleEbookModel.f2318a = Integer.valueOf(eBookInfo.f1227a).intValue();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        simpleEbookModel.b = eBookInfo.e;
        simpleEbookModel.c = eBookInfo.b;
        simpleEbookModel.d = eBookInfo.d;
        simpleEbookModel.e = eBookInfo.c;
        simpleEbookModel.f = eBookInfo.g;
        simpleEbookModel.g = eBookInfo.f;
        simpleEbookModel.h = eBookInfo.i;
        simpleEbookModel.i = eBookInfo.j;
        simpleEbookModel.k = eBookInfo.h;
        if (eBookInfo.l != null) {
            simpleEbookModel.l = eBookInfo.l.f1125a;
        }
        if (eBookInfo.k == 0) {
            simpleEbookModel.j = SimpleEbookModel.CARD_TYPE.NORMAL;
            return simpleEbookModel;
        } else if (eBookInfo.k == 1) {
            simpleEbookModel.j = SimpleEbookModel.CARD_TYPE.RICH;
            return simpleEbookModel;
        } else {
            simpleEbookModel.j = SimpleEbookModel.CARD_TYPE.NORMAL;
            return simpleEbookModel;
        }
    }

    public static List<SimpleEbookModel> a(List<EBookInfo> list) {
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (EBookInfo a2 : list) {
            arrayList.add(a(a2));
        }
        return arrayList;
    }
}
