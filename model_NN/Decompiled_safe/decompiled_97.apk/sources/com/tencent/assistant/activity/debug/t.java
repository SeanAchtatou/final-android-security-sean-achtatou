package com.tencent.assistant.activity.debug;

import android.view.View;
import android.widget.Toast;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.nucleus.manager.root.e;

/* compiled from: ProGuard */
class t implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DActivity f519a;

    t(DActivity dActivity) {
        this.f519a = dActivity;
    }

    public void onClick(View view) {
        if (e.a().c()) {
            TemporaryThreadManager.get().start(new u(this));
        } else {
            Toast.makeText(this.f519a.getApplicationContext(), "没有临时root权限", 0).show();
        }
    }
}
