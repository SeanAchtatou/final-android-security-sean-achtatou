package com.tencent.assistant.activity;

import android.util.Log;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.module.k;
import com.tencent.securemodule.impl.AppInfo;
import com.tencent.securemodule.service.CloudScanListener;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
class di implements CloudScanListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ StartScanActivity f529a;

    private di(StartScanActivity startScanActivity) {
        this.f529a = startScanActivity;
    }

    /* synthetic */ di(StartScanActivity startScanActivity, da daVar) {
        this(startScanActivity);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.StartScanActivity.a(com.tencent.assistant.activity.StartScanActivity, java.lang.Boolean):java.lang.Boolean
     arg types: [com.tencent.assistant.activity.StartScanActivity, int]
     candidates:
      com.tencent.assistant.activity.StartScanActivity.a(com.tencent.assistant.activity.StartScanActivity, long):long
      com.tencent.assistant.activity.StartScanActivity.a(com.tencent.assistant.activity.StartScanActivity, com.tencent.assistant.localres.model.LocalApkInfo):java.lang.Boolean
      com.tencent.assistant.activity.StartScanActivity.a(long, java.util.List<com.tencent.securemodule.impl.AppInfo>):void
      com.tencent.assistant.activity.StartScanActivity.a(com.tencent.assistant.activity.StartScanActivity, int):void
      com.tencent.assistant.activity.StartScanActivity.a(int, int):void
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.module.callback.e.a(int, int):void
      com.tencent.assistant.activity.StartScanActivity.a(com.tencent.assistant.activity.StartScanActivity, java.lang.Boolean):java.lang.Boolean */
    public void onFinish(int i) {
        long j = 0;
        long unused = this.f529a.Z = System.currentTimeMillis();
        if (this.f529a.Y != 0) {
            j = this.f529a.Z - this.f529a.Y;
        }
        this.f529a.a(j, (List) null);
        Boolean unused2 = this.f529a.S = (Boolean) true;
        this.f529a.n.cancel();
        Log.i("StartScanActivity", "onFinish");
        if (i != 0) {
            this.f529a.d(10);
        } else if (!this.f529a.S.booleanValue() || !StartScanActivity.a(this.f529a.x)) {
            this.f529a.d(10);
        } else {
            this.f529a.d(11);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.StartScanActivity.a(com.tencent.assistant.activity.StartScanActivity, java.lang.Boolean):java.lang.Boolean
     arg types: [com.tencent.assistant.activity.StartScanActivity, int]
     candidates:
      com.tencent.assistant.activity.StartScanActivity.a(com.tencent.assistant.activity.StartScanActivity, long):long
      com.tencent.assistant.activity.StartScanActivity.a(com.tencent.assistant.activity.StartScanActivity, com.tencent.assistant.localres.model.LocalApkInfo):java.lang.Boolean
      com.tencent.assistant.activity.StartScanActivity.a(long, java.util.List<com.tencent.securemodule.impl.AppInfo>):void
      com.tencent.assistant.activity.StartScanActivity.a(com.tencent.assistant.activity.StartScanActivity, int):void
      com.tencent.assistant.activity.StartScanActivity.a(int, int):void
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.module.callback.e.a(int, int):void
      com.tencent.assistant.activity.StartScanActivity.a(com.tencent.assistant.activity.StartScanActivity, java.lang.Boolean):java.lang.Boolean */
    public void onRiskFoud(List<AppInfo> list) {
        long j = 0;
        Log.i("StartScanActivity", "onRiskFoun");
        long unused = this.f529a.Z = System.currentTimeMillis();
        if (this.f529a.Y != 0) {
            j = this.f529a.Z - this.f529a.Y;
        }
        this.f529a.a(j, list);
        Boolean unused2 = this.f529a.S = (Boolean) true;
        this.f529a.J.clear();
        this.f529a.J.addAll(list);
        if (this.f529a.J != null && this.f529a.J.size() > 0) {
            ArrayList<SimpleAppModel> f = k.f(this.f529a.J);
            this.f529a.L.clear();
            this.f529a.L.addAll(f);
            int unused3 = this.f529a.W = this.f529a.O.a(f);
        } else if (this.f529a.S.booleanValue()) {
            this.f529a.n.cancel();
            this.f529a.d(11);
        }
    }

    @Deprecated
    public void onRiskFound() {
    }
}
