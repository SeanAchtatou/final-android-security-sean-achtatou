package com.tencent.assistant.activity;

import android.view.animation.Animation;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
class ac implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ab f388a;

    ac(ab abVar) {
        this.f388a = abVar;
    }

    public void onAnimationStart(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationEnd(Animation animation) {
        if (this.f388a.f387a.ad) {
            this.f388a.f387a.P.setText(this.f388a.f387a.getString(R.string.backup_success));
        } else {
            this.f388a.f387a.P.setText(this.f388a.f387a.getString(R.string.backup_failed));
        }
        this.f388a.f387a.n.setEnabled(true);
        this.f388a.f387a.n.setText(this.f388a.f387a.getResources().getString(R.string.app_backup_list));
    }
}
