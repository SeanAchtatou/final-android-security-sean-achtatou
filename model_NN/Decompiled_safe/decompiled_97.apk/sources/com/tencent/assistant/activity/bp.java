package com.tencent.assistant.activity;

import android.view.View;
import android.widget.Toast;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.module.k;
import com.tencent.assistant.net.c;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class bp extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PanelManagerActivity f427a;

    bp(PanelManagerActivity panelManagerActivity) {
        this.f427a = panelManagerActivity;
    }

    public void onTMAClick(View view) {
        if (c.a()) {
            this.f427a.x();
        } else {
            Toast.makeText(AstApp.i(), (int) R.string.qube_network_unavaliable, 0).show();
        }
    }

    public STInfoV2 getStInfo() {
        return STInfoBuilder.buildSTInfo(this.f427a, this.f427a.x, "03_001", a.a(k.d(this.f427a.x)), a.a(k.d(this.f427a.x), this.f427a.x));
    }
}
