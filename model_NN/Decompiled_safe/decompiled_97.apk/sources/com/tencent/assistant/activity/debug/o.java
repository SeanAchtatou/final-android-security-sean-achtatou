package com.tencent.assistant.activity.debug;

import android.net.Uri;
import android.view.View;
import com.tencent.pangu.link.b;
import java.net.URLEncoder;

/* compiled from: ProGuard */
class o implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DActivity f514a;

    o(DActivity dActivity) {
        this.f514a = dActivity;
    }

    public void onClick(View view) {
        b.a(this.f514a, Uri.parse("tmast://download?downl_biz_id=qb&downl_url=" + URLEncoder.encode(this.f514a.e.getText().toString())));
    }
}
