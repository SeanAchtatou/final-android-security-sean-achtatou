package com.tencent.assistant.activity.debug;

import android.view.View;
import android.widget.Toast;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.plugin.mgr.i;
import com.tencent.assistant.plugin.proxy.PluginProxyActivity;
import java.util.List;

/* compiled from: ProGuard */
class au implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DActivity f494a;

    au(DActivity dActivity) {
        this.f494a = dActivity;
    }

    public void onClick(View view) {
        List<PluginInfo> a2 = i.b().a(1);
        if (a2 == null || a2.size() == 0) {
            Toast.makeText(this.f494a, "请先安装插件!", 1).show();
            return;
        }
        for (PluginInfo next : a2) {
            if (next != null && next.getPackageName().equals("p.com.tencent.android.qqdownloader")) {
                PluginProxyActivity.a(this.f494a, next.getPackageName(), next.getVersion(), "p.com.tencent.assistant.activity.PhotoBackupNewActivity", next.getInProcess(), null, null);
                return;
            }
        }
    }
}
