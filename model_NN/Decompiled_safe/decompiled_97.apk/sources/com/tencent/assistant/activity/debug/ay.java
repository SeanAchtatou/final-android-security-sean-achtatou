package com.tencent.assistant.activity.debug;

import android.view.View;
import android.widget.AdapterView;
import com.tencent.assistant.Global;
import com.tencent.assistant.m;

/* compiled from: ProGuard */
class ay implements AdapterView.OnItemSelectedListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ServerAdressSettingActivity f498a;

    ay(ServerAdressSettingActivity serverAdressSettingActivity) {
        this.f498a = serverAdressSettingActivity;
    }

    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
        if (i != this.f498a.g) {
            m.a().a(this.f498a.d = i);
            this.f498a.b.setSelection(this.f498a.h);
            this.f498a.e.setText("当前：" + Global.getServerAddressName());
            this.f498a.d();
        }
    }

    public void onNothingSelected(AdapterView<?> adapterView) {
    }
}
