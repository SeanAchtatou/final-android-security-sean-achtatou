package com.tencent.assistant.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.jg.EType;
import com.jg.JgClassChecked;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.component.txscrollview.TXTabBarLayout;
import com.tencent.assistant.manager.webview.component.TxWebViewContainer;
import com.tencent.assistant.manager.webview.component.WebViewFooter;
import com.tencent.assistant.manager.webview.component.b;
import com.tencent.assistant.manager.webview.component.k;
import com.tencent.assistant.manager.webview.js.m;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.protocol.jce.ActionUrl;
import com.tencent.assistant.st.STConst;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.bm;
import com.tencent.assistant.utils.bw;
import com.tencent.assistant.utils.by;
import com.tencent.assistant.utils.ca;
import com.tencent.assistantv2.component.SecondNavigationTitleViewV5;
import com.tencent.assistantv2.st.l;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.connect.common.Constants;
import com.tencent.nucleus.socialcontact.a;
import com.tencent.nucleus.socialcontact.login.j;
import com.tencent.pangu.activity.ShareBaseActivity;
import com.tencent.pangu.c.p;
import com.tencent.pangu.component.appdetail.AppdetailFloatingDialog;
import com.tencent.pangu.component.appdetail.t;
import com.tencent.pangu.model.ShareBaseModel;
import com.tencent.pangu.utils.d;
import java.io.Serializable;
import java.util.Stack;

@JgClassChecked(author = 71024, fComment = "确认已进行安全校验", lastDate = "20140527", level = 1, reviewer = 89182, vComment = {EType.JSEXECUTECHECK, EType.ACTIVITYCHECK})
/* compiled from: ProGuard */
public class BrowserActivity extends ShareBaseActivity {
    private static String K;
    private static String L;
    private static long M;
    /* access modifiers changed from: private */
    public static String N = "0";
    public View.OnClickListener A = new View.OnClickListener() {
        public void onClick(View view) {
            if ("1".equals(BrowserActivity.N)) {
                if (BrowserActivity.this.u == null || !BrowserActivity.this.u.e()) {
                    BrowserActivity.this.finish();
                    return;
                }
                BrowserActivity.this.J();
                BrowserActivity.this.u.g();
            } else if (!"2".equals(BrowserActivity.N)) {
                BrowserActivity.this.finish();
            } else if (BrowserActivity.this.u != null) {
                BrowserActivity.this.J();
                BrowserActivity.this.u.a("javascript:if(!!window.backClick){backClick()};void(0);");
            } else {
                BrowserActivity.this.finish();
            }
        }
    };
    public View.OnClickListener B = new View.OnClickListener() {
        public void onClick(View view) {
            try {
                String b = j.a().l() ? a.b() : a.a();
                Intent intent = new Intent(BrowserActivity.this, BrowserActivity.class);
                intent.putExtra("com.tencent.assistant.BROWSER_URL", b);
                BrowserActivity.this.n.startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    private RelativeLayout C;
    private boolean D = false;
    private boolean E = false;
    /* access modifiers changed from: private */
    public String F;
    /* access modifiers changed from: private */
    public String G = Constants.STR_EMPTY;
    /* access modifiers changed from: private */
    public String H = "NONE";
    private AppdetailFloatingDialog I;
    private boolean J = false;
    private String O = "/qqdownloader/3";
    private boolean P = false;
    private ShareBaseModel Q;
    private String R = ".*mq.wsq.qq.com.*|.*m.wsq.qq.com.*";
    private String S = "1";
    private String T = "0";
    private int U = 0;
    private boolean V = false;
    private boolean W = false;
    private k X = new k() {
        public void onFresh() {
            BrowserActivity.this.u.i();
        }

        public void onForward() {
            BrowserActivity.this.u.h();
        }

        public void onBack() {
            BrowserActivity.this.u.g();
        }
    };
    private t Y = new t() {
        public void showPermission() {
        }

        public void showReport() {
        }

        public void shareToQQ() {
            BrowserActivity.this.w();
        }

        public void shareToQZ() {
            BrowserActivity.this.x();
        }

        public void shareToWX() {
            BrowserActivity.this.y();
        }

        public void shareToTimeLine() {
            BrowserActivity.this.z();
        }

        public void doCollectioon() {
        }
    };
    protected Context n;
    protected TxWebViewContainer u;
    protected SecondNavigationTitleViewV5 v;
    public WebViewFooter w;
    Stack<SaveInfoItem> x = new Stack<>();
    Intent y = null;
    String z;

    @JgClassChecked(author = 71024, fComment = "确认已进行安全校验", lastDate = "20140527", level = 1, reviewer = 89182, vComment = {EType.JSEXECUTECHECK})
    /* compiled from: ProGuard */
    public interface WebChromeClientListener {
        Activity getActivity();

        void onProgressChanged(WebView webView, int i);

        void onReceivedTitle(WebView webView, String str);
    }

    static /* synthetic */ String a(BrowserActivity browserActivity, Object obj) {
        String str = browserActivity.F + obj;
        browserActivity.F = str;
        return str;
    }

    public int f() {
        int i = STConst.ST_PAGE_FROM_WEBVIEW;
        if (this.F == null) {
            return STConst.ST_PAGE_FROM_WEBVIEW;
        }
        if (this.F.contains(a.a()) || this.F.contains(a.b())) {
            return STConst.ST_PAGE_MY_APPBAR;
        }
        if (this.F != null && this.F.matches(this.R)) {
            i = STConst.ST_PAGE_ASSISTANT_APP_LOCAL;
        }
        if (this.P) {
            return STConst.ST_PAGE_ASSISTANT_APP_EXTERNAL;
        }
        return i;
    }

    public boolean g() {
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.BrowserActivity.a(android.content.Intent, boolean):void
     arg types: [android.content.Intent, int]
     candidates:
      com.tencent.assistant.activity.BrowserActivity.a(int, java.lang.String):java.lang.String
      com.tencent.assistant.activity.BrowserActivity.a(com.tencent.assistant.activity.BrowserActivity, java.lang.Object):java.lang.String
      com.tencent.assistant.activity.BrowserActivity.a(com.tencent.assistant.activity.BrowserActivity, java.lang.String):java.lang.String
      com.tencent.assistant.activity.BrowserActivity.a(int, int):boolean
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.activity.BrowserActivity.a(android.content.Intent, boolean):void */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getWindow().requestFeature(1);
        ca.a();
        this.n = this;
        try {
            setContentView((int) R.layout.browser_layout);
            this.y = getIntent();
            a(getIntent(), true);
        } catch (Throwable th) {
            this.W = true;
            com.tencent.assistant.manager.t.a().b();
            finish();
        }
    }

    /* compiled from: ProGuard */
    class SaveInfoItem {
        Intent myIntent;
        String myTitle;

        SaveInfoItem() {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.BrowserActivity.a(android.content.Intent, boolean):void
     arg types: [android.content.Intent, int]
     candidates:
      com.tencent.assistant.activity.BrowserActivity.a(int, java.lang.String):java.lang.String
      com.tencent.assistant.activity.BrowserActivity.a(com.tencent.assistant.activity.BrowserActivity, java.lang.Object):java.lang.String
      com.tencent.assistant.activity.BrowserActivity.a(com.tencent.assistant.activity.BrowserActivity, java.lang.String):java.lang.String
      com.tencent.assistant.activity.BrowserActivity.a(int, int):boolean
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.activity.BrowserActivity.a(android.content.Intent, boolean):void */
    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (this.y != null) {
            SaveInfoItem saveInfoItem = new SaveInfoItem();
            saveInfoItem.myIntent = this.y;
            saveInfoItem.myTitle = this.z;
            this.x.push(saveInfoItem);
        }
        this.y = intent;
        a(intent, true);
    }

    private void a(Intent intent, boolean z2) {
        this.G = Constants.STR_EMPTY;
        try {
            b(intent);
        } catch (Throwable th) {
        }
        G();
        D();
        if (z2) {
            b bVar = new b();
            bVar.f912a = this.O;
            bVar.b = this.U;
            if (this.F != null && this.F.matches(this.R)) {
                bVar.d = 2;
            }
            this.u.a(bVar);
            K();
        }
    }

    private void b(Intent intent) {
        String str;
        ActionUrl actionUrl;
        if (intent.hasExtra("com.tencent.assistant.BROWSER_URL")) {
            this.F = d.a(intent, "com.tencent.assistant.BROWSER_URL");
            if (!TextUtils.isEmpty(this.F)) {
                this.J = this.F.contains(".swf");
                this.P = this.F.contains("qOpenAppId") || this.F.contains("qPackageName");
                if (this.F.matches(this.R)) {
                    this.D = true;
                }
            }
        }
        Bundle a2 = d.a(getIntent());
        if (a2 == null || !a2.containsKey("com.tencent.assistant.activity.BROWSER_TYPE")) {
            str = null;
        } else {
            String string = a2.getString("com.tencent.assistant.activity.BROWSER_TYPE");
            if ("0".equals(string)) {
                this.D = true;
            }
            str = string;
        }
        if (a2 != null && a2.containsKey("com.tencent.assistant.activity.BROWSER_ACCELERATE")) {
            String string2 = a2.getString("com.tencent.assistant.activity.BROWSER_ACCELERATE");
            if ("0".equals(string2)) {
                this.U = 2;
            }
            if ("1".equals(string2)) {
                this.U = 1;
            }
        }
        if (a2 != null && a2.containsKey("suport.zoom")) {
            String string3 = a2.getString("suport.zoom");
            if ("0".equals(string3)) {
                this.V = false;
            }
            if ("1".equals(string3)) {
                this.V = true;
            }
        }
        if (a2 != null && a2.containsKey("com.tencent.assistant.activity.PKGNAME_APPBAR")) {
            this.G = a2.getString("com.tencent.assistant.activity.PKGNAME_APPBAR");
        }
        if (a2 != null && a2.containsKey("goback")) {
            N = a2.getString("goback");
        }
        if (intent.hasExtra("com.tencent.assistant.ACTION_URL")) {
            Serializable b = d.b(intent, "com.tencent.assistant.ACTION_URL");
            if ((b instanceof ActionUrl) && (actionUrl = (ActionUrl) b) != null) {
                int b2 = actionUrl.b();
                this.H = a(b2, this.F);
                if (str == null) {
                    this.D = a(b2, 1);
                }
                this.E = a(b2, 2);
                return;
            }
            return;
        }
        this.H = a(0, this.F);
    }

    private void D() {
        if (this.P) {
            this.O = "/qqdownloader/3/external";
        } else {
            this.O = "/qqdownloader/3";
        }
        if (!TextUtils.isEmpty(this.F)) {
            Uri parse = Uri.parse(this.F);
            K = TextUtils.isEmpty(K) ? parse.getQueryParameter("qOpenId") : K;
            L = TextUtils.isEmpty(L) ? parse.getQueryParameter("qAccessToken") : L;
            M = M == 0 ? bm.c(parse.getQueryParameter("qOpenAppId")) : M;
            this.r = parse.getQueryParameter("qPackageName");
            this.F = this.F.replaceAll("((qOpenAppId)|(qPackageName)|(qAccessToken)|(qOpenId))=[^& ]*&*", Constants.STR_EMPTY);
            STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(this, 100);
            buildSTInfo.pushInfo = getIntent().getStringExtra("preActivityPushInfo");
            l.a(buildSTInfo);
        }
    }

    private void F() {
        m mVar = new m();
        mVar.f934a = K;
        mVar.b = L;
        mVar.c = M;
        com.tencent.assistant.manager.webview.js.l.a(this.n, this.F, mVar);
    }

    private String a(int i, String str) {
        String str2 = a(i, 0) ? "ALL" : "NONE";
        if (!str2.equals("NONE") || TextUtils.isEmpty(str) || !com.tencent.assistant.manager.webview.js.l.a(Uri.parse(str).getHost())) {
            return str2;
        }
        return "ALL";
    }

    private boolean a(int i, int i2) {
        return ((i >>> i2) & 1) == 1;
    }

    @SuppressLint({"NewApi"})
    private void G() {
        this.u = (TxWebViewContainer) findViewById(R.id.webviewcontainer);
        this.C = (RelativeLayout) findViewById(R.id.browser_footer_layout);
        H();
        u();
        b(false);
    }

    private void H() {
        try {
            this.v = (SecondNavigationTitleViewV5) findViewById(R.id.browser_header_view);
            this.v.a(this);
            if (this.J || this.E) {
                this.v.setVisibility(8);
            } else {
                this.v.setVisibility(0);
            }
            this.v.i();
            if (this.P) {
                this.v.a((int) R.drawable.bar_white);
                this.v.g();
            } else {
                this.v.h();
            }
            this.v.a(this.Y);
            this.v.b(this.B);
            this.v.e(this.A);
            this.v.b(" ");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void b(boolean z2) {
        XLog.d("BrowserActivity", "setButtonVisiable-- flag = " + z2, new Throwable("xx"));
        if (this.v != null) {
            this.v.a(z2);
            this.v.a(new OnTMAParamClickListener() {
                public void onTMAClick(View view) {
                    if (BrowserActivity.this.u != null) {
                        BrowserActivity.this.u.l();
                    }
                }

                public STInfoV2 getStInfo() {
                    STInfoV2 buildSTInfo = STInfoBuilder.buildSTInfo(BrowserActivity.this.n, 200);
                    if (buildSTInfo != null) {
                        buildSTInfo.slotId = "03_001";
                    }
                    return buildSTInfo;
                }
            });
        }
    }

    public void t() {
        if (this.v != null) {
            this.I = this.v.o();
            if (this.I != null && !isFinishing()) {
                try {
                    this.I.show();
                    this.I.d();
                    this.I.c();
                    Window window = this.I.getWindow();
                    WindowManager.LayoutParams attributes = window.getAttributes();
                    attributes.gravity = 53;
                    if (this.v.p() != null) {
                        attributes.y = getResources().getDimensionPixelSize(R.dimen.app_detail_float_bar_content_height) - getResources().getDimensionPixelSize(R.dimen.app_detail_float_share_yyb_dialog_y);
                    }
                    this.I.getWindow().getWindowManager();
                    window.setAttributes(attributes);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void setTitle(CharSequence charSequence) {
        if (this.v != null && !TextUtils.isEmpty(charSequence)) {
            String obj = charSequence.toString();
            this.v.b(obj);
            this.z = obj;
        }
    }

    public void u() {
        this.w = (WebViewFooter) findViewById(R.id.browser_footer_view);
        this.w.setVisibility(0);
        this.w.a(this.X);
        I();
        c(this.D);
    }

    public void c(boolean z2) {
        if (!this.W) {
            if (z2 || this.J) {
                this.w.setVisibility(8);
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.u.getLayoutParams();
                if (layoutParams == null) {
                    layoutParams = new RelativeLayout.LayoutParams(-1, -1);
                }
                layoutParams.bottomMargin = 0;
                this.u.setLayoutParams(layoutParams);
            }
            if (!z2 && !this.J) {
                this.w.setVisibility(0);
                RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) this.u.getLayoutParams();
                if (layoutParams2 == null) {
                    layoutParams2 = new RelativeLayout.LayoutParams(-1, -1);
                }
                layoutParams2.bottomMargin = by.a(getApplicationContext(), 50.0f);
                this.u.setLayoutParams(layoutParams2);
            }
        }
    }

    private void I() {
        if (this.u != null) {
            if (this.u.e()) {
                this.w.a(true);
            } else {
                this.w.a(false);
            }
            if (this.u.f()) {
                this.w.b(true);
            } else {
                this.w.b(false);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.activity.BrowserActivity.a(android.content.Intent, boolean):void
     arg types: [android.content.Intent, int]
     candidates:
      com.tencent.assistant.activity.BrowserActivity.a(int, java.lang.String):java.lang.String
      com.tencent.assistant.activity.BrowserActivity.a(com.tencent.assistant.activity.BrowserActivity, java.lang.Object):java.lang.String
      com.tencent.assistant.activity.BrowserActivity.a(com.tencent.assistant.activity.BrowserActivity, java.lang.String):java.lang.String
      com.tencent.assistant.activity.BrowserActivity.a(int, int):boolean
      com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
      com.tencent.assistant.activity.BrowserActivity.a(android.content.Intent, boolean):void */
    /* access modifiers changed from: private */
    public void J() {
        SaveInfoItem pop;
        if (!this.x.isEmpty() && (pop = this.x.pop()) != null) {
            a(pop.myIntent, false);
            setTitle(pop.myTitle);
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4 || this.s || !this.u.e()) {
            return super.onKeyDown(i, keyEvent);
        }
        J();
        this.u.g();
        return true;
    }

    private void K() {
        this.u.a(0);
        ah.a().post(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.tencent.assistant.activity.BrowserActivity.a(com.tencent.assistant.activity.BrowserActivity, java.lang.Object):java.lang.String
             arg types: [com.tencent.assistant.activity.BrowserActivity, java.lang.String]
             candidates:
              com.tencent.assistant.activity.BrowserActivity.a(int, java.lang.String):java.lang.String
              com.tencent.assistant.activity.BrowserActivity.a(com.tencent.assistant.activity.BrowserActivity, java.lang.String):java.lang.String
              com.tencent.assistant.activity.BrowserActivity.a(android.content.Intent, boolean):void
              com.tencent.assistant.activity.BrowserActivity.a(int, int):boolean
              com.tencent.assistant.activity.BaseActivity.a(java.lang.String, java.lang.String):void
              com.tencent.assistant.activity.BrowserActivity.a(com.tencent.assistant.activity.BrowserActivity, java.lang.Object):java.lang.String */
            public void run() {
                if (!TextUtils.isEmpty(BrowserActivity.this.F)) {
                    com.tencent.assistant.manager.webview.js.l.a(BrowserActivity.this, BrowserActivity.this.F, BrowserActivity.this.H);
                    if (BrowserActivity.this.u != null) {
                        BrowserActivity.this.u.m();
                    }
                    if (BrowserActivity.this.u != null) {
                        if (BrowserActivity.this.G != null && !TextUtils.isEmpty(BrowserActivity.this.G)) {
                            BrowserActivity.a(BrowserActivity.this, (Object) ("&pkgName=" + BrowserActivity.this.G));
                        }
                        XLog.i("xjp", "[doRefresh] ---> url : " + BrowserActivity.this.F);
                        BrowserActivity.this.u.a(BrowserActivity.this.F);
                        String unused = BrowserActivity.this.G = Constants.STR_EMPTY;
                    }
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!this.W) {
            if (this.u != null) {
                this.u.a();
            }
            F();
            this.v.l();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (!this.W) {
            if (this.u != null) {
                this.u.b();
            }
            this.v.m();
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.W) {
            super.onDestroy();
            return;
        }
        if (this.u != null) {
            this.u.c();
        }
        this.B = null;
        super.onDestroy();
    }

    public void onDetachedFromWindow() {
        if (this.u != null) {
            this.u.d();
        }
        super.onDetachedFromWindow();
    }

    public void d(boolean z2) {
        if (this.v != null) {
            if (z2) {
                this.v.setVisibility(0);
            } else {
                this.v.setVisibility(8);
            }
        }
    }

    public void v() {
        try {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(this.F));
            intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, f());
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 100 && this.u != null) {
            ValueCallback<Uri> j = this.u.j();
            if (j != null) {
                j.onReceiveValue((intent == null || i2 != -1) ? null : intent.getData());
                this.u.k();
            } else {
                return;
            }
        }
        if (i != 101) {
            return;
        }
        if (intent != null) {
            Uri data = intent.getData();
            if (data != null) {
                String a2 = bw.a(this.n, data);
                if (a2 == null) {
                    if (this.u != null) {
                        this.u.a(this.T, Constants.STR_EMPTY);
                    }
                } else if (this.u != null) {
                    this.u.a(this.S, a2);
                }
            }
        } else if (this.u != null) {
            this.u.a(this.T, Constants.STR_EMPTY);
        }
    }

    public void a(ShareBaseModel shareBaseModel) {
        this.Q = shareBaseModel;
    }

    public void w() {
        E().b(this, this.Q);
    }

    public void x() {
        E().a(this, this.Q);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.c.e.a(android.content.Context, com.tencent.pangu.model.ShareBaseModel, boolean):void
     arg types: [com.tencent.assistant.activity.BrowserActivity, com.tencent.pangu.model.ShareBaseModel, int]
     candidates:
      com.tencent.pangu.c.e.a(android.app.Activity, com.tencent.pangu.model.ShareAppModel, boolean):void
      com.tencent.pangu.c.e.a(android.app.Activity, com.tencent.pangu.model.ShareBaseModel, boolean):void
      com.tencent.pangu.c.e.a(android.content.Context, com.tencent.pangu.model.ShareAppModel, boolean):void
      com.tencent.pangu.c.e.a(android.content.Context, com.tencent.pangu.model.ShareBaseModel, boolean):void */
    public void y() {
        E().a((Context) this, this.Q, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.c.e.a(android.content.Context, com.tencent.pangu.model.ShareBaseModel, boolean):void
     arg types: [com.tencent.assistant.activity.BrowserActivity, com.tencent.pangu.model.ShareBaseModel, int]
     candidates:
      com.tencent.pangu.c.e.a(android.app.Activity, com.tencent.pangu.model.ShareAppModel, boolean):void
      com.tencent.pangu.c.e.a(android.app.Activity, com.tencent.pangu.model.ShareBaseModel, boolean):void
      com.tencent.pangu.c.e.a(android.content.Context, com.tencent.pangu.model.ShareAppModel, boolean):void
      com.tencent.pangu.c.e.a(android.content.Context, com.tencent.pangu.model.ShareBaseModel, boolean):void */
    public void z() {
        E().a((Context) this, this.Q, true);
    }

    public void A() {
        p.a().a(this, this.Q);
    }

    public void B() {
        Intent intent = new Intent("android.intent.action.GET_CONTENT");
        intent.setType("*/*");
        intent.addCategory("android.intent.category.OPENABLE");
        try {
            startActivityForResult(Intent.createChooser(intent, "请选择一个文件"), TXTabBarLayout.TABITEM_TIPS_TEXT_ID);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, "please install File Manager", 0).show();
        }
    }
}
