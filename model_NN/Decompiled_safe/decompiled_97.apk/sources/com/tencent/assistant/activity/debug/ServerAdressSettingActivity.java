package com.tencent.assistant.activity.debug;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.Global;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.m;

/* compiled from: ProGuard */
public class ServerAdressSettingActivity extends Activity {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Spinner f472a = null;
    /* access modifiers changed from: private */
    public Spinner b = null;
    private Spinner c = null;
    /* access modifiers changed from: private */
    public int d = 0;
    /* access modifiers changed from: private */
    public TextView e = null;
    /* access modifiers changed from: private */
    public int f = 0;
    /* access modifiers changed from: private */
    public int g = 0;
    /* access modifiers changed from: private */
    public int h = 0;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.server_adress_setting);
        this.e = (TextView) findViewById(R.id.server_name);
        this.e.setText("当前：" + Global.getServerAddressName());
        this.d = m.a().c();
        this.f = m.a().d();
        a();
        b();
        c();
    }

    public boolean a(int i, int i2) {
        if (i == 0) {
            if (i2 >= this.g || i2 < 0) {
                return false;
            }
            return true;
        } else if (1 != i) {
            return false;
        } else {
            if (i2 < this.g || i2 >= this.g + this.h) {
                return false;
            }
            return true;
        }
    }

    public void a() {
        int i;
        this.f472a = (Spinner) findViewById(R.id.spinner1);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, R.layout.server_adress_setting_item);
        if (Global.DIVIDING_POS < Global.SERVER_ENVIRONMENT_NAME.length) {
            for (int i2 = 0; i2 <= Global.DIVIDING_POS; i2++) {
                arrayAdapter.add(Global.SERVER_ENVIRONMENT_NAME[i2]);
            }
            arrayAdapter.add("未设置");
            this.g = arrayAdapter.getCount() > 0 ? arrayAdapter.getCount() - 1 : 0;
            this.f472a.setAdapter((SpinnerAdapter) arrayAdapter);
            this.f472a.setOnItemSelectedListener(new ay(this));
            boolean a2 = a(0, this.d);
            Spinner spinner = this.f472a;
            if (a2) {
                i = this.d;
            } else {
                i = this.g;
            }
            spinner.setSelection(i);
        }
    }

    public void b() {
        int i;
        this.b = (Spinner) findViewById(R.id.spinner2);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, R.layout.server_adress_setting_item);
        if (Global.DIVIDING_POS < Global.SERVER_ENVIRONMENT_NAME.length) {
            int i2 = Global.DIVIDING_POS;
            while (true) {
                i2++;
                if (i2 >= Global.SERVER_ENVIRONMENT_NAME.length) {
                    break;
                }
                arrayAdapter.add(Global.SERVER_ENVIRONMENT_NAME[i2]);
            }
            arrayAdapter.add("未设置");
            this.h = arrayAdapter.getCount() > 0 ? arrayAdapter.getCount() - 1 : 0;
            this.b.setAdapter((SpinnerAdapter) arrayAdapter);
            this.b.setOnItemSelectedListener(new az(this));
            boolean a2 = a(1, this.d);
            Spinner spinner = this.b;
            if (a2) {
                i = this.d - this.g;
            } else {
                i = this.h;
            }
            spinner.setSelection(i);
        }
    }

    public void c() {
        this.c = (Spinner) findViewById(R.id.spinner3);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, R.layout.server_adress_setting_item);
        for (String add : Global.H5_SERVER_ENVIRONMENT_NAMES) {
            arrayAdapter.add(add);
        }
        arrayAdapter.add("未设置");
        this.c.setAdapter((SpinnerAdapter) arrayAdapter);
        this.c.setOnItemSelectedListener(new ba(this));
        int count = this.c.getCount() - 1;
        Spinner spinner = this.c;
        if (this.f >= 0 && this.f < Global.H5_SERVER_ENVIRONMENT_NAMES.length) {
            count = this.f;
        }
        spinner.setSelection(count);
    }

    /* access modifiers changed from: private */
    public void d() {
        AstApp.i().j().sendMessage(AstApp.i().j().obtainMessage(EventDispatcherEnum.UI_EVENT_SERVER_ENVIRONMENT_CHANGE));
    }
}
