package com.tencent.assistant.localres;

import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.m;
import java.util.Comparator;

/* compiled from: ProGuard */
final class p implements Comparator<LocalApkInfo> {
    p() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean
      com.tencent.assistant.m.a(java.lang.String, long):long */
    /* renamed from: a */
    public int compare(LocalApkInfo localApkInfo, LocalApkInfo localApkInfo2) {
        long j;
        long j2;
        int i;
        if (localApkInfo == null || localApkInfo2 == null) {
            return 0;
        }
        if (System.currentTimeMillis() - m.a().a("key_first_load_installed_app_time", 0L) >= 259200000) {
            j = localApkInfo.mFakeLastLaunchTime;
            j2 = localApkInfo2.mFakeLastLaunchTime;
        } else {
            j = localApkInfo.mLastLaunchTime;
            j2 = localApkInfo2.mLastLaunchTime;
        }
        long j3 = localApkInfo2.occupySize - localApkInfo.occupySize;
        if (j3 == 0) {
            if (j > 0 && j2 > 0) {
                long j4 = j - j2;
                if (j4 > 0) {
                    i = 1;
                } else {
                    i = j4 == 0 ? 0 : -1;
                }
                return i;
            } else if (j > 0 && j2 <= 0) {
                return -1;
            } else {
                if (j > 0 || j2 <= 0) {
                    return 0;
                }
                return 1;
            }
        } else if (j3 <= 0) {
            return -1;
        } else {
            return 1;
        }
    }
}
