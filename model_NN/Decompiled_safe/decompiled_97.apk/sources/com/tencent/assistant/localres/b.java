package com.tencent.assistant.localres;

import com.qq.AppService.AstApp;
import com.tencent.assistant.db.table.p;
import com.tencent.assistant.localres.model.LocalApkInfo;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
class b implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ List f821a;
    final /* synthetic */ long b;
    final /* synthetic */ ApkResourceManager c;

    b(ApkResourceManager apkResourceManager, List list, long j) {
        this.c = apkResourceManager;
        this.f821a = list;
        this.b = j;
    }

    public void run() {
        for (String str : this.f821a) {
            LocalApkInfo localApkInfo = (LocalApkInfo) this.c.h.get(str);
            if (localApkInfo != null) {
                localApkInfo.mLastLaunchTime = this.b;
                localApkInfo.mFakeLastLaunchTime = this.b;
                if (this.c.c == null) {
                    p unused = this.c.c = new p(AstApp.i());
                }
                ArrayList arrayList = new ArrayList();
                arrayList.add(str);
                this.c.c.a(arrayList, this.b);
            }
        }
    }
}
