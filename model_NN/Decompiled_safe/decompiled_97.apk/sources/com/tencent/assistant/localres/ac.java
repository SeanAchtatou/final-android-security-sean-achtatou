package com.tencent.assistant.localres;

import com.tencent.assistant.localres.localapk.j;
import com.tencent.assistant.localres.model.LocalApkInfo;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
class ac implements j {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ab f809a;

    ac(ab abVar) {
        this.f809a = abVar;
    }

    public void a(LocalApkInfo localApkInfo) {
        boolean z;
        boolean z2 = false;
        if (localApkInfo != null) {
            if (!this.f809a.c.contains(localApkInfo)) {
                this.f809a.c.add(localApkInfo);
                this.f809a.b.a(localApkInfo);
            } else {
                Iterator it = this.f809a.c.iterator();
                boolean z3 = false;
                while (true) {
                    if (!it.hasNext()) {
                        z2 = z3;
                        break;
                    }
                    LocalApkInfo localApkInfo2 = (LocalApkInfo) it.next();
                    if (!localApkInfo2.equals(localApkInfo)) {
                        z = z3;
                    } else if (localApkInfo2.mLocalFilePath.equals(localApkInfo.mLocalFilePath)) {
                        break;
                    } else {
                        z = true;
                    }
                    z3 = z;
                }
                if (z2) {
                    this.f809a.c.add(localApkInfo);
                    this.f809a.b.a(localApkInfo);
                }
            }
            ArrayList arrayList = new ArrayList(this.f809a.c);
            if (arrayList.size() != 0 && arrayList.size() % 4 == 0) {
                this.f809a.a(arrayList);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.localres.ab.a(com.tencent.assistant.localres.ab, boolean, int):void
     arg types: [com.tencent.assistant.localres.ab, int, int]
     candidates:
      com.tencent.assistant.localres.ab.a(java.lang.String, int, int):com.tencent.assistant.localres.model.LocalApkInfo
      com.tencent.assistant.localres.ab.a(com.tencent.assistant.localres.ab, boolean, int):void */
    public void a(int i) {
        if (!this.f809a.c.isEmpty()) {
            this.f809a.b.b();
            this.f809a.b.c();
            this.f809a.a(true, 0);
            return;
        }
        this.f809a.a(true, -1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.localres.ak.a(com.tencent.assistant.localres.model.LocalApkInfo, boolean):void
     arg types: [com.tencent.assistant.localres.model.LocalApkInfo, int]
     candidates:
      com.tencent.assistant.localres.ak.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, java.lang.String):int
      com.tencent.assistant.localres.ak.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, boolean):java.util.ArrayList<com.tencent.assistant.localres.model.LocalApkInfo>
      com.tencent.assistant.localres.ak.a(java.util.List<com.tencent.assistant.localres.model.LocalApkInfo>, java.util.List<java.lang.Integer>):void
      com.tencent.assistant.localres.ak.a(com.tencent.assistant.localres.model.LocalApkInfo, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.localres.ab.a(com.tencent.assistant.localres.ab, boolean, int):void
     arg types: [com.tencent.assistant.localres.ab, int, int]
     candidates:
      com.tencent.assistant.localres.ab.a(java.lang.String, int, int):com.tencent.assistant.localres.model.LocalApkInfo
      com.tencent.assistant.localres.ab.a(com.tencent.assistant.localres.ab, boolean, int):void */
    public void a(int i, String str, LocalApkInfo localApkInfo) {
        if (localApkInfo != null && this.f809a.e == i) {
            localApkInfo.mApkState = 2;
            this.f809a.c.add(0, localApkInfo);
            this.f809a.f808a.a(localApkInfo);
            this.f809a.b.a(localApkInfo, false);
            this.f809a.b.b();
            this.f809a.b.c();
            this.f809a.a(false, 0);
        }
    }
}
