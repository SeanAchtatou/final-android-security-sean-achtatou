package com.tencent.assistant.d;

import android.content.Context;
import com.tencent.assistant.utils.TemporaryThreadManager;

/* compiled from: ProGuard */
public class j {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static volatile boolean f737a = false;

    public static int[] a(Context context, long j) {
        if (f737a) {
            return null;
        }
        TemporaryThreadManager.get().start(new k(context, j));
        return null;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00a8, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00a9, code lost:
        r7 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00d9, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00da, code lost:
        r7 = r2;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00c5  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00d9 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:4:0x0043] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void c(android.content.Context r10, long r11) {
        /*
            r6 = 0
            r9 = 1039(0x40f, float:1.456E-42)
            r7 = 0
            com.qq.AppService.AstApp r0 = com.qq.AppService.AstApp.i()
            com.tencent.assistant.event.EventDispatcher r8 = r0.j()
            android.content.ContentResolver r0 = r10.getContentResolver()     // Catch:{ Throwable -> 0x00dc }
            android.net.Uri r1 = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI     // Catch:{ Throwable -> 0x00dc }
            r2 = 3
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Throwable -> 0x00dc }
            r3 = 0
            java.lang.String r4 = "_id"
            r2[r3] = r4     // Catch:{ Throwable -> 0x00dc }
            r3 = 1
            java.lang.String r4 = "date_added"
            r2[r3] = r4     // Catch:{ Throwable -> 0x00dc }
            r3 = 2
            java.lang.String r4 = "_data"
            r2[r3] = r4     // Catch:{ Throwable -> 0x00dc }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00dc }
            r3.<init>()     // Catch:{ Throwable -> 0x00dc }
            java.lang.String r4 = "date_added > "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x00dc }
            r4 = 1000(0x3e8, double:4.94E-321)
            long r4 = r11 / r4
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x00dc }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x00dc }
            r4 = 0
            r5 = 0
            android.database.Cursor r2 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Throwable -> 0x00dc }
            if (r2 == 0) goto L_0x00e3
            boolean r0 = r2.moveToFirst()     // Catch:{ Throwable -> 0x00df, all -> 0x00d9 }
            if (r0 == 0) goto L_0x00e3
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ Throwable -> 0x00df, all -> 0x00d9 }
            r1.<init>()     // Catch:{ Throwable -> 0x00df, all -> 0x00d9 }
            java.lang.String r0 = "_data"
            int r0 = r2.getColumnIndexOrThrow(r0)     // Catch:{ Throwable -> 0x00a8, all -> 0x00d9 }
            java.lang.String r3 = "_id"
            int r3 = r2.getColumnIndexOrThrow(r3)     // Catch:{ Throwable -> 0x00a8, all -> 0x00d9 }
        L_0x005a:
            java.lang.String r4 = r2.getString(r0)     // Catch:{ Throwable -> 0x00a8, all -> 0x00d9 }
            com.tencent.connector.ipc.a r5 = com.tencent.connector.ipc.a.a()     // Catch:{ Throwable -> 0x00a8, all -> 0x00d9 }
            int r4 = r5.b(r4)     // Catch:{ Throwable -> 0x00a8, all -> 0x00d9 }
            if (r4 > 0) goto L_0x009c
        L_0x0068:
            boolean r4 = r2.moveToNext()     // Catch:{ Throwable -> 0x00a8, all -> 0x00d9 }
            if (r4 != 0) goto L_0x005a
        L_0x006e:
            if (r2 == 0) goto L_0x0073
            r2.close()
        L_0x0073:
            if (r1 == 0) goto L_0x00d1
            boolean r0 = r1.isEmpty()
            if (r0 != 0) goto L_0x00d1
            int r0 = r1.size()
            int[] r2 = new int[r0]
            java.util.Iterator r3 = r1.iterator()
            r1 = r6
        L_0x0086:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x00c9
            java.lang.Object r0 = r3.next()
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r4 = r0.intValue()
            int r0 = r1 + 1
            r2[r1] = r4
            r1 = r0
            goto L_0x0086
        L_0x009c:
            int r4 = r2.getInt(r3)     // Catch:{ Throwable -> 0x00a8, all -> 0x00d9 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Throwable -> 0x00a8, all -> 0x00d9 }
            r1.add(r4)     // Catch:{ Throwable -> 0x00a8, all -> 0x00d9 }
            goto L_0x0068
        L_0x00a8:
            r0 = move-exception
            r7 = r2
        L_0x00aa:
            r0.printStackTrace()     // Catch:{ all -> 0x00c2 }
            if (r1 == 0) goto L_0x00b2
            r1.clear()     // Catch:{ all -> 0x00c2 }
        L_0x00b2:
            r0 = 1039(0x40f, float:1.456E-42)
            r1 = 0
            android.os.Message r0 = r8.obtainMessage(r0, r1)     // Catch:{ all -> 0x00c2 }
            r8.sendMessage(r0)     // Catch:{ all -> 0x00c2 }
            if (r7 == 0) goto L_0x00c1
            r7.close()
        L_0x00c1:
            return
        L_0x00c2:
            r0 = move-exception
        L_0x00c3:
            if (r7 == 0) goto L_0x00c8
            r7.close()
        L_0x00c8:
            throw r0
        L_0x00c9:
            android.os.Message r0 = r8.obtainMessage(r9, r2)
            r8.sendMessage(r0)
            goto L_0x00c1
        L_0x00d1:
            android.os.Message r0 = r8.obtainMessage(r9, r7)
            r8.sendMessage(r0)
            goto L_0x00c1
        L_0x00d9:
            r0 = move-exception
            r7 = r2
            goto L_0x00c3
        L_0x00dc:
            r0 = move-exception
            r1 = r7
            goto L_0x00aa
        L_0x00df:
            r0 = move-exception
            r1 = r7
            r7 = r2
            goto L_0x00aa
        L_0x00e3:
            r1 = r7
            goto L_0x006e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.d.j.c(android.content.Context, long):void");
    }
}
