package com.tencent.assistant.smartcard.component;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.activity.AppDetailActivityV5;

/* compiled from: ProGuard */
class al extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ STInfoV2 f1713a;
    final /* synthetic */ SmartSquareAppWithUserItem b;

    al(SmartSquareAppWithUserItem smartSquareAppWithUserItem, STInfoV2 sTInfoV2) {
        this.b = smartSquareAppWithUserItem;
        this.f1713a = sTInfoV2;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.b.f1699a, AppDetailActivityV5.class);
        if (this.f1713a != null) {
            intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.f1713a.scene);
        }
        intent.putExtra("simpleModeInfo", this.b.i.f1768a);
        this.b.f1699a.startActivity(intent);
    }

    public STInfoV2 getStInfo() {
        if (this.f1713a != null) {
            this.f1713a.actionId = 200;
            this.f1713a.updateStatusToDetail(this.b.i.f1768a);
        }
        return this.f1713a;
    }
}
