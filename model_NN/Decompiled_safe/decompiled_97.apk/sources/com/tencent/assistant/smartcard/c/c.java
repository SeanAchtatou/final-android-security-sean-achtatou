package com.tencent.assistant.smartcard.c;

import android.content.Context;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.smartcard.component.NormalSmartcardBaseItem;
import com.tencent.assistant.smartcard.component.as;
import com.tencent.assistant.smartcard.d.a;
import com.tencent.assistant.smartcard.d.n;

/* compiled from: ProGuard */
public abstract class c {

    /* renamed from: a  reason: collision with root package name */
    private z f1681a = null;
    private Object b = new Object();

    public abstract NormalSmartcardBaseItem a(Context context, n nVar, as asVar, IViewInvalidater iViewInvalidater);

    public abstract Class<? extends JceStruct> a();

    public abstract a b();

    /* access modifiers changed from: protected */
    public abstract z c();

    public z d() {
        z zVar;
        synchronized (this.b) {
            if (this.f1681a == null) {
                this.f1681a = c();
            }
            zVar = this.f1681a;
        }
        return zVar;
    }
}
