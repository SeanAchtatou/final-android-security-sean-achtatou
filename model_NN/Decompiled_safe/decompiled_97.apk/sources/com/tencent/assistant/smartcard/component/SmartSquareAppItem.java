package com.tencent.assistant.smartcard.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.d;
import com.tencent.assistant.smartcard.d.y;
import com.tencent.assistant.utils.at;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.j;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.component.appdetail.TXDwonloadProcessBar;

/* compiled from: ProGuard */
public class SmartSquareAppItem extends RelativeLayout {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f1698a;
    private LayoutInflater b;
    private TXAppIconView c;
    private TextView d;
    private TextView e;
    private TextView f;
    private DownloadButton g;
    private TXDwonloadProcessBar h;
    /* access modifiers changed from: private */
    public y i;

    public void a(IViewInvalidater iViewInvalidater) {
        this.c.setInvalidater(iViewInvalidater);
    }

    public SmartSquareAppItem(Context context) {
        this(context, null);
    }

    public SmartSquareAppItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f1698a = context;
        this.b = (LayoutInflater) this.f1698a.getSystemService("layout_inflater");
        a();
    }

    private void a() {
        this.b.inflate((int) R.layout.smartcard_square_item_app, this);
        this.c = (TXAppIconView) findViewById(R.id.icon);
        this.d = (TextView) findViewById(R.id.name);
        this.e = (TextView) findViewById(R.id.size);
        this.f = (TextView) findViewById(R.id.desc);
        this.g = (DownloadButton) findViewById(R.id.downloadButton);
        this.h = (TXDwonloadProcessBar) findViewById(R.id.progress);
    }

    public void a(y yVar, STInfoV2 sTInfoV2) {
        this.i = yVar;
        if (this.i != null) {
            if (this.i.f1768a != null) {
                this.c.updateImageView(this.i.f1768a.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            }
            this.d.setText(this.i.f1768a.d);
            this.e.setText(at.a(this.i.f1768a.k));
            this.f.setText(this.i.a());
            this.g.a(this.i.f1768a);
            this.h.a(this.i.f1768a, new View[]{this.e, this.f});
            this.g.a(sTInfoV2, (j) null, (d) null, this.g, this.h);
            setOnClickListener(new ak(this, sTInfoV2));
        }
    }
}
