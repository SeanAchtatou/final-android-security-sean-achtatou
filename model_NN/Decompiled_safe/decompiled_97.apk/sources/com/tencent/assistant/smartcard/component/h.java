package com.tencent.assistant.smartcard.component;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
class h extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1725a;
    final /* synthetic */ STInfoV2 b;
    final /* synthetic */ NormalSmartCardQianghaoItem c;

    h(NormalSmartCardQianghaoItem normalSmartCardQianghaoItem, String str, STInfoV2 sTInfoV2) {
        this.c = normalSmartCardQianghaoItem;
        this.f1725a = str;
        this.b = sTInfoV2;
    }

    public void onTMAClick(View view) {
        b.a(this.c.getContext(), this.f1725a);
    }

    public STInfoV2 getStInfo() {
        if (!(this.b == null || this.c.i == null)) {
            this.b.updateStatusToDetail(this.c.i.b);
        }
        return this.b;
    }
}
