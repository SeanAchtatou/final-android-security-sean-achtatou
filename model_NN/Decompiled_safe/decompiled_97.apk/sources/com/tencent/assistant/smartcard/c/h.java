package com.tencent.assistant.smartcard.c;

import com.tencent.assistant.smartcard.d.b;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.smartcard.d.u;
import com.tencent.assistant.smartcard.d.w;
import com.tencent.assistant.smartcard.d.x;
import java.util.List;

/* compiled from: ProGuard */
public class h extends z {
    public boolean a(n nVar, List<Long> list) {
        if (nVar == null || nVar.j != 2) {
            return false;
        }
        return a((b) nVar, (w) this.f1692a.get(Integer.valueOf(nVar.j)), (x) this.b.get(Integer.valueOf(nVar.j)), (List) this.c.get(Integer.valueOf(nVar.j)), list);
    }

    private boolean a(b bVar, w wVar, x xVar, List<u> list, List<Long> list2) {
        if (xVar == null) {
            return false;
        }
        if (wVar == null) {
            wVar = new w();
            wVar.f = bVar.k;
            wVar.e = bVar.j;
            this.f1692a.put(Integer.valueOf(wVar.e), wVar);
        }
        bVar.a(bVar.j, list);
        bVar.a(list2);
        if (bVar.c == null || bVar.c.size() < xVar.g) {
            a(bVar.t, bVar.k + "||" + bVar.j + "|" + 3, bVar.j);
            return false;
        } else if (wVar.b >= xVar.b) {
            a(bVar.t, bVar.k + "||" + bVar.j + "|" + 1, bVar.j);
            return false;
        } else if (wVar.f1766a < xVar.f1767a) {
            return true;
        } else {
            a(bVar.t, bVar.k + "||" + bVar.j + "|" + 2, bVar.j);
            return false;
        }
    }

    public void a(n nVar) {
        if (nVar != null && nVar.j == 2) {
            b bVar = (b) nVar;
            x xVar = (x) this.b.get(Integer.valueOf(bVar.j));
            if (xVar != null) {
                bVar.q = xVar.d;
                bVar.f1749a = a(bVar, xVar.g, xVar.j);
            }
        }
    }

    private int a(b bVar, int i, int i2) {
        if (!(bVar == null || bVar.c == null)) {
            int size = bVar.c.size();
            if (size >= i && size < i2) {
                return i;
            }
            if (size >= i2) {
                return i2;
            }
        }
        return 0;
    }
}
