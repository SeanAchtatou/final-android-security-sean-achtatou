package com.tencent.assistant.smartcard.d;

import com.tencent.assistant.protocol.jce.SmartCardOp;

/* compiled from: ProGuard */
public class aa extends n {

    /* renamed from: a  reason: collision with root package name */
    public String f1745a;
    public boolean b;
    public int c;
    public int d;
    public int e;

    public boolean a() {
        return this.b;
    }

    public void a(boolean z) {
        this.b = z;
    }

    public void a(SmartCardOp smartCardOp) {
        if (smartCardOp != null) {
            this.l = smartCardOp.b;
            this.n = smartCardOp.c;
            this.o = smartCardOp.e;
            this.p = smartCardOp.j;
            this.f1745a = smartCardOp.d;
            this.k = smartCardOp.f1511a;
            this.b = smartCardOp.f;
            this.c = smartCardOp.g;
            this.d = smartCardOp.h;
            this.e = smartCardOp.i;
        }
    }
}
