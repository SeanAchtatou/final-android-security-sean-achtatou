package com.tencent.assistant.smartcard.component;

import android.widget.ImageView;
import com.tencent.assistantv2.component.k;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.a;

/* compiled from: ProGuard */
class t extends k {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ NormalSmartcardPersonalizedItem f1737a;

    t(NormalSmartcardPersonalizedItem normalSmartcardPersonalizedItem) {
        this.f1737a = normalSmartcardPersonalizedItem;
    }

    public void a(DownloadInfo downloadInfo) {
        a.a().a(downloadInfo);
        com.tencent.assistant.utils.a.a((ImageView) this.f1737a.findViewWithTag(downloadInfo.downloadTicket));
    }
}
