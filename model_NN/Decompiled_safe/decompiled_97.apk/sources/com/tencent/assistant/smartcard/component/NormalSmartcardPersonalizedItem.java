package com.tencent.assistant.smartcard.component;

import android.content.Context;
import android.graphics.Canvas;
import android.text.Html;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXImageView;
import com.tencent.assistant.model.SimpleAppModel;
import com.tencent.assistant.model.d;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.smartcard.d.p;
import com.tencent.assistant.smartcard.d.q;
import com.tencent.assistant.utils.at;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.st.b.e;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;
import com.tencent.pangu.component.appdetail.process.s;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class NormalSmartcardPersonalizedItem extends NormalSmartcardBaseItem {
    private View i;
    private TextView l;
    private TextView m;
    private ImageView n;
    private ImageView o;
    private LinearLayout p;
    /* access modifiers changed from: private */
    public int q;
    private ArrayList<q> r;

    static /* synthetic */ int a(NormalSmartcardPersonalizedItem normalSmartcardPersonalizedItem, int i2) {
        int i3 = normalSmartcardPersonalizedItem.q + i2;
        normalSmartcardPersonalizedItem.q = i3;
        return i3;
    }

    public NormalSmartcardPersonalizedItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public NormalSmartcardPersonalizedItem(Context context, n nVar, as asVar, IViewInvalidater iViewInvalidater) {
        super(context, nVar, asVar, iViewInvalidater);
    }

    public NormalSmartcardPersonalizedItem(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.f) {
            this.f = true;
            d();
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.c = this.b.inflate((int) R.layout.smartcard_personalized, this);
        this.i = findViewById(R.id.title_layout);
        this.l = (TextView) findViewById(R.id.title);
        this.m = (TextView) findViewById(R.id.desc);
        this.n = (ImageView) findViewById(R.id.close);
        this.o = (ImageView) findViewById(R.id.divider);
        this.p = (LinearLayout) findViewById(R.id.app_list);
        this.q = 0;
        f();
    }

    /* access modifiers changed from: protected */
    public void b() {
        f();
    }

    private void f() {
        this.p.removeAllViews();
        p pVar = (p) this.d;
        if (pVar == null || pVar.d <= 0 || pVar.c == null || pVar.c.size() == 0 || pVar.c.size() < pVar.d) {
            a(8);
            setBackgroundResource(17170445);
            setPadding(0, 0, 0, 0);
            return;
        }
        setBackgroundResource(R.drawable.bg_card_selector_padding);
        a(0);
        if (pVar.q) {
            this.n.setVisibility(0);
        } else {
            this.n.setVisibility(8);
        }
        this.n.setOnClickListener(this.j);
        this.l.setText(pVar.l);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.o.getLayoutParams();
        if (TextUtils.isEmpty(pVar.n)) {
            layoutParams.setMargins(0, by.b(14.0f), 0, 0);
            this.m.setVisibility(8);
        } else {
            layoutParams.setMargins(0, by.b(9.0f), 0, 0);
            this.m.setText(pVar.n);
            this.m.setVisibility(0);
        }
        ArrayList<q> g = g();
        if (this.r == null) {
            this.r = new ArrayList<>();
        }
        this.r.clear();
        this.r.addAll(g);
        int i2 = 0;
        while (i2 < g.size()) {
            View a2 = a(g.get(i2), i2, i2 == g.size() + -1);
            if (a2 != null) {
                this.p.addView(a2);
            }
            i2++;
        }
        if (pVar.c.size() >= pVar.d * 2) {
            View a3 = a(pVar.p);
            a3.setTag(R.id.tma_st_smartcard_tag, e());
            a3.setOnClickListener(new s(this, pVar));
            this.p.addView(a3, new LinearLayout.LayoutParams(-1, by.b(40.0f)));
        }
    }

    private ArrayList<q> g() {
        ArrayList<q> arrayList = new ArrayList<>();
        ArrayList arrayList2 = new ArrayList(((p) this.d).c);
        int i2 = 0;
        int i3 = this.q;
        while (true) {
            int i4 = i2;
            if (i4 >= ((p) this.d).d) {
                return arrayList;
            }
            int size = i3 % ((p) this.d).c.size();
            arrayList.add(arrayList2.get(size));
            i3 = size + 1;
            i2 = i4 + 1;
        }
    }

    private View a(q qVar, int i2, boolean z) {
        if (qVar == null || qVar.f1760a == null) {
            return null;
        }
        STInfoV2 a2 = a(qVar, i2);
        View inflate = this.b.inflate((int) R.layout.smartcard_personalized_item, (ViewGroup) null);
        TXImageView tXImageView = (TXImageView) inflate.findViewById(R.id.icon);
        tXImageView.updateImageView(qVar.f1760a.e, R.drawable.pic_defaule, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
        ((TextView) inflate.findViewById(R.id.name)).setText(qVar.f1760a.d);
        DownloadButton downloadButton = (DownloadButton) inflate.findViewById(R.id.btn);
        downloadButton.a(qVar.f1760a);
        SimpleAppModel simpleAppModel = qVar.f1760a;
        tXImageView.setTag(simpleAppModel.q());
        downloadButton.setTag(R.id.tma_st_smartcard_tag, e());
        if (s.a(simpleAppModel)) {
            downloadButton.setClickable(false);
        } else {
            downloadButton.setClickable(true);
            downloadButton.a(a2, new t(this), (d) null, downloadButton);
        }
        inflate.setTag(R.id.tma_st_smartcard_tag, e());
        inflate.setOnClickListener(new u(this, simpleAppModel, a2));
        ((TextView) inflate.findViewById(R.id.app_size)).setText(at.a(simpleAppModel.k));
        TXImageView tXImageView2 = (TXImageView) inflate.findViewById(R.id.nicke_cion_1);
        TXImageView tXImageView3 = (TXImageView) inflate.findViewById(R.id.nicke_cion_2);
        if (qVar.c != null) {
            if (qVar.c.size() > 0) {
                tXImageView2.setVisibility(0);
                tXImageView2.updateImageView(qVar.c.get(0).b, R.drawable.tab_my_face, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            } else {
                tXImageView2.setVisibility(8);
            }
            if (qVar.c.size() > 1) {
                tXImageView3.setVisibility(0);
                tXImageView3.updateImageView(qVar.c.get(1).b, R.drawable.tab_my_face, TXImageView.TXImageViewType.NETWORK_IMAGE_ICON);
            } else {
                tXImageView3.setVisibility(8);
            }
        }
        ((TextView) inflate.findViewById(R.id.desc)).setText(Html.fromHtml(qVar.b));
        ImageView imageView = (ImageView) inflate.findViewById(R.id.divider);
        if (z) {
            imageView.setVisibility(8);
        } else {
            imageView.setVisibility(0);
        }
        return inflate;
    }

    private void a(int i2) {
        this.c.setVisibility(i2);
        this.i.setVisibility(i2);
        this.p.setVisibility(i2);
    }

    private View a(String str) {
        View inflate = this.b.inflate((int) R.layout.smartcard_list_footer, (ViewGroup) null);
        ((TextView) inflate.findViewById(R.id.text)).setText(str);
        ImageView imageView = (ImageView) inflate.findViewById(R.id.icon);
        imageView.setImageResource(R.drawable.next);
        imageView.setVisibility(0);
        return inflate;
    }

    private STInfoV2 a(q qVar, int i2) {
        STInfoV2 a2 = a(a.a("03", i2), 100);
        if (!(a2 == null || qVar == null)) {
            a2.updateWithSimpleAppModel(qVar.f1760a);
        }
        if (this.h == null) {
            this.h = new e();
        }
        this.h.a(a2);
        return a2;
    }
}
