package com.tencent.assistant.smartcard.component;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.manager.SelfUpdateManager;

/* compiled from: ProGuard */
class j extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SelfUpdateManager.SelfUpdateInfo f1727a;
    final /* synthetic */ STInfoV2 b;
    final /* synthetic */ NormalSmartCardSelfUpdateItem c;

    j(NormalSmartCardSelfUpdateItem normalSmartCardSelfUpdateItem, SelfUpdateManager.SelfUpdateInfo selfUpdateInfo, STInfoV2 sTInfoV2) {
        this.c = normalSmartCardSelfUpdateItem;
        this.f1727a = selfUpdateInfo;
        this.b = sTInfoV2;
    }

    public void onTMAClick(View view) {
        if (this.c.w == this.f1727a.f3781a) {
            long unused = this.c.w = 0;
            this.c.u.setVisibility(0);
            this.c.v.setVisibility(8);
            this.c.s.setImageDrawable(this.c.f1693a.getResources().getDrawable(R.drawable.icon_open));
            return;
        }
        long unused2 = this.c.w = this.f1727a.f3781a;
        this.c.u.setVisibility(8);
        this.c.v.setVisibility(0);
        this.c.s.setImageDrawable(this.c.f1693a.getResources().getDrawable(R.drawable.icon_close));
    }

    public STInfoV2 getStInfo() {
        return this.b;
    }
}
