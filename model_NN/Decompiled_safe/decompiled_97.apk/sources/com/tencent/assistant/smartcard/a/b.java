package com.tencent.assistant.smartcard.a;

import android.content.Context;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.protocol.jce.SmartCardTemplate3;
import com.tencent.assistant.smartcard.c.c;
import com.tencent.assistant.smartcard.c.z;
import com.tencent.assistant.smartcard.component.NormalSmartcardBaseItem;
import com.tencent.assistant.smartcard.component.as;
import com.tencent.assistant.smartcard.d.a;
import com.tencent.assistant.smartcard.d.j;
import com.tencent.assistant.smartcard.d.n;
import com.tencent.assistant.smartcard.view.NormalSmartCardGiftTemplateItem;

/* compiled from: ProGuard */
public class b extends c {
    public Class<? extends JceStruct> a() {
        return SmartCardTemplate3.class;
    }

    public a b() {
        return new j();
    }

    /* access modifiers changed from: protected */
    public z c() {
        return new com.tencent.assistant.smartcard.e.b();
    }

    public NormalSmartcardBaseItem a(Context context, n nVar, as asVar, IViewInvalidater iViewInvalidater) {
        return new NormalSmartCardGiftTemplateItem(context, nVar, asVar, iViewInvalidater);
    }
}
