package com.tencent.assistant.smartcard.component;

import android.content.Intent;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.activity.AppDetailActivityV5;

/* compiled from: ProGuard */
class ak extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ STInfoV2 f1712a;
    final /* synthetic */ SmartSquareAppItem b;

    ak(SmartSquareAppItem smartSquareAppItem, STInfoV2 sTInfoV2) {
        this.b = smartSquareAppItem;
        this.f1712a = sTInfoV2;
    }

    public void onTMAClick(View view) {
        Intent intent = new Intent(this.b.f1698a, AppDetailActivityV5.class);
        if (this.f1712a != null) {
            intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.f1712a.scene);
        }
        intent.putExtra("simpleModeInfo", this.b.i.f1768a);
        this.b.f1698a.startActivity(intent);
    }

    public STInfoV2 getStInfo() {
        if (this.f1712a != null) {
            this.f1712a.actionId = 200;
            this.f1712a.updateStatusToDetail(this.b.i.f1768a);
        }
        return this.f1712a;
    }
}
