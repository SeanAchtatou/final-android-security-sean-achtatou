package com.tencent.assistant.adapter;

import android.view.View;
import android.widget.TextView;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistant.k;
import com.tencent.assistantv2.model.ItemElement;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
class i extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ j f593a;
    final /* synthetic */ ItemElement b;
    final /* synthetic */ int c;
    final /* synthetic */ ChildSettingAdapter d;

    i(ChildSettingAdapter childSettingAdapter, j jVar, ItemElement itemElement, int i) {
        this.d = childSettingAdapter;
        this.f593a = jVar;
        this.b = itemElement;
        this.c = i;
    }

    public void onTMAClick(View view) {
        this.f593a.e.setSelected(!this.f593a.e.isSelected());
        k.a(this.b.d, this.f593a.e.isSelected());
    }

    public STInfoV2 getStInfo(View view) {
        if (!(view instanceof TextView)) {
            return null;
        }
        return this.d.a(this.d.a(this.c), this.d.a(view.isSelected()), 200);
    }
}
