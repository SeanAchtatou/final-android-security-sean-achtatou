package com.tencent.assistant.adapter;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.k;
import com.tencent.assistant.module.bb;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.bm;
import com.tencent.assistant.utils.by;
import com.tencent.assistantv2.manager.a;
import com.tencent.assistantv2.model.ItemElement;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.STPageInfo;
import com.tencent.nucleus.manager.component.SwitchButton;
import com.tencent.nucleus.manager.floatingwindow.n;
import com.tencent.nucleus.socialcontact.login.j;
import com.tencent.nucleus.socialcontact.login.l;
import com.tencent.nucleus.socialcontact.login.m;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public class y extends BaseExpandableListAdapter {

    /* renamed from: a  reason: collision with root package name */
    protected STPageInfo f609a = null;
    public boolean b = false;
    public boolean c = false;
    /* access modifiers changed from: private */
    public Context d;
    private LayoutInflater e;
    private Map<String, List<ItemElement>> f = new HashMap();
    private Map<String, Integer> g = new HashMap();
    private List<String> h = new ArrayList();
    private STInfoV2 i;
    /* access modifiers changed from: private */
    public volatile boolean j = false;
    /* access modifiers changed from: private */
    public Handler k = new ac(this);

    public y(Context context) {
        this.d = context;
        if (this.d instanceof BaseActivity) {
            this.f609a = ((BaseActivity) this.d).n();
        } else {
            this.f609a = new STPageInfo();
        }
        this.e = LayoutInflater.from(context);
    }

    public void a(String str, int i2, int i3, List<ItemElement> list) {
        if (i3 <= this.h.size() && i2 >= 0) {
            if (i3 == -1) {
                this.h.add(str);
            } else {
                this.h.add(i3, str);
            }
            this.g.put(str, Integer.valueOf(i2));
            this.f.put(str, list);
        }
    }

    public void a() {
        if (this.h != null && this.h.size() > 0) {
            this.h.clear();
        }
        if (this.f != null && this.f.size() > 0) {
            this.f.clear();
        }
    }

    public ItemElement a(int i2, int i3) {
        List list;
        String str = this.h.get(i2);
        if (TextUtils.isEmpty(str) || (list = this.f.get(str)) == null || i3 >= list.size()) {
            return null;
        }
        return (ItemElement) list.get(i3);
    }

    public int b(int i2, int i3) {
        ItemElement itemElement;
        List list;
        String str = this.h.get(i2);
        if (TextUtils.isEmpty(str) || (list = this.f.get(str)) == null || i3 >= list.size()) {
            itemElement = null;
        } else {
            itemElement = (ItemElement) list.get(i3);
        }
        if (itemElement != null) {
            return itemElement.c;
        }
        return 0;
    }

    public ItemElement c(int i2, int i3) {
        List list;
        String str = this.h.get(i2);
        if (TextUtils.isEmpty(str) || (list = this.f.get(str)) == null || i3 >= list.size()) {
            return null;
        }
        return (ItemElement) list.get(i3);
    }

    public View getChildView(int i2, int i3, boolean z, View view, ViewGroup viewGroup) {
        ItemElement c2 = c(i2, i3);
        if (c2 != null) {
            switch (b(i2, i3)) {
                case 0:
                    view = a(c2, view, i2, i3, z);
                    break;
                case 1:
                    view = b(c2, view, i2, i3, z);
                    break;
            }
            view.setOnClickListener(new z(this, i2, i3));
            if (!z || i2 != getGroupCount() - 1) {
                view.setPadding(0, 0, 0, 0);
            } else {
                view.setPadding(0, 0, 0, by.a(this.d, 5.0f));
            }
        }
        return view;
    }

    private View a(ItemElement itemElement, View view, int i2, int i3, boolean z) {
        ad adVar;
        if (view == null || !(view.getTag() instanceof ad)) {
            view = this.e.inflate((int) R.layout.setting_item_arrow, (ViewGroup) null);
            adVar = new ad(this, null);
            adVar.f583a = view.findViewById(R.id.item_layout);
            adVar.b = view.findViewById(R.id.top_margin);
            adVar.c = (TextView) view.findViewById(R.id.item_title);
            adVar.d = (TextView) view.findViewById(R.id.item_description);
            adVar.e = (LinearLayout) view.findViewById(R.id.item_left_layout);
            adVar.f = view.findViewById(R.id.item_line);
            adVar.g = view.findViewById(R.id.bottom_margin);
            view.setTag(adVar);
        } else {
            adVar = (ad) view.getTag();
        }
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) adVar.f583a.getLayoutParams();
        marginLayoutParams.setMargins(marginLayoutParams.leftMargin, by.b((float) itemElement.e), marginLayoutParams.rightMargin, marginLayoutParams.bottomMargin);
        adVar.f583a.setLayoutParams(marginLayoutParams);
        adVar.c.setText(itemElement.f2018a);
        if (itemElement.b != null) {
            adVar.d.setText(itemElement.b);
            adVar.d.setVisibility(0);
            adVar.e.setPadding(0, by.a(this.d, 20.0f), 0, by.a(this.d, 17.0f));
        } else {
            adVar.e.setPadding(0, 0, 0, 0);
            adVar.d.setVisibility(8);
        }
        adVar.f583a.setBackgroundResource(R.drawable.helper_cardbg_all_selector_new);
        if (z) {
            adVar.f.setVisibility(8);
            adVar.b.setVisibility(8);
            adVar.g.setVisibility(0);
        } else if (i3 == 0) {
            adVar.f.setVisibility(0);
            adVar.b.setVisibility(0);
            adVar.g.setVisibility(8);
        } else {
            adVar.f.setVisibility(0);
            adVar.b.setVisibility(8);
            adVar.g.setVisibility(8);
        }
        return view;
    }

    private View b(ItemElement itemElement, View view, int i2, int i3, boolean z) {
        af afVar;
        String string;
        int i4 = itemElement.d;
        if (view == null || !(view.getTag() instanceof af)) {
            view = this.e.inflate((int) R.layout.setting_item_switch, (ViewGroup) null);
            afVar = new af(this);
            afVar.f584a = view.findViewById(R.id.item_layout);
            afVar.b = view.findViewById(R.id.top_margin);
            afVar.c = (TextView) view.findViewById(R.id.item_title);
            afVar.d = (TextView) view.findViewById(R.id.item_description);
            afVar.e = (TextView) view.findViewById(R.id.item_sub_description);
            afVar.f = (LinearLayout) view.findViewById(R.id.item_mid_layout);
            afVar.h = view.findViewById(R.id.item_line);
            afVar.i = view.findViewById(R.id.bottom_margin);
            afVar.g = (SwitchButton) view.findViewById(R.id.item_switch);
            afVar.g.a(this.d.getString(R.string.setting_left_title), this.d.getString(R.string.setting_right_title));
            view.setTag(afVar);
        } else {
            afVar = (af) view.getTag();
        }
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) afVar.f584a.getLayoutParams();
        marginLayoutParams.setMargins(marginLayoutParams.leftMargin, by.b((float) itemElement.e), marginLayoutParams.rightMargin, marginLayoutParams.bottomMargin);
        afVar.f584a.setLayoutParams(marginLayoutParams);
        afVar.c.setText(itemElement.f2018a);
        afVar.f584a.setBackgroundResource(R.drawable.helper_cardbg_all_selector_new);
        if (itemElement.b != null) {
            afVar.d.setText(itemElement.b);
            afVar.d.setVisibility(0);
            afVar.f.setPadding(0, by.a(this.d, 20.0f), 0, by.a(this.d, 17.0f));
        } else {
            afVar.d.setVisibility(8);
            afVar.f.setPadding(0, 0, 0, 0);
        }
        afVar.g.a(new aa(this, i4, i3, i2));
        if (i4 == 8) {
            afVar.e.setVisibility(0);
            if (j.a().j()) {
                m f2 = l.f();
                afVar.g.a((f2.c & 1) == 1);
                if ((f2.c & 1) == 1) {
                    string = this.d.getString(R.string.setting_privacy_protection_item_sub_des_on);
                } else {
                    string = this.d.getString(R.string.setting_privacy_protection_item_sub_des_off);
                }
                if (j.a().k()) {
                    afVar.e.setText(string + "   " + this.d.getString(R.string.setting_privacy_protection_item_sub_des_qq) + j.a().p());
                }
                if (j.a().l()) {
                    afVar.e.setText(string + "   " + this.d.getString(R.string.setting_privacy_protection_item_sub_des_wx) + j.a().q());
                }
            } else {
                afVar.e.setText(this.d.getString(R.string.setting_privacy_protection_item_sub_des));
                afVar.g.a(false);
            }
        } else {
            afVar.e.setVisibility(8);
            afVar.g.a(k.a(itemElement.d));
        }
        if (z) {
            afVar.h.setVisibility(8);
            afVar.b.setVisibility(8);
            afVar.i.setVisibility(0);
        } else if (i3 == 0) {
            afVar.h.setVisibility(0);
            afVar.b.setVisibility(0);
            afVar.i.setVisibility(8);
        } else {
            afVar.h.setVisibility(0);
            afVar.b.setVisibility(8);
            afVar.i.setVisibility(8);
        }
        return view;
    }

    public void a(int i2, View view, int i3, boolean z) {
        switch (i2) {
            case 1:
                k.a(i2, z);
                AstApp.i().j().sendEmptyMessage(EventDispatcherEnum.UI_EVENT_AUTO_UPDATE_SWITCH_VALUE_CHANGE);
                bb.a().a(z, AppConst.WISE_DOWNLOAD_SWITCH_TYPE.UPDATE);
                return;
            case 2:
                k.a(i2, z);
                AstApp.i().j().sendEmptyMessage(EventDispatcherEnum.UI_EVENT_AUTO_NEW_DOWNLOAD_SWITCH_VALUE_CHANGE);
                bb.a().a(z, AppConst.WISE_DOWNLOAD_SWITCH_TYPE.NEW_DOWNLOAD);
                return;
            case 3:
                a(view);
                return;
            case 6:
                k.a(i2, z);
                a.a().b().a(z);
                return;
            case 8:
                if (j.a().j()) {
                    bb.a().a(z, 4);
                    return;
                }
                this.b = z;
                b();
                return;
            case 16:
                k.a(i2, z);
                a(z);
                return;
            default:
                k.a(i2, z);
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
     arg types: [java.lang.String, boolean]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(int, byte[]):void
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean */
    private void a(boolean z) {
        if (z) {
            if (!com.tencent.assistant.m.a().a("key_has_show_float_window_create_tips", false)) {
                com.tencent.assistant.m.a().b("key_has_show_float_window_create_tips", (Object) true);
            }
            n.a().c();
        }
    }

    private void a(View view) {
        View view2;
        if (view instanceof RelativeLayout) {
            view2 = view.findViewById(R.id.item_switch);
        } else {
            view2 = view;
        }
        SwitchButton switchButton = (SwitchButton) view2;
        if (com.tencent.assistant.m.a().j()) {
            k.a(3, false);
        } else if (!this.j) {
            this.c = true;
            TemporaryThreadManager.get().start(new ab(this, switchButton));
        }
    }

    public Object getChild(int i2, int i3) {
        List list;
        String str = this.h.get(i2);
        if (TextUtils.isEmpty(str) || (list = this.f.get(str)) == null || i3 >= list.size()) {
            return null;
        }
        return (ItemElement) list.get(i3);
    }

    public long getChildId(int i2, int i3) {
        return (long) i3;
    }

    public int getChildrenCount(int i2) {
        List list;
        String str = this.h.get(i2);
        if (TextUtils.isEmpty(str) || (list = this.f.get(str)) == null) {
            return 0;
        }
        return list.size();
    }

    public Object getGroup(int i2) {
        return this.h.get(i2);
    }

    public int getGroupCount() {
        return this.f.size();
    }

    public long getGroupId(int i2) {
        return (long) i2;
    }

    public View getGroupView(int i2, boolean z, View view, ViewGroup viewGroup) {
        if (this.e == null) {
            this.e = LayoutInflater.from(this.d);
        }
        View inflate = this.e.inflate((int) R.layout.setting_group_item, (ViewGroup) null);
        ((TextView) inflate.findViewById(R.id.group_title)).setText(this.d.getResources().getString(this.g.get(this.h.get(i2)).intValue()));
        return inflate;
    }

    public boolean hasStableIds() {
        return false;
    }

    public boolean isChildSelectable(int i2, int i3) {
        return true;
    }

    private void b() {
        Bundle bundle = new Bundle();
        bundle.putInt(AppConst.KEY_LOGIN_TYPE, 2);
        bundle.putInt(AppConst.KEY_FROM_TYPE, 15);
        j.a().a(AppConst.IdentityType.MOBILEQ, bundle);
    }

    /* access modifiers changed from: private */
    public String d(int i2, int i3) {
        switch (i2) {
            case 0:
                return "03_" + bm.a(i3 + 1);
            case 1:
                return "04_" + bm.a(i3 + 1);
            case 2:
                return "05_" + bm.a(i3 + 1);
            case 3:
                return "06_" + bm.a(i3 + 1);
            case 4:
                return "07_" + bm.a(i3 + 1);
            default:
                return null;
        }
    }

    /* access modifiers changed from: private */
    public String b(boolean z) {
        if (z) {
            return "01";
        }
        return "02";
    }

    /* access modifiers changed from: private */
    public STInfoV2 a(String str, String str2, int i2) {
        if (this.i == null) {
            this.i = new STInfoV2(this.f609a.f2060a, str, this.f609a.c, this.f609a.d, i2);
        }
        this.i.status = str2;
        this.i.slotId = str;
        this.i.actionId = i2;
        return this.i;
    }
}
