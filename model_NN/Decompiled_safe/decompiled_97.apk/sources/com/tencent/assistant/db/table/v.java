package com.tencent.assistant.db.table;

import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import com.qq.AppService.AstApp;
import com.tencent.assistant.db.helper.AstDbHelper;
import com.tencent.assistant.db.helper.SqliteHelper;

/* compiled from: ProGuard */
public class v implements IBaseTable {

    /* renamed from: a  reason: collision with root package name */
    public static final Uri f753a = Uri.parse("content://com.tencent.android.qqdownloader.external.provider/qube_theme_download_status");

    public int tableVersion() {
        return 1;
    }

    public String tableName() {
        return "qube_theme_download_status";
    }

    public String createTableSQL() {
        return "CREATE TABLE qube_theme_download_status (package  TEXT UNIQUE ON CONFLICT REPLACE PRIMARY KEY,state VARCHAR(16));";
    }

    public String[] getAlterSQL(int i, int i2) {
        return null;
    }

    public void beforeTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public void afterTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public SqliteHelper getHelper() {
        return AstDbHelper.get(AstApp.i());
    }
}
