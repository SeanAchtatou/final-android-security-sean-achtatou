package com.tencent.assistant.db.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.tencent.a.a.a;
import com.tencent.assistant.db.table.m;
import com.tencent.assistant.db.table.q;
import com.tencent.assistant.db.table.s;

/* compiled from: ProGuard */
public class DownloadDbHelper extends SqliteHelper {
    private static final String DB_NAME = "mobile_ast_download2.db";
    private static final int DB_VERSION = 11;
    private static final Class<?>[] TABLESS = {q.class, a.class, m.class, s.class};
    private static final String TAG = DownloadDbHelper.class.getSimpleName();
    private static volatile SqliteHelper instance;

    public static synchronized SqliteHelper get(Context context) {
        SqliteHelper sqliteHelper;
        synchronized (DownloadDbHelper.class) {
            if (instance == null) {
                instance = new DownloadDbHelper(context, DB_NAME, null, 11);
            }
            sqliteHelper = instance;
        }
        return sqliteHelper;
    }

    public DownloadDbHelper(Context context, String str, SQLiteDatabase.CursorFactory cursorFactory, int i) {
        super(context, DB_NAME, null, i);
    }

    public Class<?>[] getTables() {
        return TABLESS;
    }

    public int getDBVersion() {
        return 11;
    }
}
