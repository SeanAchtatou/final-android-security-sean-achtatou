package com.tencent.assistant.db.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.tencent.assistant.db.table.ai;
import com.tencent.assistant.db.table.h;
import com.tencent.assistant.db.table.n;
import com.tencent.nucleus.socialcontact.tagpage.k;

/* compiled from: ProGuard */
public class MediaDbHelper extends SqliteHelper {
    private static final String DB_NAME = "mobile_media.db";
    private static final int DB_VERSION = 3;
    private static final Class<?>[] TABLESS = {ai.class, n.class, h.class, k.class};
    private static volatile SqliteHelper instance;

    public static synchronized SqliteHelper get(Context context) {
        SqliteHelper sqliteHelper;
        synchronized (MediaDbHelper.class) {
            if (instance == null) {
                instance = new MediaDbHelper(context, DB_NAME, null, 3);
            }
            sqliteHelper = instance;
        }
        return sqliteHelper;
    }

    public MediaDbHelper(Context context, String str, SQLiteDatabase.CursorFactory cursorFactory, int i) {
        super(context, DB_NAME, null, i);
    }

    public Class<?>[] getTables() {
        return TABLESS;
    }

    public int getDBVersion() {
        return 3;
    }
}
