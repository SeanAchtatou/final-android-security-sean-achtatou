package com.tencent.assistant.db.table;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.qq.AppService.AstApp;
import com.tencent.assistant.db.helper.LocalApkDbHelper;
import com.tencent.assistant.db.helper.SqliteHelper;
import com.tencent.assistant.localres.model.LocalApkInfo;

/* compiled from: ProGuard */
public class o implements IBaseTable {
    public int tableVersion() {
        return 1;
    }

    public String tableName() {
        return "local_apkinfo";
    }

    public String createTableSQL() {
        return "CREATE TABLE if not exists local_apkinfo (_id INTEGER PRIMARY KEY AUTOINCREMENT,iconres INTEGER,pkgname TEXT,vercode TEXT,vername TEXT,filepath TEXT,signature TEXT,grayvercode INTEGER,appname TEXT,occupysize INTEGER,lastmodified INTEGER,isinternal INTEGER);";
    }

    public String[] getAlterSQL(int i, int i2) {
        return null;
    }

    public void beforeTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public void afterTableAlter(int i, int i2, SQLiteDatabase sQLiteDatabase) {
    }

    public SqliteHelper getHelper() {
        return LocalApkDbHelper.get(AstApp.i());
    }

    /* access modifiers changed from: protected */
    public LocalApkInfo a(Cursor cursor) {
        boolean z = true;
        LocalApkInfo localApkInfo = new LocalApkInfo();
        localApkInfo.mAppIconRes = cursor.getInt(cursor.getColumnIndex("iconres"));
        localApkInfo.mPackageName = cursor.getString(cursor.getColumnIndex("pkgname"));
        localApkInfo.mVersionCode = cursor.getInt(cursor.getColumnIndex("vercode"));
        localApkInfo.mVersionName = cursor.getString(cursor.getColumnIndex("vername"));
        localApkInfo.mLocalFilePath = cursor.getString(cursor.getColumnIndex("filepath"));
        localApkInfo.signature = cursor.getString(cursor.getColumnIndex("signature"));
        localApkInfo.mGrayVersionCode = cursor.getInt(cursor.getColumnIndex("grayvercode"));
        localApkInfo.mAppName = cursor.getString(cursor.getColumnIndex("appname"));
        localApkInfo.occupySize = cursor.getLong(cursor.getColumnIndex("occupysize"));
        localApkInfo.mLastModified = cursor.getLong(cursor.getColumnIndex("lastmodified"));
        if (cursor.getInt(cursor.getColumnIndex("isinternal")) != 1) {
            z = false;
        }
        localApkInfo.mIsInternalDownload = z;
        return localApkInfo;
    }

    public synchronized boolean a(LocalApkInfo localApkInfo) {
        ContentValues contentValues;
        contentValues = new ContentValues();
        contentValues.put("iconres", Integer.valueOf(localApkInfo.mAppIconRes));
        contentValues.put("pkgname", localApkInfo.mPackageName);
        contentValues.put("vercode", Integer.valueOf(localApkInfo.mVersionCode));
        contentValues.put("vername", localApkInfo.mVersionName);
        contentValues.put("filepath", localApkInfo.mLocalFilePath);
        contentValues.put("signature", localApkInfo.signature);
        contentValues.put("grayvercode", Integer.valueOf(localApkInfo.mGrayVersionCode));
        contentValues.put("appname", localApkInfo.mAppName);
        contentValues.put("occupysize", Long.valueOf(localApkInfo.occupySize));
        contentValues.put("lastmodified", Long.valueOf(localApkInfo.mLastModified));
        contentValues.put("isinternal", Boolean.valueOf(localApkInfo.mIsInternalDownload));
        return getHelper().getWritableDatabaseWrapper().insert("local_apkinfo", null, contentValues) > 0;
    }

    public synchronized void a(String str) {
        getHelper().getWritableDatabaseWrapper().delete("local_apkinfo", "filepath= ?", new String[]{str});
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0046  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<com.tencent.assistant.localres.model.LocalApkInfo> a() {
        /*
            r10 = this;
            r9 = 0
            java.util.ArrayList r8 = new java.util.ArrayList
            r8.<init>()
            com.tencent.assistant.db.helper.SqliteHelper r0 = r10.getHelper()
            com.tencent.assistant.db.helper.SQLiteDatabaseWrapper r0 = r0.getReadableDatabaseWrapper()
            java.lang.String r1 = "local_apkinfo"
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x0036, all -> 0x0042 }
            if (r1 == 0) goto L_0x002f
            boolean r0 = r1.moveToFirst()     // Catch:{ Exception -> 0x004c }
            if (r0 == 0) goto L_0x002f
        L_0x0022:
            com.tencent.assistant.localres.model.LocalApkInfo r0 = r10.a(r1)     // Catch:{ Exception -> 0x004c }
            r8.add(r0)     // Catch:{ Exception -> 0x004c }
            boolean r0 = r1.moveToNext()     // Catch:{ Exception -> 0x004c }
            if (r0 != 0) goto L_0x0022
        L_0x002f:
            if (r1 == 0) goto L_0x0034
            r1.close()
        L_0x0034:
            r0 = r8
        L_0x0035:
            return r0
        L_0x0036:
            r0 = move-exception
            r1 = r9
        L_0x0038:
            r0.printStackTrace()     // Catch:{ all -> 0x004a }
            if (r1 == 0) goto L_0x0040
            r1.close()
        L_0x0040:
            r0 = r8
            goto L_0x0035
        L_0x0042:
            r0 = move-exception
            r1 = r9
        L_0x0044:
            if (r1 == 0) goto L_0x0049
            r1.close()
        L_0x0049:
            throw r0
        L_0x004a:
            r0 = move-exception
            goto L_0x0044
        L_0x004c:
            r0 = move-exception
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.assistant.db.table.o.a():java.util.List");
    }
}
