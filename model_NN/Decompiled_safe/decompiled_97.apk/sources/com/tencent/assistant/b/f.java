package com.tencent.assistant.b;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.telephony.CellLocation;
import android.telephony.NeighboringCellInfo;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import com.qq.AppService.AstApp;
import com.tencent.assistant.protocol.jce.LbsCell;
import com.tencent.assistant.protocol.jce.LbsData;
import com.tencent.assistant.protocol.jce.LbsWifiMac;
import com.tencent.assistant.utils.XLog;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ProGuard */
public class f {

    /* renamed from: a  reason: collision with root package name */
    private static f f616a;
    private static ArrayList<String> u = new ArrayList<>();
    private Context b;
    private List<b> c;
    private Handler d;
    private TelephonyManager e;
    private WifiManager f;
    private BroadcastReceiver g;
    /* access modifiers changed from: private */
    public boolean h;
    private int i;
    private int j;
    private int k;
    private int l;
    private ArrayList<NeighboringCellInfo> m;
    private ArrayList<LbsWifiMac> n;
    private Object o;
    private boolean p;
    private boolean q;
    private long r;
    private boolean s;
    private Runnable t;

    public static synchronized f a() {
        f fVar;
        synchronized (f.class) {
            if (f616a == null) {
                f616a = new f();
            }
            fVar = f616a;
        }
        return fVar;
    }

    static {
        u.add("GT-I9100");
    }

    public f() {
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = false;
        this.m = null;
        this.n = null;
        this.o = new Object();
        this.p = false;
        this.q = false;
        this.s = false;
        this.t = new g(this);
        this.b = AstApp.i();
        this.c = new ArrayList();
        this.d = new Handler(Looper.getMainLooper());
        this.e = (TelephonyManager) this.b.getSystemService("phone");
        this.f = (WifiManager) this.b.getSystemService("wifi");
        k();
    }

    public void b() {
        if (!this.s) {
            this.s = true;
            k();
            if (m()) {
                this.p = g();
                this.r = System.currentTimeMillis();
            }
            if (n()) {
                this.q = h();
                this.r = System.currentTimeMillis();
                return;
            }
            l();
        }
    }

    public void c() {
        d();
    }

    public void a(b bVar) {
        int i2;
        boolean z = true;
        if (bVar != null) {
            long currentTimeMillis = System.currentTimeMillis() - this.r;
            if (currentTimeMillis < 0 || currentTimeMillis > 600000 || (!this.q && !this.p)) {
                if (this.c == null) {
                    this.c = new ArrayList();
                }
                Iterator<b> it = this.c.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (it.next() == bVar) {
                            break;
                        }
                    } else {
                        z = false;
                        break;
                    }
                }
                if (!z) {
                    this.c.add(bVar);
                }
                b();
                return;
            }
            if (this.l != 0) {
                i2 = 1;
            } else {
                i2 = 0;
            }
            if (this.n != null && this.n.size() > 0) {
                i2 |= 2;
            }
            bVar.a(i2, e());
        }
    }

    public synchronized void d() {
        if (this.h) {
            f();
        }
    }

    public LbsData e() {
        a aVar = new a();
        ArrayList arrayList = new ArrayList();
        arrayList.add(new LbsCell(this.i, this.j, this.k, this.l, 0));
        Iterator<NeighboringCellInfo> it = this.m.iterator();
        while (it.hasNext()) {
            NeighboringCellInfo next = it.next();
            arrayList.add(new LbsCell(this.i, this.j, this.k, next.getCid(), next.getRssi()));
        }
        aVar.a(arrayList);
        ArrayList arrayList2 = new ArrayList();
        if (this.n != null) {
            arrayList2.addAll(this.n);
        }
        aVar.b(arrayList2);
        return aVar.a();
    }

    /* access modifiers changed from: private */
    public boolean f() {
        synchronized (this.o) {
            if (this.h) {
                if (this.g != null) {
                    try {
                        XLog.d("LBS", "unregister mWifiReceiver ");
                        this.b.unregisterReceiver(this.g);
                        this.d.removeCallbacks(this.t);
                    } catch (Exception e2) {
                    }
                }
            }
        }
        return true;
    }

    private boolean g() {
        XLog.d("LBS", "request Cell location ");
        switch (this.e.getPhoneType()) {
            case 1:
                try {
                    GsmCellLocation gsmCellLocation = (GsmCellLocation) this.e.getCellLocation();
                    if (gsmCellLocation != null) {
                        this.k = gsmCellLocation.getLac();
                        this.l = gsmCellLocation.getCid();
                        if (this.k <= 0 && this.l <= 0) {
                            this.k = 0;
                            this.l = 0;
                            break;
                        } else {
                            String networkOperator = this.e.getNetworkOperator();
                            if (networkOperator != null) {
                                try {
                                    this.i = Integer.parseInt(networkOperator.substring(0, 3));
                                    this.j = Integer.parseInt(networkOperator.substring(3));
                                } catch (Exception e2) {
                                    this.i = 0;
                                    this.j = 0;
                                    this.k = 0;
                                    this.l = 0;
                                    return false;
                                }
                            }
                            List neighboringCellInfo = this.e.getNeighboringCellInfo();
                            if (neighboringCellInfo != null) {
                                this.m.addAll(neighboringCellInfo);
                                break;
                            }
                        }
                    }
                } catch (Exception e3) {
                    return false;
                }
                break;
            case 2:
                try {
                    Class<?> cls = Class.forName("android.telephony.cdma.CdmaCellLocation");
                    if (cls != null) {
                        cls.getConstructor(null);
                        CellLocation cellLocation = this.e.getCellLocation();
                        Method method = cls.getMethod("getSystemId", new Class[0]);
                        if (method != null) {
                            this.j = ((Integer) method.invoke(cellLocation, null)).intValue();
                        }
                        Method method2 = cls.getMethod("getNetworkId", new Class[0]);
                        if (method2 != null) {
                            this.k = ((Integer) method2.invoke(cellLocation, null)).intValue();
                        }
                        Method method3 = cls.getMethod("getBaseStationId", new Class[0]);
                        if (method3 != null) {
                            this.l = ((Integer) method3.invoke(cellLocation, null)).intValue();
                        }
                        String networkOperator2 = this.e.getNetworkOperator();
                        if (networkOperator2 != null) {
                            try {
                                this.i = Integer.parseInt(networkOperator2.substring(0, 3));
                                break;
                            } catch (Exception e4) {
                                break;
                            }
                        }
                    }
                } catch (Exception e5) {
                    e5.printStackTrace();
                    break;
                }
                break;
            default:
                return false;
        }
        return true;
    }

    private boolean h() {
        XLog.d("LBS", "request wifi location ");
        synchronized (this.o) {
            if (this.h) {
                return true;
            }
            try {
                i();
                if (this.n == null) {
                    this.n = new ArrayList<>();
                }
                if (this.n.size() == 0) {
                    if (this.g == null) {
                        this.g = new h(this);
                    }
                    IntentFilter intentFilter = new IntentFilter();
                    intentFilter.addAction("android.net.wifi.SCAN_RESULTS");
                    this.b.registerReceiver(this.g, intentFilter);
                    this.h = this.f.startScan();
                    this.d.postDelayed(this.t, 3000);
                } else {
                    this.h = false;
                    l();
                }
                return true;
            } catch (Exception e2) {
                return false;
            }
        }
    }

    private void i() {
        List<ScanResult> scanResults = this.f.getScanResults();
        if (scanResults != null) {
            if (this.n == null) {
                this.n = new ArrayList<>();
            }
            for (ScanResult next : scanResults) {
                this.n.add(new LbsWifiMac(next.BSSID, next.level));
            }
        }
    }

    /* access modifiers changed from: private */
    public void j() {
        if (this.h) {
            try {
                i();
                this.h = false;
            } catch (Exception e2) {
                this.h = false;
            } catch (Throwable th) {
                this.h = false;
                l();
                throw th;
            }
            l();
        }
    }

    private void k() {
        this.i = 0;
        this.j = 0;
        this.k = 0;
        this.l = 0;
        if (this.m == null) {
            this.m = new ArrayList<>();
        } else {
            this.m.clear();
        }
        if (this.n == null) {
            this.n = new ArrayList<>();
        } else {
            this.n.clear();
        }
        this.p = false;
        this.q = false;
        this.r = -1;
    }

    /* access modifiers changed from: private */
    public void l() {
        int i2;
        int i3 = 0;
        this.s = false;
        if (this.l != 0) {
            i3 = 1;
        }
        if (this.n == null || this.n.size() <= 0) {
            i2 = i3;
        } else {
            i2 = i3 | 2;
        }
        XLog.d("LBS", "result " + i2);
        if (this.c != null) {
            LbsData e2 = e();
            for (b next : this.c) {
                if (next != null) {
                    next.a(i2, e2);
                }
            }
            this.c.clear();
        }
        synchronized (this.o) {
            this.d.removeCallbacks(this.t);
        }
    }

    private boolean m() {
        int i2;
        boolean z = false;
        synchronized (this.o) {
            if (this.e != null) {
                try {
                    i2 = Settings.System.getInt(this.b.getContentResolver(), "airplane_mode_on", 0);
                } catch (Exception e2) {
                    i2 = 0;
                }
                if (i2 == 0) {
                    z = true;
                }
            }
        }
        return z;
    }

    private boolean n() {
        if (this.f == null) {
            return false;
        }
        try {
            return this.f.isWifiEnabled();
        } catch (Throwable th) {
            th.printStackTrace();
            return false;
        }
    }
}
