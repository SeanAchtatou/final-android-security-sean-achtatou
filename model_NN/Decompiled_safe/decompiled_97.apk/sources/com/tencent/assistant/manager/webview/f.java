package com.tencent.assistant.manager.webview;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import com.tencent.assistant.Global;
import com.tencent.assistant.manager.webview.component.b;
import com.tencent.assistant.manager.webview.component.e;
import com.tencent.assistant.manager.webview.js.JsBridge;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.net.c;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ah;
import com.tencent.assistant.utils.bm;
import com.tencent.assistant.utils.t;
import com.tencent.connect.common.Constants;
import com.tencent.smtt.export.external.interfaces.WebResourceResponse;
import com.tencent.smtt.sdk.WebView;
import com.tencent.smtt.sdk.WebViewClient;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* compiled from: ProGuard */
public class f extends WebViewClient {

    /* renamed from: a  reason: collision with root package name */
    private static String f918a = "mq.wsq.qq.com/direct?route=myMessage&source=myapp";
    private Context b;
    /* access modifiers changed from: private */
    public JsBridge c;
    private e d;
    private b e;

    public f(Context context, JsBridge jsBridge, e eVar, b bVar) {
        this.b = context;
        this.c = jsBridge;
        this.d = eVar;
        this.e = bVar;
    }

    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        if (this.d != null) {
            this.d.c();
        }
        if (!this.e.c) {
            TemporaryThreadManager.get().start(new g(this, webView));
        }
        if ((str.startsWith("http") || str.startsWith("https")) && this.c != null) {
            this.c.loadAuthorization(str);
        }
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
        if (this.d != null) {
            this.d.b();
        }
        super.onReceivedError(webView, i, str, str2);
    }

    public void onPageFinished(WebView webView, String str) {
        if (this.d != null) {
            this.d.d();
        }
        if (this.c != null) {
            this.c.updateLoadedTime();
            this.c.doPageLoadFinished();
        }
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        XLog.i("Jie", "shouldOverrideUrlLoading ---url = " + str);
        if (TextUtils.isEmpty(str)) {
            XLog.i("Jie", "Interface url is empty");
            return false;
        } else if (str.startsWith("http") || str.startsWith("https")) {
            return super.shouldOverrideUrlLoading(webView, str);
        } else {
            if (str.startsWith(JsBridge.JS_BRIDGE_SCHEME)) {
                XLog.i("Jie", "Interface request:" + str);
                XLog.i("Donaldxu", "Interface request:" + str);
                if (this.c == null) {
                    return true;
                }
                this.c.invoke(str);
                return true;
            } else if (!str.equals("about:blank;") && !str.equals("about:blank")) {
                Uri parse = Uri.parse(str);
                Intent intent = new Intent("android.intent.action.VIEW", parse);
                if (!com.tencent.pangu.link.b.a(webView.getContext(), intent)) {
                    return false;
                }
                String scheme = intent.getScheme();
                if (scheme == null || !scheme.equals("tmast")) {
                    intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.d.a());
                    this.b.startActivity(intent);
                    return true;
                }
                Bundle bundle = new Bundle();
                int a2 = bm.a(parse.getQueryParameter("scene"), 0);
                if (a2 != 0) {
                    bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, a2);
                } else {
                    bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.d.a());
                }
                com.tencent.pangu.link.b.b(this.b, str, bundle);
                return true;
            } else if (Build.VERSION.SDK_INT >= 11) {
                return false;
            } else {
                return true;
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(WebView webView) {
        String str;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            String str2 = FileUtil.getFilesDir() + File.separator + "agentdata.js";
            XLog.d("TxWebViewClient", "agentData jsfilepath = " + str2);
            if (FileUtil.readFile(str2, byteArrayOutputStream)) {
                XLog.d("TxWebViewClient", "js File exist.. read it!");
                str = byteArrayOutputStream.toString();
                byteArrayOutputStream.close();
            } else {
                str = "javascript:window.agentData = {};agentData.width='" + t.b + "';" + "agentData.height='" + t.c + "';" + "agentData.androidId='" + t.l() + "';" + "agentData.androidIdSdCard='" + t.m() + "';" + "agentData.imei='" + t.g() + "';" + "agentData.imsi='" + t.h() + "';" + "agentData.macAdress='" + t.k() + "';";
                FileUtil.writeToAppData("agentdata.js", str, 0);
            }
            com.tencent.assistant.net.b i = c.i();
            if (i.f1059a == APN.UN_DETECT) {
                c.k();
            }
            String str3 = str + ("agentData.imsi='" + t.h() + "';" + "agentData.apn='" + i.f1059a + "';" + "agentData.isWap='" + i.d + "';" + "agentData.networkOperator='" + i.b + "';" + "agentData.networkType='" + i.c + "';" + "agentData.channelId='" + i.c + "';" + "agentData.qua='" + Global.getQUA() + "';" + "agentData.versionName='" + Global.getAppVersionName() + "';" + "agentData.versionCode='" + Global.getAppVersionCode() + "';" + "agentData.phoneGuid='" + Global.getPhoneGuid() + "';") + "void(0);";
            XLog.d("TxWebViewClient", "js code = " + str3);
            ah.a().post(new h(this, str3, webView));
            if (byteArrayOutputStream != null) {
                byteArrayOutputStream.close();
            }
        } catch (Exception e2) {
            XLog.d("TxWebViewClient", "initLocalStorage exception.." + e2.getMessage());
            if (byteArrayOutputStream != null) {
                try {
                    byteArrayOutputStream.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
            }
            e2.printStackTrace();
        }
    }

    public WebResourceResponse shouldInterceptRequest(WebView webView, String str) {
        XLog.i("Jie", "[Before] shouldInterceptRequest --- url = " + str);
        if (TextUtils.isEmpty(str)) {
            XLog.i("Jie", "Interface url is empty");
            return super.shouldInterceptRequest(webView, str);
        }
        Matcher matcher = Pattern.compile("^http://([\\w-]+\\.)*qq\\.com/__yybjsbridge__/").matcher(str);
        if (matcher.find()) {
            str = matcher.replaceAll(JsBridge.JS_BRIDGE_SCHEME);
            XLog.i("Jie", "[After] shouldInterceptRequest --- url = " + str);
        } else {
            XLog.i("Jie", "no matcher");
        }
        WebResourceResponse webResourceResponse = new WebResourceResponse("*/*", "UTF-8", new ByteArrayInputStream(Constants.STR_EMPTY.getBytes()));
        if (str.startsWith("javascript:void(0)")) {
            XLog.i("Jie", "*** intercept ***");
            return super.shouldInterceptRequest(webView, str);
        }
        if (!str.startsWith("http") && !str.startsWith("https")) {
            if (str.startsWith(JsBridge.JS_BRIDGE_SCHEME)) {
                ah.a().post(new i(this, str));
                return webResourceResponse;
            } else if (!str.equals("about:blank;") && !str.equals("about:blank")) {
                Uri parse = Uri.parse(str);
                Intent intent = new Intent("android.intent.action.VIEW", parse);
                if (com.tencent.pangu.link.b.a(webView.getContext(), intent)) {
                    String scheme = intent.getScheme();
                    if (scheme == null || !scheme.equals("tmast")) {
                        intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.d.a());
                        this.b.startActivity(intent);
                        return webResourceResponse;
                    }
                    Bundle bundle = new Bundle();
                    int a2 = bm.a(parse.getQueryParameter("scene"), 0);
                    if (a2 != 0) {
                        bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, a2);
                    } else {
                        bundle.putInt(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.d.a());
                    }
                    com.tencent.pangu.link.b.b(this.b, str, bundle);
                    return webResourceResponse;
                }
            } else if (Build.VERSION.SDK_INT < 11) {
                return webResourceResponse;
            }
        }
        return super.shouldInterceptRequest(webView, str);
    }
}
