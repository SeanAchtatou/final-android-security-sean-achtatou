package com.tencent.assistant.manager.notification.a;

import android.graphics.Bitmap;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.manager.notification.a.a.e;

/* compiled from: ProGuard */
class f implements e {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ e f871a;

    f(e eVar) {
        this.f871a = eVar;
    }

    public void a(Bitmap bitmap) {
        if (bitmap != null && !bitmap.isRecycled()) {
            this.f871a.i.setImageViewBitmap(R.id.big_icon, bitmap);
        }
    }
}
