package com.tencent.assistant.manager.notification;

import android.app.NotificationManager;
import android.graphics.Bitmap;
import com.qq.AppService.AstApp;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistant.utils.ah;
import com.tencent.pangu.download.DownloadInfo;

/* compiled from: ProGuard */
class b implements g {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadInfo f878a;
    final /* synthetic */ a b;

    b(a aVar, DownloadInfo downloadInfo) {
        this.b = aVar;
        this.f878a = downloadInfo;
    }

    public void a(Bitmap bitmap) {
        XLog.d("BookingDownload", "LoadImageFinish SUCC ");
        ((NotificationManager) AstApp.i().getSystemService("notification")).cancel(115);
        ah.a().post(new c(this, bitmap));
    }
}
