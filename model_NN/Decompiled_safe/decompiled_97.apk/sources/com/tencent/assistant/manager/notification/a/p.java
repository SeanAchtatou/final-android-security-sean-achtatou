package com.tencent.assistant.manager.notification.a;

import android.text.Html;
import android.text.TextUtils;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.protocol.jce.PushInfo;

/* compiled from: ProGuard */
public class p extends e {
    public p(int i, PushInfo pushInfo, byte[] bArr) {
        super(i, pushInfo, bArr);
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        if (!super.c()) {
            return false;
        }
        if (TextUtils.isEmpty(this.c.c)) {
            return false;
        }
        if (this.c.h == null || TextUtils.isEmpty(this.c.h.get("extra_key_1")) || TextUtils.isEmpty(this.c.h.get("extra_key_2"))) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        b(R.layout.notification_card_1);
        if (this.i == null) {
            return false;
        }
        i();
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean g() {
        if (!(this.c == null || this.c.h == null)) {
            String str = this.c.h.get("extra_key_1");
            String str2 = this.c.h.get("extra_key_2");
            if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
                this.j = c(R.layout.notification_card1_right5);
                if (this.m != null) {
                    this.j.setTextColor(R.id.topText, this.m.intValue());
                    this.j.setTextColor(R.id.bottomText, this.m.intValue());
                }
                this.j.setTextViewText(R.id.topText, Html.fromHtml(str));
                this.j.setTextViewText(R.id.bottomText, Html.fromHtml(str2));
                this.i.removeAllViews(R.id.rightContainer);
                this.i.addView(R.id.rightContainer, this.j);
                this.i.setViewVisibility(R.id.rightContainer, 0);
            }
        }
        return this.j != null;
    }

    /* access modifiers changed from: protected */
    public boolean h() {
        return true;
    }
}
