package com.tencent.assistant.manager.notification.a;

import android.app.Notification;
import com.tencent.assistant.manager.notification.a.a.g;
import com.tencent.assistant.manager.notification.v;

/* compiled from: ProGuard */
public abstract class a {

    /* renamed from: a  reason: collision with root package name */
    protected int f864a = 0;
    protected Notification b = null;
    private c c = null;
    private g d = new g();

    public abstract String a(int i);

    /* access modifiers changed from: protected */
    public abstract boolean b();

    /* access modifiers changed from: protected */
    public abstract boolean c();

    /* access modifiers changed from: protected */
    public abstract boolean d();

    public a(int i) {
        this.f864a = i;
        this.d.a(new b(this));
    }

    public void a(c cVar) {
        this.c = cVar;
    }

    public void a() {
        if (b()) {
            b(7);
        } else if (!c()) {
            b(5);
        } else if (!d() || this.b == null) {
            b(6);
        } else {
            this.d.b();
        }
    }

    /* access modifiers changed from: protected */
    public void a(com.tencent.assistant.manager.notification.a.a.a aVar) {
        if (aVar != null) {
            this.d.a(aVar);
        }
    }

    /* access modifiers changed from: private */
    public void b(int i) {
        if (this.c != null) {
            this.c.a(this, i);
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        v.a().a(this.f864a, this.b);
    }
}
