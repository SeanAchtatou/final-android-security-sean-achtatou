package com.tencent.assistant.manager.notification.a;

import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.m;
import com.tencent.assistant.module.update.AppUpdateConst;
import com.tencent.assistant.module.update.b;
import com.tencent.assistant.protocol.jce.DataUpdateInfo;
import com.tencent.assistant.protocol.jce.InTimePushCfg;
import com.tencent.assistant.utils.XLog;
import java.util.Iterator;
import java.util.Map;

/* compiled from: ProGuard */
public class d {

    /* renamed from: a  reason: collision with root package name */
    private static d f870a;
    private InTimePushCfg b;

    public static synchronized d a() {
        d dVar;
        synchronized (d.class) {
            if (f870a == null) {
                f870a = new d();
            }
            dVar = f870a;
        }
        return dVar;
    }

    public void a(InTimePushCfg inTimePushCfg) {
        XLog.d("IntimePushManager", "setIntimePushCfg");
        this.b = inTimePushCfg;
        if (inTimePushCfg != null && inTimePushCfg.f1391a != null) {
            Iterator<DataUpdateInfo> it = inTimePushCfg.f1391a.iterator();
            while (it.hasNext()) {
                a(it.next());
            }
        }
    }

    public void a(DataUpdateInfo dataUpdateInfo) {
        if (dataUpdateInfo != null) {
            XLog.d("IntimePushManager", "processDataUpdateInfo:" + ((int) dataUpdateInfo.f1213a));
            switch (dataUpdateInfo.f1213a) {
                case 19:
                    b(dataUpdateInfo);
                    return;
                default:
                    return;
            }
        }
    }

    private void b(DataUpdateInfo dataUpdateInfo) {
        if (m.a().a(dataUpdateInfo.f1213a) < dataUpdateInfo.b && ApkResourceManager.getInstance().isLocalApkDataReady()) {
            m.a().a(dataUpdateInfo.f1213a, dataUpdateInfo.b);
            b.a().a(AppUpdateConst.RequestLaunchType.TYPE_INTIME_UPDATE_PUSH, (Map<String, String>) null);
        }
    }
}
