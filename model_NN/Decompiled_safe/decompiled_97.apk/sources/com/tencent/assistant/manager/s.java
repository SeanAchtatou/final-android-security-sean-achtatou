package com.tencent.assistant.manager;

import android.text.TextUtils;
import com.tencent.assistant.manager.SplashManager;
import com.tencent.assistant.model.k;
import com.tencent.assistant.utils.FileUtil;
import com.tencent.assistant.utils.XLog;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
class s implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ List f904a;
    final /* synthetic */ SplashManager b;

    s(SplashManager splashManager, List list) {
        this.b = splashManager;
        this.f904a = list;
    }

    public void run() {
        boolean z;
        boolean z2;
        List<k> a2 = this.b.b.a(false);
        this.b.b.a();
        this.b.b.a(this.f904a);
        XLog.d("splashInfo", "服务器返回的 SplashList = " + this.f904a);
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (k kVar : this.f904a) {
            if (kVar.l() != 0 && kVar.e() >= System.currentTimeMillis() / 1000 && !TextUtils.isEmpty(kVar.i())) {
                if (kVar.r() != 1 || !TextUtils.isEmpty(kVar.m())) {
                    kVar.d(this.b.a(kVar.i()));
                    if (kVar.r() == 1) {
                        kVar.h(this.b.a(kVar.m()));
                    }
                    k kVar2 = null;
                    if (a2.contains(kVar)) {
                        kVar2 = a2.remove(a2.indexOf(kVar));
                        z2 = kVar2.i().equalsIgnoreCase(kVar.i()) && this.b.c(kVar.j());
                        if (kVar.r() != 1 || !kVar2.m().equalsIgnoreCase(kVar.m()) || !this.b.c(this.b.a(kVar.m()))) {
                            z = false;
                        } else {
                            z = true;
                        }
                    } else {
                        z = false;
                        z2 = false;
                    }
                    if (!z2) {
                        arrayList.add(kVar.i());
                    } else {
                        kVar.e(kVar2.k());
                        kVar.d(kVar2.h());
                    }
                    if (!z && kVar.r() == 1) {
                        arrayList2.add(kVar.m());
                    }
                    this.b.b.b(kVar);
                }
            }
        }
        this.b.a(arrayList, SplashManager.ImageType.SPLASH);
        this.b.a(arrayList2, SplashManager.ImageType.BUTTON_BG);
        for (k next : a2) {
            String j = next.j();
            if (TextUtils.isEmpty(j)) {
                j = this.b.a(next.i());
            }
            FileUtil.deleteFile(j);
            if (!TextUtils.isEmpty(next.v())) {
                FileUtil.deleteFile(next.v());
            }
        }
        XLog.d("splashInfo", "finish get bitmap ");
    }
}
