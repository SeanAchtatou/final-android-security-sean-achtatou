package com.tencent.assistant.manager.notification;

import android.text.TextUtils;
import com.qq.AppService.AstApp;
import com.tencent.assistant.manager.notification.a.a;
import com.tencent.assistant.manager.notification.a.e;
import com.tencent.assistant.manager.notification.a.g;
import com.tencent.assistant.manager.notification.a.h;
import com.tencent.assistant.manager.notification.a.i;
import com.tencent.assistant.manager.notification.a.k;
import com.tencent.assistant.manager.notification.a.l;
import com.tencent.assistant.manager.notification.a.n;
import com.tencent.assistant.manager.notification.a.p;
import com.tencent.assistant.manager.notification.a.q;
import com.tencent.assistant.manager.notification.a.r;
import com.tencent.assistant.manager.notification.a.s;
import com.tencent.assistant.protocol.jce.AutoDownloadInfo;
import com.tencent.assistant.protocol.jce.PushInfo;
import com.tencent.assistant.utils.XLog;
import com.tencent.assistantv2.st.page.c;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public class o {

    /* renamed from: a  reason: collision with root package name */
    private static o f890a;
    /* access modifiers changed from: private */
    public Map<Integer, a> b = new HashMap();
    private r c = null;

    public static synchronized o a() {
        o oVar;
        synchronized (o.class) {
            if (f890a == null) {
                f890a = new o();
            }
            oVar = f890a;
        }
        return oVar;
    }

    public Integer b() {
        return Integer.valueOf(this.c != null ? this.c.a().intValue() : 0);
    }

    public float c() {
        if (this.c != null) {
            return this.c.b();
        }
        return 0.0f;
    }

    public Integer d() {
        return Integer.valueOf(this.c != null ? this.c.c().intValue() : 0);
    }

    public float e() {
        if (this.c != null) {
            return this.c.d();
        }
        return 0.0f;
    }

    public void a(int i, PushInfo pushInfo, byte[] bArr, boolean z) {
        a(i, pushInfo, bArr, z, (String) null);
    }

    public void a(int i, PushInfo pushInfo, byte[] bArr, boolean z, String str) {
        e iVar;
        if (pushInfo == null || pushInfo.d == null || TextUtils.isEmpty(pushInfo.d.f1125a)) {
            a(i, pushInfo, 7, bArr, str);
            XLog.d("NotificationSender", "push Info is null or actionurl is null");
            return;
        }
        if (this.c == null) {
            this.c = new r(AstApp.i(), z);
        }
        XLog.d("NotificationSender", "pushInfo.getPushTemplate(): " + ((int) pushInfo.e()));
        switch (pushInfo.e()) {
            case 1:
                iVar = new g(i, pushInfo, bArr);
                break;
            case 2:
                iVar = new k(i, pushInfo, bArr);
                break;
            case 3:
                iVar = new l(i, pushInfo, bArr);
                break;
            case 4:
                iVar = new n(i, pushInfo, bArr);
                break;
            case 5:
                iVar = new p(i, pushInfo, bArr);
                break;
            case 6:
                iVar = new q(i, pushInfo, bArr);
                break;
            case 7:
                iVar = new r(i, pushInfo, bArr);
                break;
            case 8:
            case 9:
            case 11:
            default:
                a(i, pushInfo, 4, bArr, str);
                iVar = null;
                break;
            case 10:
                iVar = new h(i, pushInfo, bArr);
                break;
            case 12:
                iVar = new i(i, pushInfo, bArr);
                break;
        }
        if (iVar != null) {
            iVar.a(new p(this, i, pushInfo, bArr, str));
            this.b.put(Integer.valueOf(i), iVar);
            iVar.a();
        }
    }

    public void a(int i, List<AutoDownloadInfo> list) {
        s sVar = new s(i, list);
        if (sVar != null) {
            sVar.a(new q(this, i));
            this.b.put(Integer.valueOf(i), sVar);
            sVar.a();
        }
    }

    public static void a(int i, PushInfo pushInfo, int i2, byte[] bArr) {
        c.a(i, pushInfo, bArr, i2);
    }

    public static void a(int i, PushInfo pushInfo, int i2, byte[] bArr, String str) {
        c.a(i, pushInfo, bArr, i2, str);
    }

    public static void a(int i, PushInfo pushInfo, byte[] bArr, String str, String str2) {
        c.a(i, pushInfo, bArr, str, str2);
    }
}
