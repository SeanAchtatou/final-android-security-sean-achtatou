package com.tencent.assistant.plugin.annotation;

/* compiled from: ProGuard */
public abstract class PluginActivityLaunchMode {
    public static final String LAUNCH_MODE_SINGLE_INSTANCE = "singleInstance";
    public static final String LAUNCH_MODE_SINGLE_TASK = "singleTask";
    public static final String LAUNCH_MODE_SINGLE_TOP = "singleTop";
}
