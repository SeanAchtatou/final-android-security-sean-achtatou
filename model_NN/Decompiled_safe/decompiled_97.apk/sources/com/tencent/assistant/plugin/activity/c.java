package com.tencent.assistant.plugin.activity;

import android.view.View;
import com.tencent.assistant.AppConst;
import com.tencent.assistant.module.k;
import com.tencent.assistant.plugin.PluginDownloadInfo;
import com.tencent.assistant.plugin.PluginInfo;
import com.tencent.assistant.utils.TemporaryThreadManager;
import com.tencent.pangu.download.DownloadInfo;

/* compiled from: ProGuard */
class c implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadInfo f1083a;
    final /* synthetic */ PluginDownloadInfo b;
    final /* synthetic */ PluginDownActivity c;

    c(PluginDownActivity pluginDownActivity, DownloadInfo downloadInfo, PluginDownloadInfo pluginDownloadInfo) {
        this.c = pluginDownActivity;
        this.f1083a = downloadInfo;
        this.b = pluginDownloadInfo;
    }

    public void onClick(View view) {
        AppConst.AppState a2 = k.a(this.f1083a, this.b, (PluginInfo) null);
        if (a2 == AppConst.AppState.DOWNLOADED) {
            String downloadingPath = this.f1083a.getDownloadingPath();
            if (this.b != null) {
                TemporaryThreadManager.get().start(new d(this, downloadingPath, this.b.pluginPackageName));
            }
        } else if (a2 != AppConst.AppState.DOWNLOADING && a2 != AppConst.AppState.QUEUING) {
            com.tencent.assistant.plugin.mgr.c.a().c(this.b);
        }
    }
}
