package com.tencent.assistant.protocol.jce;

import java.io.Serializable;

/* compiled from: ProGuard */
public final class PHONE_USER_OPERATION_TYPE implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public static final PHONE_USER_OPERATION_TYPE f1427a = new PHONE_USER_OPERATION_TYPE(0, -1, "RECOMMEND_DEFAULT");
    public static final PHONE_USER_OPERATION_TYPE b = new PHONE_USER_OPERATION_TYPE(1, 0, "RECOMMEND_NOTHING");
    public static final PHONE_USER_OPERATION_TYPE c = new PHONE_USER_OPERATION_TYPE(2, 1, "RECOMMEND_OLD_APP_LIST_AND_CHANGE");
    public static final PHONE_USER_OPERATION_TYPE d = new PHONE_USER_OPERATION_TYPE(3, 2, "RECOMMEND_OLD_APP_LIST_AND_POPUP");
    public static final PHONE_USER_OPERATION_TYPE e = new PHONE_USER_OPERATION_TYPE(4, 3, "RECOMMEND_NEW_APP_LIST_AND_CHANGE");
    public static final PHONE_USER_OPERATION_TYPE f = new PHONE_USER_OPERATION_TYPE(5, 4, "RECOMMEND_NEW_APP_LIST_AND_POPUP");
    static final /* synthetic */ boolean g = (!PHONE_USER_OPERATION_TYPE.class.desiredAssertionStatus());
    private static PHONE_USER_OPERATION_TYPE[] h = new PHONE_USER_OPERATION_TYPE[6];
    private int i;
    private String j = new String();

    public int a() {
        return this.i;
    }

    public String toString() {
        return this.j;
    }

    private PHONE_USER_OPERATION_TYPE(int i2, int i3, String str) {
        this.j = str;
        this.i = i3;
        h[i2] = this;
    }
}
