package com.tencent.assistant.thumbnailCache;

/* compiled from: ProGuard */
class l implements p {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ k f1799a;

    l(k kVar) {
        this.f1799a = kVar;
    }

    public void thumbnailRequestCompleted(o oVar) {
        synchronized (this.f1799a.d) {
            if (((o) this.f1799a.d.remove(oVar.c())) != null) {
                o oVar2 = (o) this.f1799a.e.remove(oVar.c());
                if (oVar2 != null) {
                    oVar2.f = oVar.f;
                    oVar2.b(0);
                }
                if (this.f1799a.d.size() < this.f1799a.f1798a.c()) {
                    this.f1799a.d.notify();
                }
                if (this.f1799a.g < System.currentTimeMillis()) {
                    k.a(this.f1799a, 60000);
                    this.f1799a.c();
                }
            }
        }
    }

    public void thumbnailRequestFailed(o oVar) {
        synchronized (this.f1799a.d) {
            if (((o) this.f1799a.d.remove(oVar.c())) != null) {
                o oVar2 = (o) this.f1799a.e.remove(oVar.c());
                if (oVar2 != null) {
                    oVar2.b(2);
                }
                if (this.f1799a.d.size() < this.f1799a.f1798a.c()) {
                    this.f1799a.d.notify();
                }
            }
        }
    }
}
