package com.tencent.assistant;

import com.qq.AppService.AstApp;
import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
final class l implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f793a;
    final /* synthetic */ boolean b;

    l(int i, boolean z) {
        this.f793a = i;
        this.b = z;
    }

    public void run() {
        switch (this.f793a) {
            case 0:
                m.a().g(this.b);
                return;
            case 1:
                m.a().a(AppConst.WISE_DOWNLOAD_SWITCH_TYPE.UPDATE, this.b);
                return;
            case 2:
                m.a().a(AppConst.WISE_DOWNLOAD_SWITCH_TYPE.NEW_DOWNLOAD, this.b);
                return;
            case 3:
                m.a().b(this.b);
                return;
            case 4:
                m.a().c(this.b);
                return;
            case 5:
                m.a().d(this.b);
                return;
            case 6:
                m.a().t(this.b);
                return;
            case 7:
            case 8:
            case 9:
            case 10:
            default:
                return;
            case 11:
                m.a().h(this.b);
                return;
            case 12:
                m.a().i(this.b);
                return;
            case 13:
                m.a().j(this.b);
                return;
            case 14:
                m.a().k(this.b);
                return;
            case 15:
                m.a().l(this.b);
                if (this.b) {
                    AstApp.i().b();
                    return;
                } else {
                    AstApp.i().c();
                    return;
                }
            case 16:
                m.a().f(this.b);
                m.a().b("key_float_window_manual_state", Boolean.valueOf(this.b));
                return;
        }
    }
}
