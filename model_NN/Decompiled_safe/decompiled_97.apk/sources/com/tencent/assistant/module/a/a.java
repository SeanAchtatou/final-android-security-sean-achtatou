package com.tencent.assistant.module.a;

import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public abstract class a implements g {

    /* renamed from: a  reason: collision with root package name */
    private List<g> f950a = new ArrayList();
    private k b;
    private boolean c = false;

    public abstract boolean d();

    public void a(k kVar) {
        this.b = kVar;
    }

    public void run() {
        if (this.b != null) {
            this.b.b(this);
        }
        boolean z = false;
        try {
            z = d();
            this.c = true;
        } catch (Exception e) {
            if (this.b != null) {
                this.b.a(this, e);
            }
        }
        if (z && this.b != null) {
            this.b.c(this);
        }
    }

    public List<g> a() {
        return this.f950a;
    }

    public int b() {
        return 0;
    }

    public int c() {
        return 0;
    }
}
