package com.tencent.assistant.module.update;

import android.text.TextUtils;
import com.tencent.assistant.localres.ApkResourceManager;
import com.tencent.assistant.localres.model.LocalApkInfo;
import com.tencent.assistant.module.update.AppUpdateConst;
import com.tencent.assistant.protocol.jce.AppInfoForUpdate;
import com.tencent.assistant.protocol.jce.LbsData;
import com.tencent.assistant.utils.bm;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.Map;

/* compiled from: ProGuard */
class m implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppUpdateConst.RequestLaunchType f1039a;
    final /* synthetic */ LbsData b;
    final /* synthetic */ Map c;
    final /* synthetic */ j d;

    m(j jVar, AppUpdateConst.RequestLaunchType requestLaunchType, LbsData lbsData, Map map) {
        this.d = jVar;
        this.f1039a = requestLaunchType;
        this.b = lbsData;
        this.c = map;
    }

    public void run() {
        LocalApkInfo installedApkInfo;
        AppInfoForUpdate a2;
        int i;
        int i2 = 0;
        switch (q.f1043a[this.f1039a.ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
                int unused = this.d.h = this.d.a(this.b, this.f1039a.ordinal());
                return;
            case 5:
                String str = this.c != null ? (String) this.c.get("packagename") : Constants.STR_EMPTY;
                if (this.c != null) {
                    i = bm.a((String) this.c.get("versioncode"), 0);
                } else {
                    i = 0;
                }
                AppInfoForUpdate appInfoForUpdate = new AppInfoForUpdate();
                appInfoForUpdate.f1157a = str;
                appInfoForUpdate.c = i;
                ArrayList arrayList = new ArrayList();
                arrayList.add(appInfoForUpdate);
                int unused2 = this.d.h = this.d.a(arrayList, this.b, (byte) -1, this.f1039a.ordinal());
                return;
            case 6:
            case 7:
                ArrayList arrayList2 = new ArrayList();
                String str2 = this.c != null ? (String) this.c.get("packagename") : Constants.STR_EMPTY;
                if (this.c != null) {
                    i2 = bm.a((String) this.c.get("versioncode"), 0);
                }
                if (!TextUtils.isEmpty(str2) && (installedApkInfo = ApkResourceManager.getInstance().getInstalledApkInfo(str2)) != null && installedApkInfo.mVersionCode == i2 && (a2 = j.b(installedApkInfo)) != null) {
                    arrayList2.add(a2);
                }
                int unused3 = this.d.h = this.d.a(arrayList2, this.b, (byte) 1, this.f1039a.ordinal());
                return;
            default:
                return;
        }
    }
}
