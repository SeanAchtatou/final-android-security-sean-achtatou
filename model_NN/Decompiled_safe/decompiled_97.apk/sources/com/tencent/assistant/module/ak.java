package com.tencent.assistant.module;

import com.tencent.assistant.model.QuickEntranceNotify;
import java.util.ArrayList;

/* compiled from: ProGuard */
public interface ak {
    void onLocalDataHasUpdate();

    void onPromptHasNewNotify(ArrayList<QuickEntranceNotify> arrayList);
}
