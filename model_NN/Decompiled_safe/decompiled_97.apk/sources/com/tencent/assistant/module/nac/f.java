package com.tencent.assistant.module.nac;

import com.tencent.assistant.module.nac.NACEngine;
import com.tencent.assistant.protocol.jce.IPDataAddress;
import com.tencent.connect.common.Constants;
import java.net.MalformedURLException;
import java.net.URL;

/* compiled from: ProGuard */
public class f {

    /* renamed from: a  reason: collision with root package name */
    public IPDataAddress f1007a;
    public NACEngine.NACEMode b;
    public String c;
    public int d;
    public String e;
    private String f;

    public f(IPDataAddress iPDataAddress, NACEngine.NACEMode nACEMode, String str, int i, String str2) {
        this.f1007a = iPDataAddress;
        this.b = nACEMode;
        this.c = str;
        this.d = i;
        this.e = str2;
    }

    public f(String str) {
        try {
            URL url = new URL(str);
            this.c = url.getHost();
            this.d = url.getPort();
            this.e = url.getFile();
            this.f = url.getProtocol();
            this.f1007a = null;
            this.b = NACEngine.NACEMode.NACMODE_DOMAIN;
        } catch (MalformedURLException e2) {
            e2.printStackTrace();
        }
    }

    public String a() {
        if (this.f1007a == null) {
            return d() + "://" + this.c + ":" + c() + b();
        }
        return d() + "://" + this.f1007a.f1388a + ":" + ((int) this.f1007a.b) + b();
    }

    public String b() {
        if (this.e == null) {
            return Constants.STR_EMPTY;
        }
        return this.e;
    }

    public int c() {
        if (this.d < 0) {
            return 80;
        }
        return this.d;
    }

    public String d() {
        if (this.f == null) {
            return "http";
        }
        return this.f;
    }
}
