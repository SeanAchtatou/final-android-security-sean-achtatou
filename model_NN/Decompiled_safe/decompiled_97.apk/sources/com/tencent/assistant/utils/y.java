package com.tencent.assistant.utils;

import android.content.Intent;
import com.qq.AppService.AstApp;
import com.tencent.assistant.a.a;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.activity.InstalledAppManagerActivity;
import com.tencent.assistant.activity.SpaceCleanActivity;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistant.utils.FunctionUtils;

/* compiled from: ProGuard */
final class y implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FunctionUtils.InstallSpaceNotEnoughForwardPage f1854a;
    final /* synthetic */ int b;
    final /* synthetic */ boolean c;

    y(FunctionUtils.InstallSpaceNotEnoughForwardPage installSpaceNotEnoughForwardPage, int i, boolean z) {
        this.f1854a = installSpaceNotEnoughForwardPage;
        this.b = i;
        this.c = z;
    }

    public void run() {
        Intent intent;
        BaseActivity m = AstApp.m();
        if (m != null) {
            if (this.f1854a == FunctionUtils.InstallSpaceNotEnoughForwardPage.SPACE_CLEAR_MANAGER) {
                intent = new Intent(m, SpaceCleanActivity.class);
            } else {
                intent = new Intent(m, InstalledAppManagerActivity.class);
            }
            intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, this.b);
            intent.putExtra(a.N, this.c);
            intent.setFlags(268435456);
            m.startActivity(intent);
        }
    }
}
