package com.tencent.assistant.utils;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import com.qq.AppService.AstApp;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistantv2.st.page.STInfoV2;

/* compiled from: ProGuard */
final class af extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Dialog f1816a;
    final /* synthetic */ Activity b;

    af(Dialog dialog, Activity activity) {
        this.f1816a = dialog;
        this.b = activity;
    }

    public void onTMAClick(View view) {
        this.f1816a.dismiss();
        this.b.finish();
        AstApp.i().f();
    }

    public STInfoV2 getStInfo() {
        STInfoV2 b2 = FunctionUtils.b(this.b);
        b2.slotId += "_01_002";
        b2.actionId = 200;
        return b2;
    }
}
