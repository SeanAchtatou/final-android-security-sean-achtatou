package com.tencent.assistant.utils;

import android.os.Handler;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

/* compiled from: ProGuard */
public class TemporaryThreadManager {

    /* renamed from: a  reason: collision with root package name */
    private static TemporaryThreadManager f1804a;
    /* access modifiers changed from: private */
    public ExecutorService b;
    private Handler c;

    private TemporaryThreadManager() {
        try {
            this.b = Executors.newFixedThreadPool(5, new q("temporary"));
            this.c = ah.a("temporary-thread-time-out-monitor");
        } catch (Throwable th) {
            this.b = Executors.newCachedThreadPool(new q("temporary"));
        }
    }

    public static synchronized TemporaryThreadManager get() {
        TemporaryThreadManager temporaryThreadManager;
        synchronized (TemporaryThreadManager.class) {
            if (f1804a == null) {
                f1804a = new TemporaryThreadManager();
            }
            temporaryThreadManager = f1804a;
        }
        return temporaryThreadManager;
    }

    public void start(Runnable runnable) {
        try {
            this.b.submit(runnable);
        } catch (Throwable th) {
        }
    }

    public void startDelayed(Runnable runnable, long j) {
        ah.a().postDelayed(new bj(this, runnable), j);
    }

    public <T> Future<T> start(Callable<T> callable, int i) {
        FutureTask futureTask = new FutureTask(callable);
        Future<T> submit = this.b.submit(callable);
        this.c.postDelayed(new bk(this, futureTask), (long) (i * 1000));
        return submit;
    }
}
