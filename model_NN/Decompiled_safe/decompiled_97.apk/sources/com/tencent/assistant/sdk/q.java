package com.tencent.assistant.sdk;

import android.content.Context;
import com.tencent.assistant.sdk.param.a;
import com.tencent.assistant.sdk.param.jce.IPCHead;
import com.tencent.assistant.sdk.param.jce.IPCRequest;

/* compiled from: ProGuard */
public class q {
    public static p a(Context context, byte[] bArr, String str) {
        IPCHead a2;
        IPCRequest a3 = a.a(bArr);
        if (a3 == null || (a2 = a3.a()) == null) {
            return null;
        }
        switch (a2.b) {
            case 1:
                return new m(context, a3);
            case 2:
            case 3:
            case 8:
            default:
                return null;
            case 4:
                return new t(context, a3);
            case 5:
                return new u(context, a3);
            case 6:
                return new b(context, a3);
            case 7:
                return new c(context, a3);
            case 9:
                return s.a(context, a3, str);
        }
    }
}
