package com.tencent.assistant.sdk.param;

import android.text.TextUtils;
import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceStruct;
import com.tencent.assistant.sdk.param.jce.IPCCmd;
import com.tencent.assistant.sdk.param.jce.IPCHead;
import com.tencent.assistant.sdk.param.jce.IPCRequest;
import com.tencent.assistant.sdk.param.jce.IPCResponse;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static int f1656a = 0;
    private static String b = Constants.STR_EMPTY;
    private static String c = Constants.STR_EMPTY;

    public static IPCResponse a(IPCHead iPCHead, JceStruct jceStruct) {
        if (jceStruct == null) {
            return null;
        }
        IPCResponse iPCResponse = new IPCResponse();
        iPCResponse.f1668a = iPCHead;
        iPCResponse.b = a(jceStruct);
        return iPCResponse;
    }

    public static byte[] a(IPCResponse iPCResponse) {
        if (iPCResponse == null) {
            return null;
        }
        iPCResponse.b = com.tencent.c.a.a.a(iPCResponse.b, "ji*9^&43U0X-~./(".getBytes());
        return a((JceStruct) iPCResponse);
    }

    public static IPCRequest a(byte[] bArr) {
        if (bArr == null || bArr.length < 4) {
            return null;
        }
        IPCRequest iPCRequest = new IPCRequest();
        try {
            JceInputStream jceInputStream = new JceInputStream(bArr);
            jceInputStream.setServerEncoding("utf-8");
            iPCRequest.readFrom(jceInputStream);
            if (TextUtils.isEmpty(iPCRequest.f1667a.c)) {
                return iPCRequest;
            }
            iPCRequest.b = com.tencent.c.a.a.b(iPCRequest.b, "ji*9^&43U0X-~./(".getBytes());
            return iPCRequest;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static JceStruct a(IPCRequest iPCRequest) {
        JceStruct a2 = a(IPCCmd.a(iPCRequest.f1667a.b).toString());
        if (a2 == null || iPCRequest.b.length <= 0) {
            return null;
        }
        try {
            JceInputStream jceInputStream = new JceInputStream(iPCRequest.b);
            jceInputStream.setServerEncoding("utf-8");
            a2.readFrom(jceInputStream);
            return a2;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static JceStruct a(String str) {
        String str2;
        JceStruct jceStruct;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            str2 = a.class.getPackage().getName();
        } catch (Exception e) {
            str2 = "com.tencent.assistant.sdk.param";
        }
        try {
            jceStruct = (JceStruct) Class.forName((str2 + ".jce." + str) + "Request").newInstance();
        } catch (ClassNotFoundException e2) {
            e2.printStackTrace();
            jceStruct = null;
        } catch (IllegalAccessException e3) {
            e3.printStackTrace();
            jceStruct = null;
        } catch (InstantiationException e4) {
            e4.printStackTrace();
            jceStruct = null;
        }
        return jceStruct;
    }

    public static byte[] a(JceStruct jceStruct) {
        return com.tencent.c.a.a.a(jceStruct);
    }
}
