package com.tencent.assistant.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.tencent.assistant.protocol.jce.ActionUrl;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class AppGroupInfo implements Parcelable {
    public static final Parcelable.Creator<AppGroupInfo> CREATOR = new a();

    /* renamed from: a  reason: collision with root package name */
    public int f935a = 0;
    public String b = Constants.STR_EMPTY;
    public String c = Constants.STR_EMPTY;
    public String d = Constants.STR_EMPTY;
    public byte e = 0;
    public int f = 0;
    public ActionUrl g = null;

    public void a(int i) {
        this.f935a = i;
    }

    public String a() {
        return this.b;
    }

    public void a(String str) {
        this.b = str;
    }

    public void b(String str) {
        this.c = str;
    }

    public void c(String str) {
        this.d = str;
    }

    public void a(byte b2) {
        this.e = b2;
    }

    public void b(int i) {
        this.f = i;
    }

    public void a(ActionUrl actionUrl) {
        this.g = actionUrl;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f935a);
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        parcel.writeByte(this.e);
        parcel.writeInt(this.f);
        if (this.g != null) {
            parcel.writeString(this.g.f1125a);
            parcel.writeInt(this.g.b);
            return;
        }
        parcel.writeString(Constants.STR_EMPTY);
        parcel.writeInt(0);
    }
}
