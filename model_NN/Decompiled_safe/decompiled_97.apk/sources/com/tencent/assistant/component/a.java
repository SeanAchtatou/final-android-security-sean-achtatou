package com.tencent.assistant.component;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import com.tencent.assistant.activity.BaseActivity;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.manager.b;
import com.tencent.assistant.module.k;
import com.tencent.assistant.plugin.PluginActivity;
import com.tencent.assistantv2.st.model.StatInfo;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.pangu.activity.AppDetailActivityV5;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;

/* compiled from: ProGuard */
class a extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppIconView f621a;

    a(AppIconView appIconView) {
        this.f621a = appIconView;
    }

    public void onTMAClick(View view) {
        DownloadInfo a2 = DownloadProxy.a().a(this.f621a.mAppModel);
        if (a2 != null && a2.needReCreateInfo(this.f621a.mAppModel)) {
            DownloadProxy.a().b(a2.downloadTicket);
            a2 = null;
        }
        StatInfo a3 = com.tencent.assistantv2.st.page.a.a(this.f621a.statInfo);
        if (a2 == null) {
            a2 = DownloadInfo.createDownloadInfo(this.f621a.mAppModel, a3);
            if ((view instanceof b) && this.f621a.mAppModel != null) {
                com.tencent.assistant.manager.a.a().a(this.f621a.mAppModel.q(), (b) view);
            }
        } else {
            a2.updateDownloadInfoStatInfo(a3);
        }
        if (!TextUtils.isEmpty(this.f621a.mAppModel.q())) {
            switch (d.f645a[k.d(this.f621a.mAppModel).ordinal()]) {
                case 1:
                case 2:
                    if (this.f621a.mProgressBar != null) {
                        this.f621a.mProgressBar.setVisibility(0);
                    }
                    com.tencent.pangu.download.a.a().b(a2.downloadTicket);
                    return;
                case 3:
                case 4:
                    if (this.f621a.mProgressBar != null) {
                        this.f621a.mProgressBar.setVisibility(0);
                    }
                    com.tencent.pangu.download.a.a().b(a2);
                    return;
                default:
                    if (this.f621a.iconClickListener != null) {
                        this.f621a.iconClickListener.onClick();
                        return;
                    }
                    Intent intent = new Intent(this.f621a.mContext, AppDetailActivityV5.class);
                    if (this.f621a.mContext instanceof BaseActivity) {
                        intent.putExtra(PluginActivity.PARAMS_PRE_ACTIVITY_TAG_NAME, ((BaseActivity) this.f621a.mContext).f());
                    }
                    intent.putExtra("simpleModeInfo", this.f621a.mAppModel);
                    intent.putExtra("statInfo", this.f621a.statInfo);
                    this.f621a.mContext.startActivity(intent);
                    return;
            }
        }
    }

    public STInfoV2 getStInfo() {
        if (this.f621a.statInfo == null || !(this.f621a.statInfo instanceof STInfoV2)) {
            return null;
        }
        this.f621a.statInfo.updateStatus(this.f621a.mAppModel);
        return (STInfoV2) this.f621a.statInfo;
    }
}
