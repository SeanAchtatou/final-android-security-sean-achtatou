package com.tencent.assistant.component.txscrollview;

import android.view.View;
import android.widget.AbsListView;

/* compiled from: ProGuard */
public interface IScrollListener {
    void onScroll(View view, int i, int i2, int i3);

    void onScrollStateChanged(AbsListView absListView, int i);
}
