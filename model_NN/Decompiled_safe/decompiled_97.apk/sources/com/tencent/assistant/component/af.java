package com.tencent.assistant.component;

import android.view.View;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.activity.PhotoBackupNewActivity;

/* compiled from: ProGuard */
class af implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PhotoBackupBottomView f627a;

    af(PhotoBackupBottomView photoBackupBottomView) {
        this.f627a = photoBackupBottomView;
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_backup /*2131166322*/:
                if (this.f627a.activity != null && (this.f627a.activity instanceof PhotoBackupNewActivity)) {
                    ((PhotoBackupNewActivity) this.f627a.activity).w();
                    return;
                }
                return;
            default:
                return;
        }
    }
}
