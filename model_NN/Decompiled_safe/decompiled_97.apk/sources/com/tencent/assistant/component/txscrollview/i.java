package com.tencent.assistant.component.txscrollview;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import com.tencent.assistant.component.invalidater.ViewInvalidateMessage;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.thumbnailCache.o;
import java.util.HashMap;

/* compiled from: ProGuard */
class i extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TXImageView f711a;

    i(TXImageView tXImageView) {
        this.f711a = tXImageView;
    }

    public void handleMessage(Message message) {
        Bitmap bitmap;
        if (message.what == 0) {
            o oVar = (o) message.obj;
            if (oVar != null) {
                Bitmap unused = this.f711a.mBitmap = oVar.f;
                if (this.f711a.invalidater != null) {
                    if (oVar.c().equals(this.f711a.mImageUrlString)) {
                        ViewInvalidateMessage viewInvalidateMessage = new ViewInvalidateMessage(0);
                        HashMap hashMap = new HashMap();
                        hashMap.put("URL", oVar.c());
                        hashMap.put("TYPE", Integer.valueOf(oVar.d()));
                        viewInvalidateMessage.params = hashMap;
                        viewInvalidateMessage.target = this.f711a.invalidateHandler;
                        this.f711a.invalidater.sendMessage(viewInvalidateMessage);
                    }
                } else if (oVar.d() == this.f711a.mImageType.getThumbnailRequestType() && oVar.c().equals(this.f711a.mImageUrlString) && (bitmap = oVar.f) != null && !bitmap.isRecycled()) {
                    try {
                        this.f711a.setImageBitmap(bitmap);
                    } catch (Throwable th) {
                        t.a().b();
                    }
                    boolean unused2 = this.f711a.mIsLoadFinish = true;
                    this.f711a.onImageLoadFinishCallListener(bitmap);
                }
            }
        } else {
            this.f711a.sendImageRequestToInvalidater();
        }
    }
}
