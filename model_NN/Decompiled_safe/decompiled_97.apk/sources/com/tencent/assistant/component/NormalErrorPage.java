package com.tencent.assistant.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.manager.NetworkMonitor;
import com.tencent.assistant.manager.t;
import com.tencent.assistant.net.APN;
import com.tencent.assistant.utils.by;

/* compiled from: ProGuard */
public class NormalErrorPage extends RelativeLayout implements NetworkMonitor.ConnectivityChangeListener {
    public static final int ERROR_TYPE_APPDETAIL_EMPTY = 5;
    public static final int ERROR_TYPE_EMPTY = 1;
    public static final int ERROR_TYPE_EMPTY_TO_HOME = 4;
    public static final int ERROR_TYPE_FAIL = 2;
    public static final int ERROR_TYPE_NO_WIFI = 3;
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public int currentState = 2;
    private View.OnClickListener defaultListener = new u(this);
    private TextView errorHint;
    private ImageView errorImg;
    private TextView errorText;
    private Button freshBtn;
    private LayoutInflater inflater;
    /* access modifiers changed from: private */
    public View.OnClickListener listener;

    public NormalErrorPage(Context context2) {
        super(context2);
        initView(context2);
    }

    public NormalErrorPage(Context context2, AttributeSet attributeSet, int i) {
        super(context2, attributeSet, i);
        initView(context2);
    }

    public NormalErrorPage(Context context2, AttributeSet attributeSet) {
        super(context2, attributeSet);
        initView(context2);
    }

    private void initView(Context context2) {
        this.context = context2;
        this.inflater = LayoutInflater.from(context2);
        this.inflater.inflate((int) R.layout.network_disconnected_page, this);
        this.errorImg = (ImageView) findViewById(R.id.no_wifi_image);
        this.errorText = (TextView) findViewById(R.id.no_wifi_text);
        this.errorHint = (TextView) findViewById(R.id.no_wifi_tips);
        this.freshBtn = (Button) findViewById(R.id.setting_network);
        this.freshBtn.setOnClickListener(this.defaultListener);
        t.a().a(this);
    }

    public void setErrorType(int i) {
        int i2 = R.drawable.icon_empty_nodata;
        this.currentState = i;
        switch (i) {
            case 1:
                this.errorText.setVisibility(0);
                this.errorHint.setVisibility(8);
                this.errorText.setText(getResources().getString(R.string.empty_content));
                this.errorHint.setText(getResources().getString(R.string.empty_content_tips));
                this.freshBtn.setText(getResources().getString(R.string.get_content_fail_btn_txt));
                this.freshBtn.setVisibility(0);
                break;
            case 2:
                i2 = R.drawable.network_disconnected;
                this.errorText.setVisibility(8);
                this.errorHint.setText(getResources().getString(R.string.get_content_fail_tips));
                this.freshBtn.setText(getResources().getString(R.string.get_content_fail_btn_txt));
                this.freshBtn.setVisibility(0);
                break;
            case 3:
                i2 = R.drawable.network_disconnected;
                this.errorText.setVisibility(8);
                this.errorText.setText(getResources().getString(R.string.disconnected));
                this.errorHint.setText(getResources().getString(R.string.disconnected_tips));
                this.freshBtn.setText(getResources().getString(R.string.disconnected_setting));
                this.freshBtn.setVisibility(0);
                break;
            case 4:
                this.errorText.setVisibility(8);
                this.errorText.setText(getResources().getString(R.string.empty_content));
                this.errorHint.setVisibility(8);
                this.freshBtn.setText(getResources().getString(R.string.to_home_btn_text));
                this.freshBtn.setVisibility(0);
                break;
            case 5:
                i2 = R.drawable.shafa;
                this.errorText.setVisibility(0);
                this.errorHint.setVisibility(8);
                this.errorText.setText(getResources().getString(R.string.empty_content));
                this.errorHint.setText(getResources().getString(R.string.empty_content_tips));
                this.freshBtn.setText(getResources().getString(R.string.get_content_fail_btn_txt));
                this.freshBtn.setVisibility(8);
                break;
            default:
                i2 = 0;
                break;
        }
        try {
            this.errorImg.setBackgroundResource(i2);
        } catch (Throwable th) {
            t.a().b();
        }
    }

    public void setErrorText(String str, String str2) {
        this.errorText.setText(str);
        this.errorHint.setText(str2);
    }

    public void setErrorTextVisibility(int i) {
        this.errorText.setVisibility(i);
    }

    public void setErrorImage(int i) {
        this.errorImg.setBackgroundResource(i);
    }

    public void setErrorHint(String str) {
        this.errorHint.setText(str);
    }

    public void setErrorHintTextSize(float f) {
        this.errorHint.setTextSize(0, f);
    }

    public void setErrorHintTextColor(int i) {
        this.errorHint.setTextColor(i);
    }

    public void setErrorHintVisibility(int i) {
        this.errorHint.setVisibility(i);
    }

    public void setFreshButtonVisibility(int i) {
        this.freshBtn.setVisibility(i);
    }

    public void setButtonText(String str) {
        this.freshBtn.setText(str);
    }

    public void setButtonSelector(int i) {
        this.freshBtn.setBackgroundResource(i);
    }

    public void setButtonMarginTop(float f) {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.freshBtn.getLayoutParams();
        if (layoutParams == null) {
            layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(3, R.id.no_wifi_image);
        }
        layoutParams.topMargin = by.a(getContext(), f);
        this.freshBtn.setLayoutParams(layoutParams);
    }

    public void setButtonClickListener(View.OnClickListener onClickListener) {
        this.listener = onClickListener;
    }

    public void onConnected(APN apn) {
        if (this.currentState == 4) {
            setErrorType(this.currentState);
            return;
        }
        setErrorType(2);
        if (this.freshBtn != null) {
            this.freshBtn.performClick();
        }
    }

    public void onDisconnected(APN apn) {
    }

    public void onConnectivityChanged(APN apn, APN apn2) {
    }

    public int getCurrentState() {
        return this.currentState;
    }

    public void destory() {
        t.a().b(this);
    }

    public void setVisibility(int i) {
        if (!(this.errorImg == null || i == 0)) {
            this.errorImg.setBackgroundResource(0);
        }
        super.setVisibility(i);
    }
}
