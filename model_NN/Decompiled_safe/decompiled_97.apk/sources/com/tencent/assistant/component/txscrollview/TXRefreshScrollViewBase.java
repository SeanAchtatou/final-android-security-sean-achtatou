package com.tencent.assistant.component.txscrollview;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;

/* compiled from: ProGuard */
public abstract class TXRefreshScrollViewBase<T extends View> extends TXScrollViewBase<T> {
    protected static int l = 0;
    protected TXLoadingLayoutBase g;
    protected TXLoadingLayoutBase h;
    protected boolean i = true;
    protected RefreshState j = RefreshState.RESET;
    protected ITXRefreshListViewListener k = null;

    /* compiled from: ProGuard */
    public enum RefreshState {
        RESET,
        PULL_TO_REFRESH,
        RELEASE_TO_REFRESH,
        REFRESHING,
        REFRESH_LOAD_FINISH
    }

    /* access modifiers changed from: protected */
    public abstract TXLoadingLayoutBase a(Context context, TXScrollViewBase.ScrollMode scrollMode);

    public TXRefreshScrollViewBase(Context context, TXScrollViewBase.ScrollDirection scrollDirection, TXScrollViewBase.ScrollMode scrollMode) {
        super(context, scrollDirection, scrollMode);
    }

    public TXRefreshScrollViewBase(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
    }

    public TXRefreshScrollViewBase(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public TXRefreshScrollViewBase(Context context) {
        super(context);
    }

    public void onRefreshComplete(boolean z) {
        if (this.j != RefreshState.REFRESHING) {
            return;
        }
        if (!z) {
            a(RefreshState.REFRESH_LOAD_FINISH);
        } else {
            a(RefreshState.RESET);
        }
    }

    /* access modifiers changed from: protected */
    public void c(Context context) {
        if (this.o == TXScrollViewBase.ScrollMode.PULL_FROM_START) {
            this.g = a(context, TXScrollViewBase.ScrollMode.PULL_FROM_START);
        } else if (this.o == TXScrollViewBase.ScrollMode.PULL_FROM_END) {
            this.h = a(context, TXScrollViewBase.ScrollMode.PULL_FROM_END);
        } else if (this.o == TXScrollViewBase.ScrollMode.BOTH) {
            this.g = a(context, TXScrollViewBase.ScrollMode.PULL_FROM_START);
            this.h = a(context, TXScrollViewBase.ScrollMode.PULL_FROM_END);
        }
        super.c(context);
        a();
        g();
    }

    /* access modifiers changed from: protected */
    public void a(int i2, int i3) {
        l();
        super.a(i2, i3);
    }

    private void g() {
        try {
            l = (int) getContext().getResources().getDimension(R.dimen.refresh_header_loading_layout_content_height);
        } catch (Resources.NotFoundException e) {
        }
    }

    /* access modifiers changed from: protected */
    public boolean j() {
        if (!this.m) {
            return false;
        }
        this.m = false;
        if (this.j == RefreshState.RELEASE_TO_REFRESH) {
            a(RefreshState.REFRESHING);
            setVerticalFadingEdgeEnabled(false);
            return true;
        } else if (this.j == RefreshState.REFRESHING) {
            if (!(this instanceof RankRefreshGetMoreListView) || this.n != TXScrollViewBase.ScrollState.ScrollState_FromStart) {
                smoothScrollTo(0);
                return true;
            }
            a(RefreshState.REFRESHING);
            setVerticalFadingEdgeEnabled(false);
            return true;
        } else if (this.j == RefreshState.REFRESH_LOAD_FINISH) {
            smoothScrollTo(0);
            return true;
        } else {
            a(RefreshState.RESET);
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public int i() {
        int i2 = super.i();
        int i3 = 0;
        if (!(i2 == 0 || this.j == RefreshState.REFRESHING)) {
            if (this.n == TXScrollViewBase.ScrollState.ScrollState_FromStart && this.g != null) {
                i3 = this.g.getTriggerSize();
                this.g.onPull(i2);
            } else if (this.n == TXScrollViewBase.ScrollState.ScrollState_FromEnd && this.h != null) {
                i3 = this.h.getContentSize();
                this.h.onPull(i2);
            }
            if (this.n == TXScrollViewBase.ScrollState.ScrollState_FromEnd && this.j == RefreshState.REFRESH_LOAD_FINISH) {
                return i2;
            }
            if (this.j != RefreshState.PULL_TO_REFRESH && i3 >= Math.abs(i2)) {
                a(RefreshState.PULL_TO_REFRESH);
            } else if (this.j == RefreshState.PULL_TO_REFRESH && i3 < Math.abs(i2)) {
                a(RefreshState.RELEASE_TO_REFRESH);
            }
        }
        return i2;
    }

    /* access modifiers changed from: protected */
    public void a(int i2) {
        super.a(i2);
        if (!this.i) {
            return;
        }
        if (i2 < 0 && this.g != null) {
            this.g.setVisibility(0);
        } else if (i2 <= 0 || this.h == null) {
            if (this.g != null) {
                this.g.setVisibility(4);
            }
            if (this.h != null) {
                this.h.setVisibility(4);
            }
        } else {
            this.h.setVisibility(0);
        }
    }

    /* access modifiers changed from: protected */
    public LinearLayout.LayoutParams k() {
        if (this.p == TXScrollViewBase.ScrollDirection.SCROLL_DIRECTION_HORIZONTAL) {
            return new LinearLayout.LayoutParams(-2, -1);
        }
        return new LinearLayout.LayoutParams(-1, -2);
    }

    /* access modifiers changed from: protected */
    public void a() {
        LinearLayout.LayoutParams k2 = k();
        if (this.g != null) {
            if (this.g.getParent() == this) {
                removeView(this.g);
            }
            a(this.g, 0, k2);
        }
        if (this.h != null) {
            if (this.h.getParent() == this) {
                removeView(this.h);
            }
            a(this.h, -1, k2);
        }
        l();
    }

    /* access modifiers changed from: protected */
    public final void l() {
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7 = 0;
        int o = (int) (((float) o()) * 1.2f);
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();
        if (this.p == TXScrollViewBase.ScrollDirection.SCROLL_DIRECTION_HORIZONTAL) {
            if ((this.o == TXScrollViewBase.ScrollMode.PULL_FROM_START || this.o == TXScrollViewBase.ScrollMode.BOTH) && this.g != null) {
                this.g.setWidth(o);
                i6 = -o;
            } else {
                i6 = 0;
            }
            if ((this.o == TXScrollViewBase.ScrollMode.PULL_FROM_END || this.o == TXScrollViewBase.ScrollMode.BOTH) && this.h != null) {
                this.h.setWidth(o);
                i5 = i6;
                i4 = -o;
                i7 = paddingBottom;
                i3 = paddingTop;
            } else {
                i5 = i6;
                i4 = 0;
                i7 = paddingBottom;
                i3 = paddingTop;
            }
        } else {
            if ((this.o == TXScrollViewBase.ScrollMode.PULL_FROM_START || this.o == TXScrollViewBase.ScrollMode.BOTH) && this.g != null) {
                this.g.setHeight(o);
                i2 = -o;
            } else {
                i2 = 0;
            }
            if ((this.o == TXScrollViewBase.ScrollMode.PULL_FROM_END || this.o == TXScrollViewBase.ScrollMode.BOTH) && this.h != null) {
                this.h.setHeight(o);
                i7 = -o;
                i3 = i2;
                i4 = paddingRight;
                i5 = paddingLeft;
            } else {
                i3 = i2;
                i4 = paddingRight;
                i5 = paddingLeft;
            }
        }
        setPadding(i5, i3, i4, i7);
    }

    /* access modifiers changed from: protected */
    public final void a(View view, int i2, ViewGroup.LayoutParams layoutParams) {
        super.a(view, i2, layoutParams);
    }

    /* access modifiers changed from: protected */
    public void a(RefreshState refreshState) {
        this.j = refreshState;
        switch (r.f720a[refreshState.ordinal()]) {
            case 1:
                b();
                return;
            case 2:
                m();
                return;
            case 3:
                n();
                return;
            case 4:
                c();
                return;
            case 5:
                d();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        this.m = false;
        this.i = true;
        if (this.g != null) {
            this.g.reset();
        }
        if (this.h != null) {
            this.h.reset();
        }
        smoothScrollTo(0);
    }

    /* access modifiers changed from: protected */
    public void m() {
        if (this.n == TXScrollViewBase.ScrollState.ScrollState_FromStart && this.g != null) {
            this.g.setVisibility(0);
            this.g.pullToRefresh();
        } else if (this.n == TXScrollViewBase.ScrollState.ScrollState_FromEnd && this.h != null) {
            this.h.setVisibility(0);
            this.h.pullToRefresh();
        }
    }

    /* access modifiers changed from: protected */
    public void n() {
        if (this.n == TXScrollViewBase.ScrollState.ScrollState_FromStart && this.g != null) {
            this.g.releaseToRefresh();
        } else if (this.n == TXScrollViewBase.ScrollState.ScrollState_FromEnd && this.h != null) {
            this.h.releaseToRefresh();
        }
    }

    /* access modifiers changed from: protected */
    public void c() {
        if ((this.o == TXScrollViewBase.ScrollMode.PULL_FROM_START || this.o == TXScrollViewBase.ScrollMode.BOTH) && this.g != null) {
            this.g.refreshing();
        }
        if ((this.o == TXScrollViewBase.ScrollMode.PULL_FROM_END || this.o == TXScrollViewBase.ScrollMode.BOTH) && this.h != null) {
            this.h.refreshing();
        }
        q qVar = new q(this);
        if (this.n == TXScrollViewBase.ScrollState.ScrollState_FromStart && this.g != null) {
            smoothScrollTo(-this.g.getContentSize(), qVar);
        } else if (this.n == TXScrollViewBase.ScrollState.ScrollState_FromEnd && this.h != null) {
            smoothScrollTo(this.h.getContentSize(), qVar);
        }
    }

    /* access modifiers changed from: protected */
    public void d() {
        b();
    }

    public void setRefreshListViewListener(ITXRefreshListViewListener iTXRefreshListViewListener) {
        this.k = iTXRefreshListViewListener;
    }
}
