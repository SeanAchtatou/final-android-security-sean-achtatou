package com.tencent.assistant.component.invalidater;

/* compiled from: ProGuard */
public interface IViewInvalidater {
    void dispatchEmptyMessage(int i);

    void dispatchMessage(ViewInvalidateMessage viewInvalidateMessage);

    void handleMessage(ViewInvalidateMessage viewInvalidateMessage);

    void sendEmptyMessage(int i);

    void sendMessage(ViewInvalidateMessage viewInvalidateMessage);
}
