package com.tencent.assistant.component;

import android.content.Context;
import android.graphics.Canvas;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.Scroller;

/* compiled from: ProGuard */
public class HorizonScrollLayout extends ViewGroup {
    private static final int INVALID_SCREEN = -999;
    private static final int SNAP_VELOCITY = 600;
    private static final String TAG = "HorizonScrollLayout";
    private static final int TOUCH_STATE_LOCK = 2;
    private static final int TOUCH_STATE_REST = 0;
    private static final int TOUCH_STATE_SCROLLING = 1;
    private float mAngleSlop;
    boolean mChangeCoordinate;
    private int mChildScreenWidth;
    private int mCurScreen;
    private int mDefaultScreen;
    private boolean mEnableOverScroll;
    private boolean mEnableScroll;
    boolean mIsCircle;
    private float mLastMotionX;
    private float mLastMotionY;
    private boolean mLockAllWhenTouch;
    private float mMaxScrollX;
    private float mMinScrollX;
    private int mNextScreen;
    private float mPreScrollX;
    private int mScreenWidth;
    private int mScrollState;
    int mScrollX;
    int mScrollY;
    private Scroller mScroller;
    private OnTouchScrollListener mTouchScrollListener;
    private int mTouchSlop;
    private int mTouchState;
    private VelocityTracker mVelocityTracker;

    /* compiled from: ProGuard */
    public interface OnTouchScrollListener {
        public static final int SCROLL_STATE_FLING = 2;
        public static final int SCROLL_STATE_IDLE = 0;
        public static final int SCROLL_STATE_TOUCH_SCROLL = 1;

        void onScreenChange(int i);

        void onScroll(View view, float f, float f2);

        void onScrollStateChanged(int i, int i2);
    }

    public HorizonScrollLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        this.mScroller = new Scroller(context);
        this.mCurScreen = this.mDefaultScreen;
        this.mTouchSlop = ViewConfiguration.get(getContext()).getScaledTouchSlop();
    }

    public HorizonScrollLayout(Context context) {
        super(context);
        this.mCurScreen = 0;
        this.mDefaultScreen = 0;
        this.mChildScreenWidth = 0;
        this.mScreenWidth = 0;
        this.mTouchState = 0;
        this.mTouchSlop = 24;
        this.mAngleSlop = 0.577f;
        this.mMinScrollX = 0.0f;
        this.mMaxScrollX = 0.0f;
        this.mPreScrollX = 0.0f;
        this.mEnableOverScroll = true;
        this.mScrollState = 0;
        this.mEnableScroll = true;
        this.mLockAllWhenTouch = false;
        this.mTouchScrollListener = null;
        this.mNextScreen = INVALID_SCREEN;
        this.mIsCircle = false;
        this.mChangeCoordinate = true;
        this.mScroller = new Scroller(context);
        this.mCurScreen = this.mDefaultScreen;
        this.mTouchSlop = ViewConfiguration.get(getContext()).getScaledTouchSlop();
    }

    public HorizonScrollLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.mCurScreen = 0;
        this.mDefaultScreen = 0;
        this.mChildScreenWidth = 0;
        this.mScreenWidth = 0;
        this.mTouchState = 0;
        this.mTouchSlop = 24;
        this.mAngleSlop = 0.577f;
        this.mMinScrollX = 0.0f;
        this.mMaxScrollX = 0.0f;
        this.mPreScrollX = 0.0f;
        this.mEnableOverScroll = true;
        this.mScrollState = 0;
        this.mEnableScroll = true;
        this.mLockAllWhenTouch = false;
        this.mTouchScrollListener = null;
        this.mNextScreen = INVALID_SCREEN;
        this.mIsCircle = false;
        this.mChangeCoordinate = true;
        this.mScroller = new Scroller(context);
        this.mCurScreen = this.mDefaultScreen;
        this.mTouchSlop = ViewConfiguration.get(getContext()).getScaledTouchSlop();
    }

    public void setEnableOverScroll(boolean z) {
        this.mEnableOverScroll = z;
    }

    public void setScrollSlop(float f) {
        this.mAngleSlop = f;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int childCount = getChildCount();
        int i5 = 0;
        int i6 = 0;
        while (true) {
            if (i5 >= childCount) {
                break;
            }
            View childAt = getChildAt(i5);
            if (childAt.getVisibility() != 8) {
                int measuredWidth = childAt.getMeasuredWidth();
                if (!z) {
                    if (i5 == this.mCurScreen) {
                        childAt.layout(i6, 0, i6 + measuredWidth, childAt.getMeasuredHeight());
                        childAt.postInvalidate();
                        break;
                    }
                } else {
                    childAt.layout(i6, 0, i6 + measuredWidth, childAt.getMeasuredHeight());
                    childAt.postInvalidate();
                }
                i6 += measuredWidth;
            }
            i5++;
        }
        this.mChildScreenWidth = getWidth();
        this.mScreenWidth = this.mChildScreenWidth * childCount;
        if (this.mEnableOverScroll) {
            this.mMinScrollX = (float) (-(this.mChildScreenWidth >> 2));
            this.mMaxScrollX = ((float) (this.mScreenWidth - this.mChildScreenWidth)) - this.mMinScrollX;
        } else {
            this.mMinScrollX = 0.0f;
            this.mMaxScrollX = (float) (this.mScreenWidth - this.mChildScreenWidth);
        }
        if (z) {
            if (!this.mScroller.isFinished()) {
                this.mScroller.abortAnimation();
            }
            scrollTo(this.mCurScreen * getWidth(), 0);
        }
    }

    public void layoutChild(int i) {
        View childAt = getChildAt(i);
        if (childAt != null) {
            childAt.requestLayout();
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        View.MeasureSpec.getSize(i);
        if (View.MeasureSpec.getMode(i) != 1073741824) {
        }
        if (View.MeasureSpec.getMode(i2) != 1073741824) {
        }
        int childCount = getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            getChildAt(i3).measure(i, i2);
        }
    }

    /* access modifiers changed from: protected */
    public void snapToDestination() {
        int width = getWidth();
        snapToScreen((this.mScrollX + (width / 2)) / width, true);
    }

    /* access modifiers changed from: protected */
    public void snapToScreen(int i, boolean z) {
        int max = Math.max(0, Math.min(i, getChildCount() - 1));
        this.mNextScreen = max;
        if (this.mScrollX != getWidth() * max) {
            if (!(!z || this.mTouchScrollListener == null || this.mCurScreen == max)) {
                this.mTouchScrollListener.onScreenChange(max);
            }
            int width = (getWidth() * max) - this.mScrollX;
            this.mScroller.startScroll(this.mScrollX, 0, width, 0, (int) (Math.atan(Math.abs((3.141592653589793d * ((double) width)) / 1000.0d)) * 1000.0d));
            this.mCurScreen = max;
            layoutChild(this.mCurScreen);
            invalidate();
        }
    }

    /* access modifiers changed from: protected */
    public void snapToLastScreen() {
        int childCount = getChildCount();
        if (childCount != 0) {
            this.mNextScreen = childCount - 1;
            if (this.mScrollX != (childCount - 1) * getWidth()) {
                if (this.mTouchScrollListener != null) {
                    this.mTouchScrollListener.onScreenChange(this.mNextScreen);
                }
                if (this.mScrollX < 0 && this.mScrollX > 0 - (getWidth() / 2)) {
                    this.mScrollX = (getWidth() * childCount) + this.mScrollX;
                    this.mChangeCoordinate = false;
                    scrollTo(this.mScrollX, this.mScrollY);
                    this.mChangeCoordinate = true;
                }
                int width = 0 - (this.mScrollX - ((childCount - 1) * getWidth()));
                this.mScroller.startScroll(this.mScrollX, 0, width, 0, (int) (Math.atan(Math.abs((3.141592653589793d * ((double) width)) / 1000.0d)) * 1000.0d));
                this.mCurScreen = this.mNextScreen;
                layoutChild(this.mCurScreen);
                invalidate();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void snapToFirstScreen() {
        this.mNextScreen = 0;
        if (this.mScrollX != 0) {
            if (this.mTouchScrollListener != null) {
                this.mTouchScrollListener.onScreenChange(this.mNextScreen);
            }
            if (this.mScrollX > ((getChildCount() - 1) * getWidth()) - 1 && this.mScrollX < (getChildCount() * getWidth()) - (getWidth() / 2)) {
                this.mScrollX -= getChildCount() * getWidth();
                this.mChangeCoordinate = false;
                scrollTo(this.mScrollX, this.mScrollY);
                this.mChangeCoordinate = true;
            }
            int i = 0 - this.mScrollX;
            this.mScroller.startScroll(this.mScrollX, 0, i, 0, (int) (Math.atan(Math.abs((3.141592653589793d * ((double) i)) / 1000.0d)) * 1000.0d));
            this.mCurScreen = this.mNextScreen;
            layoutChild(this.mCurScreen);
            invalidate();
        }
    }

    public void displayNextScreen() {
        if (this.mCurScreen < getChildCount() - 1) {
            setDisplayedChild(this.mCurScreen + 1, true);
        } else {
            snapToFirstScreen();
        }
    }

    public void displayPreScreen() {
        if (this.mCurScreen > 0) {
            setDisplayedChild(this.mCurScreen - 1, true);
        } else {
            snapToLastScreen();
        }
    }

    public int getCurScreen() {
        return this.mCurScreen;
    }

    public void computeScroll() {
        if (this.mScroller.computeScrollOffset()) {
            scrollTo(this.mScroller.getCurrX(), this.mScroller.getCurrY());
            postInvalidate();
            if (this.mTouchScrollListener != null) {
                if (this.mScroller.isFinished()) {
                    if (this.mScrollState != 0) {
                        this.mTouchScrollListener.onScrollStateChanged(0, this.mCurScreen);
                        this.mScrollState = 0;
                    }
                    this.mNextScreen = INVALID_SCREEN;
                } else if (this.mScrollState != 2) {
                    this.mTouchScrollListener.onScrollStateChanged(2, this.mCurScreen);
                    this.mScrollState = 2;
                }
            }
        } else if (!(this.mTouchScrollListener == null || this.mScrollState == 1 || this.mTouchState != 1)) {
            this.mTouchScrollListener.onScrollStateChanged(1, this.mCurScreen);
            this.mScrollState = 1;
        }
        if (this.mPreScrollX != ((float) getScrollX())) {
            this.mPreScrollX = (float) getScrollX();
            if (this.mTouchScrollListener != null) {
                this.mTouchScrollListener.onScroll(this, (float) getScrollX(), (float) this.mScreenWidth);
            }
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.mEnableScroll) {
            if (this.mVelocityTracker == null) {
                this.mVelocityTracker = VelocityTracker.obtain();
            }
            this.mVelocityTracker.addMovement(motionEvent);
            int action = motionEvent.getAction();
            float rawX = motionEvent.getRawX();
            motionEvent.getRawY();
            switch (action) {
                case 0:
                    if (!this.mScroller.isFinished()) {
                        this.mScroller.abortAnimation();
                    }
                    this.mLastMotionX = rawX;
                    break;
                case 1:
                case 3:
                    VelocityTracker velocityTracker = this.mVelocityTracker;
                    velocityTracker.computeCurrentVelocity(1000);
                    int xVelocity = (int) velocityTracker.getXVelocity();
                    if (xVelocity > SNAP_VELOCITY && this.mCurScreen >= 0) {
                        int i = this.mCurScreen - 1;
                        if (!this.mIsCircle || i >= 0) {
                            snapToScreen(i, true);
                        } else {
                            int childCount = i + getChildCount();
                            snapToLastScreen();
                        }
                    } else if (xVelocity >= -600 || this.mCurScreen > getChildCount() - 1) {
                        snapToDestination();
                    } else {
                        int i2 = this.mCurScreen + 1;
                        if (!this.mIsCircle || i2 <= getChildCount() - 1) {
                            snapToScreen(i2, true);
                        } else {
                            int childCount2 = i2 - getChildCount();
                            snapToFirstScreen();
                        }
                    }
                    if (this.mVelocityTracker != null) {
                        this.mVelocityTracker.recycle();
                        this.mVelocityTracker = null;
                    }
                    this.mTouchState = 0;
                    break;
                case 2:
                    if (this.mTouchState != 2) {
                        if (getParent() != null) {
                            getParent().requestDisallowInterceptTouchEvent(true);
                        }
                        int i3 = (int) (this.mLastMotionX - rawX);
                        this.mLastMotionX = rawX;
                        int scrollX = getScrollX() + i3;
                        if (this.mIsCircle || (((float) scrollX) > this.mMinScrollX && ((float) scrollX) < this.mMaxScrollX)) {
                            scrollBy(i3, 0);
                            break;
                        }
                    }
                    break;
            }
        }
        return true;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (this.mLockAllWhenTouch) {
            ViewParent parent = getParent();
            while (parent != null && !(parent instanceof ViewPager)) {
                parent.requestDisallowInterceptTouchEvent(true);
                parent = parent.getParent();
            }
        }
        if (!this.mEnableScroll) {
            return false;
        }
        int action = motionEvent.getAction();
        if (action == 2 && this.mTouchState == 1) {
            return true;
        }
        float rawX = motionEvent.getRawX();
        float rawY = motionEvent.getRawY();
        switch (action) {
            case 0:
                this.mLastMotionX = rawX;
                this.mLastMotionY = rawY;
                this.mTouchState = this.mScroller.isFinished() ? 0 : 1;
                break;
            case 1:
            case 3:
                this.mTouchState = 0;
                break;
            case 2:
                if (this.mTouchState != 2) {
                    float abs = Math.abs(this.mLastMotionX - rawX);
                    float abs2 = Math.abs(this.mLastMotionY - rawY);
                    if (abs > ((float) this.mTouchSlop)) {
                        if (abs2 / abs >= this.mAngleSlop) {
                            this.mTouchState = 2;
                            break;
                        } else {
                            this.mTouchState = 1;
                            break;
                        }
                    }
                }
                break;
        }
        if (this.mTouchState != 1) {
            return false;
        }
        return true;
    }

    public void setDisplayedChild(int i) {
        snapToScreen(i, false);
    }

    public void setDisplayedChild(int i, boolean z) {
        snapToScreen(i, z);
    }

    public void setDisplayedChildNoAmin(int i) {
        if (this.mCurScreen != i) {
            int max = Math.max(0, Math.min(i, getChildCount() - 1));
            this.mCurScreen = max;
            scrollTo(getWidth() * max, 0);
            if (this.mTouchScrollListener != null) {
                this.mTouchScrollListener.onScreenChange(max);
            }
            layoutChild(this.mCurScreen);
            invalidate();
        }
    }

    public int getDisplayedChild() {
        return this.mCurScreen;
    }

    public void setOnTouchScrollListener(OnTouchScrollListener onTouchScrollListener) {
        this.mTouchScrollListener = onTouchScrollListener;
    }

    public OnTouchScrollListener getOnTouchScrollListener() {
        return this.mTouchScrollListener;
    }

    public void setTouchScrollEnable(boolean z) {
        this.mEnableScroll = z;
    }

    public boolean getEnableScroll() {
        return this.mEnableScroll;
    }

    public void setLockAllWhenTouch(boolean z) {
        this.mLockAllWhenTouch = z;
    }

    public void setDefaultScreem(int i) {
        this.mDefaultScreen = i;
        this.mCurScreen = this.mDefaultScreen;
    }

    public int getDefaultScreem() {
        return this.mDefaultScreen;
    }

    public void destroy() {
        this.mScroller = null;
        this.mVelocityTracker = null;
        this.mTouchScrollListener = null;
    }

    public void scrollTo(int i, int i2) {
        if (this.mIsCircle && this.mChangeCoordinate) {
            int width = getWidth();
            int childCount = getChildCount();
            if (i <= (-width) / 2) {
                i += width * childCount;
            } else if (i >= (width * childCount) - (width / 2)) {
                i -= width * childCount;
            }
        }
        this.mScrollX = i;
        this.mScrollY = i2;
        super.scrollTo(i, i2);
    }

    public void setCircle(boolean z) {
        this.mIsCircle = z;
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        boolean z;
        int min;
        int i;
        boolean z2 = false;
        if (!this.mIsCircle || this.mCurScreen >= getChildCount() || this.mCurScreen < 0) {
            super.dispatchDraw(canvas);
            return;
        }
        if (this.mTouchState == 1 || this.mNextScreen != INVALID_SCREEN) {
            z = false;
        } else {
            z = true;
        }
        if (z) {
            drawChild(canvas, getChildAt(this.mCurScreen), getDrawingTime());
            return;
        }
        long drawingTime = getDrawingTime();
        int width = getWidth();
        float scrollX = ((float) getScrollX()) / ((float) width);
        int childCount = getChildCount();
        if (scrollX < 0.0f) {
            min = childCount - 1;
            i = 0;
        } else {
            min = Math.min((int) scrollX, childCount - 1);
            i = (min + 1) % childCount;
            z2 = true;
        }
        if (isScreenNoValid(min)) {
            if (i != 0 || z2) {
                drawChild(canvas, getChildAt(min), drawingTime);
            } else {
                int i2 = childCount * width;
                canvas.translate((float) (-i2), 0.0f);
                drawChild(canvas, getChildAt(min), drawingTime);
                canvas.translate((float) i2, 0.0f);
            }
        }
        if (((double) Math.abs(scrollX - ((float) min))) > 0.01d && isScreenNoValid(i)) {
            if (i != 0 || !z2) {
                drawChild(canvas, getChildAt(i), drawingTime);
                return;
            }
            int i3 = childCount * width;
            canvas.translate((float) i3, 0.0f);
            drawChild(canvas, getChildAt(i), drawingTime);
            canvas.translate((float) (-i3), 0.0f);
        }
    }

    private boolean isScreenNoValid(int i) {
        return i >= 0 && i < getChildCount();
    }
}
