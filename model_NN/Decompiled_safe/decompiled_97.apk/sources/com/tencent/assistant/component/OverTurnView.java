package com.tencent.assistant.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ViewSwitcher;

/* compiled from: ProGuard */
public abstract class OverTurnView extends ViewSwitcher implements ViewSwitcher.ViewFactory {
    private Animation mInDown;
    private Animation mInUp;
    private Animation mOutDown;
    private Animation mOutUp;

    public abstract void doBeforeInit(Context context);

    public abstract View makeView();

    public abstract Animation setInDownAnim(Context context);

    public abstract Animation setInUpAnim(Context context);

    public abstract Animation setOutDownAnim(Context context);

    public abstract Animation setOutUpAnim(Context context);

    public OverTurnView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        doBeforeInit(context);
        this.mInDown = setInDownAnim(context);
        this.mOutDown = setOutDownAnim(context);
        this.mInUp = setInUpAnim(context);
        this.mOutUp = setOutUpAnim(context);
        init();
    }

    /* access modifiers changed from: protected */
    public void init() {
        setFactory(this);
    }

    public void previous() {
        if (getInAnimation() != this.mInDown) {
            setInAnimation(this.mInDown);
        }
        if (getOutAnimation() != this.mOutDown) {
            setOutAnimation(this.mOutDown);
        }
        showPrevious();
    }

    public void next() {
        if (getInAnimation() != this.mInUp) {
            setInAnimation(this.mInUp);
        }
        if (getOutAnimation() != this.mOutUp) {
            setOutAnimation(this.mOutUp);
        }
        showNext();
    }
}
