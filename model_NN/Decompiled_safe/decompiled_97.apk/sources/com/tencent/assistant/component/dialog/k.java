package com.tencent.assistant.component.dialog;

import android.content.DialogInterface;
import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
final class k implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppConst.TwoBtnDialogInfo f657a;

    k(AppConst.TwoBtnDialogInfo twoBtnDialogInfo) {
        this.f657a = twoBtnDialogInfo;
    }

    public void onCancel(DialogInterface dialogInterface) {
        this.f657a.onCancell();
    }
}
