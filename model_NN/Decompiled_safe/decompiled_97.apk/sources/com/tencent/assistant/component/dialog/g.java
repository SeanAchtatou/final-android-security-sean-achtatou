package com.tencent.assistant.component.dialog;

import android.app.Dialog;
import com.tencent.assistant.AppConst;
import com.tencent.pangu.component.bq;

/* compiled from: ProGuard */
final class g implements bq {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppConst.TwoBtnDialogInfo f653a;
    final /* synthetic */ Dialog b;

    g(AppConst.TwoBtnDialogInfo twoBtnDialogInfo, Dialog dialog) {
        this.f653a = twoBtnDialogInfo;
        this.b = dialog;
    }

    public void a() {
        if (this.f653a != null) {
            this.f653a.onRightBtnClick();
            this.b.dismiss();
        }
    }

    public void b() {
        if (this.f653a != null) {
            this.f653a.onLeftBtnClick();
            this.b.dismiss();
        }
    }
}
