package com.tencent.assistant.component.dialog;

import android.content.DialogInterface;
import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
final class c implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppConst.OneBtnDialogInfo f649a;

    c(AppConst.OneBtnDialogInfo oneBtnDialogInfo) {
        this.f649a = oneBtnDialogInfo;
    }

    public void onCancel(DialogInterface dialogInterface) {
        this.f649a.onCancell();
    }
}
