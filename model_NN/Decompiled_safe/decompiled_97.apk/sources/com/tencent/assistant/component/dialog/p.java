package com.tencent.assistant.component.dialog;

import android.app.Dialog;
import android.view.View;
import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
final class p implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppConst.TwoBtnDialogInfo f662a;
    final /* synthetic */ Dialog b;

    p(AppConst.TwoBtnDialogInfo twoBtnDialogInfo, Dialog dialog) {
        this.f662a = twoBtnDialogInfo;
        this.b = dialog;
    }

    public void onClick(View view) {
        if (this.f662a != null) {
            this.f662a.onRightBtnClick();
            this.b.dismiss();
        }
    }
}
