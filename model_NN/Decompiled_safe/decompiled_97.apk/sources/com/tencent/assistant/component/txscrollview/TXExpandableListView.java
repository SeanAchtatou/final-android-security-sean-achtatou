package com.tencent.assistant.component.txscrollview;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ListAdapter;
import com.tencent.assistant.component.txscrollview.TXRefreshScrollViewBase;
import com.tencent.assistant.component.txscrollview.TXScrollViewBase;
import com.tencent.assistant.utils.bd;

/* compiled from: ProGuard */
public class TXExpandableListView extends TXScrollViewBase<ExpandableListView> implements AbsListView.OnScrollListener {

    /* renamed from: a  reason: collision with root package name */
    protected View f690a;
    protected TXLoadingLayoutBase b;
    protected ITXRefreshListViewListener c = null;
    protected AbsListView.OnScrollListener d = null;
    protected TXRefreshScrollViewBase.RefreshState e = TXRefreshScrollViewBase.RefreshState.RESET;
    private ExpandableListAdapter f;
    private boolean g = false;

    public TXExpandableListView(Context context) {
        super(context, TXScrollViewBase.ScrollDirection.SCROLL_DIRECTION_VERTICAL, TXScrollViewBase.ScrollMode.NONE);
    }

    public TXExpandableListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void onRefreshComplete(boolean z) {
        onRefreshComplete(z, true);
    }

    public void onRefreshComplete(boolean z, boolean z2) {
        if (this.e == TXRefreshScrollViewBase.RefreshState.REFRESHING) {
            if (z) {
                this.e = TXRefreshScrollViewBase.RefreshState.RESET;
            } else {
                this.e = TXRefreshScrollViewBase.RefreshState.REFRESH_LOAD_FINISH;
            }
        } else if (this.e == TXRefreshScrollViewBase.RefreshState.REFRESH_LOAD_FINISH) {
            this.e = TXRefreshScrollViewBase.RefreshState.RESET;
        } else if (this.e == TXRefreshScrollViewBase.RefreshState.RESET && !z) {
            this.e = TXRefreshScrollViewBase.RefreshState.REFRESH_LOAD_FINISH;
        }
        a(z2);
    }

    public void setAdapter(ExpandableListAdapter expandableListAdapter) {
        if (expandableListAdapter != null) {
            this.f = expandableListAdapter;
            if (this.s != null) {
                ((ExpandableListView) this.s).setAdapter(expandableListAdapter);
            }
        }
    }

    public ExpandableListAdapter getRawAdapter() {
        return this.f;
    }

    public ExpandableListView getContentView() {
        return (ExpandableListView) this.s;
    }

    public void setDivider(Drawable drawable) {
        if (this.s != null) {
            ((ExpandableListView) this.s).setDivider(drawable);
        }
    }

    public void setChildDivider(Drawable drawable) {
        ((ExpandableListView) this.s).setChildDivider(drawable);
    }

    public void addHeaderView(View view) {
        if (view != null) {
            if (this.f690a != null) {
                ((ExpandableListView) this.s).removeHeaderView(this.f690a);
            }
            this.f690a = view;
            ((ExpandableListView) this.s).addHeaderView(view);
            this.f690a.setVisibility(0);
        }
    }

    public void removeHeaderView() {
        if (this.f690a != null) {
            ((ExpandableListView) this.s).removeHeaderView(this.f690a);
            this.f690a = null;
        }
    }

    public void addFooterView(TXLoadingLayoutBase tXLoadingLayoutBase) {
        if (tXLoadingLayoutBase != null) {
            if (this.b != null) {
                ((ExpandableListView) this.s).removeFooterView(this.b);
            }
            this.b = tXLoadingLayoutBase;
            ((ExpandableListView) this.s).addFooterView(this.b);
            this.b.setVisibility(0);
            a();
        }
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        ((ExpandableListView) this.s).setOnItemClickListener(onItemClickListener);
    }

    public void setOnChildClickListener(ExpandableListView.OnChildClickListener onChildClickListener) {
        ((ExpandableListView) this.s).setOnChildClickListener(onChildClickListener);
    }

    public void setOnScrollListener(AbsListView.OnScrollListener onScrollListener) {
        this.d = onScrollListener;
    }

    public void setOnItemSelectedListener(AdapterView.OnItemSelectedListener onItemSelectedListener) {
        ((ExpandableListView) this.s).setOnItemSelectedListener(onItemSelectedListener);
    }

    public void setRefreshListViewListener(ITXRefreshListViewListener iTXRefreshListViewListener) {
        this.c = iTXRefreshListViewListener;
    }

    /* access modifiers changed from: protected */
    public void a() {
        a(true);
    }

    /* access modifiers changed from: protected */
    public void a(boolean z) {
        if (this.b != null) {
            switch (d.f706a[this.e.ordinal()]) {
                case 1:
                    if (z) {
                        this.b.loadSuc();
                        return;
                    } else {
                        this.b.loadFail();
                        return;
                    }
                case 2:
                    updateFootViewText();
                    return;
                case 3:
                    this.b.refreshing();
                    return;
                default:
                    return;
            }
        }
    }

    public void updateFootViewText() {
        int i = 0;
        if (getRawAdapter() != null) {
            int groupCount = getRawAdapter().getGroupCount();
            int i2 = 0;
            while (i2 < groupCount) {
                int childrenCount = getRawAdapter().getChildrenCount(i2) + i;
                i2++;
                i = childrenCount;
            }
        }
        if (this.b != null && i > 0) {
            this.b.loadFinish(bd.b(getContext(), i));
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ExpandableListView b(Context context) {
        ExpandableListView expandableListView = new ExpandableListView(context);
        expandableListView.setDivider(null);
        expandableListView.setGroupIndicator(null);
        expandableListView.setChildDivider(null);
        if (!(this.o == TXScrollViewBase.ScrollMode.NONE || this.o == TXScrollViewBase.ScrollMode.NOSCROLL)) {
            this.b = new RefreshListLoading(context, TXScrollViewBase.ScrollDirection.SCROLL_DIRECTION_VERTICAL, TXScrollViewBase.ScrollMode.PULL_FROM_END);
            this.b.setVisibility(0);
            expandableListView.addFooterView(this.b);
        }
        expandableListView.setOnScrollListener(this);
        return expandableListView;
    }

    /* access modifiers changed from: protected */
    public boolean e() {
        View childAt;
        ListAdapter adapter = ((ExpandableListView) this.s).getAdapter();
        if (adapter == null || adapter.isEmpty()) {
            return true;
        }
        if (((ExpandableListView) this.s).getFirstVisiblePosition() > 0 || (childAt = ((ExpandableListView) this.s).getChildAt(0)) == null) {
            return false;
        }
        return childAt.getTop() >= ((ExpandableListView) this.s).getTop();
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        View childAt;
        ListAdapter adapter = ((ExpandableListView) this.s).getAdapter();
        if (adapter == null || adapter.isEmpty()) {
            return true;
        }
        int count = ((ExpandableListView) this.s).getCount() - 1;
        int lastVisiblePosition = ((ExpandableListView) this.s).getLastVisiblePosition();
        if (lastVisiblePosition < count || (childAt = ((ExpandableListView) this.s).getChildAt(lastVisiblePosition - ((ExpandableListView) this.s).getFirstVisiblePosition())) == null) {
            return false;
        }
        return childAt.getBottom() <= ((ExpandableListView) this.s).getBottom();
    }

    /* access modifiers changed from: protected */
    public boolean b() {
        if (((ExpandableListView) this.s).getFirstVisiblePosition() > 0) {
            return true;
        }
        if (((ExpandableListView) this.s).getLastVisiblePosition() < ((ExpandableListView) this.s).getCount() - 1) {
            return true;
        }
        return false;
    }

    public void onScrollStateChanged(AbsListView absListView, int i) {
        if (this.d != null) {
            this.d.onScrollStateChanged(absListView, i);
        }
        if (i == 0 && this.g) {
            c();
        }
    }

    private void c() {
        if (this.e == TXRefreshScrollViewBase.RefreshState.RESET) {
            if (this.c != null) {
                this.c.onTXRefreshListViewRefresh(TXScrollViewBase.ScrollState.ScrollState_FromEnd);
            }
            this.e = TXRefreshScrollViewBase.RefreshState.REFRESHING;
            a();
        }
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        if (this.d != null) {
            this.d.onScroll(absListView, i, i2, i3);
        }
        if (this.s != null) {
            this.g = f();
            if (this.g && !b()) {
                c();
            }
        }
    }

    public void setGroupIndicator(Drawable drawable) {
        ((ExpandableListView) this.s).setGroupIndicator(drawable);
    }

    public void setSelector(int i) {
        ((ExpandableListView) this.s).setSelector(i);
    }

    public void setOnGroupClickListener(ExpandableListView.OnGroupClickListener onGroupClickListener) {
        ((ExpandableListView) this.s).setOnGroupClickListener(onGroupClickListener);
    }

    public void expandGroup(int i) {
        ((ExpandableListView) this.s).expandGroup(i);
    }

    public long getExpandableListPosition(int i) {
        return ((ExpandableListView) this.s).getExpandableListPosition(i);
    }

    public int getFirstVisiblePosition() {
        return ((ExpandableListView) this.s).getFirstVisiblePosition();
    }

    public boolean isGroupExpanded(int i) {
        return ((ExpandableListView) this.s).isGroupExpanded(i);
    }

    public int pointToPosition(int i, int i2) {
        return ((ExpandableListView) this.s).pointToPosition(i, i2);
    }

    public View getExpandChildAt(int i) {
        return ((ExpandableListView) this.s).getChildAt(i);
    }

    public TXRefreshScrollViewBase.RefreshState getMoreRefreshState() {
        return this.e;
    }

    public void setSelection(int i) {
        ((ExpandableListView) this.s).setSelection(i);
    }
}
