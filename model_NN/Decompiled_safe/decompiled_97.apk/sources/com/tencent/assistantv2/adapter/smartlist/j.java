package com.tencent.assistantv2.adapter.smartlist;

import android.content.Context;
import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamExClickListener;
import com.tencent.assistantv2.st.page.STInfoBuilder;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.cloud.model.SimpleEbookModel;
import com.tencent.pangu.link.b;

/* compiled from: ProGuard */
public class j extends OnTMAParamExClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ g f1913a;
    private Context b;
    private int c;
    private SimpleEbookModel d;
    private aa e;

    public j(g gVar, Context context, int i, SimpleEbookModel simpleEbookModel, aa aaVar) {
        this.f1913a = gVar;
        this.b = context;
        this.c = i;
        this.d = simpleEbookModel;
        this.e = aaVar;
    }

    public void onTMAClick(View view) {
        if (this.d != null) {
            b.a(this.b, this.d.l);
        }
    }

    public STInfoV2 getStInfo(View view) {
        if (this.e == null || this.e.e() == null) {
            return STInfoBuilder.buildSTInfo(this.b, 200);
        }
        STInfoV2 e2 = this.e.e();
        e2.actionId = 200;
        return e2;
    }
}
