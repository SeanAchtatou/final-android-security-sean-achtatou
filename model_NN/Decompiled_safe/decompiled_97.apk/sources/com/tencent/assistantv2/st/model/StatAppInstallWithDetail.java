package com.tencent.assistantv2.st.model;

import com.tencent.assistant.protocol.jce.StatAppInstall;
import com.tencent.connect.common.Constants;
import java.io.Serializable;
import java.util.List;

/* compiled from: ProGuard */
public class StatAppInstallWithDetail implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public String f2057a = Constants.STR_EMPTY;
    public int b = 0;
    public StatAppInstall c;
    public List<AppInstallDetail> d;
}
