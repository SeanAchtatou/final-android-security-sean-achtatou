package com.tencent.assistantv2.component;

import com.tencent.assistant.AppConst;

/* compiled from: ProGuard */
class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1999a;
    final /* synthetic */ AppConst.AppState b;
    final /* synthetic */ DownloadButton c;

    g(DownloadButton downloadButton, String str, AppConst.AppState appState) {
        this.c = downloadButton;
        this.f1999a = str;
        this.b = appState;
    }

    public void run() {
        String a2 = this.c.d();
        if (a2 != null && a2.equals(this.f1999a)) {
            this.c.b(this.b);
        }
    }
}
