package com.tencent.assistantv2.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;

/* compiled from: ProGuard */
public class TabView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    boolean f1943a = false;
    private TextView b;
    private Button c;

    public TabView(Context context) {
        super(context);
        a(context, this.f1943a);
    }

    public TabView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context, this.f1943a);
    }

    public TabView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(context, this.f1943a);
    }

    private void a(Context context, boolean z) {
        setBackgroundResource(R.drawable.v2_button_background_selector);
        this.b = new TextView(getContext());
        this.b.setId(R.id.title);
        this.b.setGravity(17);
        this.b.setSingleLine(true);
        this.b.setTextColor(getResources().getColorStateList(R.drawable.v2_tab_item_txt_color_selector));
        this.b.setTextSize(2, 16.0f);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -1);
        layoutParams.addRule(13);
        addView(this.b, layoutParams);
        if (z) {
            View view = new View(getContext());
            view.setId(R.id.divider);
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, 1);
            layoutParams2.addRule(12);
            view.setBackgroundColor(getContext().getResources().getColor(R.color.appadmin_list_item_divide_line));
            addView(view, layoutParams2);
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        return true;
    }

    public void a(int i) {
        if (this.c != null) {
            if (i > 0) {
                this.c.setVisibility(0);
                if (i > 99) {
                    this.c.setText(String.valueOf("99+"));
                } else {
                    this.c.setText(String.valueOf(i));
                }
            } else {
                this.c.setVisibility(8);
            }
        }
    }
}
