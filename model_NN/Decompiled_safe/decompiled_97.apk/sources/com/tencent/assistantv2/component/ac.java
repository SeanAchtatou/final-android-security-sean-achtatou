package com.tencent.assistantv2.component;

import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.assistant.component.txscrollview.TXImageView;

/* compiled from: ProGuard */
class ac {

    /* renamed from: a  reason: collision with root package name */
    public RelativeLayout f1950a;
    public TextView b;
    public TXImageView c;
    public TextView d;

    public ac(RelativeLayout relativeLayout, TextView textView, TXImageView tXImageView, TextView textView2) {
        this.f1950a = relativeLayout;
        this.b = textView;
        this.c = tXImageView;
        this.d = textView2;
    }
}
