package com.tencent.assistantv2.component;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.manager.t;

/* compiled from: ProGuard */
public class MovingProgressBar extends FrameLayout {

    /* renamed from: a  reason: collision with root package name */
    View f1937a;
    FrameLayout.LayoutParams b;
    Drawable c;
    ap d;
    int e = 1500;
    int f = 0;
    int g = -1;
    /* access modifiers changed from: private */
    public Context h;

    public MovingProgressBar(Context context) {
        super(context);
        a(context);
    }

    public MovingProgressBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    private void a(Context context) {
        this.h = context;
        this.c = getBackground();
    }

    public void a(int i) {
        this.f = i;
    }

    @Deprecated
    public void b(int i) {
        this.g = i;
    }

    public void a() {
        try {
            setBackgroundResource(R.drawable.download_centrer_down_ing);
        } catch (Throwable th) {
            t.a().b();
        }
        if (this.d == null) {
            this.f1937a = new View(this.h);
            try {
                this.f1937a.setBackgroundResource(R.drawable.download_centrer_down_doing);
            } catch (Throwable th2) {
                t.a().b();
            }
            this.b = new FrameLayout.LayoutParams(this.f, -1);
            addView(this.f1937a, this.b);
            this.d = new ap(this);
            this.d.f1963a = true;
            postDelayed(this.d, 5);
        } else if (!this.d.f1963a) {
            this.f1937a.setVisibility(0);
            this.d.f1963a = true;
            postDelayed(this.d, 5);
        }
    }

    public void b() {
        if (this.c != null && (this.d == null || this.d.f1963a)) {
            setBackgroundDrawable(this.c);
        }
        if (this.d != null) {
            this.d.f1963a = false;
            this.f1937a.setVisibility(8);
        }
    }
}
