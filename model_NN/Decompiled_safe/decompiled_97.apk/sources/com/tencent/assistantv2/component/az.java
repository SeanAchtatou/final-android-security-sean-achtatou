package com.tencent.assistantv2.component;

import android.view.View;
import com.tencent.assistant.component.listener.OnTMAParamClickListener;
import com.tencent.assistant.st.STConst;
import com.tencent.assistantv2.st.page.STInfoV2;
import com.tencent.assistantv2.st.page.a;

/* compiled from: ProGuard */
class az extends OnTMAParamClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SecondNavigationTitleViewV5 f1971a;

    az(SecondNavigationTitleViewV5 secondNavigationTitleViewV5) {
        this.f1971a = secondNavigationTitleViewV5;
    }

    public void onTMAClick(View view) {
        if (this.f1971a.k != null) {
            this.f1971a.k.finish();
        }
    }

    public STInfoV2 getStInfo() {
        return this.f1971a.e(a.a(STConst.ST_STATUS_DEFAULT, "001"));
    }
}
