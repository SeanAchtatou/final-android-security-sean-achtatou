package com.tencent.assistantv2.component.fps;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.adapter.a.c;
import com.tencent.assistant.component.HorizonMultiImageView;
import com.tencent.assistant.component.invalidater.IViewInvalidater;
import com.tencent.assistant.component.txscrollview.TXAppIconView;
import com.tencent.assistantv2.adapter.smartlist.v;
import com.tencent.assistantv2.component.DownloadButton;
import com.tencent.assistantv2.component.ListItemInfoView;
import com.tencent.assistantv2.component.ListItemRelateNewsView;

/* compiled from: ProGuard */
public class FPSNormalItemNoReasonView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    public v f1990a = new v();
    private Context b;
    private IViewInvalidater c;

    public FPSNormalItemNoReasonView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.b = context;
        a();
    }

    public FPSNormalItemNoReasonView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.b = context;
        a();
    }

    public FPSNormalItemNoReasonView(Context context) {
        super(context);
        this.b = context;
        a();
    }

    public FPSNormalItemNoReasonView(Context context, IViewInvalidater iViewInvalidater) {
        super(context);
        this.b = context;
        this.c = iViewInvalidater;
        a();
    }

    private void a() {
        View inflate = LayoutInflater.from(this.b).inflate((int) R.layout.app_universal_item_no_reason, this);
        this.f1990a.f1920a = (TextView) inflate.findViewById(R.id.download_rate_desc);
        this.f1990a.b = (TXAppIconView) inflate.findViewById(R.id.app_icon_img);
        this.f1990a.b.setInvalidater(this.c);
        this.f1990a.c = (TextView) inflate.findViewById(R.id.title);
        this.f1990a.d = (DownloadButton) inflate.findViewById(R.id.state_app_btn);
        this.f1990a.e = (ListItemInfoView) inflate.findViewById(R.id.download_info);
        this.f1990a.h = (HorizonMultiImageView) inflate.findViewById(R.id.snap_shot_pics);
        this.f1990a.i = inflate.findViewById(R.id.empty_padding_bottom);
        this.f1990a.m = (ListItemRelateNewsView) inflate.findViewById(R.id.relate_news);
        this.f1990a.j = new c();
        this.f1990a.j.f579a = (ListView) inflate.findViewById(R.id.one_more_list);
        this.f1990a.j.b = (RelativeLayout) inflate.findViewById(R.id.one_more_list_parent);
        this.f1990a.j.f579a.setDivider(null);
        this.f1990a.j.e = (ImageView) inflate.findViewById(R.id.one_more_app_line_top_long);
        this.f1990a.j.j = (RelativeLayout) inflate.findViewById(R.id.app_one_more_loading_parent);
        this.f1990a.j.i = (TextView) inflate.findViewById(R.id.app_one_more_loading);
        this.f1990a.j.f = (ProgressBar) inflate.findViewById(R.id.app_one_more_loading_gif);
        this.f1990a.j.g = (ImageView) inflate.findViewById(R.id.one_more_app_error_img);
        this.f1990a.j.d = (ImageView) inflate.findViewById(R.id.one_more_app_arrow);
        this.f1990a.j.h = (TextView) inflate.findViewById(R.id.app_one_more_desc);
        this.f1990a.l = (TextView) inflate.findViewById(R.id.sort_text);
        setBackgroundResource(R.drawable.bg_card_selector_padding);
    }
}
