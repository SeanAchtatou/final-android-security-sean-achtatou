package com.tencent.assistantv2.component;

import android.content.Context;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewSwitcher;
import com.qq.AppService.AstApp;
import com.tencent.android.qqdownloader.R;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.assistant.event.listener.UIEventListener;
import com.tencent.assistant.m;
import com.tencent.assistant.module.k;
import com.tencent.assistant.st.STConst;
import com.tencent.connect.common.Constants;
import com.tencent.pangu.download.DownloadInfo;
import com.tencent.pangu.download.SimpleDownloadInfo;
import com.tencent.pangu.manager.DownloadProxy;
import com.tencent.pangu.mediadownload.e;
import com.tencent.pangu.mediadownload.q;
import com.tencent.pangu.mediadownload.r;
import com.tencent.pangu.model.d;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ProGuard */
public class DownloadCenterButton extends RelativeLayout implements UIEventListener {

    /* renamed from: a  reason: collision with root package name */
    MovingProgressBar f1927a;
    String b = "DownloadCenterButton";
    int c = -1;
    /* access modifiers changed from: private */
    public View d;
    /* access modifiers changed from: private */
    public Context e;
    private TextView f;
    /* access modifiers changed from: private */
    public ViewSwitcher g;
    private int h;
    private int i;
    /* access modifiers changed from: private */
    public boolean j = false;
    /* access modifiers changed from: private */
    public String k = STConst.ST_DEFAULT_SLOT;
    private int l;
    private ImageView m = null;

    public DownloadCenterButton(Context context) {
        super(context);
        a(context);
    }

    public DownloadCenterButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    public void a(String str) {
        this.k = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.tencent.assistantv2.component.DownloadCenterButton, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void a(Context context) {
        this.e = context;
        ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.v2_download_center_button, (ViewGroup) this, true);
        this.d = findViewById(R.id.my_red_dot);
        this.f = (TextView) findViewById(R.id.my_downloading_count);
        this.g = (ViewSwitcher) findViewById(R.id.mybtn_download_btn_up_area);
        this.f1927a = (MovingProgressBar) findViewById(R.id.mybtn_download_btn_down_area);
        this.f1927a.a(getResources().getDimensionPixelSize(R.dimen.app_detail_pic_gap));
        this.m = (ImageView) findViewById(R.id.mybtn_download_btn_up);
        setOnClickListener(new m(this));
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        return true;
    }

    public void a() {
        a(false);
        AstApp.i().k().addUIEventListener(1001, this);
        AstApp.i().k().addUIEventListener(1002, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING_START, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_PAUSE, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC, this);
        AstApp.i().k().addUIEventListener(1007, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_QUEUING, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        AstApp.i().k().addUIEventListener(1010, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_UPDATE_START, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_START, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOADING, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_PAUSE, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_QUEUING, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_FAIL, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_ADD, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_DELETE, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_SUCC, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_HOST_DOWNLOAD_START, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_HOST_INSTALLED, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_START, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_FILENAME, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOADING, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_PAUSE, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_QUEUING, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_FAIL, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_ADD, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_DELETE, this);
        AstApp.i().k().addUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_SUCC, this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.download.SimpleDownloadInfo$DownloadType, boolean):java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>
     arg types: [com.tencent.pangu.download.SimpleDownloadInfo$DownloadType, int]
     candidates:
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.manager.DownloadProxy, android.app.Dialog):android.app.Dialog
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.manager.DownloadProxy, com.tencent.assistant.net.APN):com.tencent.assistant.net.APN
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.manager.DownloadProxy, com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager):com.tencent.pangu.utils.installuninstall.InstallUninstallDialogManager
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.assistant.localres.model.LocalApkInfo, java.util.ArrayList<com.tencent.pangu.download.DownloadInfo>):java.lang.String
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.download.DownloadInfo, com.tencent.pangu.download.SimpleDownloadInfo$DownloadState):void
      com.tencent.pangu.manager.DownloadProxy.a(java.lang.String, int):com.tencent.pangu.download.DownloadInfo
      com.tencent.pangu.manager.DownloadProxy.a(java.lang.String, boolean):void
      com.tencent.pangu.manager.DownloadProxy.a(com.tencent.pangu.download.SimpleDownloadInfo$DownloadType, boolean):java.util.ArrayList<com.tencent.pangu.download.DownloadInfo> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.assistant.m.a(java.lang.String, byte):byte
      com.tencent.assistant.m.a(java.lang.String, int):int
      com.tencent.assistant.m.a(java.lang.String, long):long
      com.tencent.assistant.m.a(java.lang.String, java.lang.Object):java.lang.String
      com.tencent.assistant.m.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.assistant.m.a(byte, byte):void
      com.tencent.assistant.m.a(byte, int):void
      com.tencent.assistant.m.a(byte, long):void
      com.tencent.assistant.m.a(byte, java.lang.String):void
      com.tencent.assistant.m.a(int, byte[]):void
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, int):void
      com.tencent.assistant.m.a(java.lang.String, byte[]):void
      com.tencent.assistant.m.a(long, int):boolean
      com.tencent.assistant.m.a(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.a(java.lang.Long, int):boolean
      com.tencent.assistant.m.a(java.lang.String, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState
     arg types: [com.tencent.pangu.download.DownloadInfo, int, int]
     candidates:
      com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, com.tencent.assistant.plugin.PluginDownloadInfo, com.tencent.assistant.plugin.PluginInfo):com.tencent.assistant.AppConst$AppState
      com.tencent.assistant.module.k.a(java.util.ArrayList<com.tencent.assistant.protocol.jce.CardItem>, com.tencent.assistant.module.m, int):java.util.ArrayList<com.tencent.assistant.model.SimpleAppModel>
      com.tencent.assistant.module.k.a(com.tencent.pangu.download.DownloadInfo, boolean, boolean):com.tencent.assistant.AppConst$AppState */
    private void a(boolean z) {
        ArrayList<DownloadInfo> a2 = DownloadProxy.a().a(SimpleDownloadInfo.DownloadType.APK, true);
        List<q> a3 = r.c().a();
        List<d> a4 = e.c().a();
        boolean a5 = m.a().a("key_download_center_red_dot", false);
        this.h = 0;
        this.i = 0;
        this.l = 0;
        for (DownloadInfo a6 : a2) {
            switch (r.f2008a[k.a(a6, true, true).ordinal()]) {
                case 1:
                case 2:
                    this.i++;
                    this.l++;
                    break;
                case 3:
                case 4:
                case 5:
                    this.l++;
                    break;
                case 6:
                    this.h++;
                    break;
            }
        }
        for (q qVar : a3) {
            switch (r.b[qVar.s.ordinal()]) {
                case 1:
                case 2:
                    this.i++;
                    this.l++;
                    break;
                case 3:
                case 4:
                    this.l++;
                    break;
            }
        }
        for (d dVar : a4) {
            switch (r.b[dVar.s.ordinal()]) {
                case 1:
                case 2:
                    this.i++;
                    this.l++;
                    break;
                case 3:
                case 4:
                    this.l++;
                    break;
            }
        }
        if (this.h <= 0 || !a5) {
            this.d.setVisibility(8);
        } else {
            this.d.setVisibility(0);
        }
        if (z && this.l > 0) {
            if (this.l < 100) {
                this.f.setText(Constants.STR_EMPTY + this.l);
            } else {
                this.f.setText("99");
            }
            if (!this.j) {
                this.j = true;
                this.g.showNext();
                this.g.postDelayed(new n(this), 1000);
                this.g.postDelayed(new o(this), 2000);
                this.g.postDelayed(new p(this), 3000);
                this.g.postDelayed(new q(this), 3600);
            }
        }
        if (this.i <= 0) {
            this.f1927a.b();
        } else if (this.c != 1006) {
            this.f1927a.a();
        }
    }

    public void b() {
        this.f1927a.b();
        AstApp.i().k().removeUIEventListener(1001, this);
        AstApp.i().k().removeUIEventListener(1002, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING_START, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_PAUSE, this);
        AstApp.i().k().removeUIEventListener(1007, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_QUEUING, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE, this);
        AstApp.i().k().removeUIEventListener(1010, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_UPDATE_START, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_UPDATE_DELETE, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_START, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOADING, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_PAUSE, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_QUEUING, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_FAIL, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_ADD, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_DELETE, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_SUCC, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_HOST_DOWNLOAD_START, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_VIDEO_HOST_INSTALLED, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_START, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_FILENAME, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOADING, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_PAUSE, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_QUEUING, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_FAIL, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_ADD, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_DELETE, this);
        AstApp.i().k().removeUIEventListener(EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_SUCC, this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean
     arg types: [java.lang.String, boolean]
     candidates:
      com.tencent.assistant.m.b(byte, int):void
      com.tencent.assistant.m.b(byte, java.lang.String):void
      com.tencent.assistant.m.b(int, byte[]):void
      com.tencent.assistant.m.b(com.tencent.assistant.AppConst$WISE_DOWNLOAD_SWITCH_TYPE, boolean):boolean
      com.tencent.assistant.m.b(java.lang.Long, int):boolean
      com.tencent.assistant.m.b(java.lang.String, java.lang.Object):boolean */
    public void handleUIEvent(Message message) {
        this.c = message.what;
        switch (message.what) {
            case 1001:
                Log.d(this.b, "---------DOWNLOAD");
                return;
            case 1002:
                a(false);
                Log.d(this.b, "---------DOWNLOAD_start");
                return;
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING:
            default:
                return;
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DOWNLOADING_START:
                Log.d(this.b, "---------DOWNLOAD_ing_start--------");
                return;
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_PAUSE:
                Log.d(this.b, "---------DOWNLOAD_pause");
                a(false);
                return;
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_SUCC:
                DownloadInfo c2 = DownloadProxy.a().c(message.obj.toString());
                if (c2 != null && c2.uiType == SimpleDownloadInfo.UIType.NORMAL) {
                    m.a().b("key_download_center_red_dot", (Object) true);
                    a(false);
                }
                Log.d(this.b, "---------DOWNLOAD_succeed:" + message.obj);
                return;
            case 1007:
                a(false);
                Log.d(this.b, "---------DOWNLOAD_fail");
                return;
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_QUEUING:
                Log.d(this.b, "---------DOWNLOAD_queing--------");
                return;
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_DELETE:
                Log.d(this.b, "---------DOWNLOAD_delete");
                a(false);
                return;
            case 1010:
                Log.d(this.b, "---------DOWNLOAD_complete");
                return;
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_ADD:
                Log.d(this.b, "---------DOWNLOAD_add------");
                a(true);
                return;
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_UPDATE_START:
                Log.d(this.b, "---------DOWNLOAD_update_start11--------");
                return;
            case EventDispatcherEnum.UI_EVENT_APP_DOWNLOAD_UPDATE_DELETE:
                Log.d(this.b, "---------DOWNLOAD_update_delete");
                a(false);
                return;
            case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_START:
            case EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_START:
                Log.d(this.b, "---------UI_EVENT_VIDEO_DOWNLOAD_START");
                a(false);
                return;
            case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_PAUSE:
            case EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_PAUSE:
                Log.d(this.b, "---------UI_EVENT_VIDEO_DOWNLOAD_PAUSE");
                a(false);
                return;
            case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_QUEUING:
            case EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_QUEUING:
                Log.d(this.b, "---------UI_EVENT_VIDEO_DOWNLOAD_QUEUING");
                a(false);
                return;
            case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_FAIL:
            case EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_FAIL:
                Log.d(this.b, "---------UI_EVENT_VIDEO_DOWNLOAD_FAIL");
                a(false);
                return;
            case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_ADD:
            case EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_ADD:
                Log.d(this.b, "---------UI_EVENT_VIDEO_DOWNLOAD_ADD");
                a(true);
                return;
            case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_DELETE:
            case EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_DELETE:
                Log.d(this.b, "---------UI_EVENT_VIDEO_DOWNLOAD_DELETE");
                a(false);
                return;
            case EventDispatcherEnum.UI_EVENT_VIDEO_DOWNLOAD_SUCC:
            case EventDispatcherEnum.UI_EVENT_FILE_DOWNLOAD_SUCC:
                Log.d(this.b, "---------UI_EVENT_VIDEO_DOWNLOAD_SUCC");
                a(false);
                return;
            case EventDispatcherEnum.UI_EVENT_VIDEO_HOST_DOWNLOAD_START:
                Log.d(this.b, "---------UI_EVENT_VIDEO_HOST_DOWNLOAD_START");
                a(true);
                return;
            case EventDispatcherEnum.UI_EVENT_VIDEO_HOST_INSTALLED:
                Log.d(this.b, "---------UI_EVENT_VIDEO_HOST_INSTALLED");
                a(false);
                return;
        }
    }

    public void c() {
        ((ImageView) this.g.findViewById(R.id.mybtn_download_btn_up)).setImageResource(R.drawable.guanjia_style_download_centrer_up);
        ((TextView) this.g.findViewById(R.id.my_downloading_count)).setTextColor(-1);
        this.f1927a.setBackgroundResource(R.drawable.guanjia_style_download_centrer_down);
        this.f1927a.c = getResources().getDrawable(R.drawable.guanjia_style_download_centrer_down);
    }

    public void d() {
        ((ImageView) this.g.findViewById(R.id.mybtn_download_btn_up)).setImageResource(R.drawable.download_centrer_up);
        ((TextView) this.g.findViewById(R.id.my_downloading_count)).setTextColor(getResources().getColor(R.color.appdetail_tag_text_color_blue_n));
        this.f1927a.setBackgroundResource(R.drawable.download_centrer_down);
        this.f1927a.c = getResources().getDrawable(R.drawable.download_centrer_down);
    }

    public void a(int i2) {
        if (this.m != null) {
            this.m.setImageResource(i2);
        }
    }

    public void b(int i2) {
        if (this.f1927a != null) {
            this.f1927a.setBackgroundResource(i2);
            this.f1927a.c = getResources().getDrawable(i2);
        }
    }
}
