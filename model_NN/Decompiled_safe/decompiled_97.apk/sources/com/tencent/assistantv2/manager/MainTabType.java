package com.tencent.assistantv2.manager;

/* compiled from: ProGuard */
public enum MainTabType {
    DISCOVER,
    HOT,
    APP,
    GAME,
    TREASURY,
    WEBVIEW,
    EBOOK,
    VIDEO,
    THEME,
    OTHERWISE
}
