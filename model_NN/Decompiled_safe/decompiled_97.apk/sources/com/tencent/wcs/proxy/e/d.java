package com.tencent.wcs.proxy.e;

/* compiled from: ProGuard */
public class d {
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0038 A[SYNTHETIC, Splitter:B:24:0x0038] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0044 A[SYNTHETIC, Splitter:B:30:0x0044] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:12:0x0023=Splitter:B:12:0x0023, B:21:0x0033=Splitter:B:21:0x0033} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] a(byte[] r4) {
        /*
            r0 = 0
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream
            r3.<init>()
            java.util.zip.GZIPOutputStream r2 = new java.util.zip.GZIPOutputStream     // Catch:{ IOException -> 0x0021, Exception -> 0x0031, all -> 0x003f }
            r2.<init>(r3)     // Catch:{ IOException -> 0x0021, Exception -> 0x0031, all -> 0x003f }
            r2.write(r4)     // Catch:{ IOException -> 0x0051, Exception -> 0x004f }
            r2.flush()     // Catch:{ IOException -> 0x0051, Exception -> 0x004f }
            r2.finish()     // Catch:{ IOException -> 0x0051, Exception -> 0x004f }
            byte[] r0 = r3.toByteArray()     // Catch:{ IOException -> 0x0051, Exception -> 0x004f }
            if (r2 == 0) goto L_0x001d
            r2.close()     // Catch:{ IOException -> 0x002f }
        L_0x001d:
            r3.close()     // Catch:{ IOException -> 0x002f }
        L_0x0020:
            return r0
        L_0x0021:
            r1 = move-exception
            r2 = r0
        L_0x0023:
            r1.printStackTrace()     // Catch:{ all -> 0x004d }
            if (r2 == 0) goto L_0x002b
            r2.close()     // Catch:{ IOException -> 0x002f }
        L_0x002b:
            r3.close()     // Catch:{ IOException -> 0x002f }
            goto L_0x0020
        L_0x002f:
            r1 = move-exception
            goto L_0x0020
        L_0x0031:
            r1 = move-exception
            r2 = r0
        L_0x0033:
            r1.printStackTrace()     // Catch:{ all -> 0x004d }
            if (r2 == 0) goto L_0x003b
            r2.close()     // Catch:{ IOException -> 0x002f }
        L_0x003b:
            r3.close()     // Catch:{ IOException -> 0x002f }
            goto L_0x0020
        L_0x003f:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x0042:
            if (r2 == 0) goto L_0x0047
            r2.close()     // Catch:{ IOException -> 0x004b }
        L_0x0047:
            r3.close()     // Catch:{ IOException -> 0x004b }
        L_0x004a:
            throw r0
        L_0x004b:
            r1 = move-exception
            goto L_0x004a
        L_0x004d:
            r0 = move-exception
            goto L_0x0042
        L_0x004f:
            r1 = move-exception
            goto L_0x0033
        L_0x0051:
            r1 = move-exception
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.wcs.proxy.e.d.a(byte[]):byte[]");
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0061 A[SYNTHETIC, Splitter:B:25:0x0061] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] b(byte[] r7) {
        /*
            r1 = 0
            r2 = 0
            java.io.ByteArrayInputStream r4 = new java.io.ByteArrayInputStream
            r4.<init>(r7)
            java.io.ByteArrayOutputStream r5 = new java.io.ByteArrayOutputStream
            r5.<init>()
            com.tencent.wcs.proxy.b.e r0 = com.tencent.wcs.proxy.b.e.b()
            java.lang.Integer[] r2 = new java.lang.Integer[r2]
            java.lang.Object r0 = r0.c(r2)
            byte[] r0 = (byte[]) r0
            java.util.zip.GZIPInputStream r3 = new java.util.zip.GZIPInputStream     // Catch:{ IOException -> 0x007a, all -> 0x005c }
            r3.<init>(r4)     // Catch:{ IOException -> 0x007a, all -> 0x005c }
        L_0x001d:
            int r2 = r3.read(r0)     // Catch:{ IOException -> 0x0029 }
            r6 = -1
            if (r2 == r6) goto L_0x0041
            r6 = 0
            r5.write(r0, r6, r2)     // Catch:{ IOException -> 0x0029 }
            goto L_0x001d
        L_0x0029:
            r2 = move-exception
        L_0x002a:
            r2.printStackTrace()     // Catch:{ all -> 0x0078 }
            if (r3 == 0) goto L_0x0032
            r3.close()     // Catch:{ IOException -> 0x0074 }
        L_0x0032:
            r4.close()     // Catch:{ IOException -> 0x0074 }
            r5.close()     // Catch:{ IOException -> 0x0074 }
        L_0x0038:
            com.tencent.wcs.proxy.b.e r2 = com.tencent.wcs.proxy.b.e.b()
            r2.b(r0)
            r0 = r1
        L_0x0040:
            return r0
        L_0x0041:
            r5.flush()     // Catch:{ IOException -> 0x0029 }
            byte[] r1 = r5.toByteArray()     // Catch:{ IOException -> 0x0029 }
            if (r3 == 0) goto L_0x004d
            r3.close()     // Catch:{ IOException -> 0x0072 }
        L_0x004d:
            r4.close()     // Catch:{ IOException -> 0x0072 }
            r5.close()     // Catch:{ IOException -> 0x0072 }
        L_0x0053:
            com.tencent.wcs.proxy.b.e r2 = com.tencent.wcs.proxy.b.e.b()
            r2.b(r0)
            r0 = r1
            goto L_0x0040
        L_0x005c:
            r2 = move-exception
            r3 = r1
            r1 = r2
        L_0x005f:
            if (r3 == 0) goto L_0x0064
            r3.close()     // Catch:{ IOException -> 0x0076 }
        L_0x0064:
            r4.close()     // Catch:{ IOException -> 0x0076 }
            r5.close()     // Catch:{ IOException -> 0x0076 }
        L_0x006a:
            com.tencent.wcs.proxy.b.e r2 = com.tencent.wcs.proxy.b.e.b()
            r2.b(r0)
            throw r1
        L_0x0072:
            r2 = move-exception
            goto L_0x0053
        L_0x0074:
            r2 = move-exception
            goto L_0x0038
        L_0x0076:
            r2 = move-exception
            goto L_0x006a
        L_0x0078:
            r1 = move-exception
            goto L_0x005f
        L_0x007a:
            r2 = move-exception
            r3 = r1
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.wcs.proxy.e.d.b(byte[]):byte[]");
    }
}
