package com.tencent.wcs.proxy;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.tencent.wcs.proxy.e.a;
import java.util.concurrent.TimeUnit;

/* compiled from: ProGuard */
class j extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f4139a;

    private j(c cVar) {
        this.f4139a = cVar;
    }

    /* synthetic */ j(c cVar, d dVar) {
        this(cVar);
    }

    public void onReceive(Context context, Intent intent) {
        if (!this.f4139a.k) {
            boolean unused = this.f4139a.k = true;
            a.a().a(new k(this), 3000, TimeUnit.MILLISECONDS);
        }
    }
}
