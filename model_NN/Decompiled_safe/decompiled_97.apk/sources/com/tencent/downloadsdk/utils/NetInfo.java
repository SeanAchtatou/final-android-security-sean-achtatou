package com.tencent.downloadsdk.utils;

import com.tencent.connect.common.Constants;

public class NetInfo {

    /* renamed from: a  reason: collision with root package name */
    public APN f2512a = APN.UN_DETECT;
    public String b = Constants.STR_EMPTY;
    public int c = -1;
    public boolean d = false;

    public enum APN {
        UN_DETECT,
        WIFI,
        CMWAP,
        CMNET,
        UNIWAP,
        UNINET,
        WAP3G,
        NET3G,
        CTWAP,
        CTNET,
        UNKNOWN,
        UNKNOW_WAP,
        NO_NETWORK
    }
}
