package com.tencent.downloadsdk;

import com.tencent.downloadsdk.storage.a.e;
import com.tencent.downloadsdk.utils.f;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ak {

    /* renamed from: a  reason: collision with root package name */
    private ArrayList<an> f2460a;
    private long b = 0;
    private String c;
    private ai d;
    private long e;
    private DownloadSettingInfo f;
    private Comparator<an> g = new al(this);
    private Comparator<an> h = new am(this);

    public ak(String str, DownloadSettingInfo downloadSettingInfo, long j, ai aiVar) {
        if (str == null || downloadSettingInfo == null || j <= 0) {
            throw new IllegalArgumentException("taskId: " + str + " downloadSetting: " + downloadSettingInfo + " fileLength: " + j);
        }
        this.c = str;
        this.d = aiVar;
        this.f = downloadSettingInfo;
        e eVar = new e();
        this.f2460a = eVar.a(str);
        if (this.f2460a == null) {
            this.f2460a = new ArrayList<>();
        }
        f.b("SegStragey", "task id:" + this.c + " load segFileTable size: " + this.f2460a.size());
        f.b("SegStragey", "task id:" + this.c + " load segFileTable:" + this.f2460a.toString());
        if (!this.f2460a.isEmpty()) {
            Collections.sort(this.f2460a, this.h);
            this.b = this.f2460a.get(0).c + 1;
            int i = 0;
            int i2 = 0;
            while (true) {
                int i3 = i;
                if (i3 >= this.f2460a.size()) {
                    break;
                }
                this.e = this.e + this.f2460a.get(i3).g;
                i2 = (int) (((long) i2) + this.f2460a.get(i3).e);
                i = i3 + 1;
            }
            if (j != ((long) i2)) {
                this.f2460a.clear();
                eVar.b(str);
                this.e = 0;
            }
        }
        if (!this.f2460a.isEmpty()) {
            f.d("SegStragey", "task id:" + this.c + "! mSegList.isEmpty()");
            int size = this.f2460a.size();
            while (true) {
                int i4 = size;
                if (i4 >= this.f.i) {
                    break;
                }
                Collections.sort(this.f2460a, this.g);
                an anVar = this.f2460a.get(0);
                long b2 = anVar.b() / 2;
                if (b2 < this.f.c) {
                    break;
                }
                anVar.e -= b2;
                ArrayList<an> arrayList = this.f2460a;
                String str2 = this.c;
                long j2 = this.b;
                this.b = 1 + j2;
                arrayList.add(new an(str2, j2, this.d.a(0), b2, anVar.e + anVar.f));
                size = i4 + 1;
            }
            f.d("SegStragey", "task id:" + this.c + " after reseg:" + this.f2460a.toString());
        } else if (downloadSettingInfo.i <= 1 || j <= 2 * downloadSettingInfo.c || downloadSettingInfo.c <= 0) {
            ArrayList<an> arrayList2 = this.f2460a;
            long j3 = this.b;
            this.b = 1 + j3;
            arrayList2.add(new an(str, j3, aiVar.a(0), j, 0));
        } else {
            long j4 = j / downloadSettingInfo.c;
            j4 = j4 <= 0 ? 1 : j4;
            long j5 = j4 >= ((long) downloadSettingInfo.i) ? (long) downloadSettingInfo.i : j4;
            long j6 = j / j5;
            int i5 = 0;
            while (true) {
                int i6 = i5;
                if (((long) i6) < j5) {
                    if (((long) i6) == j5 - 1) {
                        ArrayList<an> arrayList3 = this.f2460a;
                        long j7 = this.b;
                        this.b = 1 + j7;
                        arrayList3.add(new an(str, j7, aiVar.a(0), j - (((long) i6) * j6), ((long) i6) * j6));
                    } else {
                        ArrayList<an> arrayList4 = this.f2460a;
                        long j8 = this.b;
                        this.b = 1 + j8;
                        arrayList4.add(new an(str, j8, aiVar.a(0), j6, ((long) i6) * j6));
                    }
                    i5 = i6 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public long a() {
        return this.e;
    }

    public synchronized an b() {
        an anVar;
        f.b("SegStragey", "reseg()");
        if (!this.f.j) {
            anVar = null;
        } else if (this.f2460a.isEmpty()) {
            anVar = null;
        } else {
            int size = this.f2460a.size();
            Collections.sort(this.f2460a, this.g);
            an anVar2 = this.f2460a.get(0);
            synchronized (anVar2) {
                long b2 = anVar2.b() / 2;
                if (b2 < this.f.c) {
                    anVar = null;
                } else {
                    anVar2.e -= b2;
                    ArrayList<an> arrayList = this.f2460a;
                    String str = this.c;
                    long j = this.b;
                    this.b = 1 + j;
                    arrayList.add(new an(str, j, this.d.a(0), b2, anVar2.f + anVar2.e));
                    if (size == this.f2460a.size()) {
                        anVar = null;
                    } else {
                        Collections.sort(this.f2460a, this.g);
                        anVar = this.f2460a.get(0);
                    }
                }
            }
        }
        return anVar;
    }

    public synchronized an c() {
        an anVar;
        if (this.f2460a.isEmpty()) {
            anVar = null;
        } else {
            Collections.sort(this.f2460a, this.g);
            anVar = this.f2460a.get(0);
            if (anVar == null || anVar.h || anVar.a()) {
                anVar = null;
            }
        }
        return anVar;
    }
}
