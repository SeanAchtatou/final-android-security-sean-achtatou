package com.tencent.downloadsdk;

import java.util.concurrent.RejectedExecutionException;

class e implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f2472a;

    e(c cVar) {
        this.f2472a = cVar;
    }

    public void run() {
        an b;
        if (this.f2472a.j.get() < this.f2472a.f2470a && (b = this.f2472a.t.b()) != null) {
            b.d = this.f2472a.C.a(0);
            b.j = this.f2472a.n;
            b.h = true;
            b.c();
            try {
                k kVar = new k(this.f2472a.r, this.f2472a.s, b, this.f2472a.C, this.f2472a.p, this.f2472a.z, this.f2472a, this.f2472a.J);
                kVar.d = this.f2472a.u.submit(kVar);
                this.f2472a.B.put(Long.valueOf(b.c), kVar);
            } catch (RejectedExecutionException e) {
                e.printStackTrace();
            }
        }
    }
}
