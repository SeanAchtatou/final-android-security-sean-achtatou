package com.tencent.feedback.common;

import android.content.Context;
import com.tencent.feedback.b.b;
import com.tencent.feedback.b.c;
import com.tencent.feedback.b.h;
import com.tencent.feedback.b.i;
import com.tencent.feedback.proguard.an;
import com.tencent.feedback.proguard.ao;
import com.tencent.feedback.proguard.at;
import com.tencent.feedback.proguard.au;
import com.tencent.feedback.proguard.av;
import com.tencent.securemodule.service.Constants;

/* compiled from: ProGuard */
public class l implements an, av {

    /* renamed from: a  reason: collision with root package name */
    protected final Context f2549a;
    private int b;
    private int c;
    private int d;
    private boolean e;
    private boolean f;
    private i g;
    private h h;
    private int i = 0;

    public l(Context context, String str, int i2, int i3, int i4, i iVar, h hVar, b bVar) {
        Context applicationContext;
        if (!(context == null || (applicationContext = context.getApplicationContext()) == null)) {
            context = applicationContext;
        }
        this.f2549a = context;
        e a2 = e.a(this.f2549a);
        if (str != null && str.trim().length() > 0) {
            a2.a(str);
        }
        this.b = 3;
        this.c = Constants.PLATFROM_ANDROID_PAD;
        this.d = 302;
        this.g = iVar;
        this.h = hVar;
        if (iVar != null) {
            iVar.a(302, hVar);
            iVar.a(bVar);
        }
        ao a3 = ao.a(this.f2549a);
        a3.a((an) this);
        a3.a((av) this);
        a3.a(3, iVar);
    }

    public final void a(boolean z) {
        au e2;
        boolean z2 = true;
        ao a2 = ao.a(this.f2549a);
        if (a2 != null && (e2 = a2.b().e(this.b)) != null && e2.b() != z) {
            g.a("rqdp{  mid:}%d rqdp{  change user switch} %b", Integer.valueOf(this.b), Boolean.valueOf(z));
            e2.a(z);
            if (!e2.b() || !e2.c()) {
                z2 = false;
            }
            if (z2 != a()) {
                b(z2);
            }
        }
    }

    public final synchronized boolean a() {
        return this.e;
    }

    public synchronized void b(boolean z) {
        this.e = z;
    }

    public final synchronized boolean b() {
        return this.f;
    }

    private synchronized void c(boolean z) {
        this.f = true;
    }

    public final synchronized i c() {
        return this.g;
    }

    private synchronized h l() {
        return this.h;
    }

    public final void d() {
        g.b("rqdp{  com query start }%s", getClass().toString());
        a(k() + 1);
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x007b A[Catch:{ Throwable -> 0x00f0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x007e A[Catch:{ Throwable -> 0x00f0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00ce A[Catch:{ Throwable -> 0x00f0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00d0 A[Catch:{ Throwable -> 0x00f0 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void e() {
        /*
            r8 = this;
            r1 = 1
            r2 = 0
            java.lang.String r0 = "rqdp{  com query end }%s"
            java.lang.Object[] r3 = new java.lang.Object[r1]
            java.lang.Class r4 = r8.getClass()
            java.lang.String r4 = r4.toString()
            r3[r2] = r4
            com.tencent.feedback.common.g.b(r0, r3)
            boolean r0 = r8.b()
            if (r0 != 0) goto L_0x0023
            java.lang.String r0 = "rqdp{  step after query}"
            java.lang.Object[] r3 = new java.lang.Object[r2]
            com.tencent.feedback.common.g.b(r0, r3)
            r8.c(r1)
        L_0x0023:
            android.content.Context r0 = r8.f2549a     // Catch:{ Throwable -> 0x00f0 }
            com.tencent.feedback.proguard.ao r0 = com.tencent.feedback.proguard.ao.a(r0)     // Catch:{ Throwable -> 0x00f0 }
            com.tencent.feedback.proguard.at r0 = r0.b()     // Catch:{ Throwable -> 0x00f0 }
            int r3 = r8.b     // Catch:{ Throwable -> 0x00f0 }
            com.tencent.feedback.proguard.au r4 = r0.e(r3)     // Catch:{ Throwable -> 0x00f0 }
            boolean r3 = r8.a()     // Catch:{ Throwable -> 0x00f0 }
            if (r3 == 0) goto L_0x0108
            if (r4 == 0) goto L_0x0108
            java.lang.String r3 = "rqdp{  isable}"
            r5 = 0
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Throwable -> 0x00f0 }
            com.tencent.feedback.common.g.b(r3, r5)     // Catch:{ Throwable -> 0x00f0 }
            boolean r3 = r4.d()     // Catch:{ Throwable -> 0x00f0 }
            boolean r5 = r0.j()     // Catch:{ Throwable -> 0x00f0 }
            if (r3 == 0) goto L_0x00ba
            if (r5 == 0) goto L_0x009c
            r3 = r1
        L_0x0050:
            java.lang.String r5 = "rqdp{  needDetail} %b rqdp{  allQ:}%b rqdp{  result:}%b"
            r6 = 3
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Throwable -> 0x00f0 }
            r7 = 0
            boolean r4 = r4.d()     // Catch:{ Throwable -> 0x00f0 }
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r4)     // Catch:{ Throwable -> 0x00f0 }
            r6[r7] = r4     // Catch:{ Throwable -> 0x00f0 }
            r4 = 1
            boolean r0 = r0.j()     // Catch:{ Throwable -> 0x00f0 }
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ Throwable -> 0x00f0 }
            r6[r4] = r0     // Catch:{ Throwable -> 0x00f0 }
            r0 = 2
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r3)     // Catch:{ Throwable -> 0x00f0 }
            r6[r0] = r4     // Catch:{ Throwable -> 0x00f0 }
            com.tencent.feedback.common.g.b(r5, r6)     // Catch:{ Throwable -> 0x00f0 }
            int r0 = r8.g()     // Catch:{ Throwable -> 0x00f0 }
            if (r0 <= 0) goto L_0x00ce
            r0 = r1
        L_0x007c:
            if (r0 == 0) goto L_0x00d0
            java.lang.String r0 = "rqdp{  asyn up module} %d"
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x00f0 }
            r4 = 0
            int r5 = r8.b     // Catch:{ Throwable -> 0x00f0 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ Throwable -> 0x00f0 }
            r3[r4] = r5     // Catch:{ Throwable -> 0x00f0 }
            com.tencent.feedback.common.g.b(r0, r3)     // Catch:{ Throwable -> 0x00f0 }
            com.tencent.feedback.common.c r0 = com.tencent.feedback.common.c.a()     // Catch:{ Throwable -> 0x00f0 }
            com.tencent.feedback.common.m r3 = new com.tencent.feedback.common.m     // Catch:{ Throwable -> 0x00f0 }
            r3.<init>(r8)     // Catch:{ Throwable -> 0x00f0 }
            r0.a(r3)     // Catch:{ Throwable -> 0x00f0 }
        L_0x009b:
            return
        L_0x009c:
            boolean r3 = r8.i()     // Catch:{ Throwable -> 0x00f0 }
            if (r3 != 0) goto L_0x00ba
            android.content.Context r3 = r8.f2549a     // Catch:{ Throwable -> 0x00f0 }
            int r5 = r8.d     // Catch:{ Throwable -> 0x00f0 }
            com.tencent.feedback.proguard.ba r3 = com.tencent.feedback.proguard.ac.a(r3, r5)     // Catch:{ Throwable -> 0x00f0 }
            if (r3 == 0) goto L_0x00c6
            com.tencent.feedback.b.h r5 = r8.l()     // Catch:{ Throwable -> 0x00f0 }
            if (r5 != 0) goto L_0x00bc
            java.lang.String r3 = "rqdp{  imposiable eup reshandler not ready}"
            r5 = 0
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Throwable -> 0x00f0 }
            com.tencent.feedback.common.g.c(r3, r5)     // Catch:{ Throwable -> 0x00f0 }
        L_0x00ba:
            r3 = r2
            goto L_0x0050
        L_0x00bc:
            int r6 = r8.d     // Catch:{ Throwable -> 0x00f0 }
            byte[] r3 = r3.c()     // Catch:{ Throwable -> 0x00f0 }
            r7 = 0
            r5.a(r6, r3, r7)     // Catch:{ Throwable -> 0x00f0 }
        L_0x00c6:
            boolean r3 = r8.i()     // Catch:{ Throwable -> 0x00f0 }
            if (r3 != 0) goto L_0x00ba
            r3 = r1
            goto L_0x0050
        L_0x00ce:
            r0 = r2
            goto L_0x007c
        L_0x00d0:
            if (r3 == 0) goto L_0x009b
            java.lang.String r0 = "rqdp{  asyn query module }%d"
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x00f0 }
            r4 = 0
            int r5 = r8.b     // Catch:{ Throwable -> 0x00f0 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ Throwable -> 0x00f0 }
            r3[r4] = r5     // Catch:{ Throwable -> 0x00f0 }
            com.tencent.feedback.common.g.b(r0, r3)     // Catch:{ Throwable -> 0x00f0 }
            com.tencent.feedback.common.c r0 = com.tencent.feedback.common.c.a()     // Catch:{ Throwable -> 0x00f0 }
            com.tencent.feedback.common.n r3 = new com.tencent.feedback.common.n     // Catch:{ Throwable -> 0x00f0 }
            r3.<init>(r8)     // Catch:{ Throwable -> 0x00f0 }
            r0.a(r3)     // Catch:{ Throwable -> 0x00f0 }
            goto L_0x009b
        L_0x00f0:
            r0 = move-exception
            boolean r3 = com.tencent.feedback.common.g.a(r0)
            if (r3 != 0) goto L_0x00fa
            r0.printStackTrace()
        L_0x00fa:
            java.lang.String r3 = "rqdp{  common query end error} %s"
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.String r0 = r0.toString()
            r1[r2] = r0
            com.tencent.feedback.common.g.d(r3, r1)
            goto L_0x009b
        L_0x0108:
            java.lang.String r0 = "rqdp{  disable}"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x00f0 }
            com.tencent.feedback.common.g.b(r0, r3)     // Catch:{ Throwable -> 0x00f0 }
            goto L_0x009b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.common.l.e():void");
    }

    public void f() {
        g.a("rqdp{  app first start} %s", getClass().toString());
    }

    public int g() {
        if (!a()) {
            return -1;
        }
        return 0;
    }

    public boolean h() {
        if (!a()) {
            return false;
        }
        return true;
    }

    public boolean i() {
        return true;
    }

    public final boolean j() {
        i c2;
        if (!a() || (c2 = c()) == null) {
            return false;
        }
        c2.a(new c(this.f2549a, this.b, this.c));
        return true;
    }

    public final void a(at atVar) {
        au e2;
        g.b("rqdp{  com strateyg changed }%s", getClass().toString());
        if (atVar != null && (e2 = atVar.e(this.b)) != null) {
            boolean z = e2.c() && e2.b();
            if (a() != z) {
                g.a("rqdp{  module} %d rqdp{  able changed }%b", Integer.valueOf(this.b), Boolean.valueOf(z));
                b(z);
            }
        }
    }

    public final synchronized int k() {
        return this.i;
    }

    private synchronized void a(int i2) {
        this.i = i2;
    }
}
