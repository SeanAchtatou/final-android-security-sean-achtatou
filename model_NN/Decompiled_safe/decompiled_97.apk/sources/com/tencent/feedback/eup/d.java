package com.tencent.feedback.eup;

import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.feedback.common.g;
import java.util.Locale;

/* compiled from: ProGuard */
public class d implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private int f2554a = 10;
    private int b = 10;
    private int c = 3;
    private int d = 10;
    private boolean e = true;
    private boolean f = false;
    private boolean g = true;
    private int h = 100;
    private String i = null;
    private boolean j = false;
    private String k = null;
    private int l = EventDispatcherEnum.CONNECTION_EVENT_START;
    private int m = 3;
    private int n = 100;
    private boolean o = false;
    private int p = 60;
    private int q = 50;
    private boolean r = true;
    private boolean s = false;
    private boolean t = false;

    public synchronized int a() {
        return this.f2554a;
    }

    public synchronized void a(int i2) {
        if (i2 > 0 && i2 <= 20) {
            this.f2554a = i2;
        }
    }

    public synchronized int b() {
        return this.b;
    }

    public synchronized void b(int i2) {
        if (i2 > 0) {
            this.b = i2;
        }
    }

    public synchronized int c() {
        return this.c;
    }

    public synchronized void c(int i2) {
        if (i2 > 0) {
            this.c = i2;
        }
    }

    public synchronized int d() {
        return this.d;
    }

    public synchronized void d(int i2) {
        if (i2 > 0) {
            this.d = i2;
        }
    }

    public synchronized boolean e() {
        return this.e;
    }

    public synchronized void a(boolean z) {
        this.e = z;
    }

    public synchronized void b(boolean z) {
        this.f = z;
    }

    public synchronized void c(boolean z) {
        this.g = z;
    }

    public synchronized int f() {
        return this.h;
    }

    public synchronized void e(int i2) {
        if (i2 > 0) {
            this.h = i2;
        }
    }

    public synchronized String g() {
        return this.i;
    }

    public synchronized void a(String str) {
        this.i = str;
    }

    public synchronized boolean h() {
        return this.j;
    }

    public synchronized int i() {
        return this.l;
    }

    public synchronized String toString() {
        String str;
        try {
            str = String.format(Locale.US, "[MSNum:%d ,Wifi:%d,GPRS:%d,ODay:%d,isMerged:%b,AfQ:%b,Silent:%b,mLog:%d,tag:%s,assert:%s, interval:%s, limit:%s]", Integer.valueOf(this.f2554a), Integer.valueOf(this.b), Integer.valueOf(this.c), Integer.valueOf(this.d), Boolean.valueOf(this.e), Boolean.valueOf(this.f), Boolean.valueOf(this.g), Integer.valueOf(this.h), this.i, Boolean.valueOf(this.o), Integer.valueOf(this.q), Integer.valueOf(this.p));
        } catch (Throwable th) {
            if (!g.a(th)) {
                th.printStackTrace();
            }
            str = "error";
        }
        return str;
    }

    /* renamed from: j */
    public synchronized d clone() {
        d dVar;
        dVar = new d();
        dVar.b(this.f);
        dVar.a(this.f2554a);
        dVar.c(this.c);
        dVar.b(this.b);
        dVar.a(this.e);
        dVar.d(this.d);
        dVar.c(this.g);
        dVar.e(this.h);
        dVar.a(this.i);
        dVar.d(this.o);
        dVar.f(this.p);
        dVar.g(this.q);
        return dVar;
    }

    public synchronized void d(boolean z) {
        this.o = z;
    }

    public synchronized boolean k() {
        return this.o;
    }

    public synchronized void f(int i2) {
        if (i2 < 60) {
            g.a("rqdp{The interval of assert check task is smaller than the default time.} [%s s]", Integer.valueOf(i2));
        }
        if (i2 <= 0) {
            i2 = 60;
        }
        this.p = i2;
    }

    public synchronized int l() {
        return this.p;
    }

    public synchronized void g(int i2) {
        if (i2 < 50) {
            g.a("rqdp{The trigger count of the assert store is smaller than the default count.} [%s]", Integer.valueOf(i2));
        }
        if (i2 <= 0) {
            i2 = 50;
        }
        this.q = i2;
    }

    public synchronized int m() {
        return this.q;
    }

    public synchronized String n() {
        return this.k;
    }

    public synchronized int o() {
        return this.m;
    }

    public synchronized int p() {
        return this.n;
    }

    public synchronized boolean q() {
        return this.r;
    }

    public synchronized void e(boolean z) {
        this.r = z;
    }

    public synchronized boolean r() {
        return this.s;
    }

    public synchronized void f(boolean z) {
        this.s = z;
    }

    public synchronized boolean s() {
        return this.t;
    }

    public synchronized void g(boolean z) {
        this.t = z;
    }
}
