package com.tencent.feedback.proguard;

import android.content.Context;
import com.tencent.connect.common.Constants;
import com.tencent.feedback.common.e;
import com.tencent.feedback.common.g;
import com.tencent.feedback.eup.a;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Array;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/* compiled from: ProGuard */
public class ac {

    /* renamed from: a  reason: collision with root package name */
    protected HashMap<String, HashMap<String, byte[]>> f2583a = new HashMap<>();
    protected String b;
    ag c;
    private HashMap<String, Object> d;

    public static int a(Context context, p[] pVarArr) {
        if (context == null || pVarArr == null || pVarArr.length <= 0) {
            return -1;
        }
        ArrayList arrayList = new ArrayList(pVarArr.length);
        for (p pVar : pVarArr) {
            byte[] a2 = a(pVar);
            if (a2 != null) {
                aj ajVar = new aj(7, 0, 0, a2);
                ajVar.a(pVar.a());
                arrayList.add(ajVar);
            }
        }
        if (arrayList.size() <= 0) {
            return 0;
        }
        if (aj.b(context, arrayList)) {
            return arrayList.size();
        }
        return -1;
    }

    ac() {
        new HashMap();
        this.d = new HashMap<>();
        this.b = "GBK";
        this.c = new ag();
    }

    /* JADX INFO: finally extract failed */
    public static byte[] a(String str, int i) {
        g.a("rqdp{  LogcatManager.getLogDatas() start + count:}" + i, new Object[0]);
        ArrayList arrayList = new ArrayList();
        arrayList.clear();
        arrayList.add("logcat");
        arrayList.add("-d");
        arrayList.add("-v");
        arrayList.add("time");
        if (str != null && str.length() > 0) {
            arrayList.add("-s");
            arrayList.add(str);
        }
        String[] strArr = new String[arrayList.size()];
        arrayList.toArray(strArr);
        a aVar = new a(i);
        a(strArr, aVar);
        if (aVar.size() <= 0) {
            return null;
        }
        g.a("rqdp{  get log success in list size:}" + aVar.size(), new Object[0]);
        try {
            StringBuffer stringBuffer = new StringBuffer();
            Iterator it = aVar.iterator();
            while (it.hasNext()) {
                stringBuffer.append(((String) it.next()) + "\n");
            }
            aVar.clear();
            byte[] bytes = stringBuffer.toString().getBytes("utf-8");
            stringBuffer.setLength(0);
            g.a("rqdp{  LogcatManager.getLogDatas() end}", new Object[0]);
            return bytes;
        } catch (Throwable th) {
            g.a("rqdp{  LogcatManager.getLogDatas() end}", new Object[0]);
            throw th;
        }
    }

    public static Long[] a(LinkedHashMap<Long, Long> linkedHashMap, long j) {
        long j2 = 0;
        if (linkedHashMap == null || linkedHashMap.size() <= 0 || j <= 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        Iterator<Long> it = linkedHashMap.keySet().iterator();
        while (true) {
            long j3 = j2;
            if (it.hasNext() && j3 < j) {
                long longValue = it.next().longValue();
                long longValue2 = linkedHashMap.get(Long.valueOf(longValue)).longValue();
                if (j3 + longValue2 <= j) {
                    arrayList.add(Long.valueOf(longValue));
                    j2 = j3 + longValue2;
                } else {
                    j2 = j3;
                }
            }
        }
        if (arrayList.size() > 0) {
            return (Long[]) arrayList.toArray(new Long[1]);
        }
        return null;
    }

    public static Map<String, String> b() {
        Map<Thread, StackTraceElement[]> allStackTraces = Thread.getAllStackTraces();
        if (allStackTraces == null) {
            return null;
        }
        HashMap hashMap = new HashMap();
        StringBuilder sb = new StringBuilder();
        try {
            for (Map.Entry next : allStackTraces.entrySet()) {
                sb.setLength(0);
                for (StackTraceElement stackTraceElement : (StackTraceElement[]) next.getValue()) {
                    sb.append(stackTraceElement.toString()).append("\n");
                }
                hashMap.put(((Thread) next.getKey()).getName(), sb.toString());
            }
        } catch (Throwable th) {
            g.d("add all thread error", new Object[0]);
            if (!g.a(th)) {
                th.printStackTrace();
            }
        }
        return hashMap;
    }

    public static synchronized int a(Context context, q[] qVarArr) {
        int i;
        synchronized (ac.class) {
            if (!(context == null || qVarArr == null)) {
                if (qVarArr.length > 0) {
                    ArrayList arrayList = new ArrayList(qVarArr.length);
                    for (q qVar : qVarArr) {
                        byte[] a2 = a(qVar);
                        if (a2 == null) {
                            g.c("rqdp{ getSerData error }", new Object[0]);
                        } else {
                            aj ajVar = new aj(9, 0, qVar.a(), a2);
                            ajVar.a(qVar.c());
                            arrayList.add(ajVar);
                        }
                    }
                    i = (arrayList.size() <= 0 || !aj.a(context, arrayList)) ? 0 : arrayList.size();
                }
            }
            g.c("rqdp{  args error}", new Object[0]);
            i = 0;
        }
        return i;
    }

    public static List<p> a(Context context) {
        List<aj> a2;
        if (context == null || (a2 = aj.a(context, new int[]{7}, -1, -1, Long.MAX_VALUE, 5, null, -1, -1, -1, -1, -1, Long.MAX_VALUE)) == null || a2.size() <= 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList(a2.size());
        for (aj next : a2) {
            try {
                p cast = p.class.cast(b(next.b()));
                cast.a(next.a());
                arrayList.add(cast);
            } catch (Throwable th) {
                if (!g.a(th)) {
                    th.printStackTrace();
                }
                g.d("rqdp{  netconsume error }%s", th.toString());
            }
        }
        return arrayList;
    }

    public static synchronized int b(Context context, q[] qVarArr) {
        int i = 0;
        synchronized (ac.class) {
            if (!(context == null || qVarArr == null)) {
                if (qVarArr.length > 0) {
                    ArrayList arrayList = new ArrayList(qVarArr.length);
                    for (q qVar : qVarArr) {
                        if (qVar.d() >= 0) {
                            arrayList.add(Long.valueOf(qVar.d()));
                        }
                    }
                    if (arrayList.size() > 0) {
                        i = aj.a(context, (Long[]) arrayList.toArray(new Long[0]));
                    }
                }
            }
            g.c("rqdp{  args error}", new Object[0]);
        }
        return i;
    }

    public static boolean a(String[] strArr, a<String> aVar) {
        if (strArr == null || strArr.length <= 0) {
            return false;
        }
        Process process = null;
        try {
            process = Runtime.getRuntime().exec(strArr);
            if (aVar != null) {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
                while (true) {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        break;
                    }
                    aVar.add(readLine);
                }
                bufferedReader.close();
            } else {
                process.waitFor();
            }
            if (process != null) {
                try {
                    process.getOutputStream().close();
                } catch (IOException e) {
                    if (!g.a(e)) {
                        e.printStackTrace();
                    }
                }
                try {
                    process.getInputStream().close();
                } catch (IOException e2) {
                    if (!g.a(e2)) {
                        e2.printStackTrace();
                    }
                }
                try {
                    process.getErrorStream().close();
                } catch (IOException e3) {
                    if (!g.a(e3)) {
                        e3.printStackTrace();
                    }
                }
            }
            return true;
        } catch (Throwable th) {
            if (process != null) {
                try {
                    process.getOutputStream().close();
                } catch (IOException e4) {
                    if (!g.a(e4)) {
                        e4.printStackTrace();
                    }
                }
                try {
                    process.getInputStream().close();
                } catch (IOException e5) {
                    if (!g.a(e5)) {
                        e5.printStackTrace();
                    }
                }
                try {
                    process.getErrorStream().close();
                } catch (IOException e6) {
                    if (!g.a(e6)) {
                        e6.printStackTrace();
                    }
                }
            }
            throw th;
        }
    }

    public static String a(ArrayList<String> arrayList) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < arrayList.size(); i++) {
            String str = arrayList.get(i);
            if (str.equals("java.lang.Integer") || str.equals("int")) {
                str = "int32";
            } else if (str.equals("java.lang.Boolean") || str.equals("boolean")) {
                str = "bool";
            } else if (str.equals("java.lang.Byte") || str.equals("byte")) {
                str = "char";
            } else if (str.equals("java.lang.Double") || str.equals("double")) {
                str = "double";
            } else if (str.equals("java.lang.Float") || str.equals("float")) {
                str = "float";
            } else if (str.equals("java.lang.Long") || str.equals("long")) {
                str = "int64";
            } else if (str.equals("java.lang.Short") || str.equals("short")) {
                str = "short";
            } else if (str.equals("java.lang.Character")) {
                throw new IllegalArgumentException("can not support java.lang.Character");
            } else if (str.equals("java.lang.String")) {
                str = "string";
            } else if (str.equals("java.util.List")) {
                str = "list";
            } else if (str.equals("java.util.Map")) {
                str = "map";
            }
            arrayList.set(i, str);
        }
        Collections.reverse(arrayList);
        for (int i2 = 0; i2 < arrayList.size(); i2++) {
            String str2 = arrayList.get(i2);
            if (str2.equals("list")) {
                arrayList.set(i2 - 1, "<" + arrayList.get(i2 - 1));
                arrayList.set(0, arrayList.get(0) + ">");
            } else if (str2.equals("map")) {
                arrayList.set(i2 - 1, "<" + arrayList.get(i2 - 1) + ",");
                arrayList.set(0, arrayList.get(0) + ">");
            } else if (str2.equals("Array")) {
                arrayList.set(i2 - 1, "<" + arrayList.get(i2 - 1));
                arrayList.set(0, arrayList.get(0) + ">");
            }
        }
        Collections.reverse(arrayList);
        Iterator<String> it = arrayList.iterator();
        while (it.hasNext()) {
            stringBuffer.append(it.next());
        }
        return stringBuffer.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ah.a(java.lang.Object, int):void
     arg types: [T, int]
     candidates:
      com.tencent.feedback.proguard.ah.a(byte, int):void
      com.tencent.feedback.proguard.ah.a(float, int):void
      com.tencent.feedback.proguard.ah.a(int, int):void
      com.tencent.feedback.proguard.ah.a(long, int):void
      com.tencent.feedback.proguard.ah.a(com.tencent.feedback.proguard.j, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.String, int):void
      com.tencent.feedback.proguard.ah.a(java.util.Collection, int):void
      com.tencent.feedback.proguard.ah.a(java.util.Map, int):void
      com.tencent.feedback.proguard.ah.a(short, int):void
      com.tencent.feedback.proguard.ah.a(boolean, int):void
      com.tencent.feedback.proguard.ah.a(byte[], int):void
      com.tencent.feedback.proguard.ah.a(java.lang.Object, int):void */
    public <T> void a(String str, T t) {
        if (str == null) {
            throw new IllegalArgumentException("put key can not is null");
        } else if (t == null) {
            throw new IllegalArgumentException("put value can not is null");
        } else if (t instanceof Set) {
            throw new IllegalArgumentException("can not support Set");
        } else {
            ah ahVar = new ah();
            ahVar.a(this.b);
            ahVar.a((Object) t, 0);
            byte[] a2 = ai.a(ahVar.a());
            HashMap hashMap = new HashMap(1);
            ArrayList arrayList = new ArrayList(1);
            a(arrayList, t);
            hashMap.put(a((ArrayList<String>) arrayList), a2);
            this.d.remove(str);
            this.f2583a.put(str, hashMap);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:46:0x0085 A[SYNTHETIC, Splitter:B:46:0x0085] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] a(java.lang.Object r5) {
        /*
            r0 = 0
            r3 = 0
            java.lang.String r1 = "rqdp{  en obj 2 bytes}"
            java.lang.Object[] r2 = new java.lang.Object[r3]
            com.tencent.feedback.common.g.b(r1, r2)
            if (r5 == 0) goto L_0x0013
            java.lang.Class<java.io.Serializable> r1 = java.io.Serializable.class
            boolean r1 = r1.isInstance(r5)
            if (r1 != 0) goto L_0x001b
        L_0x0013:
            java.lang.String r1 = "rqdp{  not serial obj}"
            java.lang.Object[] r2 = new java.lang.Object[r3]
            com.tencent.feedback.common.g.c(r1, r2)
        L_0x001a:
            return r0
        L_0x001b:
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream
            r3.<init>()
            java.io.ObjectOutputStream r2 = new java.io.ObjectOutputStream     // Catch:{ Throwable -> 0x004c, all -> 0x0080 }
            r2.<init>(r3)     // Catch:{ Throwable -> 0x004c, all -> 0x0080 }
            r2.writeObject(r5)     // Catch:{ Throwable -> 0x00a4 }
            r2.flush()     // Catch:{ Throwable -> 0x00a4 }
            byte[] r0 = r3.toByteArray()     // Catch:{ Throwable -> 0x00a4 }
            r2.close()     // Catch:{ IOException -> 0x0041 }
        L_0x0032:
            r3.close()     // Catch:{ IOException -> 0x0036 }
            goto L_0x001a
        L_0x0036:
            r1 = move-exception
            boolean r2 = com.tencent.feedback.common.g.a(r1)
            if (r2 != 0) goto L_0x001a
            r1.printStackTrace()
            goto L_0x001a
        L_0x0041:
            r1 = move-exception
            boolean r2 = com.tencent.feedback.common.g.a(r1)
            if (r2 != 0) goto L_0x0032
            r1.printStackTrace()
            goto L_0x0032
        L_0x004c:
            r1 = move-exception
            r2 = r0
        L_0x004e:
            boolean r4 = com.tencent.feedback.common.g.a(r1)     // Catch:{ all -> 0x00a2 }
            if (r4 != 0) goto L_0x0057
            r1.printStackTrace()     // Catch:{ all -> 0x00a2 }
        L_0x0057:
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x00a2 }
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x00a2 }
            com.tencent.feedback.common.g.d(r1, r4)     // Catch:{ all -> 0x00a2 }
            if (r2 == 0) goto L_0x0066
            r2.close()     // Catch:{ IOException -> 0x0075 }
        L_0x0066:
            r3.close()     // Catch:{ IOException -> 0x006a }
            goto L_0x001a
        L_0x006a:
            r1 = move-exception
            boolean r2 = com.tencent.feedback.common.g.a(r1)
            if (r2 != 0) goto L_0x001a
            r1.printStackTrace()
            goto L_0x001a
        L_0x0075:
            r1 = move-exception
            boolean r2 = com.tencent.feedback.common.g.a(r1)
            if (r2 != 0) goto L_0x0066
            r1.printStackTrace()
            goto L_0x0066
        L_0x0080:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x0083:
            if (r2 == 0) goto L_0x0088
            r2.close()     // Catch:{ IOException -> 0x008c }
        L_0x0088:
            r3.close()     // Catch:{ IOException -> 0x0097 }
        L_0x008b:
            throw r0
        L_0x008c:
            r1 = move-exception
            boolean r2 = com.tencent.feedback.common.g.a(r1)
            if (r2 != 0) goto L_0x0088
            r1.printStackTrace()
            goto L_0x0088
        L_0x0097:
            r1 = move-exception
            boolean r2 = com.tencent.feedback.common.g.a(r1)
            if (r2 != 0) goto L_0x008b
            r1.printStackTrace()
            goto L_0x008b
        L_0x00a2:
            r0 = move-exception
            goto L_0x0083
        L_0x00a4:
            r1 = move-exception
            goto L_0x004e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.proguard.ac.a(java.lang.Object):byte[]");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v1, resolved type: com.tencent.feedback.proguard.n} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v5, resolved type: android.content.ContentValues} */
    /* JADX WARN: Type inference failed for: r3v0 */
    /* JADX WARN: Type inference failed for: r3v2 */
    /* JADX WARN: Type inference failed for: r3v3 */
    /* JADX WARN: Type inference failed for: r3v12 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00af A[Catch:{ all -> 0x0114 }] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00bf  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x010b  */
    /* JADX WARNING: Removed duplicated region for block: B:80:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(android.content.Context r10, com.tencent.feedback.proguard.ba r11) {
        /*
            r8 = 0
            r3 = 0
            r1 = 1
            r0 = 0
            if (r10 == 0) goto L_0x0009
            if (r11 != 0) goto L_0x0011
        L_0x0009:
            java.lang.String r1 = "rqdp{  context == null || bean == null}"
            java.lang.Object[] r2 = new java.lang.Object[r0]
            com.tencent.feedback.common.g.c(r1, r2)
        L_0x0010:
            return r0
        L_0x0011:
            com.tencent.feedback.proguard.n r4 = new com.tencent.feedback.proguard.n     // Catch:{ Throwable -> 0x0117, all -> 0x00fb }
            r4.<init>(r10)     // Catch:{ Throwable -> 0x0117, all -> 0x00fb }
            android.database.sqlite.SQLiteDatabase r2 = r4.getWritableDatabase()     // Catch:{ Throwable -> 0x011a, all -> 0x010f }
            if (r2 != 0) goto L_0x0033
            java.lang.String r1 = "get db fail!,return "
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x0098, all -> 0x0112 }
            com.tencent.feedback.common.g.d(r1, r3)     // Catch:{ Throwable -> 0x0098, all -> 0x0112 }
            if (r2 == 0) goto L_0x002f
            boolean r1 = r2.isOpen()
            if (r1 == 0) goto L_0x002f
            r2.close()
        L_0x002f:
            r4.close()
            goto L_0x0010
        L_0x0033:
            if (r11 != 0) goto L_0x005a
        L_0x0035:
            if (r3 == 0) goto L_0x00eb
            java.lang.String r5 = "strategy"
            java.lang.String r6 = "_id"
            long r5 = r2.replace(r5, r6, r3)     // Catch:{ Throwable -> 0x0098, all -> 0x0112 }
            int r3 = (r5 > r8 ? 1 : (r5 == r8 ? 0 : -1))
            if (r3 >= 0) goto L_0x00c4
            java.lang.String r1 = "rqdp{  insert failure! return}"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x0098, all -> 0x0112 }
            com.tencent.feedback.common.g.c(r1, r3)     // Catch:{ Throwable -> 0x0098, all -> 0x0112 }
            if (r2 == 0) goto L_0x0056
            boolean r1 = r2.isOpen()
            if (r1 == 0) goto L_0x0056
            r2.close()
        L_0x0056:
            r4.close()
            goto L_0x0010
        L_0x005a:
            android.content.ContentValues r3 = new android.content.ContentValues     // Catch:{ Throwable -> 0x0098, all -> 0x0112 }
            r3.<init>()     // Catch:{ Throwable -> 0x0098, all -> 0x0112 }
            long r5 = r11.a()     // Catch:{ Throwable -> 0x0098, all -> 0x0112 }
            int r5 = (r5 > r8 ? 1 : (r5 == r8 ? 0 : -1))
            if (r5 < 0) goto L_0x0074
            java.lang.String r5 = "_id"
            long r6 = r11.a()     // Catch:{ Throwable -> 0x0098, all -> 0x0112 }
            java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch:{ Throwable -> 0x0098, all -> 0x0112 }
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x0098, all -> 0x0112 }
        L_0x0074:
            java.lang.String r5 = "_key"
            int r6 = r11.b()     // Catch:{ Throwable -> 0x0098, all -> 0x0112 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Throwable -> 0x0098, all -> 0x0112 }
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x0098, all -> 0x0112 }
            java.lang.String r5 = "_datas"
            byte[] r6 = r11.c()     // Catch:{ Throwable -> 0x0098, all -> 0x0112 }
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x0098, all -> 0x0112 }
            java.lang.String r5 = "_ut"
            long r6 = r11.d()     // Catch:{ Throwable -> 0x0098, all -> 0x0112 }
            java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch:{ Throwable -> 0x0098, all -> 0x0112 }
            r3.put(r5, r6)     // Catch:{ Throwable -> 0x0098, all -> 0x0112 }
            goto L_0x0035
        L_0x0098:
            r1 = move-exception
            r3 = r4
        L_0x009a:
            java.lang.String r4 = "rqdp{  Error strategy update!} %s"
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x0114 }
            r6 = 0
            java.lang.String r7 = r1.toString()     // Catch:{ all -> 0x0114 }
            r5[r6] = r7     // Catch:{ all -> 0x0114 }
            com.tencent.feedback.common.g.d(r4, r5)     // Catch:{ all -> 0x0114 }
            boolean r4 = com.tencent.feedback.common.g.a(r1)     // Catch:{ all -> 0x0114 }
            if (r4 != 0) goto L_0x00b2
            r1.printStackTrace()     // Catch:{ all -> 0x0114 }
        L_0x00b2:
            if (r2 == 0) goto L_0x00bd
            boolean r1 = r2.isOpen()
            if (r1 == 0) goto L_0x00bd
            r2.close()
        L_0x00bd:
            if (r3 == 0) goto L_0x0010
            r3.close()
            goto L_0x0010
        L_0x00c4:
            r11.a(r5)     // Catch:{ Throwable -> 0x0098, all -> 0x0112 }
            java.lang.String r3 = "rqdp{  update strategy} %d rqdp{  true}"
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Throwable -> 0x0098, all -> 0x0112 }
            r6 = 0
            int r7 = r11.b()     // Catch:{ Throwable -> 0x0098, all -> 0x0112 }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ Throwable -> 0x0098, all -> 0x0112 }
            r5[r6] = r7     // Catch:{ Throwable -> 0x0098, all -> 0x0112 }
            com.tencent.feedback.common.g.b(r3, r5)     // Catch:{ Throwable -> 0x0098, all -> 0x0112 }
            if (r2 == 0) goto L_0x00e5
            boolean r0 = r2.isOpen()
            if (r0 == 0) goto L_0x00e5
            r2.close()
        L_0x00e5:
            r4.close()
            r0 = r1
            goto L_0x0010
        L_0x00eb:
            if (r2 == 0) goto L_0x00f6
            boolean r1 = r2.isOpen()
            if (r1 == 0) goto L_0x00f6
            r2.close()
        L_0x00f6:
            r4.close()
            goto L_0x0010
        L_0x00fb:
            r0 = move-exception
            r2 = r3
            r4 = r3
        L_0x00fe:
            if (r2 == 0) goto L_0x0109
            boolean r1 = r2.isOpen()
            if (r1 == 0) goto L_0x0109
            r2.close()
        L_0x0109:
            if (r4 == 0) goto L_0x010e
            r4.close()
        L_0x010e:
            throw r0
        L_0x010f:
            r0 = move-exception
            r2 = r3
            goto L_0x00fe
        L_0x0112:
            r0 = move-exception
            goto L_0x00fe
        L_0x0114:
            r0 = move-exception
            r4 = r3
            goto L_0x00fe
        L_0x0117:
            r1 = move-exception
            r2 = r3
            goto L_0x009a
        L_0x011a:
            r1 = move-exception
            r2 = r3
            r3 = r4
            goto L_0x009a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.proguard.ac.a(android.content.Context, com.tencent.feedback.proguard.ba):boolean");
    }

    public static synchronized q[] a(Context context, String str) {
        q[] qVarArr;
        synchronized (ac.class) {
            if (context == null || str == null) {
                g.c("rqdp{  args error}", new Object[0]);
                qVarArr = null;
            } else {
                List<aj> a2 = aj.a(context, new int[]{9}, -1, -1, 10000, -1, str, -1, -1, -1, -1, -1, Long.MAX_VALUE);
                if (a2 == null || a2.size() <= 0) {
                    qVarArr = null;
                } else {
                    ArrayList arrayList = new ArrayList(a2.size());
                    for (aj next : a2) {
                        Object b2 = b(next.b());
                        if (b2 != null && q.class.isInstance(b2)) {
                            q cast = q.class.cast(b2);
                            cast.b(next.a());
                            arrayList.add(cast);
                        }
                    }
                    if (arrayList.size() > 0) {
                        qVarArr = (q[]) arrayList.toArray(new q[0]);
                    } else {
                        qVarArr = null;
                    }
                }
            }
        }
        return qVarArr;
    }

    public static synchronized int b(Context context, String str) {
        int i = 0;
        synchronized (ac.class) {
            if (context == null || str == null) {
                g.c("rqdp{  args error}", new Object[0]);
            } else {
                i = aj.a(context, new int[]{9}, -1, Long.MAX_VALUE, str);
            }
        }
        return i;
    }

    /* JADX WARNING: Removed duplicated region for block: B:45:0x0073 A[SYNTHETIC, Splitter:B:45:0x0073] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Object b(byte[] r5) {
        /*
            r2 = 0
            r0 = 0
            java.lang.String r1 = "rqdp{  de byte 2 obj}"
            java.lang.Object[] r2 = new java.lang.Object[r2]
            com.tencent.feedback.common.g.b(r1, r2)
            if (r5 == 0) goto L_0x000e
            int r1 = r5.length
            if (r1 >= 0) goto L_0x000f
        L_0x000e:
            return r0
        L_0x000f:
            java.io.ByteArrayInputStream r3 = new java.io.ByteArrayInputStream
            r3.<init>(r5)
            java.io.ObjectInputStream r2 = new java.io.ObjectInputStream     // Catch:{ Throwable -> 0x003a, all -> 0x006e }
            r2.<init>(r3)     // Catch:{ Throwable -> 0x003a, all -> 0x006e }
            java.lang.Object r0 = r2.readObject()     // Catch:{ Throwable -> 0x0092 }
            r2.close()     // Catch:{ IOException -> 0x002f }
        L_0x0020:
            r3.close()     // Catch:{ IOException -> 0x0024 }
            goto L_0x000e
        L_0x0024:
            r1 = move-exception
            boolean r2 = com.tencent.feedback.common.g.a(r1)
            if (r2 != 0) goto L_0x000e
            r1.printStackTrace()
            goto L_0x000e
        L_0x002f:
            r1 = move-exception
            boolean r2 = com.tencent.feedback.common.g.a(r1)
            if (r2 != 0) goto L_0x0020
            r1.printStackTrace()
            goto L_0x0020
        L_0x003a:
            r1 = move-exception
            r2 = r0
        L_0x003c:
            boolean r4 = com.tencent.feedback.common.g.a(r1)     // Catch:{ all -> 0x0090 }
            if (r4 != 0) goto L_0x0045
            r1.printStackTrace()     // Catch:{ all -> 0x0090 }
        L_0x0045:
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x0090 }
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x0090 }
            com.tencent.feedback.common.g.d(r1, r4)     // Catch:{ all -> 0x0090 }
            if (r2 == 0) goto L_0x0054
            r2.close()     // Catch:{ IOException -> 0x0063 }
        L_0x0054:
            r3.close()     // Catch:{ IOException -> 0x0058 }
            goto L_0x000e
        L_0x0058:
            r1 = move-exception
            boolean r2 = com.tencent.feedback.common.g.a(r1)
            if (r2 != 0) goto L_0x000e
            r1.printStackTrace()
            goto L_0x000e
        L_0x0063:
            r1 = move-exception
            boolean r2 = com.tencent.feedback.common.g.a(r1)
            if (r2 != 0) goto L_0x0054
            r1.printStackTrace()
            goto L_0x0054
        L_0x006e:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x0071:
            if (r2 == 0) goto L_0x0076
            r2.close()     // Catch:{ IOException -> 0x007a }
        L_0x0076:
            r3.close()     // Catch:{ IOException -> 0x0085 }
        L_0x0079:
            throw r0
        L_0x007a:
            r1 = move-exception
            boolean r2 = com.tencent.feedback.common.g.a(r1)
            if (r2 != 0) goto L_0x0076
            r1.printStackTrace()
            goto L_0x0076
        L_0x0085:
            r1 = move-exception
            boolean r2 = com.tencent.feedback.common.g.a(r1)
            if (r2 != 0) goto L_0x0079
            r1.printStackTrace()
            goto L_0x0079
        L_0x0090:
            r0 = move-exception
            goto L_0x0071
        L_0x0092:
            r1 = move-exception
            goto L_0x003c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.proguard.ac.b(byte[]):java.lang.Object");
    }

    public static void a(Context context, int i, byte[] bArr) {
        if (bArr != null) {
            ba baVar = new ba();
            baVar.a(i);
            baVar.a(bArr);
            baVar.b(new Date().getTime());
            a(context, baVar);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:80:0x014d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.tencent.feedback.proguard.ba a(android.content.Context r11, int r12) {
        /*
            r1 = 0
            r8 = 0
            if (r11 != 0) goto L_0x000d
            java.lang.String r0 = "rqdp{  context == null}"
            java.lang.Object[] r1 = new java.lang.Object[r1]
            com.tencent.feedback.common.g.c(r0, r1)
            r0 = r8
        L_0x000c:
            return r0
        L_0x000d:
            com.tencent.feedback.proguard.n r9 = new com.tencent.feedback.proguard.n     // Catch:{ Throwable -> 0x0162, all -> 0x0132 }
            r9.<init>(r11)     // Catch:{ Throwable -> 0x0162, all -> 0x0132 }
            android.database.sqlite.SQLiteDatabase r0 = r9.getWritableDatabase()     // Catch:{ Throwable -> 0x0168, all -> 0x0151 }
            if (r0 != 0) goto L_0x0030
            java.lang.String r1 = "rqdp{  getWritableDatabase fail!}"
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Throwable -> 0x016e, all -> 0x0154 }
            com.tencent.feedback.common.g.c(r1, r2)     // Catch:{ Throwable -> 0x016e, all -> 0x0154 }
            if (r0 == 0) goto L_0x002b
            boolean r1 = r0.isOpen()
            if (r1 == 0) goto L_0x002b
            r0.close()
        L_0x002b:
            r9.close()
            r0 = r8
            goto L_0x000c
        L_0x0030:
            java.util.Locale r1 = java.util.Locale.US     // Catch:{ Throwable -> 0x016e, all -> 0x0154 }
            java.lang.String r2 = " %s = %d "
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x016e, all -> 0x0154 }
            r4 = 0
            java.lang.String r5 = "_key"
            r3[r4] = r5     // Catch:{ Throwable -> 0x016e, all -> 0x0154 }
            r4 = 1
            java.lang.Integer r5 = java.lang.Integer.valueOf(r12)     // Catch:{ Throwable -> 0x016e, all -> 0x0154 }
            r3[r4] = r5     // Catch:{ Throwable -> 0x016e, all -> 0x0154 }
            java.lang.String r3 = java.lang.String.format(r1, r2, r3)     // Catch:{ Throwable -> 0x016e, all -> 0x0154 }
            java.lang.String r1 = "strategy"
            r2 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r2 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Throwable -> 0x016e, all -> 0x0154 }
            if (r2 == 0) goto L_0x0118
            boolean r1 = r2.moveToNext()     // Catch:{ Throwable -> 0x00dc, all -> 0x0159 }
            if (r1 == 0) goto L_0x0118
            if (r2 == 0) goto L_0x0068
            boolean r1 = r2.isBeforeFirst()     // Catch:{ Throwable -> 0x00dc, all -> 0x0159 }
            if (r1 != 0) goto L_0x0068
            boolean r1 = r2.isAfterLast()     // Catch:{ Throwable -> 0x00dc, all -> 0x0159 }
            if (r1 == 0) goto L_0x009a
        L_0x0068:
            r1 = r8
        L_0x0069:
            if (r1 == 0) goto L_0x0118
            java.lang.String r3 = "rqdp{  read strategy key:}%d"
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x00dc, all -> 0x0159 }
            r5 = 0
            int r6 = r1.b()     // Catch:{ Throwable -> 0x00dc, all -> 0x0159 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Throwable -> 0x00dc, all -> 0x0159 }
            r4[r5] = r6     // Catch:{ Throwable -> 0x00dc, all -> 0x0159 }
            com.tencent.feedback.common.g.b(r3, r4)     // Catch:{ Throwable -> 0x00dc, all -> 0x0159 }
            if (r2 == 0) goto L_0x0089
            boolean r3 = r2.isClosed()
            if (r3 != 0) goto L_0x0089
            r2.close()
        L_0x0089:
            if (r0 == 0) goto L_0x0094
            boolean r2 = r0.isOpen()
            if (r2 == 0) goto L_0x0094
            r0.close()
        L_0x0094:
            r9.close()
            r0 = r1
            goto L_0x000c
        L_0x009a:
            java.lang.String r1 = "rqdp{  parse bean}"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x00dc, all -> 0x0159 }
            com.tencent.feedback.common.g.b(r1, r3)     // Catch:{ Throwable -> 0x00dc, all -> 0x0159 }
            com.tencent.feedback.proguard.ba r1 = new com.tencent.feedback.proguard.ba     // Catch:{ Throwable -> 0x00dc, all -> 0x0159 }
            r1.<init>()     // Catch:{ Throwable -> 0x00dc, all -> 0x0159 }
            java.lang.String r3 = "_id"
            int r3 = r2.getColumnIndex(r3)     // Catch:{ Throwable -> 0x00dc, all -> 0x0159 }
            long r3 = r2.getLong(r3)     // Catch:{ Throwable -> 0x00dc, all -> 0x0159 }
            r1.a(r3)     // Catch:{ Throwable -> 0x00dc, all -> 0x0159 }
            java.lang.String r3 = "_key"
            int r3 = r2.getColumnIndex(r3)     // Catch:{ Throwable -> 0x00dc, all -> 0x0159 }
            int r3 = r2.getInt(r3)     // Catch:{ Throwable -> 0x00dc, all -> 0x0159 }
            r1.a(r3)     // Catch:{ Throwable -> 0x00dc, all -> 0x0159 }
            java.lang.String r3 = "_ut"
            int r3 = r2.getColumnIndex(r3)     // Catch:{ Throwable -> 0x00dc, all -> 0x0159 }
            long r3 = r2.getLong(r3)     // Catch:{ Throwable -> 0x00dc, all -> 0x0159 }
            r1.b(r3)     // Catch:{ Throwable -> 0x00dc, all -> 0x0159 }
            java.lang.String r3 = "_datas"
            int r3 = r2.getColumnIndex(r3)     // Catch:{ Throwable -> 0x00dc, all -> 0x0159 }
            byte[] r3 = r2.getBlob(r3)     // Catch:{ Throwable -> 0x00dc, all -> 0x0159 }
            r1.a(r3)     // Catch:{ Throwable -> 0x00dc, all -> 0x0159 }
            goto L_0x0069
        L_0x00dc:
            r1 = move-exception
            r3 = r9
            r10 = r2
            r2 = r0
            r0 = r1
            r1 = r10
        L_0x00e2:
            boolean r4 = com.tencent.feedback.common.g.a(r0)     // Catch:{ all -> 0x015d }
            if (r4 != 0) goto L_0x00eb
            r0.printStackTrace()     // Catch:{ all -> 0x015d }
        L_0x00eb:
            java.lang.String r4 = "rqdp{  Error strategy query!} %s"
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x015d }
            r6 = 0
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x015d }
            r5[r6] = r0     // Catch:{ all -> 0x015d }
            com.tencent.feedback.common.g.d(r4, r5)     // Catch:{ all -> 0x015d }
            if (r1 == 0) goto L_0x0105
            boolean r0 = r1.isClosed()
            if (r0 != 0) goto L_0x0105
            r1.close()
        L_0x0105:
            if (r2 == 0) goto L_0x0110
            boolean r0 = r2.isOpen()
            if (r0 == 0) goto L_0x0110
            r2.close()
        L_0x0110:
            if (r3 == 0) goto L_0x0115
            r3.close()
        L_0x0115:
            r0 = r8
            goto L_0x000c
        L_0x0118:
            if (r2 == 0) goto L_0x0123
            boolean r1 = r2.isClosed()
            if (r1 != 0) goto L_0x0123
            r2.close()
        L_0x0123:
            if (r0 == 0) goto L_0x012e
            boolean r1 = r0.isOpen()
            if (r1 == 0) goto L_0x012e
            r0.close()
        L_0x012e:
            r9.close()
            goto L_0x0115
        L_0x0132:
            r0 = move-exception
            r2 = r8
            r9 = r8
        L_0x0135:
            if (r2 == 0) goto L_0x0140
            boolean r1 = r2.isClosed()
            if (r1 != 0) goto L_0x0140
            r2.close()
        L_0x0140:
            if (r8 == 0) goto L_0x014b
            boolean r1 = r8.isOpen()
            if (r1 == 0) goto L_0x014b
            r8.close()
        L_0x014b:
            if (r9 == 0) goto L_0x0150
            r9.close()
        L_0x0150:
            throw r0
        L_0x0151:
            r0 = move-exception
            r2 = r8
            goto L_0x0135
        L_0x0154:
            r1 = move-exception
            r2 = r8
            r8 = r0
            r0 = r1
            goto L_0x0135
        L_0x0159:
            r1 = move-exception
            r8 = r0
            r0 = r1
            goto L_0x0135
        L_0x015d:
            r0 = move-exception
            r8 = r2
            r9 = r3
            r2 = r1
            goto L_0x0135
        L_0x0162:
            r0 = move-exception
            r1 = r8
            r2 = r8
            r3 = r8
            goto L_0x00e2
        L_0x0168:
            r0 = move-exception
            r1 = r8
            r2 = r8
            r3 = r9
            goto L_0x00e2
        L_0x016e:
            r1 = move-exception
            r2 = r0
            r3 = r9
            r0 = r1
            r1 = r8
            goto L_0x00e2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.proguard.ac.a(android.content.Context, int):com.tencent.feedback.proguard.ba");
    }

    public static byte[] a(byte[] bArr, int i, String str) {
        ab zVar;
        if (bArr == null || i == -1) {
            return bArr;
        }
        g.b("rqdp{  enD:} %d %d", Integer.valueOf(bArr.length), Integer.valueOf(i));
        if (i == 1) {
            try {
                zVar = new aa();
            } catch (Throwable th) {
                if (!g.a(th)) {
                    th.printStackTrace();
                }
                g.d("rqdp{  err enD: }%s", th.toString());
                return null;
            }
        } else {
            zVar = i == 3 ? new z() : null;
        }
        if (zVar == null) {
            return null;
        }
        zVar.a(str);
        return zVar.b(bArr);
    }

    public static byte[] b(byte[] bArr, int i, String str) {
        ab zVar;
        if (bArr == null || i == -1) {
            return bArr;
        }
        if (i == 1) {
            try {
                zVar = new aa();
            } catch (Throwable th) {
                if (!g.a(th)) {
                    th.printStackTrace();
                }
                g.d("rqdp{  err unD:} %s", th.toString());
                return null;
            }
        } else {
            zVar = i == 3 ? new z() : null;
        }
        if (zVar == null) {
            return null;
        }
        zVar.a(str);
        return zVar.a(bArr);
    }

    public static byte[] a(byte[] bArr, int i) {
        if (bArr == null || i == -1) {
            return bArr;
        }
        g.b("rqdp{  zp:} %s rqdp{  len:} %s", Integer.valueOf(i), Integer.valueOf(bArr.length));
        try {
            o a2 = m.a(i);
            if (a2 == null) {
                return null;
            }
            return a2.a(bArr);
        } catch (Throwable th) {
            if (!g.a(th)) {
                th.printStackTrace();
            }
            g.d("rqdp{  err zp :} %s", th.toString());
            return null;
        }
    }

    private void a(ArrayList<String> arrayList, Object obj) {
        if (obj.getClass().isArray()) {
            if (!obj.getClass().getComponentType().toString().equals("byte")) {
                throw new IllegalArgumentException("only byte[] is supported");
            } else if (Array.getLength(obj) > 0) {
                arrayList.add("java.util.List");
                a(arrayList, Array.get(obj, 0));
            } else {
                arrayList.add("Array");
                arrayList.add("?");
            }
        } else if (obj instanceof Array) {
            throw new IllegalArgumentException("can not support Array, please use List");
        } else if (obj instanceof List) {
            arrayList.add("java.util.List");
            List list = (List) obj;
            if (list.size() > 0) {
                a(arrayList, list.get(0));
            } else {
                arrayList.add("?");
            }
        } else if (obj instanceof Map) {
            arrayList.add("java.util.Map");
            Map map = (Map) obj;
            if (map.size() > 0) {
                Object next = map.keySet().iterator().next();
                Object obj2 = map.get(next);
                arrayList.add(next.getClass().getName());
                a(arrayList, obj2);
                return;
            }
            arrayList.add("?");
            arrayList.add("?");
        } else {
            arrayList.add(obj.getClass().getName());
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x007c A[Catch:{ all -> 0x00b3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x009b  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:49:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int b(android.content.Context r8, int r9) {
        /*
            r2 = 0
            r0 = 0
            if (r8 != 0) goto L_0x000c
            java.lang.String r1 = "rqdp{  context == null ||key < -1}"
            java.lang.Object[] r2 = new java.lang.Object[r0]
            com.tencent.feedback.common.g.c(r1, r2)
        L_0x000b:
            return r0
        L_0x000c:
            com.tencent.feedback.proguard.n r3 = new com.tencent.feedback.proguard.n     // Catch:{ Throwable -> 0x0074, all -> 0x00a0 }
            r3.<init>(r8)     // Catch:{ Throwable -> 0x0074, all -> 0x00a0 }
            android.database.sqlite.SQLiteDatabase r2 = r3.getWritableDatabase()     // Catch:{ Throwable -> 0x00b5 }
            if (r2 != 0) goto L_0x002e
            java.lang.String r1 = "get db fail!,return "
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x00b5 }
            com.tencent.feedback.common.g.d(r1, r4)     // Catch:{ Throwable -> 0x00b5 }
            if (r2 == 0) goto L_0x002a
            boolean r1 = r2.isOpen()
            if (r1 == 0) goto L_0x002a
            r2.close()
        L_0x002a:
            r3.close()
            goto L_0x000b
        L_0x002e:
            java.lang.String r1 = "%s = %d"
            r4 = 2
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x00b5 }
            r5 = 0
            java.lang.String r6 = "_key"
            r4[r5] = r6     // Catch:{ Throwable -> 0x00b5 }
            r5 = 1
            r6 = 302(0x12e, float:4.23E-43)
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Throwable -> 0x00b5 }
            r4[r5] = r6     // Catch:{ Throwable -> 0x00b5 }
            java.lang.String r1 = java.lang.String.format(r1, r4)     // Catch:{ Throwable -> 0x00b5 }
            java.lang.String r4 = "strategy"
            r5 = 0
            int r1 = r2.delete(r4, r1, r5)     // Catch:{ Throwable -> 0x00b5 }
            java.lang.String r4 = "rqdp{  delete Strategy key} %d rqdp{  , num} %d"
            r5 = 2
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Throwable -> 0x00b5 }
            r6 = 0
            r7 = 302(0x12e, float:4.23E-43)
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ Throwable -> 0x00b5 }
            r5[r6] = r7     // Catch:{ Throwable -> 0x00b5 }
            r6 = 1
            java.lang.Integer r7 = java.lang.Integer.valueOf(r1)     // Catch:{ Throwable -> 0x00b5 }
            r5[r6] = r7     // Catch:{ Throwable -> 0x00b5 }
            com.tencent.feedback.common.g.b(r4, r5)     // Catch:{ Throwable -> 0x00b5 }
            if (r2 == 0) goto L_0x006f
            boolean r0 = r2.isOpen()
            if (r0 == 0) goto L_0x006f
            r2.close()
        L_0x006f:
            r3.close()
            r0 = r1
            goto L_0x000b
        L_0x0074:
            r1 = move-exception
            r3 = r2
        L_0x0076:
            boolean r4 = com.tencent.feedback.common.g.a(r1)     // Catch:{ all -> 0x00b3 }
            if (r4 != 0) goto L_0x007f
            r1.printStackTrace()     // Catch:{ all -> 0x00b3 }
        L_0x007f:
            java.lang.String r4 = "rqdp{  Error strategy delete!} %s"
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x00b3 }
            r6 = 0
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00b3 }
            r5[r6] = r1     // Catch:{ all -> 0x00b3 }
            com.tencent.feedback.common.g.d(r4, r5)     // Catch:{ all -> 0x00b3 }
            if (r2 == 0) goto L_0x0099
            boolean r1 = r2.isOpen()
            if (r1 == 0) goto L_0x0099
            r2.close()
        L_0x0099:
            if (r3 == 0) goto L_0x000b
            r3.close()
            goto L_0x000b
        L_0x00a0:
            r0 = move-exception
            r3 = r2
        L_0x00a2:
            if (r2 == 0) goto L_0x00ad
            boolean r1 = r2.isOpen()
            if (r1 == 0) goto L_0x00ad
            r2.close()
        L_0x00ad:
            if (r3 == 0) goto L_0x00b2
            r3.close()
        L_0x00b2:
            throw r0
        L_0x00b3:
            r0 = move-exception
            goto L_0x00a2
        L_0x00b5:
            r1 = move-exception
            goto L_0x0076
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.proguard.ac.b(android.content.Context, int):int");
    }

    public static byte[] b(byte[] bArr, int i) {
        if (bArr == null || i == -1) {
            return bArr;
        }
        g.b("rqdp{  unzp:} %s rqdp{  len:} %s", Integer.valueOf(i), Integer.valueOf(bArr.length));
        try {
            o a2 = m.a(i);
            if (a2 == null) {
                return null;
            }
            return a2.b(bArr);
        } catch (Throwable th) {
            if (!g.a(th)) {
                th.printStackTrace();
            }
            g.d("rqdp{  err unzp}" + th.toString(), new Object[0]);
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ah.a(java.util.Map, int):void
     arg types: [java.util.HashMap<java.lang.String, java.util.HashMap<java.lang.String, byte[]>>, int]
     candidates:
      com.tencent.feedback.proguard.ah.a(byte, int):void
      com.tencent.feedback.proguard.ah.a(float, int):void
      com.tencent.feedback.proguard.ah.a(int, int):void
      com.tencent.feedback.proguard.ah.a(long, int):void
      com.tencent.feedback.proguard.ah.a(com.tencent.feedback.proguard.j, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.Object, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.String, int):void
      com.tencent.feedback.proguard.ah.a(java.util.Collection, int):void
      com.tencent.feedback.proguard.ah.a(short, int):void
      com.tencent.feedback.proguard.ah.a(boolean, int):void
      com.tencent.feedback.proguard.ah.a(byte[], int):void
      com.tencent.feedback.proguard.ah.a(java.util.Map, int):void */
    public byte[] a() {
        ah ahVar = new ah(0);
        ahVar.a(this.b);
        ahVar.a((Map) this.f2583a, 0);
        return ai.a(ahVar.a());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
     arg types: [java.util.HashMap, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V> */
    public void a(byte[] bArr) {
        this.c.a(bArr);
        this.c.a(this.b);
        HashMap hashMap = new HashMap(1);
        HashMap hashMap2 = new HashMap(1);
        hashMap2.put(Constants.STR_EMPTY, new byte[0]);
        hashMap.put(Constants.STR_EMPTY, hashMap2);
        this.f2583a = this.c.a((Map) hashMap, 0, false);
    }

    public static C a(int i, e eVar, byte[] bArr, int i2, int i3) {
        g.b("rqdp{  en2Req }", new Object[0]);
        if (eVar == null) {
            g.d("rqdp{  error no com info!}", new Object[0]);
            return null;
        }
        try {
            C c2 = new C();
            synchronized (eVar) {
                c2.h = eVar.a();
                c2.f2566a = eVar.b();
                c2.b = eVar.o();
                c2.c = eVar.B();
                c2.d = eVar.d();
                c2.e = eVar.e();
                c2.i = eVar.h();
                c2.l = eVar.l().trim().length() > 0 ? eVar.l() : eVar.m();
                c2.m = Constants.STR_EMPTY;
                c2.n = eVar.c();
                c2.o = Constants.STR_EMPTY;
            }
            c2.f = i;
            c2.j = (byte) i3;
            c2.k = (byte) i2;
            if (bArr == null) {
                bArr = Constants.STR_EMPTY.getBytes();
            }
            c2.g = bArr;
            return c2;
        } catch (Throwable th) {
            if (g.a(th)) {
                return null;
            }
            th.printStackTrace();
            return null;
        }
    }

    public static byte[] a(byte[] bArr, int i, int i2, String str) {
        if (bArr == null) {
            return null;
        }
        try {
            return a(a(bArr, i), i2, str);
        } catch (Throwable th) {
            if (g.a(th)) {
                return null;
            }
            th.printStackTrace();
            return null;
        }
    }

    public static byte[] b(byte[] bArr, int i, int i2, String str) {
        try {
            return b(b(bArr, i2, str), i);
        } catch (Exception e) {
            if (!g.a(e)) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public static long c() {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            return simpleDateFormat.parse(simpleDateFormat.format(new Date())).getTime();
        } catch (Throwable th) {
            if (!g.a(th)) {
                th.printStackTrace();
            }
            return -1;
        }
    }

    private static String d(byte[] bArr) {
        if (bArr == null) {
            return Constants.STR_EMPTY;
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (byte b2 : bArr) {
            String hexString = Integer.toHexString(b2 & 255);
            if (hexString.length() == 1) {
                stringBuffer.append("0");
            }
            stringBuffer.append(hexString);
        }
        return stringBuffer.toString().toUpperCase();
    }

    public static String c(byte[] bArr) {
        if (bArr == null || bArr.length == 0) {
            return "NULL";
        }
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-1");
            instance.update(bArr);
            return d(instance.digest());
        } catch (NoSuchAlgorithmException e) {
            if (!g.a(e)) {
                e.printStackTrace();
            }
            g.d(e.getMessage(), new Object[0]);
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:43:0x007a A[Catch:{ all -> 0x00ae }] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0089 A[SYNTHETIC, Splitter:B:46:0x0089] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x009f A[SYNTHETIC, Splitter:B:55:0x009f] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:40:0x0074=Splitter:B:40:0x0074, B:18:0x0037=Splitter:B:18:0x0037} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(java.lang.String r6) {
        /*
            r0 = 0
            if (r6 == 0) goto L_0x0009
            int r1 = r6.length()
            if (r1 != 0) goto L_0x000a
        L_0x0009:
            return r0
        L_0x000a:
            java.io.File r1 = new java.io.File
            r1.<init>(r6)
            boolean r2 = r1.exists()
            if (r2 == 0) goto L_0x0009
            boolean r2 = r1.canRead()
            if (r2 == 0) goto L_0x0009
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ IOException -> 0x00b2, NoSuchAlgorithmException -> 0x0072, all -> 0x009a }
            r2.<init>(r1)     // Catch:{ IOException -> 0x00b2, NoSuchAlgorithmException -> 0x0072, all -> 0x009a }
            java.lang.String r1 = "SHA-1"
            java.security.MessageDigest r1 = java.security.MessageDigest.getInstance(r1)     // Catch:{ IOException -> 0x0036, NoSuchAlgorithmException -> 0x00b0 }
            r3 = 4096(0x1000, float:5.74E-42)
            byte[] r3 = new byte[r3]     // Catch:{ IOException -> 0x0036, NoSuchAlgorithmException -> 0x00b0 }
        L_0x002a:
            int r4 = r2.read(r3)     // Catch:{ IOException -> 0x0036, NoSuchAlgorithmException -> 0x00b0 }
            r5 = -1
            if (r4 == r5) goto L_0x005b
            r5 = 0
            r1.update(r3, r5, r4)     // Catch:{ IOException -> 0x0036, NoSuchAlgorithmException -> 0x00b0 }
            goto L_0x002a
        L_0x0036:
            r1 = move-exception
        L_0x0037:
            boolean r3 = com.tencent.feedback.common.g.a(r1)     // Catch:{ all -> 0x00ae }
            if (r3 != 0) goto L_0x0040
            r1.printStackTrace()     // Catch:{ all -> 0x00ae }
        L_0x0040:
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x00ae }
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x00ae }
            com.tencent.feedback.common.g.d(r1, r3)     // Catch:{ all -> 0x00ae }
            if (r2 == 0) goto L_0x0009
            r2.close()     // Catch:{ IOException -> 0x0050 }
            goto L_0x0009
        L_0x0050:
            r1 = move-exception
            boolean r2 = com.tencent.feedback.common.g.a(r1)
            if (r2 != 0) goto L_0x0009
            r1.printStackTrace()
            goto L_0x0009
        L_0x005b:
            byte[] r1 = r1.digest()     // Catch:{ IOException -> 0x0036, NoSuchAlgorithmException -> 0x00b0 }
            java.lang.String r0 = d(r1)     // Catch:{ IOException -> 0x0036, NoSuchAlgorithmException -> 0x00b0 }
            r2.close()     // Catch:{ IOException -> 0x0067 }
            goto L_0x0009
        L_0x0067:
            r1 = move-exception
            boolean r2 = com.tencent.feedback.common.g.a(r1)
            if (r2 != 0) goto L_0x0009
            r1.printStackTrace()
            goto L_0x0009
        L_0x0072:
            r1 = move-exception
            r2 = r0
        L_0x0074:
            boolean r3 = com.tencent.feedback.common.g.a(r1)     // Catch:{ all -> 0x00ae }
            if (r3 != 0) goto L_0x007d
            r1.printStackTrace()     // Catch:{ all -> 0x00ae }
        L_0x007d:
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x00ae }
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x00ae }
            com.tencent.feedback.common.g.d(r1, r3)     // Catch:{ all -> 0x00ae }
            if (r2 == 0) goto L_0x0009
            r2.close()     // Catch:{ IOException -> 0x008e }
            goto L_0x0009
        L_0x008e:
            r1 = move-exception
            boolean r2 = com.tencent.feedback.common.g.a(r1)
            if (r2 != 0) goto L_0x0009
            r1.printStackTrace()
            goto L_0x0009
        L_0x009a:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x009d:
            if (r2 == 0) goto L_0x00a2
            r2.close()     // Catch:{ IOException -> 0x00a3 }
        L_0x00a2:
            throw r0
        L_0x00a3:
            r1 = move-exception
            boolean r2 = com.tencent.feedback.common.g.a(r1)
            if (r2 != 0) goto L_0x00a2
            r1.printStackTrace()
            goto L_0x00a2
        L_0x00ae:
            r0 = move-exception
            goto L_0x009d
        L_0x00b0:
            r1 = move-exception
            goto L_0x0074
        L_0x00b2:
            r1 = move-exception
            r2 = r0
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.proguard.ac.a(java.lang.String):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x0092 A[Catch:{ all -> 0x0126 }] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0097 A[SYNTHETIC, Splitter:B:41:0x0097] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x009c A[SYNTHETIC, Splitter:B:44:0x009c] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x00fb A[SYNTHETIC, Splitter:B:77:0x00fb] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x0100 A[SYNTHETIC, Splitter:B:80:0x0100] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(java.io.File r6, java.io.File r7, int r8) {
        /*
            r3 = 0
            r0 = 0
            java.lang.String r1 = "rqdp{  ZF start}"
            java.lang.Object[] r2 = new java.lang.Object[r0]
            com.tencent.feedback.common.g.b(r1, r2)
            if (r6 == 0) goto L_0x0013
            if (r7 == 0) goto L_0x0013
            boolean r1 = r6.equals(r7)
            if (r1 == 0) goto L_0x001b
        L_0x0013:
            java.lang.String r1 = "rqdp{  err ZF 1R!}"
            java.lang.Object[] r2 = new java.lang.Object[r0]
            com.tencent.feedback.common.g.c(r1, r2)
        L_0x001a:
            return r0
        L_0x001b:
            boolean r1 = r6.exists()
            if (r1 == 0) goto L_0x0027
            boolean r1 = r6.canRead()
            if (r1 != 0) goto L_0x002f
        L_0x0027:
            java.lang.String r1 = "rqdp{  !sFile.exists() || !sFile.canRead(),pls check ,return!}"
            java.lang.Object[] r2 = new java.lang.Object[r0]
            com.tencent.feedback.common.g.c(r1, r2)
            goto L_0x001a
        L_0x002f:
            java.io.File r1 = r7.getParentFile()     // Catch:{ Throwable -> 0x00a8 }
            if (r1 == 0) goto L_0x0046
            java.io.File r1 = r7.getParentFile()     // Catch:{ Throwable -> 0x00a8 }
            boolean r1 = r1.exists()     // Catch:{ Throwable -> 0x00a8 }
            if (r1 != 0) goto L_0x0046
            java.io.File r1 = r7.getParentFile()     // Catch:{ Throwable -> 0x00a8 }
            r1.mkdirs()     // Catch:{ Throwable -> 0x00a8 }
        L_0x0046:
            boolean r1 = r7.exists()     // Catch:{ Throwable -> 0x00a8 }
            if (r1 != 0) goto L_0x004f
            r7.createNewFile()     // Catch:{ Throwable -> 0x00a8 }
        L_0x004f:
            boolean r1 = r7.exists()
            if (r1 == 0) goto L_0x001a
            boolean r1 = r7.canRead()
            if (r1 == 0) goto L_0x001a
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ Throwable -> 0x0129, all -> 0x00f6 }
            r4.<init>(r6)     // Catch:{ Throwable -> 0x0129, all -> 0x00f6 }
            java.util.zip.ZipOutputStream r2 = new java.util.zip.ZipOutputStream     // Catch:{ Throwable -> 0x012d, all -> 0x0121 }
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Throwable -> 0x012d, all -> 0x0121 }
            r1.<init>(r7)     // Catch:{ Throwable -> 0x012d, all -> 0x0121 }
            r2.<init>(r1)     // Catch:{ Throwable -> 0x012d, all -> 0x0121 }
            r1 = 8
            r2.setMethod(r1)     // Catch:{ Throwable -> 0x008a, all -> 0x0124 }
            java.util.zip.ZipEntry r1 = new java.util.zip.ZipEntry     // Catch:{ Throwable -> 0x008a, all -> 0x0124 }
            java.lang.String r3 = r6.getName()     // Catch:{ Throwable -> 0x008a, all -> 0x0124 }
            r1.<init>(r3)     // Catch:{ Throwable -> 0x008a, all -> 0x0124 }
            r2.putNextEntry(r1)     // Catch:{ Throwable -> 0x008a, all -> 0x0124 }
            r1 = 5000(0x1388, float:7.006E-42)
            byte[] r1 = new byte[r1]     // Catch:{ Throwable -> 0x008a, all -> 0x0124 }
        L_0x007f:
            int r3 = r4.read(r1)     // Catch:{ Throwable -> 0x008a, all -> 0x0124 }
            if (r3 <= 0) goto L_0x00b4
            r5 = 0
            r2.write(r1, r5, r3)     // Catch:{ Throwable -> 0x008a, all -> 0x0124 }
            goto L_0x007f
        L_0x008a:
            r1 = move-exception
            r3 = r4
        L_0x008c:
            boolean r4 = com.tencent.feedback.common.g.a(r1)     // Catch:{ all -> 0x0126 }
            if (r4 != 0) goto L_0x0095
            r1.printStackTrace()     // Catch:{ all -> 0x0126 }
        L_0x0095:
            if (r3 == 0) goto L_0x009a
            r3.close()     // Catch:{ IOException -> 0x00e0 }
        L_0x009a:
            if (r2 == 0) goto L_0x009f
            r2.close()     // Catch:{ IOException -> 0x00eb }
        L_0x009f:
            java.lang.String r1 = "rqdp{  ZF end}"
            java.lang.Object[] r2 = new java.lang.Object[r0]
            com.tencent.feedback.common.g.b(r1, r2)
            goto L_0x001a
        L_0x00a8:
            r1 = move-exception
            boolean r2 = com.tencent.feedback.common.g.a(r1)
            if (r2 != 0) goto L_0x001a
            r1.printStackTrace()
            goto L_0x001a
        L_0x00b4:
            r2.flush()     // Catch:{ Throwable -> 0x008a, all -> 0x0124 }
            r2.closeEntry()     // Catch:{ Throwable -> 0x008a, all -> 0x0124 }
            r4.close()     // Catch:{ IOException -> 0x00ca }
        L_0x00bd:
            r2.close()     // Catch:{ IOException -> 0x00d5 }
        L_0x00c0:
            java.lang.String r1 = "rqdp{  ZF end}"
            java.lang.Object[] r0 = new java.lang.Object[r0]
            com.tencent.feedback.common.g.b(r1, r0)
            r0 = 1
            goto L_0x001a
        L_0x00ca:
            r1 = move-exception
            boolean r3 = com.tencent.feedback.common.g.a(r1)
            if (r3 != 0) goto L_0x00bd
            r1.printStackTrace()
            goto L_0x00bd
        L_0x00d5:
            r1 = move-exception
            boolean r2 = com.tencent.feedback.common.g.a(r1)
            if (r2 != 0) goto L_0x00c0
            r1.printStackTrace()
            goto L_0x00c0
        L_0x00e0:
            r1 = move-exception
            boolean r3 = com.tencent.feedback.common.g.a(r1)
            if (r3 != 0) goto L_0x009a
            r1.printStackTrace()
            goto L_0x009a
        L_0x00eb:
            r1 = move-exception
            boolean r2 = com.tencent.feedback.common.g.a(r1)
            if (r2 != 0) goto L_0x009f
            r1.printStackTrace()
            goto L_0x009f
        L_0x00f6:
            r1 = move-exception
            r2 = r3
            r4 = r3
        L_0x00f9:
            if (r4 == 0) goto L_0x00fe
            r4.close()     // Catch:{ IOException -> 0x010b }
        L_0x00fe:
            if (r2 == 0) goto L_0x0103
            r2.close()     // Catch:{ IOException -> 0x0116 }
        L_0x0103:
            java.lang.String r2 = "rqdp{  ZF end}"
            java.lang.Object[] r0 = new java.lang.Object[r0]
            com.tencent.feedback.common.g.b(r2, r0)
            throw r1
        L_0x010b:
            r3 = move-exception
            boolean r4 = com.tencent.feedback.common.g.a(r3)
            if (r4 != 0) goto L_0x00fe
            r3.printStackTrace()
            goto L_0x00fe
        L_0x0116:
            r2 = move-exception
            boolean r3 = com.tencent.feedback.common.g.a(r2)
            if (r3 != 0) goto L_0x0103
            r2.printStackTrace()
            goto L_0x0103
        L_0x0121:
            r1 = move-exception
            r2 = r3
            goto L_0x00f9
        L_0x0124:
            r1 = move-exception
            goto L_0x00f9
        L_0x0126:
            r1 = move-exception
            r4 = r3
            goto L_0x00f9
        L_0x0129:
            r1 = move-exception
            r2 = r3
            goto L_0x008c
        L_0x012d:
            r1 = move-exception
            r2 = r3
            r3 = r4
            goto L_0x008c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.proguard.ac.a(java.io.File, java.io.File, int):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x002e A[Catch:{ all -> 0x00af }] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0033 A[SYNTHETIC, Splitter:B:14:0x0033] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0038 A[SYNTHETIC, Splitter:B:17:0x0038] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x008e A[SYNTHETIC, Splitter:B:50:0x008e] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0093 A[SYNTHETIC, Splitter:B:53:0x0093] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.ArrayList<java.lang.String> a(java.lang.String[] r6) {
        /*
            r1 = 0
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            java.lang.Runtime r2 = java.lang.Runtime.getRuntime()     // Catch:{ Throwable -> 0x00b2, all -> 0x008a }
            java.lang.Process r4 = r2.exec(r6)     // Catch:{ Throwable -> 0x00b2, all -> 0x008a }
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ Throwable -> 0x00b2, all -> 0x008a }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ Throwable -> 0x00b2, all -> 0x008a }
            java.io.InputStream r5 = r4.getInputStream()     // Catch:{ Throwable -> 0x00b2, all -> 0x008a }
            r2.<init>(r5)     // Catch:{ Throwable -> 0x00b2, all -> 0x008a }
            r3.<init>(r2)     // Catch:{ Throwable -> 0x00b2, all -> 0x008a }
        L_0x001c:
            java.lang.String r2 = r3.readLine()     // Catch:{ Throwable -> 0x0026, all -> 0x00ad }
            if (r2 == 0) goto L_0x003d
            r0.add(r2)     // Catch:{ Throwable -> 0x0026, all -> 0x00ad }
            goto L_0x001c
        L_0x0026:
            r0 = move-exception
            r2 = r1
        L_0x0028:
            boolean r4 = com.tencent.feedback.common.g.a(r0)     // Catch:{ all -> 0x00af }
            if (r4 != 0) goto L_0x0031
            r0.printStackTrace()     // Catch:{ all -> 0x00af }
        L_0x0031:
            if (r3 == 0) goto L_0x0036
            r3.close()     // Catch:{ IOException -> 0x0074 }
        L_0x0036:
            if (r2 == 0) goto L_0x003b
            r2.close()     // Catch:{ IOException -> 0x007f }
        L_0x003b:
            r0 = r1
        L_0x003c:
            return r0
        L_0x003d:
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ Throwable -> 0x0026, all -> 0x00ad }
            java.io.InputStreamReader r5 = new java.io.InputStreamReader     // Catch:{ Throwable -> 0x0026, all -> 0x00ad }
            java.io.InputStream r4 = r4.getErrorStream()     // Catch:{ Throwable -> 0x0026, all -> 0x00ad }
            r5.<init>(r4)     // Catch:{ Throwable -> 0x0026, all -> 0x00ad }
            r2.<init>(r5)     // Catch:{ Throwable -> 0x0026, all -> 0x00ad }
        L_0x004b:
            java.lang.String r4 = r2.readLine()     // Catch:{ Throwable -> 0x0055 }
            if (r4 == 0) goto L_0x0057
            r0.add(r4)     // Catch:{ Throwable -> 0x0055 }
            goto L_0x004b
        L_0x0055:
            r0 = move-exception
            goto L_0x0028
        L_0x0057:
            r3.close()     // Catch:{ IOException -> 0x0069 }
        L_0x005a:
            r2.close()     // Catch:{ IOException -> 0x005e }
            goto L_0x003c
        L_0x005e:
            r1 = move-exception
            boolean r2 = com.tencent.feedback.common.g.a(r1)
            if (r2 != 0) goto L_0x003c
            r1.printStackTrace()
            goto L_0x003c
        L_0x0069:
            r1 = move-exception
            boolean r3 = com.tencent.feedback.common.g.a(r1)
            if (r3 != 0) goto L_0x005a
            r1.printStackTrace()
            goto L_0x005a
        L_0x0074:
            r0 = move-exception
            boolean r3 = com.tencent.feedback.common.g.a(r0)
            if (r3 != 0) goto L_0x0036
            r0.printStackTrace()
            goto L_0x0036
        L_0x007f:
            r0 = move-exception
            boolean r2 = com.tencent.feedback.common.g.a(r0)
            if (r2 != 0) goto L_0x003b
            r0.printStackTrace()
            goto L_0x003b
        L_0x008a:
            r0 = move-exception
            r3 = r1
        L_0x008c:
            if (r3 == 0) goto L_0x0091
            r3.close()     // Catch:{ IOException -> 0x0097 }
        L_0x0091:
            if (r1 == 0) goto L_0x0096
            r1.close()     // Catch:{ IOException -> 0x00a2 }
        L_0x0096:
            throw r0
        L_0x0097:
            r2 = move-exception
            boolean r3 = com.tencent.feedback.common.g.a(r2)
            if (r3 != 0) goto L_0x0091
            r2.printStackTrace()
            goto L_0x0091
        L_0x00a2:
            r1 = move-exception
            boolean r2 = com.tencent.feedback.common.g.a(r1)
            if (r2 != 0) goto L_0x0096
            r1.printStackTrace()
            goto L_0x0096
        L_0x00ad:
            r0 = move-exception
            goto L_0x008c
        L_0x00af:
            r0 = move-exception
            r1 = r2
            goto L_0x008c
        L_0x00b2:
            r0 = move-exception
            r2 = r1
            r3 = r1
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.feedback.proguard.ac.a(java.lang.String[]):java.util.ArrayList");
    }

    public static String b(String str) {
        if (str == null || str.trim().equals(Constants.STR_EMPTY)) {
            return Constants.STR_EMPTY;
        }
        ArrayList<String> a2 = a(new String[]{"/system/bin/sh", "-c", "getprop " + str});
        if (a2 == null || a2.size() <= 0) {
            return "fail";
        }
        return a2.get(0);
    }

    public static String d() {
        try {
            return UUID.randomUUID().toString();
        } catch (Throwable th) {
            if (!g.a(th)) {
                th.printStackTrace();
            }
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    public static void a(String str, String str2, int i) {
        FileOutputStream fileOutputStream;
        FileOutputStream fileOutputStream2;
        g.b("rqdp{  sv sd start} %s", str);
        if (str2 != null && str2.trim().length() > 0) {
            File file = new File(str);
            try {
                if (!file.exists()) {
                    if (file.getParentFile() != null) {
                        file.getParentFile().mkdirs();
                    }
                    file.createNewFile();
                }
                fileOutputStream = null;
                if (file.length() >= ((long) i)) {
                    fileOutputStream2 = new FileOutputStream(file, false);
                } else {
                    fileOutputStream2 = new FileOutputStream(file, true);
                }
                fileOutputStream2.write(str2.getBytes("UTF-8"));
                fileOutputStream2.flush();
                fileOutputStream2.close();
            } catch (Throwable th) {
                if (!g.a(th)) {
                    th.printStackTrace();
                }
            }
            g.b("rqdp{  sv sd end}", new Object[0]);
        }
    }

    public static String a(Throwable th) {
        if (th == null) {
            return Constants.STR_EMPTY;
        }
        try {
            StringWriter stringWriter = new StringWriter();
            th.printStackTrace(new PrintWriter(stringWriter));
            return stringWriter.getBuffer().toString();
        } catch (Throwable th2) {
            if (!g.a(th2) && !g.a(th2)) {
                th2.printStackTrace();
            }
            return "fail";
        }
    }
}
