package com.tencent.feedback.proguard;

import com.qq.taf.jce.JceStruct;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
public final class ah {

    /* renamed from: a  reason: collision with root package name */
    private ByteBuffer f2586a;
    private String b;

    public ah(int i) {
        this.b = "GBK";
        this.f2586a = ByteBuffer.allocate(i);
    }

    public ah() {
        this(128);
    }

    public final ByteBuffer a() {
        return this.f2586a;
    }

    public final byte[] b() {
        byte[] bArr = new byte[this.f2586a.position()];
        System.arraycopy(this.f2586a.array(), 0, bArr, 0, this.f2586a.position());
        return bArr;
    }

    private void a(int i) {
        if (this.f2586a.remaining() < i) {
            ByteBuffer allocate = ByteBuffer.allocate((this.f2586a.capacity() + i) << 1);
            allocate.put(this.f2586a.array(), 0, this.f2586a.position());
            this.f2586a = allocate;
        }
    }

    private void b(byte b2, int i) {
        if (i < 15) {
            this.f2586a.put((byte) ((i << 4) | b2));
        } else if (i < 256) {
            this.f2586a.put((byte) (b2 | 240));
            this.f2586a.put((byte) i);
        } else {
            throw new C0005b("tag is too large: " + i);
        }
    }

    public final void a(boolean z, int i) {
        a((byte) (z ? 1 : 0), i);
    }

    public final void a(byte b2, int i) {
        a(3);
        if (b2 == 0) {
            b(JceStruct.ZERO_TAG, i);
            return;
        }
        b((byte) 0, i);
        this.f2586a.put(b2);
    }

    public final void a(short s, int i) {
        a(4);
        if (s < -128 || s > 127) {
            b((byte) 1, i);
            this.f2586a.putShort(s);
            return;
        }
        a((byte) s, i);
    }

    public final void a(int i, int i2) {
        a(6);
        if (i < -32768 || i > 32767) {
            b((byte) 2, i2);
            this.f2586a.putInt(i);
            return;
        }
        a((short) i, i2);
    }

    public final void a(long j, int i) {
        a(10);
        if (j < -2147483648L || j > 2147483647L) {
            b((byte) 3, i);
            this.f2586a.putLong(j);
            return;
        }
        a((int) j, i);
    }

    public final void a(float f, int i) {
        a(6);
        b((byte) 4, i);
        this.f2586a.putFloat(f);
    }

    public final void a(String str, int i) {
        byte[] bytes;
        try {
            bytes = str.getBytes(this.b);
        } catch (UnsupportedEncodingException e) {
            bytes = str.getBytes();
        }
        a(bytes.length + 10);
        if (bytes.length > 255) {
            b((byte) 7, i);
            this.f2586a.putInt(bytes.length);
            this.f2586a.put(bytes);
            return;
        }
        b((byte) 6, i);
        this.f2586a.put((byte) bytes.length);
        this.f2586a.put(bytes);
    }

    public final <K, V> void a(Map map, int i) {
        a(8);
        b((byte) 8, i);
        a(map == null ? 0 : map.size(), 0);
        if (map != null) {
            for (Map.Entry entry : map.entrySet()) {
                a(entry.getKey(), 0);
                a(entry.getValue(), 1);
            }
        }
    }

    public final void a(byte[] bArr, int i) {
        a(bArr.length + 8);
        b(JceStruct.SIMPLE_LIST, i);
        b((byte) 0, 0);
        a(bArr.length, 0);
        this.f2586a.put(bArr);
    }

    public final <T> void a(Collection collection, int i) {
        a(8);
        b((byte) 9, i);
        a(collection == null ? 0 : collection.size(), 0);
        if (collection != null) {
            for (Object a2 : collection) {
                a(a2, 0);
            }
        }
    }

    public final void a(C0008j jVar, int i) {
        a(2);
        b((byte) 10, i);
        jVar.a(this);
        a(2);
        b(JceStruct.STRUCT_END, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ah.a(java.util.Collection, int):void
     arg types: [java.util.List, int]
     candidates:
      com.tencent.feedback.proguard.ah.a(byte, int):void
      com.tencent.feedback.proguard.ah.a(float, int):void
      com.tencent.feedback.proguard.ah.a(int, int):void
      com.tencent.feedback.proguard.ah.a(long, int):void
      com.tencent.feedback.proguard.ah.a(com.tencent.feedback.proguard.j, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.Object, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.String, int):void
      com.tencent.feedback.proguard.ah.a(java.util.Map, int):void
      com.tencent.feedback.proguard.ah.a(short, int):void
      com.tencent.feedback.proguard.ah.a(boolean, int):void
      com.tencent.feedback.proguard.ah.a(byte[], int):void
      com.tencent.feedback.proguard.ah.a(java.util.Collection, int):void */
    public final void a(Object obj, int i) {
        int i2 = 1;
        if (obj instanceof Byte) {
            a(((Byte) obj).byteValue(), i);
        } else if (obj instanceof Boolean) {
            if (!((Boolean) obj).booleanValue()) {
                i2 = 0;
            }
            a((byte) i2, i);
        } else if (obj instanceof Short) {
            a(((Short) obj).shortValue(), i);
        } else if (obj instanceof Integer) {
            a(((Integer) obj).intValue(), i);
        } else if (obj instanceof Long) {
            a(((Long) obj).longValue(), i);
        } else if (obj instanceof Float) {
            float floatValue = ((Float) obj).floatValue();
            a(6);
            b((byte) 4, i);
            this.f2586a.putFloat(floatValue);
        } else if (obj instanceof Double) {
            double doubleValue = ((Double) obj).doubleValue();
            a(10);
            b((byte) 5, i);
            this.f2586a.putDouble(doubleValue);
        } else if (obj instanceof String) {
            a((String) obj, i);
        } else if (obj instanceof Map) {
            a((Map) obj, i);
        } else if (obj instanceof List) {
            a((Collection) ((List) obj), i);
        } else if (obj instanceof C0008j) {
            a(2);
            b((byte) 10, i);
            ((C0008j) obj).a(this);
            a(2);
            b(JceStruct.STRUCT_END, 0);
        } else if (obj instanceof byte[]) {
            a((byte[]) obj, i);
        } else if (obj instanceof boolean[]) {
            boolean[] zArr = (boolean[]) obj;
            a(8);
            b((byte) 9, i);
            a(zArr.length, 0);
            int length = zArr.length;
            for (int i3 = 0; i3 < length; i3++) {
                a((byte) (zArr[i3] ? 1 : 0), 0);
            }
        } else if (obj instanceof short[]) {
            short[] sArr = (short[]) obj;
            a(8);
            b((byte) 9, i);
            a(sArr.length, 0);
            for (short a2 : sArr) {
                a(a2, 0);
            }
        } else if (obj instanceof int[]) {
            int[] iArr = (int[]) obj;
            a(8);
            b((byte) 9, i);
            a(iArr.length, 0);
            for (int a3 : iArr) {
                a(a3, 0);
            }
        } else if (obj instanceof long[]) {
            long[] jArr = (long[]) obj;
            a(8);
            b((byte) 9, i);
            a(jArr.length, 0);
            for (long a4 : jArr) {
                a(a4, 0);
            }
        } else if (obj instanceof float[]) {
            float[] fArr = (float[]) obj;
            a(8);
            b((byte) 9, i);
            a(fArr.length, 0);
            for (float putFloat : fArr) {
                a(6);
                b((byte) 4, 0);
                this.f2586a.putFloat(putFloat);
            }
        } else if (obj instanceof double[]) {
            double[] dArr = (double[]) obj;
            a(8);
            b((byte) 9, i);
            a(dArr.length, 0);
            for (double putDouble : dArr) {
                a(10);
                b((byte) 5, 0);
                this.f2586a.putDouble(putDouble);
            }
        } else if (obj.getClass().isArray()) {
            Object[] objArr = (Object[]) obj;
            a(8);
            b((byte) 9, i);
            a(objArr.length, 0);
            for (Object a5 : objArr) {
                a(a5, 0);
            }
        } else if (obj instanceof Collection) {
            a((Collection) obj, i);
        } else {
            throw new C0005b("write object error: unsupport type. " + obj.getClass());
        }
    }

    public final int a(String str) {
        this.b = str;
        return 0;
    }
}
