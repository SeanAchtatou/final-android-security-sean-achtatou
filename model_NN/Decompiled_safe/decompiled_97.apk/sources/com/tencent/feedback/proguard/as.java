package com.tencent.feedback.proguard;

import android.content.Context;
import com.tencent.assistant.event.EventDispatcherEnum;
import com.tencent.feedback.b.a;
import com.tencent.feedback.common.g;

/* compiled from: ProGuard */
public final class as extends a {
    private B d = null;

    public as(Context context, int i, int i2, B b) {
        super(context, EventDispatcherEnum.UI_EVENT_PLUGIN_DOWNLIST_FAIL, 200);
        this.d = b;
    }

    public final void a(boolean z) {
    }

    public final C a() {
        try {
            return a(this.c, this.f2535a, this.d == null ? null : this.d.a());
        } catch (Exception e) {
            if (g.a(e)) {
                return null;
            }
            e.printStackTrace();
            return null;
        }
    }
}
