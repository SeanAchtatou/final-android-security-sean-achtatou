package com.tencent.feedback.proguard;

import com.tencent.connect.common.Constants;
import com.tencent.feedback.eup.g;
import java.util.HashMap;
import java.util.Map;

/* renamed from: com.tencent.feedback.proguard.f  reason: case insensitive filesystem */
/* compiled from: ProGuard */
public final class C0006f extends C0008j {
    private static byte[] k = null;
    private static Map<String, String> l = null;
    private static /* synthetic */ boolean m;

    /* renamed from: a  reason: collision with root package name */
    public short f2602a = 0;
    public int b = 0;
    public String c = null;
    public String d = null;
    public byte[] e;
    private byte f = 0;
    private int g = 0;
    private int h = 0;
    private Map<String, String> i;
    private Map<String, String> j;

    static {
        boolean z;
        if (!C0006f.class.desiredAssertionStatus()) {
            z = true;
        } else {
            z = false;
        }
        m = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ai.a(java.lang.Object, java.lang.Object):boolean
     arg types: [int, java.lang.String]
     candidates:
      com.tencent.feedback.proguard.ai.a(int, int):boolean
      com.tencent.feedback.proguard.ai.a(java.lang.Object, java.lang.Object):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ai.a(java.lang.Object, java.lang.Object):boolean
     arg types: [int, byte[]]
     candidates:
      com.tencent.feedback.proguard.ai.a(int, int):boolean
      com.tencent.feedback.proguard.ai.a(java.lang.Object, java.lang.Object):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ai.a(java.lang.Object, java.lang.Object):boolean
     arg types: [int, java.util.Map<java.lang.String, java.lang.String>]
     candidates:
      com.tencent.feedback.proguard.ai.a(int, int):boolean
      com.tencent.feedback.proguard.ai.a(java.lang.Object, java.lang.Object):boolean */
    public final boolean equals(Object obj) {
        C0006f fVar = (C0006f) obj;
        if (!ai.a(1, fVar.f2602a) || !ai.a(1, fVar.f) || !ai.a(1, fVar.g) || !ai.a(1, fVar.b) || !ai.a((Object) 1, (Object) fVar.c) || !ai.a((Object) 1, (Object) fVar.d) || !ai.a((Object) 1, (Object) fVar.e) || !ai.a(1, fVar.h) || !ai.a((Object) 1, (Object) fVar.i) || !ai.a((Object) 1, (Object) fVar.j)) {
            return false;
        }
        return true;
    }

    public final Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e2) {
            if (m) {
                return null;
            }
            throw new AssertionError();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ah.a(java.util.Map, int):void
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int]
     candidates:
      com.tencent.feedback.proguard.ah.a(byte, int):void
      com.tencent.feedback.proguard.ah.a(float, int):void
      com.tencent.feedback.proguard.ah.a(int, int):void
      com.tencent.feedback.proguard.ah.a(long, int):void
      com.tencent.feedback.proguard.ah.a(com.tencent.feedback.proguard.j, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.Object, int):void
      com.tencent.feedback.proguard.ah.a(java.lang.String, int):void
      com.tencent.feedback.proguard.ah.a(java.util.Collection, int):void
      com.tencent.feedback.proguard.ah.a(short, int):void
      com.tencent.feedback.proguard.ah.a(boolean, int):void
      com.tencent.feedback.proguard.ah.a(byte[], int):void
      com.tencent.feedback.proguard.ah.a(java.util.Map, int):void */
    public final void a(ah ahVar) {
        ahVar.a(this.f2602a, 1);
        ahVar.a(this.f, 2);
        ahVar.a(this.g, 3);
        ahVar.a(this.b, 4);
        ahVar.a(this.c, 5);
        ahVar.a(this.d, 6);
        ahVar.a(this.e, 7);
        ahVar.a(this.h, 8);
        ahVar.a((Map) this.i, 9);
        ahVar.a((Map) this.j, 10);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(short, int, boolean):short
     arg types: [short, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
     arg types: [byte, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int, int]
     candidates:
      com.tencent.feedback.proguard.ag.a(double, int, boolean):double
      com.tencent.feedback.proguard.ag.a(java.lang.Object[], int, boolean):T[]
      com.tencent.feedback.proguard.ag.a(byte, int, boolean):byte
      com.tencent.feedback.proguard.ag.a(float, int, boolean):float
      com.tencent.feedback.proguard.ag.a(int, int, boolean):int
      com.tencent.feedback.proguard.ag.a(long, int, boolean):long
      com.tencent.feedback.proguard.ag.a(com.tencent.feedback.proguard.j, int, boolean):com.tencent.feedback.proguard.j
      com.tencent.feedback.proguard.ag.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.feedback.proguard.ag.a(short, int, boolean):short
      com.tencent.feedback.proguard.ag.a(java.lang.Object, int, boolean):java.lang.Object */
    public final void a(ag agVar) {
        try {
            this.f2602a = agVar.a(this.f2602a, 1, true);
            this.f = agVar.a(this.f, 2, true);
            this.g = agVar.a(this.g, 3, true);
            this.b = agVar.a(this.b, 4, true);
            this.c = agVar.b(5, true);
            this.d = agVar.b(6, true);
            if (k == null) {
                k = new byte[]{0};
            }
            byte[] bArr = k;
            this.e = agVar.c(7, true);
            this.h = agVar.a(this.h, 8, true);
            if (l == null) {
                HashMap hashMap = new HashMap();
                l = hashMap;
                hashMap.put(Constants.STR_EMPTY, Constants.STR_EMPTY);
            }
            this.i = (Map) agVar.a((Object) l, 9, true);
            if (l == null) {
                HashMap hashMap2 = new HashMap();
                l = hashMap2;
                hashMap2.put(Constants.STR_EMPTY, Constants.STR_EMPTY);
            }
            this.j = (Map) agVar.a((Object) l, 10, true);
        } catch (Exception e2) {
            e2.printStackTrace();
            System.out.println("RequestPacket decode error " + af.a(this.e));
            throw new RuntimeException(e2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.feedback.eup.g.a(java.util.Map, java.lang.String):com.tencent.feedback.eup.g
     arg types: [java.util.Map<java.lang.String, java.lang.String>, java.lang.String]
     candidates:
      com.tencent.feedback.eup.g.a(android.content.Context, java.util.List<com.tencent.feedback.eup.e>):int
      com.tencent.feedback.eup.g.a(java.lang.Throwable, com.tencent.feedback.eup.d):java.lang.String
      com.tencent.feedback.eup.g.a(android.content.Context, com.tencent.feedback.eup.e):boolean
      com.tencent.feedback.eup.g.a(byte, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(char, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(double, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(float, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(int, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(long, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(com.tencent.feedback.proguard.j, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.lang.Object, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.lang.String, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.util.Collection, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(short, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(boolean, java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(byte[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(double[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(float[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(int[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(long[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.lang.Object[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(short[], java.lang.String):com.tencent.feedback.eup.g
      com.tencent.feedback.eup.g.a(java.util.Map, java.lang.String):com.tencent.feedback.eup.g */
    public final void a(StringBuilder sb, int i2) {
        g gVar = new g(sb, i2);
        gVar.a(this.f2602a, "iVersion");
        gVar.a(this.f, "cPacketType");
        gVar.a(this.g, "iMessageType");
        gVar.a(this.b, "iRequestId");
        gVar.a(this.c, "sServantName");
        gVar.a(this.d, "sFuncName");
        gVar.a(this.e, "sBuffer");
        gVar.a(this.h, "iTimeout");
        gVar.a((Map) this.i, "context");
        gVar.a((Map) this.j, "status");
    }
}
