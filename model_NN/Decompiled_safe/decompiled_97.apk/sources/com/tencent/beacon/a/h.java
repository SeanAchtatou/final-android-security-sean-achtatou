package com.tencent.beacon.a;

import android.app.ActivityManager;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Debug;
import android.os.Environment;
import android.os.Process;
import android.os.StatFs;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.tencent.assistant.st.STConst;
import com.tencent.beacon.a.a.c;
import com.tencent.beacon.a.a.d;
import com.tencent.beacon.a.a.e;
import com.tencent.beacon.a.b.k;
import com.tencent.beacon.b.a;
import com.tencent.beacon.event.i;
import com.tencent.connect.common.Constants;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/* compiled from: ProGuard */
public class h {

    /* renamed from: a  reason: collision with root package name */
    private static h f2101a;
    private Context b;

    public static byte[] a(int i, byte[] bArr) {
        if (i == 1) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ZipOutputStream zipOutputStream = new ZipOutputStream(byteArrayOutputStream);
            ZipEntry zipEntry = new ZipEntry("zip");
            zipEntry.setSize((long) bArr.length);
            zipOutputStream.putNextEntry(zipEntry);
            zipOutputStream.write(bArr);
            zipOutputStream.closeEntry();
            zipOutputStream.close();
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            byteArrayOutputStream.close();
            return byteArray;
        } else if (i != 2) {
            return null;
        } else {
            ByteArrayOutputStream byteArrayOutputStream2 = new ByteArrayOutputStream();
            GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream2);
            gZIPOutputStream.write(bArr);
            gZIPOutputStream.finish();
            gZIPOutputStream.close();
            byte[] byteArray2 = byteArrayOutputStream2.toByteArray();
            byteArrayOutputStream2.close();
            return byteArray2;
        }
    }

    public static byte[] a(int i, String str, byte[] bArr) {
        if (i == 1) {
            if (str == null || bArr == null) {
                return null;
            }
            Cipher instance = Cipher.getInstance("DES/CBC/PKCS5Padding");
            instance.init(2, SecretKeyFactory.getInstance("DES").generateSecret(new DESKeySpec(str.getBytes("UTF-8"))), new IvParameterSpec(str.getBytes("UTF-8")));
            return instance.doFinal(bArr);
        } else if (i == 3) {
            return a(str, bArr);
        } else {
            return null;
        }
    }

    public static int a(Context context, d[] dVarArr) {
        if (context == null || dVarArr == null || dVarArr.length <= 0) {
            return -1;
        }
        ArrayList arrayList = new ArrayList(dVarArr.length);
        for (d dVar : dVarArr) {
            byte[] a2 = a.a(dVar);
            if (a2 != null) {
                com.tencent.beacon.a.a.a aVar = new com.tencent.beacon.a.a.a(6, 0, 0, a2);
                aVar.a(dVar.a());
                arrayList.add(aVar);
            }
        }
        if (arrayList.size() <= 0) {
            return 0;
        }
        if (com.tencent.beacon.a.a.a.b(context, arrayList)) {
            return arrayList.size();
        }
        return -1;
    }

    public static int p(Context context) {
        com.tencent.beacon.d.a.a(" RecordDAO.countRecordNum() start", new Object[0]);
        if (context == null) {
            com.tencent.beacon.d.a.d(" countRecordNum() have null args!", new Object[0]);
            return -1;
        }
        return com.tencent.beacon.a.a.a.b(context, new int[]{1, 2, 3, 4}, -1, Long.MAX_VALUE);
    }

    public static e o(Context context) {
        Object a2;
        if (context == null) {
            return null;
        }
        List<com.tencent.beacon.a.a.a> a3 = com.tencent.beacon.a.a.a.a(context, new int[]{8}, -1, -1, 1, -1, -1, -1, -1, -1, -1);
        if (a3 == null || a3.size() <= 0) {
            return null;
        }
        com.tencent.beacon.a.a.a aVar = a3.get(0);
        if (aVar == null || (a2 = a.a(aVar.b())) == null || !e.class.isInstance(a2)) {
            return null;
        }
        e cast = e.class.cast(a2);
        if (cast == null) {
            return cast;
        }
        cast.e(aVar.a());
        return cast;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static String a(String str) {
        String trim = str.replace('|', '_').trim();
        if (trim.length() == 0) {
            com.tencent.beacon.d.a.c("eventName is invalid!! eventName length == 0!", new Object[0]);
            return null;
        } else if (!c(trim)) {
            com.tencent.beacon.d.a.c("eventName is invalid!! eventName should be ASCII code in 32-126! eventName:" + str, new Object[0]);
            return null;
        } else if (trim.length() <= 128) {
            return trim;
        } else {
            com.tencent.beacon.d.a.c("eventName is invalid!! eventName length should be less than 128! eventName:" + str, new Object[0]);
            return trim.substring(0, 128);
        }
    }

    public static byte[] b(int i, byte[] bArr) {
        byte[] bArr2 = null;
        if (i == 1) {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
            ZipInputStream zipInputStream = new ZipInputStream(byteArrayInputStream);
            while (zipInputStream.getNextEntry() != null) {
                byte[] bArr3 = new byte[1024];
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                while (true) {
                    int read = zipInputStream.read(bArr3, 0, bArr3.length);
                    if (read == -1) {
                        break;
                    }
                    byteArrayOutputStream.write(bArr3, 0, read);
                }
                bArr2 = byteArrayOutputStream.toByteArray();
                byteArrayOutputStream.flush();
                byteArrayOutputStream.close();
            }
            zipInputStream.close();
            byteArrayInputStream.close();
            return bArr2;
        } else if (i == 2) {
            return a(bArr);
        } else {
            return null;
        }
    }

    public static byte[] b(int i, String str, byte[] bArr) {
        if (i == 1) {
            if (str == null || bArr == null) {
                return null;
            }
            Cipher instance = Cipher.getInstance("DES/CBC/PKCS5Padding");
            instance.init(1, SecretKeyFactory.getInstance("DES").generateSecret(new DESKeySpec(str.getBytes("UTF-8"))), new IvParameterSpec(str.getBytes("UTF-8")));
            return instance.doFinal(bArr);
        } else if (i == 3) {
            return b(str, bArr);
        } else {
            return null;
        }
    }

    public static boolean a(Context context, i iVar) {
        int i;
        boolean z;
        int i2 = 3;
        com.tencent.beacon.d.a.a(" RecordDAO.insert() start", new Object[0]);
        if (context == null || iVar == null || iVar.b() == null) {
            com.tencent.beacon.d.a.d(" insert() have null args!", new Object[0]);
            return false;
        }
        if (iVar.b().equals("UA")) {
            i = 1;
        } else if (iVar.b().equals("IP")) {
            i = 2;
            i2 = 0;
        } else if (iVar.b().equals("DN")) {
            i = 3;
            i2 = 0;
        } else if (iVar.b().equals("HO")) {
            i = 4;
            i2 = 0;
        } else {
            com.tencent.beacon.d.a.d(" bean's type is error!", new Object[0]);
            return false;
        }
        try {
            com.tencent.beacon.a.a.a aVar = new com.tencent.beacon.a.a.a(i, i2, iVar.c(), a.a(iVar));
            if (context == null || aVar == null) {
                com.tencent.beacon.d.a.a("AnalyticsDAO.insert() have null args", new Object[0]);
                z = false;
            } else {
                ArrayList arrayList = new ArrayList();
                arrayList.add(aVar);
                z = com.tencent.beacon.a.a.a.a(context, arrayList);
            }
            if (z) {
                iVar.a(aVar.a());
            }
            com.tencent.beacon.d.a.a(" RecordDAO.insert() end", new Object[0]);
            return z;
        } catch (Throwable th) {
            com.tencent.beacon.d.a.a(" RecordDAO.insert() end", new Object[0]);
            throw th;
        }
    }

    public static synchronized h a(Context context) {
        h hVar;
        synchronized (h.class) {
            if (f2101a == null && context != null) {
                f2101a = new h(context);
            }
            hVar = f2101a;
        }
        return hVar;
    }

    public static List<d> m(Context context) {
        List<com.tencent.beacon.a.a.a> a2;
        if (context == null || (a2 = com.tencent.beacon.a.a.a.a(context, new int[]{6}, -1, -1, 5, -1, -1, -1, -1, -1, 0)) == null || a2.size() <= 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList(a2.size());
        for (com.tencent.beacon.a.a.a next : a2) {
            try {
                d cast = d.class.cast(a.a(next.b()));
                if (cast != null) {
                    cast.a(next.a());
                    arrayList.add(cast);
                }
            } catch (Throwable th) {
                th.printStackTrace();
                com.tencent.beacon.d.a.d("netconsume error %s", th.toString());
            }
        }
        return arrayList;
    }

    public static String m() {
        try {
            return new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(new Date());
        } catch (Throwable th) {
            th.printStackTrace();
            return Constants.STR_EMPTY;
        }
    }

    private h(Context context) {
        this.b = context;
    }

    private static byte[] a(String str, byte[] bArr) {
        if (str == null || bArr == null) {
            return null;
        }
        for (int length = str.length(); length < 16; length++) {
            str = String.valueOf(str) + "0";
        }
        String substring = str.substring(0, 16);
        StringBuffer stringBuffer = new StringBuffer();
        int length2 = bArr.length;
        for (int i = 0; i < length2; i++) {
            stringBuffer.append(String.valueOf((int) bArr[i]) + " ");
        }
        SecretKeySpec secretKeySpec = new SecretKeySpec(substring.getBytes(), "AES");
        Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
        instance.init(2, secretKeySpec, new IvParameterSpec(substring.getBytes()));
        byte[] doFinal = instance.doFinal(bArr);
        StringBuffer stringBuffer2 = new StringBuffer();
        int length3 = doFinal.length;
        for (int i2 = 0; i2 < length3; i2++) {
            stringBuffer2.append(String.valueOf((int) doFinal[i2]) + " ");
        }
        return doFinal;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x003c A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String b(java.lang.String r7) {
        /*
            r6 = 16
            r2 = 0
            java.lang.String r0 = "unknown"
            if (r7 == 0) goto L_0x0011
            java.lang.String r1 = r7.trim()
            int r1 = r1.length()
            if (r1 != 0) goto L_0x0012
        L_0x0011:
            return r0
        L_0x0012:
            java.lang.String r3 = r7.trim()
            int r1 = r3.length()
        L_0x001a:
            int r1 = r1 + -1
            if (r1 < 0) goto L_0x003c
            char r4 = r3.charAt(r1)
            r5 = 48
            if (r4 < r5) goto L_0x002a
            r5 = 57
            if (r4 <= r5) goto L_0x001a
        L_0x002a:
            r1 = r2
        L_0x002b:
            if (r1 == 0) goto L_0x003e
            java.lang.String r0 = r7.trim()
            int r1 = r0.length()
            if (r1 <= r6) goto L_0x0011
            java.lang.String r0 = r0.substring(r2, r6)
            goto L_0x0011
        L_0x003c:
            r1 = 1
            goto L_0x002b
        L_0x003e:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r3 = "channelID is invalid!! channelID should be Numeric! channelID:"
            r1.<init>(r3)
            java.lang.StringBuilder r1 = r1.append(r7)
            java.lang.String r1 = r1.toString()
            java.lang.Object[] r2 = new java.lang.Object[r2]
            com.tencent.beacon.d.a.c(r1, r2)
            goto L_0x0011
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.beacon.a.h.b(java.lang.String):java.lang.String");
    }

    public static String a() {
        try {
            return Build.MODEL;
        } catch (Throwable th) {
            com.tencent.beacon.d.a.d("getDeviceName error", new Object[0]);
            th.printStackTrace();
            return Constants.STR_EMPTY;
        }
    }

    public static boolean a(Context context, k kVar) {
        SQLiteDatabase sQLiteDatabase;
        ContentValues contentValues = null;
        boolean z = false;
        if (context == null || kVar == null) {
            com.tencent.beacon.d.a.c("context == null || bean == null}", new Object[0]);
        } else {
            try {
                sQLiteDatabase = c.a(context).getWritableDatabase();
                if (sQLiteDatabase == null) {
                    try {
                        com.tencent.beacon.d.a.d("get db fail!,return false ", new Object[0]);
                        if (sQLiteDatabase != null && sQLiteDatabase.isOpen()) {
                            sQLiteDatabase.close();
                        }
                    } catch (Throwable th) {
                        th = th;
                        try {
                            com.tencent.beacon.d.a.d("Error strategy update!  %s", th.toString());
                            if (sQLiteDatabase != null && sQLiteDatabase.isOpen()) {
                                sQLiteDatabase.close();
                            }
                            return z;
                        } catch (Throwable th2) {
                            th = th2;
                            if (sQLiteDatabase != null && sQLiteDatabase.isOpen()) {
                                sQLiteDatabase.close();
                            }
                            throw th;
                        }
                    }
                } else {
                    if (kVar != null) {
                        contentValues = new ContentValues();
                        if (kVar.a() >= 0) {
                            contentValues.put("_id", Long.valueOf(kVar.a()));
                        }
                        contentValues.put("_key", Integer.valueOf(kVar.b()));
                        contentValues.put("_datas", kVar.c());
                    }
                    if (contentValues != null) {
                        long replace = sQLiteDatabase.replace("t_strategy", "_id", contentValues);
                        if (replace < 0) {
                            com.tencent.beacon.d.a.c("insert failure! return false ", new Object[0]);
                        } else {
                            kVar.a(replace);
                            com.tencent.beacon.d.a.e("update strategy  %d true ", Integer.valueOf(kVar.b()));
                            z = true;
                        }
                    }
                    if (sQLiteDatabase != null && sQLiteDatabase.isOpen()) {
                        sQLiteDatabase.close();
                    }
                }
            } catch (Throwable th3) {
                th = th3;
                sQLiteDatabase = null;
                sQLiteDatabase.close();
                throw th;
            }
        }
        return z;
    }

    public static String b() {
        try {
            return Build.VERSION.RELEASE;
        } catch (Throwable th) {
            com.tencent.beacon.d.a.d("getVersion error", new Object[0]);
            th.printStackTrace();
            return Constants.STR_EMPTY;
        }
    }

    private static byte[] a(byte[] bArr) {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
        GZIPInputStream gZIPInputStream = new GZIPInputStream(byteArrayInputStream);
        byte[] bArr2 = new byte[1024];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while (true) {
            int read = gZIPInputStream.read(bArr2, 0, bArr2.length);
            if (read == -1) {
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                byteArrayOutputStream.flush();
                byteArrayOutputStream.close();
                gZIPInputStream.close();
                byteArrayInputStream.close();
                return byteArray;
            }
            byteArrayOutputStream.write(bArr2, 0, read);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static String a(Map<String, String> map) {
        Set<String> keySet;
        com.tencent.beacon.d.a.b("map 2 str", new Object[0]);
        if (map == null || (keySet = map.keySet()) == null) {
            return Constants.STR_EMPTY;
        }
        if (keySet.size() > 50) {
            com.tencent.beacon.d.a.c("The Map<String, String> params size is more than 50, effective size is <= 50!", new Object[0]);
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (String next : keySet) {
            int length = next.trim().length();
            if (length <= 0 || !c(next)) {
                com.tencent.beacon.d.a.c("The Map<String, String> params key is invalid!! key should be ASCII code in 32-126! key:" + next, new Object[0]);
            } else {
                String trim = next.trim();
                if (length > 64) {
                    trim = trim.substring(0, 64);
                }
                stringBuffer.append("&");
                stringBuffer.append(trim.replace("|", "%7C").replace("&", "%26").replace("=", "%3D"));
                stringBuffer.append("=");
                String str = map.get(next);
                if (str != null) {
                    String trim2 = str.trim();
                    if (trim2.contains(";")) {
                        if (trim2.length() > 10240) {
                            String substring = trim2.substring(0, 10240);
                            trim2 = substring.substring(0, substring.lastIndexOf(";"));
                        }
                    } else if (trim2.length() > 1024) {
                        trim2 = trim2.substring(0, 1024);
                    }
                    stringBuffer.append(trim2.replace(10, ' ').replace(13, ' ').replace("|", "%7C").replace("&", "%26").replace("=", "%3D"));
                }
            }
        }
        String substring2 = stringBuffer.substring(1);
        stringBuffer.setLength(0);
        return substring2;
    }

    public static int b(Context context, d[] dVarArr) {
        if (context == null) {
            return -1;
        }
        if (dVarArr == null) {
            return com.tencent.beacon.a.a.a.a(context, new int[]{6}, -1, Long.MAX_VALUE);
        }
        ArrayList arrayList = new ArrayList();
        for (d dVar : dVarArr) {
            if (dVar.a() >= 0) {
                arrayList.add(Long.valueOf(dVar.a()));
            }
        }
        if (arrayList.size() > 0) {
            return com.tencent.beacon.a.a.a.a(context, (Long[]) arrayList.toArray(new Long[0]));
        }
        return 0;
    }

    public static String c() {
        try {
            return Build.VERSION.SDK;
        } catch (Throwable th) {
            com.tencent.beacon.d.a.d("getApiLevel error", new Object[0]);
            th.printStackTrace();
            return Constants.STR_EMPTY;
        }
    }

    public static Long[] a(Context context, List<i> list) {
        int i;
        int i2;
        com.tencent.beacon.d.a.a(" RecordDAO.insertList() start", new Object[0]);
        if (context == null || list == null) {
            com.tencent.beacon.d.a.d(" insertList() have null args!", new Object[0]);
            return null;
        }
        int size = list.size();
        if (size == 0) {
            com.tencent.beacon.d.a.b(" list siez == 0 , return true!", new Object[0]);
            return null;
        }
        Long[] lArr = new Long[size];
        ArrayList<com.tencent.beacon.a.a.a> arrayList = new ArrayList<>();
        for (int i3 = 0; i3 < size; i3++) {
            i iVar = list.get(i3);
            if (iVar.b().equals("UA")) {
                i = 1;
                i2 = 3;
            } else if (iVar.b().equals("IP")) {
                i = 2;
                i2 = 0;
            } else if (iVar.b().equals("DN")) {
                i2 = 0;
                i = 3;
            } else if (iVar.b().equals("HO")) {
                i = 4;
                i2 = 0;
            } else {
                com.tencent.beacon.d.a.d(" bean's type is error!", new Object[0]);
            }
            try {
                arrayList.add(new com.tencent.beacon.a.a.a(i, i2, iVar.c(), a.a(iVar)));
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        if (!com.tencent.beacon.a.a.a.a(context, arrayList)) {
            return null;
        }
        int i4 = 0;
        for (com.tencent.beacon.a.a.a aVar : arrayList) {
            if (i4 < size) {
                lArr[i4] = Long.valueOf(aVar.a());
            }
            i4++;
        }
        return lArr;
    }

    private static byte[] b(String str, byte[] bArr) {
        if (str == null || bArr == null) {
            return null;
        }
        for (int length = str.length(); length < 16; length++) {
            str = String.valueOf(str) + "0";
        }
        String substring = str.substring(0, 16);
        StringBuffer stringBuffer = new StringBuffer();
        int length2 = bArr.length;
        for (int i = 0; i < length2; i++) {
            stringBuffer.append(String.valueOf((int) bArr[i]) + " ");
        }
        SecretKeySpec secretKeySpec = new SecretKeySpec(substring.getBytes(), "AES");
        Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
        instance.init(1, secretKeySpec, new IvParameterSpec(substring.getBytes()));
        byte[] doFinal = instance.doFinal(bArr);
        StringBuffer stringBuffer2 = new StringBuffer();
        int length3 = doFinal.length;
        for (int i2 = 0; i2 < length3; i2++) {
            stringBuffer2.append(String.valueOf((int) doFinal[i2]) + " ");
        }
        return doFinal;
    }

    public static String b(Context context) {
        String str;
        Throwable th;
        String str2 = Constants.STR_EMPTY;
        if (context == null) {
            com.tencent.beacon.d.a.d("getImei but context == null!", new Object[0]);
            return str2;
        }
        try {
            str2 = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
            if (str2 == null) {
                str = Constants.STR_EMPTY;
            } else {
                str = str2.toLowerCase();
            }
            try {
                com.tencent.beacon.d.a.a("IMEI:" + str, new Object[0]);
                return str;
            } catch (Throwable th2) {
                th = th2;
            }
        } catch (Throwable th3) {
            Throwable th4 = th3;
            str = str2;
            th = th4;
        }
        com.tencent.beacon.d.a.d("getImei error!", new Object[0]);
        th.printStackTrace();
        return str;
    }

    public static int c(Context context, d[] dVarArr) {
        if (context == null || dVarArr == null || dVarArr.length <= 0) {
            return -1;
        }
        ArrayList arrayList = new ArrayList(dVarArr.length);
        for (d dVar : dVarArr) {
            byte[] a2 = a.a(dVar);
            if (a2 != null) {
                com.tencent.beacon.a.a.a aVar = new com.tencent.beacon.a.a.a(7, 0, 0, a2);
                aVar.a(dVar.a());
                arrayList.add(aVar);
            }
        }
        if (arrayList.size() <= 0) {
            return 0;
        }
        if (com.tencent.beacon.a.a.a.b(context, arrayList)) {
            return arrayList.size();
        }
        return -1;
    }

    public static String c(Context context) {
        if (context == null) {
            com.tencent.beacon.d.a.d("getImsi but context == null!", new Object[0]);
            return Constants.STR_EMPTY;
        }
        try {
            String subscriberId = ((TelephonyManager) context.getSystemService("phone")).getSubscriberId();
            if (subscriberId == null) {
                return Constants.STR_EMPTY;
            }
            return subscriberId.toLowerCase();
        } catch (Throwable th) {
            Throwable th2 = th;
            String str = Constants.STR_EMPTY;
            com.tencent.beacon.d.a.d("getImsi error!", new Object[0]);
            th2.printStackTrace();
            return str;
        }
    }

    /* JADX WARN: Type inference failed for: r8v0 */
    /* JADX WARN: Type inference failed for: r8v1, types: [android.database.sqlite.SQLiteDatabase] */
    /* JADX WARN: Type inference failed for: r8v2, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r8v3 */
    /* JADX WARN: Type inference failed for: r8v4 */
    /* JADX WARN: Type inference failed for: r8v5 */
    /* JADX WARN: Type inference failed for: r8v6 */
    /* JADX WARN: Type inference failed for: r8v7, types: [com.tencent.beacon.a.b.k] */
    /* JADX WARN: Type inference failed for: r8v8 */
    /* JADX WARN: Type inference failed for: r8v9 */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00f5, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x0121, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x0122, code lost:
        r8 = r0;
        r0 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x0136, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x0137, code lost:
        r3 = r9;
        r10 = r2;
        r2 = r0;
        r0 = null;
        r8 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:?, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:?, code lost:
        return r0;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00f5  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0115  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x0121 A[ExcHandler: all (r1v15 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:21:0x0057] */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x0144  */
    /* JADX WARNING: Removed duplicated region for block: B:96:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.tencent.beacon.a.b.k a(android.content.Context r11, int r12) {
        /*
            r1 = 0
            r8 = 0
            if (r11 != 0) goto L_0x000d
            java.lang.String r0 = "context == null}"
            java.lang.Object[] r1 = new java.lang.Object[r1]
            com.tencent.beacon.d.a.c(r0, r1)
            r0 = r8
        L_0x000c:
            return r0
        L_0x000d:
            com.tencent.beacon.a.a.c r9 = com.tencent.beacon.a.a.c.a(r11)     // Catch:{ Throwable -> 0x00d5, all -> 0x00fa }
            android.database.sqlite.SQLiteDatabase r0 = r9.getWritableDatabase()     // Catch:{ Throwable -> 0x012b, all -> 0x0119 }
            if (r0 != 0) goto L_0x0031
            java.lang.String r1 = "getWritableDatabase fail! "
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Throwable -> 0x0131, all -> 0x011c }
            com.tencent.beacon.d.a.c(r1, r2)     // Catch:{ Throwable -> 0x0131, all -> 0x011c }
            if (r0 == 0) goto L_0x002a
            boolean r1 = r0.isOpen()
            if (r1 == 0) goto L_0x002a
            r0.close()
        L_0x002a:
            if (r9 == 0) goto L_0x002f
            r9.close()
        L_0x002f:
            r0 = r8
            goto L_0x000c
        L_0x0031:
            java.util.Locale r1 = java.util.Locale.US     // Catch:{ Throwable -> 0x0131, all -> 0x011c }
            java.lang.String r2 = " %s = %d "
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x0131, all -> 0x011c }
            r4 = 0
            java.lang.String r5 = "_key"
            r3[r4] = r5     // Catch:{ Throwable -> 0x0131, all -> 0x011c }
            r4 = 1
            r5 = 101(0x65, float:1.42E-43)
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ Throwable -> 0x0131, all -> 0x011c }
            r3[r4] = r5     // Catch:{ Throwable -> 0x0131, all -> 0x011c }
            java.lang.String r3 = java.lang.String.format(r1, r2, r3)     // Catch:{ Throwable -> 0x0131, all -> 0x011c }
            java.lang.String r1 = "t_strategy"
            r2 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r2 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Throwable -> 0x0131, all -> 0x011c }
            if (r2 == 0) goto L_0x0147
            boolean r1 = r2.moveToNext()     // Catch:{ Throwable -> 0x0136, all -> 0x0121 }
            if (r1 == 0) goto L_0x0147
            if (r2 == 0) goto L_0x006b
            boolean r1 = r2.isBeforeFirst()     // Catch:{ Throwable -> 0x0136, all -> 0x0121 }
            if (r1 != 0) goto L_0x006b
            boolean r1 = r2.isAfterLast()     // Catch:{ Throwable -> 0x0136, all -> 0x0121 }
            if (r1 == 0) goto L_0x009f
        L_0x006b:
            if (r8 == 0) goto L_0x0080
            java.lang.String r1 = "read strategy key: %d"
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x013d, all -> 0x0121 }
            r4 = 0
            int r5 = r8.b()     // Catch:{ Throwable -> 0x013d, all -> 0x0121 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ Throwable -> 0x013d, all -> 0x0121 }
            r3[r4] = r5     // Catch:{ Throwable -> 0x013d, all -> 0x0121 }
            com.tencent.beacon.d.a.g(r1, r3)     // Catch:{ Throwable -> 0x013d, all -> 0x0121 }
        L_0x0080:
            r1 = r8
        L_0x0081:
            if (r2 == 0) goto L_0x008c
            boolean r3 = r2.isClosed()
            if (r3 != 0) goto L_0x008c
            r2.close()
        L_0x008c:
            if (r0 == 0) goto L_0x0097
            boolean r2 = r0.isOpen()
            if (r2 == 0) goto L_0x0097
            r0.close()
        L_0x0097:
            if (r9 == 0) goto L_0x0144
            r9.close()
            r0 = r1
            goto L_0x000c
        L_0x009f:
            java.lang.String r1 = "parse bean}"
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x0136, all -> 0x0121 }
            com.tencent.beacon.d.a.b(r1, r3)     // Catch:{ Throwable -> 0x0136, all -> 0x0121 }
            com.tencent.beacon.a.b.k r1 = new com.tencent.beacon.a.b.k     // Catch:{ Throwable -> 0x0136, all -> 0x0121 }
            r1.<init>()     // Catch:{ Throwable -> 0x0136, all -> 0x0121 }
            java.lang.String r3 = "_id"
            int r3 = r2.getColumnIndex(r3)     // Catch:{ Throwable -> 0x0136, all -> 0x0121 }
            long r3 = r2.getLong(r3)     // Catch:{ Throwable -> 0x0136, all -> 0x0121 }
            r1.a(r3)     // Catch:{ Throwable -> 0x0136, all -> 0x0121 }
            java.lang.String r3 = "_key"
            int r3 = r2.getColumnIndex(r3)     // Catch:{ Throwable -> 0x0136, all -> 0x0121 }
            int r3 = r2.getInt(r3)     // Catch:{ Throwable -> 0x0136, all -> 0x0121 }
            r1.a(r3)     // Catch:{ Throwable -> 0x0136, all -> 0x0121 }
            java.lang.String r3 = "_datas"
            int r3 = r2.getColumnIndex(r3)     // Catch:{ Throwable -> 0x0136, all -> 0x0121 }
            byte[] r3 = r2.getBlob(r3)     // Catch:{ Throwable -> 0x0136, all -> 0x0121 }
            r1.a(r3)     // Catch:{ Throwable -> 0x0136, all -> 0x0121 }
            r8 = r1
            goto L_0x006b
        L_0x00d5:
            r0 = move-exception
            r1 = r0
            r2 = r8
            r3 = r8
            r0 = r8
        L_0x00da:
            r1.printStackTrace()     // Catch:{ all -> 0x0125 }
            if (r8 == 0) goto L_0x00e8
            boolean r1 = r8.isClosed()
            if (r1 != 0) goto L_0x00e8
            r8.close()
        L_0x00e8:
            if (r2 == 0) goto L_0x00f3
            boolean r1 = r2.isOpen()
            if (r1 == 0) goto L_0x00f3
            r2.close()
        L_0x00f3:
            if (r3 == 0) goto L_0x000c
            r3.close()
            goto L_0x000c
        L_0x00fa:
            r0 = move-exception
            r2 = r8
            r9 = r8
        L_0x00fd:
            if (r2 == 0) goto L_0x0108
            boolean r1 = r2.isClosed()
            if (r1 != 0) goto L_0x0108
            r2.close()
        L_0x0108:
            if (r8 == 0) goto L_0x0113
            boolean r1 = r8.isOpen()
            if (r1 == 0) goto L_0x0113
            r8.close()
        L_0x0113:
            if (r9 == 0) goto L_0x0118
            r9.close()
        L_0x0118:
            throw r0
        L_0x0119:
            r0 = move-exception
            r2 = r8
            goto L_0x00fd
        L_0x011c:
            r1 = move-exception
            r2 = r8
            r8 = r0
            r0 = r1
            goto L_0x00fd
        L_0x0121:
            r1 = move-exception
            r8 = r0
            r0 = r1
            goto L_0x00fd
        L_0x0125:
            r0 = move-exception
            r9 = r3
            r10 = r2
            r2 = r8
            r8 = r10
            goto L_0x00fd
        L_0x012b:
            r0 = move-exception
            r1 = r0
            r2 = r8
            r3 = r9
            r0 = r8
            goto L_0x00da
        L_0x0131:
            r1 = move-exception
            r2 = r0
            r3 = r9
            r0 = r8
            goto L_0x00da
        L_0x0136:
            r1 = move-exception
            r3 = r9
            r10 = r2
            r2 = r0
            r0 = r8
            r8 = r10
            goto L_0x00da
        L_0x013d:
            r1 = move-exception
            r3 = r9
            r10 = r2
            r2 = r0
            r0 = r8
            r8 = r10
            goto L_0x00da
        L_0x0144:
            r0 = r1
            goto L_0x000c
        L_0x0147:
            r1 = r8
            goto L_0x0081
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.beacon.a.h.a(android.content.Context, int):com.tencent.beacon.a.b.k");
    }

    public static String d(Context context) {
        String str = Constants.STR_EMPTY;
        if (context == null) {
            com.tencent.beacon.d.a.d("getAndroidId but context == null!", new Object[0]);
            return str;
        }
        try {
            String string = Settings.Secure.getString(context.getContentResolver(), "android_id");
            if (string == null) {
                return Constants.STR_EMPTY;
            }
            try {
                return string.toLowerCase();
            } catch (Throwable th) {
                Throwable th2 = th;
                str = string;
                th = th2;
            }
        } catch (Throwable th3) {
            th = th3;
        }
        com.tencent.beacon.d.a.d("getAndroidId error!", new Object[0]);
        th.printStackTrace();
        return str;
    }

    public static List<d> n(Context context) {
        List<com.tencent.beacon.a.a.a> a2;
        if (context == null || (a2 = com.tencent.beacon.a.a.a.a(context, new int[]{7}, -1, -1, 5, -1, -1, -1, -1, -1, 0)) == null || a2.size() <= 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList(a2.size());
        for (com.tencent.beacon.a.a.a next : a2) {
            try {
                d cast = d.class.cast(a.a(next.b()));
                cast.a(next.a());
                arrayList.add(cast);
            } catch (Throwable th) {
                th.printStackTrace();
                com.tencent.beacon.d.a.d(" netconsume error:%s", th.toString());
            }
        }
        return arrayList;
    }

    public static List<i> a(Context context, String[] strArr, int i) {
        com.tencent.beacon.d.a.a(" RecordDAO.queryRecentRecord() start", new Object[0]);
        if (context == null) {
            com.tencent.beacon.d.a.d(" queryRecentRecord() have null args!", new Object[0]);
            return null;
        }
        List<com.tencent.beacon.a.a.a> a2 = com.tencent.beacon.a.a.a.a(context, new int[]{1, 2, 3, 4}, 1, 2, i, -1, -1, -1, -1, -1, -1);
        if (a2 == null || a2.size() <= 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        Iterator<com.tencent.beacon.a.a.a> it = a2.iterator();
        while (it.hasNext()) {
            com.tencent.beacon.a.a.a next = it.next();
            try {
                Object a3 = a.a(next.b());
                if (a3 != null && i.class.isInstance(a3)) {
                    i cast = i.class.cast(a3);
                    cast.a(next.a());
                    arrayList.add(cast);
                    it.remove();
                }
            } catch (Throwable th) {
                th.printStackTrace();
                com.tencent.beacon.d.a.d(" query have error!", new Object[0]);
            }
        }
        if (a2.size() > 0) {
            com.tencent.beacon.d.a.a(" there are error datas ,should be remove " + a2.size(), new Object[0]);
            Long[] lArr = new Long[a2.size()];
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 >= a2.size()) {
                    break;
                }
                lArr[i3] = Long.valueOf(a2.get(i3).a());
                i2 = i3 + 1;
            }
            com.tencent.beacon.a.a.a.a(context, lArr);
        }
        com.tencent.beacon.d.a.a(" RecordDAO.queryRecentRecord() end", new Object[0]);
        return arrayList;
    }

    public static boolean c(String str) {
        boolean z = true;
        int length = str.length();
        while (true) {
            length--;
            if (length < 0) {
                return z;
            }
            char charAt = str.charAt(length);
            if (charAt < ' ' || charAt >= 127) {
                z = false;
            }
        }
    }

    public static String e(Context context) {
        if (context == null) {
            com.tencent.beacon.d.a.d("getMacAddress but context == null!", new Object[0]);
            return Constants.STR_EMPTY;
        }
        try {
            String macAddress = ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getMacAddress();
            if (macAddress == null) {
                return Constants.STR_EMPTY;
            }
            return macAddress.toLowerCase();
        } catch (Throwable th) {
            Throwable th2 = th;
            String str = Constants.STR_EMPTY;
            th2.printStackTrace();
            com.tencent.beacon.d.a.d("getMacAddress error!", new Object[0]);
            return str;
        }
    }

    public static String f(Context context) {
        String str;
        if (context == null) {
            com.tencent.beacon.d.a.d("getMacAddress but context == null!", new Object[0]);
            return Constants.STR_EMPTY;
        }
        try {
            str = ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getBSSID();
        } catch (Throwable th) {
            th.printStackTrace();
            com.tencent.beacon.d.a.d("getMacAddress error!", new Object[0]);
            str = Constants.STR_EMPTY;
        }
        return str;
    }

    /* JADX WARNING: Removed duplicated region for block: B:42:0x009b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int b(android.content.Context r7, int r8) {
        /*
            r2 = 0
            r0 = 0
            if (r7 != 0) goto L_0x000c
            java.lang.String r1 = "context == null ||key < -1}"
            java.lang.Object[] r2 = new java.lang.Object[r0]
            com.tencent.beacon.d.a.c(r1, r2)
        L_0x000b:
            return r0
        L_0x000c:
            com.tencent.beacon.a.a.c r3 = com.tencent.beacon.a.a.c.a(r7)     // Catch:{ Throwable -> 0x0076, all -> 0x008c }
            android.database.sqlite.SQLiteDatabase r2 = r3.getWritableDatabase()     // Catch:{ Throwable -> 0x00a1 }
            if (r2 != 0) goto L_0x002f
            java.lang.String r1 = "get db fail!,return "
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x00a1 }
            com.tencent.beacon.d.a.d(r1, r4)     // Catch:{ Throwable -> 0x00a1 }
            if (r2 == 0) goto L_0x0029
            boolean r1 = r2.isOpen()
            if (r1 == 0) goto L_0x0029
            r2.close()
        L_0x0029:
            if (r3 == 0) goto L_0x000b
            r3.close()
            goto L_0x000b
        L_0x002f:
            java.lang.String r1 = "%s = %d"
            r4 = 2
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x00a1 }
            r5 = 0
            java.lang.String r6 = "_key"
            r4[r5] = r6     // Catch:{ Throwable -> 0x00a1 }
            r5 = 1
            r6 = 101(0x65, float:1.42E-43)
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Throwable -> 0x00a1 }
            r4[r5] = r6     // Catch:{ Throwable -> 0x00a1 }
            java.lang.String r1 = java.lang.String.format(r1, r4)     // Catch:{ Throwable -> 0x00a1 }
            java.lang.String r4 = "t_strategy"
            r5 = 0
            int r0 = r2.delete(r4, r1, r5)     // Catch:{ Throwable -> 0x00a1 }
            java.lang.String r1 = "delete Strategy key} %d , num} %d"
            r4 = 2
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Throwable -> 0x00a1 }
            r5 = 0
            r6 = 101(0x65, float:1.42E-43)
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Throwable -> 0x00a1 }
            r4[r5] = r6     // Catch:{ Throwable -> 0x00a1 }
            r5 = 1
            java.lang.Integer r6 = java.lang.Integer.valueOf(r0)     // Catch:{ Throwable -> 0x00a1 }
            r4[r5] = r6     // Catch:{ Throwable -> 0x00a1 }
            com.tencent.beacon.d.a.g(r1, r4)     // Catch:{ Throwable -> 0x00a1 }
            if (r2 == 0) goto L_0x0070
            boolean r1 = r2.isOpen()
            if (r1 == 0) goto L_0x0070
            r2.close()
        L_0x0070:
            if (r3 == 0) goto L_0x000b
            r3.close()
            goto L_0x000b
        L_0x0076:
            r1 = move-exception
            r3 = r2
        L_0x0078:
            r1.printStackTrace()     // Catch:{ all -> 0x009f }
            if (r2 == 0) goto L_0x0086
            boolean r1 = r2.isOpen()
            if (r1 == 0) goto L_0x0086
            r2.close()
        L_0x0086:
            if (r3 == 0) goto L_0x000b
            r3.close()
            goto L_0x000b
        L_0x008c:
            r0 = move-exception
            r3 = r2
        L_0x008e:
            if (r2 == 0) goto L_0x0099
            boolean r1 = r2.isOpen()
            if (r1 == 0) goto L_0x0099
            r2.close()
        L_0x0099:
            if (r3 == 0) goto L_0x009e
            r3.close()
        L_0x009e:
            throw r0
        L_0x009f:
            r0 = move-exception
            goto L_0x008e
        L_0x00a1:
            r1 = move-exception
            goto L_0x0078
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.beacon.a.h.b(android.content.Context, int):int");
    }

    public static String g(Context context) {
        String str;
        if (context == null) {
            com.tencent.beacon.d.a.d("getWifiSSID but context == null!", new Object[0]);
            return Constants.STR_EMPTY;
        }
        try {
            WifiInfo connectionInfo = ((WifiManager) context.getSystemService("wifi")).getConnectionInfo();
            if (connectionInfo.getBSSID() != null) {
                str = connectionInfo.getSSID();
                return str;
            }
        } catch (Throwable th) {
            th.printStackTrace();
            com.tencent.beacon.d.a.d("getWifiSSID error!", new Object[0]);
        }
        str = Constants.STR_EMPTY;
        return str;
    }

    public static DisplayMetrics h(Context context) {
        if (context == null) {
            com.tencent.beacon.d.a.d("getDisplayMetrics but context == null!", new Object[0]);
            return null;
        }
        try {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
            return displayMetrics;
        } catch (Throwable th) {
            com.tencent.beacon.d.a.d("getDisplayMetrics error!", new Object[0]);
            th.printStackTrace();
            return null;
        }
    }

    public static int a(Context context, Long[] lArr) {
        com.tencent.beacon.d.a.a(" RecordDAO.deleteRecordList() start", new Object[0]);
        if (context == null) {
            com.tencent.beacon.d.a.d(" deleteRecordList() have null args!", new Object[0]);
            return -1;
        }
        com.tencent.beacon.d.a.a(" RecordDAO.deleteRecordList() end", new Object[0]);
        return com.tencent.beacon.a.a.a.a(context, lArr);
    }

    public static String d() {
        try {
            Object obj = Build.class.getField("HARDWARE").get(null);
            if (obj != null) {
                return obj.toString();
            }
            return null;
        } catch (Throwable th) {
            com.tencent.beacon.d.a.c("getCpuProductorName error!", new Object[0]);
            return null;
        }
    }

    public static String e() {
        try {
            StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
            return new StringBuilder().append(((((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize())) / 1024) / 1024).toString();
        } catch (Throwable th) {
            com.tencent.beacon.d.a.d("getDisplayMetrics error!", new Object[0]);
            th.printStackTrace();
            return null;
        }
    }

    public static long i(Context context) {
        if (context == null) {
            com.tencent.beacon.d.a.d("getFreeMem but context == null!", new Object[0]);
            return -1;
        }
        try {
            ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
            ((ActivityManager) context.getSystemService("activity")).getMemoryInfo(memoryInfo);
            return memoryInfo.availMem;
        } catch (Throwable th) {
            com.tencent.beacon.d.a.d("getFreeMem error!", new Object[0]);
            th.printStackTrace();
            return -1;
        }
    }

    public final String f() {
        String str;
        if (this.b == null) {
            com.tencent.beacon.d.a.d("getFreeMem but context == null!", new Object[0]);
            return Constants.STR_EMPTY;
        }
        try {
            ActivityManager activityManager = (ActivityManager) this.b.getSystemService("activity");
            if (a.f2082a == 0) {
                a.f2082a = Process.myPid();
            }
            Debug.MemoryInfo[] processMemoryInfo = activityManager.getProcessMemoryInfo(new int[]{a.f2082a});
            if (processMemoryInfo != null && processMemoryInfo.length == 1) {
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append(processMemoryInfo[0].getTotalPrivateDirty()).append(",").append(processMemoryInfo[0].dalvikPrivateDirty).append(",").append(processMemoryInfo[0].nativePrivateDirty).append(",").append(processMemoryInfo[0].otherPrivateDirty).append(",").append(processMemoryInfo[0].getTotalPss()).append(",").append(processMemoryInfo[0].dalvikPss).append(",").append(processMemoryInfo[0].nativePss).append(",").append(processMemoryInfo[0].otherPss);
                str = stringBuffer.toString();
                return str;
            }
        } catch (Throwable th) {
            com.tencent.beacon.d.a.d("getFreeMem error!", new Object[0]);
            th.printStackTrace();
        }
        str = Constants.STR_EMPTY;
        return str;
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x011a A[Catch:{ Exception -> 0x0224 }] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x01ad A[Catch:{ Exception -> 0x0224 }] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0208 A[Catch:{ Exception -> 0x0224 }] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0227  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x00bb A[Catch:{ Exception -> 0x0224 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String g() {
        /*
            r12 = 10
            r2 = 0
            java.lang.String r4 = "0"
            java.lang.String r5 = "/proc/stat"
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0224 }
            java.lang.String r1 = "/proc/"
            r0.<init>(r1)     // Catch:{ Exception -> 0x0224 }
            int r1 = com.tencent.beacon.a.a.f2082a     // Catch:{ Exception -> 0x0224 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0224 }
            java.lang.String r1 = "/stat"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0224 }
            java.lang.String r9 = r0.toString()     // Catch:{ Exception -> 0x0224 }
            java.lang.String r0 = com.tencent.beacon.b.a.b(r5)     // Catch:{ Exception -> 0x0224 }
            if (r0 == 0) goto L_0x022f
            java.lang.String r1 = " "
            java.lang.String[] r0 = r0.split(r1)     // Catch:{ Exception -> 0x0224 }
            int r1 = r0.length     // Catch:{ Exception -> 0x0224 }
            r6 = 11
            if (r1 < r6) goto L_0x022f
            r1 = 2
            r1 = r0[r1]     // Catch:{ Exception -> 0x0224 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ Exception -> 0x0224 }
            int r1 = r1.intValue()     // Catch:{ Exception -> 0x0224 }
            r6 = 3
            r6 = r0[r6]     // Catch:{ Exception -> 0x0224 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Exception -> 0x0224 }
            int r6 = r6.intValue()     // Catch:{ Exception -> 0x0224 }
            int r1 = r1 + r6
            r6 = 4
            r6 = r0[r6]     // Catch:{ Exception -> 0x0224 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Exception -> 0x0224 }
            int r6 = r6.intValue()     // Catch:{ Exception -> 0x0224 }
            int r1 = r1 + r6
            r6 = 5
            r6 = r0[r6]     // Catch:{ Exception -> 0x0224 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Exception -> 0x0224 }
            int r6 = r6.intValue()     // Catch:{ Exception -> 0x0224 }
            int r1 = r1 + r6
            r6 = 6
            r6 = r0[r6]     // Catch:{ Exception -> 0x0224 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Exception -> 0x0224 }
            int r6 = r6.intValue()     // Catch:{ Exception -> 0x0224 }
            int r1 = r1 + r6
            r6 = 7
            r6 = r0[r6]     // Catch:{ Exception -> 0x0224 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Exception -> 0x0224 }
            int r6 = r6.intValue()     // Catch:{ Exception -> 0x0224 }
            int r1 = r1 + r6
            r6 = 8
            r6 = r0[r6]     // Catch:{ Exception -> 0x0224 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Exception -> 0x0224 }
            int r6 = r6.intValue()     // Catch:{ Exception -> 0x0224 }
            int r1 = r1 + r6
            r6 = 9
            r6 = r0[r6]     // Catch:{ Exception -> 0x0224 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Exception -> 0x0224 }
            int r6 = r6.intValue()     // Catch:{ Exception -> 0x0224 }
            int r1 = r1 + r6
            r6 = 10
            r0 = r0[r6]     // Catch:{ Exception -> 0x0224 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ Exception -> 0x0224 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x0224 }
            int r0 = r0 + r1
            long r0 = (long) r0     // Catch:{ Exception -> 0x0224 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0224 }
            java.lang.String r7 = "totalCpuTime1:"
            r6.<init>(r7)     // Catch:{ Exception -> 0x0224 }
            java.lang.StringBuilder r6 = r6.append(r0)     // Catch:{ Exception -> 0x0224 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x0224 }
            r7 = 0
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Exception -> 0x0224 }
            com.tencent.beacon.d.a.b(r6, r7)     // Catch:{ Exception -> 0x0224 }
            r7 = r0
        L_0x00b5:
            java.lang.String r0 = com.tencent.beacon.b.a.b(r9)     // Catch:{ Exception -> 0x0224 }
            if (r0 == 0) goto L_0x022c
            java.lang.String r1 = " "
            java.lang.String[] r0 = r0.split(r1)     // Catch:{ Exception -> 0x0224 }
            int r1 = r0.length     // Catch:{ Exception -> 0x0224 }
            r6 = 18
            if (r1 < r6) goto L_0x022c
            r1 = 13
            r1 = r0[r1]     // Catch:{ Exception -> 0x0224 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ Exception -> 0x0224 }
            int r1 = r1.intValue()     // Catch:{ Exception -> 0x0224 }
            r6 = 14
            r6 = r0[r6]     // Catch:{ Exception -> 0x0224 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Exception -> 0x0224 }
            int r6 = r6.intValue()     // Catch:{ Exception -> 0x0224 }
            int r1 = r1 + r6
            r6 = 15
            r6 = r0[r6]     // Catch:{ Exception -> 0x0224 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Exception -> 0x0224 }
            int r6 = r6.intValue()     // Catch:{ Exception -> 0x0224 }
            int r1 = r1 + r6
            r6 = 16
            r0 = r0[r6]     // Catch:{ Exception -> 0x0224 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ Exception -> 0x0224 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x0224 }
            int r0 = r0 + r1
            long r0 = (long) r0     // Catch:{ Exception -> 0x0224 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0224 }
            java.lang.String r10 = "proCpuTime1:"
            r6.<init>(r10)     // Catch:{ Exception -> 0x0224 }
            java.lang.StringBuilder r6 = r6.append(r0)     // Catch:{ Exception -> 0x0224 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x0224 }
            r10 = 0
            java.lang.Object[] r10 = new java.lang.Object[r10]     // Catch:{ Exception -> 0x0224 }
            com.tencent.beacon.d.a.b(r6, r10)     // Catch:{ Exception -> 0x0224 }
        L_0x010f:
            r10 = 500(0x1f4, double:2.47E-321)
            java.lang.Thread.sleep(r10)     // Catch:{ InterruptedException -> 0x021e }
        L_0x0114:
            java.lang.String r5 = com.tencent.beacon.b.a.b(r5)     // Catch:{ Exception -> 0x0224 }
            if (r5 == 0) goto L_0x0229
            java.lang.String r6 = " "
            java.lang.String[] r5 = r5.split(r6)     // Catch:{ Exception -> 0x0224 }
            int r6 = r5.length     // Catch:{ Exception -> 0x0224 }
            if (r6 < r12) goto L_0x0229
            r6 = 2
            r6 = r5[r6]     // Catch:{ Exception -> 0x0224 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Exception -> 0x0224 }
            int r6 = r6.intValue()     // Catch:{ Exception -> 0x0224 }
            r10 = 3
            r10 = r5[r10]     // Catch:{ Exception -> 0x0224 }
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x0224 }
            int r10 = r10.intValue()     // Catch:{ Exception -> 0x0224 }
            int r6 = r6 + r10
            r10 = 4
            r10 = r5[r10]     // Catch:{ Exception -> 0x0224 }
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x0224 }
            int r10 = r10.intValue()     // Catch:{ Exception -> 0x0224 }
            int r6 = r6 + r10
            r10 = 5
            r10 = r5[r10]     // Catch:{ Exception -> 0x0224 }
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x0224 }
            int r10 = r10.intValue()     // Catch:{ Exception -> 0x0224 }
            int r6 = r6 + r10
            r10 = 6
            r10 = r5[r10]     // Catch:{ Exception -> 0x0224 }
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x0224 }
            int r10 = r10.intValue()     // Catch:{ Exception -> 0x0224 }
            int r6 = r6 + r10
            r10 = 7
            r10 = r5[r10]     // Catch:{ Exception -> 0x0224 }
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x0224 }
            int r10 = r10.intValue()     // Catch:{ Exception -> 0x0224 }
            int r6 = r6 + r10
            r10 = 8
            r10 = r5[r10]     // Catch:{ Exception -> 0x0224 }
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x0224 }
            int r10 = r10.intValue()     // Catch:{ Exception -> 0x0224 }
            int r6 = r6 + r10
            r10 = 9
            r10 = r5[r10]     // Catch:{ Exception -> 0x0224 }
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x0224 }
            int r10 = r10.intValue()     // Catch:{ Exception -> 0x0224 }
            int r6 = r6 + r10
            r10 = 10
            r5 = r5[r10]     // Catch:{ Exception -> 0x0224 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ Exception -> 0x0224 }
            int r5 = r5.intValue()     // Catch:{ Exception -> 0x0224 }
            int r5 = r5 + r6
            long r5 = (long) r5     // Catch:{ Exception -> 0x0224 }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0224 }
            java.lang.String r11 = "totalCpuTime2:"
            r10.<init>(r11)     // Catch:{ Exception -> 0x0224 }
            java.lang.StringBuilder r10 = r10.append(r5)     // Catch:{ Exception -> 0x0224 }
            java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x0224 }
            r11 = 0
            java.lang.Object[] r11 = new java.lang.Object[r11]     // Catch:{ Exception -> 0x0224 }
            com.tencent.beacon.d.a.b(r10, r11)     // Catch:{ Exception -> 0x0224 }
        L_0x01a7:
            java.lang.String r9 = com.tencent.beacon.b.a.b(r9)     // Catch:{ Exception -> 0x0224 }
            if (r9 == 0) goto L_0x0201
            java.lang.String r10 = " "
            java.lang.String[] r9 = r9.split(r10)     // Catch:{ Exception -> 0x0224 }
            int r10 = r9.length     // Catch:{ Exception -> 0x0224 }
            r11 = 18
            if (r10 < r11) goto L_0x0201
            r2 = 13
            r2 = r9[r2]     // Catch:{ Exception -> 0x0224 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ Exception -> 0x0224 }
            int r2 = r2.intValue()     // Catch:{ Exception -> 0x0224 }
            r3 = 14
            r3 = r9[r3]     // Catch:{ Exception -> 0x0224 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0224 }
            int r3 = r3.intValue()     // Catch:{ Exception -> 0x0224 }
            int r2 = r2 + r3
            r3 = 15
            r3 = r9[r3]     // Catch:{ Exception -> 0x0224 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0224 }
            int r3 = r3.intValue()     // Catch:{ Exception -> 0x0224 }
            int r2 = r2 + r3
            r3 = 16
            r3 = r9[r3]     // Catch:{ Exception -> 0x0224 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0224 }
            int r3 = r3.intValue()     // Catch:{ Exception -> 0x0224 }
            int r2 = r2 + r3
            long r2 = (long) r2     // Catch:{ Exception -> 0x0224 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0224 }
            java.lang.String r10 = "proCpuTime2:"
            r9.<init>(r10)     // Catch:{ Exception -> 0x0224 }
            java.lang.StringBuilder r9 = r9.append(r2)     // Catch:{ Exception -> 0x0224 }
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x0224 }
            r10 = 0
            java.lang.Object[] r10 = new java.lang.Object[r10]     // Catch:{ Exception -> 0x0224 }
            com.tencent.beacon.d.a.b(r9, r10)     // Catch:{ Exception -> 0x0224 }
        L_0x0201:
            long r5 = r5 - r7
            float r5 = (float) r5     // Catch:{ Exception -> 0x0224 }
            r6 = 0
            int r6 = (r5 > r6 ? 1 : (r5 == r6 ? 0 : -1))
            if (r6 <= 0) goto L_0x0227
            long r0 = r2 - r0
            float r0 = (float) r0     // Catch:{ Exception -> 0x0224 }
            float r0 = r0 / r5
            double r0 = (double) r0     // Catch:{ Exception -> 0x0224 }
            java.lang.String r2 = "%.2f"
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x0224 }
            r5 = 0
            java.lang.Double r0 = java.lang.Double.valueOf(r0)     // Catch:{ Exception -> 0x0224 }
            r3[r5] = r0     // Catch:{ Exception -> 0x0224 }
            java.lang.String r0 = java.lang.String.format(r2, r3)     // Catch:{ Exception -> 0x0224 }
        L_0x021d:
            return r0
        L_0x021e:
            r6 = move-exception
            r6.printStackTrace()     // Catch:{ Exception -> 0x0224 }
            goto L_0x0114
        L_0x0224:
            r0 = move-exception
            r0 = r4
            goto L_0x021d
        L_0x0227:
            r0 = r4
            goto L_0x021d
        L_0x0229:
            r5 = r2
            goto L_0x01a7
        L_0x022c:
            r0 = r2
            goto L_0x010f
        L_0x022f:
            r7 = r2
            goto L_0x00b5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.beacon.a.h.g():java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0065 A[SYNTHETIC, Splitter:B:18:0x0065] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x006a A[Catch:{ Throwable -> 0x006e }] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0080 A[SYNTHETIC, Splitter:B:27:0x0080] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0085 A[Catch:{ Throwable -> 0x0089 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String h() {
        /*
            r0 = 0
            r8 = 0
            java.lang.String r1 = "/proc/meminfo"
            java.io.FileReader r3 = new java.io.FileReader     // Catch:{ Throwable -> 0x0055, all -> 0x007a }
            r3.<init>(r1)     // Catch:{ Throwable -> 0x0055, all -> 0x007a }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ Throwable -> 0x009b, all -> 0x0095 }
            r1 = 8192(0x2000, float:1.14794E-41)
            r2.<init>(r3, r1)     // Catch:{ Throwable -> 0x009b, all -> 0x0095 }
            java.lang.String r1 = r2.readLine()     // Catch:{ Throwable -> 0x009e }
            java.lang.String r4 = ":\\s+"
            r5 = 2
            java.lang.String[] r1 = r1.split(r4, r5)     // Catch:{ Throwable -> 0x009e }
            r4 = 1
            r1 = r1[r4]     // Catch:{ Throwable -> 0x009e }
            java.lang.String r1 = r1.toLowerCase()     // Catch:{ Throwable -> 0x009e }
            java.lang.String r4 = "kb"
            java.lang.String r5 = ""
            java.lang.String r1 = r1.replace(r4, r5)     // Catch:{ Throwable -> 0x009e }
            java.lang.String r1 = r1.trim()     // Catch:{ Throwable -> 0x009e }
            long r4 = java.lang.Long.parseLong(r1)     // Catch:{ Throwable -> 0x009e }
            r6 = 1024(0x400, double:5.06E-321)
            long r4 = r4 / r6
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x009e }
            r1.<init>()     // Catch:{ Throwable -> 0x009e }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Throwable -> 0x009e }
            java.lang.String r0 = r1.toString()     // Catch:{ Throwable -> 0x009e }
            r2.close()     // Catch:{ Throwable -> 0x0049 }
            r3.close()     // Catch:{ Throwable -> 0x0049 }
        L_0x0048:
            return r0
        L_0x0049:
            r1 = move-exception
            java.lang.String r2 = "getFreeMem error!"
            java.lang.Object[] r3 = new java.lang.Object[r8]
            com.tencent.beacon.d.a.d(r2, r3)
            r1.printStackTrace()
            goto L_0x0048
        L_0x0055:
            r1 = move-exception
            r2 = r0
            r3 = r0
        L_0x0058:
            java.lang.String r4 = "getFreeMem error!"
            r5 = 0
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x0099 }
            com.tencent.beacon.d.a.d(r4, r5)     // Catch:{ all -> 0x0099 }
            r1.printStackTrace()     // Catch:{ all -> 0x0099 }
            if (r2 == 0) goto L_0x0068
            r2.close()     // Catch:{ Throwable -> 0x006e }
        L_0x0068:
            if (r3 == 0) goto L_0x0048
            r3.close()     // Catch:{ Throwable -> 0x006e }
            goto L_0x0048
        L_0x006e:
            r1 = move-exception
            java.lang.String r2 = "getFreeMem error!"
            java.lang.Object[] r3 = new java.lang.Object[r8]
            com.tencent.beacon.d.a.d(r2, r3)
            r1.printStackTrace()
            goto L_0x0048
        L_0x007a:
            r1 = move-exception
            r2 = r0
            r3 = r0
            r0 = r1
        L_0x007e:
            if (r2 == 0) goto L_0x0083
            r2.close()     // Catch:{ Throwable -> 0x0089 }
        L_0x0083:
            if (r3 == 0) goto L_0x0088
            r3.close()     // Catch:{ Throwable -> 0x0089 }
        L_0x0088:
            throw r0
        L_0x0089:
            r1 = move-exception
            java.lang.String r2 = "getFreeMem error!"
            java.lang.Object[] r3 = new java.lang.Object[r8]
            com.tencent.beacon.d.a.d(r2, r3)
            r1.printStackTrace()
            goto L_0x0088
        L_0x0095:
            r1 = move-exception
            r2 = r0
            r0 = r1
            goto L_0x007e
        L_0x0099:
            r0 = move-exception
            goto L_0x007e
        L_0x009b:
            r1 = move-exception
            r2 = r0
            goto L_0x0058
        L_0x009e:
            r1 = move-exception
            goto L_0x0058
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.beacon.a.h.h():java.lang.String");
    }

    public final long i() {
        boolean z;
        if (Environment.getExternalStorageState().equals("mounted")) {
            z = true;
        } else {
            z = false;
        }
        if (!z) {
            return 0;
        }
        try {
            StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
            int blockSize = statFs.getBlockSize();
            return ((((long) blockSize) * ((long) statFs.getBlockCount())) / 1024) / 1024;
        } catch (Throwable th) {
            th.printStackTrace();
            return 0;
        }
    }

    public static String j() {
        try {
            return Locale.getDefault().getCountry();
        } catch (Throwable th) {
            com.tencent.beacon.d.a.d("getCountry error!", new Object[0]);
            th.printStackTrace();
            return null;
        }
    }

    public static String k() {
        try {
            return Locale.getDefault().getLanguage();
        } catch (Throwable th) {
            com.tencent.beacon.d.a.d("getLanguage error!", new Object[0]);
            th.printStackTrace();
            return null;
        }
    }

    public static String j(Context context) {
        String str;
        String str2;
        String str3;
        String str4;
        Throwable th;
        String str5;
        String str6;
        String str7;
        if (context == null) {
            com.tencent.beacon.d.a.d("getSensor but context == null!", new Object[0]);
            return null;
        }
        com.tencent.beacon.d.a.a("getSensor start", new Object[0]);
        String str8 = "X";
        StringBuffer stringBuffer = new StringBuffer();
        if (Integer.parseInt(Build.VERSION.SDK) >= 10) {
            try {
                Class<?> cls = Class.forName("android.hardware.Camera");
                int intValue = ((Integer) cls.getMethod("getNumberOfCameras", new Class[0]).invoke(cls, new Object[0])).intValue();
                if (intValue == 0) {
                    str6 = "N";
                    str7 = "N";
                } else {
                    Class<?> cls2 = Class.forName("android.hardware.Camera$CameraInfo");
                    Object newInstance = cls2.newInstance();
                    Method[] methods = cls.getMethods();
                    Method method = null;
                    int length = methods.length;
                    int i = 0;
                    while (true) {
                        if (i >= length) {
                            break;
                        }
                        Method method2 = methods[i];
                        if (method2.getName().equals("getCameraInfo")) {
                            method = method2;
                            break;
                        }
                        i++;
                    }
                    Field field = cls2.getField("facing");
                    Field field2 = cls2.getField("CAMERA_FACING_BACK");
                    Field field3 = cls2.getField("CAMERA_FACING_FRONT");
                    if (method != null) {
                        String str9 = str8;
                        String str10 = "X";
                        int i2 = 0;
                        while (i2 < intValue) {
                            try {
                                method.invoke(cls, Integer.valueOf(i2), newInstance);
                                int i3 = field.getInt(newInstance);
                                int i4 = field2.getInt(newInstance);
                                int i5 = field3.getInt(newInstance);
                                if (i3 == i4) {
                                    str10 = "Y";
                                    if (intValue == 1) {
                                        str9 = "N";
                                    }
                                } else if (i3 == i5) {
                                    str9 = "Y";
                                    if (intValue == 1) {
                                        str10 = "N";
                                    }
                                }
                                i2++;
                            } catch (Throwable th2) {
                                th = th2;
                                str4 = str10;
                                str8 = str9;
                                str5 = "X";
                                com.tencent.beacon.d.a.d("getSensor error!", new Object[0]);
                                th.printStackTrace();
                                str2 = str8;
                                str3 = str5;
                                str = "X";
                                stringBuffer.append(str4).append(str2).append(str3).append(str);
                                return stringBuffer.toString();
                            }
                        }
                        str7 = str9;
                        str6 = str10;
                    } else {
                        str6 = "X";
                        str7 = str8;
                    }
                }
                try {
                    SensorManager sensorManager = (SensorManager) context.getSystemService("sensor");
                    if (sensorManager.getDefaultSensor(9) != null) {
                        str3 = "Y";
                    } else {
                        str3 = "N";
                    }
                    try {
                        if (sensorManager.getDefaultSensor(4) != null) {
                            str = "Y";
                            str2 = str7;
                            str4 = str6;
                        } else {
                            str = "N";
                            str2 = str7;
                            str4 = str6;
                        }
                    } catch (Throwable th3) {
                        str4 = str6;
                        String str11 = str3;
                        str8 = str7;
                        th = th3;
                        str5 = str11;
                        com.tencent.beacon.d.a.d("getSensor error!", new Object[0]);
                        th.printStackTrace();
                        str2 = str8;
                        str3 = str5;
                        str = "X";
                        stringBuffer.append(str4).append(str2).append(str3).append(str);
                        return stringBuffer.toString();
                    }
                } catch (Throwable th4) {
                    str8 = str7;
                    th = th4;
                    str5 = "X";
                    str4 = str6;
                    com.tencent.beacon.d.a.d("getSensor error!", new Object[0]);
                    th.printStackTrace();
                    str2 = str8;
                    str3 = str5;
                    str = "X";
                    stringBuffer.append(str4).append(str2).append(str3).append(str);
                    return stringBuffer.toString();
                }
            } catch (Throwable th5) {
                Throwable th6 = th5;
                str5 = "X";
                str4 = "X";
                th = th6;
                com.tencent.beacon.d.a.d("getSensor error!", new Object[0]);
                th.printStackTrace();
                str2 = str8;
                str3 = str5;
                str = "X";
                stringBuffer.append(str4).append(str2).append(str3).append(str);
                return stringBuffer.toString();
            }
        } else {
            str = "X";
            str2 = str8;
            str3 = "X";
            str4 = "X";
        }
        stringBuffer.append(str4).append(str2).append(str3).append(str);
        return stringBuffer.toString();
    }

    public static String l() {
        try {
            return Build.BRAND;
        } catch (Throwable th) {
            com.tencent.beacon.d.a.d("getBrand error!", new Object[0]);
            th.printStackTrace();
            return null;
        }
    }

    public static String k(Context context) {
        String str;
        TelephonyManager telephonyManager;
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo == null) {
                return STConst.ST_INSTALL_FAIL_STR_UNKNOWN;
            }
            if (activeNetworkInfo.getType() == 1) {
                str = "wifi";
            } else {
                if (activeNetworkInfo.getType() == 0 && (telephonyManager = (TelephonyManager) context.getSystemService("phone")) != null) {
                    switch (telephonyManager.getNetworkType()) {
                        case 1:
                            str = "GPRS";
                            break;
                        case 2:
                            str = "EDGE";
                            break;
                        case 3:
                            str = "UMTS";
                            break;
                        case 4:
                            str = "CDMA";
                            break;
                        case 5:
                            str = "EVDO_0";
                            break;
                        case 6:
                            str = "EVDO_A";
                            break;
                        case 7:
                            str = "1xRTT";
                            break;
                        case 8:
                            str = "HSDPA";
                            break;
                        case 9:
                            str = "HSUPA";
                            break;
                        case 10:
                            str = "HSPA";
                            break;
                        case 11:
                            str = "iDen";
                            break;
                        case 12:
                            str = "EVDO_B";
                            break;
                        case 13:
                            str = "LTE";
                            break;
                        case 14:
                            str = "eHRPD";
                            break;
                        case 15:
                            str = "HSPA+";
                            break;
                        default:
                            str = STConst.ST_INSTALL_FAIL_STR_UNKNOWN;
                            break;
                    }
                }
                str = STConst.ST_INSTALL_FAIL_STR_UNKNOWN;
            }
            return str;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int l(Context context) {
        int i;
        if (context == null) {
            com.tencent.beacon.d.a.d("getWifiSignalLevel but context == null!", new Object[0]);
            return 0;
        }
        try {
            WifiInfo connectionInfo = ((WifiManager) context.getSystemService("wifi")).getConnectionInfo();
            if (connectionInfo.getBSSID() != null) {
                i = WifiManager.calculateSignalLevel(connectionInfo.getRssi(), 5);
                return i;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        i = 0;
        return i;
    }
}
