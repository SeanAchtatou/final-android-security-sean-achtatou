package com.tencent.beacon.c.e;

import com.tencent.beacon.e.a;
import com.tencent.beacon.e.c;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/* compiled from: ProGuard */
public final class d extends c {
    private static Map<String, String> h;
    private static ArrayList<String> i;
    private static e j;
    private static ArrayList<String> k;

    /* renamed from: a  reason: collision with root package name */
    public byte f2123a = 0;
    public byte b = 0;
    public String c = Constants.STR_EMPTY;
    public Map<String, String> d = null;
    public ArrayList<String> e = null;
    public e f = null;
    public ArrayList<String> g = null;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.d.a(java.util.Map, int):void
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int]
     candidates:
      com.tencent.beacon.e.d.a(byte, int):void
      com.tencent.beacon.e.d.a(int, int):void
      com.tencent.beacon.e.d.a(long, int):void
      com.tencent.beacon.e.d.a(com.tencent.beacon.e.c, int):void
      com.tencent.beacon.e.d.a(java.lang.Object, int):void
      com.tencent.beacon.e.d.a(java.lang.String, int):void
      com.tencent.beacon.e.d.a(java.util.Collection, int):void
      com.tencent.beacon.e.d.a(short, int):void
      com.tencent.beacon.e.d.a(boolean, int):void
      com.tencent.beacon.e.d.a(byte[], int):void
      com.tencent.beacon.e.d.a(java.util.Map, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.d.a(java.util.Collection, int):void
     arg types: [java.util.ArrayList<java.lang.String>, int]
     candidates:
      com.tencent.beacon.e.d.a(byte, int):void
      com.tencent.beacon.e.d.a(int, int):void
      com.tencent.beacon.e.d.a(long, int):void
      com.tencent.beacon.e.d.a(com.tencent.beacon.e.c, int):void
      com.tencent.beacon.e.d.a(java.lang.Object, int):void
      com.tencent.beacon.e.d.a(java.lang.String, int):void
      com.tencent.beacon.e.d.a(java.util.Map, int):void
      com.tencent.beacon.e.d.a(short, int):void
      com.tencent.beacon.e.d.a(boolean, int):void
      com.tencent.beacon.e.d.a(byte[], int):void
      com.tencent.beacon.e.d.a(java.util.Collection, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.d.a(com.tencent.beacon.e.c, int):void
     arg types: [com.tencent.beacon.c.e.e, int]
     candidates:
      com.tencent.beacon.e.d.a(byte, int):void
      com.tencent.beacon.e.d.a(int, int):void
      com.tencent.beacon.e.d.a(long, int):void
      com.tencent.beacon.e.d.a(java.lang.Object, int):void
      com.tencent.beacon.e.d.a(java.lang.String, int):void
      com.tencent.beacon.e.d.a(java.util.Collection, int):void
      com.tencent.beacon.e.d.a(java.util.Map, int):void
      com.tencent.beacon.e.d.a(short, int):void
      com.tencent.beacon.e.d.a(boolean, int):void
      com.tencent.beacon.e.d.a(byte[], int):void
      com.tencent.beacon.e.d.a(com.tencent.beacon.e.c, int):void */
    public final void a(com.tencent.beacon.e.d dVar) {
        dVar.a(this.f2123a, 0);
        dVar.a(this.b, 1);
        dVar.a(this.c, 2);
        dVar.a((Map) this.d, 3);
        if (this.e != null) {
            dVar.a((Collection) this.e, 4);
        }
        if (this.f != null) {
            dVar.a((c) this.f, 5);
        }
        if (this.g != null) {
            dVar.a((Collection) this.g, 6);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(byte, int, boolean):byte
     arg types: [byte, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(int, int, boolean):int
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(byte, int, boolean):byte */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(int, int, boolean):int
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.ArrayList<java.lang.String>, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(int, int, boolean):int
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c
     arg types: [com.tencent.beacon.c.e.e, int, int]
     candidates:
      com.tencent.beacon.e.a.a(double, int, boolean):double
      com.tencent.beacon.e.a.a(float, int, boolean):float
      com.tencent.beacon.e.a.a(byte, int, boolean):byte
      com.tencent.beacon.e.a.a(int, int, boolean):int
      com.tencent.beacon.e.a.a(long, int, boolean):long
      com.tencent.beacon.e.a.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.beacon.e.a.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.beacon.e.a.a(short, int, boolean):short
      com.tencent.beacon.e.a.a(com.tencent.beacon.e.c, int, boolean):com.tencent.beacon.e.c */
    public final void a(a aVar) {
        this.f2123a = aVar.a(this.f2123a, 0, true);
        this.b = aVar.a(this.b, 1, true);
        this.c = aVar.b(2, true);
        if (h == null) {
            h = new HashMap();
            h.put(Constants.STR_EMPTY, Constants.STR_EMPTY);
        }
        this.d = (Map) aVar.a((Object) h, 3, true);
        if (i == null) {
            i = new ArrayList<>();
            i.add(Constants.STR_EMPTY);
        }
        this.e = (ArrayList) aVar.a((Object) i, 4, false);
        if (j == null) {
            j = new e();
        }
        this.f = (e) aVar.a((c) j, 5, false);
        if (k == null) {
            k = new ArrayList<>();
            k.add(Constants.STR_EMPTY);
        }
        this.g = (ArrayList) aVar.a((Object) k, 6, false);
    }
}
