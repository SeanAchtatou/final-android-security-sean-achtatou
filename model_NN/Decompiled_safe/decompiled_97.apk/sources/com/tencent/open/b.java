package com.tencent.open;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.webkit.WebChromeClient;

/* compiled from: ProGuard */
public abstract class b extends Dialog {
    protected c jsBridge;
    @SuppressLint({"NewApi"})
    protected final WebChromeClient mChromeClient = new f(this);

    /* access modifiers changed from: protected */
    public abstract void onConsoleMessage(String str);

    public b(Context context) {
        super(context);
    }

    public b(Context context, int i) {
        super(context, i);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.jsBridge = new c();
    }
}
