package com.tencent.open.a;

import java.io.File;
import java.io.FileFilter;

/* compiled from: ProGuard */
class d implements FileFilter {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f3240a;

    d(b bVar) {
        this.f3240a = bVar;
    }

    public boolean accept(File file) {
        if (file.getName().endsWith(this.f3240a.i()) && b.f(file) != -1) {
            return true;
        }
        return false;
    }
}
