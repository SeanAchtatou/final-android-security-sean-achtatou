package com.tencent.open.b;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/* compiled from: ProGuard */
class h extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ g f3255a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    h(g gVar, Looper looper) {
        super(looper);
        this.f3255a = gVar;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 1000:
                this.f3255a.b();
                break;
            case 1001:
                this.f3255a.e();
                break;
        }
        super.handleMessage(message);
    }
}
