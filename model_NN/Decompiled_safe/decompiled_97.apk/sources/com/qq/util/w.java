package com.qq.util;

import android.net.Uri;
import android.provider.BaseColumns;

/* compiled from: ProGuard */
public final class w implements BaseColumns {

    /* renamed from: a  reason: collision with root package name */
    public static final Uri f375a = Uri.parse("content://mms-sms/");
    public static final Uri b = Uri.parse("content://mms-sms/conversations");
    public static final Uri c = Uri.parse("content://mms-sms/messages/byphone");
    public static final Uri d = Uri.parse("content://mms-sms/undelivered");
    public static final Uri e = Uri.parse("content://mms-sms/draft");
    public static final Uri f = Uri.parse("content://mms-sms/locked");
    public static final Uri g = Uri.parse("content://mms-sms/search");
}
