package com.qq.util;

import java.io.FileDescriptor;
import java.lang.reflect.Field;
import java.net.Socket;
import java.net.SocketImpl;

/* compiled from: ProGuard */
public final class q {

    /* renamed from: a  reason: collision with root package name */
    private static Field f373a = null;
    private static Field b = null;
    private static Field c = null;

    public static int a(Socket socket) {
        if (socket == null) {
            return -1;
        }
        if (f373a == null) {
            try {
                f373a = Socket.class.getDeclaredField("impl");
                f373a.setAccessible(true);
                b = SocketImpl.class.getDeclaredField("fd");
                b.setAccessible(true);
                c = FileDescriptor.class.getDeclaredField("descriptor");
                c.setAccessible(true);
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        try {
            if (f373a == null) {
                return 0;
            }
            Object obj = f373a.get(socket);
            if (b == null || obj == null) {
                return 0;
            }
            Object obj2 = b.get(obj);
            if (c == null || obj2 == null) {
                return 0;
            }
            return c.getInt(obj2);
        } catch (Throwable th2) {
            th2.printStackTrace();
            return 0;
        }
    }
}
