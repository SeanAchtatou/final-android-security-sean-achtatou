package com.qq.util;

import java.security.MessageDigest;

/* compiled from: ProGuard */
public class m {

    /* renamed from: a  reason: collision with root package name */
    private static final char[] f370a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    public static String a(byte[] bArr) {
        StringBuilder sb = new StringBuilder(bArr.length * 3);
        for (byte b : bArr) {
            byte b2 = b & 255;
            sb.append(f370a[b2 >> 4]);
            sb.append(f370a[b2 & 15]);
        }
        return sb.toString().toUpperCase();
    }

    public static String b(byte[] bArr) {
        return a(c(bArr));
    }

    public static byte[] c(byte[] bArr) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(bArr);
            return instance.digest();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
