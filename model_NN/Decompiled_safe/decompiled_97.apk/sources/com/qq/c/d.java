package com.qq.c;

import android.content.Context;
import android.util.Log;
import com.qq.AppService.AstApp;
import com.qq.AppService.AuthorReceiver;
import com.qq.AppService.r;
import com.qq.b.a;
import com.qq.b.c;
import com.qq.ndk.Native;
import com.qq.util.j;
import java.io.File;
import java.io.IOException;

/* compiled from: ProGuard */
public class d {

    /* renamed from: a  reason: collision with root package name */
    public static volatile String f266a = null;
    public static final String[] b = {"/system/bin/su", "/system/xbin/su", "/system/sbin/su", "/vendor/bin/su", "/sbin/su"};
    public static volatile int c = -1;

    public static String a(Context context) {
        if (f266a == null) {
            b(context);
        }
        return f266a;
    }

    public static boolean a(String str) {
        if (str == null) {
            return false;
        }
        Native nativeR = new Native();
        String realLinkFile = nativeR.getRealLinkFile(str);
        if (realLinkFile != null) {
            str = realLinkFile;
        }
        File file = new File(str);
        if (!file.exists() || !file.canRead()) {
            return false;
        }
        int filePermission = nativeR.getFilePermission(str);
        if (filePermission < 4000 && file.length() != 113032 && file.length() != 113036 && file.length() != 30880 && file.length() != 311872 && j.c < 17) {
            return false;
        }
        if (filePermission % 2 == 0) {
            return false;
        }
        if (nativeR.getUid(str) != 0) {
            return false;
        }
        return true;
    }

    public static void b(Context context) {
        if (f266a == null || !new File(f266a).exists()) {
            int i = 0;
            while (i < b.length) {
                if (!a(b[i])) {
                    i++;
                } else {
                    f266a = b[i];
                    return;
                }
            }
        }
    }

    public static synchronized void a() {
        synchronized (d.class) {
            f266a = null;
            c = -1;
        }
    }

    public static synchronized int a(int i, Context context) {
        int i2 = 0;
        synchronized (d.class) {
            Log.d("com.qq.connect", "in init");
            if (i <= 0 || !a.c) {
                if (!a.f263a.exists() || !a.d.exists()) {
                    a.a();
                }
                a aVar = new a();
                if (!aVar.a("aurora_root")) {
                    a.c = false;
                } else {
                    aVar.a();
                }
                a.a(context);
                AuthorReceiver.a(2500);
                if (!a.c) {
                    if (f266a == null || !new File(f266a).exists()) {
                        b(context);
                    }
                    if (f266a == null) {
                        i2 = -1;
                    } else {
                        AuthorReceiver.a();
                        a.a(context);
                        if (!a.c) {
                            a aVar2 = new a();
                            if (aVar2.a("aurora_root")) {
                                AuthorReceiver.a(2500);
                            }
                            aVar2.a();
                        }
                        if (!a.c) {
                            b(i, context);
                        }
                        a aVar3 = new a();
                        if (aVar3.a("aurora_root")) {
                            a.c = true;
                            c = 0;
                            aVar3.a();
                        } else {
                            aVar3.a();
                            i2 = 1;
                        }
                    }
                }
            } else {
                c = 0;
            }
        }
        return i2;
    }

    public static int c(Context context) {
        return a(0, context);
    }

    public static int b(int i, Context context) {
        if (f266a == null) {
            return -1;
        }
        if (f266a != null) {
            if (!a.d.exists() || !a.d.isFile()) {
                try {
                    Runtime.getRuntime().exec(f266a + " -c " + ("/data/data/" + AstApp.i().getPackageName() + "/lib/libaurora.so"));
                } catch (IOException e) {
                    e.printStackTrace();
                    return -1;
                }
            } else {
                try {
                    Runtime.getRuntime().exec(f266a + " -c " + a.d.getAbsolutePath());
                } catch (IOException e2) {
                    e2.printStackTrace();
                    return -1;
                }
            }
        }
        return 0;
    }

    public static c a(String str, String str2, int i) {
        if (i <= 0) {
            i = 1500;
        }
        c cVar = new c();
        a aVar = new a();
        c b2 = aVar.b(str2, i);
        aVar.a();
        if (b2 == null) {
            cVar.d = 3;
        } else {
            if (b2.f262a == 200 || b2.f262a == 201) {
                cVar.d = 0;
            }
            if (b2.b != null) {
                cVar.b = r.a(b2.b);
            }
        }
        return cVar;
    }

    public static c b(String str) {
        c cVar = new c();
        a aVar = new a();
        c b2 = aVar.b(str, 2222);
        aVar.a();
        if (b2 == null) {
            cVar.d = 3;
        } else {
            if (b2.f262a == 200 || b2.f262a == 201) {
                cVar.d = 0;
            }
            if (b2.b != null) {
                cVar.b = r.a(b2.b);
            }
        }
        return cVar;
    }
}
