package com.qq.d.g;

/* compiled from: ProGuard */
public class e extends r {
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0068  */
    /* JADX WARNING: Removed duplicated region for block: B:30:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(java.util.Map<java.lang.String, java.lang.String> r9, boolean r10, com.qq.d.g.s r11) {
        /*
            r8 = this;
            r2 = 1
            r1 = 0
            java.lang.String r0 = "ro.rommanager.developerid"
            java.lang.String r0 = android.os.SystemProperties.get(r0)
            java.lang.String r3 = "ro.cm.version"
            java.lang.String r3 = android.os.SystemProperties.get(r3)
            java.lang.String r4 = "ro.build.date.utc"
            java.lang.String r4 = android.os.SystemProperties.get(r4)
            boolean r5 = android.text.TextUtils.isEmpty(r0)
            if (r5 != 0) goto L_0x007c
            java.lang.String r5 = r0.toLowerCase()
            java.lang.String r6 = "smartisan"
            boolean r5 = r5.equals(r6)
            java.lang.String r0 = r0.toLowerCase()
            java.lang.String r6 = "smartisannightly"
            boolean r0 = r0.equals(r6)
            if (r5 != 0) goto L_0x0032
            if (r0 == 0) goto L_0x007c
        L_0x0032:
            java.lang.String r6 = "rombrand"
            java.lang.String r7 = "smartisan"
            r8.a(r9, r6, r7)
            if (r5 == 0) goto L_0x005e
            java.lang.String r0 = "rombranch"
            java.lang.String r5 = "release"
            r8.a(r9, r0, r5)
        L_0x0042:
            boolean r0 = android.text.TextUtils.isEmpty(r3)
            if (r0 != 0) goto L_0x004d
            java.lang.String r0 = "romversion"
            r8.a(r9, r0, r3)
        L_0x004d:
            boolean r0 = android.text.TextUtils.isEmpty(r4)
            if (r0 != 0) goto L_0x0058
            java.lang.String r0 = "builddate"
            r8.a(r9, r0, r4)
        L_0x0058:
            r0 = r2
        L_0x0059:
            com.qq.d.g.r r3 = r8.b
            if (r3 != 0) goto L_0x0068
        L_0x005d:
            return r0
        L_0x005e:
            if (r0 == 0) goto L_0x0042
            java.lang.String r0 = "rombranch"
            java.lang.String r5 = "alpha"
            r8.a(r9, r0, r5)
            goto L_0x0042
        L_0x0068:
            com.qq.d.g.r r4 = r8.b
            if (r10 != 0) goto L_0x006e
            if (r0 == 0) goto L_0x007a
        L_0x006e:
            r3 = r2
        L_0x006f:
            boolean r3 = r4.a(r9, r3, r11)
            if (r0 != 0) goto L_0x0077
            if (r3 == 0) goto L_0x0078
        L_0x0077:
            r1 = r2
        L_0x0078:
            r0 = r1
            goto L_0x005d
        L_0x007a:
            r3 = r1
            goto L_0x006f
        L_0x007c:
            r0 = r1
            goto L_0x0059
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.d.g.e.a(java.util.Map, boolean, com.qq.d.g.s):boolean");
    }
}
