package com.qq.d.g;

import android.content.Context;
import android.os.Build;

/* compiled from: ProGuard */
public final class s {

    /* renamed from: a  reason: collision with root package name */
    private String f285a = Build.VERSION.INCREMENTAL;
    private String b = Build.ID;
    private String c = Build.DISPLAY;
    private String d = Build.VERSION.RELEASE;
    private Context e;

    public s(Context context) {
        this.e = context;
    }

    public String a() {
        return this.f285a;
    }

    public String b() {
        return this.b;
    }

    public String c() {
        return this.c;
    }

    public Context d() {
        return this.e;
    }

    public String e() {
        return this.d;
    }
}
