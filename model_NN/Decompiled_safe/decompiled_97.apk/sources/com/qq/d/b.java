package com.qq.d;

import com.qq.AppService.r;
import com.qq.provider.h;
import com.tencent.tmsecurelite.commom.DataEntity;
import com.tencent.tmsecurelite.commom.DataEntityKeyConst;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;

/* compiled from: ProGuard */
public class b {

    /* renamed from: a  reason: collision with root package name */
    public int f270a = 0;
    public String b = null;
    public String c = null;
    public String d = null;
    public String e = null;
    public String f = null;
    public String g = null;
    public int h = 0;
    public int i = 0;
    public int j = 0;

    public static ArrayList<b> a(List<DataEntity> list) {
        if (list == null) {
            return null;
        }
        ArrayList<b> arrayList = new ArrayList<>();
        int size = list.size();
        for (int i2 = 0; i2 < size; i2++) {
            DataEntity dataEntity = list.get(i2);
            if (dataEntity != null) {
                b bVar = new b(0, dataEntity);
                if (bVar.i >= 3) {
                    arrayList.add(bVar);
                }
            }
        }
        return arrayList;
    }

    public b(int i2, DataEntity dataEntity) {
        this.f270a = i2;
        if (dataEntity != null) {
            try {
                this.c = dataEntity.getString(DataEntityKeyConst.PackageName_STR);
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
            try {
                this.b = dataEntity.getString(DataEntityKeyConst.AppName_STR);
            } catch (JSONException e3) {
                e3.printStackTrace();
            }
            try {
                this.i = dataEntity.getInt(DataEntityKeyConst.VirusType_INT);
            } catch (JSONException e4) {
                e4.printStackTrace();
            }
            try {
                this.e = dataEntity.getString(DataEntityKeyConst.VirusDescribe_STR);
            } catch (JSONException e5) {
                e5.printStackTrace();
            }
            try {
                this.f = dataEntity.getString(DataEntityKeyConst.VirusLabel_STR);
            } catch (JSONException e6) {
                e6.printStackTrace();
            }
            try {
                this.g = dataEntity.getString(DataEntityKeyConst.VirusName_STR);
            } catch (JSONException e7) {
                e7.printStackTrace();
            }
            try {
                this.h = dataEntity.getInt(DataEntityKeyConst.VirusAdvise_INT);
            } catch (JSONException e8) {
                e8.printStackTrace();
            }
            try {
                this.j = dataEntity.getInt(DataEntityKeyConst.VirusId_INT);
            } catch (JSONException e9) {
                e9.printStackTrace();
            }
        }
    }

    public static byte[] a(int i2, b bVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(16));
        arrayList.add(r.a(i2));
        arrayList.add(r.a(bVar.f270a));
        arrayList.add(r.a(bVar.b));
        arrayList.add(r.a(bVar.c));
        arrayList.add(r.a(bVar.d));
        arrayList.add(r.a(bVar.e));
        arrayList.add(r.a(bVar.f));
        arrayList.add(r.a(bVar.g));
        arrayList.add(r.a(bVar.h));
        arrayList.add(r.a(bVar.i));
        arrayList.add(r.a(bVar.j));
        byte[] a2 = h.a(arrayList);
        System.arraycopy(r.a(a2.length), 0, a2, 0, 4);
        return a2;
    }

    public static byte[] a(int i2, ArrayList<b> arrayList) {
        int i3;
        int i4;
        ArrayList arrayList2 = new ArrayList();
        arrayList2.add(r.a(16));
        arrayList2.add(r.a(i2));
        if (arrayList != null) {
            i3 = arrayList.size();
        } else {
            i3 = 0;
        }
        arrayList2.add(r.a(i3));
        if (i3 > 0) {
            int i5 = 0;
            int i6 = 0;
            while (i5 < i3) {
                b bVar = arrayList.get(i5);
                if (bVar == null) {
                    i4 = i6;
                } else {
                    arrayList2.add(r.a(bVar.b));
                    arrayList2.add(r.a(bVar.c));
                    arrayList2.add(r.a(bVar.d));
                    arrayList2.add(r.a(bVar.e));
                    arrayList2.add(r.a(bVar.f));
                    arrayList2.add(r.a(bVar.g));
                    arrayList2.add(r.a(bVar.h));
                    arrayList2.add(r.a(bVar.i));
                    arrayList2.add(r.a(bVar.j));
                    i4 = i6 + 1;
                }
                i5++;
                i6 = i4;
            }
            if (i3 != i6) {
                arrayList2.set(2, r.a(i6));
            }
        }
        byte[] a2 = h.a(arrayList2);
        System.arraycopy(r.a(a2.length), 0, a2, 0, 4);
        return a2;
    }
}
