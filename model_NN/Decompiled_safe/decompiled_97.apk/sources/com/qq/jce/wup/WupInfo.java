package com.qq.jce.wup;

import com.tencent.assistant.st.STConst;
import java.io.InputStream;
import java.util.Properties;

public class WupInfo {
    private static String clientBuilt;
    private static String clientInfo;
    private static String clientNumber;

    static {
        clientInfo = null;
        clientBuilt = null;
        clientNumber = null;
        try {
            InputStream is = WupInfo.class.getResourceAsStream("/com/qq/jce/wup/wup.properties");
            Properties props = new Properties();
            props.load(is);
            is.close();
            clientInfo = props.getProperty("client.info");
            clientBuilt = props.getProperty("client.built");
            clientNumber = props.getProperty("client.number");
        } catch (Throwable th) {
        }
        if (clientInfo == null) {
            clientInfo = "Tencent Taf";
        }
        if (clientBuilt == null) {
            clientBuilt = STConst.ST_INSTALL_FAIL_STR_UNKNOWN;
        }
        if (clientNumber == null) {
            clientNumber = STConst.ST_INSTALL_FAIL_STR_UNKNOWN;
        }
    }

    public static String getClientInfo() {
        return clientInfo;
    }

    public static String getClientBuilt() {
        return clientBuilt;
    }

    public static String getClientNumber() {
        return clientNumber;
    }

    public static String showString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Client version: " + getClientInfo() + "\n");
        sb.append("Client built:   " + getClientBuilt() + "\n");
        sb.append("Client number:  " + getClientNumber() + "\n");
        sb.append("OS Name:        " + System.getProperty("os.name") + "\n");
        sb.append("OS Version:     " + System.getProperty("os.version") + "\n");
        sb.append("Architecture:   " + System.getProperty("os.arch") + "\n");
        sb.append("JVM Version:    " + System.getProperty("java.runtime.version") + "\n");
        sb.append("JVM Vendor:     " + System.getProperty("java.vm.vendor") + "\n");
        return sb.toString();
    }
}
