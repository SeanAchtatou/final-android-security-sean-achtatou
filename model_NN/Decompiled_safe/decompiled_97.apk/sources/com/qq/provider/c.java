package com.qq.provider;

import android.content.ContentValues;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import com.qq.AppService.r;
import com.qq.a.a.b;
import com.qq.util.j;
import com.tencent.connect.common.Constants;
import com.tencent.securemodule.impl.ErrorCode;
import java.io.File;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class c {

    /* renamed from: a  reason: collision with root package name */
    public final String[] f333a = {"_id", "audio_id", "playlist_id", "play_order"};

    private c() {
    }

    public static c a() {
        return new c();
    }

    public void a(Context context, com.qq.g.c cVar) {
        File file;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        if (h <= 0) {
            cVar.a(1);
        } else if (cVar.e() < h + 1) {
            cVar.a(1);
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("_id");
            sb.append(" in ");
            sb.append(" (");
            for (int i = 0; i < h; i++) {
                int h2 = cVar.h();
                if (i != 0) {
                    sb.append(", ");
                }
                sb.append(h2);
            }
            sb.append(" )");
            String sb2 = sb.toString();
            Cursor query = context.getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, null, sb2, null, null);
            if (query == null) {
                cVar.a(8);
                return;
            }
            for (boolean moveToFirst = query.moveToFirst(); moveToFirst; moveToFirst = query.moveToNext()) {
                String string = query.getString(query.getColumnIndex("_data"));
                if (!(string == null || (file = new File(string)) == null || !file.exists())) {
                    file.delete();
                }
            }
            query.close();
            context.getContentResolver().delete(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, sb2, null);
            cVar.a(0);
        }
    }

    public void b(Context context, com.qq.g.c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        if (r.b(j) || !new File(j).exists()) {
            cVar.a(1);
            return;
        }
        b bVar = new b(context);
        bVar.a(j);
        int a2 = bVar.a();
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(a2));
        cVar.a(0);
        cVar.a(arrayList);
    }

    public void c(Context context, com.qq.g.c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        if (r.b(j) || !new File(j).exists()) {
            cVar.a(1);
            return;
        }
        new b(context).a(j);
        cVar.a(0);
    }

    public void d(Context context, com.qq.g.c cVar) {
        String str;
        String str2;
        Uri uri;
        if (cVar.e() < 3) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        String j = cVar.j();
        String j2 = cVar.j();
        if (r.b(j)) {
            str = null;
        } else {
            str = j;
        }
        if (r.b(j2)) {
            str2 = null;
        } else {
            str2 = j2;
        }
        if (h > 0) {
            uri = MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI;
        } else {
            uri = MediaStore.Audio.Albums.INTERNAL_CONTENT_URI;
        }
        Cursor query = context.getContentResolver().query(uri, null, "album = '" + str + "' and " + "artist" + " = '" + str2 + "'", null, null);
        if (query == null) {
            cVar.a(8);
        } else if (!query.moveToFirst()) {
            query.close();
            cVar.a(7);
        } else {
            String string = query.getString(query.getColumnIndex("album_art"));
            query.close();
            ArrayList arrayList = new ArrayList();
            arrayList.add(r.a(string));
            cVar.a(0);
            cVar.a(arrayList);
        }
    }

    public void e(Context context, com.qq.g.c cVar) {
        Uri uri;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        ArrayList arrayList = new ArrayList();
        if (cVar.h() > 0) {
            uri = MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI;
        } else {
            uri = MediaStore.Audio.Albums.INTERNAL_CONTENT_URI;
        }
        Cursor query = context.getContentResolver().query(uri, null, null, null, null);
        if (query == null) {
            cVar.a(8);
            return;
        }
        boolean moveToFirst = query.moveToFirst();
        arrayList.add(r.a(0));
        boolean z = moveToFirst;
        int i = 0;
        while (z) {
            String string = query.getString(query.getColumnIndex("album"));
            String string2 = query.getString(query.getColumnIndex("album_art"));
            String string3 = query.getString(query.getColumnIndex("artist"));
            arrayList.add(r.a(string));
            arrayList.add(r.a(string3));
            arrayList.add(r.a(string2));
            z = query.moveToNext();
            i++;
        }
        query.close();
        arrayList.set(0, r.a(i));
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void f(Context context, com.qq.g.c cVar) {
        Uri uri;
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        if (h > 0) {
            uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        } else {
            uri = MediaStore.Audio.Media.INTERNAL_CONTENT_URI;
        }
        Cursor query = context.getContentResolver().query(uri, j.c >= 8 ? new String[]{"_size", "is_ringtone", "is_music", "is_alarm", "is_notification", "is_podcast"} : new String[]{"_size", "is_ringtone", "is_music", "is_alarm", "is_notification"}, null, null, null);
        if (query == null) {
            cVar.a(8);
            return;
        }
        long j = 0;
        for (boolean moveToFirst = query.moveToFirst(); moveToFirst; moveToFirst = query.moveToNext()) {
            long j2 = query.getLong(query.getColumnIndex("_size"));
            if (h2 == 1) {
                if (query.getInt(query.getColumnIndex("is_ringtone")) > 0) {
                    j += j2;
                }
            } else if (h2 == 2) {
                if (query.getInt(query.getColumnIndex("is_music")) > 0) {
                    j += j2;
                }
            } else if (h2 == 4) {
                if (query.getInt(query.getColumnIndex("is_alarm")) > 0) {
                    j += j2;
                }
            } else if (h2 == 8) {
                if (query.getInt(query.getColumnIndex("is_notification")) > 0) {
                    j += j2;
                }
            } else if (h2 != 16 || j.c < 8) {
                j += j2;
            } else if (query.getInt(query.getColumnIndex("is_podcast")) > 0) {
                j += j2;
            }
        }
        query.close();
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(j));
        cVar.a(0);
        cVar.a(arrayList);
    }

    public void g(Context context, com.qq.g.c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        Uri withAppendedPath = Uri.withAppendedPath(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, Constants.STR_EMPTY + cVar.h());
        Cursor query = context.getContentResolver().query(withAppendedPath, null, null, null, null);
        if (query == null) {
            cVar.a(8);
        } else if (!query.moveToFirst()) {
            cVar.a(1);
            query.close();
        } else {
            String string = query.getString(query.getColumnIndex("_data"));
            query.close();
            if (context.getContentResolver().delete(withAppendedPath, null, null) > 0) {
                try {
                    new File(string).delete();
                } catch (Exception e) {
                }
            } else {
                cVar.a(8);
            }
        }
    }

    public void h(Context context, com.qq.g.c cVar) {
        Uri uri;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        if (cVar.h() > 0) {
            uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        } else {
            uri = MediaStore.Audio.Media.INTERNAL_CONTENT_URI;
        }
        Cursor query = context.getContentResolver().query(uri, null, null, null, null);
        if (query == null) {
            cVar.a(8);
            return;
        }
        int count = query.getCount();
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(count));
        for (boolean moveToFirst = query.moveToFirst(); moveToFirst; moveToFirst = query.moveToNext()) {
            arrayList.add(r.a(query.getInt(query.getColumnIndex("_id"))));
        }
        query.close();
        cVar.a(0);
        cVar.a(arrayList);
    }

    public void i(Context context, com.qq.g.c cVar) {
        Uri uri;
        byte[] bArr;
        byte[] bArr2;
        byte[] bArr3;
        byte[] bArr4;
        byte[] bArr5;
        byte[] bArr6;
        byte[] bArr7;
        if (cVar.e() < 3) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        int h3 = cVar.h();
        if (h2 > 0) {
            uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        } else {
            uri = MediaStore.Audio.Media.INTERNAL_CONTENT_URI;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(0));
        String str = null;
        if (h3 == 9) {
            str = "_id > " + h + " and (" + "is_ringtone" + " > 0 " + " or " + "is_notification" + " > 0 " + ") ";
        } else if (h3 == 1) {
            str = "_id > " + h + " and " + "is_ringtone" + " > 0";
        } else if (h3 == 2) {
            str = "_id > " + h + " and " + "is_music" + " > 0";
        } else if (h3 == 3) {
            str = "_id > " + h + " and (" + "is_ringtone" + " > 0 " + " or " + "is_music" + " > 0 " + ") ";
        } else if (h3 == 4) {
            str = "_id > " + h + " and " + "is_alarm" + " > 0";
        } else if (h3 == 5) {
            str = "_id > " + h + " and (" + "is_alarm" + " > 0 " + " or " + "is_ringtone" + " > 0 " + ") ";
        } else if (h3 == 6) {
            str = "_id > " + h + " and (" + "is_alarm" + " > 0 " + " or " + "is_music" + " > 0 " + ") ";
        } else if (h3 == 7) {
            str = "_id > " + h + " and (" + "is_alarm" + " > 0 " + " or " + "is_music" + " > 0 " + " or " + "is_ringtone" + " > 0 " + ") ";
        } else if (h3 == 10) {
            str = "_id > " + h + " and (" + "is_music" + " > 0 " + " or " + "is_notification" + " > 0 " + ") ";
        } else if (h3 == 11) {
            str = "_id > " + h + " and (" + "is_ringtone" + " > 0 " + " or " + "is_notification" + " > 0 " + " or " + "is_music" + " > 0" + ") ";
        } else if (h3 == 12) {
            str = "_id > " + h + " and (" + "is_alarm" + " > 0 " + " or " + "is_notification" + " > 0 " + ") ";
        } else if (h3 == 13) {
            str = "_id > " + h + " and (" + "is_ringtone" + " > 0 " + " or " + "is_notification" + " > 0 " + " or " + "is_alarm" + " > 0 ) ";
        } else if (h3 == 14) {
            str = "_id > " + h + " and (" + "is_alarm" + " > 0 " + " or " + "is_notification" + " > 0 " + " or " + "is_music" + " > 0" + ") ";
        } else if (h3 == 15) {
            str = "_id > " + h + " and (" + "is_alarm" + " > 0 " + " or " + "is_notification" + " > 0 " + " or " + "is_music" + " > 0" + " or " + "is_ringtone" + " > 0" + ") ";
        } else if (h3 == 8) {
            str = "_id > " + h + " and " + "is_notification" + " > 0";
        } else if (h3 == 16 && j.c >= 8) {
            str = "_id > " + h + " and " + "is_podcast" + " > 0";
        }
        Cursor query = context.getContentResolver().query(uri, null, str, null, "_id");
        if (query == null) {
            cVar.a(8);
            return;
        }
        int i = 0;
        for (boolean moveToFirst = query.moveToFirst(); moveToFirst; moveToFirst = query.moveToNext()) {
            int i2 = query.getInt(query.getColumnIndex("_id"));
            byte[] blob = query.getBlob(query.getColumnIndex("_data"));
            byte[] blob2 = query.getBlob(query.getColumnIndex("_display_name"));
            long j = query.getLong(query.getColumnIndex("_size"));
            byte[] blob3 = query.getBlob(query.getColumnIndex("title"));
            byte[] blob4 = query.getBlob(query.getColumnIndex("mime_type"));
            long j2 = 1000 * query.getLong(query.getColumnIndex("date_added"));
            long j3 = 1000 * query.getLong(query.getColumnIndex("date_modified"));
            long j4 = query.getLong(query.getColumnIndex("duration"));
            byte[] blob5 = query.getBlob(query.getColumnIndex("artist"));
            byte[] blob6 = query.getBlob(query.getColumnIndex("composer"));
            byte[] blob7 = query.getBlob(query.getColumnIndex("album"));
            int i3 = query.getInt(query.getColumnIndex("track"));
            int i4 = query.getInt(query.getColumnIndex("year"));
            int i5 = query.getInt(query.getColumnIndex("is_ringtone"));
            int i6 = query.getInt(query.getColumnIndex("is_music"));
            int i7 = query.getInt(query.getColumnIndex("is_alarm"));
            int i8 = query.getInt(query.getColumnIndex("is_notification"));
            int i9 = 0;
            if (j.c >= 8) {
                i9 = query.getInt(query.getColumnIndex("is_podcast"));
            }
            int i10 = (i5 * 1) + (i6 * 2) + (i7 * 4) + (i8 * 8) + (i9 * 16);
            arrayList.add(r.a(i2));
            if (blob == null) {
                bArr = r.hr;
            } else {
                bArr = blob;
            }
            arrayList.add(bArr);
            if (blob2 == null) {
                bArr2 = r.hr;
            } else {
                bArr2 = blob2;
            }
            arrayList.add(bArr2);
            arrayList.add(r.a(j));
            if (blob3 == null) {
                bArr3 = r.hr;
            } else {
                bArr3 = blob3;
            }
            arrayList.add(bArr3);
            if (blob4 == null) {
                bArr4 = r.hr;
            } else {
                bArr4 = blob4;
            }
            arrayList.add(bArr4);
            arrayList.add(r.a(j2));
            arrayList.add(r.a(j3));
            arrayList.add(r.a(j4));
            if (blob5 == null) {
                bArr5 = r.hr;
            } else {
                bArr5 = blob5;
            }
            arrayList.add(bArr5);
            if (blob6 == null) {
                bArr6 = r.hr;
            } else {
                bArr6 = blob6;
            }
            arrayList.add(bArr6);
            if (blob7 == null) {
                bArr7 = r.hr;
            } else {
                bArr7 = blob7;
            }
            arrayList.add(bArr7);
            arrayList.add(r.a(i3));
            arrayList.add(r.a(i4));
            arrayList.add(r.a(i10));
            i++;
        }
        query.close();
        arrayList.set(0, r.a(i));
        cVar.a(0);
        cVar.a(arrayList);
    }

    public void j(Context context, com.qq.g.c cVar) {
        Uri uri;
        String str;
        int i;
        int i2;
        byte[] bArr;
        byte[] bArr2;
        byte[] bArr3;
        byte[] bArr4;
        byte[] bArr5;
        byte[] bArr6;
        byte[] bArr7;
        if (cVar.e() < 4) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        int h3 = cVar.h();
        int h4 = cVar.h();
        if (h2 <= 0) {
            cVar.a(1);
            return;
        }
        if (h3 > 0) {
            uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        } else {
            uri = MediaStore.Audio.Media.INTERNAL_CONTENT_URI;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(0));
        int i3 = 0;
        if (h4 == 9) {
            str = " and (is_ringtone > 0  or is_notification > 0 ) ";
        } else if (h4 == 1) {
            str = " and is_ringtone > 0";
        } else if (h4 == 2) {
            str = " and is_music > 0";
        } else if (h4 == 3) {
            str = " and (is_ringtone > 0  or is_music > 0 ) ";
        } else if (h4 == 4) {
            str = " and is_alarm > 0";
        } else if (h4 == 5) {
            str = " and (is_alarm > 0  or is_ringtone > 0 ) ";
        } else if (h4 == 6) {
            str = " and (is_alarm > 0  or is_music > 0 ) ";
        } else if (h4 == 7) {
            str = " and (is_alarm > 0  or is_music > 0  or is_ringtone > 0 ) ";
        } else if (h4 == 10) {
            str = " and (is_music > 0  or is_notification > 0 ) ";
        } else if (h4 == 11) {
            str = " and (is_ringtone > 0  or is_notification > 0  or is_music > 0) ";
        } else if (h4 == 12) {
            str = " and (is_alarm > 0  or is_notification > 0 ) ";
        } else if (h4 == 13) {
            str = " and (is_ringtone > 0  or is_notification > 0  or is_alarm > 0 ) ";
        } else if (h4 == 14) {
            str = " and (is_alarm > 0  or is_notification > 0  or is_music > 0) ";
        } else if (h4 == 15) {
            str = " and (is_alarm > 0  or is_notification > 0  or is_music > 0 or is_ringtone > 0) ";
        } else if (h4 == 8) {
            str = " and is_notification > 0";
        } else if (h4 != 16 || j.c < 8) {
            str = null;
        } else {
            str = " and is_podcast > 0";
        }
        int i4 = -1;
        if (h <= 0) {
            Cursor query = context.getContentResolver().query(uri, j.c >= 8 ? new String[]{"_id", "is_ringtone", "is_music", "is_alarm", "is_notification", "is_podcast"} : new String[]{"_id", "is_ringtone", "is_music", "is_alarm", "is_notification"}, "_id > 0" + str, null, "_id");
            if (query == null) {
                Log.d("com.qq.connect", "could not open the database !");
                cVar.a(8);
                return;
            } else if (!query.moveToLast()) {
                query.close();
                cVar.a(arrayList);
                cVar.a(0);
                return;
            } else {
                h = query.getInt(0) + 1;
                i4 = query.getCount();
                Log.d("com.qq.connect", " count:" + query.getCount() + " lastID:" + (h - 1));
                query.close();
            }
        }
        int i5 = i4;
        while (i3 < h2 && h > 0) {
            int i6 = (h - h2) - 1000;
            if (i5 != -1 && i5 < h2) {
                i6 = 0;
            } else if (i5 != -1 && i5 < h2 * 2) {
                i6 += ErrorCode.ERR_RECEIVE;
            }
            if (i6 < 0) {
                i = 0;
            } else {
                i = i6;
            }
            Cursor query2 = context.getContentResolver().query(uri, null, "_id" + '<' + h + str + " and " + "_id" + " >= " + i, null, "_id");
            if (query2 == null) {
                cVar.a(8);
                return;
            }
            int count = query2.getCount();
            if (count > h2) {
                count = h2;
            }
            int i7 = 0;
            int i8 = i3;
            int i9 = 0;
            boolean moveToLast = query2.moveToLast();
            while (i7 < count && moveToLast) {
                int i10 = query2.getInt(query2.getColumnIndex("_id"));
                byte[] blob = query2.getBlob(query2.getColumnIndex("_data"));
                byte[] blob2 = query2.getBlob(query2.getColumnIndex("_display_name"));
                long j = query2.getLong(query2.getColumnIndex("_size"));
                byte[] blob3 = query2.getBlob(query2.getColumnIndex("title"));
                byte[] blob4 = query2.getBlob(query2.getColumnIndex("mime_type"));
                long j2 = 1000 * query2.getLong(query2.getColumnIndex("date_added"));
                long j3 = 1000 * query2.getLong(query2.getColumnIndex("date_modified"));
                long j4 = query2.getLong(query2.getColumnIndex("duration"));
                byte[] blob5 = query2.getBlob(query2.getColumnIndex("artist"));
                byte[] blob6 = query2.getBlob(query2.getColumnIndex("composer"));
                byte[] blob7 = query2.getBlob(query2.getColumnIndex("album"));
                int i11 = query2.getInt(query2.getColumnIndex("track"));
                int i12 = query2.getInt(query2.getColumnIndex("year"));
                int i13 = query2.getInt(query2.getColumnIndex("is_ringtone"));
                int i14 = query2.getInt(query2.getColumnIndex("is_music"));
                int i15 = query2.getInt(query2.getColumnIndex("is_alarm"));
                int i16 = query2.getInt(query2.getColumnIndex("is_notification"));
                int i17 = 0;
                if (j.c >= 8) {
                    i17 = query2.getInt(query2.getColumnIndex("is_podcast"));
                }
                int i18 = (i13 * 1) + (i14 * 2) + (i15 * 4) + (i16 * 8) + (i17 * 16);
                arrayList.add(r.a(i10));
                if (blob == null) {
                    bArr = r.hr;
                } else {
                    bArr = blob;
                }
                arrayList.add(bArr);
                if (blob2 == null) {
                    bArr2 = r.hr;
                } else {
                    bArr2 = blob2;
                }
                arrayList.add(bArr2);
                arrayList.add(r.a(j));
                if (blob3 == null) {
                    bArr3 = r.hr;
                } else {
                    bArr3 = blob3;
                }
                arrayList.add(bArr3);
                if (blob4 == null) {
                    bArr4 = r.hr;
                } else {
                    bArr4 = blob4;
                }
                arrayList.add(bArr4);
                arrayList.add(r.a(j2));
                arrayList.add(r.a(j3));
                arrayList.add(r.a(j4));
                if (blob5 == null) {
                    bArr5 = r.hr;
                } else {
                    bArr5 = blob5;
                }
                arrayList.add(bArr5);
                if (blob6 == null) {
                    bArr6 = r.hr;
                } else {
                    bArr6 = blob6;
                }
                arrayList.add(bArr6);
                if (blob7 == null) {
                    bArr7 = r.hr;
                } else {
                    bArr7 = blob7;
                }
                arrayList.add(bArr7);
                arrayList.add(r.a(i11));
                arrayList.add(r.a(i12));
                arrayList.add(r.a(i18));
                i8++;
                moveToLast = query2.moveToPrevious();
                i7++;
                i9 = i10;
            }
            if (i9 > 0) {
                i2 = i9;
            } else {
                i2 = i;
            }
            if (i2 > 0) {
                int i19 = (i2 - h2) - 100;
            }
            query2.close();
            i3 = i8;
            h = i2;
        }
        arrayList.set(0, r.a(i3));
        cVar.a(0);
        cVar.a(arrayList);
    }

    public void k(Context context, com.qq.g.c cVar) {
        Uri withAppendedPath;
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        if (cVar.h() > 0) {
            withAppendedPath = Uri.withAppendedPath(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, Constants.STR_EMPTY + h);
        } else {
            withAppendedPath = Uri.withAppendedPath(MediaStore.Audio.Media.INTERNAL_CONTENT_URI, Constants.STR_EMPTY + h);
        }
        Cursor query = context.getContentResolver().query(withAppendedPath, null, null, null, null);
        if (query == null) {
            cVar.a(8);
            return;
        }
        if (query.moveToFirst()) {
            ArrayList arrayList = new ArrayList();
            String string = query.getString(query.getColumnIndex("_data"));
            String string2 = query.getString(query.getColumnIndex("_display_name"));
            long j = query.getLong(query.getColumnIndex("_size"));
            String string3 = query.getString(query.getColumnIndex("title"));
            String string4 = query.getString(query.getColumnIndex("mime_type"));
            String b = r.b(query.getLong(query.getColumnIndex("date_added")) * 1000);
            String b2 = r.b(query.getLong(query.getColumnIndex("date_modified")) * 1000);
            long j2 = query.getLong(query.getColumnIndex("duration"));
            String string5 = query.getString(query.getColumnIndex("artist"));
            String string6 = query.getString(query.getColumnIndex("composer"));
            String string7 = query.getString(query.getColumnIndex("album"));
            int i = query.getInt(query.getColumnIndex("track"));
            int i2 = query.getInt(query.getColumnIndex("year"));
            int i3 = query.getInt(query.getColumnIndex("is_ringtone"));
            int i4 = query.getInt(query.getColumnIndex("is_music"));
            int i5 = query.getInt(query.getColumnIndex("is_alarm"));
            int i6 = query.getInt(query.getColumnIndex("is_notification"));
            arrayList.add(r.a(h));
            arrayList.add(r.a(string));
            arrayList.add(r.a(string2));
            arrayList.add(r.a(j));
            arrayList.add(r.a(string3));
            arrayList.add(r.a(string4));
            arrayList.add(r.a(b));
            arrayList.add(r.a(b2));
            arrayList.add(r.a(j2));
            arrayList.add(r.a(string5));
            arrayList.add(r.a(string6));
            arrayList.add(r.a(string7));
            arrayList.add(r.a(i));
            arrayList.add(r.a(i2));
            arrayList.add(r.a((i3 * 1) + (i4 * 2) + (i5 * 4) + (i6 * 8) + (query.getInt(query.getColumnIndex("is_podcast")) * 16)));
            cVar.a(arrayList);
            cVar.a(0);
        } else {
            cVar.a(1);
        }
        query.close();
    }

    public void l(Context context, com.qq.g.c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        if (r.b(j)) {
            cVar.a(1);
            return;
        }
        File file = new File(j);
        if (!file.exists()) {
            cVar.a(1);
            return;
        }
        Cursor query = context.getContentResolver().query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, null, "_data = '" + file.getAbsolutePath() + "'", null, null);
        if (query == null) {
            cVar.a(8);
            return;
        }
        ArrayList arrayList = new ArrayList();
        if (query.moveToFirst()) {
            arrayList.add(r.a(query.getInt(query.getColumnIndex("_id"))));
            cVar.a(0);
        } else {
            cVar.a(7);
        }
        query.close();
        cVar.a(arrayList);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void m(Context context, com.qq.g.c cVar) {
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        int h = cVar.h();
        if (r.b(j)) {
            cVar.a(1);
            return;
        }
        File file = new File(j);
        if (!file.exists()) {
            cVar.a(1);
            return;
        }
        ContentValues contentValues = new ContentValues();
        if (j.c >= 8) {
            if (h >= 16) {
                contentValues.put("is_podcast", (Integer) 1);
            } else {
                contentValues.put("is_podcast", (Integer) 0);
            }
        }
        int i = h % 16;
        if (i >= 8) {
            contentValues.put("is_notification", (Integer) 1);
        } else {
            contentValues.put("is_notification", (Integer) 0);
        }
        int i2 = i % 8;
        if (i2 >= 4) {
            contentValues.put("is_alarm", (Integer) 1);
        } else {
            contentValues.put("is_alarm", (Integer) 0);
        }
        int i3 = i2 % 4;
        if (i3 >= 2) {
            contentValues.put("is_music", (Integer) 1);
        } else {
            contentValues.put("is_music", (Integer) 0);
        }
        if (i3 % 2 == 1) {
            contentValues.put("is_ringtone", (Integer) 1);
        } else {
            contentValues.put("is_ringtone", (Integer) 0);
        }
        if (context.getContentResolver().update(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, contentValues, "_data = '" + file.getAbsolutePath() + "'", null) > 0) {
            cVar.a(0);
        } else {
            cVar.a(1);
        }
    }

    public void n(Context context, com.qq.g.c cVar) {
        Uri uri;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        if (h <= 0 || Environment.getExternalStorageState().equals("mounted")) {
            if (h > 0) {
                uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            } else {
                uri = MediaStore.Audio.Media.INTERNAL_CONTENT_URI;
            }
            Cursor query = context.getContentResolver().query(uri, null, null, null, null);
            if (query == null) {
                cVar.a(8);
                return;
            }
            int count = query.getCount();
            ArrayList arrayList = new ArrayList();
            arrayList.add(r.a(count));
            query.close();
            cVar.a(0);
            cVar.a(arrayList);
            return;
        }
        cVar.a(9);
    }

    public void o(Context context, com.qq.g.c cVar) {
        Uri uri;
        String str;
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        if (h < 16 || j.c >= 8) {
            if (cVar.h() > 0) {
                uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            } else {
                uri = MediaStore.Audio.Media.INTERNAL_CONTENT_URI;
            }
            if (h >= 16) {
                str = "is_podcast = 1";
            } else if (h >= 8) {
                str = "is_notification = 1";
            } else if (h >= 4) {
                str = "is_alarm = 1";
            } else if (h >= 2) {
                str = "is_music = 1";
            } else if (h >= 1) {
                str = "is_ringtone = 1 ";
            } else {
                str = null;
            }
            Cursor query = context.getContentResolver().query(uri, null, str, null, null);
            if (query == null) {
                cVar.a(8);
                return;
            }
            int count = query.getCount();
            query.close();
            ArrayList arrayList = new ArrayList();
            arrayList.add(r.a(count));
            cVar.a(arrayList);
            cVar.a(0);
            return;
        }
        cVar.a(4);
    }

    public void p(Context context, com.qq.g.c cVar) {
        Uri withAppendedPath;
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        if (cVar.h() > 0) {
            withAppendedPath = Uri.withAppendedPath(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, Constants.STR_EMPTY + h);
        } else {
            withAppendedPath = Uri.withAppendedPath(MediaStore.Audio.Media.INTERNAL_CONTENT_URI, Constants.STR_EMPTY + h);
        }
        Cursor query = context.getContentResolver().query(withAppendedPath, null, null, null, null);
        if (query == null) {
            cVar.a(8);
        } else if (!query.moveToFirst()) {
            query.close();
            cVar.a(1);
        } else {
            String string = query.getString(query.getColumnIndex("_data"));
            query.close();
            ArrayList arrayList = new ArrayList();
            arrayList.add(r.a(string));
            cVar.a(arrayList);
            cVar.a(0);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void q(Context context, com.qq.g.c cVar) {
        Uri withAppendedPath;
        if (cVar.e() < 3) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        int h3 = cVar.h();
        if (h2 > 0) {
            withAppendedPath = Uri.withAppendedPath(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, Constants.STR_EMPTY + h);
        } else {
            withAppendedPath = Uri.withAppendedPath(MediaStore.Audio.Media.INTERNAL_CONTENT_URI, Constants.STR_EMPTY + h);
        }
        ContentValues contentValues = new ContentValues();
        if (j.c >= 8) {
            if (h3 >= 16) {
                contentValues.put("is_podcast", (Integer) 1);
            } else {
                contentValues.put("is_podcast", (Integer) 0);
            }
        }
        int i = h3 % 16;
        if (i >= 8) {
            contentValues.put("is_notification", (Integer) 1);
        } else {
            contentValues.put("is_notification", (Integer) 0);
        }
        int i2 = i % 8;
        if (i2 >= 4) {
            contentValues.put("is_alarm", (Integer) 1);
        } else {
            contentValues.put("is_alarm", (Integer) 0);
        }
        int i3 = i2 % 4;
        if (i3 >= 2) {
            contentValues.put("is_music", (Integer) 1);
        } else {
            contentValues.put("is_music", (Integer) 0);
        }
        if (i3 % 2 == 1) {
            contentValues.put("is_ringtone", (Integer) 1);
        } else {
            contentValues.put("is_ringtone", (Integer) 0);
        }
        if (context.getContentResolver().update(withAppendedPath, contentValues, null, null) > 0) {
            cVar.a(0);
        } else {
            cVar.a(1);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void r(Context context, com.qq.g.c cVar) {
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        Uri withAppendedPath = Uri.withAppendedPath(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, Constants.STR_EMPTY + h);
        ContentValues contentValues = new ContentValues();
        if (j.c >= 8) {
            if (h2 >= 16) {
                contentValues.put("is_podcast", (Integer) 1);
            } else {
                contentValues.put("is_podcast", (Integer) 0);
            }
        }
        int i = h2 % 16;
        if (i >= 8) {
            contentValues.put("is_notification", (Integer) 1);
        } else {
            contentValues.put("is_notification", (Integer) 0);
        }
        int i2 = i % 8;
        if (i2 >= 4) {
            contentValues.put("is_alarm", (Integer) 1);
        } else {
            contentValues.put("is_alarm", (Integer) 0);
        }
        int i3 = i2 % 4;
        if (i3 >= 2) {
            contentValues.put("is_music", (Integer) 1);
        } else {
            contentValues.put("is_music", (Integer) 0);
        }
        if (i3 % 2 == 1) {
            contentValues.put("is_ringtone", (Integer) 1);
        } else {
            contentValues.put("is_ringtone", (Integer) 0);
        }
        if (context.getContentResolver().update(withAppendedPath, contentValues, null, null) > 0) {
            cVar.a(0);
        } else {
            cVar.a(1);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void s(Context context, com.qq.g.c cVar) {
        int i;
        String str;
        String str2;
        if (cVar.e() < 15) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        cVar.g();
        String j = cVar.j();
        cVar.g();
        String j2 = cVar.j();
        String j3 = cVar.j();
        cVar.g();
        cVar.g();
        cVar.g();
        cVar.g();
        cVar.g();
        cVar.g();
        cVar.g();
        cVar.g();
        int h2 = cVar.h();
        ContentValues contentValues = new ContentValues();
        if (h <= 0) {
            cVar.a(1);
            return;
        }
        if (r.b(j)) {
            contentValues.putNull("_display_name");
        } else {
            contentValues.put("_display_name", j);
        }
        if (r.b(j2)) {
            contentValues.putNull("title");
        } else {
            contentValues.put("title", j2);
        }
        if (r.b(j3)) {
            contentValues.putNull("mime_type");
        } else {
            contentValues.put("mime_type", j3);
        }
        if (j.c >= 8) {
            if (h2 >= 16) {
                contentValues.put("is_podcast", (Integer) 1);
            } else {
                contentValues.put("is_podcast", (Integer) 0);
            }
        }
        int i2 = h2 % 16;
        if (i2 >= 8) {
            contentValues.put("is_notification", (Integer) 1);
        } else {
            contentValues.put("is_notification", (Integer) 0);
        }
        int i3 = i2 % 8;
        if (i3 >= 4) {
            contentValues.put("is_alarm", (Integer) 1);
        } else {
            contentValues.put("is_alarm", (Integer) 0);
        }
        int i4 = i3 % 4;
        if (i4 >= 2) {
            contentValues.put("is_music", (Integer) 1);
        } else {
            contentValues.put("is_music", (Integer) 0);
        }
        if (i4 % 2 == 1) {
            contentValues.put("is_ringtone", (Integer) 1);
        } else {
            contentValues.put("is_ringtone", (Integer) 0);
        }
        Uri withAppendedPath = Uri.withAppendedPath(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, Constants.STR_EMPTY + h);
        try {
            i = context.getContentResolver().update(withAppendedPath, contentValues, null, null);
        } catch (Throwable th) {
            th.printStackTrace();
            i = -1;
        }
        if (i == -1) {
            Cursor query = context.getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, null, "_id", null, null);
            if (query != null) {
                if (query.moveToFirst()) {
                    str2 = query.getString(query.getColumnIndex("album"));
                    str = query.getString(query.getColumnIndex("artist"));
                } else {
                    str = null;
                    str2 = null;
                }
                query.close();
            } else {
                str = null;
                str2 = null;
            }
            if (str2 != null) {
                contentValues.put("album", str2);
            } else {
                contentValues.putNull("album");
            }
            if (str != null) {
                contentValues.put("artist", str);
            } else {
                contentValues.putNull("artist");
            }
            context.getContentResolver().update(withAppendedPath, contentValues, null, null);
        }
        cVar.a(0);
        cVar.a((ArrayList<byte[]>) null);
    }

    public void t(Context context, com.qq.g.c cVar) {
        Uri withAppendedPath;
        if (cVar.e() < 3) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        int h3 = cVar.h();
        if (h2 == 1 || h2 == 2 || h2 == 4 || h2 == 7) {
            if (h3 > 0) {
                withAppendedPath = Uri.withAppendedPath(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, Constants.STR_EMPTY + h);
            } else {
                withAppendedPath = Uri.withAppendedPath(MediaStore.Audio.Media.INTERNAL_CONTENT_URI, Constants.STR_EMPTY + h);
            }
            Cursor query = context.getContentResolver().query(withAppendedPath, null, null, null, null);
            if (query == null) {
                cVar.a(8);
            } else if (!query.moveToFirst()) {
                cVar.a(1);
                query.close();
            } else {
                query.close();
                RingtoneManager.setActualDefaultRingtoneUri(context, h2, withAppendedPath);
                cVar.a(0);
            }
        } else {
            cVar.a(1);
        }
    }

    public void u(Context context, com.qq.g.c cVar) {
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        int h = cVar.h();
        if (r.b(j)) {
            cVar.a(1);
            return;
        }
        File file = new File(j);
        if (!file.exists()) {
            cVar.a(1);
        } else if (h == 1 || h == 2 || h == 4 || h == 7) {
            Cursor query = context.getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, null, "_data = '" + file.getAbsolutePath() + "'", null, null);
            if (query == null) {
                cVar.a(1);
            } else if (!query.moveToFirst()) {
                query.close();
                cVar.a(1);
            } else {
                int i = query.getInt(query.getColumnIndex("_id"));
                query.close();
                RingtoneManager.setActualDefaultRingtoneUri(context, h, Uri.withAppendedPath(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, Constants.STR_EMPTY + i));
                cVar.a(0);
            }
        } else {
            cVar.a(1);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0090, code lost:
        if (r9.equals(android.net.Uri.withAppendedPath(android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, com.tencent.connect.common.Constants.STR_EMPTY + r8)) == true) goto L_0x0092;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void v(android.content.Context r11, com.qq.g.c r12) {
        /*
            r10 = this;
            r8 = 7
            r6 = 0
            r7 = 1
            r2 = 0
            int r0 = r12.e()
            if (r0 >= r7) goto L_0x000e
            r12.a(r7)
        L_0x000d:
            return
        L_0x000e:
            int r0 = r12.h()
            r1 = 4
            if (r0 == r1) goto L_0x001e
            r1 = 2
            if (r0 == r1) goto L_0x001e
            if (r0 == r7) goto L_0x001e
            r12.a(r7)
            goto L_0x000d
        L_0x001e:
            android.net.Uri r1 = android.media.RingtoneManager.getDefaultUri(r0)
            if (r1 != 0) goto L_0x0028
            r12.a(r8)
            goto L_0x000d
        L_0x0028:
            android.content.ContentResolver r0 = r11.getContentResolver()
            r3 = r2
            r4 = r2
            r5 = r2
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)
            if (r0 != 0) goto L_0x003b
            r0 = 8
            r12.a(r0)
            goto L_0x000d
        L_0x003b:
            boolean r1 = r0.moveToFirst()
            if (r1 != 0) goto L_0x0048
            r0.close()
            r12.a(r8)
            goto L_0x000d
        L_0x0048:
            java.lang.String r1 = "value"
            int r1 = r0.getColumnIndex(r1)
            java.lang.String r1 = r0.getString(r1)
            android.net.Uri r9 = android.net.Uri.parse(r1)
            r0.close()
            if (r9 != 0) goto L_0x005f
            r12.a(r8)
            goto L_0x000d
        L_0x005f:
            java.lang.String r0 = r9.toString()
            java.lang.String r1 = "content"
            boolean r0 = r0.startsWith(r1)
            if (r0 == 0) goto L_0x00ad
            java.lang.String r0 = r9.getLastPathSegment()
            int r8 = java.lang.Integer.parseInt(r0)
            android.net.Uri r0 = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = ""
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r8)
            java.lang.String r1 = r1.toString()
            android.net.Uri r0 = android.net.Uri.withAppendedPath(r0, r1)
            boolean r0 = r9.equals(r0)
            if (r0 != r7) goto L_0x0142
        L_0x0092:
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            byte[] r1 = com.qq.AppService.r.a(r8)
            r0.add(r1)
            byte[] r1 = com.qq.AppService.r.a(r7)
            r0.add(r1)
            r12.a(r6)
            r12.a(r0)
            goto L_0x000d
        L_0x00ad:
            java.lang.String r0 = r9.toString()
            java.lang.String r1 = "/"
            boolean r0 = r0.startsWith(r1)
            if (r0 == 0) goto L_0x014a
            android.content.ContentResolver r0 = r11.getContentResolver()
            android.net.Uri r1 = android.provider.MediaStore.Audio.Media.INTERNAL_CONTENT_URI
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "_data = '"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = r9.toString()
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = "'"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r4 = r2
            r5 = r2
            android.database.Cursor r3 = r0.query(r1, r2, r3, r4, r5)
            if (r3 == 0) goto L_0x0147
            boolean r1 = r3.moveToFirst()
            if (r1 == 0) goto L_0x0145
            java.lang.String r0 = "_id"
            int r0 = r3.getColumnIndex(r0)
            int r0 = r3.getInt(r0)
        L_0x00f4:
            r3.close()
            r8 = r0
            r0 = r1
        L_0x00f9:
            if (r0 != 0) goto L_0x0142
            android.content.ContentResolver r0 = r11.getContentResolver()
            android.net.Uri r1 = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "_data = '"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = r9.toString()
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = "'"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r4 = r2
            r5 = r2
            android.database.Cursor r2 = r0.query(r1, r2, r3, r4, r5)
            if (r2 == 0) goto L_0x0142
            boolean r0 = r2.moveToFirst()
            if (r0 == 0) goto L_0x013f
            java.lang.String r0 = "_id"
            int r0 = r2.getColumnIndex(r0)
            int r0 = r2.getInt(r0)
            r1 = r0
            r0 = r7
        L_0x0138:
            r2.close()
            r7 = r0
            r8 = r1
            goto L_0x0092
        L_0x013f:
            r0 = r6
            r1 = r8
            goto L_0x0138
        L_0x0142:
            r7 = r6
            goto L_0x0092
        L_0x0145:
            r0 = r6
            goto L_0x00f4
        L_0x0147:
            r0 = r6
            r8 = r6
            goto L_0x00f9
        L_0x014a:
            r7 = r6
            r8 = r6
            goto L_0x0092
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.provider.c.v(android.content.Context, com.qq.g.c):void");
    }

    public void w(Context context, com.qq.g.c cVar) {
        Cursor query = context.getContentResolver().query(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI, null, null, null, null);
        if (query == null) {
            cVar.a(8);
            return;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(query.getCount()));
        for (boolean moveToFirst = query.moveToFirst(); moveToFirst; moveToFirst = query.moveToNext()) {
            int i = query.getInt(query.getColumnIndex("_id"));
            String string = query.getString(query.getColumnIndex("_data"));
            String string2 = query.getString(query.getColumnIndex("name"));
            long j = query.getLong(query.getColumnIndex("date_added"));
            long j2 = query.getLong(query.getColumnIndex("date_modified"));
            arrayList.add(r.a(i));
            arrayList.add(r.a(string));
            arrayList.add(r.a(string2));
            arrayList.add(r.a(r.b(j)));
            arrayList.add(r.a(r.b(j2)));
            Cursor query2 = context.getContentResolver().query(MediaStore.Audio.Playlists.Members.getContentUri("external", (long) i), this.f333a, null, null, null);
            if (query2 == null) {
                query.close();
                cVar.a(8);
                return;
            }
            arrayList.add(r.a(query2.getCount()));
            for (boolean moveToFirst2 = query2.moveToFirst(); moveToFirst2; moveToFirst2 = query2.moveToNext()) {
                int i2 = query2.getInt(query2.getColumnIndex("audio_id"));
                int i3 = query2.getInt(query2.getColumnIndex("play_order"));
                arrayList.add(r.a(i2));
                arrayList.add(r.a(i3));
            }
            query2.close();
        }
        query.close();
        cVar.a(0);
        cVar.a(arrayList);
    }

    public void x(Context context, com.qq.g.c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        Cursor query = context.getContentResolver().query(Uri.withAppendedPath(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI, Constants.STR_EMPTY + cVar.h()), null, null, null, null);
        if (query == null) {
            cVar.a(8);
            return;
        }
        boolean moveToFirst = query.moveToFirst();
        ArrayList arrayList = new ArrayList();
        if (moveToFirst) {
            int i = query.getInt(query.getColumnIndex("_id"));
            String string = query.getString(query.getColumnIndex("_data"));
            String string2 = query.getString(query.getColumnIndex("name"));
            long j = query.getLong(query.getColumnIndex("date_added"));
            long j2 = query.getLong(query.getColumnIndex("date_modified"));
            arrayList.add(r.a(i));
            arrayList.add(r.a(string));
            arrayList.add(r.a(string2));
            arrayList.add(r.a(r.b(j)));
            arrayList.add(r.a(r.b(j2)));
            query.moveToNext();
            cVar.a(0);
        } else {
            cVar.a(7);
        }
        query.close();
        cVar.a(arrayList);
    }

    public void y(Context context, com.qq.g.c cVar) {
        String str;
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        if (h == 0) {
            str = ("name = 'Favorite List'" + " or " + "name" + " = '" + "Default List" + "'") + " or " + "name" + " = '" + "Recent Add" + "'";
        } else if (h == 1) {
            str = "name = 'Favorite List'";
        } else if (h == 2) {
            str = "name = 'Default List'";
        } else if (h == 3) {
            str = "name = 'Recent Add'";
        } else {
            cVar.a(1);
            return;
        }
        Cursor query = context.getContentResolver().query(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI, null, str, null, null);
        if (query == null) {
            cVar.a(8);
            return;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(query.getCount()));
        for (boolean moveToFirst = query.moveToFirst(); moveToFirst; moveToFirst = query.moveToNext()) {
            int i = query.getInt(query.getColumnIndex("_id"));
            String string = query.getString(query.getColumnIndex("_data"));
            String string2 = query.getString(query.getColumnIndex("name"));
            long j = query.getLong(query.getColumnIndex("date_added"));
            long j2 = query.getLong(query.getColumnIndex("date_modified"));
            arrayList.add(r.a(i));
            arrayList.add(r.a(string));
            arrayList.add(r.a(string2));
            arrayList.add(r.a(r.b(j)));
            arrayList.add(r.a(r.b(j2)));
        }
        query.close();
        cVar.a(arrayList);
        cVar.a(0);
    }

    public void z(Context context, com.qq.g.c cVar) {
        Uri contentUri;
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        if (cVar.h() > 0) {
            contentUri = MediaStore.Audio.Playlists.Members.getContentUri("external", (long) h);
        } else {
            contentUri = MediaStore.Audio.Playlists.Members.getContentUri("internal", (long) h);
        }
        Cursor query = context.getContentResolver().query(contentUri, this.f333a, null, null, null);
        if (query == null) {
            cVar.a(8);
            return;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(r.a(query.getCount()));
        for (boolean moveToFirst = query.moveToFirst(); moveToFirst; moveToFirst = query.moveToNext()) {
            int i = query.getInt(query.getColumnIndex("audio_id"));
            int i2 = query.getInt(query.getColumnIndex("play_order"));
            arrayList.add(r.a(i));
            arrayList.add(r.a(i2));
        }
        query.close();
        cVar.a(1);
        cVar.a(arrayList);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentResolver.notifyChange(android.net.Uri, android.database.ContentObserver, boolean):void}
     arg types: [android.net.Uri, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.content.ContentResolver.notifyChange(android.net.Uri, android.database.ContentObserver, int):void}
      ClspMth{android.content.ContentResolver.notifyChange(android.net.Uri, android.database.ContentObserver, boolean):void} */
    public void A(Context context, com.qq.g.c cVar) {
        int i = 1;
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        String j = cVar.j();
        if (r.b(j)) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        if (h < 0 || cVar.e() == h + 2) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("name", j);
            Uri insert = context.getContentResolver().insert(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI, contentValues);
            context.getContentResolver().notifyChange(insert, (ContentObserver) null, false);
            try {
                int parseInt = Integer.parseInt(insert.getLastPathSegment());
                if (parseInt <= 0) {
                    cVar.a(8);
                    return;
                }
                if (h > 0) {
                    Uri contentUri = MediaStore.Audio.Playlists.Members.getContentUri("external", (long) parseInt);
                    for (int i2 = 0; i2 < h; i2++) {
                        int h2 = cVar.h();
                        if (h2 > 0) {
                            contentValues.clear();
                            contentValues.put("playlist_id", Integer.valueOf(parseInt));
                            contentValues.put("audio_id", Integer.valueOf(h2));
                            contentValues.put("play_order", Integer.valueOf(i));
                            i++;
                            context.getContentResolver().insert(contentUri, contentValues);
                        }
                    }
                    context.getContentResolver().notifyChange(contentUri, (ContentObserver) null, false);
                }
                ArrayList arrayList = new ArrayList();
                arrayList.add(r.a(parseInt));
                cVar.a(0);
                cVar.a(arrayList);
            } catch (Exception e) {
                e.printStackTrace();
                cVar.a(8);
            }
        } else {
            cVar.a(1);
        }
    }

    public void B(Context context, com.qq.g.c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        if (context.getContentResolver().delete(Uri.withAppendedPath(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI, Constants.STR_EMPTY + cVar.h()), null, null) <= 0) {
            cVar.a(1);
        }
    }

    public void C(Context context, com.qq.g.c cVar) {
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        String j = cVar.j();
        if (r.b(j)) {
            cVar.a(1);
            return;
        }
        Uri withAppendedPath = Uri.withAppendedPath(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI, Constants.STR_EMPTY + h);
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", j);
        if (context.getContentResolver().update(withAppendedPath, contentValues, null, null) > 0) {
            cVar.a(0);
        } else {
            cVar.a(8);
        }
    }

    public void D(Context context, com.qq.g.c cVar) {
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        if (h2 < 0 || cVar.e() != (h2 * 2) + 2) {
            cVar.a(1);
            return;
        }
        Uri contentUri = MediaStore.Audio.Playlists.Members.getContentUri("external", (long) h);
        Cursor query = context.getContentResolver().query(contentUri, this.f333a, null, null, null);
        if (query == null) {
            cVar.a(8);
            return;
        }
        int count = query.getCount();
        query.close();
        if (count > 0) {
            ContentValues contentValues = new ContentValues();
            for (int i = 0; i < h2; i++) {
                int h3 = cVar.h();
                int h4 = cVar.h();
                contentValues.put("audio_id", Integer.valueOf(h3));
                contentValues.put("play_order", Integer.valueOf(h4));
                context.getContentResolver().insert(contentUri, contentValues);
                contentValues.clear();
            }
        }
        cVar.a(0);
    }

    public void E(Context context, com.qq.g.c cVar) {
        if (cVar.e() < 1) {
            cVar.a(1);
            return;
        }
        if (context.getContentResolver().delete(MediaStore.Audio.Playlists.Members.getContentUri("external", (long) cVar.h()), null, null) > 0) {
            cVar.a(0);
        } else {
            cVar.a(8);
        }
    }

    public void F(Context context, com.qq.g.c cVar) {
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        if (h2 < 0 || cVar.e() != h2 + 2) {
            cVar.a(1);
            return;
        }
        Uri contentUri = MediaStore.Audio.Playlists.Members.getContentUri("external", (long) h);
        context.getContentResolver().delete(contentUri, null, null);
        if (h2 > 0) {
            ContentValues contentValues = new ContentValues();
            for (int i = 0; i < h2; i++) {
                int h3 = cVar.h();
                contentValues.clear();
                contentValues.put("playlist_id", Integer.valueOf(h));
                contentValues.put("audio_id", Integer.valueOf(h3));
                context.getContentResolver().insert(contentUri, contentValues);
            }
        }
        cVar.a(0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void G(Context context, com.qq.g.c cVar) {
        int i;
        int i2;
        if (cVar.e() < 4) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        int h3 = cVar.h();
        int h4 = cVar.h();
        Uri contentUri = MediaStore.Audio.Playlists.Members.getContentUri("external", (long) h);
        if (contentUri == null) {
            cVar.a(1);
        } else if (h3 == 0) {
            Cursor query = context.getContentResolver().query(contentUri, null, "playlist_id = " + h, null, null);
            if (query == null) {
                cVar.a(1);
                return;
            }
            for (boolean moveToFirst = query.moveToFirst(); moveToFirst; moveToFirst = query.moveToNext()) {
                int i3 = query.getInt(query.getColumnIndex("audio_id"));
                int i4 = query.getInt(query.getColumnIndex("play_order"));
                int i5 = query.getInt(query.getColumnIndex("_id"));
                ContentValues contentValues = new ContentValues();
                contentValues.put("audio_id", Integer.valueOf(i3));
                contentValues.put("playlist_id", Integer.valueOf(h));
                contentValues.put("play_order", Integer.valueOf(i4 + 1));
                context.getContentResolver().update(contentUri, contentValues, "_id = " + i5, null);
            }
            query.close();
            ContentValues contentValues2 = new ContentValues();
            contentValues2.put("audio_id", Integer.valueOf(h2));
            contentValues2.put("playlist_id", Integer.valueOf(h));
            contentValues2.put("play_order", (Integer) 1);
            if (context.getContentResolver().insert(contentUri, contentValues2) == null) {
                cVar.a(8);
                return;
            }
            ArrayList arrayList = new ArrayList();
            arrayList.add(r.a(1));
            cVar.a(arrayList);
            cVar.a(0);
        } else if (h3 == 1) {
            if (h4 <= 0) {
                i2 = 1;
            } else {
                i2 = h4;
            }
            Cursor query2 = context.getContentResolver().query(contentUri, null, "playlist_id = " + h + " and " + "play_order" + " >= " + i2, null, null);
            if (query2 == null) {
                cVar.a(1);
                return;
            }
            for (boolean moveToFirst2 = query2.moveToFirst(); moveToFirst2; moveToFirst2 = query2.moveToNext()) {
                int i6 = query2.getInt(query2.getColumnIndex("audio_id"));
                int i7 = query2.getInt(query2.getColumnIndex("play_order"));
                int i8 = query2.getInt(query2.getColumnIndex("_id"));
                ContentValues contentValues3 = new ContentValues();
                contentValues3.put("audio_id", Integer.valueOf(i6));
                contentValues3.put("playlist_id", Integer.valueOf(h));
                contentValues3.put("play_order", Integer.valueOf(i7 + 1));
                context.getContentResolver().update(contentUri, contentValues3, "_id = " + i8, null);
            }
            query2.close();
            ContentValues contentValues4 = new ContentValues();
            contentValues4.put("audio_id", Integer.valueOf(h2));
            contentValues4.put("playlist_id", Integer.valueOf(h));
            contentValues4.put("play_order", Integer.valueOf(i2));
            if (context.getContentResolver().insert(contentUri, contentValues4) == null) {
                cVar.a(8);
                return;
            }
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(r.a(i2));
            cVar.a(arrayList2);
            cVar.a(0);
        } else if (h3 == 2) {
            Cursor query3 = context.getContentResolver().query(contentUri, null, "playlist_id = " + h, null, "play_order");
            if (query3 == null) {
                cVar.a(1);
                return;
            }
            if (query3.moveToLast()) {
                i = query3.getInt(query3.getColumnIndex("play_order"));
            } else {
                i = 0;
            }
            query3.close();
            ContentValues contentValues5 = new ContentValues();
            contentValues5.put("audio_id", Integer.valueOf(h2));
            contentValues5.put("playlist_id", Integer.valueOf(h));
            contentValues5.put("play_order", Integer.valueOf(i + 1));
            if (context.getContentResolver().insert(contentUri, contentValues5) == null) {
                cVar.a(8);
                return;
            }
            ArrayList arrayList3 = new ArrayList();
            arrayList3.add(r.a(i + 1));
            cVar.a(arrayList3);
            cVar.a(0);
        } else {
            cVar.a(1);
        }
    }

    public void H(Context context, com.qq.g.c cVar) {
        if (cVar.e() < 2) {
            cVar.a(1);
            return;
        }
        int h = cVar.h();
        int h2 = cVar.h();
        Uri contentUri = MediaStore.Audio.Playlists.Members.getContentUri("external", (long) h);
        if (contentUri == null) {
            cVar.a(1);
        } else if (context.getContentResolver().delete(contentUri, "play_order = " + h2, null) > 0) {
            cVar.a(0);
        } else {
            cVar.a(1);
        }
    }
}
