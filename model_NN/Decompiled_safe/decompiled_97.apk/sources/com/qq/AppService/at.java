package com.qq.AppService;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import com.qq.a.a.d;
import com.qq.d.f.a;
import com.qq.provider.h;
import com.tencent.open.SocialConstants;
import java.util.Vector;

/* compiled from: ProGuard */
public final class at extends Thread {

    /* renamed from: a  reason: collision with root package name */
    public Vector<au> f226a;
    private Context b;
    private volatile boolean c;
    private volatile boolean d;

    public void a() {
        while (!this.c && this.d) {
            synchronized (this) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        return;
    }

    public void b() {
        if (this.f226a.size() > 0) {
            au auVar = this.f226a.get(0);
            Log.d("com.qq.connect", " querySMS2 " + auVar.b);
            Cursor query = this.b.getContentResolver().query(d.f258a, null, "address like '" + auVar.b + '\'', null, "_id DESC");
            if (query != null) {
                if (query.moveToFirst()) {
                    Log.d("com.qq.connect", "find new Message");
                    String string = query.getString(query.getColumnIndex("body"));
                    String string2 = query.getString(query.getColumnIndex("address"));
                    int i = query.getInt(query.getColumnIndex(SocialConstants.PARAM_TYPE));
                    Log.d("com.qq.connect", "find new Message type: " + i);
                    if (string == null) {
                        Log.d("com.qq.connect", "find new Message,body is null");
                        query.close();
                        return;
                    } else if (i != 1) {
                        Log.d("com.qq.connect", "find new Message,not in inbox");
                        query.close();
                        return;
                    } else if (!r.b(string.getBytes(), auVar.f227a.getBytes())) {
                        Log.d("com.qq.connect", "find new Message ,content is not match");
                        query.close();
                        return;
                    } else {
                        a aVar = new a();
                        aVar.j = string;
                        aVar.c = string2;
                        aVar.f280a = query.getInt(query.getColumnIndex("_id"));
                        aVar.b = query.getInt(query.getColumnIndex("thread_id"));
                        aVar.g = query.getInt(query.getColumnIndex("status"));
                        aVar.h = i;
                        aVar.f = query.getInt(query.getColumnIndex("read"));
                        aVar.e = r.b(query.getLong(query.getColumnIndex("date")));
                        aVar.d = query.getString(query.getColumnIndex("person"));
                        aVar.i = query.getString(query.getColumnIndex("subject"));
                        h.a(aVar);
                        this.f226a.remove(0);
                    }
                } else {
                    Log.d("com.qq.connect", "not find new Message");
                    if (auVar.c + 150000 < System.currentTimeMillis()) {
                        Log.d("com.qq.connect", "not find new Message,remove it");
                        this.f226a.remove(0);
                    }
                }
                query.close();
            }
        }
    }

    public void c() {
        if (this.f226a.size() <= 0) {
            Log.d("com.qq.connect", "setState 1 >>>");
            this.c = false;
            return;
        }
        Log.d("com.qq.connect", "setState 2 >>>");
        long currentTimeMillis = System.currentTimeMillis();
        if (this.f226a.get(0).c > currentTimeMillis || currentTimeMillis - this.f226a.get(0).c > 10000) {
            Log.d("com.qq.connect", "setState 2 ,remove it");
            this.f226a.remove(0);
        }
    }

    public void run() {
        super.run();
        this.d = true;
        while (this.d) {
            a();
            if (this.d) {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Log.d("com.qq.connect", "start querySMS2 >>>");
                try {
                    b();
                } catch (Throwable th) {
                    th.printStackTrace();
                }
                c();
            } else {
                return;
            }
        }
    }
}
