package com.google.zxing.b;

import com.google.zxing.NotFoundException;
import com.google.zxing.common.a;
import com.tencent.assistant.component.NormalErrorRecommendPage;

public final class o extends n {

    /* renamed from: a  reason: collision with root package name */
    private static final int[] f150a = {1, 1, 1, 1, 1, 1};
    private static final int[][] f = {new int[]{56, 52, 50, 49, 44, 38, 35, 42, 41, 37}, new int[]{7, 11, 13, 14, 19, 25, 28, 21, 22, 26}};
    private final int[] g = new int[4];

    private static void a(StringBuffer stringBuffer, int i) {
        for (int i2 = 0; i2 <= 1; i2++) {
            for (int i3 = 0; i3 < 10; i3++) {
                if (i == f[i2][i3]) {
                    stringBuffer.insert(0, (char) (i2 + 48));
                    stringBuffer.append((char) (i3 + 48));
                    return;
                }
            }
        }
        throw NotFoundException.a();
    }

    public static String b(String str) {
        char[] cArr = new char[6];
        str.getChars(1, 7, cArr, 0);
        StringBuffer stringBuffer = new StringBuffer(12);
        stringBuffer.append(str.charAt(0));
        char c = cArr[5];
        switch (c) {
            case '0':
            case '1':
            case NormalErrorRecommendPage.ERROR_TYPE_EMPTY_TO_HOME /*50*/:
                stringBuffer.append(cArr, 0, 2);
                stringBuffer.append(c);
                stringBuffer.append("0000");
                stringBuffer.append(cArr, 2, 3);
                break;
            case '3':
                stringBuffer.append(cArr, 0, 3);
                stringBuffer.append("00000");
                stringBuffer.append(cArr, 3, 2);
                break;
            case '4':
                stringBuffer.append(cArr, 0, 4);
                stringBuffer.append("00000");
                stringBuffer.append(cArr[4]);
                break;
            default:
                stringBuffer.append(cArr, 0, 5);
                stringBuffer.append("0000");
                stringBuffer.append(c);
                break;
        }
        stringBuffer.append(str.charAt(7));
        return stringBuffer.toString();
    }

    /* access modifiers changed from: protected */
    public int a(a aVar, int[] iArr, StringBuffer stringBuffer) {
        int[] iArr2 = this.g;
        iArr2[0] = 0;
        iArr2[1] = 0;
        iArr2[2] = 0;
        iArr2[3] = 0;
        int a2 = aVar.a();
        int i = iArr[1];
        int i2 = 0;
        int i3 = 0;
        while (i2 < 6 && i < a2) {
            int a3 = a(aVar, iArr2, i, e);
            stringBuffer.append((char) ((a3 % 10) + 48));
            int i4 = i;
            for (int i5 : iArr2) {
                i4 += i5;
            }
            int i6 = a3 >= 10 ? (1 << (5 - i2)) | i3 : i3;
            i2++;
            i3 = i6;
            i = i4;
        }
        a(stringBuffer, i3);
        return i;
    }

    /* access modifiers changed from: protected */
    public boolean a(String str) {
        return super.a(b(str));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.zxing.b.n.a(com.google.zxing.common.a, int, boolean, int[]):int[]
     arg types: [com.google.zxing.common.a, int, int, int[]]
     candidates:
      com.google.zxing.b.n.a(com.google.zxing.common.a, int[], int, int[][]):int
      com.google.zxing.b.n.a(int, com.google.zxing.common.a, int[], java.util.Hashtable):com.google.zxing.h
      com.google.zxing.b.n.a(com.google.zxing.common.a, int, boolean, int[]):int[] */
    /* access modifiers changed from: protected */
    public int[] a(a aVar, int i) {
        return a(aVar, i, true, f150a);
    }

    /* access modifiers changed from: package-private */
    public com.google.zxing.a b() {
        return com.google.zxing.a.c;
    }
}
