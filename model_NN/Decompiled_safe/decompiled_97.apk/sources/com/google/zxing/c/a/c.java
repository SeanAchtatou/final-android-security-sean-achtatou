package com.google.zxing.c.a;

import com.google.zxing.FormatException;
import com.google.zxing.common.b;
import com.google.zxing.common.g;

public final class c {
    private static int a(int[] iArr, int[] iArr2, int i) {
        if ((iArr2 != null && iArr2.length > (i / 2) + 3) || i < 0 || i > 512) {
            throw FormatException.a();
        } else if (iArr2 == null || iArr2.length <= 3) {
            return 0;
        } else {
            throw FormatException.a();
        }
    }

    private static void a(int[] iArr, int i) {
        if (iArr.length < 4) {
            throw FormatException.a();
        }
        int i2 = iArr[0];
        if (i2 > iArr.length) {
            throw FormatException.a();
        } else if (i2 != 0) {
        } else {
            if (i < iArr.length) {
                iArr[0] = iArr.length - i;
                return;
            }
            throw FormatException.a();
        }
    }

    public g a(b bVar) {
        a aVar = new a(bVar);
        int[] a2 = aVar.a();
        if (a2 == null || a2.length == 0) {
            throw FormatException.a();
        }
        int c = 1 << (aVar.c() + 1);
        a(a2, aVar.b(), c);
        a(a2, c);
        return b.a(a2);
    }
}
