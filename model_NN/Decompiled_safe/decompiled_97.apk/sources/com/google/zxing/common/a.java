package com.google.zxing.common;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    public int[] f156a;
    public int b;

    public a() {
        this.b = 0;
        this.f156a = new int[1];
    }

    public a(int i) {
        this.b = i;
        this.f156a = c(i);
    }

    private static int[] c(int i) {
        return new int[((i + 31) >> 5)];
    }

    public int a() {
        return this.b;
    }

    public boolean a(int i) {
        return (this.f156a[i >> 5] & (1 << (i & 31))) != 0;
    }

    public boolean a(int i, int i2, boolean z) {
        int i3;
        if (i2 < i) {
            throw new IllegalArgumentException();
        } else if (i2 == i) {
            return true;
        } else {
            int i4 = i2 - 1;
            int i5 = i >> 5;
            int i6 = i4 >> 5;
            int i7 = i5;
            while (i7 <= i6) {
                int i8 = i7 > i5 ? 0 : i & 31;
                int i9 = i7 < i6 ? 31 : i4 & 31;
                if (i8 == 0 && i9 == 31) {
                    i3 = -1;
                } else {
                    int i10 = i8;
                    i3 = 0;
                    while (i10 <= i9) {
                        int i11 = (1 << i10) | i3;
                        i10++;
                        i3 = i11;
                    }
                }
                int i12 = this.f156a[i7] & i3;
                if (!z) {
                    i3 = 0;
                }
                if (i12 != i3) {
                    return false;
                }
                i7++;
            }
            return true;
        }
    }

    public void b() {
        int length = this.f156a.length;
        for (int i = 0; i < length; i++) {
            this.f156a[i] = 0;
        }
    }

    public void b(int i) {
        int[] iArr = this.f156a;
        int i2 = i >> 5;
        iArr[i2] = iArr[i2] | (1 << (i & 31));
    }

    public void c() {
        int[] iArr = new int[this.f156a.length];
        int i = this.b;
        for (int i2 = 0; i2 < i; i2++) {
            if (a((i - i2) - 1)) {
                int i3 = i2 >> 5;
                iArr[i3] = iArr[i3] | (1 << (i2 & 31));
            }
        }
        this.f156a = iArr;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer(this.b);
        for (int i = 0; i < this.b; i++) {
            if ((i & 7) == 0) {
                stringBuffer.append(' ');
            }
            stringBuffer.append(a(i) ? 'X' : '.');
        }
        return stringBuffer.toString();
    }
}
