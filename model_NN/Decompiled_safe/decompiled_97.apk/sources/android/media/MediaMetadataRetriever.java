package android.media;

import android.graphics.Bitmap;
import java.io.FileDescriptor;

/* compiled from: ProGuard */
public class MediaMetadataRetriever {
    public static final int METADATA_KEY_ALBUM = 1;
    public static final int METADATA_KEY_ARTIST = 2;
    public static final int METADATA_KEY_AUTHOR = 3;
    public static final int METADATA_KEY_BIT_RATE = 16;
    public static final int METADATA_KEY_CD_TRACK_NUMBER = 0;
    public static final int METADATA_KEY_CODEC = 12;
    public static final int METADATA_KEY_COMMENT = 14;
    public static final int METADATA_KEY_COMPOSER = 4;
    public static final int METADATA_KEY_COPYRIGHT = 15;
    public static final int METADATA_KEY_DATE = 5;
    public static final int METADATA_KEY_DURATION = 9;
    public static final int METADATA_KEY_FRAME_RATE = 17;
    public static final int METADATA_KEY_GENRE = 6;
    public static final int METADATA_KEY_IS_DRM_CRIPPLED = 11;
    public static final int METADATA_KEY_NUM_TRACKS = 10;
    public static final int METADATA_KEY_RATING = 13;
    public static final int METADATA_KEY_TITLE = 7;
    public static final int METADATA_KEY_VIDEO_FORMAT = 18;
    public static final int METADATA_KEY_VIDEO_HEIGHT = 19;
    public static final int METADATA_KEY_VIDEO_WIDTH = 20;
    public static final int METADATA_KEY_YEAR = 8;
    public static final int MODE_CAPTURE_FRAME_ONLY = 2;
    public static final int MODE_GET_METADATA_ONLY = 1;

    private final native void native_finalize();

    private native void native_setup();

    public native Bitmap captureFrame();

    public native byte[] extractAlbumArt();

    public native String extractMetadata(int i);

    public native int getMode();

    public native void release();

    public native void setDataSource(FileDescriptor fileDescriptor, long j, long j2);

    public native void setDataSource(String str);

    public native void setMode(int i);

    static {
        System.loadLibrary("media_jni");
    }

    public MediaMetadataRetriever() {
        native_setup();
    }

    public void setDataSource(FileDescriptor fileDescriptor) {
        setDataSource(fileDescriptor, 0, 576460752303423487L);
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:0x005c A[SYNTHETIC, Splitter:B:36:0x005c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setDataSource(android.content.Context r8, android.net.Uri r9) {
        /*
            r7 = this;
            if (r9 != 0) goto L_0x0008
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>()
            throw r0
        L_0x0008:
            java.lang.String r0 = r9.getScheme()
            if (r0 == 0) goto L_0x0016
            java.lang.String r1 = "file"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x001e
        L_0x0016:
            java.lang.String r0 = r9.getPath()
            r7.setDataSource(r0)
        L_0x001d:
            return
        L_0x001e:
            r0 = 0
            android.content.ContentResolver r1 = r8.getContentResolver()     // Catch:{ SecurityException -> 0x0047, all -> 0x0086 }
            java.lang.String r2 = "r"
            android.content.res.AssetFileDescriptor r6 = r1.openAssetFileDescriptor(r9, r2)     // Catch:{ FileNotFoundException -> 0x0040 }
            if (r6 != 0) goto L_0x0049
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ SecurityException -> 0x0031, all -> 0x0059 }
            r0.<init>()     // Catch:{ SecurityException -> 0x0031, all -> 0x0059 }
            throw r0     // Catch:{ SecurityException -> 0x0031, all -> 0x0059 }
        L_0x0031:
            r0 = move-exception
            r0 = r6
        L_0x0033:
            if (r0 == 0) goto L_0x0038
            r0.close()     // Catch:{ IOException -> 0x0082 }
        L_0x0038:
            java.lang.String r0 = r9.toString()
            r7.setDataSource(r0)
            goto L_0x001d
        L_0x0040:
            r1 = move-exception
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException     // Catch:{ SecurityException -> 0x0047, all -> 0x0086 }
            r1.<init>()     // Catch:{ SecurityException -> 0x0047, all -> 0x0086 }
            throw r1     // Catch:{ SecurityException -> 0x0047, all -> 0x0086 }
        L_0x0047:
            r1 = move-exception
            goto L_0x0033
        L_0x0049:
            java.io.FileDescriptor r1 = r6.getFileDescriptor()     // Catch:{ SecurityException -> 0x0031, all -> 0x0059 }
            boolean r0 = r1.valid()     // Catch:{ SecurityException -> 0x0031, all -> 0x0059 }
            if (r0 != 0) goto L_0x0060
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ SecurityException -> 0x0031, all -> 0x0059 }
            r0.<init>()     // Catch:{ SecurityException -> 0x0031, all -> 0x0059 }
            throw r0     // Catch:{ SecurityException -> 0x0031, all -> 0x0059 }
        L_0x0059:
            r0 = move-exception
        L_0x005a:
            if (r6 == 0) goto L_0x005f
            r6.close()     // Catch:{ IOException -> 0x0084 }
        L_0x005f:
            throw r0
        L_0x0060:
            long r2 = r6.getDeclaredLength()     // Catch:{ SecurityException -> 0x0031, all -> 0x0059 }
            r4 = 0
            int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r0 >= 0) goto L_0x0075
            r7.setDataSource(r1)     // Catch:{ SecurityException -> 0x0031, all -> 0x0059 }
        L_0x006d:
            if (r6 == 0) goto L_0x001d
            r6.close()     // Catch:{ IOException -> 0x0073 }
            goto L_0x001d
        L_0x0073:
            r0 = move-exception
            goto L_0x001d
        L_0x0075:
            long r2 = r6.getStartOffset()     // Catch:{ SecurityException -> 0x0031, all -> 0x0059 }
            long r4 = r6.getDeclaredLength()     // Catch:{ SecurityException -> 0x0031, all -> 0x0059 }
            r0 = r7
            r0.setDataSource(r1, r2, r4)     // Catch:{ SecurityException -> 0x0031, all -> 0x0059 }
            goto L_0x006d
        L_0x0082:
            r0 = move-exception
            goto L_0x0038
        L_0x0084:
            r1 = move-exception
            goto L_0x005f
        L_0x0086:
            r1 = move-exception
            r6 = r0
            r0 = r1
            goto L_0x005a
        */
        throw new UnsupportedOperationException("Method not decompiled: android.media.MediaMetadataRetriever.setDataSource(android.content.Context, android.net.Uri):void");
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        try {
            native_finalize();
        } finally {
            super.finalize();
        }
    }
}
