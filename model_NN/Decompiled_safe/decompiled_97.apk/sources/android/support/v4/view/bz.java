package android.support.v4.view;

import android.support.v4.view.ViewPager;
import android.view.View;
import java.util.Comparator;

/* compiled from: ProGuard */
class bz implements Comparator<View> {
    bz() {
    }

    /* renamed from: a */
    public int compare(View view, View view2) {
        ViewPager.LayoutParams layoutParams = (ViewPager.LayoutParams) view.getLayoutParams();
        ViewPager.LayoutParams layoutParams2 = (ViewPager.LayoutParams) view2.getLayoutParams();
        if (layoutParams.isDecor != layoutParams2.isDecor) {
            return layoutParams.isDecor ? 1 : -1;
        }
        return layoutParams.position - layoutParams2.position;
    }
}
