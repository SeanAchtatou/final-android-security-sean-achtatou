package android.support.v4.app;

import android.os.Build;

/* compiled from: ProGuard */
public class ac {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final ai f47a;

    static {
        if (Build.VERSION.SDK_INT >= 16) {
            f47a = new am();
        } else if (Build.VERSION.SDK_INT >= 14) {
            f47a = new al();
        } else if (Build.VERSION.SDK_INT >= 11) {
            f47a = new ak();
        } else {
            f47a = new aj();
        }
    }
}
