package com.zemariamm.appirater;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.util.Log;

public class AppirateUtils {
    private static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences("DB_APPIRATER", 0);
    }

    private static boolean isNeverShowAppirater(Context context) {
        return getPreferences(context).getBoolean("neverrate", false);
    }

    public static void markNeverRate(Context context) {
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putBoolean("neverrate", true);
        editor.commit();
    }

    private static int getTimesOpened(Context context) {
        int times = getPreferences(context).getInt("times", 0) + 1;
        SharedPreferences.Editor editor = getPreferences(context).edit();
        editor.putInt("times", times);
        editor.commit();
        return times;
    }

    private static int getShowAppirater(Context context) {
        try {
            int default_times = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.getInt("appirater");
            if (default_times == 0) {
                return 4;
            }
            return default_times;
        } catch (Exception e) {
            e.printStackTrace();
            return 4;
        }
    }

    public static boolean shouldAppirater(Context context) {
        boolean shouldShow = false;
        if (getTimesOpened(context) % getShowAppirater(context) == 0) {
            shouldShow = true;
        }
        if (isNeverShowAppirater(context)) {
            return false;
        }
        return shouldShow;
    }

    public static void appiraterDialog(final Context context, final AppiraterBase parent) {
        AlertDialog.Builder builderInvite = new AlertDialog.Builder(context);
        String packageName = "";
        String appName = "";
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            packageName = info.packageName;
            appName = context.getResources().getString(info.applicationInfo.labelRes);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d("Appirater", "PackageName: " + packageName);
        final String marketLink = "market://details?id=" + packageName;
        builderInvite.setMessage("If you enjoy using " + appName + ", would you mind taking a moment to rate it? It won't take more than a minute. Thanks for your support!");
        builderInvite.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                AppiraterBase.this.processRate();
                AppirateUtils.markNeverRate(context);
                context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(marketLink)));
                dialog.dismiss();
            }
        }).setNeutralButton("Remind me later", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                AppiraterBase.this.processRemindMe();
                dialog.dismiss();
            }
        }).setNegativeButton("No, Thanks", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                AppirateUtils.markNeverRate(context);
                parent.processNever();
                dialog.cancel();
            }
        });
        builderInvite.create().show();
    }
}
