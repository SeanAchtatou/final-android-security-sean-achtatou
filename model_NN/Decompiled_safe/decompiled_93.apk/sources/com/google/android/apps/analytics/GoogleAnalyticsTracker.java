package com.google.android.apps.analytics;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.util.Log;
import com.google.android.apps.analytics.Dispatcher;
import com.google.android.apps.analytics.PersistentEventStore;
import java.util.HashMap;
import java.util.Map;

public class GoogleAnalyticsTracker {
    private static final GoogleAnalyticsTracker INSTANCE = new GoogleAnalyticsTracker();
    private String accountId;
    private ConnectivityManager connetivityManager;
    private CustomVariableBuffer customVariableBuffer;
    private boolean debug = false;
    private int dispatchPeriod;
    private Runnable dispatchRunner = new Runnable() {
        public void run() {
            GoogleAnalyticsTracker.this.dispatch();
        }
    };
    private Dispatcher dispatcher;
    private boolean dispatcherIsBusy;
    private boolean dryRun = false;
    /* access modifiers changed from: private */
    public EventStore eventStore;
    /* access modifiers changed from: private */
    public Handler handler;
    private Map<String, Map<String, Item>> itemMap = new HashMap();
    private Context parent;
    private boolean powerSaveMode;
    private Map<String, Transaction> transactionMap = new HashMap();
    private String userAgentProduct = "GoogleAnalytics";
    private String userAgentVersion = "1.2";

    final class DispatcherCallbacks implements Dispatcher.Callbacks {
        DispatcherCallbacks() {
        }

        public void dispatchFinished() {
            GoogleAnalyticsTracker.this.handler.post(new Runnable() {
                public void run() {
                    GoogleAnalyticsTracker.this.dispatchFinished();
                }
            });
        }

        public void eventDispatched(long j) {
            GoogleAnalyticsTracker.this.eventStore.deleteEvent(j);
        }
    }

    private GoogleAnalyticsTracker() {
    }

    private void cancelPendingDispatches() {
        this.handler.removeCallbacks(this.dispatchRunner);
    }

    private void createEvent(String str, String str2, String str3, String str4, int i) {
        Event event = new Event(this.eventStore.getStoreId(), str, str2, str3, str4, i, this.parent.getResources().getDisplayMetrics().widthPixels, this.parent.getResources().getDisplayMetrics().heightPixels);
        event.setCustomVariableBuffer(this.customVariableBuffer);
        this.customVariableBuffer = new CustomVariableBuffer();
        this.eventStore.putEvent(event);
        resetPowerSaveMode();
    }

    public static GoogleAnalyticsTracker getInstance() {
        return INSTANCE;
    }

    private void maybeScheduleNextDispatch() {
        if (this.dispatchPeriod >= 0 && this.handler.postDelayed(this.dispatchRunner, (long) (this.dispatchPeriod * 1000)) && this.debug) {
            Log.v("GoogleAnalyticsTracker", "Scheduled next dispatch");
        }
    }

    private void resetPowerSaveMode() {
        if (this.powerSaveMode) {
            this.powerSaveMode = false;
            maybeScheduleNextDispatch();
        }
    }

    public boolean dispatch() {
        if (this.debug) {
            Log.v("GoogleAnalyticsTracker", "Called dispatch");
        }
        if (this.dispatcherIsBusy) {
            if (this.debug) {
                Log.v("GoogleAnalyticsTracker", "...but dispatcher was busy");
            }
            maybeScheduleNextDispatch();
            return false;
        }
        NetworkInfo activeNetworkInfo = this.connetivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isAvailable()) {
            if (this.debug) {
                Log.v("GoogleAnalyticsTracker", "...but there was no network available");
            }
            maybeScheduleNextDispatch();
            return false;
        } else if (this.eventStore.getNumStoredEvents() != 0) {
            Event[] peekEvents = this.eventStore.peekEvents();
            this.dispatcher.dispatchEvents(peekEvents);
            this.dispatcherIsBusy = true;
            maybeScheduleNextDispatch();
            if (this.debug) {
                Log.v("GoogleAnalyticsTracker", "Sending " + peekEvents.length + " to dispatcher");
            }
            return true;
        } else {
            this.powerSaveMode = true;
            if (this.debug) {
                Log.v("GoogleAnalyticsTracker", "...but there was nothing to dispatch");
            }
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public void dispatchFinished() {
        this.dispatcherIsBusy = false;
    }

    public boolean getDebug() {
        return this.debug;
    }

    public void setDispatchPeriod(int i) {
        int i2 = this.dispatchPeriod;
        this.dispatchPeriod = i;
        if (i2 <= 0) {
            maybeScheduleNextDispatch();
        } else if (i2 > 0) {
            cancelPendingDispatches();
            maybeScheduleNextDispatch();
        }
    }

    public void start(String str, int i, Context context) {
        NetworkDispatcher networkDispatcher;
        PersistentEventStore persistentEventStore = this.eventStore == null ? new PersistentEventStore(new PersistentEventStore.DataBaseHelper(context)) : this.eventStore;
        if (this.dispatcher == null) {
            NetworkDispatcher networkDispatcher2 = new NetworkDispatcher(this.userAgentProduct, this.userAgentVersion);
            networkDispatcher2.setDryRun(this.dryRun);
            networkDispatcher = networkDispatcher2;
        } else {
            networkDispatcher = this.dispatcher;
        }
        start(str, i, context, persistentEventStore, networkDispatcher);
    }

    /* access modifiers changed from: package-private */
    public void start(String str, int i, Context context, EventStore eventStore2, Dispatcher dispatcher2) {
        start(str, i, context, eventStore2, dispatcher2, new DispatcherCallbacks());
    }

    /* access modifiers changed from: package-private */
    public void start(String str, int i, Context context, EventStore eventStore2, Dispatcher dispatcher2, Dispatcher.Callbacks callbacks) {
        this.accountId = str;
        this.parent = context;
        this.eventStore = eventStore2;
        this.eventStore.startNewVisit();
        this.dispatcher = dispatcher2;
        this.dispatcher.init(callbacks, this.eventStore.getReferrer());
        this.dispatcherIsBusy = false;
        if (this.connetivityManager == null) {
            this.connetivityManager = (ConnectivityManager) this.parent.getSystemService("connectivity");
        }
        if (this.handler == null) {
            this.handler = new Handler(context.getMainLooper());
        } else {
            cancelPendingDispatches();
        }
        setDispatchPeriod(i);
    }

    public void stop() {
        this.dispatcher.stop();
        cancelPendingDispatches();
    }

    public void trackPageView(String str) {
        createEvent(this.accountId, "__##GOOGLEPAGEVIEW##__", str, null, -1);
    }
}
