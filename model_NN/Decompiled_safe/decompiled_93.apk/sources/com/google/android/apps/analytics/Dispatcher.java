package com.google.android.apps.analytics;

interface Dispatcher {

    public interface Callbacks {
        void dispatchFinished();

        void eventDispatched(long j);
    }

    void dispatchEvents(Event[] eventArr);

    void init(Callbacks callbacks, String str);

    void setDryRun(boolean z);

    void stop();
}
