package com.google.android.apps.analytics;

interface EventStore {
    void deleteEvent(long j);

    int getNumStoredEvents();

    String getReferrer();

    int getStoreId();

    Event[] peekEvents();

    void putEvent(Event event);

    void startNewVisit();
}
