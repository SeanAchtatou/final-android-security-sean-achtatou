package scala;

import scala.runtime.BoxesRunTime;

/* compiled from: Function2.scala */
public interface Function2<T1, T2, R> extends ScalaObject {
    R apply(T1 t1, T2 t2);

    int apply$mcIII$sp(int i, int i2);

    Function1<T1, Function1<T2, R>> curried();

    Function1<T1, Function1<T2, R>> curry();

    Function1<Tuple2<T1, T2>, R> tupled();

    /* renamed from: scala.Function2$class  reason: invalid class name */
    /* compiled from: Function2.scala */
    public abstract class Cclass {
        public static void $init$(Function2 $this) {
        }

        public static double apply$mcDDD$sp(Function2 $this, double v1, double v2) {
            return BoxesRunTime.unboxToDouble($this.apply(BoxesRunTime.boxToDouble(v1), BoxesRunTime.boxToDouble(v2)));
        }

        public static double apply$mcDDI$sp(Function2 $this, double v1, int v2) {
            return BoxesRunTime.unboxToDouble($this.apply(BoxesRunTime.boxToDouble(v1), BoxesRunTime.boxToInteger(v2)));
        }

        public static double apply$mcDDL$sp(Function2 $this, double v1, long v2) {
            return BoxesRunTime.unboxToDouble($this.apply(BoxesRunTime.boxToDouble(v1), BoxesRunTime.boxToLong(v2)));
        }

        public static double apply$mcDID$sp(Function2 $this, int v1, double v2) {
            return BoxesRunTime.unboxToDouble($this.apply(BoxesRunTime.boxToInteger(v1), BoxesRunTime.boxToDouble(v2)));
        }

        public static double apply$mcDII$sp(Function2 $this, int v1, int v2) {
            return BoxesRunTime.unboxToDouble($this.apply(BoxesRunTime.boxToInteger(v1), BoxesRunTime.boxToInteger(v2)));
        }

        public static double apply$mcDIL$sp(Function2 $this, int v1, long v2) {
            return BoxesRunTime.unboxToDouble($this.apply(BoxesRunTime.boxToInteger(v1), BoxesRunTime.boxToLong(v2)));
        }

        public static double apply$mcDLD$sp(Function2 $this, long v1, double v2) {
            return BoxesRunTime.unboxToDouble($this.apply(BoxesRunTime.boxToLong(v1), BoxesRunTime.boxToDouble(v2)));
        }

        public static double apply$mcDLI$sp(Function2 $this, long v1, int v2) {
            return BoxesRunTime.unboxToDouble($this.apply(BoxesRunTime.boxToLong(v1), BoxesRunTime.boxToInteger(v2)));
        }

        public static double apply$mcDLL$sp(Function2 $this, long v1, long v2) {
            return BoxesRunTime.unboxToDouble($this.apply(BoxesRunTime.boxToLong(v1), BoxesRunTime.boxToLong(v2)));
        }

        public static float apply$mcFDD$sp(Function2 $this, double v1, double v2) {
            return BoxesRunTime.unboxToFloat($this.apply(BoxesRunTime.boxToDouble(v1), BoxesRunTime.boxToDouble(v2)));
        }

        public static float apply$mcFDI$sp(Function2 $this, double v1, int v2) {
            return BoxesRunTime.unboxToFloat($this.apply(BoxesRunTime.boxToDouble(v1), BoxesRunTime.boxToInteger(v2)));
        }

        public static float apply$mcFDL$sp(Function2 $this, double v1, long v2) {
            return BoxesRunTime.unboxToFloat($this.apply(BoxesRunTime.boxToDouble(v1), BoxesRunTime.boxToLong(v2)));
        }

        public static float apply$mcFID$sp(Function2 $this, int v1, double v2) {
            return BoxesRunTime.unboxToFloat($this.apply(BoxesRunTime.boxToInteger(v1), BoxesRunTime.boxToDouble(v2)));
        }

        public static float apply$mcFII$sp(Function2 $this, int v1, int v2) {
            return BoxesRunTime.unboxToFloat($this.apply(BoxesRunTime.boxToInteger(v1), BoxesRunTime.boxToInteger(v2)));
        }

        public static float apply$mcFIL$sp(Function2 $this, int v1, long v2) {
            return BoxesRunTime.unboxToFloat($this.apply(BoxesRunTime.boxToInteger(v1), BoxesRunTime.boxToLong(v2)));
        }

        public static float apply$mcFLD$sp(Function2 $this, long v1, double v2) {
            return BoxesRunTime.unboxToFloat($this.apply(BoxesRunTime.boxToLong(v1), BoxesRunTime.boxToDouble(v2)));
        }

        public static float apply$mcFLI$sp(Function2 $this, long v1, int v2) {
            return BoxesRunTime.unboxToFloat($this.apply(BoxesRunTime.boxToLong(v1), BoxesRunTime.boxToInteger(v2)));
        }

        public static float apply$mcFLL$sp(Function2 $this, long v1, long v2) {
            return BoxesRunTime.unboxToFloat($this.apply(BoxesRunTime.boxToLong(v1), BoxesRunTime.boxToLong(v2)));
        }

        public static int apply$mcIDD$sp(Function2 $this, double v1, double v2) {
            return BoxesRunTime.unboxToInt($this.apply(BoxesRunTime.boxToDouble(v1), BoxesRunTime.boxToDouble(v2)));
        }

        public static int apply$mcIDI$sp(Function2 $this, double v1, int v2) {
            return BoxesRunTime.unboxToInt($this.apply(BoxesRunTime.boxToDouble(v1), BoxesRunTime.boxToInteger(v2)));
        }

        public static int apply$mcIDL$sp(Function2 $this, double v1, long v2) {
            return BoxesRunTime.unboxToInt($this.apply(BoxesRunTime.boxToDouble(v1), BoxesRunTime.boxToLong(v2)));
        }

        public static int apply$mcIID$sp(Function2 $this, int v1, double v2) {
            return BoxesRunTime.unboxToInt($this.apply(BoxesRunTime.boxToInteger(v1), BoxesRunTime.boxToDouble(v2)));
        }

        public static int apply$mcIII$sp(Function2 $this, int v1, int v2) {
            return BoxesRunTime.unboxToInt($this.apply(BoxesRunTime.boxToInteger(v1), BoxesRunTime.boxToInteger(v2)));
        }

        public static int apply$mcIIL$sp(Function2 $this, int v1, long v2) {
            return BoxesRunTime.unboxToInt($this.apply(BoxesRunTime.boxToInteger(v1), BoxesRunTime.boxToLong(v2)));
        }

        public static int apply$mcILD$sp(Function2 $this, long v1, double v2) {
            return BoxesRunTime.unboxToInt($this.apply(BoxesRunTime.boxToLong(v1), BoxesRunTime.boxToDouble(v2)));
        }

        public static int apply$mcILI$sp(Function2 $this, long v1, int v2) {
            return BoxesRunTime.unboxToInt($this.apply(BoxesRunTime.boxToLong(v1), BoxesRunTime.boxToInteger(v2)));
        }

        public static int apply$mcILL$sp(Function2 $this, long v1, long v2) {
            return BoxesRunTime.unboxToInt($this.apply(BoxesRunTime.boxToLong(v1), BoxesRunTime.boxToLong(v2)));
        }

        public static long apply$mcLDD$sp(Function2 $this, double v1, double v2) {
            return BoxesRunTime.unboxToLong($this.apply(BoxesRunTime.boxToDouble(v1), BoxesRunTime.boxToDouble(v2)));
        }

        public static long apply$mcLDI$sp(Function2 $this, double v1, int v2) {
            return BoxesRunTime.unboxToLong($this.apply(BoxesRunTime.boxToDouble(v1), BoxesRunTime.boxToInteger(v2)));
        }

        public static long apply$mcLDL$sp(Function2 $this, double v1, long v2) {
            return BoxesRunTime.unboxToLong($this.apply(BoxesRunTime.boxToDouble(v1), BoxesRunTime.boxToLong(v2)));
        }

        public static long apply$mcLID$sp(Function2 $this, int v1, double v2) {
            return BoxesRunTime.unboxToLong($this.apply(BoxesRunTime.boxToInteger(v1), BoxesRunTime.boxToDouble(v2)));
        }

        public static long apply$mcLII$sp(Function2 $this, int v1, int v2) {
            return BoxesRunTime.unboxToLong($this.apply(BoxesRunTime.boxToInteger(v1), BoxesRunTime.boxToInteger(v2)));
        }

        public static long apply$mcLIL$sp(Function2 $this, int v1, long v2) {
            return BoxesRunTime.unboxToLong($this.apply(BoxesRunTime.boxToInteger(v1), BoxesRunTime.boxToLong(v2)));
        }

        public static long apply$mcLLD$sp(Function2 $this, long v1, double v2) {
            return BoxesRunTime.unboxToLong($this.apply(BoxesRunTime.boxToLong(v1), BoxesRunTime.boxToDouble(v2)));
        }

        public static long apply$mcLLI$sp(Function2 $this, long v1, int v2) {
            return BoxesRunTime.unboxToLong($this.apply(BoxesRunTime.boxToLong(v1), BoxesRunTime.boxToInteger(v2)));
        }

        public static long apply$mcLLL$sp(Function2 $this, long v1, long v2) {
            return BoxesRunTime.unboxToLong($this.apply(BoxesRunTime.boxToLong(v1), BoxesRunTime.boxToLong(v2)));
        }

        public static void apply$mcVDD$sp(Function2 $this, double v1, double v2) {
            $this.apply(BoxesRunTime.boxToDouble(v1), BoxesRunTime.boxToDouble(v2));
        }

        public static void apply$mcVDI$sp(Function2 $this, double v1, int v2) {
            $this.apply(BoxesRunTime.boxToDouble(v1), BoxesRunTime.boxToInteger(v2));
        }

        public static void apply$mcVDL$sp(Function2 $this, double v1, long v2) {
            $this.apply(BoxesRunTime.boxToDouble(v1), BoxesRunTime.boxToLong(v2));
        }

        public static void apply$mcVID$sp(Function2 $this, int v1, double v2) {
            $this.apply(BoxesRunTime.boxToInteger(v1), BoxesRunTime.boxToDouble(v2));
        }

        public static void apply$mcVII$sp(Function2 $this, int v1, int v2) {
            $this.apply(BoxesRunTime.boxToInteger(v1), BoxesRunTime.boxToInteger(v2));
        }

        public static void apply$mcVIL$sp(Function2 $this, int v1, long v2) {
            $this.apply(BoxesRunTime.boxToInteger(v1), BoxesRunTime.boxToLong(v2));
        }

        public static void apply$mcVLD$sp(Function2 $this, long v1, double v2) {
            $this.apply(BoxesRunTime.boxToLong(v1), BoxesRunTime.boxToDouble(v2));
        }

        public static void apply$mcVLI$sp(Function2 $this, long v1, int v2) {
            $this.apply(BoxesRunTime.boxToLong(v1), BoxesRunTime.boxToInteger(v2));
        }

        public static void apply$mcVLL$sp(Function2 $this, long v1, long v2) {
            $this.apply(BoxesRunTime.boxToLong(v1), BoxesRunTime.boxToLong(v2));
        }

        public static boolean apply$mcZDD$sp(Function2 $this, double v1, double v2) {
            return BoxesRunTime.unboxToBoolean($this.apply(BoxesRunTime.boxToDouble(v1), BoxesRunTime.boxToDouble(v2)));
        }

        public static boolean apply$mcZDI$sp(Function2 $this, double v1, int v2) {
            return BoxesRunTime.unboxToBoolean($this.apply(BoxesRunTime.boxToDouble(v1), BoxesRunTime.boxToInteger(v2)));
        }

        public static boolean apply$mcZDL$sp(Function2 $this, double v1, long v2) {
            return BoxesRunTime.unboxToBoolean($this.apply(BoxesRunTime.boxToDouble(v1), BoxesRunTime.boxToLong(v2)));
        }

        public static boolean apply$mcZID$sp(Function2 $this, int v1, double v2) {
            return BoxesRunTime.unboxToBoolean($this.apply(BoxesRunTime.boxToInteger(v1), BoxesRunTime.boxToDouble(v2)));
        }

        public static boolean apply$mcZII$sp(Function2 $this, int v1, int v2) {
            return BoxesRunTime.unboxToBoolean($this.apply(BoxesRunTime.boxToInteger(v1), BoxesRunTime.boxToInteger(v2)));
        }

        public static boolean apply$mcZIL$sp(Function2 $this, int v1, long v2) {
            return BoxesRunTime.unboxToBoolean($this.apply(BoxesRunTime.boxToInteger(v1), BoxesRunTime.boxToLong(v2)));
        }

        public static boolean apply$mcZLD$sp(Function2 $this, long v1, double v2) {
            return BoxesRunTime.unboxToBoolean($this.apply(BoxesRunTime.boxToLong(v1), BoxesRunTime.boxToDouble(v2)));
        }

        public static boolean apply$mcZLI$sp(Function2 $this, long v1, int v2) {
            return BoxesRunTime.unboxToBoolean($this.apply(BoxesRunTime.boxToLong(v1), BoxesRunTime.boxToInteger(v2)));
        }

        public static boolean apply$mcZLL$sp(Function2 $this, long v1, long v2) {
            return BoxesRunTime.unboxToBoolean($this.apply(BoxesRunTime.boxToLong(v1), BoxesRunTime.boxToLong(v2)));
        }

        public static String toString(Function2 $this) {
            return "<function2>";
        }

        public static Function1 curried$mcDDD$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcDDI$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcDDL$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcDID$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcDII$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcDIL$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcDLD$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcDLI$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcDLL$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcFDD$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcFDI$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcFDL$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcFID$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcFII$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcFIL$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcFLD$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcFLI$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcFLL$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcIDD$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcIDI$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcIDL$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcIID$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcIII$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcIIL$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcILD$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcILI$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcILL$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcLDD$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcLDI$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcLDL$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcLID$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcLII$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcLIL$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcLLD$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcLLI$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcLLL$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcVDD$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcVDI$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcVDL$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcVID$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcVII$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcVIL$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcVLD$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcVLI$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcVLL$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcZDD$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcZDI$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcZDL$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcZID$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcZII$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcZIL$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcZLD$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcZLI$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried$mcZLL$sp(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curried(Function2 $this) {
            return new Function2$$anonfun$curried$1($this);
        }

        public static Function1 curry(Function2 $this) {
            return $this.curried();
        }

        public static Function1 curry$mcDDD$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcDDI$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcDDL$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcDID$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcDII$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcDIL$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcDLD$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcDLI$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcDLL$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcFDD$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcFDI$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcFDL$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcFID$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcFII$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcFIL$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcFLD$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcFLI$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcFLL$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcIDD$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcIDI$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcIDL$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcIID$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcIII$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcIIL$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcILD$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcILI$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcILL$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcLDD$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcLDI$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcLDL$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcLID$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcLII$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcLIL$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcLLD$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcLLI$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcLLL$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcVDD$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcVDI$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcVDL$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcVID$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcVII$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcVIL$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcVLD$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcVLI$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcVLL$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcZDD$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcZDI$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcZDL$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcZID$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcZII$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcZIL$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcZLD$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcZLI$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 curry$mcZLL$sp(Function2 $this) {
            return $this.curry();
        }

        public static Function1 tupled(Function2 $this) {
            return new Function2$$anonfun$tupled$1($this);
        }

        public static Function1 tupled$mcDDD$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcDDI$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcDDL$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcDID$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcDII$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcDIL$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcDLD$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcDLI$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcDLL$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcFDD$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcFDI$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcFDL$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcFID$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcFII$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcFIL$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcFLD$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcFLI$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcFLL$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcIDD$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcIDI$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcIDL$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcIID$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcIII$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcIIL$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcILD$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcILI$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcILL$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcLDD$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcLDI$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcLDL$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcLID$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcLII$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcLIL$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcLLD$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcLLI$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcLLL$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcVDD$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcVDI$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcVDL$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcVID$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcVII$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcVIL$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcVLD$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcVLI$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcVLL$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcZDD$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcZDI$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcZDL$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcZID$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcZII$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcZIL$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcZLD$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcZLI$sp(Function2 $this) {
            return $this.tupled();
        }

        public static Function1 tupled$mcZLL$sp(Function2 $this) {
            return $this.tupled();
        }
    }
}
