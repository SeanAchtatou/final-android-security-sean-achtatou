package scala.actors;

import java.io.Serializable;
import scala.Product;
import scala.ScalaObject;
import scala.collection.Iterator;
import scala.collection.mutable.StringBuilder;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime$;

/* compiled from: Actor.scala */
public class Exit implements Serializable, Product, ScalaObject {
    private final AbstractActor from;
    private final Object reason;

    public Exit(AbstractActor from2, Object reason2) {
        this.from = from2;
        this.reason = reason2;
        Product.Cclass.$init$(this);
    }

    private final /* synthetic */ boolean gd1$1(AbstractActor abstractActor, Object obj) {
        AbstractActor from2 = from();
        if (abstractActor != null ? abstractActor.equals(from2) : from2 == null) {
            Object reason2 = reason();
            if (obj != reason2 ? obj != null ? !(obj instanceof Number) ? !(obj instanceof Character) ? obj.equals(reason2) : BoxesRunTime.equalsCharObject((Character) obj, reason2) : BoxesRunTime.equalsNumObject((Number) obj, reason2) : false : true) {
                return true;
            }
        }
        return false;
    }

    public boolean canEqual(Object obj) {
        return obj instanceof Exit;
    }

    public boolean equals(Object obj) {
        boolean z;
        if (this != obj) {
            if (obj instanceof Exit) {
                Exit exit = (Exit) obj;
                z = gd1$1(exit.from(), exit.reason()) ? ((Exit) obj).canEqual(this) : false;
            } else {
                z = false;
            }
            if (!z) {
                return false;
            }
        }
        return true;
    }

    public AbstractActor from() {
        return this.from;
    }

    public int hashCode() {
        return ScalaRunTime$.MODULE$._hashCode(this);
    }

    public int productArity() {
        return 2;
    }

    public Object productElement(int i) {
        switch (i) {
            case 0:
                return from();
            case 1:
                return reason();
            default:
                throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(i).toString());
        }
    }

    public Iterator<Object> productIterator() {
        return Product.Cclass.productIterator(this);
    }

    public String productPrefix() {
        return "Exit";
    }

    public Object reason() {
        return this.reason;
    }

    public String toString() {
        return productIterator().mkString(new StringBuilder().append((Object) productPrefix()).append((Object) "(").toString(), ",", ")");
    }
}
