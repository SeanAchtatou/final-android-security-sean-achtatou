package scala.actors;

import $anonfun$act$2.$anonfun;
import java.io.Serializable;
import scala.Function1;
import scala.MatchError;
import scala.PartialFunction;
import scala.runtime.BoxedUnit;

/* compiled from: Future.scala */
public final class FutureActor$$anonfun$act$2$$anonfun$apply$mcV$sp$1$$anonfun$apply$1 implements Serializable, PartialFunction {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ FutureActor$$anonfun$act$2$$anonfun$apply$mcV$sp$1 $outer;

    public FutureActor$$anonfun$act$2$$anonfun$apply$mcV$sp$1$$anonfun$apply$1(FutureActor<T>.$anonfun.apply.mcV.sp.1 $outer2) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        Function1.Cclass.$init$(this);
        PartialFunction.Cclass.$init$(this);
    }

    public <C> PartialFunction<Object, C> andThen(Function1<Object, C> k) {
        return PartialFunction.Cclass.andThen(this, k);
    }

    public final void apply(Object obj) {
        Eval$ eval$ = Eval$.MODULE$;
        if (eval$ != null ? !eval$.equals(obj) : obj != null) {
            throw new MatchError(obj);
        }
        this.$outer.$outer.$outer.reply(BoxedUnit.UNIT);
    }

    public int apply$mcII$sp(int v1) {
        return Function1.Cclass.apply$mcII$sp(this, v1);
    }

    public void apply$mcVI$sp(int v1) {
        Function1.Cclass.apply$mcVI$sp(this, v1);
    }

    public void apply$mcVL$sp(long v1) {
        Function1.Cclass.apply$mcVL$sp(this, v1);
    }

    public <A> Function1<A, Object> compose(Function1<A, Object> g) {
        return Function1.Cclass.compose(this, g);
    }

    public final boolean isDefinedAt(Object obj) {
        Eval$ eval$ = Eval$.MODULE$;
        return eval$ != null ? eval$.equals(obj) : obj == null;
    }

    public String toString() {
        return Function1.Cclass.toString(this);
    }
}
