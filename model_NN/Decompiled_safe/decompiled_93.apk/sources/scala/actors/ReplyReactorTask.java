package scala.actors;

import scala.Function0;
import scala.PartialFunction;
import scala.ScalaObject;

/* compiled from: ReplyReactorTask.scala */
public class ReplyReactorTask extends ReactorTask<Object> implements ScalaObject {
    private final ReplyReactor reactor;
    private ReplyReactor saved;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ReplyReactorTask(ReplyReactor reactor2, Function0<Object> fun, PartialFunction<Object, Object> handler, Object msg) {
        super(reactor2, fun, handler, msg);
        this.reactor = reactor2;
    }

    public ReplyReactor saved() {
        return this.saved;
    }

    public void saved_$eq(ReplyReactor replyReactor) {
        this.saved = replyReactor;
    }

    public void beginExecution() {
        saved_$eq(Actor$.MODULE$.tl().get());
        Actor$.MODULE$.tl().set(this.reactor);
    }

    public void suspendExecution() {
        Actor$.MODULE$.tl().set(saved());
    }
}
