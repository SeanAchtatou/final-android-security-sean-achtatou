package scala.actors;

import java.util.concurrent.Callable;
import scala.Console$;
import scala.Function0;
import scala.PartialFunction;
import scala.ScalaObject;
import scala.collection.mutable.StringBuilder;
import scala.concurrent.forkjoin.RecursiveAction;
import scala.runtime.StringAdd;

/* compiled from: ReactorTask.scala */
public class ReactorTask<Msg> extends RecursiveAction implements Callable<Object>, Runnable, ScalaObject {
    private Function0<Object> fun;
    private PartialFunction<Msg, Object> handler;
    private Msg msg;
    private Reactor<Msg> reactor;

    public ReactorTask(Reactor<Msg> reactor2, Function0<Object> fun2, PartialFunction<Msg, Object> handler2, Msg msg2) {
        this.reactor = reactor2;
        this.fun = fun2;
        this.handler = handler2;
        this.msg = msg2;
    }

    public Reactor<Msg> reactor() {
        return this.reactor;
    }

    public void reactor_$eq(Reactor<Msg> reactor2) {
        this.reactor = reactor2;
    }

    public Function0<Object> fun() {
        return this.fun;
    }

    public void fun_$eq(Function0<Object> function0) {
        this.fun = function0;
    }

    public PartialFunction<Msg, Object> handler() {
        return this.handler;
    }

    public void handler_$eq(PartialFunction<Msg, Object> partialFunction) {
        this.handler = partialFunction;
    }

    public Msg msg() {
        return this.msg;
    }

    public void msg_$eq(Msg msg2) {
        this.msg = msg2;
    }

    public void run() {
        try {
            beginExecution();
            if (fun() == null) {
                handler().apply(msg());
            } else {
                fun().apply();
            }
        } catch (SuspendActorControl e) {
            suspendExecution();
            reactor_$eq(null);
            fun_$eq(null);
            handler_$eq(null);
            msg_$eq(null);
            return;
        } catch (Throwable th) {
            try {
                terminateExecution(th);
                reactor().terminated();
                if (!(th instanceof Exception)) {
                    throw th;
                }
                return;
            } finally {
                suspendExecution();
                reactor_$eq(null);
                fun_$eq(null);
                handler_$eq(null);
                msg_$eq(null);
            }
        }
        reactor().kill().apply$mcV$sp();
        suspendExecution();
        reactor_$eq(null);
        fun_$eq(null);
        handler_$eq(null);
        msg_$eq(null);
    }

    private final /* synthetic */ boolean gd1$1(Exception exc) {
        return reactor().exceptionHandler().isDefinedAt(exc);
    }

    public void call() {
        run();
    }

    public void compute() {
        run();
    }

    public void beginExecution() {
    }

    public void suspendExecution() {
    }

    public void terminateExecution(Throwable e) {
        Console$.MODULE$.err().println(new StringBuilder().append((Object) new StringAdd(reactor()).$plus(": caught ")).append(e).toString());
        e.printStackTrace();
    }
}
