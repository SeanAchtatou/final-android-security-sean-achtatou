package scala.actors;

import scala.Function2;
import scala.None$;
import scala.Option;
import scala.Predef$$anon$1;
import scala.ScalaObject;
import scala.Some;
import scala.runtime.IntRef;

/* compiled from: MessageQueue.scala */
public class MQueue<Msg> implements ScalaObject {
    private int _size = 0;
    private MQueueElement<Msg> first = null;
    private final String label;
    private MQueueElement<Msg> last = null;

    public MQueue(String label2) {
        this.label = label2;
    }

    public MQueueElement<Msg> first() {
        return this.first;
    }

    public void first_$eq(MQueueElement<Msg> mQueueElement) {
        this.first = mQueueElement;
    }

    public MQueueElement<Msg> last() {
        return this.last;
    }

    public void last_$eq(MQueueElement<Msg> mQueueElement) {
        this.last = mQueueElement;
    }

    private int _size() {
        return this._size;
    }

    private void _size_$eq(int i) {
        this._size = i;
    }

    public final boolean isEmpty() {
        return last() == null;
    }

    public void changeSize(int diff) {
        _size_$eq(_size() + diff);
    }

    public void append(Msg msg, OutputChannel<Object> session) {
        changeSize(1);
        MQueueElement el = new MQueueElement(msg, session);
        if (isEmpty()) {
            first_$eq(el);
        } else {
            last().next_$eq(el);
        }
        last_$eq(el);
    }

    public void append(MQueueElement<Msg> el) {
        changeSize(1);
        if (isEmpty()) {
            first_$eq(el);
        } else {
            last().next_$eq(el);
        }
        last_$eq(el);
    }

    public void foreach(Function2<Msg, OutputChannel<Object>, Object> f) {
        for (MQueueElement curr = first(); curr != null; curr = curr.next()) {
            f.apply(curr.msg(), curr.session());
        }
    }

    public void foreachDequeue(MQueue<Msg> target) {
        for (MQueueElement curr = first(); curr != null; curr = curr.next()) {
            target.append(curr);
        }
        first_$eq(null);
        last_$eq(null);
        _size_$eq(0);
    }

    public MQueueElement<Msg> extractFirst(Function2<Msg, OutputChannel<Object>, Boolean> p) {
        return (MQueueElement) removeInternal(0, p).orNull(new Predef$$anon$1());
    }

    private Option<MQueueElement<Msg>> removeInternal(int n$2, Function2<Msg, OutputChannel<Object>, Boolean> p$2) {
        IntRef pos$2 = new IntRef(0);
        if (isEmpty()) {
            return None$.MODULE$;
        }
        if (test$2(first().msg(), first().session(), n$2, p$2, pos$2)) {
            MQueueElement res = first();
            first_$eq(first().next());
            if (res == last()) {
                last_$eq(null);
            }
            return foundMsg$1(res);
        }
        MQueueElement prev = first();
        for (MQueueElement curr = first().next(); curr != null; curr = curr.next()) {
            if (test$2(curr.msg(), curr.session(), n$2, p$2, pos$2)) {
                prev.next_$eq(curr.next());
                if (curr == last()) {
                    last_$eq(prev);
                }
                return foundMsg$1(curr);
            }
            prev = curr;
        }
        return None$.MODULE$;
    }

    private final Some foundMsg$1(MQueueElement x) {
        changeSize(-1);
        return new Some(x);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0015, code lost:
        if (0 != 0) goto L_0x0017;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final boolean test$2(java.lang.Object r3, scala.actors.OutputChannel r4, int r5, scala.Function2 r6, scala.runtime.IntRef r7) {
        /*
            r2 = this;
            r1 = 0
            java.lang.Object r0 = r6.apply(r3, r4)
            boolean r0 = scala.runtime.BoxesRunTime.unboxToBoolean(r0)
            if (r0 == 0) goto L_0x0019
            int r0 = r7.elem
            if (r0 == r5) goto L_0x0017
            int r0 = r7.elem
            int r0 = r0 + 1
            r7.elem = r0
            if (r1 == 0) goto L_0x0019
        L_0x0017:
            r0 = 1
        L_0x0018:
            return r0
        L_0x0019:
            r0 = r1
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.actors.MQueue.test$2(java.lang.Object, scala.actors.OutputChannel, int, scala.Function2, scala.runtime.IntRef):boolean");
    }
}
