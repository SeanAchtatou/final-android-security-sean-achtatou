package scala.actors;

import $anonfun;
import java.io.Serializable;
import scala.Function1;
import scala.PartialFunction;

/* compiled from: Reactor.scala */
public final class Reactor$$anonfun$seq$1$$anonfun$apply$1 implements Serializable, PartialFunction {
    public static final long serialVersionUID = 0;
    private final /* synthetic */ Reactor$$anonfun$seq$1 $outer;

    public Reactor$$anonfun$seq$1$$anonfun$apply$1(Reactor<Msg>.$anonfun.seq.1 $outer2) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        Function1.Cclass.$init$(this);
        PartialFunction.Cclass.$init$(this);
    }

    public <C> PartialFunction<Msg, C> andThen(Function1<Object, C> k) {
        return PartialFunction.Cclass.andThen(this, k);
    }

    public final Object apply(Msg msg) {
        return this.$outer.next$1.apply();
    }

    public int apply$mcII$sp(int v1) {
        return Function1.Cclass.apply$mcII$sp(this, v1);
    }

    public void apply$mcVI$sp(int v1) {
        Function1.Cclass.apply$mcVI$sp(this, v1);
    }

    public void apply$mcVL$sp(long v1) {
        Function1.Cclass.apply$mcVL$sp(this, v1);
    }

    public <A> Function1<A, Object> compose(Function1<A, Msg> g) {
        return Function1.Cclass.compose(this, g);
    }

    public final boolean isDefinedAt(Msg msg) {
        return true;
    }

    public String toString() {
        return Function1.Cclass.toString(this);
    }
}
