package scala.actors;

import scala.ScalaObject;

/* compiled from: ActorCanReply.scala */
public interface ActorCanReply extends ScalaObject, ReactorCanReply {
    Object $bang$qmark(Object obj);

    /* renamed from: scala.actors.ActorCanReply$class  reason: invalid class name */
    /* compiled from: ActorCanReply.scala */
    public abstract class Cclass {
        public static void $init$(AbstractActor $this) {
        }

        public static Object $bang$qmark(AbstractActor $this, Object msg) {
            Channel replyCh = new Channel((Actor) Actor$.MODULE$.rawSelf(((Reactor) $this).scheduler()));
            ((Reactor) $this).send(msg, replyCh);
            return replyCh.$qmark();
        }
    }
}
