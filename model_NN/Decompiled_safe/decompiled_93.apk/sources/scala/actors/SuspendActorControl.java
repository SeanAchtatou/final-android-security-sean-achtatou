package scala.actors;

import scala.ScalaObject;
import scala.util.control.ControlThrowable;
import scala.util.control.NoStackTrace;

/* compiled from: Actor.scala */
public class SuspendActorControl extends Throwable implements ScalaObject, ControlThrowable {
    public SuspendActorControl() {
        NoStackTrace.Cclass.$init$(this);
    }

    public Throwable fillInStackTrace() {
        return NoStackTrace.Cclass.fillInStackTrace(this);
    }
}
