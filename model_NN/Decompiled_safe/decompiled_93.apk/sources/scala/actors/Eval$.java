package scala.actors;

import java.io.Serializable;
import scala.Product;
import scala.ScalaObject;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;

/* compiled from: Future.scala */
public final class Eval$ implements Serializable, Product, ScalaObject {
    public static final Eval$ MODULE$ = null;

    static {
        new Eval$();
    }

    public boolean canEqual(Object obj) {
        return obj instanceof Eval$;
    }

    public int productArity() {
        return 0;
    }

    public Object productElement(int i) {
        throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(i).toString());
    }

    public Iterator<Object> productIterator() {
        return Product.Cclass.productIterator(this);
    }

    public String productPrefix() {
        return "Eval";
    }

    public final String toString() {
        return "Eval";
    }

    private Eval$() {
        MODULE$ = this;
        Product.Cclass.$init$(this);
    }
}
