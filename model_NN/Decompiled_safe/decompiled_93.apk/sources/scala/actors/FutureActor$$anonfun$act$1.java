package scala.actors;

import java.io.Serializable;
import scala.concurrent.SyncVar;
import scala.runtime.AbstractFunction0$mcV$sp;

/* compiled from: Future.scala */
public final class FutureActor$$anonfun$act$1 extends AbstractFunction0$mcV$sp implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ FutureActor $outer;
    public final /* synthetic */ SyncVar res$1;

    /* JADX WARN: Type inference failed for: r3v0, types: [scala.concurrent.SyncVar, scala.actors.FutureActor<T>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public FutureActor$$anonfun$act$1(scala.actors.FutureActor r2, scala.actors.FutureActor<T> r3) {
        /*
            r1 = this;
            if (r2 != 0) goto L_0x0008
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            r0.<init>()
            throw r0
        L_0x0008:
            r1.$outer = r2
            r1.res$1 = r3
            r1.<init>()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.actors.FutureActor$$anonfun$act$1.<init>(scala.actors.FutureActor, scala.concurrent.SyncVar):void");
    }

    public final void apply() {
        this.$outer.scala$actors$FutureActor$$fun.apply(this.res$1);
    }

    public void apply$mcV$sp() {
        this.$outer.scala$actors$FutureActor$$fun.apply(this.res$1);
    }
}
