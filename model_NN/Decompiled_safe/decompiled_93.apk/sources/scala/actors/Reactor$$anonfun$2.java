package scala.actors;

import java.io.Serializable;
import scala.runtime.AbstractFunction0;
import scala.runtime.Nothing$;

/* compiled from: Reactor.scala */
public final class Reactor$$anonfun$2 extends AbstractFunction0 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Reactor $outer;

    /* JADX WARN: Type inference failed for: r2v0, types: [scala.actors.Reactor$class, scala.actors.Reactor] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Reactor$$anonfun$2(scala.actors.Reactor.Cclass r2) {
        /*
            r1 = this;
            if (r2 != 0) goto L_0x0008
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            r0.<init>()
            throw r0
        L_0x0008:
            r1.$outer = r2
            r1.<init>()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.actors.Reactor$$anonfun$2.<init>(scala.actors.Reactor):void");
    }

    public final Nothing$ apply() {
        return this.$outer.exit();
    }
}
