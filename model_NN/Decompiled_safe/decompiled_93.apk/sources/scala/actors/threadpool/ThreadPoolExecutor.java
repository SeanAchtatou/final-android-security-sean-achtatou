package scala.actors.threadpool;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import scala.actors.threadpool.locks.Condition;
import scala.actors.threadpool.locks.ReentrantLock;

public class ThreadPoolExecutor extends AbstractExecutorService {
    private static final RejectedExecutionHandler defaultHandler = new AbortPolicy();
    private static final RuntimePermission shutdownPerm = new RuntimePermission("modifyThread");
    private volatile boolean allowCoreThreadTimeOut;
    private long completedTaskCount;
    private volatile int corePoolSize;
    private final AtomicInteger ctl = new AtomicInteger(ctlOf(-536870912, 0));
    private volatile RejectedExecutionHandler handler;
    private volatile long keepAliveTime;
    private int largestPoolSize;
    public final ReentrantLock mainLock = new ReentrantLock();
    private volatile int maximumPoolSize;
    private final Condition termination = this.mainLock.newCondition();
    private volatile ThreadFactory threadFactory;
    private final BlockingQueue workQueue;
    public final HashSet workers = new HashSet();

    public static class AbortPolicy implements RejectedExecutionHandler {
        public void rejectedExecution(Runnable runnable, ThreadPoolExecutor threadPoolExecutor) {
            throw new RejectedExecutionException();
        }
    }

    public static class CallerRunsPolicy implements RejectedExecutionHandler {
        public void rejectedExecution(Runnable runnable, ThreadPoolExecutor threadPoolExecutor) {
            if (!threadPoolExecutor.isShutdown()) {
                runnable.run();
            }
        }
    }

    public final class Worker extends ReentrantLock implements Runnable {
        volatile long completedTasks;
        Runnable firstTask;
        public final Thread thread;

        Worker(Runnable runnable) {
            this.firstTask = runnable;
            this.thread = ThreadPoolExecutor.this.getThreadFactory().newThread(this);
        }

        public void run() {
            ThreadPoolExecutor.this.runWorker(this);
        }
    }

    public ThreadPoolExecutor(int i, int i2, long j, TimeUnit timeUnit, BlockingQueue blockingQueue, ThreadFactory threadFactory2, RejectedExecutionHandler rejectedExecutionHandler) {
        if (i < 0 || i2 <= 0 || i2 < i || j < 0) {
            throw new IllegalArgumentException();
        } else if (blockingQueue == null || threadFactory2 == null || rejectedExecutionHandler == null) {
            throw new NullPointerException();
        } else {
            this.corePoolSize = i;
            this.maximumPoolSize = i2;
            this.workQueue = blockingQueue;
            this.keepAliveTime = timeUnit.toNanos(j);
            this.threadFactory = threadFactory2;
            this.handler = rejectedExecutionHandler;
        }
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:50:?, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean addWorker(java.lang.Runnable r6, boolean r7) {
        /*
            r5 = this;
            r4 = 0
        L_0x0001:
            scala.actors.threadpool.AtomicInteger r0 = r5.ctl
            int r0 = r0.get()
            int r1 = runStateOf(r0)
            if (r1 < 0) goto L_0x001b
            if (r1 != 0) goto L_0x0019
            if (r6 != 0) goto L_0x0019
            scala.actors.threadpool.BlockingQueue r2 = r5.workQueue
            boolean r2 = r2.isEmpty()
            if (r2 == 0) goto L_0x001b
        L_0x0019:
            r0 = r4
        L_0x001a:
            return r0
        L_0x001b:
            int r2 = workerCountOf(r0)
            r3 = 536870911(0x1fffffff, float:1.0842021E-19)
            if (r2 >= r3) goto L_0x002a
            if (r7 == 0) goto L_0x002c
            int r3 = r5.corePoolSize
        L_0x0028:
            if (r2 < r3) goto L_0x002f
        L_0x002a:
            r0 = r4
            goto L_0x001a
        L_0x002c:
            int r3 = r5.maximumPoolSize
            goto L_0x0028
        L_0x002f:
            boolean r0 = r5.compareAndIncrementWorkerCount(r0)
            if (r0 == 0) goto L_0x005e
            scala.actors.threadpool.ThreadPoolExecutor$Worker r0 = new scala.actors.threadpool.ThreadPoolExecutor$Worker
            r0.<init>(r6)
            java.lang.Thread r1 = r0.thread
            scala.actors.threadpool.locks.ReentrantLock r2 = r5.mainLock
            r2.lock()
            scala.actors.threadpool.AtomicInteger r3 = r5.ctl     // Catch:{ all -> 0x009b }
            int r3 = r3.get()     // Catch:{ all -> 0x009b }
            int r3 = runStateOf(r3)     // Catch:{ all -> 0x009b }
            if (r1 == 0) goto L_0x0053
            if (r3 < 0) goto L_0x006b
            if (r3 != 0) goto L_0x0053
            if (r6 == 0) goto L_0x006b
        L_0x0053:
            r5.decrementWorkerCount()     // Catch:{ all -> 0x009b }
            r5.tryTerminate()     // Catch:{ all -> 0x009b }
            r2.unlock()
            r0 = r4
            goto L_0x001a
        L_0x005e:
            scala.actors.threadpool.AtomicInteger r0 = r5.ctl
            int r0 = r0.get()
            int r2 = runStateOf(r0)
            if (r2 == r1) goto L_0x001b
            goto L_0x0001
        L_0x006b:
            java.util.HashSet r3 = r5.workers     // Catch:{ all -> 0x009b }
            r3.add(r0)     // Catch:{ all -> 0x009b }
            java.util.HashSet r0 = r5.workers     // Catch:{ all -> 0x009b }
            int r0 = r0.size()     // Catch:{ all -> 0x009b }
            int r3 = r5.largestPoolSize     // Catch:{ all -> 0x009b }
            if (r0 <= r3) goto L_0x007c
            r5.largestPoolSize = r0     // Catch:{ all -> 0x009b }
        L_0x007c:
            r2.unlock()
            r1.start()
            scala.actors.threadpool.AtomicInteger r0 = r5.ctl
            int r0 = r0.get()
            int r0 = runStateOf(r0)
            r2 = 536870912(0x20000000, float:1.0842022E-19)
            if (r0 != r2) goto L_0x0099
            boolean r0 = r1.isInterrupted()
            if (r0 != 0) goto L_0x0099
            r1.interrupt()
        L_0x0099:
            r0 = 1
            goto L_0x001a
        L_0x009b:
            r0 = move-exception
            r2.unlock()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.actors.threadpool.ThreadPoolExecutor.addWorker(java.lang.Runnable, boolean):boolean");
    }

    private void advanceRunState(int i) {
        int i2;
        do {
            i2 = this.ctl.get();
            if (runStateAtLeast(i2, i)) {
                return;
            }
        } while (!this.ctl.compareAndSet(i2, ctlOf(i, workerCountOf(i2))));
    }

    private void checkShutdownAccess() {
        SecurityManager securityManager = System.getSecurityManager();
        if (securityManager != null) {
            securityManager.checkPermission(shutdownPerm);
            ReentrantLock reentrantLock = this.mainLock;
            reentrantLock.lock();
            try {
                Iterator it = this.workers.iterator();
                while (it.hasNext()) {
                    securityManager.checkAccess(((Worker) it.next()).thread);
                }
            } finally {
                reentrantLock.unlock();
            }
        }
    }

    private void clearInterruptsForTaskRun() {
        if (runStateLessThan(this.ctl.get(), 536870912) && Thread.interrupted() && runStateAtLeast(this.ctl.get(), 536870912)) {
            Thread.currentThread().interrupt();
        }
    }

    private boolean compareAndDecrementWorkerCount(int i) {
        return this.ctl.compareAndSet(i, i - 1);
    }

    private boolean compareAndIncrementWorkerCount(int i) {
        return this.ctl.compareAndSet(i, i + 1);
    }

    private static int ctlOf(int i, int i2) {
        return i | i2;
    }

    private void decrementWorkerCount() {
        do {
        } while (!compareAndDecrementWorkerCount(this.ctl.get()));
    }

    private List drainQueue() {
        BlockingQueue blockingQueue = this.workQueue;
        ArrayList arrayList = new ArrayList();
        blockingQueue.drainTo(arrayList);
        if (!blockingQueue.isEmpty()) {
            Runnable[] runnableArr = (Runnable[]) blockingQueue.toArray(new Runnable[0]);
            for (Runnable runnable : runnableArr) {
                if (blockingQueue.remove(runnable)) {
                    arrayList.add(runnable);
                }
            }
        }
        return arrayList;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0036, code lost:
        if (r4 == false) goto L_0x005f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        r0 = (java.lang.Runnable) r9.workQueue.poll(r9.keepAliveTime, scala.actors.threadpool.TimeUnit.NANOSECONDS);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0044, code lost:
        if (r0 != null) goto L_0x0020;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0046, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        r0 = (java.lang.Runnable) r9.workQueue.take();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0069, code lost:
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
        return r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.Runnable getTask() {
        /*
            r9 = this;
            r8 = 0
            r7 = 1
            r6 = 0
            r0 = r6
        L_0x0004:
            scala.actors.threadpool.AtomicInteger r1 = r9.ctl
            int r1 = r1.get()
            int r2 = runStateOf(r1)
            if (r2 < 0) goto L_0x0021
            r3 = 536870912(0x20000000, float:1.0842022E-19)
            if (r2 >= r3) goto L_0x001c
            scala.actors.threadpool.BlockingQueue r3 = r9.workQueue
            boolean r3 = r3.isEmpty()
            if (r3 == 0) goto L_0x0021
        L_0x001c:
            r9.decrementWorkerCount()
            r0 = r8
        L_0x0020:
            return r0
        L_0x0021:
            int r3 = workerCountOf(r1)
            boolean r4 = r9.allowCoreThreadTimeOut
            if (r4 != 0) goto L_0x002d
            int r4 = r9.corePoolSize
            if (r3 <= r4) goto L_0x0048
        L_0x002d:
            r4 = r7
        L_0x002e:
            int r5 = r9.maximumPoolSize
            if (r3 > r5) goto L_0x004a
            if (r0 == 0) goto L_0x0036
            if (r4 != 0) goto L_0x004a
        L_0x0036:
            if (r4 == 0) goto L_0x005f
            scala.actors.threadpool.BlockingQueue r0 = r9.workQueue     // Catch:{ InterruptedException -> 0x0068 }
            long r1 = r9.keepAliveTime     // Catch:{ InterruptedException -> 0x0068 }
            scala.actors.threadpool.TimeUnit r3 = scala.actors.threadpool.TimeUnit.NANOSECONDS     // Catch:{ InterruptedException -> 0x0068 }
            java.lang.Object r0 = r0.poll(r1, r3)     // Catch:{ InterruptedException -> 0x0068 }
            java.lang.Runnable r0 = (java.lang.Runnable) r0     // Catch:{ InterruptedException -> 0x0068 }
        L_0x0044:
            if (r0 != 0) goto L_0x0020
            r0 = r7
            goto L_0x0004
        L_0x0048:
            r4 = r6
            goto L_0x002e
        L_0x004a:
            boolean r1 = r9.compareAndDecrementWorkerCount(r1)
            if (r1 == 0) goto L_0x0052
            r0 = r8
            goto L_0x0020
        L_0x0052:
            scala.actors.threadpool.AtomicInteger r1 = r9.ctl
            int r1 = r1.get()
            int r3 = runStateOf(r1)
            if (r3 == r2) goto L_0x0021
            goto L_0x0004
        L_0x005f:
            scala.actors.threadpool.BlockingQueue r0 = r9.workQueue     // Catch:{ InterruptedException -> 0x0068 }
            java.lang.Object r0 = r0.take()     // Catch:{ InterruptedException -> 0x0068 }
            java.lang.Runnable r0 = (java.lang.Runnable) r0     // Catch:{ InterruptedException -> 0x0068 }
            goto L_0x0044
        L_0x0068:
            r0 = move-exception
            r0 = r6
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.actors.threadpool.ThreadPoolExecutor.getTask():java.lang.Runnable");
    }

    private void interruptIdleWorkers() {
        interruptIdleWorkers(false);
    }

    private void interruptIdleWorkers(boolean z) {
        Worker worker;
        ReentrantLock reentrantLock = this.mainLock;
        reentrantLock.lock();
        try {
            Iterator it = this.workers.iterator();
            while (it.hasNext()) {
                worker = (Worker) it.next();
                Thread thread = worker.thread;
                if (!thread.isInterrupted() && worker.tryLock()) {
                    thread.interrupt();
                    worker.unlock();
                    continue;
                }
                if (z) {
                    break;
                }
            }
        } catch (SecurityException e) {
            worker.unlock();
            continue;
        } catch (Throwable th) {
            reentrantLock.unlock();
            throw th;
        }
        reentrantLock.unlock();
    }

    private void interruptWorkers() {
        ReentrantLock reentrantLock = this.mainLock;
        reentrantLock.lock();
        try {
            Iterator it = this.workers.iterator();
            while (it.hasNext()) {
                try {
                    ((Worker) it.next()).thread.interrupt();
                } catch (SecurityException e) {
                }
            }
        } finally {
            reentrantLock.unlock();
        }
    }

    private static boolean isRunning(int i) {
        return i < 0;
    }

    /* JADX INFO: finally extract failed */
    private void processWorkerExit(Worker worker, boolean z) {
        if (z) {
            decrementWorkerCount();
        }
        ReentrantLock reentrantLock = this.mainLock;
        reentrantLock.lock();
        try {
            this.completedTaskCount += worker.completedTasks;
            this.workers.remove(worker);
            reentrantLock.unlock();
            tryTerminate();
            int i = this.ctl.get();
            if (runStateLessThan(i, 536870912)) {
                if (!z) {
                    int i2 = this.allowCoreThreadTimeOut ? 0 : this.corePoolSize;
                    if (i2 == 0 && !this.workQueue.isEmpty()) {
                        i2 = 1;
                    }
                    if (workerCountOf(i) >= i2) {
                        return;
                    }
                }
                addWorker(null, false);
            }
        } catch (Throwable th) {
            reentrantLock.unlock();
            throw th;
        }
    }

    private static boolean runStateAtLeast(int i, int i2) {
        return i >= i2;
    }

    private static boolean runStateLessThan(int i, int i2) {
        return i < i2;
    }

    private static int runStateOf(int i) {
        return -536870912 & i;
    }

    private static int workerCountOf(int i) {
        return 536870911 & i;
    }

    /* access modifiers changed from: protected */
    public void afterExecute(Runnable runnable, Throwable th) {
    }

    /* access modifiers changed from: protected */
    public void beforeExecute(Thread thread, Runnable runnable) {
    }

    public void execute(Runnable runnable) {
        if (runnable == null) {
            throw new NullPointerException();
        }
        int i = this.ctl.get();
        if (workerCountOf(i) < this.corePoolSize) {
            if (!addWorker(runnable, true)) {
                i = this.ctl.get();
            } else {
                return;
            }
        }
        if (isRunning(i) && this.workQueue.offer(runnable)) {
            int i2 = this.ctl.get();
            if (!isRunning(i2) && remove(runnable)) {
                reject(runnable);
            } else if (workerCountOf(i2) == 0) {
                addWorker(null, false);
            }
        } else if (!addWorker(runnable, false)) {
            reject(runnable);
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        shutdown();
    }

    public int getActiveCount() {
        ReentrantLock reentrantLock = this.mainLock;
        reentrantLock.lock();
        int i = 0;
        try {
            Iterator it = this.workers.iterator();
            while (it.hasNext()) {
                if (((Worker) it.next()).isLocked()) {
                    i++;
                }
            }
            return i;
        } finally {
            reentrantLock.unlock();
        }
    }

    public ThreadFactory getThreadFactory() {
        return this.threadFactory;
    }

    public boolean isShutdown() {
        return !isRunning(this.ctl.get());
    }

    /* access modifiers changed from: package-private */
    public void onShutdown() {
    }

    /* access modifiers changed from: package-private */
    public final void reject(Runnable runnable) {
        this.handler.rejectedExecution(runnable, this);
    }

    public boolean remove(Runnable runnable) {
        boolean remove = this.workQueue.remove(runnable);
        tryTerminate();
        return remove;
    }

    /* access modifiers changed from: package-private */
    public final void runWorker(Worker worker) {
        RuntimeException runtimeException;
        Runnable runnable = worker.firstTask;
        worker.firstTask = null;
        while (true) {
            if (runnable == null) {
                try {
                    runnable = getTask();
                    if (runnable == null) {
                        processWorkerExit(worker, false);
                        return;
                    }
                } catch (Throwable th) {
                    processWorkerExit(worker, true);
                    throw th;
                }
            }
            worker.lock();
            clearInterruptsForTaskRun();
            beforeExecute(worker.thread, runnable);
            try {
                runnable.run();
                afterExecute(runnable, null);
                worker.completedTasks++;
                worker.unlock();
                runnable = null;
            } catch (RuntimeException e) {
                throw e;
            } catch (Error e2) {
                throw e2;
            } catch (Throwable th2) {
                Throwable th3 = th2;
                runtimeException = th;
                th = th3;
            }
        }
        afterExecute(runnable, runtimeException);
        throw th;
    }

    public void setCorePoolSize(int i) {
        if (i < 0) {
            throw new IllegalArgumentException();
        }
        int i2 = i - this.corePoolSize;
        this.corePoolSize = i;
        if (workerCountOf(this.ctl.get()) > i) {
            interruptIdleWorkers();
        } else if (i2 > 0) {
            int min = Math.min(i2, this.workQueue.size());
            while (true) {
                int i3 = min - 1;
                if (min > 0 && addWorker(null, true) && !this.workQueue.isEmpty()) {
                    min = i3;
                } else {
                    return;
                }
            }
        }
    }

    /* JADX INFO: finally extract failed */
    public void shutdown() {
        ReentrantLock reentrantLock = this.mainLock;
        reentrantLock.lock();
        try {
            checkShutdownAccess();
            advanceRunState(0);
            interruptIdleWorkers();
            onShutdown();
            reentrantLock.unlock();
            tryTerminate();
        } catch (Throwable th) {
            reentrantLock.unlock();
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    public List shutdownNow() {
        ReentrantLock reentrantLock = this.mainLock;
        reentrantLock.lock();
        try {
            checkShutdownAccess();
            advanceRunState(536870912);
            interruptWorkers();
            List drainQueue = drainQueue();
            reentrantLock.unlock();
            tryTerminate();
            return drainQueue;
        } catch (Throwable th) {
            reentrantLock.unlock();
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public void terminated() {
    }

    /* access modifiers changed from: package-private */
    public final void tryTerminate() {
        while (true) {
            int i = this.ctl.get();
            if (!isRunning(i) && !runStateAtLeast(i, 1073741824)) {
                if (runStateOf(i) == 0 && !this.workQueue.isEmpty()) {
                    return;
                }
                if (workerCountOf(i) != 0) {
                    interruptIdleWorkers(true);
                    return;
                }
                ReentrantLock reentrantLock = this.mainLock;
                reentrantLock.lock();
                try {
                    if (this.ctl.compareAndSet(i, ctlOf(1073741824, 0))) {
                        terminated();
                        this.ctl.set(ctlOf(1610612736, 0));
                        this.termination.signalAll();
                        reentrantLock.unlock();
                        return;
                    }
                    reentrantLock.unlock();
                } catch (Throwable th) {
                    reentrantLock.unlock();
                    throw th;
                }
            } else {
                return;
            }
        }
    }
}
