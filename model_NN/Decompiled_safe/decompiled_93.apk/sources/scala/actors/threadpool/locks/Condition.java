package scala.actors.threadpool.locks;

public interface Condition {
    void signalAll();
}
