package scala.actors.threadpool.locks;

import java.io.Serializable;
import scala.actors.threadpool.helpers.FIFOWaitQueue;
import scala.actors.threadpool.helpers.WaitQueue;
import scala.actors.threadpool.locks.CondVar;

class FIFOCondVar extends CondVar implements Serializable, Condition {
    private static final WaitQueue.QueuedSync sync = new WaitQueue.QueuedSync() {
        public void takeOver(WaitQueue.WaitNode waitNode) {
        }
    };
    private final WaitQueue wq = new FIFOWaitQueue();

    FIFOCondVar(CondVar.ExclusiveLock exclusiveLock) {
        super(exclusiveLock);
    }

    public void signalAll() {
        if (!this.lock.isHeldByCurrentThread()) {
            throw new IllegalMonitorStateException();
        }
        while (true) {
            WaitQueue.WaitNode extract = this.wq.extract();
            if (extract != null) {
                extract.signal(sync);
            } else {
                return;
            }
        }
    }
}
