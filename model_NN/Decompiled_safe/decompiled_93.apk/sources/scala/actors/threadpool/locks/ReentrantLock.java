package scala.actors.threadpool.locks;

import java.io.Serializable;
import scala.actors.threadpool.locks.CondVar;

public class ReentrantLock implements Serializable, CondVar.ExclusiveLock {
    private final Sync sync = new NonfairSync();

    static final class NonfairSync extends Sync {
        NonfairSync() {
        }

        public final boolean isFair() {
            return false;
        }

        /* JADX WARNING: Can't wrap try/catch for region: R(4:17|18|19|20) */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x0039, code lost:
            r1 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x003b, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x003c, code lost:
            if (r1 != false) goto L_0x003e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:32:0x003e, code lost:
            java.lang.Thread.currentThread().interrupt();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:33:0x0045, code lost:
            throw r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
            return;
         */
        /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void lock() {
            /*
                r4 = this;
                r3 = 1
                java.lang.Thread r0 = java.lang.Thread.currentThread()
                monitor-enter(r4)
                java.lang.Thread r1 = r4.owner_     // Catch:{ all -> 0x001a }
                if (r1 != 0) goto L_0x0011
                r4.owner_ = r0     // Catch:{ all -> 0x001a }
                r0 = 1
                r4.holds_ = r0     // Catch:{ all -> 0x001a }
                monitor-exit(r4)     // Catch:{ all -> 0x001a }
            L_0x0010:
                return
            L_0x0011:
                java.lang.Thread r1 = r4.owner_     // Catch:{ all -> 0x001a }
                if (r0 != r1) goto L_0x001d
                r4.incHolds()     // Catch:{ all -> 0x001a }
                monitor-exit(r4)     // Catch:{ all -> 0x001a }
                goto L_0x0010
            L_0x001a:
                r0 = move-exception
                monitor-exit(r4)     // Catch:{ all -> 0x001a }
                throw r0
            L_0x001d:
                boolean r1 = java.lang.Thread.interrupted()     // Catch:{ all -> 0x001a }
            L_0x0021:
                r4.wait()     // Catch:{ InterruptedException -> 0x0038 }
            L_0x0024:
                java.lang.Thread r2 = r4.owner_     // Catch:{ all -> 0x003b }
                if (r2 != 0) goto L_0x0021
                r4.owner_ = r0     // Catch:{ all -> 0x003b }
                r0 = 1
                r4.holds_ = r0     // Catch:{ all -> 0x003b }
                if (r1 == 0) goto L_0x0036
                java.lang.Thread r0 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x001a }
                r0.interrupt()     // Catch:{ all -> 0x001a }
            L_0x0036:
                monitor-exit(r4)     // Catch:{ all -> 0x001a }
                goto L_0x0010
            L_0x0038:
                r1 = move-exception
                r1 = r3
                goto L_0x0024
            L_0x003b:
                r0 = move-exception
                if (r1 == 0) goto L_0x0045
                java.lang.Thread r1 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x001a }
                r1.interrupt()     // Catch:{ all -> 0x001a }
            L_0x0045:
                throw r0     // Catch:{ all -> 0x001a }
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.actors.threadpool.locks.ReentrantLock.NonfairSync.lock():void");
        }

        public synchronized void unlock() {
            if (Thread.currentThread() != this.owner_) {
                throw new IllegalMonitorStateException("Not owner");
            }
            int i = this.holds_ - 1;
            this.holds_ = i;
            if (i == 0) {
                this.owner_ = null;
                notify();
            }
        }
    }

    static abstract class Sync implements Serializable {
        protected transient int holds_ = 0;
        protected transient Thread owner_ = null;

        protected Sync() {
        }

        /* access modifiers changed from: protected */
        public synchronized Thread getOwner() {
            return this.owner_;
        }

        /* access modifiers changed from: package-private */
        public final void incHolds() {
            int i = this.holds_ + 1;
            this.holds_ = i;
            if (i < 0) {
                throw new Error("Maximum lock count exceeded");
            }
            this.holds_ = i;
        }

        public abstract boolean isFair();

        public synchronized boolean isHeldByCurrentThread() {
            return this.holds_ > 0 && Thread.currentThread() == this.owner_;
        }

        public synchronized boolean isLocked() {
            return this.owner_ != null;
        }

        public abstract void lock();

        public boolean tryLock() {
            Thread currentThread = Thread.currentThread();
            synchronized (this) {
                if (this.owner_ == null) {
                    this.owner_ = currentThread;
                    this.holds_ = 1;
                    return true;
                } else if (currentThread != this.owner_) {
                    return false;
                } else {
                    incHolds();
                    return true;
                }
            }
        }

        public abstract void unlock();
    }

    /* access modifiers changed from: protected */
    public Thread getOwner() {
        return this.sync.getOwner();
    }

    public final boolean isFair() {
        return this.sync.isFair();
    }

    public boolean isHeldByCurrentThread() {
        return this.sync.isHeldByCurrentThread();
    }

    public boolean isLocked() {
        return this.sync.isLocked();
    }

    public void lock() {
        this.sync.lock();
    }

    public Condition newCondition() {
        return isFair() ? new FIFOCondVar(this) : new CondVar(this);
    }

    public String toString() {
        Thread owner = getOwner();
        return super.toString() + (owner == null ? "[Unlocked]" : "[Locked by thread " + owner.getName() + "]");
    }

    public boolean tryLock() {
        return this.sync.tryLock();
    }

    public void unlock() {
        this.sync.unlock();
    }
}
