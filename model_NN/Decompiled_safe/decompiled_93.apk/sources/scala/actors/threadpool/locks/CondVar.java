package scala.actors.threadpool.locks;

import java.io.Serializable;

class CondVar implements Serializable, Condition {
    protected final ExclusiveLock lock;

    interface ExclusiveLock {
        boolean isHeldByCurrentThread();
    }

    CondVar(ExclusiveLock exclusiveLock) {
        this.lock = exclusiveLock;
    }

    public synchronized void signalAll() {
        if (!this.lock.isHeldByCurrentThread()) {
            throw new IllegalMonitorStateException();
        }
        notifyAll();
    }
}
