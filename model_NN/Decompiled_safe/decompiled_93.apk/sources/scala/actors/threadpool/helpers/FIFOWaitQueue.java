package scala.actors.threadpool.helpers;

import java.io.Serializable;
import scala.actors.threadpool.helpers.WaitQueue;

public class FIFOWaitQueue extends WaitQueue implements Serializable {
    protected transient WaitQueue.WaitNode head_ = null;
    protected transient WaitQueue.WaitNode tail_ = null;

    public WaitQueue.WaitNode extract() {
        if (this.head_ == null) {
            return null;
        }
        WaitQueue.WaitNode waitNode = this.head_;
        this.head_ = waitNode.next;
        if (this.head_ == null) {
            this.tail_ = null;
        }
        waitNode.next = null;
        return waitNode;
    }
}
