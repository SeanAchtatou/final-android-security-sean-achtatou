package scala.actors.threadpool.helpers;

public abstract class WaitQueue {

    public interface QueuedSync {
        void takeOver(WaitNode waitNode);
    }

    public static class WaitNode {
        WaitNode next = null;
        final Thread owner = Thread.currentThread();
        boolean waiting = true;

        public synchronized boolean signal(QueuedSync queuedSync) {
            boolean z;
            z = this.waiting;
            if (z) {
                this.waiting = false;
                notify();
                queuedSync.takeOver(this);
            }
            return z;
        }
    }

    public abstract WaitNode extract();
}
