package scala.actors.threadpool;

import java.io.Serializable;

public abstract class TimeUnit implements Serializable {
    public static final TimeUnit DAYS = new TimeUnit(6, "DAYS") {
        public long toNanos(long j) {
            return x(j, 86400000000000L, 106751);
        }
    };
    public static final TimeUnit HOURS = new TimeUnit(5, "HOURS") {
        public long toNanos(long j) {
            return x(j, 3600000000000L, 2562047);
        }
    };
    public static final TimeUnit MICROSECONDS = new TimeUnit(1, "MICROSECONDS") {
        public long toNanos(long j) {
            return x(j, 1000, 9223372036854775L);
        }
    };
    public static final TimeUnit MILLISECONDS = new TimeUnit(2, "MILLISECONDS") {
        public long toNanos(long j) {
            return x(j, 1000000, 9223372036854L);
        }
    };
    public static final TimeUnit MINUTES = new TimeUnit(4, "MINUTES") {
        public long toNanos(long j) {
            return x(j, 60000000000L, 153722867);
        }
    };
    public static final TimeUnit NANOSECONDS = new TimeUnit(0, "NANOSECONDS") {
        public long toNanos(long j) {
            return j;
        }
    };
    public static final TimeUnit SECONDS = new TimeUnit(3, "SECONDS") {
        public long toNanos(long j) {
            return x(j, 1000000000, 9223372036L);
        }
    };
    private static final TimeUnit[] values = {NANOSECONDS, MICROSECONDS, MILLISECONDS, SECONDS, MINUTES, HOURS, DAYS};
    private final int index;
    private final String name;

    TimeUnit(int i, String str) {
        this.index = i;
        this.name = str;
    }

    static long x(long j, long j2, long j3) {
        if (j > j3) {
            return Long.MAX_VALUE;
        }
        if (j < (-j3)) {
            return Long.MIN_VALUE;
        }
        return j * j2;
    }

    public abstract long toNanos(long j);

    public String toString() {
        return this.name;
    }
}
