package scala.actors.threadpool;

public interface ThreadFactory {
    Thread newThread(Runnable runnable);
}
