package scala.actors.threadpool;

import java.io.Serializable;

public class AtomicInteger extends Number implements Serializable {
    private volatile int value;

    public AtomicInteger() {
    }

    public AtomicInteger(int i) {
        this.value = i;
    }

    public final synchronized boolean compareAndSet(int i, int i2) {
        boolean z;
        if (this.value == i) {
            this.value = i2;
            z = true;
        } else {
            z = false;
        }
        return z;
    }

    public double doubleValue() {
        return (double) get();
    }

    public float floatValue() {
        return (float) get();
    }

    public final int get() {
        return this.value;
    }

    public int intValue() {
        return get();
    }

    public long longValue() {
        return (long) get();
    }

    public final synchronized void set(int i) {
        this.value = i;
    }

    public String toString() {
        return Integer.toString(get());
    }
}
