package scala.actors.threadpool;

public class RejectedExecutionException extends RuntimeException {
    public RejectedExecutionException() {
    }

    public RejectedExecutionException(String str) {
        super(str);
    }

    public RejectedExecutionException(String str, Throwable th) {
        super(str, th);
    }

    public RejectedExecutionException(Throwable th) {
        super(th);
    }
}
