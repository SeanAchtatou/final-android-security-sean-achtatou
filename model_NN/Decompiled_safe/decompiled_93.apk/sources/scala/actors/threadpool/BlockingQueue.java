package scala.actors.threadpool;

import java.util.Collection;
import java.util.Queue;

public interface BlockingQueue<E> extends Queue<E> {
    int drainTo(Collection<? super E> collection);

    boolean offer(E e);

    E poll(long j, TimeUnit timeUnit) throws InterruptedException;

    boolean remove(Object obj);

    E take() throws InterruptedException;
}
