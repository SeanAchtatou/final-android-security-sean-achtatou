package scala.actors;

import scala.ScalaObject;

/* compiled from: AbstractActor.scala */
public interface AbstractActor extends OutputChannel<Object>, ScalaObject {
    void exit(AbstractActor abstractActor, Object obj);

    boolean exiting();

    /* renamed from: scala.actors.AbstractActor$class  reason: invalid class name */
    /* compiled from: AbstractActor.scala */
    public abstract class Cclass {
        public static void $init$(AbstractActor $this) {
        }
    }
}
