package scala.actors;

import java.io.Serializable;
import scala.PartialFunction;
import scala.collection.immutable.List;
import scala.runtime.AbstractFunction2;
import scala.runtime.BoxesRunTime;

/* compiled from: Actor.scala */
public final class Actor$$anonfun$6 extends AbstractFunction2 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Actor $outer;
    public final /* synthetic */ PartialFunction f$1;

    public Actor$$anonfun$6(Actor $outer2, PartialFunction partialFunction) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.f$1 = partialFunction;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1, Object v2) {
        return BoxesRunTime.boxToBoolean(apply(v1, (OutputChannel<Object>) ((OutputChannel) v2)));
    }

    public final boolean apply(Object m, OutputChannel<Object> replyTo) {
        this.$outer.senders_$eq(this.$outer.senders().$colon$colon(replyTo));
        boolean matches = this.f$1.isDefinedAt(m);
        this.$outer.senders_$eq((List) this.$outer.senders().tail());
        return matches;
    }
}
