package scala.actors;

import java.io.Serializable;
import scala.Function0;
import scala.runtime.AbstractFunction0$mcV$sp;

/* compiled from: Combinators.scala */
public final class Combinators$$anonfun$loop$1 extends AbstractFunction0$mcV$sp implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Combinators $outer;
    public final /* synthetic */ Function0 body$1;

    public Combinators$$anonfun$loop$1(Combinators $outer2, Function0 function0) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.body$1 = function0;
    }

    public final void apply() {
        this.$outer.loop(this.body$1);
    }

    public void apply$mcV$sp() {
        this.$outer.loop(this.body$1);
    }
}
