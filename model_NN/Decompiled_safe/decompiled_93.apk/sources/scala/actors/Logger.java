package scala.actors;

import scala.ScalaObject;
import scala.collection.mutable.StringBuilder;

/* compiled from: Debug.scala */
public class Logger implements ScalaObject {
    private int lev = 2;
    private final String tagString;

    public Logger(String tag) {
        this.tagString = (tag != null ? !tag.equals("") : "" != 0) ? new StringBuilder().append((Object) " [").append((Object) tag).append((Object) "]").toString() : "";
    }

    private int lev() {
        return this.lev;
    }

    private String tagString() {
        return this.tagString;
    }

    public void info(String s) {
        if (lev() > 2) {
            System.out.println(new StringBuilder().append((Object) "Info").append((Object) tagString()).append((Object) ": ").append((Object) s).toString());
        }
    }
}
