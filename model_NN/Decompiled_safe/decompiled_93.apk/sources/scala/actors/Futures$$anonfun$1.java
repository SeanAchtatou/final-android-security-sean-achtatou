package scala.actors;

import java.io.Serializable;
import scala.Function0;
import scala.concurrent.SyncVar;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxedUnit;

/* compiled from: Future.scala */
public final class Futures$$anonfun$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Function0 body$1;

    public Futures$$anonfun$1(Function0 function0) {
        this.body$1 = function0;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        apply((SyncVar) ((SyncVar) v1));
        return BoxedUnit.UNIT;
    }

    public final void apply(SyncVar<T> syncVar) {
        syncVar.set(this.body$1.apply());
    }
}
