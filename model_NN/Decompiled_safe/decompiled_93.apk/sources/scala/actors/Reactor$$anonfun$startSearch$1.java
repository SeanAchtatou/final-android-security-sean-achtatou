package scala.actors;

import java.io.Serializable;
import scala.PartialFunction;
import scala.runtime.AbstractFunction0$mcV$sp;

/* compiled from: Reactor.scala */
public final class Reactor$$anonfun$startSearch$1 extends AbstractFunction0$mcV$sp implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Reactor $outer;
    public final /* synthetic */ PartialFunction handler$1;
    public final /* synthetic */ Object msg$1;
    public final /* synthetic */ OutputChannel replyTo$1;

    public Reactor$$anonfun$startSearch$1(Reactor $outer2, Object obj, OutputChannel outputChannel, PartialFunction partialFunction) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.msg$1 = obj;
        this.replyTo$1 = outputChannel;
        this.handler$1 = partialFunction;
    }

    public final void apply() {
        this.$outer.scheduler().execute(this.$outer.makeReaction(new Reactor$$anonfun$startSearch$1$$anonfun$apply$mcV$sp$1(this)));
    }

    public void apply$mcV$sp() {
        this.$outer.scheduler().execute(this.$outer.makeReaction(new Reactor$$anonfun$startSearch$1$$anonfun$apply$mcV$sp$1(this)));
    }
}
