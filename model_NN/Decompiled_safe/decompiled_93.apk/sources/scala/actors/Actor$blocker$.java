package scala.actors;

import scala.ScalaObject;
import scala.actors.Actor;
import scala.concurrent.ManagedBlocker;

/* compiled from: Actor.scala */
public final class Actor$blocker$ implements ScalaObject, ManagedBlocker {
    private final /* synthetic */ Actor $outer;

    public Actor$blocker$(Actor $outer2) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
    }

    public boolean block() {
        Actor.Cclass.scala$actors$Actor$$suspendActor(this.$outer);
        return true;
    }

    public boolean isReleasable() {
        return !this.$outer.scala$actors$Actor$$isSuspended();
    }
}
