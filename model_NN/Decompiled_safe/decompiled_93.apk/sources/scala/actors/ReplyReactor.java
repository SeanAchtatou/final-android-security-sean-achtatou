package scala.actors;

import java.util.TimerTask;
import scala.None$;
import scala.Option;
import scala.PartialFunction;
import scala.Predef$;
import scala.ScalaObject;
import scala.Tuple2;
import scala.collection.Seq;
import scala.collection.immutable.List;
import scala.collection.immutable.List$;
import scala.collection.immutable.Nil$;
import scala.collection.mutable.StringBuilder;
import scala.runtime.Nothing$;

/* compiled from: ReplyReactor.scala */
public interface ReplyReactor extends Reactor<Object>, ReactorCanReply, ScalaObject {
    void $bang(Object obj);

    Option<TimerTask> onTimeout();

    void onTimeout_$eq(Option<TimerTask> option);

    void reply(Object obj);

    void resumeReceiver(Tuple2<Object, OutputChannel<Object>> tuple2, PartialFunction<Object, Object> partialFunction, boolean z);

    Nothing$ scala$actors$ReplyReactor$$super$react(PartialFunction partialFunction);

    void scala$actors$ReplyReactor$$super$resumeReceiver(Tuple2 tuple2, PartialFunction partialFunction, boolean z);

    OutputChannel<Object> sender();

    List<OutputChannel<Object>> senders();

    void senders_$eq(List<OutputChannel<Object>> list);

    /* renamed from: scala.actors.ReplyReactor$class  reason: invalid class name */
    /* compiled from: ReplyReactor.scala */
    public abstract class Cclass {
        public static void $init$(ReplyReactor $this) {
            $this.senders_$eq(Nil$.MODULE$);
            $this.onTimeout_$eq(None$.MODULE$);
        }

        public static OutputChannel sender(ReplyReactor $this) {
            return $this.senders().head();
        }

        public static void reply(ReplyReactor $this, Object msg) {
            $this.sender().$bang(msg);
        }

        public static void $bang(ReplyReactor $this, Object msg) {
            $this.send(msg, Actor$.MODULE$.rawSelf($this.scheduler()));
        }

        public static void resumeReceiver(ReplyReactor $this, Tuple2 item, PartialFunction handler, boolean onSameThread) {
            synchronized ($this) {
                if (!$this.onTimeout().isEmpty()) {
                    $this.onTimeout().get().cancel();
                    $this.onTimeout_$eq(None$.MODULE$);
                }
            }
            $this.senders_$eq(List$.MODULE$.apply((Seq) Predef$.MODULE$.wrapRefArray((Object[]) new OutputChannel[]{(OutputChannel) item._2()})));
            $this.scala$actors$ReplyReactor$$super$resumeReceiver(item, handler, onSameThread);
        }

        public static Nothing$ react(ReplyReactor $this, PartialFunction handler) {
            ReplyReactor rawSelf = Actor$.MODULE$.rawSelf($this.scheduler());
            if (rawSelf != null ? rawSelf.equals($this) : $this == null) {
                return $this.scala$actors$ReplyReactor$$super$react(handler);
            }
            throw new AssertionError(new StringBuilder().append((Object) "assertion failed: ").append((Object) "react on channel belonging to other actor").toString());
        }
    }
}
