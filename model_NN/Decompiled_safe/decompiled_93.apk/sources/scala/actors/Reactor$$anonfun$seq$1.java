package scala.actors;

import java.io.Serializable;
import scala.Function0;
import scala.runtime.AbstractFunction0;
import scala.runtime.Nothing$;

/* compiled from: Reactor.scala */
public final class Reactor$$anonfun$seq$1 extends AbstractFunction0 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Reactor $outer;
    public final /* synthetic */ Function0 killNext$1;
    public final /* synthetic */ Function0 next$1;

    public Reactor$$anonfun$seq$1(Reactor $outer2, Function0 function0, Function0 function02) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.next$1 = function0;
        this.killNext$1 = function02;
    }

    public final Nothing$ apply() {
        this.$outer.kill_$eq(this.killNext$1);
        this.$outer.scheduleActor(new Reactor$$anonfun$seq$1$$anonfun$apply$1(this), null);
        throw Actor$.MODULE$.suspendException();
    }
}
