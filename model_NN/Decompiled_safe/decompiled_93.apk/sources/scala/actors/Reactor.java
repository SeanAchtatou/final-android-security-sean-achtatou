package scala.actors;

import scala.Enumeration;
import scala.Function0;
import scala.PartialFunction;
import scala.Predef$;
import scala.ScalaObject;
import scala.Tuple2;
import scala.actors.Actor;
import scala.runtime.Nothing$;

/* compiled from: Reactor.scala */
public interface Reactor<Msg> extends OutputChannel<Msg>, Combinators, ScalaObject {
    Enumeration.Value _state();

    void _state_$eq(Enumeration.Value value);

    void act();

    void dostart();

    void drainSendBuffer(MQueue<Msg> mQueue);

    PartialFunction<Exception, Object> exceptionHandler();

    Nothing$ exit();

    Function0<Object> kill();

    void kill_$eq(Function0<Object> function0);

    MQueue<Msg> mailbox();

    Runnable makeReaction(Function0<Object> function0);

    Runnable makeReaction(Function0<Object> function0, PartialFunction<Msg, Object> partialFunction, Msg msg);

    <A> Object mkBody(Function0<A> function0);

    void scala$actors$Reactor$_setter_$mailbox_$eq(MQueue mQueue);

    void scala$actors$Reactor$_setter_$sendBuffer_$eq(MQueue mQueue);

    void scheduleActor(PartialFunction<Msg, Object> partialFunction, Msg msg);

    IScheduler scheduler();

    void searchMailbox(MQueue<Msg> mQueue, PartialFunction<Msg, Object> partialFunction, boolean z);

    void send(Msg msg, OutputChannel<Object> outputChannel);

    MQueue<Msg> sendBuffer();

    <a, b> void seq(Function0<a> function0, Function0<b> function02);

    Function0<Object> startSearch(Msg msg, OutputChannel<Object> outputChannel, PartialFunction<Msg, Object> partialFunction);

    void terminated();

    PartialFunction<Msg, Object> waitingFor();

    void waitingFor_$eq(PartialFunction<Msg, Object> partialFunction);

    /* renamed from: scala.actors.Reactor$class  reason: invalid class name */
    /* compiled from: Reactor.scala */
    public abstract class Cclass {
        public static void $init$(Reactor reactor) {
            reactor.scala$actors$Reactor$_setter_$mailbox_$eq(new MQueue("Reactor"));
            reactor.scala$actors$Reactor$_setter_$sendBuffer_$eq(new MQueue("SendBuffer"));
            reactor.waitingFor_$eq(Reactor$.MODULE$.waitingForNone());
            reactor._state_$eq(Actor$State$.MODULE$.New());
            reactor.kill_$eq(new Reactor$$anonfun$2(reactor));
        }

        public static PartialFunction exceptionHandler(Reactor $this) {
            return Predef$.MODULE$.Map().apply(Predef$.MODULE$.wrapRefArray((Object[]) new Tuple2[0]));
        }

        public static void send(Reactor $this, Object msg, OutputChannel replyTo) {
            ScalaObject reactor$$anonfun$1;
            synchronized ($this) {
                if ($this.waitingFor() != Reactor$.MODULE$.waitingForNone()) {
                    PartialFunction savedWaitingFor = $this.waitingFor();
                    $this.waitingFor_$eq(Reactor$.MODULE$.waitingForNone());
                    reactor$$anonfun$1 = $this.startSearch(msg, replyTo, savedWaitingFor);
                } else {
                    $this.sendBuffer().append(msg, replyTo);
                    reactor$$anonfun$1 = new Reactor$$anonfun$1($this);
                }
            }
            ((Function0) reactor$$anonfun$1).apply$mcV$sp();
        }

        public static Function0 startSearch(Reactor $this, Object msg$1, OutputChannel replyTo$1, PartialFunction handler$1) {
            return new Reactor$$anonfun$startSearch$1($this, msg$1, replyTo$1, handler$1);
        }

        public static final Runnable makeReaction(Reactor $this, Function0 fun) {
            return $this.makeReaction(fun, null, null);
        }

        public static void resumeReceiver(Reactor $this, Tuple2 item, PartialFunction handler, boolean onSameThread) {
            if (onSameThread) {
                $this.makeReaction(null, handler, item._1()).run();
            } else {
                $this.scheduleActor(handler, item._1());
            }
            throw Actor$.MODULE$.suspendException();
        }

        public static void drainSendBuffer(Reactor $this, MQueue mbox) {
            $this.sendBuffer().foreachDequeue(mbox);
        }

        public static Nothing$ react(Reactor $this, PartialFunction handler) {
            synchronized ($this) {
                $this.drainSendBuffer($this.mailbox());
            }
            $this.searchMailbox($this.mailbox(), handler, false);
            throw Actor$.MODULE$.suspendException();
        }

        public static void dostart(Reactor $this) {
            $this._state_$eq(Actor$State$.MODULE$.Runnable());
            $this.scheduler().newActor($this);
            $this.scheduler().execute($this.makeReaction(new Reactor$$anonfun$dostart$1($this), null, null));
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x0017, code lost:
            if (r0.equals(r1) != false) goto L_0x0019;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:11:0x0019, code lost:
            r2.dostart();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:4:0x000d, code lost:
            if (r1 != null) goto L_0x000f;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public static scala.actors.Reactor start(scala.actors.Reactor r2) {
            /*
                monitor-enter(r2)
                scala.Enumeration$Value r0 = r2._state()     // Catch:{ all -> 0x001d }
                scala.actors.Actor$State$ r1 = scala.actors.Actor$State$.MODULE$     // Catch:{ all -> 0x001d }
                scala.Enumeration$Value r1 = r1.New()     // Catch:{ all -> 0x001d }
                if (r0 != 0) goto L_0x0013
                if (r1 == 0) goto L_0x0019
            L_0x000f:
                monitor-exit(r2)     // Catch:{ all -> 0x001d }
                scala.actors.Reactor r2 = (scala.actors.Reactor) r2
                return r2
            L_0x0013:
                boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x001d }
                if (r0 == 0) goto L_0x000f
            L_0x0019:
                r2.dostart()     // Catch:{ all -> 0x001d }
                goto L_0x000f
            L_0x001d:
                r0 = move-exception
                monitor-exit(r2)
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.actors.Reactor.Cclass.start(scala.actors.Reactor):scala.actors.Reactor");
        }

        public static Actor.Body mkBody(Reactor $this, Function0 body$1) {
            return new Reactor$$anon$3($this, body$1);
        }

        public static void seq(Reactor $this, Function0 first, Function0 next$1) {
            $this.kill_$eq(new Reactor$$anonfun$seq$1($this, next$1, $this.kill()));
            first.apply();
            throw new KillActorControl();
        }

        public static Nothing$ exit(Reactor $this) {
            $this.terminated();
            throw Actor$.MODULE$.suspendException();
        }

        public static void terminated(Reactor $this) {
            synchronized ($this) {
                $this._state_$eq(Actor$State$.MODULE$.Terminated());
                $this.waitingFor_$eq(Reactor$.MODULE$.waitingForNone());
            }
            $this.scheduler().terminated($this);
        }
    }
}
