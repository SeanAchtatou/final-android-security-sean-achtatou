package scala.actors;

import java.io.Serializable;
import scala.runtime.AbstractFunction2;
import scala.runtime.BoxedUnit;

/* compiled from: Actor.scala */
public final class Actor$$anonfun$searchMailbox$1 extends AbstractFunction2 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Actor $outer;

    public Actor$$anonfun$searchMailbox$1(Actor $outer2) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1, Object v2) {
        apply(v1, (OutputChannel<Object>) ((OutputChannel) v2));
        return BoxedUnit.UNIT;
    }

    public final void apply(Object m, OutputChannel<Object> s) {
        this.$outer.mailbox().append(m, s);
    }
}
