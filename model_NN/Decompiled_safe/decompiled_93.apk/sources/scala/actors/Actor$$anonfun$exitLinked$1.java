package scala.actors;

import java.io.Serializable;
import scala.collection.immutable.List;
import scala.runtime.AbstractFunction0$mcV$sp;

/* compiled from: Actor.scala */
public final class Actor$$anonfun$exitLinked$1 extends AbstractFunction0$mcV$sp implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Actor $outer;
    public final /* synthetic */ List mylinks$1;

    public Actor$$anonfun$exitLinked$1(Actor $outer2, List list) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.mylinks$1 = list;
    }

    public final void apply() {
        this.mylinks$1.foreach(new Actor$$anonfun$exitLinked$1$$anonfun$apply$mcV$sp$1(this));
    }

    public void apply$mcV$sp() {
        this.mylinks$1.foreach(new Actor$$anonfun$exitLinked$1$$anonfun$apply$mcV$sp$1(this));
    }
}
