package scala.actors;

import scala.PartialFunction;
import scala.ScalaObject;
import scala.actors.scheduler.DelegatingScheduler;

/* compiled from: Reactor.scala */
public final class Reactor$ implements ScalaObject {
    public static final Reactor$ MODULE$ = null;
    private final DelegatingScheduler scheduler = new Reactor$$anon$2();
    private final PartialFunction waitingForNone = new Reactor$$anon$1();

    static {
        new Reactor$();
    }

    private Reactor$() {
        MODULE$ = this;
    }

    public PartialFunction waitingForNone() {
        return this.waitingForNone;
    }
}
