package scala.actors;

import scala.Function0;
import scala.None$;
import scala.Option;
import scala.PartialFunction;
import scala.ScalaObject;
import scala.Some;

/* compiled from: ActorTask.scala */
public class ActorTask extends ReplyReactorTask implements ScalaObject {
    private final Actor actor;
    private final Object msg;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActorTask(Actor actor2, Function0<Object> fun, PartialFunction<Object, Object> handler, Object msg2) {
        super(actor2, fun, handler, msg2);
        this.actor = actor2;
        this.msg = msg2;
    }

    public void beginExecution() {
        super.beginExecution();
        synchronized (this.actor) {
            if (this.actor.shouldExit()) {
                throw this.actor.exit();
            }
        }
    }

    public void terminateExecution(Throwable e) {
        Option option;
        ActorTask$$anonfun$1 todo;
        try {
            option = new Some(this.actor.sender());
        } catch (Exception e2) {
            option = None$.MODULE$;
        }
        UncaughtException uncaught = new UncaughtException(this.actor, this.msg == null ? None$.MODULE$ : new Some(this.msg), option, Thread.currentThread(), e);
        synchronized (this.actor) {
            if (this.actor.links().isEmpty()) {
                super.terminateExecution(e);
                todo = new ActorTask$$anonfun$1(this);
            } else {
                todo = this.actor.exitLinked(uncaught);
            }
        }
        todo.apply$mcV$sp();
    }
}
