package scala.actors;

import scala.Enumeration;
import scala.Function0;
import scala.None$;
import scala.Option;
import scala.PartialFunction;
import scala.ScalaObject;
import scala.Some;
import scala.Symbol;
import scala.Symbol$;
import scala.Tuple2;
import scala.collection.immutable.List;
import scala.collection.immutable.Nil$;
import scala.collection.mutable.StringBuilder;
import scala.runtime.Nothing$;

/* compiled from: Actor.scala */
public interface Actor extends AbstractActor, ReplyReactor, ActorCanReply, ScalaObject, ReplyReactor {
    public static final long serialVersionUID = -781154067877019505L;

    /* compiled from: Actor.scala */
    public interface Body<a> {
        <b> void andThen(Function0<b> function0);
    }

    Nothing$ exit();

    Function0<Object> exitLinked();

    Function0<Object> exitLinked(Object obj);

    List<AbstractActor> links();

    void links_$eq(List<AbstractActor> list);

    Nothing$ react(PartialFunction<Object, Object> partialFunction);

    <R> R receive(PartialFunction<Object, R> partialFunction);

    Actor$blocker$ scala$actors$Actor$$blocker();

    Object scala$actors$Actor$$exitReason();

    void scala$actors$Actor$$exitReason_$eq(Object obj);

    boolean scala$actors$Actor$$isSuspended();

    void scala$actors$Actor$$isSuspended_$eq(boolean z);

    Option scala$actors$Actor$$received();

    void scala$actors$Actor$$received_$eq(Option option);

    void scala$actors$Actor$$super$dostart();

    Nothing$ scala$actors$Actor$$super$exit();

    Nothing$ scala$actors$Actor$$super$react(PartialFunction partialFunction);

    Reactor scala$actors$Actor$$super$start();

    Function0 scala$actors$Actor$$super$startSearch(Object obj, OutputChannel outputChannel, PartialFunction partialFunction);

    void scheduleActor(PartialFunction<Object, Object> partialFunction, Object obj);

    IScheduler scheduler();

    boolean shouldExit();

    void shouldExit_$eq(boolean z);

    Actor start();

    boolean trapExit();

    void trapExit_$eq(boolean z);

    void unlinkFrom(AbstractActor abstractActor);

    /* renamed from: scala.actors.Actor$class  reason: invalid class name */
    /* compiled from: Actor.scala */
    public abstract class Cclass {
        private static final /* synthetic */ Symbol symbol$1 = ((Symbol) Symbol$.MODULE$.apply("normal"));

        public static void $init$(Actor $this) {
            $this.scala$actors$Actor$$isSuspended_$eq(false);
            $this.scala$actors$Actor$$received_$eq(None$.MODULE$);
            $this.links_$eq(Nil$.MODULE$);
            $this.trapExit_$eq(false);
            $this.scala$actors$Actor$$exitReason_$eq(symbol$1);
            $this.shouldExit_$eq(false);
        }

        public static Function0 startSearch(Actor $this, Object msg$1, OutputChannel replyTo$1, PartialFunction handler) {
            if ($this.scala$actors$Actor$$isSuspended()) {
                return new Actor$$anonfun$startSearch$1($this, msg$1, replyTo$1);
            }
            return $this.scala$actors$Actor$$super$startSearch(msg$1, replyTo$1, handler);
        }

        public static void searchMailbox(Actor $this, MQueue startMbox, PartialFunction handler$1, boolean resumeOnSameThread) {
            MQueue tmpMbox;
            boolean done = false;
            MQueue tmpMbox2 = startMbox;
            while (!done) {
                MQueueElement qel = tmpMbox2.extractFirst(new Actor$$anonfun$5($this, handler$1));
                if (tmpMbox2 != $this.mailbox()) {
                    tmpMbox2.foreach(new Actor$$anonfun$searchMailbox$1($this));
                }
                if (qel == null) {
                    synchronized ($this) {
                        try {
                            if (!$this.sendBuffer().isEmpty()) {
                                tmpMbox = new MQueue("Temp");
                                try {
                                    $this.drainSendBuffer(tmpMbox);
                                } catch (Throwable th) {
                                    th = th;
                                    throw th;
                                }
                            } else if ($this.shouldExit()) {
                                throw $this.exit();
                            } else {
                                $this.waitingFor_$eq(handler$1);
                                throw Actor$.MODULE$.suspendException();
                            }
                        } catch (Throwable th2) {
                            th = th2;
                            throw th;
                        }
                    }
                } else {
                    $this.resumeReceiver(new Tuple2(qel.msg(), qel.session()), handler$1, resumeOnSameThread);
                    done = true;
                    tmpMbox = tmpMbox2;
                }
                tmpMbox2 = tmpMbox;
            }
        }

        public static Runnable makeReaction(Actor $this, Function0 fun, PartialFunction handler, Object msg) {
            return new ActorTask($this, fun, handler, msg);
        }

        public static Object receive(Actor $this, PartialFunction f$1) {
            Actor actor = (Actor) Actor$.MODULE$.rawSelf($this.scheduler());
            if (!(actor != null ? actor.equals($this) : $this == null)) {
                throw new AssertionError(new StringBuilder().append((Object) "assertion failed: ").append((Object) "receive from channel belonging to other actor").toString());
            }
            synchronized ($this) {
                if ($this.shouldExit()) {
                    throw $this.exit();
                }
                $this.drainSendBuffer($this.mailbox());
            }
            boolean done = false;
            while (!done) {
                MQueueElement qel = $this.mailbox().extractFirst(new Actor$$anonfun$6($this, f$1));
                if (qel == null) {
                    synchronized ($this) {
                        if ($this.sendBuffer().isEmpty()) {
                            $this.waitingFor_$eq(f$1);
                            $this.scala$actors$Actor$$isSuspended_$eq(true);
                            $this.scheduler().managedBlock($this.scala$actors$Actor$$blocker());
                            $this.drainSendBuffer($this.mailbox());
                        } else {
                            $this.drainSendBuffer($this.mailbox());
                        }
                    }
                } else {
                    $this.scala$actors$Actor$$received_$eq(new Some(qel.msg()));
                    $this.senders_$eq($this.senders().$colon$colon(qel.session()));
                    done = true;
                }
            }
            Object result = f$1.apply($this.scala$actors$Actor$$received().get());
            $this.scala$actors$Actor$$received_$eq(None$.MODULE$);
            $this.senders_$eq((List) $this.senders().tail());
            return result;
        }

        public static Nothing$ react(Actor $this, PartialFunction handler) {
            synchronized ($this) {
                if ($this.shouldExit()) {
                    throw $this.exit();
                }
            }
            return $this.scala$actors$Actor$$super$react(handler);
        }

        public static void scheduleActor(Actor $this, PartialFunction f, Object msg) {
            if (f != null) {
                $this.scheduler().executeFromActor(new ActorTask($this, null, f, msg));
            }
        }

        public static final void scala$actors$Actor$$suspendActor(Actor $this) {
            synchronized ($this) {
                while ($this.scala$actors$Actor$$isSuspended()) {
                    liftedTree1$1($this);
                }
                if ($this.shouldExit()) {
                    throw $this.exit();
                }
            }
        }

        private static final void liftedTree1$1(Actor $this) {
            try {
                $this.wait();
            } catch (InterruptedException e) {
            }
        }

        public static final void scala$actors$Actor$$resumeActor(Actor $this) {
            $this.scala$actors$Actor$$isSuspended_$eq(false);
            $this.notify();
        }

        public static boolean exiting(Actor $this) {
            boolean z;
            synchronized ($this) {
                Enumeration.Value _state = $this._state();
                Enumeration.Value Terminated = Actor$State$.MODULE$.Terminated();
                z = _state != null ? _state.equals(Terminated) : Terminated == null;
            }
            return z;
        }

        public static void dostart(Actor $this) {
            $this.scala$actors$Actor$$exitReason_$eq(symbol$1);
            $this.shouldExit_$eq(false);
            $this.scala$actors$Actor$$super$dostart();
        }

        public static Actor start(Actor $this) {
            synchronized ($this) {
                $this.scala$actors$Actor$$super$start();
            }
            return $this;
        }

        public static void unlinkFrom(Actor $this, AbstractActor from$2) {
            synchronized ($this) {
                $this.links_$eq((List) $this.links().filterNot(new Actor$$anonfun$unlinkFrom$1($this, from$2)));
            }
        }

        public static Nothing$ exit(Actor $this) {
            Actor$$anonfun$4 todo;
            synchronized ($this) {
                if ($this.links().isEmpty()) {
                    todo = new Actor$$anonfun$4($this);
                } else {
                    todo = $this.exitLinked();
                }
            }
            todo.apply$mcV$sp();
            return $this.scala$actors$Actor$$super$exit();
        }

        public static Function0 exitLinked(Actor $this) {
            $this._state_$eq(Actor$State$.MODULE$.Terminated());
            $this.waitingFor_$eq(Reactor$.MODULE$.waitingForNone());
            List mylinks$1 = (List) $this.links().filterNot(new Actor$$anonfun$8($this));
            mylinks$1.foreach(new Actor$$anonfun$exitLinked$2($this));
            return new Actor$$anonfun$exitLinked$1($this, mylinks$1);
        }

        public static Function0 exitLinked(Actor $this, Object reason) {
            $this.scala$actors$Actor$$exitReason_$eq(reason);
            return $this.exitLinked();
        }

        public static void exit(Actor $this, AbstractActor from, Object reason) {
            if ($this.trapExit()) {
                $this.$bang(new Exit(from, reason));
                return;
            }
            Symbol symbol = symbol$1;
            if (reason == null) {
                if (symbol == null) {
                    return;
                }
            } else if (reason.equals(symbol)) {
                return;
            }
            synchronized ($this) {
                $this.shouldExit_$eq(true);
                $this.scala$actors$Actor$$exitReason_$eq(reason);
                if ($this.scala$actors$Actor$$isSuspended()) {
                    scala$actors$Actor$$resumeActor($this);
                } else if ($this.waitingFor() != Reactor$.MODULE$.waitingForNone()) {
                    $this.waitingFor_$eq(Reactor$.MODULE$.waitingForNone());
                    $this.scheduleActor($this.waitingFor(), null);
                }
            }
        }
    }
}
