package scala.actors;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;

/* compiled from: Actor.scala */
public final class Actor$$anonfun$unlinkFrom$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ AbstractActor from$2;

    public Actor$$anonfun$unlinkFrom$1(Actor $outer, AbstractActor abstractActor) {
        this.from$2 = abstractActor;
    }

    public final boolean apply(Object obj) {
        AbstractActor abstractActor = this.from$2;
        return abstractActor != null ? abstractActor.equals(obj) : obj == null;
    }
}
