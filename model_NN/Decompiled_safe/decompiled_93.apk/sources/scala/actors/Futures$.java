package scala.actors;

import scala.Function0;
import scala.ScalaObject;
import scala.actors.scheduler.DaemonScheduler$;

/* compiled from: Future.scala */
public final class Futures$ implements ScalaObject {
    public static final Futures$ MODULE$ = null;

    static {
        new Futures$();
    }

    private Futures$() {
        MODULE$ = this;
    }

    public <T> Future<T> future(Function0<T> body$1) {
        FutureActor a = new FutureActor(new Futures$$anonfun$1(body$1), new Channel((Actor) Actor$.MODULE$.rawSelf(DaemonScheduler$.MODULE$)));
        a.start();
        return a;
    }
}
