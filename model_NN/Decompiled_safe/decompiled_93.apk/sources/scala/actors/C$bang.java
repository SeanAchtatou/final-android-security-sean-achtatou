package scala.actors;

import scala.Product;
import scala.ScalaObject;
import scala.collection.Iterator;
import scala.collection.mutable.StringBuilder;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime$;

/* renamed from: scala.actors.$bang  reason: invalid class name */
/* compiled from: Channel.scala */
public class C$bang<a> implements ScalaObject, Product, ScalaObject {
    private final Channel<a> ch;
    private final a msg;

    public C$bang(Channel<a> ch2, a msg2) {
        this.ch = ch2;
        this.msg = msg2;
        Product.Cclass.$init$(this);
    }

    private final /* synthetic */ boolean gd1$1(Channel channel, Object obj) {
        Channel ch2 = ch();
        if (channel != null ? channel.equals(ch2) : ch2 == null) {
            Object msg2 = msg();
            if (obj != msg2 ? obj != null ? !(obj instanceof Number) ? !(obj instanceof Character) ? obj.equals(msg2) : BoxesRunTime.equalsCharObject((Character) obj, msg2) : BoxesRunTime.equalsNumObject((Number) obj, msg2) : false : true) {
                return true;
            }
        }
        return false;
    }

    public boolean canEqual(Object obj) {
        return obj instanceof C$bang;
    }

    public Channel<a> ch() {
        return this.ch;
    }

    public boolean equals(Object obj) {
        boolean z;
        if (this != obj) {
            if (obj instanceof C$bang) {
                C$bang _bang = (C$bang) obj;
                z = gd1$1(_bang.ch(), _bang.msg()) ? ((C$bang) obj).canEqual(this) : false;
            } else {
                z = false;
            }
            if (!z) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return ScalaRunTime$.MODULE$._hashCode(this);
    }

    public a msg() {
        return this.msg;
    }

    public int productArity() {
        return 2;
    }

    public Object productElement(int i) {
        switch (i) {
            case 0:
                return ch();
            case 1:
                return msg();
            default:
                throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(i).toString());
        }
    }

    public Iterator<Object> productIterator() {
        return Product.Cclass.productIterator(this);
    }

    public String productPrefix() {
        return "!";
    }

    public String toString() {
        return productIterator().mkString(new StringBuilder().append((Object) productPrefix()).append((Object) "(").toString(), ",", ")");
    }
}
