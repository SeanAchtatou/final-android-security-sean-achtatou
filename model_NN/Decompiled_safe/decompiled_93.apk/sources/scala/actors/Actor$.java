package scala.actors;

import java.io.Serializable;
import java.util.Timer;
import scala.Function0;
import scala.ScalaObject;
import scala.actors.Combinators;

/* compiled from: Actor.scala */
public final class Actor$ implements Serializable, ScalaObject, Combinators {
    public static final Actor$ MODULE$ = null;
    private final SuspendActorControl suspendException = new SuspendActorControl();
    private final Timer timer = new Timer(true);
    private final ThreadLocal<ReplyReactor> tl = new ThreadLocal<>();

    static {
        new Actor$();
    }

    private Actor$() {
        MODULE$ = this;
        Combinators.Cclass.$init$(this);
    }

    public void loop(Function0<Object> body) {
        Combinators.Cclass.loop(this, body);
    }

    public ThreadLocal<ReplyReactor> tl() {
        return this.tl;
    }

    public SuspendActorControl suspendException() {
        return this.suspendException;
    }

    public Actor self() {
        return (Actor) rawSelf(Scheduler$.MODULE$);
    }

    public ReplyReactor rawSelf(IScheduler sched) {
        ReplyReactor s = this.tl.get();
        if (s != null) {
            return s;
        }
        ActorProxy r = new ActorProxy(Thread.currentThread(), sched);
        this.tl.set(r);
        return r;
    }

    public <a> Object mkBody(Function0<a> body$3) {
        return new Actor$$anon$6(body$3);
    }
}
