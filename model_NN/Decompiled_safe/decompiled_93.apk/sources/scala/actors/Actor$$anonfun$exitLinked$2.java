package scala.actors;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxedUnit;

/* compiled from: Actor.scala */
public final class Actor$$anonfun$exitLinked$2 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Actor $outer;

    public Actor$$anonfun$exitLinked$2(Actor $outer2) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        apply((AbstractActor) v1);
        return BoxedUnit.UNIT;
    }

    public final void apply(AbstractActor abstractActor) {
        this.$outer.unlinkFrom(abstractActor);
    }
}
