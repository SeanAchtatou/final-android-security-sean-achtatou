package scala.actors;

import scala.Enumeration;
import scala.ScalaObject;

/* compiled from: Actor.scala */
public final class Actor$State$ extends Enumeration implements ScalaObject {
    public static final Actor$State$ MODULE$ = null;
    private final Enumeration.Value Blocked = Value();
    private final Enumeration.Value New = Value();
    private final Enumeration.Value Runnable = Value();
    private final Enumeration.Value Suspended = Value();
    private final Enumeration.Value Terminated = Value();
    private final Enumeration.Value TimedBlocked = Value();
    private final Enumeration.Value TimedSuspended = Value();

    static {
        new Actor$State$();
    }

    public Actor$State$() {
        MODULE$ = this;
    }

    public Enumeration.Value New() {
        return this.New;
    }

    public Enumeration.Value Runnable() {
        return this.Runnable;
    }

    public Enumeration.Value Terminated() {
        return this.Terminated;
    }
}
