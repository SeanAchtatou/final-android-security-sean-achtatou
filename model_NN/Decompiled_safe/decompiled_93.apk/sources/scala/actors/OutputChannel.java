package scala.actors;

/* compiled from: OutputChannel.scala */
public interface OutputChannel<Msg> {
    void $bang(Msg msg);
}
