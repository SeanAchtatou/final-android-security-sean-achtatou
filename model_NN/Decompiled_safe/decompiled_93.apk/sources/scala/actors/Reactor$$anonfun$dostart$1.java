package scala.actors;

import java.io.Serializable;
import scala.runtime.AbstractFunction0$mcV$sp;

/* compiled from: Reactor.scala */
public final class Reactor$$anonfun$dostart$1 extends AbstractFunction0$mcV$sp implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Reactor $outer;

    public Reactor$$anonfun$dostart$1(Reactor<Msg> $outer2) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
    }

    public final void apply() {
        this.$outer.act();
    }

    public void apply$mcV$sp() {
        this.$outer.act();
    }
}
