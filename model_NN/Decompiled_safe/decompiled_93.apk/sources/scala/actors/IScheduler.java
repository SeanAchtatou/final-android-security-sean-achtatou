package scala.actors;

import scala.ScalaObject;
import scala.concurrent.ManagedBlocker;

/* compiled from: IScheduler.scala */
public interface IScheduler extends ScalaObject {
    void execute(Runnable runnable);

    void executeFromActor(Runnable runnable);

    boolean isActive();

    void managedBlock(ManagedBlocker managedBlocker);

    void newActor(Reactor<?> reactor);

    void terminated(Reactor<?> reactor);

    /* renamed from: scala.actors.IScheduler$class  reason: invalid class name */
    /* compiled from: IScheduler.scala */
    public abstract class Cclass {
        public static void $init$(IScheduler $this) {
        }

        public static void executeFromActor(IScheduler $this, Runnable task) {
            $this.execute(task);
        }
    }
}
