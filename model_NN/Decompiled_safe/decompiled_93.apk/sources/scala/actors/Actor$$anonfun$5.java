package scala.actors;

import java.io.Serializable;
import scala.PartialFunction;
import scala.Predef$;
import scala.collection.Seq;
import scala.collection.immutable.List$;
import scala.runtime.AbstractFunction2;
import scala.runtime.BoxesRunTime;

/* compiled from: Actor.scala */
public final class Actor$$anonfun$5 extends AbstractFunction2 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Actor $outer;
    public final /* synthetic */ PartialFunction handler$1;

    public Actor$$anonfun$5(Actor $outer2, PartialFunction partialFunction) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.handler$1 = partialFunction;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1, Object v2) {
        return BoxesRunTime.boxToBoolean(apply(v1, (OutputChannel<Object>) ((OutputChannel) v2)));
    }

    public final boolean apply(Object msg, OutputChannel<Object> replyTo) {
        this.$outer.senders_$eq(List$.MODULE$.apply((Seq) Predef$.MODULE$.wrapRefArray((Object[]) new OutputChannel[]{replyTo})));
        return this.handler$1.isDefinedAt(msg);
    }
}
