package scala.actors;

import java.io.Serializable;
import scala.Function1;
import scala.MatchError;
import scala.PartialFunction;

/* compiled from: Channel.scala */
public final class Channel$$anonfun$receive$1 implements Serializable, PartialFunction {
    public static final long serialVersionUID = 0;
    private final /* synthetic */ Channel C$1;
    private final /* synthetic */ PartialFunction f$3;

    public Channel$$anonfun$receive$1(Channel $outer, PartialFunction partialFunction, Channel channel) {
        this.f$3 = partialFunction;
        this.C$1 = channel;
        Function1.Cclass.$init$(this);
        PartialFunction.Cclass.$init$(this);
    }

    public <C> PartialFunction<Object, C> andThen(Function1<R, C> k) {
        return PartialFunction.Cclass.andThen(this, k);
    }

    public final R apply(Object obj) {
        if (obj instanceof C$bang) {
            C$bang _bang = (C$bang) obj;
            Object msg = _bang.msg();
            Channel channel = this.C$1;
            Channel ch = _bang.ch();
            if (channel != null ? !channel.equals(ch) : ch != null) {
                throw new MatchError(obj);
            } else if (gd2$1(msg)) {
                return this.f$3.apply(msg);
            } else {
                throw new MatchError(obj);
            }
        } else {
            throw new MatchError(obj);
        }
    }

    public int apply$mcII$sp(int v1) {
        return Function1.Cclass.apply$mcII$sp(this, v1);
    }

    public void apply$mcVI$sp(int v1) {
        Function1.Cclass.apply$mcVI$sp(this, v1);
    }

    public void apply$mcVL$sp(long v1) {
        Function1.Cclass.apply$mcVL$sp(this, v1);
    }

    public <A> Function1<A, R> compose(Function1<A, Object> g) {
        return Function1.Cclass.compose(this, g);
    }

    public final boolean isDefinedAt(Object obj) {
        if (!(obj instanceof C$bang)) {
            return false;
        }
        C$bang _bang = (C$bang) obj;
        Channel channel = this.C$1;
        Channel ch = _bang.ch();
        if (channel != null ? !channel.equals(ch) : ch != null) {
            return false;
        }
        return gd3$1(_bang.msg());
    }

    public String toString() {
        return Function1.Cclass.toString(this);
    }

    private final /* synthetic */ boolean gd2$1(Object obj) {
        return this.f$3.isDefinedAt(obj);
    }

    private final /* synthetic */ boolean gd3$1(Object obj) {
        return this.f$3.isDefinedAt(obj);
    }
}
