package scala.actors;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxedUnit;

/* compiled from: Actor.scala */
public final class Actor$$anonfun$exitLinked$1$$anonfun$apply$mcV$sp$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Actor$$anonfun$exitLinked$1 $outer;

    public Actor$$anonfun$exitLinked$1$$anonfun$apply$mcV$sp$1(Actor$$anonfun$exitLinked$1 $outer2) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        apply((AbstractActor) v1);
        return BoxedUnit.UNIT;
    }

    public final void apply(AbstractActor linked) {
        synchronized (linked) {
            if (!linked.exiting()) {
                linked.exit(this.$outer.$outer, this.$outer.$outer.scala$actors$Actor$$exitReason());
            }
        }
    }
}
