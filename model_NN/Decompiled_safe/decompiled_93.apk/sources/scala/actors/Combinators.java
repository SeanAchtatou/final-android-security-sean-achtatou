package scala.actors;

import scala.Function0;
import scala.ScalaObject;
import scala.actors.Actor;

/* compiled from: Combinators.scala */
public interface Combinators extends ScalaObject {
    void loop(Function0<Object> function0);

    <a> Actor.Body<a> mkBody(Function0<a> function0);

    /* renamed from: scala.actors.Combinators$class  reason: invalid class name */
    /* compiled from: Combinators.scala */
    public abstract class Cclass {
        public static void $init$(Combinators $this) {
        }

        public static void loop(Combinators $this, Function0 body$1) {
            $this.mkBody(body$1).andThen(new Combinators$$anonfun$loop$1($this, body$1));
        }
    }
}
