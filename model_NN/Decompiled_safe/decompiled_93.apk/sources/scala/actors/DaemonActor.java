package scala.actors;

import scala.ScalaObject;
import scala.actors.scheduler.DaemonScheduler$;

/* compiled from: DaemonActor.scala */
public interface DaemonActor extends ScalaObject, Actor {

    /* renamed from: scala.actors.DaemonActor$class  reason: invalid class name */
    /* compiled from: DaemonActor.scala */
    public abstract class Cclass {
        public static void $init$(DaemonActor $this) {
        }

        public static IScheduler scheduler(DaemonActor $this) {
            return DaemonScheduler$.MODULE$;
        }
    }
}
