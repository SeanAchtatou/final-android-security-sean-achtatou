package scala.actors;

import scala.Function0;
import scala.actors.Actor;

/* compiled from: Reactor.scala */
public final class Reactor$$anon$3 implements Actor.Body<Object> {
    private final /* synthetic */ Reactor $outer;
    private final /* synthetic */ Function0 body$1;

    /* JADX WARN: Type inference failed for: r3v0, types: [scala.actors.Reactor<Msg>, scala.Function0] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Reactor$$anon$3(scala.actors.Reactor r2, scala.actors.Reactor<Msg> r3) {
        /*
            r1 = this;
            if (r2 != 0) goto L_0x0008
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            r0.<init>()
            throw r0
        L_0x0008:
            r1.$outer = r2
            r1.body$1 = r3
            r1.<init>()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.actors.Reactor$$anon$3.<init>(scala.actors.Reactor, scala.Function0):void");
    }

    public <B> void andThen(Function0<B> other) {
        this.$outer.seq(this.body$1, other);
    }
}
