package scala.actors;

import scala.Function0;
import scala.actors.Actor;

/* compiled from: Actor.scala */
public final class Actor$$anon$6 implements Actor.Body<Object> {
    private final /* synthetic */ Function0 body$3;

    public Actor$$anon$6(Function0 function0) {
        this.body$3 = function0;
    }

    public <b> void andThen(Function0<b> other) {
        Actor$.MODULE$.rawSelf(Scheduler$.MODULE$).seq(this.body$3, other);
    }
}
