package scala.actors;

import scala.ScalaObject;
import scala.util.control.ControlThrowable;
import scala.util.control.NoStackTrace;

/* compiled from: Reaction.scala */
public class KillActorControl extends Throwable implements ScalaObject, ControlThrowable {
    public KillActorControl() {
        NoStackTrace.Cclass.$init$(this);
    }

    public Throwable fillInStackTrace() {
        return NoStackTrace.Cclass.fillInStackTrace(this);
    }
}
