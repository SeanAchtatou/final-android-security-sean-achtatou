package scala.actors;

import scala.Function1;
import scala.PartialFunction;

/* compiled from: Reactor.scala */
public final class Reactor$$anon$1 implements PartialFunction<Object, Object> {
    public Reactor$$anon$1() {
        Function1.Cclass.$init$(this);
        PartialFunction.Cclass.$init$(this);
    }

    public <C> PartialFunction<Object, C> andThen(Function1<Object, C> k) {
        return PartialFunction.Cclass.andThen(this, k);
    }

    public int apply$mcII$sp(int v1) {
        return Function1.Cclass.apply$mcII$sp(this, v1);
    }

    public void apply$mcVI$sp(int v1) {
        Function1.Cclass.apply$mcVI$sp(this, v1);
    }

    public void apply$mcVL$sp(long v1) {
        Function1.Cclass.apply$mcVL$sp(this, v1);
    }

    public <A> Function1<A, Object> compose(Function1<A, Object> g) {
        return Function1.Cclass.compose(this, g);
    }

    public String toString() {
        return Function1.Cclass.toString(this);
    }

    public boolean isDefinedAt(Object x) {
        return false;
    }

    public void apply(Object x) {
    }
}
