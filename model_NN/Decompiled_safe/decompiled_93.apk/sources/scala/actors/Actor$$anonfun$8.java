package scala.actors;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;

/* compiled from: Actor.scala */
public final class Actor$$anonfun$8 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Actor $outer;

    public Actor$$anonfun$8(Actor $outer2) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
    }

    public final boolean apply(Object obj) {
        Actor actor = this.$outer;
        return actor != null ? actor.equals(obj) : obj == null;
    }
}
