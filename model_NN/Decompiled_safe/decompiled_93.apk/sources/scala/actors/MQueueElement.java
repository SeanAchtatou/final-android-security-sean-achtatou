package scala.actors;

import scala.ScalaObject;

/* compiled from: MessageQueue.scala */
public class MQueueElement<Msg> implements ScalaObject {
    private final Msg msg;
    private MQueueElement<Msg> next;
    private final OutputChannel<Object> session;

    public MQueueElement(Msg msg2, OutputChannel<Object> session2, MQueueElement<Msg> next2) {
        this.msg = msg2;
        this.session = session2;
        this.next = next2;
    }

    public Msg msg() {
        return this.msg;
    }

    public MQueueElement<Msg> next() {
        return this.next;
    }

    public void next_$eq(MQueueElement<Msg> mQueueElement) {
        this.next = mQueueElement;
    }

    public OutputChannel<Object> session() {
        return this.session;
    }

    public MQueueElement() {
        this(null, null, null);
    }

    public MQueueElement(Msg msg2, OutputChannel<Object> session2) {
        this(msg2, session2, null);
    }
}
