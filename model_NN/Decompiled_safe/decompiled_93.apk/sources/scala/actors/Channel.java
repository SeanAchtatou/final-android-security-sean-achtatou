package scala.actors;

import scala.PartialFunction;
import scala.ScalaObject;

/* compiled from: Channel.scala */
public class Channel<Msg> implements OutputChannel<Msg>, ScalaObject {
    private final Actor receiver;

    public Channel(Actor receiver2) {
        this.receiver = receiver2;
    }

    public Actor receiver() {
        return this.receiver;
    }

    public Channel() {
        this(Actor$.MODULE$.self());
    }

    public void $bang(Msg msg) {
        receiver().$bang(new C$bang(this, msg));
    }

    public <R> R receive(PartialFunction<Msg, R> f$3) {
        return receiver().receive(new Channel$$anonfun$receive$1(this, f$3, this));
    }

    public Msg $qmark() {
        return receive(new Channel$$anonfun$$qmark$1(this));
    }
}
