package scala.actors;

import java.io.Serializable;
import scala.Option;
import scala.Product;
import scala.ScalaObject;
import scala.collection.Iterator;
import scala.collection.mutable.StringBuilder;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime$;

/* compiled from: UncaughtException.scala */
public class UncaughtException extends Exception implements Serializable, Product, ScalaObject {
    private final Actor actor;
    private final Throwable cause;
    private final Option<Object> message;
    private final Option<OutputChannel<Object>> sender;
    private final Thread thread;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UncaughtException(Actor actor2, Option<Object> message2, Option<OutputChannel<Object>> sender2, Thread thread2, Throwable cause2) {
        super(cause2);
        this.actor = actor2;
        this.message = message2;
        this.sender = sender2;
        this.thread = thread2;
        this.cause = cause2;
        Product.Cclass.$init$(this);
    }

    private final /* synthetic */ boolean gd1$1(Actor actor2, Option option, Option option2, Thread thread2, Throwable th) {
        Actor actor3 = actor();
        if (actor2 != null ? actor2.equals(actor3) : actor3 == null) {
            Option<Object> message2 = message();
            if (option != null ? option.equals(message2) : message2 == null) {
                Option<OutputChannel<Object>> sender2 = sender();
                if (option2 != null ? option2.equals(sender2) : sender2 == null) {
                    Thread thread3 = thread();
                    if (thread2 != null ? thread2.equals(thread3) : thread3 == null) {
                        Throwable cause2 = cause();
                        return th != null ? th.equals(cause2) : cause2 == null;
                    }
                }
            }
        }
    }

    public Actor actor() {
        return this.actor;
    }

    public boolean canEqual(Object obj) {
        return obj instanceof UncaughtException;
    }

    public boolean equals(Object obj) {
        boolean z;
        if (this != obj) {
            if (obj instanceof UncaughtException) {
                UncaughtException uncaughtException = (UncaughtException) obj;
                z = gd1$1(uncaughtException.actor(), uncaughtException.message(), uncaughtException.sender(), uncaughtException.thread(), uncaughtException.cause()) ? ((UncaughtException) obj).canEqual(this) : false;
            } else {
                z = false;
            }
            if (!z) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return ScalaRunTime$.MODULE$._hashCode(this);
    }

    public int productArity() {
        return 5;
    }

    public Object productElement(int i) {
        switch (i) {
            case 0:
                return actor();
            case 1:
                return message();
            case 2:
                return sender();
            case 3:
                return thread();
            case 4:
                return cause();
            default:
                throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(i).toString());
        }
    }

    public Iterator<Object> productIterator() {
        return Product.Cclass.productIterator(this);
    }

    public String productPrefix() {
        return "UncaughtException";
    }

    public Option<Object> message() {
        return this.message;
    }

    public Option<OutputChannel<Object>> sender() {
        return this.sender;
    }

    public Thread thread() {
        return this.thread;
    }

    public Throwable cause() {
        return this.cause;
    }

    public String toString() {
        return new StringBuilder().append((Object) "UncaughtException(").append(actor()).append((Object) ",").append(message()).append((Object) ",").append(sender()).append((Object) ",").append(cause()).append((Object) ")").toString();
    }
}
