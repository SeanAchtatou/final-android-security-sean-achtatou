package scala.actors;

import scala.Function0;
import scala.None$;
import scala.Option;
import scala.Responder;
import scala.ScalaObject;

/* compiled from: Future.scala */
public abstract class Future<T> extends Responder<T> implements Function0<T>, ScalaObject {
    private volatile Option<Object> fvalue = None$.MODULE$;

    public Future() {
        Function0.Cclass.$init$(this);
    }

    public byte apply$mcB$sp() {
        return Function0.Cclass.apply$mcB$sp(this);
    }

    public int apply$mcI$sp() {
        return Function0.Cclass.apply$mcI$sp(this);
    }

    public void apply$mcV$sp() {
        Function0.Cclass.apply$mcV$sp(this);
    }

    public String toString() {
        return Function0.Cclass.toString(this);
    }

    public Option<Object> fvalue() {
        return this.fvalue;
    }

    public void fvalue_$eq(Option<Object> option) {
        this.fvalue = option;
    }

    public T fvalueTyped() {
        return fvalue().get();
    }
}
