package scala.actors;

import java.io.Serializable;
import scala.Function1;
import scala.PartialFunction;

/* compiled from: Channel.scala */
public final class Channel$$anonfun$$qmark$1 implements Serializable, PartialFunction {
    public static final long serialVersionUID = 0;

    public Channel$$anonfun$$qmark$1(Channel<Msg> $outer) {
        Function1.Cclass.$init$(this);
        PartialFunction.Cclass.$init$(this);
    }

    public <C> PartialFunction<Msg, C> andThen(Function1<Msg, C> k) {
        return PartialFunction.Cclass.andThen(this, k);
    }

    public int apply$mcII$sp(int v1) {
        return Function1.Cclass.apply$mcII$sp(this, v1);
    }

    public void apply$mcVI$sp(int v1) {
        Function1.Cclass.apply$mcVI$sp(this, v1);
    }

    public void apply$mcVL$sp(long v1) {
        Function1.Cclass.apply$mcVL$sp(this, v1);
    }

    public <A> Function1<A, Msg> compose(Function1<A, Msg> g) {
        return Function1.Cclass.compose(this, g);
    }

    public final boolean isDefinedAt(Msg msg) {
        return true;
    }

    public String toString() {
        return Function1.Cclass.toString(this);
    }

    public final Msg apply(Msg msg) {
        return msg;
    }
}
