package scala.actors;

import java.io.Serializable;
import scala.actors.Actor;
import scala.runtime.AbstractFunction0$mcV$sp;

/* compiled from: Actor.scala */
public final class Actor$$anonfun$startSearch$1 extends AbstractFunction0$mcV$sp implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Actor $outer;
    public final /* synthetic */ Object msg$1;
    public final /* synthetic */ OutputChannel replyTo$1;

    public Actor$$anonfun$startSearch$1(Actor $outer2, Object obj, OutputChannel outputChannel) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.msg$1 = obj;
        this.replyTo$1 = outputChannel;
    }

    public final void apply() {
        synchronized (this.$outer) {
            this.$outer.mailbox().append(this.msg$1, this.replyTo$1);
            Actor.Cclass.scala$actors$Actor$$resumeActor(this.$outer);
        }
    }

    public void apply$mcV$sp() {
        synchronized (this.$outer) {
            this.$outer.mailbox().append(this.msg$1, this.replyTo$1);
            Actor.Cclass.scala$actors$Actor$$resumeActor(this.$outer);
        }
    }
}
