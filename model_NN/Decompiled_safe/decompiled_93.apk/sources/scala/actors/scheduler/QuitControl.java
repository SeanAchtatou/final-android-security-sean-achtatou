package scala.actors.scheduler;

import scala.ScalaObject;
import scala.util.control.ControlThrowable;
import scala.util.control.NoStackTrace;

/* compiled from: QuitControl.scala */
public class QuitControl extends Throwable implements ScalaObject, ControlThrowable {
    public QuitControl() {
        NoStackTrace.Cclass.$init$(this);
    }

    public Throwable fillInStackTrace() {
        return NoStackTrace.Cclass.fillInStackTrace(this);
    }
}
