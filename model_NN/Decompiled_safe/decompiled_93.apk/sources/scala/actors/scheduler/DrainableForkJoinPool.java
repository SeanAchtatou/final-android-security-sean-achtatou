package scala.actors.scheduler;

import java.util.Collection;
import scala.ScalaObject;
import scala.concurrent.forkjoin.ForkJoinPool;
import scala.concurrent.forkjoin.ForkJoinTask;

/* compiled from: DrainableForkJoinPool.scala */
public class DrainableForkJoinPool extends ForkJoinPool implements ScalaObject {
    public int drainTasksTo(Collection<ForkJoinTask<?>> c) {
        return super.drainTasksTo(c);
    }
}
