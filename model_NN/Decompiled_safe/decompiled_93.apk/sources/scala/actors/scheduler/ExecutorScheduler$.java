package scala.actors.scheduler;

import java.util.concurrent.ExecutorService;
import scala.ScalaObject;

/* compiled from: ExecutorScheduler.scala */
public final class ExecutorScheduler$ implements ScalaObject {
    public static final ExecutorScheduler$ MODULE$ = null;

    static {
        new ExecutorScheduler$();
    }

    private ExecutorScheduler$() {
        MODULE$ = this;
    }

    private ExecutorScheduler start(ExecutorScheduler sched) {
        ((Thread) sched).start();
        return sched;
    }

    public ExecutorScheduler apply(ExecutorService exec$2) {
        return start(new ExecutorScheduler$$anon$1(exec$2));
    }
}
