package scala.actors.scheduler;

import scala.concurrent.ManagedBlocker;
import scala.concurrent.forkjoin.ForkJoinPool;

/* compiled from: ForkJoinScheduler.scala */
public final class ForkJoinScheduler$$anon$2 implements ForkJoinPool.ManagedBlocker {
    private final /* synthetic */ ManagedBlocker blocker$1;

    public ForkJoinScheduler$$anon$2(ForkJoinScheduler $outer, ManagedBlocker managedBlocker) {
        this.blocker$1 = managedBlocker;
    }

    public boolean block() {
        return this.blocker$1.block();
    }

    public boolean isReleasable() {
        return this.blocker$1.isReleasable();
    }
}
