package scala.actors.scheduler;

import java.util.concurrent.Callable;
import scala.ScalaObject;
import scala.actors.IScheduler;
import scala.concurrent.ThreadPoolRunner;

/* compiled from: ExecutorScheduler.scala */
public interface ExecutorScheduler extends ScalaObject, IScheduler, TerminationService, ThreadPoolRunner {

    /* renamed from: scala.actors.scheduler.ExecutorScheduler$class  reason: invalid class name */
    /* compiled from: ExecutorScheduler.scala */
    public abstract class Cclass {
        public static void $init$(ExecutorScheduler $this) {
        }

        public static void execute(ExecutorScheduler $this, Runnable task) {
            ThreadPoolRunner.Cclass.execute($this, (Callable) task);
        }

        public static void onShutdown(ExecutorScheduler $this) {
            $this.executor().shutdown();
        }

        public static boolean isActive(ExecutorScheduler $this) {
            return $this.executor() != null && !$this.executor().isShutdown();
        }
    }
}
