package scala.actors.scheduler;

import java.lang.Thread;
import java.util.Iterator;
import java.util.List;
import scala.Predef$;
import scala.ScalaObject;
import scala.actors.Debug$;
import scala.actors.IScheduler;
import scala.actors.Reactor;
import scala.actors.scheduler.TerminationMonitor;
import scala.actors.threadpool.LinkedBlockingQueue;
import scala.actors.threadpool.ThreadFactory;
import scala.actors.threadpool.ThreadPoolExecutor;
import scala.actors.threadpool.TimeUnit;
import scala.collection.mutable.HashMap;
import scala.collection.mutable.StringBuilder;
import scala.concurrent.ManagedBlocker;
import scala.runtime.BoxesRunTime;
import scala.runtime.StringAdd;

/* compiled from: ResizableThreadPoolScheduler.scala */
public class ResizableThreadPoolScheduler extends Thread implements ScalaObject, IScheduler, TerminationMonitor {
    private final int CHECK_FREQ;
    private int activeActors;
    private int coreSize;
    private final boolean daemon;
    private volatile List<?> drainedTasks;
    private ThreadPoolExecutor executor;
    private final int maxSize;
    private final int numCores;
    private boolean scala$actors$scheduler$TerminationMonitor$$started;
    private boolean suspending;
    private final boolean terminate;
    private boolean terminating;
    private final HashMap terminationHandlers;
    private final DaemonThreadFactory threadFac;

    public ResizableThreadPoolScheduler(boolean z, boolean z2) {
        this.terminate = z;
        this.daemon = z2;
        IScheduler.Cclass.$init$(this);
        TerminationMonitor.Cclass.$init$(this);
        setDaemon(z2);
        this.terminating = false;
        this.suspending = false;
        this.drainedTasks = null;
        this.coreSize = ThreadPoolConfig$.MODULE$.corePoolSize();
        this.maxSize = ThreadPoolConfig$.MODULE$.maxPoolSize();
        this.numCores = Runtime.getRuntime().availableProcessors();
        this.CHECK_FREQ = 10;
        this.threadFac = new DaemonThreadFactory(this);
        this.executor = makeNewPool();
        Debug$.MODULE$.info(new StringBuilder().append((Object) Predef$.MODULE$.any2stringadd(this).$plus(": corePoolSize = ")).append(BoxesRunTime.boxToInteger(coreSize())).append((Object) ", maxPoolSize = ").append(BoxesRunTime.boxToInteger(maxSize())).toString());
    }

    public int activeActors() {
        return this.activeActors;
    }

    public void activeActors_$eq(int i) {
        this.activeActors = i;
    }

    public boolean allActorsTerminated() {
        return TerminationMonitor.Cclass.allActorsTerminated(this);
    }

    public void executeFromActor(Runnable task) {
        IScheduler.Cclass.executeFromActor(this, task);
    }

    public void gc() {
        TerminationMonitor.Cclass.gc(this);
    }

    public void newActor(Reactor<?> a) {
        TerminationMonitor.Cclass.newActor(this, a);
    }

    public final boolean scala$actors$scheduler$TerminationMonitor$$started() {
        return this.scala$actors$scheduler$TerminationMonitor$$started;
    }

    public final void scala$actors$scheduler$TerminationMonitor$$started_$eq(boolean z) {
        this.scala$actors$scheduler$TerminationMonitor$$started = z;
    }

    public void scala$actors$scheduler$TerminationMonitor$_setter_$terminationHandlers_$eq(HashMap hashMap) {
        this.terminationHandlers = hashMap;
    }

    public boolean terminate() {
        return this.terminate;
    }

    public void terminated(Reactor<?> a) {
        TerminationMonitor.Cclass.terminated(this, a);
    }

    public HashMap terminationHandlers() {
        return this.terminationHandlers;
    }

    public boolean daemon() {
        return this.daemon;
    }

    private boolean terminating() {
        return this.terminating;
    }

    private void terminating_$eq(boolean z) {
        this.terminating = z;
    }

    private boolean suspending() {
        return this.suspending;
    }

    private List<?> drainedTasks() {
        return this.drainedTasks;
    }

    private void drainedTasks_$eq(List<?> list) {
        this.drainedTasks = list;
    }

    private int coreSize() {
        return this.coreSize;
    }

    private void coreSize_$eq(int i) {
        this.coreSize = i;
    }

    private int maxSize() {
        return this.maxSize;
    }

    private int numCores() {
        return this.numCores;
    }

    public int CHECK_FREQ() {
        return this.CHECK_FREQ;
    }

    /* compiled from: ResizableThreadPoolScheduler.scala */
    public class DaemonThreadFactory implements ScalaObject, ThreadFactory {
        public final /* synthetic */ ResizableThreadPoolScheduler $outer;

        public DaemonThreadFactory(ResizableThreadPoolScheduler $outer2) {
            if ($outer2 == null) {
                throw new NullPointerException();
            }
            this.$outer = $outer2;
        }

        public /* synthetic */ ResizableThreadPoolScheduler scala$actors$scheduler$ResizableThreadPoolScheduler$DaemonThreadFactory$$$outer() {
            return this.$outer;
        }

        public Thread newThread(Runnable r) {
            Thread t = new Thread(r);
            t.setDaemon(scala$actors$scheduler$ResizableThreadPoolScheduler$DaemonThreadFactory$$$outer().daemon());
            return t;
        }
    }

    private DaemonThreadFactory threadFac() {
        return this.threadFac;
    }

    private ThreadPoolExecutor makeNewPool() {
        return new ThreadPoolExecutor(coreSize(), maxSize(), 60000, TimeUnit.MILLISECONDS, new LinkedBlockingQueue(), threadFac(), new ThreadPoolExecutor.CallerRunsPolicy());
    }

    private ThreadPoolExecutor executor() {
        return this.executor;
    }

    public ResizableThreadPoolScheduler(boolean d) {
        this(true, d);
    }

    public ResizableThreadPoolScheduler() {
        this(false);
    }

    private int numWorkersBlocked() {
        executor().mainLock.lock();
        Iterator iter = executor().workers.iterator();
        int numBlocked = 0;
        while (iter.hasNext()) {
            ThreadPoolExecutor.Worker w = (ThreadPoolExecutor.Worker) iter.next();
            if (w.tryLock()) {
                w.unlock();
            } else {
                Thread.State s = w.thread.getState();
                if (s != null ? !s.equals(Thread.State.WAITING) : Thread.State.WAITING != null) {
                    if (s == null) {
                        if (Thread.State.TIMED_WAITING != null) {
                        }
                    } else if (!s.equals(Thread.State.TIMED_WAITING)) {
                    }
                }
                numBlocked++;
            }
        }
        executor().mainLock.unlock();
        return numBlocked;
    }

    public void run() {
        while (true) {
            try {
                synchronized (this) {
                    liftedTree1$1();
                    if (terminating()) {
                        throw new QuitControl();
                    } else if (suspending()) {
                        drainedTasks_$eq(executor().shutdownNow());
                        Debug$.MODULE$.info(new StringBuilder().append((Object) new StringAdd(this).$plus(": drained ")).append(BoxesRunTime.boxToInteger(drainedTasks().size())).append((Object) " tasks").toString());
                        terminating_$eq(true);
                        throw new QuitControl();
                    } else {
                        gc();
                        int numWorkersBlocked = numWorkersBlocked();
                        if (coreSize() - numWorkersBlocked < numCores() && coreSize() < maxSize()) {
                            coreSize_$eq(numWorkersBlocked + numCores());
                            executor().setCorePoolSize(coreSize());
                        } else if (terminate() && allActorsTerminated() && executor().getActiveCount() == 0) {
                            Debug$.MODULE$.info(new StringAdd(this).$plus(": initiating shutdown..."));
                            Debug$.MODULE$.info(new StringBuilder().append((Object) new StringAdd(this).$plus(": corePoolSize = ")).append(BoxesRunTime.boxToInteger(coreSize())).append((Object) ", maxPoolSize = ").append(BoxesRunTime.boxToInteger(maxSize())).toString());
                            terminating_$eq(true);
                            throw new QuitControl();
                        }
                    }
                }
            } catch (QuitControl e) {
                executor().shutdown();
                return;
            }
        }
    }

    private final void liftedTree1$1() {
        try {
            wait((long) CHECK_FREQ());
        } catch (InterruptedException e) {
        }
    }

    public void execute(Runnable task) {
        executor().execute(task);
    }

    public boolean isActive() {
        boolean z;
        synchronized (this) {
            z = !terminating() && executor() != null && !executor().isShutdown();
        }
        return z;
    }

    public void managedBlock(ManagedBlocker blocker) {
        blocker.block();
    }
}
