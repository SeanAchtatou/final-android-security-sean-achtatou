package scala.actors.scheduler;

import scala.ScalaObject;
import scala.actors.Debug$;
import scala.runtime.StringAdd;

/* compiled from: TerminationService.scala */
public interface TerminationService extends ScalaObject, TerminationMonitor {
    int CHECK_FREQ();

    void onShutdown();

    boolean scala$actors$scheduler$TerminationService$$terminating();

    void scala$actors$scheduler$TerminationService$$terminating_$eq(boolean z);

    void scala$actors$scheduler$TerminationService$_setter_$CHECK_FREQ_$eq(int i);

    void scala$actors$scheduler$TerminationService$_setter_$terminate_$eq(boolean z);

    boolean terminate();

    /* renamed from: scala.actors.scheduler.TerminationService$class  reason: invalid class name */
    /* compiled from: TerminationService.scala */
    public abstract class Cclass {
        public static void $init$(Thread $this) {
            ((TerminationService) $this).scala$actors$scheduler$TerminationService$$terminating_$eq(false);
            ((TerminationService) $this).scala$actors$scheduler$TerminationService$_setter_$terminate_$eq(true);
            ((TerminationService) $this).scala$actors$scheduler$TerminationService$_setter_$CHECK_FREQ_$eq(50);
        }

        public static void run(Thread $this) {
            while (true) {
                try {
                    synchronized ($this) {
                        liftedTree1$1($this);
                        if (!((TerminationService) $this).scala$actors$scheduler$TerminationService$$terminating() && (!((TerminationService) $this).terminate() || !((TerminationMonitor) $this).allActorsTerminated())) {
                            ((TerminationMonitor) $this).gc();
                        }
                    }
                } catch (QuitControl e) {
                    Debug$.MODULE$.info(new StringAdd($this).$plus(": initiating shutdown..."));
                    ((TerminationService) $this).onShutdown();
                    return;
                }
            }
            throw new QuitControl();
        }

        private static final void liftedTree1$1(Thread $this) {
            try {
                $this.wait((long) ((TerminationService) $this).CHECK_FREQ());
            } catch (InterruptedException e) {
            }
        }
    }
}
