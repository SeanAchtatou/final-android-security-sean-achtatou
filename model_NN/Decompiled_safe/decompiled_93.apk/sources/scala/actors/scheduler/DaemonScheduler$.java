package scala.actors.scheduler;

import scala.ScalaObject;
import scala.actors.IScheduler;
import scala.actors.Reactor;
import scala.actors.scheduler.DelegatingScheduler;
import scala.concurrent.ManagedBlocker;

/* compiled from: DaemonScheduler.scala */
public final class DaemonScheduler$ implements ScalaObject, DelegatingScheduler {
    public static final DaemonScheduler$ MODULE$ = null;
    private IScheduler sched;

    static {
        new DaemonScheduler$();
    }

    private DaemonScheduler$() {
        MODULE$ = this;
        IScheduler.Cclass.$init$(this);
        DelegatingScheduler.Cclass.$init$(this);
    }

    public void execute(Runnable task) {
        DelegatingScheduler.Cclass.execute(this, task);
    }

    public void executeFromActor(Runnable task) {
        DelegatingScheduler.Cclass.executeFromActor(this, task);
    }

    public final IScheduler impl() {
        return DelegatingScheduler.Cclass.impl(this);
    }

    public boolean isActive() {
        return DelegatingScheduler.Cclass.isActive(this);
    }

    public void managedBlock(ManagedBlocker blocker) {
        DelegatingScheduler.Cclass.managedBlock(this, blocker);
    }

    public void newActor(Reactor<?> actor) {
        DelegatingScheduler.Cclass.newActor(this, actor);
    }

    public IScheduler sched() {
        return this.sched;
    }

    public void sched_$eq(IScheduler iScheduler) {
        this.sched = iScheduler;
    }

    public void terminated(Reactor<?> actor) {
        DelegatingScheduler.Cclass.terminated(this, actor);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: scala.actors.scheduler.ResizableThreadPoolScheduler} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: scala.actors.scheduler.ForkJoinScheduler} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v3, resolved type: scala.actors.scheduler.ResizableThreadPoolScheduler} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v4, resolved type: scala.actors.scheduler.ResizableThreadPoolScheduler} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public scala.actors.IScheduler makeNewScheduler() {
        /*
            r5 = this;
            r2 = 1
            scala.actors.scheduler.ThreadPoolConfig$ r1 = scala.actors.scheduler.ThreadPoolConfig$.MODULE$
            boolean r1 = r1.useForkJoin()
            if (r1 == 0) goto L_0x0047
            scala.actors.scheduler.ForkJoinScheduler r0 = new scala.actors.scheduler.ForkJoinScheduler
            r0.<init>(r2)
            r0.start()
        L_0x0011:
            scala.actors.Debug$ r1 = scala.actors.Debug$.MODULE$
            scala.collection.mutable.StringBuilder r2 = new scala.collection.mutable.StringBuilder
            r2.<init>()
            scala.runtime.StringAdd r3 = new scala.runtime.StringAdd
            r3.<init>(r5)
            java.lang.String r4 = ": starting new "
            java.lang.String r3 = r3.$plus(r4)
            scala.collection.mutable.StringBuilder r2 = r2.append(r3)
            scala.collection.mutable.StringBuilder r2 = r2.append(r0)
            java.lang.String r3 = " ["
            scala.collection.mutable.StringBuilder r2 = r2.append(r3)
            java.lang.Class r3 = r0.getClass()
            scala.collection.mutable.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "]"
            scala.collection.mutable.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.info(r2)
            return r0
        L_0x0047:
            scala.actors.scheduler.ResizableThreadPoolScheduler r0 = new scala.actors.scheduler.ResizableThreadPoolScheduler
            r0.<init>(r2)
            r0.start()
            goto L_0x0011
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.actors.scheduler.DaemonScheduler$.makeNewScheduler():scala.actors.IScheduler");
    }
}
