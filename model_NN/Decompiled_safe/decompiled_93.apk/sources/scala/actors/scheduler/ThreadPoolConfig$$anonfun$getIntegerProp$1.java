package scala.actors.scheduler;

import java.io.Serializable;
import scala.collection.immutable.StringOps;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

/* compiled from: ThreadPoolConfig.scala */
public final class ThreadPoolConfig$$anonfun$getIntegerProp$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;

    public final int apply(String str) {
        return new StringOps(str).toInt();
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return BoxesRunTime.boxToInteger(apply((String) v1));
    }
}
