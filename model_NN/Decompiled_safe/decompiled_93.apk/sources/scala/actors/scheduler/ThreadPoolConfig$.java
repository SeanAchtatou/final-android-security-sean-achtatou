package scala.actors.scheduler;

import scala.None$;
import scala.Option;
import scala.ScalaObject;
import scala.util.Properties$;

/* compiled from: ThreadPoolConfig.scala */
public final class ThreadPoolConfig$ implements ScalaObject {
    public static final ThreadPoolConfig$ MODULE$ = null;
    private final int corePoolSize;
    private final int maxPoolSize;
    private final int minNumThreads = 4;
    private final Runtime rt = Runtime.getRuntime();

    static {
        new ThreadPoolConfig$();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0026, code lost:
        if (gd1$1(r2) != false) goto L_0x0028;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private ThreadPoolConfig$() {
        /*
            r4 = this;
            r4.<init>()
            scala.actors.scheduler.ThreadPoolConfig$.MODULE$ = r4
            java.lang.Runtime r2 = java.lang.Runtime.getRuntime()
            r4.rt = r2
            r2 = 4
            r4.minNumThreads = r2
            java.lang.String r2 = "actors.corePoolSize"
            scala.Option r2 = r4.getIntegerProp(r2)
            boolean r3 = r2 instanceof scala.Some
            if (r3 == 0) goto L_0x0047
            scala.Some r2 = (scala.Some) r2
            java.lang.Object r2 = r2.x()
            int r2 = scala.runtime.BoxesRunTime.unboxToInt(r2)
            boolean r3 = r4.gd1$1(r2)
            if (r3 == 0) goto L_0x0047
        L_0x0028:
            r4.corePoolSize = r2
            java.lang.String r2 = "actors.maxPoolSize"
            scala.Option r2 = r4.getIntegerProp(r2)
            scala.actors.scheduler.ThreadPoolConfig$$anonfun$1 r3 = new scala.actors.scheduler.ThreadPoolConfig$$anonfun$1
            r3.<init>()
            java.lang.Object r2 = r2.getOrElse(r3)
            int r1 = scala.runtime.BoxesRunTime.unboxToInt(r2)
            int r2 = r4.corePoolSize()
            if (r1 < r2) goto L_0x005e
            r2 = r1
        L_0x0044:
            r4.maxPoolSize = r2
            return
        L_0x0047:
            java.lang.Runtime r2 = r4.rt()
            int r2 = r2.availableProcessors()
            int r0 = r2 * 2
            int r2 = r4.minNumThreads()
            if (r0 <= r2) goto L_0x0059
            r2 = r0
            goto L_0x0028
        L_0x0059:
            int r2 = r4.minNumThreads()
            goto L_0x0028
        L_0x005e:
            int r2 = r4.corePoolSize()
            goto L_0x0044
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.actors.scheduler.ThreadPoolConfig$.<init>():void");
    }

    private Runtime rt() {
        return this.rt;
    }

    private int minNumThreads() {
        return this.minNumThreads;
    }

    private Option<Integer> getIntegerProp(String propName) {
        try {
            return Properties$.MODULE$.propOrNone(propName).map(new ThreadPoolConfig$$anonfun$getIntegerProp$1());
        } catch (Throwable th) {
            if ((th instanceof SecurityException) || (th instanceof NumberFormatException)) {
                return None$.MODULE$;
            }
            throw th;
        }
    }

    public int corePoolSize() {
        return this.corePoolSize;
    }

    private final /* synthetic */ boolean gd1$1(int i) {
        return i > 0;
    }

    public int maxPoolSize() {
        return this.maxPoolSize;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x008f, code lost:
        if (r1 != false) goto L_0x0091;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean useForkJoin() {
        /*
            r7 = this;
            r6 = 1
            r5 = 0
            scala.util.Properties$ r1 = scala.util.Properties$.MODULE$     // Catch:{ SecurityException -> 0x0097 }
            java.lang.String r2 = "actors.enableForkJoin"
            java.lang.String r3 = "false"
            boolean r1 = r1.propIsSetTo(r2, r3)     // Catch:{ SecurityException -> 0x0097 }
            if (r1 != 0) goto L_0x0095
            scala.util.Properties$ r1 = scala.util.Properties$.MODULE$     // Catch:{ SecurityException -> 0x0097 }
            java.lang.String r2 = "actors.enableForkJoin"
            java.lang.String r3 = "true"
            boolean r1 = r1.propIsSetTo(r2, r3)     // Catch:{ SecurityException -> 0x0097 }
            if (r1 != 0) goto L_0x0091
            scala.actors.Debug$ r1 = scala.actors.Debug$.MODULE$     // Catch:{ SecurityException -> 0x0097 }
            scala.collection.mutable.StringBuilder r2 = new scala.collection.mutable.StringBuilder     // Catch:{ SecurityException -> 0x0097 }
            r2.<init>()     // Catch:{ SecurityException -> 0x0097 }
            scala.runtime.StringAdd r3 = new scala.runtime.StringAdd     // Catch:{ SecurityException -> 0x0097 }
            r3.<init>(r7)     // Catch:{ SecurityException -> 0x0097 }
            java.lang.String r4 = ": java.version = "
            java.lang.String r3 = r3.$plus(r4)     // Catch:{ SecurityException -> 0x0097 }
            scala.collection.mutable.StringBuilder r2 = r2.append(r3)     // Catch:{ SecurityException -> 0x0097 }
            scala.util.Properties$ r3 = scala.util.Properties$.MODULE$     // Catch:{ SecurityException -> 0x0097 }
            java.lang.String r3 = r3.javaVersion()     // Catch:{ SecurityException -> 0x0097 }
            scala.collection.mutable.StringBuilder r2 = r2.append(r3)     // Catch:{ SecurityException -> 0x0097 }
            java.lang.String r2 = r2.toString()     // Catch:{ SecurityException -> 0x0097 }
            r1.info(r2)     // Catch:{ SecurityException -> 0x0097 }
            scala.actors.Debug$ r1 = scala.actors.Debug$.MODULE$     // Catch:{ SecurityException -> 0x0097 }
            scala.collection.mutable.StringBuilder r2 = new scala.collection.mutable.StringBuilder     // Catch:{ SecurityException -> 0x0097 }
            r2.<init>()     // Catch:{ SecurityException -> 0x0097 }
            scala.runtime.StringAdd r3 = new scala.runtime.StringAdd     // Catch:{ SecurityException -> 0x0097 }
            r3.<init>(r7)     // Catch:{ SecurityException -> 0x0097 }
            java.lang.String r4 = ": java.vm.vendor = "
            java.lang.String r3 = r3.$plus(r4)     // Catch:{ SecurityException -> 0x0097 }
            scala.collection.mutable.StringBuilder r2 = r2.append(r3)     // Catch:{ SecurityException -> 0x0097 }
            scala.util.Properties$ r3 = scala.util.Properties$.MODULE$     // Catch:{ SecurityException -> 0x0097 }
            java.lang.String r3 = r3.javaVmVendor()     // Catch:{ SecurityException -> 0x0097 }
            scala.collection.mutable.StringBuilder r2 = r2.append(r3)     // Catch:{ SecurityException -> 0x0097 }
            java.lang.String r2 = r2.toString()     // Catch:{ SecurityException -> 0x0097 }
            r1.info(r2)     // Catch:{ SecurityException -> 0x0097 }
            scala.util.Properties$ r1 = scala.util.Properties$.MODULE$     // Catch:{ SecurityException -> 0x0097 }
            java.lang.String r2 = "1.6"
            boolean r1 = r1.isJavaAtLeast(r2)     // Catch:{ SecurityException -> 0x0097 }
            if (r1 == 0) goto L_0x0093
            scala.util.Properties$ r1 = scala.util.Properties$.MODULE$     // Catch:{ SecurityException -> 0x0097 }
            java.lang.String r1 = r1.javaVmVendor()     // Catch:{ SecurityException -> 0x0097 }
            java.lang.String r2 = "Sun"
            boolean r1 = r1.contains(r2)     // Catch:{ SecurityException -> 0x0097 }
            if (r1 != 0) goto L_0x008e
            scala.util.Properties$ r1 = scala.util.Properties$.MODULE$     // Catch:{ SecurityException -> 0x0097 }
            java.lang.String r1 = r1.javaVmVendor()     // Catch:{ SecurityException -> 0x0097 }
            java.lang.String r2 = "Apple"
            boolean r1 = r1.contains(r2)     // Catch:{ SecurityException -> 0x0097 }
            if (r1 == 0) goto L_0x0093
        L_0x008e:
            r1 = r6
        L_0x008f:
            if (r1 == 0) goto L_0x0095
        L_0x0091:
            r0 = r6
        L_0x0092:
            return r0
        L_0x0093:
            r1 = r5
            goto L_0x008f
        L_0x0095:
            r0 = r5
            goto L_0x0092
        L_0x0097:
            r1 = move-exception
            r0 = 0
            goto L_0x0092
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.actors.scheduler.ThreadPoolConfig$.useForkJoin():boolean");
    }
}
