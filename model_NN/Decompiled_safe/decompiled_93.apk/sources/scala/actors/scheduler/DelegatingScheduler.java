package scala.actors.scheduler;

import scala.ScalaObject;
import scala.actors.IScheduler;
import scala.actors.Reactor;
import scala.concurrent.ManagedBlocker;
import scala.runtime.BoxesRunTime;

/* compiled from: DelegatingScheduler.scala */
public interface DelegatingScheduler extends ScalaObject, IScheduler {
    IScheduler impl();

    IScheduler makeNewScheduler();

    IScheduler sched();

    void sched_$eq(IScheduler iScheduler);

    /* renamed from: scala.actors.scheduler.DelegatingScheduler$class  reason: invalid class name */
    /* compiled from: DelegatingScheduler.scala */
    public abstract class Cclass {
        public static void $init$(DelegatingScheduler $this) {
            $this.sched_$eq(null);
        }

        public static final IScheduler impl(DelegatingScheduler $this) {
            IScheduler sched;
            synchronized ($this) {
                if ($this.sched() == null || !$this.sched().isActive()) {
                    $this.sched_$eq($this.makeNewScheduler());
                }
                sched = $this.sched();
            }
            return sched;
        }

        public static boolean isActive(DelegatingScheduler $this) {
            return true;
        }

        public static void execute(DelegatingScheduler $this, Runnable task) {
            $this.impl().execute(task);
        }

        public static void executeFromActor(DelegatingScheduler $this, Runnable task) {
            $this.impl().executeFromActor(task);
        }

        public static void newActor(DelegatingScheduler $this, Reactor actor) {
            Boolean boxToBoolean;
            boolean createNew;
            synchronized ($this) {
                if ($this.sched() == null) {
                    createNew = true;
                } else {
                    synchronized ($this.sched()) {
                        if ($this.sched().isActive()) {
                            $this.sched().newActor(actor);
                            boxToBoolean = BoxesRunTime.boxToBoolean(false);
                        } else {
                            boxToBoolean = BoxesRunTime.boxToBoolean(true);
                        }
                    }
                    createNew = BoxesRunTime.unboxToBoolean(boxToBoolean);
                }
                if (createNew) {
                    $this.sched_$eq($this.makeNewScheduler());
                    $this.sched().newActor(actor);
                }
            }
        }

        public static void terminated(DelegatingScheduler $this, Reactor actor) {
            $this.impl().terminated(actor);
        }

        public static void managedBlock(DelegatingScheduler $this, ManagedBlocker blocker) {
            $this.impl().managedBlock(blocker);
        }
    }
}
