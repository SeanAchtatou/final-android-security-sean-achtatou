package scala.actors.scheduler;

import scala.Function0;
import scala.ScalaObject;
import scala.actors.Reactor;
import scala.collection.mutable.HashMap;

/* compiled from: TerminationMonitor.scala */
public interface TerminationMonitor extends ScalaObject {
    int activeActors();

    void activeActors_$eq(int i);

    boolean allActorsTerminated();

    void gc();

    boolean scala$actors$scheduler$TerminationMonitor$$started();

    void scala$actors$scheduler$TerminationMonitor$$started_$eq(boolean z);

    void scala$actors$scheduler$TerminationMonitor$_setter_$terminationHandlers_$eq(HashMap hashMap);

    HashMap<Reactor<?>, Function0<Object>> terminationHandlers();

    /* renamed from: scala.actors.scheduler.TerminationMonitor$class  reason: invalid class name */
    /* compiled from: TerminationMonitor.scala */
    public abstract class Cclass {
        public static void $init$(TerminationMonitor $this) {
            $this.activeActors_$eq(0);
            $this.scala$actors$scheduler$TerminationMonitor$_setter_$terminationHandlers_$eq(new HashMap());
            $this.scala$actors$scheduler$TerminationMonitor$$started_$eq(false);
        }

        public static void newActor(TerminationMonitor $this, Reactor a) {
            synchronized ($this) {
                $this.activeActors_$eq($this.activeActors() + 1);
                if (!$this.scala$actors$scheduler$TerminationMonitor$$started()) {
                    $this.scala$actors$scheduler$TerminationMonitor$$started_$eq(true);
                }
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:15:0x0031, code lost:
            if (r1 != null) goto L_0x0033;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x0040, code lost:
            if (r2.equals(r1) != false) goto L_0x0042;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x0047, code lost:
            r0 = new scala.actors.scheduler.TerminationMonitor$$anonfun$1(r3);
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public static void terminated(scala.actors.scheduler.TerminationMonitor r3, scala.actors.Reactor r4) {
            /*
                monitor-enter(r3)
                scala.collection.mutable.HashMap r1 = r3.terminationHandlers()     // Catch:{ all -> 0x0039 }
                scala.Option r1 = r1.get(r4)     // Catch:{ all -> 0x0039 }
                boolean r2 = r1 instanceof scala.Some     // Catch:{ all -> 0x0039 }
                if (r2 == 0) goto L_0x002d
                scala.collection.mutable.HashMap r2 = r3.terminationHandlers()     // Catch:{ all -> 0x0039 }
                r2.$minus$eq(r4)     // Catch:{ all -> 0x0039 }
                scala.Some r1 = (scala.Some) r1     // Catch:{ all -> 0x0039 }
                java.lang.Object r1 = r1.x()     // Catch:{ all -> 0x0039 }
                r0 = r1
            L_0x001b:
                monitor-exit(r3)     // Catch:{ all -> 0x0039 }
                scala.Function0 r0 = (scala.Function0) r0
                r0.apply$mcV$sp()
                monitor-enter(r3)
                int r1 = r3.activeActors()     // Catch:{ all -> 0x0049 }
                r2 = 1
                int r1 = r1 - r2
                r3.activeActors_$eq(r1)     // Catch:{ all -> 0x0049 }
                monitor-exit(r3)     // Catch:{ all -> 0x0049 }
                return
            L_0x002d:
                scala.None$ r2 = scala.None$.MODULE$     // Catch:{ all -> 0x0039 }
                if (r2 != 0) goto L_0x003c
                if (r1 == 0) goto L_0x0042
            L_0x0033:
                scala.MatchError r2 = new scala.MatchError     // Catch:{ all -> 0x0039 }
                r2.<init>(r1)     // Catch:{ all -> 0x0039 }
                throw r2     // Catch:{ all -> 0x0039 }
            L_0x0039:
                r1 = move-exception
                monitor-exit(r3)
                throw r1
            L_0x003c:
                boolean r2 = r2.equals(r1)     // Catch:{ all -> 0x0039 }
                if (r2 == 0) goto L_0x0033
            L_0x0042:
                scala.actors.scheduler.TerminationMonitor$$anonfun$1 r1 = new scala.actors.scheduler.TerminationMonitor$$anonfun$1     // Catch:{ all -> 0x0039 }
                r1.<init>(r3)     // Catch:{ all -> 0x0039 }
                r0 = r1
                goto L_0x001b
            L_0x0049:
                r1 = move-exception
                monitor-exit(r3)
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.actors.scheduler.TerminationMonitor.Cclass.terminated(scala.actors.scheduler.TerminationMonitor, scala.actors.Reactor):void");
        }

        public static boolean allActorsTerminated(TerminationMonitor $this) {
            boolean z;
            synchronized ($this) {
                z = $this.scala$actors$scheduler$TerminationMonitor$$started() && $this.activeActors() <= 0;
            }
            return z;
        }

        public static void gc(TerminationMonitor $this) {
        }
    }
}
