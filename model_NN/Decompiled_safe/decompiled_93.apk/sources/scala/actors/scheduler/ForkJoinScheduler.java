package scala.actors.scheduler;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;
import scala.ScalaObject;
import scala.actors.Debug$;
import scala.actors.IScheduler;
import scala.actors.Reactor;
import scala.actors.scheduler.TerminationMonitor;
import scala.collection.mutable.HashMap;
import scala.collection.mutable.StringBuilder;
import scala.concurrent.ManagedBlocker;
import scala.concurrent.forkjoin.ForkJoinPool;
import scala.concurrent.forkjoin.ForkJoinTask;
import scala.concurrent.forkjoin.RecursiveAction;
import scala.runtime.BoxesRunTime;
import scala.runtime.StringAdd;

/* compiled from: ForkJoinScheduler.scala */
public class ForkJoinScheduler implements Runnable, ScalaObject, IScheduler, TerminationMonitor {
    private final int CHECK_FREQ;
    private int activeActors;
    public volatile int bitmap$0;
    private final boolean daemon;
    private Collection<ForkJoinTask<?>> drainedTasks;
    private final boolean fair;
    private final int initCoreSize;
    private final int maxSize;
    private DrainableForkJoinPool pool;
    private Random random;
    private boolean scala$actors$scheduler$TerminationMonitor$$started;
    private boolean snapshoting;
    private boolean terminating;
    private final HashMap terminationHandlers;

    public ForkJoinScheduler(int i, int i2, boolean z, boolean z2) {
        this.initCoreSize = i;
        this.maxSize = i2;
        this.daemon = z;
        this.fair = z2;
        IScheduler.Cclass.$init$(this);
        TerminationMonitor.Cclass.$init$(this);
        this.pool = makeNewPool();
        this.terminating = false;
        this.snapshoting = false;
        this.drainedTasks = null;
        this.CHECK_FREQ = 10;
    }

    public int activeActors() {
        return this.activeActors;
    }

    public void activeActors_$eq(int i) {
        this.activeActors = i;
    }

    public boolean allActorsTerminated() {
        return TerminationMonitor.Cclass.allActorsTerminated(this);
    }

    public void gc() {
        TerminationMonitor.Cclass.gc(this);
    }

    public int initCoreSize() {
        return this.initCoreSize;
    }

    public int maxSize() {
        return this.maxSize;
    }

    public void newActor(Reactor<?> a) {
        TerminationMonitor.Cclass.newActor(this, a);
    }

    public final boolean scala$actors$scheduler$TerminationMonitor$$started() {
        return this.scala$actors$scheduler$TerminationMonitor$$started;
    }

    public final void scala$actors$scheduler$TerminationMonitor$$started_$eq(boolean z) {
        this.scala$actors$scheduler$TerminationMonitor$$started = z;
    }

    public void scala$actors$scheduler$TerminationMonitor$_setter_$terminationHandlers_$eq(HashMap hashMap) {
        this.terminationHandlers = hashMap;
    }

    public void terminated(Reactor<?> a) {
        TerminationMonitor.Cclass.terminated(this, a);
    }

    public HashMap terminationHandlers() {
        return this.terminationHandlers;
    }

    private DrainableForkJoinPool pool() {
        return this.pool;
    }

    private boolean terminating() {
        return this.terminating;
    }

    private void terminating_$eq(boolean z) {
        this.terminating = z;
    }

    private boolean snapshoting() {
        return this.snapshoting;
    }

    private void drainedTasks_$eq(Collection<ForkJoinTask<?>> collection) {
        this.drainedTasks = collection;
    }

    public int CHECK_FREQ() {
        return this.CHECK_FREQ;
    }

    private Random random() {
        if ((this.bitmap$0 & 1) == 0) {
            synchronized (this) {
                if ((this.bitmap$0 & 1) == 0) {
                    this.random = new Random();
                    this.bitmap$0 |= 1;
                }
            }
        }
        return this.random;
    }

    public ForkJoinScheduler(boolean d, boolean f) {
        this(ThreadPoolConfig$.MODULE$.corePoolSize(), ThreadPoolConfig$.MODULE$.maxPoolSize(), d, f);
    }

    public ForkJoinScheduler(boolean d) {
        this(d, true);
    }

    public ForkJoinScheduler() {
        this(false);
    }

    private DrainableForkJoinPool makeNewPool() {
        DrainableForkJoinPool p = new DrainableForkJoinPool();
        p.setAsyncMode(true);
        p.setParallelism(initCoreSize());
        p.setMaximumPoolSize(maxSize());
        Debug$.MODULE$.info(new StringBuilder().append((Object) new StringAdd(this).$plus(": parallelism ")).append(BoxesRunTime.boxToInteger(p.getParallelism())).toString());
        Debug$.MODULE$.info(new StringBuilder().append((Object) new StringAdd(this).$plus(": max pool size ")).append(BoxesRunTime.boxToInteger(p.getMaximumPoolSize())).toString());
        return p;
    }

    public void start() {
        try {
            Thread t = new Thread(this);
            t.setDaemon(this.daemon);
            t.setName("ForkJoinScheduler");
            t.start();
        } catch (Exception e) {
            Debug$.MODULE$.info(new StringBuilder().append((Object) new StringAdd(this).$plus(": could not create scheduler thread: ")).append(e).toString());
        }
    }

    public void run() {
        while (true) {
            try {
                synchronized (this) {
                    liftedTree1$1();
                    if (terminating()) {
                        throw new QuitControl();
                    } else if (allActorsTerminated()) {
                        Debug$.MODULE$.info(new StringAdd(this).$plus(": all actors terminated"));
                        terminating_$eq(true);
                        throw new QuitControl();
                    } else if (!snapshoting()) {
                        gc();
                    } else if (pool().isQuiescent()) {
                        ArrayList arrayList = new ArrayList();
                        Debug$.MODULE$.info(new StringBuilder().append((Object) new StringAdd(this).$plus(": drained ")).append(BoxesRunTime.boxToInteger(pool().drainTasksTo(arrayList))).append((Object) " tasks").toString());
                        drainedTasks_$eq(arrayList);
                        terminating_$eq(true);
                        throw new QuitControl();
                    }
                }
            } catch (QuitControl e) {
                Debug$.MODULE$.info(new StringAdd(this).$plus(": initiating shutdown..."));
                while (!pool().isQuiescent()) {
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e2) {
                    }
                }
                pool().shutdown();
                return;
            }
        }
    }

    private final void liftedTree1$1() {
        try {
            wait((long) CHECK_FREQ());
        } catch (InterruptedException e) {
        }
    }

    public void execute(Runnable task) {
        pool().execute(task);
    }

    public void executeFromActor(Runnable runnable) {
        boolean z;
        if (this.fair) {
            synchronized (random()) {
                z = random().nextInt(50) == 1;
            }
            if (z) {
                pool().execute(runnable);
                return;
            }
        }
        ((RecursiveAction) runnable).fork();
    }

    public boolean isActive() {
        boolean z;
        synchronized (this) {
            z = !terminating() && pool() != null && !pool().isShutdown();
        }
        return z;
    }

    public void managedBlock(ManagedBlocker blocker$1) {
        ForkJoinPool.managedBlock(new ForkJoinScheduler$$anon$2(this, blocker$1), true);
    }
}
