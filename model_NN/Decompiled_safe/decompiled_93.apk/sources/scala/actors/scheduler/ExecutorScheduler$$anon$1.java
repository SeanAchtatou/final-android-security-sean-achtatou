package scala.actors.scheduler;

import java.util.concurrent.ExecutorService;
import scala.actors.IScheduler;
import scala.actors.Reactor;
import scala.actors.scheduler.ExecutorScheduler;
import scala.actors.scheduler.TerminationMonitor;
import scala.actors.scheduler.TerminationService;
import scala.collection.mutable.HashMap;
import scala.concurrent.ManagedBlocker;
import scala.concurrent.ThreadPoolRunner;

/* compiled from: ExecutorScheduler.scala */
public final class ExecutorScheduler$$anon$1 extends Thread implements ExecutorScheduler {
    private final int CHECK_FREQ;
    private int activeActors;
    private final ExecutorService executor;
    private boolean scala$actors$scheduler$TerminationMonitor$$started;
    private boolean scala$actors$scheduler$TerminationService$$terminating;
    private final boolean terminate;
    private final HashMap terminationHandlers;

    public ExecutorScheduler$$anon$1(ExecutorService executorService) {
        IScheduler.Cclass.$init$(this);
        TerminationMonitor.Cclass.$init$(this);
        TerminationService.Cclass.$init$(this);
        ThreadPoolRunner.Cclass.$init$(this);
        ExecutorScheduler.Cclass.$init$(this);
        this.executor = executorService;
    }

    public int CHECK_FREQ() {
        return this.CHECK_FREQ;
    }

    public int activeActors() {
        return this.activeActors;
    }

    public void activeActors_$eq(int i) {
        this.activeActors = i;
    }

    public boolean allActorsTerminated() {
        return TerminationMonitor.Cclass.allActorsTerminated(this);
    }

    public void execute(Runnable task) {
        ExecutorScheduler.Cclass.execute(this, task);
    }

    public void executeFromActor(Runnable task) {
        IScheduler.Cclass.executeFromActor(this, task);
    }

    public void gc() {
        TerminationMonitor.Cclass.gc(this);
    }

    public boolean isActive() {
        return ExecutorScheduler.Cclass.isActive(this);
    }

    public void managedBlock(ManagedBlocker blocker) {
        ThreadPoolRunner.Cclass.managedBlock(this, blocker);
    }

    public void newActor(Reactor<?> a) {
        TerminationMonitor.Cclass.newActor(this, a);
    }

    public void onShutdown() {
        ExecutorScheduler.Cclass.onShutdown(this);
    }

    public void run() {
        TerminationService.Cclass.run(this);
    }

    public final boolean scala$actors$scheduler$TerminationMonitor$$started() {
        return this.scala$actors$scheduler$TerminationMonitor$$started;
    }

    public final void scala$actors$scheduler$TerminationMonitor$$started_$eq(boolean z) {
        this.scala$actors$scheduler$TerminationMonitor$$started = z;
    }

    public void scala$actors$scheduler$TerminationMonitor$_setter_$terminationHandlers_$eq(HashMap hashMap) {
        this.terminationHandlers = hashMap;
    }

    public final boolean scala$actors$scheduler$TerminationService$$terminating() {
        return this.scala$actors$scheduler$TerminationService$$terminating;
    }

    public final void scala$actors$scheduler$TerminationService$$terminating_$eq(boolean z) {
        this.scala$actors$scheduler$TerminationService$$terminating = z;
    }

    public void scala$actors$scheduler$TerminationService$_setter_$CHECK_FREQ_$eq(int i) {
        this.CHECK_FREQ = i;
    }

    public void scala$actors$scheduler$TerminationService$_setter_$terminate_$eq(boolean z) {
        this.terminate = z;
    }

    public boolean terminate() {
        return this.terminate;
    }

    public void terminated(Reactor<?> a) {
        TerminationMonitor.Cclass.terminated(this, a);
    }

    public HashMap terminationHandlers() {
        return this.terminationHandlers;
    }

    public ExecutorService executor() {
        return this.executor;
    }
}
