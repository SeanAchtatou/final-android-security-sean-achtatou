package scala.actors;

import $anonfun;
import java.io.Serializable;
import scala.runtime.AbstractFunction0;
import scala.runtime.Nothing$;

/* compiled from: Future.scala */
public final class FutureActor$$anonfun$act$2$$anonfun$apply$mcV$sp$1 extends AbstractFunction0 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ FutureActor$$anonfun$act$2 $outer;

    public FutureActor$$anonfun$act$2$$anonfun$apply$mcV$sp$1(FutureActor<T>.$anonfun.act.2 $outer2) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
    }

    public final Nothing$ apply() {
        return this.$outer.$outer.react(new FutureActor$$anonfun$act$2$$anonfun$apply$mcV$sp$1$$anonfun$apply$1(this));
    }
}
