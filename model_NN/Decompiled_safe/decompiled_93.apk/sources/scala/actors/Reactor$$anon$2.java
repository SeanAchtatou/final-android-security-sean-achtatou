package scala.actors;

import scala.actors.IScheduler;
import scala.actors.scheduler.DelegatingScheduler;
import scala.concurrent.ManagedBlocker;

/* compiled from: Reactor.scala */
public final class Reactor$$anon$2 implements DelegatingScheduler {
    private IScheduler sched;

    public Reactor$$anon$2() {
        IScheduler.Cclass.$init$(this);
        DelegatingScheduler.Cclass.$init$(this);
    }

    public void execute(Runnable task) {
        DelegatingScheduler.Cclass.execute(this, task);
    }

    public void executeFromActor(Runnable task) {
        DelegatingScheduler.Cclass.executeFromActor(this, task);
    }

    public final IScheduler impl() {
        return DelegatingScheduler.Cclass.impl(this);
    }

    public boolean isActive() {
        return DelegatingScheduler.Cclass.isActive(this);
    }

    public void managedBlock(ManagedBlocker blocker) {
        DelegatingScheduler.Cclass.managedBlock(this, blocker);
    }

    public void newActor(Reactor<?> actor) {
        DelegatingScheduler.Cclass.newActor(this, actor);
    }

    public IScheduler sched() {
        return this.sched;
    }

    public void sched_$eq(IScheduler iScheduler) {
        this.sched = iScheduler;
    }

    public void terminated(Reactor<?> actor) {
        DelegatingScheduler.Cclass.terminated(this, actor);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v0, resolved type: scala.actors.scheduler.ExecutorScheduler} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v1, resolved type: scala.actors.scheduler.ExecutorScheduler} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v0, resolved type: scala.actors.scheduler.ForkJoinScheduler} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v2, resolved type: scala.actors.scheduler.ExecutorScheduler} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public scala.actors.IScheduler makeNewScheduler() {
        /*
            r11 = this;
            r2 = 0
            scala.actors.scheduler.ThreadPoolConfig$ r0 = scala.actors.scheduler.ThreadPoolConfig$.MODULE$
            boolean r0 = r0.useForkJoin()
            if (r0 == 0) goto L_0x0054
            scala.actors.scheduler.ForkJoinScheduler r8 = new scala.actors.scheduler.ForkJoinScheduler
            scala.actors.scheduler.ThreadPoolConfig$ r0 = scala.actors.scheduler.ThreadPoolConfig$.MODULE$
            int r0 = r0.corePoolSize()
            scala.actors.scheduler.ThreadPoolConfig$ r1 = scala.actors.scheduler.ThreadPoolConfig$.MODULE$
            int r1 = r1.maxPoolSize()
            r8.<init>(r0, r1, r2, r2)
            r8.start()
            r9 = r8
        L_0x001e:
            scala.actors.Debug$ r0 = scala.actors.Debug$.MODULE$
            scala.collection.mutable.StringBuilder r1 = new scala.collection.mutable.StringBuilder
            r1.<init>()
            scala.runtime.StringAdd r2 = new scala.runtime.StringAdd
            r2.<init>(r11)
            java.lang.String r3 = ": starting new "
            java.lang.String r2 = r2.$plus(r3)
            scala.collection.mutable.StringBuilder r1 = r1.append(r2)
            scala.collection.mutable.StringBuilder r1 = r1.append(r9)
            java.lang.String r2 = " ["
            scala.collection.mutable.StringBuilder r1 = r1.append(r2)
            java.lang.Class r2 = r9.getClass()
            scala.collection.mutable.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "]"
            scala.collection.mutable.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.info(r1)
            return r9
        L_0x0054:
            java.util.concurrent.LinkedBlockingQueue r6 = new java.util.concurrent.LinkedBlockingQueue
            r6.<init>()
            scala.actors.scheduler.ExecutorScheduler$ r10 = scala.actors.scheduler.ExecutorScheduler$.MODULE$
            java.util.concurrent.ThreadPoolExecutor r0 = new java.util.concurrent.ThreadPoolExecutor
            scala.actors.scheduler.ThreadPoolConfig$ r1 = scala.actors.scheduler.ThreadPoolConfig$.MODULE$
            int r1 = r1.corePoolSize()
            scala.actors.scheduler.ThreadPoolConfig$ r2 = scala.actors.scheduler.ThreadPoolConfig$.MODULE$
            int r2 = r2.maxPoolSize()
            r3 = 60000(0xea60, double:2.9644E-319)
            java.util.concurrent.TimeUnit r5 = java.util.concurrent.TimeUnit.MILLISECONDS
            java.util.concurrent.ThreadPoolExecutor$CallerRunsPolicy r7 = new java.util.concurrent.ThreadPoolExecutor$CallerRunsPolicy
            r7.<init>()
            r0.<init>(r1, r2, r3, r5, r6, r7)
            scala.actors.scheduler.ExecutorScheduler r0 = r10.apply(r0)
            r9 = r0
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.actors.Reactor$$anon$2.makeNewScheduler():scala.actors.IScheduler");
    }
}
