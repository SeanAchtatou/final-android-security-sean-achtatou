package scala.actors;

import $anonfun;
import java.io.Serializable;
import scala.runtime.AbstractFunction0$mcV$sp;

/* compiled from: Reactor.scala */
public final class Reactor$$anonfun$startSearch$1$$anonfun$apply$mcV$sp$1 extends AbstractFunction0$mcV$sp implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Reactor$$anonfun$startSearch$1 $outer;

    public Reactor$$anonfun$startSearch$1$$anonfun$apply$mcV$sp$1(Reactor<Msg>.$anonfun.startSearch.1 $outer2) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
    }

    public final void apply() {
        MQueue startMbox0 = new MQueue("Start");
        synchronized (this.$outer.$outer) {
            startMbox0.append(this.$outer.msg$1, this.$outer.replyTo$1);
        }
        this.$outer.$outer.searchMailbox(startMbox0, this.$outer.handler$1, true);
    }

    public void apply$mcV$sp() {
        MQueue startMbox = new MQueue("Start");
        synchronized (this.$outer.$outer) {
            startMbox.append(this.$outer.msg$1, this.$outer.replyTo$1);
        }
        this.$outer.$outer.searchMailbox(startMbox, this.$outer.handler$1, true);
    }
}
