package scala.actors;

import scala.Enumeration;
import scala.Function0;
import scala.Function1;
import scala.Option;
import scala.PartialFunction;
import scala.ScalaObject;
import scala.Tuple2;
import scala.actors.AbstractActor;
import scala.actors.Actor;
import scala.actors.ActorCanReply;
import scala.actors.Combinators;
import scala.actors.DaemonActor;
import scala.actors.Reactor;
import scala.actors.ReactorCanReply;
import scala.actors.ReplyReactor;
import scala.collection.immutable.List;
import scala.concurrent.SyncVar;
import scala.runtime.BoxedUnit;
import scala.runtime.Nothing$;

/* compiled from: Future.scala */
public class FutureActor<T> extends Future<T> implements DaemonActor, ScalaObject {
    private Enumeration.Value _state;
    private boolean enableChannel = false;
    private volatile Function0 kill;
    private List links;
    private final MQueue mailbox;
    private Option onTimeout;
    private /* synthetic */ Actor$blocker$ scala$actors$Actor$$blocker$module;
    private Object scala$actors$Actor$$exitReason;
    private volatile boolean scala$actors$Actor$$isSuspended;
    private volatile Option scala$actors$Actor$$received;
    public final Channel scala$actors$FutureActor$$channel;
    public final Function1 scala$actors$FutureActor$$fun;
    private final MQueue sendBuffer;
    private volatile List senders;
    private boolean shouldExit;
    private volatile boolean trapExit;
    private PartialFunction waitingFor;

    public FutureActor(Function1<SyncVar<T>, Object> fun, Channel<T> channel) {
        this.scala$actors$FutureActor$$fun = fun;
        this.scala$actors$FutureActor$$channel = channel;
        AbstractActor.Cclass.$init$(this);
        Combinators.Cclass.$init$(this);
        Reactor.Cclass.$init$(this);
        ReactorCanReply.Cclass.$init$(this);
        ReplyReactor.Cclass.$init$(this);
        ActorCanReply.Cclass.$init$(this);
        Actor.Cclass.$init$(this);
        DaemonActor.Cclass.$init$(this);
    }

    public void $bang(Object msg) {
        ReplyReactor.Cclass.$bang(this, msg);
    }

    public Object $bang$qmark(Object msg) {
        return ActorCanReply.Cclass.$bang$qmark(this, msg);
    }

    public Enumeration.Value _state() {
        return this._state;
    }

    public void _state_$eq(Enumeration.Value value) {
        this._state = value;
    }

    public void dostart() {
        Actor.Cclass.dostart(this);
    }

    public void drainSendBuffer(MQueue<Object> mbox) {
        Reactor.Cclass.drainSendBuffer(this, mbox);
    }

    public PartialFunction<Exception, Object> exceptionHandler() {
        return Reactor.Cclass.exceptionHandler(this);
    }

    public Nothing$ exit() {
        return Actor.Cclass.exit(this);
    }

    public void exit(AbstractActor from, Object reason) {
        Actor.Cclass.exit(this, from, reason);
    }

    public Function0<Object> exitLinked() {
        return Actor.Cclass.exitLinked(this);
    }

    public Function0<Object> exitLinked(Object reason) {
        return Actor.Cclass.exitLinked(this, reason);
    }

    public boolean exiting() {
        return Actor.Cclass.exiting(this);
    }

    public Function0 kill() {
        return this.kill;
    }

    public void kill_$eq(Function0 function0) {
        this.kill = function0;
    }

    public List links() {
        return this.links;
    }

    public void links_$eq(List list) {
        this.links = list;
    }

    public void loop(Function0<Object> body) {
        Combinators.Cclass.loop(this, body);
    }

    public MQueue mailbox() {
        return this.mailbox;
    }

    public final Runnable makeReaction(Function0<Object> fun) {
        return Reactor.Cclass.makeReaction(this, fun);
    }

    public Runnable makeReaction(Function0<Object> fun, PartialFunction<Object, Object> handler, Object msg) {
        return Actor.Cclass.makeReaction(this, fun, handler, msg);
    }

    public <A> Object mkBody(Function0<A> body) {
        return Reactor.Cclass.mkBody(this, body);
    }

    public Option onTimeout() {
        return this.onTimeout;
    }

    public void onTimeout_$eq(Option option) {
        this.onTimeout = option;
    }

    public Nothing$ react(PartialFunction<Object, Object> handler) {
        return Actor.Cclass.react(this, handler);
    }

    public <R> R receive(PartialFunction<Object, R> f) {
        return Actor.Cclass.receive(this, f);
    }

    public void reply(Object msg) {
        ReplyReactor.Cclass.reply(this, msg);
    }

    public void resumeReceiver(Tuple2<Object, OutputChannel<Object>> item, PartialFunction<Object, Object> handler, boolean onSameThread) {
        ReplyReactor.Cclass.resumeReceiver(this, item, handler, onSameThread);
    }

    public final Actor$blocker$ scala$actors$Actor$$blocker() {
        if (this.scala$actors$Actor$$blocker$module == null) {
            this.scala$actors$Actor$$blocker$module = new Actor$blocker$(this);
        }
        return this.scala$actors$Actor$$blocker$module;
    }

    public final Object scala$actors$Actor$$exitReason() {
        return this.scala$actors$Actor$$exitReason;
    }

    public final void scala$actors$Actor$$exitReason_$eq(Object obj) {
        this.scala$actors$Actor$$exitReason = obj;
    }

    public final boolean scala$actors$Actor$$isSuspended() {
        return this.scala$actors$Actor$$isSuspended;
    }

    public final void scala$actors$Actor$$isSuspended_$eq(boolean z) {
        this.scala$actors$Actor$$isSuspended = z;
    }

    public final Option scala$actors$Actor$$received() {
        return this.scala$actors$Actor$$received;
    }

    public final void scala$actors$Actor$$received_$eq(Option option) {
        this.scala$actors$Actor$$received = option;
    }

    public final void scala$actors$Actor$$super$dostart() {
        Reactor.Cclass.dostart(this);
    }

    public final Nothing$ scala$actors$Actor$$super$exit() {
        return Reactor.Cclass.exit(this);
    }

    public final Nothing$ scala$actors$Actor$$super$react(PartialFunction handler) {
        return ReplyReactor.Cclass.react(this, handler);
    }

    public final Reactor scala$actors$Actor$$super$start() {
        return Reactor.Cclass.start(this);
    }

    public final Function0 scala$actors$Actor$$super$startSearch(Object msg, OutputChannel replyTo, PartialFunction handler) {
        return Reactor.Cclass.startSearch(this, msg, replyTo, handler);
    }

    public void scala$actors$Reactor$_setter_$mailbox_$eq(MQueue mQueue) {
        this.mailbox = mQueue;
    }

    public void scala$actors$Reactor$_setter_$sendBuffer_$eq(MQueue mQueue) {
        this.sendBuffer = mQueue;
    }

    public final Nothing$ scala$actors$ReplyReactor$$super$react(PartialFunction handler) {
        return Reactor.Cclass.react(this, handler);
    }

    public final void scala$actors$ReplyReactor$$super$resumeReceiver(Tuple2 item, PartialFunction handler, boolean onSameThread) {
        Reactor.Cclass.resumeReceiver(this, item, handler, onSameThread);
    }

    public void scheduleActor(PartialFunction<Object, Object> f, Object msg) {
        Actor.Cclass.scheduleActor(this, f, msg);
    }

    public IScheduler scheduler() {
        return DaemonActor.Cclass.scheduler(this);
    }

    public void searchMailbox(MQueue<Object> startMbox, PartialFunction<Object, Object> handler, boolean resumeOnSameThread) {
        Actor.Cclass.searchMailbox(this, startMbox, handler, resumeOnSameThread);
    }

    public void send(Object msg, OutputChannel<Object> replyTo) {
        Reactor.Cclass.send(this, msg, replyTo);
    }

    public MQueue sendBuffer() {
        return this.sendBuffer;
    }

    public OutputChannel<Object> sender() {
        return ReplyReactor.Cclass.sender(this);
    }

    public List senders() {
        return this.senders;
    }

    public void senders_$eq(List list) {
        this.senders = list;
    }

    public <a, b> void seq(Function0<a> first, Function0<b> next) {
        Reactor.Cclass.seq(this, first, next);
    }

    public boolean shouldExit() {
        return this.shouldExit;
    }

    public void shouldExit_$eq(boolean z) {
        this.shouldExit = z;
    }

    public Actor start() {
        return Actor.Cclass.start(this);
    }

    public Function0<Object> startSearch(Object msg, OutputChannel<Object> replyTo, PartialFunction<Object, Object> handler) {
        return Actor.Cclass.startSearch(this, msg, replyTo, handler);
    }

    public void terminated() {
        Reactor.Cclass.terminated(this);
    }

    public boolean trapExit() {
        return this.trapExit;
    }

    public void trapExit_$eq(boolean z) {
        this.trapExit = z;
    }

    public void unlinkFrom(AbstractActor from) {
        Actor.Cclass.unlinkFrom(this, from);
    }

    public PartialFunction waitingFor() {
        return this.waitingFor;
    }

    public void waitingFor_$eq(PartialFunction partialFunction) {
        this.waitingFor = partialFunction;
    }

    public boolean enableChannel() {
        return this.enableChannel;
    }

    public T apply() {
        if (fvalue().isEmpty()) {
            $bang$qmark(Eval$.MODULE$);
        } else {
            BoxedUnit boxedUnit = BoxedUnit.UNIT;
        }
        return fvalueTyped();
    }

    public void act() {
        SyncVar res$1 = new SyncVar();
        mkBody(new FutureActor$$anonfun$act$1(this, res$1)).andThen(new FutureActor$$anonfun$act$2(this, res$1));
    }
}
