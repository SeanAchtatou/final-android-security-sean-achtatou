package scala;

import scala.Predef;
import scala.collection.immutable.List$;
import scala.collection.immutable.Map$;
import scala.collection.immutable.Set$;
import scala.collection.immutable.StringOps;
import scala.collection.mutable.ArrayOps;
import scala.collection.mutable.StringBuilder;
import scala.runtime.RichInt;
import scala.runtime.StringAdd;

/* compiled from: Predef.scala */
public final class Predef$ extends LowPriorityImplicits implements ScalaObject {
    public static final Predef$ MODULE$ = null;
    private final Map$ Map = Map$.MODULE$;
    private final Set$ Set = Set$.MODULE$;

    static {
        new Predef$();
    }

    private Predef$() {
        MODULE$ = this;
        package$ package_ = package$.MODULE$;
        List$ list$ = List$.MODULE$;
    }

    public Map$ Map() {
        return this.Map;
    }

    /* renamed from: assert  reason: not valid java name */
    public void m0assert(boolean assertion) {
        if (!assertion) {
            throw new AssertionError("assertion failed");
        }
    }

    /* renamed from: assert  reason: not valid java name */
    public void m1assert(boolean assertion, Function0<Object> message) {
        if (!assertion) {
            throw new AssertionError(new StringBuilder().append((Object) "assertion failed: ").append(message.apply()).toString());
        }
    }

    public void require(boolean requirement) {
        if (!requirement) {
            throw new IllegalArgumentException("requirement failed");
        }
    }

    public void require(boolean requirement, Function0<Object> message) {
        if (!requirement) {
            throw new IllegalArgumentException(new StringBuilder().append((Object) "requirement failed: ").append(message.apply()).toString());
        }
    }

    public <A> Predef.ArrowAssoc<A> any2ArrowAssoc(A x) {
        return new Predef.ArrowAssoc<>(x);
    }

    public RichInt intWrapper(int x) {
        return new RichInt(x);
    }

    public <T> ArrayOps<T> refArrayOps(T[] xs) {
        return new ArrayOps.ofRef(xs);
    }

    public StringAdd any2stringadd(Object x) {
        return new StringAdd(x);
    }

    public StringOps augmentString(String x) {
        return new StringOps(x);
    }
}
