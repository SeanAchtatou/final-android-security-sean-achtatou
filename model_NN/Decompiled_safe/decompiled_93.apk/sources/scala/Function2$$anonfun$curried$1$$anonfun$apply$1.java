package scala;

import $anonfun;
import java.io.Serializable;
import scala.runtime.AbstractFunction1;

/* compiled from: Function2.scala */
public final class Function2$$anonfun$curried$1$$anonfun$apply$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    private final /* synthetic */ Function2$$anonfun$curried$1 $outer;
    private final /* synthetic */ Object x1$1;

    public Function2$$anonfun$curried$1$$anonfun$apply$1(Function2$$anonfun$curried$1 $outer2, Function2<T1, T2, R>.$anonfun.curried.1 function2$$anonfun$curried$1) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.x1$1 = function2$$anonfun$curried$1;
    }

    public final R apply(T2 x2) {
        return this.$outer.$outer.apply(this.x1$1, x2);
    }
}
