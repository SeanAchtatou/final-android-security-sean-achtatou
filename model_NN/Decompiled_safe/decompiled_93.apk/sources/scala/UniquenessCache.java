package scala;

import java.lang.ref.WeakReference;
import java.util.WeakHashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* compiled from: Symbol.scala */
public abstract class UniquenessCache<K, V> implements ScalaObject {
    private final WeakHashMap<K, WeakReference<V>> map = new WeakHashMap<>();
    private final ReentrantReadWriteLock.ReadLock rlock = rwl().readLock();
    private final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
    private final ReentrantReadWriteLock.WriteLock wlock = rwl().writeLock();

    public abstract V valueFromKey(Object obj);

    private ReentrantReadWriteLock rwl() {
        return this.rwl;
    }

    private ReentrantReadWriteLock.ReadLock rlock() {
        return this.rlock;
    }

    private ReentrantReadWriteLock.WriteLock wlock() {
        return this.wlock;
    }

    private WeakHashMap<K, WeakReference<V>> map() {
        return this.map;
    }

    private final Object cached$1(Object obj) {
        Object exceptionResult1;
        rlock().lock();
        try {
            WeakReference reference = (WeakReference) map().get(obj);
            if (reference == null) {
                exceptionResult1 = null;
            } else {
                exceptionResult1 = reference.get();
            }
            return exceptionResult1;
        } finally {
            rlock().unlock();
        }
    }

    private final Object updateCache$1(Object obj) {
        Object exceptionResult2;
        wlock().lock();
        try {
            Object res = cached$1(obj);
            if (res == null) {
                Object sym = valueFromKey(obj);
                map().put(obj, new WeakReference(sym));
                exceptionResult2 = sym;
            } else {
                exceptionResult2 = res;
            }
            return exceptionResult2;
        } finally {
            wlock().unlock();
        }
    }

    public V apply(K name$1) {
        Object res = cached$1(name$1);
        if (res == null) {
            return updateCache$1(name$1);
        }
        return res;
    }
}
