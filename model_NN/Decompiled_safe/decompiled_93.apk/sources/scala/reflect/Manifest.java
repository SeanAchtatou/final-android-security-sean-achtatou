package scala.reflect;

import scala.Equals;
import scala.Option;
import scala.ScalaObject;
import scala.collection.immutable.List;
import scala.collection.immutable.Nil$;
import scala.collection.mutable.ArrayBuilder;
import scala.collection.mutable.StringBuilder;
import scala.collection.mutable.WrappedArray;
import scala.reflect.ClassManifest;
import scala.runtime.BoxesRunTime;

/* compiled from: Manifest.scala */
public interface Manifest<T> extends ClassManifest<T>, Equals, ScalaObject, ClassManifest {
    boolean canEqual(Object obj);

    /* renamed from: scala.reflect.Manifest$class  reason: invalid class name */
    /* compiled from: Manifest.scala */
    public abstract class Cclass {
        public static void $init$(Manifest $this) {
        }

        public static List typeArguments(Manifest $this) {
            return Nil$.MODULE$;
        }

        public static boolean canEqual(Manifest $this, Object that) {
            return that instanceof Manifest;
        }

        public static boolean equals(Manifest $this, Object that) {
            if (!(that instanceof Manifest)) {
                return false;
            }
            Manifest manifest = (Manifest) that;
            if (!manifest.canEqual($this)) {
                return false;
            }
            if (!$this.$less$colon$less(manifest) || !manifest.$less$colon$less($this)) {
                return false;
            }
            return true;
        }

        public static int hashCode(Manifest $this) {
            Class<?> erasure = $this.erasure();
            return erasure instanceof Number ? BoxesRunTime.hashFromNumber((Number) erasure) : erasure.hashCode();
        }
    }

    /* compiled from: Manifest.scala */
    public static class ClassTypeManifest<T> implements Manifest<T>, ScalaObject, Manifest {
        private final Class<?> erasure;
        private final Option<Manifest<?>> prefix;
        private final List<Manifest<?>> typeArguments;

        public ClassTypeManifest(Option<Manifest<?>> prefix2, Class<?> erasure2, List<Manifest<?>> typeArguments2) {
            this.prefix = prefix2;
            this.erasure = erasure2;
            this.typeArguments = typeArguments2;
            ClassManifest.Cclass.$init$(this);
            Cclass.$init$(this);
        }

        public boolean $less$colon$less(ClassManifest<?> that) {
            return ClassManifest.Cclass.$less$colon$less(this, that);
        }

        public String argString() {
            return ClassManifest.Cclass.argString(this);
        }

        public boolean canEqual(Object that) {
            return Cclass.canEqual(this, that);
        }

        public boolean equals(Object that) {
            return Cclass.equals(this, that);
        }

        public int hashCode() {
            return Cclass.hashCode(this);
        }

        public Object newArray(int len) {
            return ClassManifest.Cclass.newArray(this, len);
        }

        public ArrayBuilder<T> newArrayBuilder() {
            return ClassManifest.Cclass.newArrayBuilder(this);
        }

        public WrappedArray<T> newWrappedArray(int len) {
            return ClassManifest.Cclass.newWrappedArray(this, len);
        }

        public Class<?> erasure() {
            return this.erasure;
        }

        public List<Manifest<?>> typeArguments() {
            return this.typeArguments;
        }

        public String toString() {
            return new StringBuilder().append((Object) (this.prefix.isEmpty() ? "" : new StringBuilder().append((Object) this.prefix.get().toString()).append((Object) "#").toString())).append((Object) (erasure().isArray() ? "Array" : erasure().getName())).append((Object) argString()).toString();
        }
    }
}
