package scala.reflect;

import java.io.Serializable;
import scala.reflect.ClassManifest;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

/* compiled from: ClassManifest.scala */
public final class ClassManifest$$anonfun$subtype$1$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ ClassManifest $outer;
    public final /* synthetic */ Class sup$1;

    /* JADX WARN: Type inference failed for: r3v0, types: [scala.reflect.ClassManifest<T>, java.lang.Class] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ClassManifest$$anonfun$subtype$1$1(scala.reflect.ClassManifest r2, scala.reflect.ClassManifest<T> r3) {
        /*
            r1 = this;
            if (r2 != 0) goto L_0x0008
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            r0.<init>()
            throw r0
        L_0x0008:
            r1.$outer = r2
            r1.sup$1 = r3
            r1.<init>()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.reflect.ClassManifest$$anonfun$subtype$1$1.<init>(scala.reflect.ClassManifest, java.lang.Class):void");
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return BoxesRunTime.boxToBoolean(apply((Class<?>) ((Class) v1)));
    }

    public final boolean apply(Class<?> cls) {
        return ClassManifest.Cclass.subtype$1(this.$outer, cls, this.sup$1);
    }
}
