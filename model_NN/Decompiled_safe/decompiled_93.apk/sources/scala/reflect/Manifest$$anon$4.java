package scala.reflect;

import scala.collection.immutable.List;
import scala.collection.mutable.ArrayBuilder;
import scala.collection.mutable.WrappedArray;
import scala.reflect.AnyValManifest;
import scala.reflect.ClassManifest;
import scala.reflect.Manifest;

/* compiled from: Manifest.scala */
public final class Manifest$$anon$4 implements AnyValManifest<Integer> {
    public Manifest$$anon$4() {
        ClassManifest.Cclass.$init$(this);
        Manifest.Cclass.$init$(this);
        AnyValManifest.Cclass.$init$(this);
    }

    public boolean $less$colon$less(ClassManifest<?> that) {
        return AnyValManifest.Cclass.$less$colon$less(this, that);
    }

    public String argString() {
        return ClassManifest.Cclass.argString(this);
    }

    public boolean canEqual(Object other) {
        return AnyValManifest.Cclass.canEqual(this, other);
    }

    public boolean equals(Object that) {
        return AnyValManifest.Cclass.equals(this, that);
    }

    public int hashCode() {
        return AnyValManifest.Cclass.hashCode(this);
    }

    public List<Manifest<?>> typeArguments() {
        return Manifest.Cclass.typeArguments(this);
    }

    public Class<Integer> erasure() {
        return Integer.TYPE;
    }

    public String toString() {
        return "Int";
    }

    public int[] newArray(int len) {
        return new int[len];
    }

    public WrappedArray<Integer> newWrappedArray(int len) {
        return new WrappedArray.ofInt(new int[len]);
    }

    public ArrayBuilder<Integer> newArrayBuilder() {
        return new ArrayBuilder.ofInt();
    }
}
