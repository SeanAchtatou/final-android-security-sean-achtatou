package scala.reflect;

import scala.None$;
import scala.collection.immutable.Nil$;
import scala.reflect.Manifest;
import scala.runtime.Null$;

/* compiled from: Manifest.scala */
public final class Manifest$$anon$13 extends Manifest.ClassTypeManifest<Null$> {
    public Manifest$$anon$13() {
        super(None$.MODULE$, Object.class, Nil$.MODULE$);
    }

    public String toString() {
        return "Null";
    }

    public boolean $less$colon$less(ClassManifest<?> that) {
        return (that == null || that == Manifest$.MODULE$.Nothing() || that.$less$colon$less(Manifest$.MODULE$.AnyVal())) ? false : true;
    }

    public boolean equals(Object that) {
        return this == that;
    }

    public int hashCode() {
        return System.identityHashCode(this);
    }
}
