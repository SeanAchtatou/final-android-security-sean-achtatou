package scala.reflect;

import java.io.Serializable;
import scala.Tuple2;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

/* compiled from: ClassManifest.scala */
public final class ClassManifest$$anonfun$subargs$1$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;

    public ClassManifest$$anonfun$subargs$1$1(ClassManifest<T> $outer) {
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return BoxesRunTime.boxToBoolean(apply((Tuple2<OptManifest<?>, OptManifest<?>>) ((Tuple2) v1)));
    }

    public final boolean apply(Tuple2<OptManifest<?>, OptManifest<?>> tuple2) {
        if (tuple2 == null) {
            return false;
        }
        OptManifest _1 = tuple2._1();
        OptManifest _2 = tuple2._2();
        if (_1 instanceof ClassManifest) {
            ClassManifest classManifest = (ClassManifest) _1;
            if (_2 instanceof ClassManifest) {
                return classManifest.$less$colon$less((ClassManifest) _2);
            }
            return false;
        }
        NoManifest$ noManifest$ = NoManifest$.MODULE$;
        if (noManifest$ != null ? !noManifest$.equals(_1) : _1 != null) {
            return false;
        }
        NoManifest$ noManifest$2 = NoManifest$.MODULE$;
        return noManifest$2 != null ? noManifest$2.equals(_2) : _2 == null;
    }
}
