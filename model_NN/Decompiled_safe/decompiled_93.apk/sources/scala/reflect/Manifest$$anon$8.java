package scala.reflect;

import scala.collection.immutable.List;
import scala.collection.mutable.ArrayBuilder;
import scala.collection.mutable.WrappedArray;
import scala.reflect.AnyValManifest;
import scala.reflect.ClassManifest;
import scala.reflect.Manifest;

/* compiled from: Manifest.scala */
public final class Manifest$$anon$8 implements AnyValManifest<Boolean> {
    public Manifest$$anon$8() {
        ClassManifest.Cclass.$init$(this);
        Manifest.Cclass.$init$(this);
        AnyValManifest.Cclass.$init$(this);
    }

    public boolean $less$colon$less(ClassManifest<?> that) {
        return AnyValManifest.Cclass.$less$colon$less(this, that);
    }

    public String argString() {
        return ClassManifest.Cclass.argString(this);
    }

    public boolean canEqual(Object other) {
        return AnyValManifest.Cclass.canEqual(this, other);
    }

    public boolean equals(Object that) {
        return AnyValManifest.Cclass.equals(this, that);
    }

    public int hashCode() {
        return AnyValManifest.Cclass.hashCode(this);
    }

    public List<Manifest<?>> typeArguments() {
        return Manifest.Cclass.typeArguments(this);
    }

    public Class<Boolean> erasure() {
        return Boolean.TYPE;
    }

    public String toString() {
        return "Boolean";
    }

    public boolean[] newArray(int len) {
        return new boolean[len];
    }

    public WrappedArray<Boolean> newWrappedArray(int len) {
        return new WrappedArray.ofBoolean(new boolean[len]);
    }

    public ArrayBuilder<Boolean> newArrayBuilder() {
        return new ArrayBuilder.ofBoolean();
    }
}
