package scala.reflect;

import scala.Equals;
import scala.ScalaObject;

/* compiled from: Manifest.scala */
public interface AnyValManifest<T> extends Manifest<T>, Equals, ScalaObject, Manifest {

    /* renamed from: scala.reflect.AnyValManifest$class  reason: invalid class name */
    /* compiled from: Manifest.scala */
    public abstract class Cclass {
        public static void $init$(AnyValManifest $this) {
        }

        public static boolean $less$colon$less(AnyValManifest $this, ClassManifest that) {
            return that == $this || that == Manifest$.MODULE$.Any() || that == Manifest$.MODULE$.AnyVal();
        }

        public static boolean canEqual(AnyValManifest $this, Object other) {
            return other instanceof AnyValManifest;
        }

        public static boolean equals(AnyValManifest $this, Object that) {
            return $this == that;
        }

        public static int hashCode(AnyValManifest $this) {
            return System.identityHashCode($this);
        }
    }
}
