package scala.reflect;

import java.lang.reflect.Array;
import scala.Equals;
import scala.Option;
import scala.Predef$;
import scala.ScalaObject;
import scala.collection.LinearSeqOptimized;
import scala.collection.generic.TraversableFactory;
import scala.collection.immutable.List;
import scala.collection.immutable.List$;
import scala.collection.immutable.Nil$;
import scala.collection.mutable.ArrayBuilder;
import scala.collection.mutable.ArrayOps;
import scala.collection.mutable.StringBuilder;
import scala.collection.mutable.WrappedArray;
import scala.runtime.BoxesRunTime;

/* compiled from: ClassManifest.scala */
public interface ClassManifest<T> extends OptManifest<T>, Equals, ScalaObject, OptManifest {
    boolean $less$colon$less(ClassManifest<?> classManifest);

    String argString();

    boolean canEqual(Object obj);

    Class<?> erasure();

    Object newArray(int i);

    ArrayBuilder<T> newArrayBuilder();

    WrappedArray<T> newWrappedArray(int i);

    List<OptManifest<?>> typeArguments();

    /* renamed from: scala.reflect.ClassManifest$class  reason: invalid class name */
    /* compiled from: ClassManifest.scala */
    public abstract class Cclass {
        public static void $init$(ClassManifest $this) {
        }

        public static final boolean subtype$1(ClassManifest $this, Class sub, Class sup$1) {
            Object list;
            Class subSuperClass = sub.getSuperclass();
            List subSuperInterfaces = new ArrayOps.ofRef((Object[]) sub.getInterfaces()).toList();
            if (subSuperClass == null) {
                list = Nil$.MODULE$;
            } else {
                list = Predef$.MODULE$.wrapRefArray((Object[]) new Class[]{subSuperClass}).toList();
            }
            List subSuper = subSuperInterfaces.$colon$colon$colon(list);
            if (subSuper.contains(sup$1) || subSuper.exists(new ClassManifest$$anonfun$subtype$1$1($this, sup$1))) {
                return true;
            }
            return false;
        }

        private static final boolean subargs$1(ClassManifest $this, List args1, List args2) {
            return ((LinearSeqOptimized) args1.zip(args2, new TraversableFactory.GenericCanBuildFrom(List$.MODULE$))).forall(new ClassManifest$$anonfun$subargs$1$1($this));
        }

        /* JADX WARNING: Code restructure failed: missing block: B:16:0x003d, code lost:
            if (subtype$1(r3, r3.erasure(), r4.erasure()) != false) goto L_0x003f;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public static boolean $less$colon$less(scala.reflect.ClassManifest r3, scala.reflect.ClassManifest r4) {
            /*
                r2 = 0
                boolean r0 = r4 instanceof scala.reflect.AnyValManifest
                if (r0 == 0) goto L_0x0007
                r0 = r2
            L_0x0006:
                return r0
            L_0x0007:
                scala.reflect.Manifest$ r0 = scala.reflect.Manifest$.MODULE$
                scala.reflect.Manifest r0 = r0.AnyVal()
                if (r4 != 0) goto L_0x004f
                if (r0 == 0) goto L_0x0055
            L_0x0011:
                scala.reflect.Manifest$ r0 = scala.reflect.Manifest$.MODULE$
                scala.reflect.Manifest r0 = r0.Nothing()
                if (r4 != 0) goto L_0x0057
                if (r0 == 0) goto L_0x005d
            L_0x001b:
                scala.reflect.Manifest$ r0 = scala.reflect.Manifest$.MODULE$
                scala.reflect.Manifest r0 = r0.Null()
                if (r4 != 0) goto L_0x005f
                if (r0 == 0) goto L_0x0065
            L_0x0025:
                java.lang.Class r0 = r3.erasure()
                java.lang.Class r1 = r4.erasure()
                if (r0 != 0) goto L_0x0067
                if (r1 == 0) goto L_0x003f
            L_0x0031:
                java.lang.Class r0 = r3.erasure()
                java.lang.Class r1 = r4.erasure()
                boolean r0 = subtype$1(r3, r0, r1)
                if (r0 == 0) goto L_0x006e
            L_0x003f:
                scala.collection.immutable.List r0 = r3.typeArguments()
                scala.collection.immutable.List r1 = r4.typeArguments()
                boolean r0 = subargs$1(r3, r0, r1)
                if (r0 == 0) goto L_0x006e
                r0 = 1
                goto L_0x0006
            L_0x004f:
                boolean r0 = r4.equals(r0)
                if (r0 == 0) goto L_0x0011
            L_0x0055:
                r0 = r2
                goto L_0x0006
            L_0x0057:
                boolean r0 = r4.equals(r0)
                if (r0 == 0) goto L_0x001b
            L_0x005d:
                r0 = r2
                goto L_0x0006
            L_0x005f:
                boolean r0 = r4.equals(r0)
                if (r0 == 0) goto L_0x0025
            L_0x0065:
                r0 = r2
                goto L_0x0006
            L_0x0067:
                boolean r0 = r0.equals(r1)
                if (r0 != 0) goto L_0x003f
                goto L_0x0031
            L_0x006e:
                r0 = r2
                goto L_0x0006
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.reflect.ClassManifest.Cclass.$less$colon$less(scala.reflect.ClassManifest, scala.reflect.ClassManifest):boolean");
        }

        public static boolean canEqual(ClassManifest $this, Object other) {
            return other instanceof ClassManifest;
        }

        public static boolean equals(ClassManifest $this, Object that) {
            if (!(that instanceof ClassManifest)) {
                return false;
            }
            ClassManifest classManifest = (ClassManifest) that;
            if (!classManifest.canEqual($this)) {
                return false;
            }
            Class<?> erasure = $this.erasure();
            Class<?> erasure2 = classManifest.erasure();
            return erasure != null ? erasure.equals(erasure2) : erasure2 == null;
        }

        public static int hashCode(ClassManifest $this) {
            Class<?> erasure = $this.erasure();
            return erasure instanceof Number ? BoxesRunTime.hashFromNumber((Number) erasure) : erasure.hashCode();
        }

        public static Object newArray(ClassManifest $this, int len) {
            return Array.newInstance($this.erasure(), len);
        }

        public static WrappedArray newWrappedArray(ClassManifest $this, int len) {
            return new WrappedArray.ofRef((Object[]) $this.newArray(len));
        }

        public static ArrayBuilder newArrayBuilder(ClassManifest $this) {
            return new ArrayBuilder.ofRef($this);
        }

        public static String argString(ClassManifest $this) {
            if ($this.typeArguments().nonEmpty()) {
                return $this.typeArguments().mkString("[", ", ", "]");
            }
            if ($this.erasure().isArray()) {
                return new StringBuilder().append((Object) "[").append(ClassManifest$.MODULE$.fromClass($this.erasure().getComponentType())).append((Object) "]").toString();
            }
            return "";
        }
    }

    /* compiled from: ClassManifest.scala */
    public static class ClassTypeManifest<T> implements ClassManifest<T>, ScalaObject, ClassManifest {
        private final Class<?> erasure;
        private final Option<OptManifest<?>> prefix;
        private final List<OptManifest<?>> typeArguments;

        public ClassTypeManifest(Option<OptManifest<?>> prefix2, Class<?> erasure2, List<OptManifest<?>> typeArguments2) {
            this.prefix = prefix2;
            this.erasure = erasure2;
            this.typeArguments = typeArguments2;
            Cclass.$init$(this);
        }

        public boolean $less$colon$less(ClassManifest<?> that) {
            return Cclass.$less$colon$less(this, that);
        }

        public String argString() {
            return Cclass.argString(this);
        }

        public boolean canEqual(Object other) {
            return Cclass.canEqual(this, other);
        }

        public boolean equals(Object that) {
            return Cclass.equals(this, that);
        }

        public int hashCode() {
            return Cclass.hashCode(this);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [T[], java.lang.Object] */
        public T[] newArray(int len) {
            return Cclass.newArray(this, len);
        }

        public ArrayBuilder<T> newArrayBuilder() {
            return Cclass.newArrayBuilder(this);
        }

        public WrappedArray<T> newWrappedArray(int len) {
            return Cclass.newWrappedArray(this, len);
        }

        public Class<?> erasure() {
            return this.erasure;
        }

        public List<OptManifest<?>> typeArguments() {
            return this.typeArguments;
        }

        public String toString() {
            return new StringBuilder().append((Object) (this.prefix.isEmpty() ? "" : new StringBuilder().append((Object) this.prefix.get().toString()).append((Object) "#").toString())).append((Object) (erasure().isArray() ? "Array" : erasure().getName())).append((Object) argString()).toString();
        }
    }
}
