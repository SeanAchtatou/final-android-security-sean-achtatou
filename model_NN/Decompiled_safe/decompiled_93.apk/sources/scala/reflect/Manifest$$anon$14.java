package scala.reflect;

import scala.None$;
import scala.collection.immutable.Nil$;
import scala.reflect.Manifest;
import scala.runtime.Nothing$;

/* compiled from: Manifest.scala */
public final class Manifest$$anon$14 extends Manifest.ClassTypeManifest<Nothing$> {
    public Manifest$$anon$14() {
        super(None$.MODULE$, Object.class, Nil$.MODULE$);
    }

    public String toString() {
        return "Nothing";
    }

    public boolean $less$colon$less(ClassManifest<?> that) {
        return that != null;
    }

    public boolean equals(Object that) {
        return this == that;
    }

    public int hashCode() {
        return System.identityHashCode(this);
    }
}
