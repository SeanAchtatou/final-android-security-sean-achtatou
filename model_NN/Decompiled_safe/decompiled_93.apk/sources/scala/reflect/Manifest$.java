package scala.reflect;

import java.io.Serializable;
import scala.ScalaObject;
import scala.runtime.Nothing$;
import scala.runtime.Null$;

/* compiled from: Manifest.scala */
public final class Manifest$ implements Serializable, ScalaObject {
    public static final Manifest$ MODULE$ = null;
    private final Manifest<Object> Any = new Manifest$$anon$10();
    private final Manifest<Object> AnyVal = new Manifest$$anon$12();
    private final AnyValManifest<Boolean> Boolean = new Manifest$$anon$8();
    private final AnyValManifest<Byte> Byte = new Manifest$$anon$1();
    private final AnyValManifest<Character> Char = new Manifest$$anon$3();
    private final AnyValManifest<Double> Double = new Manifest$$anon$7();
    private final AnyValManifest<Float> Float = new Manifest$$anon$6();
    private final AnyValManifest<Integer> Int = new Manifest$$anon$4();
    private final AnyValManifest<Long> Long = new Manifest$$anon$5();
    private final Manifest<Nothing$> Nothing = new Manifest$$anon$14();
    private final Manifest<Null$> Null = new Manifest$$anon$13();
    private final Manifest<Object> Object = new Manifest$$anon$11();
    private final AnyValManifest<Short> Short = new Manifest$$anon$2();
    private final AnyValManifest<Object> Unit = new Manifest$$anon$9();

    static {
        new Manifest$();
    }

    private Manifest$() {
        MODULE$ = this;
    }

    public AnyValManifest<Byte> Byte() {
        return this.Byte;
    }

    public AnyValManifest<Short> Short() {
        return this.Short;
    }

    public AnyValManifest<Character> Char() {
        return this.Char;
    }

    public AnyValManifest<Integer> Int() {
        return this.Int;
    }

    public AnyValManifest<Long> Long() {
        return this.Long;
    }

    public AnyValManifest<Float> Float() {
        return this.Float;
    }

    public AnyValManifest<Double> Double() {
        return this.Double;
    }

    public AnyValManifest<Boolean> Boolean() {
        return this.Boolean;
    }

    public AnyValManifest<Object> Unit() {
        return this.Unit;
    }

    public Manifest<Object> Any() {
        return this.Any;
    }

    public Manifest<Object> Object() {
        return this.Object;
    }

    public Manifest<Object> AnyVal() {
        return this.AnyVal;
    }

    public Manifest<Null$> Null() {
        return this.Null;
    }

    public Manifest<Nothing$> Nothing() {
        return this.Nothing;
    }
}
