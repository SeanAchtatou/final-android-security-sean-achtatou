package scala.reflect;

import scala.None$;
import scala.collection.immutable.Nil$;
import scala.reflect.Manifest;

/* compiled from: Manifest.scala */
public final class Manifest$$anon$12 extends Manifest.ClassTypeManifest<Object> {
    public Manifest$$anon$12() {
        super(None$.MODULE$, Object.class, Nil$.MODULE$);
    }

    public String toString() {
        return "AnyVal";
    }

    public boolean $less$colon$less(ClassManifest<?> that) {
        return that == this || that == Manifest$.MODULE$.Any();
    }

    public boolean equals(Object that) {
        return this == that;
    }

    public int hashCode() {
        return System.identityHashCode(this);
    }
}
