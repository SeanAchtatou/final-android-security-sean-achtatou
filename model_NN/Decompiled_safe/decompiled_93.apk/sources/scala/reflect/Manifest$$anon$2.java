package scala.reflect;

import scala.collection.immutable.List;
import scala.collection.mutable.ArrayBuilder;
import scala.collection.mutable.WrappedArray;
import scala.reflect.AnyValManifest;
import scala.reflect.ClassManifest;
import scala.reflect.Manifest;

/* compiled from: Manifest.scala */
public final class Manifest$$anon$2 implements AnyValManifest<Short> {
    public Manifest$$anon$2() {
        ClassManifest.Cclass.$init$(this);
        Manifest.Cclass.$init$(this);
        AnyValManifest.Cclass.$init$(this);
    }

    public boolean $less$colon$less(ClassManifest<?> that) {
        return AnyValManifest.Cclass.$less$colon$less(this, that);
    }

    public String argString() {
        return ClassManifest.Cclass.argString(this);
    }

    public boolean canEqual(Object other) {
        return AnyValManifest.Cclass.canEqual(this, other);
    }

    public boolean equals(Object that) {
        return AnyValManifest.Cclass.equals(this, that);
    }

    public int hashCode() {
        return AnyValManifest.Cclass.hashCode(this);
    }

    public List<Manifest<?>> typeArguments() {
        return Manifest.Cclass.typeArguments(this);
    }

    public Class<Short> erasure() {
        return Short.TYPE;
    }

    public String toString() {
        return "Short";
    }

    public short[] newArray(int len) {
        return new short[len];
    }

    public WrappedArray<Short> newWrappedArray(int len) {
        return new WrappedArray.ofShort(new short[len]);
    }

    public ArrayBuilder<Short> newArrayBuilder() {
        return new ArrayBuilder.ofShort();
    }
}
