package scala.reflect;

import java.io.Serializable;
import scala.None$;
import scala.ScalaObject;
import scala.collection.Seq;
import scala.collection.immutable.Nil$;
import scala.reflect.ClassManifest;
import scala.runtime.Nothing$;
import scala.runtime.Null$;

/* compiled from: ClassManifest.scala */
public final class ClassManifest$ implements Serializable, ScalaObject {
    public static final ClassManifest$ MODULE$ = null;
    private final Manifest<Object> Any = Manifest$.MODULE$.Any();
    private final Manifest<Object> AnyVal = Manifest$.MODULE$.AnyVal();
    private final AnyValManifest<Boolean> Boolean = Manifest$.MODULE$.Boolean();
    private final AnyValManifest<Byte> Byte = Manifest$.MODULE$.Byte();
    private final AnyValManifest<Character> Char = Manifest$.MODULE$.Char();
    private final AnyValManifest<Double> Double = Manifest$.MODULE$.Double();
    private final AnyValManifest<Float> Float = Manifest$.MODULE$.Float();
    private final AnyValManifest<Integer> Int = Manifest$.MODULE$.Int();
    private final AnyValManifest<Long> Long = Manifest$.MODULE$.Long();
    private final Manifest<Nothing$> Nothing = Manifest$.MODULE$.Nothing();
    private final Manifest<Null$> Null = Manifest$.MODULE$.Null();
    private final Manifest<Object> Object = Manifest$.MODULE$.Object();
    private final AnyValManifest<Short> Short = Manifest$.MODULE$.Short();
    private final AnyValManifest<Object> Unit = Manifest$.MODULE$.Unit();

    static {
        new ClassManifest$();
    }

    private ClassManifest$() {
        MODULE$ = this;
    }

    public AnyValManifest<Byte> Byte() {
        return this.Byte;
    }

    public AnyValManifest<Short> Short() {
        return this.Short;
    }

    public AnyValManifest<Character> Char() {
        return this.Char;
    }

    public AnyValManifest<Integer> Int() {
        return this.Int;
    }

    public AnyValManifest<Long> Long() {
        return this.Long;
    }

    public AnyValManifest<Float> Float() {
        return this.Float;
    }

    public AnyValManifest<Double> Double() {
        return this.Double;
    }

    public AnyValManifest<Boolean> Boolean() {
        return this.Boolean;
    }

    public AnyValManifest<Object> Unit() {
        return this.Unit;
    }

    public <T> ClassManifest<T> fromClass(Class<T> clazz) {
        Class cls = Byte.TYPE;
        if (cls != null ? cls.equals(clazz) : clazz == null) {
            return this.Byte;
        }
        Class cls2 = Short.TYPE;
        if (cls2 != null ? cls2.equals(clazz) : clazz == null) {
            return this.Short;
        }
        Class cls3 = Character.TYPE;
        if (cls3 != null ? cls3.equals(clazz) : clazz == null) {
            return this.Char;
        }
        Class cls4 = Integer.TYPE;
        if (cls4 != null ? cls4.equals(clazz) : clazz == null) {
            return this.Int;
        }
        Class cls5 = Long.TYPE;
        if (cls5 != null ? cls5.equals(clazz) : clazz == null) {
            return this.Long;
        }
        Class cls6 = Float.TYPE;
        if (cls6 != null ? cls6.equals(clazz) : clazz == null) {
            return this.Float;
        }
        Class cls7 = Double.TYPE;
        if (cls7 != null ? cls7.equals(clazz) : clazz == null) {
            return this.Double;
        }
        Class cls8 = Boolean.TYPE;
        if (cls8 != null ? cls8.equals(clazz) : clazz == null) {
            return this.Boolean;
        }
        Class cls9 = Void.TYPE;
        if (cls9 != null ? !cls9.equals(clazz) : clazz != null) {
            return new ClassManifest.ClassTypeManifest(None$.MODULE$, clazz, Nil$.MODULE$);
        }
        return this.Unit;
    }

    public <T> ClassManifest<T> classType(Class<?> clazz) {
        return new ClassManifest.ClassTypeManifest(None$.MODULE$, clazz, Nil$.MODULE$);
    }

    public <T> ClassManifest<T> classType(Class<?> clazz, OptManifest<?> arg1, Seq<OptManifest<?>> args) {
        return new ClassManifest.ClassTypeManifest(None$.MODULE$, clazz, args.toList().$colon$colon(arg1));
    }
}
