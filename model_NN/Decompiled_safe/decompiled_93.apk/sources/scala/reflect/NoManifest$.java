package scala.reflect;

import scala.ScalaObject;
import scala.runtime.Nothing$;

/* compiled from: NoManifest.scala */
public final class NoManifest$ implements OptManifest<Nothing$>, ScalaObject, OptManifest {
    public static final NoManifest$ MODULE$ = null;

    static {
        new NoManifest$();
    }

    private NoManifest$() {
        MODULE$ = this;
    }

    public String toString() {
        return "<?>";
    }
}
