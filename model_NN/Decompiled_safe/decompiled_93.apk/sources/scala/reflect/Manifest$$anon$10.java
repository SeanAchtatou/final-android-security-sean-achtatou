package scala.reflect;

import scala.None$;
import scala.collection.immutable.Nil$;
import scala.reflect.Manifest;

/* compiled from: Manifest.scala */
public final class Manifest$$anon$10 extends Manifest.ClassTypeManifest<Object> {
    public Manifest$$anon$10() {
        super(None$.MODULE$, Object.class, Nil$.MODULE$);
    }

    public String toString() {
        return "Any";
    }

    public boolean $less$colon$less(ClassManifest<?> that) {
        return that == this;
    }

    public boolean equals(Object that) {
        return this == that;
    }

    public int hashCode() {
        return System.identityHashCode(this);
    }
}
