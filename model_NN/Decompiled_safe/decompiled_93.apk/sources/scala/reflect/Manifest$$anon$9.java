package scala.reflect;

import scala.collection.immutable.List;
import scala.collection.mutable.ArrayBuilder;
import scala.collection.mutable.WrappedArray;
import scala.reflect.AnyValManifest;
import scala.reflect.ClassManifest;
import scala.reflect.Manifest;
import scala.runtime.BoxedUnit;

/* compiled from: Manifest.scala */
public final class Manifest$$anon$9 implements AnyValManifest<Object> {
    public Manifest$$anon$9() {
        ClassManifest.Cclass.$init$(this);
        Manifest.Cclass.$init$(this);
        AnyValManifest.Cclass.$init$(this);
    }

    public boolean $less$colon$less(ClassManifest<?> that) {
        return AnyValManifest.Cclass.$less$colon$less(this, that);
    }

    public String argString() {
        return ClassManifest.Cclass.argString(this);
    }

    public boolean canEqual(Object other) {
        return AnyValManifest.Cclass.canEqual(this, other);
    }

    public boolean equals(Object that) {
        return AnyValManifest.Cclass.equals(this, that);
    }

    public int hashCode() {
        return AnyValManifest.Cclass.hashCode(this);
    }

    public List<Manifest<?>> typeArguments() {
        return Manifest.Cclass.typeArguments(this);
    }

    public Class<Void> erasure() {
        return Void.TYPE;
    }

    public String toString() {
        return "Unit";
    }

    public BoxedUnit[] newArray(int len) {
        return new BoxedUnit[len];
    }

    public WrappedArray<Object> newWrappedArray(int len) {
        return new WrappedArray.ofUnit(new BoxedUnit[len]);
    }

    public ArrayBuilder<Object> newArrayBuilder() {
        return new ArrayBuilder.ofUnit();
    }
}
