package scala;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;

/* compiled from: Function1.scala */
public final class Function1$$anonfun$compose$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    private final /* synthetic */ Function1 $outer;
    private final /* synthetic */ Function1 g$1;

    public Function1$$anonfun$compose$1(Function1 $outer2, Function1<T1, R> function1) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.g$1 = function1;
    }

    public final R apply(A x) {
        return this.$outer.apply(this.g$1.apply(x));
    }
}
