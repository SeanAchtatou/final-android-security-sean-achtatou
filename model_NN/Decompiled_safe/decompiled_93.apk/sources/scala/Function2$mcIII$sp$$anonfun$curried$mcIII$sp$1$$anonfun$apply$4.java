package scala;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

/* compiled from: Function2.scala */
public final class Function2$mcIII$sp$$anonfun$curried$mcIII$sp$1$$anonfun$apply$4 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Function2$mcIII$sp$$anonfun$curried$mcIII$sp$1 $outer;
    public final /* synthetic */ int x1$4;

    public Function2$mcIII$sp$$anonfun$curried$mcIII$sp$1$$anonfun$apply$4(Function2$mcIII$sp$$anonfun$curried$mcIII$sp$1 $outer2, int i) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.x1$4 = i;
    }

    public final int apply(int x2) {
        return this.$outer.$outer.apply(this.x1$4, x2);
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return BoxesRunTime.boxToInteger(apply(BoxesRunTime.unboxToInt(v1)));
    }
}
