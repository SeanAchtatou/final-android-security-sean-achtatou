package scala;

/* compiled from: Symbol.scala */
public final class Symbol$ extends UniquenessCache<String, Symbol> implements ScalaObject, ScalaObject {
    public static final Symbol$ MODULE$ = null;

    static {
        new Symbol$();
    }

    private Symbol$() {
        MODULE$ = this;
    }

    public Symbol valueFromKey(String name) {
        return new Symbol(name);
    }
}
