package scala;

import scala.collection.generic.CanBuildFrom;
import scala.collection.immutable.IndexedSeq;
import scala.collection.immutable.VectorBuilder;
import scala.collection.mutable.Builder;

/* compiled from: LowPriorityImplicits.scala */
public final class LowPriorityImplicits$$anon$1 implements CanBuildFrom<String, Object, IndexedSeq<Object>> {
    public LowPriorityImplicits$$anon$1(LowPriorityImplicits $outer) {
    }

    public Builder<T, IndexedSeq<T>> apply(String from) {
        return new VectorBuilder();
    }
}
