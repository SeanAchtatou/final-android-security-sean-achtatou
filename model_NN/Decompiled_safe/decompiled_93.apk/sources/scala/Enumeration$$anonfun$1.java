package scala;

import java.io.Serializable;
import java.lang.reflect.Method;
import scala.Enumeration;
import scala.collection.mutable.ArrayOps;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

/* compiled from: Enumeration.scala */
public final class Enumeration$$anonfun$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;

    public Enumeration$$anonfun$1(Enumeration $outer) {
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return BoxesRunTime.boxToBoolean(apply((Method) v1));
    }

    public final boolean apply(Method m) {
        return new ArrayOps.ofRef((Object[]) m.getParameterTypes()).isEmpty() && Enumeration.Value.class.isAssignableFrom(m.getReturnType());
    }
}
