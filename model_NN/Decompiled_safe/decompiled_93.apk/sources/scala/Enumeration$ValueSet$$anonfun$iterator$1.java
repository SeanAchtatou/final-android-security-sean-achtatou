package scala;

import java.io.Serializable;
import scala.Enumeration;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

/* compiled from: Enumeration.scala */
public final class Enumeration$ValueSet$$anonfun$iterator$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Enumeration.ValueSet $outer;

    public Enumeration$ValueSet$$anonfun$iterator$1(Enumeration.ValueSet $outer2) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return apply(BoxesRunTime.unboxToInt(v1));
    }

    public final Enumeration.Value apply(int i) {
        return this.$outer.scala$Enumeration$ValueSet$$$outer().apply(i);
    }
}
