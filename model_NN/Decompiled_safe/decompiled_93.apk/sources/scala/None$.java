package scala;

import java.util.NoSuchElementException;
import scala.runtime.BoxesRunTime;
import scala.runtime.Nothing$;

/* compiled from: Option.scala */
public final class None$ extends Option<Nothing$> implements ScalaObject, Product, ScalaObject {
    public static final None$ MODULE$ = null;

    static {
        new None$();
    }

    private None$() {
        MODULE$ = this;
    }

    public boolean canEqual(Object obj) {
        return obj instanceof None$;
    }

    public int productArity() {
        return 0;
    }

    public Object productElement(int i) {
        throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(i).toString());
    }

    public String productPrefix() {
        return "None";
    }

    public final String toString() {
        return "None";
    }

    public boolean isEmpty() {
        return true;
    }

    public Nothing$ get() {
        throw new NoSuchElementException("None.get");
    }
}
