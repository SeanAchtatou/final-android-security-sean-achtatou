package scala;

import $anonfun$curried$1.$anonfun$apply$1.$anonfun;
import java.io.Serializable;
import scala.runtime.AbstractFunction1;

/* compiled from: Function4.scala */
public final class Function4$$anonfun$curried$1$$anonfun$apply$1$$anonfun$apply$2$$anonfun$apply$3 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    private final /* synthetic */ Function4$$anonfun$curried$1$$anonfun$apply$1$$anonfun$apply$2 $outer;
    private final /* synthetic */ Object x3$1;

    public Function4$$anonfun$curried$1$$anonfun$apply$1$$anonfun$apply$2$$anonfun$apply$3(Function4$$anonfun$curried$1$$anonfun$apply$1$$anonfun$apply$2 $outer2, Function4<T1, T2, T3, T4, R>.$anonfun.apply.2 r3) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.x3$1 = r3;
    }

    public final R apply(T4 x4) {
        return this.$outer.$outer.$outer.$outer.apply(this.$outer.$outer.x1$1, this.$outer.x2$1, this.x3$1, x4);
    }
}
