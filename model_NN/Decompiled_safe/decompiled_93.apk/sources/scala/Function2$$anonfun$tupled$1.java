package scala;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;

/* compiled from: Function2.scala */
public final class Function2$$anonfun$tupled$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Function2 $outer;

    public Function2$$anonfun$tupled$1(Function2<T1, T2, R> $outer2) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return apply((Tuple2) ((Tuple2) v1));
    }

    public final R apply(Tuple2<T1, T2> tuple2) {
        if (tuple2 != null) {
            return this.$outer.apply(tuple2._1(), tuple2._2());
        }
        throw new MatchError(tuple2);
    }
}
