package scala;

import scala.runtime.BoxesRunTime;

/* compiled from: Proxy.scala */
public interface Proxy extends ScalaObject {
    Object self();

    /* renamed from: scala.Proxy$class  reason: invalid class name */
    /* compiled from: Proxy.scala */
    public abstract class Cclass {
        public static void $init$(Proxy $this) {
        }

        public static int hashCode(Proxy $this) {
            Object self = $this.self();
            return self instanceof Number ? BoxesRunTime.hashFromNumber((Number) self) : self.hashCode();
        }

        public static boolean equals(Proxy $this, Object that) {
            if (that == null) {
                return false;
            }
            return that.equals($this.self());
        }

        public static String toString(Proxy $this) {
            return $this.self().toString();
        }
    }
}
