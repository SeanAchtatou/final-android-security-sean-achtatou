package scala;

import java.io.Serializable;
import java.lang.reflect.Method;
import scala.Enumeration;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

/* compiled from: Enumeration.scala */
public final class Enumeration$$anonfun$scala$Enumeration$$populateNameMap$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Enumeration $outer;

    public Enumeration$$anonfun$scala$Enumeration$$populateNameMap$1(Enumeration $outer2) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
    }

    public final Object apply(Method m) {
        String name = m.getName();
        Enumeration.Value value = (Enumeration.Value) m.invoke(this.$outer, new Object[0]);
        if (value.scala$Enumeration$$outerEnum() != this.$outer) {
            return BoxedUnit.UNIT;
        }
        return this.$outer.scala$Enumeration$$nmap().$plus$eq(new Tuple2(BoxesRunTime.boxToInteger(BoxesRunTime.unboxToInt(Enumeration.Val.class.getMethod("id", new Class[0]).invoke(value, new Object[0]))), name));
    }
}
