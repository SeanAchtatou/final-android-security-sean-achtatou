package scala.xml;

import scala.Predef$;
import scala.ScalaObject;
import scala.collection.Seq;
import scala.collection.Seq$;
import scala.collection.mutable.StringBuilder;
import scala.runtime.BoxesRunTime;

/* compiled from: Atom.scala */
public class Atom<A> extends SpecialNode implements ScalaObject, ScalaObject {
    private final A data;

    public A data() {
        return this.data;
    }

    public Seq<Object> basisForHashCode() {
        return (Seq) Seq$.MODULE$.apply(Predef$.MODULE$.genericWrapArray(new Object[]{data()}));
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    public boolean strict_$eq$eq(Equality other) {
        if (!(other instanceof Atom)) {
            return false;
        }
        Object data2 = data();
        Object data3 = ((Atom) other).data();
        return data2 == data3 ? true : data2 == null ? false : data2 instanceof Number ? BoxesRunTime.equalsNumObject((Number) data2, data3) : data2 instanceof Character ? BoxesRunTime.equalsCharObject((Character) data2, data3) : data2.equals(data3);
    }

    public boolean canEqual(Object other) {
        return other instanceof Atom;
    }

    public String label() {
        return "#PCDATA";
    }

    public StringBuilder buildString(StringBuilder sb) {
        return Utility$.MODULE$.escape(data().toString(), sb);
    }

    public String text() {
        return data().toString();
    }
}
