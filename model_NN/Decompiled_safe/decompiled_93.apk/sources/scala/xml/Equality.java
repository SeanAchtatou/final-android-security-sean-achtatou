package scala.xml;

import scala.Equals;
import scala.MatchError;
import scala.ScalaObject;
import scala.collection.LinearSeqOptimized;
import scala.collection.Seq;
import scala.collection.generic.TraversableFactory;
import scala.collection.immutable.C$colon$colon;
import scala.collection.immutable.List$;
import scala.collection.immutable.Nil$;
import scala.runtime.BoxesRunTime;

/* compiled from: Equality.scala */
public interface Equality extends Equals, ScalaObject {
    Seq<Object> basisForHashCode();

    boolean canEqual(Object obj);

    boolean strict_$eq$eq(Equality equality);

    /* renamed from: scala.xml.Equality$class  reason: invalid class name */
    /* compiled from: Equality.scala */
    public abstract class Cclass {
        public static void $init$(Equality $this) {
        }

        public static final int scala$xml$Equality$$hashOf(Equality $this, Object x) {
            if (x == null) {
                return 1;
            }
            return x instanceof Number ? BoxesRunTime.hashFromNumber((Number) x) : x.hashCode();
        }

        public static int hashCode(Equality $this) {
            Seq<Object> basisForHashCode = $this.basisForHashCode();
            Nil$ nil$ = Nil$.MODULE$;
            if (nil$ != null ? nil$.equals(basisForHashCode) : basisForHashCode == null) {
                return 0;
            }
            if (basisForHashCode instanceof C$colon$colon) {
                C$colon$colon _colon_colon = (C$colon$colon) basisForHashCode;
                return BoxesRunTime.unboxToInt(((LinearSeqOptimized) _colon_colon.tl.map(new Equality$$anonfun$hashCode$2($this), new TraversableFactory.GenericCanBuildFrom(List$.MODULE$))).foldLeft(BoxesRunTime.boxToInteger(0), new Equality$$anonfun$hashCode$1($this))) + (scala$xml$Equality$$hashOf($this, _colon_colon.scala$collection$immutable$$colon$colon$$hd) * 41);
            }
            throw new MatchError(basisForHashCode);
        }

        public static boolean equals(Equality $this, Object other) {
            return doComparison($this, other, false);
        }

        private static boolean doComparison(Equality $this, Object other, boolean blithe) {
            boolean strictlyEqual;
            if (!(other instanceof Object)) {
                strictlyEqual = false;
            } else if (gd2$1($this, other)) {
                strictlyEqual = true;
            } else if (other instanceof Equality) {
                Equality equality = (Equality) other;
                strictlyEqual = equality.canEqual($this) && $this.strict_$eq$eq(equality);
            } else {
                strictlyEqual = false;
            }
            if (strictlyEqual || (blithe && Equality$.MODULE$.compareBlithely($this, other))) {
                return true;
            }
            return false;
        }

        private static final /* synthetic */ boolean gd2$1(Equality $this, Object obj) {
            return $this == obj;
        }
    }
}
