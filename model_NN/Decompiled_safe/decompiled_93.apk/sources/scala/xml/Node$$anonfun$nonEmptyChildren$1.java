package scala.xml;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

/* compiled from: Node.scala */
public final class Node$$anonfun$nonEmptyChildren$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;

    public Node$$anonfun$nonEmptyChildren$1(Node $outer) {
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return BoxesRunTime.boxToBoolean(apply((Node) v1));
    }

    public final boolean apply(Node node) {
        String node2 = node.toString();
        return node2 != null ? node2.equals("") : "" == 0;
    }
}
