package scala.xml;

import scala.ScalaObject;
import scala.collection.immutable.Nil$;
import scala.collection.mutable.StringBuilder;

/* compiled from: SpecialNode.scala */
public abstract class SpecialNode extends Node implements ScalaObject {
    public abstract StringBuilder buildString(StringBuilder stringBuilder);

    public final Null$ attributes() {
        return Null$.MODULE$;
    }

    public final Nil$ child() {
        return Nil$.MODULE$;
    }
}
