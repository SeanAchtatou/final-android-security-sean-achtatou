package scala.xml;

import java.io.Serializable;
import scala.runtime.AbstractFunction2$mcIII$sp;
import scala.runtime.BoxesRunTime;

/* compiled from: Equality.scala */
public final class Equality$$anonfun$hashCode$1 extends AbstractFunction2$mcIII$sp implements Serializable {
    public static final long serialVersionUID = 0;

    public Equality$$anonfun$hashCode$1(Equality $outer) {
    }

    public final int apply(int i, int i2) {
        return (i * 7) + i2;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1, Object v2) {
        return BoxesRunTime.boxToInteger(apply(BoxesRunTime.unboxToInt(v1), BoxesRunTime.unboxToInt(v2)));
    }

    public int apply$mcIII$sp(int v1, int v2) {
        return (v1 * 7) + v2;
    }
}
