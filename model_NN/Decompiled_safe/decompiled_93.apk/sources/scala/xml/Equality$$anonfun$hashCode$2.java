package scala.xml;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;
import scala.xml.Equality;

/* compiled from: Equality.scala */
public final class Equality$$anonfun$hashCode$2 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Equality $outer;

    public Equality$$anonfun$hashCode$2(Equality $outer2) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
    }

    public final int apply(Object obj) {
        return Equality.Cclass.scala$xml$Equality$$hashOf(this.$outer, obj);
    }
}
