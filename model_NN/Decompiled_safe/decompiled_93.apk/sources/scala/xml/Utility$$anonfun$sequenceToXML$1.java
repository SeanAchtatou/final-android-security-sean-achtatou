package scala.xml;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

/* compiled from: Utility.scala */
public final class Utility$$anonfun$sequenceToXML$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return BoxesRunTime.boxToBoolean(apply((Node) v1));
    }

    public final boolean apply(Node node) {
        return Utility$.MODULE$.isAtomAndNotText(node);
    }
}
