package scala.xml;

import java.io.Serializable;
import scala.Product;
import scala.ScalaObject;
import scala.collection.Iterator;
import scala.collection.mutable.StringBuilder;
import scala.runtime.BoxesRunTime;

/* compiled from: Comment.scala */
public class Comment extends SpecialNode implements Serializable, Product, ScalaObject {
    private final String commentText;

    public String commentText() {
        return this.commentText;
    }

    public int productArity() {
        return 1;
    }

    public Object productElement(int i) {
        if (i == 0) {
            return commentText();
        }
        throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(i).toString());
    }

    public Iterator<Object> productIterator() {
        return Product.Cclass.productIterator(this);
    }

    public String productPrefix() {
        return "Comment";
    }

    public String label() {
        return "#REM";
    }

    public String text() {
        return "";
    }

    public StringBuilder buildString(StringBuilder sb) {
        return sb.append(new StringBuilder().append((Object) "<!--").append((Object) commentText()).append((Object) "-->").toString());
    }
}
