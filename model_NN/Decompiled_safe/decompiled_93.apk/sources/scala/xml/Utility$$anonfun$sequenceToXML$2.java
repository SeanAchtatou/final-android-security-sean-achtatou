package scala.xml;

import java.io.Serializable;
import scala.collection.mutable.StringBuilder;
import scala.runtime.AbstractFunction1;

/* compiled from: Utility.scala */
public final class Utility$$anonfun$sequenceToXML$2 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ boolean decodeEntities$1;
    public final /* synthetic */ boolean minimizeTags$1;
    public final /* synthetic */ boolean preserveWhitespace$1;
    public final /* synthetic */ NamespaceBinding pscope$1;
    public final /* synthetic */ StringBuilder sb$1;
    public final /* synthetic */ boolean stripComments$1;

    public Utility$$anonfun$sequenceToXML$2(NamespaceBinding namespaceBinding, StringBuilder stringBuilder, boolean z, boolean z2, boolean z3, boolean z4) {
        this.pscope$1 = namespaceBinding;
        this.sb$1 = stringBuilder;
        this.stripComments$1 = z;
        this.decodeEntities$1 = z2;
        this.preserveWhitespace$1 = z3;
        this.minimizeTags$1 = z4;
    }

    public final StringBuilder apply(Node node) {
        return Utility$.MODULE$.toXML(node, this.pscope$1, this.sb$1, this.stripComments$1, this.decodeEntities$1, this.preserveWhitespace$1, this.minimizeTags$1);
    }
}
