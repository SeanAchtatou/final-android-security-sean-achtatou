package scala.xml;

import java.io.Serializable;
import scala.collection.Iterator;
import scala.runtime.AbstractFunction0;

/* compiled from: MetaData.scala */
public final class MetaData$$anonfun$iterator$1 extends AbstractFunction0 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ MetaData $outer;

    public MetaData$$anonfun$iterator$1(MetaData $outer2) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
    }

    public final Iterator<MetaData> apply() {
        return this.$outer.next().iterator();
    }
}
