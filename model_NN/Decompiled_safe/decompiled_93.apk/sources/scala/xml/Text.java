package scala.xml;

import scala.Product;
import scala.ScalaObject;
import scala.collection.Iterator;
import scala.collection.mutable.StringBuilder;
import scala.runtime.BoxesRunTime;

/* compiled from: Text.scala */
public class Text extends Atom<String> implements ScalaObject, Product, ScalaObject {
    public String _data() {
        return (String) super.data();
    }

    public int productArity() {
        return 1;
    }

    public Object productElement(int i) {
        if (i == 0) {
            return _data();
        }
        throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(i).toString());
    }

    public Iterator<Object> productIterator() {
        return Product.Cclass.productIterator(this);
    }

    public String productPrefix() {
        return "Text";
    }

    public StringBuilder buildString(StringBuilder sb) {
        return Utility$.MODULE$.escape((String) data(), sb);
    }
}
