package scala.xml;

import java.io.Serializable;
import scala.Predef$;
import scala.Product;
import scala.ScalaObject;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.immutable.StringLike;
import scala.collection.immutable.StringOps;
import scala.collection.mutable.StringBuilder;
import scala.runtime.BoxesRunTime;
import scala.xml.Equality;

/* compiled from: NamespaceBinding.scala */
public class NamespaceBinding implements Serializable, Product, ScalaObject, Equality {
    public static final long serialVersionUID = -2518644165573446725L;
    private final NamespaceBinding parent;
    private final String prefix;
    private final String uri;

    public NamespaceBinding(String prefix2, String uri2, NamespaceBinding parent2) {
        this.prefix = prefix2;
        this.uri = uri2;
        this.parent = parent2;
        Equality.Cclass.$init$(this);
        Product.Cclass.$init$(this);
        if (prefix2 == null) {
            if ("" != 0) {
                return;
            }
        } else if (!prefix2.equals("")) {
            return;
        }
        throw new IllegalArgumentException("zero length prefix not allowed");
    }

    public boolean equals(Object other) {
        return Equality.Cclass.equals(this, other);
    }

    public int hashCode() {
        return Equality.Cclass.hashCode(this);
    }

    public NamespaceBinding parent() {
        return this.parent;
    }

    public String prefix() {
        return this.prefix;
    }

    public int productArity() {
        return 3;
    }

    public Object productElement(int i) {
        switch (i) {
            case 0:
                return prefix();
            case 1:
                return uri();
            case 2:
                return parent();
            default:
                throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(i).toString());
        }
    }

    public Iterator<Object> productIterator() {
        return Product.Cclass.productIterator(this);
    }

    public String productPrefix() {
        return "NamespaceBinding";
    }

    public String uri() {
        return this.uri;
    }

    public String toString() {
        StringBuilder sb0 = new StringBuilder();
        buildString(sb0, TopScope$.MODULE$);
        return sb0.toString();
    }

    public boolean canEqual(Object other) {
        return other instanceof NamespaceBinding;
    }

    public boolean strict_$eq$eq(Equality other) {
        if (!(other instanceof NamespaceBinding)) {
            return false;
        }
        NamespaceBinding namespaceBinding = (NamespaceBinding) other;
        String prefix2 = prefix();
        String prefix3 = namespaceBinding.prefix();
        if (prefix2 != null ? prefix2.equals(prefix3) : prefix3 == null) {
            String uri2 = uri();
            String uri3 = namespaceBinding.uri();
            if (uri2 != null ? uri2.equals(uri3) : uri3 == null) {
                NamespaceBinding parent2 = parent();
                NamespaceBinding parent3 = namespaceBinding.parent();
                if (parent2 != null ? parent2.equals(parent3) : parent3 == null) {
                    return true;
                }
            }
        }
        return false;
    }

    public Seq<Object> basisForHashCode() {
        return Predef$.MODULE$.wrapRefArray(new Object[]{prefix(), uri(), parent()}).toList();
    }

    public void buildString(StringBuilder sb, NamespaceBinding stop) {
        if (this != stop) {
            StringOps stringOps = new StringOps(" xmlns%s=\"%s\"");
            Predef$ predef$ = Predef$.MODULE$;
            Object[] objArr = new Object[2];
            objArr[0] = prefix() == null ? "" : new StringBuilder().append((Object) ":").append((Object) prefix()).toString();
            objArr[1] = uri() == null ? "" : uri();
            parent().buildString(sb.append(StringLike.Cclass.format(stringOps, predef$.genericWrapArray(objArr))), stop);
        }
    }
}
