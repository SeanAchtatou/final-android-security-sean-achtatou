package scala.xml;

import scala.ScalaObject;
import scala.collection.mutable.StringBuilder;

/* compiled from: TopScope.scala */
public final class TopScope$ extends NamespaceBinding implements ScalaObject {
    public static final TopScope$ MODULE$ = null;

    static {
        new TopScope$();
    }

    private TopScope$() {
        super(null, null, null);
        MODULE$ = this;
    }

    public String toString() {
        return "";
    }

    public void buildString(StringBuilder sb, NamespaceBinding ignore) {
    }
}
