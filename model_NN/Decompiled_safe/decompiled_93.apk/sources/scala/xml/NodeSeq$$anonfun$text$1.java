package scala.xml;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;

/* compiled from: NodeSeq.scala */
public final class NodeSeq$$anonfun$text$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;

    public NodeSeq$$anonfun$text$1(NodeSeq $outer) {
    }

    public final String apply(Node node) {
        return node.text();
    }
}
