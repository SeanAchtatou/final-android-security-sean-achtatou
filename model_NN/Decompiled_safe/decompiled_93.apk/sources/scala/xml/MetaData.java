package scala.xml;

import scala.Function1;
import scala.Function2;
import scala.Predef$;
import scala.ScalaObject;
import scala.Tuple2;
import scala.collection.Iterable;
import scala.collection.IterableLike;
import scala.collection.Iterator;
import scala.collection.Iterator$$anon$4;
import scala.collection.Seq;
import scala.collection.Traversable;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericTraversableTemplate;
import scala.collection.immutable.List;
import scala.collection.immutable.Set;
import scala.collection.immutable.Stream;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.Builder;
import scala.collection.mutable.StringBuilder;
import scala.math.Numeric;
import scala.reflect.ClassManifest;
import scala.runtime.BoxesRunTime;
import scala.xml.Equality;

/* compiled from: MetaData.scala */
public abstract class MetaData implements Iterable<MetaData>, Equality, ScalaObject, Equality {
    public abstract MetaData copy(MetaData metaData);

    public abstract MetaData next();

    public abstract void toString1(StringBuilder stringBuilder);

    public MetaData() {
        TraversableOnce.Cclass.$init$(this);
        TraversableLike.Cclass.$init$(this);
        GenericTraversableTemplate.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        IterableLike.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Equality.Cclass.$init$(this);
    }

    public <B> B $div$colon(B z, Function2<B, MetaData, B> op) {
        return TraversableOnce.Cclass.$div$colon(this, z, op);
    }

    public <B, That> That $plus$plus(TraversableOnce<B> that, CanBuildFrom<Iterable<MetaData>, B, That> bf) {
        return TraversableLike.Cclass.$plus$plus(this, that, bf);
    }

    public StringBuilder addString(StringBuilder b, String start, String sep, String end) {
        return TraversableOnce.Cclass.addString(this, b, start, sep, end);
    }

    public GenericCompanion<Iterable> companion() {
        return Iterable.Cclass.companion(this);
    }

    public <B> void copyToArray(Object xs, int start) {
        TraversableOnce.Cclass.copyToArray(this, xs, start);
    }

    public <B> void copyToArray(Object xs, int start, int len) {
        IterableLike.Cclass.copyToArray(this, xs, start, len);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.Iterable<scala.xml.MetaData>] */
    public Iterable<MetaData> drop(int n) {
        return TraversableLike.Cclass.drop(this, n);
    }

    public boolean equals(Object other) {
        return Equality.Cclass.equals(this, other);
    }

    public boolean exists(Function1<MetaData, Boolean> p) {
        return IterableLike.Cclass.exists(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.Iterable<scala.xml.MetaData>] */
    public Iterable<MetaData> filterNot(Function1<MetaData, Boolean> p) {
        return TraversableLike.Cclass.filterNot(this, p);
    }

    public <B> B foldLeft(B z, Function2<B, MetaData, B> op) {
        return TraversableOnce.Cclass.foldLeft(this, z, op);
    }

    public boolean forall(Function1<MetaData, Boolean> p) {
        return IterableLike.Cclass.forall(this, p);
    }

    public <U> void foreach(Function1<MetaData, U> f) {
        IterableLike.Cclass.foreach(this, f);
    }

    public <B> Builder<B, Iterable<B>> genericBuilder() {
        return GenericTraversableTemplate.Cclass.genericBuilder(this);
    }

    public int hashCode() {
        return Equality.Cclass.hashCode(this);
    }

    public Object head() {
        return IterableLike.Cclass.head(this);
    }

    public boolean isEmpty() {
        return IterableLike.Cclass.isEmpty(this);
    }

    public final boolean isTraversableAgain() {
        return TraversableLike.Cclass.isTraversableAgain(this);
    }

    public Object last() {
        return TraversableLike.Cclass.last(this);
    }

    public <B, That> That map(Function1<MetaData, B> f, CanBuildFrom<Iterable<MetaData>, B, That> bf) {
        return TraversableLike.Cclass.map(this, f, bf);
    }

    public String mkString() {
        return TraversableOnce.Cclass.mkString(this);
    }

    public String mkString(String sep) {
        return TraversableOnce.Cclass.mkString(this, sep);
    }

    public String mkString(String start, String sep, String end) {
        return TraversableOnce.Cclass.mkString(this, start, sep, end);
    }

    public Builder<MetaData, Iterable<MetaData>> newBuilder() {
        return GenericTraversableTemplate.Cclass.newBuilder(this);
    }

    public boolean nonEmpty() {
        return TraversableOnce.Cclass.nonEmpty(this);
    }

    public <B> B reduceLeft(Function2<B, MetaData, B> op) {
        return TraversableOnce.Cclass.reduceLeft(this, op);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.Iterable<scala.xml.MetaData>] */
    public Iterable<MetaData> repr() {
        return TraversableLike.Cclass.repr(this);
    }

    public <B> boolean sameElements(Iterable<B> that) {
        return IterableLike.Cclass.sameElements(this, that);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.Iterable<scala.xml.MetaData>] */
    public Iterable<MetaData> slice(int from, int until) {
        return IterableLike.Cclass.slice(this, from, until);
    }

    public String stringPrefix() {
        return TraversableLike.Cclass.stringPrefix(this);
    }

    public <B> B sum(Numeric<B> num) {
        return TraversableOnce.Cclass.sum(this, num);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.Iterable<scala.xml.MetaData>] */
    public Iterable<MetaData> tail() {
        return TraversableLike.Cclass.tail(this);
    }

    public Iterable<MetaData> thisCollection() {
        return IterableLike.Cclass.thisCollection(this);
    }

    public <B> Object toArray(ClassManifest<B> evidence$1) {
        return TraversableOnce.Cclass.toArray(this, evidence$1);
    }

    public <B> Buffer<B> toBuffer() {
        return TraversableOnce.Cclass.toBuffer(this);
    }

    public List<MetaData> toList() {
        return TraversableOnce.Cclass.toList(this);
    }

    public <B> Set<B> toSet() {
        return TraversableOnce.Cclass.toSet(this);
    }

    public Stream<MetaData> toStream() {
        return IterableLike.Cclass.toStream(this);
    }

    public <A1, B, That> That zip(Iterable<B> that, CanBuildFrom<Iterable<MetaData>, Tuple2<A1, B>, That> bf) {
        return IterableLike.Cclass.zip(this, that, bf);
    }

    public int length() {
        return length(0);
    }

    public int length(int i) {
        return next().length(i + 1);
    }

    public boolean canEqual(Object other) {
        return other instanceof MetaData;
    }

    public boolean strict_$eq$eq(Equality other) {
        if (!(other instanceof MetaData)) {
            return false;
        }
        Set set = toSet();
        Set set2 = ((MetaData) other).toSet();
        return set != null ? set.equals(set2) : set2 == null;
    }

    public Seq<Object> basisForHashCode() {
        return Predef$.MODULE$.wrapRefArray((Object[]) new Set[]{toSet()}).toList();
    }

    public Iterator<MetaData> iterator() {
        return Iterator.Cclass.$plus$plus(new Iterator$$anon$4(this), new MetaData$$anonfun$iterator$1(this));
    }

    public int size() {
        return iterator().length() + 1;
    }

    public MetaData filter(Function1<MetaData, Boolean> f) {
        if (BoxesRunTime.unboxToBoolean(f.apply(this))) {
            return copy(next().filter(f));
        }
        return next().filter(f);
    }

    public String toString() {
        StringBuilder sb0 = new StringBuilder();
        buildString(sb0);
        return sb0.toString();
    }

    public StringBuilder buildString(StringBuilder stringBuilder) {
        stringBuilder.append(' ');
        toString1(stringBuilder);
        return next().buildString(stringBuilder);
    }
}
