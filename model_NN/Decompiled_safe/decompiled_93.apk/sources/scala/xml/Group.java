package scala.xml;

import java.io.Serializable;
import scala.Predef$;
import scala.Product;
import scala.ScalaObject;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.immutable.StringLike;
import scala.collection.immutable.StringOps;
import scala.runtime.BoxesRunTime;
import scala.runtime.Nothing$;

/* compiled from: Group.scala */
public final class Group extends Node implements Serializable, Product, ScalaObject {
    public final Seq<Node> nodes;

    public int productArity() {
        return 1;
    }

    public Object productElement(int i) {
        if (i == 0) {
            return this.nodes;
        }
        throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(i).toString());
    }

    public Iterator<Object> productIterator() {
        return Product.Cclass.productIterator(this);
    }

    public String productPrefix() {
        return "Group";
    }

    public Seq<Node> theSeq() {
        return this.nodes;
    }

    public boolean canEqual(Object other) {
        return other instanceof Group;
    }

    public boolean strict_$eq$eq(Equality other) {
        if (other instanceof Group) {
            return this.nodes.sameElements(((Group) other).nodes);
        }
        return false;
    }

    public Seq<Node> basisForHashCode() {
        return this.nodes;
    }

    private Nothing$ fail(String msg) {
        throw new UnsupportedOperationException(StringLike.Cclass.format(new StringOps("class Group does not support method '%s'"), Predef$.MODULE$.genericWrapArray(new Object[]{msg})));
    }

    public Nothing$ label() {
        return fail("label");
    }

    public Nothing$ attributes() {
        return fail("attributes");
    }

    public Nothing$ child() {
        return fail("child");
    }
}
