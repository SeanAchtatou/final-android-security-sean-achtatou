package scala.xml;

import java.io.Serializable;
import scala.Function1;
import scala.Product;
import scala.ScalaObject;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.immutable.Nil$;
import scala.collection.mutable.StringBuilder;
import scala.package$;
import scala.runtime.BoxesRunTime;

/* compiled from: Null.scala */
public final class Null$ extends MetaData implements Serializable, Product, ScalaObject {
    public static final Null$ MODULE$ = null;

    static {
        new Null$();
    }

    private Null$() {
        MODULE$ = this;
        Product.Cclass.$init$(this);
    }

    public int productArity() {
        return 0;
    }

    public Object productElement(int i) {
        throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(i).toString());
    }

    public Iterator<Object> productIterator() {
        return Product.Cclass.productIterator(this);
    }

    public String productPrefix() {
        return "Null";
    }

    public Iterator iterator() {
        return package$.MODULE$.Iterator().empty();
    }

    public MetaData filter(Function1<MetaData, Boolean> f) {
        return this;
    }

    public MetaData copy(MetaData next) {
        return next;
    }

    public scala.runtime.Null$ next() {
        return null;
    }

    public int length() {
        return 0;
    }

    public int length(int i) {
        return i;
    }

    public boolean strict_$eq$eq(Equality other) {
        if (!(other instanceof MetaData)) {
            return false;
        }
        if (((MetaData) other).length() == 0) {
            return true;
        }
        return false;
    }

    public Seq<Object> basisForHashCode() {
        return Nil$.MODULE$;
    }

    public void toString1(StringBuilder sb) {
    }

    public String toString() {
        return "";
    }

    public StringBuilder buildString(StringBuilder sb) {
        return sb;
    }
}
