package scala.xml;

import scala.ScalaObject;
import scala.collection.Seq;
import scala.collection.immutable.Nil$;
import scala.collection.mutable.StringBuilder;
import scala.runtime.BoxedUnit;

/* compiled from: Node.scala */
public abstract class Node extends NodeSeq implements ScalaObject {
    public abstract Seq<Node> child();

    public abstract String label();

    public String prefix() {
        return null;
    }

    public boolean isAtom() {
        return this instanceof Atom;
    }

    public NamespaceBinding scope() {
        return TopScope$.MODULE$;
    }

    public MetaData attributes() {
        return Null$.MODULE$;
    }

    public Seq<Node> nonEmptyChildren() {
        return (Seq) child().filterNot(new Node$$anonfun$nonEmptyChildren$1(this));
    }

    public boolean canEqual(Object other) {
        if (other instanceof Group) {
            return false;
        }
        return other instanceof Node;
    }

    public Seq<Object> basisForHashCode() {
        return nonEmptyChildren().toList().$colon$colon(attributes()).$colon$colon(label()).$colon$colon(prefix());
    }

    public boolean strict_$eq$eq(Equality other) {
        if (other instanceof Group) {
            return false;
        }
        if (!(other instanceof Node)) {
            return false;
        }
        Node node = (Node) other;
        String prefix = prefix();
        String prefix2 = node.prefix();
        if (prefix != null ? prefix.equals(prefix2) : prefix2 == null) {
            String label = label();
            String label2 = node.label();
            if (label != null ? label.equals(label2) : label2 == null) {
                MetaData attributes = attributes();
                MetaData attributes2 = node.attributes();
                if (attributes != null ? attributes.equals(attributes2) : attributes2 == null) {
                    if (nonEmptyChildren().sameElements(node.nonEmptyChildren())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public Seq<Node> theSeq() {
        return Nil$.MODULE$.$colon$colon(this);
    }

    public String buildString(boolean stripComments) {
        return Utility$.MODULE$.toXML(this, TopScope$.MODULE$, new StringBuilder(), stripComments, true, false, false).toString();
    }

    public String toString() {
        return buildString(false);
    }

    public StringBuilder nameToString(StringBuilder sb) {
        if (prefix() == null) {
            BoxedUnit boxedUnit = BoxedUnit.UNIT;
        } else {
            sb.append(prefix());
            sb.append(':');
        }
        return sb.append(label());
    }

    public String text() {
        return super.text();
    }
}
