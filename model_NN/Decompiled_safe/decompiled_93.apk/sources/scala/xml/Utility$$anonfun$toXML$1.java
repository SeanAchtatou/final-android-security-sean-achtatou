package scala.xml;

import java.io.Serializable;
import scala.collection.mutable.StringBuilder;
import scala.runtime.AbstractFunction1;

/* compiled from: Utility.scala */
public final class Utility$$anonfun$toXML$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ boolean decodeEntities$2;
    public final /* synthetic */ boolean minimizeTags$2;
    public final /* synthetic */ boolean preserveWhitespace$2;
    public final /* synthetic */ StringBuilder sb$2;
    public final /* synthetic */ boolean stripComments$2;
    public final /* synthetic */ Node x$8;

    public Utility$$anonfun$toXML$1(Node node, StringBuilder stringBuilder, boolean z, boolean z2, boolean z3, boolean z4) {
        this.x$8 = node;
        this.sb$2 = stringBuilder;
        this.stripComments$2 = z;
        this.decodeEntities$2 = z2;
        this.preserveWhitespace$2 = z3;
        this.minimizeTags$2 = z4;
    }

    public final StringBuilder apply(Node node) {
        return Utility$.MODULE$.toXML(node, this.x$8.scope(), this.sb$2, this.stripComments$2, this.decodeEntities$2, this.preserveWhitespace$2, this.minimizeTags$2);
    }
}
