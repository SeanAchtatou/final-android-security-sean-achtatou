package scala.xml;

import scala.Function1;
import scala.Function2;
import scala.PartialFunction;
import scala.ScalaObject;
import scala.Tuple2;
import scala.collection.Iterable;
import scala.collection.IterableLike;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.SeqLike;
import scala.collection.Traversable;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericTraversableTemplate;
import scala.collection.generic.TraversableFactory;
import scala.collection.immutable.Iterable;
import scala.collection.immutable.List;
import scala.collection.immutable.Seq;
import scala.collection.immutable.Seq$;
import scala.collection.immutable.Set;
import scala.collection.immutable.Stream;
import scala.collection.immutable.Traversable;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.Builder;
import scala.collection.mutable.ListBuffer;
import scala.collection.mutable.StringBuilder;
import scala.math.Numeric;
import scala.math.Ordering;
import scala.reflect.ClassManifest;
import scala.runtime.BoxesRunTime;
import scala.xml.Equality;

/* compiled from: NodeSeq.scala */
public abstract class NodeSeq implements Seq<Node>, SeqLike<Node, NodeSeq>, Equality, ScalaObject {
    public abstract scala.collection.Seq<Node> theSeq();

    public NodeSeq() {
        TraversableOnce.Cclass.$init$(this);
        TraversableLike.Cclass.$init$(this);
        GenericTraversableTemplate.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        IterableLike.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Function1.Cclass.$init$(this);
        PartialFunction.Cclass.$init$(this);
        SeqLike.Cclass.$init$(this);
        Seq.Cclass.$init$(this);
        Seq.Cclass.$init$(this);
        Equality.Cclass.$init$(this);
    }

    public <B> B $div$colon(B z, Function2<B, Node, B> op) {
        return TraversableOnce.Cclass.$div$colon(this, z, op);
    }

    public <B, That> That $plus$plus(TraversableOnce<B> that, CanBuildFrom<NodeSeq, B, That> bf) {
        return TraversableLike.Cclass.$plus$plus(this, that, bf);
    }

    public StringBuilder addString(StringBuilder b, String start, String sep, String end) {
        return TraversableOnce.Cclass.addString(this, b, start, sep, end);
    }

    public <C> PartialFunction<Integer, C> andThen(Function1<Node, C> k) {
        return PartialFunction.Cclass.andThen(this, k);
    }

    public /* bridge */ /* synthetic */ Object apply(Object v1) {
        return apply(BoxesRunTime.unboxToInt(v1));
    }

    public int apply$mcII$sp(int v1) {
        return Function1.Cclass.apply$mcII$sp(this, v1);
    }

    public void apply$mcVI$sp(int v1) {
        Function1.Cclass.apply$mcVI$sp(this, v1);
    }

    public void apply$mcVL$sp(long v1) {
        Function1.Cclass.apply$mcVL$sp(this, v1);
    }

    public GenericCompanion<scala.collection.immutable.Seq> companion() {
        return Seq.Cclass.companion(this);
    }

    public <A> Function1<A, Node> compose(Function1<A, Integer> g) {
        return Function1.Cclass.compose(this, g);
    }

    public boolean contains(Object elem) {
        return SeqLike.Cclass.contains(this, elem);
    }

    public <B> void copyToArray(Object xs, int start) {
        TraversableOnce.Cclass.copyToArray(this, xs, start);
    }

    public <B> void copyToArray(Object xs, int start, int len) {
        IterableLike.Cclass.copyToArray(this, xs, start, len);
    }

    public Object drop(int n) {
        return TraversableLike.Cclass.drop(this, n);
    }

    public boolean equals(Object other) {
        return Equality.Cclass.equals(this, other);
    }

    public boolean exists(Function1<Node, Boolean> p) {
        return IterableLike.Cclass.exists(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.xml.NodeSeq, java.lang.Object] */
    public NodeSeq filter(Function1<Node, Boolean> p) {
        return TraversableLike.Cclass.filter(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.xml.NodeSeq, java.lang.Object] */
    public NodeSeq filterNot(Function1<Node, Boolean> p) {
        return TraversableLike.Cclass.filterNot(this, p);
    }

    public <B> B foldLeft(B z, Function2<B, Node, B> op) {
        return TraversableOnce.Cclass.foldLeft(this, z, op);
    }

    public boolean forall(Function1<Node, Boolean> p) {
        return IterableLike.Cclass.forall(this, p);
    }

    public <U> void foreach(Function1<Node, U> f) {
        IterableLike.Cclass.foreach(this, f);
    }

    public <B> Builder<B, scala.collection.immutable.Seq<B>> genericBuilder() {
        return GenericTraversableTemplate.Cclass.genericBuilder(this);
    }

    public int hashCode() {
        return Equality.Cclass.hashCode(this);
    }

    public Object head() {
        return IterableLike.Cclass.head(this);
    }

    public <B> int indexOf(B elem) {
        return SeqLike.Cclass.indexOf(this, elem);
    }

    public <B> int indexOf(B elem, int from) {
        return SeqLike.Cclass.indexOf(this, elem, from);
    }

    public int indexWhere(Function1<Node, Boolean> p, int from) {
        return SeqLike.Cclass.indexWhere(this, p, from);
    }

    public boolean isDefinedAt(int idx) {
        return SeqLike.Cclass.isDefinedAt(this, idx);
    }

    public /* bridge */ /* synthetic */ boolean isDefinedAt(Object x) {
        return isDefinedAt(BoxesRunTime.unboxToInt(x));
    }

    public boolean isEmpty() {
        return IterableLike.Cclass.isEmpty(this);
    }

    public final boolean isTraversableAgain() {
        return TraversableLike.Cclass.isTraversableAgain(this);
    }

    public Object last() {
        return TraversableLike.Cclass.last(this);
    }

    public <B, That> That map(Function1<Node, B> f, CanBuildFrom<NodeSeq, B, That> bf) {
        return TraversableLike.Cclass.map(this, f, bf);
    }

    public String mkString() {
        return TraversableOnce.Cclass.mkString(this);
    }

    public String mkString(String sep) {
        return TraversableOnce.Cclass.mkString(this, sep);
    }

    public String mkString(String start, String sep, String end) {
        return TraversableOnce.Cclass.mkString(this, start, sep, end);
    }

    public boolean nonEmpty() {
        return TraversableOnce.Cclass.nonEmpty(this);
    }

    public int prefixLength(Function1<Node, Boolean> p) {
        return SeqLike.Cclass.prefixLength(this, p);
    }

    public <B> B reduceLeft(Function2<B, Node, B> op) {
        return TraversableOnce.Cclass.reduceLeft(this, op);
    }

    public Object repr() {
        return TraversableLike.Cclass.repr(this);
    }

    public Object reverse() {
        return SeqLike.Cclass.reverse(this);
    }

    public Iterator<Node> reverseIterator() {
        return SeqLike.Cclass.reverseIterator(this);
    }

    public <B> boolean sameElements(scala.collection.Iterable<B> that) {
        return IterableLike.Cclass.sameElements(this, that);
    }

    public int segmentLength(Function1<Node, Boolean> p, int from) {
        return SeqLike.Cclass.segmentLength(this, p, from);
    }

    public int size() {
        return SeqLike.Cclass.size(this);
    }

    public Object slice(int from, int until) {
        return IterableLike.Cclass.slice(this, from, until);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.xml.NodeSeq, java.lang.Object] */
    public <B> NodeSeq sortBy(Function1<Node, B> f, Ordering<B> ord) {
        return SeqLike.Cclass.sortBy(this, f, ord);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.xml.NodeSeq, java.lang.Object] */
    public <B> NodeSeq sorted(Ordering<B> ord) {
        return SeqLike.Cclass.sorted(this, ord);
    }

    public String stringPrefix() {
        return TraversableLike.Cclass.stringPrefix(this);
    }

    public <B> B sum(Numeric<B> num) {
        return TraversableOnce.Cclass.sum(this, num);
    }

    public Object tail() {
        return TraversableLike.Cclass.tail(this);
    }

    public scala.collection.Seq<Node> thisCollection() {
        return SeqLike.Cclass.thisCollection(this);
    }

    public <B> Object toArray(ClassManifest<B> evidence$1) {
        return TraversableOnce.Cclass.toArray(this, evidence$1);
    }

    public <B> Buffer<B> toBuffer() {
        return TraversableOnce.Cclass.toBuffer(this);
    }

    public scala.collection.Seq<Node> toCollection(NodeSeq repr) {
        return SeqLike.Cclass.toCollection(this, repr);
    }

    public List<Node> toList() {
        return TraversableOnce.Cclass.toList(this);
    }

    public <B> Set<B> toSet() {
        return TraversableOnce.Cclass.toSet(this);
    }

    public Stream<Node> toStream() {
        return IterableLike.Cclass.toStream(this);
    }

    public <A1, B, That> That zip(scala.collection.Iterable<B> that, CanBuildFrom<NodeSeq, Tuple2<A1, B>, That> bf) {
        return IterableLike.Cclass.zip(this, that, bf);
    }

    public Builder<Node, NodeSeq> newBuilder() {
        return Builder.Cclass.mapResult(new ListBuffer(), new NodeSeq$$anonfun$newBuilder$1());
    }

    public int length() {
        return theSeq().length();
    }

    public Iterator<Node> iterator() {
        return theSeq().iterator();
    }

    public Node apply(int i) {
        return theSeq().apply(i);
    }

    public scala.collection.Seq<Object> basisForHashCode() {
        return theSeq();
    }

    public boolean canEqual(Object other) {
        return other instanceof NodeSeq;
    }

    public boolean strict_$eq$eq(Equality other) {
        if (!(other instanceof NodeSeq)) {
            return false;
        }
        NodeSeq nodeSeq = (NodeSeq) other;
        if (length() != nodeSeq.length() || !theSeq().sameElements(nodeSeq.theSeq())) {
            return false;
        }
        return true;
    }

    public String toString() {
        return theSeq().mkString();
    }

    public String text() {
        return ((TraversableOnce) map(new NodeSeq$$anonfun$text$1(this), new TraversableFactory.GenericCanBuildFrom(Seq$.MODULE$))).mkString();
    }
}
