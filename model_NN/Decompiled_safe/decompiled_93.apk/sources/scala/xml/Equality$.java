package scala.xml;

import scala.ScalaObject;

/* compiled from: Equality.scala */
public final class Equality$ implements ScalaObject {
    public static final Equality$ MODULE$ = null;

    static {
        new Equality$();
    }

    private Equality$() {
        MODULE$ = this;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        return true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean compareBlithely(java.lang.Object r4, java.lang.String r5) {
        /*
            r3 = this;
            r2 = 1
            r1 = 0
            boolean r0 = r4 instanceof scala.xml.Atom
            if (r0 == 0) goto L_0x001a
            scala.xml.Atom r4 = (scala.xml.Atom) r4
            java.lang.Object r0 = r4.data()
            if (r0 != 0) goto L_0x0012
            if (r5 == 0) goto L_0x0018
        L_0x0010:
            r0 = r1
        L_0x0011:
            return r0
        L_0x0012:
            boolean r0 = r0.equals(r5)
            if (r0 == 0) goto L_0x0010
        L_0x0018:
            r0 = r2
            goto L_0x0011
        L_0x001a:
            boolean r0 = r4 instanceof scala.xml.NodeSeq
            if (r0 == 0) goto L_0x0032
            scala.xml.NodeSeq r4 = (scala.xml.NodeSeq) r4
            java.lang.String r0 = r4.text()
            if (r0 != 0) goto L_0x002a
            if (r5 == 0) goto L_0x0030
        L_0x0028:
            r0 = r1
            goto L_0x0011
        L_0x002a:
            boolean r0 = r0.equals(r5)
            if (r0 == 0) goto L_0x0028
        L_0x0030:
            r0 = r2
            goto L_0x0011
        L_0x0032:
            r0 = r1
            goto L_0x0011
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.xml.Equality$.compareBlithely(java.lang.Object, java.lang.String):boolean");
    }

    public boolean compareBlithely(Object x1, Node x2) {
        if (!(x1 instanceof NodeSeq)) {
            return false;
        }
        NodeSeq nodeSeq = (NodeSeq) x1;
        if (!gd1$1(nodeSeq)) {
            return false;
        }
        Node apply = nodeSeq.apply(0);
        return x2 != null ? x2.equals(apply) : apply == null;
    }

    private final /* synthetic */ boolean gd1$1(NodeSeq nodeSeq) {
        return nodeSeq.length() == 1;
    }

    public boolean compareBlithely(Object x1, Object x2) {
        if (x1 == null || x2 == null) {
            return x1 == x2;
        }
        if (x2 instanceof String) {
            return compareBlithely(x1, (String) x2);
        }
        if (x2 instanceof Node) {
            return compareBlithely(x1, (Node) x2);
        }
        return false;
    }
}
