package scala.xml;

import scala.ScalaObject;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.mutable.StringBuilder;
import scala.runtime.BoxedUnit;
import scala.xml.parsing.TokenTests;

/* compiled from: Utility.scala */
public final class Utility$ implements ScalaObject, TokenTests {
    public static final Utility$ MODULE$ = null;

    static {
        new Utility$();
    }

    private Utility$() {
        MODULE$ = this;
        TokenTests.Cclass.$init$(this);
    }

    public boolean isAtomAndNotText(Node x) {
        return x.isAtom() && !(x instanceof Text);
    }

    public final StringBuilder escape(String str, StringBuilder stringBuilder) {
        int length = str.length();
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            switch (charAt) {
                case 9:
                    stringBuilder.append(9);
                    break;
                case 10:
                    stringBuilder.append(10);
                    break;
                case 13:
                    stringBuilder.append(13);
                    break;
                case '\"':
                    stringBuilder.append("&quot;");
                    break;
                case '&':
                    stringBuilder.append("&amp;");
                    break;
                case '<':
                    stringBuilder.append("&lt;");
                    break;
                case '>':
                    stringBuilder.append("&gt;");
                    break;
                default:
                    if (charAt < ' ') {
                        BoxedUnit boxedUnit = BoxedUnit.UNIT;
                        break;
                    } else {
                        stringBuilder.append(charAt);
                        break;
                    }
            }
        }
        return stringBuilder;
    }

    public StringBuilder toXML(Node node, NamespaceBinding namespaceBinding, StringBuilder stringBuilder, boolean z, boolean z2, boolean z3, boolean z4) {
        if (node instanceof Comment) {
            return z ? stringBuilder : ((Comment) node).buildString(stringBuilder);
        }
        if (node instanceof SpecialNode) {
            return ((SpecialNode) node).buildString(stringBuilder);
        }
        if (node instanceof Group) {
            ((Group) node).nodes.foreach(new Utility$$anonfun$toXML$1(node, stringBuilder, z, z2, z3, z4));
            return stringBuilder;
        }
        stringBuilder.append('<');
        node.nameToString(stringBuilder);
        if (node.attributes() != null) {
            node.attributes().buildString(stringBuilder);
        } else {
            BoxedUnit boxedUnit = BoxedUnit.UNIT;
        }
        node.scope().buildString(stringBuilder, namespaceBinding);
        if (node.child().isEmpty() && z4) {
            return stringBuilder.append(" />");
        }
        stringBuilder.append('>');
        sequenceToXML(node.child(), node.scope(), stringBuilder, z, z2, z3, z4);
        stringBuilder.append("</");
        node.nameToString(stringBuilder);
        return stringBuilder.append('>');
    }

    public void sequenceToXML(Seq<Node> seq, NamespaceBinding namespaceBinding, StringBuilder stringBuilder, boolean z, boolean z2, boolean z3, boolean z4) {
        if (!seq.isEmpty()) {
            if (seq.forall(new Utility$$anonfun$sequenceToXML$1())) {
                Iterator<Node> it = seq.iterator();
                toXML(it.next(), namespaceBinding, stringBuilder, z, z2, z3, z4);
                while (it.hasNext()) {
                    stringBuilder.append(' ');
                    toXML(it.next(), namespaceBinding, stringBuilder, z, z2, z3, z4);
                }
                return;
            }
            seq.foreach(new Utility$$anonfun$sequenceToXML$2(namespaceBinding, stringBuilder, z, z2, z3, z4));
        }
    }
}
