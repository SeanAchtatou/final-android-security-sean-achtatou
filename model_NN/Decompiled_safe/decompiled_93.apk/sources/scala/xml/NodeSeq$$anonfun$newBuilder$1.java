package scala.xml;

import java.io.Serializable;
import scala.collection.Seq;
import scala.runtime.AbstractFunction1;

/* compiled from: NodeSeq.scala */
public final class NodeSeq$$anonfun$newBuilder$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return apply((Seq<Node>) ((Seq) v1));
    }

    public final NodeSeq apply(Seq<Node> seq) {
        return new NodeSeq$$anon$1(seq);
    }
}
