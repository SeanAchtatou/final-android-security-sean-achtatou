package scala;

import scala.Function1;
import scala.PartialFunction;

/* compiled from: PartialFunction.scala */
public final class PartialFunction$$anon$2 implements PartialFunction<A, Object> {
    private final /* synthetic */ PartialFunction $outer;
    private final /* synthetic */ Function1 k$1;

    public PartialFunction$$anon$2(PartialFunction $outer2, PartialFunction<A, B> partialFunction) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.k$1 = partialFunction;
        Function1.Cclass.$init$(this);
        PartialFunction.Cclass.$init$(this);
    }

    public <C> PartialFunction<A, C> andThen(Function1<C, C> k) {
        return PartialFunction.Cclass.andThen(this, k);
    }

    public int apply$mcII$sp(int v1) {
        return Function1.Cclass.apply$mcII$sp(this, v1);
    }

    public void apply$mcVI$sp(int v1) {
        Function1.Cclass.apply$mcVI$sp(this, v1);
    }

    public void apply$mcVL$sp(long v1) {
        Function1.Cclass.apply$mcVL$sp(this, v1);
    }

    public <A> Function1<A, C> compose(Function1<A, A> g) {
        return Function1.Cclass.compose(this, g);
    }

    public String toString() {
        return Function1.Cclass.toString(this);
    }

    public boolean isDefinedAt(A x) {
        return this.$outer.isDefinedAt(x);
    }

    public C apply(A x) {
        return this.k$1.apply(this.$outer.apply(x));
    }
}
