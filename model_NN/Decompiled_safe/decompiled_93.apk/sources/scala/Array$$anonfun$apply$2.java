package scala;

import java.io.Serializable;
import scala.runtime.AbstractFunction1$mcVL$sp;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.IntRef;

/* compiled from: Array.scala */
public final class Array$$anonfun$apply$2 extends AbstractFunction1$mcVL$sp implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ long[] array$7;
    public final /* synthetic */ IntRef i$7;

    public Array$$anonfun$apply$2(long[] jArr, IntRef intRef) {
        this.array$7 = jArr;
        this.i$7 = intRef;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        apply(BoxesRunTime.unboxToLong(v1));
        return BoxedUnit.UNIT;
    }

    public final void apply(long x) {
        this.array$7[this.i$7.elem] = x;
        this.i$7.elem++;
    }

    public void apply$mcVL$sp(long v1) {
        this.array$7[this.i$7.elem] = v1;
        this.i$7.elem++;
    }
}
