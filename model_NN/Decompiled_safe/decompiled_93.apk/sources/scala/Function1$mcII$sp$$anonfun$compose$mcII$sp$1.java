package scala;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

/* compiled from: Function1.scala */
public final class Function1$mcII$sp$$anonfun$compose$mcII$sp$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Function1$mcII$sp $outer;
    public final /* synthetic */ Function1 g$7;

    public Function1$mcII$sp$$anonfun$compose$mcII$sp$1(Function1$mcII$sp $outer2, Function1 function1) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.g$7 = function1;
    }

    public final int apply(A x) {
        return this.$outer.apply(BoxesRunTime.unboxToInt(this.g$7.apply(x)));
    }
}
