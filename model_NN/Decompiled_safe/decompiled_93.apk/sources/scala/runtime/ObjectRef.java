package scala.runtime;

import java.io.Serializable;

public class ObjectRef implements Serializable {
    public Object elem;

    public ObjectRef(Object obj) {
        this.elem = obj;
    }

    public String toString() {
        return "" + this.elem;
    }
}
