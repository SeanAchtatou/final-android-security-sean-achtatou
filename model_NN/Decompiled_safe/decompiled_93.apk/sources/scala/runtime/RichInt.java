package scala.runtime;

import scala.Proxy;
import scala.ScalaObject;
import scala.collection.immutable.Range;
import scala.collection.immutable.Range$$anon$1;
import scala.collection.immutable.Range$$anon$2;
import scala.math.Ordered;

/* compiled from: RichInt.scala */
public final class RichInt implements Proxy, Ordered<Integer>, ScalaObject {
    public final int start;

    public RichInt(int start2) {
        this.start = start2;
        Proxy.Cclass.$init$(this);
        Ordered.Cclass.$init$(this);
    }

    public boolean $less(Object that) {
        return Ordered.Cclass.$less(this, that);
    }

    public /* bridge */ /* synthetic */ int compare(Object that) {
        return compare(BoxesRunTime.unboxToInt(that));
    }

    public int compareTo(Object that) {
        return Ordered.Cclass.compareTo(this, that);
    }

    public boolean equals(Object that) {
        return Proxy.Cclass.equals(this, that);
    }

    public int hashCode() {
        return Proxy.Cclass.hashCode(this);
    }

    public String toString() {
        return Proxy.Cclass.toString(this);
    }

    public Object self() {
        return BoxesRunTime.boxToInteger(this.start);
    }

    public int compare(int that) {
        if (this.start < that) {
            return -1;
        }
        return this.start > that ? 1 : 0;
    }

    public Range.ByOne until(int end) {
        return new Range$$anon$2(this.start, end);
    }

    public Range.Inclusive to(int end) {
        return new Range$$anon$1(this.start, end);
    }

    public int min(int that) {
        return this.start < that ? this.start : that;
    }

    public int max(int that) {
        return this.start > that ? this.start : that;
    }
}
