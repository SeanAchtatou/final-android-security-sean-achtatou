package scala.runtime;

class ArrayRuntime {
    ArrayRuntime() {
    }

    static byte[] cloneArray(byte[] bArr) {
        return (byte[]) bArr.clone();
    }

    static char[] cloneArray(char[] cArr) {
        return (char[]) cArr.clone();
    }

    static double[] cloneArray(double[] dArr) {
        return (double[]) dArr.clone();
    }

    static float[] cloneArray(float[] fArr) {
        return (float[]) fArr.clone();
    }

    static int[] cloneArray(int[] iArr) {
        return (int[]) iArr.clone();
    }

    static long[] cloneArray(long[] jArr) {
        return (long[]) jArr.clone();
    }

    static Object[] cloneArray(Object[] objArr) {
        return (Object[]) objArr.clone();
    }

    static short[] cloneArray(short[] sArr) {
        return (short[]) sArr.clone();
    }

    static boolean[] cloneArray(boolean[] zArr) {
        return (boolean[]) zArr.clone();
    }
}
