package scala.runtime;

import scala.MatchError;
import scala.Product;
import scala.ScalaObject;
import scala.collection.mutable.StringBuilder;

/* compiled from: ScalaRunTime.scala */
public final class ScalaRunTime$ implements ScalaObject {
    public static final ScalaRunTime$ MODULE$ = null;

    static {
        new ScalaRunTime$();
    }

    private ScalaRunTime$() {
        MODULE$ = this;
    }

    public Object array_apply(Object xs, int idx) {
        if (xs instanceof Object[]) {
            return ((Object[]) xs)[idx];
        }
        if (xs instanceof int[]) {
            return BoxesRunTime.boxToInteger(((int[]) xs)[idx]);
        }
        if (xs instanceof double[]) {
            return BoxesRunTime.boxToDouble(((double[]) xs)[idx]);
        }
        if (xs instanceof long[]) {
            return BoxesRunTime.boxToLong(((long[]) xs)[idx]);
        }
        if (xs instanceof float[]) {
            return BoxesRunTime.boxToFloat(((float[]) xs)[idx]);
        }
        if (xs instanceof char[]) {
            return BoxesRunTime.boxToCharacter(((char[]) xs)[idx]);
        }
        if (xs instanceof byte[]) {
            return BoxesRunTime.boxToByte(((byte[]) xs)[idx]);
        }
        if (xs instanceof short[]) {
            return BoxesRunTime.boxToShort(((short[]) xs)[idx]);
        }
        if (xs instanceof boolean[]) {
            return BoxesRunTime.boxToBoolean(((boolean[]) xs)[idx]);
        }
        if (xs instanceof BoxedUnit[]) {
            return ((BoxedUnit[]) xs)[idx];
        }
        if (xs == null) {
            throw new NullPointerException();
        }
        throw new MatchError(xs);
    }

    public void array_update(Object xs, int idx, Object value) {
        if (xs instanceof Object[]) {
            ((Object[]) xs)[idx] = value;
        } else if (xs instanceof int[]) {
            ((int[]) xs)[idx] = BoxesRunTime.unboxToInt(value);
        } else if (xs instanceof double[]) {
            ((double[]) xs)[idx] = BoxesRunTime.unboxToDouble(value);
        } else if (xs instanceof long[]) {
            ((long[]) xs)[idx] = BoxesRunTime.unboxToLong(value);
        } else if (xs instanceof float[]) {
            ((float[]) xs)[idx] = BoxesRunTime.unboxToFloat(value);
        } else if (xs instanceof char[]) {
            ((char[]) xs)[idx] = BoxesRunTime.unboxToChar(value);
        } else if (xs instanceof byte[]) {
            ((byte[]) xs)[idx] = BoxesRunTime.unboxToByte(value);
        } else if (xs instanceof short[]) {
            ((short[]) xs)[idx] = BoxesRunTime.unboxToShort(value);
        } else if (xs instanceof boolean[]) {
            ((boolean[]) xs)[idx] = BoxesRunTime.unboxToBoolean(value);
        } else if (xs instanceof BoxedUnit[]) {
            ((BoxedUnit[]) xs)[idx] = (BoxedUnit) value;
        } else if (xs == null) {
            throw new NullPointerException();
        } else {
            throw new MatchError(xs);
        }
    }

    public int array_length(Object xs) {
        if (xs instanceof Object[]) {
            return ((Object[]) xs).length;
        }
        if (xs instanceof int[]) {
            return ((int[]) xs).length;
        }
        if (xs instanceof double[]) {
            return ((double[]) xs).length;
        }
        if (xs instanceof long[]) {
            return ((long[]) xs).length;
        }
        if (xs instanceof float[]) {
            return ((float[]) xs).length;
        }
        if (xs instanceof char[]) {
            return ((char[]) xs).length;
        }
        if (xs instanceof byte[]) {
            return ((byte[]) xs).length;
        }
        if (xs instanceof short[]) {
            return ((short[]) xs).length;
        }
        if (xs instanceof boolean[]) {
            return ((boolean[]) xs).length;
        }
        if (xs instanceof BoxedUnit[]) {
            return ((BoxedUnit[]) xs).length;
        }
        if (xs == null) {
            throw new NullPointerException();
        }
        throw new MatchError(xs);
    }

    public Object array_clone(Object xs) {
        if (xs instanceof Object[]) {
            return ArrayRuntime.cloneArray((Object[]) xs);
        }
        if (xs instanceof int[]) {
            return ArrayRuntime.cloneArray((int[]) xs);
        }
        if (xs instanceof double[]) {
            return ArrayRuntime.cloneArray((double[]) xs);
        }
        if (xs instanceof long[]) {
            return ArrayRuntime.cloneArray((long[]) xs);
        }
        if (xs instanceof float[]) {
            return ArrayRuntime.cloneArray((float[]) xs);
        }
        if (xs instanceof char[]) {
            return ArrayRuntime.cloneArray((char[]) xs);
        }
        if (xs instanceof byte[]) {
            return ArrayRuntime.cloneArray((byte[]) xs);
        }
        if (xs instanceof short[]) {
            return ArrayRuntime.cloneArray((short[]) xs);
        }
        if (xs instanceof boolean[]) {
            return ArrayRuntime.cloneArray((boolean[]) xs);
        }
        if (xs instanceof BoxedUnit[]) {
            return (BoxedUnit[]) xs;
        }
        if (xs == null) {
            throw new NullPointerException();
        }
        throw new MatchError(xs);
    }

    public String _toString(Product product) {
        return product.productIterator().mkString(new StringBuilder().append((Object) product.productPrefix()).append((Object) "(").toString(), ",", ")");
    }

    public int _hashCode(Product product) {
        int productArity = product.productArity();
        int i = productArity;
        for (int i2 = 0; i2 < productArity; i2++) {
            Object productElement = product.productElement(i2);
            i = (i * 41) + (productElement == null ? 0 : productElement instanceof Number ? BoxesRunTime.hashFromNumber((Number) productElement) : productElement.hashCode());
        }
        return i;
    }
}
