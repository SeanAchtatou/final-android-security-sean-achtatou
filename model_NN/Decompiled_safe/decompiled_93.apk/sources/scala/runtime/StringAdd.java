package scala.runtime;

import scala.ScalaObject;
import scala.collection.mutable.StringBuilder;

/* compiled from: StringAdd.scala */
public final class StringAdd implements ScalaObject {
    public final Object self;

    public StringAdd(Object self2) {
        this.self = self2;
    }

    public String $plus(String other) {
        return new StringBuilder().append((Object) String.valueOf(this.self)).append((Object) other).toString();
    }
}
