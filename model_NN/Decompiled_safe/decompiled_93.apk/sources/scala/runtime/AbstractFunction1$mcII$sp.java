package scala.runtime;

import scala.Function1;
import scala.Function1$mcII$sp;

/* compiled from: AbstractFunction1.scala */
public abstract class AbstractFunction1$mcII$sp extends AbstractFunction1<Integer, Integer> implements Function1$mcII$sp {
    public AbstractFunction1$mcII$sp() {
        Function1$mcII$sp.Cclass.$init$(this);
    }

    public <A> Function1<Integer, A> andThen(Function1<Integer, A> g) {
        return Function1$mcII$sp.Cclass.andThen(this, g);
    }

    public <A> Function1<Integer, A> andThen$mcII$sp(Function1<Integer, A> g) {
        return Function1$mcII$sp.Cclass.andThen$mcII$sp(this, g);
    }

    public int apply(int v1) {
        return Function1$mcII$sp.Cclass.apply(this, v1);
    }

    public /* bridge */ /* synthetic */ Object apply(Object v1) {
        return BoxesRunTime.boxToInteger(apply(BoxesRunTime.unboxToInt(v1)));
    }

    public <A> Function1<A, Integer> compose(Function1<A, Integer> g) {
        return Function1$mcII$sp.Cclass.compose(this, g);
    }

    public <A> Function1<A, Integer> compose$mcII$sp(Function1<A, Integer> g) {
        return Function1$mcII$sp.Cclass.compose$mcII$sp(this, g);
    }
}
