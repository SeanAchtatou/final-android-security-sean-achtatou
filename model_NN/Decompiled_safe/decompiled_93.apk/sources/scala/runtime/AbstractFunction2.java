package scala.runtime;

import scala.Function1;
import scala.Function2;
import scala.ScalaObject;
import scala.Tuple2;

/* compiled from: AbstractFunction2.scala */
public abstract class AbstractFunction2<T1, T2, R> implements Function2<T1, T2, R>, ScalaObject {
    public AbstractFunction2() {
        Function2.Cclass.$init$(this);
    }

    public double apply$mcDDD$sp(double v1, double v2) {
        return Function2.Cclass.apply$mcDDD$sp(this, v1, v2);
    }

    public double apply$mcDDI$sp(double v1, int v2) {
        return Function2.Cclass.apply$mcDDI$sp(this, v1, v2);
    }

    public double apply$mcDDL$sp(double v1, long v2) {
        return Function2.Cclass.apply$mcDDL$sp(this, v1, v2);
    }

    public double apply$mcDID$sp(int v1, double v2) {
        return Function2.Cclass.apply$mcDID$sp(this, v1, v2);
    }

    public double apply$mcDII$sp(int v1, int v2) {
        return Function2.Cclass.apply$mcDII$sp(this, v1, v2);
    }

    public double apply$mcDIL$sp(int v1, long v2) {
        return Function2.Cclass.apply$mcDIL$sp(this, v1, v2);
    }

    public double apply$mcDLD$sp(long v1, double v2) {
        return Function2.Cclass.apply$mcDLD$sp(this, v1, v2);
    }

    public double apply$mcDLI$sp(long v1, int v2) {
        return Function2.Cclass.apply$mcDLI$sp(this, v1, v2);
    }

    public double apply$mcDLL$sp(long v1, long v2) {
        return Function2.Cclass.apply$mcDLL$sp(this, v1, v2);
    }

    public float apply$mcFDD$sp(double v1, double v2) {
        return Function2.Cclass.apply$mcFDD$sp(this, v1, v2);
    }

    public float apply$mcFDI$sp(double v1, int v2) {
        return Function2.Cclass.apply$mcFDI$sp(this, v1, v2);
    }

    public float apply$mcFDL$sp(double v1, long v2) {
        return Function2.Cclass.apply$mcFDL$sp(this, v1, v2);
    }

    public float apply$mcFID$sp(int v1, double v2) {
        return Function2.Cclass.apply$mcFID$sp(this, v1, v2);
    }

    public float apply$mcFII$sp(int v1, int v2) {
        return Function2.Cclass.apply$mcFII$sp(this, v1, v2);
    }

    public float apply$mcFIL$sp(int v1, long v2) {
        return Function2.Cclass.apply$mcFIL$sp(this, v1, v2);
    }

    public float apply$mcFLD$sp(long v1, double v2) {
        return Function2.Cclass.apply$mcFLD$sp(this, v1, v2);
    }

    public float apply$mcFLI$sp(long v1, int v2) {
        return Function2.Cclass.apply$mcFLI$sp(this, v1, v2);
    }

    public float apply$mcFLL$sp(long v1, long v2) {
        return Function2.Cclass.apply$mcFLL$sp(this, v1, v2);
    }

    public int apply$mcIDD$sp(double v1, double v2) {
        return Function2.Cclass.apply$mcIDD$sp(this, v1, v2);
    }

    public int apply$mcIDI$sp(double v1, int v2) {
        return Function2.Cclass.apply$mcIDI$sp(this, v1, v2);
    }

    public int apply$mcIDL$sp(double v1, long v2) {
        return Function2.Cclass.apply$mcIDL$sp(this, v1, v2);
    }

    public int apply$mcIID$sp(int v1, double v2) {
        return Function2.Cclass.apply$mcIID$sp(this, v1, v2);
    }

    public int apply$mcIII$sp(int v1, int v2) {
        return Function2.Cclass.apply$mcIII$sp(this, v1, v2);
    }

    public int apply$mcIIL$sp(int v1, long v2) {
        return Function2.Cclass.apply$mcIIL$sp(this, v1, v2);
    }

    public int apply$mcILD$sp(long v1, double v2) {
        return Function2.Cclass.apply$mcILD$sp(this, v1, v2);
    }

    public int apply$mcILI$sp(long v1, int v2) {
        return Function2.Cclass.apply$mcILI$sp(this, v1, v2);
    }

    public int apply$mcILL$sp(long v1, long v2) {
        return Function2.Cclass.apply$mcILL$sp(this, v1, v2);
    }

    public long apply$mcLDD$sp(double v1, double v2) {
        return Function2.Cclass.apply$mcLDD$sp(this, v1, v2);
    }

    public long apply$mcLDI$sp(double v1, int v2) {
        return Function2.Cclass.apply$mcLDI$sp(this, v1, v2);
    }

    public long apply$mcLDL$sp(double v1, long v2) {
        return Function2.Cclass.apply$mcLDL$sp(this, v1, v2);
    }

    public long apply$mcLID$sp(int v1, double v2) {
        return Function2.Cclass.apply$mcLID$sp(this, v1, v2);
    }

    public long apply$mcLII$sp(int v1, int v2) {
        return Function2.Cclass.apply$mcLII$sp(this, v1, v2);
    }

    public long apply$mcLIL$sp(int v1, long v2) {
        return Function2.Cclass.apply$mcLIL$sp(this, v1, v2);
    }

    public long apply$mcLLD$sp(long v1, double v2) {
        return Function2.Cclass.apply$mcLLD$sp(this, v1, v2);
    }

    public long apply$mcLLI$sp(long v1, int v2) {
        return Function2.Cclass.apply$mcLLI$sp(this, v1, v2);
    }

    public long apply$mcLLL$sp(long v1, long v2) {
        return Function2.Cclass.apply$mcLLL$sp(this, v1, v2);
    }

    public void apply$mcVDD$sp(double v1, double v2) {
        Function2.Cclass.apply$mcVDD$sp(this, v1, v2);
    }

    public void apply$mcVDI$sp(double v1, int v2) {
        Function2.Cclass.apply$mcVDI$sp(this, v1, v2);
    }

    public void apply$mcVDL$sp(double v1, long v2) {
        Function2.Cclass.apply$mcVDL$sp(this, v1, v2);
    }

    public void apply$mcVID$sp(int v1, double v2) {
        Function2.Cclass.apply$mcVID$sp(this, v1, v2);
    }

    public void apply$mcVII$sp(int v1, int v2) {
        Function2.Cclass.apply$mcVII$sp(this, v1, v2);
    }

    public void apply$mcVIL$sp(int v1, long v2) {
        Function2.Cclass.apply$mcVIL$sp(this, v1, v2);
    }

    public void apply$mcVLD$sp(long v1, double v2) {
        Function2.Cclass.apply$mcVLD$sp(this, v1, v2);
    }

    public void apply$mcVLI$sp(long v1, int v2) {
        Function2.Cclass.apply$mcVLI$sp(this, v1, v2);
    }

    public void apply$mcVLL$sp(long v1, long v2) {
        Function2.Cclass.apply$mcVLL$sp(this, v1, v2);
    }

    public boolean apply$mcZDD$sp(double v1, double v2) {
        return Function2.Cclass.apply$mcZDD$sp(this, v1, v2);
    }

    public boolean apply$mcZDI$sp(double v1, int v2) {
        return Function2.Cclass.apply$mcZDI$sp(this, v1, v2);
    }

    public boolean apply$mcZDL$sp(double v1, long v2) {
        return Function2.Cclass.apply$mcZDL$sp(this, v1, v2);
    }

    public boolean apply$mcZID$sp(int v1, double v2) {
        return Function2.Cclass.apply$mcZID$sp(this, v1, v2);
    }

    public boolean apply$mcZII$sp(int v1, int v2) {
        return Function2.Cclass.apply$mcZII$sp(this, v1, v2);
    }

    public boolean apply$mcZIL$sp(int v1, long v2) {
        return Function2.Cclass.apply$mcZIL$sp(this, v1, v2);
    }

    public boolean apply$mcZLD$sp(long v1, double v2) {
        return Function2.Cclass.apply$mcZLD$sp(this, v1, v2);
    }

    public boolean apply$mcZLI$sp(long v1, int v2) {
        return Function2.Cclass.apply$mcZLI$sp(this, v1, v2);
    }

    public boolean apply$mcZLL$sp(long v1, long v2) {
        return Function2.Cclass.apply$mcZLL$sp(this, v1, v2);
    }

    public Function1<T1, Function1<T2, R>> curried() {
        return Function2.Cclass.curried(this);
    }

    public Function1<Double, Function1<Double, Double>> curried$mcDDD$sp() {
        return Function2.Cclass.curried$mcDDD$sp(this);
    }

    public Function1<Double, Function1<Integer, Double>> curried$mcDDI$sp() {
        return Function2.Cclass.curried$mcDDI$sp(this);
    }

    public Function1<Double, Function1<Long, Double>> curried$mcDDL$sp() {
        return Function2.Cclass.curried$mcDDL$sp(this);
    }

    public Function1<Integer, Function1<Double, Double>> curried$mcDID$sp() {
        return Function2.Cclass.curried$mcDID$sp(this);
    }

    public Function1<Integer, Function1<Integer, Double>> curried$mcDII$sp() {
        return Function2.Cclass.curried$mcDII$sp(this);
    }

    public Function1<Integer, Function1<Long, Double>> curried$mcDIL$sp() {
        return Function2.Cclass.curried$mcDIL$sp(this);
    }

    public Function1<Long, Function1<Double, Double>> curried$mcDLD$sp() {
        return Function2.Cclass.curried$mcDLD$sp(this);
    }

    public Function1<Long, Function1<Integer, Double>> curried$mcDLI$sp() {
        return Function2.Cclass.curried$mcDLI$sp(this);
    }

    public Function1<Long, Function1<Long, Double>> curried$mcDLL$sp() {
        return Function2.Cclass.curried$mcDLL$sp(this);
    }

    public Function1<Double, Function1<Double, Float>> curried$mcFDD$sp() {
        return Function2.Cclass.curried$mcFDD$sp(this);
    }

    public Function1<Double, Function1<Integer, Float>> curried$mcFDI$sp() {
        return Function2.Cclass.curried$mcFDI$sp(this);
    }

    public Function1<Double, Function1<Long, Float>> curried$mcFDL$sp() {
        return Function2.Cclass.curried$mcFDL$sp(this);
    }

    public Function1<Integer, Function1<Double, Float>> curried$mcFID$sp() {
        return Function2.Cclass.curried$mcFID$sp(this);
    }

    public Function1<Integer, Function1<Integer, Float>> curried$mcFII$sp() {
        return Function2.Cclass.curried$mcFII$sp(this);
    }

    public Function1<Integer, Function1<Long, Float>> curried$mcFIL$sp() {
        return Function2.Cclass.curried$mcFIL$sp(this);
    }

    public Function1<Long, Function1<Double, Float>> curried$mcFLD$sp() {
        return Function2.Cclass.curried$mcFLD$sp(this);
    }

    public Function1<Long, Function1<Integer, Float>> curried$mcFLI$sp() {
        return Function2.Cclass.curried$mcFLI$sp(this);
    }

    public Function1<Long, Function1<Long, Float>> curried$mcFLL$sp() {
        return Function2.Cclass.curried$mcFLL$sp(this);
    }

    public Function1<Double, Function1<Double, Integer>> curried$mcIDD$sp() {
        return Function2.Cclass.curried$mcIDD$sp(this);
    }

    public Function1<Double, Function1<Integer, Integer>> curried$mcIDI$sp() {
        return Function2.Cclass.curried$mcIDI$sp(this);
    }

    public Function1<Double, Function1<Long, Integer>> curried$mcIDL$sp() {
        return Function2.Cclass.curried$mcIDL$sp(this);
    }

    public Function1<Integer, Function1<Double, Integer>> curried$mcIID$sp() {
        return Function2.Cclass.curried$mcIID$sp(this);
    }

    public Function1<Integer, Function1<Integer, Integer>> curried$mcIII$sp() {
        return Function2.Cclass.curried$mcIII$sp(this);
    }

    public Function1<Integer, Function1<Long, Integer>> curried$mcIIL$sp() {
        return Function2.Cclass.curried$mcIIL$sp(this);
    }

    public Function1<Long, Function1<Double, Integer>> curried$mcILD$sp() {
        return Function2.Cclass.curried$mcILD$sp(this);
    }

    public Function1<Long, Function1<Integer, Integer>> curried$mcILI$sp() {
        return Function2.Cclass.curried$mcILI$sp(this);
    }

    public Function1<Long, Function1<Long, Integer>> curried$mcILL$sp() {
        return Function2.Cclass.curried$mcILL$sp(this);
    }

    public Function1<Double, Function1<Double, Long>> curried$mcLDD$sp() {
        return Function2.Cclass.curried$mcLDD$sp(this);
    }

    public Function1<Double, Function1<Integer, Long>> curried$mcLDI$sp() {
        return Function2.Cclass.curried$mcLDI$sp(this);
    }

    public Function1<Double, Function1<Long, Long>> curried$mcLDL$sp() {
        return Function2.Cclass.curried$mcLDL$sp(this);
    }

    public Function1<Integer, Function1<Double, Long>> curried$mcLID$sp() {
        return Function2.Cclass.curried$mcLID$sp(this);
    }

    public Function1<Integer, Function1<Integer, Long>> curried$mcLII$sp() {
        return Function2.Cclass.curried$mcLII$sp(this);
    }

    public Function1<Integer, Function1<Long, Long>> curried$mcLIL$sp() {
        return Function2.Cclass.curried$mcLIL$sp(this);
    }

    public Function1<Long, Function1<Double, Long>> curried$mcLLD$sp() {
        return Function2.Cclass.curried$mcLLD$sp(this);
    }

    public Function1<Long, Function1<Integer, Long>> curried$mcLLI$sp() {
        return Function2.Cclass.curried$mcLLI$sp(this);
    }

    public Function1<Long, Function1<Long, Long>> curried$mcLLL$sp() {
        return Function2.Cclass.curried$mcLLL$sp(this);
    }

    public Function1<Double, Function1<Double, Object>> curried$mcVDD$sp() {
        return Function2.Cclass.curried$mcVDD$sp(this);
    }

    public Function1<Double, Function1<Integer, Object>> curried$mcVDI$sp() {
        return Function2.Cclass.curried$mcVDI$sp(this);
    }

    public Function1<Double, Function1<Long, Object>> curried$mcVDL$sp() {
        return Function2.Cclass.curried$mcVDL$sp(this);
    }

    public Function1<Integer, Function1<Double, Object>> curried$mcVID$sp() {
        return Function2.Cclass.curried$mcVID$sp(this);
    }

    public Function1<Integer, Function1<Integer, Object>> curried$mcVII$sp() {
        return Function2.Cclass.curried$mcVII$sp(this);
    }

    public Function1<Integer, Function1<Long, Object>> curried$mcVIL$sp() {
        return Function2.Cclass.curried$mcVIL$sp(this);
    }

    public Function1<Long, Function1<Double, Object>> curried$mcVLD$sp() {
        return Function2.Cclass.curried$mcVLD$sp(this);
    }

    public Function1<Long, Function1<Integer, Object>> curried$mcVLI$sp() {
        return Function2.Cclass.curried$mcVLI$sp(this);
    }

    public Function1<Long, Function1<Long, Object>> curried$mcVLL$sp() {
        return Function2.Cclass.curried$mcVLL$sp(this);
    }

    public Function1<Double, Function1<Double, Boolean>> curried$mcZDD$sp() {
        return Function2.Cclass.curried$mcZDD$sp(this);
    }

    public Function1<Double, Function1<Integer, Boolean>> curried$mcZDI$sp() {
        return Function2.Cclass.curried$mcZDI$sp(this);
    }

    public Function1<Double, Function1<Long, Boolean>> curried$mcZDL$sp() {
        return Function2.Cclass.curried$mcZDL$sp(this);
    }

    public Function1<Integer, Function1<Double, Boolean>> curried$mcZID$sp() {
        return Function2.Cclass.curried$mcZID$sp(this);
    }

    public Function1<Integer, Function1<Integer, Boolean>> curried$mcZII$sp() {
        return Function2.Cclass.curried$mcZII$sp(this);
    }

    public Function1<Integer, Function1<Long, Boolean>> curried$mcZIL$sp() {
        return Function2.Cclass.curried$mcZIL$sp(this);
    }

    public Function1<Long, Function1<Double, Boolean>> curried$mcZLD$sp() {
        return Function2.Cclass.curried$mcZLD$sp(this);
    }

    public Function1<Long, Function1<Integer, Boolean>> curried$mcZLI$sp() {
        return Function2.Cclass.curried$mcZLI$sp(this);
    }

    public Function1<Long, Function1<Long, Boolean>> curried$mcZLL$sp() {
        return Function2.Cclass.curried$mcZLL$sp(this);
    }

    public Function1<T1, Function1<T2, R>> curry() {
        return Function2.Cclass.curry(this);
    }

    public Function1<Double, Function1<Double, Double>> curry$mcDDD$sp() {
        return Function2.Cclass.curry$mcDDD$sp(this);
    }

    public Function1<Double, Function1<Integer, Double>> curry$mcDDI$sp() {
        return Function2.Cclass.curry$mcDDI$sp(this);
    }

    public Function1<Double, Function1<Long, Double>> curry$mcDDL$sp() {
        return Function2.Cclass.curry$mcDDL$sp(this);
    }

    public Function1<Integer, Function1<Double, Double>> curry$mcDID$sp() {
        return Function2.Cclass.curry$mcDID$sp(this);
    }

    public Function1<Integer, Function1<Integer, Double>> curry$mcDII$sp() {
        return Function2.Cclass.curry$mcDII$sp(this);
    }

    public Function1<Integer, Function1<Long, Double>> curry$mcDIL$sp() {
        return Function2.Cclass.curry$mcDIL$sp(this);
    }

    public Function1<Long, Function1<Double, Double>> curry$mcDLD$sp() {
        return Function2.Cclass.curry$mcDLD$sp(this);
    }

    public Function1<Long, Function1<Integer, Double>> curry$mcDLI$sp() {
        return Function2.Cclass.curry$mcDLI$sp(this);
    }

    public Function1<Long, Function1<Long, Double>> curry$mcDLL$sp() {
        return Function2.Cclass.curry$mcDLL$sp(this);
    }

    public Function1<Double, Function1<Double, Float>> curry$mcFDD$sp() {
        return Function2.Cclass.curry$mcFDD$sp(this);
    }

    public Function1<Double, Function1<Integer, Float>> curry$mcFDI$sp() {
        return Function2.Cclass.curry$mcFDI$sp(this);
    }

    public Function1<Double, Function1<Long, Float>> curry$mcFDL$sp() {
        return Function2.Cclass.curry$mcFDL$sp(this);
    }

    public Function1<Integer, Function1<Double, Float>> curry$mcFID$sp() {
        return Function2.Cclass.curry$mcFID$sp(this);
    }

    public Function1<Integer, Function1<Integer, Float>> curry$mcFII$sp() {
        return Function2.Cclass.curry$mcFII$sp(this);
    }

    public Function1<Integer, Function1<Long, Float>> curry$mcFIL$sp() {
        return Function2.Cclass.curry$mcFIL$sp(this);
    }

    public Function1<Long, Function1<Double, Float>> curry$mcFLD$sp() {
        return Function2.Cclass.curry$mcFLD$sp(this);
    }

    public Function1<Long, Function1<Integer, Float>> curry$mcFLI$sp() {
        return Function2.Cclass.curry$mcFLI$sp(this);
    }

    public Function1<Long, Function1<Long, Float>> curry$mcFLL$sp() {
        return Function2.Cclass.curry$mcFLL$sp(this);
    }

    public Function1<Double, Function1<Double, Integer>> curry$mcIDD$sp() {
        return Function2.Cclass.curry$mcIDD$sp(this);
    }

    public Function1<Double, Function1<Integer, Integer>> curry$mcIDI$sp() {
        return Function2.Cclass.curry$mcIDI$sp(this);
    }

    public Function1<Double, Function1<Long, Integer>> curry$mcIDL$sp() {
        return Function2.Cclass.curry$mcIDL$sp(this);
    }

    public Function1<Integer, Function1<Double, Integer>> curry$mcIID$sp() {
        return Function2.Cclass.curry$mcIID$sp(this);
    }

    public Function1<Integer, Function1<Integer, Integer>> curry$mcIII$sp() {
        return Function2.Cclass.curry$mcIII$sp(this);
    }

    public Function1<Integer, Function1<Long, Integer>> curry$mcIIL$sp() {
        return Function2.Cclass.curry$mcIIL$sp(this);
    }

    public Function1<Long, Function1<Double, Integer>> curry$mcILD$sp() {
        return Function2.Cclass.curry$mcILD$sp(this);
    }

    public Function1<Long, Function1<Integer, Integer>> curry$mcILI$sp() {
        return Function2.Cclass.curry$mcILI$sp(this);
    }

    public Function1<Long, Function1<Long, Integer>> curry$mcILL$sp() {
        return Function2.Cclass.curry$mcILL$sp(this);
    }

    public Function1<Double, Function1<Double, Long>> curry$mcLDD$sp() {
        return Function2.Cclass.curry$mcLDD$sp(this);
    }

    public Function1<Double, Function1<Integer, Long>> curry$mcLDI$sp() {
        return Function2.Cclass.curry$mcLDI$sp(this);
    }

    public Function1<Double, Function1<Long, Long>> curry$mcLDL$sp() {
        return Function2.Cclass.curry$mcLDL$sp(this);
    }

    public Function1<Integer, Function1<Double, Long>> curry$mcLID$sp() {
        return Function2.Cclass.curry$mcLID$sp(this);
    }

    public Function1<Integer, Function1<Integer, Long>> curry$mcLII$sp() {
        return Function2.Cclass.curry$mcLII$sp(this);
    }

    public Function1<Integer, Function1<Long, Long>> curry$mcLIL$sp() {
        return Function2.Cclass.curry$mcLIL$sp(this);
    }

    public Function1<Long, Function1<Double, Long>> curry$mcLLD$sp() {
        return Function2.Cclass.curry$mcLLD$sp(this);
    }

    public Function1<Long, Function1<Integer, Long>> curry$mcLLI$sp() {
        return Function2.Cclass.curry$mcLLI$sp(this);
    }

    public Function1<Long, Function1<Long, Long>> curry$mcLLL$sp() {
        return Function2.Cclass.curry$mcLLL$sp(this);
    }

    public Function1<Double, Function1<Double, Object>> curry$mcVDD$sp() {
        return Function2.Cclass.curry$mcVDD$sp(this);
    }

    public Function1<Double, Function1<Integer, Object>> curry$mcVDI$sp() {
        return Function2.Cclass.curry$mcVDI$sp(this);
    }

    public Function1<Double, Function1<Long, Object>> curry$mcVDL$sp() {
        return Function2.Cclass.curry$mcVDL$sp(this);
    }

    public Function1<Integer, Function1<Double, Object>> curry$mcVID$sp() {
        return Function2.Cclass.curry$mcVID$sp(this);
    }

    public Function1<Integer, Function1<Integer, Object>> curry$mcVII$sp() {
        return Function2.Cclass.curry$mcVII$sp(this);
    }

    public Function1<Integer, Function1<Long, Object>> curry$mcVIL$sp() {
        return Function2.Cclass.curry$mcVIL$sp(this);
    }

    public Function1<Long, Function1<Double, Object>> curry$mcVLD$sp() {
        return Function2.Cclass.curry$mcVLD$sp(this);
    }

    public Function1<Long, Function1<Integer, Object>> curry$mcVLI$sp() {
        return Function2.Cclass.curry$mcVLI$sp(this);
    }

    public Function1<Long, Function1<Long, Object>> curry$mcVLL$sp() {
        return Function2.Cclass.curry$mcVLL$sp(this);
    }

    public Function1<Double, Function1<Double, Boolean>> curry$mcZDD$sp() {
        return Function2.Cclass.curry$mcZDD$sp(this);
    }

    public Function1<Double, Function1<Integer, Boolean>> curry$mcZDI$sp() {
        return Function2.Cclass.curry$mcZDI$sp(this);
    }

    public Function1<Double, Function1<Long, Boolean>> curry$mcZDL$sp() {
        return Function2.Cclass.curry$mcZDL$sp(this);
    }

    public Function1<Integer, Function1<Double, Boolean>> curry$mcZID$sp() {
        return Function2.Cclass.curry$mcZID$sp(this);
    }

    public Function1<Integer, Function1<Integer, Boolean>> curry$mcZII$sp() {
        return Function2.Cclass.curry$mcZII$sp(this);
    }

    public Function1<Integer, Function1<Long, Boolean>> curry$mcZIL$sp() {
        return Function2.Cclass.curry$mcZIL$sp(this);
    }

    public Function1<Long, Function1<Double, Boolean>> curry$mcZLD$sp() {
        return Function2.Cclass.curry$mcZLD$sp(this);
    }

    public Function1<Long, Function1<Integer, Boolean>> curry$mcZLI$sp() {
        return Function2.Cclass.curry$mcZLI$sp(this);
    }

    public Function1<Long, Function1<Long, Boolean>> curry$mcZLL$sp() {
        return Function2.Cclass.curry$mcZLL$sp(this);
    }

    public String toString() {
        return Function2.Cclass.toString(this);
    }

    public Function1<Tuple2<T1, T2>, R> tupled() {
        return Function2.Cclass.tupled(this);
    }

    public Function1<Tuple2<Double, Double>, Double> tupled$mcDDD$sp() {
        return Function2.Cclass.tupled$mcDDD$sp(this);
    }

    public Function1<Tuple2<Double, Integer>, Double> tupled$mcDDI$sp() {
        return Function2.Cclass.tupled$mcDDI$sp(this);
    }

    public Function1<Tuple2<Double, Long>, Double> tupled$mcDDL$sp() {
        return Function2.Cclass.tupled$mcDDL$sp(this);
    }

    public Function1<Tuple2<Integer, Double>, Double> tupled$mcDID$sp() {
        return Function2.Cclass.tupled$mcDID$sp(this);
    }

    public Function1<Tuple2<Integer, Integer>, Double> tupled$mcDII$sp() {
        return Function2.Cclass.tupled$mcDII$sp(this);
    }

    public Function1<Tuple2<Integer, Long>, Double> tupled$mcDIL$sp() {
        return Function2.Cclass.tupled$mcDIL$sp(this);
    }

    public Function1<Tuple2<Long, Double>, Double> tupled$mcDLD$sp() {
        return Function2.Cclass.tupled$mcDLD$sp(this);
    }

    public Function1<Tuple2<Long, Integer>, Double> tupled$mcDLI$sp() {
        return Function2.Cclass.tupled$mcDLI$sp(this);
    }

    public Function1<Tuple2<Long, Long>, Double> tupled$mcDLL$sp() {
        return Function2.Cclass.tupled$mcDLL$sp(this);
    }

    public Function1<Tuple2<Double, Double>, Float> tupled$mcFDD$sp() {
        return Function2.Cclass.tupled$mcFDD$sp(this);
    }

    public Function1<Tuple2<Double, Integer>, Float> tupled$mcFDI$sp() {
        return Function2.Cclass.tupled$mcFDI$sp(this);
    }

    public Function1<Tuple2<Double, Long>, Float> tupled$mcFDL$sp() {
        return Function2.Cclass.tupled$mcFDL$sp(this);
    }

    public Function1<Tuple2<Integer, Double>, Float> tupled$mcFID$sp() {
        return Function2.Cclass.tupled$mcFID$sp(this);
    }

    public Function1<Tuple2<Integer, Integer>, Float> tupled$mcFII$sp() {
        return Function2.Cclass.tupled$mcFII$sp(this);
    }

    public Function1<Tuple2<Integer, Long>, Float> tupled$mcFIL$sp() {
        return Function2.Cclass.tupled$mcFIL$sp(this);
    }

    public Function1<Tuple2<Long, Double>, Float> tupled$mcFLD$sp() {
        return Function2.Cclass.tupled$mcFLD$sp(this);
    }

    public Function1<Tuple2<Long, Integer>, Float> tupled$mcFLI$sp() {
        return Function2.Cclass.tupled$mcFLI$sp(this);
    }

    public Function1<Tuple2<Long, Long>, Float> tupled$mcFLL$sp() {
        return Function2.Cclass.tupled$mcFLL$sp(this);
    }

    public Function1<Tuple2<Double, Double>, Integer> tupled$mcIDD$sp() {
        return Function2.Cclass.tupled$mcIDD$sp(this);
    }

    public Function1<Tuple2<Double, Integer>, Integer> tupled$mcIDI$sp() {
        return Function2.Cclass.tupled$mcIDI$sp(this);
    }

    public Function1<Tuple2<Double, Long>, Integer> tupled$mcIDL$sp() {
        return Function2.Cclass.tupled$mcIDL$sp(this);
    }

    public Function1<Tuple2<Integer, Double>, Integer> tupled$mcIID$sp() {
        return Function2.Cclass.tupled$mcIID$sp(this);
    }

    public Function1<Tuple2<Integer, Integer>, Integer> tupled$mcIII$sp() {
        return Function2.Cclass.tupled$mcIII$sp(this);
    }

    public Function1<Tuple2<Integer, Long>, Integer> tupled$mcIIL$sp() {
        return Function2.Cclass.tupled$mcIIL$sp(this);
    }

    public Function1<Tuple2<Long, Double>, Integer> tupled$mcILD$sp() {
        return Function2.Cclass.tupled$mcILD$sp(this);
    }

    public Function1<Tuple2<Long, Integer>, Integer> tupled$mcILI$sp() {
        return Function2.Cclass.tupled$mcILI$sp(this);
    }

    public Function1<Tuple2<Long, Long>, Integer> tupled$mcILL$sp() {
        return Function2.Cclass.tupled$mcILL$sp(this);
    }

    public Function1<Tuple2<Double, Double>, Long> tupled$mcLDD$sp() {
        return Function2.Cclass.tupled$mcLDD$sp(this);
    }

    public Function1<Tuple2<Double, Integer>, Long> tupled$mcLDI$sp() {
        return Function2.Cclass.tupled$mcLDI$sp(this);
    }

    public Function1<Tuple2<Double, Long>, Long> tupled$mcLDL$sp() {
        return Function2.Cclass.tupled$mcLDL$sp(this);
    }

    public Function1<Tuple2<Integer, Double>, Long> tupled$mcLID$sp() {
        return Function2.Cclass.tupled$mcLID$sp(this);
    }

    public Function1<Tuple2<Integer, Integer>, Long> tupled$mcLII$sp() {
        return Function2.Cclass.tupled$mcLII$sp(this);
    }

    public Function1<Tuple2<Integer, Long>, Long> tupled$mcLIL$sp() {
        return Function2.Cclass.tupled$mcLIL$sp(this);
    }

    public Function1<Tuple2<Long, Double>, Long> tupled$mcLLD$sp() {
        return Function2.Cclass.tupled$mcLLD$sp(this);
    }

    public Function1<Tuple2<Long, Integer>, Long> tupled$mcLLI$sp() {
        return Function2.Cclass.tupled$mcLLI$sp(this);
    }

    public Function1<Tuple2<Long, Long>, Long> tupled$mcLLL$sp() {
        return Function2.Cclass.tupled$mcLLL$sp(this);
    }

    public Function1<Tuple2<Double, Double>, Object> tupled$mcVDD$sp() {
        return Function2.Cclass.tupled$mcVDD$sp(this);
    }

    public Function1<Tuple2<Double, Integer>, Object> tupled$mcVDI$sp() {
        return Function2.Cclass.tupled$mcVDI$sp(this);
    }

    public Function1<Tuple2<Double, Long>, Object> tupled$mcVDL$sp() {
        return Function2.Cclass.tupled$mcVDL$sp(this);
    }

    public Function1<Tuple2<Integer, Double>, Object> tupled$mcVID$sp() {
        return Function2.Cclass.tupled$mcVID$sp(this);
    }

    public Function1<Tuple2<Integer, Integer>, Object> tupled$mcVII$sp() {
        return Function2.Cclass.tupled$mcVII$sp(this);
    }

    public Function1<Tuple2<Integer, Long>, Object> tupled$mcVIL$sp() {
        return Function2.Cclass.tupled$mcVIL$sp(this);
    }

    public Function1<Tuple2<Long, Double>, Object> tupled$mcVLD$sp() {
        return Function2.Cclass.tupled$mcVLD$sp(this);
    }

    public Function1<Tuple2<Long, Integer>, Object> tupled$mcVLI$sp() {
        return Function2.Cclass.tupled$mcVLI$sp(this);
    }

    public Function1<Tuple2<Long, Long>, Object> tupled$mcVLL$sp() {
        return Function2.Cclass.tupled$mcVLL$sp(this);
    }

    public Function1<Tuple2<Double, Double>, Boolean> tupled$mcZDD$sp() {
        return Function2.Cclass.tupled$mcZDD$sp(this);
    }

    public Function1<Tuple2<Double, Integer>, Boolean> tupled$mcZDI$sp() {
        return Function2.Cclass.tupled$mcZDI$sp(this);
    }

    public Function1<Tuple2<Double, Long>, Boolean> tupled$mcZDL$sp() {
        return Function2.Cclass.tupled$mcZDL$sp(this);
    }

    public Function1<Tuple2<Integer, Double>, Boolean> tupled$mcZID$sp() {
        return Function2.Cclass.tupled$mcZID$sp(this);
    }

    public Function1<Tuple2<Integer, Integer>, Boolean> tupled$mcZII$sp() {
        return Function2.Cclass.tupled$mcZII$sp(this);
    }

    public Function1<Tuple2<Integer, Long>, Boolean> tupled$mcZIL$sp() {
        return Function2.Cclass.tupled$mcZIL$sp(this);
    }

    public Function1<Tuple2<Long, Double>, Boolean> tupled$mcZLD$sp() {
        return Function2.Cclass.tupled$mcZLD$sp(this);
    }

    public Function1<Tuple2<Long, Integer>, Boolean> tupled$mcZLI$sp() {
        return Function2.Cclass.tupled$mcZLI$sp(this);
    }

    public Function1<Tuple2<Long, Long>, Boolean> tupled$mcZLL$sp() {
        return Function2.Cclass.tupled$mcZLL$sp(this);
    }
}
