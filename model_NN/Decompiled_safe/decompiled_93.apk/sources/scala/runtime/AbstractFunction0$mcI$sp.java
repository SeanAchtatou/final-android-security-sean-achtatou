package scala.runtime;

import scala.Function0$mcI$sp;

/* compiled from: AbstractFunction0.scala */
public abstract class AbstractFunction0$mcI$sp extends AbstractFunction0<Integer> implements Function0$mcI$sp {
    public AbstractFunction0$mcI$sp() {
        Function0$mcI$sp.Cclass.$init$(this);
    }

    public int apply() {
        return Function0$mcI$sp.Cclass.apply(this);
    }
}
