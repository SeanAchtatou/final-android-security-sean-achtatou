package scala.runtime;

import scala.Function1;
import scala.Function1$mcVL$sp;

/* compiled from: AbstractFunction1.scala */
public abstract class AbstractFunction1$mcVL$sp extends AbstractFunction1<Long, Object> implements Function1$mcVL$sp {
    public AbstractFunction1$mcVL$sp() {
        Function1$mcVL$sp.Cclass.$init$(this);
    }

    public <A> Function1<Long, A> andThen(Function1<Object, A> g) {
        return Function1$mcVL$sp.Cclass.andThen(this, g);
    }

    public <A> Function1<Long, A> andThen$mcVL$sp(Function1<Object, A> g) {
        return Function1$mcVL$sp.Cclass.andThen$mcVL$sp(this, g);
    }

    public /* bridge */ /* synthetic */ Object apply(Object v1) {
        apply(BoxesRunTime.unboxToLong(v1));
        return BoxedUnit.UNIT;
    }

    public void apply(long v1) {
        Function1$mcVL$sp.Cclass.apply(this, v1);
    }

    public <A> Function1<A, Object> compose(Function1<A, Long> g) {
        return Function1$mcVL$sp.Cclass.compose(this, g);
    }

    public <A> Function1<A, Object> compose$mcVL$sp(Function1<A, Long> g) {
        return Function1$mcVL$sp.Cclass.compose$mcVL$sp(this, g);
    }
}
