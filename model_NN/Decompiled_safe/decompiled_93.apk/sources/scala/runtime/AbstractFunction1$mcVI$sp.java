package scala.runtime;

import scala.Function1;
import scala.Function1$mcVI$sp;

/* compiled from: AbstractFunction1.scala */
public abstract class AbstractFunction1$mcVI$sp extends AbstractFunction1<Integer, Object> implements Function1$mcVI$sp {
    public AbstractFunction1$mcVI$sp() {
        Function1$mcVI$sp.Cclass.$init$(this);
    }

    public <A> Function1<Integer, A> andThen(Function1<Object, A> g) {
        return Function1$mcVI$sp.Cclass.andThen(this, g);
    }

    public <A> Function1<Integer, A> andThen$mcVI$sp(Function1<Object, A> g) {
        return Function1$mcVI$sp.Cclass.andThen$mcVI$sp(this, g);
    }

    public /* bridge */ /* synthetic */ Object apply(Object v1) {
        apply(BoxesRunTime.unboxToInt(v1));
        return BoxedUnit.UNIT;
    }

    public void apply(int v1) {
        Function1$mcVI$sp.Cclass.apply(this, v1);
    }

    public <A> Function1<A, Object> compose(Function1<A, Integer> g) {
        return Function1$mcVI$sp.Cclass.compose(this, g);
    }

    public <A> Function1<A, Object> compose$mcVI$sp(Function1<A, Integer> g) {
        return Function1$mcVI$sp.Cclass.compose$mcVI$sp(this, g);
    }
}
