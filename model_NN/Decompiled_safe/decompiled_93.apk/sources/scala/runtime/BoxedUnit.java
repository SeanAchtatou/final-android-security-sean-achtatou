package scala.runtime;

import java.io.Serializable;

public final class BoxedUnit implements Serializable {
    public static final Class<Void> TYPE = Void.TYPE;
    public static final BoxedUnit UNIT = new BoxedUnit();

    private BoxedUnit() {
    }

    public boolean equals(Object obj) {
        return this == obj;
    }

    public int hashCode() {
        return 0;
    }

    public String toString() {
        return "()";
    }
}
