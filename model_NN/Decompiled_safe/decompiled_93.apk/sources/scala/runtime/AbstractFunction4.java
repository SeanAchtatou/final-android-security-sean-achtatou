package scala.runtime;

import scala.Function1;
import scala.Function4;
import scala.ScalaObject;
import scala.Tuple4;

/* compiled from: AbstractFunction4.scala */
public abstract class AbstractFunction4<T1, T2, T3, T4, R> implements Function4<T1, T2, T3, T4, R>, ScalaObject {
    public AbstractFunction4() {
        Function4.Cclass.$init$(this);
    }

    public Function1<T1, Function1<T2, Function1<T3, Function1<T4, R>>>> curried() {
        return Function4.Cclass.curried(this);
    }

    public Function1<T1, Function1<T2, Function1<T3, Function1<T4, R>>>> curry() {
        return Function4.Cclass.curry(this);
    }

    public String toString() {
        return Function4.Cclass.toString(this);
    }

    public Function1<Tuple4<T1, T2, T3, T4>, R> tupled() {
        return Function4.Cclass.tupled(this);
    }
}
