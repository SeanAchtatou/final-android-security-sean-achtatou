package scala.runtime;

import scala.Function1;
import scala.ScalaObject;

/* compiled from: AbstractFunction1.scala */
public abstract class AbstractFunction1<T1, R> implements Function1<T1, R>, ScalaObject {
    public AbstractFunction1() {
        Function1.Cclass.$init$(this);
    }

    public <A> Function1<T1, A> andThen(Function1<R, A> g) {
        return Function1.Cclass.andThen(this, g);
    }

    public <A> Function1<Double, A> andThen$mcDD$sp(Function1<Double, A> g) {
        return Function1.Cclass.andThen$mcDD$sp(this, g);
    }

    public <A> Function1<Float, A> andThen$mcDF$sp(Function1<Double, A> g) {
        return Function1.Cclass.andThen$mcDF$sp(this, g);
    }

    public <A> Function1<Integer, A> andThen$mcDI$sp(Function1<Double, A> g) {
        return Function1.Cclass.andThen$mcDI$sp(this, g);
    }

    public <A> Function1<Long, A> andThen$mcDL$sp(Function1<Double, A> g) {
        return Function1.Cclass.andThen$mcDL$sp(this, g);
    }

    public <A> Function1<Double, A> andThen$mcFD$sp(Function1<Float, A> g) {
        return Function1.Cclass.andThen$mcFD$sp(this, g);
    }

    public <A> Function1<Float, A> andThen$mcFF$sp(Function1<Float, A> g) {
        return Function1.Cclass.andThen$mcFF$sp(this, g);
    }

    public <A> Function1<Integer, A> andThen$mcFI$sp(Function1<Float, A> g) {
        return Function1.Cclass.andThen$mcFI$sp(this, g);
    }

    public <A> Function1<Long, A> andThen$mcFL$sp(Function1<Float, A> g) {
        return Function1.Cclass.andThen$mcFL$sp(this, g);
    }

    public <A> Function1<Double, A> andThen$mcID$sp(Function1<Integer, A> g) {
        return Function1.Cclass.andThen$mcID$sp(this, g);
    }

    public <A> Function1<Float, A> andThen$mcIF$sp(Function1<Integer, A> g) {
        return Function1.Cclass.andThen$mcIF$sp(this, g);
    }

    public <A> Function1<Integer, A> andThen$mcII$sp(Function1<Integer, A> g) {
        return Function1.Cclass.andThen$mcII$sp(this, g);
    }

    public <A> Function1<Long, A> andThen$mcIL$sp(Function1<Integer, A> g) {
        return Function1.Cclass.andThen$mcIL$sp(this, g);
    }

    public <A> Function1<Double, A> andThen$mcLD$sp(Function1<Long, A> g) {
        return Function1.Cclass.andThen$mcLD$sp(this, g);
    }

    public <A> Function1<Float, A> andThen$mcLF$sp(Function1<Long, A> g) {
        return Function1.Cclass.andThen$mcLF$sp(this, g);
    }

    public <A> Function1<Integer, A> andThen$mcLI$sp(Function1<Long, A> g) {
        return Function1.Cclass.andThen$mcLI$sp(this, g);
    }

    public <A> Function1<Long, A> andThen$mcLL$sp(Function1<Long, A> g) {
        return Function1.Cclass.andThen$mcLL$sp(this, g);
    }

    public <A> Function1<Double, A> andThen$mcVD$sp(Function1<Object, A> g) {
        return Function1.Cclass.andThen$mcVD$sp(this, g);
    }

    public <A> Function1<Float, A> andThen$mcVF$sp(Function1<Object, A> g) {
        return Function1.Cclass.andThen$mcVF$sp(this, g);
    }

    public <A> Function1<Integer, A> andThen$mcVI$sp(Function1<Object, A> g) {
        return Function1.Cclass.andThen$mcVI$sp(this, g);
    }

    public <A> Function1<Long, A> andThen$mcVL$sp(Function1<Object, A> g) {
        return Function1.Cclass.andThen$mcVL$sp(this, g);
    }

    public <A> Function1<Double, A> andThen$mcZD$sp(Function1<Boolean, A> g) {
        return Function1.Cclass.andThen$mcZD$sp(this, g);
    }

    public <A> Function1<Float, A> andThen$mcZF$sp(Function1<Boolean, A> g) {
        return Function1.Cclass.andThen$mcZF$sp(this, g);
    }

    public <A> Function1<Integer, A> andThen$mcZI$sp(Function1<Boolean, A> g) {
        return Function1.Cclass.andThen$mcZI$sp(this, g);
    }

    public <A> Function1<Long, A> andThen$mcZL$sp(Function1<Boolean, A> g) {
        return Function1.Cclass.andThen$mcZL$sp(this, g);
    }

    public double apply$mcDD$sp(double v1) {
        return Function1.Cclass.apply$mcDD$sp(this, v1);
    }

    public double apply$mcDF$sp(float v1) {
        return Function1.Cclass.apply$mcDF$sp(this, v1);
    }

    public double apply$mcDI$sp(int v1) {
        return Function1.Cclass.apply$mcDI$sp(this, v1);
    }

    public double apply$mcDL$sp(long v1) {
        return Function1.Cclass.apply$mcDL$sp(this, v1);
    }

    public float apply$mcFD$sp(double v1) {
        return Function1.Cclass.apply$mcFD$sp(this, v1);
    }

    public float apply$mcFF$sp(float v1) {
        return Function1.Cclass.apply$mcFF$sp(this, v1);
    }

    public float apply$mcFI$sp(int v1) {
        return Function1.Cclass.apply$mcFI$sp(this, v1);
    }

    public float apply$mcFL$sp(long v1) {
        return Function1.Cclass.apply$mcFL$sp(this, v1);
    }

    public int apply$mcID$sp(double v1) {
        return Function1.Cclass.apply$mcID$sp(this, v1);
    }

    public int apply$mcIF$sp(float v1) {
        return Function1.Cclass.apply$mcIF$sp(this, v1);
    }

    public int apply$mcII$sp(int v1) {
        return Function1.Cclass.apply$mcII$sp(this, v1);
    }

    public int apply$mcIL$sp(long v1) {
        return Function1.Cclass.apply$mcIL$sp(this, v1);
    }

    public long apply$mcLD$sp(double v1) {
        return Function1.Cclass.apply$mcLD$sp(this, v1);
    }

    public long apply$mcLF$sp(float v1) {
        return Function1.Cclass.apply$mcLF$sp(this, v1);
    }

    public long apply$mcLI$sp(int v1) {
        return Function1.Cclass.apply$mcLI$sp(this, v1);
    }

    public long apply$mcLL$sp(long v1) {
        return Function1.Cclass.apply$mcLL$sp(this, v1);
    }

    public void apply$mcVD$sp(double v1) {
        Function1.Cclass.apply$mcVD$sp(this, v1);
    }

    public void apply$mcVF$sp(float v1) {
        Function1.Cclass.apply$mcVF$sp(this, v1);
    }

    public void apply$mcVI$sp(int v1) {
        Function1.Cclass.apply$mcVI$sp(this, v1);
    }

    public void apply$mcVL$sp(long v1) {
        Function1.Cclass.apply$mcVL$sp(this, v1);
    }

    public boolean apply$mcZD$sp(double v1) {
        return Function1.Cclass.apply$mcZD$sp(this, v1);
    }

    public boolean apply$mcZF$sp(float v1) {
        return Function1.Cclass.apply$mcZF$sp(this, v1);
    }

    public boolean apply$mcZI$sp(int v1) {
        return Function1.Cclass.apply$mcZI$sp(this, v1);
    }

    public boolean apply$mcZL$sp(long v1) {
        return Function1.Cclass.apply$mcZL$sp(this, v1);
    }

    public <A> Function1<A, R> compose(Function1<A, T1> g) {
        return Function1.Cclass.compose(this, g);
    }

    public <A> Function1<A, Double> compose$mcDD$sp(Function1<A, Double> g) {
        return Function1.Cclass.compose$mcDD$sp(this, g);
    }

    public <A> Function1<A, Double> compose$mcDF$sp(Function1<A, Float> g) {
        return Function1.Cclass.compose$mcDF$sp(this, g);
    }

    public <A> Function1<A, Double> compose$mcDI$sp(Function1<A, Integer> g) {
        return Function1.Cclass.compose$mcDI$sp(this, g);
    }

    public <A> Function1<A, Double> compose$mcDL$sp(Function1<A, Long> g) {
        return Function1.Cclass.compose$mcDL$sp(this, g);
    }

    public <A> Function1<A, Float> compose$mcFD$sp(Function1<A, Double> g) {
        return Function1.Cclass.compose$mcFD$sp(this, g);
    }

    public <A> Function1<A, Float> compose$mcFF$sp(Function1<A, Float> g) {
        return Function1.Cclass.compose$mcFF$sp(this, g);
    }

    public <A> Function1<A, Float> compose$mcFI$sp(Function1<A, Integer> g) {
        return Function1.Cclass.compose$mcFI$sp(this, g);
    }

    public <A> Function1<A, Float> compose$mcFL$sp(Function1<A, Long> g) {
        return Function1.Cclass.compose$mcFL$sp(this, g);
    }

    public <A> Function1<A, Integer> compose$mcID$sp(Function1<A, Double> g) {
        return Function1.Cclass.compose$mcID$sp(this, g);
    }

    public <A> Function1<A, Integer> compose$mcIF$sp(Function1<A, Float> g) {
        return Function1.Cclass.compose$mcIF$sp(this, g);
    }

    public <A> Function1<A, Integer> compose$mcII$sp(Function1<A, Integer> g) {
        return Function1.Cclass.compose$mcII$sp(this, g);
    }

    public <A> Function1<A, Integer> compose$mcIL$sp(Function1<A, Long> g) {
        return Function1.Cclass.compose$mcIL$sp(this, g);
    }

    public <A> Function1<A, Long> compose$mcLD$sp(Function1<A, Double> g) {
        return Function1.Cclass.compose$mcLD$sp(this, g);
    }

    public <A> Function1<A, Long> compose$mcLF$sp(Function1<A, Float> g) {
        return Function1.Cclass.compose$mcLF$sp(this, g);
    }

    public <A> Function1<A, Long> compose$mcLI$sp(Function1<A, Integer> g) {
        return Function1.Cclass.compose$mcLI$sp(this, g);
    }

    public <A> Function1<A, Long> compose$mcLL$sp(Function1<A, Long> g) {
        return Function1.Cclass.compose$mcLL$sp(this, g);
    }

    public <A> Function1<A, Object> compose$mcVD$sp(Function1<A, Double> g) {
        return Function1.Cclass.compose$mcVD$sp(this, g);
    }

    public <A> Function1<A, Object> compose$mcVF$sp(Function1<A, Float> g) {
        return Function1.Cclass.compose$mcVF$sp(this, g);
    }

    public <A> Function1<A, Object> compose$mcVI$sp(Function1<A, Integer> g) {
        return Function1.Cclass.compose$mcVI$sp(this, g);
    }

    public <A> Function1<A, Object> compose$mcVL$sp(Function1<A, Long> g) {
        return Function1.Cclass.compose$mcVL$sp(this, g);
    }

    public <A> Function1<A, Boolean> compose$mcZD$sp(Function1<A, Double> g) {
        return Function1.Cclass.compose$mcZD$sp(this, g);
    }

    public <A> Function1<A, Boolean> compose$mcZF$sp(Function1<A, Float> g) {
        return Function1.Cclass.compose$mcZF$sp(this, g);
    }

    public <A> Function1<A, Boolean> compose$mcZI$sp(Function1<A, Integer> g) {
        return Function1.Cclass.compose$mcZI$sp(this, g);
    }

    public <A> Function1<A, Boolean> compose$mcZL$sp(Function1<A, Long> g) {
        return Function1.Cclass.compose$mcZL$sp(this, g);
    }

    public String toString() {
        return Function1.Cclass.toString(this);
    }
}
