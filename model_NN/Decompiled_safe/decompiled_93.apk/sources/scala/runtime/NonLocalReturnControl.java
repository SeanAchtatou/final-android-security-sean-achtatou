package scala.runtime;

import scala.ScalaObject;
import scala.util.control.ControlThrowable;
import scala.util.control.NoStackTrace;

/* compiled from: NonLocalReturnControl.scala */
public class NonLocalReturnControl<T> extends Throwable implements ControlThrowable, ScalaObject {
    private final Object key;
    private final T value;

    public NonLocalReturnControl(Object key2, T value2) {
        this.key = key2;
        this.value = value2;
        NoStackTrace.Cclass.$init$(this);
    }

    public Throwable fillInStackTrace() {
        return NoStackTrace.Cclass.fillInStackTrace(this);
    }

    public Object key() {
        return this.key;
    }

    public T value() {
        return this.value;
    }
}
