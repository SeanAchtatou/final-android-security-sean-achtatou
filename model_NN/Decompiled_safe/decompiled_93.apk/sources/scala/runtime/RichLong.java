package scala.runtime;

import scala.Proxy;
import scala.ScalaObject;
import scala.math.Ordered;

/* compiled from: RichLong.scala */
public final class RichLong implements Proxy, Ordered<Long>, ScalaObject {
    public final long x;

    public RichLong(long x2) {
        this.x = x2;
        Proxy.Cclass.$init$(this);
        Ordered.Cclass.$init$(this);
    }

    public boolean $less(Object that) {
        return Ordered.Cclass.$less(this, that);
    }

    public /* bridge */ /* synthetic */ int compare(Object that) {
        return compare(BoxesRunTime.unboxToLong(that));
    }

    public int compareTo(Object that) {
        return Ordered.Cclass.compareTo(this, that);
    }

    public boolean equals(Object that) {
        return Proxy.Cclass.equals(this, that);
    }

    public int hashCode() {
        return Proxy.Cclass.hashCode(this);
    }

    public String toString() {
        return Proxy.Cclass.toString(this);
    }

    public Object self() {
        return BoxesRunTime.boxToLong(this.x);
    }

    public int compare(long y) {
        if (this.x < y) {
            return -1;
        }
        return this.x > y ? 1 : 0;
    }

    public long min(long y) {
        return this.x < y ? this.x : y;
    }

    public long max(long y) {
        return this.x > y ? this.x : y;
    }
}
