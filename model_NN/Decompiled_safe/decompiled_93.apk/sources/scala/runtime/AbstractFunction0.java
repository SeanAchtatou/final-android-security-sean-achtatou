package scala.runtime;

import scala.Function0;
import scala.ScalaObject;

/* compiled from: AbstractFunction0.scala */
public abstract class AbstractFunction0<R> implements Function0<R>, ScalaObject {
    public AbstractFunction0() {
        Function0.Cclass.$init$(this);
    }

    public byte apply$mcB$sp() {
        return Function0.Cclass.apply$mcB$sp(this);
    }

    public char apply$mcC$sp() {
        return Function0.Cclass.apply$mcC$sp(this);
    }

    public double apply$mcD$sp() {
        return Function0.Cclass.apply$mcD$sp(this);
    }

    public float apply$mcF$sp() {
        return Function0.Cclass.apply$mcF$sp(this);
    }

    public int apply$mcI$sp() {
        return Function0.Cclass.apply$mcI$sp(this);
    }

    public long apply$mcL$sp() {
        return Function0.Cclass.apply$mcL$sp(this);
    }

    public short apply$mcS$sp() {
        return Function0.Cclass.apply$mcS$sp(this);
    }

    public void apply$mcV$sp() {
        Function0.Cclass.apply$mcV$sp(this);
    }

    public boolean apply$mcZ$sp() {
        return Function0.Cclass.apply$mcZ$sp(this);
    }

    public String toString() {
        return Function0.Cclass.toString(this);
    }
}
