package scala.runtime;

import java.io.Serializable;

public class IntRef implements Serializable {
    public int elem;

    public IntRef(int i) {
        this.elem = i;
    }

    public String toString() {
        return Integer.toString(this.elem);
    }
}
