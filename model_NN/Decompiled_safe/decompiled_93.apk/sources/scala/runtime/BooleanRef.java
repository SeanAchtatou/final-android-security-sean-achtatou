package scala.runtime;

import java.io.Serializable;

public class BooleanRef implements Serializable {
    public boolean elem;

    public BooleanRef(boolean z) {
        this.elem = z;
    }

    public String toString() {
        return "" + this.elem;
    }
}
