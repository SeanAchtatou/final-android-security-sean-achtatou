package scala.runtime;

import scala.Function1;
import scala.Function2$mcIII$sp;
import scala.Tuple2;

/* compiled from: AbstractFunction2.scala */
public abstract class AbstractFunction2$mcIII$sp extends AbstractFunction2<Integer, Integer, Integer> implements Function2$mcIII$sp {
    public AbstractFunction2$mcIII$sp() {
        Function2$mcIII$sp.Cclass.$init$(this);
    }

    public int apply(int v1, int v2) {
        return Function2$mcIII$sp.Cclass.apply(this, v1, v2);
    }

    public /* bridge */ /* synthetic */ Object apply(Object v1, Object v2) {
        return BoxesRunTime.boxToInteger(apply(BoxesRunTime.unboxToInt(v1), BoxesRunTime.unboxToInt(v2)));
    }

    public Function1<Integer, Function1<Integer, Integer>> curried() {
        return Function2$mcIII$sp.Cclass.curried(this);
    }

    public Function1<Integer, Function1<Integer, Integer>> curried$mcIII$sp() {
        return Function2$mcIII$sp.Cclass.curried$mcIII$sp(this);
    }

    public Function1<Integer, Function1<Integer, Integer>> curry() {
        return Function2$mcIII$sp.Cclass.curry(this);
    }

    public Function1<Integer, Function1<Integer, Integer>> curry$mcIII$sp() {
        return Function2$mcIII$sp.Cclass.curry$mcIII$sp(this);
    }

    public Function1<Tuple2<Integer, Integer>, Integer> tupled() {
        return Function2$mcIII$sp.Cclass.tupled(this);
    }

    public Function1<Tuple2<Integer, Integer>, Integer> tupled$mcIII$sp() {
        return Function2$mcIII$sp.Cclass.tupled$mcIII$sp(this);
    }
}
