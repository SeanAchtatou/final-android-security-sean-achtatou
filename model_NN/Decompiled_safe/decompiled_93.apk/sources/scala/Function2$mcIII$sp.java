package scala;

/* compiled from: Function2.scala */
public interface Function2$mcIII$sp extends Function2<Integer, Integer, Integer> {
    int apply(int i, int i2);

    Function1<Integer, Function1<Integer, Integer>> curried$mcIII$sp();

    Function1<Integer, Function1<Integer, Integer>> curry$mcIII$sp();

    Function1<Tuple2<Integer, Integer>, Integer> tupled$mcIII$sp();

    /* renamed from: scala.Function2$mcIII$sp$class  reason: invalid class name */
    /* compiled from: Function2.scala */
    public abstract class Cclass {
        public static void $init$(Function2$mcIII$sp $this) {
        }

        public static int apply(Function2$mcIII$sp $this, int v1, int v2) {
            return $this.apply$mcIII$sp(v1, v2);
        }

        public static Function1 curried(Function2$mcIII$sp $this) {
            return $this.curried$mcIII$sp();
        }

        public static Function1 curried$mcIII$sp(Function2$mcIII$sp $this) {
            return new Function2$mcIII$sp$$anonfun$curried$mcIII$sp$1($this);
        }

        public static Function1 curry(Function2$mcIII$sp $this) {
            return $this.curry$mcIII$sp();
        }

        public static Function1 curry$mcIII$sp(Function2$mcIII$sp $this) {
            return $this.curried$mcIII$sp();
        }

        public static Function1 tupled(Function2$mcIII$sp $this) {
            return $this.tupled$mcIII$sp();
        }

        public static Function1 tupled$mcIII$sp(Function2$mcIII$sp $this) {
            return new Function2$mcIII$sp$$anonfun$tupled$mcIII$sp$1($this);
        }
    }
}
