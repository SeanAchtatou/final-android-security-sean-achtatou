package scala;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

/* compiled from: Function2.scala */
public final class Function2$mcIII$sp$$anonfun$tupled$mcIII$sp$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Function2$mcIII$sp $outer;

    public Function2$mcIII$sp$$anonfun$tupled$mcIII$sp$1(Function2$mcIII$sp $outer2) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
    }

    public final int apply(Tuple2<Integer, Integer> x0$1) {
        if (x0$1 != null) {
            return this.$outer.apply(BoxesRunTime.unboxToInt(x0$1._1()), BoxesRunTime.unboxToInt(x0$1._2()));
        }
        throw new MatchError(x0$1);
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return BoxesRunTime.boxToInteger(apply((Tuple2<Integer, Integer>) ((Tuple2) v1)));
    }
}
