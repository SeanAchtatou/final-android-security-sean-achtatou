package scala;

import scala.collection.Iterator;

/* compiled from: Product.scala */
public interface Product extends Equals, ScalaObject {
    int productArity();

    Object productElement(int i);

    Iterator<Object> productIterator();

    String productPrefix();

    /* renamed from: scala.Product$class  reason: invalid class name */
    /* compiled from: Product.scala */
    public abstract class Cclass {
        public static void $init$(Product $this) {
        }

        public static Iterator productIterator(Product $this) {
            return new Product$$anon$1($this);
        }

        public static Iterator productElements(Product $this) {
            return $this.productIterator();
        }

        public static String productPrefix(Product $this) {
            return "";
        }
    }
}
