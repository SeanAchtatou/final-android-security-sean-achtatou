package scala;

import scala.collection.generic.CanBuildFrom;
import scala.collection.immutable.IndexedSeq;
import scala.collection.mutable.WrappedArray;
import scala.collection.mutable.WrappedArray$;
import scala.runtime.BoxedUnit;

/* compiled from: LowPriorityImplicits.scala */
public class LowPriorityImplicits implements ScalaObject {
    public <T> WrappedArray<T> genericWrapArray(Object xs) {
        if (xs != null) {
            return WrappedArray$.MODULE$.make(xs);
        }
        return null;
    }

    public <T> WrappedArray<T> wrapRefArray(T[] xs) {
        if (xs != null) {
            return new WrappedArray.ofRef(xs);
        }
        return null;
    }

    public WrappedArray<Integer> wrapIntArray(int[] xs) {
        if (xs != null) {
            return new WrappedArray.ofInt(xs);
        }
        return null;
    }

    public WrappedArray<Double> wrapDoubleArray(double[] xs) {
        if (xs != null) {
            return new WrappedArray.ofDouble(xs);
        }
        return null;
    }

    public WrappedArray<Long> wrapLongArray(long[] xs) {
        if (xs != null) {
            return new WrappedArray.ofLong(xs);
        }
        return null;
    }

    public WrappedArray<Float> wrapFloatArray(float[] xs) {
        if (xs != null) {
            return new WrappedArray.ofFloat(xs);
        }
        return null;
    }

    public WrappedArray<Character> wrapCharArray(char[] xs) {
        if (xs != null) {
            return new WrappedArray.ofChar(xs);
        }
        return null;
    }

    public WrappedArray<Byte> wrapByteArray(byte[] xs) {
        if (xs != null) {
            return new WrappedArray.ofByte(xs);
        }
        return null;
    }

    public WrappedArray<Short> wrapShortArray(short[] xs) {
        if (xs != null) {
            return new WrappedArray.ofShort(xs);
        }
        return null;
    }

    public WrappedArray<Boolean> wrapBooleanArray(boolean[] xs) {
        if (xs != null) {
            return new WrappedArray.ofBoolean(xs);
        }
        return null;
    }

    public WrappedArray<Object> wrapUnitArray(BoxedUnit[] xs) {
        if (xs != null) {
            return new WrappedArray.ofUnit(xs);
        }
        return null;
    }

    public <T> CanBuildFrom<String, T, IndexedSeq<T>> fallbackStringCanBuildFrom() {
        return new LowPriorityImplicits$$anon$1(this);
    }
}
