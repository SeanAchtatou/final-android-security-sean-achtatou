package scala;

/* compiled from: PartialFunction.scala */
public interface PartialFunction<A, B> extends Function1<A, B>, ScalaObject {
    <C> PartialFunction<A, C> andThen(Function1 function1);

    boolean isDefinedAt(Object obj);

    /* renamed from: scala.PartialFunction$class  reason: invalid class name */
    /* compiled from: PartialFunction.scala */
    public abstract class Cclass {
        public static void $init$(PartialFunction $this) {
        }

        public static PartialFunction andThen(PartialFunction $this, Function1 k$1) {
            return new PartialFunction$$anon$2($this, k$1);
        }
    }
}
