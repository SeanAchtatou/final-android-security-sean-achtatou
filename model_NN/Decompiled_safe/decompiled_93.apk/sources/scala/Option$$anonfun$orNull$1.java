package scala;

import java.io.Serializable;
import scala.runtime.AbstractFunction0;

/* compiled from: Option.scala */
public final class Option$$anonfun$orNull$1 extends AbstractFunction0 implements Serializable {
    public static final long serialVersionUID = 0;
    private final /* synthetic */ Predef$$less$colon$less ev$1;

    /* JADX WARN: Type inference failed for: r2v0, types: [scala.Predef$$less$colon$less, scala.Option<A>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Option$$anonfun$orNull$1(scala.Option r1, scala.Option<A> r2) {
        /*
            r0 = this;
            r0.ev$1 = r2
            r0.<init>()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.Option$$anonfun$orNull$1.<init>(scala.Option, scala.Predef$$less$colon$less):void");
    }

    public final A1 apply() {
        return this.ev$1.apply(null);
    }
}
