package scala.math;

import java.io.Serializable;
import java.math.MathContext;
import scala.None$;
import scala.Option;
import scala.ScalaObject;
import scala.Some;
import scala.math.ScalaNumericConversions;
import scala.runtime.BoxesRunTime;

/* compiled from: BigDecimal.scala */
public class BigDecimal extends ScalaNumber implements Serializable, ScalaObject, ScalaNumericConversions {
    private final java.math.BigDecimal bigDecimal;
    private final MathContext mc;

    public boolean isValidByte() {
        return ScalaNumericConversions.Cclass.isValidByte(this);
    }

    public boolean isValidChar() {
        return ScalaNumericConversions.Cclass.isValidChar(this);
    }

    public boolean isValidInt() {
        return ScalaNumericConversions.Cclass.isValidInt(this);
    }

    public boolean isValidShort() {
        return ScalaNumericConversions.Cclass.isValidShort(this);
    }

    public byte toByte() {
        return ScalaNumericConversions.Cclass.toByte(this);
    }

    public double toDouble() {
        return ScalaNumericConversions.Cclass.toDouble(this);
    }

    public float toFloat() {
        return ScalaNumericConversions.Cclass.toFloat(this);
    }

    public int toInt() {
        return ScalaNumericConversions.Cclass.toInt(this);
    }

    public long toLong() {
        return ScalaNumericConversions.Cclass.toLong(this);
    }

    public short toShort() {
        return ScalaNumericConversions.Cclass.toShort(this);
    }

    public boolean unifiedPrimitiveEquals(Object x) {
        return ScalaNumericConversions.Cclass.unifiedPrimitiveEquals(this, x);
    }

    public int unifiedPrimitiveHashcode() {
        return ScalaNumericConversions.Cclass.unifiedPrimitiveHashcode(this);
    }

    public BigDecimal(java.math.BigDecimal bigDecimal2, MathContext mc2) {
        this.bigDecimal = bigDecimal2;
        this.mc = mc2;
        ScalaNumericConversions.Cclass.$init$(this);
    }

    public java.math.BigDecimal bigDecimal() {
        return this.bigDecimal;
    }

    public MathContext mc() {
        return this.mc;
    }

    private BigDecimal bigdec2BigDecimal(java.math.BigDecimal x) {
        return new BigDecimal(x, mc());
    }

    public int hashCode() {
        if (isWhole()) {
            return unifiedPrimitiveHashcode();
        }
        double doubleValue = doubleValue();
        int iv0 = (int) doubleValue;
        if (((double) iv0) == doubleValue) {
            return iv0;
        }
        long lv0 = (long) doubleValue;
        return ((double) lv0) == doubleValue ? BoxesRunTime.boxToLong(lv0).hashCode() : BoxesRunTime.boxToDouble(doubleValue).hashCode();
    }

    public boolean equals(Object that) {
        if (that instanceof BigDecimal) {
            return equals((BigDecimal) that);
        }
        if (that instanceof BigInt) {
            return toBigIntExact().exists(new BigDecimal$$anonfun$equals$1(this, (BigInt) that));
        }
        if ((that instanceof Float) || (that instanceof Double)) {
            return unifiedPrimitiveEquals(that);
        }
        return isWhole() && $less$eq(BigDecimal$.MODULE$.MaxLong()) && $greater$eq(BigDecimal$.MODULE$.MinLong()) && unifiedPrimitiveEquals(that);
    }

    public boolean isWhole() {
        BigDecimal remainder = remainder(BigDecimal$.MODULE$.int2bigDecimal(1));
        BigDecimal apply = BigDecimal$.MODULE$.apply(0);
        return remainder == apply ? true : remainder == null ? false : remainder instanceof Number ? BoxesRunTime.equalsNumObject(remainder, apply) : remainder instanceof Character ? BoxesRunTime.equalsCharObject((Character) remainder, apply) : remainder.equals(apply);
    }

    public java.math.BigDecimal underlying() {
        return bigDecimal();
    }

    public boolean equals(BigDecimal that) {
        return compare(that) == 0;
    }

    public int compare(BigDecimal that) {
        return bigDecimal().compareTo(that.bigDecimal());
    }

    public boolean $less$eq(BigDecimal that) {
        return compare(that) <= 0;
    }

    public boolean $greater$eq(BigDecimal that) {
        return compare(that) >= 0;
    }

    public BigDecimal remainder(BigDecimal that) {
        return bigdec2BigDecimal(bigDecimal().remainder(that.bigDecimal(), mc()));
    }

    public byte byteValue() {
        return (byte) intValue();
    }

    public short shortValue() {
        return (short) intValue();
    }

    public int intValue() {
        return bigDecimal().intValue();
    }

    public long longValue() {
        return bigDecimal().longValue();
    }

    public float floatValue() {
        return bigDecimal().floatValue();
    }

    public double doubleValue() {
        return bigDecimal().doubleValue();
    }

    public Option<BigInt> toBigIntExact() {
        try {
            return new Some(new BigInt(bigDecimal().toBigIntegerExact()));
        } catch (ArithmeticException e) {
            return None$.MODULE$;
        }
    }

    public String toString() {
        return bigDecimal().toString();
    }
}
