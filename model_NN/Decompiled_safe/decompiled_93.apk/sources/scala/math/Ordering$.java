package scala.math;

import java.io.Serializable;
import scala.ScalaObject;
import scala.math.LowPriorityOrderingImplicits;

/* compiled from: Ordering.scala */
public final class Ordering$ implements Serializable, ScalaObject, LowPriorityOrderingImplicits {
    public static final Ordering$ MODULE$ = null;

    static {
        new Ordering$();
    }

    private Ordering$() {
        MODULE$ = this;
        LowPriorityOrderingImplicits.Cclass.$init$(this);
    }
}
