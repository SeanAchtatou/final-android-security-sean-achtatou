package scala.math;

import java.io.Serializable;
import java.math.BigInteger;
import scala.ScalaObject;

/* compiled from: BigInt.scala */
public final class BigInt$ implements Serializable, ScalaObject {
    public static final BigInt$ MODULE$ = null;
    private final BigInt MaxLong = apply(Long.MAX_VALUE);
    private final BigInt MinLong = apply(Long.MIN_VALUE);
    private final BigInt[] cache = new BigInt[((maxCached() - minCached()) + 1)];
    private final int maxCached = 1024;
    private final int minCached = -1024;

    static {
        new BigInt$();
    }

    private BigInt$() {
        MODULE$ = this;
    }

    private int minCached() {
        return this.minCached;
    }

    private int maxCached() {
        return this.maxCached;
    }

    private BigInt[] cache() {
        return this.cache;
    }

    public BigInt MinLong() {
        return this.MinLong;
    }

    public BigInt MaxLong() {
        return this.MaxLong;
    }

    public BigInt apply(int i) {
        if (minCached() > i || i > maxCached()) {
            return new BigInt(BigInteger.valueOf((long) i));
        }
        int offset = i - minCached();
        BigInt n = cache()[offset];
        if (n == null) {
            n = new BigInt(BigInteger.valueOf((long) i));
            cache()[offset] = n;
        }
        return n;
    }

    public BigInt apply(long l) {
        if (((long) minCached()) > l || l > ((long) maxCached())) {
            return new BigInt(BigInteger.valueOf(l));
        }
        return apply((int) l);
    }
}
