package scala.math;

import scala.Function1;
import scala.ScalaObject;
import scala.math.Ordering;
import scala.math.PartialOrdering;
import scala.runtime.BoxesRunTime;

/* compiled from: Ordering.scala */
public final class Ordering$Int$ implements ScalaObject, Ordering.IntOrdering {
    public static final Ordering$Int$ MODULE$ = null;

    static {
        new Ordering$Int$();
    }

    public Ordering$Int$() {
        MODULE$ = this;
        PartialOrdering.Cclass.$init$(this);
        Ordering.Cclass.$init$(this);
        Ordering.IntOrdering.Cclass.$init$(this);
    }

    public int compare(int x, int y) {
        return Ordering.IntOrdering.Cclass.compare(this, x, y);
    }

    public /* bridge */ /* synthetic */ int compare(Object x, Object y) {
        return compare(BoxesRunTime.unboxToInt(x), BoxesRunTime.unboxToInt(y));
    }

    public <U> Ordering<U> on(Function1<U, Integer> f) {
        return Ordering.Cclass.on(this, f);
    }
}
