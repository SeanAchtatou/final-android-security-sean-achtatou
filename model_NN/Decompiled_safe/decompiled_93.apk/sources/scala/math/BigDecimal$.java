package scala.math;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.MathContext;
import scala.ScalaObject;

/* compiled from: BigDecimal.scala */
public final class BigDecimal$ implements Serializable, ScalaObject {
    public static final BigDecimal$ MODULE$ = null;
    private final BigDecimal MaxLong = new BigDecimal(BigDecimal.valueOf(Long.MAX_VALUE), defaultMathContext());
    private final BigDecimal MinLong = new BigDecimal(BigDecimal.valueOf(Long.MIN_VALUE), defaultMathContext());
    public volatile int bitmap$0;
    private BigDecimal[] cache;
    private final MathContext defaultMathContext = MathContext.UNLIMITED;
    private final int maxCached = 512;
    private final int minCached = -512;

    static {
        new BigDecimal$();
    }

    private BigDecimal$() {
        MODULE$ = this;
    }

    private int minCached() {
        return this.minCached;
    }

    private int maxCached() {
        return this.maxCached;
    }

    public MathContext defaultMathContext() {
        return this.defaultMathContext;
    }

    public BigDecimal MinLong() {
        return this.MinLong;
    }

    public BigDecimal MaxLong() {
        return this.MaxLong;
    }

    private BigDecimal[] cache() {
        if ((this.bitmap$0 & 1) == 0) {
            synchronized (this) {
                if ((this.bitmap$0 & 1) == 0) {
                    this.cache = new BigDecimal[((maxCached() - minCached()) + 1)];
                    this.bitmap$0 |= 1;
                }
            }
        }
        return this.cache;
    }

    public BigDecimal apply(int i) {
        return apply(i, this.defaultMathContext);
    }

    public BigDecimal apply(int i, MathContext mc) {
        MathContext mathContext = this.defaultMathContext;
        if (mc != null ? mc.equals(mathContext) : mathContext == null) {
            if (minCached() <= i && i <= maxCached()) {
                int offset = i - minCached();
                BigDecimal n = cache()[offset];
                if (n == null) {
                    n = new BigDecimal(BigDecimal.valueOf((long) i), mc);
                    cache()[offset] = n;
                }
                return n;
            }
        }
        return new BigDecimal(BigDecimal.valueOf((long) i), mc);
    }

    public BigDecimal int2bigDecimal(int i) {
        return apply(i, this.defaultMathContext);
    }
}
