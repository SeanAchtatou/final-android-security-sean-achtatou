package scala.math;

import scala.ScalaObject;

/* compiled from: Numeric.scala */
public interface Numeric<T> extends Ordering<T>, ScalaObject {
    T fromInt(int i);

    T plus(T t, T t2);

    T zero();

    /* compiled from: Numeric.scala */
    public interface IntIsIntegral extends Integral<Integer>, ScalaObject {
        int fromInt(int i);

        int plus(int i, int i2);

        /* renamed from: scala.math.Numeric$IntIsIntegral$class  reason: invalid class name */
        /* compiled from: Numeric.scala */
        public abstract class Cclass {
            public static void $init$(IntIsIntegral $this) {
            }

            public static int plus(IntIsIntegral $this, int x, int y) {
                return x + y;
            }

            public static int fromInt(IntIsIntegral $this, int x) {
                return x;
            }
        }
    }

    /* renamed from: scala.math.Numeric$class  reason: invalid class name */
    /* compiled from: Numeric.scala */
    public abstract class Cclass {
        public static void $init$(Numeric $this) {
        }

        public static Object zero(Numeric $this) {
            return $this.fromInt(0);
        }
    }
}
