package scala.math;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

/* compiled from: BigDecimal.scala */
public final class BigDecimal$$anonfun$equals$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ BigInt that$1;

    public BigDecimal$$anonfun$equals$1(BigDecimal $outer, BigInt bigInt) {
        this.that$1 = bigInt;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return BoxesRunTime.boxToBoolean(apply((BigInt) v1));
    }

    public final boolean apply(BigInt bigInt) {
        return this.that$1.equals(bigInt);
    }
}
