package scala.math;

import scala.ScalaObject;
import scala.runtime.BoxesRunTime;

/* compiled from: ScalaNumericConversions.scala */
public interface ScalaNumericConversions extends ScalaObject {
    boolean isValidByte();

    boolean isValidChar();

    boolean isValidInt();

    boolean isValidShort();

    byte toByte();

    double toDouble();

    float toFloat();

    int toInt();

    long toLong();

    short toShort();

    boolean unifiedPrimitiveEquals(Object obj);

    int unifiedPrimitiveHashcode();

    /* renamed from: scala.math.ScalaNumericConversions$class  reason: invalid class name */
    /* compiled from: ScalaNumericConversions.scala */
    public abstract class Cclass {
        public static void $init$(ScalaNumericConversions $this) {
        }

        public static byte toByte(ScalaNumericConversions $this) {
            return ((Number) $this).byteValue();
        }

        public static short toShort(ScalaNumericConversions $this) {
            return ((Number) $this).shortValue();
        }

        public static int toInt(ScalaNumericConversions $this) {
            return ((Number) $this).intValue();
        }

        public static long toLong(ScalaNumericConversions $this) {
            return ((Number) $this).longValue();
        }

        public static float toFloat(ScalaNumericConversions $this) {
            return ((Number) $this).floatValue();
        }

        public static double toDouble(ScalaNumericConversions $this) {
            return ((Number) $this).doubleValue();
        }

        public static boolean isValidByte(ScalaNumericConversions $this) {
            return ((ScalaNumber) $this).isWhole() && $this.toByte() == $this.toInt();
        }

        public static boolean isValidShort(ScalaNumericConversions $this) {
            return ((ScalaNumber) $this).isWhole() && $this.toShort() == $this.toInt();
        }

        public static boolean isValidInt(ScalaNumericConversions $this) {
            return ((ScalaNumber) $this).isWhole() && ((long) $this.toInt()) == $this.toLong();
        }

        public static boolean isValidChar(ScalaNumericConversions $this) {
            return ((ScalaNumber) $this).isWhole() && $this.toInt() >= 0 && $this.toInt() <= 65535;
        }

        public static int unifiedPrimitiveHashcode(ScalaNumericConversions $this) {
            long lv = $this.toLong();
            if (lv >= -2147483648L && lv <= 2147483647L) {
                return (int) lv;
            }
            int iv0 = (int) lv;
            return ((long) iv0) == lv ? iv0 : BoxesRunTime.boxToLong(lv).hashCode();
        }

        public static boolean unifiedPrimitiveEquals(ScalaNumericConversions $this, Object x) {
            if (x instanceof Character) {
                if (!$this.isValidChar() || $this.toInt() != BoxesRunTime.unboxToChar(x)) {
                    return false;
                }
                return true;
            } else if (x instanceof Byte) {
                if (!$this.isValidByte() || $this.toByte() != BoxesRunTime.unboxToByte(x)) {
                    return false;
                }
                return true;
            } else if (x instanceof Short) {
                if (!$this.isValidShort() || $this.toShort() != BoxesRunTime.unboxToShort(x)) {
                    return false;
                }
                return true;
            } else if (x instanceof Integer) {
                if (!$this.isValidInt() || $this.toInt() != BoxesRunTime.unboxToInt(x)) {
                    return false;
                }
                return true;
            } else if (x instanceof Long) {
                return $this.toLong() == BoxesRunTime.unboxToLong(x);
            } else {
                if (x instanceof Float) {
                    return $this.toFloat() == BoxesRunTime.unboxToFloat(x);
                }
                if (!(x instanceof Double)) {
                    return false;
                }
                if ($this.toDouble() == BoxesRunTime.unboxToDouble(x)) {
                    return true;
                }
                return false;
            }
        }
    }
}
