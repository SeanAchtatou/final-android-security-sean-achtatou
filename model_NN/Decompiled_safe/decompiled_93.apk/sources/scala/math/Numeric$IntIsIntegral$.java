package scala.math;

import scala.Function1;
import scala.ScalaObject;
import scala.math.Integral;
import scala.math.Numeric;
import scala.math.Ordering;
import scala.math.PartialOrdering;
import scala.runtime.BoxesRunTime;

/* compiled from: Numeric.scala */
public final class Numeric$IntIsIntegral$ implements ScalaObject, Numeric.IntIsIntegral, Ordering.IntOrdering {
    public static final Numeric$IntIsIntegral$ MODULE$ = null;

    static {
        new Numeric$IntIsIntegral$();
    }

    public Numeric$IntIsIntegral$() {
        MODULE$ = this;
        PartialOrdering.Cclass.$init$(this);
        Ordering.Cclass.$init$(this);
        Numeric.Cclass.$init$(this);
        Integral.Cclass.$init$(this);
        Numeric.IntIsIntegral.Cclass.$init$(this);
        Ordering.IntOrdering.Cclass.$init$(this);
    }

    public int compare(int x, int y) {
        return Ordering.IntOrdering.Cclass.compare(this, x, y);
    }

    public /* bridge */ /* synthetic */ int compare(Object x, Object y) {
        return compare(BoxesRunTime.unboxToInt(x), BoxesRunTime.unboxToInt(y));
    }

    public int fromInt(int x) {
        return Numeric.IntIsIntegral.Cclass.fromInt(this, x);
    }

    public <U> Ordering<U> on(Function1<U, Integer> f) {
        return Ordering.Cclass.on(this, f);
    }

    public int plus(int x, int y) {
        return Numeric.IntIsIntegral.Cclass.plus(this, x, y);
    }

    public /* bridge */ /* synthetic */ Object plus(Object x, Object y) {
        return BoxesRunTime.boxToInteger(plus(BoxesRunTime.unboxToInt(x), BoxesRunTime.unboxToInt(y)));
    }

    public Object zero() {
        return Numeric.Cclass.zero(this);
    }
}
