package scala.math;

import scala.Function1;
import scala.math.Ordering;

/* compiled from: Ordering.scala */
public final class Ordering$$anon$2 implements Ordering<Object> {
    private final /* synthetic */ Ordering $outer;
    private final /* synthetic */ Function1 f$1;

    /* JADX WARN: Type inference failed for: r3v0, types: [scala.Function1, scala.math.Ordering<T>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Ordering$$anon$2(scala.math.Ordering r2, scala.math.Ordering<T> r3) {
        /*
            r1 = this;
            if (r2 != 0) goto L_0x0008
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            r0.<init>()
            throw r0
        L_0x0008:
            r1.$outer = r2
            r1.f$1 = r3
            r1.<init>()
            scala.math.PartialOrdering.Cclass.$init$(r1)
            scala.math.Ordering.Cclass.$init$(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.math.Ordering$$anon$2.<init>(scala.math.Ordering, scala.Function1):void");
    }

    public <U> Ordering<U> on(Function1<U, U> f) {
        return Ordering.Cclass.on(this, f);
    }

    public int compare(U x, U y) {
        return this.$outer.compare(this.f$1.apply(x), this.f$1.apply(y));
    }
}
