package scala.math;

import java.io.Serializable;
import java.math.BigInteger;
import scala.ScalaObject;
import scala.math.ScalaNumericConversions;
import scala.runtime.BoxesRunTime;

/* compiled from: BigInt.scala */
public class BigInt extends ScalaNumber implements Serializable, ScalaObject, ScalaNumericConversions {
    private final BigInteger bigInteger;

    public BigInt(BigInteger bigInteger2) {
        this.bigInteger = bigInteger2;
        ScalaNumericConversions.Cclass.$init$(this);
    }

    public BigInteger bigInteger() {
        return this.bigInteger;
    }

    public boolean isValidByte() {
        return ScalaNumericConversions.Cclass.isValidByte(this);
    }

    public boolean isValidChar() {
        return ScalaNumericConversions.Cclass.isValidChar(this);
    }

    public boolean isValidInt() {
        return ScalaNumericConversions.Cclass.isValidInt(this);
    }

    public boolean isValidShort() {
        return ScalaNumericConversions.Cclass.isValidShort(this);
    }

    public byte toByte() {
        return ScalaNumericConversions.Cclass.toByte(this);
    }

    public double toDouble() {
        return ScalaNumericConversions.Cclass.toDouble(this);
    }

    public float toFloat() {
        return ScalaNumericConversions.Cclass.toFloat(this);
    }

    public int toInt() {
        return ScalaNumericConversions.Cclass.toInt(this);
    }

    public long toLong() {
        return ScalaNumericConversions.Cclass.toLong(this);
    }

    public short toShort() {
        return ScalaNumericConversions.Cclass.toShort(this);
    }

    public boolean unifiedPrimitiveEquals(Object x) {
        return ScalaNumericConversions.Cclass.unifiedPrimitiveEquals(this, x);
    }

    public int unifiedPrimitiveHashcode() {
        return ScalaNumericConversions.Cclass.unifiedPrimitiveHashcode(this);
    }

    public int hashCode() {
        if (!$greater$eq(BigInt$.MODULE$.MinLong()) || !$less$eq(BigInt$.MODULE$.MaxLong())) {
            return BoxesRunTime.hashFromNumber(bigInteger());
        }
        return unifiedPrimitiveHashcode();
    }

    public boolean equals(Object that) {
        if (that instanceof BigInt) {
            return equals((BigInt) that);
        }
        if (that instanceof BigDecimal) {
            return ((BigDecimal) that).toBigIntExact().exists(new BigInt$$anonfun$equals$1(this));
        }
        return $less$eq(BigInt$.MODULE$.MaxLong()) && $greater$eq(BigInt$.MODULE$.MinLong()) && unifiedPrimitiveEquals(that);
    }

    public boolean isWhole() {
        return true;
    }

    public BigInteger underlying() {
        return bigInteger();
    }

    public boolean equals(BigInt that) {
        return compare(that) == 0;
    }

    public int compare(BigInt that) {
        return bigInteger().compareTo(that.bigInteger());
    }

    public boolean $less$eq(BigInt that) {
        return compare(that) <= 0;
    }

    public boolean $greater$eq(BigInt that) {
        return compare(that) >= 0;
    }

    public byte byteValue() {
        return (byte) intValue();
    }

    public short shortValue() {
        return (short) intValue();
    }

    public int intValue() {
        return bigInteger().intValue();
    }

    public long longValue() {
        return bigInteger().longValue();
    }

    public float floatValue() {
        return bigInteger().floatValue();
    }

    public double doubleValue() {
        return bigInteger().doubleValue();
    }

    public String toString() {
        return bigInteger().toString();
    }
}
