package scala.math;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

/* compiled from: BigInt.scala */
public final class BigInt$$anonfun$equals$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ BigInt $outer;

    public BigInt$$anonfun$equals$1(BigInt $outer2) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return BoxesRunTime.boxToBoolean(apply((BigInt) v1));
    }

    public final boolean apply(BigInt bigInt) {
        return this.$outer.equals(bigInt);
    }
}
