package scala.math;

public abstract class ScalaNumber extends Number {
    /* access modifiers changed from: protected */
    public abstract boolean isWhole();

    public abstract Object underlying();
}
