package scala.math;

import scala.ScalaObject;

/* compiled from: Ordered.scala */
public interface Ordered<A> extends Comparable<A>, ScalaObject {
    boolean $less(A a);

    int compare(Object obj);

    /* renamed from: scala.math.Ordered$class  reason: invalid class name */
    /* compiled from: Ordered.scala */
    public abstract class Cclass {
        public static void $init$(Ordered $this) {
        }

        public static boolean $less(Ordered $this, Object that) {
            return $this.compare(that) < 0;
        }

        public static int compareTo(Ordered $this, Object that) {
            return $this.compare(that);
        }
    }
}
