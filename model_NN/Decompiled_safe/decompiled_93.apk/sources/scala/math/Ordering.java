package scala.math;

import java.util.Comparator;
import scala.Function1;
import scala.ScalaObject;

/* compiled from: Ordering.scala */
public interface Ordering<T> extends Comparator<T>, PartialOrdering<T>, ScalaObject, PartialOrdering {
    int compare(Object obj, Object obj2);

    <U> Ordering<U> on(Function1<U, T> function1);

    /* renamed from: scala.math.Ordering$class  reason: invalid class name */
    /* compiled from: Ordering.scala */
    public abstract class Cclass {
        public static void $init$(Ordering $this) {
        }

        public static Ordering on(Ordering $this, Function1 f$1) {
            return new Ordering$$anon$2($this, f$1);
        }
    }

    /* compiled from: Ordering.scala */
    public interface IntOrdering extends Ordering<Integer>, ScalaObject {
        int compare(int i, int i2);

        /* renamed from: scala.math.Ordering$IntOrdering$class  reason: invalid class name */
        /* compiled from: Ordering.scala */
        public abstract class Cclass {
            public static void $init$(IntOrdering $this) {
            }

            public static int compare(IntOrdering $this, int x, int y) {
                if (x < y) {
                    return -1;
                }
                if (x == y) {
                    return 0;
                }
                return 1;
            }
        }
    }
}
