package scala;

/* compiled from: MathCommon.scala */
public class MathCommon implements ScalaObject {
    private final double E = 2.718281828459045d;
    private final double Pi = 3.141592653589793d;

    public int max(int x, int y) {
        return Math.max(x, y);
    }

    public int min(int x, int y) {
        return Math.min(x, y);
    }
}
