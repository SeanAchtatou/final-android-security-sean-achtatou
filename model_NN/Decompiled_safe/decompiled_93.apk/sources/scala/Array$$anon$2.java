package scala;

import scala.collection.generic.CanBuildFrom;
import scala.collection.mutable.ArrayBuilder;
import scala.reflect.ClassManifest;

/* compiled from: Array.scala */
public final class Array$$anon$2 implements CanBuildFrom<Object, Object, Object> {
    public final /* synthetic */ ClassManifest m$1;

    public Array$$anon$2(ClassManifest classManifest) {
        this.m$1 = classManifest;
    }

    public ArrayBuilder<T> apply(Object from) {
        return this.m$1.newArrayBuilder();
    }
}
