package scala;

import scala.collection.Seq;
import scala.collection.generic.CanBuildFrom;
import scala.collection.mutable.ArrayBuilder;
import scala.reflect.ClassManifest;
import scala.runtime.BoxesRunTime;
import scala.runtime.IntRef;
import scala.runtime.ScalaRunTime$;

/* compiled from: Array.scala */
public final class Array$ extends FallbackArrayBuilding implements ScalaObject {
    public static final Array$ MODULE$ = null;

    static {
        new Array$();
    }

    private Array$() {
        MODULE$ = this;
    }

    public <T> CanBuildFrom<Object, T, Object> canBuildFrom(ClassManifest<T> m$1) {
        return new Array$$anon$2(m$1);
    }

    private void slowcopy(Object src, int srcPos, Object dest, int destPos, int length) {
        int i = srcPos;
        int j = destPos;
        int srcUntil = srcPos + length;
        while (i < srcUntil) {
            ScalaRunTime$.MODULE$.array_update(dest, j, ScalaRunTime$.MODULE$.array_apply(src, i));
            i++;
            j++;
        }
    }

    public void copy(Object src, int srcPos, Object dest, int destPos, int length) {
        Class srcClass = src.getClass();
        if (!srcClass.isArray() || !dest.getClass().isAssignableFrom(srcClass)) {
            slowcopy(src, srcPos, dest, destPos, length);
        } else {
            System.arraycopy(src, srcPos, dest, destPos, length);
        }
    }

    public <T> Object apply(Seq<T> xs, ClassManifest<T> evidence$2) {
        Object array$1 = evidence$2.newArray(xs.length());
        xs.iterator().foreach(new Array$$anonfun$apply$5(array$1, new IntRef(0)));
        return array$1;
    }

    public <T> Object fill(int n, Function0<T> elem, ClassManifest<T> evidence$9) {
        ArrayBuilder b = evidence$9.newArrayBuilder();
        b.sizeHint(n);
        for (int i = 0; i < n; i++) {
            b.$plus$eq((Object) elem.apply());
        }
        return b.result();
    }

    public <T> Object tabulate(int n, Function1<Integer, T> f, ClassManifest<T> evidence$14) {
        ArrayBuilder b = evidence$14.newArrayBuilder();
        b.sizeHint(n);
        for (int i = 0; i < n; i++) {
            b.$plus$eq((Object) f.apply(BoxesRunTime.boxToInteger(i)));
        }
        return b.result();
    }
}
