package scala;

/* compiled from: Function4.scala */
public interface Function4<T1, T2, T3, T4, R> extends ScalaObject {
    R apply(T1 t1, T2 t2, T3 t3, T4 t4);

    Function1<T1, Function1<T2, Function1<T3, Function1<T4, R>>>> curried();

    /* renamed from: scala.Function4$class  reason: invalid class name */
    /* compiled from: Function4.scala */
    public abstract class Cclass {
        public static void $init$(Function4 $this) {
        }

        public static String toString(Function4 $this) {
            return "<function4>";
        }

        public static Function1 curried(Function4 $this) {
            return new Function4$$anonfun$curried$1($this);
        }

        public static Function1 curry(Function4 $this) {
            return $this.curried();
        }

        public static Function1 tupled(Function4 $this) {
            return new Function4$$anonfun$tupled$1($this);
        }
    }
}
