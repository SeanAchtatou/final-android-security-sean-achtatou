package scala.util;

/* compiled from: DynamicVariable.scala */
public final class DynamicVariable$$anon$1 extends InheritableThreadLocal<T> {
    private final /* synthetic */ DynamicVariable $outer;

    public DynamicVariable$$anon$1(DynamicVariable<T> $outer2) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
    }

    public T initialValue() {
        return this.$outer.scala$util$DynamicVariable$$init;
    }
}
