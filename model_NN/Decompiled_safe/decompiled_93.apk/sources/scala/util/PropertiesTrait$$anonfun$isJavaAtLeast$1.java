package scala.util;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

/* compiled from: Properties.scala */
public final class PropertiesTrait$$anonfun$isJavaAtLeast$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ PropertiesTrait $outer;

    public PropertiesTrait$$anonfun$isJavaAtLeast$1(PropertiesTrait $outer2) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return BoxesRunTime.boxToBoolean(apply((String) v1));
    }

    public final boolean apply(String str) {
        return this.$outer.javaVersion().startsWith(str);
    }
}
