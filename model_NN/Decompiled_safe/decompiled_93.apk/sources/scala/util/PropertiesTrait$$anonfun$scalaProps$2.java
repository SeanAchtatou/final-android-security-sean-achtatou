package scala.util;

import java.io.InputStream;
import java.io.Serializable;
import scala.runtime.AbstractFunction0$mcV$sp;

/* compiled from: Properties.scala */
public final class PropertiesTrait$$anonfun$scalaProps$2 extends AbstractFunction0$mcV$sp implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ InputStream stream$1;

    public PropertiesTrait$$anonfun$scalaProps$2(PropertiesTrait $outer, InputStream inputStream) {
        this.stream$1 = inputStream;
    }

    public final void apply() {
        this.stream$1.close();
    }

    public void apply$mcV$sp() {
        this.stream$1.close();
    }
}
