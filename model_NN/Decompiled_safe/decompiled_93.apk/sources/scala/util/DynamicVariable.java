package scala.util;

import scala.ScalaObject;
import scala.collection.mutable.StringBuilder;

/* compiled from: DynamicVariable.scala */
public class DynamicVariable<T> implements ScalaObject {
    public final Object scala$util$DynamicVariable$$init;
    private final InheritableThreadLocal<T> tl = new DynamicVariable$$anon$1(this);

    public DynamicVariable(T init) {
        this.scala$util$DynamicVariable$$init = init;
    }

    private InheritableThreadLocal<T> tl() {
        return this.tl;
    }

    public T value() {
        return tl().get();
    }

    public String toString() {
        return new StringBuilder().append((Object) "DynamicVariable(").append(value()).append((Object) ")").toString();
    }
}
