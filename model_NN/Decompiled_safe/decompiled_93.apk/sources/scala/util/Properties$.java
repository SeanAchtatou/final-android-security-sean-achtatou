package scala.util;

import java.util.Properties;
import scala.Option;
import scala.ScalaObject;
import scala.util.PropertiesTrait;

/* compiled from: Properties.scala */
public final class Properties$ implements ScalaObject, PropertiesTrait {
    public static final Properties$ MODULE$ = null;
    public volatile int bitmap$0;
    private final String copyrightString;
    private final String propFilename;
    private final Properties scalaProps;
    private final String versionString;

    static {
        new Properties$();
    }

    private Properties$() {
        MODULE$ = this;
        PropertiesTrait.Cclass.$init$(this);
    }

    public boolean isJavaAtLeast(String version) {
        return PropertiesTrait.Cclass.isJavaAtLeast(this, version);
    }

    public String javaVersion() {
        return PropertiesTrait.Cclass.javaVersion(this);
    }

    public String javaVmVendor() {
        return PropertiesTrait.Cclass.javaVmVendor(this);
    }

    public String propFilename() {
        return this.propFilename;
    }

    public boolean propIsSetTo(String name, String value) {
        return PropertiesTrait.Cclass.propIsSetTo(this, name, value);
    }

    public String propOrElse(String name, String alt) {
        return PropertiesTrait.Cclass.propOrElse(this, name, alt);
    }

    public String propOrEmpty(String name) {
        return PropertiesTrait.Cclass.propOrEmpty(this, name);
    }

    public Option<String> propOrNone(String name) {
        return PropertiesTrait.Cclass.propOrNone(this, name);
    }

    public String propOrNull(String name) {
        return PropertiesTrait.Cclass.propOrNull(this, name);
    }

    public void scala$util$PropertiesTrait$_setter_$copyrightString_$eq(String str) {
        this.copyrightString = str;
    }

    public void scala$util$PropertiesTrait$_setter_$propFilename_$eq(String str) {
        this.propFilename = str;
    }

    public void scala$util$PropertiesTrait$_setter_$versionString_$eq(String str) {
        this.versionString = str;
    }

    public String scalaPropOrElse(String name, String alt) {
        return PropertiesTrait.Cclass.scalaPropOrElse(this, name, alt);
    }

    public Properties scalaProps() {
        if ((this.bitmap$0 & 1) == 0) {
            synchronized (this) {
                if ((this.bitmap$0 & 1) == 0) {
                    this.scalaProps = PropertiesTrait.Cclass.scalaProps(this);
                    this.bitmap$0 |= 1;
                }
            }
        }
        return this.scalaProps;
    }

    public String propCategory() {
        return "library";
    }

    public Class<ScalaObject> pickJarBasedOn() {
        return ScalaObject.class;
    }
}
