package scala.util.control;

import scala.ScalaObject;

/* compiled from: NoStackTrace.scala */
public interface NoStackTrace extends ScalaObject {

    /* renamed from: scala.util.control.NoStackTrace$class  reason: invalid class name */
    /* compiled from: NoStackTrace.scala */
    public abstract class Cclass {
        public static void $init$(NoStackTrace $this) {
        }

        public static Throwable fillInStackTrace(NoStackTrace $this) {
            return (Throwable) $this;
        }
    }
}
