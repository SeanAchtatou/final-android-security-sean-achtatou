package scala.util.control;

import scala.ScalaObject;
import scala.util.control.NoStackTrace;

/* compiled from: Breaks.scala */
public class BreakControl extends Throwable implements ScalaObject, ControlThrowable {
    public BreakControl() {
        NoStackTrace.Cclass.$init$(this);
    }

    public Throwable fillInStackTrace() {
        return NoStackTrace.Cclass.fillInStackTrace(this);
    }
}
