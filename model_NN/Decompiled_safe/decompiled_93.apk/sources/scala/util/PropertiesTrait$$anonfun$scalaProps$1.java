package scala.util;

import java.io.InputStream;
import java.io.Serializable;
import java.util.Properties;
import scala.runtime.AbstractFunction0$mcV$sp;

/* compiled from: Properties.scala */
public final class PropertiesTrait$$anonfun$scalaProps$1 extends AbstractFunction0$mcV$sp implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Properties props$1;
    public final /* synthetic */ InputStream stream$1;

    public PropertiesTrait$$anonfun$scalaProps$1(PropertiesTrait $outer, Properties properties, InputStream inputStream) {
        this.props$1 = properties;
        this.stream$1 = inputStream;
    }

    public final void apply() {
        this.props$1.load(this.stream$1);
    }

    public void apply$mcV$sp() {
        this.props$1.load(this.stream$1);
    }
}
