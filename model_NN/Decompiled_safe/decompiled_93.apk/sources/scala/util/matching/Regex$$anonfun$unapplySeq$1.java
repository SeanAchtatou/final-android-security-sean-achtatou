package scala.util.matching;

import java.io.Serializable;
import java.util.regex.Matcher;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

/* compiled from: Regex.scala */
public final class Regex$$anonfun$unapplySeq$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Matcher m$1;

    public Regex$$anonfun$unapplySeq$1(Regex $outer, Matcher matcher) {
        this.m$1 = matcher;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return apply(BoxesRunTime.unboxToInt(v1));
    }

    public final String apply(int i) {
        return this.m$1.group(i);
    }
}
