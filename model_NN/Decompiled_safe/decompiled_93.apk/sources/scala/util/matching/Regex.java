package scala.util.matching;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import scala.None$;
import scala.Option;
import scala.ScalaObject;
import scala.Some;
import scala.collection.Seq;
import scala.collection.generic.TraversableFactory;
import scala.collection.immutable.List;
import scala.collection.immutable.List$;
import scala.collection.immutable.Range$$anon$1;

/* compiled from: Regex.scala */
public class Regex implements Serializable, ScalaObject {
    public static final long serialVersionUID = -2094783597747625537L;
    private final Seq<String> groupNames;
    private final Pattern pattern;
    private final String regex;

    public Regex(String regex2, Seq<String> groupNames2) {
        this.regex = regex2;
        this.groupNames = groupNames2;
        this.pattern = Pattern.compile(regex2);
    }

    public Pattern pattern() {
        return this.pattern;
    }

    public Option<List<String>> unapplySeq(Object target) {
        if (target instanceof CharSequence) {
            Matcher m$1 = pattern().matcher((CharSequence) target);
            if (m$1.matches()) {
                return new Some(new Range$$anon$1(1, m$1.groupCount()).toList().map(new Regex$$anonfun$unapplySeq$1(this, m$1), new TraversableFactory.GenericCanBuildFrom(List$.MODULE$)));
            }
            return None$.MODULE$;
        }
        if (target instanceof Match) {
            Some some = new Some(((Match) target).matched());
            if (1 != 0) {
                return unapplySeq(some.x);
            }
        }
        return None$.MODULE$;
    }

    public String toString() {
        return this.regex;
    }

    /* compiled from: Regex.scala */
    public interface MatchData extends ScalaObject {
        int end();

        String matched();

        CharSequence source();

        int start();

        /* renamed from: scala.util.matching.Regex$MatchData$class  reason: invalid class name */
        /* compiled from: Regex.scala */
        public abstract class Cclass {
            public static String matched(MatchData $this) {
                if ($this.start() >= 0) {
                    return $this.source().subSequence($this.start(), $this.end()).toString();
                }
                return null;
            }

            public static String toString(MatchData $this) {
                return $this.matched();
            }
        }
    }

    /* compiled from: Regex.scala */
    public static class Match implements ScalaObject, MatchData {
        private final int end;
        private final CharSequence source;
        private final int start;

        public String matched() {
            return MatchData.Cclass.matched(this);
        }

        public CharSequence source() {
            return this.source;
        }

        public String toString() {
            return MatchData.Cclass.toString(this);
        }

        public int start() {
            return this.start;
        }

        public int end() {
            return this.end;
        }
    }
}
