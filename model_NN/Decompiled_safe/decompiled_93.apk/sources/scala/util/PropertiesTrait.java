package scala.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import scala.Function0;
import scala.Option;
import scala.Option$;
import scala.Predef$;
import scala.ScalaObject;
import scala.collection.immutable.Nil$;
import scala.collection.mutable.StringBuilder;

/* compiled from: Properties.scala */
public interface PropertiesTrait extends ScalaObject {
    boolean isJavaAtLeast(String str);

    String javaVersion();

    String javaVmVendor();

    Class<?> pickJarBasedOn();

    String propCategory();

    String propFilename();

    boolean propIsSetTo(String str, String str2);

    String propOrElse(String str, String str2);

    String propOrEmpty(String str);

    Option<String> propOrNone(String str);

    String propOrNull(String str);

    void scala$util$PropertiesTrait$_setter_$copyrightString_$eq(String str);

    void scala$util$PropertiesTrait$_setter_$propFilename_$eq(String str);

    void scala$util$PropertiesTrait$_setter_$versionString_$eq(String str);

    String scalaPropOrElse(String str, String str2);

    Properties scalaProps();

    /* renamed from: scala.util.PropertiesTrait$class  reason: invalid class name */
    /* compiled from: Properties.scala */
    public abstract class Cclass {
        public static void $init$(PropertiesTrait propertiesTrait) {
            propertiesTrait.scala$util$PropertiesTrait$_setter_$propFilename_$eq(new StringBuilder().append((Object) "/").append((Object) propertiesTrait.propCategory()).append((Object) ".properties").toString());
            propertiesTrait.scala$util$PropertiesTrait$_setter_$versionString_$eq(new StringBuilder().append((Object) "version ").append((Object) propertiesTrait.scalaPropOrElse("version.number", "(unknown)")).toString());
            propertiesTrait.scala$util$PropertiesTrait$_setter_$copyrightString_$eq(propertiesTrait.scalaPropOrElse("copyright.string", "(c) 2002-2010 LAMP/EPFL"));
        }

        public static Properties scalaProps(PropertiesTrait $this) {
            Properties props$1 = new Properties();
            InputStream stream$1 = $this.pickJarBasedOn().getResourceAsStream($this.propFilename());
            if (stream$1 != null) {
                quietlyDispose($this, new PropertiesTrait$$anonfun$scalaProps$1($this, props$1, stream$1), new PropertiesTrait$$anonfun$scalaProps$2($this, stream$1));
            }
            return props$1;
        }

        private static void quietlyDispose(PropertiesTrait $this, Function0 action, Function0 disposal) {
            try {
                action.apply$mcV$sp();
            } finally {
                try {
                    disposal.apply$mcV$sp();
                } catch (IOException e) {
                }
            }
        }

        public static boolean propIsSetTo(PropertiesTrait $this, String name, String value) {
            String propOrNull = $this.propOrNull(name);
            return propOrNull != null ? propOrNull.equals(value) : value == null;
        }

        public static String propOrElse(PropertiesTrait $this, String name, String alt) {
            return System.getProperty(name, alt);
        }

        public static String propOrEmpty(PropertiesTrait $this, String name) {
            return $this.propOrElse(name, "");
        }

        public static String propOrNull(PropertiesTrait $this, String name) {
            return $this.propOrElse(name, null);
        }

        public static Option propOrNone(PropertiesTrait $this, String name) {
            return Option$.MODULE$.apply($this.propOrNull(name));
        }

        public static String scalaPropOrElse(PropertiesTrait $this, String name, String alt) {
            return $this.scalaProps().getProperty(name, alt);
        }

        public static String javaVersion(PropertiesTrait $this) {
            return $this.propOrEmpty("java.version");
        }

        public static String javaVmVendor(PropertiesTrait $this) {
            return $this.propOrEmpty("java.vm.vendor");
        }

        public static boolean isJavaAtLeast(PropertiesTrait propertiesTrait, String str) {
            Object list;
            if (str != null ? str.equals("1.5") : "1.5" == 0) {
                list = Predef$.MODULE$.wrapRefArray((Object[]) new String[]{"1.5", "1.6", "1.7"}).toList();
            } else if (str != null ? str.equals("1.6") : "1.6" == 0) {
                list = Predef$.MODULE$.wrapRefArray((Object[]) new String[]{"1.6", "1.7"}).toList();
            } else if (str != null ? !str.equals("1.7") : "1.7" != 0) {
                list = Nil$.MODULE$;
            } else {
                list = Predef$.MODULE$.wrapRefArray((Object[]) new String[]{"1.7"}).toList();
            }
            return list.exists(new PropertiesTrait$$anonfun$isJavaAtLeast$1(propertiesTrait));
        }
    }
}
