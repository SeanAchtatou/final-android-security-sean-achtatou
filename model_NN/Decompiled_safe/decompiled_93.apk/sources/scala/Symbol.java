package scala;

import java.io.Serializable;
import scala.collection.mutable.StringBuilder;

/* compiled from: Symbol.scala */
public final class Symbol implements Serializable, ScalaObject {
    public final String name;

    public Symbol(String name2) {
        this.name = name2;
    }

    public String toString() {
        return new StringBuilder().append((Object) "'").append((Object) this.name).toString();
    }
}
