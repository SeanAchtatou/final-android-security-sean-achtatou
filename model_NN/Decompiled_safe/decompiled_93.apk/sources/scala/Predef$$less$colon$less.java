package scala;

import scala.Function1;

/* compiled from: Predef.scala */
public abstract class Predef$$less$colon$less<From, To> implements Function1<From, To>, ScalaObject {
    public Predef$$less$colon$less() {
        Function1.Cclass.$init$(this);
    }

    public <A> Function1<From, A> andThen(Function1<To, A> g) {
        return Function1.Cclass.andThen(this, g);
    }

    public int apply$mcII$sp(int v1) {
        return Function1.Cclass.apply$mcII$sp(this, v1);
    }

    public void apply$mcVI$sp(int v1) {
        Function1.Cclass.apply$mcVI$sp(this, v1);
    }

    public void apply$mcVL$sp(long v1) {
        Function1.Cclass.apply$mcVL$sp(this, v1);
    }

    public <A> Function1<A, To> compose(Function1<A, From> g) {
        return Function1.Cclass.compose(this, g);
    }

    public String toString() {
        return Function1.Cclass.toString(this);
    }
}
