package scala;

import $anonfun;
import java.io.Serializable;
import scala.runtime.AbstractFunction1;

/* compiled from: Function4.scala */
public final class Function4$$anonfun$curried$1$$anonfun$apply$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Function4$$anonfun$curried$1 $outer;
    public final /* synthetic */ Object x1$1;

    public Function4$$anonfun$curried$1$$anonfun$apply$1(Function4$$anonfun$curried$1 $outer2, Function4<T1, T2, T3, T4, R>.$anonfun.curried.1 function4$$anonfun$curried$1) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.x1$1 = function4$$anonfun$curried$1;
    }

    public final Function1<T3, Function1<T4, R>> apply(T2 x2$1) {
        return new Function4$$anonfun$curried$1$$anonfun$apply$1$$anonfun$apply$2(this, x2$1);
    }
}
