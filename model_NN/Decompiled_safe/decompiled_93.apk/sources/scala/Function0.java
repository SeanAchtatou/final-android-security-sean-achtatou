package scala;

import scala.runtime.BoxesRunTime;

/* compiled from: Function0.scala */
public interface Function0<R> extends ScalaObject {
    R apply();

    byte apply$mcB$sp();

    int apply$mcI$sp();

    void apply$mcV$sp();

    /* renamed from: scala.Function0$class  reason: invalid class name */
    /* compiled from: Function0.scala */
    public abstract class Cclass {
        public static void $init$(Function0 $this) {
        }

        public static byte apply$mcB$sp(Function0 $this) {
            return BoxesRunTime.unboxToByte($this.apply());
        }

        public static char apply$mcC$sp(Function0 $this) {
            return BoxesRunTime.unboxToChar($this.apply());
        }

        public static double apply$mcD$sp(Function0 $this) {
            return BoxesRunTime.unboxToDouble($this.apply());
        }

        public static float apply$mcF$sp(Function0 $this) {
            return BoxesRunTime.unboxToFloat($this.apply());
        }

        public static int apply$mcI$sp(Function0 $this) {
            return BoxesRunTime.unboxToInt($this.apply());
        }

        public static long apply$mcL$sp(Function0 $this) {
            return BoxesRunTime.unboxToLong($this.apply());
        }

        public static short apply$mcS$sp(Function0 $this) {
            return BoxesRunTime.unboxToShort($this.apply());
        }

        public static void apply$mcV$sp(Function0 $this) {
            $this.apply();
        }

        public static boolean apply$mcZ$sp(Function0 $this) {
            return BoxesRunTime.unboxToBoolean($this.apply());
        }

        public static String toString(Function0 $this) {
            return "<function0>";
        }
    }
}
