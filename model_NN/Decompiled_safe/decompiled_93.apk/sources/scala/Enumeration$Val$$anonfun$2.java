package scala;

import java.io.Serializable;
import scala.Enumeration;
import scala.collection.mutable.StringBuilder;
import scala.runtime.AbstractFunction0;
import scala.runtime.BoxesRunTime;

/* compiled from: Enumeration.scala */
public final class Enumeration$Val$$anonfun$2 extends AbstractFunction0 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Enumeration.Val $outer;

    public Enumeration$Val$$anonfun$2(Enumeration.Val $outer2) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
    }

    public final String apply() {
        return new StringBuilder().append((Object) "Duplicate id: ").append(BoxesRunTime.boxToInteger(this.$outer.scala$Enumeration$Val$$i)).toString();
    }
}
