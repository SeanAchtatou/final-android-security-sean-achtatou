package scala;

import scala.Enumeration;
import scala.collection.immutable.BitSet$;

/* compiled from: Enumeration.scala */
public final class Enumeration$ValueSet$ implements ScalaObject {
    private final Enumeration.ValueSet empty;

    public Enumeration$ValueSet$(Enumeration $outer) {
        this.empty = new Enumeration.ValueSet($outer, BitSet$.MODULE$.empty());
    }

    public Enumeration.ValueSet empty() {
        return this.empty;
    }
}
