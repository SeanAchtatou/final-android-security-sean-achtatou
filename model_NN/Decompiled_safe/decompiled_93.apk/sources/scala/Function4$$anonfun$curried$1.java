package scala;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;

/* compiled from: Function4.scala */
public final class Function4$$anonfun$curried$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Function4 $outer;

    public Function4$$anonfun$curried$1(Function4<T1, T2, T3, T4, R> $outer2) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
    }

    public final Function1<T2, Function1<T3, Function1<T4, R>>> apply(T1 x1$1) {
        return new Function4$$anonfun$curried$1$$anonfun$apply$1(this, x1$1);
    }
}
