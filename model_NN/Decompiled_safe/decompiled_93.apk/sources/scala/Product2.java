package scala;

import scala.runtime.BoxesRunTime;

/* compiled from: Product2.scala */
public interface Product2<T1, T2> extends Product, ScalaObject {
    T1 _1();

    T2 _2();

    /* renamed from: scala.Product2$class  reason: invalid class name */
    /* compiled from: Product2.scala */
    public abstract class Cclass {
        public static void $init$(Product2 $this) {
        }

        public static int productArity(Product2 $this) {
            return 2;
        }

        public static Object productElement(Product2 $this, int n) throws IndexOutOfBoundsException {
            switch (n) {
                case 0:
                    return $this._1();
                case 1:
                    return $this._2();
                default:
                    throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(n).toString());
            }
        }
    }
}
