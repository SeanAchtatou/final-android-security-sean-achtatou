package scala;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

/* compiled from: Function1.scala */
public final class Function1$mcVI$sp$$anonfun$compose$mcVI$sp$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Function1$mcVI$sp $outer;
    public final /* synthetic */ Function1 g$3;

    public Function1$mcVI$sp$$anonfun$compose$mcVI$sp$1(Function1$mcVI$sp $outer2, Function1 function1) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.g$3 = function1;
    }

    public final void apply(A x) {
        this.$outer.apply(BoxesRunTime.unboxToInt(this.g$3.apply(x)));
    }
}
