package scala.collection;

import java.util.NoSuchElementException;
import scala.Console$;
import scala.Function0;
import scala.Function1;
import scala.MatchError;
import scala.None$;
import scala.Option;
import scala.PartialFunction;
import scala.ScalaObject;
import scala.Some;
import scala.Tuple2;
import scala.collection.Map;
import scala.collection.MapLike;
import scala.collection.TraversableLike;
import scala.collection.generic.Subtractable;
import scala.collection.generic.TraversableFactory;
import scala.collection.mutable.Builder;
import scala.collection.mutable.MapBuilder;
import scala.collection.mutable.StringBuilder;
import scala.math.Numeric$IntIsIntegral$;
import scala.runtime.BoxesRunTime;
import scala.runtime.ObjectRef;

/* compiled from: MapLike.scala */
public interface MapLike<A, B, This extends MapLike<A, B, This> & Map<A, B>> extends PartialFunction<A, B>, IterableLike<Tuple2<A, B>, This>, Subtractable<A, This>, ScalaObject {
    This $minus(A a);

    <B1> Map<A, B1> $plus(Tuple2<A, B1> tuple2);

    B apply(A a);

    boolean contains(A a);

    /* renamed from: default  reason: not valid java name */
    B m2default(A a);

    This empty();

    This filterNot(Function1<Tuple2<A, B>, Boolean> function1);

    Option<B> get(A a);

    <B1> B1 getOrElse(A a, Function0<B1> function0);

    boolean isDefinedAt(A a);

    boolean isEmpty();

    Iterator<Tuple2<A, B>> iterator();

    /* renamed from: scala.collection.MapLike$class  reason: invalid class name */
    /* compiled from: MapLike.scala */
    public abstract class Cclass {
        public static void $init$(MapLike $this) {
        }

        public static Builder newBuilder(MapLike $this) {
            return new MapBuilder($this.empty());
        }

        public static boolean isEmpty(MapLike $this) {
            return $this.size() == 0;
        }

        public static Object getOrElse(MapLike $this, Object key, Function0 function0) {
            Option option = $this.get(key);
            if (option instanceof Some) {
                return ((Some) option).x;
            }
            None$ none$ = None$.MODULE$;
            if (none$ != null ? none$.equals(option) : option == null) {
                return function0.apply();
            }
            throw new MatchError(option);
        }

        public static Object apply(MapLike $this, Object key) {
            Option option = $this.get(key);
            None$ none$ = None$.MODULE$;
            if (none$ != null ? none$.equals(option) : option == null) {
                return $this.m2default(key);
            }
            if (option instanceof Some) {
                return ((Some) option).x;
            }
            throw new MatchError(option);
        }

        public static boolean contains(MapLike $this, Object key) {
            Option option = $this.get(key);
            None$ none$ = None$.MODULE$;
            if (none$ != null ? none$.equals(option) : option == null) {
                return false;
            }
            if (option instanceof Some) {
                return true;
            }
            throw new MatchError(option);
        }

        public static boolean isDefinedAt(MapLike $this, Object key) {
            return $this.contains(key);
        }

        /* renamed from: default  reason: not valid java name */
        public static Object m3default(MapLike $this, Object key) {
            throw new NoSuchElementException(new StringBuilder().append((Object) "key not found: ").append(key).toString());
        }

        public static Map filterNot(MapLike $this, Function1 p$2) {
            ObjectRef res$1 = new ObjectRef((Map) $this.repr());
            $this.foreach(new MapLike$$anonfun$filterNot$1($this, p$2, res$1));
            return (Map) res$1.elem;
        }

        public static StringBuilder addString(MapLike $this, StringBuilder b, String start, String sep, String end) {
            return $this.iterator().map(new MapLike$$anonfun$addString$1($this)).addString(b, start, sep, end);
        }

        public static String stringPrefix(MapLike $this) {
            return "Map";
        }

        public static String toString(MapLike $this) {
            return TraversableLike.Cclass.toString($this);
        }

        public static int hashCode(MapLike $this) {
            return BoxesRunTime.unboxToInt(((TraversableOnce) $this.map(new MapLike$$anonfun$hashCode$1($this), new TraversableFactory.GenericCanBuildFrom(Iterable$.MODULE$))).sum(Numeric$IntIsIntegral$.MODULE$));
        }

        public static boolean equals(MapLike $this, Object that) {
            if (!(that instanceof Map)) {
                return false;
            }
            Map map = (Map) that;
            if ($this == map || (map.canEqual($this) && $this.size() == map.size() && liftedTree1$1($this, map))) {
                return true;
            }
            return false;
        }

        private static final boolean liftedTree1$1(MapLike mapLike, Map map) {
            try {
                return mapLike.forall(new MapLike$$anonfun$liftedTree1$1$1(mapLike, map));
            } catch (ClassCastException e) {
                Console$.MODULE$.println("class cast ");
                return false;
            }
        }
    }
}
