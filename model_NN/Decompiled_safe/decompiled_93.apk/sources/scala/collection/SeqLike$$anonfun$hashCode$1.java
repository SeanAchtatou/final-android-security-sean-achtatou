package scala.collection;

import java.io.Serializable;
import scala.runtime.AbstractFunction2;
import scala.runtime.BoxesRunTime;

/* compiled from: SeqLike.scala */
public final class SeqLike$$anonfun$hashCode$1 extends AbstractFunction2 implements Serializable {
    public static final long serialVersionUID = 0;

    public SeqLike$$anonfun$hashCode$1(SeqLike<A, Repr> $outer) {
    }

    public final int apply(int i, A a) {
        return (i * 41) + (a instanceof Number ? BoxesRunTime.hashFromNumber((Number) a) : a.hashCode());
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1, Object v2) {
        return BoxesRunTime.boxToInteger(apply(BoxesRunTime.unboxToInt(v1), v2));
    }
}
