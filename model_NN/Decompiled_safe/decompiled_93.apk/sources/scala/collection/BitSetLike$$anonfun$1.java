package scala.collection;

import java.io.Serializable;
import scala.runtime.AbstractFunction1$mcII$sp;
import scala.runtime.BoxesRunTime;

/* compiled from: BitSetLike.scala */
public final class BitSetLike$$anonfun$1 extends AbstractFunction1$mcII$sp implements Serializable {
    public static final long serialVersionUID = 0;

    public final int apply(int i) {
        return BitSetLike$.MODULE$.countBits$1(i);
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return BoxesRunTime.boxToInteger(apply(BoxesRunTime.unboxToInt(v1)));
    }

    public int apply$mcII$sp(int v1) {
        return BitSetLike$.MODULE$.countBits$1(v1);
    }
}
