package scala.collection;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

/* compiled from: SeqLike.scala */
public final class SeqLike$$anonfun$indexOf$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Object elem$1;

    public SeqLike$$anonfun$indexOf$1(SeqLike $outer, SeqLike<A, Repr> seqLike) {
        this.elem$1 = seqLike;
    }

    public final boolean apply(Object obj) {
        Object obj2 = this.elem$1;
        return obj2 == obj ? true : obj2 == null ? false : obj2 instanceof Number ? BoxesRunTime.equalsNumObject((Number) obj2, obj) : obj2 instanceof Character ? BoxesRunTime.equalsCharObject((Character) obj2, obj) : obj2.equals(obj);
    }
}
