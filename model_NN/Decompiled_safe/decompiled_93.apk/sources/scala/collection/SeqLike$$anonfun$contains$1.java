package scala.collection;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

/* compiled from: SeqLike.scala */
public final class SeqLike$$anonfun$contains$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Object elem$4;

    public SeqLike$$anonfun$contains$1(SeqLike $outer, SeqLike<A, Repr> seqLike) {
        this.elem$4 = seqLike;
    }

    public final boolean apply(A a) {
        A a2 = this.elem$4;
        return a == a2 ? true : a == null ? false : a instanceof Number ? BoxesRunTime.equalsNumObject((Number) a, a2) : a instanceof Character ? BoxesRunTime.equalsCharObject((Character) a, a2) : a.equals(a2);
    }
}
