package scala.collection;

import scala.ScalaObject;
import scala.collection.generic.SeqFactory;
import scala.collection.immutable.VectorBuilder;
import scala.collection.mutable.Builder;

/* compiled from: IndexedSeq.scala */
public final class IndexedSeq$ extends SeqFactory<IndexedSeq> implements ScalaObject {
    public static final IndexedSeq$ MODULE$ = null;

    static {
        new IndexedSeq$();
    }

    private IndexedSeq$() {
        MODULE$ = this;
    }

    public <A> Builder<A, IndexedSeq<A>> newBuilder() {
        return new VectorBuilder();
    }
}
