package scala.collection;

import java.util.Arrays;
import scala.Function1;
import scala.ScalaObject;
import scala.collection.TraversableLike;
import scala.collection.immutable.List;
import scala.collection.immutable.Nil$;
import scala.collection.mutable.ArraySeq;
import scala.collection.mutable.Builder;
import scala.math.Ordering;
import scala.runtime.BoxesRunTime;
import scala.runtime.IntRef;
import scala.runtime.ObjectRef;

/* compiled from: SeqLike.scala */
public interface SeqLike<A, Repr> extends IterableLike<A, Repr>, ScalaObject {
    A apply(int i);

    boolean contains(Object obj);

    boolean equals(Object obj);

    <B> int indexOf(B b);

    <B> int indexOf(B b, int i);

    int indexWhere(Function1<A, Boolean> function1, int i);

    boolean isDefinedAt(int i);

    int length();

    int prefixLength(Function1<A, Boolean> function1);

    Repr reverse();

    Iterator<A> reverseIterator();

    int segmentLength(Function1<A, Boolean> function1, int i);

    int size();

    <B> Repr sortBy(Function1<A, B> function1, Ordering<B> ordering);

    <B> Repr sorted(Ordering<B> ordering);

    Seq<A> thisCollection();

    Seq<A> toCollection(Repr repr);

    String toString();

    /* renamed from: scala.collection.SeqLike$class  reason: invalid class name */
    /* compiled from: SeqLike.scala */
    public abstract class Cclass {
        public static void $init$(SeqLike $this) {
        }

        public static Seq thisCollection(SeqLike $this) {
            return (Seq) $this;
        }

        public static Seq toCollection(SeqLike $this, Object repr) {
            return (Seq) repr;
        }

        public static int size(SeqLike $this) {
            return $this.length();
        }

        public static boolean isDefinedAt(SeqLike $this, int idx) {
            return idx >= 0 && idx < $this.length();
        }

        public static int segmentLength(SeqLike $this, Function1 p, int from) {
            int i = 0;
            Iterator it = $this.iterator().drop(from);
            while (it.hasNext() && BoxesRunTime.unboxToBoolean(p.apply(it.next()))) {
                i++;
            }
            return i;
        }

        public static int prefixLength(SeqLike $this, Function1 p) {
            return $this.segmentLength(p, 0);
        }

        public static int indexWhere(SeqLike $this, Function1 p, int from) {
            int i = from;
            Iterator it = $this.iterator().drop(from);
            while (it.hasNext()) {
                if (BoxesRunTime.unboxToBoolean(p.apply(it.next()))) {
                    return i;
                }
                i++;
            }
            return -1;
        }

        public static int indexOf(SeqLike $this, Object elem) {
            return $this.indexOf(elem, 0);
        }

        public static int indexOf(SeqLike $this, Object elem$1, int from) {
            return $this.indexWhere(new SeqLike$$anonfun$indexOf$1($this, elem$1), from);
        }

        public static Object reverse(SeqLike $this) {
            ObjectRef xs$1 = new ObjectRef(Nil$.MODULE$);
            $this.foreach(new SeqLike$$anonfun$reverse$1($this, xs$1));
            Builder b$1 = $this.newBuilder();
            b$1.sizeHint($this, b$1.sizeHint$default$2());
            ((List) xs$1.elem).foreach(new SeqLike$$anonfun$reverse$2($this, b$1));
            return b$1.result();
        }

        public static Iterator reverseIterator(SeqLike $this) {
            return $this.toCollection($this.reverse()).iterator();
        }

        public static boolean contains(SeqLike $this, Object elem$4) {
            return $this.exists(new SeqLike$$anonfun$contains$1($this, elem$4));
        }

        public static Object sortBy(SeqLike $this, Function1 f, Ordering ord) {
            return $this.sorted(ord.on(f));
        }

        public static Object sorted(SeqLike $this, Ordering ord) {
            ArraySeq arr$1 = new ArraySeq($this.length());
            $this.foreach(new SeqLike$$anonfun$sorted$1($this, arr$1, new IntRef(0)));
            Arrays.sort(arr$1.array(), ord);
            Builder b$6 = $this.newBuilder();
            b$6.sizeHint($this, b$6.sizeHint$default$2());
            arr$1.foreach(new SeqLike$$anonfun$sorted$2($this, b$6));
            return b$6.result();
        }

        public static int hashCode(SeqLike $this) {
            return BoxesRunTime.unboxToInt($this.$div$colon(BoxesRunTime.boxToInteger(Seq$.MODULE$.hashSeed()), new SeqLike$$anonfun$hashCode$1($this)));
        }

        public static boolean equals(SeqLike $this, Object that) {
            if (!(that instanceof Seq)) {
                return false;
            }
            Seq seq = (Seq) that;
            if (!seq.canEqual($this) || !$this.sameElements(seq)) {
                return false;
            }
            return true;
        }

        public static String toString(SeqLike $this) {
            return TraversableLike.Cclass.toString($this);
        }
    }
}
