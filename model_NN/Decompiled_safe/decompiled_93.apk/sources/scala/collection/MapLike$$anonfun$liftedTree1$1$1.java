package scala.collection;

import java.io.Serializable;
import scala.MatchError;
import scala.Option;
import scala.Some;
import scala.Tuple2;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

/* compiled from: MapLike.scala */
public final class MapLike$$anonfun$liftedTree1$1$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Map that$1;

    /* JADX WARN: Type inference failed for: r2v0, types: [scala.collection.Map, scala.collection.MapLike<A, B, This>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public MapLike$$anonfun$liftedTree1$1$1(scala.collection.MapLike r1, scala.collection.MapLike<A, B, This> r2) {
        /*
            r0 = this;
            r0.that$1 = r2
            r0.<init>()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.collection.MapLike$$anonfun$liftedTree1$1$1.<init>(scala.collection.MapLike, scala.collection.Map):void");
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return BoxesRunTime.boxToBoolean(apply((Tuple2) ((Tuple2) v1)));
    }

    public final boolean apply(Tuple2<A, B> tuple2) {
        if (tuple2 == null) {
            throw new MatchError(tuple2);
        }
        Option option = this.that$1.get(tuple2._1());
        if (!(option instanceof Some)) {
            return false;
        }
        B _2 = tuple2._2();
        A a = ((Some) option).x;
        return _2 == a ? true : _2 == null ? false : _2 instanceof Number ? BoxesRunTime.equalsNumObject((Number) _2, a) : _2 instanceof Character ? BoxesRunTime.equalsCharObject((Character) _2, a) : _2.equals(a);
    }
}
