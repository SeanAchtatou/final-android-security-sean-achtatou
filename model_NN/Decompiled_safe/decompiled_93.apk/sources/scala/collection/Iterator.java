package scala.collection;

import scala.Function0;
import scala.Function1;
import scala.ScalaObject;
import scala.collection.immutable.Stream;
import scala.collection.immutable.Stream$Empty$;
import scala.collection.mutable.StringBuilder;
import scala.runtime.BoxesRunTime;
import scala.runtime.RichInt;
import scala.runtime.ScalaRunTime$;

/* compiled from: Iterator.scala */
public interface Iterator<A> extends TraversableOnce<A>, ScalaObject {
    Iterator<A> drop(int i);

    boolean exists(Function1<A, Boolean> function1);

    boolean forall(Function1<A, Boolean> function1);

    <U> void foreach(Function1<A, U> function1);

    boolean hasNext();

    int length();

    <B> Iterator<B> map(Function1<A, B> function1);

    A next();

    Stream<A> toStream();

    /* renamed from: scala.collection.Iterator$class  reason: invalid class name */
    /* compiled from: Iterator.scala */
    public abstract class Cclass {
        public static void $init$(Iterator $this) {
        }

        public static boolean isEmpty(Iterator $this) {
            return !$this.hasNext();
        }

        public static boolean isTraversableAgain(Iterator $this) {
            return false;
        }

        private static final Iterator loop$1(Iterator $this, int left) {
            while (left > 0 && $this.hasNext()) {
                $this.next();
                left--;
            }
            return $this;
        }

        public static Iterator drop(Iterator $this, int n) {
            return loop$1($this, n);
        }

        public static Iterator map(Iterator $this, Function1 f$1) {
            return new Iterator$$anon$19($this, f$1);
        }

        public static Iterator $plus$plus(Iterator $this, Function0 that$1) {
            return new Iterator$$anon$20($this, that$1);
        }

        public static void foreach(Iterator $this, Function1 f) {
            while ($this.hasNext()) {
                f.apply($this.next());
            }
        }

        public static boolean forall(Iterator $this, Function1 p) {
            boolean res = true;
            while (res && $this.hasNext()) {
                res = BoxesRunTime.unboxToBoolean(p.apply($this.next()));
            }
            return res;
        }

        public static boolean exists(Iterator $this, Function1 p) {
            boolean res = false;
            while (!res && $this.hasNext()) {
                res = BoxesRunTime.unboxToBoolean(p.apply($this.next()));
            }
            return res;
        }

        public static int length(Iterator $this) {
            return $this.size();
        }

        public static void copyToArray(Iterator $this, Object xs, int start, int len) {
            int i = start;
            int end = new RichInt(start + len).min(ScalaRunTime$.MODULE$.array_length(xs));
            while ($this.hasNext() && i < end) {
                ScalaRunTime$.MODULE$.array_update(xs, i, $this.next());
                i++;
            }
        }

        public static Stream toStream(Iterator $this) {
            if ($this.hasNext()) {
                return new Stream.Cons($this.next(), new Iterator$$anonfun$toStream$1($this));
            }
            return Stream$Empty$.MODULE$;
        }

        public static String toString(Iterator iterator) {
            return new StringBuilder().append((Object) (iterator.hasNext() ? "non-empty" : "empty")).append((Object) " iterator").toString();
        }
    }
}
