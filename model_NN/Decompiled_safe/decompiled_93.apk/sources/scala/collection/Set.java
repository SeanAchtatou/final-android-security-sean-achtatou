package scala.collection;

import scala.Function1;
import scala.ScalaObject;
import scala.collection.generic.GenericSetTemplate;

/* compiled from: Set.scala */
public interface Set<A> extends Function1<A, Boolean>, Iterable<A>, GenericSetTemplate<A, Set>, SetLike<A, Set<A>>, ScalaObject {

    /* renamed from: scala.collection.Set$class  reason: invalid class name */
    /* compiled from: Set.scala */
    public abstract class Cclass {
        public static void $init$(Set $this) {
        }
    }
}
