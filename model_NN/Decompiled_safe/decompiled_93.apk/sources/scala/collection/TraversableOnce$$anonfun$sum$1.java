package scala.collection;

import java.io.Serializable;
import scala.math.Numeric;
import scala.runtime.AbstractFunction2;

/* compiled from: TraversableOnce.scala */
public final class TraversableOnce$$anonfun$sum$1 extends AbstractFunction2 implements Serializable {
    public static final long serialVersionUID = 0;
    private final /* synthetic */ Numeric num$1;

    /* JADX WARN: Type inference failed for: r2v0, types: [scala.math.Numeric, scala.collection.TraversableOnce<A>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public TraversableOnce$$anonfun$sum$1(scala.collection.TraversableOnce r1, scala.collection.TraversableOnce<A> r2) {
        /*
            r0 = this;
            r0.num$1 = r2
            r0.<init>()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.collection.TraversableOnce$$anonfun$sum$1.<init>(scala.collection.TraversableOnce, scala.math.Numeric):void");
    }

    public final B apply(B b, B b2) {
        return this.num$1.plus(b, b2);
    }
}
