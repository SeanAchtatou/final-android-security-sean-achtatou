package scala.collection;

import scala.ScalaObject;
import scala.collection.generic.TraversableFactory;
import scala.collection.mutable.Builder;
import scala.collection.mutable.ListBuffer;

/* compiled from: Iterable.scala */
public final class Iterable$ extends TraversableFactory<Iterable> implements ScalaObject {
    public static final Iterable$ MODULE$ = null;

    static {
        new Iterable$();
    }

    private Iterable$() {
        MODULE$ = this;
    }

    public <A> Builder<A, Iterable<A>> newBuilder() {
        return new ListBuffer();
    }
}
