package scala.collection;

import java.io.Serializable;
import scala.Function2;
import scala.runtime.AbstractFunction1;
import scala.runtime.ObjectRef;

/* compiled from: TraversableOnce.scala */
public final class TraversableOnce$$anonfun$foldLeft$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Function2 op$1;
    public final /* synthetic */ ObjectRef result$2;

    public TraversableOnce$$anonfun$foldLeft$1(TraversableOnce $outer, Function2 function2, ObjectRef objectRef) {
        this.op$1 = function2;
        this.result$2 = objectRef;
    }

    public final void apply(A x) {
        this.result$2.elem = this.op$1.apply(this.result$2.elem, x);
    }
}
