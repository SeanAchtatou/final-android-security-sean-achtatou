package scala.collection;

import java.util.NoSuchElementException;
import scala.Equals;
import scala.Function1;
import scala.ScalaObject;
import scala.Tuple2;
import scala.collection.generic.CanBuildFrom;
import scala.collection.immutable.Stream;
import scala.collection.mutable.Builder;
import scala.runtime.BoxesRunTime;
import scala.runtime.RichInt;
import scala.runtime.ScalaRunTime$;

/* compiled from: IterableLike.scala */
public interface IterableLike<A, Repr> extends Equals, TraversableLike<A, Repr>, ScalaObject {
    boolean canEqual(Object obj);

    <B> void copyToArray(Object obj, int i, int i2);

    boolean exists(Function1<A, Boolean> function1);

    boolean forall(Function1<A, Boolean> function1);

    <U> void foreach(Function1<A, U> function1);

    A head();

    boolean isEmpty();

    Iterator<A> iterator();

    <B> boolean sameElements(Iterable<B> iterable);

    Repr slice(int i, int i2);

    Iterable<A> thisCollection();

    Stream<A> toStream();

    <A1, B, That> That zip(Iterable<B> iterable, CanBuildFrom<Repr, Tuple2<A1, B>, That> canBuildFrom);

    /* renamed from: scala.collection.IterableLike$class  reason: invalid class name */
    /* compiled from: IterableLike.scala */
    public abstract class Cclass {
        public static void $init$(IterableLike $this) {
        }

        public static Iterable thisCollection(IterableLike $this) {
            return (Iterable) $this;
        }

        public static void foreach(IterableLike $this, Function1 f) {
            $this.iterator().foreach(f);
        }

        public static boolean forall(IterableLike $this, Function1 p) {
            return $this.iterator().forall(p);
        }

        public static boolean exists(IterableLike $this, Function1 p) {
            return $this.iterator().exists(p);
        }

        public static boolean isEmpty(IterableLike $this) {
            return !$this.iterator().hasNext();
        }

        public static Object head(IterableLike $this) {
            if (!$this.isEmpty()) {
                return $this.iterator().next();
            }
            throw new NoSuchElementException();
        }

        public static Object slice(IterableLike $this, int from, int until) {
            Builder b = $this.newBuilder();
            b.sizeHintBounded(until - from, $this);
            Iterator it = $this.iterator().drop(from);
            for (int i = from; i < until && it.hasNext(); i++) {
                b.$plus$eq(it.next());
            }
            return b.result();
        }

        public static void copyToArray(IterableLike $this, Object xs, int start, int len) {
            int end = new RichInt(start + len).min(ScalaRunTime$.MODULE$.array_length(xs));
            Iterator it = $this.iterator();
            for (int i = start; i < end && it.hasNext(); i++) {
                ScalaRunTime$.MODULE$.array_update(xs, i, it.next());
            }
        }

        public static Object zip(IterableLike $this, Iterable that, CanBuildFrom bf) {
            Builder b = bf.apply($this.repr());
            Iterator these = $this.iterator();
            Iterator those = that.iterator();
            while (these.hasNext() && those.hasNext()) {
                b.$plus$eq((Object) new Tuple2(these.next(), those.next()));
            }
            return b.result();
        }

        public static boolean sameElements(IterableLike $this, Iterable that) {
            boolean equals;
            Iterator these = $this.iterator();
            Iterator those = that.iterator();
            while (these.hasNext() && those.hasNext()) {
                Object next = these.next();
                Object next2 = those.next();
                if (next == next2) {
                    equals = true;
                    continue;
                } else if (next == null) {
                    equals = false;
                    continue;
                } else if (next instanceof Number) {
                    equals = BoxesRunTime.equalsNumObject((Number) next, next2);
                    continue;
                } else if (next instanceof Character) {
                    equals = BoxesRunTime.equalsCharObject((Character) next, next2);
                    continue;
                } else {
                    equals = next.equals(next2);
                    continue;
                }
                if (!equals) {
                    return false;
                }
            }
            return !these.hasNext() && !those.hasNext();
        }

        public static Stream toStream(IterableLike $this) {
            return $this.iterator().toStream();
        }

        public static boolean canEqual(IterableLike $this, Object that) {
            return true;
        }
    }
}
