package scala.collection;

import java.io.Serializable;
import scala.Tuple2;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

/* compiled from: MapLike.scala */
public final class MapLike$$anonfun$hashCode$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;

    public final int apply(Tuple2<A, B> tuple2) {
        return tuple2 instanceof Number ? BoxesRunTime.hashFromNumber((Number) tuple2) : tuple2.hashCode();
    }

    public MapLike$$anonfun$hashCode$1(MapLike<A, B, This> $outer) {
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return BoxesRunTime.boxToInteger(apply((Tuple2) ((Tuple2) v1)));
    }
}
