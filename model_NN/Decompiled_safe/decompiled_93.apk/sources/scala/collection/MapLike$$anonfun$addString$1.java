package scala.collection;

import java.io.Serializable;
import scala.MatchError;
import scala.Tuple2;
import scala.collection.mutable.StringBuilder;
import scala.runtime.AbstractFunction1;

/* compiled from: MapLike.scala */
public final class MapLike$$anonfun$addString$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;

    public MapLike$$anonfun$addString$1(MapLike<A, B, This> $outer) {
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return apply((Tuple2) ((Tuple2) v1));
    }

    public final String apply(Tuple2<A, B> tuple2) {
        if (tuple2 == null) {
            throw new MatchError(tuple2);
        }
        return new StringBuilder().append((Object) new StringBuilder().append((Object) String.valueOf(tuple2._1())).append((Object) " -> ").toString()).append((Object) tuple2._2()).toString();
    }
}
