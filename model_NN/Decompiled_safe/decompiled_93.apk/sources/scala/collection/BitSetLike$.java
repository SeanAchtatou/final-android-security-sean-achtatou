package scala.collection;

import scala.Array$;
import scala.Predef$;
import scala.ScalaObject;
import scala.reflect.Manifest$;

/* compiled from: BitSetLike.scala */
public final class BitSetLike$ implements ScalaObject {
    public static final BitSetLike$ MODULE$ = null;
    private final int LogWL = 6;
    private final int[] pc1 = ((int[]) Array$.MODULE$.tabulate(256, new BitSetLike$$anonfun$1(), Manifest$.MODULE$.Int()));
    private final int scala$collection$BitSetLike$$WordLength = 64;

    static {
        new BitSetLike$();
    }

    private BitSetLike$() {
        MODULE$ = this;
    }

    public int LogWL() {
        return this.LogWL;
    }

    public final int scala$collection$BitSetLike$$WordLength() {
        return this.scala$collection$BitSetLike$$WordLength;
    }

    public long[] updateArray(long[] elems, int idx, long w) {
        int len = elems.length;
        while (len > 0 && (elems[len - 1] == 0 || (w == 0 && idx == len - 1))) {
            len--;
        }
        int newlen = len;
        if (idx >= len && w != 0) {
            newlen = idx + 1;
        }
        long[] newelems = new long[newlen];
        Array$.MODULE$.copy(elems, 0, newelems, 0, len);
        if (idx < newlen) {
            newelems[idx] = w;
        } else {
            Predef$.MODULE$.m0assert(w == 0);
        }
        return newelems;
    }

    private int[] pc1() {
        return this.pc1;
    }

    public final int countBits$1(int x) {
        if (x == 0) {
            return 0;
        }
        return (x % 2) + countBits$1(x >>> 1);
    }

    private final int pc2$1(int w) {
        if (w == 0) {
            return 0;
        }
        return pc1()[w & 255] + pc1()[w >>> 8];
    }

    private final int pc4$1(int w) {
        if (w == 0) {
            return 0;
        }
        return pc2$1(65535 & w) + pc2$1(w >>> 16);
    }

    public final int scala$collection$BitSetLike$$popCount(long w) {
        if (w == 0) {
            return 0;
        }
        return pc4$1((int) w) + pc4$1((int) (w >>> 32));
    }
}
