package scala.collection;

import scala.Function1;
import scala.Function2;
import scala.ScalaObject;
import scala.collection.BufferedIterator;
import scala.collection.Iterator;
import scala.collection.TraversableOnce;
import scala.collection.immutable.List;
import scala.collection.immutable.Set;
import scala.collection.immutable.Stream;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.StringBuilder;
import scala.math.Numeric;
import scala.reflect.ClassManifest;

/* compiled from: IndexedSeqLike.scala */
public interface IndexedSeqLike<A, Repr> extends SeqLike<A, Repr>, ScalaObject {
    IndexedSeq<A> thisCollection();

    IndexedSeq<A> toCollection(Object obj);

    /* renamed from: scala.collection.IndexedSeqLike$class  reason: invalid class name */
    /* compiled from: IndexedSeqLike.scala */
    public abstract class Cclass {
        public static void $init$(IndexedSeqLike $this) {
        }

        public static IndexedSeq thisCollection(IndexedSeqLike $this) {
            return (IndexedSeq) $this;
        }

        public static IndexedSeq toCollection(IndexedSeqLike $this, Object repr) {
            return (IndexedSeq) repr;
        }

        public static Iterator iterator(IndexedSeqLike $this) {
            return new Elements($this, 0, $this.length());
        }
    }

    /* compiled from: IndexedSeqLike.scala */
    public class Elements implements BufferedIterator<A>, ScalaObject, BufferedIterator {
        public static final long serialVersionUID = 1756321872811029277L;
        public final /* synthetic */ IndexedSeqLike $outer;
        private final int end;
        private int i;

        public Elements(IndexedSeqLike<A, Repr> $outer2, int start, int end2) {
            this.end = end2;
            if ($outer2 == null) {
                throw new NullPointerException();
            }
            this.$outer = $outer2;
            TraversableOnce.Cclass.$init$(this);
            Iterator.Cclass.$init$(this);
            BufferedIterator.Cclass.$init$(this);
            this.i = start;
        }

        public <B> B $div$colon(B z, Function2<B, A, B> op) {
            return TraversableOnce.Cclass.$div$colon(this, z, op);
        }

        public StringBuilder addString(StringBuilder b, String start, String sep, String end2) {
            return TraversableOnce.Cclass.addString(this, b, start, sep, end2);
        }

        public <B> void copyToArray(Object xs, int start) {
            TraversableOnce.Cclass.copyToArray(this, xs, start);
        }

        public <B> void copyToArray(Object xs, int start, int len) {
            Iterator.Cclass.copyToArray(this, xs, start, len);
        }

        public boolean exists(Function1<A, Boolean> p) {
            return Iterator.Cclass.exists(this, p);
        }

        public <B> B foldLeft(B z, Function2<B, A, B> op) {
            return TraversableOnce.Cclass.foldLeft(this, z, op);
        }

        public boolean forall(Function1<A, Boolean> p) {
            return Iterator.Cclass.forall(this, p);
        }

        public <U> void foreach(Function1<A, U> f) {
            Iterator.Cclass.foreach(this, f);
        }

        public boolean isEmpty() {
            return Iterator.Cclass.isEmpty(this);
        }

        public boolean isTraversableAgain() {
            return Iterator.Cclass.isTraversableAgain(this);
        }

        public int length() {
            return Iterator.Cclass.length(this);
        }

        public <B> Iterator<B> map(Function1<A, B> f) {
            return Iterator.Cclass.map(this, f);
        }

        public String mkString() {
            return TraversableOnce.Cclass.mkString(this);
        }

        public String mkString(String sep) {
            return TraversableOnce.Cclass.mkString(this, sep);
        }

        public String mkString(String start, String sep, String end2) {
            return TraversableOnce.Cclass.mkString(this, start, sep, end2);
        }

        public boolean nonEmpty() {
            return TraversableOnce.Cclass.nonEmpty(this);
        }

        public <B> B reduceLeft(Function2<B, A, B> op) {
            return TraversableOnce.Cclass.reduceLeft(this, op);
        }

        public /* synthetic */ IndexedSeqLike scala$collection$IndexedSeqLike$Elements$$$outer() {
            return this.$outer;
        }

        public int size() {
            return TraversableOnce.Cclass.size(this);
        }

        public <B> B sum(Numeric<B> num) {
            return TraversableOnce.Cclass.sum(this, num);
        }

        public <B> Object toArray(ClassManifest<B> evidence$1) {
            return TraversableOnce.Cclass.toArray(this, evidence$1);
        }

        public <B> Buffer<B> toBuffer() {
            return TraversableOnce.Cclass.toBuffer(this);
        }

        public List<A> toList() {
            return TraversableOnce.Cclass.toList(this);
        }

        public <B> Set<B> toSet() {
            return TraversableOnce.Cclass.toSet(this);
        }

        public Stream<A> toStream() {
            return Iterator.Cclass.toStream(this);
        }

        public String toString() {
            return Iterator.Cclass.toString(this);
        }

        private int i() {
            return this.i;
        }

        private void i_$eq(int i2) {
            this.i = i2;
        }

        public boolean hasNext() {
            return i() < this.end;
        }

        public A next() {
            if (i() >= this.end) {
                return Iterator$.MODULE$.empty().next();
            }
            Object x = scala$collection$IndexedSeqLike$Elements$$$outer().apply(i());
            i_$eq(i() + 1);
            return x;
        }

        public Iterator<A> drop(int n) {
            return n > 0 ? new Elements(scala$collection$IndexedSeqLike$Elements$$$outer(), i() + n, this.end) : this;
        }
    }
}
