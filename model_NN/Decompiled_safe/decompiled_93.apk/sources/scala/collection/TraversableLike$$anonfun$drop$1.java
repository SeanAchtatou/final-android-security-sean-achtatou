package scala.collection;

import java.io.Serializable;
import scala.collection.mutable.Builder;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxedUnit;
import scala.runtime.IntRef;

/* compiled from: TraversableLike.scala */
public final class TraversableLike$$anonfun$drop$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Builder b$10;
    public final /* synthetic */ IntRef i$2;
    public final /* synthetic */ int n$2;

    public TraversableLike$$anonfun$drop$1(TraversableLike $outer, int i, Builder builder, IntRef intRef) {
        this.n$2 = i;
        this.b$10 = builder;
        this.i$2 = intRef;
    }

    public final void apply(A x) {
        if (this.i$2.elem >= this.n$2) {
            this.b$10.$plus$eq((Object) x);
        } else {
            BoxedUnit boxedUnit = BoxedUnit.UNIT;
        }
        this.i$2.elem++;
    }
}
