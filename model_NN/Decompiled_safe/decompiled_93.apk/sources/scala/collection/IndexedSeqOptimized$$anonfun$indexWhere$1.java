package scala.collection;

import java.io.Serializable;
import scala.Function1;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

/* compiled from: IndexedSeqOptimized.scala */
public final class IndexedSeqOptimized$$anonfun$indexWhere$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Function1 p$4;

    /* JADX WARN: Type inference failed for: r2v0, types: [scala.collection.IndexedSeqOptimized<A, Repr>, scala.Function1] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public IndexedSeqOptimized$$anonfun$indexWhere$1(scala.collection.IndexedSeqOptimized r1, scala.collection.IndexedSeqOptimized<A, Repr> r2) {
        /*
            r0 = this;
            r0.p$4 = r2
            r0.<init>()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.collection.IndexedSeqOptimized$$anonfun$indexWhere$1.<init>(scala.collection.IndexedSeqOptimized, scala.Function1):void");
    }

    public final boolean apply(A a) {
        return !BoxesRunTime.unboxToBoolean(this.p$4.apply(a));
    }
}
