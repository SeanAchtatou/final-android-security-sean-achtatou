package scala.collection;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.ObjectRef;

/* compiled from: TraversableLike.scala */
public final class TraversableLike$$anonfun$last$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ ObjectRef lst$1;

    /* JADX WARN: Type inference failed for: r2v0, types: [scala.collection.TraversableLike<A, Repr>, scala.runtime.ObjectRef] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public TraversableLike$$anonfun$last$1(scala.collection.TraversableLike r1, scala.collection.TraversableLike<A, Repr> r2) {
        /*
            r0 = this;
            r0.lst$1 = r2
            r0.<init>()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.collection.TraversableLike$$anonfun$last$1.<init>(scala.collection.TraversableLike, scala.runtime.ObjectRef):void");
    }

    public final void apply(A x) {
        this.lst$1.elem = x;
    }
}
