package scala.collection;

import scala.ScalaObject;
import scala.collection.generic.GenericTraversableTemplate;

/* compiled from: IndexedSeq.scala */
public interface IndexedSeq<A> extends Seq<A>, GenericTraversableTemplate<A, IndexedSeq>, IndexedSeqLike<A, IndexedSeq<A>>, ScalaObject {

    /* renamed from: scala.collection.IndexedSeq$class  reason: invalid class name */
    /* compiled from: IndexedSeq.scala */
    public abstract class Cclass {
        public static void $init$(IndexedSeq $this) {
        }
    }
}
