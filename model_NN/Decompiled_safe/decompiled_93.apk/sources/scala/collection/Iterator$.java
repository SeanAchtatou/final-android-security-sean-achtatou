package scala.collection;

import scala.ScalaObject;

/* compiled from: Iterator.scala */
public final class Iterator$ implements ScalaObject {
    public static final Iterator$ MODULE$ = null;
    private final Iterator empty = new Iterator$$anon$3();

    static {
        new Iterator$();
    }

    private Iterator$() {
        MODULE$ = this;
    }

    public Iterator empty() {
        return this.empty;
    }
}
