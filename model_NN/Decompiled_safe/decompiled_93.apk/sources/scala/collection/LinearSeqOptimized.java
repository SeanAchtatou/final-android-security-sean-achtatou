package scala.collection;

import java.util.NoSuchElementException;
import scala.Function1;
import scala.Function2;
import scala.ScalaObject;
import scala.collection.LinearSeqOptimized;
import scala.collection.mutable.Builder;
import scala.runtime.BoxesRunTime;

/* compiled from: LinearSeqOptimized.scala */
public interface LinearSeqOptimized<A, Repr extends LinearSeqOptimized<A, Repr>> extends LinearSeqLike<A, Repr>, ScalaObject {
    A apply(int i);

    int count(Function1<A, Boolean> function1);

    Repr drop(int i);

    boolean exists(Function1<A, Boolean> function1);

    <B> B foldLeft(B b, Function2<B, A, B> function2);

    boolean forall(Function1<A, Boolean> function1);

    <B> void foreach(Function1<A, B> function1);

    boolean isDefinedAt(int i);

    int length();

    int lengthCompare(int i);

    boolean scala$collection$LinearSeqOptimized$$super$sameElements(Iterable iterable);

    Repr slice(int i, int i2);

    /* renamed from: scala.collection.LinearSeqOptimized$class  reason: invalid class name */
    /* compiled from: LinearSeqOptimized.scala */
    public abstract class Cclass {
        public static void $init$(LinearSeqOptimized $this) {
        }

        public static int length(LinearSeqOptimized $this) {
            int len = 0;
            for (LinearSeqOptimized these = $this; !these.isEmpty(); these = (LinearSeqOptimized) these.tail()) {
                len++;
            }
            return len;
        }

        public static Object apply(LinearSeqOptimized $this, int n) {
            LinearSeqOptimized rest = $this.drop(n);
            if (n >= 0 && !rest.isEmpty()) {
                return rest.head();
            }
            throw new IndexOutOfBoundsException();
        }

        public static void foreach(LinearSeqOptimized $this, Function1 f) {
            for (LinearSeqOptimized these = $this; !these.isEmpty(); these = (LinearSeqOptimized) these.tail()) {
                f.apply(these.head());
            }
        }

        public static boolean forall(LinearSeqOptimized $this, Function1 p) {
            for (LinearSeqOptimized these = $this; !these.isEmpty(); these = (LinearSeqOptimized) these.tail()) {
                if (!BoxesRunTime.unboxToBoolean(p.apply(these.head()))) {
                    return false;
                }
            }
            return true;
        }

        public static boolean exists(LinearSeqOptimized $this, Function1 p) {
            for (LinearSeqOptimized these = $this; !these.isEmpty(); these = (LinearSeqOptimized) these.tail()) {
                if (BoxesRunTime.unboxToBoolean(p.apply(these.head()))) {
                    return true;
                }
            }
            return false;
        }

        public static int count(LinearSeqOptimized $this, Function1 p) {
            int cnt = 0;
            for (LinearSeqOptimized these = $this; !these.isEmpty(); these = (LinearSeqOptimized) these.tail()) {
                if (BoxesRunTime.unboxToBoolean(p.apply(these.head()))) {
                    cnt++;
                }
            }
            return cnt;
        }

        public static Object foldLeft(LinearSeqOptimized $this, Object z, Function2 f) {
            Object acc = z;
            for (LinearSeqOptimized these = $this; !these.isEmpty(); these = (LinearSeqOptimized) these.tail()) {
                acc = f.apply(acc, these.head());
            }
            return acc;
        }

        public static Object reduceLeft(LinearSeqOptimized $this, Function2 f) {
            if (!$this.isEmpty()) {
                return ((LinearSeqOptimized) $this.tail()).foldLeft($this.head(), f);
            }
            throw new UnsupportedOperationException("empty.reduceLeft");
        }

        public static Object last(LinearSeqOptimized $this) {
            if ($this.isEmpty()) {
                throw new NoSuchElementException();
            }
            LinearSeqOptimized these = $this;
            Object tail = $this.tail();
            while (true) {
                LinearSeqOptimized nx = (LinearSeqOptimized) tail;
                if (nx.isEmpty()) {
                    return these.head();
                }
                these = nx;
                tail = nx.tail();
            }
        }

        public static LinearSeqOptimized drop(LinearSeqOptimized $this, int n) {
            LinearSeqOptimized these = (LinearSeqOptimized) $this.repr();
            int count = n;
            while (!these.isEmpty() && count > 0) {
                these = (LinearSeqOptimized) these.tail();
                count--;
            }
            return these;
        }

        public static LinearSeqOptimized slice(LinearSeqOptimized $this, int from, int until) {
            Builder b = $this.newBuilder();
            LinearSeqOptimized these = $this.drop(from);
            for (int i = from; i < until && !these.isEmpty(); i++) {
                b.$plus$eq(these.head());
                these = (LinearSeqOptimized) these.tail();
            }
            return (LinearSeqOptimized) b.result();
        }

        public static boolean sameElements(LinearSeqOptimized $this, Iterable that) {
            if (!(that instanceof LinearSeq)) {
                return $this.scala$collection$LinearSeqOptimized$$super$sameElements(that);
            }
            LinearSeq those = (LinearSeq) that;
            LinearSeqOptimized these = $this;
            while (!these.isEmpty() && !those.isEmpty()) {
                Object head = these.head();
                Object head2 = those.head();
                if (!(head == head2 ? true : head == null ? false : head instanceof Number ? BoxesRunTime.equalsNumObject((Number) head, head2) : head instanceof Character ? BoxesRunTime.equalsCharObject((Character) head, head2) : head.equals(head2))) {
                    break;
                }
                these = (LinearSeqOptimized) these.tail();
                those = (LinearSeq) those.tail();
            }
            return these.isEmpty() && those.isEmpty();
        }

        public static int lengthCompare(LinearSeqOptimized $this, int len) {
            int i = 0;
            for (LinearSeqOptimized these = $this; !these.isEmpty() && i <= len; these = (LinearSeqOptimized) these.tail()) {
                i++;
            }
            return i - len;
        }

        public static boolean isDefinedAt(LinearSeqOptimized $this, int x) {
            return x >= 0 && $this.lengthCompare(x) > 0;
        }

        public static int segmentLength(LinearSeqOptimized $this, Function1 p, int from) {
            int i = 0;
            LinearSeqOptimized these = $this.drop(from);
            while (!these.isEmpty() && BoxesRunTime.unboxToBoolean(p.apply(these.head()))) {
                i++;
                these = (LinearSeqOptimized) these.tail();
            }
            return i;
        }

        public static int indexWhere(LinearSeqOptimized $this, Function1 p, int from) {
            int i = from;
            for (LinearSeqOptimized these = $this.drop(from); these.nonEmpty(); these = (LinearSeqOptimized) these.tail()) {
                if (BoxesRunTime.unboxToBoolean(p.apply(these.head()))) {
                    return i;
                }
                i++;
            }
            return -1;
        }
    }
}
