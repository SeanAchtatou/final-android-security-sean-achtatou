package scala.collection;

import java.io.Serializable;
import scala.Function1;
import scala.collection.mutable.Builder;
import scala.runtime.AbstractFunction1;

/* compiled from: TraversableLike.scala */
public final class TraversableLike$$anonfun$map$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Builder b$1;
    public final /* synthetic */ Function1 f$1;

    public TraversableLike$$anonfun$map$1(TraversableLike $outer, Function1 function1, Builder builder) {
        this.f$1 = function1;
        this.b$1 = builder;
    }

    public final Builder<B, That> apply(A x) {
        return this.b$1.$plus$eq(this.f$1.apply(x));
    }
}
