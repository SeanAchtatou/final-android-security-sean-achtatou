package scala.collection.generic;

import scala.ScalaObject;
import scala.collection.Set;
import scala.collection.mutable.Builder;

/* compiled from: SetFactory.scala */
public abstract class SetFactory<CC extends Set<Object>> extends GenericCompanion<CC> implements ScalaObject {
    public abstract <A> Builder<A, CC> newBuilder();

    public <A> Object setCanBuildFrom() {
        return new SetFactory$$anon$1(this);
    }
}
