package scala.collection.generic;

import scala.ScalaObject;
import scala.collection.immutable.Map;

/* compiled from: ImmutableMapFactory.scala */
public abstract class ImmutableMapFactory<CC extends Map<Object, Object>> extends MapFactory<CC> implements ScalaObject {
}
