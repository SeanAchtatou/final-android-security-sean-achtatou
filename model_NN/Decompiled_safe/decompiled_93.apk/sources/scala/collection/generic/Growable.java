package scala.collection.generic;

import scala.ScalaObject;
import scala.collection.TraversableOnce;

/* compiled from: Growable.scala */
public interface Growable<A> extends ScalaObject {
    Growable<A> $plus$eq(Object obj);

    Growable<A> $plus$plus$eq(TraversableOnce traversableOnce);

    /* renamed from: scala.collection.generic.Growable$class  reason: invalid class name */
    /* compiled from: Growable.scala */
    public abstract class Cclass {
        public static void $init$(Growable $this) {
        }

        public static Growable $plus$plus$eq(Growable $this, TraversableOnce xs) {
            xs.foreach(new Growable$$anonfun$$plus$plus$eq$1($this));
            return $this;
        }
    }
}
