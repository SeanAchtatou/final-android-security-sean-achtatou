package scala.collection.generic;

import java.io.Serializable;
import scala.runtime.AbstractFunction2;

/* compiled from: Addable.scala */
public final class Addable$$anonfun$$plus$plus$1 extends AbstractFunction2 implements Serializable {
    public static final long serialVersionUID = 0;

    public Addable$$anonfun$$plus$plus$1(Addable<A, Repr> $outer) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: scala.collection.generic.Addable$$anonfun$$plus$plus$1.apply(scala.collection.generic.Addable, java.lang.Object):Repr
     arg types: [java.lang.Object, java.lang.Object]
     candidates:
      scala.collection.generic.Addable$$anonfun$$plus$plus$1.apply(java.lang.Object, java.lang.Object):java.lang.Object
      scala.Function2.apply(java.lang.Object, java.lang.Object):R
      scala.collection.generic.Addable$$anonfun$$plus$plus$1.apply(scala.collection.generic.Addable, java.lang.Object):Repr */
    public final /* bridge */ /* synthetic */ Object apply(Object v1, Object v2) {
        return apply((Addable) ((Addable) v1), v2);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: Repr
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final Repr apply(Repr r2, A r3) {
        /*
            r1 = this;
            scala.collection.generic.Addable r0 = r2.$plus(r3)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.collection.generic.Addable$$anonfun$$plus$plus$1.apply(scala.collection.generic.Addable, java.lang.Object):scala.collection.generic.Addable");
    }
}
