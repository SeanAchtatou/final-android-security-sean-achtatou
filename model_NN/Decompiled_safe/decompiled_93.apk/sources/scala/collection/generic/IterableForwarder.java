package scala.collection.generic;

import scala.ScalaObject;
import scala.collection.Iterable;

/* compiled from: IterableForwarder.scala */
public interface IterableForwarder<A> extends Iterable<A>, TraversableForwarder<A>, ScalaObject {
    Iterable<A> underlying();

    /* renamed from: scala.collection.generic.IterableForwarder$class  reason: invalid class name */
    /* compiled from: IterableForwarder.scala */
    public abstract class Cclass {
        public static void $init$(IterableForwarder $this) {
        }

        public static boolean sameElements(IterableForwarder $this, Iterable that) {
            return $this.underlying().sameElements(that);
        }
    }
}
