package scala.collection.generic;

import scala.Function1;
import scala.ScalaObject;
import scala.collection.Traversable;
import scala.collection.mutable.Builder;

/* compiled from: GenericTraversableTemplate.scala */
public interface GenericTraversableTemplate<A, CC extends Traversable<Object>> extends ScalaObject {
    GenericCompanion<CC> companion();

    <U> void foreach(Function1<A, U> function1);

    <B> Builder<B, CC> genericBuilder();

    /* renamed from: scala.collection.generic.GenericTraversableTemplate$class  reason: invalid class name */
    /* compiled from: GenericTraversableTemplate.scala */
    public abstract class Cclass {
        public static void $init$(GenericTraversableTemplate $this) {
        }

        public static Builder newBuilder(GenericTraversableTemplate $this) {
            return $this.companion().newBuilder();
        }

        public static Builder genericBuilder(GenericTraversableTemplate $this) {
            return $this.companion().newBuilder();
        }
    }
}
