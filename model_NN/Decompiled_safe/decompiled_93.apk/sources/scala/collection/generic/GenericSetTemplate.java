package scala.collection.generic;

import scala.ScalaObject;
import scala.collection.Set;

/* compiled from: GenericSetTemplate.scala */
public interface GenericSetTemplate<A, CC extends Set<Object>> extends GenericTraversableTemplate<A, CC>, ScalaObject {

    /* renamed from: scala.collection.generic.GenericSetTemplate$class  reason: invalid class name */
    /* compiled from: GenericSetTemplate.scala */
    public abstract class Cclass {
        public static void $init$(GenericSetTemplate $this) {
        }

        public static Set empty(GenericSetTemplate $this) {
            return (Set) $this.companion().empty();
        }
    }
}
