package scala.collection.generic;

import scala.ScalaObject;
import scala.collection.TraversableOnce;
import scala.collection.generic.Addable;

/* compiled from: Addable.scala */
public interface Addable<A, Repr extends Addable<A, Repr>> extends ScalaObject {
    Repr $plus(Object obj);

    Repr $plus$plus(TraversableOnce<A> traversableOnce);

    Repr repr();

    /* renamed from: scala.collection.generic.Addable$class  reason: invalid class name */
    /* compiled from: Addable.scala */
    public abstract class Cclass {
        public static void $init$(Addable $this) {
        }

        public static Addable $plus$plus(Addable $this, TraversableOnce xs) {
            return (Addable) xs.$div$colon($this.repr(), new Addable$$anonfun$$plus$plus$1($this));
        }
    }
}
