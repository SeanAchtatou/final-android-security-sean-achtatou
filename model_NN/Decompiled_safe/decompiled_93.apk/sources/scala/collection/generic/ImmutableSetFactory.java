package scala.collection.generic;

import scala.ScalaObject;
import scala.collection.immutable.Set;
import scala.collection.mutable.AddingBuilder;
import scala.collection.mutable.Builder;

/* compiled from: ImmutableSetFactory.scala */
public abstract class ImmutableSetFactory<CC extends Set<Object>> extends SetFactory<CC> implements ScalaObject {
    public <A> Builder<A, CC> newBuilder() {
        return new AddingBuilder((Addable) empty());
    }
}
