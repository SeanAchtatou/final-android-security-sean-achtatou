package scala.collection.generic;

import scala.Function0;
import scala.ScalaObject;
import scala.collection.Traversable;
import scala.collection.mutable.Builder;

/* compiled from: TraversableFactory.scala */
public abstract class TraversableFactory<CC extends Traversable<Object>> extends GenericCompanion<CC> implements ScalaObject {

    /* compiled from: TraversableFactory.scala */
    public class GenericCanBuildFrom<A> implements CanBuildFrom<CC, A, CC>, ScalaObject {
        public final /* synthetic */ TraversableFactory $outer;

        public GenericCanBuildFrom(TraversableFactory<CC> $outer2) {
            if ($outer2 == null) {
                throw new NullPointerException();
            }
            this.$outer = $outer2;
        }

        public /* bridge */ /* synthetic */ Builder apply(Object from) {
            return apply((Traversable) ((Traversable) from));
        }

        public Builder<A, CC> apply(CC from) {
            return from.genericBuilder();
        }
    }

    public <A> CC fill(int n, Function0<A> elem) {
        Builder b = newBuilder();
        b.sizeHint(n);
        for (int i = 0; i < n; i++) {
            b.$plus$eq((Object) elem.apply());
        }
        return (Traversable) b.result();
    }
}
