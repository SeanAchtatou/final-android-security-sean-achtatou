package scala.collection.generic;

import scala.ScalaObject;
import scala.Tuple2;
import scala.collection.Map;
import scala.collection.Seq;
import scala.collection.mutable.Builder;
import scala.collection.mutable.MapBuilder;

/* compiled from: MapFactory.scala */
public abstract class MapFactory<CC extends Map<Object, Object>> implements ScalaObject {
    public abstract <A, B> CC empty();

    public <A, B> CC apply(Seq<Tuple2<A, B>> elems) {
        return (Map) ((Builder) newBuilder().$plus$plus$eq(elems)).result();
    }

    public <A, B> Builder<Tuple2<A, B>, CC> newBuilder() {
        return new MapBuilder(empty());
    }

    /* compiled from: MapFactory.scala */
    public class MapCanBuildFrom<A, B> implements CanBuildFrom<CC, Tuple2<A, B>, CC>, ScalaObject {
        public final /* synthetic */ MapFactory $outer;

        public MapCanBuildFrom(MapFactory<CC> $outer2) {
            if ($outer2 == null) {
                throw new NullPointerException();
            }
            this.$outer = $outer2;
        }

        public /* bridge */ /* synthetic */ Builder apply(Object from) {
            return apply((Map) ((Map) from));
        }

        public /* synthetic */ MapFactory scala$collection$generic$MapFactory$MapCanBuildFrom$$$outer() {
            return this.$outer;
        }

        public Builder<Tuple2<A, B>, CC> apply(CC from) {
            return scala$collection$generic$MapFactory$MapCanBuildFrom$$$outer().newBuilder();
        }
    }
}
