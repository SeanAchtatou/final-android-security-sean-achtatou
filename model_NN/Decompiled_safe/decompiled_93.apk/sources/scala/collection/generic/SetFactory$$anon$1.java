package scala.collection.generic;

import scala.collection.Set;
import scala.collection.mutable.Builder;

/* compiled from: SetFactory.scala */
public final class SetFactory$$anon$1 implements CanBuildFrom<CC, Object, CC> {
    public final /* synthetic */ SetFactory $outer;

    public SetFactory$$anon$1(SetFactory<CC> $outer2) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
    }

    public /* bridge */ /* synthetic */ Builder apply(Object from) {
        return apply((Set) ((Set) from));
    }

    public Builder<A, CC> apply(CC from) {
        return this.$outer.newBuilder();
    }
}
