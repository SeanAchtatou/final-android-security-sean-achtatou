package scala.collection.generic;

import scala.ScalaObject;
import scala.collection.Seq;
import scala.collection.Traversable;
import scala.collection.mutable.Builder;

/* compiled from: GenericCompanion.scala */
public abstract class GenericCompanion<CC extends Traversable<Object>> implements ScalaObject {
    public abstract <A> Builder<A, CC> newBuilder();

    public <A> CC empty() {
        return (Traversable) newBuilder().result();
    }

    public <A> CC apply(Seq<A> elems) {
        Builder b = newBuilder();
        b.$plus$plus$eq(elems);
        return (Traversable) b.result();
    }
}
