package scala.collection.generic;

import scala.collection.mutable.Builder;

/* compiled from: CanBuildFrom.scala */
public interface CanBuildFrom<From, Elem, To> {
    Builder<Elem, To> apply(Object obj);
}
