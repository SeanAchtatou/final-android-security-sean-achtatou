package scala.collection.generic;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;

/* compiled from: Growable.scala */
public final class Growable$$anonfun$$plus$plus$eq$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Growable $outer;

    public Growable$$anonfun$$plus$plus$eq$1(Growable<A> $outer2) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
    }

    public final Growable<A> apply(A a) {
        return this.$outer.$plus$eq(a);
    }
}
