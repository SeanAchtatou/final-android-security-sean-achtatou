package scala.collection.generic;

import scala.Function1;
import scala.ScalaObject;
import scala.collection.Iterator;
import scala.collection.Seq;

/* compiled from: SeqForwarder.scala */
public interface SeqForwarder<A> extends Seq<A>, IterableForwarder<A>, ScalaObject {
    boolean isDefinedAt(int i);

    Seq<A> underlying();

    /* renamed from: scala.collection.generic.SeqForwarder$class  reason: invalid class name */
    /* compiled from: SeqForwarder.scala */
    public abstract class Cclass {
        public static void $init$(SeqForwarder $this) {
        }

        public static Object apply(SeqForwarder $this, int idx) {
            return $this.underlying().apply(idx);
        }

        public static boolean isDefinedAt(SeqForwarder $this, int x) {
            return $this.underlying().isDefinedAt(x);
        }

        public static int segmentLength(SeqForwarder $this, Function1 p, int from) {
            return $this.underlying().segmentLength(p, from);
        }

        public static int prefixLength(SeqForwarder $this, Function1 p) {
            return $this.underlying().prefixLength(p);
        }

        public static int indexWhere(SeqForwarder $this, Function1 p, int from) {
            return $this.underlying().indexWhere(p, from);
        }

        public static int indexOf(SeqForwarder $this, Object elem) {
            return $this.underlying().indexOf(elem);
        }

        public static int indexOf(SeqForwarder $this, Object elem, int from) {
            return $this.underlying().indexOf(elem, from);
        }

        public static Iterator reverseIterator(SeqForwarder $this) {
            return $this.underlying().reverseIterator();
        }

        public static boolean contains(SeqForwarder $this, Object elem) {
            return $this.underlying().contains(elem);
        }
    }
}
