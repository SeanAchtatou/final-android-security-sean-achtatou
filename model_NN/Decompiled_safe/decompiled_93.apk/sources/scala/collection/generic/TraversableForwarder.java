package scala.collection.generic;

import scala.Function1;
import scala.Function2;
import scala.ScalaObject;
import scala.collection.Traversable;
import scala.collection.immutable.Set;
import scala.collection.immutable.Stream;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.StringBuilder;
import scala.math.Numeric;
import scala.reflect.ClassManifest;

/* compiled from: TraversableForwarder.scala */
public interface TraversableForwarder<A> extends Traversable<A>, ScalaObject {
    Traversable<A> underlying();

    /* renamed from: scala.collection.generic.TraversableForwarder$class  reason: invalid class name */
    /* compiled from: TraversableForwarder.scala */
    public abstract class Cclass {
        public static void $init$(TraversableForwarder $this) {
        }

        public static void foreach(TraversableForwarder $this, Function1 f) {
            $this.underlying().foreach(f);
        }

        public static boolean isEmpty(TraversableForwarder $this) {
            return $this.underlying().isEmpty();
        }

        public static boolean nonEmpty(TraversableForwarder $this) {
            return $this.underlying().nonEmpty();
        }

        public static int size(TraversableForwarder $this) {
            return $this.underlying().size();
        }

        public static boolean forall(TraversableForwarder $this, Function1 p) {
            return $this.underlying().forall(p);
        }

        public static boolean exists(TraversableForwarder $this, Function1 p) {
            return $this.underlying().exists(p);
        }

        public static Object foldLeft(TraversableForwarder $this, Object z, Function2 op) {
            return $this.underlying().foldLeft(z, op);
        }

        public static Object $div$colon(TraversableForwarder $this, Object z, Function2 op) {
            return $this.underlying().$div$colon(z, op);
        }

        public static Object reduceLeft(TraversableForwarder $this, Function2 op) {
            return $this.underlying().reduceLeft(op);
        }

        public static Object sum(TraversableForwarder $this, Numeric num) {
            return $this.underlying().sum(num);
        }

        public static Object head(TraversableForwarder $this) {
            return $this.underlying().head();
        }

        public static Object last(TraversableForwarder $this) {
            return $this.underlying().last();
        }

        public static void copyToArray(TraversableForwarder $this, Object xs, int start, int len) {
            $this.underlying().copyToArray(xs, start, len);
        }

        public static void copyToArray(TraversableForwarder $this, Object xs, int start) {
            $this.underlying().copyToArray(xs, start);
        }

        public static Object toArray(TraversableForwarder $this, ClassManifest evidence$1) {
            return $this.underlying().toArray(evidence$1);
        }

        public static Buffer toBuffer(TraversableForwarder $this) {
            return $this.underlying().toBuffer();
        }

        public static Stream toStream(TraversableForwarder $this) {
            return $this.underlying().toStream();
        }

        public static Set toSet(TraversableForwarder $this) {
            return $this.underlying().toSet();
        }

        public static String mkString(TraversableForwarder $this, String start, String sep, String end) {
            return $this.underlying().mkString(start, sep, end);
        }

        public static String mkString(TraversableForwarder $this, String sep) {
            return $this.underlying().mkString(sep);
        }

        public static String mkString(TraversableForwarder $this) {
            return $this.underlying().mkString();
        }

        public static StringBuilder addString(TraversableForwarder $this, StringBuilder b, String start, String sep, String end) {
            return $this.underlying().addString(b, start, sep, end);
        }
    }
}
