package scala.collection;

import java.io.Serializable;
import scala.collection.mutable.Builder;
import scala.runtime.AbstractFunction1;

/* compiled from: SeqLike.scala */
public final class SeqLike$$anonfun$sorted$2 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Builder b$6;

    /* JADX WARN: Type inference failed for: r2v0, types: [scala.collection.SeqLike<A, Repr>, scala.collection.mutable.Builder] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SeqLike$$anonfun$sorted$2(scala.collection.SeqLike r1, scala.collection.SeqLike<A, Repr> r2) {
        /*
            r0 = this;
            r0.b$6 = r2
            r0.<init>()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.collection.SeqLike$$anonfun$sorted$2.<init>(scala.collection.SeqLike, scala.collection.mutable.Builder):void");
    }

    public final Builder<A, Repr> apply(A x) {
        return this.b$6.$plus$eq((Object) x);
    }
}
