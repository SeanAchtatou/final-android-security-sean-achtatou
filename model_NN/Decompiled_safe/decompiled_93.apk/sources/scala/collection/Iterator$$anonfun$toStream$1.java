package scala.collection;

import java.io.Serializable;
import scala.collection.immutable.Stream;
import scala.runtime.AbstractFunction0;

/* compiled from: Iterator.scala */
public final class Iterator$$anonfun$toStream$1 extends AbstractFunction0 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Iterator $outer;

    public Iterator$$anonfun$toStream$1(Iterator<A> $outer2) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
    }

    public final Stream<A> apply() {
        return this.$outer.toStream();
    }
}
