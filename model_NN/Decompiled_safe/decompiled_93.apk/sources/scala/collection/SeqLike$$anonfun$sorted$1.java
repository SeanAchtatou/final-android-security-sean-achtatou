package scala.collection;

import java.io.Serializable;
import scala.collection.mutable.ArraySeq;
import scala.runtime.AbstractFunction1;
import scala.runtime.IntRef;

/* compiled from: SeqLike.scala */
public final class SeqLike$$anonfun$sorted$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ ArraySeq arr$1;
    public final /* synthetic */ IntRef i$2;

    public SeqLike$$anonfun$sorted$1(SeqLike $outer, ArraySeq arraySeq, IntRef intRef) {
        this.arr$1 = arraySeq;
        this.i$2 = intRef;
    }

    public final void apply(A x) {
        this.arr$1.update(this.i$2.elem, x);
        this.i$2.elem++;
    }
}
