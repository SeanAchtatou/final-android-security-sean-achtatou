package scala.collection;

import java.io.Serializable;
import scala.collection.mutable.StringBuilder;
import scala.runtime.AbstractFunction1;
import scala.runtime.BooleanRef;
import scala.runtime.BoxedUnit;

/* compiled from: TraversableOnce.scala */
public final class TraversableOnce$$anonfun$addString$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    private final /* synthetic */ StringBuilder b$1;
    private final /* synthetic */ BooleanRef first$2;
    private final /* synthetic */ String sep$1;

    public TraversableOnce$$anonfun$addString$1(TraversableOnce $outer, StringBuilder stringBuilder, String str, BooleanRef booleanRef) {
        this.b$1 = stringBuilder;
        this.sep$1 = str;
        this.first$2 = booleanRef;
    }

    public final Object apply(A x) {
        if (this.first$2.elem) {
            this.b$1.append((Object) x);
            this.first$2.elem = false;
            return BoxedUnit.UNIT;
        }
        this.b$1.append(this.sep$1);
        return this.b$1.append((Object) x);
    }
}
