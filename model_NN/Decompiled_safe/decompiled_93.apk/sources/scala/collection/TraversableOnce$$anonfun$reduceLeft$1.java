package scala.collection;

import java.io.Serializable;
import scala.Function2;
import scala.runtime.AbstractFunction1;
import scala.runtime.BooleanRef;
import scala.runtime.ObjectRef;

/* compiled from: TraversableOnce.scala */
public final class TraversableOnce$$anonfun$reduceLeft$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ ObjectRef acc$1;
    public final /* synthetic */ BooleanRef first$1;
    public final /* synthetic */ Function2 op$3;

    public TraversableOnce$$anonfun$reduceLeft$1(TraversableOnce $outer, Function2 function2, BooleanRef booleanRef, ObjectRef objectRef) {
        this.op$3 = function2;
        this.first$1 = booleanRef;
        this.acc$1 = objectRef;
    }

    public final void apply(A x) {
        if (this.first$1.elem) {
            this.acc$1.elem = x;
            this.first$1.elem = false;
            return;
        }
        this.acc$1.elem = this.op$3.apply(this.acc$1.elem, x);
    }
}
