package scala.collection.immutable;

import java.io.Serializable;
import scala.runtime.AbstractFunction0;
import scala.runtime.ObjectRef;

/* compiled from: Stream.scala */
public final class Stream$$anonfun$1 extends AbstractFunction0 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ ObjectRef result$1;

    /* JADX WARN: Type inference failed for: r2v0, types: [scala.collection.immutable.Stream<A>, scala.runtime.ObjectRef] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Stream$$anonfun$1(scala.collection.immutable.Stream r1, scala.collection.immutable.Stream<A> r2) {
        /*
            r0 = this;
            r0.result$1 = r2
            r0.<init>()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.Stream$$anonfun$1.<init>(scala.collection.immutable.Stream, scala.runtime.ObjectRef):void");
    }

    public final Stream<A> apply() {
        return (Stream) this.result$1.elem;
    }
}
