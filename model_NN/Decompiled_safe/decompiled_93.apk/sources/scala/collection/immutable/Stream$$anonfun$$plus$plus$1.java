package scala.collection.immutable;

import java.io.Serializable;
import scala.collection.TraversableOnce;
import scala.collection.immutable.Stream;
import scala.runtime.AbstractFunction0;

/* compiled from: Stream.scala */
public final class Stream$$anonfun$$plus$plus$1 extends AbstractFunction0 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Stream $outer;
    public final /* synthetic */ TraversableOnce that$1;

    public Stream$$anonfun$$plus$plus$1(Stream $outer2, Stream<A> stream) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.that$1 = stream;
    }

    public final Stream<A> apply() {
        return (Stream) ((Stream) this.$outer.tail()).$plus$plus(this.that$1, new Stream.StreamCanBuildFrom());
    }
}
