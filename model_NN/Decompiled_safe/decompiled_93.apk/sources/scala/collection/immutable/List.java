package scala.collection.immutable;

import scala.Function1;
import scala.Function2;
import scala.PartialFunction;
import scala.Product;
import scala.ScalaObject;
import scala.Tuple2;
import scala.collection.Iterable;
import scala.collection.IterableLike;
import scala.collection.Iterator;
import scala.collection.LinearSeq;
import scala.collection.LinearSeqLike;
import scala.collection.LinearSeqOptimized;
import scala.collection.Seq;
import scala.collection.SeqLike;
import scala.collection.Traversable;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericTraversableTemplate;
import scala.collection.generic.Growable;
import scala.collection.immutable.Iterable;
import scala.collection.immutable.LinearSeq;
import scala.collection.immutable.Seq;
import scala.collection.immutable.Stream;
import scala.collection.immutable.Traversable;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.Builder;
import scala.collection.mutable.ListBuffer;
import scala.collection.mutable.StringBuilder;
import scala.math.Numeric;
import scala.math.Ordering;
import scala.reflect.ClassManifest;
import scala.runtime.BoxesRunTime;

/* compiled from: List.scala */
public abstract class List<A> implements LinearSeq<A>, Product, GenericTraversableTemplate<A, List>, LinearSeqOptimized<A, List<A>>, ScalaObject {
    public List() {
        TraversableOnce.Cclass.$init$(this);
        TraversableLike.Cclass.$init$(this);
        GenericTraversableTemplate.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        IterableLike.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Function1.Cclass.$init$(this);
        PartialFunction.Cclass.$init$(this);
        SeqLike.Cclass.$init$(this);
        Seq.Cclass.$init$(this);
        Seq.Cclass.$init$(this);
        LinearSeqLike.Cclass.$init$(this);
        LinearSeq.Cclass.$init$(this);
        LinearSeq.Cclass.$init$(this);
        Product.Cclass.$init$(this);
        LinearSeqOptimized.Cclass.$init$(this);
    }

    public <B> B $div$colon(B z, Function2<B, A, B> op) {
        return TraversableOnce.Cclass.$div$colon(this, z, op);
    }

    public StringBuilder addString(StringBuilder b, String start, String sep, String end) {
        return TraversableOnce.Cclass.addString(this, b, start, sep, end);
    }

    public <C> PartialFunction<Integer, C> andThen(Function1<A, C> k) {
        return PartialFunction.Cclass.andThen(this, k);
    }

    public A apply(int n) {
        return LinearSeqOptimized.Cclass.apply(this, n);
    }

    public /* bridge */ /* synthetic */ Object apply(Object v1) {
        return apply(BoxesRunTime.unboxToInt(v1));
    }

    public int apply$mcII$sp(int v1) {
        return Function1.Cclass.apply$mcII$sp(this, v1);
    }

    public void apply$mcVI$sp(int v1) {
        Function1.Cclass.apply$mcVI$sp(this, v1);
    }

    public void apply$mcVL$sp(long v1) {
        Function1.Cclass.apply$mcVL$sp(this, v1);
    }

    public boolean canEqual(Object that) {
        return IterableLike.Cclass.canEqual(this, that);
    }

    public <A> Function1<A, A> compose(Function1<A, Integer> g) {
        return Function1.Cclass.compose(this, g);
    }

    public boolean contains(Object elem) {
        return SeqLike.Cclass.contains(this, elem);
    }

    public <B> void copyToArray(Object xs, int start) {
        TraversableOnce.Cclass.copyToArray(this, xs, start);
    }

    public <B> void copyToArray(Object xs, int start, int len) {
        IterableLike.Cclass.copyToArray(this, xs, start, len);
    }

    public int count(Function1<A, Boolean> p) {
        return LinearSeqOptimized.Cclass.count(this, p);
    }

    public boolean equals(Object that) {
        return SeqLike.Cclass.equals(this, that);
    }

    public boolean exists(Function1<A, Boolean> p) {
        return LinearSeqOptimized.Cclass.exists(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.List<A>, java.lang.Object] */
    public List<A> filter(Function1<A, Boolean> p) {
        return TraversableLike.Cclass.filter(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.List<A>, java.lang.Object] */
    public List<A> filterNot(Function1<A, Boolean> p) {
        return TraversableLike.Cclass.filterNot(this, p);
    }

    public <B> B foldLeft(B z, Function2<B, A, B> f) {
        return LinearSeqOptimized.Cclass.foldLeft(this, z, f);
    }

    public boolean forall(Function1<A, Boolean> p) {
        return LinearSeqOptimized.Cclass.forall(this, p);
    }

    public <B> void foreach(Function1<A, B> f) {
        LinearSeqOptimized.Cclass.foreach(this, f);
    }

    public <B> Builder<B, List<B>> genericBuilder() {
        return GenericTraversableTemplate.Cclass.genericBuilder(this);
    }

    public int hashCode() {
        return SeqLike.Cclass.hashCode(this);
    }

    public <B> int indexOf(B elem) {
        return SeqLike.Cclass.indexOf(this, elem);
    }

    public <B> int indexOf(B elem, int from) {
        return SeqLike.Cclass.indexOf(this, elem, from);
    }

    public int indexWhere(Function1<A, Boolean> p, int from) {
        return LinearSeqOptimized.Cclass.indexWhere(this, p, from);
    }

    public boolean isDefinedAt(int x) {
        return LinearSeqOptimized.Cclass.isDefinedAt(this, x);
    }

    public /* bridge */ /* synthetic */ boolean isDefinedAt(Object x) {
        return isDefinedAt(BoxesRunTime.unboxToInt(x));
    }

    public final boolean isTraversableAgain() {
        return TraversableLike.Cclass.isTraversableAgain(this);
    }

    public Iterator<A> iterator() {
        return LinearSeqLike.Cclass.iterator(this);
    }

    public A last() {
        return LinearSeqOptimized.Cclass.last(this);
    }

    public int length() {
        return LinearSeqOptimized.Cclass.length(this);
    }

    public int lengthCompare(int len) {
        return LinearSeqOptimized.Cclass.lengthCompare(this, len);
    }

    public <B, That> That map(Function1<A, B> f, CanBuildFrom<List<A>, B, That> bf) {
        return TraversableLike.Cclass.map(this, f, bf);
    }

    public String mkString() {
        return TraversableOnce.Cclass.mkString(this);
    }

    public String mkString(String sep) {
        return TraversableOnce.Cclass.mkString(this, sep);
    }

    public String mkString(String start, String sep, String end) {
        return TraversableOnce.Cclass.mkString(this, start, sep, end);
    }

    public Builder<A, List<A>> newBuilder() {
        return GenericTraversableTemplate.Cclass.newBuilder(this);
    }

    public boolean nonEmpty() {
        return TraversableOnce.Cclass.nonEmpty(this);
    }

    public int prefixLength(Function1<A, Boolean> p) {
        return SeqLike.Cclass.prefixLength(this, p);
    }

    public Iterator<Object> productIterator() {
        return Product.Cclass.productIterator(this);
    }

    public String productPrefix() {
        return Product.Cclass.productPrefix(this);
    }

    public <B> B reduceLeft(Function2<B, A, B> f) {
        return LinearSeqOptimized.Cclass.reduceLeft(this, f);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.List<A>, java.lang.Object] */
    public List<A> repr() {
        return TraversableLike.Cclass.repr(this);
    }

    public Iterator<A> reverseIterator() {
        return SeqLike.Cclass.reverseIterator(this);
    }

    public <B> boolean sameElements(scala.collection.Iterable<B> that) {
        return LinearSeqOptimized.Cclass.sameElements(this, that);
    }

    public final boolean scala$collection$LinearSeqOptimized$$super$sameElements(scala.collection.Iterable that) {
        return IterableLike.Cclass.sameElements(this, that);
    }

    public int segmentLength(Function1<A, Boolean> p, int from) {
        return LinearSeqOptimized.Cclass.segmentLength(this, p, from);
    }

    public int size() {
        return SeqLike.Cclass.size(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.List<A>, java.lang.Object] */
    public <B> List<A> sortBy(Function1<A, B> f, Ordering<B> ord) {
        return SeqLike.Cclass.sortBy(this, f, ord);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.List<A>, java.lang.Object] */
    public <B> List<A> sorted(Ordering<B> ord) {
        return SeqLike.Cclass.sorted(this, ord);
    }

    public <B> B sum(Numeric<B> num) {
        return TraversableOnce.Cclass.sum(this, num);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.List<A>, java.lang.Object] */
    public List<A> tail() {
        return TraversableLike.Cclass.tail(this);
    }

    public scala.collection.LinearSeq<A> thisCollection() {
        return LinearSeqLike.Cclass.thisCollection(this);
    }

    public <B> Object toArray(ClassManifest<B> evidence$1) {
        return TraversableOnce.Cclass.toArray(this, evidence$1);
    }

    public <B> Buffer<B> toBuffer() {
        return TraversableOnce.Cclass.toBuffer(this);
    }

    public scala.collection.LinearSeq<A> toCollection(List<A> repr) {
        return LinearSeqLike.Cclass.toCollection(this, repr);
    }

    public /* bridge */ /* synthetic */ scala.collection.Seq toCollection(Object repr) {
        return toCollection((LinearSeqLike) ((LinearSeqLike) repr));
    }

    public <B> Set<B> toSet() {
        return TraversableOnce.Cclass.toSet(this);
    }

    public String toString() {
        return SeqLike.Cclass.toString(this);
    }

    public <A1, B, That> That zip(scala.collection.Iterable<B> that, CanBuildFrom<List<A>, Tuple2<A1, B>, That> bf) {
        return IterableLike.Cclass.zip(this, that, bf);
    }

    public GenericCompanion<List> companion() {
        return List$.MODULE$;
    }

    public <B> List<B> $colon$colon(B x) {
        return new C$colon$colon(x, this);
    }

    public <B> List<B> $colon$colon$colon(List<B> prefix) {
        if (isEmpty()) {
            return prefix;
        }
        return ((ListBuffer) Growable.Cclass.$plus$plus$eq(new ListBuffer(), prefix)).prependToList(this);
    }

    public <B, That> That $plus$plus(TraversableOnce<B> that, CanBuildFrom<List<A>, B, That> bf) {
        if (bf.apply(this) instanceof ListBuffer) {
            return that.toList().$colon$colon$colon(this);
        }
        return TraversableLike.Cclass.$plus$plus(this, that, bf);
    }

    public List<A> toList() {
        return this;
    }

    public List<A> take(int n) {
        ListBuffer b = new ListBuffer();
        int i = 0;
        List these = this;
        while (!these.isEmpty() && i < n) {
            i++;
            b.$plus$eq(these.head());
            these = (List) these.tail();
        }
        if (these.isEmpty()) {
            return this;
        }
        return b.toList();
    }

    public List<A> drop(int n) {
        List these = this;
        int count = n;
        while (!these.isEmpty() && count > 0) {
            these = (List) these.tail();
            count--;
        }
        return these;
    }

    public List<A> slice(int start, int end) {
        int len = end;
        if (start > 0) {
            len = end - start;
        }
        return drop(start).take(len);
    }

    public List<A> reverse() {
        List result = Nil$.MODULE$;
        for (List these = this; !these.isEmpty(); these = (List) these.tail()) {
            result = result.$colon$colon(these.head());
        }
        return result;
    }

    public String stringPrefix() {
        return "List";
    }

    public Stream<A> toStream() {
        if (isEmpty()) {
            return Stream$Empty$.MODULE$;
        }
        return new Stream.Cons(head(), new List$$anonfun$toStream$1(this));
    }
}
