package scala.collection.immutable;

import scala.ScalaObject;
import scala.collection.immutable.Map;
import scala.collection.immutable.MapLike;

/* compiled from: MapLike.scala */
public interface MapLike<A, B, This extends MapLike<A, B, This> & Map<A, B>> extends scala.collection.MapLike<A, B, This>, ScalaObject {

    /* renamed from: scala.collection.immutable.MapLike$class  reason: invalid class name */
    /* compiled from: MapLike.scala */
    public abstract class Cclass {
        public static void $init$(MapLike $this) {
        }
    }
}
