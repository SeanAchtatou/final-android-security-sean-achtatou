package scala.collection.immutable;

import java.io.Serializable;
import scala.Function1;
import scala.collection.TraversableLike;
import scala.collection.immutable.Stream;
import scala.runtime.AbstractFunction0;
import scala.runtime.ObjectRef;

/* compiled from: Stream.scala */
public final class Stream$$anonfun$flatMap$1 extends AbstractFunction0 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Stream $outer;
    public final /* synthetic */ Function1 f$2;
    public final /* synthetic */ ObjectRef nonEmptyPrefix$1;

    public Stream$$anonfun$flatMap$1(Stream $outer2, Function1 function1, ObjectRef objectRef) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.f$2 = function1;
        this.nonEmptyPrefix$1 = objectRef;
    }

    /* Debug info: failed to restart local var, previous not found, register: 5 */
    public final Stream<B> apply() {
        Stream$Empty$ append;
        Stream stream = (Stream) ((Stream) this.nonEmptyPrefix$1.elem).tail();
        Function1 function1 = this.f$2;
        new Stream.StreamCanBuildFrom();
        if (stream.isEmpty()) {
            append = Stream$Empty$.MODULE$;
        } else {
            ObjectRef nonEmptyPrefix$10 = new ObjectRef(stream);
            Stream prefix0 = ((TraversableLike) function1.apply(((Stream) nonEmptyPrefix$10.elem).head())).toStream();
            while (!((Stream) nonEmptyPrefix$10.elem).isEmpty() && prefix0.isEmpty()) {
                nonEmptyPrefix$10.elem = (Stream) ((Stream) nonEmptyPrefix$10.elem).tail();
                if (!((Stream) nonEmptyPrefix$10.elem).isEmpty()) {
                    prefix0 = ((TraversableLike) function1.apply(((Stream) nonEmptyPrefix$10.elem).head())).toStream();
                }
            }
            append = ((Stream) nonEmptyPrefix$10.elem).isEmpty() ? Stream$Empty$.MODULE$ : prefix0.append(new Stream$$anonfun$flatMap$1(stream, function1, nonEmptyPrefix$10));
        }
        return append;
    }
}
