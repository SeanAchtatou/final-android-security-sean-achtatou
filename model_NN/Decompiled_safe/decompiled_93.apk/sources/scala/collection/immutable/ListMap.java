package scala.collection.immutable;

import java.util.NoSuchElementException;
import scala.Function0;
import scala.Function1;
import scala.Function2;
import scala.None$;
import scala.Option;
import scala.PartialFunction;
import scala.ScalaObject;
import scala.Some;
import scala.Tuple2;
import scala.collection.Iterable;
import scala.collection.IterableLike;
import scala.collection.Iterator;
import scala.collection.Map;
import scala.collection.MapLike;
import scala.collection.Traversable;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericTraversableTemplate;
import scala.collection.generic.Subtractable;
import scala.collection.immutable.Iterable;
import scala.collection.immutable.Map;
import scala.collection.immutable.MapLike;
import scala.collection.immutable.Traversable;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.Builder;
import scala.collection.mutable.StringBuilder;
import scala.math.Numeric;
import scala.reflect.ClassManifest;
import scala.runtime.BoxesRunTime;
import scala.runtime.Nothing$;

/* compiled from: ListMap.scala */
public class ListMap<A, B> implements Map<A, B>, MapLike<A, B, ListMap<A, B>>, ScalaObject, MapLike {
    public static final long serialVersionUID = 301002838095710379L;

    public ListMap() {
        TraversableOnce.Cclass.$init$(this);
        TraversableLike.Cclass.$init$(this);
        GenericTraversableTemplate.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        IterableLike.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Function1.Cclass.$init$(this);
        PartialFunction.Cclass.$init$(this);
        Subtractable.Cclass.$init$(this);
        MapLike.Cclass.$init$(this);
        Map.Cclass.$init$(this);
        MapLike.Cclass.$init$(this);
        Map.Cclass.$init$(this);
    }

    public <B> B $div$colon(B z, Function2<B, Tuple2<A, B>, B> op) {
        return TraversableOnce.Cclass.$div$colon(this, z, op);
    }

    public <B, That> That $plus$plus(TraversableOnce<B> that, CanBuildFrom<ListMap<A, B>, B, That> bf) {
        return TraversableLike.Cclass.$plus$plus(this, that, bf);
    }

    public StringBuilder addString(StringBuilder b, String start, String sep, String end) {
        return MapLike.Cclass.addString(this, b, start, sep, end);
    }

    public <C> PartialFunction<A, C> andThen(Function1<B, C> k) {
        return PartialFunction.Cclass.andThen(this, k);
    }

    public B apply(A key) {
        return MapLike.Cclass.apply(this, key);
    }

    public int apply$mcII$sp(int v1) {
        return Function1.Cclass.apply$mcII$sp(this, v1);
    }

    public void apply$mcVI$sp(int v1) {
        Function1.Cclass.apply$mcVI$sp(this, v1);
    }

    public void apply$mcVL$sp(long v1) {
        Function1.Cclass.apply$mcVL$sp(this, v1);
    }

    public boolean canEqual(Object that) {
        return IterableLike.Cclass.canEqual(this, that);
    }

    public GenericCompanion<Iterable> companion() {
        return Iterable.Cclass.companion(this);
    }

    public <A> Function1<A, B> compose(Function1<A, A> g) {
        return Function1.Cclass.compose(this, g);
    }

    public boolean contains(A key) {
        return MapLike.Cclass.contains(this, key);
    }

    public <B> void copyToArray(Object xs, int start) {
        TraversableOnce.Cclass.copyToArray(this, xs, start);
    }

    public <B> void copyToArray(Object xs, int start, int len) {
        IterableLike.Cclass.copyToArray(this, xs, start, len);
    }

    /* renamed from: default  reason: not valid java name */
    public B m5default(A key) {
        return MapLike.Cclass.m3default(this, key);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.ListMap<A, B>, java.lang.Object] */
    public ListMap<A, B> drop(int n) {
        return TraversableLike.Cclass.drop(this, n);
    }

    public boolean equals(Object that) {
        return MapLike.Cclass.equals(this, that);
    }

    public boolean exists(Function1<Tuple2<A, B>, Boolean> p) {
        return IterableLike.Cclass.exists(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.ListMap<A, B>, java.lang.Object] */
    public ListMap<A, B> filter(Function1<Tuple2<A, B>, Boolean> p) {
        return TraversableLike.Cclass.filter(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.Map, scala.collection.immutable.ListMap<A, B>] */
    public ListMap<A, B> filterNot(Function1<Tuple2<A, B>, Boolean> p) {
        return MapLike.Cclass.filterNot(this, p);
    }

    public <B> B foldLeft(B z, Function2<B, Tuple2<A, B>, B> op) {
        return TraversableOnce.Cclass.foldLeft(this, z, op);
    }

    public boolean forall(Function1<Tuple2<A, B>, Boolean> p) {
        return IterableLike.Cclass.forall(this, p);
    }

    public <U> void foreach(Function1<Tuple2<A, B>, U> f) {
        IterableLike.Cclass.foreach(this, f);
    }

    public <B> Builder<B, Iterable<B>> genericBuilder() {
        return GenericTraversableTemplate.Cclass.genericBuilder(this);
    }

    public <B1> B1 getOrElse(A key, Function0<B1> function0) {
        return MapLike.Cclass.getOrElse(this, key, function0);
    }

    public int hashCode() {
        return MapLike.Cclass.hashCode(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.Tuple2<A, B>] */
    public Tuple2<A, B> head() {
        return IterableLike.Cclass.head(this);
    }

    public boolean isDefinedAt(A key) {
        return MapLike.Cclass.isDefinedAt(this, key);
    }

    public boolean isEmpty() {
        return MapLike.Cclass.isEmpty(this);
    }

    public final boolean isTraversableAgain() {
        return TraversableLike.Cclass.isTraversableAgain(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.Tuple2<A, B>] */
    public Tuple2<A, B> last() {
        return TraversableLike.Cclass.last(this);
    }

    public <B, That> That map(Function1<Tuple2<A, B>, B> f, CanBuildFrom<ListMap<A, B>, B, That> bf) {
        return TraversableLike.Cclass.map(this, f, bf);
    }

    public String mkString() {
        return TraversableOnce.Cclass.mkString(this);
    }

    public String mkString(String sep) {
        return TraversableOnce.Cclass.mkString(this, sep);
    }

    public String mkString(String start, String sep, String end) {
        return TraversableOnce.Cclass.mkString(this, start, sep, end);
    }

    public Builder<Tuple2<A, B>, ListMap<A, B>> newBuilder() {
        return MapLike.Cclass.newBuilder(this);
    }

    public boolean nonEmpty() {
        return TraversableOnce.Cclass.nonEmpty(this);
    }

    public <B> B reduceLeft(Function2<B, Tuple2<A, B>, B> op) {
        return TraversableOnce.Cclass.reduceLeft(this, op);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.ListMap<A, B>, java.lang.Object] */
    public ListMap<A, B> repr() {
        return TraversableLike.Cclass.repr(this);
    }

    public <B> boolean sameElements(scala.collection.Iterable<B> that) {
        return IterableLike.Cclass.sameElements(this, that);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.ListMap<A, B>, java.lang.Object] */
    public ListMap<A, B> slice(int from, int until) {
        return IterableLike.Cclass.slice(this, from, until);
    }

    public String stringPrefix() {
        return MapLike.Cclass.stringPrefix(this);
    }

    public <B> B sum(Numeric<B> num) {
        return TraversableOnce.Cclass.sum(this, num);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.ListMap<A, B>, java.lang.Object] */
    public ListMap<A, B> tail() {
        return TraversableLike.Cclass.tail(this);
    }

    public scala.collection.Iterable<Tuple2<A, B>> thisCollection() {
        return IterableLike.Cclass.thisCollection(this);
    }

    public <B> Object toArray(ClassManifest<B> evidence$1) {
        return TraversableOnce.Cclass.toArray(this, evidence$1);
    }

    public <B> Buffer<B> toBuffer() {
        return TraversableOnce.Cclass.toBuffer(this);
    }

    public List<Tuple2<A, B>> toList() {
        return TraversableOnce.Cclass.toList(this);
    }

    public <B> Set<B> toSet() {
        return TraversableOnce.Cclass.toSet(this);
    }

    public Stream<Tuple2<A, B>> toStream() {
        return IterableLike.Cclass.toStream(this);
    }

    public String toString() {
        return MapLike.Cclass.toString(this);
    }

    public <A1, B, That> That zip(scala.collection.Iterable<B> that, CanBuildFrom<ListMap<A, B>, Tuple2<A1, B>, That> bf) {
        return IterableLike.Cclass.zip(this, that, bf);
    }

    public ListMap<A, Nothing$> empty() {
        return new ListMap<>();
    }

    public int size() {
        return 0;
    }

    public Option<B> get(A key) {
        return None$.MODULE$;
    }

    public <B1> ListMap<A, B1> updated(A key, B1 value) {
        return new Node(this, key, value);
    }

    public <B1> ListMap<A, B1> $plus(Tuple2<A, B1> kv) {
        return updated(kv._1(), kv._2());
    }

    public ListMap<A, B> $minus(Object key) {
        return this;
    }

    public Iterator<Tuple2<A, B>> iterator() {
        return TraversableOnce.Cclass.toList(new ListMap$$anon$1(this)).reverseIterator();
    }

    public A key() {
        throw new NoSuchElementException("empty map");
    }

    public B value() {
        throw new NoSuchElementException("empty map");
    }

    public ListMap<A, B> next() {
        throw new NoSuchElementException("empty map");
    }

    /* compiled from: ListMap.scala */
    public class Node<B1> extends ListMap<A, B1> implements ScalaObject, ScalaObject {
        public static final long serialVersionUID = -6453056603889598734L;
        public final /* synthetic */ ListMap $outer;
        private final A key;
        private final B1 value;

        public Node(ListMap<A, B> $outer2, A key2, B1 value2) {
            this.key = key2;
            this.value = value2;
            if ($outer2 == null) {
                throw new NullPointerException();
            }
            this.$outer = $outer2;
        }

        public A key() {
            return this.key;
        }

        public /* synthetic */ ListMap scala$collection$immutable$ListMap$Node$$$outer() {
            return this.$outer;
        }

        public B1 value() {
            return this.value;
        }

        public int size() {
            return next().size() + 1;
        }

        public boolean isEmpty() {
            return false;
        }

        public B1 apply(A k) {
            A key2 = key();
            return k == key2 ? true : k == null ? false : k instanceof Number ? BoxesRunTime.equalsNumObject((Number) k, key2) : k instanceof Character ? BoxesRunTime.equalsCharObject((Character) k, key2) : k.equals(key2) ? value() : next().apply(k);
        }

        public Option<B1> get(A k) {
            A key2 = key();
            return k == key2 ? true : k == null ? false : k instanceof Number ? BoxesRunTime.equalsNumObject((Number) k, key2) : k instanceof Character ? BoxesRunTime.equalsCharObject((Character) k, key2) : k.equals(key2) ? new Some(value()) : next().get(k);
        }

        public <B2> ListMap<A, B2> updated(A k, B2 v) {
            ListMap m;
            if (contains(k)) {
                m = $minus((Object) k);
            } else {
                m = this;
            }
            return new Node(m, k, v);
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public scala.collection.immutable.ListMap<A, B1> $minus(A r6) {
            /*
                r5 = this;
                java.lang.Object r3 = r5.key()
                if (r6 != r3) goto L_0x000e
                r2 = 1
            L_0x0007:
                if (r2 == 0) goto L_0x0031
                scala.collection.immutable.ListMap r2 = r5.next()
            L_0x000d:
                return r2
            L_0x000e:
                if (r6 != 0) goto L_0x0012
                r2 = 0
                goto L_0x0007
            L_0x0012:
                boolean r2 = r6 instanceof java.lang.Number
                if (r2 == 0) goto L_0x001f
                r0 = r6
                java.lang.Number r0 = (java.lang.Number) r0
                r2 = r0
                boolean r2 = scala.runtime.BoxesRunTime.equalsNumObject(r2, r3)
                goto L_0x0007
            L_0x001f:
                boolean r2 = r6 instanceof java.lang.Character
                if (r2 == 0) goto L_0x002c
                r0 = r6
                java.lang.Character r0 = (java.lang.Character) r0
                r2 = r0
                boolean r2 = scala.runtime.BoxesRunTime.equalsCharObject(r2, r3)
                goto L_0x0007
            L_0x002c:
                boolean r2 = r6.equals(r3)
                goto L_0x0007
            L_0x0031:
                scala.collection.immutable.ListMap r2 = r5.next()
                scala.collection.immutable.ListMap r1 = r2.$minus(r6)
                scala.collection.immutable.ListMap r2 = r5.next()
                if (r1 != r2) goto L_0x0041
                r2 = r5
                goto L_0x000d
            L_0x0041:
                scala.collection.immutable.ListMap$Node r2 = new scala.collection.immutable.ListMap$Node
                java.lang.Object r3 = r5.key()
                java.lang.Object r4 = r5.value()
                r2.<init>(r1, r3, r4)
                goto L_0x000d
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.ListMap.Node.$minus(java.lang.Object):scala.collection.immutable.ListMap");
        }

        public ListMap<A, B1> next() {
            return scala$collection$immutable$ListMap$Node$$$outer();
        }
    }
}
