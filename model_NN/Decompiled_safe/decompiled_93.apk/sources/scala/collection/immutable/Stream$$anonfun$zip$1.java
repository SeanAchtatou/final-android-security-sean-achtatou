package scala.collection.immutable;

import java.io.Serializable;
import scala.Tuple2;
import scala.collection.Iterable;
import scala.collection.immutable.Stream;
import scala.runtime.AbstractFunction0;

/* compiled from: Stream.scala */
public final class Stream$$anonfun$zip$1 extends AbstractFunction0 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Stream $outer;
    public final /* synthetic */ Iterable that$2;

    public Stream$$anonfun$zip$1(Stream $outer2, Stream<A> stream) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.that$2 = stream;
    }

    public final Stream<Tuple2<A1, B>> apply() {
        return (Stream) ((Stream) this.$outer.tail()).zip((Iterable) this.that$2.tail(), new Stream.StreamCanBuildFrom());
    }
}
