package scala.collection.immutable;

import scala.Array$$anonfun$apply$2;
import scala.Function1;
import scala.Function2;
import scala.Predef$;
import scala.ScalaObject;
import scala.Tuple2;
import scala.collection.BitSet;
import scala.collection.BitSetLike;
import scala.collection.BitSetLike$;
import scala.collection.Iterable;
import scala.collection.IterableLike;
import scala.collection.Iterator;
import scala.collection.Set;
import scala.collection.SetLike;
import scala.collection.Traversable;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.Addable;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericSetTemplate;
import scala.collection.generic.GenericTraversableTemplate;
import scala.collection.generic.Subtractable;
import scala.collection.immutable.Iterable;
import scala.collection.immutable.Set;
import scala.collection.immutable.Traversable;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.Builder;
import scala.collection.mutable.StringBuilder;
import scala.collection.mutable.WrappedArray;
import scala.math.Numeric;
import scala.reflect.ClassManifest;
import scala.runtime.BoxesRunTime;
import scala.runtime.IntRef;

/* compiled from: BitSet.scala */
public abstract class BitSet implements Set<Integer>, scala.collection.BitSet, BitSetLike<BitSet>, ScalaObject, Set {
    public static final long serialVersionUID = 1611436763290191562L;

    public abstract BitSet updateWord(int i, long j);

    public BitSet() {
        TraversableOnce.Cclass.$init$(this);
        TraversableLike.Cclass.$init$(this);
        GenericTraversableTemplate.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        IterableLike.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Function1.Cclass.$init$(this);
        GenericSetTemplate.Cclass.$init$(this);
        Addable.Cclass.$init$(this);
        Subtractable.Cclass.$init$(this);
        SetLike.Cclass.$init$(this);
        Set.Cclass.$init$(this);
        Set.Cclass.$init$(this);
        BitSetLike.Cclass.$init$(this);
        BitSet.Cclass.$init$(this);
    }

    public <B> B $div$colon(B z, Function2<B, Integer, B> op) {
        return TraversableOnce.Cclass.$div$colon(this, z, op);
    }

    public /* bridge */ /* synthetic */ Addable $plus(Object elem) {
        return $plus(BoxesRunTime.unboxToInt(elem));
    }

    public <B, That> That $plus$plus(TraversableOnce<B> that, CanBuildFrom<BitSet, B, That> bf) {
        return TraversableLike.Cclass.$plus$plus(this, that, bf);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.generic.Addable, scala.collection.immutable.BitSet] */
    public BitSet $plus$plus(TraversableOnce<Integer> xs) {
        return Addable.Cclass.$plus$plus(this, xs);
    }

    public StringBuilder addString(StringBuilder sb, String start, String sep, String end) {
        return BitSetLike.Cclass.addString(this, sb, start, sep, end);
    }

    public <A> Function1<Integer, A> andThen(Function1<Boolean, A> g) {
        return Function1.Cclass.andThen(this, g);
    }

    public boolean apply(Object elem) {
        return SetLike.Cclass.apply(this, elem);
    }

    public int apply$mcII$sp(int v1) {
        return Function1.Cclass.apply$mcII$sp(this, v1);
    }

    public void apply$mcVI$sp(int v1) {
        Function1.Cclass.apply$mcVI$sp(this, v1);
    }

    public void apply$mcVL$sp(long v1) {
        Function1.Cclass.apply$mcVL$sp(this, v1);
    }

    public boolean canEqual(Object that) {
        return IterableLike.Cclass.canEqual(this, that);
    }

    public GenericCompanion<Set> companion() {
        return Set.Cclass.companion(this);
    }

    public <A> Function1<A, Boolean> compose(Function1<A, Integer> g) {
        return Function1.Cclass.compose(this, g);
    }

    public boolean contains(int elem) {
        return BitSetLike.Cclass.contains(this, elem);
    }

    public /* bridge */ /* synthetic */ boolean contains(Object elem) {
        return contains(BoxesRunTime.unboxToInt(elem));
    }

    public <B> void copyToArray(Object xs, int start) {
        TraversableOnce.Cclass.copyToArray(this, xs, start);
    }

    public <B> void copyToArray(Object xs, int start, int len) {
        IterableLike.Cclass.copyToArray(this, xs, start, len);
    }

    public Object drop(int n) {
        return TraversableLike.Cclass.drop(this, n);
    }

    public boolean equals(Object that) {
        return SetLike.Cclass.equals(this, that);
    }

    public boolean exists(Function1<Integer, Boolean> p) {
        return IterableLike.Cclass.exists(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.BitSet] */
    public BitSet filter(Function1<Integer, Boolean> p) {
        return TraversableLike.Cclass.filter(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.BitSet] */
    public BitSet filterNot(Function1<Integer, Boolean> p) {
        return TraversableLike.Cclass.filterNot(this, p);
    }

    public <B> B foldLeft(B z, Function2<B, Integer, B> op) {
        return TraversableOnce.Cclass.foldLeft(this, z, op);
    }

    public boolean forall(Function1<Integer, Boolean> p) {
        return IterableLike.Cclass.forall(this, p);
    }

    public <B> void foreach(Function1<Integer, B> f) {
        BitSetLike.Cclass.foreach(this, f);
    }

    public <B> Builder<B, Set<B>> genericBuilder() {
        return GenericTraversableTemplate.Cclass.genericBuilder(this);
    }

    public int hashCode() {
        return SetLike.Cclass.hashCode(this);
    }

    public Object head() {
        return IterableLike.Cclass.head(this);
    }

    public boolean isEmpty() {
        return SetLike.Cclass.isEmpty(this);
    }

    public final boolean isTraversableAgain() {
        return TraversableLike.Cclass.isTraversableAgain(this);
    }

    public Iterator iterator() {
        return BitSetLike.Cclass.iterator(this);
    }

    public Object last() {
        return TraversableLike.Cclass.last(this);
    }

    public <B, That> That map(Function1<Integer, B> f, CanBuildFrom<BitSet, B, That> bf) {
        return TraversableLike.Cclass.map(this, f, bf);
    }

    public String mkString() {
        return TraversableOnce.Cclass.mkString(this);
    }

    public String mkString(String sep) {
        return TraversableOnce.Cclass.mkString(this, sep);
    }

    public String mkString(String start, String sep, String end) {
        return TraversableOnce.Cclass.mkString(this, start, sep, end);
    }

    public Builder<Integer, BitSet> newBuilder() {
        return SetLike.Cclass.newBuilder(this);
    }

    public boolean nonEmpty() {
        return TraversableOnce.Cclass.nonEmpty(this);
    }

    public <B> B reduceLeft(Function2<B, Integer, B> op) {
        return TraversableOnce.Cclass.reduceLeft(this, op);
    }

    public Object repr() {
        return TraversableLike.Cclass.repr(this);
    }

    public <B> boolean sameElements(scala.collection.Iterable<B> that) {
        return IterableLike.Cclass.sameElements(this, that);
    }

    public int size() {
        return BitSetLike.Cclass.size(this);
    }

    public Object slice(int from, int until) {
        return IterableLike.Cclass.slice(this, from, until);
    }

    public String stringPrefix() {
        return BitSetLike.Cclass.stringPrefix(this);
    }

    public boolean subsetOf(scala.collection.Set<Integer> that) {
        return SetLike.Cclass.subsetOf(this, that);
    }

    public <B> B sum(Numeric<B> num) {
        return TraversableOnce.Cclass.sum(this, num);
    }

    public Object tail() {
        return TraversableLike.Cclass.tail(this);
    }

    public scala.collection.Iterable<Integer> thisCollection() {
        return IterableLike.Cclass.thisCollection(this);
    }

    public <B> Object toArray(ClassManifest<B> evidence$1) {
        return TraversableOnce.Cclass.toArray(this, evidence$1);
    }

    public <B> Buffer<B> toBuffer() {
        return TraversableOnce.Cclass.toBuffer(this);
    }

    public List<Integer> toList() {
        return TraversableOnce.Cclass.toList(this);
    }

    public <B> Set<B> toSet() {
        return TraversableOnce.Cclass.toSet(this);
    }

    public Stream<Integer> toStream() {
        return IterableLike.Cclass.toStream(this);
    }

    public String toString() {
        return SetLike.Cclass.toString(this);
    }

    public <A1, B, That> That zip(scala.collection.Iterable<B> that, CanBuildFrom<BitSet, Tuple2<A1, B>, That> bf) {
        return IterableLike.Cclass.zip(this, that, bf);
    }

    public BitSet empty() {
        return BitSet$.MODULE$.empty();
    }

    public BitSet fromArray(long[] elems) {
        return BitSet$.MODULE$.fromArray(elems);
    }

    public BitSet $plus(int i) {
        if (!(i >= 0)) {
            throw new IllegalArgumentException(new StringBuilder().append((Object) "requirement failed: ").append((Object) "bitset element must be >= 0").toString());
        } else if (contains(i)) {
            return this;
        } else {
            int LogWL = i >> BitSetLike$.MODULE$.LogWL();
            return updateWord(LogWL, word(LogWL) | (1 << i));
        }
    }

    /* compiled from: BitSet.scala */
    public static class BitSet1 extends BitSet implements ScalaObject {
        private final long elems;

        public BitSet1(long elems2) {
            this.elems = elems2;
        }

        public long elems() {
            return this.elems;
        }

        public int nwords() {
            return 1;
        }

        public long word(int idx) {
            if (idx == 0) {
                return elems();
            }
            return 0;
        }

        public BitSet updateWord(int idx, long w) {
            if (idx == 0) {
                return new BitSet1(w);
            }
            if (idx == 1) {
                return new BitSet2(elems(), w);
            }
            BitSetLike$ bitSetLike$ = BitSetLike$.MODULE$;
            long elems2 = elems();
            WrappedArray<Long> wrapLongArray = Predef$.MODULE$.wrapLongArray(new long[0]);
            long[] array$70 = new long[(wrapLongArray.length() + 1)];
            array$70[0] = elems2;
            wrapLongArray.iterator().foreach(new Array$$anonfun$apply$2(array$70, new IntRef(1)));
            return fromArray(bitSetLike$.updateArray(array$70, idx, w));
        }
    }

    /* compiled from: BitSet.scala */
    public static class BitSet2 extends BitSet implements ScalaObject {
        private final long elems0;
        private final long elems1;

        public BitSet2(long elems02, long elems12) {
            this.elems0 = elems02;
            this.elems1 = elems12;
        }

        public long elems0() {
            return this.elems0;
        }

        public int nwords() {
            return 2;
        }

        public long word(int idx) {
            if (idx == 0) {
                return elems0();
            }
            if (idx == 1) {
                return this.elems1;
            }
            return 0;
        }

        public BitSet updateWord(int idx, long w) {
            if (idx == 0) {
                return new BitSet2(w, this.elems1);
            }
            if (idx == 1) {
                return new BitSet2(elems0(), w);
            }
            BitSetLike$ bitSetLike$ = BitSetLike$.MODULE$;
            long elems02 = elems0();
            WrappedArray<Long> wrapLongArray = Predef$.MODULE$.wrapLongArray(new long[]{this.elems1});
            long[] array$70 = new long[(wrapLongArray.length() + 1)];
            array$70[0] = elems02;
            wrapLongArray.iterator().foreach(new Array$$anonfun$apply$2(array$70, new IntRef(1)));
            return fromArray(bitSetLike$.updateArray(array$70, idx, w));
        }
    }

    /* compiled from: BitSet.scala */
    public static class BitSetN extends BitSet implements ScalaObject {
        private final long[] elems;

        public BitSetN(long[] elems2) {
            this.elems = elems2;
        }

        public long[] elems() {
            return this.elems;
        }

        public int nwords() {
            return elems().length;
        }

        public long word(int idx) {
            if (idx < nwords()) {
                return elems()[idx];
            }
            return 0;
        }

        public BitSet updateWord(int idx, long w) {
            return fromArray(BitSetLike$.MODULE$.updateArray(elems(), idx, w));
        }
    }
}
