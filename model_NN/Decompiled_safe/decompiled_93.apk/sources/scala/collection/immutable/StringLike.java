package scala.collection.immutable;

import java.util.regex.PatternSyntaxException;
import scala.Predef$;
import scala.ScalaObject;
import scala.collection.IndexedSeqOptimized;
import scala.collection.Seq;
import scala.collection.Seq$;
import scala.collection.TraversableOnce;
import scala.collection.generic.TraversableFactory;
import scala.collection.mutable.StringBuilder;
import scala.math.Ordered;
import scala.math.ScalaNumber;
import scala.reflect.Manifest$;
import scala.runtime.BoxesRunTime;
import scala.util.matching.Regex;

/* compiled from: StringLike.scala */
public interface StringLike<Repr> extends IndexedSeqOptimized<Character, Repr>, Ordered<String>, ScalaObject {
    char apply(int i);

    int compare(String str);

    String format(Seq<Object> seq);

    Regex r();

    int toInt();

    /* renamed from: scala.collection.immutable.StringLike$class  reason: invalid class name */
    /* compiled from: StringLike.scala */
    public abstract class Cclass {
        public static void $init$(StringLike $this) {
        }

        public static char apply(StringLike $this, int n) {
            return $this.toString().charAt(n);
        }

        public static int length(StringLike $this) {
            return $this.toString().length();
        }

        public static String mkString(StringLike $this) {
            return $this.toString();
        }

        public static int compare(StringLike $this, String other) {
            return $this.toString().compareTo(other);
        }

        public static String stripSuffix(StringLike $this, String suffix) {
            if ($this.toString().endsWith(suffix)) {
                return $this.toString().substring(0, $this.toString().length() - suffix.length());
            }
            return $this.toString();
        }

        public static final String scala$collection$immutable$StringLike$$escape(StringLike $this, char ch) {
            return new StringBuilder().append((Object) "\\Q").append(BoxesRunTime.boxToCharacter(ch)).append((Object) "\\E").toString();
        }

        public static String[] split(StringLike $this, char separator) throws PatternSyntaxException {
            return $this.toString().split(scala$collection$immutable$StringLike$$escape($this, separator));
        }

        public static Regex r(StringLike $this) {
            return new Regex($this.toString(), Predef$.MODULE$.wrapRefArray((Object[]) new String[0]));
        }

        public static int toInt(StringLike $this) {
            return Integer.parseInt($this.toString());
        }

        public static final Object scala$collection$immutable$StringLike$$unwrapArg(StringLike $this, Object arg) {
            if (arg instanceof ScalaNumber) {
                return ((ScalaNumber) arg).underlying();
            }
            return arg;
        }

        public static String format(StringLike $this, Seq args) {
            return String.format($this.toString(), (Object[]) ((TraversableOnce) args.map(new StringLike$$anonfun$format$1($this), new TraversableFactory.GenericCanBuildFrom(Seq$.MODULE$))).toArray(Manifest$.MODULE$.Object()));
        }
    }
}
