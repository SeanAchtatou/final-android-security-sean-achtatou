package scala.collection.immutable;

import scala.ScalaObject;
import scala.collection.generic.ImmutableSetFactory;

/* compiled from: HashSet.scala */
public final class HashSet$ extends ImmutableSetFactory<HashSet> implements ScalaObject, ScalaObject {
    public static final HashSet$ MODULE$ = null;

    static {
        new HashSet$();
    }

    private HashSet$() {
        MODULE$ = this;
    }

    public <A> HashSet<A> empty() {
        return HashSet$EmptyHashSet$.MODULE$;
    }
}
