package scala.collection.immutable;

import scala.Function1;
import scala.Function2;
import scala.ScalaObject;
import scala.Tuple2;
import scala.collection.Iterable;
import scala.collection.IterableLike;
import scala.collection.Iterator;
import scala.collection.Iterator$;
import scala.collection.Set;
import scala.collection.SetLike;
import scala.collection.Traversable;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.Addable;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericSetTemplate;
import scala.collection.generic.GenericTraversableTemplate;
import scala.collection.generic.Subtractable;
import scala.collection.immutable.Iterable;
import scala.collection.immutable.Set;
import scala.collection.immutable.Traversable;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.Builder;
import scala.collection.mutable.StringBuilder;
import scala.math.Numeric;
import scala.reflect.ClassManifest;

/* compiled from: Set.scala */
public final class Set$EmptySet$ implements Set<Object>, ScalaObject, Set {
    public static final Set$EmptySet$ MODULE$ = null;

    static {
        new Set$EmptySet$();
    }

    public Set$EmptySet$() {
        MODULE$ = this;
        TraversableOnce.Cclass.$init$(this);
        TraversableLike.Cclass.$init$(this);
        GenericTraversableTemplate.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        IterableLike.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Function1.Cclass.$init$(this);
        GenericSetTemplate.Cclass.$init$(this);
        Addable.Cclass.$init$(this);
        Subtractable.Cclass.$init$(this);
        SetLike.Cclass.$init$(this);
        Set.Cclass.$init$(this);
        Set.Cclass.$init$(this);
    }

    public <B> B $div$colon(B z, Function2<B, Object, B> op) {
        return TraversableOnce.Cclass.$div$colon(this, z, op);
    }

    public <B, That> That $plus$plus(TraversableOnce<B> that, CanBuildFrom<Set<Object>, B, That> bf) {
        return TraversableLike.Cclass.$plus$plus(this, that, bf);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.generic.Addable, scala.collection.immutable.Set<java.lang.Object>] */
    public Set<Object> $plus$plus(TraversableOnce<Object> xs) {
        return Addable.Cclass.$plus$plus(this, xs);
    }

    public StringBuilder addString(StringBuilder b, String start, String sep, String end) {
        return TraversableOnce.Cclass.addString(this, b, start, sep, end);
    }

    public <A> Function1<Object, A> andThen(Function1<Boolean, A> g) {
        return Function1.Cclass.andThen(this, g);
    }

    public boolean apply(Object elem) {
        return SetLike.Cclass.apply(this, elem);
    }

    public int apply$mcII$sp(int v1) {
        return Function1.Cclass.apply$mcII$sp(this, v1);
    }

    public void apply$mcVI$sp(int v1) {
        Function1.Cclass.apply$mcVI$sp(this, v1);
    }

    public void apply$mcVL$sp(long v1) {
        Function1.Cclass.apply$mcVL$sp(this, v1);
    }

    public boolean canEqual(Object that) {
        return IterableLike.Cclass.canEqual(this, that);
    }

    public GenericCompanion<Set> companion() {
        return Set.Cclass.companion(this);
    }

    public <A> Function1<A, Boolean> compose(Function1<A, Object> g) {
        return Function1.Cclass.compose(this, g);
    }

    public <B> void copyToArray(Object xs, int start) {
        TraversableOnce.Cclass.copyToArray(this, xs, start);
    }

    public <B> void copyToArray(Object xs, int start, int len) {
        IterableLike.Cclass.copyToArray(this, xs, start, len);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.Set<java.lang.Object>] */
    public Set<Object> drop(int n) {
        return TraversableLike.Cclass.drop(this, n);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.Set, scala.collection.immutable.Set<java.lang.Object>] */
    public Set<Object> empty() {
        return GenericSetTemplate.Cclass.empty(this);
    }

    public boolean equals(Object that) {
        return SetLike.Cclass.equals(this, that);
    }

    public boolean exists(Function1<Object, Boolean> p) {
        return IterableLike.Cclass.exists(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.Set<java.lang.Object>] */
    public Set<Object> filter(Function1<Object, Boolean> p) {
        return TraversableLike.Cclass.filter(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.Set<java.lang.Object>] */
    public Set<Object> filterNot(Function1<Object, Boolean> p) {
        return TraversableLike.Cclass.filterNot(this, p);
    }

    public <B> B foldLeft(B z, Function2<B, Object, B> op) {
        return TraversableOnce.Cclass.foldLeft(this, z, op);
    }

    public boolean forall(Function1<Object, Boolean> p) {
        return IterableLike.Cclass.forall(this, p);
    }

    public <B> Builder<B, Set<B>> genericBuilder() {
        return GenericTraversableTemplate.Cclass.genericBuilder(this);
    }

    public int hashCode() {
        return SetLike.Cclass.hashCode(this);
    }

    public Object head() {
        return IterableLike.Cclass.head(this);
    }

    public boolean isEmpty() {
        return SetLike.Cclass.isEmpty(this);
    }

    public final boolean isTraversableAgain() {
        return TraversableLike.Cclass.isTraversableAgain(this);
    }

    public Object last() {
        return TraversableLike.Cclass.last(this);
    }

    public <B, That> That map(Function1<Object, B> f, CanBuildFrom<Set<Object>, B, That> bf) {
        return TraversableLike.Cclass.map(this, f, bf);
    }

    public String mkString() {
        return TraversableOnce.Cclass.mkString(this);
    }

    public String mkString(String sep) {
        return TraversableOnce.Cclass.mkString(this, sep);
    }

    public String mkString(String start, String sep, String end) {
        return TraversableOnce.Cclass.mkString(this, start, sep, end);
    }

    public Builder<Object, Set<Object>> newBuilder() {
        return SetLike.Cclass.newBuilder(this);
    }

    public boolean nonEmpty() {
        return TraversableOnce.Cclass.nonEmpty(this);
    }

    public <B> B reduceLeft(Function2<B, Object, B> op) {
        return TraversableOnce.Cclass.reduceLeft(this, op);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.Set<java.lang.Object>] */
    public Set<Object> repr() {
        return TraversableLike.Cclass.repr(this);
    }

    public <B> boolean sameElements(scala.collection.Iterable<B> that) {
        return IterableLike.Cclass.sameElements(this, that);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.Set<java.lang.Object>] */
    public Set<Object> slice(int from, int until) {
        return IterableLike.Cclass.slice(this, from, until);
    }

    public String stringPrefix() {
        return SetLike.Cclass.stringPrefix(this);
    }

    public boolean subsetOf(scala.collection.Set<Object> that) {
        return SetLike.Cclass.subsetOf(this, that);
    }

    public <B> B sum(Numeric<B> num) {
        return TraversableOnce.Cclass.sum(this, num);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.Set<java.lang.Object>] */
    public Set<Object> tail() {
        return TraversableLike.Cclass.tail(this);
    }

    public scala.collection.Iterable<Object> thisCollection() {
        return IterableLike.Cclass.thisCollection(this);
    }

    public <B> Object toArray(ClassManifest<B> evidence$1) {
        return TraversableOnce.Cclass.toArray(this, evidence$1);
    }

    public <B> Buffer<B> toBuffer() {
        return TraversableOnce.Cclass.toBuffer(this);
    }

    public List<Object> toList() {
        return TraversableOnce.Cclass.toList(this);
    }

    public <B> Set<B> toSet() {
        return TraversableOnce.Cclass.toSet(this);
    }

    public Stream<Object> toStream() {
        return IterableLike.Cclass.toStream(this);
    }

    public String toString() {
        return SetLike.Cclass.toString(this);
    }

    public <A1, B, That> That zip(scala.collection.Iterable<B> that, CanBuildFrom<Set<Object>, Tuple2<A1, B>, That> bf) {
        return IterableLike.Cclass.zip(this, that, bf);
    }

    public int size() {
        return 0;
    }

    public boolean contains(Object elem) {
        return false;
    }

    public Set<Object> $plus(Object elem) {
        return new Set.Set1(elem);
    }

    public Iterator<Object> iterator() {
        return Iterator$.MODULE$.empty();
    }

    public <U> void foreach(Function1<Object, U> f) {
    }
}
