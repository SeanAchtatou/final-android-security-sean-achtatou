package scala.collection.immutable;

import java.io.Serializable;
import scala.collection.TraversableOnce;
import scala.collection.immutable.Stream;
import scala.runtime.AbstractFunction1;

/* compiled from: Stream.scala */
public final class Stream$StreamBuilder$$anonfun$result$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;

    public Stream$StreamBuilder$$anonfun$result$1(Stream.StreamBuilder<A> $outer) {
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return apply((TraversableOnce) ((TraversableOnce) v1));
    }

    public final Stream<A> apply(TraversableOnce<A> traversableOnce) {
        return traversableOnce.toStream();
    }
}
