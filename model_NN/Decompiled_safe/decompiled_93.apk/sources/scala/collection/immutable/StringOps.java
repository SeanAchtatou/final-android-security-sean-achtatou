package scala.collection.immutable;

import scala.Function1;
import scala.Function2;
import scala.ScalaObject;
import scala.Tuple2;
import scala.collection.IndexedSeqLike;
import scala.collection.IndexedSeqOptimized;
import scala.collection.Iterable;
import scala.collection.IterableLike;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.SeqLike;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.CanBuildFrom;
import scala.collection.immutable.StringLike;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.StringBuilder;
import scala.math.Numeric;
import scala.math.Ordered;
import scala.math.Ordering;
import scala.reflect.ClassManifest;
import scala.runtime.RichInt;
import scala.util.matching.Regex;

/* compiled from: StringOps.scala */
public final class StringOps implements StringLike<String>, ScalaObject {
    public final String repr;

    public StringOps(String repr2) {
        this.repr = repr2;
        TraversableOnce.Cclass.$init$(this);
        TraversableLike.Cclass.$init$(this);
        IterableLike.Cclass.$init$(this);
        SeqLike.Cclass.$init$(this);
        IndexedSeqLike.Cclass.$init$(this);
        IndexedSeqOptimized.Cclass.$init$(this);
        Ordered.Cclass.$init$(this);
        StringLike.Cclass.$init$(this);
    }

    public <B> B $div$colon(B z, Function2<B, Character, B> op) {
        return TraversableOnce.Cclass.$div$colon(this, z, op);
    }

    public boolean $less(Object that) {
        return Ordered.Cclass.$less(this, that);
    }

    public <B, That> That $plus$plus(TraversableOnce<B> that, CanBuildFrom<String, B, That> bf) {
        return TraversableLike.Cclass.$plus$plus(this, that, bf);
    }

    public StringBuilder addString(StringBuilder b, String start, String sep, String end) {
        return TraversableOnce.Cclass.addString(this, b, start, sep, end);
    }

    public char apply(int n) {
        return StringLike.Cclass.apply(this, n);
    }

    public boolean canEqual(Object that) {
        return IterableLike.Cclass.canEqual(this, that);
    }

    public /* bridge */ /* synthetic */ int compare(Object that) {
        return compare((String) that);
    }

    public int compare(String other) {
        return StringLike.Cclass.compare(this, other);
    }

    public int compareTo(Object that) {
        return Ordered.Cclass.compareTo(this, that);
    }

    public boolean contains(Object elem) {
        return SeqLike.Cclass.contains(this, elem);
    }

    public <B> void copyToArray(Object xs, int start) {
        TraversableOnce.Cclass.copyToArray(this, xs, start);
    }

    public <B> void copyToArray(Object xs, int start, int len) {
        IndexedSeqOptimized.Cclass.copyToArray(this, xs, start, len);
    }

    public Object drop(int n) {
        return IndexedSeqOptimized.Cclass.drop(this, n);
    }

    public boolean equals(Object that) {
        return SeqLike.Cclass.equals(this, that);
    }

    public boolean exists(Function1<Character, Boolean> p) {
        return IndexedSeqOptimized.Cclass.exists(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, java.lang.String] */
    public String filter(Function1<Character, Boolean> p) {
        return TraversableLike.Cclass.filter(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, java.lang.String] */
    public String filterNot(Function1<Character, Boolean> p) {
        return TraversableLike.Cclass.filterNot(this, p);
    }

    public <B> B foldLeft(B z, Function2<B, Character, B> op) {
        return IndexedSeqOptimized.Cclass.foldLeft(this, z, op);
    }

    public boolean forall(Function1<Character, Boolean> p) {
        return IndexedSeqOptimized.Cclass.forall(this, p);
    }

    public <U> void foreach(Function1<Character, U> f) {
        IndexedSeqOptimized.Cclass.foreach(this, f);
    }

    public String format(Seq<Object> args) {
        return StringLike.Cclass.format(this, args);
    }

    public int hashCode() {
        return SeqLike.Cclass.hashCode(this);
    }

    public Object head() {
        return IndexedSeqOptimized.Cclass.head(this);
    }

    public <B> int indexOf(B elem) {
        return SeqLike.Cclass.indexOf(this, elem);
    }

    public <B> int indexOf(B elem, int from) {
        return SeqLike.Cclass.indexOf(this, elem, from);
    }

    public int indexWhere(Function1<Character, Boolean> p, int from) {
        return IndexedSeqOptimized.Cclass.indexWhere(this, p, from);
    }

    public boolean isDefinedAt(int idx) {
        return SeqLike.Cclass.isDefinedAt(this, idx);
    }

    public boolean isEmpty() {
        return IndexedSeqOptimized.Cclass.isEmpty(this);
    }

    public final boolean isTraversableAgain() {
        return TraversableLike.Cclass.isTraversableAgain(this);
    }

    public Iterator<Character> iterator() {
        return IndexedSeqLike.Cclass.iterator(this);
    }

    public Object last() {
        return IndexedSeqOptimized.Cclass.last(this);
    }

    public int length() {
        return StringLike.Cclass.length(this);
    }

    public <B, That> That map(Function1<Character, B> f, CanBuildFrom<String, B, That> bf) {
        return TraversableLike.Cclass.map(this, f, bf);
    }

    public String mkString() {
        return StringLike.Cclass.mkString(this);
    }

    public String mkString(String sep) {
        return TraversableOnce.Cclass.mkString(this, sep);
    }

    public String mkString(String start, String sep, String end) {
        return TraversableOnce.Cclass.mkString(this, start, sep, end);
    }

    public boolean nonEmpty() {
        return TraversableOnce.Cclass.nonEmpty(this);
    }

    public int prefixLength(Function1<Character, Boolean> p) {
        return SeqLike.Cclass.prefixLength(this, p);
    }

    public Regex r() {
        return StringLike.Cclass.r(this);
    }

    public <B> B reduceLeft(Function2<B, Character, B> op) {
        return IndexedSeqOptimized.Cclass.reduceLeft(this, op);
    }

    public String repr() {
        return this.repr;
    }

    public Object reverse() {
        return IndexedSeqOptimized.Cclass.reverse(this);
    }

    public Iterator<Character> reverseIterator() {
        return IndexedSeqOptimized.Cclass.reverseIterator(this);
    }

    public <B> boolean sameElements(Iterable<B> that) {
        return IndexedSeqOptimized.Cclass.sameElements(this, that);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$head() {
        return IterableLike.Cclass.head(this);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$last() {
        return TraversableLike.Cclass.last(this);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$reduceLeft(Function2 op) {
        return TraversableOnce.Cclass.reduceLeft(this, op);
    }

    public final boolean scala$collection$IndexedSeqOptimized$$super$sameElements(Iterable that) {
        return IterableLike.Cclass.sameElements(this, that);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$tail() {
        return TraversableLike.Cclass.tail(this);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$zip(Iterable that, CanBuildFrom bf) {
        return IterableLike.Cclass.zip(this, that, bf);
    }

    public int segmentLength(Function1<Character, Boolean> p, int from) {
        return IndexedSeqOptimized.Cclass.segmentLength(this, p, from);
    }

    public int size() {
        return SeqLike.Cclass.size(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, java.lang.String] */
    public <B> String sortBy(Function1<Character, B> f, Ordering<B> ord) {
        return SeqLike.Cclass.sortBy(this, f, ord);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, java.lang.String] */
    public <B> String sorted(Ordering<B> ord) {
        return SeqLike.Cclass.sorted(this, ord);
    }

    public String stringPrefix() {
        return TraversableLike.Cclass.stringPrefix(this);
    }

    public <B> B sum(Numeric<B> num) {
        return TraversableOnce.Cclass.sum(this, num);
    }

    public Object tail() {
        return IndexedSeqOptimized.Cclass.tail(this);
    }

    public <B> Object toArray(ClassManifest<B> evidence$1) {
        return TraversableOnce.Cclass.toArray(this, evidence$1);
    }

    public <B> Buffer<B> toBuffer() {
        return TraversableOnce.Cclass.toBuffer(this);
    }

    public int toInt() {
        return StringLike.Cclass.toInt(this);
    }

    public List<Character> toList() {
        return TraversableOnce.Cclass.toList(this);
    }

    public <B> Set<B> toSet() {
        return TraversableOnce.Cclass.toSet(this);
    }

    public Stream<Character> toStream() {
        return IterableLike.Cclass.toStream(this);
    }

    public <A1, B, That> That zip(Iterable<B> that, CanBuildFrom<String, Tuple2<A1, B>, That> bf) {
        return IndexedSeqOptimized.Cclass.zip(this, that, bf);
    }

    public WrappedString thisCollection() {
        return new WrappedString(this.repr);
    }

    public WrappedString toCollection(String repr2) {
        return new WrappedString(repr2);
    }

    public StringBuilder newBuilder() {
        return new StringBuilder();
    }

    public String slice(int i, int i2) {
        int max = new RichInt(i).max(0);
        int min = new RichInt(i2).min(this.repr.length());
        if (max >= min) {
            return "";
        }
        return this.repr.substring(max, min);
    }

    public String toString() {
        return this.repr;
    }
}
