package scala.collection.immutable;

import java.io.Serializable;
import scala.Function1;
import scala.collection.immutable.Stream;
import scala.runtime.AbstractFunction0;

/* compiled from: Stream.scala */
public final class Stream$$anonfun$map$1 extends AbstractFunction0 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Stream $outer;
    public final /* synthetic */ Function1 f$1;

    public Stream$$anonfun$map$1(Stream $outer2, Stream<A> stream) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.f$1 = stream;
    }

    /* Debug info: failed to restart local var, previous not found, register: 5 */
    public final Stream<B> apply() {
        Stream stream = (Stream) this.$outer.tail();
        Function1 function1 = this.f$1;
        new Stream.StreamCanBuildFrom();
        return (Stream) (stream.isEmpty() ? Stream$Empty$.MODULE$ : new Stream.Cons(function1.apply(stream.head()), new Stream$$anonfun$map$1(stream, function1)));
    }
}
