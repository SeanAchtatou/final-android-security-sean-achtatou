package scala.collection.immutable;

import scala.ScalaObject;
import scala.collection.Seq;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.SeqFactory;
import scala.collection.generic.TraversableFactory;
import scala.collection.mutable.Builder;
import scala.collection.mutable.ListBuffer;

/* compiled from: List.scala */
public final class List$ extends SeqFactory<List> implements ScalaObject {
    public static final List$ MODULE$ = null;

    static {
        new List$();
    }

    private List$() {
        MODULE$ = this;
    }

    public <A> CanBuildFrom<List<?>, A, List<A>> canBuildFrom() {
        return new TraversableFactory.GenericCanBuildFrom(this);
    }

    public <A> Builder<A, List<A>> newBuilder() {
        return new ListBuffer();
    }

    public <A> List<A> empty() {
        return Nil$.MODULE$;
    }

    public <A> List<A> apply(Seq<A> xs) {
        return xs.toList();
    }
}
