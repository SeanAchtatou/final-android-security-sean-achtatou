package scala.collection.immutable;

import scala.ScalaObject;
import scala.collection.SeqLike;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericTraversableTemplate;

/* compiled from: Seq.scala */
public interface Seq<A> extends Iterable<A>, scala.collection.Seq<A>, GenericTraversableTemplate<A, Seq>, SeqLike<A, Seq<A>>, ScalaObject {

    /* renamed from: scala.collection.immutable.Seq$class  reason: invalid class name */
    /* compiled from: Seq.scala */
    public abstract class Cclass {
        public static void $init$(Seq $this) {
        }

        public static GenericCompanion companion(Seq $this) {
            return Seq$.MODULE$;
        }
    }
}
