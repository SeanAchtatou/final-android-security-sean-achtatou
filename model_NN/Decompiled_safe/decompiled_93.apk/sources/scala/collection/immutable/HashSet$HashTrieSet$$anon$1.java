package scala.collection.immutable;

import scala.Function1;
import scala.Function2;
import scala.collection.Iterator;
import scala.collection.TraversableOnce;
import scala.collection.immutable.HashSet;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.StringBuilder;
import scala.math.Numeric;
import scala.reflect.ClassManifest;

/* compiled from: HashSet.scala */
public final class HashSet$HashTrieSet$$anon$1 implements Iterator<A> {
    private HashSet<A>[] arrayD;
    private HashSet<A>[][] arrayStack = new HashSet[6][];
    private int depth = 0;
    private int posD;
    private int[] posStack = new int[6];
    private Iterator<A> subIter;

    public HashSet$HashTrieSet$$anon$1(HashSet.HashTrieSet<A> hashTrieSet) {
        TraversableOnce.Cclass.$init$(this);
        Iterator.Cclass.$init$(this);
        this.arrayD = hashTrieSet.scala$collection$immutable$HashSet$HashTrieSet$$elems();
        this.posD = 0;
        this.subIter = null;
    }

    public <B> B $div$colon(B z, Function2<B, A, B> op) {
        return TraversableOnce.Cclass.$div$colon(this, z, op);
    }

    public StringBuilder addString(StringBuilder b, String start, String sep, String end) {
        return TraversableOnce.Cclass.addString(this, b, start, sep, end);
    }

    public <B> void copyToArray(Object xs, int start) {
        TraversableOnce.Cclass.copyToArray(this, xs, start);
    }

    public <B> void copyToArray(Object xs, int start, int len) {
        Iterator.Cclass.copyToArray(this, xs, start, len);
    }

    public Iterator<A> drop(int n) {
        return Iterator.Cclass.drop(this, n);
    }

    public boolean exists(Function1<A, Boolean> p) {
        return Iterator.Cclass.exists(this, p);
    }

    public <B> B foldLeft(B z, Function2<B, A, B> op) {
        return TraversableOnce.Cclass.foldLeft(this, z, op);
    }

    public boolean forall(Function1<A, Boolean> p) {
        return Iterator.Cclass.forall(this, p);
    }

    public <U> void foreach(Function1<A, U> f) {
        Iterator.Cclass.foreach(this, f);
    }

    public boolean isEmpty() {
        return Iterator.Cclass.isEmpty(this);
    }

    public boolean isTraversableAgain() {
        return Iterator.Cclass.isTraversableAgain(this);
    }

    public int length() {
        return Iterator.Cclass.length(this);
    }

    public <B> Iterator<B> map(Function1<A, B> f) {
        return Iterator.Cclass.map(this, f);
    }

    public String mkString() {
        return TraversableOnce.Cclass.mkString(this);
    }

    public String mkString(String sep) {
        return TraversableOnce.Cclass.mkString(this, sep);
    }

    public String mkString(String start, String sep, String end) {
        return TraversableOnce.Cclass.mkString(this, start, sep, end);
    }

    public boolean nonEmpty() {
        return TraversableOnce.Cclass.nonEmpty(this);
    }

    public <B> B reduceLeft(Function2<B, A, B> op) {
        return TraversableOnce.Cclass.reduceLeft(this, op);
    }

    public int size() {
        return TraversableOnce.Cclass.size(this);
    }

    public <B> B sum(Numeric<B> num) {
        return TraversableOnce.Cclass.sum(this, num);
    }

    public <B> Object toArray(ClassManifest<B> evidence$1) {
        return TraversableOnce.Cclass.toArray(this, evidence$1);
    }

    public <B> Buffer<B> toBuffer() {
        return TraversableOnce.Cclass.toBuffer(this);
    }

    public List<A> toList() {
        return TraversableOnce.Cclass.toList(this);
    }

    public <B> Set<B> toSet() {
        return TraversableOnce.Cclass.toSet(this);
    }

    public Stream<A> toStream() {
        return Iterator.Cclass.toStream(this);
    }

    public String toString() {
        return Iterator.Cclass.toString(this);
    }

    public boolean hasNext() {
        return this.subIter != null || this.depth >= 0;
    }

    public A next() {
        if (this.subIter == null) {
            return next0(this.arrayD, this.posD);
        }
        Object el = this.subIter.next();
        if (!this.subIter.hasNext()) {
            this.subIter = null;
        }
        return el;
    }

    private A next0(HashSet<A>[] elems, int i) {
        HashSet<A> hashSet;
        while (true) {
            if (i == elems.length - 1) {
                this.depth--;
                if (this.depth >= 0) {
                    this.arrayD = this.arrayStack[this.depth];
                    this.posD = this.posStack[this.depth];
                    this.arrayStack[this.depth] = null;
                } else {
                    this.arrayD = null;
                    this.posD = 0;
                }
            } else {
                this.posD++;
            }
            hashSet = elems[i];
            if (!(hashSet instanceof HashSet.HashTrieSet)) {
                break;
            }
            HashSet.HashTrieSet hashTrieSet = (HashSet.HashTrieSet) hashSet;
            if (this.depth >= 0) {
                this.arrayStack[this.depth] = this.arrayD;
                this.posStack[this.depth] = this.posD;
            }
            this.depth++;
            this.arrayD = hashTrieSet.scala$collection$immutable$HashSet$HashTrieSet$$elems;
            this.posD = 0;
            elems = hashTrieSet.scala$collection$immutable$HashSet$HashTrieSet$$elems;
            i = 0;
        }
        if (hashSet instanceof HashSet.HashSet1) {
            return ((HashSet.HashSet1) hashSet).key();
        }
        this.subIter = hashSet.iterator();
        return this.subIter.next();
    }
}
