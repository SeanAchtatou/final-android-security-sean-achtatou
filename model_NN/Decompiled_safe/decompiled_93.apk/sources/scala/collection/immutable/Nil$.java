package scala.collection.immutable;

import java.util.NoSuchElementException;
import scala.Product;
import scala.ScalaObject;
import scala.runtime.BoxesRunTime;
import scala.runtime.Nothing$;

/* compiled from: List.scala */
public final class Nil$ extends List<Nothing$> implements ScalaObject, Product, ScalaObject {
    public static final Nil$ MODULE$ = null;
    public static final long serialVersionUID = -8256821097970055419L;

    static {
        new Nil$();
    }

    private Nil$() {
        MODULE$ = this;
    }

    public int productArity() {
        return 0;
    }

    public Object productElement(int i) {
        throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(i).toString());
    }

    public String productPrefix() {
        return "Nil";
    }

    public boolean isEmpty() {
        return true;
    }

    public Nothing$ head() {
        throw new NoSuchElementException("head of empty list");
    }

    public List<Nothing$> tail() {
        throw new UnsupportedOperationException("tail of empty list");
    }

    public boolean equals(Object that) {
        if (that instanceof Seq) {
            return ((Seq) that).isEmpty();
        }
        return false;
    }
}
