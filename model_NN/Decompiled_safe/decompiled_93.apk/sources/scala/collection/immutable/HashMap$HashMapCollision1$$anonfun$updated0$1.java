package scala.collection.immutable;

import java.io.Serializable;
import scala.MatchError;
import scala.Tuple2;
import scala.collection.immutable.HashMap;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxedUnit;
import scala.runtime.ObjectRef;

/* compiled from: HashMap.scala */
public final class HashMap$HashMapCollision1$$anonfun$updated0$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ HashMap.HashMapCollision1 $outer;
    public final /* synthetic */ int level$1;
    public final /* synthetic */ ObjectRef m$1;

    public HashMap$HashMapCollision1$$anonfun$updated0$1(HashMap.HashMapCollision1 $outer2, int i, ObjectRef objectRef) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.level$1 = i;
        this.m$1 = objectRef;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        apply((Tuple2) ((Tuple2) v1));
        return BoxedUnit.UNIT;
    }

    public final void apply(Tuple2<A, B> tuple2) {
        if (tuple2 == null) {
            throw new MatchError(tuple2);
        }
        this.m$1.elem = ((HashMap) this.m$1.elem).updated0(tuple2._1(), this.$outer.hash(), this.level$1, tuple2._2(), null);
    }
}
