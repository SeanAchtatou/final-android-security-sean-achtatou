package scala.collection.immutable;

import scala.Function0;
import scala.Function1;
import scala.Function2;
import scala.None$;
import scala.Option;
import scala.PartialFunction;
import scala.ScalaObject;
import scala.Tuple2;
import scala.collection.Iterable;
import scala.collection.IterableLike;
import scala.collection.Iterator;
import scala.collection.Iterator$;
import scala.collection.Map;
import scala.collection.MapLike;
import scala.collection.Traversable;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericTraversableTemplate;
import scala.collection.generic.Subtractable;
import scala.collection.immutable.Iterable;
import scala.collection.immutable.Map;
import scala.collection.immutable.MapLike;
import scala.collection.immutable.Traversable;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.Builder;
import scala.collection.mutable.StringBuilder;
import scala.math.Numeric;
import scala.reflect.ClassManifest;
import scala.runtime.Nothing$;

/* compiled from: Map.scala */
public final class Map$EmptyMap$ implements Map<Object, Nothing$>, ScalaObject, Map {
    public static final Map$EmptyMap$ MODULE$ = null;

    static {
        new Map$EmptyMap$();
    }

    public Map$EmptyMap$() {
        MODULE$ = this;
        TraversableOnce.Cclass.$init$(this);
        TraversableLike.Cclass.$init$(this);
        GenericTraversableTemplate.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        IterableLike.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Function1.Cclass.$init$(this);
        PartialFunction.Cclass.$init$(this);
        Subtractable.Cclass.$init$(this);
        MapLike.Cclass.$init$(this);
        Map.Cclass.$init$(this);
        MapLike.Cclass.$init$(this);
        Map.Cclass.$init$(this);
    }

    public <B> B $div$colon(B z, Function2<B, Tuple2<Object, Nothing$>, B> op) {
        return TraversableOnce.Cclass.$div$colon(this, z, op);
    }

    public <B, That> That $plus$plus(TraversableOnce<B> that, CanBuildFrom<Map<Object, Nothing$>, B, That> bf) {
        return TraversableLike.Cclass.$plus$plus(this, that, bf);
    }

    public StringBuilder addString(StringBuilder b, String start, String sep, String end) {
        return MapLike.Cclass.addString(this, b, start, sep, end);
    }

    public <C> PartialFunction<Object, C> andThen(Function1<Nothing$, C> k) {
        return PartialFunction.Cclass.andThen(this, k);
    }

    public Object apply(Object key) {
        return MapLike.Cclass.apply(this, key);
    }

    public int apply$mcII$sp(int v1) {
        return Function1.Cclass.apply$mcII$sp(this, v1);
    }

    public void apply$mcVI$sp(int v1) {
        Function1.Cclass.apply$mcVI$sp(this, v1);
    }

    public void apply$mcVL$sp(long v1) {
        Function1.Cclass.apply$mcVL$sp(this, v1);
    }

    public boolean canEqual(Object that) {
        return IterableLike.Cclass.canEqual(this, that);
    }

    public GenericCompanion<Iterable> companion() {
        return Iterable.Cclass.companion(this);
    }

    public <A> Function1<A, Nothing$> compose(Function1<A, Object> g) {
        return Function1.Cclass.compose(this, g);
    }

    public boolean contains(Object key) {
        return MapLike.Cclass.contains(this, key);
    }

    public <B> void copyToArray(Object xs, int start) {
        TraversableOnce.Cclass.copyToArray(this, xs, start);
    }

    public <B> void copyToArray(Object xs, int start, int len) {
        IterableLike.Cclass.copyToArray(this, xs, start, len);
    }

    /* renamed from: default  reason: not valid java name */
    public Object m6default(Object key) {
        return MapLike.Cclass.m3default(this, key);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.Map<java.lang.Object, scala.runtime.Nothing$>] */
    public Map<Object, Nothing$> drop(int n) {
        return TraversableLike.Cclass.drop(this, n);
    }

    public Map<Object, Nothing$> empty() {
        return Map.Cclass.empty(this);
    }

    public boolean equals(Object that) {
        return MapLike.Cclass.equals(this, that);
    }

    public boolean exists(Function1<Tuple2<Object, Nothing$>, Boolean> p) {
        return IterableLike.Cclass.exists(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.Map<java.lang.Object, scala.runtime.Nothing$>] */
    public Map<Object, Nothing$> filter(Function1<Tuple2<Object, Nothing$>, Boolean> p) {
        return TraversableLike.Cclass.filter(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.Map, scala.collection.immutable.Map<java.lang.Object, scala.runtime.Nothing$>] */
    public Map<Object, Nothing$> filterNot(Function1<Tuple2<Object, Nothing$>, Boolean> p) {
        return MapLike.Cclass.filterNot(this, p);
    }

    public <B> B foldLeft(B z, Function2<B, Tuple2<Object, Nothing$>, B> op) {
        return TraversableOnce.Cclass.foldLeft(this, z, op);
    }

    public boolean forall(Function1<Tuple2<Object, Nothing$>, Boolean> p) {
        return IterableLike.Cclass.forall(this, p);
    }

    public <U> void foreach(Function1<Tuple2<Object, Nothing$>, U> f) {
        IterableLike.Cclass.foreach(this, f);
    }

    public <B> Builder<B, Iterable<B>> genericBuilder() {
        return GenericTraversableTemplate.Cclass.genericBuilder(this);
    }

    public <B1> B1 getOrElse(Object key, Function0<B1> function0) {
        return MapLike.Cclass.getOrElse(this, key, function0);
    }

    public int hashCode() {
        return MapLike.Cclass.hashCode(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.Tuple2<java.lang.Object, scala.runtime.Nothing$>, java.lang.Object] */
    public Tuple2<Object, Nothing$> head() {
        return IterableLike.Cclass.head(this);
    }

    public boolean isDefinedAt(Object key) {
        return MapLike.Cclass.isDefinedAt(this, key);
    }

    public boolean isEmpty() {
        return MapLike.Cclass.isEmpty(this);
    }

    public final boolean isTraversableAgain() {
        return TraversableLike.Cclass.isTraversableAgain(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.Tuple2<java.lang.Object, scala.runtime.Nothing$>, java.lang.Object] */
    public Tuple2<Object, Nothing$> last() {
        return TraversableLike.Cclass.last(this);
    }

    public <B, That> That map(Function1<Tuple2<Object, Nothing$>, B> f, CanBuildFrom<Map<Object, Nothing$>, B, That> bf) {
        return TraversableLike.Cclass.map(this, f, bf);
    }

    public String mkString() {
        return TraversableOnce.Cclass.mkString(this);
    }

    public String mkString(String sep) {
        return TraversableOnce.Cclass.mkString(this, sep);
    }

    public String mkString(String start, String sep, String end) {
        return TraversableOnce.Cclass.mkString(this, start, sep, end);
    }

    public Builder<Tuple2<Object, Nothing$>, Map<Object, Nothing$>> newBuilder() {
        return MapLike.Cclass.newBuilder(this);
    }

    public boolean nonEmpty() {
        return TraversableOnce.Cclass.nonEmpty(this);
    }

    public <B> B reduceLeft(Function2<B, Tuple2<Object, Nothing$>, B> op) {
        return TraversableOnce.Cclass.reduceLeft(this, op);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.Map<java.lang.Object, scala.runtime.Nothing$>] */
    public Map<Object, Nothing$> repr() {
        return TraversableLike.Cclass.repr(this);
    }

    public <B> boolean sameElements(scala.collection.Iterable<B> that) {
        return IterableLike.Cclass.sameElements(this, that);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.Map<java.lang.Object, scala.runtime.Nothing$>] */
    public Map<Object, Nothing$> slice(int from, int until) {
        return IterableLike.Cclass.slice(this, from, until);
    }

    public String stringPrefix() {
        return MapLike.Cclass.stringPrefix(this);
    }

    public <B> B sum(Numeric<B> num) {
        return TraversableOnce.Cclass.sum(this, num);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.Map<java.lang.Object, scala.runtime.Nothing$>] */
    public Map<Object, Nothing$> tail() {
        return TraversableLike.Cclass.tail(this);
    }

    public scala.collection.Iterable<Tuple2<Object, Nothing$>> thisCollection() {
        return IterableLike.Cclass.thisCollection(this);
    }

    public <B> Object toArray(ClassManifest<B> evidence$1) {
        return TraversableOnce.Cclass.toArray(this, evidence$1);
    }

    public <B> Buffer<B> toBuffer() {
        return TraversableOnce.Cclass.toBuffer(this);
    }

    public List<Tuple2<Object, Nothing$>> toList() {
        return TraversableOnce.Cclass.toList(this);
    }

    public <B> Set<B> toSet() {
        return TraversableOnce.Cclass.toSet(this);
    }

    public Stream<Tuple2<Object, Nothing$>> toStream() {
        return IterableLike.Cclass.toStream(this);
    }

    public String toString() {
        return MapLike.Cclass.toString(this);
    }

    public <A1, B, That> That zip(scala.collection.Iterable<B> that, CanBuildFrom<Map<Object, Nothing$>, Tuple2<A1, B>, That> bf) {
        return IterableLike.Cclass.zip(this, that, bf);
    }

    public int size() {
        return 0;
    }

    public Option<Nothing$> get(Object key) {
        return None$.MODULE$;
    }

    public Iterator<Tuple2<Object, Nothing$>> iterator() {
        return Iterator$.MODULE$.empty();
    }

    public <B1> Map<Object, B1> $plus(Tuple2<Object, B1> kv) {
        return new Map.Map1(kv._1(), kv._2());
    }

    public Map<Object, Nothing$> $minus(Object key) {
        return this;
    }
}
