package scala.collection.immutable;

import scala.ScalaObject;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.SeqFactory;
import scala.collection.generic.TraversableFactory;
import scala.collection.mutable.Builder;

/* compiled from: IndexedSeq.scala */
public final class IndexedSeq$ extends SeqFactory<IndexedSeq> implements ScalaObject {
    public static final IndexedSeq$ MODULE$ = null;

    static {
        new IndexedSeq$();
    }

    private IndexedSeq$() {
        MODULE$ = this;
    }

    public <A> CanBuildFrom<IndexedSeq<?>, A, IndexedSeq<A>> canBuildFrom() {
        return new TraversableFactory.GenericCanBuildFrom(this);
    }

    public <A> Builder<A, IndexedSeq<A>> newBuilder() {
        return new VectorBuilder();
    }
}
