package scala.collection.immutable;

import scala.Product;
import scala.ScalaObject;
import scala.runtime.BoxesRunTime;

/* renamed from: scala.collection.immutable.$colon$colon  reason: invalid class name */
/* compiled from: List.scala */
public final class C$colon$colon<B> extends List<B> implements ScalaObject, Product, ScalaObject {
    public static final long serialVersionUID = -8476791151983527571L;
    public Object scala$collection$immutable$$colon$colon$$hd;
    public List<B> tl;

    public C$colon$colon(B hd, List<B> tl2) {
        this.scala$collection$immutable$$colon$colon$$hd = hd;
        this.tl = tl2;
    }

    public int productArity() {
        return 2;
    }

    public Object productElement(int i) {
        switch (i) {
            case 0:
                return this.scala$collection$immutable$$colon$colon$$hd;
            case 1:
                return this.tl;
            default:
                throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(i).toString());
        }
    }

    public String productPrefix() {
        return "::";
    }

    public B head() {
        return this.scala$collection$immutable$$colon$colon$$hd;
    }

    public List<B> tail() {
        return this.tl;
    }

    public boolean isEmpty() {
        return false;
    }
}
