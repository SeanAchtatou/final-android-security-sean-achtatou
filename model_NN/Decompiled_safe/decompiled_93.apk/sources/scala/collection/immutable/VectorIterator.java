package scala.collection.immutable;

import java.util.NoSuchElementException;
import scala.Function1;
import scala.Function2;
import scala.ScalaObject;
import scala.collection.Iterator;
import scala.collection.TraversableOnce;
import scala.collection.immutable.VectorPointer;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.StringBuilder;
import scala.math.Numeric;
import scala.math.package$;
import scala.reflect.ClassManifest;

/* compiled from: Vector.scala */
public final class VectorIterator<A> implements Iterator<A>, VectorPointer<A>, ScalaObject {
    private boolean _hasNext;
    private int blockIndex;
    private int depth;
    private Object[] display0;
    private Object[] display1;
    private Object[] display2;
    private Object[] display3;
    private Object[] display4;
    private Object[] display5;
    private int endIndex;
    private int endLo = package$.MODULE$.min(endIndex() - blockIndex(), 32);
    private int lo;

    public VectorIterator(int i, int i2) {
        TraversableOnce.Cclass.$init$(this);
        Iterator.Cclass.$init$(this);
        VectorPointer.Cclass.$init$(this);
        boolean z = true ^ true;
        this.blockIndex = i & -32;
        this.lo = i & 31;
        this.endIndex = i2;
        this._hasNext = blockIndex() + lo() < endIndex();
    }

    public <B> B $div$colon(B z, Function2<B, A, B> op) {
        return TraversableOnce.Cclass.$div$colon(this, z, op);
    }

    public StringBuilder addString(StringBuilder b, String start, String sep, String end) {
        return TraversableOnce.Cclass.addString(this, b, start, sep, end);
    }

    public final Object[] copyOf(Object[] a) {
        return VectorPointer.Cclass.copyOf(this, a);
    }

    public <B> void copyToArray(Object xs, int start) {
        TraversableOnce.Cclass.copyToArray(this, xs, start);
    }

    public <B> void copyToArray(Object xs, int start, int len) {
        Iterator.Cclass.copyToArray(this, xs, start, len);
    }

    public int depth() {
        return this.depth;
    }

    public void depth_$eq(int i) {
        this.depth = i;
    }

    public Object[] display0() {
        return this.display0;
    }

    public void display0_$eq(Object[] objArr) {
        this.display0 = objArr;
    }

    public Object[] display1() {
        return this.display1;
    }

    public void display1_$eq(Object[] objArr) {
        this.display1 = objArr;
    }

    public Object[] display2() {
        return this.display2;
    }

    public void display2_$eq(Object[] objArr) {
        this.display2 = objArr;
    }

    public Object[] display3() {
        return this.display3;
    }

    public void display3_$eq(Object[] objArr) {
        this.display3 = objArr;
    }

    public Object[] display4() {
        return this.display4;
    }

    public void display4_$eq(Object[] objArr) {
        this.display4 = objArr;
    }

    public Object[] display5() {
        return this.display5;
    }

    public void display5_$eq(Object[] objArr) {
        this.display5 = objArr;
    }

    public Iterator<A> drop(int n) {
        return Iterator.Cclass.drop(this, n);
    }

    public boolean exists(Function1<A, Boolean> p) {
        return Iterator.Cclass.exists(this, p);
    }

    public <B> B foldLeft(B z, Function2<B, A, B> op) {
        return TraversableOnce.Cclass.foldLeft(this, z, op);
    }

    public boolean forall(Function1<A, Boolean> p) {
        return Iterator.Cclass.forall(this, p);
    }

    public <U> void foreach(Function1<A, U> f) {
        Iterator.Cclass.foreach(this, f);
    }

    public final A getElem(int index, int xor) {
        return VectorPointer.Cclass.getElem(this, index, xor);
    }

    public final void gotoNextBlockStart(int index, int xor) {
        VectorPointer.Cclass.gotoNextBlockStart(this, index, xor);
    }

    public final void gotoNextBlockStartWritable(int index, int xor) {
        VectorPointer.Cclass.gotoNextBlockStartWritable(this, index, xor);
    }

    public final void gotoPos(int index, int xor) {
        VectorPointer.Cclass.gotoPos(this, index, xor);
    }

    public final void gotoPosWritable0(int newIndex, int xor) {
        VectorPointer.Cclass.gotoPosWritable0(this, newIndex, xor);
    }

    public final void gotoPosWritable1(int oldIndex, int newIndex, int xor) {
        VectorPointer.Cclass.gotoPosWritable1(this, oldIndex, newIndex, xor);
    }

    public final <U> void initFrom(VectorPointer<U> that) {
        VectorPointer.Cclass.initFrom(this, that);
    }

    public final <U> void initFrom(VectorPointer<U> that, int depth2) {
        VectorPointer.Cclass.initFrom(this, that, depth2);
    }

    public boolean isEmpty() {
        return Iterator.Cclass.isEmpty(this);
    }

    public boolean isTraversableAgain() {
        return Iterator.Cclass.isTraversableAgain(this);
    }

    public int length() {
        return Iterator.Cclass.length(this);
    }

    public <B> Iterator<B> map(Function1<A, B> f) {
        return Iterator.Cclass.map(this, f);
    }

    public String mkString() {
        return TraversableOnce.Cclass.mkString(this);
    }

    public String mkString(String sep) {
        return TraversableOnce.Cclass.mkString(this, sep);
    }

    public String mkString(String start, String sep, String end) {
        return TraversableOnce.Cclass.mkString(this, start, sep, end);
    }

    public boolean nonEmpty() {
        return TraversableOnce.Cclass.nonEmpty(this);
    }

    public final Object[] nullSlotAndCopy(Object[] array, int index) {
        return VectorPointer.Cclass.nullSlotAndCopy(this, array, index);
    }

    public <B> B reduceLeft(Function2<B, A, B> op) {
        return TraversableOnce.Cclass.reduceLeft(this, op);
    }

    public int size() {
        return TraversableOnce.Cclass.size(this);
    }

    public final void stabilize(int index) {
        VectorPointer.Cclass.stabilize(this, index);
    }

    public <B> B sum(Numeric<B> num) {
        return TraversableOnce.Cclass.sum(this, num);
    }

    public <B> Object toArray(ClassManifest<B> evidence$1) {
        return TraversableOnce.Cclass.toArray(this, evidence$1);
    }

    public <B> Buffer<B> toBuffer() {
        return TraversableOnce.Cclass.toBuffer(this);
    }

    public List<A> toList() {
        return TraversableOnce.Cclass.toList(this);
    }

    public <B> Set<B> toSet() {
        return TraversableOnce.Cclass.toSet(this);
    }

    public Stream<A> toStream() {
        return Iterator.Cclass.toStream(this);
    }

    public String toString() {
        return Iterator.Cclass.toString(this);
    }

    private int blockIndex() {
        return this.blockIndex;
    }

    private void blockIndex_$eq(int i) {
        this.blockIndex = i;
    }

    private int lo() {
        return this.lo;
    }

    private void lo_$eq(int i) {
        this.lo = i;
    }

    private int endIndex() {
        return this.endIndex;
    }

    private int endLo() {
        return this.endLo;
    }

    private void endLo_$eq(int i) {
        this.endLo = i;
    }

    public boolean hasNext() {
        return _hasNext();
    }

    private boolean _hasNext() {
        return this._hasNext;
    }

    private void _hasNext_$eq(boolean z) {
        this._hasNext = z;
    }

    public A next() {
        if (_hasNext()) {
            A a = this.display0[lo()];
            lo_$eq(lo() + 1);
            if (lo() == endLo()) {
                if (blockIndex() + lo() < endIndex()) {
                    int blockIndex2 = blockIndex() + 32;
                    gotoNextBlockStart(blockIndex2, blockIndex() ^ blockIndex2);
                    blockIndex_$eq(blockIndex2);
                    endLo_$eq(package$.MODULE$.min(endIndex() - blockIndex(), 32));
                    lo_$eq(0);
                } else {
                    _hasNext_$eq(false);
                }
            }
            return a;
        }
        throw new NoSuchElementException("reached iterator end");
    }
}
