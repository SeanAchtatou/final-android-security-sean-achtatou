package scala.collection.immutable;

import scala.ScalaObject;
import scala.collection.generic.ImmutableSetFactory;

/* compiled from: ListSet.scala */
public final class ListSet$ extends ImmutableSetFactory<ListSet> implements ScalaObject, ScalaObject {
    public static final ListSet$ MODULE$ = null;

    static {
        new ListSet$();
    }

    private ListSet$() {
        MODULE$ = this;
    }

    public <A> ListSet<A> empty() {
        return new ListSet<>();
    }
}
