package scala.collection.immutable;

import java.io.Serializable;
import scala.Function0;
import scala.runtime.AbstractFunction0;

/* compiled from: Stream.scala */
public final class Stream$$anonfun$append$1 extends AbstractFunction0 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Stream $outer;
    public final /* synthetic */ Function0 rest$1;

    /* JADX WARN: Type inference failed for: r3v0, types: [scala.collection.immutable.Stream<A>, scala.Function0] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Stream$$anonfun$append$1(scala.collection.immutable.Stream r2, scala.collection.immutable.Stream<A> r3) {
        /*
            r1 = this;
            if (r2 != 0) goto L_0x0008
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            r0.<init>()
            throw r0
        L_0x0008:
            r1.$outer = r2
            r1.rest$1 = r3
            r1.<init>()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.Stream$$anonfun$append$1.<init>(scala.collection.immutable.Stream, scala.Function0):void");
    }

    public final Stream<B> apply() {
        return ((Stream) this.$outer.tail()).append(this.rest$1);
    }
}
