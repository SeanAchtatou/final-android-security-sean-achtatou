package scala.collection.immutable;

import scala.ScalaObject;
import scala.collection.generic.ImmutableMapFactory;

/* compiled from: Map.scala */
public final class Map$ extends ImmutableMapFactory<Map> implements ScalaObject {
    public static final Map$ MODULE$ = null;

    static {
        new Map$();
    }

    private Map$() {
        MODULE$ = this;
    }

    public <A, B> Map<A, B> empty() {
        return Map$EmptyMap$.MODULE$;
    }
}
