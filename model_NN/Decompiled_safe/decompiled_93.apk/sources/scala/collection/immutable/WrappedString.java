package scala.collection.immutable;

import scala.Function1;
import scala.Function2;
import scala.PartialFunction;
import scala.Proxy;
import scala.ScalaObject;
import scala.Tuple2;
import scala.collection.IndexedSeq;
import scala.collection.IndexedSeqLike;
import scala.collection.IndexedSeqOptimized;
import scala.collection.Iterable;
import scala.collection.IterableLike;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.SeqLike;
import scala.collection.Traversable;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericTraversableTemplate;
import scala.collection.immutable.IndexedSeq;
import scala.collection.immutable.Iterable;
import scala.collection.immutable.Seq;
import scala.collection.immutable.StringLike;
import scala.collection.immutable.Traversable;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.Builder;
import scala.collection.mutable.StringBuilder;
import scala.math.Numeric;
import scala.math.Ordered;
import scala.math.Ordering;
import scala.reflect.ClassManifest;
import scala.runtime.BoxesRunTime;
import scala.runtime.RichInt;
import scala.util.matching.Regex;

/* compiled from: WrappedString.scala */
public class WrappedString implements IndexedSeq<Character>, StringLike<WrappedString>, Proxy, ScalaObject {
    private final String self;

    public WrappedString(String self2) {
        this.self = self2;
        TraversableOnce.Cclass.$init$(this);
        TraversableLike.Cclass.$init$(this);
        GenericTraversableTemplate.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        IterableLike.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Function1.Cclass.$init$(this);
        PartialFunction.Cclass.$init$(this);
        SeqLike.Cclass.$init$(this);
        Seq.Cclass.$init$(this);
        Seq.Cclass.$init$(this);
        IndexedSeqLike.Cclass.$init$(this);
        IndexedSeq.Cclass.$init$(this);
        IndexedSeq.Cclass.$init$(this);
        IndexedSeqOptimized.Cclass.$init$(this);
        Ordered.Cclass.$init$(this);
        StringLike.Cclass.$init$(this);
        Proxy.Cclass.$init$(this);
    }

    public <B> B $div$colon(B z, Function2<B, Character, B> op) {
        return TraversableOnce.Cclass.$div$colon(this, z, op);
    }

    public boolean $less(Object that) {
        return Ordered.Cclass.$less(this, that);
    }

    public <B, That> That $plus$plus(TraversableOnce<B> that, CanBuildFrom<WrappedString, B, That> bf) {
        return TraversableLike.Cclass.$plus$plus(this, that, bf);
    }

    public StringBuilder addString(StringBuilder b, String start, String sep, String end) {
        return TraversableOnce.Cclass.addString(this, b, start, sep, end);
    }

    public <C> PartialFunction<Integer, C> andThen(Function1<Character, C> k) {
        return PartialFunction.Cclass.andThen(this, k);
    }

    public char apply(int n) {
        return StringLike.Cclass.apply(this, n);
    }

    public /* bridge */ /* synthetic */ Object apply(Object v1) {
        return BoxesRunTime.boxToCharacter(apply(BoxesRunTime.unboxToInt(v1)));
    }

    public int apply$mcII$sp(int v1) {
        return Function1.Cclass.apply$mcII$sp(this, v1);
    }

    public void apply$mcVI$sp(int v1) {
        Function1.Cclass.apply$mcVI$sp(this, v1);
    }

    public void apply$mcVL$sp(long v1) {
        Function1.Cclass.apply$mcVL$sp(this, v1);
    }

    public boolean canEqual(Object that) {
        return IterableLike.Cclass.canEqual(this, that);
    }

    public GenericCompanion<IndexedSeq> companion() {
        return IndexedSeq.Cclass.companion(this);
    }

    public /* bridge */ /* synthetic */ int compare(Object that) {
        return compare((String) that);
    }

    public int compare(String other) {
        return StringLike.Cclass.compare(this, other);
    }

    public int compareTo(Object that) {
        return Ordered.Cclass.compareTo(this, that);
    }

    public <A> Function1<A, Character> compose(Function1<A, Integer> g) {
        return Function1.Cclass.compose(this, g);
    }

    public boolean contains(Object elem) {
        return SeqLike.Cclass.contains(this, elem);
    }

    public <B> void copyToArray(Object xs, int start) {
        TraversableOnce.Cclass.copyToArray(this, xs, start);
    }

    public <B> void copyToArray(Object xs, int start, int len) {
        IndexedSeqOptimized.Cclass.copyToArray(this, xs, start, len);
    }

    public Object drop(int n) {
        return IndexedSeqOptimized.Cclass.drop(this, n);
    }

    public boolean equals(Object that) {
        return Proxy.Cclass.equals(this, that);
    }

    public boolean exists(Function1<Character, Boolean> p) {
        return IndexedSeqOptimized.Cclass.exists(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.WrappedString, java.lang.Object] */
    public WrappedString filter(Function1<Character, Boolean> p) {
        return TraversableLike.Cclass.filter(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.WrappedString, java.lang.Object] */
    public WrappedString filterNot(Function1<Character, Boolean> p) {
        return TraversableLike.Cclass.filterNot(this, p);
    }

    public <B> B foldLeft(B z, Function2<B, Character, B> op) {
        return IndexedSeqOptimized.Cclass.foldLeft(this, z, op);
    }

    public boolean forall(Function1<Character, Boolean> p) {
        return IndexedSeqOptimized.Cclass.forall(this, p);
    }

    public <U> void foreach(Function1<Character, U> f) {
        IndexedSeqOptimized.Cclass.foreach(this, f);
    }

    public String format(scala.collection.Seq<Object> args) {
        return StringLike.Cclass.format(this, args);
    }

    public <B> Builder<B, IndexedSeq<B>> genericBuilder() {
        return GenericTraversableTemplate.Cclass.genericBuilder(this);
    }

    public int hashCode() {
        return Proxy.Cclass.hashCode(this);
    }

    public Object head() {
        return IndexedSeqOptimized.Cclass.head(this);
    }

    public <B> int indexOf(B elem) {
        return SeqLike.Cclass.indexOf(this, elem);
    }

    public <B> int indexOf(B elem, int from) {
        return SeqLike.Cclass.indexOf(this, elem, from);
    }

    public int indexWhere(Function1<Character, Boolean> p, int from) {
        return IndexedSeqOptimized.Cclass.indexWhere(this, p, from);
    }

    public boolean isDefinedAt(int idx) {
        return SeqLike.Cclass.isDefinedAt(this, idx);
    }

    public /* bridge */ /* synthetic */ boolean isDefinedAt(Object x) {
        return isDefinedAt(BoxesRunTime.unboxToInt(x));
    }

    public boolean isEmpty() {
        return IndexedSeqOptimized.Cclass.isEmpty(this);
    }

    public final boolean isTraversableAgain() {
        return TraversableLike.Cclass.isTraversableAgain(this);
    }

    public Iterator<Character> iterator() {
        return IndexedSeqLike.Cclass.iterator(this);
    }

    public Object last() {
        return IndexedSeqOptimized.Cclass.last(this);
    }

    public int length() {
        return StringLike.Cclass.length(this);
    }

    public <B, That> That map(Function1<Character, B> f, CanBuildFrom<WrappedString, B, That> bf) {
        return TraversableLike.Cclass.map(this, f, bf);
    }

    public String mkString() {
        return StringLike.Cclass.mkString(this);
    }

    public String mkString(String sep) {
        return TraversableOnce.Cclass.mkString(this, sep);
    }

    public String mkString(String start, String sep, String end) {
        return TraversableOnce.Cclass.mkString(this, start, sep, end);
    }

    public boolean nonEmpty() {
        return TraversableOnce.Cclass.nonEmpty(this);
    }

    public int prefixLength(Function1<Character, Boolean> p) {
        return SeqLike.Cclass.prefixLength(this, p);
    }

    public Regex r() {
        return StringLike.Cclass.r(this);
    }

    public <B> B reduceLeft(Function2<B, Character, B> op) {
        return IndexedSeqOptimized.Cclass.reduceLeft(this, op);
    }

    public Object repr() {
        return TraversableLike.Cclass.repr(this);
    }

    public Object reverse() {
        return IndexedSeqOptimized.Cclass.reverse(this);
    }

    public Iterator<Character> reverseIterator() {
        return IndexedSeqOptimized.Cclass.reverseIterator(this);
    }

    public <B> boolean sameElements(scala.collection.Iterable<B> that) {
        return IndexedSeqOptimized.Cclass.sameElements(this, that);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$head() {
        return IterableLike.Cclass.head(this);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$last() {
        return TraversableLike.Cclass.last(this);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$reduceLeft(Function2 op) {
        return TraversableOnce.Cclass.reduceLeft(this, op);
    }

    public final boolean scala$collection$IndexedSeqOptimized$$super$sameElements(scala.collection.Iterable that) {
        return IterableLike.Cclass.sameElements(this, that);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$tail() {
        return TraversableLike.Cclass.tail(this);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$zip(scala.collection.Iterable that, CanBuildFrom bf) {
        return IterableLike.Cclass.zip(this, that, bf);
    }

    public int segmentLength(Function1<Character, Boolean> p, int from) {
        return IndexedSeqOptimized.Cclass.segmentLength(this, p, from);
    }

    public String self() {
        return this.self;
    }

    public int size() {
        return SeqLike.Cclass.size(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.WrappedString, java.lang.Object] */
    public <B> WrappedString sortBy(Function1<Character, B> f, Ordering<B> ord) {
        return SeqLike.Cclass.sortBy(this, f, ord);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.WrappedString, java.lang.Object] */
    public <B> WrappedString sorted(Ordering<B> ord) {
        return SeqLike.Cclass.sorted(this, ord);
    }

    public String stringPrefix() {
        return TraversableLike.Cclass.stringPrefix(this);
    }

    public <B> B sum(Numeric<B> num) {
        return TraversableOnce.Cclass.sum(this, num);
    }

    public Object tail() {
        return IndexedSeqOptimized.Cclass.tail(this);
    }

    public <B> Object toArray(ClassManifest<B> evidence$1) {
        return TraversableOnce.Cclass.toArray(this, evidence$1);
    }

    public <B> Buffer<B> toBuffer() {
        return TraversableOnce.Cclass.toBuffer(this);
    }

    public int toInt() {
        return StringLike.Cclass.toInt(this);
    }

    public List<Character> toList() {
        return TraversableOnce.Cclass.toList(this);
    }

    public <B> Set<B> toSet() {
        return TraversableOnce.Cclass.toSet(this);
    }

    public Stream<Character> toStream() {
        return IterableLike.Cclass.toStream(this);
    }

    public String toString() {
        return Proxy.Cclass.toString(this);
    }

    public <A1, B, That> That zip(scala.collection.Iterable<B> that, CanBuildFrom<WrappedString, Tuple2<A1, B>, That> bf) {
        return IndexedSeqOptimized.Cclass.zip(this, that, bf);
    }

    public WrappedString thisCollection() {
        return this;
    }

    public WrappedString toCollection(WrappedString repr) {
        return repr;
    }

    public Builder<Character, WrappedString> newBuilder() {
        return Builder.Cclass.mapResult(new StringBuilder(), new WrappedString$$anonfun$newBuilder$1());
    }

    public WrappedString slice(int from, int until) {
        return new WrappedString(self().substring(new RichInt(from).max(0), new RichInt(until).min(self().length())));
    }
}
