package scala.collection.immutable;

import scala.Function1;
import scala.Function2;
import scala.Predef$;
import scala.ScalaObject;
import scala.Tuple2;
import scala.collection.Iterable;
import scala.collection.IterableLike;
import scala.collection.Iterator;
import scala.collection.Set;
import scala.collection.SetLike;
import scala.collection.Traversable;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.Addable;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericSetTemplate;
import scala.collection.generic.GenericTraversableTemplate;
import scala.collection.generic.Subtractable;
import scala.collection.immutable.Iterable;
import scala.collection.immutable.Traversable;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.Builder;
import scala.collection.mutable.StringBuilder;
import scala.math.Numeric;
import scala.reflect.ClassManifest;
import scala.runtime.BoxesRunTime;

/* compiled from: Set.scala */
public interface Set<A> extends Iterable<A>, scala.collection.Set<A>, GenericSetTemplate<A, Set>, SetLike<A, Set<A>>, ScalaObject {

    /* renamed from: scala.collection.immutable.Set$class  reason: invalid class name */
    /* compiled from: Set.scala */
    public abstract class Cclass {
        public static void $init$(Set $this) {
        }

        public static GenericCompanion companion(Set $this) {
            return Set$.MODULE$;
        }
    }

    /* compiled from: Set.scala */
    public static class Set1<A> implements Set<A>, ScalaObject, Set {
        public static final long serialVersionUID = 1233385750652442003L;
        private final A elem1;

        public Set1(A elem12) {
            this.elem1 = elem12;
            TraversableOnce.Cclass.$init$(this);
            TraversableLike.Cclass.$init$(this);
            GenericTraversableTemplate.Cclass.$init$(this);
            Traversable.Cclass.$init$(this);
            Traversable.Cclass.$init$(this);
            IterableLike.Cclass.$init$(this);
            Iterable.Cclass.$init$(this);
            Iterable.Cclass.$init$(this);
            Function1.Cclass.$init$(this);
            GenericSetTemplate.Cclass.$init$(this);
            Addable.Cclass.$init$(this);
            Subtractable.Cclass.$init$(this);
            SetLike.Cclass.$init$(this);
            Set.Cclass.$init$(this);
            Cclass.$init$(this);
        }

        public <B> B $div$colon(B z, Function2<B, A, B> op) {
            return TraversableOnce.Cclass.$div$colon(this, z, op);
        }

        public <B, That> That $plus$plus(TraversableOnce<B> that, CanBuildFrom<Set<A>, B, That> bf) {
            return TraversableLike.Cclass.$plus$plus(this, that, bf);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.generic.Addable, scala.collection.immutable.Set<A>] */
        public Set<A> $plus$plus(TraversableOnce<A> xs) {
            return Addable.Cclass.$plus$plus(this, xs);
        }

        public StringBuilder addString(StringBuilder b, String start, String sep, String end) {
            return TraversableOnce.Cclass.addString(this, b, start, sep, end);
        }

        public <A> Function1<A, A> andThen(Function1<Boolean, A> g) {
            return Function1.Cclass.andThen(this, g);
        }

        public boolean apply(A elem) {
            return SetLike.Cclass.apply(this, elem);
        }

        public int apply$mcII$sp(int v1) {
            return Function1.Cclass.apply$mcII$sp(this, v1);
        }

        public void apply$mcVI$sp(int v1) {
            Function1.Cclass.apply$mcVI$sp(this, v1);
        }

        public void apply$mcVL$sp(long v1) {
            Function1.Cclass.apply$mcVL$sp(this, v1);
        }

        public boolean canEqual(Object that) {
            return IterableLike.Cclass.canEqual(this, that);
        }

        public GenericCompanion<Set> companion() {
            return Cclass.companion(this);
        }

        public <A> Function1<A, Boolean> compose(Function1<A, A> g) {
            return Function1.Cclass.compose(this, g);
        }

        public <B> void copyToArray(Object xs, int start) {
            TraversableOnce.Cclass.copyToArray(this, xs, start);
        }

        public <B> void copyToArray(Object xs, int start, int len) {
            IterableLike.Cclass.copyToArray(this, xs, start, len);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.Set<A>] */
        public Set<A> drop(int n) {
            return TraversableLike.Cclass.drop(this, n);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.Set, scala.collection.immutable.Set<A>] */
        public Set<A> empty() {
            return GenericSetTemplate.Cclass.empty(this);
        }

        public boolean equals(Object that) {
            return SetLike.Cclass.equals(this, that);
        }

        public boolean exists(Function1<A, Boolean> p) {
            return IterableLike.Cclass.exists(this, p);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.Set<A>] */
        public Set<A> filter(Function1<A, Boolean> p) {
            return TraversableLike.Cclass.filter(this, p);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.Set<A>] */
        public Set<A> filterNot(Function1<A, Boolean> p) {
            return TraversableLike.Cclass.filterNot(this, p);
        }

        public <B> B foldLeft(B z, Function2<B, A, B> op) {
            return TraversableOnce.Cclass.foldLeft(this, z, op);
        }

        public boolean forall(Function1<A, Boolean> p) {
            return IterableLike.Cclass.forall(this, p);
        }

        public <B> Builder<B, Set<B>> genericBuilder() {
            return GenericTraversableTemplate.Cclass.genericBuilder(this);
        }

        public int hashCode() {
            return SetLike.Cclass.hashCode(this);
        }

        public A head() {
            return IterableLike.Cclass.head(this);
        }

        public boolean isEmpty() {
            return SetLike.Cclass.isEmpty(this);
        }

        public final boolean isTraversableAgain() {
            return TraversableLike.Cclass.isTraversableAgain(this);
        }

        public A last() {
            return TraversableLike.Cclass.last(this);
        }

        public <B, That> That map(Function1<A, B> f, CanBuildFrom<Set<A>, B, That> bf) {
            return TraversableLike.Cclass.map(this, f, bf);
        }

        public String mkString() {
            return TraversableOnce.Cclass.mkString(this);
        }

        public String mkString(String sep) {
            return TraversableOnce.Cclass.mkString(this, sep);
        }

        public String mkString(String start, String sep, String end) {
            return TraversableOnce.Cclass.mkString(this, start, sep, end);
        }

        public Builder<A, Set<A>> newBuilder() {
            return SetLike.Cclass.newBuilder(this);
        }

        public boolean nonEmpty() {
            return TraversableOnce.Cclass.nonEmpty(this);
        }

        public <B> B reduceLeft(Function2<B, A, B> op) {
            return TraversableOnce.Cclass.reduceLeft(this, op);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.Set<A>] */
        public Set<A> repr() {
            return TraversableLike.Cclass.repr(this);
        }

        public <B> boolean sameElements(scala.collection.Iterable<B> that) {
            return IterableLike.Cclass.sameElements(this, that);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.Set<A>] */
        public Set<A> slice(int from, int until) {
            return IterableLike.Cclass.slice(this, from, until);
        }

        public String stringPrefix() {
            return SetLike.Cclass.stringPrefix(this);
        }

        public boolean subsetOf(scala.collection.Set<A> that) {
            return SetLike.Cclass.subsetOf(this, that);
        }

        public <B> B sum(Numeric<B> num) {
            return TraversableOnce.Cclass.sum(this, num);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.Set<A>] */
        public Set<A> tail() {
            return TraversableLike.Cclass.tail(this);
        }

        public scala.collection.Iterable<A> thisCollection() {
            return IterableLike.Cclass.thisCollection(this);
        }

        public <B> Object toArray(ClassManifest<B> evidence$1) {
            return TraversableOnce.Cclass.toArray(this, evidence$1);
        }

        public <B> Buffer<B> toBuffer() {
            return TraversableOnce.Cclass.toBuffer(this);
        }

        public List<A> toList() {
            return TraversableOnce.Cclass.toList(this);
        }

        public <B> Set<B> toSet() {
            return TraversableOnce.Cclass.toSet(this);
        }

        public Stream<A> toStream() {
            return IterableLike.Cclass.toStream(this);
        }

        public String toString() {
            return SetLike.Cclass.toString(this);
        }

        public <A1, B, That> That zip(scala.collection.Iterable<B> that, CanBuildFrom<Set<A>, Tuple2<A1, B>, That> bf) {
            return IterableLike.Cclass.zip(this, that, bf);
        }

        public int size() {
            return 1;
        }

        public boolean contains(A elem) {
            A a = this.elem1;
            return elem == a ? true : elem == null ? false : elem instanceof Number ? BoxesRunTime.equalsNumObject((Number) elem, a) : elem instanceof Character ? BoxesRunTime.equalsCharObject((Character) elem, a) : elem.equals(a);
        }

        public Set<A> $plus(A elem) {
            if (contains(elem)) {
                return this;
            }
            return new Set2(this.elem1, elem);
        }

        public Iterator<A> iterator() {
            return Predef$.MODULE$.genericWrapArray(new Object[]{this.elem1}).iterator();
        }

        public <U> void foreach(Function1<A, U> f) {
            f.apply(this.elem1);
        }
    }

    /* compiled from: Set.scala */
    public static class Set2<A> implements Set<A>, ScalaObject, Set {
        public static final long serialVersionUID = -6443011234944830092L;
        private final A elem1;
        private final A elem2;

        public Set2(A elem12, A elem22) {
            this.elem1 = elem12;
            this.elem2 = elem22;
            TraversableOnce.Cclass.$init$(this);
            TraversableLike.Cclass.$init$(this);
            GenericTraversableTemplate.Cclass.$init$(this);
            Traversable.Cclass.$init$(this);
            Traversable.Cclass.$init$(this);
            IterableLike.Cclass.$init$(this);
            Iterable.Cclass.$init$(this);
            Iterable.Cclass.$init$(this);
            Function1.Cclass.$init$(this);
            GenericSetTemplate.Cclass.$init$(this);
            Addable.Cclass.$init$(this);
            Subtractable.Cclass.$init$(this);
            SetLike.Cclass.$init$(this);
            Set.Cclass.$init$(this);
            Cclass.$init$(this);
        }

        public <B> B $div$colon(B z, Function2<B, A, B> op) {
            return TraversableOnce.Cclass.$div$colon(this, z, op);
        }

        public <B, That> That $plus$plus(TraversableOnce<B> that, CanBuildFrom<Set<A>, B, That> bf) {
            return TraversableLike.Cclass.$plus$plus(this, that, bf);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.generic.Addable, scala.collection.immutable.Set<A>] */
        public Set<A> $plus$plus(TraversableOnce<A> xs) {
            return Addable.Cclass.$plus$plus(this, xs);
        }

        public StringBuilder addString(StringBuilder b, String start, String sep, String end) {
            return TraversableOnce.Cclass.addString(this, b, start, sep, end);
        }

        public <A> Function1<A, A> andThen(Function1<Boolean, A> g) {
            return Function1.Cclass.andThen(this, g);
        }

        public boolean apply(A elem) {
            return SetLike.Cclass.apply(this, elem);
        }

        public int apply$mcII$sp(int v1) {
            return Function1.Cclass.apply$mcII$sp(this, v1);
        }

        public void apply$mcVI$sp(int v1) {
            Function1.Cclass.apply$mcVI$sp(this, v1);
        }

        public void apply$mcVL$sp(long v1) {
            Function1.Cclass.apply$mcVL$sp(this, v1);
        }

        public boolean canEqual(Object that) {
            return IterableLike.Cclass.canEqual(this, that);
        }

        public GenericCompanion<Set> companion() {
            return Cclass.companion(this);
        }

        public <A> Function1<A, Boolean> compose(Function1<A, A> g) {
            return Function1.Cclass.compose(this, g);
        }

        public <B> void copyToArray(Object xs, int start) {
            TraversableOnce.Cclass.copyToArray(this, xs, start);
        }

        public <B> void copyToArray(Object xs, int start, int len) {
            IterableLike.Cclass.copyToArray(this, xs, start, len);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.Set<A>] */
        public Set<A> drop(int n) {
            return TraversableLike.Cclass.drop(this, n);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.Set, scala.collection.immutable.Set<A>] */
        public Set<A> empty() {
            return GenericSetTemplate.Cclass.empty(this);
        }

        public boolean equals(Object that) {
            return SetLike.Cclass.equals(this, that);
        }

        public boolean exists(Function1<A, Boolean> p) {
            return IterableLike.Cclass.exists(this, p);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.Set<A>] */
        public Set<A> filter(Function1<A, Boolean> p) {
            return TraversableLike.Cclass.filter(this, p);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.Set<A>] */
        public Set<A> filterNot(Function1<A, Boolean> p) {
            return TraversableLike.Cclass.filterNot(this, p);
        }

        public <B> B foldLeft(B z, Function2<B, A, B> op) {
            return TraversableOnce.Cclass.foldLeft(this, z, op);
        }

        public boolean forall(Function1<A, Boolean> p) {
            return IterableLike.Cclass.forall(this, p);
        }

        public <B> Builder<B, Set<B>> genericBuilder() {
            return GenericTraversableTemplate.Cclass.genericBuilder(this);
        }

        public int hashCode() {
            return SetLike.Cclass.hashCode(this);
        }

        public A head() {
            return IterableLike.Cclass.head(this);
        }

        public boolean isEmpty() {
            return SetLike.Cclass.isEmpty(this);
        }

        public final boolean isTraversableAgain() {
            return TraversableLike.Cclass.isTraversableAgain(this);
        }

        public A last() {
            return TraversableLike.Cclass.last(this);
        }

        public <B, That> That map(Function1<A, B> f, CanBuildFrom<Set<A>, B, That> bf) {
            return TraversableLike.Cclass.map(this, f, bf);
        }

        public String mkString() {
            return TraversableOnce.Cclass.mkString(this);
        }

        public String mkString(String sep) {
            return TraversableOnce.Cclass.mkString(this, sep);
        }

        public String mkString(String start, String sep, String end) {
            return TraversableOnce.Cclass.mkString(this, start, sep, end);
        }

        public Builder<A, Set<A>> newBuilder() {
            return SetLike.Cclass.newBuilder(this);
        }

        public boolean nonEmpty() {
            return TraversableOnce.Cclass.nonEmpty(this);
        }

        public <B> B reduceLeft(Function2<B, A, B> op) {
            return TraversableOnce.Cclass.reduceLeft(this, op);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.Set<A>] */
        public Set<A> repr() {
            return TraversableLike.Cclass.repr(this);
        }

        public <B> boolean sameElements(scala.collection.Iterable<B> that) {
            return IterableLike.Cclass.sameElements(this, that);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.Set<A>] */
        public Set<A> slice(int from, int until) {
            return IterableLike.Cclass.slice(this, from, until);
        }

        public String stringPrefix() {
            return SetLike.Cclass.stringPrefix(this);
        }

        public boolean subsetOf(scala.collection.Set<A> that) {
            return SetLike.Cclass.subsetOf(this, that);
        }

        public <B> B sum(Numeric<B> num) {
            return TraversableOnce.Cclass.sum(this, num);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.Set<A>] */
        public Set<A> tail() {
            return TraversableLike.Cclass.tail(this);
        }

        public scala.collection.Iterable<A> thisCollection() {
            return IterableLike.Cclass.thisCollection(this);
        }

        public <B> Object toArray(ClassManifest<B> evidence$1) {
            return TraversableOnce.Cclass.toArray(this, evidence$1);
        }

        public <B> Buffer<B> toBuffer() {
            return TraversableOnce.Cclass.toBuffer(this);
        }

        public List<A> toList() {
            return TraversableOnce.Cclass.toList(this);
        }

        public <B> Set<B> toSet() {
            return TraversableOnce.Cclass.toSet(this);
        }

        public Stream<A> toStream() {
            return IterableLike.Cclass.toStream(this);
        }

        public String toString() {
            return SetLike.Cclass.toString(this);
        }

        public <A1, B, That> That zip(scala.collection.Iterable<B> that, CanBuildFrom<Set<A>, Tuple2<A1, B>, That> bf) {
            return IterableLike.Cclass.zip(this, that, bf);
        }

        public int size() {
            return 2;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public boolean contains(A r6) {
            /*
                r5 = this;
                r4 = 1
                r3 = 0
                A r2 = r5.elem1
                if (r6 != r2) goto L_0x0012
                r1 = r4
            L_0x0007:
                if (r1 != 0) goto L_0x0010
                A r1 = r5.elem2
                if (r6 != r1) goto L_0x0035
                r1 = r4
            L_0x000e:
                if (r1 == 0) goto L_0x0054
            L_0x0010:
                r1 = r4
            L_0x0011:
                return r1
            L_0x0012:
                if (r6 != 0) goto L_0x0016
                r1 = r3
                goto L_0x0007
            L_0x0016:
                boolean r1 = r6 instanceof java.lang.Number
                if (r1 == 0) goto L_0x0023
                r0 = r6
                java.lang.Number r0 = (java.lang.Number) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsNumObject(r1, r2)
                goto L_0x0007
            L_0x0023:
                boolean r1 = r6 instanceof java.lang.Character
                if (r1 == 0) goto L_0x0030
                r0 = r6
                java.lang.Character r0 = (java.lang.Character) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsCharObject(r1, r2)
                goto L_0x0007
            L_0x0030:
                boolean r1 = r6.equals(r2)
                goto L_0x0007
            L_0x0035:
                if (r6 != 0) goto L_0x0039
                r1 = r3
                goto L_0x000e
            L_0x0039:
                boolean r2 = r6 instanceof java.lang.Number
                if (r2 == 0) goto L_0x0044
                java.lang.Number r6 = (java.lang.Number) r6
                boolean r1 = scala.runtime.BoxesRunTime.equalsNumObject(r6, r1)
                goto L_0x000e
            L_0x0044:
                boolean r2 = r6 instanceof java.lang.Character
                if (r2 == 0) goto L_0x004f
                java.lang.Character r6 = (java.lang.Character) r6
                boolean r1 = scala.runtime.BoxesRunTime.equalsCharObject(r6, r1)
                goto L_0x000e
            L_0x004f:
                boolean r1 = r6.equals(r1)
                goto L_0x000e
            L_0x0054:
                r1 = r3
                goto L_0x0011
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.Set.Set2.contains(java.lang.Object):boolean");
        }

        public Set<A> $plus(A elem) {
            if (contains(elem)) {
                return this;
            }
            return new Set3(this.elem1, this.elem2, elem);
        }

        public Iterator<A> iterator() {
            return Predef$.MODULE$.genericWrapArray(new Object[]{this.elem1, this.elem2}).iterator();
        }

        public <U> void foreach(Function1<A, U> f) {
            f.apply(this.elem1);
            f.apply(this.elem2);
        }
    }

    /* compiled from: Set.scala */
    public static class Set3<A> implements Set<A>, ScalaObject, Set {
        public static final long serialVersionUID = -3590273538119220064L;
        private final A elem1;
        private final A elem2;
        private final A elem3;

        public Set3(A elem12, A elem22, A elem32) {
            this.elem1 = elem12;
            this.elem2 = elem22;
            this.elem3 = elem32;
            TraversableOnce.Cclass.$init$(this);
            TraversableLike.Cclass.$init$(this);
            GenericTraversableTemplate.Cclass.$init$(this);
            Traversable.Cclass.$init$(this);
            Traversable.Cclass.$init$(this);
            IterableLike.Cclass.$init$(this);
            Iterable.Cclass.$init$(this);
            Iterable.Cclass.$init$(this);
            Function1.Cclass.$init$(this);
            GenericSetTemplate.Cclass.$init$(this);
            Addable.Cclass.$init$(this);
            Subtractable.Cclass.$init$(this);
            SetLike.Cclass.$init$(this);
            Set.Cclass.$init$(this);
            Cclass.$init$(this);
        }

        public <B> B $div$colon(B z, Function2<B, A, B> op) {
            return TraversableOnce.Cclass.$div$colon(this, z, op);
        }

        public <B, That> That $plus$plus(TraversableOnce<B> that, CanBuildFrom<Set<A>, B, That> bf) {
            return TraversableLike.Cclass.$plus$plus(this, that, bf);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.generic.Addable, scala.collection.immutable.Set<A>] */
        public Set<A> $plus$plus(TraversableOnce<A> xs) {
            return Addable.Cclass.$plus$plus(this, xs);
        }

        public StringBuilder addString(StringBuilder b, String start, String sep, String end) {
            return TraversableOnce.Cclass.addString(this, b, start, sep, end);
        }

        public <A> Function1<A, A> andThen(Function1<Boolean, A> g) {
            return Function1.Cclass.andThen(this, g);
        }

        public boolean apply(A elem) {
            return SetLike.Cclass.apply(this, elem);
        }

        public int apply$mcII$sp(int v1) {
            return Function1.Cclass.apply$mcII$sp(this, v1);
        }

        public void apply$mcVI$sp(int v1) {
            Function1.Cclass.apply$mcVI$sp(this, v1);
        }

        public void apply$mcVL$sp(long v1) {
            Function1.Cclass.apply$mcVL$sp(this, v1);
        }

        public boolean canEqual(Object that) {
            return IterableLike.Cclass.canEqual(this, that);
        }

        public GenericCompanion<Set> companion() {
            return Cclass.companion(this);
        }

        public <A> Function1<A, Boolean> compose(Function1<A, A> g) {
            return Function1.Cclass.compose(this, g);
        }

        public <B> void copyToArray(Object xs, int start) {
            TraversableOnce.Cclass.copyToArray(this, xs, start);
        }

        public <B> void copyToArray(Object xs, int start, int len) {
            IterableLike.Cclass.copyToArray(this, xs, start, len);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.Set<A>] */
        public Set<A> drop(int n) {
            return TraversableLike.Cclass.drop(this, n);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.Set, scala.collection.immutable.Set<A>] */
        public Set<A> empty() {
            return GenericSetTemplate.Cclass.empty(this);
        }

        public boolean equals(Object that) {
            return SetLike.Cclass.equals(this, that);
        }

        public boolean exists(Function1<A, Boolean> p) {
            return IterableLike.Cclass.exists(this, p);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.Set<A>] */
        public Set<A> filter(Function1<A, Boolean> p) {
            return TraversableLike.Cclass.filter(this, p);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.Set<A>] */
        public Set<A> filterNot(Function1<A, Boolean> p) {
            return TraversableLike.Cclass.filterNot(this, p);
        }

        public <B> B foldLeft(B z, Function2<B, A, B> op) {
            return TraversableOnce.Cclass.foldLeft(this, z, op);
        }

        public boolean forall(Function1<A, Boolean> p) {
            return IterableLike.Cclass.forall(this, p);
        }

        public <B> Builder<B, Set<B>> genericBuilder() {
            return GenericTraversableTemplate.Cclass.genericBuilder(this);
        }

        public int hashCode() {
            return SetLike.Cclass.hashCode(this);
        }

        public A head() {
            return IterableLike.Cclass.head(this);
        }

        public boolean isEmpty() {
            return SetLike.Cclass.isEmpty(this);
        }

        public final boolean isTraversableAgain() {
            return TraversableLike.Cclass.isTraversableAgain(this);
        }

        public A last() {
            return TraversableLike.Cclass.last(this);
        }

        public <B, That> That map(Function1<A, B> f, CanBuildFrom<Set<A>, B, That> bf) {
            return TraversableLike.Cclass.map(this, f, bf);
        }

        public String mkString() {
            return TraversableOnce.Cclass.mkString(this);
        }

        public String mkString(String sep) {
            return TraversableOnce.Cclass.mkString(this, sep);
        }

        public String mkString(String start, String sep, String end) {
            return TraversableOnce.Cclass.mkString(this, start, sep, end);
        }

        public Builder<A, Set<A>> newBuilder() {
            return SetLike.Cclass.newBuilder(this);
        }

        public boolean nonEmpty() {
            return TraversableOnce.Cclass.nonEmpty(this);
        }

        public <B> B reduceLeft(Function2<B, A, B> op) {
            return TraversableOnce.Cclass.reduceLeft(this, op);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.Set<A>] */
        public Set<A> repr() {
            return TraversableLike.Cclass.repr(this);
        }

        public <B> boolean sameElements(scala.collection.Iterable<B> that) {
            return IterableLike.Cclass.sameElements(this, that);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.Set<A>] */
        public Set<A> slice(int from, int until) {
            return IterableLike.Cclass.slice(this, from, until);
        }

        public String stringPrefix() {
            return SetLike.Cclass.stringPrefix(this);
        }

        public boolean subsetOf(scala.collection.Set<A> that) {
            return SetLike.Cclass.subsetOf(this, that);
        }

        public <B> B sum(Numeric<B> num) {
            return TraversableOnce.Cclass.sum(this, num);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.Set<A>] */
        public Set<A> tail() {
            return TraversableLike.Cclass.tail(this);
        }

        public scala.collection.Iterable<A> thisCollection() {
            return IterableLike.Cclass.thisCollection(this);
        }

        public <B> Object toArray(ClassManifest<B> evidence$1) {
            return TraversableOnce.Cclass.toArray(this, evidence$1);
        }

        public <B> Buffer<B> toBuffer() {
            return TraversableOnce.Cclass.toBuffer(this);
        }

        public List<A> toList() {
            return TraversableOnce.Cclass.toList(this);
        }

        public <B> Set<B> toSet() {
            return TraversableOnce.Cclass.toSet(this);
        }

        public Stream<A> toStream() {
            return IterableLike.Cclass.toStream(this);
        }

        public String toString() {
            return SetLike.Cclass.toString(this);
        }

        public <A1, B, That> That zip(scala.collection.Iterable<B> that, CanBuildFrom<Set<A>, Tuple2<A1, B>, That> bf) {
            return IterableLike.Cclass.zip(this, that, bf);
        }

        public int size() {
            return 3;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public boolean contains(A r6) {
            /*
                r5 = this;
                r4 = 1
                r3 = 0
                A r2 = r5.elem1
                if (r6 != r2) goto L_0x0019
                r1 = r4
            L_0x0007:
                if (r1 != 0) goto L_0x0017
                A r2 = r5.elem2
                if (r6 != r2) goto L_0x003c
                r1 = r4
            L_0x000e:
                if (r1 != 0) goto L_0x0017
                A r1 = r5.elem3
                if (r6 != r1) goto L_0x005f
                r1 = r4
            L_0x0015:
                if (r1 == 0) goto L_0x007e
            L_0x0017:
                r1 = r4
            L_0x0018:
                return r1
            L_0x0019:
                if (r6 != 0) goto L_0x001d
                r1 = r3
                goto L_0x0007
            L_0x001d:
                boolean r1 = r6 instanceof java.lang.Number
                if (r1 == 0) goto L_0x002a
                r0 = r6
                java.lang.Number r0 = (java.lang.Number) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsNumObject(r1, r2)
                goto L_0x0007
            L_0x002a:
                boolean r1 = r6 instanceof java.lang.Character
                if (r1 == 0) goto L_0x0037
                r0 = r6
                java.lang.Character r0 = (java.lang.Character) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsCharObject(r1, r2)
                goto L_0x0007
            L_0x0037:
                boolean r1 = r6.equals(r2)
                goto L_0x0007
            L_0x003c:
                if (r6 != 0) goto L_0x0040
                r1 = r3
                goto L_0x000e
            L_0x0040:
                boolean r1 = r6 instanceof java.lang.Number
                if (r1 == 0) goto L_0x004d
                r0 = r6
                java.lang.Number r0 = (java.lang.Number) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsNumObject(r1, r2)
                goto L_0x000e
            L_0x004d:
                boolean r1 = r6 instanceof java.lang.Character
                if (r1 == 0) goto L_0x005a
                r0 = r6
                java.lang.Character r0 = (java.lang.Character) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsCharObject(r1, r2)
                goto L_0x000e
            L_0x005a:
                boolean r1 = r6.equals(r2)
                goto L_0x000e
            L_0x005f:
                if (r6 != 0) goto L_0x0063
                r1 = r3
                goto L_0x0015
            L_0x0063:
                boolean r2 = r6 instanceof java.lang.Number
                if (r2 == 0) goto L_0x006e
                java.lang.Number r6 = (java.lang.Number) r6
                boolean r1 = scala.runtime.BoxesRunTime.equalsNumObject(r6, r1)
                goto L_0x0015
            L_0x006e:
                boolean r2 = r6 instanceof java.lang.Character
                if (r2 == 0) goto L_0x0079
                java.lang.Character r6 = (java.lang.Character) r6
                boolean r1 = scala.runtime.BoxesRunTime.equalsCharObject(r6, r1)
                goto L_0x0015
            L_0x0079:
                boolean r1 = r6.equals(r1)
                goto L_0x0015
            L_0x007e:
                r1 = r3
                goto L_0x0018
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.Set.Set3.contains(java.lang.Object):boolean");
        }

        public Set<A> $plus(A elem) {
            if (contains(elem)) {
                return this;
            }
            return new Set4(this.elem1, this.elem2, this.elem3, elem);
        }

        public Iterator<A> iterator() {
            return Predef$.MODULE$.genericWrapArray(new Object[]{this.elem1, this.elem2, this.elem3}).iterator();
        }

        public <U> void foreach(Function1<A, U> f) {
            f.apply(this.elem1);
            f.apply(this.elem2);
            f.apply(this.elem3);
        }
    }

    /* compiled from: Set.scala */
    public static class Set4<A> implements Set<A>, ScalaObject, Set {
        public static final long serialVersionUID = -3622399588156184395L;
        private final A elem1;
        private final A elem2;
        private final A elem3;
        private final A elem4;

        public Set4(A elem12, A elem22, A elem32, A elem42) {
            this.elem1 = elem12;
            this.elem2 = elem22;
            this.elem3 = elem32;
            this.elem4 = elem42;
            TraversableOnce.Cclass.$init$(this);
            TraversableLike.Cclass.$init$(this);
            GenericTraversableTemplate.Cclass.$init$(this);
            Traversable.Cclass.$init$(this);
            Traversable.Cclass.$init$(this);
            IterableLike.Cclass.$init$(this);
            Iterable.Cclass.$init$(this);
            Iterable.Cclass.$init$(this);
            Function1.Cclass.$init$(this);
            GenericSetTemplate.Cclass.$init$(this);
            Addable.Cclass.$init$(this);
            Subtractable.Cclass.$init$(this);
            SetLike.Cclass.$init$(this);
            Set.Cclass.$init$(this);
            Cclass.$init$(this);
        }

        public <B> B $div$colon(B z, Function2<B, A, B> op) {
            return TraversableOnce.Cclass.$div$colon(this, z, op);
        }

        public <B, That> That $plus$plus(TraversableOnce<B> that, CanBuildFrom<Set<A>, B, That> bf) {
            return TraversableLike.Cclass.$plus$plus(this, that, bf);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.generic.Addable, scala.collection.immutable.Set<A>] */
        public Set<A> $plus$plus(TraversableOnce<A> xs) {
            return Addable.Cclass.$plus$plus(this, xs);
        }

        public StringBuilder addString(StringBuilder b, String start, String sep, String end) {
            return TraversableOnce.Cclass.addString(this, b, start, sep, end);
        }

        public <A> Function1<A, A> andThen(Function1<Boolean, A> g) {
            return Function1.Cclass.andThen(this, g);
        }

        public boolean apply(A elem) {
            return SetLike.Cclass.apply(this, elem);
        }

        public int apply$mcII$sp(int v1) {
            return Function1.Cclass.apply$mcII$sp(this, v1);
        }

        public void apply$mcVI$sp(int v1) {
            Function1.Cclass.apply$mcVI$sp(this, v1);
        }

        public void apply$mcVL$sp(long v1) {
            Function1.Cclass.apply$mcVL$sp(this, v1);
        }

        public boolean canEqual(Object that) {
            return IterableLike.Cclass.canEqual(this, that);
        }

        public GenericCompanion<Set> companion() {
            return Cclass.companion(this);
        }

        public <A> Function1<A, Boolean> compose(Function1<A, A> g) {
            return Function1.Cclass.compose(this, g);
        }

        public <B> void copyToArray(Object xs, int start) {
            TraversableOnce.Cclass.copyToArray(this, xs, start);
        }

        public <B> void copyToArray(Object xs, int start, int len) {
            IterableLike.Cclass.copyToArray(this, xs, start, len);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.Set<A>] */
        public Set<A> drop(int n) {
            return TraversableLike.Cclass.drop(this, n);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.Set, scala.collection.immutable.Set<A>] */
        public Set<A> empty() {
            return GenericSetTemplate.Cclass.empty(this);
        }

        public boolean equals(Object that) {
            return SetLike.Cclass.equals(this, that);
        }

        public boolean exists(Function1<A, Boolean> p) {
            return IterableLike.Cclass.exists(this, p);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.Set<A>] */
        public Set<A> filter(Function1<A, Boolean> p) {
            return TraversableLike.Cclass.filter(this, p);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.Set<A>] */
        public Set<A> filterNot(Function1<A, Boolean> p) {
            return TraversableLike.Cclass.filterNot(this, p);
        }

        public <B> B foldLeft(B z, Function2<B, A, B> op) {
            return TraversableOnce.Cclass.foldLeft(this, z, op);
        }

        public boolean forall(Function1<A, Boolean> p) {
            return IterableLike.Cclass.forall(this, p);
        }

        public <B> Builder<B, Set<B>> genericBuilder() {
            return GenericTraversableTemplate.Cclass.genericBuilder(this);
        }

        public int hashCode() {
            return SetLike.Cclass.hashCode(this);
        }

        public A head() {
            return IterableLike.Cclass.head(this);
        }

        public boolean isEmpty() {
            return SetLike.Cclass.isEmpty(this);
        }

        public final boolean isTraversableAgain() {
            return TraversableLike.Cclass.isTraversableAgain(this);
        }

        public A last() {
            return TraversableLike.Cclass.last(this);
        }

        public <B, That> That map(Function1<A, B> f, CanBuildFrom<Set<A>, B, That> bf) {
            return TraversableLike.Cclass.map(this, f, bf);
        }

        public String mkString() {
            return TraversableOnce.Cclass.mkString(this);
        }

        public String mkString(String sep) {
            return TraversableOnce.Cclass.mkString(this, sep);
        }

        public String mkString(String start, String sep, String end) {
            return TraversableOnce.Cclass.mkString(this, start, sep, end);
        }

        public Builder<A, Set<A>> newBuilder() {
            return SetLike.Cclass.newBuilder(this);
        }

        public boolean nonEmpty() {
            return TraversableOnce.Cclass.nonEmpty(this);
        }

        public <B> B reduceLeft(Function2<B, A, B> op) {
            return TraversableOnce.Cclass.reduceLeft(this, op);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.Set<A>] */
        public Set<A> repr() {
            return TraversableLike.Cclass.repr(this);
        }

        public <B> boolean sameElements(scala.collection.Iterable<B> that) {
            return IterableLike.Cclass.sameElements(this, that);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.Set<A>] */
        public Set<A> slice(int from, int until) {
            return IterableLike.Cclass.slice(this, from, until);
        }

        public String stringPrefix() {
            return SetLike.Cclass.stringPrefix(this);
        }

        public boolean subsetOf(scala.collection.Set<A> that) {
            return SetLike.Cclass.subsetOf(this, that);
        }

        public <B> B sum(Numeric<B> num) {
            return TraversableOnce.Cclass.sum(this, num);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.Set<A>] */
        public Set<A> tail() {
            return TraversableLike.Cclass.tail(this);
        }

        public scala.collection.Iterable<A> thisCollection() {
            return IterableLike.Cclass.thisCollection(this);
        }

        public <B> Object toArray(ClassManifest<B> evidence$1) {
            return TraversableOnce.Cclass.toArray(this, evidence$1);
        }

        public <B> Buffer<B> toBuffer() {
            return TraversableOnce.Cclass.toBuffer(this);
        }

        public List<A> toList() {
            return TraversableOnce.Cclass.toList(this);
        }

        public <B> Set<B> toSet() {
            return TraversableOnce.Cclass.toSet(this);
        }

        public Stream<A> toStream() {
            return IterableLike.Cclass.toStream(this);
        }

        public String toString() {
            return SetLike.Cclass.toString(this);
        }

        public <A1, B, That> That zip(scala.collection.Iterable<B> that, CanBuildFrom<Set<A>, Tuple2<A1, B>, That> bf) {
            return IterableLike.Cclass.zip(this, that, bf);
        }

        public int size() {
            return 4;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public boolean contains(A r6) {
            /*
                r5 = this;
                r4 = 1
                r3 = 0
                A r2 = r5.elem1
                if (r6 != r2) goto L_0x0020
                r1 = r4
            L_0x0007:
                if (r1 != 0) goto L_0x001e
                A r2 = r5.elem2
                if (r6 != r2) goto L_0x0043
                r1 = r4
            L_0x000e:
                if (r1 != 0) goto L_0x001e
                A r2 = r5.elem3
                if (r6 != r2) goto L_0x0066
                r1 = r4
            L_0x0015:
                if (r1 != 0) goto L_0x001e
                A r1 = r5.elem4
                if (r6 != r1) goto L_0x0089
                r1 = r4
            L_0x001c:
                if (r1 == 0) goto L_0x00aa
            L_0x001e:
                r1 = r4
            L_0x001f:
                return r1
            L_0x0020:
                if (r6 != 0) goto L_0x0024
                r1 = r3
                goto L_0x0007
            L_0x0024:
                boolean r1 = r6 instanceof java.lang.Number
                if (r1 == 0) goto L_0x0031
                r0 = r6
                java.lang.Number r0 = (java.lang.Number) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsNumObject(r1, r2)
                goto L_0x0007
            L_0x0031:
                boolean r1 = r6 instanceof java.lang.Character
                if (r1 == 0) goto L_0x003e
                r0 = r6
                java.lang.Character r0 = (java.lang.Character) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsCharObject(r1, r2)
                goto L_0x0007
            L_0x003e:
                boolean r1 = r6.equals(r2)
                goto L_0x0007
            L_0x0043:
                if (r6 != 0) goto L_0x0047
                r1 = r3
                goto L_0x000e
            L_0x0047:
                boolean r1 = r6 instanceof java.lang.Number
                if (r1 == 0) goto L_0x0054
                r0 = r6
                java.lang.Number r0 = (java.lang.Number) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsNumObject(r1, r2)
                goto L_0x000e
            L_0x0054:
                boolean r1 = r6 instanceof java.lang.Character
                if (r1 == 0) goto L_0x0061
                r0 = r6
                java.lang.Character r0 = (java.lang.Character) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsCharObject(r1, r2)
                goto L_0x000e
            L_0x0061:
                boolean r1 = r6.equals(r2)
                goto L_0x000e
            L_0x0066:
                if (r6 != 0) goto L_0x006a
                r1 = r3
                goto L_0x0015
            L_0x006a:
                boolean r1 = r6 instanceof java.lang.Number
                if (r1 == 0) goto L_0x0077
                r0 = r6
                java.lang.Number r0 = (java.lang.Number) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsNumObject(r1, r2)
                goto L_0x0015
            L_0x0077:
                boolean r1 = r6 instanceof java.lang.Character
                if (r1 == 0) goto L_0x0084
                r0 = r6
                java.lang.Character r0 = (java.lang.Character) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsCharObject(r1, r2)
                goto L_0x0015
            L_0x0084:
                boolean r1 = r6.equals(r2)
                goto L_0x0015
            L_0x0089:
                if (r6 != 0) goto L_0x008d
                r1 = r3
                goto L_0x001c
            L_0x008d:
                boolean r2 = r6 instanceof java.lang.Number
                if (r2 == 0) goto L_0x0098
                java.lang.Number r6 = (java.lang.Number) r6
                boolean r1 = scala.runtime.BoxesRunTime.equalsNumObject(r6, r1)
                goto L_0x001c
            L_0x0098:
                boolean r2 = r6 instanceof java.lang.Character
                if (r2 == 0) goto L_0x00a4
                java.lang.Character r6 = (java.lang.Character) r6
                boolean r1 = scala.runtime.BoxesRunTime.equalsCharObject(r6, r1)
                goto L_0x001c
            L_0x00a4:
                boolean r1 = r6.equals(r1)
                goto L_0x001c
            L_0x00aa:
                r1 = r3
                goto L_0x001f
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.Set.Set4.contains(java.lang.Object):boolean");
        }

        public Set<A> $plus(A elem) {
            if (contains(elem)) {
                return this;
            }
            return new HashSet().$plus(this.elem1, this.elem2, Predef$.MODULE$.genericWrapArray(new Object[]{this.elem3, this.elem4, elem}));
        }

        public Iterator<A> iterator() {
            return Predef$.MODULE$.genericWrapArray(new Object[]{this.elem1, this.elem2, this.elem3, this.elem4}).iterator();
        }

        public <U> void foreach(Function1<A, U> f) {
            f.apply(this.elem1);
            f.apply(this.elem2);
            f.apply(this.elem3);
            f.apply(this.elem4);
        }
    }
}
