package scala.collection.immutable;

import java.io.Serializable;
import scala.Function0;
import scala.runtime.AbstractFunction0;

/* compiled from: Stream.scala */
public final class Stream$$anonfun$fill$1 extends AbstractFunction0 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Function0 elem$2;
    public final /* synthetic */ int n$2;

    public Stream$$anonfun$fill$1(int i, Function0 function0) {
        this.n$2 = i;
        this.elem$2 = function0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: scala.collection.immutable.Stream$.fill(int, scala.Function0):scala.collection.immutable.Stream<A>
     arg types: [int, scala.Function0]
     candidates:
      scala.collection.immutable.Stream$.fill(int, scala.Function0):scala.collection.Traversable
      scala.collection.generic.TraversableFactory.fill(int, scala.Function0):CC
      scala.collection.immutable.Stream$.fill(int, scala.Function0):scala.collection.immutable.Stream<A> */
    public final Stream<A> apply() {
        return Stream$.MODULE$.fill(this.n$2 - 1, this.elem$2);
    }
}
