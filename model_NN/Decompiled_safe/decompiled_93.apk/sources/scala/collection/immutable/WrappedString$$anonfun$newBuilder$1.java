package scala.collection.immutable;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;

/* compiled from: WrappedString.scala */
public final class WrappedString$$anonfun$newBuilder$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;

    public final WrappedString apply(String str) {
        return new WrappedString(str);
    }
}
