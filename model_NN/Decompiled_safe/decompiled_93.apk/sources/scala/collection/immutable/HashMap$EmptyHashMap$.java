package scala.collection.immutable;

import scala.ScalaObject;
import scala.runtime.Nothing$;

/* compiled from: HashMap.scala */
public final class HashMap$EmptyHashMap$ extends HashMap<Object, Nothing$> implements ScalaObject {
    public static final HashMap$EmptyHashMap$ MODULE$ = null;

    static {
        new HashMap$EmptyHashMap$();
    }

    public HashMap$EmptyHashMap$() {
        MODULE$ = this;
    }
}
