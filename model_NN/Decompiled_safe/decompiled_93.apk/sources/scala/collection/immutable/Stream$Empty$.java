package scala.collection.immutable;

import java.util.NoSuchElementException;
import scala.ScalaObject;
import scala.runtime.Nothing$;

/* compiled from: Stream.scala */
public final class Stream$Empty$ extends Stream<Nothing$> implements ScalaObject {
    public static final Stream$Empty$ MODULE$ = null;

    static {
        new Stream$Empty$();
    }

    public Stream$Empty$() {
        MODULE$ = this;
    }

    public boolean isEmpty() {
        return true;
    }

    public Nothing$ head() {
        throw new NoSuchElementException("head of empty stream");
    }

    public Nothing$ tail() {
        throw new UnsupportedOperationException("tail of empty stream");
    }

    public boolean tailDefined() {
        return false;
    }
}
