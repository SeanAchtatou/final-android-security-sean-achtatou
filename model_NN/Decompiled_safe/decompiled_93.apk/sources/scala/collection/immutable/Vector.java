package scala.collection.immutable;

import scala.Function1;
import scala.Function2;
import scala.MatchError;
import scala.PartialFunction;
import scala.ScalaObject;
import scala.Tuple2;
import scala.collection.IndexedSeq;
import scala.collection.IndexedSeqLike;
import scala.collection.Iterable;
import scala.collection.IterableLike;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.SeqLike;
import scala.collection.Traversable;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericTraversableTemplate;
import scala.collection.immutable.IndexedSeq;
import scala.collection.immutable.Iterable;
import scala.collection.immutable.Seq;
import scala.collection.immutable.Traversable;
import scala.collection.immutable.VectorPointer;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.Builder;
import scala.collection.mutable.StringBuilder;
import scala.math.Numeric;
import scala.math.Ordering;
import scala.reflect.ClassManifest;
import scala.runtime.BoxesRunTime;

/* compiled from: Vector.scala */
public final class Vector<A> implements IndexedSeq<A>, GenericTraversableTemplate<A, Vector>, IndexedSeqLike<A, Vector<A>>, VectorPointer<A>, ScalaObject, VectorPointer {
    private int depth;
    private boolean dirty = false;
    private Object[] display0;
    private Object[] display1;
    private Object[] display2;
    private Object[] display3;
    private Object[] display4;
    private Object[] display5;
    public final int endIndex;
    public final int focus;
    public final int startIndex;

    public Vector(int startIndex2, int endIndex2, int focus2) {
        this.startIndex = startIndex2;
        this.endIndex = endIndex2;
        this.focus = focus2;
        TraversableOnce.Cclass.$init$(this);
        TraversableLike.Cclass.$init$(this);
        GenericTraversableTemplate.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        IterableLike.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Function1.Cclass.$init$(this);
        PartialFunction.Cclass.$init$(this);
        SeqLike.Cclass.$init$(this);
        Seq.Cclass.$init$(this);
        Seq.Cclass.$init$(this);
        IndexedSeqLike.Cclass.$init$(this);
        IndexedSeq.Cclass.$init$(this);
        IndexedSeq.Cclass.$init$(this);
        VectorPointer.Cclass.$init$(this);
    }

    public <B> B $div$colon(B z, Function2<B, A, B> op) {
        return TraversableOnce.Cclass.$div$colon(this, z, op);
    }

    public <B, That> That $plus$plus(TraversableOnce<B> that, CanBuildFrom<Vector<A>, B, That> bf) {
        return TraversableLike.Cclass.$plus$plus(this, that, bf);
    }

    public StringBuilder addString(StringBuilder b, String start, String sep, String end) {
        return TraversableOnce.Cclass.addString(this, b, start, sep, end);
    }

    public <C> PartialFunction<Integer, C> andThen(Function1<A, C> k) {
        return PartialFunction.Cclass.andThen(this, k);
    }

    public /* bridge */ /* synthetic */ Object apply(Object v1) {
        return apply(BoxesRunTime.unboxToInt(v1));
    }

    public int apply$mcII$sp(int v1) {
        return Function1.Cclass.apply$mcII$sp(this, v1);
    }

    public void apply$mcVI$sp(int v1) {
        Function1.Cclass.apply$mcVI$sp(this, v1);
    }

    public void apply$mcVL$sp(long v1) {
        Function1.Cclass.apply$mcVL$sp(this, v1);
    }

    public boolean canEqual(Object that) {
        return IterableLike.Cclass.canEqual(this, that);
    }

    public <A> Function1<A, A> compose(Function1<A, Integer> g) {
        return Function1.Cclass.compose(this, g);
    }

    public boolean contains(Object elem) {
        return SeqLike.Cclass.contains(this, elem);
    }

    public final Object[] copyOf(Object[] a) {
        return VectorPointer.Cclass.copyOf(this, a);
    }

    public <B> void copyToArray(Object xs, int start) {
        TraversableOnce.Cclass.copyToArray(this, xs, start);
    }

    public <B> void copyToArray(Object xs, int start, int len) {
        IterableLike.Cclass.copyToArray(this, xs, start, len);
    }

    public int depth() {
        return this.depth;
    }

    public void depth_$eq(int i) {
        this.depth = i;
    }

    public Object[] display0() {
        return this.display0;
    }

    public void display0_$eq(Object[] objArr) {
        this.display0 = objArr;
    }

    public Object[] display1() {
        return this.display1;
    }

    public void display1_$eq(Object[] objArr) {
        this.display1 = objArr;
    }

    public Object[] display2() {
        return this.display2;
    }

    public void display2_$eq(Object[] objArr) {
        this.display2 = objArr;
    }

    public Object[] display3() {
        return this.display3;
    }

    public void display3_$eq(Object[] objArr) {
        this.display3 = objArr;
    }

    public Object[] display4() {
        return this.display4;
    }

    public void display4_$eq(Object[] objArr) {
        this.display4 = objArr;
    }

    public Object[] display5() {
        return this.display5;
    }

    public void display5_$eq(Object[] objArr) {
        this.display5 = objArr;
    }

    public boolean equals(Object that) {
        return SeqLike.Cclass.equals(this, that);
    }

    public boolean exists(Function1<A, Boolean> p) {
        return IterableLike.Cclass.exists(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Vector<A>, java.lang.Object] */
    public Vector<A> filter(Function1<A, Boolean> p) {
        return TraversableLike.Cclass.filter(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Vector<A>, java.lang.Object] */
    public Vector<A> filterNot(Function1<A, Boolean> p) {
        return TraversableLike.Cclass.filterNot(this, p);
    }

    public <B> B foldLeft(B z, Function2<B, A, B> op) {
        return TraversableOnce.Cclass.foldLeft(this, z, op);
    }

    public boolean forall(Function1<A, Boolean> p) {
        return IterableLike.Cclass.forall(this, p);
    }

    public <U> void foreach(Function1<A, U> f) {
        IterableLike.Cclass.foreach(this, f);
    }

    public <B> Builder<B, Vector<B>> genericBuilder() {
        return GenericTraversableTemplate.Cclass.genericBuilder(this);
    }

    public final A getElem(int index, int xor) {
        return VectorPointer.Cclass.getElem(this, index, xor);
    }

    public final void gotoNextBlockStart(int index, int xor) {
        VectorPointer.Cclass.gotoNextBlockStart(this, index, xor);
    }

    public final void gotoNextBlockStartWritable(int index, int xor) {
        VectorPointer.Cclass.gotoNextBlockStartWritable(this, index, xor);
    }

    public final void gotoPos(int index, int xor) {
        VectorPointer.Cclass.gotoPos(this, index, xor);
    }

    public final void gotoPosWritable0(int newIndex, int xor) {
        VectorPointer.Cclass.gotoPosWritable0(this, newIndex, xor);
    }

    public final void gotoPosWritable1(int oldIndex, int newIndex, int xor) {
        VectorPointer.Cclass.gotoPosWritable1(this, oldIndex, newIndex, xor);
    }

    public int hashCode() {
        return SeqLike.Cclass.hashCode(this);
    }

    public <B> int indexOf(B elem) {
        return SeqLike.Cclass.indexOf(this, elem);
    }

    public <B> int indexOf(B elem, int from) {
        return SeqLike.Cclass.indexOf(this, elem, from);
    }

    public int indexWhere(Function1<A, Boolean> p, int from) {
        return SeqLike.Cclass.indexWhere(this, p, from);
    }

    public final <U> void initFrom(VectorPointer<U> that) {
        VectorPointer.Cclass.initFrom(this, that);
    }

    public final <U> void initFrom(VectorPointer<U> that, int depth2) {
        VectorPointer.Cclass.initFrom(this, that, depth2);
    }

    public boolean isDefinedAt(int idx) {
        return SeqLike.Cclass.isDefinedAt(this, idx);
    }

    public /* bridge */ /* synthetic */ boolean isDefinedAt(Object x) {
        return isDefinedAt(BoxesRunTime.unboxToInt(x));
    }

    public boolean isEmpty() {
        return IterableLike.Cclass.isEmpty(this);
    }

    public final boolean isTraversableAgain() {
        return TraversableLike.Cclass.isTraversableAgain(this);
    }

    public <B, That> That map(Function1<A, B> f, CanBuildFrom<Vector<A>, B, That> bf) {
        return TraversableLike.Cclass.map(this, f, bf);
    }

    public String mkString() {
        return TraversableOnce.Cclass.mkString(this);
    }

    public String mkString(String sep) {
        return TraversableOnce.Cclass.mkString(this, sep);
    }

    public String mkString(String start, String sep, String end) {
        return TraversableOnce.Cclass.mkString(this, start, sep, end);
    }

    public Builder<A, Vector<A>> newBuilder() {
        return GenericTraversableTemplate.Cclass.newBuilder(this);
    }

    public boolean nonEmpty() {
        return TraversableOnce.Cclass.nonEmpty(this);
    }

    public final Object[] nullSlotAndCopy(Object[] array, int index) {
        return VectorPointer.Cclass.nullSlotAndCopy(this, array, index);
    }

    public int prefixLength(Function1<A, Boolean> p) {
        return SeqLike.Cclass.prefixLength(this, p);
    }

    public <B> B reduceLeft(Function2<B, A, B> op) {
        return TraversableOnce.Cclass.reduceLeft(this, op);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Vector<A>, java.lang.Object] */
    public Vector<A> repr() {
        return TraversableLike.Cclass.repr(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Vector<A>, java.lang.Object] */
    public Vector<A> reverse() {
        return SeqLike.Cclass.reverse(this);
    }

    public <B> boolean sameElements(scala.collection.Iterable<B> that) {
        return IterableLike.Cclass.sameElements(this, that);
    }

    public int segmentLength(Function1<A, Boolean> p, int from) {
        return SeqLike.Cclass.segmentLength(this, p, from);
    }

    public int size() {
        return SeqLike.Cclass.size(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Vector<A>, java.lang.Object] */
    public <B> Vector<A> sortBy(Function1<A, B> f, Ordering<B> ord) {
        return SeqLike.Cclass.sortBy(this, f, ord);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Vector<A>, java.lang.Object] */
    public <B> Vector<A> sorted(Ordering<B> ord) {
        return SeqLike.Cclass.sorted(this, ord);
    }

    public final void stabilize(int index) {
        VectorPointer.Cclass.stabilize(this, index);
    }

    public String stringPrefix() {
        return TraversableLike.Cclass.stringPrefix(this);
    }

    public <B> B sum(Numeric<B> num) {
        return TraversableOnce.Cclass.sum(this, num);
    }

    public scala.collection.IndexedSeq<A> thisCollection() {
        return IndexedSeqLike.Cclass.thisCollection(this);
    }

    public <B> Object toArray(ClassManifest<B> evidence$1) {
        return TraversableOnce.Cclass.toArray(this, evidence$1);
    }

    public <B> Buffer<B> toBuffer() {
        return TraversableOnce.Cclass.toBuffer(this);
    }

    public scala.collection.IndexedSeq<A> toCollection(Vector<A> repr) {
        return IndexedSeqLike.Cclass.toCollection(this, repr);
    }

    public List<A> toList() {
        return TraversableOnce.Cclass.toList(this);
    }

    public <B> Set<B> toSet() {
        return TraversableOnce.Cclass.toSet(this);
    }

    public Stream<A> toStream() {
        return IterableLike.Cclass.toStream(this);
    }

    public String toString() {
        return SeqLike.Cclass.toString(this);
    }

    public <A1, B, That> That zip(scala.collection.Iterable<B> that, CanBuildFrom<Vector<A>, Tuple2<A1, B>, That> bf) {
        return IterableLike.Cclass.zip(this, that, bf);
    }

    public GenericCompanion<Vector> companion() {
        return Vector$.MODULE$;
    }

    public boolean dirty() {
        return this.dirty;
    }

    public int length() {
        return this.endIndex - this.startIndex;
    }

    public VectorIterator<A> iterator() {
        VectorIterator s = new VectorIterator(this.startIndex, this.endIndex);
        s.initFrom(this);
        if (dirty()) {
            s.stabilize(this.focus);
        }
        if (s.depth() > 1) {
            s.gotoPos(this.startIndex, this.startIndex ^ this.focus);
        }
        return s;
    }

    public Iterator<A> reverseIterator() {
        return new Vector$$anon$2(this);
    }

    public A apply(int index) {
        int idx = checkRangeConvert(index);
        return getElem(idx, this.focus ^ idx);
    }

    private int checkRangeConvert(int index) {
        int idx = index + this.startIndex;
        if (index >= 0 && idx < this.endIndex) {
            return idx;
        }
        throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(index).toString());
    }

    public Vector<A> take(int n) {
        if (n <= 0) {
            return Vector$.MODULE$.NIL();
        }
        if (this.startIndex + n < this.endIndex) {
            return dropBack0(this.startIndex + n);
        }
        return this;
    }

    public Vector<A> drop(int n) {
        if (n <= 0) {
            return this;
        }
        if (this.startIndex + n < this.endIndex) {
            return dropFront0(this.startIndex + n);
        }
        return Vector$.MODULE$.NIL();
    }

    public A head() {
        if (IterableLike.Cclass.isEmpty(this)) {
            throw new UnsupportedOperationException("empty.head");
        }
        int checkRangeConvert = checkRangeConvert(0);
        return getElem(checkRangeConvert, this.focus ^ checkRangeConvert);
    }

    public Vector<A> tail() {
        if (!IterableLike.Cclass.isEmpty(this)) {
            return drop(1);
        }
        throw new UnsupportedOperationException("empty.tail");
    }

    public A last() {
        if (IterableLike.Cclass.isEmpty(this)) {
            throw new UnsupportedOperationException("empty.last");
        }
        int checkRangeConvert = checkRangeConvert((this.endIndex - this.startIndex) - 1);
        return getElem(checkRangeConvert, this.focus ^ checkRangeConvert);
    }

    public Vector<A> slice(int from, int until) {
        return take(until).drop(from);
    }

    private void gotoPosWritable(int oldIndex, int newIndex, int xor) {
        if (this.dirty) {
            gotoPosWritable1(oldIndex, newIndex, xor);
            return;
        }
        gotoPosWritable0(newIndex, xor);
        this.dirty = true;
    }

    private void zeroLeft(Object[] array, int index) {
        for (int i = 0; i < index; i++) {
            array[i] = null;
        }
    }

    private void zeroRight(Object[] array, int index) {
        for (int i = index; i < array.length; i++) {
            array[i] = null;
        }
    }

    private Object[] copyLeft(Object[] array, int right) {
        Object[] a2 = new Object[array.length];
        System.arraycopy(array, 0, a2, 0, right);
        return a2;
    }

    private Object[] copyRight(Object[] array, int left) {
        Object[] a2 = new Object[array.length];
        System.arraycopy(array, left, a2, left, a2.length - left);
        return a2;
    }

    private void preClean(int depth2) {
        this.depth = depth2;
        int i = depth2 - 1;
        switch (i) {
            case 0:
                this.display1 = null;
                this.display2 = null;
                this.display3 = null;
                display4_$eq(null);
                display5_$eq(null);
                return;
            case 1:
                this.display2 = null;
                this.display3 = null;
                this.display4 = null;
                display5_$eq(null);
                return;
            case 2:
                this.display3 = null;
                this.display4 = null;
                this.display5 = null;
                return;
            case 3:
                this.display4 = null;
                this.display5 = null;
                return;
            case 4:
                this.display5 = null;
                return;
            case 5:
                return;
            default:
                throw new MatchError(BoxesRunTime.boxToInteger(i));
        }
    }

    private void cleanLeftEdge(int i) {
        if (i < 32) {
            zeroLeft(this.display0, i);
        } else if (i < 1024) {
            zeroLeft(this.display0, i & 31);
            this.display1 = copyRight(this.display1, i >>> 5);
        } else if (i < 32768) {
            zeroLeft(this.display0, i & 31);
            this.display1 = copyRight(this.display1, (i >>> 5) & 31);
            this.display2 = copyRight(this.display2, i >>> 10);
        } else if (i < 1048576) {
            zeroLeft(display0(), i & 31);
            this.display1 = copyRight(this.display1, (i >>> 5) & 31);
            this.display2 = copyRight(this.display2, (i >>> 10) & 31);
            this.display3 = copyRight(this.display3, i >>> 15);
        } else if (i < 33554432) {
            zeroLeft(display0(), i & 31);
            display1_$eq(copyRight(display1(), (i >>> 5) & 31));
            display2_$eq(copyRight(display2(), (i >>> 10) & 31));
            display3_$eq(copyRight(display3(), (i >>> 15) & 31));
            display4_$eq(copyRight(display4(), i >>> 20));
        } else if (i < 1073741824) {
            zeroLeft(display0(), i & 31);
            display1_$eq(copyRight(display1(), (i >>> 5) & 31));
            display2_$eq(copyRight(display2(), (i >>> 10) & 31));
            display3_$eq(copyRight(display3(), (i >>> 15) & 31));
            display4_$eq(copyRight(display4(), (i >>> 20) & 31));
            display5_$eq(copyRight(display5(), i >>> 25));
        } else {
            throw new IllegalArgumentException();
        }
    }

    private void cleanRightEdge(int i) {
        if (i <= 32) {
            zeroRight(this.display0, i);
        } else if (i <= 1024) {
            zeroRight(this.display0, ((i - 1) & 31) + 1);
            this.display1 = copyLeft(this.display1, i >>> 5);
        } else if (i <= 32768) {
            zeroRight(this.display0, ((i - 1) & 31) + 1);
            this.display1 = copyLeft(this.display1, (((i - 1) >>> 5) & 31) + 1);
            this.display2 = copyLeft(this.display2, i >>> 10);
        } else if (i <= 1048576) {
            zeroRight(display0(), ((i - 1) & 31) + 1);
            this.display1 = copyLeft(this.display1, (((i - 1) >>> 5) & 31) + 1);
            this.display2 = copyLeft(this.display2, (((i - 1) >>> 10) & 31) + 1);
            this.display3 = copyLeft(this.display3, i >>> 15);
        } else if (i <= 33554432) {
            zeroRight(display0(), ((i - 1) & 31) + 1);
            display1_$eq(copyLeft(display1(), (((i - 1) >>> 5) & 31) + 1));
            display2_$eq(copyLeft(display2(), (((i - 1) >>> 10) & 31) + 1));
            display3_$eq(copyLeft(display3(), (((i - 1) >>> 15) & 31) + 1));
            display4_$eq(copyLeft(display4(), i >>> 20));
        } else if (i <= 1073741824) {
            zeroRight(display0(), ((i - 1) & 31) + 1);
            display1_$eq(copyLeft(display1(), (((i - 1) >>> 5) & 31) + 1));
            display2_$eq(copyLeft(display2(), (((i - 1) >>> 10) & 31) + 1));
            display3_$eq(copyLeft(display3(), (((i - 1) >>> 15) & 31) + 1));
            display4_$eq(copyLeft(display4(), (((i - 1) >>> 20) & 31) + 1));
            display5_$eq(copyLeft(display5(), i >>> 25));
        } else {
            throw new IllegalArgumentException();
        }
    }

    private int requiredDepth(int i) {
        if (i < 32) {
            return 1;
        }
        if (i < 1024) {
            return 2;
        }
        if (i < 32768) {
            return 3;
        }
        if (i < 1048576) {
            return 4;
        }
        if (i < 33554432) {
            return 5;
        }
        if (i < 1073741824) {
            return 6;
        }
        throw new IllegalArgumentException();
    }

    private Vector<A> dropFront0(int i) {
        boolean z = true ^ true;
        int i2 = i & -32;
        int requiredDepth = requiredDepth((this.endIndex - 1) ^ i);
        int i3 = (((1 << (requiredDepth * 5)) - 1) ^ -1) & i;
        Vector<A> vector = new Vector<>(i - i3, this.endIndex - i3, i2 - i3);
        vector.initFrom(this);
        vector.dirty = this.dirty;
        vector.gotoPosWritable(this.focus, i2, this.focus ^ i2);
        vector.preClean(requiredDepth);
        vector.cleanLeftEdge(i - i3);
        return vector;
    }

    private Vector<A> dropBack0(int i) {
        boolean z = true ^ true;
        int i2 = (i - 1) & -32;
        int requiredDepth = requiredDepth(this.startIndex ^ (i - 1));
        int i3 = this.startIndex & (((1 << (requiredDepth * 5)) - 1) ^ -1);
        Vector<A> vector = new Vector<>(this.startIndex - i3, i - i3, i2 - i3);
        vector.initFrom(this);
        vector.dirty = this.dirty;
        vector.gotoPosWritable(this.focus, i2, this.focus ^ i2);
        vector.preClean(requiredDepth);
        vector.cleanRightEdge(i - i3);
        return vector;
    }
}
