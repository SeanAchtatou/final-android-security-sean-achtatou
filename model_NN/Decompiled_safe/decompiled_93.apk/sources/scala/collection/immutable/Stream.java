package scala.collection.immutable;

import scala.Function0;
import scala.Function1;
import scala.Function2;
import scala.PartialFunction;
import scala.ScalaObject;
import scala.Tuple2;
import scala.collection.Iterable;
import scala.collection.IterableLike;
import scala.collection.Iterator;
import scala.collection.LinearSeq;
import scala.collection.LinearSeqLike;
import scala.collection.LinearSeqOptimized;
import scala.collection.Seq;
import scala.collection.SeqLike;
import scala.collection.Traversable;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericTraversableTemplate;
import scala.collection.generic.TraversableFactory;
import scala.collection.generic.TraversableForwarder;
import scala.collection.immutable.Iterable;
import scala.collection.immutable.LinearSeq;
import scala.collection.immutable.Seq;
import scala.collection.immutable.Traversable;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.Builder;
import scala.collection.mutable.LazyBuilder;
import scala.collection.mutable.StringBuilder;
import scala.math.Numeric;
import scala.math.Ordering;
import scala.reflect.ClassManifest;
import scala.runtime.BoxesRunTime;
import scala.runtime.ObjectRef;

/* compiled from: Stream.scala */
public abstract class Stream<A> implements LinearSeq<A>, GenericTraversableTemplate<A, Stream>, LinearSeqOptimized<A, Stream<A>>, ScalaObject {
    public abstract boolean tailDefined();

    public Stream() {
        TraversableOnce.Cclass.$init$(this);
        TraversableLike.Cclass.$init$(this);
        GenericTraversableTemplate.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        IterableLike.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Function1.Cclass.$init$(this);
        PartialFunction.Cclass.$init$(this);
        SeqLike.Cclass.$init$(this);
        Seq.Cclass.$init$(this);
        Seq.Cclass.$init$(this);
        LinearSeqLike.Cclass.$init$(this);
        LinearSeq.Cclass.$init$(this);
        LinearSeq.Cclass.$init$(this);
        LinearSeqOptimized.Cclass.$init$(this);
    }

    public <B> B $div$colon(B z, Function2<B, A, B> op) {
        return TraversableOnce.Cclass.$div$colon(this, z, op);
    }

    public <C> PartialFunction<Integer, C> andThen(Function1<A, C> k) {
        return PartialFunction.Cclass.andThen(this, k);
    }

    public A apply(int n) {
        return LinearSeqOptimized.Cclass.apply(this, n);
    }

    public /* bridge */ /* synthetic */ Object apply(Object v1) {
        return apply(BoxesRunTime.unboxToInt(v1));
    }

    public int apply$mcII$sp(int v1) {
        return Function1.Cclass.apply$mcII$sp(this, v1);
    }

    public void apply$mcVI$sp(int v1) {
        Function1.Cclass.apply$mcVI$sp(this, v1);
    }

    public void apply$mcVL$sp(long v1) {
        Function1.Cclass.apply$mcVL$sp(this, v1);
    }

    public boolean canEqual(Object that) {
        return IterableLike.Cclass.canEqual(this, that);
    }

    public <A> Function1<A, A> compose(Function1<A, Integer> g) {
        return Function1.Cclass.compose(this, g);
    }

    public boolean contains(Object elem) {
        return SeqLike.Cclass.contains(this, elem);
    }

    public <B> void copyToArray(Object xs, int start) {
        TraversableOnce.Cclass.copyToArray(this, xs, start);
    }

    public <B> void copyToArray(Object xs, int start, int len) {
        IterableLike.Cclass.copyToArray(this, xs, start, len);
    }

    public int count(Function1<A, Boolean> p) {
        return LinearSeqOptimized.Cclass.count(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.LinearSeqOptimized, scala.collection.immutable.Stream<A>] */
    public Stream<A> drop(int n) {
        return LinearSeqOptimized.Cclass.drop(this, n);
    }

    public boolean equals(Object that) {
        return SeqLike.Cclass.equals(this, that);
    }

    public boolean exists(Function1<A, Boolean> p) {
        return LinearSeqOptimized.Cclass.exists(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Stream<A>, java.lang.Object] */
    public Stream<A> filterNot(Function1<A, Boolean> p) {
        return TraversableLike.Cclass.filterNot(this, p);
    }

    public boolean forall(Function1<A, Boolean> p) {
        return LinearSeqOptimized.Cclass.forall(this, p);
    }

    public <B> Builder<B, Stream<B>> genericBuilder() {
        return GenericTraversableTemplate.Cclass.genericBuilder(this);
    }

    public int hashCode() {
        return SeqLike.Cclass.hashCode(this);
    }

    public <B> int indexOf(B elem) {
        return SeqLike.Cclass.indexOf(this, elem);
    }

    public <B> int indexOf(B elem, int from) {
        return SeqLike.Cclass.indexOf(this, elem, from);
    }

    public int indexWhere(Function1<A, Boolean> p, int from) {
        return LinearSeqOptimized.Cclass.indexWhere(this, p, from);
    }

    public boolean isDefinedAt(int x) {
        return LinearSeqOptimized.Cclass.isDefinedAt(this, x);
    }

    public /* bridge */ /* synthetic */ boolean isDefinedAt(Object x) {
        return isDefinedAt(BoxesRunTime.unboxToInt(x));
    }

    public final boolean isTraversableAgain() {
        return TraversableLike.Cclass.isTraversableAgain(this);
    }

    public Iterator<A> iterator() {
        return LinearSeqLike.Cclass.iterator(this);
    }

    public A last() {
        return LinearSeqOptimized.Cclass.last(this);
    }

    public int lengthCompare(int len) {
        return LinearSeqOptimized.Cclass.lengthCompare(this, len);
    }

    public Builder<A, Stream<A>> newBuilder() {
        return GenericTraversableTemplate.Cclass.newBuilder(this);
    }

    public boolean nonEmpty() {
        return TraversableOnce.Cclass.nonEmpty(this);
    }

    public int prefixLength(Function1<A, Boolean> p) {
        return SeqLike.Cclass.prefixLength(this, p);
    }

    public <B> B reduceLeft(Function2<B, A, B> f) {
        return LinearSeqOptimized.Cclass.reduceLeft(this, f);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Stream<A>, java.lang.Object] */
    public Stream<A> repr() {
        return TraversableLike.Cclass.repr(this);
    }

    public Iterator<A> reverseIterator() {
        return SeqLike.Cclass.reverseIterator(this);
    }

    public <B> boolean sameElements(scala.collection.Iterable<B> that) {
        return LinearSeqOptimized.Cclass.sameElements(this, that);
    }

    public final boolean scala$collection$LinearSeqOptimized$$super$sameElements(scala.collection.Iterable that) {
        return IterableLike.Cclass.sameElements(this, that);
    }

    public int segmentLength(Function1<A, Boolean> p, int from) {
        return LinearSeqOptimized.Cclass.segmentLength(this, p, from);
    }

    public int size() {
        return SeqLike.Cclass.size(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Stream<A>, java.lang.Object] */
    public <B> Stream<A> sortBy(Function1<A, B> f, Ordering<B> ord) {
        return SeqLike.Cclass.sortBy(this, f, ord);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Stream<A>, java.lang.Object] */
    public <B> Stream<A> sorted(Ordering<B> ord) {
        return SeqLike.Cclass.sorted(this, ord);
    }

    public <B> B sum(Numeric<B> num) {
        return TraversableOnce.Cclass.sum(this, num);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Stream<A>, java.lang.Object] */
    public Stream<A> tail() {
        return TraversableLike.Cclass.tail(this);
    }

    public scala.collection.LinearSeq<A> thisCollection() {
        return LinearSeqLike.Cclass.thisCollection(this);
    }

    public <B> Object toArray(ClassManifest<B> evidence$1) {
        return TraversableOnce.Cclass.toArray(this, evidence$1);
    }

    public <B> Buffer<B> toBuffer() {
        return TraversableOnce.Cclass.toBuffer(this);
    }

    public scala.collection.LinearSeq<A> toCollection(Stream<A> repr) {
        return LinearSeqLike.Cclass.toCollection(this, repr);
    }

    public /* bridge */ /* synthetic */ scala.collection.Seq toCollection(Object repr) {
        return toCollection((LinearSeqLike) repr);
    }

    public List<A> toList() {
        return TraversableOnce.Cclass.toList(this);
    }

    public <B> Set<B> toSet() {
        return TraversableOnce.Cclass.toSet(this);
    }

    public GenericCompanion<Stream> companion() {
        return Stream$.MODULE$;
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public <B> Stream<B> append(Function0<scala.collection.Traversable<B>> rest$1) {
        return isEmpty() ? rest$1.apply().toStream() : new Cons(head(), new Stream$$anonfun$append$1(this, rest$1));
    }

    public Stream<A> force() {
        for (Stream these = this; !these.isEmpty(); these = (Stream) these.tail()) {
        }
        return this;
    }

    public int length() {
        int len = 0;
        for (Stream left = this; !left.isEmpty(); left = (Stream) left.tail()) {
            len++;
        }
        return len;
    }

    public Stream<A> toStream() {
        return this;
    }

    public <B, That> That $plus$plus(TraversableOnce<B> that$1, CanBuildFrom<Stream<A>, B, That> bf) {
        if (isEmpty()) {
            return that$1.toStream();
        }
        return new Cons(head(), new Stream$$anonfun$$plus$plus$1(this, that$1));
    }

    public final <B, That> That map(Function1<A, B> f$1, CanBuildFrom<Stream<A>, B, That> bf) {
        if (isEmpty()) {
            return Stream$Empty$.MODULE$;
        }
        return new Cons(f$1.apply(head()), new Stream$$anonfun$map$1(this, f$1));
    }

    public Stream<A> filter(Function1<A, Boolean> p) {
        Stream rest = this;
        while (!rest.isEmpty() && !BoxesRunTime.unboxToBoolean(p.apply(rest.head()))) {
            rest = (Stream) rest.tail();
        }
        if (rest.nonEmpty()) {
            return new Cons(rest.head(), new Stream$$anonfun$filteredTail$1(rest, p));
        }
        return Stream$Empty$.MODULE$;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public final <B> void foreach(Function1<A, B> f) {
        while (!this.isEmpty()) {
            f.apply(this.head());
            this = (Stream) this.tail();
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public final <B> B foldLeft(B z, Function2<B, A, B> op) {
        while (!this.isEmpty()) {
            z = op.apply(z, this.head());
            this = (Stream) this.tail();
        }
        return z;
    }

    public final <A1, B, That> That zip(scala.collection.Iterable<B> that$2, CanBuildFrom<Stream<A>, Tuple2<A1, B>, That> bf) {
        if (isEmpty() || that$2.isEmpty()) {
            return Stream$Empty$.MODULE$;
        }
        return new Cons(new Tuple2(head(), that$2.head()), new Stream$$anonfun$zip$1(this, that$2));
    }

    private final void loop$3(String str, Stream stream, StringBuilder stringBuilder, String str2, String str3) {
        Stream stream2 = stream;
        String str4 = str;
        while (!stream2.isEmpty()) {
            stringBuilder.append(str4).append(stream2.head());
            if (stream2.tailDefined()) {
                stream2 = (Stream) stream2.tail();
                str4 = str2;
            } else {
                stringBuilder.append(str2).append("?").append(str3);
                return;
            }
        }
        stringBuilder.append(str3);
    }

    public StringBuilder addString(StringBuilder stringBuilder, String str, String str2, String str3) {
        stringBuilder.append(str);
        loop$3("", this, stringBuilder, str2, str3);
        return stringBuilder;
    }

    public String mkString(String start, String sep, String end) {
        force();
        return TraversableOnce.Cclass.mkString(this, start, sep, end);
    }

    public String mkString(String sep) {
        force();
        return TraversableOnce.Cclass.mkString(this, sep);
    }

    public String mkString() {
        force();
        return TraversableOnce.Cclass.mkString(this);
    }

    public String toString() {
        return TraversableOnce.Cclass.mkString(this, new StringBuilder().append((Object) stringPrefix()).append((Object) "(").toString(), ", ", ")");
    }

    public Stream<A> take(int n$1) {
        if (n$1 <= 0 || isEmpty()) {
            return Stream$Empty$.MODULE$;
        }
        return new Cons(head(), new Stream$$anonfun$take$1(this, n$1));
    }

    public Stream<A> slice(int start, int end) {
        int len = end;
        if (start > 0) {
            len = end - start;
        }
        return ((Stream) drop(start)).take(len);
    }

    /* Debug info: failed to restart local var, previous not found, register: 5 */
    public Stream<A> reverse() {
        ObjectRef result$1 = new ObjectRef(Stream$Empty$.MODULE$);
        for (Stream these = this; !these.isEmpty(); these = (Stream) these.tail()) {
            Stream r = new ConsWrapper(new Stream$$anonfun$1(this, result$1)).$hash$colon$colon(these.head());
            r.tail();
            result$1.elem = r;
        }
        return (Stream) result$1.elem;
    }

    public String stringPrefix() {
        return "Stream";
    }

    /* compiled from: Stream.scala */
    public static class StreamCanBuildFrom<A> extends TraversableFactory<Stream>.GenericCanBuildFrom<A> implements ScalaObject {
        public StreamCanBuildFrom() {
            super(Stream$.MODULE$);
        }
    }

    /* compiled from: Stream.scala */
    public static class StreamBuilder<A> extends LazyBuilder<A, Stream<A>> implements ScalaObject {
        /* Debug info: failed to restart local var, previous not found, register: 5 */
        public Stream<A> result() {
            Stream$Empty$ append;
            Stream stream = TraversableForwarder.Cclass.toStream(parts());
            Stream$StreamBuilder$$anonfun$result$1 stream$StreamBuilder$$anonfun$result$1 = new Stream$StreamBuilder$$anonfun$result$1(this);
            new StreamCanBuildFrom();
            if (stream.isEmpty()) {
                append = Stream$Empty$.MODULE$;
            } else {
                ObjectRef nonEmptyPrefix$10 = new ObjectRef(stream);
                Stream prefix0 = ((TraversableOnce) ((Stream) nonEmptyPrefix$10.elem).head()).toStream().toStream();
                while (!((Stream) nonEmptyPrefix$10.elem).isEmpty() && prefix0.isEmpty()) {
                    nonEmptyPrefix$10.elem = (Stream) ((Stream) nonEmptyPrefix$10.elem).tail();
                    if (!((Stream) nonEmptyPrefix$10.elem).isEmpty()) {
                        prefix0 = ((TraversableOnce) ((Stream) nonEmptyPrefix$10.elem).head()).toStream().toStream();
                    }
                }
                append = ((Stream) nonEmptyPrefix$10.elem).isEmpty() ? Stream$Empty$.MODULE$ : prefix0.append(new Stream$$anonfun$flatMap$1(stream, stream$StreamBuilder$$anonfun$result$1, nonEmptyPrefix$10));
            }
            return append;
        }
    }

    /* compiled from: Stream.scala */
    public static class ConsWrapper<A> implements ScalaObject {
        private final Function0<Stream<A>> tl;

        public ConsWrapper(Function0<Stream<A>> tl2) {
            this.tl = tl2;
        }

        public Stream<A> $hash$colon$colon(A hd) {
            return new Cons(hd, this.tl);
        }
    }

    /* compiled from: Stream.scala */
    public static final class Cons<A> extends Stream<A> implements ScalaObject, ScalaObject {
        public static final long serialVersionUID = -602202424901551803L;
        private final A hd;
        private final Function0<Stream<A>> tl;
        private volatile Stream<A> tlVal;

        public Cons(A hd2, Function0<Stream<A>> tl2) {
            this.hd = hd2;
            this.tl = tl2;
        }

        public boolean isEmpty() {
            return false;
        }

        public A head() {
            return this.hd;
        }

        public boolean tailDefined() {
            return this.tlVal != null;
        }

        public Stream<A> tail() {
            if (!tailDefined()) {
                synchronized (this) {
                    if (!tailDefined()) {
                        this.tlVal = this.tl.apply();
                    }
                }
            }
            return this.tlVal;
        }
    }
}
