package scala.collection.immutable;

import scala.Function1;
import scala.Function2;
import scala.PartialFunction;
import scala.Predef$;
import scala.ScalaObject;
import scala.Tuple2;
import scala.collection.IndexedSeq;
import scala.collection.IndexedSeqLike;
import scala.collection.Iterable;
import scala.collection.IterableLike;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.SeqLike;
import scala.collection.Traversable;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericTraversableTemplate;
import scala.collection.immutable.IndexedSeq;
import scala.collection.immutable.Iterable;
import scala.collection.immutable.Seq;
import scala.collection.immutable.Traversable;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.Builder;
import scala.collection.mutable.StringBuilder;
import scala.math.Numeric;
import scala.math.Ordering;
import scala.reflect.ClassManifest;
import scala.runtime.BoxesRunTime;
import scala.runtime.RichLong;

/* compiled from: Range.scala */
public class Range implements IndexedSeq<Integer>, ScalaObject, IndexedSeq {
    public static final long serialVersionUID = 7618862778670199309L;
    private final int end;
    private final int start;
    private final int step;

    public Range(int start2, int end2, int step2) {
        this.start = start2;
        this.end = end2;
        this.step = step2;
        TraversableOnce.Cclass.$init$(this);
        TraversableLike.Cclass.$init$(this);
        GenericTraversableTemplate.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        IterableLike.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Function1.Cclass.$init$(this);
        PartialFunction.Cclass.$init$(this);
        SeqLike.Cclass.$init$(this);
        Seq.Cclass.$init$(this);
        Seq.Cclass.$init$(this);
        IndexedSeqLike.Cclass.$init$(this);
        IndexedSeq.Cclass.$init$(this);
        IndexedSeq.Cclass.$init$(this);
        Predef$.MODULE$.require(step2 != 0);
    }

    public <B> B $div$colon(B z, Function2<B, Integer, B> op) {
        return TraversableOnce.Cclass.$div$colon(this, z, op);
    }

    public <B, That> That $plus$plus(TraversableOnce<B> that, CanBuildFrom<IndexedSeq<Integer>, B, That> bf) {
        return TraversableLike.Cclass.$plus$plus(this, that, bf);
    }

    public StringBuilder addString(StringBuilder b, String start2, String sep, String end2) {
        return TraversableOnce.Cclass.addString(this, b, start2, sep, end2);
    }

    public <C> PartialFunction<Integer, C> andThen(Function1<Integer, C> k) {
        return PartialFunction.Cclass.andThen(this, k);
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return BoxesRunTime.boxToInteger(apply(BoxesRunTime.unboxToInt(v1)));
    }

    public void apply$mcVI$sp(int v1) {
        Function1.Cclass.apply$mcVI$sp(this, v1);
    }

    public void apply$mcVL$sp(long v1) {
        Function1.Cclass.apply$mcVL$sp(this, v1);
    }

    public boolean canEqual(Object that) {
        return IterableLike.Cclass.canEqual(this, that);
    }

    public GenericCompanion<IndexedSeq> companion() {
        return IndexedSeq.Cclass.companion(this);
    }

    public <A> Function1<A, Integer> compose(Function1<A, Integer> g) {
        return Function1.Cclass.compose(this, g);
    }

    public boolean contains(Object elem) {
        return SeqLike.Cclass.contains(this, elem);
    }

    public <B> void copyToArray(Object xs, int start2) {
        TraversableOnce.Cclass.copyToArray(this, xs, start2);
    }

    public <B> void copyToArray(Object xs, int start2, int len) {
        IterableLike.Cclass.copyToArray(this, xs, start2, len);
    }

    public int end() {
        return this.end;
    }

    public boolean exists(Function1<Integer, Boolean> p) {
        return IterableLike.Cclass.exists(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.IndexedSeq<java.lang.Integer>] */
    public IndexedSeq<Integer> filter(Function1<Integer, Boolean> p) {
        return TraversableLike.Cclass.filter(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.IndexedSeq<java.lang.Integer>] */
    public IndexedSeq<Integer> filterNot(Function1<Integer, Boolean> p) {
        return TraversableLike.Cclass.filterNot(this, p);
    }

    public <B> B foldLeft(B z, Function2<B, Integer, B> op) {
        return TraversableOnce.Cclass.foldLeft(this, z, op);
    }

    public boolean forall(Function1<Integer, Boolean> p) {
        return IterableLike.Cclass.forall(this, p);
    }

    public <B> Builder<B, IndexedSeq<B>> genericBuilder() {
        return GenericTraversableTemplate.Cclass.genericBuilder(this);
    }

    public int hashCode() {
        return SeqLike.Cclass.hashCode(this);
    }

    public Object head() {
        return IterableLike.Cclass.head(this);
    }

    public <B> int indexOf(B elem) {
        return SeqLike.Cclass.indexOf(this, elem);
    }

    public <B> int indexOf(B elem, int from) {
        return SeqLike.Cclass.indexOf(this, elem, from);
    }

    public int indexWhere(Function1<Integer, Boolean> p, int from) {
        return SeqLike.Cclass.indexWhere(this, p, from);
    }

    public boolean isDefinedAt(int idx) {
        return SeqLike.Cclass.isDefinedAt(this, idx);
    }

    public /* bridge */ /* synthetic */ boolean isDefinedAt(Object x) {
        return isDefinedAt(BoxesRunTime.unboxToInt(x));
    }

    public final boolean isTraversableAgain() {
        return TraversableLike.Cclass.isTraversableAgain(this);
    }

    public Iterator<Integer> iterator() {
        return IndexedSeqLike.Cclass.iterator(this);
    }

    public <B, That> That map(Function1<Integer, B> f, CanBuildFrom<IndexedSeq<Integer>, B, That> bf) {
        return TraversableLike.Cclass.map(this, f, bf);
    }

    public String mkString() {
        return TraversableOnce.Cclass.mkString(this);
    }

    public String mkString(String sep) {
        return TraversableOnce.Cclass.mkString(this, sep);
    }

    public String mkString(String start2, String sep, String end2) {
        return TraversableOnce.Cclass.mkString(this, start2, sep, end2);
    }

    public Builder<Integer, IndexedSeq<Integer>> newBuilder() {
        return GenericTraversableTemplate.Cclass.newBuilder(this);
    }

    public boolean nonEmpty() {
        return TraversableOnce.Cclass.nonEmpty(this);
    }

    public int prefixLength(Function1<Integer, Boolean> p) {
        return SeqLike.Cclass.prefixLength(this, p);
    }

    public <B> B reduceLeft(Function2<B, Integer, B> op) {
        return TraversableOnce.Cclass.reduceLeft(this, op);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.IndexedSeq<java.lang.Integer>] */
    public IndexedSeq<Integer> repr() {
        return TraversableLike.Cclass.repr(this);
    }

    public Iterator<Integer> reverseIterator() {
        return SeqLike.Cclass.reverseIterator(this);
    }

    public <B> boolean sameElements(scala.collection.Iterable<B> that) {
        return IterableLike.Cclass.sameElements(this, that);
    }

    public int segmentLength(Function1<Integer, Boolean> p, int from) {
        return SeqLike.Cclass.segmentLength(this, p, from);
    }

    public int size() {
        return SeqLike.Cclass.size(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.IndexedSeq<java.lang.Integer>] */
    public <B> IndexedSeq<Integer> sortBy(Function1<Integer, B> f, Ordering<B> ord) {
        return SeqLike.Cclass.sortBy(this, f, ord);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.IndexedSeq<java.lang.Integer>] */
    public <B> IndexedSeq<Integer> sorted(Ordering<B> ord) {
        return SeqLike.Cclass.sorted(this, ord);
    }

    public int start() {
        return this.start;
    }

    public int step() {
        return this.step;
    }

    public String stringPrefix() {
        return TraversableLike.Cclass.stringPrefix(this);
    }

    public <B> B sum(Numeric<B> num) {
        return TraversableOnce.Cclass.sum(this, num);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.IndexedSeq<java.lang.Integer>] */
    public IndexedSeq<Integer> tail() {
        return TraversableLike.Cclass.tail(this);
    }

    public scala.collection.IndexedSeq<Integer> thisCollection() {
        return IndexedSeqLike.Cclass.thisCollection(this);
    }

    public <B> Object toArray(ClassManifest<B> evidence$1) {
        return TraversableOnce.Cclass.toArray(this, evidence$1);
    }

    public <B> Buffer<B> toBuffer() {
        return TraversableOnce.Cclass.toBuffer(this);
    }

    public scala.collection.IndexedSeq<Integer> toCollection(IndexedSeq<Integer> repr) {
        return IndexedSeqLike.Cclass.toCollection(this, repr);
    }

    public List<Integer> toList() {
        return TraversableOnce.Cclass.toList(this);
    }

    public <B> Set<B> toSet() {
        return TraversableOnce.Cclass.toSet(this);
    }

    public Stream<Integer> toStream() {
        return IterableLike.Cclass.toStream(this);
    }

    public <A1, B, That> That zip(scala.collection.Iterable<B> that, CanBuildFrom<IndexedSeq<Integer>, Tuple2<A1, B>, That> bf) {
        return IterableLike.Cclass.zip(this, that, bf);
    }

    public Range copy(int start2, int end2, int step2) {
        return new Range(start2, end2, step2);
    }

    public Range by(int step2) {
        return copy(start(), end(), step2);
    }

    public <U> void foreach(Function1<Integer, U> f) {
        if (fullLength() > 0) {
            int last = last();
            int i = start();
            while (i != last) {
                f.apply(BoxesRunTime.boxToInteger(i));
                i += step();
            }
            f.apply(BoxesRunTime.boxToInteger(i));
        }
    }

    public void foreach$mVc$sp(Function1<Integer, Object> f) {
        if (fullLength() > 0) {
            int last = last();
            int i = start();
            while (i != last) {
                f.apply$mcVI$sp(i);
                i += step();
            }
            f.apply$mcVI$sp(i);
        }
    }

    public int last() {
        if (step() == 1 || step() == -1) {
            return end() - step();
        }
        long size = ((long) end()) - ((long) start());
        int inclusiveLast = (int) (((size / ((long) step())) * ((long) step())) + ((long) start()));
        return size % ((long) step()) == 0 ? inclusiveLast - step() : inclusiveLast;
    }

    public int length() {
        return (int) fullLength();
    }

    public long fullLength() {
        if ((end() > start()) != (step() > 0) || start() == end()) {
            return 0;
        }
        return ((((long) last()) - ((long) start())) / ((long) step())) + 1;
    }

    public final boolean isEmpty() {
        return length() == 0;
    }

    public final int apply(int idx) {
        return apply$mcII$sp(idx);
    }

    public int apply$mcII$sp(int v1) {
        if (v1 >= 0 && v1 < length()) {
            return start() + (step() * v1);
        }
        throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(v1).toString());
    }

    private int locationAfterN(int n) {
        if (n <= 0) {
            return start();
        }
        if (step() > 0) {
            return (int) new RichLong(((long) start()) + (((long) step()) * ((long) n))).min((long) last());
        }
        return (int) new RichLong(((long) start()) + (((long) step()) * ((long) n))).max((long) last());
    }

    public final Range take(int n) {
        if (n <= 0 || length() <= 0) {
            return new Range(start(), start(), step());
        }
        return new Range(start(), locationAfterN(n - 1), step()).inclusive();
    }

    public final Range drop(int n) {
        if (n < length()) {
            return copy(locationAfterN(n), end(), step());
        }
        if (step() > 0) {
            return copy(end() + 1, end(), step());
        }
        return copy(end() - 1, end(), step());
    }

    public final Range slice(int from, int until) {
        return drop(from).take(until - from);
    }

    public final Range reverse() {
        return length() > 0 ? new Inclusive(last(), start(), -step()) : this;
    }

    public Inclusive inclusive() {
        return new Inclusive(start(), end(), step());
    }

    public boolean equals(Object other) {
        if (!(other instanceof Range)) {
            return SeqLike.Cclass.equals(this, other);
        }
        Range range = (Range) other;
        if (length() == range.length() && (length() == 0 || (start() == range.start() && (length() == 1 || step() == range.step())))) {
            return true;
        }
        return false;
    }

    public String toString() {
        return take(Range$.MODULE$.MAX_PRINT()).mkString("Range(", ", ", length() > Range$.MODULE$.MAX_PRINT() ? ", ... )" : ")");
    }

    /* compiled from: Range.scala */
    public static class Inclusive extends Range implements ScalaObject {
        public Inclusive(int start, int end, int step) {
            super(start, end, step);
        }

        public Range copy(int start, int end, int step) {
            return new Inclusive(start, end, step);
        }

        public int last() {
            if (Range.super.step() == 1 || Range.super.step() == -1) {
                return Range.super.end();
            }
            return (int) ((((((long) Range.super.end()) - ((long) Range.super.start())) / ((long) Range.super.step())) * ((long) Range.super.step())) + ((long) Range.super.start()));
        }

        public long fullLength() {
            if ((Range.super.end() > Range.super.start()) == (Range.super.step() > 0) || Range.super.start() == Range.super.end()) {
                return ((((long) last()) - ((long) Range.super.start())) / ((long) Range.super.step())) + 1;
            }
            return 0;
        }
    }

    /* compiled from: Range.scala */
    public interface ByOne extends ScalaObject {
        <U> void foreach(Function1<Integer, U> function1);

        void foreach$mVc$sp(Function1<Integer, Object> function1);

        /* renamed from: scala.collection.immutable.Range$ByOne$class  reason: invalid class name */
        /* compiled from: Range.scala */
        public abstract class Cclass {
            public static void $init$(ByOne $this) {
            }

            public static final void foreach(ByOne $this, Function1 f) {
                if (((Range) $this).length() > 0) {
                    int last = ((Range) $this).last();
                    int i = ((Range) $this).start();
                    while (i != last) {
                        f.apply(BoxesRunTime.boxToInteger(i));
                        i++;
                    }
                    f.apply(BoxesRunTime.boxToInteger(i));
                }
            }

            public static final void foreach$mVc$sp(ByOne $this, Function1 f) {
                if (((Range) $this).length() > 0) {
                    int last = ((Range) $this).last();
                    int i = ((Range) $this).start();
                    while (i != last) {
                        f.apply$mcVI$sp(i);
                        i++;
                    }
                    f.apply$mcVI$sp(i);
                }
            }
        }
    }
}
