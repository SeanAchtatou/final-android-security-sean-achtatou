package scala.collection.immutable;

import scala.ScalaObject;
import scala.collection.generic.SeqFactory;
import scala.collection.generic.TraversableFactory;
import scala.collection.mutable.Builder;
import scala.runtime.Nothing$;

/* compiled from: Vector.scala */
public final class Vector$ extends SeqFactory<Vector> implements ScalaObject, ScalaObject {
    public static final Vector$ MODULE$ = null;
    private final TraversableFactory<Vector>.GenericCanBuildFrom<Nothing$> BF = new Vector$$anon$1();
    private final Vector<Nothing$> NIL = new Vector<>(0, 0, 0);

    static {
        new Vector$();
    }

    private Vector$() {
        MODULE$ = this;
    }

    public <A> Builder<A, Vector<A>> newBuilder() {
        return new VectorBuilder();
    }

    public Vector<Nothing$> NIL() {
        return this.NIL;
    }

    public <A> Vector<A> empty() {
        return NIL();
    }
}
