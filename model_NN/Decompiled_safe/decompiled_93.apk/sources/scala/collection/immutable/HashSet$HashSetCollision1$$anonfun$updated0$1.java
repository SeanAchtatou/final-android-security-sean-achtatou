package scala.collection.immutable;

import java.io.Serializable;
import scala.collection.immutable.HashSet;
import scala.runtime.AbstractFunction1;
import scala.runtime.ObjectRef;

/* compiled from: HashSet.scala */
public final class HashSet$HashSetCollision1$$anonfun$updated0$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ HashSet.HashSetCollision1 $outer;
    public final /* synthetic */ int level$1;
    public final /* synthetic */ ObjectRef m$1;

    public HashSet$HashSetCollision1$$anonfun$updated0$1(HashSet.HashSetCollision1 $outer2, int i, ObjectRef objectRef) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.level$1 = i;
        this.m$1 = objectRef;
    }

    public final void apply(A k) {
        this.m$1.elem = ((HashSet) this.m$1.elem).updated0(k, this.$outer.hash(), this.level$1);
    }
}
