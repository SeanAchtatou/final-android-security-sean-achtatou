package scala.collection.immutable;

import scala.ScalaObject;
import scala.collection.TraversableLike;
import scala.collection.generic.GenericTraversableTemplate;

/* compiled from: Traversable.scala */
public interface Traversable<A> extends scala.collection.Traversable<A>, GenericTraversableTemplate<A, Traversable>, TraversableLike<A, Traversable<A>>, ScalaObject {

    /* renamed from: scala.collection.immutable.Traversable$class  reason: invalid class name */
    /* compiled from: Traversable.scala */
    public abstract class Cclass {
        public static void $init$(Traversable $this) {
        }
    }
}
