package scala.collection.immutable;

import scala.ScalaObject;
import scala.collection.generic.ImmutableMapFactory;

/* compiled from: HashMap.scala */
public final class HashMap$ extends ImmutableMapFactory<HashMap> implements ScalaObject, ScalaObject {
    public static final HashMap$ MODULE$ = null;

    static {
        new HashMap$();
    }

    private HashMap$() {
        MODULE$ = this;
    }

    public <A, B> HashMap<A, B> empty() {
        return HashMap$EmptyHashMap$.MODULE$;
    }
}
