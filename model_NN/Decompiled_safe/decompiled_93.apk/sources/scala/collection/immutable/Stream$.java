package scala.collection.immutable;

import scala.Function0;
import scala.ScalaObject;
import scala.collection.Seq;
import scala.collection.generic.SeqFactory;
import scala.collection.immutable.Stream;
import scala.collection.mutable.Builder;

/* compiled from: Stream.scala */
public final class Stream$ extends SeqFactory<Stream> implements ScalaObject {
    public static final Stream$ MODULE$ = null;

    static {
        new Stream$();
    }

    private Stream$() {
        MODULE$ = this;
    }

    public <A> Builder<A, Stream<A>> newBuilder() {
        return new Stream.StreamBuilder();
    }

    public <A> Stream<A> empty() {
        return Stream$Empty$.MODULE$;
    }

    public <A> Stream<A> apply(Seq<A> xs) {
        return xs.toStream();
    }

    public <A> Stream<A> fill(int n$2, Function0<A> elem$2) {
        return n$2 <= 0 ? Stream$Empty$.MODULE$ : new Stream.Cons(elem$2.apply(), new Stream$$anonfun$fill$1(n$2, elem$2));
    }
}
