package scala.collection.immutable;

import java.io.Serializable;
import scala.runtime.AbstractFunction0;

/* compiled from: List.scala */
public final class List$$anonfun$toStream$1 extends AbstractFunction0 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ List $outer;

    public List$$anonfun$toStream$1(List<A> $outer2) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
    }

    public final Stream<A> apply() {
        return ((List) this.$outer.tail()).toStream();
    }
}
