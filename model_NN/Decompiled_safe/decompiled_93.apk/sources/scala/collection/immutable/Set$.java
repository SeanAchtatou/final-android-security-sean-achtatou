package scala.collection.immutable;

import scala.ScalaObject;
import scala.collection.generic.ImmutableSetFactory;

/* compiled from: Set.scala */
public final class Set$ extends ImmutableSetFactory<Set> implements ScalaObject {
    public static final Set$ MODULE$ = null;
    private final int hashSeed = "Set".hashCode();

    static {
        new Set$();
    }

    private Set$() {
        MODULE$ = this;
    }

    public <A> Set<A> empty() {
        return Set$EmptySet$.MODULE$;
    }
}
