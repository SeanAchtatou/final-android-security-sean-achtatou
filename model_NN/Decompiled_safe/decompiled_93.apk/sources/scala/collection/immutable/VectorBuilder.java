package scala.collection.immutable;

import scala.Function1;
import scala.ScalaObject;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.Growable;
import scala.collection.immutable.VectorPointer;
import scala.collection.mutable.Builder;

/* compiled from: Vector.scala */
public final class VectorBuilder<A> implements Builder<A, Vector<A>>, VectorPointer<A>, ScalaObject {
    private int blockIndex = 0;
    private int depth;
    private Object[] display0;
    private Object[] display1;
    private Object[] display2;
    private Object[] display3;
    private Object[] display4;
    private Object[] display5;
    private int lo = 0;

    public VectorBuilder() {
        Growable.Cclass.$init$(this);
        Builder.Cclass.$init$(this);
        VectorPointer.Cclass.$init$(this);
        display0_$eq(new Object[32]);
        depth_$eq(1);
    }

    public Growable<A> $plus$plus$eq(TraversableOnce<A> xs) {
        return Growable.Cclass.$plus$plus$eq(this, xs);
    }

    public final Object[] copyOf(Object[] a) {
        return VectorPointer.Cclass.copyOf(this, a);
    }

    public int depth() {
        return this.depth;
    }

    public void depth_$eq(int i) {
        this.depth = i;
    }

    public Object[] display0() {
        return this.display0;
    }

    public void display0_$eq(Object[] objArr) {
        this.display0 = objArr;
    }

    public Object[] display1() {
        return this.display1;
    }

    public void display1_$eq(Object[] objArr) {
        this.display1 = objArr;
    }

    public Object[] display2() {
        return this.display2;
    }

    public void display2_$eq(Object[] objArr) {
        this.display2 = objArr;
    }

    public Object[] display3() {
        return this.display3;
    }

    public void display3_$eq(Object[] objArr) {
        this.display3 = objArr;
    }

    public Object[] display4() {
        return this.display4;
    }

    public void display4_$eq(Object[] objArr) {
        this.display4 = objArr;
    }

    public Object[] display5() {
        return this.display5;
    }

    public void display5_$eq(Object[] objArr) {
        this.display5 = objArr;
    }

    public final A getElem(int index, int xor) {
        return VectorPointer.Cclass.getElem(this, index, xor);
    }

    public final void gotoNextBlockStart(int index, int xor) {
        VectorPointer.Cclass.gotoNextBlockStart(this, index, xor);
    }

    public final void gotoNextBlockStartWritable(int index, int xor) {
        VectorPointer.Cclass.gotoNextBlockStartWritable(this, index, xor);
    }

    public final void gotoPos(int index, int xor) {
        VectorPointer.Cclass.gotoPos(this, index, xor);
    }

    public final void gotoPosWritable0(int newIndex, int xor) {
        VectorPointer.Cclass.gotoPosWritable0(this, newIndex, xor);
    }

    public final void gotoPosWritable1(int oldIndex, int newIndex, int xor) {
        VectorPointer.Cclass.gotoPosWritable1(this, oldIndex, newIndex, xor);
    }

    public final <U> void initFrom(VectorPointer<U> that) {
        VectorPointer.Cclass.initFrom(this, that);
    }

    public final <U> void initFrom(VectorPointer<U> that, int depth2) {
        VectorPointer.Cclass.initFrom(this, that, depth2);
    }

    public <NewTo> Builder<A, NewTo> mapResult(Function1<Vector<A>, NewTo> f) {
        return Builder.Cclass.mapResult(this, f);
    }

    public final Object[] nullSlotAndCopy(Object[] array, int index) {
        return VectorPointer.Cclass.nullSlotAndCopy(this, array, index);
    }

    public void sizeHint(int size) {
        Builder.Cclass.sizeHint(this, size);
    }

    public void sizeHint(TraversableLike<?, ?> coll, int delta) {
        Builder.Cclass.sizeHint(this, coll, delta);
    }

    public /* synthetic */ int sizeHint$default$2() {
        return Builder.Cclass.sizeHint$default$2(this);
    }

    public void sizeHintBounded(int size, TraversableLike<?, ?> boundingColl) {
        Builder.Cclass.sizeHintBounded(this, size, boundingColl);
    }

    public final void stabilize(int index) {
        VectorPointer.Cclass.stabilize(this, index);
    }

    private int blockIndex() {
        return this.blockIndex;
    }

    private void blockIndex_$eq(int i) {
        this.blockIndex = i;
    }

    private int lo() {
        return this.lo;
    }

    private void lo_$eq(int i) {
        this.lo = i;
    }

    public VectorBuilder<A> $plus$eq(A elem) {
        if (lo() >= this.display0.length) {
            int newBlockIndex = blockIndex() + 32;
            gotoNextBlockStartWritable(newBlockIndex, blockIndex() ^ newBlockIndex);
            blockIndex_$eq(newBlockIndex);
            lo_$eq(0);
        }
        this.display0[lo()] = elem;
        lo_$eq(lo() + 1);
        return this;
    }

    public Vector<A> result() {
        int size = blockIndex() + lo();
        if (size == 0) {
            return Vector$.MODULE$.NIL();
        }
        Vector s = new Vector(0, size, 0);
        s.initFrom(this);
        if (this.depth > 1) {
            s.gotoPos(0, size - 1);
        }
        return s;
    }
}
