package scala.collection.immutable;

import scala.Array$;
import scala.Function0;
import scala.Function1;
import scala.Function2;
import scala.None$;
import scala.Option;
import scala.PartialFunction;
import scala.Predef$;
import scala.ScalaObject;
import scala.Tuple2;
import scala.collection.Iterable;
import scala.collection.IterableLike;
import scala.collection.Iterator;
import scala.collection.Iterator$;
import scala.collection.Map;
import scala.collection.MapLike;
import scala.collection.Seq;
import scala.collection.Traversable;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericTraversableTemplate;
import scala.collection.generic.MapFactory;
import scala.collection.generic.Subtractable;
import scala.collection.immutable.Iterable;
import scala.collection.immutable.Map;
import scala.collection.immutable.MapLike;
import scala.collection.immutable.Traversable;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.Builder;
import scala.collection.mutable.StringBuilder;
import scala.math.Numeric;
import scala.reflect.ClassManifest;
import scala.runtime.BoxesRunTime;
import scala.runtime.ObjectRef;

/* compiled from: HashMap.scala */
public class HashMap<A, B> implements Map<A, B>, MapLike<A, B, HashMap<A, B>>, ScalaObject, MapLike {
    public static final long serialVersionUID = 2;

    public HashMap() {
        TraversableOnce.Cclass.$init$(this);
        TraversableLike.Cclass.$init$(this);
        GenericTraversableTemplate.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        IterableLike.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Function1.Cclass.$init$(this);
        PartialFunction.Cclass.$init$(this);
        Subtractable.Cclass.$init$(this);
        MapLike.Cclass.$init$(this);
        Map.Cclass.$init$(this);
        MapLike.Cclass.$init$(this);
        Map.Cclass.$init$(this);
    }

    public <B> B $div$colon(B z, Function2<B, Tuple2<A, B>, B> op) {
        return TraversableOnce.Cclass.$div$colon(this, z, op);
    }

    public <B, That> That $plus$plus(TraversableOnce<B> that, CanBuildFrom<HashMap<A, B>, B, That> bf) {
        return TraversableLike.Cclass.$plus$plus(this, that, bf);
    }

    public StringBuilder addString(StringBuilder b, String start, String sep, String end) {
        return MapLike.Cclass.addString(this, b, start, sep, end);
    }

    public <C> PartialFunction<A, C> andThen(Function1<B, C> k) {
        return PartialFunction.Cclass.andThen(this, k);
    }

    public B apply(A key) {
        return MapLike.Cclass.apply(this, key);
    }

    public int apply$mcII$sp(int v1) {
        return Function1.Cclass.apply$mcII$sp(this, v1);
    }

    public void apply$mcVI$sp(int v1) {
        Function1.Cclass.apply$mcVI$sp(this, v1);
    }

    public void apply$mcVL$sp(long v1) {
        Function1.Cclass.apply$mcVL$sp(this, v1);
    }

    public boolean canEqual(Object that) {
        return IterableLike.Cclass.canEqual(this, that);
    }

    public GenericCompanion<Iterable> companion() {
        return Iterable.Cclass.companion(this);
    }

    public <A> Function1<A, B> compose(Function1<A, A> g) {
        return Function1.Cclass.compose(this, g);
    }

    public boolean contains(A key) {
        return MapLike.Cclass.contains(this, key);
    }

    public <B> void copyToArray(Object xs, int start) {
        TraversableOnce.Cclass.copyToArray(this, xs, start);
    }

    public <B> void copyToArray(Object xs, int start, int len) {
        IterableLike.Cclass.copyToArray(this, xs, start, len);
    }

    /* renamed from: default  reason: not valid java name */
    public B m4default(A key) {
        return MapLike.Cclass.m3default(this, key);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.HashMap<A, B>] */
    public HashMap<A, B> drop(int n) {
        return TraversableLike.Cclass.drop(this, n);
    }

    public boolean equals(Object that) {
        return MapLike.Cclass.equals(this, that);
    }

    public boolean exists(Function1<Tuple2<A, B>, Boolean> p) {
        return IterableLike.Cclass.exists(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.HashMap<A, B>] */
    public HashMap<A, B> filter(Function1<Tuple2<A, B>, Boolean> p) {
        return TraversableLike.Cclass.filter(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.Map, scala.collection.immutable.HashMap<A, B>] */
    public HashMap<A, B> filterNot(Function1<Tuple2<A, B>, Boolean> p) {
        return MapLike.Cclass.filterNot(this, p);
    }

    public <B> B foldLeft(B z, Function2<B, Tuple2<A, B>, B> op) {
        return TraversableOnce.Cclass.foldLeft(this, z, op);
    }

    public boolean forall(Function1<Tuple2<A, B>, Boolean> p) {
        return IterableLike.Cclass.forall(this, p);
    }

    public <B> Builder<B, Iterable<B>> genericBuilder() {
        return GenericTraversableTemplate.Cclass.genericBuilder(this);
    }

    public <B1> B1 getOrElse(A key, Function0<B1> function0) {
        return MapLike.Cclass.getOrElse(this, key, function0);
    }

    public int hashCode() {
        return MapLike.Cclass.hashCode(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.Tuple2<A, B>] */
    public Tuple2<A, B> head() {
        return IterableLike.Cclass.head(this);
    }

    public boolean isDefinedAt(A key) {
        return MapLike.Cclass.isDefinedAt(this, key);
    }

    public boolean isEmpty() {
        return MapLike.Cclass.isEmpty(this);
    }

    public final boolean isTraversableAgain() {
        return TraversableLike.Cclass.isTraversableAgain(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.Tuple2<A, B>] */
    public Tuple2<A, B> last() {
        return TraversableLike.Cclass.last(this);
    }

    public <B, That> That map(Function1<Tuple2<A, B>, B> f, CanBuildFrom<HashMap<A, B>, B, That> bf) {
        return TraversableLike.Cclass.map(this, f, bf);
    }

    public String mkString() {
        return TraversableOnce.Cclass.mkString(this);
    }

    public String mkString(String sep) {
        return TraversableOnce.Cclass.mkString(this, sep);
    }

    public String mkString(String start, String sep, String end) {
        return TraversableOnce.Cclass.mkString(this, start, sep, end);
    }

    public Builder<Tuple2<A, B>, HashMap<A, B>> newBuilder() {
        return MapLike.Cclass.newBuilder(this);
    }

    public boolean nonEmpty() {
        return TraversableOnce.Cclass.nonEmpty(this);
    }

    public <B> B reduceLeft(Function2<B, Tuple2<A, B>, B> op) {
        return TraversableOnce.Cclass.reduceLeft(this, op);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.HashMap<A, B>] */
    public HashMap<A, B> repr() {
        return TraversableLike.Cclass.repr(this);
    }

    public <B> boolean sameElements(scala.collection.Iterable<B> that) {
        return IterableLike.Cclass.sameElements(this, that);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.HashMap<A, B>] */
    public HashMap<A, B> slice(int from, int until) {
        return IterableLike.Cclass.slice(this, from, until);
    }

    public String stringPrefix() {
        return MapLike.Cclass.stringPrefix(this);
    }

    public <B> B sum(Numeric<B> num) {
        return TraversableOnce.Cclass.sum(this, num);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.immutable.HashMap<A, B>] */
    public HashMap<A, B> tail() {
        return TraversableLike.Cclass.tail(this);
    }

    public scala.collection.Iterable<Tuple2<A, B>> thisCollection() {
        return IterableLike.Cclass.thisCollection(this);
    }

    public <B> Object toArray(ClassManifest<B> evidence$1) {
        return TraversableOnce.Cclass.toArray(this, evidence$1);
    }

    public <B> Buffer<B> toBuffer() {
        return TraversableOnce.Cclass.toBuffer(this);
    }

    public List<Tuple2<A, B>> toList() {
        return TraversableOnce.Cclass.toList(this);
    }

    public <B> Set<B> toSet() {
        return TraversableOnce.Cclass.toSet(this);
    }

    public Stream<Tuple2<A, B>> toStream() {
        return IterableLike.Cclass.toStream(this);
    }

    public String toString() {
        return MapLike.Cclass.toString(this);
    }

    public <A1, B, That> That zip(scala.collection.Iterable<B> that, CanBuildFrom<HashMap<A, B>, Tuple2<A1, B>, That> bf) {
        return IterableLike.Cclass.zip(this, that, bf);
    }

    public int size() {
        return 0;
    }

    public HashMap<A, B> empty() {
        return HashMap$EmptyHashMap$.MODULE$;
    }

    public Iterator<Tuple2<A, B>> iterator() {
        return Iterator$.MODULE$.empty();
    }

    public <U> void foreach(Function1<Tuple2<A, B>, U> f) {
    }

    public Option<B> get(A key) {
        return get0(key, computeHash(key), 0);
    }

    public <B1> HashMap<A, B1> $plus(Tuple2<A, B1> kv) {
        return updated0(kv._1(), computeHash(kv._1()), 0, kv._2(), kv);
    }

    public <B1> HashMap<A, B1> $plus(Tuple2<A, B1> elem1, Tuple2<A, B1> elem2, Seq<Tuple2<A, B1>> elems) {
        return (HashMap) $plus((Tuple2) elem1).$plus((Tuple2) elem2).$plus$plus(elems, new MapFactory.MapCanBuildFrom(HashMap$.MODULE$));
    }

    public HashMap<A, B> $minus(A key) {
        return removed0(key, computeHash(key), 0);
    }

    public int elemHashCode(A key) {
        if (key == null) {
            return 0;
        }
        return key instanceof Number ? BoxesRunTime.hashFromNumber((Number) key) : key.hashCode();
    }

    public int computeHash(A a) {
        int elemHashCode = elemHashCode(a);
        int i = elemHashCode + ((elemHashCode << 9) ^ -1);
        int i2 = i ^ (i >>> 14);
        int i3 = i2 + (i2 << 4);
        return i3 ^ (i3 >>> 10);
    }

    public Option<B> get0(A key, int hash, int level) {
        return None$.MODULE$;
    }

    public <B1> HashMap<A, B1> updated0(A key, int hash, int level, B1 value, Tuple2<A, B1> kv) {
        return new HashMap1(key, hash, value, kv);
    }

    public HashMap<A, B> removed0(A key, int hash, int level) {
        return this;
    }

    /* compiled from: HashMap.scala */
    public static class HashMap1<A, B> extends HashMap<A, B> implements ScalaObject {
        private int hash;
        private A key;
        private Tuple2<A, B> kv;
        private B value;

        public HashMap1(A key2, int hash2, B value2, Tuple2<A, B> kv2) {
            this.key = key2;
            this.hash = hash2;
            this.value = value2;
            this.kv = kv2;
        }

        private A key() {
            return this.key;
        }

        private Tuple2<A, B> kv() {
            return this.kv;
        }

        private void kv_$eq(Tuple2<A, B> tuple2) {
            this.kv = tuple2;
        }

        private B value() {
            return this.value;
        }

        public int hash() {
            return this.hash;
        }

        public int size() {
            return 1;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public scala.Option<B> get0(A r3, int r4, int r5) {
            /*
                r2 = this;
                int r0 = r2.hash()
                if (r4 != r0) goto L_0x0038
                java.lang.Object r0 = r2.key()
                if (r3 != r0) goto L_0x0019
                r0 = 1
            L_0x000d:
                if (r0 == 0) goto L_0x0038
                scala.Some r0 = new scala.Some
                java.lang.Object r1 = r2.value()
                r0.<init>(r1)
            L_0x0018:
                return r0
            L_0x0019:
                if (r3 != 0) goto L_0x001d
                r0 = 0
                goto L_0x000d
            L_0x001d:
                boolean r1 = r3 instanceof java.lang.Number
                if (r1 == 0) goto L_0x0028
                java.lang.Number r3 = (java.lang.Number) r3
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r3, r0)
                goto L_0x000d
            L_0x0028:
                boolean r1 = r3 instanceof java.lang.Character
                if (r1 == 0) goto L_0x0033
                java.lang.Character r3 = (java.lang.Character) r3
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r3, r0)
                goto L_0x000d
            L_0x0033:
                boolean r0 = r3.equals(r0)
                goto L_0x000d
            L_0x0038:
                scala.None$ r0 = scala.None$.MODULE$
                goto L_0x0018
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.HashMap.HashMap1.get0(java.lang.Object, int, int):scala.Option");
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public <B1> scala.collection.immutable.HashMap<A, B1> updated0(A r9, int r10, int r11, B1 r12, scala.Tuple2<A, B1> r13) {
            /*
                r8 = this;
                r4 = 0
                int r2 = r8.hash()
                if (r10 != r2) goto L_0x0039
                java.lang.Object r3 = r8.key()
                if (r9 != r3) goto L_0x0016
                r2 = 1
            L_0x000e:
                if (r2 == 0) goto L_0x0039
                scala.collection.immutable.HashMap$HashMap1 r2 = new scala.collection.immutable.HashMap$HashMap1
                r2.<init>(r9, r10, r12, r13)
            L_0x0015:
                return r2
            L_0x0016:
                if (r9 != 0) goto L_0x001a
                r2 = r4
                goto L_0x000e
            L_0x001a:
                boolean r2 = r9 instanceof java.lang.Number
                if (r2 == 0) goto L_0x0027
                r0 = r9
                java.lang.Number r0 = (java.lang.Number) r0
                r2 = r0
                boolean r2 = scala.runtime.BoxesRunTime.equalsNumObject(r2, r3)
                goto L_0x000e
            L_0x0027:
                boolean r2 = r9 instanceof java.lang.Character
                if (r2 == 0) goto L_0x0034
                r0 = r9
                java.lang.Character r0 = (java.lang.Character) r0
                r2 = r0
                boolean r2 = scala.runtime.BoxesRunTime.equalsCharObject(r2, r3)
                goto L_0x000e
            L_0x0034:
                boolean r2 = r9.equals(r3)
                goto L_0x000e
            L_0x0039:
                int r2 = r8.hash()
                if (r10 == r2) goto L_0x0065
                scala.collection.immutable.HashMap$HashTrieMap r1 = new scala.collection.immutable.HashMap$HashTrieMap
                scala.collection.immutable.HashMap[] r2 = new scala.collection.immutable.HashMap[r4]
                r1.<init>(r4, r2, r4)
                java.lang.Object r2 = r8.key()
                int r3 = r8.hash()
                java.lang.Object r5 = r8.value()
                scala.Tuple2 r6 = r8.kv()
                r4 = r11
                scala.collection.immutable.HashMap r2 = r1.updated0(r2, r3, r4, r5, r6)
                r3 = r9
                r4 = r10
                r5 = r11
                r6 = r12
                r7 = r13
                scala.collection.immutable.HashMap r2 = r2.updated0(r3, r4, r5, r6, r7)
                goto L_0x0015
            L_0x0065:
                scala.collection.immutable.HashMap$HashMapCollision1 r2 = new scala.collection.immutable.HashMap$HashMapCollision1
                scala.collection.immutable.ListMap r3 = new scala.collection.immutable.ListMap
                r3.<init>()
                java.lang.Object r4 = r8.key()
                java.lang.Object r5 = r8.value()
                scala.collection.immutable.ListMap r3 = r3.updated(r4, r5)
                scala.collection.immutable.ListMap r3 = r3.updated(r9, r12)
                r2.<init>(r10, r3)
                goto L_0x0015
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.HashMap.HashMap1.updated0(java.lang.Object, int, int, java.lang.Object, scala.Tuple2):scala.collection.immutable.HashMap");
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public scala.collection.immutable.HashMap<A, B> removed0(A r3, int r4, int r5) {
            /*
                r2 = this;
                int r0 = r2.hash()
                if (r4 != r0) goto L_0x0031
                java.lang.Object r0 = r2.key()
                if (r3 != r0) goto L_0x0012
                r0 = 1
            L_0x000d:
                if (r0 == 0) goto L_0x0031
                scala.collection.immutable.HashMap$EmptyHashMap$ r0 = scala.collection.immutable.HashMap$EmptyHashMap$.MODULE$
            L_0x0011:
                return r0
            L_0x0012:
                if (r3 != 0) goto L_0x0016
                r0 = 0
                goto L_0x000d
            L_0x0016:
                boolean r1 = r3 instanceof java.lang.Number
                if (r1 == 0) goto L_0x0021
                java.lang.Number r3 = (java.lang.Number) r3
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r3, r0)
                goto L_0x000d
            L_0x0021:
                boolean r1 = r3 instanceof java.lang.Character
                if (r1 == 0) goto L_0x002c
                java.lang.Character r3 = (java.lang.Character) r3
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r3, r0)
                goto L_0x000d
            L_0x002c:
                boolean r0 = r3.equals(r0)
                goto L_0x000d
            L_0x0031:
                r0 = r2
                goto L_0x0011
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.HashMap.HashMap1.removed0(java.lang.Object, int, int):scala.collection.immutable.HashMap");
        }

        public Iterator<Tuple2<A, B>> iterator() {
            return Predef$.MODULE$.wrapRefArray((Object[]) new Tuple2[]{ensurePair()}).iterator();
        }

        public <U> void foreach(Function1<Tuple2<A, B>, U> f) {
            f.apply(ensurePair());
        }

        public Tuple2<A, B> ensurePair() {
            if (kv() != null) {
                return kv();
            }
            kv_$eq(new Tuple2(key(), value()));
            return kv();
        }
    }

    /* compiled from: HashMap.scala */
    public static class HashMapCollision1<A, B> extends HashMap<A, B> implements ScalaObject {
        private int hash;
        private ListMap<A, B> kvs;

        public HashMapCollision1(int hash2, ListMap<A, B> kvs2) {
            this.hash = hash2;
            this.kvs = kvs2;
        }

        public int hash() {
            return this.hash;
        }

        public ListMap<A, B> kvs() {
            return this.kvs;
        }

        public int size() {
            return kvs().size();
        }

        public Option<B> get0(A key, int hash2, int level) {
            return hash2 == hash() ? kvs().get(key) : None$.MODULE$;
        }

        public <B1> HashMap<A, B1> updated0(A key, int hash2, int level$1, B1 value, Tuple2<A, B1> kv) {
            if (hash2 == hash()) {
                return new HashMapCollision1(hash2, kvs().updated(key, value));
            }
            ObjectRef m$1 = new ObjectRef(new HashTrieMap(0, new HashMap[0], 0));
            kvs().foreach(new HashMap$HashMapCollision1$$anonfun$updated0$1(this, level$1, m$1));
            return ((HashMap) m$1.elem).updated0(key, hash2, level$1, value, kv);
        }

        public HashMap<A, B> removed0(A key, int hash2, int level) {
            if (hash2 != hash()) {
                return this;
            }
            ListMap kvs1 = kvs().$minus((Object) key);
            if (kvs1.isEmpty()) {
                return HashMap$EmptyHashMap$.MODULE$;
            }
            return new HashMapCollision1(hash2, kvs1);
        }

        public Iterator<Tuple2<A, B>> iterator() {
            return kvs().iterator();
        }

        public <U> void foreach(Function1<Tuple2<A, B>, U> f) {
            kvs().foreach(f);
        }
    }

    /* compiled from: HashMap.scala */
    public static class HashTrieMap<A, B> extends HashMap<A, B> implements ScalaObject {
        private int bitmap;
        public HashMap[] scala$collection$immutable$HashMap$HashTrieMap$$elems;
        private int size0;

        public HashTrieMap(int bitmap2, HashMap<A, B>[] elems, int size02) {
            this.bitmap = bitmap2;
            this.scala$collection$immutable$HashMap$HashTrieMap$$elems = elems;
            this.size0 = size02;
        }

        private int bitmap() {
            return this.bitmap;
        }

        public final HashMap[] scala$collection$immutable$HashMap$HashTrieMap$$elems() {
            return this.scala$collection$immutable$HashMap$HashTrieMap$$elems;
        }

        private int size0() {
            return this.size0;
        }

        public int size() {
            return size0();
        }

        public Option<B> get0(A key, int hash, int level) {
            int index = (hash >>> level) & 31;
            int mask = 1 << index;
            if (bitmap() == -1) {
                return this.scala$collection$immutable$HashMap$HashTrieMap$$elems[index & 31].get0(key, hash, level + 5);
            }
            if ((bitmap() & mask) == 0) {
                return None$.MODULE$;
            }
            return this.scala$collection$immutable$HashMap$HashTrieMap$$elems[Integer.bitCount(bitmap() & (mask - 1))].get0(key, hash, level + 5);
        }

        public <B1> HashMap<A, B1> updated0(A key, int hash, int level, B1 value, Tuple2<A, B1> kv) {
            int mask = 1 << ((hash >>> level) & 31);
            int offset = Integer.bitCount(bitmap() & (mask - 1));
            if ((bitmap() & mask) != 0) {
                HashMap[] elemsNew = new HashMap[this.scala$collection$immutable$HashMap$HashTrieMap$$elems.length];
                Array$.MODULE$.copy(this.scala$collection$immutable$HashMap$HashTrieMap$$elems, 0, elemsNew, 0, this.scala$collection$immutable$HashMap$HashTrieMap$$elems.length);
                HashMap sub = scala$collection$immutable$HashMap$HashTrieMap$$elems()[offset];
                HashMap subNew = sub.updated0(key, hash, level + 5, value, kv);
                elemsNew[offset] = subNew;
                return new HashTrieMap(bitmap(), elemsNew, size() + (subNew.size() - sub.size()));
            }
            HashMap[] elemsNew2 = new HashMap[(scala$collection$immutable$HashMap$HashTrieMap$$elems().length + 1)];
            Array$.MODULE$.copy(scala$collection$immutable$HashMap$HashTrieMap$$elems(), 0, elemsNew2, 0, offset);
            elemsNew2[offset] = new HashMap1(key, hash, value, kv);
            Array$.MODULE$.copy(scala$collection$immutable$HashMap$HashTrieMap$$elems(), offset, elemsNew2, offset + 1, scala$collection$immutable$HashMap$HashTrieMap$$elems().length - offset);
            return new HashTrieMap(bitmap() | mask, elemsNew2, size() + 1);
        }

        public HashMap<A, B> removed0(A key, int hash, int level) {
            int mask = 1 << ((hash >>> level) & 31);
            int offset = Integer.bitCount(bitmap() & (mask - 1));
            if ((bitmap() & mask) == 0) {
                return this;
            }
            HashMap sub = this.scala$collection$immutable$HashMap$HashTrieMap$$elems[offset];
            HashMap subNew = sub.removed0(key, hash, level + 5);
            if (subNew.isEmpty()) {
                int bitmapNew = bitmap() ^ mask;
                if (bitmapNew == 0) {
                    return HashMap$EmptyHashMap$.MODULE$;
                }
                HashMap[] elemsNew = new HashMap[(this.scala$collection$immutable$HashMap$HashTrieMap$$elems.length - 1)];
                Array$.MODULE$.copy(this.scala$collection$immutable$HashMap$HashTrieMap$$elems, 0, elemsNew, 0, offset);
                Array$.MODULE$.copy(scala$collection$immutable$HashMap$HashTrieMap$$elems(), offset + 1, elemsNew, offset, (scala$collection$immutable$HashMap$HashTrieMap$$elems().length - offset) - 1);
                return new HashTrieMap(bitmapNew, elemsNew, size() - sub.size());
            }
            HashMap[] elemsNew2 = new HashMap[scala$collection$immutable$HashMap$HashTrieMap$$elems().length];
            Array$.MODULE$.copy(scala$collection$immutable$HashMap$HashTrieMap$$elems(), 0, elemsNew2, 0, scala$collection$immutable$HashMap$HashTrieMap$$elems().length);
            elemsNew2[offset] = subNew;
            return new HashTrieMap(bitmap(), elemsNew2, size() + (subNew.size() - sub.size()));
        }

        public Iterator iterator() {
            return new HashMap$HashTrieMap$$anon$1(this);
        }

        public <U> void foreach(Function1<Tuple2<A, B>, U> f) {
            for (HashMap foreach : this.scala$collection$immutable$HashMap$HashTrieMap$$elems) {
                foreach.foreach(f);
            }
        }
    }
}
