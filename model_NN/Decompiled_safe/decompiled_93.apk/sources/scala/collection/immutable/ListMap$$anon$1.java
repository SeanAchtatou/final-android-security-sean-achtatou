package scala.collection.immutable;

import java.util.NoSuchElementException;
import scala.Function1;
import scala.Function2;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.collection.TraversableOnce;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.StringBuilder;
import scala.math.Numeric;
import scala.reflect.ClassManifest;

/* compiled from: ListMap.scala */
public final class ListMap$$anon$1 implements Iterator<Tuple2<A, B>> {
    private ListMap<A, B> self;

    public ListMap$$anon$1(ListMap<A, B> $outer) {
        TraversableOnce.Cclass.$init$(this);
        Iterator.Cclass.$init$(this);
        this.self = $outer;
    }

    public <B> B $div$colon(B z, Function2<B, Tuple2<A, B>, B> op) {
        return TraversableOnce.Cclass.$div$colon(this, z, op);
    }

    public StringBuilder addString(StringBuilder b, String start, String sep, String end) {
        return TraversableOnce.Cclass.addString(this, b, start, sep, end);
    }

    public <B> void copyToArray(Object xs, int start) {
        TraversableOnce.Cclass.copyToArray(this, xs, start);
    }

    public <B> void copyToArray(Object xs, int start, int len) {
        Iterator.Cclass.copyToArray(this, xs, start, len);
    }

    public Iterator<Tuple2<A, B>> drop(int n) {
        return Iterator.Cclass.drop(this, n);
    }

    public boolean exists(Function1<Tuple2<A, B>, Boolean> p) {
        return Iterator.Cclass.exists(this, p);
    }

    public <B> B foldLeft(B z, Function2<B, Tuple2<A, B>, B> op) {
        return TraversableOnce.Cclass.foldLeft(this, z, op);
    }

    public boolean forall(Function1<Tuple2<A, B>, Boolean> p) {
        return Iterator.Cclass.forall(this, p);
    }

    public <U> void foreach(Function1<Tuple2<A, B>, U> f) {
        Iterator.Cclass.foreach(this, f);
    }

    public boolean isEmpty() {
        return Iterator.Cclass.isEmpty(this);
    }

    public boolean isTraversableAgain() {
        return Iterator.Cclass.isTraversableAgain(this);
    }

    public int length() {
        return Iterator.Cclass.length(this);
    }

    public <B> Iterator<B> map(Function1<Tuple2<A, B>, B> f) {
        return Iterator.Cclass.map(this, f);
    }

    public String mkString() {
        return TraversableOnce.Cclass.mkString(this);
    }

    public String mkString(String sep) {
        return TraversableOnce.Cclass.mkString(this, sep);
    }

    public String mkString(String start, String sep, String end) {
        return TraversableOnce.Cclass.mkString(this, start, sep, end);
    }

    public boolean nonEmpty() {
        return TraversableOnce.Cclass.nonEmpty(this);
    }

    public <B> B reduceLeft(Function2<B, Tuple2<A, B>, B> op) {
        return TraversableOnce.Cclass.reduceLeft(this, op);
    }

    public int size() {
        return TraversableOnce.Cclass.size(this);
    }

    public <B> B sum(Numeric<B> num) {
        return TraversableOnce.Cclass.sum(this, num);
    }

    public <B> Object toArray(ClassManifest<B> evidence$1) {
        return TraversableOnce.Cclass.toArray(this, evidence$1);
    }

    public <B> Buffer<B> toBuffer() {
        return TraversableOnce.Cclass.toBuffer(this);
    }

    public List<Tuple2<A, B>> toList() {
        return TraversableOnce.Cclass.toList(this);
    }

    public <B> Set<B> toSet() {
        return TraversableOnce.Cclass.toSet(this);
    }

    public Stream<Tuple2<A, B>> toStream() {
        return Iterator.Cclass.toStream(this);
    }

    public String toString() {
        return Iterator.Cclass.toString(this);
    }

    public boolean hasNext() {
        return !this.self.isEmpty();
    }

    public Tuple2<A, B> next() {
        if (hasNext()) {
            Tuple2<A, B> tuple2 = new Tuple2<>(this.self.key(), this.self.value());
            this.self = this.self.next();
            return tuple2;
        }
        throw new NoSuchElementException("next on empty iterator");
    }
}
