package scala.collection.immutable;

import java.io.Serializable;
import scala.ScalaObject;

/* compiled from: Range.scala */
public final class Range$ implements Serializable, ScalaObject {
    public static final Range$ MODULE$ = null;
    private final int MAX_PRINT = 512;

    static {
        new Range$();
    }

    private Range$() {
        MODULE$ = this;
    }

    public int MAX_PRINT() {
        return this.MAX_PRINT;
    }
}
