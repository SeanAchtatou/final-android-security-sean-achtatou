package scala.collection.immutable;

import scala.Function1;
import scala.Function2;
import scala.Tuple2;
import scala.collection.Iterator;
import scala.collection.TraversableOnce;
import scala.collection.immutable.HashMap;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.StringBuilder;
import scala.math.Numeric;
import scala.reflect.ClassManifest;

/* compiled from: HashMap.scala */
public final class HashMap$HashTrieMap$$anon$1 implements Iterator<Tuple2<A, B>> {
    private HashMap<A, B>[] arrayD;
    private HashMap<A, B>[][] arrayStack = new HashMap[6][];
    private int depth = 0;
    private int posD;
    private int[] posStack = new int[6];
    private Iterator<Tuple2<A, B>> subIter;

    public HashMap$HashTrieMap$$anon$1(HashMap.HashTrieMap<A, B> hashTrieMap) {
        TraversableOnce.Cclass.$init$(this);
        Iterator.Cclass.$init$(this);
        this.arrayD = hashTrieMap.scala$collection$immutable$HashMap$HashTrieMap$$elems();
        this.posD = 0;
        this.subIter = null;
    }

    public <B> B $div$colon(B z, Function2<B, Tuple2<A, B>, B> op) {
        return TraversableOnce.Cclass.$div$colon(this, z, op);
    }

    public StringBuilder addString(StringBuilder b, String start, String sep, String end) {
        return TraversableOnce.Cclass.addString(this, b, start, sep, end);
    }

    public <B> void copyToArray(Object xs, int start) {
        TraversableOnce.Cclass.copyToArray(this, xs, start);
    }

    public <B> void copyToArray(Object xs, int start, int len) {
        Iterator.Cclass.copyToArray(this, xs, start, len);
    }

    public Iterator<Tuple2<A, B>> drop(int n) {
        return Iterator.Cclass.drop(this, n);
    }

    public boolean exists(Function1<Tuple2<A, B>, Boolean> p) {
        return Iterator.Cclass.exists(this, p);
    }

    public <B> B foldLeft(B z, Function2<B, Tuple2<A, B>, B> op) {
        return TraversableOnce.Cclass.foldLeft(this, z, op);
    }

    public boolean forall(Function1<Tuple2<A, B>, Boolean> p) {
        return Iterator.Cclass.forall(this, p);
    }

    public <U> void foreach(Function1<Tuple2<A, B>, U> f) {
        Iterator.Cclass.foreach(this, f);
    }

    public boolean isEmpty() {
        return Iterator.Cclass.isEmpty(this);
    }

    public boolean isTraversableAgain() {
        return Iterator.Cclass.isTraversableAgain(this);
    }

    public int length() {
        return Iterator.Cclass.length(this);
    }

    public <B> Iterator<B> map(Function1<Tuple2<A, B>, B> f) {
        return Iterator.Cclass.map(this, f);
    }

    public String mkString() {
        return TraversableOnce.Cclass.mkString(this);
    }

    public String mkString(String sep) {
        return TraversableOnce.Cclass.mkString(this, sep);
    }

    public String mkString(String start, String sep, String end) {
        return TraversableOnce.Cclass.mkString(this, start, sep, end);
    }

    public boolean nonEmpty() {
        return TraversableOnce.Cclass.nonEmpty(this);
    }

    public <B> B reduceLeft(Function2<B, Tuple2<A, B>, B> op) {
        return TraversableOnce.Cclass.reduceLeft(this, op);
    }

    public int size() {
        return TraversableOnce.Cclass.size(this);
    }

    public <B> B sum(Numeric<B> num) {
        return TraversableOnce.Cclass.sum(this, num);
    }

    public <B> Object toArray(ClassManifest<B> evidence$1) {
        return TraversableOnce.Cclass.toArray(this, evidence$1);
    }

    public <B> Buffer<B> toBuffer() {
        return TraversableOnce.Cclass.toBuffer(this);
    }

    public List<Tuple2<A, B>> toList() {
        return TraversableOnce.Cclass.toList(this);
    }

    public <B> Set<B> toSet() {
        return TraversableOnce.Cclass.toSet(this);
    }

    public Stream<Tuple2<A, B>> toStream() {
        return Iterator.Cclass.toStream(this);
    }

    public String toString() {
        return Iterator.Cclass.toString(this);
    }

    public boolean hasNext() {
        return this.subIter != null || this.depth >= 0;
    }

    public Tuple2<A, B> next() {
        if (this.subIter == null) {
            return next0(this.arrayD, this.posD);
        }
        Tuple2 el = this.subIter.next();
        if (!this.subIter.hasNext()) {
            this.subIter = null;
        }
        return el;
    }

    /* Debug info: failed to restart local var, previous not found, register: 7 */
    private Tuple2<A, B> next0(HashMap<A, B>[] elems, int i) {
        HashMap<A, B> hashMap;
        while (true) {
            if (i == elems.length - 1) {
                this.depth--;
                if (this.depth >= 0) {
                    this.arrayD = this.arrayStack[this.depth];
                    this.posD = this.posStack[this.depth];
                    this.arrayStack[this.depth] = null;
                } else {
                    this.arrayD = null;
                    this.posD = 0;
                }
            } else {
                this.posD++;
            }
            hashMap = elems[i];
            if (!(hashMap instanceof HashMap.HashTrieMap)) {
                break;
            }
            HashMap.HashTrieMap hashTrieMap = (HashMap.HashTrieMap) hashMap;
            if (this.depth >= 0) {
                this.arrayStack[this.depth] = this.arrayD;
                this.posStack[this.depth] = this.posD;
            }
            this.depth++;
            this.arrayD = hashTrieMap.scala$collection$immutable$HashMap$HashTrieMap$$elems;
            this.posD = 0;
            elems = hashTrieMap.scala$collection$immutable$HashMap$HashTrieMap$$elems;
            i = 0;
        }
        if (hashMap instanceof HashMap.HashMap1) {
            return ((HashMap.HashMap1) hashMap).ensurePair();
        }
        this.subIter = hashMap.iterator();
        return this.subIter.next();
    }
}
