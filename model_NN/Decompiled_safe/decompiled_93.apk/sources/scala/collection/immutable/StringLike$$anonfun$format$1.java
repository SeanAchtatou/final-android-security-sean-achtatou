package scala.collection.immutable;

import java.io.Serializable;
import scala.collection.immutable.StringLike;
import scala.runtime.AbstractFunction1;

/* compiled from: StringLike.scala */
public final class StringLike$$anonfun$format$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    private final /* synthetic */ StringLike $outer;

    public StringLike$$anonfun$format$1(StringLike<Repr> $outer2) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
    }

    public final Object apply(Object obj) {
        return StringLike.Cclass.scala$collection$immutable$StringLike$$unwrapArg(this.$outer, obj);
    }
}
