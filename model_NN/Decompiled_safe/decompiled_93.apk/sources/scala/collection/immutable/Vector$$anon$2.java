package scala.collection.immutable;

import scala.Function1;
import scala.Function2;
import scala.collection.Iterator;
import scala.collection.Iterator$;
import scala.collection.TraversableOnce;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.StringBuilder;
import scala.math.Numeric;
import scala.reflect.ClassManifest;

/* compiled from: Vector.scala */
public final class Vector$$anon$2 implements Iterator<A> {
    private final /* synthetic */ Vector $outer;
    private int i;

    public Vector$$anon$2(Vector<A> $outer2) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        TraversableOnce.Cclass.$init$(this);
        Iterator.Cclass.$init$(this);
        this.i = $outer2.length();
    }

    public <B> B $div$colon(B z, Function2<B, A, B> op) {
        return TraversableOnce.Cclass.$div$colon(this, z, op);
    }

    public StringBuilder addString(StringBuilder b, String start, String sep, String end) {
        return TraversableOnce.Cclass.addString(this, b, start, sep, end);
    }

    public <B> void copyToArray(Object xs, int start) {
        TraversableOnce.Cclass.copyToArray(this, xs, start);
    }

    public <B> void copyToArray(Object xs, int start, int len) {
        Iterator.Cclass.copyToArray(this, xs, start, len);
    }

    public Iterator<A> drop(int n) {
        return Iterator.Cclass.drop(this, n);
    }

    public boolean exists(Function1<A, Boolean> p) {
        return Iterator.Cclass.exists(this, p);
    }

    public <B> B foldLeft(B z, Function2<B, A, B> op) {
        return TraversableOnce.Cclass.foldLeft(this, z, op);
    }

    public boolean forall(Function1<A, Boolean> p) {
        return Iterator.Cclass.forall(this, p);
    }

    public <U> void foreach(Function1<A, U> f) {
        Iterator.Cclass.foreach(this, f);
    }

    public boolean isEmpty() {
        return Iterator.Cclass.isEmpty(this);
    }

    public boolean isTraversableAgain() {
        return Iterator.Cclass.isTraversableAgain(this);
    }

    public int length() {
        return Iterator.Cclass.length(this);
    }

    public <B> Iterator<B> map(Function1<A, B> f) {
        return Iterator.Cclass.map(this, f);
    }

    public String mkString() {
        return TraversableOnce.Cclass.mkString(this);
    }

    public String mkString(String sep) {
        return TraversableOnce.Cclass.mkString(this, sep);
    }

    public String mkString(String start, String sep, String end) {
        return TraversableOnce.Cclass.mkString(this, start, sep, end);
    }

    public boolean nonEmpty() {
        return TraversableOnce.Cclass.nonEmpty(this);
    }

    public <B> B reduceLeft(Function2<B, A, B> op) {
        return TraversableOnce.Cclass.reduceLeft(this, op);
    }

    public int size() {
        return TraversableOnce.Cclass.size(this);
    }

    public <B> B sum(Numeric<B> num) {
        return TraversableOnce.Cclass.sum(this, num);
    }

    public <B> Object toArray(ClassManifest<B> evidence$1) {
        return TraversableOnce.Cclass.toArray(this, evidence$1);
    }

    public <B> Buffer<B> toBuffer() {
        return TraversableOnce.Cclass.toBuffer(this);
    }

    public List<A> toList() {
        return TraversableOnce.Cclass.toList(this);
    }

    public <B> Set<B> toSet() {
        return TraversableOnce.Cclass.toSet(this);
    }

    public Stream<A> toStream() {
        return Iterator.Cclass.toStream(this);
    }

    public String toString() {
        return Iterator.Cclass.toString(this);
    }

    private int i() {
        return this.i;
    }

    private void i_$eq(int i2) {
        this.i = i2;
    }

    public boolean hasNext() {
        return i() > 0;
    }

    public A next() {
        if (i() <= 0) {
            return Iterator$.MODULE$.empty().next();
        }
        i_$eq(i() - 1);
        return this.$outer.apply(i());
    }
}
