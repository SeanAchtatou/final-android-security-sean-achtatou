package scala.collection.immutable;

import scala.Console$;
import scala.MatchError;
import scala.ScalaObject;
import scala.runtime.BoxesRunTime;

/* compiled from: Vector.scala */
public interface VectorPointer<T> extends ScalaObject {
    Object[] copyOf(Object[] objArr);

    int depth();

    void depth_$eq(int i);

    Object[] display0();

    void display0_$eq(Object[] objArr);

    Object[] display1();

    void display1_$eq(Object[] objArr);

    Object[] display2();

    void display2_$eq(Object[] objArr);

    Object[] display3();

    void display3_$eq(Object[] objArr);

    Object[] display4();

    void display4_$eq(Object[] objArr);

    Object[] display5();

    void display5_$eq(Object[] objArr);

    T getElem(int i, int i2);

    void gotoNextBlockStart(int i, int i2);

    void gotoNextBlockStartWritable(int i, int i2);

    void gotoPos(int i, int i2);

    void gotoPosWritable0(int i, int i2);

    void gotoPosWritable1(int i, int i2, int i3);

    <U> void initFrom(VectorPointer<U> vectorPointer);

    <U> void initFrom(VectorPointer<U> vectorPointer, int i);

    Object[] nullSlotAndCopy(Object[] objArr, int i);

    void stabilize(int i);

    /* renamed from: scala.collection.immutable.VectorPointer$class  reason: invalid class name */
    /* compiled from: Vector.scala */
    public abstract class Cclass {
        public static void $init$(VectorPointer $this) {
        }

        public static final void initFrom(VectorPointer $this, VectorPointer that) {
            $this.initFrom(that, that.depth());
        }

        public static final void initFrom(VectorPointer $this, VectorPointer that, int depth) {
            $this.depth_$eq(depth);
            int i = depth - 1;
            switch (i) {
                case -1:
                    return;
                case 0:
                    $this.display0_$eq(that.display0());
                    return;
                case 1:
                    $this.display1_$eq(that.display1());
                    $this.display0_$eq(that.display0());
                    return;
                case 2:
                    $this.display2_$eq(that.display2());
                    $this.display1_$eq(that.display1());
                    $this.display0_$eq(that.display0());
                    return;
                case 3:
                    $this.display3_$eq(that.display3());
                    $this.display2_$eq(that.display2());
                    $this.display1_$eq(that.display1());
                    $this.display0_$eq(that.display0());
                    return;
                case 4:
                    $this.display4_$eq(that.display4());
                    $this.display3_$eq(that.display3());
                    $this.display2_$eq(that.display2());
                    $this.display1_$eq(that.display1());
                    $this.display0_$eq(that.display0());
                    return;
                case 5:
                    $this.display5_$eq(that.display5());
                    $this.display4_$eq(that.display4());
                    $this.display3_$eq(that.display3());
                    $this.display2_$eq(that.display2());
                    $this.display1_$eq(that.display1());
                    $this.display0_$eq(that.display0());
                    return;
                default:
                    throw new MatchError(BoxesRunTime.boxToInteger(i));
            }
        }

        public static final Object getElem(VectorPointer $this, int index, int xor) {
            if (xor < 32) {
                return $this.display0()[index & 31];
            }
            if (xor < 1024) {
                return ((Object[]) $this.display1()[(index >> 5) & 31])[index & 31];
            }
            if (xor < 32768) {
                return ((Object[]) ((Object[]) $this.display2()[(index >> 10) & 31])[(index >> 5) & 31])[index & 31];
            }
            if (xor < 1048576) {
                return ((Object[]) ((Object[]) ((Object[]) $this.display3()[(index >> 15) & 31])[(index >> 10) & 31])[(index >> 5) & 31])[index & 31];
            }
            if (xor < 33554432) {
                return ((Object[]) ((Object[]) ((Object[]) ((Object[]) $this.display4()[(index >> 20) & 31])[(index >> 15) & 31])[(index >> 10) & 31])[(index >> 5) & 31])[index & 31];
            }
            if (xor < 1073741824) {
                return ((Object[]) ((Object[]) ((Object[]) ((Object[]) ((Object[]) $this.display5()[(index >> 25) & 31])[(index >> 20) & 31])[(index >> 15) & 31])[(index >> 10) & 31])[(index >> 5) & 31])[index & 31];
            }
            throw new IllegalArgumentException();
        }

        public static final void gotoPos(VectorPointer $this, int index, int xor) {
            if (xor < 32) {
                return;
            }
            if (xor < 1024) {
                $this.display0_$eq((Object[]) $this.display1()[(index >> 5) & 31]);
            } else if (xor < 32768) {
                $this.display1_$eq((Object[]) $this.display2()[(index >> 10) & 31]);
                $this.display0_$eq((Object[]) $this.display1()[(index >> 5) & 31]);
            } else if (xor < 1048576) {
                $this.display2_$eq((Object[]) $this.display3()[(index >> 15) & 31]);
                $this.display1_$eq((Object[]) $this.display2()[(index >> 10) & 31]);
                $this.display0_$eq((Object[]) $this.display1()[(index >> 5) & 31]);
            } else if (xor < 33554432) {
                $this.display3_$eq((Object[]) $this.display4()[(index >> 20) & 31]);
                $this.display2_$eq((Object[]) $this.display3()[(index >> 15) & 31]);
                $this.display1_$eq((Object[]) $this.display2()[(index >> 10) & 31]);
                $this.display0_$eq((Object[]) $this.display1()[(index >> 5) & 31]);
            } else if (xor < 1073741824) {
                $this.display4_$eq((Object[]) $this.display5()[(index >> 25) & 31]);
                $this.display3_$eq((Object[]) $this.display4()[(index >> 20) & 31]);
                $this.display2_$eq((Object[]) $this.display3()[(index >> 15) & 31]);
                $this.display1_$eq((Object[]) $this.display2()[(index >> 10) & 31]);
                $this.display0_$eq((Object[]) $this.display1()[(index >> 5) & 31]);
            } else {
                throw new IllegalArgumentException();
            }
        }

        public static final void gotoNextBlockStart(VectorPointer $this, int index, int xor) {
            if (xor < 1024) {
                $this.display0_$eq((Object[]) $this.display1()[(index >> 5) & 31]);
            } else if (xor < 32768) {
                $this.display1_$eq((Object[]) $this.display2()[(index >> 10) & 31]);
                $this.display0_$eq((Object[]) $this.display1()[0]);
            } else if (xor < 1048576) {
                $this.display2_$eq((Object[]) $this.display3()[(index >> 15) & 31]);
                $this.display1_$eq((Object[]) $this.display2()[0]);
                $this.display0_$eq((Object[]) $this.display1()[0]);
            } else if (xor < 33554432) {
                $this.display3_$eq((Object[]) $this.display4()[(index >> 20) & 31]);
                $this.display2_$eq((Object[]) $this.display3()[0]);
                $this.display1_$eq((Object[]) $this.display2()[0]);
                $this.display0_$eq((Object[]) $this.display1()[0]);
            } else if (xor < 1073741824) {
                $this.display4_$eq((Object[]) $this.display5()[(index >> 25) & 31]);
                $this.display3_$eq((Object[]) $this.display4()[0]);
                $this.display2_$eq((Object[]) $this.display3()[0]);
                $this.display1_$eq((Object[]) $this.display2()[0]);
                $this.display0_$eq((Object[]) $this.display1()[0]);
            } else {
                throw new IllegalArgumentException();
            }
        }

        public static final void gotoNextBlockStartWritable(VectorPointer $this, int index, int xor) {
            if (xor < 1024) {
                if ($this.depth() == 1) {
                    $this.display1_$eq(new Object[32]);
                    $this.display1()[0] = $this.display0();
                    $this.depth_$eq($this.depth() + 1);
                }
                $this.display0_$eq(new Object[32]);
                $this.display1()[(index >> 5) & 31] = $this.display0();
            } else if (xor < 32768) {
                if ($this.depth() == 2) {
                    $this.display2_$eq(new Object[32]);
                    $this.display2()[0] = $this.display1();
                    $this.depth_$eq($this.depth() + 1);
                }
                $this.display0_$eq(new Object[32]);
                $this.display1_$eq(new Object[32]);
                $this.display1()[(index >> 5) & 31] = $this.display0();
                $this.display2()[(index >> 10) & 31] = $this.display1();
            } else if (xor < 1048576) {
                if ($this.depth() == 3) {
                    $this.display3_$eq(new Object[32]);
                    $this.display3()[0] = $this.display2();
                    $this.depth_$eq($this.depth() + 1);
                }
                $this.display0_$eq(new Object[32]);
                $this.display1_$eq(new Object[32]);
                $this.display2_$eq(new Object[32]);
                $this.display1()[(index >> 5) & 31] = $this.display0();
                $this.display2()[(index >> 10) & 31] = $this.display1();
                $this.display3()[(index >> 15) & 31] = $this.display2();
            } else if (xor < 33554432) {
                if ($this.depth() == 4) {
                    $this.display4_$eq(new Object[32]);
                    $this.display4()[0] = $this.display3();
                    $this.depth_$eq($this.depth() + 1);
                }
                $this.display0_$eq(new Object[32]);
                $this.display1_$eq(new Object[32]);
                $this.display2_$eq(new Object[32]);
                $this.display3_$eq(new Object[32]);
                $this.display1()[(index >> 5) & 31] = $this.display0();
                $this.display2()[(index >> 10) & 31] = $this.display1();
                $this.display3()[(index >> 15) & 31] = $this.display2();
                $this.display4()[(index >> 20) & 31] = $this.display3();
            } else if (xor < 1073741824) {
                if ($this.depth() == 5) {
                    $this.display5_$eq(new Object[32]);
                    $this.display5()[0] = $this.display4();
                    $this.depth_$eq($this.depth() + 1);
                }
                $this.display0_$eq(new Object[32]);
                $this.display1_$eq(new Object[32]);
                $this.display2_$eq(new Object[32]);
                $this.display3_$eq(new Object[32]);
                $this.display4_$eq(new Object[32]);
                $this.display1()[(index >> 5) & 31] = $this.display0();
                $this.display2()[(index >> 10) & 31] = $this.display1();
                $this.display3()[(index >> 15) & 31] = $this.display2();
                $this.display4()[(index >> 20) & 31] = $this.display3();
                $this.display5()[(index >> 25) & 31] = $this.display4();
            } else {
                throw new IllegalArgumentException();
            }
        }

        public static final Object[] copyOf(VectorPointer $this, Object[] a) {
            if (a == null) {
                Console$.MODULE$.println("NULL");
            }
            Object[] b = new Object[a.length];
            System.arraycopy(a, 0, b, 0, a.length);
            return b;
        }

        public static final Object[] nullSlotAndCopy(VectorPointer $this, Object[] array, int index) {
            Object x = array[index];
            array[index] = null;
            return $this.copyOf((Object[]) x);
        }

        public static final void stabilize(VectorPointer $this, int index) {
            int depth = $this.depth() - 1;
            switch (depth) {
                case 0:
                    return;
                case 1:
                    $this.display1_$eq($this.copyOf($this.display1()));
                    $this.display1()[(index >> 5) & 31] = $this.display0();
                    return;
                case 2:
                    $this.display2_$eq($this.copyOf($this.display2()));
                    $this.display1_$eq($this.copyOf($this.display1()));
                    $this.display2()[(index >> 10) & 31] = $this.display1();
                    $this.display1()[(index >> 5) & 31] = $this.display0();
                    return;
                case 3:
                    $this.display3_$eq($this.copyOf($this.display3()));
                    $this.display2_$eq($this.copyOf($this.display2()));
                    $this.display1_$eq($this.copyOf($this.display1()));
                    $this.display3()[(index >> 15) & 31] = $this.display2();
                    $this.display2()[(index >> 10) & 31] = $this.display1();
                    $this.display1()[(index >> 5) & 31] = $this.display0();
                    return;
                case 4:
                    $this.display4_$eq($this.copyOf($this.display4()));
                    $this.display3_$eq($this.copyOf($this.display3()));
                    $this.display2_$eq($this.copyOf($this.display2()));
                    $this.display1_$eq($this.copyOf($this.display1()));
                    $this.display4()[(index >> 20) & 31] = $this.display3();
                    $this.display3()[(index >> 15) & 31] = $this.display2();
                    $this.display2()[(index >> 10) & 31] = $this.display1();
                    $this.display1()[(index >> 5) & 31] = $this.display0();
                    return;
                case 5:
                    $this.display5_$eq($this.copyOf($this.display5()));
                    $this.display4_$eq($this.copyOf($this.display4()));
                    $this.display3_$eq($this.copyOf($this.display3()));
                    $this.display2_$eq($this.copyOf($this.display2()));
                    $this.display1_$eq($this.copyOf($this.display1()));
                    $this.display5()[(index >> 25) & 31] = $this.display4();
                    $this.display4()[(index >> 20) & 31] = $this.display3();
                    $this.display3()[(index >> 15) & 31] = $this.display2();
                    $this.display2()[(index >> 10) & 31] = $this.display1();
                    $this.display1()[(index >> 5) & 31] = $this.display0();
                    return;
                default:
                    throw new MatchError(BoxesRunTime.boxToInteger(depth));
            }
        }

        public static final void gotoPosWritable0(VectorPointer $this, int newIndex, int xor) {
            int depth = $this.depth() - 1;
            switch (depth) {
                case 0:
                    $this.display0_$eq($this.copyOf($this.display0()));
                    return;
                case 1:
                    $this.display1_$eq($this.copyOf($this.display1()));
                    $this.display0_$eq($this.nullSlotAndCopy($this.display1(), (newIndex >> 5) & 31));
                    return;
                case 2:
                    $this.display2_$eq($this.copyOf($this.display2()));
                    $this.display1_$eq($this.nullSlotAndCopy($this.display2(), (newIndex >> 10) & 31));
                    $this.display0_$eq($this.nullSlotAndCopy($this.display1(), (newIndex >> 5) & 31));
                    return;
                case 3:
                    $this.display3_$eq($this.copyOf($this.display3()));
                    $this.display2_$eq($this.nullSlotAndCopy($this.display3(), (newIndex >> 15) & 31));
                    $this.display1_$eq($this.nullSlotAndCopy($this.display2(), (newIndex >> 10) & 31));
                    $this.display0_$eq($this.nullSlotAndCopy($this.display1(), (newIndex >> 5) & 31));
                    return;
                case 4:
                    $this.display4_$eq($this.copyOf($this.display4()));
                    $this.display3_$eq($this.nullSlotAndCopy($this.display4(), (newIndex >> 20) & 31));
                    $this.display2_$eq($this.nullSlotAndCopy($this.display3(), (newIndex >> 15) & 31));
                    $this.display1_$eq($this.nullSlotAndCopy($this.display2(), (newIndex >> 10) & 31));
                    $this.display0_$eq($this.nullSlotAndCopy($this.display1(), (newIndex >> 5) & 31));
                    return;
                case 5:
                    $this.display5_$eq($this.copyOf($this.display5()));
                    $this.display4_$eq($this.nullSlotAndCopy($this.display5(), (newIndex >> 25) & 31));
                    $this.display3_$eq($this.nullSlotAndCopy($this.display4(), (newIndex >> 20) & 31));
                    $this.display2_$eq($this.nullSlotAndCopy($this.display3(), (newIndex >> 15) & 31));
                    $this.display1_$eq($this.nullSlotAndCopy($this.display2(), (newIndex >> 10) & 31));
                    $this.display0_$eq($this.nullSlotAndCopy($this.display1(), (newIndex >> 5) & 31));
                    return;
                default:
                    throw new MatchError(BoxesRunTime.boxToInteger(depth));
            }
        }

        public static final void gotoPosWritable1(VectorPointer $this, int oldIndex, int newIndex, int xor) {
            if (xor < 32) {
                $this.display0_$eq($this.copyOf($this.display0()));
            } else if (xor < 1024) {
                $this.display1_$eq($this.copyOf($this.display1()));
                $this.display1()[(oldIndex >> 5) & 31] = $this.display0();
                $this.display0_$eq($this.nullSlotAndCopy($this.display1(), (newIndex >> 5) & 31));
            } else if (xor < 32768) {
                $this.display1_$eq($this.copyOf($this.display1()));
                $this.display2_$eq($this.copyOf($this.display2()));
                $this.display1()[(oldIndex >> 5) & 31] = $this.display0();
                $this.display2()[(oldIndex >> 10) & 31] = $this.display1();
                $this.display1_$eq($this.nullSlotAndCopy($this.display2(), (newIndex >> 10) & 31));
                $this.display0_$eq($this.nullSlotAndCopy($this.display1(), (newIndex >> 5) & 31));
            } else if (xor < 1048576) {
                $this.display1_$eq($this.copyOf($this.display1()));
                $this.display2_$eq($this.copyOf($this.display2()));
                $this.display3_$eq($this.copyOf($this.display3()));
                $this.display1()[(oldIndex >> 5) & 31] = $this.display0();
                $this.display2()[(oldIndex >> 10) & 31] = $this.display1();
                $this.display3()[(oldIndex >> 15) & 31] = $this.display2();
                $this.display2_$eq($this.nullSlotAndCopy($this.display3(), (newIndex >> 15) & 31));
                $this.display1_$eq($this.nullSlotAndCopy($this.display2(), (newIndex >> 10) & 31));
                $this.display0_$eq($this.nullSlotAndCopy($this.display1(), (newIndex >> 5) & 31));
            } else if (xor < 33554432) {
                $this.display1_$eq($this.copyOf($this.display1()));
                $this.display2_$eq($this.copyOf($this.display2()));
                $this.display3_$eq($this.copyOf($this.display3()));
                $this.display4_$eq($this.copyOf($this.display4()));
                $this.display1()[(oldIndex >> 5) & 31] = $this.display0();
                $this.display2()[(oldIndex >> 10) & 31] = $this.display1();
                $this.display3()[(oldIndex >> 15) & 31] = $this.display2();
                $this.display4()[(oldIndex >> 20) & 31] = $this.display3();
                $this.display3_$eq($this.nullSlotAndCopy($this.display4(), (newIndex >> 20) & 31));
                $this.display2_$eq($this.nullSlotAndCopy($this.display3(), (newIndex >> 15) & 31));
                $this.display1_$eq($this.nullSlotAndCopy($this.display2(), (newIndex >> 10) & 31));
                $this.display0_$eq($this.nullSlotAndCopy($this.display1(), (newIndex >> 5) & 31));
            } else if (xor < 1073741824) {
                $this.display1_$eq($this.copyOf($this.display1()));
                $this.display2_$eq($this.copyOf($this.display2()));
                $this.display3_$eq($this.copyOf($this.display3()));
                $this.display4_$eq($this.copyOf($this.display4()));
                $this.display5_$eq($this.copyOf($this.display5()));
                $this.display1()[(oldIndex >> 5) & 31] = $this.display0();
                $this.display2()[(oldIndex >> 10) & 31] = $this.display1();
                $this.display3()[(oldIndex >> 15) & 31] = $this.display2();
                $this.display4()[(oldIndex >> 20) & 31] = $this.display3();
                $this.display5()[(oldIndex >> 25) & 31] = $this.display4();
                $this.display4_$eq($this.nullSlotAndCopy($this.display5(), (newIndex >> 25) & 31));
                $this.display3_$eq($this.nullSlotAndCopy($this.display4(), (newIndex >> 20) & 31));
                $this.display2_$eq($this.nullSlotAndCopy($this.display3(), (newIndex >> 15) & 31));
                $this.display1_$eq($this.nullSlotAndCopy($this.display2(), (newIndex >> 10) & 31));
                $this.display0_$eq($this.nullSlotAndCopy($this.display1(), (newIndex >> 5) & 31));
            } else {
                throw new IllegalArgumentException();
            }
        }
    }
}
