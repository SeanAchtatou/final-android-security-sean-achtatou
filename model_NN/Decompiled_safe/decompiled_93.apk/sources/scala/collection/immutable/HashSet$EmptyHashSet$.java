package scala.collection.immutable;

import scala.ScalaObject;

/* compiled from: HashSet.scala */
public final class HashSet$EmptyHashSet$ extends HashSet<Object> implements ScalaObject {
    public static final HashSet$EmptyHashSet$ MODULE$ = null;

    static {
        new HashSet$EmptyHashSet$();
    }

    public HashSet$EmptyHashSet$() {
        MODULE$ = this;
    }
}
