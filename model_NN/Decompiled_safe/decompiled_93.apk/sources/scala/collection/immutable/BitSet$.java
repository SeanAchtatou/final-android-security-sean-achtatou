package scala.collection.immutable;

import scala.ScalaObject;
import scala.collection.generic.BitSetFactory;
import scala.collection.immutable.BitSet;

/* compiled from: BitSet.scala */
public final class BitSet$ implements BitSetFactory<BitSet>, ScalaObject, BitSetFactory {
    public static final BitSet$ MODULE$ = null;
    private final BitSet empty = new BitSet.BitSet1(0);
    private final int hashSeed = "BitSet".hashCode();

    static {
        new BitSet$();
    }

    private BitSet$() {
        MODULE$ = this;
        BitSetFactory.Cclass.$init$(this);
    }

    public BitSet empty() {
        return this.empty;
    }

    public BitSet fromArray(long[] elems) {
        int len = elems.length;
        if (len == 0) {
            return this.empty;
        }
        if (len == 1) {
            return new BitSet.BitSet1(elems[0]);
        }
        if (len == 2) {
            return new BitSet.BitSet2(elems[0], elems[1]);
        }
        return new BitSet.BitSetN(elems);
    }
}
