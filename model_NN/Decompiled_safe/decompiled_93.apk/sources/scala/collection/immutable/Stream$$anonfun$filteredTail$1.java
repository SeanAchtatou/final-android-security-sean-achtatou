package scala.collection.immutable;

import java.io.Serializable;
import scala.Function1;
import scala.runtime.AbstractFunction0;

/* compiled from: Stream.scala */
public final class Stream$$anonfun$filteredTail$1 extends AbstractFunction0 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Function1 p$1;
    public final /* synthetic */ Stream stream$1;

    public Stream$$anonfun$filteredTail$1(Stream stream, Function1 function1) {
        this.stream$1 = stream;
        this.p$1 = function1;
    }

    public final Stream<A> apply() {
        return ((Stream) this.stream$1.tail()).filter(this.p$1);
    }
}
