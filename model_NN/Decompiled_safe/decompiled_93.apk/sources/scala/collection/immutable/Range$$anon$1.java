package scala.collection.immutable;

import scala.Function1;
import scala.collection.immutable.Range;

/* compiled from: Range.scala */
public final class Range$$anon$1 extends Range.Inclusive implements Range.ByOne {
    public Range$$anon$1(int i, int i2) {
        super(i, i2, 1);
        Range.ByOne.Cclass.$init$(this);
    }

    public final <U> void foreach(Function1<Integer, U> f) {
        Range.ByOne.Cclass.foreach(this, f);
    }

    public final void foreach$mVc$sp(Function1<Integer, Object> f) {
        Range.ByOne.Cclass.foreach$mVc$sp(this, f);
    }
}
