package scala.collection.immutable;

import scala.Array$;
import scala.Function1;
import scala.Function2;
import scala.Predef$;
import scala.ScalaObject;
import scala.Tuple2;
import scala.collection.Iterable;
import scala.collection.IterableLike;
import scala.collection.Iterator;
import scala.collection.Iterator$;
import scala.collection.Seq;
import scala.collection.Set;
import scala.collection.SetLike;
import scala.collection.Traversable;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.Addable;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericSetTemplate;
import scala.collection.generic.GenericTraversableTemplate;
import scala.collection.generic.Subtractable;
import scala.collection.immutable.Iterable;
import scala.collection.immutable.Set;
import scala.collection.immutable.Traversable;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.Builder;
import scala.collection.mutable.StringBuilder;
import scala.math.Numeric;
import scala.reflect.ClassManifest;
import scala.runtime.BoxesRunTime;
import scala.runtime.ObjectRef;

/* compiled from: HashSet.scala */
public class HashSet<A> implements Set<A>, GenericSetTemplate<A, HashSet>, SetLike<A, HashSet<A>>, ScalaObject, Set {
    public static final long serialVersionUID = 2;

    public HashSet() {
        TraversableOnce.Cclass.$init$(this);
        TraversableLike.Cclass.$init$(this);
        GenericTraversableTemplate.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        IterableLike.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Function1.Cclass.$init$(this);
        GenericSetTemplate.Cclass.$init$(this);
        Addable.Cclass.$init$(this);
        Subtractable.Cclass.$init$(this);
        SetLike.Cclass.$init$(this);
        Set.Cclass.$init$(this);
        Set.Cclass.$init$(this);
    }

    public <B> B $div$colon(B z, Function2<B, A, B> op) {
        return TraversableOnce.Cclass.$div$colon(this, z, op);
    }

    public <B, That> That $plus$plus(TraversableOnce<B> that, CanBuildFrom<HashSet<A>, B, That> bf) {
        return TraversableLike.Cclass.$plus$plus(this, that, bf);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.HashSet<A>, scala.collection.generic.Addable] */
    public HashSet<A> $plus$plus(TraversableOnce<A> xs) {
        return Addable.Cclass.$plus$plus(this, xs);
    }

    public StringBuilder addString(StringBuilder b, String start, String sep, String end) {
        return TraversableOnce.Cclass.addString(this, b, start, sep, end);
    }

    public <A> Function1<A, A> andThen(Function1<Boolean, A> g) {
        return Function1.Cclass.andThen(this, g);
    }

    public boolean apply(A elem) {
        return SetLike.Cclass.apply(this, elem);
    }

    public int apply$mcII$sp(int v1) {
        return Function1.Cclass.apply$mcII$sp(this, v1);
    }

    public void apply$mcVI$sp(int v1) {
        Function1.Cclass.apply$mcVI$sp(this, v1);
    }

    public void apply$mcVL$sp(long v1) {
        Function1.Cclass.apply$mcVL$sp(this, v1);
    }

    public boolean canEqual(Object that) {
        return IterableLike.Cclass.canEqual(this, that);
    }

    public <A> Function1<A, Boolean> compose(Function1<A, A> g) {
        return Function1.Cclass.compose(this, g);
    }

    public <B> void copyToArray(Object xs, int start) {
        TraversableOnce.Cclass.copyToArray(this, xs, start);
    }

    public <B> void copyToArray(Object xs, int start, int len) {
        IterableLike.Cclass.copyToArray(this, xs, start, len);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.HashSet<A>, java.lang.Object] */
    public HashSet<A> drop(int n) {
        return TraversableLike.Cclass.drop(this, n);
    }

    public boolean equals(Object that) {
        return SetLike.Cclass.equals(this, that);
    }

    public boolean exists(Function1<A, Boolean> p) {
        return IterableLike.Cclass.exists(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.HashSet<A>, java.lang.Object] */
    public HashSet<A> filter(Function1<A, Boolean> p) {
        return TraversableLike.Cclass.filter(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.HashSet<A>, java.lang.Object] */
    public HashSet<A> filterNot(Function1<A, Boolean> p) {
        return TraversableLike.Cclass.filterNot(this, p);
    }

    public <B> B foldLeft(B z, Function2<B, A, B> op) {
        return TraversableOnce.Cclass.foldLeft(this, z, op);
    }

    public boolean forall(Function1<A, Boolean> p) {
        return IterableLike.Cclass.forall(this, p);
    }

    public <B> Builder<B, HashSet<B>> genericBuilder() {
        return GenericTraversableTemplate.Cclass.genericBuilder(this);
    }

    public int hashCode() {
        return SetLike.Cclass.hashCode(this);
    }

    public A head() {
        return IterableLike.Cclass.head(this);
    }

    public boolean isEmpty() {
        return SetLike.Cclass.isEmpty(this);
    }

    public final boolean isTraversableAgain() {
        return TraversableLike.Cclass.isTraversableAgain(this);
    }

    public A last() {
        return TraversableLike.Cclass.last(this);
    }

    public <B, That> That map(Function1<A, B> f, CanBuildFrom<HashSet<A>, B, That> bf) {
        return TraversableLike.Cclass.map(this, f, bf);
    }

    public String mkString() {
        return TraversableOnce.Cclass.mkString(this);
    }

    public String mkString(String sep) {
        return TraversableOnce.Cclass.mkString(this, sep);
    }

    public String mkString(String start, String sep, String end) {
        return TraversableOnce.Cclass.mkString(this, start, sep, end);
    }

    public Builder<A, HashSet<A>> newBuilder() {
        return SetLike.Cclass.newBuilder(this);
    }

    public boolean nonEmpty() {
        return TraversableOnce.Cclass.nonEmpty(this);
    }

    public <B> B reduceLeft(Function2<B, A, B> op) {
        return TraversableOnce.Cclass.reduceLeft(this, op);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.HashSet<A>, java.lang.Object] */
    public HashSet<A> repr() {
        return TraversableLike.Cclass.repr(this);
    }

    public <B> boolean sameElements(scala.collection.Iterable<B> that) {
        return IterableLike.Cclass.sameElements(this, that);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.HashSet<A>, java.lang.Object] */
    public HashSet<A> slice(int from, int until) {
        return IterableLike.Cclass.slice(this, from, until);
    }

    public String stringPrefix() {
        return SetLike.Cclass.stringPrefix(this);
    }

    public boolean subsetOf(scala.collection.Set<A> that) {
        return SetLike.Cclass.subsetOf(this, that);
    }

    public <B> B sum(Numeric<B> num) {
        return TraversableOnce.Cclass.sum(this, num);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.HashSet<A>, java.lang.Object] */
    public HashSet<A> tail() {
        return TraversableLike.Cclass.tail(this);
    }

    public scala.collection.Iterable<A> thisCollection() {
        return IterableLike.Cclass.thisCollection(this);
    }

    public <B> Object toArray(ClassManifest<B> evidence$1) {
        return TraversableOnce.Cclass.toArray(this, evidence$1);
    }

    public <B> Buffer<B> toBuffer() {
        return TraversableOnce.Cclass.toBuffer(this);
    }

    public List<A> toList() {
        return TraversableOnce.Cclass.toList(this);
    }

    public <B> Set<B> toSet() {
        return TraversableOnce.Cclass.toSet(this);
    }

    public Stream<A> toStream() {
        return IterableLike.Cclass.toStream(this);
    }

    public String toString() {
        return SetLike.Cclass.toString(this);
    }

    public <A1, B, That> That zip(scala.collection.Iterable<B> that, CanBuildFrom<HashSet<A>, Tuple2<A1, B>, That> bf) {
        return IterableLike.Cclass.zip(this, that, bf);
    }

    public GenericCompanion<HashSet> companion() {
        return HashSet$.MODULE$;
    }

    public int size() {
        return 0;
    }

    public HashSet<A> empty() {
        return HashSet$EmptyHashSet$.MODULE$;
    }

    public Iterator<A> iterator() {
        return Iterator$.MODULE$.empty();
    }

    public <U> void foreach(Function1<A, U> f) {
    }

    public boolean contains(A e) {
        return get0(e, computeHash(e), 0);
    }

    public HashSet<A> $plus(A e) {
        return updated0(e, computeHash(e), 0);
    }

    public HashSet<A> $plus(A elem1, A elem2, Seq<A> elems) {
        return (HashSet) $plus((Object) elem1).$plus((Object) elem2).$plus$plus(elems);
    }

    public int elemHashCode(A key) {
        if (key == null) {
            return 0;
        }
        return key instanceof Number ? BoxesRunTime.hashFromNumber((Number) key) : key.hashCode();
    }

    public int computeHash(A a) {
        int elemHashCode = elemHashCode(a);
        int i = elemHashCode + ((elemHashCode << 9) ^ -1);
        int i2 = i ^ (i >>> 14);
        int i3 = i2 + (i2 << 4);
        return i3 ^ (i3 >>> 10);
    }

    public boolean get0(A key, int hash, int level) {
        return false;
    }

    public HashSet<A> updated0(A key, int hash, int level) {
        return new HashSet1(key, hash);
    }

    /* compiled from: HashSet.scala */
    public static class HashSet1<A> extends HashSet<A> implements ScalaObject {
        private int hash;
        private A key;

        public HashSet1(A key2, int hash2) {
            this.key = key2;
            this.hash = hash2;
        }

        public int hash() {
            return this.hash;
        }

        public A key() {
            return this.key;
        }

        public int size() {
            return 1;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public boolean get0(A r5, int r6, int r7) {
            /*
                r4 = this;
                r3 = 1
                r2 = 0
                int r0 = r4.hash()
                if (r6 != r0) goto L_0x0032
                java.lang.Object r0 = r4.key()
                if (r5 != r0) goto L_0x0013
                r0 = r3
            L_0x000f:
                if (r0 == 0) goto L_0x0032
                r0 = r3
            L_0x0012:
                return r0
            L_0x0013:
                if (r5 != 0) goto L_0x0017
                r0 = r2
                goto L_0x000f
            L_0x0017:
                boolean r1 = r5 instanceof java.lang.Number
                if (r1 == 0) goto L_0x0022
                java.lang.Number r5 = (java.lang.Number) r5
                boolean r0 = scala.runtime.BoxesRunTime.equalsNumObject(r5, r0)
                goto L_0x000f
            L_0x0022:
                boolean r1 = r5 instanceof java.lang.Character
                if (r1 == 0) goto L_0x002d
                java.lang.Character r5 = (java.lang.Character) r5
                boolean r0 = scala.runtime.BoxesRunTime.equalsCharObject(r5, r0)
                goto L_0x000f
            L_0x002d:
                boolean r0 = r5.equals(r0)
                goto L_0x000f
            L_0x0032:
                r0 = r2
                goto L_0x0012
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.HashSet.HashSet1.get0(java.lang.Object, int, int):boolean");
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public scala.collection.immutable.HashSet<A> updated0(A r6, int r7, int r8) {
            /*
                r5 = this;
                r4 = 0
                int r2 = r5.hash()
                if (r7 != r2) goto L_0x0035
                java.lang.Object r3 = r5.key()
                if (r6 != r3) goto L_0x0012
                r2 = 1
            L_0x000e:
                if (r2 == 0) goto L_0x0035
                r2 = r5
            L_0x0011:
                return r2
            L_0x0012:
                if (r6 != 0) goto L_0x0016
                r2 = r4
                goto L_0x000e
            L_0x0016:
                boolean r2 = r6 instanceof java.lang.Number
                if (r2 == 0) goto L_0x0023
                r0 = r6
                java.lang.Number r0 = (java.lang.Number) r0
                r2 = r0
                boolean r2 = scala.runtime.BoxesRunTime.equalsNumObject(r2, r3)
                goto L_0x000e
            L_0x0023:
                boolean r2 = r6 instanceof java.lang.Character
                if (r2 == 0) goto L_0x0030
                r0 = r6
                java.lang.Character r0 = (java.lang.Character) r0
                r2 = r0
                boolean r2 = scala.runtime.BoxesRunTime.equalsCharObject(r2, r3)
                goto L_0x000e
            L_0x0030:
                boolean r2 = r6.equals(r3)
                goto L_0x000e
            L_0x0035:
                int r2 = r5.hash()
                if (r7 == r2) goto L_0x0053
                scala.collection.immutable.HashSet$HashTrieSet r1 = new scala.collection.immutable.HashSet$HashTrieSet
                scala.collection.immutable.HashSet[] r2 = new scala.collection.immutable.HashSet[r4]
                r1.<init>(r4, r2, r4)
                java.lang.Object r2 = r5.key()
                int r3 = r5.hash()
                scala.collection.immutable.HashSet r2 = r1.updated0(r2, r3, r8)
                scala.collection.immutable.HashSet r2 = r2.updated0(r6, r7, r8)
                goto L_0x0011
            L_0x0053:
                scala.collection.immutable.HashSet$HashSetCollision1 r2 = new scala.collection.immutable.HashSet$HashSetCollision1
                scala.collection.immutable.ListSet r3 = new scala.collection.immutable.ListSet
                r3.<init>()
                java.lang.Object r4 = r5.key()
                scala.collection.immutable.ListSet r3 = r3.$plus(r4)
                scala.collection.immutable.ListSet r3 = r3.$plus(r6)
                r2.<init>(r7, r3)
                goto L_0x0011
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.HashSet.HashSet1.updated0(java.lang.Object, int, int):scala.collection.immutable.HashSet");
        }

        public Iterator<A> iterator() {
            return Predef$.MODULE$.genericWrapArray(new Object[]{key()}).iterator();
        }

        public <U> void foreach(Function1<A, U> f) {
            f.apply(key());
        }
    }

    /* compiled from: HashSet.scala */
    public static class HashSetCollision1<A> extends HashSet<A> implements ScalaObject {
        private int hash;
        private ListSet<A> ks;

        public HashSetCollision1(int hash2, ListSet<A> ks2) {
            this.hash = hash2;
            this.ks = ks2;
        }

        public int hash() {
            return this.hash;
        }

        public ListSet<A> ks() {
            return this.ks;
        }

        public int size() {
            return ks().size();
        }

        public boolean get0(A key, int hash2, int level) {
            if (hash2 == hash()) {
                return ks().contains(key);
            }
            return false;
        }

        /* Debug info: failed to restart local var, previous not found, register: 4 */
        public HashSet<A> updated0(A key, int hash2, int level$1) {
            if (hash2 == hash()) {
                return new HashSetCollision1(hash2, ks().$plus((Object) key));
            }
            ObjectRef m$1 = new ObjectRef(new HashTrieSet(0, new HashSet[0], 0));
            ks().foreach(new HashSet$HashSetCollision1$$anonfun$updated0$1(this, level$1, m$1));
            return ((HashSet) m$1.elem).updated0(key, hash2, level$1);
        }

        public Iterator<A> iterator() {
            return ks().iterator();
        }

        public <U> void foreach(Function1<A, U> f) {
            ks().foreach(f);
        }
    }

    /* compiled from: HashSet.scala */
    public static class HashTrieSet<A> extends HashSet<A> implements ScalaObject {
        private int bitmap;
        public HashSet[] scala$collection$immutable$HashSet$HashTrieSet$$elems;
        private int size0;

        public HashTrieSet(int bitmap2, HashSet<A>[] elems, int size02) {
            this.bitmap = bitmap2;
            this.scala$collection$immutable$HashSet$HashTrieSet$$elems = elems;
            this.size0 = size02;
        }

        private int bitmap() {
            return this.bitmap;
        }

        public final HashSet[] scala$collection$immutable$HashSet$HashTrieSet$$elems() {
            return this.scala$collection$immutable$HashSet$HashTrieSet$$elems;
        }

        private int size0() {
            return this.size0;
        }

        public int size() {
            return size0();
        }

        public boolean get0(A key, int hash, int level) {
            int index = (hash >>> level) & 31;
            int mask = 1 << index;
            if (bitmap() == -1) {
                return this.scala$collection$immutable$HashSet$HashTrieSet$$elems[index & 31].get0(key, hash, level + 5);
            }
            if ((bitmap() & mask) == 0) {
                return false;
            }
            return this.scala$collection$immutable$HashSet$HashTrieSet$$elems[Integer.bitCount(bitmap() & (mask - 1))].get0(key, hash, level + 5);
        }

        public HashSet<A> updated0(A key, int hash, int level) {
            int mask = 1 << ((hash >>> level) & 31);
            int offset = Integer.bitCount(bitmap() & (mask - 1));
            if ((bitmap() & mask) != 0) {
                HashSet[] elemsNew = new HashSet[this.scala$collection$immutable$HashSet$HashTrieSet$$elems.length];
                Array$.MODULE$.copy(this.scala$collection$immutable$HashSet$HashTrieSet$$elems, 0, elemsNew, 0, this.scala$collection$immutable$HashSet$HashTrieSet$$elems.length);
                HashSet sub = scala$collection$immutable$HashSet$HashTrieSet$$elems()[offset];
                HashSet subNew = sub.updated0(key, hash, level + 5);
                elemsNew[offset] = subNew;
                return new HashTrieSet(bitmap(), elemsNew, size() + (subNew.size() - sub.size()));
            }
            HashSet[] elemsNew2 = new HashSet[(scala$collection$immutable$HashSet$HashTrieSet$$elems().length + 1)];
            Array$.MODULE$.copy(scala$collection$immutable$HashSet$HashTrieSet$$elems(), 0, elemsNew2, 0, offset);
            elemsNew2[offset] = new HashSet1(key, hash);
            int i = offset;
            Array$.MODULE$.copy(scala$collection$immutable$HashSet$HashTrieSet$$elems(), i, elemsNew2, offset + 1, scala$collection$immutable$HashSet$HashTrieSet$$elems().length - offset);
            return new HashTrieSet(bitmap() | mask, elemsNew2, size() + 1);
        }

        public Iterator iterator() {
            return new HashSet$HashTrieSet$$anon$1(this);
        }

        public <U> void foreach(Function1<A, U> f) {
            for (HashSet foreach : this.scala$collection$immutable$HashSet$HashTrieSet$$elems) {
                foreach.foreach(f);
            }
        }
    }
}
