package scala.collection.immutable;

import scala.Function0;
import scala.Function1;
import scala.Function2;
import scala.None$;
import scala.Option;
import scala.PartialFunction;
import scala.Predef$;
import scala.ScalaObject;
import scala.Some;
import scala.Tuple2;
import scala.collection.Iterable;
import scala.collection.IterableLike;
import scala.collection.Iterator;
import scala.collection.Map;
import scala.collection.MapLike;
import scala.collection.Traversable;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericTraversableTemplate;
import scala.collection.generic.Subtractable;
import scala.collection.immutable.Iterable;
import scala.collection.immutable.MapLike;
import scala.collection.immutable.Traversable;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.Builder;
import scala.collection.mutable.StringBuilder;
import scala.math.Numeric;
import scala.reflect.ClassManifest;
import scala.runtime.BoxesRunTime;

/* compiled from: Map.scala */
public interface Map<A, B> extends Iterable<Tuple2<A, B>>, scala.collection.Map<A, B>, MapLike<A, B, Map<A, B>>, ScalaObject {
    Map<A, B> empty();

    /* renamed from: scala.collection.immutable.Map$class  reason: invalid class name */
    /* compiled from: Map.scala */
    public abstract class Cclass {
        public static void $init$(Map $this) {
        }

        public static Map empty(Map $this) {
            return Map$EmptyMap$.MODULE$;
        }
    }

    /* compiled from: Map.scala */
    public static class Map1<A, B> implements Map<A, B>, ScalaObject, Map {
        private final A key1;
        private final B value1;

        public Map1(A key12, B value12) {
            this.key1 = key12;
            this.value1 = value12;
            TraversableOnce.Cclass.$init$(this);
            TraversableLike.Cclass.$init$(this);
            GenericTraversableTemplate.Cclass.$init$(this);
            Traversable.Cclass.$init$(this);
            Traversable.Cclass.$init$(this);
            IterableLike.Cclass.$init$(this);
            Iterable.Cclass.$init$(this);
            Iterable.Cclass.$init$(this);
            Function1.Cclass.$init$(this);
            PartialFunction.Cclass.$init$(this);
            Subtractable.Cclass.$init$(this);
            MapLike.Cclass.$init$(this);
            Map.Cclass.$init$(this);
            MapLike.Cclass.$init$(this);
            Cclass.$init$(this);
        }

        public <B> B $div$colon(B z, Function2<B, Tuple2<A, B>, B> op) {
            return TraversableOnce.Cclass.$div$colon(this, z, op);
        }

        public <B, That> That $plus$plus(TraversableOnce<B> that, CanBuildFrom<Map<A, B>, B, That> bf) {
            return TraversableLike.Cclass.$plus$plus(this, that, bf);
        }

        public StringBuilder addString(StringBuilder b, String start, String sep, String end) {
            return MapLike.Cclass.addString(this, b, start, sep, end);
        }

        public <C> PartialFunction<A, C> andThen(Function1<B, C> k) {
            return PartialFunction.Cclass.andThen(this, k);
        }

        public B apply(A key) {
            return MapLike.Cclass.apply(this, key);
        }

        public int apply$mcII$sp(int v1) {
            return Function1.Cclass.apply$mcII$sp(this, v1);
        }

        public void apply$mcVI$sp(int v1) {
            Function1.Cclass.apply$mcVI$sp(this, v1);
        }

        public void apply$mcVL$sp(long v1) {
            Function1.Cclass.apply$mcVL$sp(this, v1);
        }

        public boolean canEqual(Object that) {
            return IterableLike.Cclass.canEqual(this, that);
        }

        public GenericCompanion<Iterable> companion() {
            return Iterable.Cclass.companion(this);
        }

        public <A> Function1<A, B> compose(Function1<A, A> g) {
            return Function1.Cclass.compose(this, g);
        }

        public boolean contains(A key) {
            return MapLike.Cclass.contains(this, key);
        }

        public <B> void copyToArray(Object xs, int start) {
            TraversableOnce.Cclass.copyToArray(this, xs, start);
        }

        public <B> void copyToArray(Object xs, int start, int len) {
            IterableLike.Cclass.copyToArray(this, xs, start, len);
        }

        /* renamed from: default  reason: not valid java name */
        public B m7default(A key) {
            return MapLike.Cclass.m3default(this, key);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Map<A, B>, java.lang.Object] */
        public Map<A, B> drop(int n) {
            return TraversableLike.Cclass.drop(this, n);
        }

        public Map<A, B> empty() {
            return Cclass.empty(this);
        }

        public boolean equals(Object that) {
            return MapLike.Cclass.equals(this, that);
        }

        public boolean exists(Function1<Tuple2<A, B>, Boolean> p) {
            return IterableLike.Cclass.exists(this, p);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Map<A, B>, java.lang.Object] */
        public Map<A, B> filter(Function1<Tuple2<A, B>, Boolean> p) {
            return TraversableLike.Cclass.filter(this, p);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.Map, scala.collection.immutable.Map<A, B>] */
        public Map<A, B> filterNot(Function1<Tuple2<A, B>, Boolean> p) {
            return MapLike.Cclass.filterNot(this, p);
        }

        public <B> B foldLeft(B z, Function2<B, Tuple2<A, B>, B> op) {
            return TraversableOnce.Cclass.foldLeft(this, z, op);
        }

        public boolean forall(Function1<Tuple2<A, B>, Boolean> p) {
            return IterableLike.Cclass.forall(this, p);
        }

        public <B> Builder<B, Iterable<B>> genericBuilder() {
            return GenericTraversableTemplate.Cclass.genericBuilder(this);
        }

        public <B1> B1 getOrElse(A key, Function0<B1> function0) {
            return MapLike.Cclass.getOrElse(this, key, function0);
        }

        public int hashCode() {
            return MapLike.Cclass.hashCode(this);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.Tuple2<A, B>] */
        public Tuple2<A, B> head() {
            return IterableLike.Cclass.head(this);
        }

        public boolean isDefinedAt(A key) {
            return MapLike.Cclass.isDefinedAt(this, key);
        }

        public boolean isEmpty() {
            return MapLike.Cclass.isEmpty(this);
        }

        public final boolean isTraversableAgain() {
            return TraversableLike.Cclass.isTraversableAgain(this);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.Tuple2<A, B>] */
        public Tuple2<A, B> last() {
            return TraversableLike.Cclass.last(this);
        }

        public <B, That> That map(Function1<Tuple2<A, B>, B> f, CanBuildFrom<Map<A, B>, B, That> bf) {
            return TraversableLike.Cclass.map(this, f, bf);
        }

        public String mkString() {
            return TraversableOnce.Cclass.mkString(this);
        }

        public String mkString(String sep) {
            return TraversableOnce.Cclass.mkString(this, sep);
        }

        public String mkString(String start, String sep, String end) {
            return TraversableOnce.Cclass.mkString(this, start, sep, end);
        }

        public Builder<Tuple2<A, B>, Map<A, B>> newBuilder() {
            return MapLike.Cclass.newBuilder(this);
        }

        public boolean nonEmpty() {
            return TraversableOnce.Cclass.nonEmpty(this);
        }

        public <B> B reduceLeft(Function2<B, Tuple2<A, B>, B> op) {
            return TraversableOnce.Cclass.reduceLeft(this, op);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Map<A, B>, java.lang.Object] */
        public Map<A, B> repr() {
            return TraversableLike.Cclass.repr(this);
        }

        public <B> boolean sameElements(scala.collection.Iterable<B> that) {
            return IterableLike.Cclass.sameElements(this, that);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Map<A, B>, java.lang.Object] */
        public Map<A, B> slice(int from, int until) {
            return IterableLike.Cclass.slice(this, from, until);
        }

        public String stringPrefix() {
            return MapLike.Cclass.stringPrefix(this);
        }

        public <B> B sum(Numeric<B> num) {
            return TraversableOnce.Cclass.sum(this, num);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Map<A, B>, java.lang.Object] */
        public Map<A, B> tail() {
            return TraversableLike.Cclass.tail(this);
        }

        public scala.collection.Iterable<Tuple2<A, B>> thisCollection() {
            return IterableLike.Cclass.thisCollection(this);
        }

        public <B> Object toArray(ClassManifest<B> evidence$1) {
            return TraversableOnce.Cclass.toArray(this, evidence$1);
        }

        public <B> Buffer<B> toBuffer() {
            return TraversableOnce.Cclass.toBuffer(this);
        }

        public List<Tuple2<A, B>> toList() {
            return TraversableOnce.Cclass.toList(this);
        }

        public <B> Set<B> toSet() {
            return TraversableOnce.Cclass.toSet(this);
        }

        public Stream<Tuple2<A, B>> toStream() {
            return IterableLike.Cclass.toStream(this);
        }

        public String toString() {
            return MapLike.Cclass.toString(this);
        }

        public <A1, B, That> That zip(scala.collection.Iterable<B> that, CanBuildFrom<Map<A, B>, Tuple2<A1, B>, That> bf) {
            return IterableLike.Cclass.zip(this, that, bf);
        }

        public int size() {
            return 1;
        }

        public Option<B> get(A key) {
            A a = this.key1;
            return key == a ? true : key == null ? false : key instanceof Number ? BoxesRunTime.equalsNumObject((Number) key, a) : key instanceof Character ? BoxesRunTime.equalsCharObject((Character) key, a) : key.equals(a) ? new Some(this.value1) : None$.MODULE$;
        }

        public Iterator<Tuple2<A, B>> iterator() {
            return Predef$.MODULE$.wrapRefArray((Object[]) new Tuple2[]{new Tuple2(this.key1, this.value1)}).iterator();
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public <B1> scala.collection.immutable.Map<A, B1> updated(A r5, B1 r6) {
            /*
                r4 = this;
                A r2 = r4.key1
                if (r5 != r2) goto L_0x000f
                r1 = 1
            L_0x0005:
                if (r1 == 0) goto L_0x0032
                scala.collection.immutable.Map$Map1 r1 = new scala.collection.immutable.Map$Map1
                A r2 = r4.key1
                r1.<init>(r2, r6)
            L_0x000e:
                return r1
            L_0x000f:
                if (r5 != 0) goto L_0x0013
                r1 = 0
                goto L_0x0005
            L_0x0013:
                boolean r1 = r5 instanceof java.lang.Number
                if (r1 == 0) goto L_0x0020
                r0 = r5
                java.lang.Number r0 = (java.lang.Number) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsNumObject(r1, r2)
                goto L_0x0005
            L_0x0020:
                boolean r1 = r5 instanceof java.lang.Character
                if (r1 == 0) goto L_0x002d
                r0 = r5
                java.lang.Character r0 = (java.lang.Character) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsCharObject(r1, r2)
                goto L_0x0005
            L_0x002d:
                boolean r1 = r5.equals(r2)
                goto L_0x0005
            L_0x0032:
                scala.collection.immutable.Map$Map2 r1 = new scala.collection.immutable.Map$Map2
                A r2 = r4.key1
                B r3 = r4.value1
                r1.<init>(r2, r3, r5, r6)
                goto L_0x000e
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.Map.Map1.updated(java.lang.Object, java.lang.Object):scala.collection.immutable.Map");
        }

        public <B1> Map<A, B1> $plus(Tuple2<A, B1> kv) {
            return updated(kv._1(), kv._2());
        }

        public Map<A, B> $minus(A key) {
            A a = this.key1;
            return key == a ? true : key == null ? false : key instanceof Number ? BoxesRunTime.equalsNumObject((Number) key, a) : key instanceof Character ? BoxesRunTime.equalsCharObject((Character) key, a) : key.equals(a) ? Map$EmptyMap$.MODULE$ : this;
        }

        public <U> void foreach(Function1<Tuple2<A, B>, U> f) {
            f.apply(new Tuple2(this.key1, this.value1));
        }
    }

    /* compiled from: Map.scala */
    public static class Map2<A, B> implements Map<A, B>, ScalaObject, Map {
        private final A key1;
        private final A key2;
        private final B value1;
        private final B value2;

        public Map2(A key12, B value12, A key22, B value22) {
            this.key1 = key12;
            this.value1 = value12;
            this.key2 = key22;
            this.value2 = value22;
            TraversableOnce.Cclass.$init$(this);
            TraversableLike.Cclass.$init$(this);
            GenericTraversableTemplate.Cclass.$init$(this);
            Traversable.Cclass.$init$(this);
            Traversable.Cclass.$init$(this);
            IterableLike.Cclass.$init$(this);
            Iterable.Cclass.$init$(this);
            Iterable.Cclass.$init$(this);
            Function1.Cclass.$init$(this);
            PartialFunction.Cclass.$init$(this);
            Subtractable.Cclass.$init$(this);
            MapLike.Cclass.$init$(this);
            Map.Cclass.$init$(this);
            MapLike.Cclass.$init$(this);
            Cclass.$init$(this);
        }

        public <B> B $div$colon(B z, Function2<B, Tuple2<A, B>, B> op) {
            return TraversableOnce.Cclass.$div$colon(this, z, op);
        }

        public <B, That> That $plus$plus(TraversableOnce<B> that, CanBuildFrom<Map<A, B>, B, That> bf) {
            return TraversableLike.Cclass.$plus$plus(this, that, bf);
        }

        public StringBuilder addString(StringBuilder b, String start, String sep, String end) {
            return MapLike.Cclass.addString(this, b, start, sep, end);
        }

        public <C> PartialFunction<A, C> andThen(Function1<B, C> k) {
            return PartialFunction.Cclass.andThen(this, k);
        }

        public B apply(A key) {
            return MapLike.Cclass.apply(this, key);
        }

        public int apply$mcII$sp(int v1) {
            return Function1.Cclass.apply$mcII$sp(this, v1);
        }

        public void apply$mcVI$sp(int v1) {
            Function1.Cclass.apply$mcVI$sp(this, v1);
        }

        public void apply$mcVL$sp(long v1) {
            Function1.Cclass.apply$mcVL$sp(this, v1);
        }

        public boolean canEqual(Object that) {
            return IterableLike.Cclass.canEqual(this, that);
        }

        public GenericCompanion<Iterable> companion() {
            return Iterable.Cclass.companion(this);
        }

        public <A> Function1<A, B> compose(Function1<A, A> g) {
            return Function1.Cclass.compose(this, g);
        }

        public boolean contains(A key) {
            return MapLike.Cclass.contains(this, key);
        }

        public <B> void copyToArray(Object xs, int start) {
            TraversableOnce.Cclass.copyToArray(this, xs, start);
        }

        public <B> void copyToArray(Object xs, int start, int len) {
            IterableLike.Cclass.copyToArray(this, xs, start, len);
        }

        /* renamed from: default  reason: not valid java name */
        public B m8default(A key) {
            return MapLike.Cclass.m3default(this, key);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Map<A, B>, java.lang.Object] */
        public Map<A, B> drop(int n) {
            return TraversableLike.Cclass.drop(this, n);
        }

        public Map<A, B> empty() {
            return Cclass.empty(this);
        }

        public boolean equals(Object that) {
            return MapLike.Cclass.equals(this, that);
        }

        public boolean exists(Function1<Tuple2<A, B>, Boolean> p) {
            return IterableLike.Cclass.exists(this, p);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Map<A, B>, java.lang.Object] */
        public Map<A, B> filter(Function1<Tuple2<A, B>, Boolean> p) {
            return TraversableLike.Cclass.filter(this, p);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.Map, scala.collection.immutable.Map<A, B>] */
        public Map<A, B> filterNot(Function1<Tuple2<A, B>, Boolean> p) {
            return MapLike.Cclass.filterNot(this, p);
        }

        public <B> B foldLeft(B z, Function2<B, Tuple2<A, B>, B> op) {
            return TraversableOnce.Cclass.foldLeft(this, z, op);
        }

        public boolean forall(Function1<Tuple2<A, B>, Boolean> p) {
            return IterableLike.Cclass.forall(this, p);
        }

        public <B> Builder<B, Iterable<B>> genericBuilder() {
            return GenericTraversableTemplate.Cclass.genericBuilder(this);
        }

        public <B1> B1 getOrElse(A key, Function0<B1> function0) {
            return MapLike.Cclass.getOrElse(this, key, function0);
        }

        public int hashCode() {
            return MapLike.Cclass.hashCode(this);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.Tuple2<A, B>] */
        public Tuple2<A, B> head() {
            return IterableLike.Cclass.head(this);
        }

        public boolean isDefinedAt(A key) {
            return MapLike.Cclass.isDefinedAt(this, key);
        }

        public boolean isEmpty() {
            return MapLike.Cclass.isEmpty(this);
        }

        public final boolean isTraversableAgain() {
            return TraversableLike.Cclass.isTraversableAgain(this);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.Tuple2<A, B>] */
        public Tuple2<A, B> last() {
            return TraversableLike.Cclass.last(this);
        }

        public <B, That> That map(Function1<Tuple2<A, B>, B> f, CanBuildFrom<Map<A, B>, B, That> bf) {
            return TraversableLike.Cclass.map(this, f, bf);
        }

        public String mkString() {
            return TraversableOnce.Cclass.mkString(this);
        }

        public String mkString(String sep) {
            return TraversableOnce.Cclass.mkString(this, sep);
        }

        public String mkString(String start, String sep, String end) {
            return TraversableOnce.Cclass.mkString(this, start, sep, end);
        }

        public Builder<Tuple2<A, B>, Map<A, B>> newBuilder() {
            return MapLike.Cclass.newBuilder(this);
        }

        public boolean nonEmpty() {
            return TraversableOnce.Cclass.nonEmpty(this);
        }

        public <B> B reduceLeft(Function2<B, Tuple2<A, B>, B> op) {
            return TraversableOnce.Cclass.reduceLeft(this, op);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Map<A, B>, java.lang.Object] */
        public Map<A, B> repr() {
            return TraversableLike.Cclass.repr(this);
        }

        public <B> boolean sameElements(scala.collection.Iterable<B> that) {
            return IterableLike.Cclass.sameElements(this, that);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Map<A, B>, java.lang.Object] */
        public Map<A, B> slice(int from, int until) {
            return IterableLike.Cclass.slice(this, from, until);
        }

        public String stringPrefix() {
            return MapLike.Cclass.stringPrefix(this);
        }

        public <B> B sum(Numeric<B> num) {
            return TraversableOnce.Cclass.sum(this, num);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Map<A, B>, java.lang.Object] */
        public Map<A, B> tail() {
            return TraversableLike.Cclass.tail(this);
        }

        public scala.collection.Iterable<Tuple2<A, B>> thisCollection() {
            return IterableLike.Cclass.thisCollection(this);
        }

        public <B> Object toArray(ClassManifest<B> evidence$1) {
            return TraversableOnce.Cclass.toArray(this, evidence$1);
        }

        public <B> Buffer<B> toBuffer() {
            return TraversableOnce.Cclass.toBuffer(this);
        }

        public List<Tuple2<A, B>> toList() {
            return TraversableOnce.Cclass.toList(this);
        }

        public <B> Set<B> toSet() {
            return TraversableOnce.Cclass.toSet(this);
        }

        public Stream<Tuple2<A, B>> toStream() {
            return IterableLike.Cclass.toStream(this);
        }

        public String toString() {
            return MapLike.Cclass.toString(this);
        }

        public <A1, B, That> That zip(scala.collection.Iterable<B> that, CanBuildFrom<Map<A, B>, Tuple2<A1, B>, That> bf) {
            return IterableLike.Cclass.zip(this, that, bf);
        }

        public int size() {
            return 2;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public scala.Option<B> get(A r6) {
            /*
                r5 = this;
                r4 = 1
                r3 = 0
                A r2 = r5.key1
                if (r6 != r2) goto L_0x0011
                r1 = r4
            L_0x0007:
                if (r1 == 0) goto L_0x0034
                scala.Some r1 = new scala.Some
                B r2 = r5.value1
                r1.<init>(r2)
            L_0x0010:
                return r1
            L_0x0011:
                if (r6 != 0) goto L_0x0015
                r1 = r3
                goto L_0x0007
            L_0x0015:
                boolean r1 = r6 instanceof java.lang.Number
                if (r1 == 0) goto L_0x0022
                r0 = r6
                java.lang.Number r0 = (java.lang.Number) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsNumObject(r1, r2)
                goto L_0x0007
            L_0x0022:
                boolean r1 = r6 instanceof java.lang.Character
                if (r1 == 0) goto L_0x002f
                r0 = r6
                java.lang.Character r0 = (java.lang.Character) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsCharObject(r1, r2)
                goto L_0x0007
            L_0x002f:
                boolean r1 = r6.equals(r2)
                goto L_0x0007
            L_0x0034:
                A r1 = r5.key2
                if (r6 != r1) goto L_0x0043
                r1 = r4
            L_0x0039:
                if (r1 == 0) goto L_0x0062
                scala.Some r1 = new scala.Some
                B r2 = r5.value2
                r1.<init>(r2)
                goto L_0x0010
            L_0x0043:
                if (r6 != 0) goto L_0x0047
                r1 = r3
                goto L_0x0039
            L_0x0047:
                boolean r2 = r6 instanceof java.lang.Number
                if (r2 == 0) goto L_0x0052
                java.lang.Number r6 = (java.lang.Number) r6
                boolean r1 = scala.runtime.BoxesRunTime.equalsNumObject(r6, r1)
                goto L_0x0039
            L_0x0052:
                boolean r2 = r6 instanceof java.lang.Character
                if (r2 == 0) goto L_0x005d
                java.lang.Character r6 = (java.lang.Character) r6
                boolean r1 = scala.runtime.BoxesRunTime.equalsCharObject(r6, r1)
                goto L_0x0039
            L_0x005d:
                boolean r1 = r6.equals(r1)
                goto L_0x0039
            L_0x0062:
                scala.None$ r1 = scala.None$.MODULE$
                goto L_0x0010
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.Map.Map2.get(java.lang.Object):scala.Option");
        }

        public Iterator<Tuple2<A, B>> iterator() {
            return Predef$.MODULE$.wrapRefArray((Object[]) new Tuple2[]{new Tuple2(this.key1, this.value1), new Tuple2(this.key2, this.value2)}).iterator();
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public <B1> scala.collection.immutable.Map<A, B1> updated(A r9, B1 r10) {
            /*
                r8 = this;
                r4 = 1
                r3 = 0
                A r2 = r8.key1
                if (r9 != r2) goto L_0x0015
                r1 = r4
            L_0x0007:
                if (r1 == 0) goto L_0x0038
                scala.collection.immutable.Map$Map2 r1 = new scala.collection.immutable.Map$Map2
                A r2 = r8.key1
                A r3 = r8.key2
                B r4 = r8.value2
                r1.<init>(r2, r10, r3, r4)
            L_0x0014:
                return r1
            L_0x0015:
                if (r9 != 0) goto L_0x0019
                r1 = r3
                goto L_0x0007
            L_0x0019:
                boolean r1 = r9 instanceof java.lang.Number
                if (r1 == 0) goto L_0x0026
                r0 = r9
                java.lang.Number r0 = (java.lang.Number) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsNumObject(r1, r2)
                goto L_0x0007
            L_0x0026:
                boolean r1 = r9 instanceof java.lang.Character
                if (r1 == 0) goto L_0x0033
                r0 = r9
                java.lang.Character r0 = (java.lang.Character) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsCharObject(r1, r2)
                goto L_0x0007
            L_0x0033:
                boolean r1 = r9.equals(r2)
                goto L_0x0007
            L_0x0038:
                A r2 = r8.key2
                if (r9 != r2) goto L_0x004b
                r1 = r4
            L_0x003d:
                if (r1 == 0) goto L_0x006e
                scala.collection.immutable.Map$Map2 r1 = new scala.collection.immutable.Map$Map2
                A r2 = r8.key1
                B r3 = r8.value1
                A r4 = r8.key2
                r1.<init>(r2, r3, r4, r10)
                goto L_0x0014
            L_0x004b:
                if (r9 != 0) goto L_0x004f
                r1 = r3
                goto L_0x003d
            L_0x004f:
                boolean r1 = r9 instanceof java.lang.Number
                if (r1 == 0) goto L_0x005c
                r0 = r9
                java.lang.Number r0 = (java.lang.Number) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsNumObject(r1, r2)
                goto L_0x003d
            L_0x005c:
                boolean r1 = r9 instanceof java.lang.Character
                if (r1 == 0) goto L_0x0069
                r0 = r9
                java.lang.Character r0 = (java.lang.Character) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsCharObject(r1, r2)
                goto L_0x003d
            L_0x0069:
                boolean r1 = r9.equals(r2)
                goto L_0x003d
            L_0x006e:
                scala.collection.immutable.Map$Map3 r1 = new scala.collection.immutable.Map$Map3
                A r2 = r8.key1
                B r3 = r8.value1
                A r4 = r8.key2
                B r5 = r8.value2
                r6 = r9
                r7 = r10
                r1.<init>(r2, r3, r4, r5, r6, r7)
                goto L_0x0014
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.Map.Map2.updated(java.lang.Object, java.lang.Object):scala.collection.immutable.Map");
        }

        public <B1> Map<A, B1> $plus(Tuple2<A, B1> kv) {
            return updated(kv._1(), kv._2());
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public scala.collection.immutable.Map<A, B> $minus(A r6) {
            /*
                r5 = this;
                r4 = 1
                r3 = 0
                A r2 = r5.key1
                if (r6 != r2) goto L_0x0013
                r1 = r4
            L_0x0007:
                if (r1 == 0) goto L_0x0036
                scala.collection.immutable.Map$Map1 r1 = new scala.collection.immutable.Map$Map1
                A r2 = r5.key2
                B r3 = r5.value2
                r1.<init>(r2, r3)
            L_0x0012:
                return r1
            L_0x0013:
                if (r6 != 0) goto L_0x0017
                r1 = r3
                goto L_0x0007
            L_0x0017:
                boolean r1 = r6 instanceof java.lang.Number
                if (r1 == 0) goto L_0x0024
                r0 = r6
                java.lang.Number r0 = (java.lang.Number) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsNumObject(r1, r2)
                goto L_0x0007
            L_0x0024:
                boolean r1 = r6 instanceof java.lang.Character
                if (r1 == 0) goto L_0x0031
                r0 = r6
                java.lang.Character r0 = (java.lang.Character) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsCharObject(r1, r2)
                goto L_0x0007
            L_0x0031:
                boolean r1 = r6.equals(r2)
                goto L_0x0007
            L_0x0036:
                A r1 = r5.key2
                if (r6 != r1) goto L_0x0047
                r1 = r4
            L_0x003b:
                if (r1 == 0) goto L_0x0066
                scala.collection.immutable.Map$Map1 r1 = new scala.collection.immutable.Map$Map1
                A r2 = r5.key1
                B r3 = r5.value1
                r1.<init>(r2, r3)
                goto L_0x0012
            L_0x0047:
                if (r6 != 0) goto L_0x004b
                r1 = r3
                goto L_0x003b
            L_0x004b:
                boolean r2 = r6 instanceof java.lang.Number
                if (r2 == 0) goto L_0x0056
                java.lang.Number r6 = (java.lang.Number) r6
                boolean r1 = scala.runtime.BoxesRunTime.equalsNumObject(r6, r1)
                goto L_0x003b
            L_0x0056:
                boolean r2 = r6 instanceof java.lang.Character
                if (r2 == 0) goto L_0x0061
                java.lang.Character r6 = (java.lang.Character) r6
                boolean r1 = scala.runtime.BoxesRunTime.equalsCharObject(r6, r1)
                goto L_0x003b
            L_0x0061:
                boolean r1 = r6.equals(r1)
                goto L_0x003b
            L_0x0066:
                r1 = r5
                goto L_0x0012
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.Map.Map2.$minus(java.lang.Object):scala.collection.immutable.Map");
        }

        public <U> void foreach(Function1<Tuple2<A, B>, U> f) {
            f.apply(new Tuple2(this.key1, this.value1));
            f.apply(new Tuple2(this.key2, this.value2));
        }
    }

    /* compiled from: Map.scala */
    public static class Map3<A, B> implements Map<A, B>, ScalaObject, Map {
        private final A key1;
        private final A key2;
        private final A key3;
        private final B value1;
        private final B value2;
        private final B value3;

        public Map3(A key12, B value12, A key22, B value22, A key32, B value32) {
            this.key1 = key12;
            this.value1 = value12;
            this.key2 = key22;
            this.value2 = value22;
            this.key3 = key32;
            this.value3 = value32;
            TraversableOnce.Cclass.$init$(this);
            TraversableLike.Cclass.$init$(this);
            GenericTraversableTemplate.Cclass.$init$(this);
            Traversable.Cclass.$init$(this);
            Traversable.Cclass.$init$(this);
            IterableLike.Cclass.$init$(this);
            Iterable.Cclass.$init$(this);
            Iterable.Cclass.$init$(this);
            Function1.Cclass.$init$(this);
            PartialFunction.Cclass.$init$(this);
            Subtractable.Cclass.$init$(this);
            MapLike.Cclass.$init$(this);
            Map.Cclass.$init$(this);
            MapLike.Cclass.$init$(this);
            Cclass.$init$(this);
        }

        public <B> B $div$colon(B z, Function2<B, Tuple2<A, B>, B> op) {
            return TraversableOnce.Cclass.$div$colon(this, z, op);
        }

        public <B, That> That $plus$plus(TraversableOnce<B> that, CanBuildFrom<Map<A, B>, B, That> bf) {
            return TraversableLike.Cclass.$plus$plus(this, that, bf);
        }

        public StringBuilder addString(StringBuilder b, String start, String sep, String end) {
            return MapLike.Cclass.addString(this, b, start, sep, end);
        }

        public <C> PartialFunction<A, C> andThen(Function1<B, C> k) {
            return PartialFunction.Cclass.andThen(this, k);
        }

        public B apply(A key) {
            return MapLike.Cclass.apply(this, key);
        }

        public int apply$mcII$sp(int v1) {
            return Function1.Cclass.apply$mcII$sp(this, v1);
        }

        public void apply$mcVI$sp(int v1) {
            Function1.Cclass.apply$mcVI$sp(this, v1);
        }

        public void apply$mcVL$sp(long v1) {
            Function1.Cclass.apply$mcVL$sp(this, v1);
        }

        public boolean canEqual(Object that) {
            return IterableLike.Cclass.canEqual(this, that);
        }

        public GenericCompanion<Iterable> companion() {
            return Iterable.Cclass.companion(this);
        }

        public <A> Function1<A, B> compose(Function1<A, A> g) {
            return Function1.Cclass.compose(this, g);
        }

        public boolean contains(A key) {
            return MapLike.Cclass.contains(this, key);
        }

        public <B> void copyToArray(Object xs, int start) {
            TraversableOnce.Cclass.copyToArray(this, xs, start);
        }

        public <B> void copyToArray(Object xs, int start, int len) {
            IterableLike.Cclass.copyToArray(this, xs, start, len);
        }

        /* renamed from: default  reason: not valid java name */
        public B m9default(A key) {
            return MapLike.Cclass.m3default(this, key);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Map<A, B>, java.lang.Object] */
        public Map<A, B> drop(int n) {
            return TraversableLike.Cclass.drop(this, n);
        }

        public Map<A, B> empty() {
            return Cclass.empty(this);
        }

        public boolean equals(Object that) {
            return MapLike.Cclass.equals(this, that);
        }

        public boolean exists(Function1<Tuple2<A, B>, Boolean> p) {
            return IterableLike.Cclass.exists(this, p);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Map<A, B>, java.lang.Object] */
        public Map<A, B> filter(Function1<Tuple2<A, B>, Boolean> p) {
            return TraversableLike.Cclass.filter(this, p);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.Map, scala.collection.immutable.Map<A, B>] */
        public Map<A, B> filterNot(Function1<Tuple2<A, B>, Boolean> p) {
            return MapLike.Cclass.filterNot(this, p);
        }

        public <B> B foldLeft(B z, Function2<B, Tuple2<A, B>, B> op) {
            return TraversableOnce.Cclass.foldLeft(this, z, op);
        }

        public boolean forall(Function1<Tuple2<A, B>, Boolean> p) {
            return IterableLike.Cclass.forall(this, p);
        }

        public <B> Builder<B, Iterable<B>> genericBuilder() {
            return GenericTraversableTemplate.Cclass.genericBuilder(this);
        }

        public <B1> B1 getOrElse(A key, Function0<B1> function0) {
            return MapLike.Cclass.getOrElse(this, key, function0);
        }

        public int hashCode() {
            return MapLike.Cclass.hashCode(this);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.Tuple2<A, B>] */
        public Tuple2<A, B> head() {
            return IterableLike.Cclass.head(this);
        }

        public boolean isDefinedAt(A key) {
            return MapLike.Cclass.isDefinedAt(this, key);
        }

        public boolean isEmpty() {
            return MapLike.Cclass.isEmpty(this);
        }

        public final boolean isTraversableAgain() {
            return TraversableLike.Cclass.isTraversableAgain(this);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.Tuple2<A, B>] */
        public Tuple2<A, B> last() {
            return TraversableLike.Cclass.last(this);
        }

        public <B, That> That map(Function1<Tuple2<A, B>, B> f, CanBuildFrom<Map<A, B>, B, That> bf) {
            return TraversableLike.Cclass.map(this, f, bf);
        }

        public String mkString() {
            return TraversableOnce.Cclass.mkString(this);
        }

        public String mkString(String sep) {
            return TraversableOnce.Cclass.mkString(this, sep);
        }

        public String mkString(String start, String sep, String end) {
            return TraversableOnce.Cclass.mkString(this, start, sep, end);
        }

        public Builder<Tuple2<A, B>, Map<A, B>> newBuilder() {
            return MapLike.Cclass.newBuilder(this);
        }

        public boolean nonEmpty() {
            return TraversableOnce.Cclass.nonEmpty(this);
        }

        public <B> B reduceLeft(Function2<B, Tuple2<A, B>, B> op) {
            return TraversableOnce.Cclass.reduceLeft(this, op);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Map<A, B>, java.lang.Object] */
        public Map<A, B> repr() {
            return TraversableLike.Cclass.repr(this);
        }

        public <B> boolean sameElements(scala.collection.Iterable<B> that) {
            return IterableLike.Cclass.sameElements(this, that);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Map<A, B>, java.lang.Object] */
        public Map<A, B> slice(int from, int until) {
            return IterableLike.Cclass.slice(this, from, until);
        }

        public String stringPrefix() {
            return MapLike.Cclass.stringPrefix(this);
        }

        public <B> B sum(Numeric<B> num) {
            return TraversableOnce.Cclass.sum(this, num);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Map<A, B>, java.lang.Object] */
        public Map<A, B> tail() {
            return TraversableLike.Cclass.tail(this);
        }

        public scala.collection.Iterable<Tuple2<A, B>> thisCollection() {
            return IterableLike.Cclass.thisCollection(this);
        }

        public <B> Object toArray(ClassManifest<B> evidence$1) {
            return TraversableOnce.Cclass.toArray(this, evidence$1);
        }

        public <B> Buffer<B> toBuffer() {
            return TraversableOnce.Cclass.toBuffer(this);
        }

        public List<Tuple2<A, B>> toList() {
            return TraversableOnce.Cclass.toList(this);
        }

        public <B> Set<B> toSet() {
            return TraversableOnce.Cclass.toSet(this);
        }

        public Stream<Tuple2<A, B>> toStream() {
            return IterableLike.Cclass.toStream(this);
        }

        public String toString() {
            return MapLike.Cclass.toString(this);
        }

        public <A1, B, That> That zip(scala.collection.Iterable<B> that, CanBuildFrom<Map<A, B>, Tuple2<A1, B>, That> bf) {
            return IterableLike.Cclass.zip(this, that, bf);
        }

        public int size() {
            return 3;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public scala.Option<B> get(A r6) {
            /*
                r5 = this;
                r4 = 1
                r3 = 0
                A r2 = r5.key1
                if (r6 != r2) goto L_0x0011
                r1 = r4
            L_0x0007:
                if (r1 == 0) goto L_0x0034
                scala.Some r1 = new scala.Some
                B r2 = r5.value1
                r1.<init>(r2)
            L_0x0010:
                return r1
            L_0x0011:
                if (r6 != 0) goto L_0x0015
                r1 = r3
                goto L_0x0007
            L_0x0015:
                boolean r1 = r6 instanceof java.lang.Number
                if (r1 == 0) goto L_0x0022
                r0 = r6
                java.lang.Number r0 = (java.lang.Number) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsNumObject(r1, r2)
                goto L_0x0007
            L_0x0022:
                boolean r1 = r6 instanceof java.lang.Character
                if (r1 == 0) goto L_0x002f
                r0 = r6
                java.lang.Character r0 = (java.lang.Character) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsCharObject(r1, r2)
                goto L_0x0007
            L_0x002f:
                boolean r1 = r6.equals(r2)
                goto L_0x0007
            L_0x0034:
                A r2 = r5.key2
                if (r6 != r2) goto L_0x0043
                r1 = r4
            L_0x0039:
                if (r1 == 0) goto L_0x0066
                scala.Some r1 = new scala.Some
                B r2 = r5.value2
                r1.<init>(r2)
                goto L_0x0010
            L_0x0043:
                if (r6 != 0) goto L_0x0047
                r1 = r3
                goto L_0x0039
            L_0x0047:
                boolean r1 = r6 instanceof java.lang.Number
                if (r1 == 0) goto L_0x0054
                r0 = r6
                java.lang.Number r0 = (java.lang.Number) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsNumObject(r1, r2)
                goto L_0x0039
            L_0x0054:
                boolean r1 = r6 instanceof java.lang.Character
                if (r1 == 0) goto L_0x0061
                r0 = r6
                java.lang.Character r0 = (java.lang.Character) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsCharObject(r1, r2)
                goto L_0x0039
            L_0x0061:
                boolean r1 = r6.equals(r2)
                goto L_0x0039
            L_0x0066:
                A r1 = r5.key3
                if (r6 != r1) goto L_0x0075
                r1 = r4
            L_0x006b:
                if (r1 == 0) goto L_0x0094
                scala.Some r1 = new scala.Some
                B r2 = r5.value3
                r1.<init>(r2)
                goto L_0x0010
            L_0x0075:
                if (r6 != 0) goto L_0x0079
                r1 = r3
                goto L_0x006b
            L_0x0079:
                boolean r2 = r6 instanceof java.lang.Number
                if (r2 == 0) goto L_0x0084
                java.lang.Number r6 = (java.lang.Number) r6
                boolean r1 = scala.runtime.BoxesRunTime.equalsNumObject(r6, r1)
                goto L_0x006b
            L_0x0084:
                boolean r2 = r6 instanceof java.lang.Character
                if (r2 == 0) goto L_0x008f
                java.lang.Character r6 = (java.lang.Character) r6
                boolean r1 = scala.runtime.BoxesRunTime.equalsCharObject(r6, r1)
                goto L_0x006b
            L_0x008f:
                boolean r1 = r6.equals(r1)
                goto L_0x006b
            L_0x0094:
                scala.None$ r1 = scala.None$.MODULE$
                goto L_0x0010
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.Map.Map3.get(java.lang.Object):scala.Option");
        }

        public Iterator<Tuple2<A, B>> iterator() {
            return Predef$.MODULE$.wrapRefArray((Object[]) new Tuple2[]{new Tuple2(this.key1, this.value1), new Tuple2(this.key2, this.value2), new Tuple2(this.key3, this.value3)}).iterator();
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public <B1> scala.collection.immutable.Map<A, B1> updated(A r11, B1 r12) {
            /*
                r10 = this;
                r4 = 1
                r3 = 0
                A r2 = r10.key1
                if (r11 != r2) goto L_0x001a
                r1 = r4
            L_0x0007:
                if (r1 == 0) goto L_0x003d
                scala.collection.immutable.Map$Map3 r1 = new scala.collection.immutable.Map$Map3
                A r2 = r10.key1
                A r4 = r10.key2
                B r5 = r10.value2
                A r6 = r10.key3
                B r7 = r10.value3
                r3 = r12
                r1.<init>(r2, r3, r4, r5, r6, r7)
            L_0x0019:
                return r1
            L_0x001a:
                if (r11 != 0) goto L_0x001e
                r1 = r3
                goto L_0x0007
            L_0x001e:
                boolean r1 = r11 instanceof java.lang.Number
                if (r1 == 0) goto L_0x002b
                r0 = r11
                java.lang.Number r0 = (java.lang.Number) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsNumObject(r1, r2)
                goto L_0x0007
            L_0x002b:
                boolean r1 = r11 instanceof java.lang.Character
                if (r1 == 0) goto L_0x0038
                r0 = r11
                java.lang.Character r0 = (java.lang.Character) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsCharObject(r1, r2)
                goto L_0x0007
            L_0x0038:
                boolean r1 = r11.equals(r2)
                goto L_0x0007
            L_0x003d:
                A r2 = r10.key2
                if (r11 != r2) goto L_0x0055
                r1 = r4
            L_0x0042:
                if (r1 == 0) goto L_0x0078
                scala.collection.immutable.Map$Map3 r1 = new scala.collection.immutable.Map$Map3
                A r2 = r10.key1
                B r3 = r10.value1
                A r4 = r10.key2
                A r6 = r10.key3
                B r7 = r10.value3
                r5 = r12
                r1.<init>(r2, r3, r4, r5, r6, r7)
                goto L_0x0019
            L_0x0055:
                if (r11 != 0) goto L_0x0059
                r1 = r3
                goto L_0x0042
            L_0x0059:
                boolean r1 = r11 instanceof java.lang.Number
                if (r1 == 0) goto L_0x0066
                r0 = r11
                java.lang.Number r0 = (java.lang.Number) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsNumObject(r1, r2)
                goto L_0x0042
            L_0x0066:
                boolean r1 = r11 instanceof java.lang.Character
                if (r1 == 0) goto L_0x0073
                r0 = r11
                java.lang.Character r0 = (java.lang.Character) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsCharObject(r1, r2)
                goto L_0x0042
            L_0x0073:
                boolean r1 = r11.equals(r2)
                goto L_0x0042
            L_0x0078:
                A r2 = r10.key3
                if (r11 != r2) goto L_0x0090
                r1 = r4
            L_0x007d:
                if (r1 == 0) goto L_0x00b3
                scala.collection.immutable.Map$Map3 r1 = new scala.collection.immutable.Map$Map3
                A r2 = r10.key1
                B r3 = r10.value1
                A r4 = r10.key2
                B r5 = r10.value2
                A r6 = r10.key3
                r7 = r12
                r1.<init>(r2, r3, r4, r5, r6, r7)
                goto L_0x0019
            L_0x0090:
                if (r11 != 0) goto L_0x0094
                r1 = r3
                goto L_0x007d
            L_0x0094:
                boolean r1 = r11 instanceof java.lang.Number
                if (r1 == 0) goto L_0x00a1
                r0 = r11
                java.lang.Number r0 = (java.lang.Number) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsNumObject(r1, r2)
                goto L_0x007d
            L_0x00a1:
                boolean r1 = r11 instanceof java.lang.Character
                if (r1 == 0) goto L_0x00ae
                r0 = r11
                java.lang.Character r0 = (java.lang.Character) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsCharObject(r1, r2)
                goto L_0x007d
            L_0x00ae:
                boolean r1 = r11.equals(r2)
                goto L_0x007d
            L_0x00b3:
                scala.collection.immutable.Map$Map4 r1 = new scala.collection.immutable.Map$Map4
                A r2 = r10.key1
                B r3 = r10.value1
                A r4 = r10.key2
                B r5 = r10.value2
                A r6 = r10.key3
                B r7 = r10.value3
                r8 = r11
                r9 = r12
                r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9)
                goto L_0x0019
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.Map.Map3.updated(java.lang.Object, java.lang.Object):scala.collection.immutable.Map");
        }

        public <B1> Map<A, B1> $plus(Tuple2<A, B1> kv) {
            return updated(kv._1(), kv._2());
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public scala.collection.immutable.Map<A, B> $minus(A r7) {
            /*
                r6 = this;
                r4 = 1
                r3 = 0
                A r2 = r6.key1
                if (r7 != r2) goto L_0x0017
                r1 = r4
            L_0x0007:
                if (r1 == 0) goto L_0x003a
                scala.collection.immutable.Map$Map2 r1 = new scala.collection.immutable.Map$Map2
                A r2 = r6.key2
                B r3 = r6.value2
                A r4 = r6.key3
                B r5 = r6.value3
                r1.<init>(r2, r3, r4, r5)
            L_0x0016:
                return r1
            L_0x0017:
                if (r7 != 0) goto L_0x001b
                r1 = r3
                goto L_0x0007
            L_0x001b:
                boolean r1 = r7 instanceof java.lang.Number
                if (r1 == 0) goto L_0x0028
                r0 = r7
                java.lang.Number r0 = (java.lang.Number) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsNumObject(r1, r2)
                goto L_0x0007
            L_0x0028:
                boolean r1 = r7 instanceof java.lang.Character
                if (r1 == 0) goto L_0x0035
                r0 = r7
                java.lang.Character r0 = (java.lang.Character) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsCharObject(r1, r2)
                goto L_0x0007
            L_0x0035:
                boolean r1 = r7.equals(r2)
                goto L_0x0007
            L_0x003a:
                A r2 = r6.key2
                if (r7 != r2) goto L_0x004f
                r1 = r4
            L_0x003f:
                if (r1 == 0) goto L_0x0072
                scala.collection.immutable.Map$Map2 r1 = new scala.collection.immutable.Map$Map2
                A r2 = r6.key1
                B r3 = r6.value1
                A r4 = r6.key3
                B r5 = r6.value3
                r1.<init>(r2, r3, r4, r5)
                goto L_0x0016
            L_0x004f:
                if (r7 != 0) goto L_0x0053
                r1 = r3
                goto L_0x003f
            L_0x0053:
                boolean r1 = r7 instanceof java.lang.Number
                if (r1 == 0) goto L_0x0060
                r0 = r7
                java.lang.Number r0 = (java.lang.Number) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsNumObject(r1, r2)
                goto L_0x003f
            L_0x0060:
                boolean r1 = r7 instanceof java.lang.Character
                if (r1 == 0) goto L_0x006d
                r0 = r7
                java.lang.Character r0 = (java.lang.Character) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsCharObject(r1, r2)
                goto L_0x003f
            L_0x006d:
                boolean r1 = r7.equals(r2)
                goto L_0x003f
            L_0x0072:
                A r1 = r6.key3
                if (r7 != r1) goto L_0x0087
                r1 = r4
            L_0x0077:
                if (r1 == 0) goto L_0x00a6
                scala.collection.immutable.Map$Map2 r1 = new scala.collection.immutable.Map$Map2
                A r2 = r6.key1
                B r3 = r6.value1
                A r4 = r6.key2
                B r5 = r6.value2
                r1.<init>(r2, r3, r4, r5)
                goto L_0x0016
            L_0x0087:
                if (r7 != 0) goto L_0x008b
                r1 = r3
                goto L_0x0077
            L_0x008b:
                boolean r2 = r7 instanceof java.lang.Number
                if (r2 == 0) goto L_0x0096
                java.lang.Number r7 = (java.lang.Number) r7
                boolean r1 = scala.runtime.BoxesRunTime.equalsNumObject(r7, r1)
                goto L_0x0077
            L_0x0096:
                boolean r2 = r7 instanceof java.lang.Character
                if (r2 == 0) goto L_0x00a1
                java.lang.Character r7 = (java.lang.Character) r7
                boolean r1 = scala.runtime.BoxesRunTime.equalsCharObject(r7, r1)
                goto L_0x0077
            L_0x00a1:
                boolean r1 = r7.equals(r1)
                goto L_0x0077
            L_0x00a6:
                r1 = r6
                goto L_0x0016
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.Map.Map3.$minus(java.lang.Object):scala.collection.immutable.Map");
        }

        public <U> void foreach(Function1<Tuple2<A, B>, U> f) {
            f.apply(new Tuple2(this.key1, this.value1));
            f.apply(new Tuple2(this.key2, this.value2));
            f.apply(new Tuple2(this.key3, this.value3));
        }
    }

    /* compiled from: Map.scala */
    public static class Map4<A, B> implements Map<A, B>, ScalaObject, Map {
        private final A key1;
        private final A key2;
        private final A key3;
        private final A key4;
        private final B value1;
        private final B value2;
        private final B value3;
        private final B value4;

        public Map4(A key12, B value12, A key22, B value22, A key32, B value32, A key42, B value42) {
            this.key1 = key12;
            this.value1 = value12;
            this.key2 = key22;
            this.value2 = value22;
            this.key3 = key32;
            this.value3 = value32;
            this.key4 = key42;
            this.value4 = value42;
            TraversableOnce.Cclass.$init$(this);
            TraversableLike.Cclass.$init$(this);
            GenericTraversableTemplate.Cclass.$init$(this);
            Traversable.Cclass.$init$(this);
            Traversable.Cclass.$init$(this);
            IterableLike.Cclass.$init$(this);
            Iterable.Cclass.$init$(this);
            Iterable.Cclass.$init$(this);
            Function1.Cclass.$init$(this);
            PartialFunction.Cclass.$init$(this);
            Subtractable.Cclass.$init$(this);
            MapLike.Cclass.$init$(this);
            Map.Cclass.$init$(this);
            MapLike.Cclass.$init$(this);
            Cclass.$init$(this);
        }

        public <B> B $div$colon(B z, Function2<B, Tuple2<A, B>, B> op) {
            return TraversableOnce.Cclass.$div$colon(this, z, op);
        }

        public <B, That> That $plus$plus(TraversableOnce<B> that, CanBuildFrom<Map<A, B>, B, That> bf) {
            return TraversableLike.Cclass.$plus$plus(this, that, bf);
        }

        public StringBuilder addString(StringBuilder b, String start, String sep, String end) {
            return MapLike.Cclass.addString(this, b, start, sep, end);
        }

        public <C> PartialFunction<A, C> andThen(Function1<B, C> k) {
            return PartialFunction.Cclass.andThen(this, k);
        }

        public B apply(A key) {
            return MapLike.Cclass.apply(this, key);
        }

        public int apply$mcII$sp(int v1) {
            return Function1.Cclass.apply$mcII$sp(this, v1);
        }

        public void apply$mcVI$sp(int v1) {
            Function1.Cclass.apply$mcVI$sp(this, v1);
        }

        public void apply$mcVL$sp(long v1) {
            Function1.Cclass.apply$mcVL$sp(this, v1);
        }

        public boolean canEqual(Object that) {
            return IterableLike.Cclass.canEqual(this, that);
        }

        public GenericCompanion<Iterable> companion() {
            return Iterable.Cclass.companion(this);
        }

        public <A> Function1<A, B> compose(Function1<A, A> g) {
            return Function1.Cclass.compose(this, g);
        }

        public boolean contains(A key) {
            return MapLike.Cclass.contains(this, key);
        }

        public <B> void copyToArray(Object xs, int start) {
            TraversableOnce.Cclass.copyToArray(this, xs, start);
        }

        public <B> void copyToArray(Object xs, int start, int len) {
            IterableLike.Cclass.copyToArray(this, xs, start, len);
        }

        /* renamed from: default  reason: not valid java name */
        public B m10default(A key) {
            return MapLike.Cclass.m3default(this, key);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Map<A, B>, java.lang.Object] */
        public Map<A, B> drop(int n) {
            return TraversableLike.Cclass.drop(this, n);
        }

        public Map<A, B> empty() {
            return Cclass.empty(this);
        }

        public boolean equals(Object that) {
            return MapLike.Cclass.equals(this, that);
        }

        public boolean exists(Function1<Tuple2<A, B>, Boolean> p) {
            return IterableLike.Cclass.exists(this, p);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Map<A, B>, java.lang.Object] */
        public Map<A, B> filter(Function1<Tuple2<A, B>, Boolean> p) {
            return TraversableLike.Cclass.filter(this, p);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.Map, scala.collection.immutable.Map<A, B>] */
        public Map<A, B> filterNot(Function1<Tuple2<A, B>, Boolean> p) {
            return MapLike.Cclass.filterNot(this, p);
        }

        public <B> B foldLeft(B z, Function2<B, Tuple2<A, B>, B> op) {
            return TraversableOnce.Cclass.foldLeft(this, z, op);
        }

        public boolean forall(Function1<Tuple2<A, B>, Boolean> p) {
            return IterableLike.Cclass.forall(this, p);
        }

        public <B> Builder<B, Iterable<B>> genericBuilder() {
            return GenericTraversableTemplate.Cclass.genericBuilder(this);
        }

        public <B1> B1 getOrElse(A key, Function0<B1> function0) {
            return MapLike.Cclass.getOrElse(this, key, function0);
        }

        public int hashCode() {
            return MapLike.Cclass.hashCode(this);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.Tuple2<A, B>] */
        public Tuple2<A, B> head() {
            return IterableLike.Cclass.head(this);
        }

        public boolean isDefinedAt(A key) {
            return MapLike.Cclass.isDefinedAt(this, key);
        }

        public boolean isEmpty() {
            return MapLike.Cclass.isEmpty(this);
        }

        public final boolean isTraversableAgain() {
            return TraversableLike.Cclass.isTraversableAgain(this);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.Tuple2<A, B>] */
        public Tuple2<A, B> last() {
            return TraversableLike.Cclass.last(this);
        }

        public <B, That> That map(Function1<Tuple2<A, B>, B> f, CanBuildFrom<Map<A, B>, B, That> bf) {
            return TraversableLike.Cclass.map(this, f, bf);
        }

        public String mkString() {
            return TraversableOnce.Cclass.mkString(this);
        }

        public String mkString(String sep) {
            return TraversableOnce.Cclass.mkString(this, sep);
        }

        public String mkString(String start, String sep, String end) {
            return TraversableOnce.Cclass.mkString(this, start, sep, end);
        }

        public Builder<Tuple2<A, B>, Map<A, B>> newBuilder() {
            return MapLike.Cclass.newBuilder(this);
        }

        public boolean nonEmpty() {
            return TraversableOnce.Cclass.nonEmpty(this);
        }

        public <B> B reduceLeft(Function2<B, Tuple2<A, B>, B> op) {
            return TraversableOnce.Cclass.reduceLeft(this, op);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Map<A, B>, java.lang.Object] */
        public Map<A, B> repr() {
            return TraversableLike.Cclass.repr(this);
        }

        public <B> boolean sameElements(scala.collection.Iterable<B> that) {
            return IterableLike.Cclass.sameElements(this, that);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Map<A, B>, java.lang.Object] */
        public Map<A, B> slice(int from, int until) {
            return IterableLike.Cclass.slice(this, from, until);
        }

        public String stringPrefix() {
            return MapLike.Cclass.stringPrefix(this);
        }

        public <B> B sum(Numeric<B> num) {
            return TraversableOnce.Cclass.sum(this, num);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.immutable.Map<A, B>, java.lang.Object] */
        public Map<A, B> tail() {
            return TraversableLike.Cclass.tail(this);
        }

        public scala.collection.Iterable<Tuple2<A, B>> thisCollection() {
            return IterableLike.Cclass.thisCollection(this);
        }

        public <B> Object toArray(ClassManifest<B> evidence$1) {
            return TraversableOnce.Cclass.toArray(this, evidence$1);
        }

        public <B> Buffer<B> toBuffer() {
            return TraversableOnce.Cclass.toBuffer(this);
        }

        public List<Tuple2<A, B>> toList() {
            return TraversableOnce.Cclass.toList(this);
        }

        public <B> Set<B> toSet() {
            return TraversableOnce.Cclass.toSet(this);
        }

        public Stream<Tuple2<A, B>> toStream() {
            return IterableLike.Cclass.toStream(this);
        }

        public String toString() {
            return MapLike.Cclass.toString(this);
        }

        public <A1, B, That> That zip(scala.collection.Iterable<B> that, CanBuildFrom<Map<A, B>, Tuple2<A1, B>, That> bf) {
            return IterableLike.Cclass.zip(this, that, bf);
        }

        public int size() {
            return 4;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public scala.Option<B> get(A r6) {
            /*
                r5 = this;
                r4 = 1
                r3 = 0
                A r2 = r5.key1
                if (r6 != r2) goto L_0x0011
                r1 = r4
            L_0x0007:
                if (r1 == 0) goto L_0x0034
                scala.Some r1 = new scala.Some
                B r2 = r5.value1
                r1.<init>(r2)
            L_0x0010:
                return r1
            L_0x0011:
                if (r6 != 0) goto L_0x0015
                r1 = r3
                goto L_0x0007
            L_0x0015:
                boolean r1 = r6 instanceof java.lang.Number
                if (r1 == 0) goto L_0x0022
                r0 = r6
                java.lang.Number r0 = (java.lang.Number) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsNumObject(r1, r2)
                goto L_0x0007
            L_0x0022:
                boolean r1 = r6 instanceof java.lang.Character
                if (r1 == 0) goto L_0x002f
                r0 = r6
                java.lang.Character r0 = (java.lang.Character) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsCharObject(r1, r2)
                goto L_0x0007
            L_0x002f:
                boolean r1 = r6.equals(r2)
                goto L_0x0007
            L_0x0034:
                A r2 = r5.key2
                if (r6 != r2) goto L_0x0043
                r1 = r4
            L_0x0039:
                if (r1 == 0) goto L_0x0066
                scala.Some r1 = new scala.Some
                B r2 = r5.value2
                r1.<init>(r2)
                goto L_0x0010
            L_0x0043:
                if (r6 != 0) goto L_0x0047
                r1 = r3
                goto L_0x0039
            L_0x0047:
                boolean r1 = r6 instanceof java.lang.Number
                if (r1 == 0) goto L_0x0054
                r0 = r6
                java.lang.Number r0 = (java.lang.Number) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsNumObject(r1, r2)
                goto L_0x0039
            L_0x0054:
                boolean r1 = r6 instanceof java.lang.Character
                if (r1 == 0) goto L_0x0061
                r0 = r6
                java.lang.Character r0 = (java.lang.Character) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsCharObject(r1, r2)
                goto L_0x0039
            L_0x0061:
                boolean r1 = r6.equals(r2)
                goto L_0x0039
            L_0x0066:
                A r2 = r5.key3
                if (r6 != r2) goto L_0x0075
                r1 = r4
            L_0x006b:
                if (r1 == 0) goto L_0x0098
                scala.Some r1 = new scala.Some
                B r2 = r5.value3
                r1.<init>(r2)
                goto L_0x0010
            L_0x0075:
                if (r6 != 0) goto L_0x0079
                r1 = r3
                goto L_0x006b
            L_0x0079:
                boolean r1 = r6 instanceof java.lang.Number
                if (r1 == 0) goto L_0x0086
                r0 = r6
                java.lang.Number r0 = (java.lang.Number) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsNumObject(r1, r2)
                goto L_0x006b
            L_0x0086:
                boolean r1 = r6 instanceof java.lang.Character
                if (r1 == 0) goto L_0x0093
                r0 = r6
                java.lang.Character r0 = (java.lang.Character) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsCharObject(r1, r2)
                goto L_0x006b
            L_0x0093:
                boolean r1 = r6.equals(r2)
                goto L_0x006b
            L_0x0098:
                A r1 = r5.key4
                if (r6 != r1) goto L_0x00a8
                r1 = r4
            L_0x009d:
                if (r1 == 0) goto L_0x00c7
                scala.Some r1 = new scala.Some
                B r2 = r5.value4
                r1.<init>(r2)
                goto L_0x0010
            L_0x00a8:
                if (r6 != 0) goto L_0x00ac
                r1 = r3
                goto L_0x009d
            L_0x00ac:
                boolean r2 = r6 instanceof java.lang.Number
                if (r2 == 0) goto L_0x00b7
                java.lang.Number r6 = (java.lang.Number) r6
                boolean r1 = scala.runtime.BoxesRunTime.equalsNumObject(r6, r1)
                goto L_0x009d
            L_0x00b7:
                boolean r2 = r6 instanceof java.lang.Character
                if (r2 == 0) goto L_0x00c2
                java.lang.Character r6 = (java.lang.Character) r6
                boolean r1 = scala.runtime.BoxesRunTime.equalsCharObject(r6, r1)
                goto L_0x009d
            L_0x00c2:
                boolean r1 = r6.equals(r1)
                goto L_0x009d
            L_0x00c7:
                scala.None$ r1 = scala.None$.MODULE$
                goto L_0x0010
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.Map.Map4.get(java.lang.Object):scala.Option");
        }

        public Iterator<Tuple2<A, B>> iterator() {
            return Predef$.MODULE$.wrapRefArray((Object[]) new Tuple2[]{new Tuple2(this.key1, this.value1), new Tuple2(this.key2, this.value2), new Tuple2(this.key3, this.value3), new Tuple2(this.key4, this.value4)}).iterator();
        }

        /* Debug info: failed to restart local var, previous not found, register: 11 */
        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public <B1> scala.collection.immutable.Map<A, B1> updated(A r12, B1 r13) {
            /*
                r11 = this;
                r10 = 1
                r9 = 0
                A r2 = r11.key1
                if (r12 != r2) goto L_0x001e
                r1 = r10
            L_0x0007:
                if (r1 == 0) goto L_0x0041
                scala.collection.immutable.Map$Map4 r1 = new scala.collection.immutable.Map$Map4
                A r2 = r11.key1
                A r4 = r11.key2
                B r5 = r11.value2
                A r6 = r11.key3
                B r7 = r11.value3
                A r8 = r11.key4
                B r9 = r11.value4
                r3 = r13
                r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9)
            L_0x001d:
                return r1
            L_0x001e:
                if (r12 != 0) goto L_0x0022
                r1 = r9
                goto L_0x0007
            L_0x0022:
                boolean r1 = r12 instanceof java.lang.Number
                if (r1 == 0) goto L_0x002f
                r0 = r12
                java.lang.Number r0 = (java.lang.Number) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsNumObject(r1, r2)
                goto L_0x0007
            L_0x002f:
                boolean r1 = r12 instanceof java.lang.Character
                if (r1 == 0) goto L_0x003c
                r0 = r12
                java.lang.Character r0 = (java.lang.Character) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsCharObject(r1, r2)
                goto L_0x0007
            L_0x003c:
                boolean r1 = r12.equals(r2)
                goto L_0x0007
            L_0x0041:
                A r2 = r11.key2
                if (r12 != r2) goto L_0x005d
                r1 = r10
            L_0x0046:
                if (r1 == 0) goto L_0x0080
                scala.collection.immutable.Map$Map4 r1 = new scala.collection.immutable.Map$Map4
                A r2 = r11.key1
                B r3 = r11.value1
                A r4 = r11.key2
                A r6 = r11.key3
                B r7 = r11.value3
                A r8 = r11.key4
                B r9 = r11.value4
                r5 = r13
                r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9)
                goto L_0x001d
            L_0x005d:
                if (r12 != 0) goto L_0x0061
                r1 = r9
                goto L_0x0046
            L_0x0061:
                boolean r1 = r12 instanceof java.lang.Number
                if (r1 == 0) goto L_0x006e
                r0 = r12
                java.lang.Number r0 = (java.lang.Number) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsNumObject(r1, r2)
                goto L_0x0046
            L_0x006e:
                boolean r1 = r12 instanceof java.lang.Character
                if (r1 == 0) goto L_0x007b
                r0 = r12
                java.lang.Character r0 = (java.lang.Character) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsCharObject(r1, r2)
                goto L_0x0046
            L_0x007b:
                boolean r1 = r12.equals(r2)
                goto L_0x0046
            L_0x0080:
                A r2 = r11.key3
                if (r12 != r2) goto L_0x009c
                r1 = r10
            L_0x0085:
                if (r1 == 0) goto L_0x00bf
                scala.collection.immutable.Map$Map4 r1 = new scala.collection.immutable.Map$Map4
                A r2 = r11.key1
                B r3 = r11.value1
                A r4 = r11.key2
                B r5 = r11.value2
                A r6 = r11.key3
                A r8 = r11.key4
                B r9 = r11.value4
                r7 = r13
                r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9)
                goto L_0x001d
            L_0x009c:
                if (r12 != 0) goto L_0x00a0
                r1 = r9
                goto L_0x0085
            L_0x00a0:
                boolean r1 = r12 instanceof java.lang.Number
                if (r1 == 0) goto L_0x00ad
                r0 = r12
                java.lang.Number r0 = (java.lang.Number) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsNumObject(r1, r2)
                goto L_0x0085
            L_0x00ad:
                boolean r1 = r12 instanceof java.lang.Character
                if (r1 == 0) goto L_0x00ba
                r0 = r12
                java.lang.Character r0 = (java.lang.Character) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsCharObject(r1, r2)
                goto L_0x0085
            L_0x00ba:
                boolean r1 = r12.equals(r2)
                goto L_0x0085
            L_0x00bf:
                A r2 = r11.key4
                if (r12 != r2) goto L_0x00dc
                r1 = r10
            L_0x00c4:
                if (r1 == 0) goto L_0x00ff
                scala.collection.immutable.Map$Map4 r1 = new scala.collection.immutable.Map$Map4
                A r2 = r11.key1
                B r3 = r11.value1
                A r4 = r11.key2
                B r5 = r11.value2
                A r6 = r11.key3
                B r7 = r11.value3
                A r8 = r11.key4
                r9 = r13
                r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9)
                goto L_0x001d
            L_0x00dc:
                if (r12 != 0) goto L_0x00e0
                r1 = r9
                goto L_0x00c4
            L_0x00e0:
                boolean r1 = r12 instanceof java.lang.Number
                if (r1 == 0) goto L_0x00ed
                r0 = r12
                java.lang.Number r0 = (java.lang.Number) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsNumObject(r1, r2)
                goto L_0x00c4
            L_0x00ed:
                boolean r1 = r12 instanceof java.lang.Character
                if (r1 == 0) goto L_0x00fa
                r0 = r12
                java.lang.Character r0 = (java.lang.Character) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsCharObject(r1, r2)
                goto L_0x00c4
            L_0x00fa:
                boolean r1 = r12.equals(r2)
                goto L_0x00c4
            L_0x00ff:
                scala.collection.immutable.HashMap r2 = new scala.collection.immutable.HashMap
                r2.<init>()
                scala.Tuple2 r3 = new scala.Tuple2
                A r1 = r11.key1
                B r4 = r11.value1
                r3.<init>(r1, r4)
                scala.Tuple2 r4 = new scala.Tuple2
                A r1 = r11.key2
                B r5 = r11.value2
                r4.<init>(r1, r5)
                scala.Predef$ r5 = scala.Predef$.MODULE$
                r1 = 3
                scala.Tuple2[] r1 = new scala.Tuple2[r1]
                scala.Tuple2 r6 = new scala.Tuple2
                A r7 = r11.key3
                B r8 = r11.value3
                r6.<init>(r7, r8)
                r1[r9] = r6
                scala.Tuple2 r6 = new scala.Tuple2
                A r7 = r11.key4
                B r8 = r11.value4
                r6.<init>(r7, r8)
                r1[r10] = r6
                r6 = 2
                scala.Tuple2 r7 = new scala.Tuple2
                r7.<init>(r12, r13)
                r1[r6] = r7
                r0 = r1
                java.lang.Object[] r0 = (java.lang.Object[]) r0
                r11 = r0
                scala.collection.mutable.WrappedArray r1 = r5.wrapRefArray(r11)
                scala.collection.immutable.HashMap r1 = r2.$plus(r3, r4, r1)
                goto L_0x001d
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.Map.Map4.updated(java.lang.Object, java.lang.Object):scala.collection.immutable.Map");
        }

        public <B1> Map<A, B1> $plus(Tuple2<A, B1> kv) {
            return updated(kv._1(), kv._2());
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: A
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public scala.collection.immutable.Map<A, B> $minus(A r9) {
            /*
                r8 = this;
                r4 = 1
                r3 = 0
                A r2 = r8.key1
                if (r9 != r2) goto L_0x001b
                r1 = r4
            L_0x0007:
                if (r1 == 0) goto L_0x003e
                scala.collection.immutable.Map$Map3 r1 = new scala.collection.immutable.Map$Map3
                A r2 = r8.key2
                B r3 = r8.value2
                A r4 = r8.key3
                B r5 = r8.value3
                A r6 = r8.key4
                B r7 = r8.value4
                r1.<init>(r2, r3, r4, r5, r6, r7)
            L_0x001a:
                return r1
            L_0x001b:
                if (r9 != 0) goto L_0x001f
                r1 = r3
                goto L_0x0007
            L_0x001f:
                boolean r1 = r9 instanceof java.lang.Number
                if (r1 == 0) goto L_0x002c
                r0 = r9
                java.lang.Number r0 = (java.lang.Number) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsNumObject(r1, r2)
                goto L_0x0007
            L_0x002c:
                boolean r1 = r9 instanceof java.lang.Character
                if (r1 == 0) goto L_0x0039
                r0 = r9
                java.lang.Character r0 = (java.lang.Character) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsCharObject(r1, r2)
                goto L_0x0007
            L_0x0039:
                boolean r1 = r9.equals(r2)
                goto L_0x0007
            L_0x003e:
                A r2 = r8.key2
                if (r9 != r2) goto L_0x0057
                r1 = r4
            L_0x0043:
                if (r1 == 0) goto L_0x007a
                scala.collection.immutable.Map$Map3 r1 = new scala.collection.immutable.Map$Map3
                A r2 = r8.key1
                B r3 = r8.value1
                A r4 = r8.key3
                B r5 = r8.value3
                A r6 = r8.key4
                B r7 = r8.value4
                r1.<init>(r2, r3, r4, r5, r6, r7)
                goto L_0x001a
            L_0x0057:
                if (r9 != 0) goto L_0x005b
                r1 = r3
                goto L_0x0043
            L_0x005b:
                boolean r1 = r9 instanceof java.lang.Number
                if (r1 == 0) goto L_0x0068
                r0 = r9
                java.lang.Number r0 = (java.lang.Number) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsNumObject(r1, r2)
                goto L_0x0043
            L_0x0068:
                boolean r1 = r9 instanceof java.lang.Character
                if (r1 == 0) goto L_0x0075
                r0 = r9
                java.lang.Character r0 = (java.lang.Character) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsCharObject(r1, r2)
                goto L_0x0043
            L_0x0075:
                boolean r1 = r9.equals(r2)
                goto L_0x0043
            L_0x007a:
                A r2 = r8.key3
                if (r9 != r2) goto L_0x0093
                r1 = r4
            L_0x007f:
                if (r1 == 0) goto L_0x00b6
                scala.collection.immutable.Map$Map3 r1 = new scala.collection.immutable.Map$Map3
                A r2 = r8.key1
                B r3 = r8.value1
                A r4 = r8.key2
                B r5 = r8.value2
                A r6 = r8.key4
                B r7 = r8.value4
                r1.<init>(r2, r3, r4, r5, r6, r7)
                goto L_0x001a
            L_0x0093:
                if (r9 != 0) goto L_0x0097
                r1 = r3
                goto L_0x007f
            L_0x0097:
                boolean r1 = r9 instanceof java.lang.Number
                if (r1 == 0) goto L_0x00a4
                r0 = r9
                java.lang.Number r0 = (java.lang.Number) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsNumObject(r1, r2)
                goto L_0x007f
            L_0x00a4:
                boolean r1 = r9 instanceof java.lang.Character
                if (r1 == 0) goto L_0x00b1
                r0 = r9
                java.lang.Character r0 = (java.lang.Character) r0
                r1 = r0
                boolean r1 = scala.runtime.BoxesRunTime.equalsCharObject(r1, r2)
                goto L_0x007f
            L_0x00b1:
                boolean r1 = r9.equals(r2)
                goto L_0x007f
            L_0x00b6:
                A r1 = r8.key4
                if (r9 != r1) goto L_0x00d0
                r1 = r4
            L_0x00bb:
                if (r1 == 0) goto L_0x00ef
                scala.collection.immutable.Map$Map3 r1 = new scala.collection.immutable.Map$Map3
                A r2 = r8.key1
                B r3 = r8.value1
                A r4 = r8.key2
                B r5 = r8.value2
                A r6 = r8.key3
                B r7 = r8.value3
                r1.<init>(r2, r3, r4, r5, r6, r7)
                goto L_0x001a
            L_0x00d0:
                if (r9 != 0) goto L_0x00d4
                r1 = r3
                goto L_0x00bb
            L_0x00d4:
                boolean r2 = r9 instanceof java.lang.Number
                if (r2 == 0) goto L_0x00df
                java.lang.Number r9 = (java.lang.Number) r9
                boolean r1 = scala.runtime.BoxesRunTime.equalsNumObject(r9, r1)
                goto L_0x00bb
            L_0x00df:
                boolean r2 = r9 instanceof java.lang.Character
                if (r2 == 0) goto L_0x00ea
                java.lang.Character r9 = (java.lang.Character) r9
                boolean r1 = scala.runtime.BoxesRunTime.equalsCharObject(r9, r1)
                goto L_0x00bb
            L_0x00ea:
                boolean r1 = r9.equals(r1)
                goto L_0x00bb
            L_0x00ef:
                r1 = r8
                goto L_0x001a
            */
            throw new UnsupportedOperationException("Method not decompiled: scala.collection.immutable.Map.Map4.$minus(java.lang.Object):scala.collection.immutable.Map");
        }

        public <U> void foreach(Function1<Tuple2<A, B>, U> f) {
            f.apply(new Tuple2(this.key1, this.value1));
            f.apply(new Tuple2(this.key2, this.value2));
            f.apply(new Tuple2(this.key3, this.value3));
            f.apply(new Tuple2(this.key4, this.value4));
        }
    }
}
