package scala.collection;

import scala.ScalaObject;
import scala.collection.Set;
import scala.collection.SetLike;
import scala.collection.TraversableLike;
import scala.collection.generic.Addable;
import scala.collection.generic.Subtractable;
import scala.collection.mutable.AddingBuilder;
import scala.collection.mutable.Builder;
import scala.math.Numeric$IntIsIntegral$;
import scala.runtime.BoxesRunTime;

/* compiled from: SetLike.scala */
public interface SetLike<A, This extends SetLike<A, This> & Set<A>> extends IterableLike<A, This>, Addable<A, This>, Subtractable<A, This>, ScalaObject {
    boolean apply(Object obj);

    boolean contains(Object obj);

    This empty();

    boolean subsetOf(Set<A> set);

    /* renamed from: scala.collection.SetLike$class  reason: invalid class name */
    /* compiled from: SetLike.scala */
    public abstract class Cclass {
        public static void $init$(SetLike $this) {
        }

        public static Builder newBuilder(SetLike $this) {
            return new AddingBuilder($this.empty());
        }

        public static boolean isEmpty(SetLike $this) {
            return $this.size() == 0;
        }

        public static boolean apply(SetLike $this, Object elem) {
            return $this.contains(elem);
        }

        public static boolean subsetOf(SetLike $this, Set that$1) {
            return $this.forall(new SetLike$$anonfun$subsetOf$1($this, that$1));
        }

        public static String stringPrefix(SetLike $this) {
            return "Set";
        }

        public static String toString(SetLike $this) {
            return TraversableLike.Cclass.toString($this);
        }

        public static int hashCode(SetLike $this) {
            return BoxesRunTime.unboxToInt(((TraversableOnce) $this.map(new SetLike$$anonfun$hashCode$1($this), Set$.MODULE$.setCanBuildFrom())).sum(Numeric$IntIsIntegral$.MODULE$));
        }

        public static boolean equals(SetLike $this, Object that) {
            if (!(that instanceof Set)) {
                return false;
            }
            Set set = (Set) that;
            if ($this == set || (set.canEqual($this) && $this.size() == set.size() && liftedTree1$1($this, set))) {
                return true;
            }
            return false;
        }

        private static final boolean liftedTree1$1(SetLike $this, Set set) {
            try {
                return $this.subsetOf(set);
            } catch (ClassCastException e) {
                return false;
            }
        }
    }
}
