package scala.collection;

import scala.ScalaObject;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.SeqFactory;
import scala.collection.generic.TraversableFactory;
import scala.collection.mutable.Builder;
import scala.collection.mutable.ListBuffer;

/* compiled from: Seq.scala */
public final class Seq$ extends SeqFactory<Seq> implements ScalaObject {
    public static final Seq$ MODULE$ = null;
    private final int hashSeed = "Seq".hashCode();

    static {
        new Seq$();
    }

    private Seq$() {
        MODULE$ = this;
    }

    public int hashSeed() {
        return this.hashSeed;
    }

    public <A> CanBuildFrom<Seq<?>, A, Seq<A>> canBuildFrom() {
        return new TraversableFactory.GenericCanBuildFrom(this);
    }

    public <A> Builder<A, Seq<A>> newBuilder() {
        return new ListBuffer();
    }
}
