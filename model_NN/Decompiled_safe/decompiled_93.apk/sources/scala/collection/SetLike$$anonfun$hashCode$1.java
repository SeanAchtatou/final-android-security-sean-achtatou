package scala.collection;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;

/* compiled from: SetLike.scala */
public final class SetLike$$anonfun$hashCode$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;

    public SetLike$$anonfun$hashCode$1(SetLike<A, This> $outer) {
    }

    public final int apply(A a) {
        return a.hashCode();
    }
}
