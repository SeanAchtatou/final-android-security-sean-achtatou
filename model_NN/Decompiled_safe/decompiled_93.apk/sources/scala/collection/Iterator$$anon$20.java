package scala.collection;

import scala.Function0;
import scala.Function1;
import scala.Function2;
import scala.collection.Iterator;
import scala.collection.TraversableOnce;
import scala.collection.immutable.List;
import scala.collection.immutable.Set;
import scala.collection.immutable.Stream;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.StringBuilder;
import scala.math.Numeric;
import scala.reflect.ClassManifest;

/* compiled from: Iterator.scala */
public final class Iterator$$anon$20 implements Iterator<Object> {
    private final /* synthetic */ Iterator $outer;
    public volatile int bitmap$0;
    private Iterator<B> cur;
    private Iterator<B> it;
    private final /* synthetic */ Function0 that$1;

    /* JADX WARN: Type inference failed for: r3v0, types: [scala.collection.Iterator<A>, scala.Function0] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Iterator$$anon$20(scala.collection.Iterator r2, scala.collection.Iterator<A> r3) {
        /*
            r1 = this;
            if (r2 != 0) goto L_0x0008
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            r0.<init>()
            throw r0
        L_0x0008:
            r1.$outer = r2
            r1.that$1 = r3
            r1.<init>()
            scala.collection.TraversableOnce.Cclass.$init$(r1)
            scala.collection.Iterator.Cclass.$init$(r1)
            r1.cur = r2
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.collection.Iterator$$anon$20.<init>(scala.collection.Iterator, scala.Function0):void");
    }

    public <B> B $div$colon(B z, Function2<B, B, B> op) {
        return TraversableOnce.Cclass.$div$colon(this, z, op);
    }

    public StringBuilder addString(StringBuilder b, String start, String sep, String end) {
        return TraversableOnce.Cclass.addString(this, b, start, sep, end);
    }

    public <B> void copyToArray(Object xs, int start) {
        TraversableOnce.Cclass.copyToArray(this, xs, start);
    }

    public <B> void copyToArray(Object xs, int start, int len) {
        Iterator.Cclass.copyToArray(this, xs, start, len);
    }

    public Iterator<B> drop(int n) {
        return Iterator.Cclass.drop(this, n);
    }

    public boolean exists(Function1<B, Boolean> p) {
        return Iterator.Cclass.exists(this, p);
    }

    public <B> B foldLeft(B z, Function2<B, B, B> op) {
        return TraversableOnce.Cclass.foldLeft(this, z, op);
    }

    public boolean forall(Function1<B, Boolean> p) {
        return Iterator.Cclass.forall(this, p);
    }

    public <U> void foreach(Function1<B, U> f) {
        Iterator.Cclass.foreach(this, f);
    }

    public boolean isEmpty() {
        return Iterator.Cclass.isEmpty(this);
    }

    public boolean isTraversableAgain() {
        return Iterator.Cclass.isTraversableAgain(this);
    }

    public int length() {
        return Iterator.Cclass.length(this);
    }

    public <B> Iterator<B> map(Function1<B, B> f) {
        return Iterator.Cclass.map(this, f);
    }

    public String mkString() {
        return TraversableOnce.Cclass.mkString(this);
    }

    public String mkString(String sep) {
        return TraversableOnce.Cclass.mkString(this, sep);
    }

    public String mkString(String start, String sep, String end) {
        return TraversableOnce.Cclass.mkString(this, start, sep, end);
    }

    public boolean nonEmpty() {
        return TraversableOnce.Cclass.nonEmpty(this);
    }

    public <B> B reduceLeft(Function2<B, B, B> op) {
        return TraversableOnce.Cclass.reduceLeft(this, op);
    }

    public int size() {
        return TraversableOnce.Cclass.size(this);
    }

    public <B> B sum(Numeric<B> num) {
        return TraversableOnce.Cclass.sum(this, num);
    }

    public <B> Object toArray(ClassManifest<B> evidence$1) {
        return TraversableOnce.Cclass.toArray(this, evidence$1);
    }

    public <B> Buffer<B> toBuffer() {
        return TraversableOnce.Cclass.toBuffer(this);
    }

    public List<B> toList() {
        return TraversableOnce.Cclass.toList(this);
    }

    public <B> Set<B> toSet() {
        return TraversableOnce.Cclass.toSet(this);
    }

    public Stream<B> toStream() {
        return Iterator.Cclass.toStream(this);
    }

    public String toString() {
        return Iterator.Cclass.toString(this);
    }

    private Iterator<B> cur() {
        return this.cur;
    }

    private void cur_$eq(Iterator<B> iterator) {
        this.cur = iterator;
    }

    private Iterator<B> it() {
        if ((this.bitmap$0 & 1) == 0) {
            synchronized (this) {
                if ((this.bitmap$0 & 1) == 0) {
                    this.it = (Iterator) this.that$1.apply();
                    this.bitmap$0 |= 1;
                }
            }
            this.that$1 = null;
        }
        return this.it;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0024, code lost:
        if (1 != 0) goto L_0x0026;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean hasNext() {
        /*
            r3 = this;
            r2 = 1
            scala.collection.Iterator r0 = r3.cur()
            boolean r0 = r0.hasNext()
            if (r0 != 0) goto L_0x0026
            scala.collection.Iterator r0 = r3.cur()
            scala.collection.Iterator r1 = r3.$outer
            if (r0 != r1) goto L_0x0028
            scala.collection.Iterator r0 = r3.it()
            boolean r0 = r0.hasNext()
            if (r0 == 0) goto L_0x0028
            scala.collection.Iterator r0 = r3.it()
            r3.cur_$eq(r0)
            if (r2 == 0) goto L_0x0028
        L_0x0026:
            r0 = r2
        L_0x0027:
            return r0
        L_0x0028:
            r0 = 0
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.collection.Iterator$$anon$20.hasNext():boolean");
    }

    public B next() {
        hasNext();
        return cur().next();
    }
}
