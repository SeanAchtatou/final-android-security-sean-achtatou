package scala.collection;

import scala.Function1;
import scala.ScalaObject;
import scala.collection.generic.CanBuildFrom;
import scala.collection.immutable.Stream;
import scala.collection.mutable.Builder;
import scala.collection.mutable.StringBuilder;
import scala.runtime.IntRef;
import scala.runtime.ObjectRef;

/* compiled from: TraversableLike.scala */
public interface TraversableLike<A, Repr> extends TraversableOnce<A>, ScalaObject {
    <B, That> That $plus$plus(TraversableOnce<B> traversableOnce, CanBuildFrom<Repr, B, That> canBuildFrom);

    <B> void copyToArray(Object obj, int i, int i2);

    Repr drop(int i);

    boolean exists(Function1<A, Boolean> function1);

    Repr filter(Function1<A, Boolean> function1);

    Repr filterNot(Function1<A, Boolean> function1);

    boolean forall(Function1<A, Boolean> function1);

    <U> void foreach(Function1<A, U> function1);

    A head();

    boolean isEmpty();

    A last();

    <B, That> That map(Function1<A, B> function1, CanBuildFrom<Repr, B, That> canBuildFrom);

    Builder<A, Repr> newBuilder();

    Repr repr();

    String stringPrefix();

    Repr tail();

    Traversable<A> thisCollection();

    Stream<A> toStream();

    /* renamed from: scala.collection.TraversableLike$class  reason: invalid class name */
    /* compiled from: TraversableLike.scala */
    public abstract class Cclass {
        public static void $init$(TraversableLike $this) {
        }

        public static Object repr(TraversableLike $this) {
            return $this;
        }

        public static final boolean isTraversableAgain(TraversableLike $this) {
            return true;
        }

        public static Object $plus$plus(TraversableLike $this, TraversableOnce that, CanBuildFrom bf) {
            Builder b = bf.apply($this.repr());
            if (that instanceof IndexedSeqLike) {
                b.sizeHint($this, that.size());
            }
            b.$plus$plus$eq($this.thisCollection());
            b.$plus$plus$eq(that);
            return b.result();
        }

        public static Object map(TraversableLike $this, Function1 f$1, CanBuildFrom bf) {
            Builder b$1 = bf.apply($this.repr());
            b$1.sizeHint($this, b$1.sizeHint$default$2());
            $this.foreach(new TraversableLike$$anonfun$map$1($this, f$1, b$1));
            return b$1.result();
        }

        public static Object filter(TraversableLike $this, Function1 p$2) {
            Builder b$3 = $this.newBuilder();
            $this.foreach(new TraversableLike$$anonfun$filter$1($this, p$2, b$3));
            return b$3.result();
        }

        public static Object filterNot(TraversableLike $this, Function1 p$9) {
            return $this.filter(new TraversableLike$$anonfun$filterNot$1($this, p$9));
        }

        public static Object tail(TraversableLike traversableLike) {
            if (!traversableLike.isEmpty()) {
                return traversableLike.drop(1);
            }
            throw new UnsupportedOperationException("empty.tail");
        }

        public static Object last(TraversableLike $this) {
            ObjectRef lst$1 = new ObjectRef($this.head());
            $this.foreach(new TraversableLike$$anonfun$last$1($this, lst$1));
            return lst$1.elem;
        }

        public static Object drop(TraversableLike $this, int n$2) {
            Builder b$10 = $this.newBuilder();
            if (n$2 >= 0) {
                b$10.sizeHint($this, -n$2);
            }
            $this.foreach(new TraversableLike$$anonfun$drop$1($this, n$2, b$10, new IntRef(0)));
            return b$10.result();
        }

        public static String toString(TraversableLike traversableLike) {
            return traversableLike.mkString(new StringBuilder().append((Object) traversableLike.stringPrefix()).append((Object) "(").toString(), ", ", ")");
        }

        public static String stringPrefix(TraversableLike traversableLike) {
            String name = traversableLike.repr().getClass().getName();
            int lastIndexOf = name.lastIndexOf(46);
            if (lastIndexOf != -1) {
                name = name.substring(lastIndexOf + 1);
            }
            int indexOf = name.indexOf(36);
            if (indexOf != -1) {
                return name.substring(0, indexOf);
            }
            return name;
        }
    }
}
