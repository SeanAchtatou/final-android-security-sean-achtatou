package scala.collection;

import java.io.Serializable;
import scala.Function1;
import scala.collection.immutable.Range$$anon$2;
import scala.runtime.AbstractFunction1$mcVI$sp;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

/* compiled from: BitSetLike.scala */
public final class BitSetLike$$anonfun$foreach$1 extends AbstractFunction1$mcVI$sp implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ BitSetLike $outer;
    public final /* synthetic */ Function1 f$1;

    /* JADX WARN: Type inference failed for: r3v0, types: [scala.Function1, scala.collection.BitSetLike<This>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public BitSetLike$$anonfun$foreach$1(scala.collection.BitSetLike r2, scala.collection.BitSetLike<This> r3) {
        /*
            r1 = this;
            if (r2 != 0) goto L_0x0008
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            r0.<init>()
            throw r0
        L_0x0008:
            r1.$outer = r2
            r1.f$1 = r3
            r1.<init>()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.collection.BitSetLike$$anonfun$foreach$1.<init>(scala.collection.BitSetLike, scala.Function1):void");
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        apply(BoxesRunTime.unboxToInt(v1));
        return BoxedUnit.UNIT;
    }

    public final void apply(int i) {
        new Range$$anon$2(BitSetLike$.MODULE$.scala$collection$BitSetLike$$WordLength() * i, (i + 1) * BitSetLike$.MODULE$.scala$collection$BitSetLike$$WordLength()).foreach(new BitSetLike$$anonfun$foreach$1$$anonfun$apply$mcVI$sp$1(this, this.$outer.word(i)));
    }

    public void apply$mcVI$sp(int v1) {
        new Range$$anon$2(BitSetLike$.MODULE$.scala$collection$BitSetLike$$WordLength() * v1, (v1 + 1) * BitSetLike$.MODULE$.scala$collection$BitSetLike$$WordLength()).foreach(new BitSetLike$$anonfun$foreach$1$$anonfun$apply$mcVI$sp$1(this, this.$outer.word(v1)));
    }
}
