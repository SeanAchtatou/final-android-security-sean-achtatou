package scala.collection;

import scala.ScalaObject;
import scala.collection.generic.GenericTraversableTemplate;

/* compiled from: LinearSeq.scala */
public interface LinearSeq<A> extends Seq<A>, GenericTraversableTemplate<A, LinearSeq>, LinearSeqLike<A, LinearSeq<A>>, ScalaObject {

    /* renamed from: scala.collection.LinearSeq$class  reason: invalid class name */
    /* compiled from: LinearSeq.scala */
    public abstract class Cclass {
        public static void $init$(LinearSeq $this) {
        }
    }
}
