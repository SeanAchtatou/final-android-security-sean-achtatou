package scala.collection;

import scala.ScalaObject;
import scala.collection.generic.GenericTraversableTemplate;

/* compiled from: Traversable.scala */
public interface Traversable<A> extends TraversableLike<A, Traversable<A>>, GenericTraversableTemplate<A, Traversable>, ScalaObject {

    /* renamed from: scala.collection.Traversable$class  reason: invalid class name */
    /* compiled from: Traversable.scala */
    public abstract class Cclass {
        public static void $init$(Traversable $this) {
        }
    }
}
