package scala.collection;

import scala.Function1;
import scala.ScalaObject;
import scala.collection.BitSetLike;
import scala.collection.Set;
import scala.collection.immutable.Range$$anon$2;
import scala.collection.mutable.StringBuilder;
import scala.runtime.ObjectRef;

/* compiled from: BitSetLike.scala */
public interface BitSetLike<This extends BitSetLike<This> & Set<Integer>> extends SetLike<Integer, This>, ScalaObject {
    boolean contains(int i);

    Iterator iterator();

    int nwords();

    long word(int i);

    /* renamed from: scala.collection.BitSetLike$class  reason: invalid class name */
    /* compiled from: BitSetLike.scala */
    public abstract class Cclass {
        public static void $init$(BitSetLike $this) {
        }

        public static int size(BitSetLike $this) {
            int s = 0;
            int i = $this.nwords();
            while (i > 0) {
                i--;
                s += BitSetLike$.MODULE$.scala$collection$BitSetLike$$popCount($this.word(i));
            }
            return s;
        }

        public static Iterator iterator(BitSetLike $this) {
            return new BitSetLike$$anon$1($this);
        }

        public static void foreach(BitSetLike $this, Function1 f$1) {
            new Range$$anon$2(0, $this.nwords()).foreach$mVc$sp(new BitSetLike$$anonfun$foreach$1($this, f$1));
        }

        public static boolean contains(BitSetLike $this, int elem) {
            return elem >= 0 && ($this.word(elem >> BitSetLike$.MODULE$.LogWL()) & (1 << elem)) != 0;
        }

        public static StringBuilder addString(BitSetLike $this, StringBuilder sb$1, String start, String sep$1, String end) {
            sb$1.append(start);
            new Range$$anon$2(0, $this.nwords() * BitSetLike$.MODULE$.scala$collection$BitSetLike$$WordLength()).foreach$mVc$sp(new BitSetLike$$anonfun$addString$1($this, sb$1, sep$1, new ObjectRef("")));
            return sb$1.append(end);
        }

        public static String stringPrefix(BitSetLike $this) {
            return "BitSet";
        }
    }
}
