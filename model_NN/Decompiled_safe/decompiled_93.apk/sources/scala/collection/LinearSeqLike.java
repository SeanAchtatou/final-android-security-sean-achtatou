package scala.collection;

import scala.ScalaObject;
import scala.collection.LinearSeqLike;

/* compiled from: LinearSeqLike.scala */
public interface LinearSeqLike<A, Repr extends LinearSeqLike<A, Repr>> extends SeqLike<A, Repr>, ScalaObject {
    Iterator<A> iterator();

    LinearSeq<A> thisCollection();

    LinearSeq<A> toCollection(LinearSeqLike linearSeqLike);

    /* renamed from: scala.collection.LinearSeqLike$class  reason: invalid class name */
    /* compiled from: LinearSeqLike.scala */
    public abstract class Cclass {
        public static void $init$(LinearSeqLike $this) {
        }

        public static LinearSeq thisCollection(LinearSeqLike $this) {
            return (LinearSeq) $this;
        }

        public static LinearSeq toCollection(LinearSeqLike $this, LinearSeqLike repr) {
            return (LinearSeq) repr;
        }

        public static Iterator iterator(LinearSeqLike $this) {
            return new LinearSeqLike$$anon$1($this);
        }
    }
}
