package scala.collection;

import java.io.Serializable;
import scala.collection.mutable.StringBuilder;
import scala.runtime.AbstractFunction1$mcVI$sp;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.ObjectRef;

/* compiled from: BitSetLike.scala */
public final class BitSetLike$$anonfun$addString$1 extends AbstractFunction1$mcVI$sp implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ BitSetLike $outer;
    public final /* synthetic */ ObjectRef pre$1;
    public final /* synthetic */ StringBuilder sb$1;
    public final /* synthetic */ String sep$1;

    public BitSetLike$$anonfun$addString$1(BitSetLike $outer2, StringBuilder stringBuilder, String str, ObjectRef objectRef) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.sb$1 = stringBuilder;
        this.sep$1 = str;
        this.pre$1 = objectRef;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        apply(BoxesRunTime.unboxToInt(v1));
        return BoxedUnit.UNIT;
    }

    public final void apply(int i) {
        if (this.$outer.contains(i)) {
            this.sb$1.append((String) this.pre$1.elem).append(i);
            this.pre$1.elem = this.sep$1;
        }
    }

    public void apply$mcVI$sp(int v1) {
        if (this.$outer.contains(v1)) {
            this.sb$1.append((String) this.pre$1.elem).append(v1);
            this.pre$1.elem = this.sep$1;
        }
    }
}
