package scala.collection;

import java.io.Serializable;
import scala.Function1;
import scala.collection.mutable.Builder;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

/* compiled from: TraversableLike.scala */
public final class TraversableLike$$anonfun$filter$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    private final /* synthetic */ Builder b$3;
    private final /* synthetic */ Function1 p$2;

    public TraversableLike$$anonfun$filter$1(TraversableLike $outer, Function1 function1, Builder builder) {
        this.p$2 = function1;
        this.b$3 = builder;
    }

    public final Object apply(A x) {
        return BoxesRunTime.unboxToBoolean(this.p$2.apply(x)) ? this.b$3.$plus$eq((Object) x) : BoxedUnit.UNIT;
    }
}
