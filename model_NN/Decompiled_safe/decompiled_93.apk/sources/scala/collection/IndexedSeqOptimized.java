package scala.collection;

import scala.Function1;
import scala.Function2;
import scala.ScalaObject;
import scala.Tuple2;
import scala.collection.generic.CanBuildFrom;
import scala.collection.mutable.Builder;
import scala.runtime.BoxesRunTime;
import scala.runtime.RichInt;
import scala.runtime.ScalaRunTime$;

/* compiled from: IndexedSeqOptimized.scala */
public interface IndexedSeqOptimized<A, Repr> extends IndexedSeqLike<A, Repr>, ScalaObject {
    <U> void foreach(Function1<A, U> function1);

    boolean isEmpty();

    A last();

    <B> B reduceLeft(Function2<B, A, B> function2);

    Object scala$collection$IndexedSeqOptimized$$super$head();

    Object scala$collection$IndexedSeqOptimized$$super$last();

    Object scala$collection$IndexedSeqOptimized$$super$reduceLeft(Function2 function2);

    boolean scala$collection$IndexedSeqOptimized$$super$sameElements(Iterable iterable);

    Object scala$collection$IndexedSeqOptimized$$super$tail();

    Object scala$collection$IndexedSeqOptimized$$super$zip(Iterable iterable, CanBuildFrom canBuildFrom);

    int segmentLength(Function1<A, Boolean> function1, int i);

    Repr slice(int i, int i2);

    /* renamed from: scala.collection.IndexedSeqOptimized$class  reason: invalid class name */
    /* compiled from: IndexedSeqOptimized.scala */
    public abstract class Cclass {
        public static void $init$(IndexedSeqOptimized $this) {
        }

        public static boolean isEmpty(IndexedSeqOptimized $this) {
            return $this.length() == 0;
        }

        public static void foreach(IndexedSeqOptimized $this, Function1 f) {
            int len = $this.length();
            for (int i = 0; i < len; i++) {
                f.apply($this.apply(i));
            }
        }

        public static boolean forall(IndexedSeqOptimized $this, Function1 p$1) {
            return $this.prefixLength(new IndexedSeqOptimized$$anonfun$forall$1($this, p$1)) == $this.length();
        }

        public static boolean exists(IndexedSeqOptimized $this, Function1 p$2) {
            return $this.prefixLength(new IndexedSeqOptimized$$anonfun$exists$1($this, p$2)) != $this.length();
        }

        private static Object foldl(IndexedSeqOptimized $this, int start, int end, Object z, Function2 op) {
            for (int start2 = start; start2 != end; start2++) {
                z = op.apply(z, $this.apply(start2));
            }
            return z;
        }

        public static Object foldLeft(IndexedSeqOptimized $this, Object z, Function2 op) {
            return foldl($this, 0, $this.length(), z, op);
        }

        public static Object reduceLeft(IndexedSeqOptimized $this, Function2 op) {
            return $this.length() > 0 ? foldl($this, 1, $this.length(), $this.apply(0), op) : $this.scala$collection$IndexedSeqOptimized$$super$reduceLeft(op);
        }

        public static Object zip(IndexedSeqOptimized $this, Iterable that, CanBuildFrom bf) {
            if (!(that instanceof IndexedSeq)) {
                return $this.scala$collection$IndexedSeqOptimized$$super$zip(that, bf);
            }
            IndexedSeq indexedSeq = (IndexedSeq) that;
            Builder b = bf.apply($this.repr());
            int len = new RichInt($this.length()).min(indexedSeq.length());
            b.sizeHint(len);
            for (int i = 0; i < len; i++) {
                b.$plus$eq((Object) new Tuple2($this.apply(i), indexedSeq.apply(i)));
            }
            return b.result();
        }

        public static Object slice(IndexedSeqOptimized $this, int from, int until) {
            int i = new RichInt(from).max(0);
            int end = new RichInt(until).min($this.length());
            Builder b = $this.newBuilder();
            b.sizeHint(end - i);
            while (i < end) {
                b.$plus$eq($this.apply(i));
                i++;
            }
            return b.result();
        }

        public static Object head(IndexedSeqOptimized $this) {
            return $this.isEmpty() ? $this.scala$collection$IndexedSeqOptimized$$super$head() : $this.apply(0);
        }

        public static Object tail(IndexedSeqOptimized $this) {
            return $this.isEmpty() ? $this.scala$collection$IndexedSeqOptimized$$super$tail() : $this.slice(1, $this.length());
        }

        public static Object last(IndexedSeqOptimized $this) {
            return $this.length() > 0 ? $this.apply($this.length() - 1) : $this.scala$collection$IndexedSeqOptimized$$super$last();
        }

        public static Object drop(IndexedSeqOptimized $this, int n) {
            return $this.slice(n, $this.length());
        }

        public static boolean sameElements(IndexedSeqOptimized $this, Iterable that) {
            boolean z;
            if (!(that instanceof IndexedSeq)) {
                return $this.scala$collection$IndexedSeqOptimized$$super$sameElements(that);
            }
            IndexedSeq indexedSeq = (IndexedSeq) that;
            int len = $this.length();
            if (len == indexedSeq.length()) {
                int i = 0;
                while (i < len) {
                    Object apply = $this.apply(i);
                    Object apply2 = indexedSeq.apply(i);
                    if (!(apply == apply2 ? true : apply == null ? false : apply instanceof Number ? BoxesRunTime.equalsNumObject((Number) apply, apply2) : apply instanceof Character ? BoxesRunTime.equalsCharObject((Character) apply, apply2) : apply.equals(apply2))) {
                        break;
                    }
                    i++;
                }
                if (i == len) {
                    z = true;
                } else {
                    z = false;
                }
                if (z) {
                    return true;
                }
            }
            return false;
        }

        public static void copyToArray(IndexedSeqOptimized $this, Object xs, int start, int len) {
            int i = 0;
            int j = start;
            int end = new RichInt(new RichInt($this.length()).min(len)).min(ScalaRunTime$.MODULE$.array_length(xs) - start);
            while (i < end) {
                ScalaRunTime$.MODULE$.array_update(xs, j, $this.apply(i));
                i++;
                j++;
            }
        }

        public static int segmentLength(IndexedSeqOptimized $this, Function1 p, int from) {
            int len = $this.length();
            int i = from;
            while (i < len && BoxesRunTime.unboxToBoolean(p.apply($this.apply(i)))) {
                i++;
            }
            return i - from;
        }

        private static int negLength(IndexedSeqOptimized $this, int n) {
            if (n == $this.length()) {
                return -1;
            }
            return n;
        }

        public static int indexWhere(IndexedSeqOptimized $this, Function1 p$4, int from) {
            int start = new RichInt(from).max(0);
            return negLength($this, $this.segmentLength(new IndexedSeqOptimized$$anonfun$indexWhere$1($this, p$4), start) + start);
        }

        public static Object reverse(IndexedSeqOptimized $this) {
            Builder b = $this.newBuilder();
            b.sizeHint($this.length());
            int i = $this.length();
            while (i > 0) {
                i--;
                b.$plus$eq($this.apply(i));
            }
            return b.result();
        }

        public static Iterator reverseIterator(IndexedSeqOptimized $this) {
            return new IndexedSeqOptimized$$anon$1($this);
        }
    }
}
