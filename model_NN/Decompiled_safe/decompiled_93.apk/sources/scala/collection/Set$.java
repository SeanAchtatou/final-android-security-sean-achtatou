package scala.collection;

import scala.ScalaObject;
import scala.collection.generic.SetFactory;
import scala.collection.immutable.Set;
import scala.collection.immutable.Set$EmptySet$;
import scala.collection.mutable.Builder;

/* compiled from: Set.scala */
public final class Set$ extends SetFactory<Set> implements ScalaObject {
    public static final Set$ MODULE$ = null;

    static {
        new Set$();
    }

    private Set$() {
        MODULE$ = this;
    }

    public <A> Builder<A, Set<A>> newBuilder() {
        return scala.collection.immutable.Set$.MODULE$.newBuilder();
    }

    public <A> Set<A> empty() {
        return Set$EmptySet$.MODULE$;
    }
}
