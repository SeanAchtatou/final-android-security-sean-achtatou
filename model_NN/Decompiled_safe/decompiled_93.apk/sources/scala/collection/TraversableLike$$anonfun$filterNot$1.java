package scala.collection;

import java.io.Serializable;
import scala.Function1;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

/* compiled from: TraversableLike.scala */
public final class TraversableLike$$anonfun$filterNot$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Function1 p$9;

    /* JADX WARN: Type inference failed for: r2v0, types: [scala.collection.TraversableLike<A, Repr>, scala.Function1] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public TraversableLike$$anonfun$filterNot$1(scala.collection.TraversableLike r1, scala.collection.TraversableLike<A, Repr> r2) {
        /*
            r0 = this;
            r0.p$9 = r2
            r0.<init>()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.collection.TraversableLike$$anonfun$filterNot$1.<init>(scala.collection.TraversableLike, scala.Function1):void");
    }

    public final boolean apply(A a) {
        return !BoxesRunTime.unboxToBoolean(this.p$9.apply(a));
    }
}
