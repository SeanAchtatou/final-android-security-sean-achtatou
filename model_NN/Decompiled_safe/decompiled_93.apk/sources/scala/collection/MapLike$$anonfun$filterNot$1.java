package scala.collection;

import java.io.Serializable;
import scala.Function1;
import scala.Tuple2;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.ObjectRef;

/* compiled from: MapLike.scala */
public final class MapLike$$anonfun$filterNot$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Function1 p$2;
    public final /* synthetic */ ObjectRef res$1;

    public MapLike$$anonfun$filterNot$1(MapLike $outer, Function1 function1, ObjectRef objectRef) {
        this.p$2 = function1;
        this.res$1 = objectRef;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        apply((Tuple2) ((Tuple2) v1));
        return BoxedUnit.UNIT;
    }

    public final void apply(Tuple2<A, B> kv) {
        if (BoxesRunTime.unboxToBoolean(this.p$2.apply(kv))) {
            this.res$1.elem = ((Map) this.res$1.elem).$minus(kv._1());
        }
    }
}
