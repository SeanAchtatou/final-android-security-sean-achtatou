package scala.collection;

import java.io.Serializable;
import scala.collection.immutable.List;
import scala.runtime.AbstractFunction1;
import scala.runtime.ObjectRef;

/* compiled from: SeqLike.scala */
public final class SeqLike$$anonfun$reverse$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ ObjectRef xs$1;

    /* JADX WARN: Type inference failed for: r2v0, types: [scala.collection.SeqLike<A, Repr>, scala.runtime.ObjectRef] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SeqLike$$anonfun$reverse$1(scala.collection.SeqLike r1, scala.collection.SeqLike<A, Repr> r2) {
        /*
            r0 = this;
            r0.xs$1 = r2
            r0.<init>()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.collection.SeqLike$$anonfun$reverse$1.<init>(scala.collection.SeqLike, scala.runtime.ObjectRef):void");
    }

    public final void apply(A x) {
        this.xs$1.elem = ((List) this.xs$1.elem).$colon$colon(x);
    }
}
