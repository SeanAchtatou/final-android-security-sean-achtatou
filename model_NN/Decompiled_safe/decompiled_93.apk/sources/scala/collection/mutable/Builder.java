package scala.collection.mutable;

import scala.Function1;
import scala.ScalaObject;
import scala.collection.TraversableLike;
import scala.collection.generic.Growable;
import scala.runtime.RichInt;

/* compiled from: Builder.scala */
public interface Builder<Elem, To> extends Growable<Elem>, ScalaObject {
    Builder<Elem, To> $plus$eq(Object obj);

    <NewTo> Builder<Elem, NewTo> mapResult(Function1<To, NewTo> function1);

    To result();

    void sizeHint(int i);

    void sizeHint(TraversableLike<?, ?> traversableLike, int i);

    /* synthetic */ int sizeHint$default$2();

    void sizeHintBounded(int i, TraversableLike<?, ?> traversableLike);

    /* renamed from: scala.collection.mutable.Builder$class  reason: invalid class name */
    /* compiled from: Builder.scala */
    public abstract class Cclass {
        public static void $init$(Builder $this) {
        }

        public static void sizeHint(Builder $this, int size) {
        }

        public static /* synthetic */ int sizeHint$default$2(Builder $this) {
            return 0;
        }

        public static void sizeHint(Builder $this, TraversableLike coll, int delta) {
            if (coll instanceof IndexedSeqLike) {
                $this.sizeHint(coll.size() + delta);
            }
        }

        public static void sizeHintBounded(Builder $this, int size, TraversableLike boundingColl) {
            if (boundingColl instanceof IndexedSeqLike) {
                $this.sizeHint(new RichInt(size).min(boundingColl.size()));
            }
        }

        public static Builder mapResult(Builder $this, Function1 f$1) {
            return new Builder$$anon$1($this, f$1);
        }
    }
}
