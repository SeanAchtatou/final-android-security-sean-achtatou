package scala.collection.mutable;

import scala.ScalaObject;

/* compiled from: IndexedSeqLike.scala */
public interface IndexedSeqLike<A, Repr> extends scala.collection.IndexedSeqLike<A, Repr>, ScalaObject {
    IndexedSeq<A> thisCollection();

    IndexedSeq<A> toCollection(Object obj);

    /* renamed from: scala.collection.mutable.IndexedSeqLike$class  reason: invalid class name */
    /* compiled from: IndexedSeqLike.scala */
    public abstract class Cclass {
        public static void $init$(IndexedSeqLike $this) {
        }

        public static IndexedSeq thisCollection(IndexedSeqLike $this) {
            return (IndexedSeq) $this;
        }

        public static IndexedSeq toCollection(IndexedSeqLike $this, Object repr) {
            return (IndexedSeq) repr;
        }
    }
}
