package scala.collection.mutable;

import java.util.NoSuchElementException;
import scala.Function1;
import scala.ScalaObject;
import scala.collection.mutable.Cloneable;
import scala.runtime.BoxesRunTime;

/* compiled from: Queue.scala */
public class Queue<A> extends MutableList<A> implements Cloneable<Queue<A>>, ScalaObject, ScalaObject, Cloneable {
    public Queue() {
        Cloneable.Cclass.$init$(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.Queue<A>, java.lang.Object] */
    public Queue<A> clone() {
        return Cloneable.Cclass.clone(this);
    }

    public final Object scala$collection$mutable$Cloneable$$super$clone() {
        return super.clone();
    }

    public A dequeue() {
        if (isEmpty()) {
            throw new NoSuchElementException("queue empty");
        }
        Object res = first0().elem();
        first0_$eq((LinkedList) first0().next());
        len_$eq(len() - 1);
        return res;
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public Seq<A> dequeueAll(Function1<A, Boolean> p) {
        if (first0().isEmpty()) {
            return (Seq) Seq$.MODULE$.empty();
        }
        ArrayBuffer res = new ArrayBuffer();
        while (first0().nonEmpty() && BoxesRunTime.unboxToBoolean(p.apply(first0().elem()))) {
            res.$plus$eq(first0().elem());
            first0_$eq((LinkedList) first0().next());
            len_$eq(len() - 1);
        }
        if (first0().isEmpty()) {
            return res;
        }
        return removeAllFromList(p, res);
    }

    private ArrayBuffer<A> removeAllFromList(Function1<A, Boolean> p, ArrayBuffer<A> res) {
        LinkedList leftlst = first0();
        while (leftlst.next().nonEmpty()) {
            if (BoxesRunTime.unboxToBoolean(p.apply(((LinkedListLike) leftlst.next()).elem()))) {
                res.$plus$eq(((LinkedListLike) leftlst.next()).elem());
                if (leftlst.next() == last0()) {
                    last0_$eq(leftlst);
                }
                leftlst.next_$eq(((LinkedListLike) leftlst.next()).next());
                len_$eq(len() - 1);
            } else {
                leftlst = (LinkedList) leftlst.next();
            }
        }
        return res;
    }
}
