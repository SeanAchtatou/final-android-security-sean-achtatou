package scala.collection.mutable;

import java.io.Serializable;
import scala.runtime.AbstractFunction0;

/* compiled from: LinkedListLike.scala */
public final class LinkedListLike$$anonfun$tail$1 extends AbstractFunction0 implements Serializable {
    public static final long serialVersionUID = 0;

    public LinkedListLike$$anonfun$tail$1(LinkedListLike<A, This> $outer) {
    }

    public final String apply() {
        return "tail of empty list";
    }
}
