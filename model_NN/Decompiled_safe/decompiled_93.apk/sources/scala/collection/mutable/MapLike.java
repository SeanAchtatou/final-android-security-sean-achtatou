package scala.collection.mutable;

import scala.ScalaObject;
import scala.Tuple2;
import scala.collection.TraversableOnce;
import scala.collection.generic.Growable;
import scala.collection.generic.Shrinkable;
import scala.collection.mutable.Map;
import scala.collection.mutable.MapLike;

/* compiled from: MapLike.scala */
public interface MapLike<A, B, This extends MapLike<A, B, This> & Map<A, B>> extends scala.collection.MapLike<A, B, This>, Builder<Tuple2<A, B>, This>, Growable<Tuple2<A, B>>, Shrinkable<A>, Cloneable<This>, ScalaObject {
    This $minus(A a);

    MapLike<A, B, This> $minus$eq(A a);

    <B1> Map<A, B1> $plus(Tuple2<A, B1> tuple2);

    MapLike<A, B, This> $plus$eq(Tuple2 tuple2);

    This clone();

    This result();

    void update(A a, B b);

    /* renamed from: scala.collection.mutable.MapLike$class  reason: invalid class name */
    /* compiled from: MapLike.scala */
    public abstract class Cclass {
        public static void $init$(MapLike $this) {
        }

        public static Builder newBuilder(MapLike $this) {
            return (Builder) $this.empty();
        }

        public static Map $plus(MapLike $this, Tuple2 kv) {
            return (Map) $this.clone().$plus$eq(kv);
        }

        public static Map $minus(MapLike $this, Object key) {
            return (Map) $this.clone().$minus$eq(key);
        }

        public static Map clone(MapLike $this) {
            return (Map) ((Growable) $this.empty()).$plus$plus$eq((TraversableOnce) $this.repr());
        }

        public static Map result(MapLike $this) {
            return (Map) $this.repr();
        }
    }
}
