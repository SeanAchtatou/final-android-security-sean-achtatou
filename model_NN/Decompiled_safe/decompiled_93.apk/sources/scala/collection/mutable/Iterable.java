package scala.collection.mutable;

import scala.ScalaObject;
import scala.collection.IterableLike;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericTraversableTemplate;

/* compiled from: Iterable.scala */
public interface Iterable<A> extends Traversable<A>, scala.collection.Iterable<A>, GenericTraversableTemplate<A, Iterable>, IterableLike<A, Iterable<A>>, ScalaObject {

    /* renamed from: scala.collection.mutable.Iterable$class  reason: invalid class name */
    /* compiled from: Iterable.scala */
    public abstract class Cclass {
        public static void $init$(Iterable $this) {
        }

        public static GenericCompanion companion(Iterable $this) {
            return Iterable$.MODULE$;
        }
    }
}
