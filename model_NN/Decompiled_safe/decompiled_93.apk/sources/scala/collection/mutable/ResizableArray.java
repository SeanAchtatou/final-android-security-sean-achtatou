package scala.collection.mutable;

import scala.Array$;
import scala.Function1;
import scala.ScalaObject;
import scala.collection.generic.GenericTraversableTemplate;
import scala.math.package$;
import scala.runtime.BoxesRunTime;
import scala.runtime.RichInt;
import scala.runtime.ScalaRunTime$;

/* compiled from: ResizableArray.scala */
public interface ResizableArray<A> extends IndexedSeq<A>, GenericTraversableTemplate<A, ResizableArray>, IndexedSeqOptimized<A, ResizableArray<A>>, ScalaObject {
    A apply(int i);

    Object[] array();

    void array_$eq(Object[] objArr);

    void ensureSize(int i);

    int initialSize();

    int length();

    int size0();

    void size0_$eq(int i);

    /* renamed from: scala.collection.mutable.ResizableArray$class  reason: invalid class name */
    /* compiled from: ResizableArray.scala */
    public abstract class Cclass {
        public static void $init$(ResizableArray $this) {
            $this.array_$eq(new Object[package$.MODULE$.max($this.initialSize(), 1)]);
            $this.size0_$eq(0);
        }

        public static int length(ResizableArray $this) {
            return $this.size0();
        }

        public static Object apply(ResizableArray $this, int idx) {
            if (idx < $this.size0()) {
                return $this.array()[idx];
            }
            throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(idx).toString());
        }

        public static void foreach(ResizableArray $this, Function1 f) {
            for (int i = 0; i < $this.size(); i++) {
                f.apply($this.array()[i]);
            }
        }

        public static void copyToArray(ResizableArray $this, Object xs, int start, int len) {
            Array$.MODULE$.copy($this.array(), 0, xs, start, new RichInt(new RichInt(len).min(ScalaRunTime$.MODULE$.array_length(xs) - start)).min($this.length()));
        }

        public static void ensureSize(ResizableArray $this, int n) {
            if (n > $this.array().length) {
                int newsize = $this.array().length * 2;
                while (n > newsize) {
                    newsize *= 2;
                }
                Object[] newar = new Object[newsize];
                Array$.MODULE$.copy($this.array(), 0, newar, 0, $this.size0());
                $this.array_$eq(newar);
            }
        }
    }
}
