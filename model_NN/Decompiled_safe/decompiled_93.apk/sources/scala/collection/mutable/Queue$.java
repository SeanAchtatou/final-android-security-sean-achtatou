package scala.collection.mutable;

import java.io.Serializable;
import scala.ScalaObject;
import scala.collection.Seq;

/* compiled from: Queue.scala */
public final class Queue$ implements Serializable, ScalaObject {
    public static final Queue$ MODULE$ = null;

    static {
        new Queue$();
    }

    private Queue$() {
        MODULE$ = this;
    }

    public <A> Queue<A> apply(Seq<A> xs) {
        return (Queue) new Queue().$plus$plus$eq(xs);
    }
}
