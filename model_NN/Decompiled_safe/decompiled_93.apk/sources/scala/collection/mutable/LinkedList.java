package scala.collection.mutable;

import scala.Function1;
import scala.Function2;
import scala.PartialFunction;
import scala.ScalaObject;
import scala.Tuple2;
import scala.collection.Iterable;
import scala.collection.IterableLike;
import scala.collection.Iterator;
import scala.collection.LinearSeq;
import scala.collection.LinearSeqLike;
import scala.collection.Seq;
import scala.collection.SeqLike;
import scala.collection.Traversable;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericTraversableTemplate;
import scala.collection.immutable.List;
import scala.collection.immutable.Set;
import scala.collection.immutable.Stream;
import scala.collection.mutable.Iterable;
import scala.collection.mutable.LinearSeq;
import scala.collection.mutable.LinkedListLike;
import scala.collection.mutable.Seq;
import scala.collection.mutable.Traversable;
import scala.math.Numeric;
import scala.math.Ordering;
import scala.reflect.ClassManifest;
import scala.runtime.BoxesRunTime;

/* compiled from: LinkedList.scala */
public class LinkedList<A> implements LinearSeq<A>, GenericTraversableTemplate<A, LinkedList>, LinkedListLike<A, LinkedList<A>>, ScalaObject, LinkedListLike {
    public static final long serialVersionUID = -7308240733518833071L;
    private Object elem;
    private Seq next;

    public LinkedList() {
        TraversableOnce.Cclass.$init$(this);
        TraversableLike.Cclass.$init$(this);
        GenericTraversableTemplate.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        IterableLike.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Function1.Cclass.$init$(this);
        PartialFunction.Cclass.$init$(this);
        SeqLike.Cclass.$init$(this);
        Seq.Cclass.$init$(this);
        Seq.Cclass.$init$(this);
        LinearSeqLike.Cclass.$init$(this);
        LinearSeq.Cclass.$init$(this);
        LinearSeq.Cclass.$init$(this);
        LinkedListLike.Cclass.$init$(this);
        next_$eq(this);
    }

    public <B> B $div$colon(B z, Function2<B, A, B> op) {
        return TraversableOnce.Cclass.$div$colon(this, z, op);
    }

    public <B, That> That $plus$plus(TraversableOnce<B> that, CanBuildFrom<LinkedList<A>, B, That> bf) {
        return TraversableLike.Cclass.$plus$plus(this, that, bf);
    }

    public StringBuilder addString(StringBuilder b, String start, String sep, String end) {
        return TraversableOnce.Cclass.addString(this, b, start, sep, end);
    }

    public <C> PartialFunction<Integer, C> andThen(Function1<A, C> k) {
        return PartialFunction.Cclass.andThen(this, k);
    }

    public A apply(int n) {
        return LinkedListLike.Cclass.apply(this, n);
    }

    public /* bridge */ /* synthetic */ Object apply(Object v1) {
        return apply(BoxesRunTime.unboxToInt(v1));
    }

    public int apply$mcII$sp(int v1) {
        return Function1.Cclass.apply$mcII$sp(this, v1);
    }

    public void apply$mcVI$sp(int v1) {
        Function1.Cclass.apply$mcVI$sp(this, v1);
    }

    public void apply$mcVL$sp(long v1) {
        Function1.Cclass.apply$mcVL$sp(this, v1);
    }

    public boolean canEqual(Object that) {
        return IterableLike.Cclass.canEqual(this, that);
    }

    public <A> Function1<A, A> compose(Function1<A, Integer> g) {
        return Function1.Cclass.compose(this, g);
    }

    public boolean contains(Object elem2) {
        return SeqLike.Cclass.contains(this, elem2);
    }

    public <B> void copyToArray(Object xs, int start) {
        TraversableOnce.Cclass.copyToArray(this, xs, start);
    }

    public <B> void copyToArray(Object xs, int start, int len) {
        IterableLike.Cclass.copyToArray(this, xs, start, len);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.Seq, scala.collection.mutable.LinkedList<A>] */
    public LinkedList<A> drop(int n) {
        return LinkedListLike.Cclass.drop(this, n);
    }

    public Object elem() {
        return this.elem;
    }

    public void elem_$eq(Object obj) {
        this.elem = obj;
    }

    public boolean equals(Object that) {
        return SeqLike.Cclass.equals(this, that);
    }

    public boolean exists(Function1<A, Boolean> p) {
        return IterableLike.Cclass.exists(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.mutable.LinkedList<A>] */
    public LinkedList<A> filter(Function1<A, Boolean> p) {
        return TraversableLike.Cclass.filter(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.mutable.LinkedList<A>] */
    public LinkedList<A> filterNot(Function1<A, Boolean> p) {
        return TraversableLike.Cclass.filterNot(this, p);
    }

    public <B> B foldLeft(B z, Function2<B, A, B> op) {
        return TraversableOnce.Cclass.foldLeft(this, z, op);
    }

    public boolean forall(Function1<A, Boolean> p) {
        return IterableLike.Cclass.forall(this, p);
    }

    public <B> void foreach(Function1<A, B> f) {
        LinkedListLike.Cclass.foreach(this, f);
    }

    public <B> Builder<B, LinkedList<B>> genericBuilder() {
        return GenericTraversableTemplate.Cclass.genericBuilder(this);
    }

    public int hashCode() {
        return SeqLike.Cclass.hashCode(this);
    }

    public A head() {
        return LinkedListLike.Cclass.head(this);
    }

    public <B> int indexOf(B elem2) {
        return SeqLike.Cclass.indexOf(this, elem2);
    }

    public <B> int indexOf(B elem2, int from) {
        return SeqLike.Cclass.indexOf(this, elem2, from);
    }

    public int indexWhere(Function1<A, Boolean> p, int from) {
        return SeqLike.Cclass.indexWhere(this, p, from);
    }

    public boolean isDefinedAt(int idx) {
        return SeqLike.Cclass.isDefinedAt(this, idx);
    }

    public /* bridge */ /* synthetic */ boolean isDefinedAt(Object x) {
        return isDefinedAt(BoxesRunTime.unboxToInt(x));
    }

    public boolean isEmpty() {
        return LinkedListLike.Cclass.isEmpty(this);
    }

    public final boolean isTraversableAgain() {
        return TraversableLike.Cclass.isTraversableAgain(this);
    }

    public Iterator<A> iterator() {
        return LinkedListLike.Cclass.iterator(this);
    }

    public A last() {
        return TraversableLike.Cclass.last(this);
    }

    public int length() {
        return LinkedListLike.Cclass.length(this);
    }

    public <B, That> That map(Function1<A, B> f, CanBuildFrom<LinkedList<A>, B, That> bf) {
        return TraversableLike.Cclass.map(this, f, bf);
    }

    public String mkString() {
        return TraversableOnce.Cclass.mkString(this);
    }

    public String mkString(String sep) {
        return TraversableOnce.Cclass.mkString(this, sep);
    }

    public String mkString(String start, String sep, String end) {
        return TraversableOnce.Cclass.mkString(this, start, sep, end);
    }

    public Builder<A, LinkedList<A>> newBuilder() {
        return GenericTraversableTemplate.Cclass.newBuilder(this);
    }

    public Seq next() {
        return this.next;
    }

    public void next_$eq(Seq seq) {
        this.next = seq;
    }

    public boolean nonEmpty() {
        return TraversableOnce.Cclass.nonEmpty(this);
    }

    public int prefixLength(Function1<A, Boolean> p) {
        return SeqLike.Cclass.prefixLength(this, p);
    }

    public <B> B reduceLeft(Function2<B, A, B> op) {
        return TraversableOnce.Cclass.reduceLeft(this, op);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.mutable.LinkedList<A>] */
    public LinkedList<A> repr() {
        return TraversableLike.Cclass.repr(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.mutable.LinkedList<A>] */
    public LinkedList<A> reverse() {
        return SeqLike.Cclass.reverse(this);
    }

    public Iterator<A> reverseIterator() {
        return SeqLike.Cclass.reverseIterator(this);
    }

    public <B> boolean sameElements(scala.collection.Iterable<B> that) {
        return IterableLike.Cclass.sameElements(this, that);
    }

    public int segmentLength(Function1<A, Boolean> p, int from) {
        return SeqLike.Cclass.segmentLength(this, p, from);
    }

    public int size() {
        return SeqLike.Cclass.size(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.mutable.LinkedList<A>] */
    public LinkedList<A> slice(int from, int until) {
        return IterableLike.Cclass.slice(this, from, until);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.mutable.LinkedList<A>] */
    public <B> LinkedList<A> sortBy(Function1<A, B> f, Ordering<B> ord) {
        return SeqLike.Cclass.sortBy(this, f, ord);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.mutable.LinkedList<A>] */
    public <B> LinkedList<A> sorted(Ordering<B> ord) {
        return SeqLike.Cclass.sorted(this, ord);
    }

    public String stringPrefix() {
        return TraversableLike.Cclass.stringPrefix(this);
    }

    public <B> B sum(Numeric<B> num) {
        return TraversableOnce.Cclass.sum(this, num);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.Seq, scala.collection.mutable.LinkedList<A>] */
    public LinkedList<A> tail() {
        return LinkedListLike.Cclass.tail(this);
    }

    public scala.collection.LinearSeq<A> thisCollection() {
        return LinearSeqLike.Cclass.thisCollection(this);
    }

    public <B> Object toArray(ClassManifest<B> evidence$1) {
        return TraversableOnce.Cclass.toArray(this, evidence$1);
    }

    public <B> Buffer<B> toBuffer() {
        return TraversableOnce.Cclass.toBuffer(this);
    }

    public scala.collection.LinearSeq<A> toCollection(LinearSeq<A> repr) {
        return LinearSeqLike.Cclass.toCollection(this, repr);
    }

    public scala.collection.Seq<A> toCollection(LinkedList<A> repr) {
        return SeqLike.Cclass.toCollection(this, repr);
    }

    public List<A> toList() {
        return TraversableOnce.Cclass.toList(this);
    }

    public <B> Set<B> toSet() {
        return TraversableOnce.Cclass.toSet(this);
    }

    public Stream<A> toStream() {
        return IterableLike.Cclass.toStream(this);
    }

    public String toString() {
        return SeqLike.Cclass.toString(this);
    }

    public <A1, B, That> That zip(scala.collection.Iterable<B> that, CanBuildFrom<LinkedList<A>, Tuple2<A1, B>, That> bf) {
        return IterableLike.Cclass.zip(this, that, bf);
    }

    public LinkedList(A elem2, LinkedList<A> next2) {
        this();
        if (next2 != null) {
            elem_$eq(elem2);
            next_$eq(next2);
        }
    }

    public GenericCompanion<LinkedList> companion() {
        return LinkedList$.MODULE$;
    }
}
