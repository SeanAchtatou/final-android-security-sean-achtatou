package scala.collection.mutable;

import scala.ScalaObject;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.SeqFactory;
import scala.collection.generic.TraversableFactory;

/* compiled from: LinearSeq.scala */
public final class LinearSeq$ extends SeqFactory<LinearSeq> implements ScalaObject {
    public static final LinearSeq$ MODULE$ = null;

    static {
        new LinearSeq$();
    }

    private LinearSeq$() {
        MODULE$ = this;
    }

    public <A> CanBuildFrom<LinearSeq<?>, A, LinearSeq<A>> canBuildFrom() {
        return new TraversableFactory.GenericCanBuildFrom(this);
    }

    public <A> Builder<A, LinearSeq<A>> newBuilder() {
        return new MutableList();
    }
}
