package scala.collection.mutable;

import scala.Function1;
import scala.Function2;
import scala.PartialFunction;
import scala.ScalaObject;
import scala.Tuple2;
import scala.collection.IndexedSeq;
import scala.collection.IndexedSeqLike;
import scala.collection.IndexedSeqOptimized;
import scala.collection.Iterable;
import scala.collection.IterableLike;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.SeqLike;
import scala.collection.Traversable;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericTraversableTemplate;
import scala.collection.generic.Growable;
import scala.collection.immutable.List;
import scala.collection.immutable.Set;
import scala.collection.immutable.Stream;
import scala.collection.mutable.Builder;
import scala.collection.mutable.IndexedSeq;
import scala.collection.mutable.IndexedSeqLike;
import scala.collection.mutable.Iterable;
import scala.collection.mutable.Seq;
import scala.collection.mutable.Traversable;
import scala.math.Numeric;
import scala.math.Ordering;
import scala.reflect.ClassManifest;
import scala.runtime.BoxesRunTime;

/* compiled from: StringBuilder.scala */
public final class StringBuilder implements Builder<Character, String>, CharSequence, IndexedSeq<Character>, IndexedSeqOptimized<Character, IndexedSeq<Character>>, ScalaObject, IndexedSeqOptimized {
    public static final long serialVersionUID = -8525408645367278351L;
    private final StringBuilder underlying;

    public StringBuilder(StringBuilder underlying2) {
        this.underlying = underlying2;
        Growable.Cclass.$init$(this);
        Builder.Cclass.$init$(this);
        TraversableOnce.Cclass.$init$(this);
        TraversableLike.Cclass.$init$(this);
        GenericTraversableTemplate.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        IterableLike.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Function1.Cclass.$init$(this);
        PartialFunction.Cclass.$init$(this);
        SeqLike.Cclass.$init$(this);
        Seq.Cclass.$init$(this);
        Seq.Cclass.$init$(this);
        IndexedSeqLike.Cclass.$init$(this);
        IndexedSeq.Cclass.$init$(this);
        IndexedSeqLike.Cclass.$init$(this);
        IndexedSeq.Cclass.$init$(this);
        IndexedSeqOptimized.Cclass.$init$(this);
    }

    private StringBuilder underlying() {
        return this.underlying;
    }

    public <B> B $div$colon(B z, Function2<B, Character, B> op) {
        return TraversableOnce.Cclass.$div$colon(this, z, op);
    }

    public <B, That> That $plus$plus(TraversableOnce<B> that, CanBuildFrom<IndexedSeq<Character>, B, That> bf) {
        return TraversableLike.Cclass.$plus$plus(this, that, bf);
    }

    public Growable<Character> $plus$plus$eq(TraversableOnce<Character> xs) {
        return Growable.Cclass.$plus$plus$eq(this, xs);
    }

    public StringBuilder addString(StringBuilder b, String start, String sep, String end) {
        return TraversableOnce.Cclass.addString(this, b, start, sep, end);
    }

    public <C> PartialFunction<Integer, C> andThen(Function1<Character, C> k) {
        return PartialFunction.Cclass.andThen(this, k);
    }

    public /* bridge */ /* synthetic */ Object apply(Object v1) {
        return BoxesRunTime.boxToCharacter(apply(BoxesRunTime.unboxToInt(v1)));
    }

    public int apply$mcII$sp(int v1) {
        return Function1.Cclass.apply$mcII$sp(this, v1);
    }

    public void apply$mcVI$sp(int v1) {
        Function1.Cclass.apply$mcVI$sp(this, v1);
    }

    public void apply$mcVL$sp(long v1) {
        Function1.Cclass.apply$mcVL$sp(this, v1);
    }

    public boolean canEqual(Object that) {
        return IterableLike.Cclass.canEqual(this, that);
    }

    public GenericCompanion<IndexedSeq> companion() {
        return IndexedSeq.Cclass.companion(this);
    }

    public <A> Function1<A, Character> compose(Function1<A, Integer> g) {
        return Function1.Cclass.compose(this, g);
    }

    public boolean contains(Object elem) {
        return SeqLike.Cclass.contains(this, elem);
    }

    public <B> void copyToArray(Object xs, int start) {
        TraversableOnce.Cclass.copyToArray(this, xs, start);
    }

    public <B> void copyToArray(Object xs, int start, int len) {
        IndexedSeqOptimized.Cclass.copyToArray(this, xs, start, len);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.IndexedSeq<java.lang.Character>, java.lang.Object] */
    public IndexedSeq<Character> drop(int n) {
        return IndexedSeqOptimized.Cclass.drop(this, n);
    }

    public boolean equals(Object that) {
        return SeqLike.Cclass.equals(this, that);
    }

    public boolean exists(Function1<Character, Boolean> p) {
        return IndexedSeqOptimized.Cclass.exists(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.IndexedSeq<java.lang.Character>, java.lang.Object] */
    public IndexedSeq<Character> filter(Function1<Character, Boolean> p) {
        return TraversableLike.Cclass.filter(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.IndexedSeq<java.lang.Character>, java.lang.Object] */
    public IndexedSeq<Character> filterNot(Function1<Character, Boolean> p) {
        return TraversableLike.Cclass.filterNot(this, p);
    }

    public <B> B foldLeft(B z, Function2<B, Character, B> op) {
        return IndexedSeqOptimized.Cclass.foldLeft(this, z, op);
    }

    public boolean forall(Function1<Character, Boolean> p) {
        return IndexedSeqOptimized.Cclass.forall(this, p);
    }

    public <U> void foreach(Function1<Character, U> f) {
        IndexedSeqOptimized.Cclass.foreach(this, f);
    }

    public <B> Builder<B, IndexedSeq<B>> genericBuilder() {
        return GenericTraversableTemplate.Cclass.genericBuilder(this);
    }

    public int hashCode() {
        return SeqLike.Cclass.hashCode(this);
    }

    public Object head() {
        return IndexedSeqOptimized.Cclass.head(this);
    }

    public <B> int indexOf(B elem) {
        return SeqLike.Cclass.indexOf(this, elem);
    }

    public <B> int indexOf(B elem, int from) {
        return SeqLike.Cclass.indexOf(this, elem, from);
    }

    public int indexWhere(Function1<Character, Boolean> p, int from) {
        return IndexedSeqOptimized.Cclass.indexWhere(this, p, from);
    }

    public boolean isDefinedAt(int idx) {
        return SeqLike.Cclass.isDefinedAt(this, idx);
    }

    public /* bridge */ /* synthetic */ boolean isDefinedAt(Object x) {
        return isDefinedAt(BoxesRunTime.unboxToInt(x));
    }

    public boolean isEmpty() {
        return IndexedSeqOptimized.Cclass.isEmpty(this);
    }

    public final boolean isTraversableAgain() {
        return TraversableLike.Cclass.isTraversableAgain(this);
    }

    public Iterator<Character> iterator() {
        return IndexedSeqLike.Cclass.iterator(this);
    }

    public Object last() {
        return IndexedSeqOptimized.Cclass.last(this);
    }

    public <B, That> That map(Function1<Character, B> f, CanBuildFrom<IndexedSeq<Character>, B, That> bf) {
        return TraversableLike.Cclass.map(this, f, bf);
    }

    public <NewTo> Builder<Character, NewTo> mapResult(Function1<String, NewTo> f) {
        return Builder.Cclass.mapResult(this, f);
    }

    public String mkString() {
        return TraversableOnce.Cclass.mkString(this);
    }

    public String mkString(String sep) {
        return TraversableOnce.Cclass.mkString(this, sep);
    }

    public String mkString(String start, String sep, String end) {
        return TraversableOnce.Cclass.mkString(this, start, sep, end);
    }

    public Builder<Character, IndexedSeq<Character>> newBuilder() {
        return GenericTraversableTemplate.Cclass.newBuilder(this);
    }

    public boolean nonEmpty() {
        return TraversableOnce.Cclass.nonEmpty(this);
    }

    public int prefixLength(Function1<Character, Boolean> p) {
        return SeqLike.Cclass.prefixLength(this, p);
    }

    public <B> B reduceLeft(Function2<B, Character, B> op) {
        return IndexedSeqOptimized.Cclass.reduceLeft(this, op);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.IndexedSeq<java.lang.Character>, java.lang.Object] */
    public IndexedSeq<Character> repr() {
        return TraversableLike.Cclass.repr(this);
    }

    public Iterator<Character> reverseIterator() {
        return IndexedSeqOptimized.Cclass.reverseIterator(this);
    }

    public <B> boolean sameElements(scala.collection.Iterable<B> that) {
        return IndexedSeqOptimized.Cclass.sameElements(this, that);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$head() {
        return IterableLike.Cclass.head(this);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$last() {
        return TraversableLike.Cclass.last(this);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$reduceLeft(Function2 op) {
        return TraversableOnce.Cclass.reduceLeft(this, op);
    }

    public final boolean scala$collection$IndexedSeqOptimized$$super$sameElements(scala.collection.Iterable that) {
        return IterableLike.Cclass.sameElements(this, that);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$tail() {
        return TraversableLike.Cclass.tail(this);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$zip(scala.collection.Iterable that, CanBuildFrom bf) {
        return IterableLike.Cclass.zip(this, that, bf);
    }

    public int segmentLength(Function1<Character, Boolean> p, int from) {
        return IndexedSeqOptimized.Cclass.segmentLength(this, p, from);
    }

    public int size() {
        return SeqLike.Cclass.size(this);
    }

    public void sizeHint(int size) {
        Builder.Cclass.sizeHint(this, size);
    }

    public void sizeHint(TraversableLike<?, ?> coll, int delta) {
        Builder.Cclass.sizeHint(this, coll, delta);
    }

    public /* synthetic */ int sizeHint$default$2() {
        return Builder.Cclass.sizeHint$default$2(this);
    }

    public void sizeHintBounded(int size, TraversableLike<?, ?> boundingColl) {
        Builder.Cclass.sizeHintBounded(this, size, boundingColl);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.IndexedSeq<java.lang.Character>, java.lang.Object] */
    public IndexedSeq<Character> slice(int from, int until) {
        return IndexedSeqOptimized.Cclass.slice(this, from, until);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.IndexedSeq<java.lang.Character>, java.lang.Object] */
    public <B> IndexedSeq<Character> sortBy(Function1<Character, B> f, Ordering<B> ord) {
        return SeqLike.Cclass.sortBy(this, f, ord);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.IndexedSeq<java.lang.Character>, java.lang.Object] */
    public <B> IndexedSeq<Character> sorted(Ordering<B> ord) {
        return SeqLike.Cclass.sorted(this, ord);
    }

    public String stringPrefix() {
        return TraversableLike.Cclass.stringPrefix(this);
    }

    public <B> B sum(Numeric<B> num) {
        return TraversableOnce.Cclass.sum(this, num);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.IndexedSeq<java.lang.Character>, java.lang.Object] */
    public IndexedSeq<Character> tail() {
        return IndexedSeqOptimized.Cclass.tail(this);
    }

    public IndexedSeq<Character> thisCollection() {
        return IndexedSeqLike.Cclass.thisCollection(this);
    }

    public <B> Object toArray(ClassManifest<B> evidence$1) {
        return TraversableOnce.Cclass.toArray(this, evidence$1);
    }

    public <B> Buffer<B> toBuffer() {
        return TraversableOnce.Cclass.toBuffer(this);
    }

    public IndexedSeq<Character> toCollection(IndexedSeq<Character> repr) {
        return IndexedSeqLike.Cclass.toCollection(this, repr);
    }

    public List<Character> toList() {
        return TraversableOnce.Cclass.toList(this);
    }

    public <B> Set<B> toSet() {
        return TraversableOnce.Cclass.toSet(this);
    }

    public Stream<Character> toStream() {
        return IterableLike.Cclass.toStream(this);
    }

    public <A1, B, That> That zip(scala.collection.Iterable<B> that, CanBuildFrom<IndexedSeq<Character>, Tuple2<A1, B>, That> bf) {
        return IndexedSeqOptimized.Cclass.zip(this, that, bf);
    }

    public StringBuilder(int initCapacity, String initValue) {
        this(new StringBuilder(initValue.length() + initCapacity).append(initValue));
    }

    public StringBuilder() {
        this(16, "");
    }

    public int length() {
        return underlying().length();
    }

    public char charAt(int index) {
        return underlying().charAt(index);
    }

    public char apply(int index) {
        return underlying().charAt(index);
    }

    public CharSequence subSequence(int start, int end) {
        return underlying().substring(start, end);
    }

    public StringBuilder $plus$eq(char x) {
        underlying().append(x);
        return this;
    }

    public StringBuilder append(Object x) {
        underlying().append(String.valueOf(x));
        return this;
    }

    public StringBuilder append(String s) {
        underlying().append(s);
        return this;
    }

    public StringBuilder append(int x) {
        underlying().append(x);
        return this;
    }

    public StringBuilder append(float x) {
        underlying().append(x);
        return this;
    }

    public StringBuilder append(char x) {
        underlying().append(x);
        return this;
    }

    public StringBuilder reverse() {
        return new StringBuilder(new StringBuilder(underlying()).reverse());
    }

    public StringBuilder clone() {
        return new StringBuilder(new StringBuilder(underlying()));
    }

    public String toString() {
        return underlying().toString();
    }

    public String result() {
        return underlying().toString();
    }
}
