package scala.collection.mutable;

import scala.ScalaObject;

/* compiled from: HashEntry.scala */
public interface HashEntry<A, E> extends ScalaObject {
    A key();

    E next();

    void next_$eq(E e);

    /* renamed from: scala.collection.mutable.HashEntry$class  reason: invalid class name */
    /* compiled from: HashEntry.scala */
    public abstract class Cclass {
        public static void $init$(HashEntry $this) {
        }
    }
}
