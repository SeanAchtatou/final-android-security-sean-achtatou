package scala.collection.mutable;

import scala.ScalaObject;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericTraversableTemplate;

/* compiled from: IndexedSeq.scala */
public interface IndexedSeq<A> extends Seq<A>, scala.collection.IndexedSeq<A>, GenericTraversableTemplate<A, IndexedSeq>, IndexedSeqLike<A, IndexedSeq<A>>, ScalaObject {

    /* renamed from: scala.collection.mutable.IndexedSeq$class  reason: invalid class name */
    /* compiled from: IndexedSeq.scala */
    public abstract class Cclass {
        public static void $init$(IndexedSeq $this) {
        }

        public static GenericCompanion companion(IndexedSeq $this) {
            return IndexedSeq$.MODULE$;
        }
    }
}
