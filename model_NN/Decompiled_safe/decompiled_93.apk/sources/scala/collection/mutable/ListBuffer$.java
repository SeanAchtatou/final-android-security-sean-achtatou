package scala.collection.mutable;

import scala.ScalaObject;
import scala.collection.generic.SeqFactory;

/* compiled from: ListBuffer.scala */
public final class ListBuffer$ extends SeqFactory<ListBuffer> implements ScalaObject, ScalaObject {
    public static final ListBuffer$ MODULE$ = null;

    static {
        new ListBuffer$();
    }

    private ListBuffer$() {
        MODULE$ = this;
    }

    public <A> Builder<A, ListBuffer<A>> newBuilder() {
        return new GrowingBuilder(new ListBuffer());
    }
}
