package scala.collection.mutable;

import scala.Function1;
import scala.ScalaObject;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;

/* compiled from: HashTable.scala */
public interface HashTable<A> extends ScalaObject {
    int _loadFactor();

    void _loadFactor_$eq(int i);

    void addEntry(HashEntry hashEntry);

    boolean elemEquals(A a, A a2);

    int elemHashCode(A a);

    Iterator<HashEntry> entriesIterator();

    HashEntry findEntry(A a);

    <C> void foreachEntry(Function1<HashEntry, C> function1);

    int improve(int i);

    int index(int i);

    int initialSize();

    int initialThreshold();

    int loadFactor();

    HashEntry removeEntry(A a);

    HashEntry<A, HashEntry>[] table();

    int tableSize();

    void tableSize_$eq(int i);

    void table_$eq(HashEntry<A, HashEntry>[] hashEntryArr);

    int threshold();

    void threshold_$eq(int i);

    /* renamed from: scala.collection.mutable.HashTable$class  reason: invalid class name */
    /* compiled from: HashTable.scala */
    public abstract class Cclass {
        public static int loadFactor(HashTable $this) {
            return 750;
        }

        public static int initialSize(HashTable $this) {
            return 16;
        }

        public static int initialThreshold(HashTable $this) {
            return newThreshold($this, initialCapacity($this));
        }

        public static void $init$(HashTable $this) {
            $this._loadFactor_$eq($this.loadFactor());
            $this.table_$eq(new HashEntry[initialCapacity($this)]);
            $this.tableSize_$eq(0);
            $this.threshold_$eq($this.initialThreshold());
        }

        private static int initialCapacity(HashTable $this) {
            return capacity($this, $this.initialSize());
        }

        private static int capacity(HashTable $this, int expectedSize) {
            if (expectedSize == 0) {
                return 1;
            }
            int c0 = expectedSize - 1;
            int c02 = c0 | (c0 >>> 1);
            int c03 = c02 | (c02 >>> 2);
            int c04 = c03 | (c03 >>> 4);
            int c05 = c04 | (c04 >>> 8);
            return (c05 | (c05 >>> 16)) + 1;
        }

        public static HashEntry findEntry(HashTable $this, Object key) {
            HashEntry e = $this.table()[$this.index($this.elemHashCode(key))];
            while (e != null && !$this.elemEquals(e.key(), key)) {
                e = (HashEntry) e.next();
            }
            return e;
        }

        public static void addEntry(HashTable $this, HashEntry e) {
            int h = $this.index($this.elemHashCode(e.key()));
            e.next_$eq($this.table()[h]);
            $this.table()[h] = e;
            $this.tableSize_$eq($this.tableSize() + 1);
            if ($this.tableSize() > $this.threshold()) {
                resize($this, $this.table().length * 2);
            }
        }

        public static HashEntry removeEntry(HashTable $this, Object key) {
            int h = $this.index($this.elemHashCode(key));
            HashEntry e = $this.table()[h];
            if (e != null) {
                if ($this.elemEquals(e.key(), key)) {
                    $this.table()[h] = (HashEntry) e.next();
                    $this.tableSize_$eq($this.tableSize() - 1);
                    return e;
                }
                HashEntry e1 = (HashEntry) e.next();
                while (e1 != null && !$this.elemEquals(e1.key(), key)) {
                    e = e1;
                    e1 = (HashEntry) e1.next();
                }
                if (e1 != null) {
                    e.next_$eq(e1.next());
                    $this.tableSize_$eq($this.tableSize() - 1);
                    return e1;
                }
            }
            return null;
        }

        public static Iterator entriesIterator(HashTable $this) {
            return new HashTable$$anon$1($this);
        }

        public static final void foreachEntry(HashTable $this, Function1 f) {
            $this.entriesIterator().foreach(f);
        }

        private static int newThreshold(HashTable $this, int size) {
            return (int) ((((long) size) * ((long) $this._loadFactor())) / 1000);
        }

        private static void resize(HashTable $this, int newSize) {
            HashEntry[] oldTable = $this.table();
            $this.table_$eq(new HashEntry[newSize]);
            for (int i = oldTable.length - 1; i >= 0; i--) {
                for (HashEntry e = oldTable[i]; e != null; e = (HashEntry) e.next()) {
                    int h = $this.index($this.elemHashCode(e.key()));
                    e.next_$eq($this.table()[h]);
                    $this.table()[h] = e;
                }
            }
            $this.threshold_$eq(newThreshold($this, newSize));
        }

        public static boolean elemEquals(HashTable $this, Object key1, Object key2) {
            return key1 == key2 ? true : key1 == null ? false : key1 instanceof Number ? BoxesRunTime.equalsNumObject((Number) key1, key2) : key1 instanceof Character ? BoxesRunTime.equalsCharObject((Character) key1, key2) : key1.equals(key2);
        }

        public static int elemHashCode(HashTable $this, Object key) {
            if (key == null) {
                return 0;
            }
            return key instanceof Number ? BoxesRunTime.hashFromNumber((Number) key) : key.hashCode();
        }

        public static final int improve(HashTable $this, int hcode) {
            int h = hcode + ((hcode << 9) ^ -1);
            int h2 = h ^ (h >>> 14);
            int h3 = h2 + (h2 << 4);
            return (h3 >>> 10) ^ h3;
        }

        public static final int index(HashTable $this, int hcode) {
            return $this.improve(hcode) & ($this.table().length - 1);
        }
    }
}
