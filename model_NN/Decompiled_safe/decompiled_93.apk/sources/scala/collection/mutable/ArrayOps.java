package scala.collection.mutable;

import scala.Array$;
import scala.Function1;
import scala.Function2;
import scala.None$;
import scala.ScalaObject;
import scala.Tuple2;
import scala.collection.IndexedSeqLike;
import scala.collection.IndexedSeqOptimized;
import scala.collection.Iterable;
import scala.collection.IterableLike;
import scala.collection.Iterator;
import scala.collection.SeqLike;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.CanBuildFrom;
import scala.collection.immutable.List;
import scala.collection.immutable.Nil$;
import scala.collection.immutable.Set;
import scala.collection.immutable.Stream;
import scala.collection.mutable.ArrayBuilder;
import scala.collection.mutable.ArrayLike;
import scala.collection.mutable.IndexedSeqLike;
import scala.collection.mutable.WrappedArray;
import scala.math.Numeric;
import scala.math.Ordering;
import scala.reflect.ClassManifest;
import scala.runtime.RichInt;
import scala.runtime.ScalaRunTime$;

/* compiled from: ArrayOps.scala */
public abstract class ArrayOps<T> implements ArrayLike<T, Object>, ScalaObject {
    public ArrayOps() {
        TraversableOnce.Cclass.$init$(this);
        TraversableLike.Cclass.$init$(this);
        IterableLike.Cclass.$init$(this);
        SeqLike.Cclass.$init$(this);
        IndexedSeqLike.Cclass.$init$(this);
        IndexedSeqLike.Cclass.$init$(this);
        IndexedSeqOptimized.Cclass.$init$(this);
        ArrayLike.Cclass.$init$(this);
    }

    public <B> B $div$colon(B z, Function2<B, T, B> op) {
        return TraversableOnce.Cclass.$div$colon(this, z, op);
    }

    public <B, That> That $plus$plus(TraversableOnce<B> that, CanBuildFrom<Object, B, That> bf) {
        return TraversableLike.Cclass.$plus$plus(this, that, bf);
    }

    public StringBuilder addString(StringBuilder b, String start, String sep, String end) {
        return TraversableOnce.Cclass.addString(this, b, start, sep, end);
    }

    public boolean canEqual(Object that) {
        return IterableLike.Cclass.canEqual(this, that);
    }

    public boolean contains(Object elem) {
        return SeqLike.Cclass.contains(this, elem);
    }

    public <B> void copyToArray(Object xs, int start) {
        TraversableOnce.Cclass.copyToArray(this, xs, start);
    }

    public Object drop(int n) {
        return IndexedSeqOptimized.Cclass.drop(this, n);
    }

    public boolean equals(Object that) {
        return SeqLike.Cclass.equals(this, that);
    }

    public boolean exists(Function1<T, Boolean> p) {
        return IndexedSeqOptimized.Cclass.exists(this, p);
    }

    public Object filter(Function1<T, Boolean> p) {
        return TraversableLike.Cclass.filter(this, p);
    }

    public Object filterNot(Function1<T, Boolean> p) {
        return TraversableLike.Cclass.filterNot(this, p);
    }

    public <B> B foldLeft(B z, Function2<B, T, B> op) {
        return IndexedSeqOptimized.Cclass.foldLeft(this, z, op);
    }

    public boolean forall(Function1<T, Boolean> p) {
        return IndexedSeqOptimized.Cclass.forall(this, p);
    }

    public <U> void foreach(Function1<T, U> f) {
        IndexedSeqOptimized.Cclass.foreach(this, f);
    }

    public int hashCode() {
        return SeqLike.Cclass.hashCode(this);
    }

    public T head() {
        return IndexedSeqOptimized.Cclass.head(this);
    }

    public <B> int indexOf(B elem) {
        return SeqLike.Cclass.indexOf(this, elem);
    }

    public <B> int indexOf(B elem, int from) {
        return SeqLike.Cclass.indexOf(this, elem, from);
    }

    public int indexWhere(Function1<T, Boolean> p, int from) {
        return IndexedSeqOptimized.Cclass.indexWhere(this, p, from);
    }

    public boolean isDefinedAt(int idx) {
        return SeqLike.Cclass.isDefinedAt(this, idx);
    }

    public boolean isEmpty() {
        return IndexedSeqOptimized.Cclass.isEmpty(this);
    }

    public final boolean isTraversableAgain() {
        return TraversableLike.Cclass.isTraversableAgain(this);
    }

    public Iterator<T> iterator() {
        return IndexedSeqLike.Cclass.iterator(this);
    }

    public T last() {
        return IndexedSeqOptimized.Cclass.last(this);
    }

    public <B, That> That map(Function1<T, B> f, CanBuildFrom<Object, B, That> bf) {
        return TraversableLike.Cclass.map(this, f, bf);
    }

    public String mkString() {
        return TraversableOnce.Cclass.mkString(this);
    }

    public String mkString(String sep) {
        return TraversableOnce.Cclass.mkString(this, sep);
    }

    public String mkString(String start, String sep, String end) {
        return TraversableOnce.Cclass.mkString(this, start, sep, end);
    }

    public boolean nonEmpty() {
        return TraversableOnce.Cclass.nonEmpty(this);
    }

    public int prefixLength(Function1<T, Boolean> p) {
        return SeqLike.Cclass.prefixLength(this, p);
    }

    public <B> B reduceLeft(Function2<B, T, B> op) {
        return IndexedSeqOptimized.Cclass.reduceLeft(this, op);
    }

    public Object repr() {
        return TraversableLike.Cclass.repr(this);
    }

    public Object reverse() {
        return IndexedSeqOptimized.Cclass.reverse(this);
    }

    public Iterator<T> reverseIterator() {
        return IndexedSeqOptimized.Cclass.reverseIterator(this);
    }

    public <B> boolean sameElements(Iterable<B> that) {
        return IndexedSeqOptimized.Cclass.sameElements(this, that);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$head() {
        return IterableLike.Cclass.head(this);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$last() {
        return TraversableLike.Cclass.last(this);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$reduceLeft(Function2 op) {
        return TraversableOnce.Cclass.reduceLeft(this, op);
    }

    public final boolean scala$collection$IndexedSeqOptimized$$super$sameElements(Iterable that) {
        return IterableLike.Cclass.sameElements(this, that);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$tail() {
        return TraversableLike.Cclass.tail(this);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$zip(Iterable that, CanBuildFrom bf) {
        return IterableLike.Cclass.zip(this, that, bf);
    }

    public int segmentLength(Function1<T, Boolean> p, int from) {
        return IndexedSeqOptimized.Cclass.segmentLength(this, p, from);
    }

    public int size() {
        return SeqLike.Cclass.size(this);
    }

    public Object slice(int from, int until) {
        return IndexedSeqOptimized.Cclass.slice(this, from, until);
    }

    public <B> Object sortBy(Function1<T, B> f, Ordering<B> ord) {
        return SeqLike.Cclass.sortBy(this, f, ord);
    }

    public <B> Object sorted(Ordering<B> ord) {
        return SeqLike.Cclass.sorted(this, ord);
    }

    public String stringPrefix() {
        return TraversableLike.Cclass.stringPrefix(this);
    }

    public <B> B sum(Numeric<B> num) {
        return TraversableOnce.Cclass.sum(this, num);
    }

    public Object tail() {
        return IndexedSeqOptimized.Cclass.tail(this);
    }

    public IndexedSeq<T> thisCollection() {
        return IndexedSeqLike.Cclass.thisCollection(this);
    }

    public <B> Buffer<B> toBuffer() {
        return TraversableOnce.Cclass.toBuffer(this);
    }

    public IndexedSeq<T> toCollection(Object repr) {
        return IndexedSeqLike.Cclass.toCollection(this, repr);
    }

    public List<T> toList() {
        return TraversableOnce.Cclass.toList(this);
    }

    public <B> Set<B> toSet() {
        return TraversableOnce.Cclass.toSet(this);
    }

    public Stream<T> toStream() {
        return IterableLike.Cclass.toStream(this);
    }

    public String toString() {
        return SeqLike.Cclass.toString(this);
    }

    public <A1, B, That> That zip(Iterable<B> that, CanBuildFrom<Object, Tuple2<A1, B>, That> bf) {
        return IndexedSeqOptimized.Cclass.zip(this, that, bf);
    }

    public <U> void copyToArray(Object xs, int start, int len) {
        int l = len;
        if (ScalaRunTime$.MODULE$.array_length(repr()) < len) {
            l = ScalaRunTime$.MODULE$.array_length(repr());
        }
        if (ScalaRunTime$.MODULE$.array_length(xs) - start < l) {
            l = new RichInt(ScalaRunTime$.MODULE$.array_length(xs) - start).max(0);
        }
        Array$.MODULE$.copy(repr(), 0, xs, start, l);
    }

    public <U> Object toArray(ClassManifest<U> evidence$1) {
        if (evidence$1.erasure() == repr().getClass().getComponentType()) {
            return repr();
        }
        return TraversableOnce.Cclass.toArray(this, evidence$1);
    }

    /* compiled from: ArrayOps.scala */
    public static class ofRef<T> extends ArrayOps<T> implements ArrayLike<T, T[]>, ScalaObject {
        private final T[] repr;

        public ofRef(T[] repr2) {
            this.repr = repr2;
        }

        public T[] repr() {
            return this.repr;
        }

        public WrappedArray<T> thisCollection() {
            return new WrappedArray.ofRef(repr());
        }

        public WrappedArray<T> toCollection(T[] repr2) {
            return new WrappedArray.ofRef(repr2);
        }

        public ArrayBuilder.ofRef<T> newBuilder() {
            return new ArrayBuilder.ofRef<>(new ClassManifest.ClassTypeManifest(None$.MODULE$, repr().getClass().getComponentType(), Nil$.MODULE$));
        }

        public int length() {
            return repr().length;
        }

        public T apply(int index) {
            return repr()[index];
        }
    }
}
