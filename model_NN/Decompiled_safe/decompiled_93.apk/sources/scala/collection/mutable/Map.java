package scala.collection.mutable;

import scala.ScalaObject;
import scala.Tuple2;

/* compiled from: Map.scala */
public interface Map<A, B> extends Iterable<Tuple2<A, B>>, scala.collection.Map<A, B>, MapLike<A, B, Map<A, B>>, ScalaObject {

    /* renamed from: scala.collection.mutable.Map$class  reason: invalid class name */
    /* compiled from: Map.scala */
    public abstract class Cclass {
        public static void $init$(Map $this) {
        }
    }
}
