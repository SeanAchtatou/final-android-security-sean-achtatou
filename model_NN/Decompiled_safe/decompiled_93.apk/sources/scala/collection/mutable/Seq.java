package scala.collection.mutable;

import scala.ScalaObject;
import scala.collection.SeqLike;
import scala.collection.generic.GenericTraversableTemplate;

/* compiled from: Seq.scala */
public interface Seq<A> extends Iterable<A>, scala.collection.Seq<A>, GenericTraversableTemplate<A, Seq>, SeqLike<A, Seq<A>>, ScalaObject {

    /* renamed from: scala.collection.mutable.Seq$class  reason: invalid class name */
    /* compiled from: Seq.scala */
    public abstract class Cclass {
        public static void $init$(Seq $this) {
        }
    }
}
