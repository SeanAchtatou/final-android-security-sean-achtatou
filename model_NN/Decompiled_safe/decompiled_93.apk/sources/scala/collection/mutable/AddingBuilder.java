package scala.collection.mutable;

import scala.Function1;
import scala.ScalaObject;
import scala.collection.Iterable;
import scala.collection.IterableLike;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.Addable;
import scala.collection.generic.Growable;
import scala.collection.mutable.Builder;

/* compiled from: AddingBuilder.scala */
public class AddingBuilder<Elem, To extends Addable<Elem, To> & Iterable<Elem> & IterableLike<Elem, To>> implements Builder<Elem, To>, ScalaObject {
    private To elems;
    private final To empty;

    public AddingBuilder(To empty2) {
        this.empty = empty2;
        Growable.Cclass.$init$(this);
        Builder.Cclass.$init$(this);
        this.elems = empty2;
    }

    public Growable<Elem> $plus$plus$eq(TraversableOnce<Elem> xs) {
        return Growable.Cclass.$plus$plus$eq(this, xs);
    }

    public <NewTo> Builder<Elem, NewTo> mapResult(Function1<To, NewTo> f) {
        return Builder.Cclass.mapResult(this, f);
    }

    public void sizeHint(int size) {
        Builder.Cclass.sizeHint(this, size);
    }

    public void sizeHint(TraversableLike<?, ?> coll, int delta) {
        Builder.Cclass.sizeHint(this, coll, delta);
    }

    public /* synthetic */ int sizeHint$default$2() {
        return Builder.Cclass.sizeHint$default$2(this);
    }

    public void sizeHintBounded(int size, TraversableLike<?, ?> boundingColl) {
        Builder.Cclass.sizeHintBounded(this, size, boundingColl);
    }

    public To elems() {
        return this.elems;
    }

    public void elems_$eq(To to) {
        this.elems = to;
    }

    public AddingBuilder<Elem, To> $plus$eq(Elem x) {
        elems_$eq(elems().$plus(x));
        return this;
    }

    public To result() {
        return elems();
    }
}
