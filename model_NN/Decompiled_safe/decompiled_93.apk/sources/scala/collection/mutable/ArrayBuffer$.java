package scala.collection.mutable;

import scala.ScalaObject;
import scala.collection.generic.SeqFactory;

/* compiled from: ArrayBuffer.scala */
public final class ArrayBuffer$ extends SeqFactory<ArrayBuffer> implements ScalaObject, ScalaObject {
    public static final ArrayBuffer$ MODULE$ = null;

    static {
        new ArrayBuffer$();
    }

    private ArrayBuffer$() {
        MODULE$ = this;
    }

    public <A> Builder<A, ArrayBuffer<A>> newBuilder() {
        return new ArrayBuffer();
    }
}
