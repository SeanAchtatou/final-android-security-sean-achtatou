package scala.collection.mutable;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;

/* compiled from: Stack.scala */
public final class Stack$$anonfun$pushAll$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Stack $outer;

    public Stack$$anonfun$pushAll$1(Stack<A> $outer2) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
    }

    public final Stack<A> apply(A a) {
        return this.$outer.push(a);
    }
}
