package scala.collection.mutable;

import scala.Function1;
import scala.Predef$;
import scala.ScalaObject;
import scala.collection.Iterator;
import scala.collection.SeqLike;
import scala.collection.mutable.LinkedListLike;
import scala.collection.mutable.Seq;
import scala.runtime.BoxesRunTime;

/* compiled from: LinkedListLike.scala */
public interface LinkedListLike<A, This extends Seq<A> & LinkedListLike<A, This>> extends SeqLike<A, This>, ScalaObject {
    A apply(int i);

    This drop(int i);

    A elem();

    void elem_$eq(A a);

    A head();

    boolean isEmpty();

    Iterator<A> iterator();

    int length();

    This next();

    void next_$eq(This thisR);

    This tail();

    /* renamed from: scala.collection.mutable.LinkedListLike$class  reason: invalid class name */
    /* compiled from: LinkedListLike.scala */
    public abstract class Cclass {
        public static void $init$(LinkedListLike $this) {
        }

        public static boolean isEmpty(LinkedListLike $this) {
            return $this.next() == $this;
        }

        public static int length(LinkedListLike $this) {
            if ($this.isEmpty()) {
                return 0;
            }
            return ((LinkedListLike) $this.next()).length() + 1;
        }

        public static Object head(LinkedListLike $this) {
            return $this.elem();
        }

        public static Seq tail(LinkedListLike $this) {
            Predef$.MODULE$.require($this.nonEmpty(), new LinkedListLike$$anonfun$tail$1($this));
            return $this.next();
        }

        public static Seq drop(LinkedListLike $this, int n) {
            Seq these = (Seq) $this.repr();
            for (int i = 0; i < n && !((LinkedListLike) these).isEmpty(); i++) {
                these = ((LinkedListLike) these).next();
            }
            return these;
        }

        private static Object atLocation(LinkedListLike $this, int n, Function1 f) {
            Seq loc = $this.drop(n);
            if (!((LinkedListLike) loc).isEmpty()) {
                return f.apply(loc);
            }
            throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(n).toString());
        }

        public static Object apply(LinkedListLike $this, int n) {
            return atLocation($this, n, new LinkedListLike$$anonfun$apply$1($this));
        }

        public static Iterator iterator(LinkedListLike $this) {
            return new LinkedListLike$$anon$1($this);
        }

        public static void foreach(LinkedListLike $this, Function1 f) {
            for (LinkedListLike these = $this; these.nonEmpty(); these = (LinkedListLike) these.next()) {
                f.apply(these.elem());
            }
        }
    }
}
