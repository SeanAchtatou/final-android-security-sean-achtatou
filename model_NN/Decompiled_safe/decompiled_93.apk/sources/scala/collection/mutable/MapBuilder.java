package scala.collection.mutable;

import scala.Function1;
import scala.ScalaObject;
import scala.Tuple2;
import scala.collection.Map;
import scala.collection.MapLike;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.Growable;
import scala.collection.mutable.Builder;

/* compiled from: MapBuilder.scala */
public class MapBuilder<A, B, Coll extends Map<A, B> & MapLike<A, B, Coll>> implements Builder<Tuple2<A, B>, Coll>, ScalaObject {
    private Coll elems;
    private final Coll empty;

    public MapBuilder(Coll empty2) {
        this.empty = empty2;
        Growable.Cclass.$init$(this);
        Builder.Cclass.$init$(this);
        this.elems = empty2;
    }

    public Growable<Tuple2<A, B>> $plus$plus$eq(TraversableOnce<Tuple2<A, B>> xs) {
        return Growable.Cclass.$plus$plus$eq(this, xs);
    }

    public <NewTo> Builder<Tuple2<A, B>, NewTo> mapResult(Function1<Coll, NewTo> f) {
        return Builder.Cclass.mapResult(this, f);
    }

    public void sizeHint(int size) {
        Builder.Cclass.sizeHint(this, size);
    }

    public void sizeHint(TraversableLike<?, ?> coll, int delta) {
        Builder.Cclass.sizeHint(this, coll, delta);
    }

    public /* synthetic */ int sizeHint$default$2() {
        return Builder.Cclass.sizeHint$default$2(this);
    }

    public void sizeHintBounded(int size, TraversableLike<?, ?> boundingColl) {
        Builder.Cclass.sizeHintBounded(this, size, boundingColl);
    }

    public Coll elems() {
        return this.elems;
    }

    public void elems_$eq(Coll coll) {
        this.elems = coll;
    }

    public MapBuilder<A, B, Coll> $plus$eq(Tuple2<A, B> x) {
        elems_$eq(elems().$plus(x));
        return this;
    }

    public Coll result() {
        return elems();
    }
}
