package scala.collection.mutable;

import scala.Array$;
import scala.Function1;
import scala.ScalaObject;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.Growable;
import scala.collection.mutable.Builder;
import scala.reflect.ClassManifest;

/* compiled from: WrappedArrayBuilder.scala */
public class WrappedArrayBuilder<A> implements Builder<A, WrappedArray<A>>, ScalaObject {
    private int capacity = 0;
    private WrappedArray<A> elems;
    private final ClassManifest<A> manifest;
    private int size = 0;

    public WrappedArrayBuilder(ClassManifest<A> manifest2) {
        this.manifest = manifest2;
        Growable.Cclass.$init$(this);
        Builder.Cclass.$init$(this);
    }

    public Growable<A> $plus$plus$eq(TraversableOnce<A> xs) {
        return Growable.Cclass.$plus$plus$eq(this, xs);
    }

    public <NewTo> Builder<A, NewTo> mapResult(Function1<WrappedArray<A>, NewTo> f) {
        return Builder.Cclass.mapResult(this, f);
    }

    public void sizeHint(TraversableLike<?, ?> coll, int delta) {
        Builder.Cclass.sizeHint(this, coll, delta);
    }

    public /* synthetic */ int sizeHint$default$2() {
        return Builder.Cclass.sizeHint$default$2(this);
    }

    public void sizeHintBounded(int size2, TraversableLike<?, ?> boundingColl) {
        Builder.Cclass.sizeHintBounded(this, size2, boundingColl);
    }

    private WrappedArray<A> elems() {
        return this.elems;
    }

    private void elems_$eq(WrappedArray<A> wrappedArray) {
        this.elems = wrappedArray;
    }

    private int capacity() {
        return this.capacity;
    }

    private void capacity_$eq(int i) {
        this.capacity = i;
    }

    private int size() {
        return this.size;
    }

    private void size_$eq(int i) {
        this.size = i;
    }

    private WrappedArray<A> mkArray(int size2) {
        WrappedArray newelems = this.manifest.newWrappedArray(size2);
        if (size() > 0) {
            Array$.MODULE$.copy(elems().array(), 0, newelems.array(), 0, size());
        }
        return newelems;
    }

    private void resize(int size2) {
        elems_$eq(mkArray(size2));
        capacity_$eq(size2);
    }

    public void sizeHint(int size2) {
        if (capacity() < size2) {
            resize(size2);
        }
    }

    private void ensureSize(int size2) {
        if (capacity() < size2) {
            int newsize = capacity() == 0 ? 16 : capacity() * 2;
            while (newsize < size2) {
                newsize *= 2;
            }
            resize(newsize);
        }
    }

    public WrappedArrayBuilder<A> $plus$eq(A elem) {
        ensureSize(size() + 1);
        elems().update(size(), elem);
        size_$eq(size() + 1);
        return this;
    }

    public WrappedArray<A> result() {
        if (capacity() == 0 || capacity() != size()) {
            return mkArray(size());
        }
        return elems();
    }
}
