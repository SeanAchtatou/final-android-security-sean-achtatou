package scala.collection.mutable;

import scala.Function1;
import scala.Function2;
import scala.PartialFunction;
import scala.ScalaObject;
import scala.Tuple2;
import scala.collection.Iterable;
import scala.collection.IterableLike;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.SeqLike;
import scala.collection.Traversable;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericTraversableTemplate;
import scala.collection.immutable.List;
import scala.collection.immutable.Nil$;
import scala.collection.immutable.Set;
import scala.collection.immutable.Stream;
import scala.collection.mutable.Cloneable;
import scala.math.Numeric;
import scala.math.Ordering;
import scala.reflect.ClassManifest;
import scala.runtime.BoxesRunTime;

/* compiled from: Stack.scala */
public class Stack<A> implements Seq<A>, Cloneable<Stack<A>>, ScalaObject, Seq, Cloneable {
    private List<A> elems;

    private Stack(List<A> elems2) {
        this.elems = elems2;
        Function1.Cclass.$init$(this);
        PartialFunction.Cclass.$init$(this);
        TraversableOnce.Cclass.$init$(this);
        TraversableLike.Cclass.$init$(this);
        GenericTraversableTemplate.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        IterableLike.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        SeqLike.Cclass.$init$(this);
        Seq.Cclass.$init$(this);
        Cloneable.Cclass.$init$(this);
    }

    public <B> B $div$colon(B z, Function2<B, A, B> op) {
        return TraversableOnce.Cclass.$div$colon(this, z, op);
    }

    public <B, That> That $plus$plus(TraversableOnce<B> that, CanBuildFrom<Seq<A>, B, That> bf) {
        return TraversableLike.Cclass.$plus$plus(this, that, bf);
    }

    public StringBuilder addString(StringBuilder b, String start, String sep, String end) {
        return TraversableOnce.Cclass.addString(this, b, start, sep, end);
    }

    public <C> PartialFunction<Integer, C> andThen(Function1<A, C> k) {
        return PartialFunction.Cclass.andThen(this, k);
    }

    public /* bridge */ /* synthetic */ Object apply(Object v1) {
        return apply(BoxesRunTime.unboxToInt(v1));
    }

    public int apply$mcII$sp(int v1) {
        return Function1.Cclass.apply$mcII$sp(this, v1);
    }

    public void apply$mcVI$sp(int v1) {
        Function1.Cclass.apply$mcVI$sp(this, v1);
    }

    public void apply$mcVL$sp(long v1) {
        Function1.Cclass.apply$mcVL$sp(this, v1);
    }

    public boolean canEqual(Object that) {
        return IterableLike.Cclass.canEqual(this, that);
    }

    public GenericCompanion<Seq> companion() {
        return Seq.Cclass.companion(this);
    }

    public <A> Function1<A, A> compose(Function1<A, Integer> g) {
        return Function1.Cclass.compose(this, g);
    }

    public boolean contains(Object elem) {
        return SeqLike.Cclass.contains(this, elem);
    }

    public <B> void copyToArray(Object xs, int start) {
        TraversableOnce.Cclass.copyToArray(this, xs, start);
    }

    public <B> void copyToArray(Object xs, int start, int len) {
        IterableLike.Cclass.copyToArray(this, xs, start, len);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.Seq<A>, java.lang.Object] */
    public Seq<A> drop(int n) {
        return TraversableLike.Cclass.drop(this, n);
    }

    public List<A> elems() {
        return this.elems;
    }

    public void elems_$eq(List<A> list) {
        this.elems = list;
    }

    public boolean equals(Object that) {
        return SeqLike.Cclass.equals(this, that);
    }

    public boolean exists(Function1<A, Boolean> p) {
        return IterableLike.Cclass.exists(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.Seq<A>, java.lang.Object] */
    public Seq<A> filter(Function1<A, Boolean> p) {
        return TraversableLike.Cclass.filter(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.Seq<A>, java.lang.Object] */
    public Seq<A> filterNot(Function1<A, Boolean> p) {
        return TraversableLike.Cclass.filterNot(this, p);
    }

    public <B> B foldLeft(B z, Function2<B, A, B> op) {
        return TraversableOnce.Cclass.foldLeft(this, z, op);
    }

    public boolean forall(Function1<A, Boolean> p) {
        return IterableLike.Cclass.forall(this, p);
    }

    public <B> Builder<B, Seq<B>> genericBuilder() {
        return GenericTraversableTemplate.Cclass.genericBuilder(this);
    }

    public int hashCode() {
        return SeqLike.Cclass.hashCode(this);
    }

    public A head() {
        return IterableLike.Cclass.head(this);
    }

    public <B> int indexOf(B elem) {
        return SeqLike.Cclass.indexOf(this, elem);
    }

    public <B> int indexOf(B elem, int from) {
        return SeqLike.Cclass.indexOf(this, elem, from);
    }

    public int indexWhere(Function1<A, Boolean> p, int from) {
        return SeqLike.Cclass.indexWhere(this, p, from);
    }

    public boolean isDefinedAt(int idx) {
        return SeqLike.Cclass.isDefinedAt(this, idx);
    }

    public /* bridge */ /* synthetic */ boolean isDefinedAt(Object x) {
        return isDefinedAt(BoxesRunTime.unboxToInt(x));
    }

    public final boolean isTraversableAgain() {
        return TraversableLike.Cclass.isTraversableAgain(this);
    }

    public A last() {
        return TraversableLike.Cclass.last(this);
    }

    public <B, That> That map(Function1<A, B> f, CanBuildFrom<Seq<A>, B, That> bf) {
        return TraversableLike.Cclass.map(this, f, bf);
    }

    public String mkString() {
        return TraversableOnce.Cclass.mkString(this);
    }

    public String mkString(String sep) {
        return TraversableOnce.Cclass.mkString(this, sep);
    }

    public String mkString(String start, String sep, String end) {
        return TraversableOnce.Cclass.mkString(this, start, sep, end);
    }

    public Builder<A, Seq<A>> newBuilder() {
        return GenericTraversableTemplate.Cclass.newBuilder(this);
    }

    public boolean nonEmpty() {
        return TraversableOnce.Cclass.nonEmpty(this);
    }

    public int prefixLength(Function1<A, Boolean> p) {
        return SeqLike.Cclass.prefixLength(this, p);
    }

    public <B> B reduceLeft(Function2<B, A, B> op) {
        return TraversableOnce.Cclass.reduceLeft(this, op);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.Seq<A>, java.lang.Object] */
    public Seq<A> repr() {
        return TraversableLike.Cclass.repr(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.Seq<A>, java.lang.Object] */
    public Seq<A> reverse() {
        return SeqLike.Cclass.reverse(this);
    }

    public Iterator<A> reverseIterator() {
        return SeqLike.Cclass.reverseIterator(this);
    }

    public <B> boolean sameElements(Iterable<B> that) {
        return IterableLike.Cclass.sameElements(this, that);
    }

    public final Object scala$collection$mutable$Cloneable$$super$clone() {
        return super.clone();
    }

    public int segmentLength(Function1<A, Boolean> p, int from) {
        return SeqLike.Cclass.segmentLength(this, p, from);
    }

    public int size() {
        return SeqLike.Cclass.size(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.Seq<A>, java.lang.Object] */
    public Seq<A> slice(int from, int until) {
        return IterableLike.Cclass.slice(this, from, until);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.Seq<A>, java.lang.Object] */
    public <B> Seq<A> sortBy(Function1<A, B> f, Ordering<B> ord) {
        return SeqLike.Cclass.sortBy(this, f, ord);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.Seq<A>, java.lang.Object] */
    public <B> Seq<A> sorted(Ordering<B> ord) {
        return SeqLike.Cclass.sorted(this, ord);
    }

    public String stringPrefix() {
        return TraversableLike.Cclass.stringPrefix(this);
    }

    public <B> B sum(Numeric<B> num) {
        return TraversableOnce.Cclass.sum(this, num);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.Seq<A>, java.lang.Object] */
    public Seq<A> tail() {
        return TraversableLike.Cclass.tail(this);
    }

    public Seq<A> thisCollection() {
        return SeqLike.Cclass.thisCollection(this);
    }

    public <B> Object toArray(ClassManifest<B> evidence$1) {
        return TraversableOnce.Cclass.toArray(this, evidence$1);
    }

    public <B> Buffer<B> toBuffer() {
        return TraversableOnce.Cclass.toBuffer(this);
    }

    public Seq<A> toCollection(Seq<A> repr) {
        return SeqLike.Cclass.toCollection(this, repr);
    }

    public <B> Set<B> toSet() {
        return TraversableOnce.Cclass.toSet(this);
    }

    public Stream<A> toStream() {
        return IterableLike.Cclass.toStream(this);
    }

    public String toString() {
        return SeqLike.Cclass.toString(this);
    }

    public <A1, B, That> That zip(Iterable<B> that, CanBuildFrom<Seq<A>, Tuple2<A1, B>, That> bf) {
        return IterableLike.Cclass.zip(this, that, bf);
    }

    public Stack() {
        this(Nil$.MODULE$);
    }

    public boolean isEmpty() {
        return elems().isEmpty();
    }

    public int length() {
        return elems().length();
    }

    public A apply(int index) {
        return elems().apply(index);
    }

    public Stack<A> push(A elem) {
        elems_$eq(elems().$colon$colon(elem));
        return this;
    }

    public Stack<A> pushAll(TraversableOnce<A> xs) {
        xs.foreach(new Stack$$anonfun$pushAll$1(this));
        return this;
    }

    public A pop() {
        Object res = elems().head();
        elems_$eq((List) elems().tail());
        return res;
    }

    public void clear() {
        elems_$eq(Nil$.MODULE$);
    }

    public Iterator<A> iterator() {
        return elems().iterator();
    }

    public List<A> toList() {
        return elems();
    }

    public <U> void foreach(Function1<A, U> f) {
        IterableLike.Cclass.foreach(this, f);
    }

    public Stack<A> clone() {
        return new Stack<>(elems());
    }
}
