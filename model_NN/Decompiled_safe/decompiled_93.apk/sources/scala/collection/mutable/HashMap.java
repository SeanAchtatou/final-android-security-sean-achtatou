package scala.collection.mutable;

import scala.Function0;
import scala.Function1;
import scala.Function2;
import scala.None$;
import scala.Option;
import scala.PartialFunction;
import scala.ScalaObject;
import scala.Some;
import scala.Tuple2;
import scala.collection.Iterable;
import scala.collection.IterableLike;
import scala.collection.Iterator;
import scala.collection.Map;
import scala.collection.MapLike;
import scala.collection.Traversable;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericTraversableTemplate;
import scala.collection.generic.Growable;
import scala.collection.generic.Shrinkable;
import scala.collection.generic.Subtractable;
import scala.collection.immutable.List;
import scala.collection.immutable.Set;
import scala.collection.immutable.Stream;
import scala.collection.mutable.Builder;
import scala.collection.mutable.Cloneable;
import scala.collection.mutable.HashTable;
import scala.collection.mutable.Iterable;
import scala.collection.mutable.Map;
import scala.collection.mutable.MapLike;
import scala.collection.mutable.Traversable;
import scala.math.Numeric;
import scala.reflect.ClassManifest;

/* compiled from: HashMap.scala */
public class HashMap<A, B> implements Map<A, B>, MapLike<A, B, HashMap<A, B>>, HashTable<A>, ScalaObject, MapLike {
    public static final long serialVersionUID = 1;
    private transient int _loadFactor;
    private transient HashEntry[] table;
    private transient int tableSize;
    private transient int threshold;

    public HashMap() {
        TraversableOnce.Cclass.$init$(this);
        TraversableLike.Cclass.$init$(this);
        GenericTraversableTemplate.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        IterableLike.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Function1.Cclass.$init$(this);
        PartialFunction.Cclass.$init$(this);
        Subtractable.Cclass.$init$(this);
        MapLike.Cclass.$init$(this);
        Map.Cclass.$init$(this);
        Growable.Cclass.$init$(this);
        Builder.Cclass.$init$(this);
        Shrinkable.Cclass.$init$(this);
        Cloneable.Cclass.$init$(this);
        MapLike.Cclass.$init$(this);
        Map.Cclass.$init$(this);
        HashTable.Cclass.$init$(this);
    }

    public <B> B $div$colon(B z, Function2<B, Tuple2<A, B>, B> op) {
        return TraversableOnce.Cclass.$div$colon(this, z, op);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.Map, scala.collection.mutable.HashMap<A, B>] */
    public HashMap<A, B> $minus(A key) {
        return MapLike.Cclass.$minus(this, key);
    }

    public <B1> Map<A, B1> $plus(Tuple2<A, B1> kv) {
        return MapLike.Cclass.$plus(this, kv);
    }

    public <B, That> That $plus$plus(TraversableOnce<B> that, CanBuildFrom<HashMap<A, B>, B, That> bf) {
        return TraversableLike.Cclass.$plus$plus(this, that, bf);
    }

    public Growable<Tuple2<A, B>> $plus$plus$eq(TraversableOnce<Tuple2<A, B>> xs) {
        return Growable.Cclass.$plus$plus$eq(this, xs);
    }

    public int _loadFactor() {
        return this._loadFactor;
    }

    public void _loadFactor_$eq(int i) {
        this._loadFactor = i;
    }

    public void addEntry(HashEntry e) {
        HashTable.Cclass.addEntry(this, e);
    }

    public StringBuilder addString(StringBuilder b, String start, String sep, String end) {
        return MapLike.Cclass.addString(this, b, start, sep, end);
    }

    public <C> PartialFunction<A, C> andThen(Function1<B, C> k) {
        return PartialFunction.Cclass.andThen(this, k);
    }

    public B apply(A key) {
        return MapLike.Cclass.apply(this, key);
    }

    public int apply$mcII$sp(int v1) {
        return Function1.Cclass.apply$mcII$sp(this, v1);
    }

    public void apply$mcVI$sp(int v1) {
        Function1.Cclass.apply$mcVI$sp(this, v1);
    }

    public void apply$mcVL$sp(long v1) {
        Function1.Cclass.apply$mcVL$sp(this, v1);
    }

    public boolean canEqual(Object that) {
        return IterableLike.Cclass.canEqual(this, that);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.Map, scala.collection.mutable.HashMap<A, B>] */
    public HashMap<A, B> clone() {
        return MapLike.Cclass.clone(this);
    }

    public GenericCompanion<Iterable> companion() {
        return Iterable.Cclass.companion(this);
    }

    public <A> Function1<A, B> compose(Function1<A, A> g) {
        return Function1.Cclass.compose(this, g);
    }

    public boolean contains(A key) {
        return MapLike.Cclass.contains(this, key);
    }

    public <B> void copyToArray(Object xs, int start) {
        TraversableOnce.Cclass.copyToArray(this, xs, start);
    }

    public <B> void copyToArray(Object xs, int start, int len) {
        IterableLike.Cclass.copyToArray(this, xs, start, len);
    }

    /* renamed from: default  reason: not valid java name */
    public B m11default(A key) {
        return MapLike.Cclass.m3default(this, key);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.mutable.HashMap<A, B>] */
    public HashMap<A, B> drop(int n) {
        return TraversableLike.Cclass.drop(this, n);
    }

    public boolean elemEquals(A key1, A key2) {
        return HashTable.Cclass.elemEquals(this, key1, key2);
    }

    public int elemHashCode(A key) {
        return HashTable.Cclass.elemHashCode(this, key);
    }

    public Iterator<HashEntry> entriesIterator() {
        return HashTable.Cclass.entriesIterator(this);
    }

    public boolean equals(Object that) {
        return MapLike.Cclass.equals(this, that);
    }

    public boolean exists(Function1<Tuple2<A, B>, Boolean> p) {
        return IterableLike.Cclass.exists(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.mutable.HashMap<A, B>] */
    public HashMap<A, B> filter(Function1<Tuple2<A, B>, Boolean> p) {
        return TraversableLike.Cclass.filter(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.Map, scala.collection.mutable.HashMap<A, B>] */
    public HashMap<A, B> filterNot(Function1<Tuple2<A, B>, Boolean> p) {
        return MapLike.Cclass.filterNot(this, p);
    }

    public HashEntry findEntry(A key) {
        return HashTable.Cclass.findEntry(this, key);
    }

    public <B> B foldLeft(B z, Function2<B, Tuple2<A, B>, B> op) {
        return TraversableOnce.Cclass.foldLeft(this, z, op);
    }

    public boolean forall(Function1<Tuple2<A, B>, Boolean> p) {
        return IterableLike.Cclass.forall(this, p);
    }

    public final <C> void foreachEntry(Function1<HashEntry, C> f) {
        HashTable.Cclass.foreachEntry(this, f);
    }

    public <B> Builder<B, Iterable<B>> genericBuilder() {
        return GenericTraversableTemplate.Cclass.genericBuilder(this);
    }

    public <B1> B1 getOrElse(A key, Function0<B1> function0) {
        return MapLike.Cclass.getOrElse(this, key, function0);
    }

    public int hashCode() {
        return MapLike.Cclass.hashCode(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.Tuple2<A, B>] */
    public Tuple2<A, B> head() {
        return IterableLike.Cclass.head(this);
    }

    public final int improve(int hcode) {
        return HashTable.Cclass.improve(this, hcode);
    }

    public final int index(int hcode) {
        return HashTable.Cclass.index(this, hcode);
    }

    public int initialSize() {
        return HashTable.Cclass.initialSize(this);
    }

    public int initialThreshold() {
        return HashTable.Cclass.initialThreshold(this);
    }

    public boolean isDefinedAt(A key) {
        return MapLike.Cclass.isDefinedAt(this, key);
    }

    public boolean isEmpty() {
        return MapLike.Cclass.isEmpty(this);
    }

    public final boolean isTraversableAgain() {
        return TraversableLike.Cclass.isTraversableAgain(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.Tuple2<A, B>] */
    public Tuple2<A, B> last() {
        return TraversableLike.Cclass.last(this);
    }

    public int loadFactor() {
        return HashTable.Cclass.loadFactor(this);
    }

    public <B, That> That map(Function1<Tuple2<A, B>, B> f, CanBuildFrom<HashMap<A, B>, B, That> bf) {
        return TraversableLike.Cclass.map(this, f, bf);
    }

    public <NewTo> Builder<Tuple2<A, B>, NewTo> mapResult(Function1<HashMap<A, B>, NewTo> f) {
        return Builder.Cclass.mapResult(this, f);
    }

    public String mkString() {
        return TraversableOnce.Cclass.mkString(this);
    }

    public String mkString(String sep) {
        return TraversableOnce.Cclass.mkString(this, sep);
    }

    public String mkString(String start, String sep, String end) {
        return TraversableOnce.Cclass.mkString(this, start, sep, end);
    }

    public Builder<Tuple2<A, B>, HashMap<A, B>> newBuilder() {
        return MapLike.Cclass.newBuilder(this);
    }

    public boolean nonEmpty() {
        return TraversableOnce.Cclass.nonEmpty(this);
    }

    public <B> B reduceLeft(Function2<B, Tuple2<A, B>, B> op) {
        return TraversableOnce.Cclass.reduceLeft(this, op);
    }

    public HashEntry removeEntry(A key) {
        return HashTable.Cclass.removeEntry(this, key);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.mutable.HashMap<A, B>] */
    public HashMap<A, B> repr() {
        return TraversableLike.Cclass.repr(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.Map, scala.collection.mutable.HashMap<A, B>] */
    public HashMap<A, B> result() {
        return MapLike.Cclass.result(this);
    }

    public <B> boolean sameElements(scala.collection.Iterable<B> that) {
        return IterableLike.Cclass.sameElements(this, that);
    }

    public final Object scala$collection$mutable$Cloneable$$super$clone() {
        return super.clone();
    }

    public void sizeHint(int size) {
        Builder.Cclass.sizeHint(this, size);
    }

    public void sizeHint(TraversableLike<?, ?> coll, int delta) {
        Builder.Cclass.sizeHint(this, coll, delta);
    }

    public /* synthetic */ int sizeHint$default$2() {
        return Builder.Cclass.sizeHint$default$2(this);
    }

    public void sizeHintBounded(int size, TraversableLike<?, ?> boundingColl) {
        Builder.Cclass.sizeHintBounded(this, size, boundingColl);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.mutable.HashMap<A, B>] */
    public HashMap<A, B> slice(int from, int until) {
        return IterableLike.Cclass.slice(this, from, until);
    }

    public String stringPrefix() {
        return MapLike.Cclass.stringPrefix(this);
    }

    public <B> B sum(Numeric<B> num) {
        return TraversableOnce.Cclass.sum(this, num);
    }

    public HashEntry[] table() {
        return this.table;
    }

    public int tableSize() {
        return this.tableSize;
    }

    public void tableSize_$eq(int i) {
        this.tableSize = i;
    }

    public void table_$eq(HashEntry[] hashEntryArr) {
        this.table = hashEntryArr;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.mutable.HashMap<A, B>] */
    public HashMap<A, B> tail() {
        return TraversableLike.Cclass.tail(this);
    }

    public scala.collection.Iterable<Tuple2<A, B>> thisCollection() {
        return IterableLike.Cclass.thisCollection(this);
    }

    public int threshold() {
        return this.threshold;
    }

    public void threshold_$eq(int i) {
        this.threshold = i;
    }

    public <B> Object toArray(ClassManifest<B> evidence$1) {
        return TraversableOnce.Cclass.toArray(this, evidence$1);
    }

    public <B> Buffer<B> toBuffer() {
        return TraversableOnce.Cclass.toBuffer(this);
    }

    public List<Tuple2<A, B>> toList() {
        return TraversableOnce.Cclass.toList(this);
    }

    public <B> Set<B> toSet() {
        return TraversableOnce.Cclass.toSet(this);
    }

    public Stream<Tuple2<A, B>> toStream() {
        return IterableLike.Cclass.toStream(this);
    }

    public String toString() {
        return MapLike.Cclass.toString(this);
    }

    public <A1, B, That> That zip(scala.collection.Iterable<B> that, CanBuildFrom<HashMap<A, B>, Tuple2<A1, B>, That> bf) {
        return IterableLike.Cclass.zip(this, that, bf);
    }

    public HashMap<A, B> empty() {
        return new HashMap<>();
    }

    public int size() {
        return tableSize();
    }

    public Option<B> get(A key) {
        DefaultEntry e = (DefaultEntry) findEntry(key);
        if (e == null) {
            return None$.MODULE$;
        }
        return new Some(e.value);
    }

    public Option<B> put(A key, B value) {
        DefaultEntry e = (DefaultEntry) findEntry(key);
        if (e == null) {
            addEntry(new DefaultEntry(key, value));
            return None$.MODULE$;
        }
        Object v = e.value;
        e.value = value;
        return new Some(v);
    }

    public void update(A key, B value) {
        put(key, value);
    }

    public HashMap<A, B> $plus$eq(Tuple2<A, B> kv) {
        DefaultEntry e = (DefaultEntry) findEntry(kv._1());
        if (e == null) {
            addEntry(new DefaultEntry(kv._1(), kv._2()));
        } else {
            e.value = kv._2();
        }
        return this;
    }

    public HashMap<A, B> $minus$eq(A key) {
        removeEntry(key);
        return this;
    }

    public Iterator<Tuple2<A, B>> iterator() {
        return entriesIterator().map(new HashMap$$anonfun$iterator$1(this));
    }

    public <C> void foreach(Function1<Tuple2<A, B>, C> f$1) {
        foreachEntry(new HashMap$$anonfun$foreach$1(this, f$1));
    }
}
