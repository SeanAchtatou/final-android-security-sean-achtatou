package scala.collection.mutable;

import scala.ScalaObject;
import scala.collection.generic.SeqFactory;

/* compiled from: IndexedSeq.scala */
public final class IndexedSeq$ extends SeqFactory<IndexedSeq> implements ScalaObject {
    public static final IndexedSeq$ MODULE$ = null;

    static {
        new IndexedSeq$();
    }

    private IndexedSeq$() {
        MODULE$ = this;
    }

    public <A> Builder<A, IndexedSeq<A>> newBuilder() {
        return new ArrayBuffer();
    }
}
