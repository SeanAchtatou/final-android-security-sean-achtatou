package scala.collection.mutable;

import scala.Array$;
import scala.Function1;
import scala.Function2;
import scala.PartialFunction;
import scala.ScalaObject;
import scala.Tuple2;
import scala.collection.IndexedSeq;
import scala.collection.IndexedSeqLike;
import scala.collection.IndexedSeqOptimized;
import scala.collection.Iterable;
import scala.collection.IterableLike;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.SeqLike;
import scala.collection.Traversable;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericTraversableTemplate;
import scala.collection.immutable.List;
import scala.collection.immutable.Set;
import scala.collection.immutable.Stream;
import scala.collection.mutable.IndexedSeq;
import scala.collection.mutable.IndexedSeqLike;
import scala.collection.mutable.Iterable;
import scala.collection.mutable.Seq;
import scala.collection.mutable.Traversable;
import scala.math.Numeric;
import scala.math.Ordering;
import scala.reflect.ClassManifest;
import scala.runtime.BoxesRunTime;
import scala.runtime.RichInt;
import scala.runtime.ScalaRunTime$;

/* compiled from: ArraySeq.scala */
public class ArraySeq<A> implements IndexedSeq<A>, GenericTraversableTemplate<A, ArraySeq>, IndexedSeqOptimized<A, ArraySeq<A>>, ScalaObject {
    private final Object[] array;
    private final int length;

    public ArraySeq(int length2) {
        this.length = length2;
        TraversableOnce.Cclass.$init$(this);
        TraversableLike.Cclass.$init$(this);
        GenericTraversableTemplate.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        IterableLike.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Function1.Cclass.$init$(this);
        PartialFunction.Cclass.$init$(this);
        SeqLike.Cclass.$init$(this);
        Seq.Cclass.$init$(this);
        Seq.Cclass.$init$(this);
        IndexedSeqLike.Cclass.$init$(this);
        IndexedSeq.Cclass.$init$(this);
        IndexedSeqLike.Cclass.$init$(this);
        IndexedSeq.Cclass.$init$(this);
        IndexedSeqOptimized.Cclass.$init$(this);
        this.array = new Object[length2];
    }

    public <B> B $div$colon(B z, Function2<B, A, B> op) {
        return TraversableOnce.Cclass.$div$colon(this, z, op);
    }

    public <B, That> That $plus$plus(TraversableOnce<B> that, CanBuildFrom<ArraySeq<A>, B, That> bf) {
        return TraversableLike.Cclass.$plus$plus(this, that, bf);
    }

    public StringBuilder addString(StringBuilder b, String start, String sep, String end) {
        return TraversableOnce.Cclass.addString(this, b, start, sep, end);
    }

    public <C> PartialFunction<Integer, C> andThen(Function1<A, C> k) {
        return PartialFunction.Cclass.andThen(this, k);
    }

    public /* bridge */ /* synthetic */ Object apply(Object v1) {
        return apply(BoxesRunTime.unboxToInt(v1));
    }

    public int apply$mcII$sp(int v1) {
        return Function1.Cclass.apply$mcII$sp(this, v1);
    }

    public void apply$mcVI$sp(int v1) {
        Function1.Cclass.apply$mcVI$sp(this, v1);
    }

    public void apply$mcVL$sp(long v1) {
        Function1.Cclass.apply$mcVL$sp(this, v1);
    }

    public boolean canEqual(Object that) {
        return IterableLike.Cclass.canEqual(this, that);
    }

    public <A> Function1<A, A> compose(Function1<A, Integer> g) {
        return Function1.Cclass.compose(this, g);
    }

    public boolean contains(Object elem) {
        return SeqLike.Cclass.contains(this, elem);
    }

    public <B> void copyToArray(Object xs, int start) {
        TraversableOnce.Cclass.copyToArray(this, xs, start);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.ArraySeq<A>, java.lang.Object] */
    public ArraySeq<A> drop(int n) {
        return IndexedSeqOptimized.Cclass.drop(this, n);
    }

    public boolean equals(Object that) {
        return SeqLike.Cclass.equals(this, that);
    }

    public boolean exists(Function1<A, Boolean> p) {
        return IndexedSeqOptimized.Cclass.exists(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.ArraySeq<A>, java.lang.Object] */
    public ArraySeq<A> filter(Function1<A, Boolean> p) {
        return TraversableLike.Cclass.filter(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.ArraySeq<A>, java.lang.Object] */
    public ArraySeq<A> filterNot(Function1<A, Boolean> p) {
        return TraversableLike.Cclass.filterNot(this, p);
    }

    public <B> B foldLeft(B z, Function2<B, A, B> op) {
        return IndexedSeqOptimized.Cclass.foldLeft(this, z, op);
    }

    public boolean forall(Function1<A, Boolean> p) {
        return IndexedSeqOptimized.Cclass.forall(this, p);
    }

    public <B> Builder<B, ArraySeq<B>> genericBuilder() {
        return GenericTraversableTemplate.Cclass.genericBuilder(this);
    }

    public int hashCode() {
        return SeqLike.Cclass.hashCode(this);
    }

    public A head() {
        return IndexedSeqOptimized.Cclass.head(this);
    }

    public <B> int indexOf(B elem) {
        return SeqLike.Cclass.indexOf(this, elem);
    }

    public <B> int indexOf(B elem, int from) {
        return SeqLike.Cclass.indexOf(this, elem, from);
    }

    public int indexWhere(Function1<A, Boolean> p, int from) {
        return IndexedSeqOptimized.Cclass.indexWhere(this, p, from);
    }

    public boolean isDefinedAt(int idx) {
        return SeqLike.Cclass.isDefinedAt(this, idx);
    }

    public /* bridge */ /* synthetic */ boolean isDefinedAt(Object x) {
        return isDefinedAt(BoxesRunTime.unboxToInt(x));
    }

    public boolean isEmpty() {
        return IndexedSeqOptimized.Cclass.isEmpty(this);
    }

    public final boolean isTraversableAgain() {
        return TraversableLike.Cclass.isTraversableAgain(this);
    }

    public Iterator<A> iterator() {
        return IndexedSeqLike.Cclass.iterator(this);
    }

    public A last() {
        return IndexedSeqOptimized.Cclass.last(this);
    }

    public int length() {
        return this.length;
    }

    public <B, That> That map(Function1<A, B> f, CanBuildFrom<ArraySeq<A>, B, That> bf) {
        return TraversableLike.Cclass.map(this, f, bf);
    }

    public String mkString() {
        return TraversableOnce.Cclass.mkString(this);
    }

    public String mkString(String sep) {
        return TraversableOnce.Cclass.mkString(this, sep);
    }

    public String mkString(String start, String sep, String end) {
        return TraversableOnce.Cclass.mkString(this, start, sep, end);
    }

    public Builder<A, ArraySeq<A>> newBuilder() {
        return GenericTraversableTemplate.Cclass.newBuilder(this);
    }

    public boolean nonEmpty() {
        return TraversableOnce.Cclass.nonEmpty(this);
    }

    public int prefixLength(Function1<A, Boolean> p) {
        return SeqLike.Cclass.prefixLength(this, p);
    }

    public <B> B reduceLeft(Function2<B, A, B> op) {
        return IndexedSeqOptimized.Cclass.reduceLeft(this, op);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.ArraySeq<A>, java.lang.Object] */
    public ArraySeq<A> repr() {
        return TraversableLike.Cclass.repr(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.ArraySeq<A>, java.lang.Object] */
    public ArraySeq<A> reverse() {
        return IndexedSeqOptimized.Cclass.reverse(this);
    }

    public Iterator<A> reverseIterator() {
        return IndexedSeqOptimized.Cclass.reverseIterator(this);
    }

    public <B> boolean sameElements(scala.collection.Iterable<B> that) {
        return IndexedSeqOptimized.Cclass.sameElements(this, that);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$head() {
        return IterableLike.Cclass.head(this);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$last() {
        return TraversableLike.Cclass.last(this);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$reduceLeft(Function2 op) {
        return TraversableOnce.Cclass.reduceLeft(this, op);
    }

    public final boolean scala$collection$IndexedSeqOptimized$$super$sameElements(scala.collection.Iterable that) {
        return IterableLike.Cclass.sameElements(this, that);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$tail() {
        return TraversableLike.Cclass.tail(this);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$zip(scala.collection.Iterable that, CanBuildFrom bf) {
        return IterableLike.Cclass.zip(this, that, bf);
    }

    public int segmentLength(Function1<A, Boolean> p, int from) {
        return IndexedSeqOptimized.Cclass.segmentLength(this, p, from);
    }

    public int size() {
        return SeqLike.Cclass.size(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.ArraySeq<A>, java.lang.Object] */
    public ArraySeq<A> slice(int from, int until) {
        return IndexedSeqOptimized.Cclass.slice(this, from, until);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.ArraySeq<A>, java.lang.Object] */
    public <B> ArraySeq<A> sortBy(Function1<A, B> f, Ordering<B> ord) {
        return SeqLike.Cclass.sortBy(this, f, ord);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.ArraySeq<A>, java.lang.Object] */
    public <B> ArraySeq<A> sorted(Ordering<B> ord) {
        return SeqLike.Cclass.sorted(this, ord);
    }

    public String stringPrefix() {
        return TraversableLike.Cclass.stringPrefix(this);
    }

    public <B> B sum(Numeric<B> num) {
        return TraversableOnce.Cclass.sum(this, num);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.ArraySeq<A>, java.lang.Object] */
    public ArraySeq<A> tail() {
        return IndexedSeqOptimized.Cclass.tail(this);
    }

    public IndexedSeq<A> thisCollection() {
        return IndexedSeqLike.Cclass.thisCollection(this);
    }

    public <B> Object toArray(ClassManifest<B> evidence$1) {
        return TraversableOnce.Cclass.toArray(this, evidence$1);
    }

    public <B> Buffer<B> toBuffer() {
        return TraversableOnce.Cclass.toBuffer(this);
    }

    public IndexedSeq<A> toCollection(ArraySeq<A> repr) {
        return IndexedSeqLike.Cclass.toCollection(this, repr);
    }

    public List<A> toList() {
        return TraversableOnce.Cclass.toList(this);
    }

    public <B> Set<B> toSet() {
        return TraversableOnce.Cclass.toSet(this);
    }

    public Stream<A> toStream() {
        return IterableLike.Cclass.toStream(this);
    }

    public String toString() {
        return SeqLike.Cclass.toString(this);
    }

    public <A1, B, That> That zip(scala.collection.Iterable<B> that, CanBuildFrom<ArraySeq<A>, Tuple2<A1, B>, That> bf) {
        return IndexedSeqOptimized.Cclass.zip(this, that, bf);
    }

    public GenericCompanion<ArraySeq> companion() {
        return ArraySeq$.MODULE$;
    }

    public Object[] array() {
        return this.array;
    }

    public A apply(int idx) {
        if (idx < length()) {
            return array()[idx];
        }
        throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(idx).toString());
    }

    public void update(int idx, A elem) {
        if (idx >= length()) {
            throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(idx).toString());
        }
        array()[idx] = elem;
    }

    public <U> void foreach(Function1<A, U> f) {
        for (int i = 0; i < length(); i++) {
            f.apply(array()[i]);
        }
    }

    public <B> void copyToArray(Object xs, int start, int len) {
        Array$.MODULE$.copy(array(), 0, xs, start, new RichInt(new RichInt(len).min(ScalaRunTime$.MODULE$.array_length(xs) - start)).min(length()));
    }
}
