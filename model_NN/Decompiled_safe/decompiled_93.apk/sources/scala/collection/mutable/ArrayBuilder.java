package scala.collection.mutable;

import scala.Array$;
import scala.Function1;
import scala.ScalaObject;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.Growable;
import scala.collection.mutable.Builder;
import scala.collection.mutable.WrappedArray;
import scala.reflect.ClassManifest;
import scala.runtime.BoxedUnit;

/* compiled from: ArrayBuilder.scala */
public abstract class ArrayBuilder<T> implements Builder<T, Object>, ScalaObject, Builder {
    public ArrayBuilder() {
        Growable.Cclass.$init$(this);
        Builder.Cclass.$init$(this);
    }

    public Growable<T> $plus$plus$eq(TraversableOnce<T> xs) {
        return Growable.Cclass.$plus$plus$eq(this, xs);
    }

    public <NewTo> Builder<T, NewTo> mapResult(Function1<Object, NewTo> f) {
        return Builder.Cclass.mapResult(this, f);
    }

    public void sizeHint(int size) {
        Builder.Cclass.sizeHint(this, size);
    }

    public void sizeHint(TraversableLike<?, ?> coll, int delta) {
        Builder.Cclass.sizeHint(this, coll, delta);
    }

    public /* synthetic */ int sizeHint$default$2() {
        return Builder.Cclass.sizeHint$default$2(this);
    }

    public void sizeHintBounded(int size, TraversableLike<?, ?> boundingColl) {
        Builder.Cclass.sizeHintBounded(this, size, boundingColl);
    }

    /* compiled from: ArrayBuilder.scala */
    public static class ofRef<T> extends ArrayBuilder<T> implements ScalaObject {
        private int capacity = 0;
        private T[] elems;
        private final ClassManifest<T> evidence$2;
        private int size = 0;

        public ofRef(ClassManifest<T> evidence$22) {
            this.evidence$2 = evidence$22;
        }

        private T[] elems() {
            return this.elems;
        }

        private void elems_$eq(T[] tArr) {
            this.elems = tArr;
        }

        private int capacity() {
            return this.capacity;
        }

        private void capacity_$eq(int i) {
            this.capacity = i;
        }

        private int size() {
            return this.size;
        }

        private void size_$eq(int i) {
            this.size = i;
        }

        private T[] mkArray(int size2) {
            Object[] newelems = (Object[]) this.evidence$2.newArray(size2);
            if (size() > 0) {
                Array$.MODULE$.copy(elems(), 0, newelems, 0, size());
            }
            return newelems;
        }

        private void resize(int size2) {
            elems_$eq(mkArray(size2));
            capacity_$eq(size2);
        }

        public void sizeHint(int size2) {
            if (capacity() < size2) {
                resize(size2);
            }
        }

        private void ensureSize(int size2) {
            if (capacity() < size2 || capacity() == 0) {
                int newsize = capacity() == 0 ? 16 : capacity() * 2;
                while (newsize < size2) {
                    newsize *= 2;
                }
                resize(newsize);
            }
        }

        public ofRef<T> $plus$eq(T elem) {
            ensureSize(size() + 1);
            elems()[size()] = elem;
            size_$eq(size() + 1);
            return this;
        }

        /* Debug info: failed to restart local var, previous not found, register: 6 */
        public ofRef<T> $plus$plus$eq(TraversableOnce<T> xs) {
            if (!(xs instanceof WrappedArray.ofRef)) {
                return (ofRef) Growable.Cclass.$plus$plus$eq(this, xs);
            }
            WrappedArray.ofRef ofref = (WrappedArray.ofRef) xs;
            ensureSize(size() + ofref.array.length);
            Array$.MODULE$.copy(ofref.array, 0, elems(), size(), ofref.array.length);
            size_$eq(size() + ofref.array().length);
            return this;
        }

        public T[] result() {
            if (capacity() == 0 || capacity() != size()) {
                return mkArray(size());
            }
            return elems();
        }

        public boolean equals(Object other) {
            if (!(other instanceof ofRef)) {
                return false;
            }
            ofRef ofref = (ofRef) other;
            if (size() == ofref.size() && elems() == ofref.elems()) {
                return true;
            }
            return false;
        }

        public String toString() {
            return "ArrayBuilder.ofRef";
        }
    }

    /* compiled from: ArrayBuilder.scala */
    public static class ofByte extends ArrayBuilder<Byte> implements ScalaObject {
        private int capacity = 0;
        private byte[] elems;
        private int size = 0;

        private byte[] elems() {
            return this.elems;
        }

        private void elems_$eq(byte[] bArr) {
            this.elems = bArr;
        }

        private int capacity() {
            return this.capacity;
        }

        private void capacity_$eq(int i) {
            this.capacity = i;
        }

        private int size() {
            return this.size;
        }

        private void size_$eq(int i) {
            this.size = i;
        }

        private byte[] mkArray(int size2) {
            byte[] newelems = new byte[size2];
            if (size() > 0) {
                Array$.MODULE$.copy(elems(), 0, newelems, 0, size());
            }
            return newelems;
        }

        private void resize(int size2) {
            elems_$eq(mkArray(size2));
            capacity_$eq(size2);
        }

        public void sizeHint(int size2) {
            if (capacity() < size2) {
                resize(size2);
            }
        }

        private void ensureSize(int size2) {
            if (capacity() < size2 || capacity() == 0) {
                int newsize = capacity() == 0 ? 16 : capacity() * 2;
                while (newsize < size2) {
                    newsize *= 2;
                }
                resize(newsize);
            }
        }

        public ofByte $plus$eq(byte elem) {
            ensureSize(size() + 1);
            elems()[size()] = elem;
            size_$eq(size() + 1);
            return this;
        }

        /* Debug info: failed to restart local var, previous not found, register: 6 */
        public ofByte $plus$plus$eq(TraversableOnce<Byte> xs) {
            if (!(xs instanceof WrappedArray.ofByte)) {
                return (ofByte) Growable.Cclass.$plus$plus$eq(this, xs);
            }
            WrappedArray.ofByte ofbyte = (WrappedArray.ofByte) xs;
            ensureSize(size() + ofbyte.array.length);
            Array$.MODULE$.copy(ofbyte.array, 0, elems(), size(), ofbyte.array.length);
            size_$eq(size() + ofbyte.array().length);
            return this;
        }

        public byte[] result() {
            if (capacity() == 0 || capacity() != size()) {
                return mkArray(size());
            }
            return elems();
        }

        public boolean equals(Object other) {
            if (!(other instanceof ofByte)) {
                return false;
            }
            ofByte ofbyte = (ofByte) other;
            if (size() == ofbyte.size() && elems() == ofbyte.elems()) {
                return true;
            }
            return false;
        }

        public String toString() {
            return "ArrayBuilder.ofByte";
        }
    }

    /* compiled from: ArrayBuilder.scala */
    public static class ofShort extends ArrayBuilder<Short> implements ScalaObject {
        private int capacity = 0;
        private short[] elems;
        private int size = 0;

        private short[] elems() {
            return this.elems;
        }

        private void elems_$eq(short[] sArr) {
            this.elems = sArr;
        }

        private int capacity() {
            return this.capacity;
        }

        private void capacity_$eq(int i) {
            this.capacity = i;
        }

        private int size() {
            return this.size;
        }

        private void size_$eq(int i) {
            this.size = i;
        }

        private short[] mkArray(int size2) {
            short[] newelems = new short[size2];
            if (size() > 0) {
                Array$.MODULE$.copy(elems(), 0, newelems, 0, size());
            }
            return newelems;
        }

        private void resize(int size2) {
            elems_$eq(mkArray(size2));
            capacity_$eq(size2);
        }

        public void sizeHint(int size2) {
            if (capacity() < size2) {
                resize(size2);
            }
        }

        private void ensureSize(int size2) {
            if (capacity() < size2 || capacity() == 0) {
                int newsize = capacity() == 0 ? 16 : capacity() * 2;
                while (newsize < size2) {
                    newsize *= 2;
                }
                resize(newsize);
            }
        }

        public ofShort $plus$eq(short elem) {
            ensureSize(size() + 1);
            elems()[size()] = elem;
            size_$eq(size() + 1);
            return this;
        }

        /* Debug info: failed to restart local var, previous not found, register: 6 */
        public ofShort $plus$plus$eq(TraversableOnce<Short> xs) {
            if (!(xs instanceof WrappedArray.ofShort)) {
                return (ofShort) Growable.Cclass.$plus$plus$eq(this, xs);
            }
            WrappedArray.ofShort ofshort = (WrappedArray.ofShort) xs;
            ensureSize(size() + ofshort.array.length);
            Array$.MODULE$.copy(ofshort.array, 0, elems(), size(), ofshort.array.length);
            size_$eq(size() + ofshort.array().length);
            return this;
        }

        public short[] result() {
            if (capacity() == 0 || capacity() != size()) {
                return mkArray(size());
            }
            return elems();
        }

        public boolean equals(Object other) {
            if (!(other instanceof ofShort)) {
                return false;
            }
            ofShort ofshort = (ofShort) other;
            if (size() == ofshort.size() && elems() == ofshort.elems()) {
                return true;
            }
            return false;
        }

        public String toString() {
            return "ArrayBuilder.ofShort";
        }
    }

    /* compiled from: ArrayBuilder.scala */
    public static class ofChar extends ArrayBuilder<Character> implements ScalaObject {
        private int capacity = 0;
        private char[] elems;
        private int size = 0;

        private char[] elems() {
            return this.elems;
        }

        private void elems_$eq(char[] cArr) {
            this.elems = cArr;
        }

        private int capacity() {
            return this.capacity;
        }

        private void capacity_$eq(int i) {
            this.capacity = i;
        }

        private int size() {
            return this.size;
        }

        private void size_$eq(int i) {
            this.size = i;
        }

        private char[] mkArray(int size2) {
            char[] newelems = new char[size2];
            if (size() > 0) {
                Array$.MODULE$.copy(elems(), 0, newelems, 0, size());
            }
            return newelems;
        }

        private void resize(int size2) {
            elems_$eq(mkArray(size2));
            capacity_$eq(size2);
        }

        public void sizeHint(int size2) {
            if (capacity() < size2) {
                resize(size2);
            }
        }

        private void ensureSize(int size2) {
            if (capacity() < size2 || capacity() == 0) {
                int newsize = capacity() == 0 ? 16 : capacity() * 2;
                while (newsize < size2) {
                    newsize *= 2;
                }
                resize(newsize);
            }
        }

        public ofChar $plus$eq(char elem) {
            ensureSize(size() + 1);
            elems()[size()] = elem;
            size_$eq(size() + 1);
            return this;
        }

        /* Debug info: failed to restart local var, previous not found, register: 6 */
        public ofChar $plus$plus$eq(TraversableOnce<Character> xs) {
            if (!(xs instanceof WrappedArray.ofChar)) {
                return (ofChar) Growable.Cclass.$plus$plus$eq(this, xs);
            }
            WrappedArray.ofChar ofchar = (WrappedArray.ofChar) xs;
            ensureSize(size() + ofchar.array.length);
            Array$.MODULE$.copy(ofchar.array, 0, elems(), size(), ofchar.array.length);
            size_$eq(size() + ofchar.array().length);
            return this;
        }

        public char[] result() {
            if (capacity() == 0 || capacity() != size()) {
                return mkArray(size());
            }
            return elems();
        }

        public boolean equals(Object other) {
            if (!(other instanceof ofChar)) {
                return false;
            }
            ofChar ofchar = (ofChar) other;
            if (size() == ofchar.size() && elems() == ofchar.elems()) {
                return true;
            }
            return false;
        }

        public String toString() {
            return "ArrayBuilder.ofChar";
        }
    }

    /* compiled from: ArrayBuilder.scala */
    public static class ofInt extends ArrayBuilder<Integer> implements ScalaObject {
        private int capacity = 0;
        private int[] elems;
        private int size = 0;

        private int[] elems() {
            return this.elems;
        }

        private void elems_$eq(int[] iArr) {
            this.elems = iArr;
        }

        private int capacity() {
            return this.capacity;
        }

        private void capacity_$eq(int i) {
            this.capacity = i;
        }

        private int size() {
            return this.size;
        }

        private void size_$eq(int i) {
            this.size = i;
        }

        private int[] mkArray(int size2) {
            int[] newelems = new int[size2];
            if (size() > 0) {
                Array$.MODULE$.copy(elems(), 0, newelems, 0, size());
            }
            return newelems;
        }

        private void resize(int size2) {
            elems_$eq(mkArray(size2));
            capacity_$eq(size2);
        }

        public void sizeHint(int size2) {
            if (capacity() < size2) {
                resize(size2);
            }
        }

        private void ensureSize(int size2) {
            if (capacity() < size2 || capacity() == 0) {
                int newsize = capacity() == 0 ? 16 : capacity() * 2;
                while (newsize < size2) {
                    newsize *= 2;
                }
                resize(newsize);
            }
        }

        public ofInt $plus$eq(int elem) {
            ensureSize(size() + 1);
            elems()[size()] = elem;
            size_$eq(size() + 1);
            return this;
        }

        /* Debug info: failed to restart local var, previous not found, register: 6 */
        public ofInt $plus$plus$eq(TraversableOnce<Integer> xs) {
            if (!(xs instanceof WrappedArray.ofInt)) {
                return (ofInt) Growable.Cclass.$plus$plus$eq(this, xs);
            }
            WrappedArray.ofInt ofint = (WrappedArray.ofInt) xs;
            ensureSize(size() + ofint.array.length);
            Array$.MODULE$.copy(ofint.array, 0, elems(), size(), ofint.array.length);
            size_$eq(size() + ofint.array().length);
            return this;
        }

        public int[] result() {
            if (capacity() == 0 || capacity() != size()) {
                return mkArray(size());
            }
            return elems();
        }

        public boolean equals(Object other) {
            if (!(other instanceof ofInt)) {
                return false;
            }
            ofInt ofint = (ofInt) other;
            if (size() == ofint.size() && elems() == ofint.elems()) {
                return true;
            }
            return false;
        }

        public String toString() {
            return "ArrayBuilder.ofInt";
        }
    }

    /* compiled from: ArrayBuilder.scala */
    public static class ofLong extends ArrayBuilder<Long> implements ScalaObject {
        private int capacity = 0;
        private long[] elems;
        private int size = 0;

        private long[] elems() {
            return this.elems;
        }

        private void elems_$eq(long[] jArr) {
            this.elems = jArr;
        }

        private int capacity() {
            return this.capacity;
        }

        private void capacity_$eq(int i) {
            this.capacity = i;
        }

        private int size() {
            return this.size;
        }

        private void size_$eq(int i) {
            this.size = i;
        }

        private long[] mkArray(int size2) {
            long[] newelems = new long[size2];
            if (size() > 0) {
                Array$.MODULE$.copy(elems(), 0, newelems, 0, size());
            }
            return newelems;
        }

        private void resize(int size2) {
            elems_$eq(mkArray(size2));
            capacity_$eq(size2);
        }

        public void sizeHint(int size2) {
            if (capacity() < size2) {
                resize(size2);
            }
        }

        private void ensureSize(int size2) {
            if (capacity() < size2 || capacity() == 0) {
                int newsize = capacity() == 0 ? 16 : capacity() * 2;
                while (newsize < size2) {
                    newsize *= 2;
                }
                resize(newsize);
            }
        }

        public ofLong $plus$eq(long elem) {
            ensureSize(size() + 1);
            elems()[size()] = elem;
            size_$eq(size() + 1);
            return this;
        }

        /* Debug info: failed to restart local var, previous not found, register: 6 */
        public ofLong $plus$plus$eq(TraversableOnce<Long> xs) {
            if (!(xs instanceof WrappedArray.ofLong)) {
                return (ofLong) Growable.Cclass.$plus$plus$eq(this, xs);
            }
            WrappedArray.ofLong oflong = (WrappedArray.ofLong) xs;
            ensureSize(size() + oflong.array.length);
            Array$.MODULE$.copy(oflong.array, 0, elems(), size(), oflong.array.length);
            size_$eq(size() + oflong.array().length);
            return this;
        }

        public long[] result() {
            if (capacity() == 0 || capacity() != size()) {
                return mkArray(size());
            }
            return elems();
        }

        public boolean equals(Object other) {
            if (!(other instanceof ofLong)) {
                return false;
            }
            ofLong oflong = (ofLong) other;
            if (size() == oflong.size() && elems() == oflong.elems()) {
                return true;
            }
            return false;
        }

        public String toString() {
            return "ArrayBuilder.ofLong";
        }
    }

    /* compiled from: ArrayBuilder.scala */
    public static class ofFloat extends ArrayBuilder<Float> implements ScalaObject {
        private int capacity = 0;
        private float[] elems;
        private int size = 0;

        private float[] elems() {
            return this.elems;
        }

        private void elems_$eq(float[] fArr) {
            this.elems = fArr;
        }

        private int capacity() {
            return this.capacity;
        }

        private void capacity_$eq(int i) {
            this.capacity = i;
        }

        private int size() {
            return this.size;
        }

        private void size_$eq(int i) {
            this.size = i;
        }

        private float[] mkArray(int size2) {
            float[] newelems = new float[size2];
            if (size() > 0) {
                Array$.MODULE$.copy(elems(), 0, newelems, 0, size());
            }
            return newelems;
        }

        private void resize(int size2) {
            elems_$eq(mkArray(size2));
            capacity_$eq(size2);
        }

        public void sizeHint(int size2) {
            if (capacity() < size2) {
                resize(size2);
            }
        }

        private void ensureSize(int size2) {
            if (capacity() < size2 || capacity() == 0) {
                int newsize = capacity() == 0 ? 16 : capacity() * 2;
                while (newsize < size2) {
                    newsize *= 2;
                }
                resize(newsize);
            }
        }

        public ofFloat $plus$eq(float elem) {
            ensureSize(size() + 1);
            elems()[size()] = elem;
            size_$eq(size() + 1);
            return this;
        }

        /* Debug info: failed to restart local var, previous not found, register: 6 */
        public ofFloat $plus$plus$eq(TraversableOnce<Float> xs) {
            if (!(xs instanceof WrappedArray.ofFloat)) {
                return (ofFloat) Growable.Cclass.$plus$plus$eq(this, xs);
            }
            WrappedArray.ofFloat offloat = (WrappedArray.ofFloat) xs;
            ensureSize(size() + offloat.array.length);
            Array$.MODULE$.copy(offloat.array, 0, elems(), size(), offloat.array.length);
            size_$eq(size() + offloat.array().length);
            return this;
        }

        public float[] result() {
            if (capacity() == 0 || capacity() != size()) {
                return mkArray(size());
            }
            return elems();
        }

        public boolean equals(Object other) {
            if (!(other instanceof ofFloat)) {
                return false;
            }
            ofFloat offloat = (ofFloat) other;
            if (size() == offloat.size() && elems() == offloat.elems()) {
                return true;
            }
            return false;
        }

        public String toString() {
            return "ArrayBuilder.ofFloat";
        }
    }

    /* compiled from: ArrayBuilder.scala */
    public static class ofDouble extends ArrayBuilder<Double> implements ScalaObject {
        private int capacity = 0;
        private double[] elems;
        private int size = 0;

        private double[] elems() {
            return this.elems;
        }

        private void elems_$eq(double[] dArr) {
            this.elems = dArr;
        }

        private int capacity() {
            return this.capacity;
        }

        private void capacity_$eq(int i) {
            this.capacity = i;
        }

        private int size() {
            return this.size;
        }

        private void size_$eq(int i) {
            this.size = i;
        }

        private double[] mkArray(int size2) {
            double[] newelems = new double[size2];
            if (size() > 0) {
                Array$.MODULE$.copy(elems(), 0, newelems, 0, size());
            }
            return newelems;
        }

        private void resize(int size2) {
            elems_$eq(mkArray(size2));
            capacity_$eq(size2);
        }

        public void sizeHint(int size2) {
            if (capacity() < size2) {
                resize(size2);
            }
        }

        private void ensureSize(int size2) {
            if (capacity() < size2 || capacity() == 0) {
                int newsize = capacity() == 0 ? 16 : capacity() * 2;
                while (newsize < size2) {
                    newsize *= 2;
                }
                resize(newsize);
            }
        }

        public ofDouble $plus$eq(double elem) {
            ensureSize(size() + 1);
            elems()[size()] = elem;
            size_$eq(size() + 1);
            return this;
        }

        /* Debug info: failed to restart local var, previous not found, register: 6 */
        public ofDouble $plus$plus$eq(TraversableOnce<Double> xs) {
            if (!(xs instanceof WrappedArray.ofDouble)) {
                return (ofDouble) Growable.Cclass.$plus$plus$eq(this, xs);
            }
            WrappedArray.ofDouble ofdouble = (WrappedArray.ofDouble) xs;
            ensureSize(size() + ofdouble.array.length);
            Array$.MODULE$.copy(ofdouble.array, 0, elems(), size(), ofdouble.array.length);
            size_$eq(size() + ofdouble.array().length);
            return this;
        }

        public double[] result() {
            if (capacity() == 0 || capacity() != size()) {
                return mkArray(size());
            }
            return elems();
        }

        public boolean equals(Object other) {
            if (!(other instanceof ofDouble)) {
                return false;
            }
            ofDouble ofdouble = (ofDouble) other;
            if (size() == ofdouble.size() && elems() == ofdouble.elems()) {
                return true;
            }
            return false;
        }

        public String toString() {
            return "ArrayBuilder.ofDouble";
        }
    }

    /* compiled from: ArrayBuilder.scala */
    public static class ofBoolean extends ArrayBuilder<Boolean> implements ScalaObject {
        private int capacity = 0;
        private boolean[] elems;
        private int size = 0;

        private boolean[] elems() {
            return this.elems;
        }

        private void elems_$eq(boolean[] zArr) {
            this.elems = zArr;
        }

        private int capacity() {
            return this.capacity;
        }

        private void capacity_$eq(int i) {
            this.capacity = i;
        }

        private int size() {
            return this.size;
        }

        private void size_$eq(int i) {
            this.size = i;
        }

        private boolean[] mkArray(int size2) {
            boolean[] newelems = new boolean[size2];
            if (size() > 0) {
                Array$.MODULE$.copy(elems(), 0, newelems, 0, size());
            }
            return newelems;
        }

        private void resize(int size2) {
            elems_$eq(mkArray(size2));
            capacity_$eq(size2);
        }

        public void sizeHint(int size2) {
            if (capacity() < size2) {
                resize(size2);
            }
        }

        private void ensureSize(int size2) {
            if (capacity() < size2 || capacity() == 0) {
                int newsize = capacity() == 0 ? 16 : capacity() * 2;
                while (newsize < size2) {
                    newsize *= 2;
                }
                resize(newsize);
            }
        }

        public ofBoolean $plus$eq(boolean elem) {
            ensureSize(size() + 1);
            elems()[size()] = elem;
            size_$eq(size() + 1);
            return this;
        }

        /* Debug info: failed to restart local var, previous not found, register: 6 */
        public ofBoolean $plus$plus$eq(TraversableOnce<Boolean> xs) {
            if (!(xs instanceof WrappedArray.ofBoolean)) {
                return (ofBoolean) Growable.Cclass.$plus$plus$eq(this, xs);
            }
            WrappedArray.ofBoolean ofboolean = (WrappedArray.ofBoolean) xs;
            ensureSize(size() + ofboolean.array.length);
            Array$.MODULE$.copy(ofboolean.array, 0, elems(), size(), ofboolean.array.length);
            size_$eq(size() + ofboolean.array().length);
            return this;
        }

        public boolean[] result() {
            if (capacity() == 0 || capacity() != size()) {
                return mkArray(size());
            }
            return elems();
        }

        public boolean equals(Object other) {
            if (!(other instanceof ofBoolean)) {
                return false;
            }
            ofBoolean ofboolean = (ofBoolean) other;
            if (size() == ofboolean.size() && elems() == ofboolean.elems()) {
                return true;
            }
            return false;
        }

        public String toString() {
            return "ArrayBuilder.ofBoolean";
        }
    }

    /* compiled from: ArrayBuilder.scala */
    public static class ofUnit extends ArrayBuilder<Object> implements ScalaObject {
        private int capacity = 0;
        private BoxedUnit[] elems;
        private int size = 0;

        private BoxedUnit[] elems() {
            return this.elems;
        }

        private void elems_$eq(BoxedUnit[] boxedUnitArr) {
            this.elems = boxedUnitArr;
        }

        private int capacity() {
            return this.capacity;
        }

        private void capacity_$eq(int i) {
            this.capacity = i;
        }

        private int size() {
            return this.size;
        }

        private void size_$eq(int i) {
            this.size = i;
        }

        private BoxedUnit[] mkArray(int size2) {
            BoxedUnit[] newelems = new BoxedUnit[size2];
            if (size() > 0) {
                Array$.MODULE$.copy(elems(), 0, newelems, 0, size());
            }
            return newelems;
        }

        private void resize(int size2) {
            elems_$eq(mkArray(size2));
            capacity_$eq(size2);
        }

        public void sizeHint(int size2) {
            if (capacity() < size2) {
                resize(size2);
            }
        }

        private void ensureSize(int size2) {
            if (capacity() < size2 || capacity() == 0) {
                int newsize = capacity() == 0 ? 16 : capacity() * 2;
                while (newsize < size2) {
                    newsize *= 2;
                }
                resize(newsize);
            }
        }

        public ofUnit $plus$eq(BoxedUnit elem) {
            ensureSize(size() + 1);
            elems()[size()] = elem;
            size_$eq(size() + 1);
            return this;
        }

        /* Debug info: failed to restart local var, previous not found, register: 6 */
        public ofUnit $plus$plus$eq(TraversableOnce<Object> xs) {
            if (!(xs instanceof WrappedArray.ofUnit)) {
                return (ofUnit) Growable.Cclass.$plus$plus$eq(this, xs);
            }
            WrappedArray.ofUnit ofunit = (WrappedArray.ofUnit) xs;
            ensureSize(size() + ofunit.array.length);
            Array$.MODULE$.copy(ofunit.array, 0, elems(), size(), ofunit.array.length);
            size_$eq(size() + ofunit.array().length);
            return this;
        }

        public BoxedUnit[] result() {
            if (capacity() == 0 || capacity() != size()) {
                return mkArray(size());
            }
            return elems();
        }

        public boolean equals(Object other) {
            if (!(other instanceof ofUnit)) {
                return false;
            }
            ofUnit ofunit = (ofUnit) other;
            if (size() == ofunit.size() && elems() == ofunit.elems()) {
                return true;
            }
            return false;
        }

        public String toString() {
            return "ArrayBuilder.ofUnit";
        }
    }
}
