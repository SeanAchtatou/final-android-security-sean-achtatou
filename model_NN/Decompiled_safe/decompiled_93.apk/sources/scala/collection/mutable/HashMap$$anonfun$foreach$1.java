package scala.collection.mutable;

import java.io.Serializable;
import scala.Function1;
import scala.Tuple2;
import scala.runtime.AbstractFunction1;

/* compiled from: HashMap.scala */
public final class HashMap$$anonfun$foreach$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Function1 f$1;

    public HashMap$$anonfun$foreach$1(HashMap $outer, HashMap<A, B> hashMap) {
        this.f$1 = hashMap;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return apply((DefaultEntry) ((DefaultEntry) v1));
    }

    public final C apply(DefaultEntry<A, B> e) {
        return this.f$1.apply(new Tuple2(e.key, e.value));
    }
}
