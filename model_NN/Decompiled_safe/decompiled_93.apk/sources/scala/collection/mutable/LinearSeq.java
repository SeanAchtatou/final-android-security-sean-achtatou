package scala.collection.mutable;

import scala.ScalaObject;
import scala.collection.LinearSeqLike;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericTraversableTemplate;

/* compiled from: LinearSeq.scala */
public interface LinearSeq<A> extends Seq<A>, scala.collection.LinearSeq<A>, GenericTraversableTemplate<A, LinearSeq>, LinearSeqLike<A, LinearSeq<A>>, ScalaObject {

    /* renamed from: scala.collection.mutable.LinearSeq$class  reason: invalid class name */
    /* compiled from: LinearSeq.scala */
    public abstract class Cclass {
        public static void $init$(LinearSeq $this) {
        }

        public static GenericCompanion companion(LinearSeq $this) {
            return LinearSeq$.MODULE$;
        }
    }
}
