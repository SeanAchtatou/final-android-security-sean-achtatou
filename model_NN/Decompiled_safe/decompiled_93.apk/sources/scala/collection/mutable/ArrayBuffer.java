package scala.collection.mutable;

import scala.Function1;
import scala.Function2;
import scala.PartialFunction;
import scala.ScalaObject;
import scala.Tuple2;
import scala.collection.IndexedSeq;
import scala.collection.IndexedSeqLike;
import scala.collection.IndexedSeqOptimized;
import scala.collection.Iterable;
import scala.collection.IterableLike;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.SeqLike;
import scala.collection.Traversable;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericTraversableTemplate;
import scala.collection.generic.Growable;
import scala.collection.generic.Shrinkable;
import scala.collection.generic.Subtractable;
import scala.collection.immutable.List;
import scala.collection.immutable.Set;
import scala.collection.immutable.Stream;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.BufferLike;
import scala.collection.mutable.Builder;
import scala.collection.mutable.Cloneable;
import scala.collection.mutable.IndexedSeq;
import scala.collection.mutable.IndexedSeqLike;
import scala.collection.mutable.Iterable;
import scala.collection.mutable.ResizableArray;
import scala.collection.mutable.Seq;
import scala.collection.mutable.Traversable;
import scala.math.Numeric;
import scala.math.Ordering;
import scala.reflect.ClassManifest;
import scala.runtime.BoxesRunTime;

/* compiled from: ArrayBuffer.scala */
public class ArrayBuffer<A> implements Buffer<A>, GenericTraversableTemplate<A, ArrayBuffer>, BufferLike<A, ArrayBuffer<A>>, IndexedSeqOptimized<A, ArrayBuffer<A>>, Builder<A, ArrayBuffer<A>>, ResizableArray<A>, ScalaObject, ResizableArray {
    public static final long serialVersionUID = 1529165946227428979L;
    private Object[] array;
    private final int initialSize;
    private int size0;

    public ArrayBuffer(int initialSize2) {
        this.initialSize = initialSize2;
        TraversableOnce.Cclass.$init$(this);
        TraversableLike.Cclass.$init$(this);
        GenericTraversableTemplate.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        IterableLike.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Function1.Cclass.$init$(this);
        PartialFunction.Cclass.$init$(this);
        SeqLike.Cclass.$init$(this);
        Seq.Cclass.$init$(this);
        Seq.Cclass.$init$(this);
        Growable.Cclass.$init$(this);
        Shrinkable.Cclass.$init$(this);
        Subtractable.Cclass.$init$(this);
        Cloneable.Cclass.$init$(this);
        BufferLike.Cclass.$init$(this);
        Buffer.Cclass.$init$(this);
        IndexedSeqLike.Cclass.$init$(this);
        IndexedSeqLike.Cclass.$init$(this);
        IndexedSeqOptimized.Cclass.$init$(this);
        Builder.Cclass.$init$(this);
        IndexedSeq.Cclass.$init$(this);
        IndexedSeq.Cclass.$init$(this);
        ResizableArray.Cclass.$init$(this);
    }

    public <B> B $div$colon(B z, Function2<B, A, B> op) {
        return TraversableOnce.Cclass.$div$colon(this, z, op);
    }

    public <B, That> That $plus$plus(TraversableOnce<B> that, CanBuildFrom<ArrayBuffer<A>, B, That> bf) {
        return TraversableLike.Cclass.$plus$plus(this, that, bf);
    }

    public StringBuilder addString(StringBuilder b, String start, String sep, String end) {
        return TraversableOnce.Cclass.addString(this, b, start, sep, end);
    }

    public <C> PartialFunction<Integer, C> andThen(Function1<A, C> k) {
        return PartialFunction.Cclass.andThen(this, k);
    }

    public A apply(int idx) {
        return ResizableArray.Cclass.apply(this, idx);
    }

    public /* bridge */ /* synthetic */ Object apply(Object v1) {
        return apply(BoxesRunTime.unboxToInt(v1));
    }

    public int apply$mcII$sp(int v1) {
        return Function1.Cclass.apply$mcII$sp(this, v1);
    }

    public void apply$mcVI$sp(int v1) {
        Function1.Cclass.apply$mcVI$sp(this, v1);
    }

    public void apply$mcVL$sp(long v1) {
        Function1.Cclass.apply$mcVL$sp(this, v1);
    }

    public Object[] array() {
        return this.array;
    }

    public void array_$eq(Object[] objArr) {
        this.array = objArr;
    }

    public boolean canEqual(Object that) {
        return IterableLike.Cclass.canEqual(this, that);
    }

    public <A> Function1<A, A> compose(Function1<A, Integer> g) {
        return Function1.Cclass.compose(this, g);
    }

    public boolean contains(Object elem) {
        return SeqLike.Cclass.contains(this, elem);
    }

    public <B> void copyToArray(Object xs, int start) {
        TraversableOnce.Cclass.copyToArray(this, xs, start);
    }

    public <B> void copyToArray(Object xs, int start, int len) {
        ResizableArray.Cclass.copyToArray(this, xs, start, len);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.mutable.ArrayBuffer<A>] */
    public ArrayBuffer<A> drop(int n) {
        return IndexedSeqOptimized.Cclass.drop(this, n);
    }

    public void ensureSize(int n) {
        ResizableArray.Cclass.ensureSize(this, n);
    }

    public boolean equals(Object that) {
        return SeqLike.Cclass.equals(this, that);
    }

    public boolean exists(Function1<A, Boolean> p) {
        return IndexedSeqOptimized.Cclass.exists(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.mutable.ArrayBuffer<A>] */
    public ArrayBuffer<A> filter(Function1<A, Boolean> p) {
        return TraversableLike.Cclass.filter(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.mutable.ArrayBuffer<A>] */
    public ArrayBuffer<A> filterNot(Function1<A, Boolean> p) {
        return TraversableLike.Cclass.filterNot(this, p);
    }

    public <B> B foldLeft(B z, Function2<B, A, B> op) {
        return IndexedSeqOptimized.Cclass.foldLeft(this, z, op);
    }

    public boolean forall(Function1<A, Boolean> p) {
        return IndexedSeqOptimized.Cclass.forall(this, p);
    }

    public <U> void foreach(Function1<A, U> f) {
        ResizableArray.Cclass.foreach(this, f);
    }

    public <B> Builder<B, ArrayBuffer<B>> genericBuilder() {
        return GenericTraversableTemplate.Cclass.genericBuilder(this);
    }

    public int hashCode() {
        return SeqLike.Cclass.hashCode(this);
    }

    public A head() {
        return IndexedSeqOptimized.Cclass.head(this);
    }

    public <B> int indexOf(B elem) {
        return SeqLike.Cclass.indexOf(this, elem);
    }

    public <B> int indexOf(B elem, int from) {
        return SeqLike.Cclass.indexOf(this, elem, from);
    }

    public int indexWhere(Function1<A, Boolean> p, int from) {
        return IndexedSeqOptimized.Cclass.indexWhere(this, p, from);
    }

    public int initialSize() {
        return this.initialSize;
    }

    public boolean isDefinedAt(int idx) {
        return SeqLike.Cclass.isDefinedAt(this, idx);
    }

    public /* bridge */ /* synthetic */ boolean isDefinedAt(Object x) {
        return isDefinedAt(BoxesRunTime.unboxToInt(x));
    }

    public boolean isEmpty() {
        return IndexedSeqOptimized.Cclass.isEmpty(this);
    }

    public final boolean isTraversableAgain() {
        return TraversableLike.Cclass.isTraversableAgain(this);
    }

    public Iterator<A> iterator() {
        return IndexedSeqLike.Cclass.iterator(this);
    }

    public A last() {
        return IndexedSeqOptimized.Cclass.last(this);
    }

    public int length() {
        return ResizableArray.Cclass.length(this);
    }

    public <B, That> That map(Function1<A, B> f, CanBuildFrom<ArrayBuffer<A>, B, That> bf) {
        return TraversableLike.Cclass.map(this, f, bf);
    }

    public <NewTo> Builder<A, NewTo> mapResult(Function1<ArrayBuffer<A>, NewTo> f) {
        return Builder.Cclass.mapResult(this, f);
    }

    public String mkString() {
        return TraversableOnce.Cclass.mkString(this);
    }

    public String mkString(String sep) {
        return TraversableOnce.Cclass.mkString(this, sep);
    }

    public String mkString(String start, String sep, String end) {
        return TraversableOnce.Cclass.mkString(this, start, sep, end);
    }

    public Builder<A, ArrayBuffer<A>> newBuilder() {
        return GenericTraversableTemplate.Cclass.newBuilder(this);
    }

    public boolean nonEmpty() {
        return TraversableOnce.Cclass.nonEmpty(this);
    }

    public int prefixLength(Function1<A, Boolean> p) {
        return SeqLike.Cclass.prefixLength(this, p);
    }

    public <B> B reduceLeft(Function2<B, A, B> op) {
        return IndexedSeqOptimized.Cclass.reduceLeft(this, op);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.mutable.ArrayBuffer<A>] */
    public ArrayBuffer<A> repr() {
        return TraversableLike.Cclass.repr(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.mutable.ArrayBuffer<A>] */
    public ArrayBuffer<A> reverse() {
        return IndexedSeqOptimized.Cclass.reverse(this);
    }

    public Iterator<A> reverseIterator() {
        return IndexedSeqOptimized.Cclass.reverseIterator(this);
    }

    public <B> boolean sameElements(scala.collection.Iterable<B> that) {
        return IndexedSeqOptimized.Cclass.sameElements(this, that);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$head() {
        return IterableLike.Cclass.head(this);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$last() {
        return TraversableLike.Cclass.last(this);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$reduceLeft(Function2 op) {
        return TraversableOnce.Cclass.reduceLeft(this, op);
    }

    public final boolean scala$collection$IndexedSeqOptimized$$super$sameElements(scala.collection.Iterable that) {
        return IterableLike.Cclass.sameElements(this, that);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$tail() {
        return TraversableLike.Cclass.tail(this);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$zip(scala.collection.Iterable that, CanBuildFrom bf) {
        return IterableLike.Cclass.zip(this, that, bf);
    }

    public final Object scala$collection$mutable$Cloneable$$super$clone() {
        return super.clone();
    }

    public int segmentLength(Function1<A, Boolean> p, int from) {
        return IndexedSeqOptimized.Cclass.segmentLength(this, p, from);
    }

    public int size() {
        return SeqLike.Cclass.size(this);
    }

    public int size0() {
        return this.size0;
    }

    public void size0_$eq(int i) {
        this.size0 = i;
    }

    public void sizeHint(TraversableLike<?, ?> coll, int delta) {
        Builder.Cclass.sizeHint(this, coll, delta);
    }

    public /* synthetic */ int sizeHint$default$2() {
        return Builder.Cclass.sizeHint$default$2(this);
    }

    public void sizeHintBounded(int size, TraversableLike<?, ?> boundingColl) {
        Builder.Cclass.sizeHintBounded(this, size, boundingColl);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.mutable.ArrayBuffer<A>] */
    public ArrayBuffer<A> slice(int from, int until) {
        return IndexedSeqOptimized.Cclass.slice(this, from, until);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.mutable.ArrayBuffer<A>] */
    public <B> ArrayBuffer<A> sortBy(Function1<A, B> f, Ordering<B> ord) {
        return SeqLike.Cclass.sortBy(this, f, ord);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.mutable.ArrayBuffer<A>] */
    public <B> ArrayBuffer<A> sorted(Ordering<B> ord) {
        return SeqLike.Cclass.sorted(this, ord);
    }

    public <B> B sum(Numeric<B> num) {
        return TraversableOnce.Cclass.sum(this, num);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.collection.mutable.ArrayBuffer<A>] */
    public ArrayBuffer<A> tail() {
        return IndexedSeqOptimized.Cclass.tail(this);
    }

    public IndexedSeq<A> thisCollection() {
        return IndexedSeqLike.Cclass.thisCollection(this);
    }

    public <B> Object toArray(ClassManifest<B> evidence$1) {
        return TraversableOnce.Cclass.toArray(this, evidence$1);
    }

    public <B> Buffer<B> toBuffer() {
        return TraversableOnce.Cclass.toBuffer(this);
    }

    public IndexedSeq<A> toCollection(ArrayBuffer<A> repr) {
        return IndexedSeqLike.Cclass.toCollection(this, repr);
    }

    public List<A> toList() {
        return TraversableOnce.Cclass.toList(this);
    }

    public <B> Set<B> toSet() {
        return TraversableOnce.Cclass.toSet(this);
    }

    public Stream<A> toStream() {
        return IterableLike.Cclass.toStream(this);
    }

    public String toString() {
        return SeqLike.Cclass.toString(this);
    }

    public <A1, B, That> That zip(scala.collection.Iterable<B> that, CanBuildFrom<ArrayBuffer<A>, Tuple2<A1, B>, That> bf) {
        return IndexedSeqOptimized.Cclass.zip(this, that, bf);
    }

    public GenericCompanion<ArrayBuffer> companion() {
        return ArrayBuffer$.MODULE$;
    }

    public ArrayBuffer() {
        this(16);
    }

    public void sizeHint(int len) {
        if (len > size() && len >= 1) {
            Object[] newarray = new Object[len];
            System.arraycopy(array(), 0, newarray, 0, size0());
            array_$eq(newarray);
        }
    }

    public ArrayBuffer<A> $plus$eq(A elem) {
        ensureSize(size0() + 1);
        array()[size0()] = elem;
        size0_$eq(size0() + 1);
        return this;
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public ArrayBuffer<A> $plus$plus$eq(TraversableOnce xs) {
        if (!(xs instanceof IndexedSeq)) {
            return (ArrayBuffer) Growable.Cclass.$plus$plus$eq(this, xs);
        }
        IndexedSeq indexedSeq = (IndexedSeq) xs;
        int n = indexedSeq.length();
        ensureSize(size0() + n);
        indexedSeq.copyToArray(array(), size0(), n);
        size0_$eq(size0() + n);
        return this;
    }

    public ArrayBuffer<A> clone() {
        return new ArrayBuffer().$plus$plus$eq((TraversableOnce) this);
    }

    public ArrayBuffer<A> result() {
        return this;
    }

    public String stringPrefix() {
        return "ArrayBuffer";
    }
}
