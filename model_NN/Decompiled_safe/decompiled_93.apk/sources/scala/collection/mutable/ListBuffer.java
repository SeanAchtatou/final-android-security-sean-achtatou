package scala.collection.mutable;

import scala.Function1;
import scala.Function2;
import scala.PartialFunction;
import scala.ScalaObject;
import scala.Tuple2;
import scala.collection.Iterable;
import scala.collection.IterableLike;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.SeqLike;
import scala.collection.Traversable;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericTraversableTemplate;
import scala.collection.generic.Growable;
import scala.collection.generic.IterableForwarder;
import scala.collection.generic.SeqForwarder;
import scala.collection.generic.Shrinkable;
import scala.collection.generic.Subtractable;
import scala.collection.generic.TraversableForwarder;
import scala.collection.immutable.C$colon$colon;
import scala.collection.immutable.List;
import scala.collection.immutable.Nil$;
import scala.collection.immutable.Set;
import scala.collection.immutable.Stream;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.BufferLike;
import scala.collection.mutable.Builder;
import scala.collection.mutable.Cloneable;
import scala.collection.mutable.Iterable;
import scala.collection.mutable.Seq;
import scala.collection.mutable.Traversable;
import scala.math.Numeric;
import scala.math.Ordering;
import scala.reflect.ClassManifest;
import scala.runtime.BoxesRunTime;

/* compiled from: ListBuffer.scala */
public final class ListBuffer<A> implements Buffer<A>, GenericTraversableTemplate<A, ListBuffer>, BufferLike<A, ListBuffer<A>>, Builder<A, List<A>>, SeqForwarder<A>, ScalaObject, Builder {
    public static final long serialVersionUID = 3419063961353022661L;
    private boolean exported = false;
    private int len = 0;
    private C$colon$colon scala$collection$mutable$ListBuffer$$last0;
    private List scala$collection$mutable$ListBuffer$$start = Nil$.MODULE$;

    public <B> B $div$colon(B z, Function2<B, A, B> op) {
        return TraversableForwarder.Cclass.$div$colon(this, z, op);
    }

    public <B, That> That $plus$plus(TraversableOnce<B> that, CanBuildFrom<ListBuffer<A>, B, That> bf) {
        return TraversableLike.Cclass.$plus$plus(this, that, bf);
    }

    public Growable<A> $plus$plus$eq(TraversableOnce<A> xs) {
        return Growable.Cclass.$plus$plus$eq(this, xs);
    }

    public StringBuilder addString(StringBuilder b, String start, String sep, String end) {
        return TraversableForwarder.Cclass.addString(this, b, start, sep, end);
    }

    public <C> PartialFunction<Integer, C> andThen(Function1<A, C> k) {
        return PartialFunction.Cclass.andThen(this, k);
    }

    public /* bridge */ /* synthetic */ Object apply(Object v1) {
        return apply(BoxesRunTime.unboxToInt(v1));
    }

    public int apply$mcII$sp(int v1) {
        return Function1.Cclass.apply$mcII$sp(this, v1);
    }

    public void apply$mcVI$sp(int v1) {
        Function1.Cclass.apply$mcVI$sp(this, v1);
    }

    public void apply$mcVL$sp(long v1) {
        Function1.Cclass.apply$mcVL$sp(this, v1);
    }

    public boolean canEqual(Object that) {
        return IterableLike.Cclass.canEqual(this, that);
    }

    public <A> Function1<A, A> compose(Function1<A, Integer> g) {
        return Function1.Cclass.compose(this, g);
    }

    public boolean contains(Object elem) {
        return SeqForwarder.Cclass.contains(this, elem);
    }

    public <B> void copyToArray(Object xs, int start) {
        TraversableForwarder.Cclass.copyToArray(this, xs, start);
    }

    public <B> void copyToArray(Object xs, int start, int len2) {
        TraversableForwarder.Cclass.copyToArray(this, xs, start, len2);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.ListBuffer<A>, java.lang.Object] */
    public ListBuffer<A> drop(int n) {
        return TraversableLike.Cclass.drop(this, n);
    }

    public boolean exists(Function1<A, Boolean> p) {
        return TraversableForwarder.Cclass.exists(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.ListBuffer<A>, java.lang.Object] */
    public ListBuffer<A> filter(Function1<A, Boolean> p) {
        return TraversableLike.Cclass.filter(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.ListBuffer<A>, java.lang.Object] */
    public ListBuffer<A> filterNot(Function1<A, Boolean> p) {
        return TraversableLike.Cclass.filterNot(this, p);
    }

    public <B> B foldLeft(B z, Function2<B, A, B> op) {
        return TraversableForwarder.Cclass.foldLeft(this, z, op);
    }

    public boolean forall(Function1<A, Boolean> p) {
        return TraversableForwarder.Cclass.forall(this, p);
    }

    public <B> void foreach(Function1<A, B> f) {
        TraversableForwarder.Cclass.foreach(this, f);
    }

    public <B> Builder<B, ListBuffer<B>> genericBuilder() {
        return GenericTraversableTemplate.Cclass.genericBuilder(this);
    }

    public int hashCode() {
        return SeqLike.Cclass.hashCode(this);
    }

    public A head() {
        return TraversableForwarder.Cclass.head(this);
    }

    public <B> int indexOf(B elem) {
        return SeqForwarder.Cclass.indexOf(this, elem);
    }

    public <B> int indexOf(B elem, int from) {
        return SeqForwarder.Cclass.indexOf(this, elem, from);
    }

    public int indexWhere(Function1<A, Boolean> p, int from) {
        return SeqForwarder.Cclass.indexWhere(this, p, from);
    }

    public boolean isDefinedAt(int x) {
        return SeqForwarder.Cclass.isDefinedAt(this, x);
    }

    public /* bridge */ /* synthetic */ boolean isDefinedAt(Object x) {
        return isDefinedAt(BoxesRunTime.unboxToInt(x));
    }

    public boolean isEmpty() {
        return TraversableForwarder.Cclass.isEmpty(this);
    }

    public final boolean isTraversableAgain() {
        return TraversableLike.Cclass.isTraversableAgain(this);
    }

    public A last() {
        return TraversableForwarder.Cclass.last(this);
    }

    public <B, That> That map(Function1<A, B> f, CanBuildFrom<ListBuffer<A>, B, That> bf) {
        return TraversableLike.Cclass.map(this, f, bf);
    }

    public <NewTo> Builder<A, NewTo> mapResult(Function1<List<A>, NewTo> f) {
        return Builder.Cclass.mapResult(this, f);
    }

    public String mkString() {
        return TraversableForwarder.Cclass.mkString(this);
    }

    public String mkString(String sep) {
        return TraversableForwarder.Cclass.mkString(this, sep);
    }

    public String mkString(String start, String sep, String end) {
        return TraversableForwarder.Cclass.mkString(this, start, sep, end);
    }

    public Builder<A, ListBuffer<A>> newBuilder() {
        return GenericTraversableTemplate.Cclass.newBuilder(this);
    }

    public boolean nonEmpty() {
        return TraversableForwarder.Cclass.nonEmpty(this);
    }

    public int prefixLength(Function1<A, Boolean> p) {
        return SeqForwarder.Cclass.prefixLength(this, p);
    }

    public <B> B reduceLeft(Function2<B, A, B> op) {
        return TraversableForwarder.Cclass.reduceLeft(this, op);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.ListBuffer<A>, java.lang.Object] */
    public ListBuffer<A> repr() {
        return TraversableLike.Cclass.repr(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.ListBuffer<A>, java.lang.Object] */
    public ListBuffer<A> reverse() {
        return SeqLike.Cclass.reverse(this);
    }

    public Iterator<A> reverseIterator() {
        return SeqForwarder.Cclass.reverseIterator(this);
    }

    public <B> boolean sameElements(Iterable<B> that) {
        return IterableForwarder.Cclass.sameElements(this, that);
    }

    public final Object scala$collection$mutable$Cloneable$$super$clone() {
        return super.clone();
    }

    public int segmentLength(Function1<A, Boolean> p, int from) {
        return SeqForwarder.Cclass.segmentLength(this, p, from);
    }

    public int size() {
        return TraversableForwarder.Cclass.size(this);
    }

    public void sizeHint(int size) {
        Builder.Cclass.sizeHint(this, size);
    }

    public void sizeHint(TraversableLike<?, ?> coll, int delta) {
        Builder.Cclass.sizeHint(this, coll, delta);
    }

    public /* synthetic */ int sizeHint$default$2() {
        return Builder.Cclass.sizeHint$default$2(this);
    }

    public void sizeHintBounded(int size, TraversableLike<?, ?> boundingColl) {
        Builder.Cclass.sizeHintBounded(this, size, boundingColl);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.ListBuffer<A>, java.lang.Object] */
    public ListBuffer<A> slice(int from, int until) {
        return IterableLike.Cclass.slice(this, from, until);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.ListBuffer<A>, java.lang.Object] */
    public <B> ListBuffer<A> sortBy(Function1<A, B> f, Ordering<B> ord) {
        return SeqLike.Cclass.sortBy(this, f, ord);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.ListBuffer<A>, java.lang.Object] */
    public <B> ListBuffer<A> sorted(Ordering<B> ord) {
        return SeqLike.Cclass.sorted(this, ord);
    }

    public <B> B sum(Numeric<B> num) {
        return TraversableForwarder.Cclass.sum(this, num);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.ListBuffer<A>, java.lang.Object] */
    public ListBuffer<A> tail() {
        return TraversableLike.Cclass.tail(this);
    }

    public Seq<A> thisCollection() {
        return SeqLike.Cclass.thisCollection(this);
    }

    public <B> Object toArray(ClassManifest<B> evidence$1) {
        return TraversableForwarder.Cclass.toArray(this, evidence$1);
    }

    public <B> Buffer<B> toBuffer() {
        return TraversableForwarder.Cclass.toBuffer(this);
    }

    public Seq<A> toCollection(ListBuffer<A> repr) {
        return SeqLike.Cclass.toCollection(this, repr);
    }

    public <B> Set<B> toSet() {
        return TraversableForwarder.Cclass.toSet(this);
    }

    public Stream<A> toStream() {
        return TraversableForwarder.Cclass.toStream(this);
    }

    public String toString() {
        return SeqLike.Cclass.toString(this);
    }

    public <A1, B, That> That zip(Iterable<B> that, CanBuildFrom<ListBuffer<A>, Tuple2<A1, B>, That> bf) {
        return IterableLike.Cclass.zip(this, that, bf);
    }

    public ListBuffer() {
        TraversableOnce.Cclass.$init$(this);
        TraversableLike.Cclass.$init$(this);
        GenericTraversableTemplate.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        IterableLike.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Function1.Cclass.$init$(this);
        PartialFunction.Cclass.$init$(this);
        SeqLike.Cclass.$init$(this);
        Seq.Cclass.$init$(this);
        Seq.Cclass.$init$(this);
        Growable.Cclass.$init$(this);
        Shrinkable.Cclass.$init$(this);
        Subtractable.Cclass.$init$(this);
        Cloneable.Cclass.$init$(this);
        BufferLike.Cclass.$init$(this);
        Buffer.Cclass.$init$(this);
        Builder.Cclass.$init$(this);
        TraversableForwarder.Cclass.$init$(this);
        IterableForwarder.Cclass.$init$(this);
        SeqForwarder.Cclass.$init$(this);
    }

    public GenericCompanion<ListBuffer> companion() {
        return ListBuffer$.MODULE$;
    }

    private void scala$collection$mutable$ListBuffer$$start_$eq(List list) {
        this.scala$collection$mutable$ListBuffer$$start = list;
    }

    public final List scala$collection$mutable$ListBuffer$$start() {
        return this.scala$collection$mutable$ListBuffer$$start;
    }

    private void scala$collection$mutable$ListBuffer$$last0_$eq(C$colon$colon _colon_colon) {
        this.scala$collection$mutable$ListBuffer$$last0 = _colon_colon;
    }

    public final C$colon$colon scala$collection$mutable$ListBuffer$$last0() {
        return this.scala$collection$mutable$ListBuffer$$last0;
    }

    private boolean exported() {
        return this.exported;
    }

    private void exported_$eq(boolean z) {
        this.exported = z;
    }

    private int len() {
        return this.len;
    }

    private void len_$eq(int i) {
        this.len = i;
    }

    public scala.collection.immutable.Seq<A> underlying() {
        return this.scala$collection$mutable$ListBuffer$$start;
    }

    public int length() {
        return len();
    }

    public A apply(int n) {
        if (n >= 0 && n < len()) {
            return SeqForwarder.Cclass.apply(this, n);
        }
        throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(n).toString());
    }

    public ListBuffer<A> $plus$eq(Object x) {
        if (exported()) {
            copy();
        }
        if (this.scala$collection$mutable$ListBuffer$$start.isEmpty()) {
            scala$collection$mutable$ListBuffer$$last0_$eq(new C$colon$colon(x, Nil$.MODULE$));
            scala$collection$mutable$ListBuffer$$start_$eq(this.scala$collection$mutable$ListBuffer$$last0);
        } else {
            C$colon$colon last1 = this.scala$collection$mutable$ListBuffer$$last0;
            scala$collection$mutable$ListBuffer$$last0_$eq(new C$colon$colon(x, Nil$.MODULE$));
            last1.tl = this.scala$collection$mutable$ListBuffer$$last0;
        }
        len_$eq(len() + 1);
        return this;
    }

    public List<A> result() {
        return toList();
    }

    public List<A> toList() {
        exported_$eq(!this.scala$collection$mutable$ListBuffer$$start.isEmpty());
        return this.scala$collection$mutable$ListBuffer$$start;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [scala.collection.immutable.List<A>, scala.collection.immutable.List<B>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public scala.collection.immutable.List<A> prependToList(scala.collection.immutable.List<A> r2) {
        /*
            r1 = this;
            scala.collection.immutable.List r0 = r1.scala$collection$mutable$ListBuffer$$start
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x000a
            r0 = r2
        L_0x0009:
            return r0
        L_0x000a:
            scala.collection.immutable.$colon$colon r0 = r1.scala$collection$mutable$ListBuffer$$last0
            r0.tl = r2
            scala.collection.immutable.List r0 = r1.toList()
            goto L_0x0009
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.collection.mutable.ListBuffer.prependToList(scala.collection.immutable.List):scala.collection.immutable.List");
    }

    public Iterator iterator() {
        return new ListBuffer$$anon$1(this);
    }

    private void copy() {
        List limit = this.scala$collection$mutable$ListBuffer$$last0.tl;
        scala$collection$mutable$ListBuffer$$start_$eq(Nil$.MODULE$);
        exported_$eq(false);
        len_$eq(0);
        for (List cursor = this.scala$collection$mutable$ListBuffer$$start; cursor != limit; cursor = (List) cursor.tail()) {
            $plus$eq((Object) cursor.head());
        }
    }

    public boolean equals(Object that) {
        if (that instanceof ListBuffer) {
            return this.scala$collection$mutable$ListBuffer$$start.equals(((ListBuffer) that).scala$collection$mutable$ListBuffer$$start);
        }
        return SeqLike.Cclass.equals(this, that);
    }

    public ListBuffer<A> clone() {
        return (ListBuffer) Growable.Cclass.$plus$plus$eq(new ListBuffer(), this);
    }

    public String stringPrefix() {
        return "ListBuffer";
    }
}
