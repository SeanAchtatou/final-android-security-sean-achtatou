package scala.collection.mutable;

import scala.ScalaObject;
import scala.collection.generic.SeqFactory;

/* compiled from: Seq.scala */
public final class Seq$ extends SeqFactory<Seq> implements ScalaObject {
    public static final Seq$ MODULE$ = null;

    static {
        new Seq$();
    }

    private Seq$() {
        MODULE$ = this;
    }

    public <A> Builder<A, Seq<A>> newBuilder() {
        return new ArrayBuffer();
    }
}
