package scala.collection.mutable;

import java.io.Serializable;
import scala.ScalaObject;
import scala.collection.Seq;

/* compiled from: Stack.scala */
public final class Stack$ implements Serializable, ScalaObject {
    public static final Stack$ MODULE$ = null;

    static {
        new Stack$();
    }

    private Stack$() {
        MODULE$ = this;
    }

    public <A> Stack<A> apply(Seq<A> xs) {
        return new Stack().pushAll(xs);
    }
}
