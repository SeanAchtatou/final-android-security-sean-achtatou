package scala.collection.mutable;

import java.io.Serializable;
import scala.Tuple2;
import scala.runtime.AbstractFunction1;

/* compiled from: HashMap.scala */
public final class HashMap$$anonfun$iterator$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;

    public HashMap$$anonfun$iterator$1(HashMap<A, B> $outer) {
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return apply((DefaultEntry) ((DefaultEntry) v1));
    }

    public final Tuple2<A, B> apply(DefaultEntry<A, B> e) {
        return new Tuple2<>(e.key, e.value);
    }
}
