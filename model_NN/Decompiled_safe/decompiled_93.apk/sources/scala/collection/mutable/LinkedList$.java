package scala.collection.mutable;

import scala.ScalaObject;
import scala.collection.generic.SeqFactory;

/* compiled from: LinkedList.scala */
public final class LinkedList$ extends SeqFactory<LinkedList> implements ScalaObject, ScalaObject {
    public static final LinkedList$ MODULE$ = null;

    static {
        new LinkedList$();
    }

    private LinkedList$() {
        MODULE$ = this;
    }

    public <A> LinkedList<A> empty() {
        return new LinkedList<>();
    }

    public <A> Builder<A, LinkedList<A>> newBuilder() {
        return new MutableList().mapResult(new LinkedList$$anonfun$newBuilder$1());
    }
}
