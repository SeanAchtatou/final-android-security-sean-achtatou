package scala.collection.mutable;

import scala.ScalaObject;
import scala.collection.mutable.HashEntry;

/* compiled from: DefaultEntry.scala */
public final class DefaultEntry<A, B> implements HashEntry<A, DefaultEntry<A, B>>, ScalaObject, HashEntry {
    public final A key;
    private Object next;
    public B value;

    public DefaultEntry(A key2, B value2) {
        this.key = key2;
        this.value = value2;
        HashEntry.Cclass.$init$(this);
    }

    public A key() {
        return this.key;
    }

    public Object next() {
        return this.next;
    }

    public void next_$eq(Object obj) {
        this.next = obj;
    }
}
