package scala.collection.mutable;

import scala.Function1;
import scala.ScalaObject;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.Growable;
import scala.collection.mutable.Builder;

/* compiled from: GrowingBuilder.scala */
public class GrowingBuilder<Elem, To extends Growable<Elem>> implements Builder<Elem, To>, ScalaObject {
    private To elems;
    private final To empty;

    public GrowingBuilder(To empty2) {
        this.empty = empty2;
        Growable.Cclass.$init$(this);
        Builder.Cclass.$init$(this);
        this.elems = empty2;
    }

    public Growable<Elem> $plus$plus$eq(TraversableOnce<Elem> xs) {
        return Growable.Cclass.$plus$plus$eq(this, xs);
    }

    public <NewTo> Builder<Elem, NewTo> mapResult(Function1<To, NewTo> f) {
        return Builder.Cclass.mapResult(this, f);
    }

    public void sizeHint(int size) {
        Builder.Cclass.sizeHint(this, size);
    }

    public void sizeHint(TraversableLike<?, ?> coll, int delta) {
        Builder.Cclass.sizeHint(this, coll, delta);
    }

    public /* synthetic */ int sizeHint$default$2() {
        return Builder.Cclass.sizeHint$default$2(this);
    }

    public void sizeHintBounded(int size, TraversableLike<?, ?> boundingColl) {
        Builder.Cclass.sizeHintBounded(this, size, boundingColl);
    }

    public To elems() {
        return this.elems;
    }

    public GrowingBuilder<Elem, To> $plus$eq(Elem x) {
        elems().$plus$eq(x);
        return this;
    }

    public To result() {
        return elems();
    }
}
