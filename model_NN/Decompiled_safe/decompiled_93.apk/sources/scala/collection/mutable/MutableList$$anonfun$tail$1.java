package scala.collection.mutable;

import java.io.Serializable;
import scala.runtime.AbstractFunction0;

/* compiled from: MutableList.scala */
public final class MutableList$$anonfun$tail$1 extends AbstractFunction0 implements Serializable {
    public static final long serialVersionUID = 0;

    public MutableList$$anonfun$tail$1(MutableList<A> $outer) {
    }

    public final String apply() {
        return "tail of empty list";
    }
}
