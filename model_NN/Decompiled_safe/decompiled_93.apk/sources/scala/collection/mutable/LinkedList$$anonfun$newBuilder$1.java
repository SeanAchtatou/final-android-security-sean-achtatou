package scala.collection.mutable;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;

/* compiled from: LinkedList.scala */
public final class LinkedList$$anonfun$newBuilder$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return apply((MutableList) ((MutableList) v1));
    }

    public final LinkedList<A> apply(MutableList<A> l) {
        return l.toLinkedList();
    }
}
