package scala.collection.mutable;

import scala.Function1;
import scala.Predef$;
import scala.ScalaObject;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.Growable;
import scala.collection.mutable.Builder;

/* compiled from: LazyBuilder.scala */
public abstract class LazyBuilder<Elem, To> implements Builder<Elem, To>, ScalaObject {
    private ListBuffer<TraversableOnce<Elem>> parts = new ListBuffer<>();

    public LazyBuilder() {
        Growable.Cclass.$init$(this);
        Builder.Cclass.$init$(this);
    }

    public <NewTo> Builder<Elem, NewTo> mapResult(Function1<To, NewTo> f) {
        return Builder.Cclass.mapResult(this, f);
    }

    public void sizeHint(int size) {
        Builder.Cclass.sizeHint(this, size);
    }

    public void sizeHint(TraversableLike<?, ?> coll, int delta) {
        Builder.Cclass.sizeHint(this, coll, delta);
    }

    public /* synthetic */ int sizeHint$default$2() {
        return Builder.Cclass.sizeHint$default$2(this);
    }

    public void sizeHintBounded(int size, TraversableLike<?, ?> boundingColl) {
        Builder.Cclass.sizeHintBounded(this, size, boundingColl);
    }

    public ListBuffer<TraversableOnce<Elem>> parts() {
        return this.parts;
    }

    public LazyBuilder<Elem, To> $plus$eq(Elem x) {
        parts().$plus$eq((Object) Predef$.MODULE$.genericWrapArray(new Object[]{x}).toList());
        return this;
    }

    public LazyBuilder<Elem, To> $plus$plus$eq(TraversableOnce<Elem> xs) {
        parts().$plus$eq((Object) xs);
        return this;
    }
}
