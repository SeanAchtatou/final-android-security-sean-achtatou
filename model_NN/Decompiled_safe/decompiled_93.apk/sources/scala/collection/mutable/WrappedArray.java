package scala.collection.mutable;

import scala.Function1;
import scala.Function2;
import scala.None$;
import scala.PartialFunction;
import scala.ScalaObject;
import scala.Tuple2;
import scala.collection.IndexedSeq;
import scala.collection.IndexedSeqLike;
import scala.collection.IndexedSeqOptimized;
import scala.collection.Iterable;
import scala.collection.IterableLike;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.SeqLike;
import scala.collection.Traversable;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericTraversableTemplate;
import scala.collection.immutable.List;
import scala.collection.immutable.Nil$;
import scala.collection.immutable.Set;
import scala.collection.immutable.Stream;
import scala.collection.mutable.ArrayLike;
import scala.collection.mutable.IndexedSeq;
import scala.collection.mutable.IndexedSeqLike;
import scala.collection.mutable.Iterable;
import scala.collection.mutable.Seq;
import scala.collection.mutable.Traversable;
import scala.math.Numeric;
import scala.math.Ordering;
import scala.reflect.AnyValManifest;
import scala.reflect.ClassManifest;
import scala.reflect.ClassManifest$;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime$;

/* compiled from: WrappedArray.scala */
public abstract class WrappedArray<T> implements IndexedSeq<T>, ArrayLike<T, WrappedArray<T>>, ScalaObject {
    public abstract Object array();

    public abstract ClassManifest<T> elemManifest();

    public abstract void update(int i, T t);

    public WrappedArray() {
        TraversableOnce.Cclass.$init$(this);
        TraversableLike.Cclass.$init$(this);
        GenericTraversableTemplate.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        IterableLike.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Function1.Cclass.$init$(this);
        PartialFunction.Cclass.$init$(this);
        SeqLike.Cclass.$init$(this);
        Seq.Cclass.$init$(this);
        Seq.Cclass.$init$(this);
        IndexedSeqLike.Cclass.$init$(this);
        IndexedSeq.Cclass.$init$(this);
        IndexedSeqLike.Cclass.$init$(this);
        IndexedSeq.Cclass.$init$(this);
        IndexedSeqOptimized.Cclass.$init$(this);
        ArrayLike.Cclass.$init$(this);
    }

    public <B> B $div$colon(B z, Function2<B, T, B> op) {
        return TraversableOnce.Cclass.$div$colon(this, z, op);
    }

    public <B, That> That $plus$plus(TraversableOnce<B> that, CanBuildFrom<WrappedArray<T>, B, That> bf) {
        return TraversableLike.Cclass.$plus$plus(this, that, bf);
    }

    public StringBuilder addString(StringBuilder b, String start, String sep, String end) {
        return TraversableOnce.Cclass.addString(this, b, start, sep, end);
    }

    public <C> PartialFunction<Integer, C> andThen(Function1<T, C> k) {
        return PartialFunction.Cclass.andThen(this, k);
    }

    public int apply$mcII$sp(int v1) {
        return Function1.Cclass.apply$mcII$sp(this, v1);
    }

    public void apply$mcVI$sp(int v1) {
        Function1.Cclass.apply$mcVI$sp(this, v1);
    }

    public void apply$mcVL$sp(long v1) {
        Function1.Cclass.apply$mcVL$sp(this, v1);
    }

    public boolean canEqual(Object that) {
        return IterableLike.Cclass.canEqual(this, that);
    }

    public GenericCompanion<IndexedSeq> companion() {
        return IndexedSeq.Cclass.companion(this);
    }

    public <A> Function1<A, T> compose(Function1<A, Integer> g) {
        return Function1.Cclass.compose(this, g);
    }

    public boolean contains(Object elem) {
        return SeqLike.Cclass.contains(this, elem);
    }

    public <B> void copyToArray(Object xs, int start) {
        TraversableOnce.Cclass.copyToArray(this, xs, start);
    }

    public <B> void copyToArray(Object xs, int start, int len) {
        IndexedSeqOptimized.Cclass.copyToArray(this, xs, start, len);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.WrappedArray<T>, java.lang.Object] */
    public WrappedArray<T> drop(int n) {
        return IndexedSeqOptimized.Cclass.drop(this, n);
    }

    public boolean equals(Object that) {
        return SeqLike.Cclass.equals(this, that);
    }

    public boolean exists(Function1<T, Boolean> p) {
        return IndexedSeqOptimized.Cclass.exists(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.WrappedArray<T>, java.lang.Object] */
    public WrappedArray<T> filter(Function1<T, Boolean> p) {
        return TraversableLike.Cclass.filter(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.WrappedArray<T>, java.lang.Object] */
    public WrappedArray<T> filterNot(Function1<T, Boolean> p) {
        return TraversableLike.Cclass.filterNot(this, p);
    }

    public <B> B foldLeft(B z, Function2<B, T, B> op) {
        return IndexedSeqOptimized.Cclass.foldLeft(this, z, op);
    }

    public boolean forall(Function1<T, Boolean> p) {
        return IndexedSeqOptimized.Cclass.forall(this, p);
    }

    public <U> void foreach(Function1<T, U> f) {
        IndexedSeqOptimized.Cclass.foreach(this, f);
    }

    public <B> Builder<B, IndexedSeq<B>> genericBuilder() {
        return GenericTraversableTemplate.Cclass.genericBuilder(this);
    }

    public int hashCode() {
        return SeqLike.Cclass.hashCode(this);
    }

    public T head() {
        return IndexedSeqOptimized.Cclass.head(this);
    }

    public <B> int indexOf(B elem) {
        return SeqLike.Cclass.indexOf(this, elem);
    }

    public <B> int indexOf(B elem, int from) {
        return SeqLike.Cclass.indexOf(this, elem, from);
    }

    public int indexWhere(Function1<T, Boolean> p, int from) {
        return IndexedSeqOptimized.Cclass.indexWhere(this, p, from);
    }

    public boolean isDefinedAt(int idx) {
        return SeqLike.Cclass.isDefinedAt(this, idx);
    }

    public /* bridge */ /* synthetic */ boolean isDefinedAt(Object x) {
        return isDefinedAt(BoxesRunTime.unboxToInt(x));
    }

    public boolean isEmpty() {
        return IndexedSeqOptimized.Cclass.isEmpty(this);
    }

    public final boolean isTraversableAgain() {
        return TraversableLike.Cclass.isTraversableAgain(this);
    }

    public Iterator<T> iterator() {
        return IndexedSeqLike.Cclass.iterator(this);
    }

    public T last() {
        return IndexedSeqOptimized.Cclass.last(this);
    }

    public <B, That> That map(Function1<T, B> f, CanBuildFrom<WrappedArray<T>, B, That> bf) {
        return TraversableLike.Cclass.map(this, f, bf);
    }

    public String mkString() {
        return TraversableOnce.Cclass.mkString(this);
    }

    public String mkString(String sep) {
        return TraversableOnce.Cclass.mkString(this, sep);
    }

    public String mkString(String start, String sep, String end) {
        return TraversableOnce.Cclass.mkString(this, start, sep, end);
    }

    public boolean nonEmpty() {
        return TraversableOnce.Cclass.nonEmpty(this);
    }

    public int prefixLength(Function1<T, Boolean> p) {
        return SeqLike.Cclass.prefixLength(this, p);
    }

    public <B> B reduceLeft(Function2<B, T, B> op) {
        return IndexedSeqOptimized.Cclass.reduceLeft(this, op);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.WrappedArray<T>, java.lang.Object] */
    public WrappedArray<T> repr() {
        return TraversableLike.Cclass.repr(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.WrappedArray<T>, java.lang.Object] */
    public WrappedArray<T> reverse() {
        return IndexedSeqOptimized.Cclass.reverse(this);
    }

    public Iterator<T> reverseIterator() {
        return IndexedSeqOptimized.Cclass.reverseIterator(this);
    }

    public <B> boolean sameElements(scala.collection.Iterable<B> that) {
        return IndexedSeqOptimized.Cclass.sameElements(this, that);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$head() {
        return IterableLike.Cclass.head(this);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$last() {
        return TraversableLike.Cclass.last(this);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$reduceLeft(Function2 op) {
        return TraversableOnce.Cclass.reduceLeft(this, op);
    }

    public final boolean scala$collection$IndexedSeqOptimized$$super$sameElements(scala.collection.Iterable that) {
        return IterableLike.Cclass.sameElements(this, that);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$tail() {
        return TraversableLike.Cclass.tail(this);
    }

    public final Object scala$collection$IndexedSeqOptimized$$super$zip(scala.collection.Iterable that, CanBuildFrom bf) {
        return IterableLike.Cclass.zip(this, that, bf);
    }

    public int segmentLength(Function1<T, Boolean> p, int from) {
        return IndexedSeqOptimized.Cclass.segmentLength(this, p, from);
    }

    public int size() {
        return SeqLike.Cclass.size(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.WrappedArray<T>, java.lang.Object] */
    public WrappedArray<T> slice(int from, int until) {
        return IndexedSeqOptimized.Cclass.slice(this, from, until);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.WrappedArray<T>, java.lang.Object] */
    public <B> WrappedArray<T> sortBy(Function1<T, B> f, Ordering<B> ord) {
        return SeqLike.Cclass.sortBy(this, f, ord);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.WrappedArray<T>, java.lang.Object] */
    public <B> WrappedArray<T> sorted(Ordering<B> ord) {
        return SeqLike.Cclass.sorted(this, ord);
    }

    public <B> B sum(Numeric<B> num) {
        return TraversableOnce.Cclass.sum(this, num);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.WrappedArray<T>, java.lang.Object] */
    public WrappedArray<T> tail() {
        return IndexedSeqOptimized.Cclass.tail(this);
    }

    public <B> Buffer<B> toBuffer() {
        return TraversableOnce.Cclass.toBuffer(this);
    }

    public List<T> toList() {
        return TraversableOnce.Cclass.toList(this);
    }

    public <B> Set<B> toSet() {
        return TraversableOnce.Cclass.toSet(this);
    }

    public Stream<T> toStream() {
        return IterableLike.Cclass.toStream(this);
    }

    public String toString() {
        return SeqLike.Cclass.toString(this);
    }

    public <A1, B, That> That zip(scala.collection.Iterable<B> that, CanBuildFrom<WrappedArray<T>, Tuple2<A1, B>, That> bf) {
        return IndexedSeqOptimized.Cclass.zip(this, that, bf);
    }

    public WrappedArray<T> thisCollection() {
        return this;
    }

    public WrappedArray<T> toCollection(WrappedArray<T> repr) {
        return repr;
    }

    public <U> Object toArray(ClassManifest<U> evidence$1) {
        if (evidence$1.erasure() == array().getClass().getComponentType()) {
            return array();
        }
        return TraversableOnce.Cclass.toArray(this, evidence$1);
    }

    public String stringPrefix() {
        return "WrappedArray";
    }

    public WrappedArray<T> clone() {
        return WrappedArray$.MODULE$.make(ScalaRunTime$.MODULE$.array_clone(array()));
    }

    public Builder<T, WrappedArray<T>> newBuilder() {
        return new WrappedArrayBuilder(elemManifest());
    }

    /* compiled from: WrappedArray.scala */
    public static final class ofRef<T> extends WrappedArray<T> implements ScalaObject, ScalaObject {
        public final T[] array;
        public volatile int bitmap$0;
        private ClassManifest<T> elemManifest;

        public ofRef(T[] array2) {
            this.array = array2;
        }

        public /* bridge */ /* synthetic */ Object apply(Object v1) {
            return apply(BoxesRunTime.unboxToInt(v1));
        }

        public T[] array() {
            return this.array;
        }

        public ClassManifest<T> elemManifest() {
            if ((this.bitmap$0 & 1) == 0) {
                synchronized (this) {
                    if ((this.bitmap$0 & 1) == 0) {
                        this.elemManifest = new ClassManifest.ClassTypeManifest(None$.MODULE$, this.array.getClass().getComponentType(), Nil$.MODULE$);
                        this.bitmap$0 |= 1;
                    }
                }
            }
            return this.elemManifest;
        }

        public int length() {
            return this.array.length;
        }

        public T apply(int index) {
            return this.array[index];
        }

        public void update(int index, T elem) {
            this.array[index] = elem;
        }
    }

    /* compiled from: WrappedArray.scala */
    public static final class ofByte extends WrappedArray<Byte> implements ScalaObject, ScalaObject {
        public final byte[] array;

        public ofByte(byte[] array2) {
            this.array = array2;
        }

        public /* bridge */ /* synthetic */ Object apply(Object v1) {
            return BoxesRunTime.boxToByte(apply(BoxesRunTime.unboxToInt(v1)));
        }

        public byte[] array() {
            return this.array;
        }

        public /* bridge */ /* synthetic */ void update(int index, Object elem) {
            update(index, BoxesRunTime.unboxToByte(elem));
        }

        public AnyValManifest<Byte> elemManifest() {
            return ClassManifest$.MODULE$.Byte();
        }

        public int length() {
            return this.array.length;
        }

        public byte apply(int index) {
            return this.array[index];
        }

        public void update(int index, byte elem) {
            this.array[index] = elem;
        }
    }

    /* compiled from: WrappedArray.scala */
    public static final class ofShort extends WrappedArray<Short> implements ScalaObject, ScalaObject {
        public final short[] array;

        public ofShort(short[] array2) {
            this.array = array2;
        }

        public /* bridge */ /* synthetic */ Object apply(Object v1) {
            return BoxesRunTime.boxToShort(apply(BoxesRunTime.unboxToInt(v1)));
        }

        public short[] array() {
            return this.array;
        }

        public /* bridge */ /* synthetic */ void update(int index, Object elem) {
            update(index, BoxesRunTime.unboxToShort(elem));
        }

        public AnyValManifest<Short> elemManifest() {
            return ClassManifest$.MODULE$.Short();
        }

        public int length() {
            return this.array.length;
        }

        public short apply(int index) {
            return this.array[index];
        }

        public void update(int index, short elem) {
            this.array[index] = elem;
        }
    }

    /* compiled from: WrappedArray.scala */
    public static final class ofChar extends WrappedArray<Character> implements ScalaObject, ScalaObject {
        public final char[] array;

        public ofChar(char[] array2) {
            this.array = array2;
        }

        public /* bridge */ /* synthetic */ Object apply(Object v1) {
            return BoxesRunTime.boxToCharacter(apply(BoxesRunTime.unboxToInt(v1)));
        }

        public char[] array() {
            return this.array;
        }

        public /* bridge */ /* synthetic */ void update(int index, Object elem) {
            update(index, BoxesRunTime.unboxToChar(elem));
        }

        public AnyValManifest<Character> elemManifest() {
            return ClassManifest$.MODULE$.Char();
        }

        public int length() {
            return this.array.length;
        }

        public char apply(int index) {
            return this.array[index];
        }

        public void update(int index, char elem) {
            this.array[index] = elem;
        }
    }

    /* compiled from: WrappedArray.scala */
    public static final class ofInt extends WrappedArray<Integer> implements ScalaObject, ScalaObject {
        public final int[] array;

        public ofInt(int[] array2) {
            this.array = array2;
        }

        public /* bridge */ /* synthetic */ Object apply(Object v1) {
            return BoxesRunTime.boxToInteger(apply(BoxesRunTime.unboxToInt(v1)));
        }

        public int[] array() {
            return this.array;
        }

        public /* bridge */ /* synthetic */ void update(int index, Object elem) {
            update(index, BoxesRunTime.unboxToInt(elem));
        }

        public AnyValManifest<Integer> elemManifest() {
            return ClassManifest$.MODULE$.Int();
        }

        public int length() {
            return this.array.length;
        }

        public int apply(int index) {
            return this.array[index];
        }

        public int apply$mcII$sp(int v1) {
            return this.array[v1];
        }

        public void update(int index, int elem) {
            this.array[index] = elem;
        }
    }

    /* compiled from: WrappedArray.scala */
    public static final class ofLong extends WrappedArray<Long> implements ScalaObject, ScalaObject {
        public final long[] array;

        public ofLong(long[] array2) {
            this.array = array2;
        }

        public /* bridge */ /* synthetic */ Object apply(Object v1) {
            return BoxesRunTime.boxToLong(apply(BoxesRunTime.unboxToInt(v1)));
        }

        public long[] array() {
            return this.array;
        }

        public /* bridge */ /* synthetic */ void update(int index, Object elem) {
            update(index, BoxesRunTime.unboxToLong(elem));
        }

        public AnyValManifest<Long> elemManifest() {
            return ClassManifest$.MODULE$.Long();
        }

        public int length() {
            return this.array.length;
        }

        public long apply(int index) {
            return this.array[index];
        }

        public void update(int index, long elem) {
            this.array[index] = elem;
        }
    }

    /* compiled from: WrappedArray.scala */
    public static final class ofFloat extends WrappedArray<Float> implements ScalaObject, ScalaObject {
        public final float[] array;

        public ofFloat(float[] array2) {
            this.array = array2;
        }

        public /* bridge */ /* synthetic */ Object apply(Object v1) {
            return BoxesRunTime.boxToFloat(apply(BoxesRunTime.unboxToInt(v1)));
        }

        public float[] array() {
            return this.array;
        }

        public /* bridge */ /* synthetic */ void update(int index, Object elem) {
            update(index, BoxesRunTime.unboxToFloat(elem));
        }

        public AnyValManifest<Float> elemManifest() {
            return ClassManifest$.MODULE$.Float();
        }

        public int length() {
            return this.array.length;
        }

        public float apply(int index) {
            return this.array[index];
        }

        public void update(int index, float elem) {
            this.array[index] = elem;
        }
    }

    /* compiled from: WrappedArray.scala */
    public static final class ofDouble extends WrappedArray<Double> implements ScalaObject, ScalaObject {
        public final double[] array;

        public ofDouble(double[] array2) {
            this.array = array2;
        }

        public /* bridge */ /* synthetic */ Object apply(Object v1) {
            return BoxesRunTime.boxToDouble(apply(BoxesRunTime.unboxToInt(v1)));
        }

        public double[] array() {
            return this.array;
        }

        public /* bridge */ /* synthetic */ void update(int index, Object elem) {
            update(index, BoxesRunTime.unboxToDouble(elem));
        }

        public AnyValManifest<Double> elemManifest() {
            return ClassManifest$.MODULE$.Double();
        }

        public int length() {
            return this.array.length;
        }

        public double apply(int index) {
            return this.array[index];
        }

        public void update(int index, double elem) {
            this.array[index] = elem;
        }
    }

    /* compiled from: WrappedArray.scala */
    public static final class ofBoolean extends WrappedArray<Boolean> implements ScalaObject, ScalaObject {
        public final boolean[] array;

        public ofBoolean(boolean[] array2) {
            this.array = array2;
        }

        public /* bridge */ /* synthetic */ Object apply(Object v1) {
            return BoxesRunTime.boxToBoolean(apply(BoxesRunTime.unboxToInt(v1)));
        }

        public boolean[] array() {
            return this.array;
        }

        public /* bridge */ /* synthetic */ void update(int index, Object elem) {
            update(index, BoxesRunTime.unboxToBoolean(elem));
        }

        public AnyValManifest<Boolean> elemManifest() {
            return ClassManifest$.MODULE$.Boolean();
        }

        public int length() {
            return this.array.length;
        }

        public boolean apply(int index) {
            return this.array[index];
        }

        public void update(int index, boolean elem) {
            this.array[index] = elem;
        }
    }

    /* compiled from: WrappedArray.scala */
    public static final class ofUnit extends WrappedArray<Object> implements ScalaObject, ScalaObject {
        public final BoxedUnit[] array;

        public ofUnit(BoxedUnit[] array2) {
            this.array = array2;
        }

        public /* bridge */ /* synthetic */ Object apply(Object v1) {
            apply(BoxesRunTime.unboxToInt(v1));
            return BoxedUnit.UNIT;
        }

        public BoxedUnit[] array() {
            return this.array;
        }

        public AnyValManifest<Object> elemManifest() {
            return ClassManifest$.MODULE$.Unit();
        }

        public int length() {
            return this.array.length;
        }

        public void apply(int index) {
        }

        public void apply$mcVI$sp(int v1) {
        }

        public void update(int index, BoxedUnit elem) {
            this.array[index] = elem;
        }
    }
}
