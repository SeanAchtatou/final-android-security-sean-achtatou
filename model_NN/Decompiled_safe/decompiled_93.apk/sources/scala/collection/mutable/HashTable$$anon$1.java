package scala.collection.mutable;

import scala.Function1;
import scala.Function2;
import scala.collection.Iterator;
import scala.collection.TraversableOnce;
import scala.collection.immutable.List;
import scala.collection.immutable.Set;
import scala.collection.immutable.Stream;
import scala.math.Numeric;
import scala.reflect.ClassManifest;

/* compiled from: HashTable.scala */
public final class HashTable$$anon$1 implements Iterator<HashEntry> {
    private HashEntry es = iterTable()[idx()];
    private int idx;
    private final HashEntry<A, HashEntry>[] iterTable;

    public HashTable$$anon$1(HashTable<A> $outer) {
        TraversableOnce.Cclass.$init$(this);
        Iterator.Cclass.$init$(this);
        this.iterTable = $outer.table();
        this.idx = $outer.table().length - 1;
        scan();
    }

    public <B> B $div$colon(B z, Function2<B, HashEntry, B> op) {
        return TraversableOnce.Cclass.$div$colon(this, z, op);
    }

    public StringBuilder addString(StringBuilder b, String start, String sep, String end) {
        return TraversableOnce.Cclass.addString(this, b, start, sep, end);
    }

    public <B> void copyToArray(Object xs, int start) {
        TraversableOnce.Cclass.copyToArray(this, xs, start);
    }

    public <B> void copyToArray(Object xs, int start, int len) {
        Iterator.Cclass.copyToArray(this, xs, start, len);
    }

    public Iterator<HashEntry> drop(int n) {
        return Iterator.Cclass.drop(this, n);
    }

    public boolean exists(Function1<HashEntry, Boolean> p) {
        return Iterator.Cclass.exists(this, p);
    }

    public <B> B foldLeft(B z, Function2<B, HashEntry, B> op) {
        return TraversableOnce.Cclass.foldLeft(this, z, op);
    }

    public boolean forall(Function1<HashEntry, Boolean> p) {
        return Iterator.Cclass.forall(this, p);
    }

    public <U> void foreach(Function1<HashEntry, U> f) {
        Iterator.Cclass.foreach(this, f);
    }

    public boolean isEmpty() {
        return Iterator.Cclass.isEmpty(this);
    }

    public boolean isTraversableAgain() {
        return Iterator.Cclass.isTraversableAgain(this);
    }

    public int length() {
        return Iterator.Cclass.length(this);
    }

    public <B> Iterator<B> map(Function1<HashEntry, B> f) {
        return Iterator.Cclass.map(this, f);
    }

    public String mkString() {
        return TraversableOnce.Cclass.mkString(this);
    }

    public String mkString(String sep) {
        return TraversableOnce.Cclass.mkString(this, sep);
    }

    public String mkString(String start, String sep, String end) {
        return TraversableOnce.Cclass.mkString(this, start, sep, end);
    }

    public boolean nonEmpty() {
        return TraversableOnce.Cclass.nonEmpty(this);
    }

    public <B> B reduceLeft(Function2<B, HashEntry, B> op) {
        return TraversableOnce.Cclass.reduceLeft(this, op);
    }

    public int size() {
        return TraversableOnce.Cclass.size(this);
    }

    public <B> B sum(Numeric<B> num) {
        return TraversableOnce.Cclass.sum(this, num);
    }

    public <B> Object toArray(ClassManifest<B> evidence$1) {
        return TraversableOnce.Cclass.toArray(this, evidence$1);
    }

    public <B> Buffer<B> toBuffer() {
        return TraversableOnce.Cclass.toBuffer(this);
    }

    public List<HashEntry> toList() {
        return TraversableOnce.Cclass.toList(this);
    }

    public <B> Set<B> toSet() {
        return TraversableOnce.Cclass.toSet(this);
    }

    public Stream<HashEntry> toStream() {
        return Iterator.Cclass.toStream(this);
    }

    public String toString() {
        return Iterator.Cclass.toString(this);
    }

    private HashEntry<A, HashEntry>[] iterTable() {
        return this.iterTable;
    }

    private int idx() {
        return this.idx;
    }

    private void idx_$eq(int i) {
        this.idx = i;
    }

    private HashEntry es() {
        return this.es;
    }

    private void es_$eq(HashEntry hashEntry) {
        this.es = hashEntry;
    }

    public boolean hasNext() {
        return es() != null;
    }

    public HashEntry next() {
        HashEntry res = es();
        es_$eq((HashEntry) es().next());
        scan();
        return res;
    }

    private void scan() {
        while (es() == null && idx() > 0) {
            idx_$eq(idx() - 1);
            es_$eq(iterTable()[idx()]);
        }
    }
}
