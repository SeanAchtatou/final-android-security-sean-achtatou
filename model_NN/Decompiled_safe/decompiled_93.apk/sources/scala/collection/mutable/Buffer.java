package scala.collection.mutable;

import scala.ScalaObject;
import scala.collection.generic.GenericTraversableTemplate;

/* compiled from: Buffer.scala */
public interface Buffer<A> extends Seq<A>, GenericTraversableTemplate<A, Buffer>, BufferLike<A, Buffer<A>>, ScalaObject, Seq {

    /* renamed from: scala.collection.mutable.Buffer$class  reason: invalid class name */
    /* compiled from: Buffer.scala */
    public abstract class Cclass {
        public static void $init$(Buffer $this) {
        }
    }
}
