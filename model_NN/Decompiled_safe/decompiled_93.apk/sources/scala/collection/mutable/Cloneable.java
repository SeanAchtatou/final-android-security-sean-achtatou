package scala.collection.mutable;

import scala.ScalaObject;

/* compiled from: Cloneable.scala */
public interface Cloneable<A> extends ScalaObject, ScalaObject {
    Object scala$collection$mutable$Cloneable$$super$clone();

    /* renamed from: scala.collection.mutable.Cloneable$class  reason: invalid class name */
    /* compiled from: Cloneable.scala */
    public abstract class Cclass {
        public static void $init$(Cloneable $this) {
        }

        public static Object clone(Cloneable $this) {
            return $this.scala$collection$mutable$Cloneable$$super$clone();
        }
    }
}
