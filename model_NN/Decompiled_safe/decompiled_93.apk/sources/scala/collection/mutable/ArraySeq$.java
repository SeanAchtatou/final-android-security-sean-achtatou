package scala.collection.mutable;

import scala.ScalaObject;
import scala.collection.generic.SeqFactory;

/* compiled from: ArraySeq.scala */
public final class ArraySeq$ extends SeqFactory<ArraySeq> implements ScalaObject {
    public static final ArraySeq$ MODULE$ = null;

    static {
        new ArraySeq$();
    }

    private ArraySeq$() {
        MODULE$ = this;
    }

    public <A> Builder<A, ArraySeq<A>> newBuilder() {
        return new ArrayBuffer().mapResult(new ArraySeq$$anonfun$newBuilder$1());
    }
}
