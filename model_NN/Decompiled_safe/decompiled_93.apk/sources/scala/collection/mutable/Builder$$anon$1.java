package scala.collection.mutable;

import $anon;
import scala.Function1;
import scala.Proxy;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.mutable.Builder;

/* compiled from: Builder.scala */
public final class Builder$$anon$1 implements Builder<Elem, Object>, Proxy {
    private final /* synthetic */ Function1 f$1;
    private final Builder<Elem, To> self;

    /* JADX WARN: Type inference failed for: r2v0, types: [scala.Function1, scala.collection.mutable.Builder<Elem, To>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Builder$$anon$1(scala.collection.mutable.Builder r1, scala.collection.mutable.Builder<Elem, To> r2) {
        /*
            r0 = this;
            r0.f$1 = r2
            r0.<init>()
            scala.collection.generic.Growable.Cclass.$init$(r0)
            scala.collection.mutable.Builder.Cclass.$init$(r0)
            scala.Proxy.Cclass.$init$(r0)
            r0.self = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.collection.mutable.Builder$$anon$1.<init>(scala.collection.mutable.Builder, scala.Function1):void");
    }

    public boolean equals(Object that) {
        return Proxy.Cclass.equals(this, that);
    }

    public int hashCode() {
        return Proxy.Cclass.hashCode(this);
    }

    public <NewTo> Builder<Elem, NewTo> mapResult(Function1<NewTo, NewTo> f) {
        return Builder.Cclass.mapResult(this, f);
    }

    public void sizeHint(int size) {
        Builder.Cclass.sizeHint(this, size);
    }

    public void sizeHint(TraversableLike<?, ?> coll, int delta) {
        Builder.Cclass.sizeHint(this, coll, delta);
    }

    public /* synthetic */ int sizeHint$default$2() {
        return Builder.Cclass.sizeHint$default$2(this);
    }

    public void sizeHintBounded(int size, TraversableLike<?, ?> boundingColl) {
        Builder.Cclass.sizeHintBounded(this, size, boundingColl);
    }

    public String toString() {
        return Proxy.Cclass.toString(this);
    }

    public Builder<Elem, To> self() {
        return this.self;
    }

    public Builder<Elem, To>.$anon.1 $plus$eq(Elem x) {
        this.self.$plus$eq((Object) x);
        return this;
    }

    public Builder<Elem, To>.$anon.1 $plus$plus$eq(TraversableOnce<Elem> xs) {
        this.self.$plus$plus$eq(xs);
        return this;
    }

    public NewTo result() {
        return this.f$1.apply(this.self.result());
    }
}
