package scala.collection.mutable;

import scala.ScalaObject;
import scala.collection.SeqLike;
import scala.collection.generic.Growable;
import scala.collection.generic.Shrinkable;
import scala.collection.generic.Subtractable;

public interface BufferLike extends Cloneable, ScalaObject, SeqLike, Growable, Shrinkable, Subtractable, Cloneable {

    /* renamed from: scala.collection.mutable.BufferLike$class  reason: invalid class name */
    /* compiled from: BufferLike.scala */
    public abstract class Cclass {
        public static void $init$(Buffer $this) {
        }
    }
}
