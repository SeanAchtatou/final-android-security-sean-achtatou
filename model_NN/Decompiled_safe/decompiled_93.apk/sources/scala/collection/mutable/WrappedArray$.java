package scala.collection.mutable;

import scala.MatchError;
import scala.Predef$;
import scala.ScalaObject;
import scala.runtime.BoxedUnit;

/* compiled from: WrappedArray.scala */
public final class WrappedArray$ implements ScalaObject {
    public static final WrappedArray$ MODULE$ = null;

    static {
        new WrappedArray$();
    }

    private WrappedArray$() {
        MODULE$ = this;
    }

    public <T> WrappedArray<T> make(Object x) {
        if (x instanceof Object[]) {
            return Predef$.MODULE$.wrapRefArray((Object[]) x);
        }
        if (x instanceof int[]) {
            return Predef$.MODULE$.wrapIntArray((int[]) x);
        }
        if (x instanceof double[]) {
            return Predef$.MODULE$.wrapDoubleArray((double[]) x);
        }
        if (x instanceof long[]) {
            return Predef$.MODULE$.wrapLongArray((long[]) x);
        }
        if (x instanceof float[]) {
            return Predef$.MODULE$.wrapFloatArray((float[]) x);
        }
        if (x instanceof char[]) {
            return Predef$.MODULE$.wrapCharArray((char[]) x);
        }
        if (x instanceof byte[]) {
            return Predef$.MODULE$.wrapByteArray((byte[]) x);
        }
        if (x instanceof short[]) {
            return Predef$.MODULE$.wrapShortArray((short[]) x);
        }
        if (x instanceof boolean[]) {
            return Predef$.MODULE$.wrapBooleanArray((boolean[]) x);
        }
        if (x instanceof BoxedUnit[]) {
            return Predef$.MODULE$.wrapUnitArray((BoxedUnit[]) x);
        }
        throw new MatchError(x);
    }
}
