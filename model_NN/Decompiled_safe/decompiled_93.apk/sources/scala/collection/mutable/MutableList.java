package scala.collection.mutable;

import java.util.NoSuchElementException;
import scala.Function1;
import scala.Function2;
import scala.PartialFunction;
import scala.Predef$;
import scala.ScalaObject;
import scala.Tuple2;
import scala.collection.Iterable;
import scala.collection.IterableLike;
import scala.collection.Iterator;
import scala.collection.LinearSeq;
import scala.collection.LinearSeqLike;
import scala.collection.LinearSeqOptimized;
import scala.collection.Seq;
import scala.collection.SeqLike;
import scala.collection.Traversable;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericTraversableTemplate;
import scala.collection.generic.Growable;
import scala.collection.immutable.List;
import scala.collection.immutable.Set;
import scala.collection.immutable.Stream;
import scala.collection.mutable.Builder;
import scala.collection.mutable.Iterable;
import scala.collection.mutable.LinearSeq;
import scala.collection.mutable.Seq;
import scala.collection.mutable.Traversable;
import scala.math.Numeric;
import scala.math.Ordering;
import scala.reflect.ClassManifest;
import scala.runtime.BoxesRunTime;

/* compiled from: MutableList.scala */
public class MutableList<A> implements LinearSeq<A>, LinearSeqOptimized<A, MutableList<A>>, Builder<A, MutableList<A>>, ScalaObject, LinearSeq {
    public static final long serialVersionUID = 5938451523372603072L;
    private LinkedList<A> first0 = new LinkedList<>();
    private LinkedList<A> last0;
    private int len = 0;

    public MutableList() {
        TraversableOnce.Cclass.$init$(this);
        TraversableLike.Cclass.$init$(this);
        GenericTraversableTemplate.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        Traversable.Cclass.$init$(this);
        IterableLike.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Iterable.Cclass.$init$(this);
        Function1.Cclass.$init$(this);
        PartialFunction.Cclass.$init$(this);
        SeqLike.Cclass.$init$(this);
        Seq.Cclass.$init$(this);
        Seq.Cclass.$init$(this);
        LinearSeqLike.Cclass.$init$(this);
        LinearSeq.Cclass.$init$(this);
        LinearSeq.Cclass.$init$(this);
        LinearSeqOptimized.Cclass.$init$(this);
        Growable.Cclass.$init$(this);
        Builder.Cclass.$init$(this);
    }

    public <B> B $div$colon(B z, Function2<B, A, B> op) {
        return TraversableOnce.Cclass.$div$colon(this, z, op);
    }

    public <B, That> That $plus$plus(TraversableOnce<B> that, CanBuildFrom<MutableList<A>, B, That> bf) {
        return TraversableLike.Cclass.$plus$plus(this, that, bf);
    }

    public Growable<A> $plus$plus$eq(TraversableOnce<A> xs) {
        return Growable.Cclass.$plus$plus$eq(this, xs);
    }

    public StringBuilder addString(StringBuilder b, String start, String sep, String end) {
        return TraversableOnce.Cclass.addString(this, b, start, sep, end);
    }

    public <C> PartialFunction<Integer, C> andThen(Function1<A, C> k) {
        return PartialFunction.Cclass.andThen(this, k);
    }

    public /* bridge */ /* synthetic */ Object apply(Object v1) {
        return apply(BoxesRunTime.unboxToInt(v1));
    }

    public int apply$mcII$sp(int v1) {
        return Function1.Cclass.apply$mcII$sp(this, v1);
    }

    public void apply$mcVI$sp(int v1) {
        Function1.Cclass.apply$mcVI$sp(this, v1);
    }

    public void apply$mcVL$sp(long v1) {
        Function1.Cclass.apply$mcVL$sp(this, v1);
    }

    public boolean canEqual(Object that) {
        return IterableLike.Cclass.canEqual(this, that);
    }

    public GenericCompanion<LinearSeq> companion() {
        return LinearSeq.Cclass.companion(this);
    }

    public <A> Function1<A, A> compose(Function1<A, Integer> g) {
        return Function1.Cclass.compose(this, g);
    }

    public boolean contains(Object elem) {
        return SeqLike.Cclass.contains(this, elem);
    }

    public <B> void copyToArray(Object xs, int start) {
        TraversableOnce.Cclass.copyToArray(this, xs, start);
    }

    public <B> void copyToArray(Object xs, int start, int len2) {
        IterableLike.Cclass.copyToArray(this, xs, start, len2);
    }

    public int count(Function1<A, Boolean> p) {
        return LinearSeqOptimized.Cclass.count(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.LinearSeqOptimized, scala.collection.mutable.MutableList<A>] */
    public MutableList<A> drop(int n) {
        return LinearSeqOptimized.Cclass.drop(this, n);
    }

    public boolean equals(Object that) {
        return SeqLike.Cclass.equals(this, that);
    }

    public boolean exists(Function1<A, Boolean> p) {
        return LinearSeqOptimized.Cclass.exists(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.MutableList<A>, java.lang.Object] */
    public MutableList<A> filter(Function1<A, Boolean> p) {
        return TraversableLike.Cclass.filter(this, p);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.MutableList<A>, java.lang.Object] */
    public MutableList<A> filterNot(Function1<A, Boolean> p) {
        return TraversableLike.Cclass.filterNot(this, p);
    }

    public <B> B foldLeft(B z, Function2<B, A, B> f) {
        return LinearSeqOptimized.Cclass.foldLeft(this, z, f);
    }

    public boolean forall(Function1<A, Boolean> p) {
        return LinearSeqOptimized.Cclass.forall(this, p);
    }

    public <B> void foreach(Function1<A, B> f) {
        LinearSeqOptimized.Cclass.foreach(this, f);
    }

    public <B> Builder<B, LinearSeq<B>> genericBuilder() {
        return GenericTraversableTemplate.Cclass.genericBuilder(this);
    }

    public int hashCode() {
        return SeqLike.Cclass.hashCode(this);
    }

    public <B> int indexOf(B elem) {
        return SeqLike.Cclass.indexOf(this, elem);
    }

    public <B> int indexOf(B elem, int from) {
        return SeqLike.Cclass.indexOf(this, elem, from);
    }

    public int indexWhere(Function1<A, Boolean> p, int from) {
        return LinearSeqOptimized.Cclass.indexWhere(this, p, from);
    }

    public boolean isDefinedAt(int x) {
        return LinearSeqOptimized.Cclass.isDefinedAt(this, x);
    }

    public /* bridge */ /* synthetic */ boolean isDefinedAt(Object x) {
        return isDefinedAt(BoxesRunTime.unboxToInt(x));
    }

    public final boolean isTraversableAgain() {
        return TraversableLike.Cclass.isTraversableAgain(this);
    }

    public int lengthCompare(int len2) {
        return LinearSeqOptimized.Cclass.lengthCompare(this, len2);
    }

    public <B, That> That map(Function1<A, B> f, CanBuildFrom<MutableList<A>, B, That> bf) {
        return TraversableLike.Cclass.map(this, f, bf);
    }

    public <NewTo> Builder<A, NewTo> mapResult(Function1<MutableList<A>, NewTo> f) {
        return Builder.Cclass.mapResult(this, f);
    }

    public String mkString() {
        return TraversableOnce.Cclass.mkString(this);
    }

    public String mkString(String sep) {
        return TraversableOnce.Cclass.mkString(this, sep);
    }

    public String mkString(String start, String sep, String end) {
        return TraversableOnce.Cclass.mkString(this, start, sep, end);
    }

    public boolean nonEmpty() {
        return TraversableOnce.Cclass.nonEmpty(this);
    }

    public int prefixLength(Function1<A, Boolean> p) {
        return SeqLike.Cclass.prefixLength(this, p);
    }

    public <B> B reduceLeft(Function2<B, A, B> f) {
        return LinearSeqOptimized.Cclass.reduceLeft(this, f);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.MutableList<A>, java.lang.Object] */
    public MutableList<A> repr() {
        return TraversableLike.Cclass.repr(this);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.MutableList<A>, java.lang.Object] */
    public MutableList<A> reverse() {
        return SeqLike.Cclass.reverse(this);
    }

    public Iterator<A> reverseIterator() {
        return SeqLike.Cclass.reverseIterator(this);
    }

    public <B> boolean sameElements(scala.collection.Iterable<B> that) {
        return LinearSeqOptimized.Cclass.sameElements(this, that);
    }

    public final boolean scala$collection$LinearSeqOptimized$$super$sameElements(scala.collection.Iterable that) {
        return IterableLike.Cclass.sameElements(this, that);
    }

    public int segmentLength(Function1<A, Boolean> p, int from) {
        return LinearSeqOptimized.Cclass.segmentLength(this, p, from);
    }

    public int size() {
        return SeqLike.Cclass.size(this);
    }

    public void sizeHint(int size) {
        Builder.Cclass.sizeHint(this, size);
    }

    public void sizeHint(TraversableLike<?, ?> coll, int delta) {
        Builder.Cclass.sizeHint(this, coll, delta);
    }

    public /* synthetic */ int sizeHint$default$2() {
        return Builder.Cclass.sizeHint$default$2(this);
    }

    public void sizeHintBounded(int size, TraversableLike<?, ?> boundingColl) {
        Builder.Cclass.sizeHintBounded(this, size, boundingColl);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.LinearSeqOptimized, scala.collection.mutable.MutableList<A>] */
    public MutableList<A> slice(int from, int until) {
        return LinearSeqOptimized.Cclass.slice(this, from, until);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.MutableList<A>, java.lang.Object] */
    public <B> MutableList<A> sortBy(Function1<A, B> f, Ordering<B> ord) {
        return SeqLike.Cclass.sortBy(this, f, ord);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.mutable.MutableList<A>, java.lang.Object] */
    public <B> MutableList<A> sorted(Ordering<B> ord) {
        return SeqLike.Cclass.sorted(this, ord);
    }

    public String stringPrefix() {
        return TraversableLike.Cclass.stringPrefix(this);
    }

    public <B> B sum(Numeric<B> num) {
        return TraversableOnce.Cclass.sum(this, num);
    }

    public scala.collection.LinearSeq<A> thisCollection() {
        return LinearSeqLike.Cclass.thisCollection(this);
    }

    public <B> Object toArray(ClassManifest<B> evidence$1) {
        return TraversableOnce.Cclass.toArray(this, evidence$1);
    }

    public <B> Buffer<B> toBuffer() {
        return TraversableOnce.Cclass.toBuffer(this);
    }

    public scala.collection.LinearSeq<A> toCollection(MutableList<A> repr) {
        return LinearSeqLike.Cclass.toCollection(this, repr);
    }

    public /* bridge */ /* synthetic */ scala.collection.Seq toCollection(Object repr) {
        return toCollection((LinearSeqLike) repr);
    }

    public <B> Set<B> toSet() {
        return TraversableOnce.Cclass.toSet(this);
    }

    public Stream<A> toStream() {
        return IterableLike.Cclass.toStream(this);
    }

    public String toString() {
        return SeqLike.Cclass.toString(this);
    }

    public <A1, B, That> That zip(scala.collection.Iterable<B> that, CanBuildFrom<MutableList<A>, Tuple2<A1, B>, That> bf) {
        return IterableLike.Cclass.zip(this, that, bf);
    }

    public MutableList<A> newBuilder() {
        return new MutableList<>();
    }

    public LinkedList<A> first0() {
        return this.first0;
    }

    public void first0_$eq(LinkedList<A> linkedList) {
        this.first0 = linkedList;
    }

    public LinkedList<A> last0() {
        return this.last0;
    }

    public void last0_$eq(LinkedList<A> linkedList) {
        this.last0 = linkedList;
    }

    public int len() {
        return this.len;
    }

    public void len_$eq(int i) {
        this.len = i;
    }

    public boolean isEmpty() {
        return len() == 0;
    }

    public A head() {
        return first0().head();
    }

    public MutableList<A> tail() {
        Predef$.MODULE$.require(nonEmpty(), new MutableList$$anonfun$tail$1(this));
        MutableList tl = new MutableList();
        tl.first0_$eq((LinkedList) first0().tail());
        tl.last0_$eq(last0());
        tl.len_$eq(len() - 1);
        return tl;
    }

    public int length() {
        return len();
    }

    public A apply(int n) {
        return first0().apply(n);
    }

    public void prependElem(A elem) {
        first0_$eq(new LinkedList(elem, first0()));
        if (len() == 0) {
            last0_$eq(first0());
        }
        len_$eq(len() + 1);
    }

    public void appendElem(A elem) {
        if (len() == 0) {
            prependElem(elem);
            return;
        }
        last0().next_$eq(new LinkedList());
        last0_$eq((LinkedList) last0().next());
        last0().elem_$eq(elem);
        last0().next_$eq(new LinkedList());
        len_$eq(len() + 1);
    }

    public Iterator<A> iterator() {
        return first0().iterator();
    }

    public A last() {
        if (!isEmpty()) {
            return last0().elem();
        }
        throw new NoSuchElementException("MutableList.empty.last");
    }

    public List<A> toList() {
        return first0().toList();
    }

    public LinkedList<A> toLinkedList() {
        return first0();
    }

    public MutableList<A> $plus$eq(A elem) {
        appendElem(elem);
        return this;
    }

    public void clear() {
        first0_$eq(new LinkedList());
        last0_$eq(first0());
        len_$eq(0);
    }

    public MutableList<A> result() {
        return this;
    }
}
