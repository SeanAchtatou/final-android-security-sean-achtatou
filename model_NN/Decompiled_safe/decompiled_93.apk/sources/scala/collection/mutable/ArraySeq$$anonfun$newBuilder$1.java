package scala.collection.mutable;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;

/* compiled from: ArraySeq.scala */
public final class ArraySeq$$anonfun$newBuilder$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return apply((ArrayBuffer) ((ArrayBuffer) v1));
    }

    public final ArraySeq<A> apply(ArrayBuffer<A> buf) {
        ArraySeq result = new ArraySeq(buf.length());
        buf.copyToArray(result.array(), 0);
        return result;
    }
}
