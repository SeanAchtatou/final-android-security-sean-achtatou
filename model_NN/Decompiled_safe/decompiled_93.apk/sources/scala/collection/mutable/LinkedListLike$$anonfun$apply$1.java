package scala.collection.mutable;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;

/* compiled from: LinkedListLike.scala */
public final class LinkedListLike$$anonfun$apply$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;

    public LinkedListLike$$anonfun$apply$1(LinkedListLike<A, This> $outer) {
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return apply((Seq) ((Seq) v1));
    }

    public final A apply(This thisR) {
        return ((LinkedListLike) thisR).elem();
    }
}
