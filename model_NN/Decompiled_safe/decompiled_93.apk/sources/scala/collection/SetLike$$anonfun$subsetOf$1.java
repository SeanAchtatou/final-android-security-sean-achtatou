package scala.collection;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;

/* compiled from: SetLike.scala */
public final class SetLike$$anonfun$subsetOf$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Set that$1;

    /* JADX WARN: Type inference failed for: r2v0, types: [scala.collection.SetLike<A, This>, scala.collection.Set] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SetLike$$anonfun$subsetOf$1(scala.collection.SetLike r1, scala.collection.SetLike<A, This> r2) {
        /*
            r0 = this;
            r0.that$1 = r2
            r0.<init>()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.collection.SetLike$$anonfun$subsetOf$1.<init>(scala.collection.SetLike, scala.collection.Set):void");
    }

    public final boolean apply(A a) {
        return this.that$1.contains(a);
    }
}
