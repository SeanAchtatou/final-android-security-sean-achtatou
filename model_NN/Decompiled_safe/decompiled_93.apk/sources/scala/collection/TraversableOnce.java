package scala.collection;

import scala.Function1;
import scala.Function2;
import scala.Predef$;
import scala.ScalaObject;
import scala.collection.generic.Addable;
import scala.collection.generic.Growable;
import scala.collection.immutable.List;
import scala.collection.immutable.Set;
import scala.collection.immutable.Set$;
import scala.collection.immutable.Stream;
import scala.collection.mutable.ArrayBuffer;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.ListBuffer;
import scala.collection.mutable.StringBuilder;
import scala.math.Numeric;
import scala.reflect.ClassManifest;
import scala.runtime.BooleanRef;
import scala.runtime.BoxesRunTime;
import scala.runtime.IntRef;
import scala.runtime.ObjectRef;
import scala.runtime.ScalaRunTime$;

/* compiled from: TraversableOnce.scala */
public interface TraversableOnce<A> extends ScalaObject {
    <B> B $div$colon(B b, Function2<B, A, B> function2);

    StringBuilder addString(StringBuilder stringBuilder, String str, String str2, String str3);

    <B> void copyToArray(Object obj, int i);

    <B> void copyToArray(Object obj, int i, int i2);

    <B> B foldLeft(B b, Function2<B, A, B> function2);

    <U> void foreach(Function1<A, U> function1);

    boolean isEmpty();

    boolean isTraversableAgain();

    String mkString();

    String mkString(String str);

    String mkString(String str, String str2, String str3);

    boolean nonEmpty();

    <B> B reduceLeft(Function2<B, A, B> function2);

    int size();

    <B> B sum(Numeric<B> numeric);

    <B> Object toArray(ClassManifest<B> classManifest);

    <B> Buffer<B> toBuffer();

    List<A> toList();

    <B> Set<B> toSet();

    Stream<A> toStream();

    /* renamed from: scala.collection.TraversableOnce$class  reason: invalid class name */
    /* compiled from: TraversableOnce.scala */
    public abstract class Cclass {
        public static void $init$(TraversableOnce $this) {
        }

        public static int size(TraversableOnce $this) {
            IntRef result$1 = new IntRef(0);
            $this.foreach(new TraversableOnce$$anonfun$size$1($this, result$1));
            return result$1.elem;
        }

        public static boolean nonEmpty(TraversableOnce $this) {
            return !$this.isEmpty();
        }

        public static Object $div$colon(TraversableOnce $this, Object z, Function2 op) {
            return $this.foldLeft(z, op);
        }

        public static Object foldLeft(TraversableOnce $this, Object z, Function2 op$1) {
            ObjectRef result$2 = new ObjectRef(z);
            $this.foreach(new TraversableOnce$$anonfun$foldLeft$1($this, op$1, result$2));
            return result$2.elem;
        }

        public static Object reduceLeft(TraversableOnce $this, Function2 op$3) {
            if ($this.isEmpty()) {
                throw new UnsupportedOperationException("empty.reduceLeft");
            }
            BooleanRef first$1 = new BooleanRef(true);
            ObjectRef acc$1 = new ObjectRef(BoxesRunTime.boxToInteger(0));
            $this.foreach(new TraversableOnce$$anonfun$reduceLeft$1($this, op$3, first$1, acc$1));
            return acc$1.elem;
        }

        public static Object sum(TraversableOnce $this, Numeric num$1) {
            return $this.foldLeft(num$1.zero(), new TraversableOnce$$anonfun$sum$1($this, num$1));
        }

        public static void copyToArray(TraversableOnce $this, Object xs, int start) {
            $this.copyToArray(xs, start, ScalaRunTime$.MODULE$.array_length(xs) - start);
        }

        public static Object toArray(TraversableOnce $this, ClassManifest evidence$1) {
            if (!$this.isTraversableAgain()) {
                return $this.toBuffer().toArray(evidence$1);
            }
            Object result = evidence$1.newArray($this.size());
            $this.copyToArray(result, 0);
            return result;
        }

        public static List toList(TraversableOnce $this) {
            return ((ListBuffer) Growable.Cclass.$plus$plus$eq(new ListBuffer(), $this)).toList();
        }

        public static Buffer toBuffer(TraversableOnce $this) {
            return new ArrayBuffer().$plus$plus$eq($this);
        }

        public static Set toSet(TraversableOnce $this) {
            return (Set) ((Addable) Set$.MODULE$.apply(Predef$.MODULE$.genericWrapArray(new Object[0]))).$plus$plus($this);
        }

        public static String mkString(TraversableOnce $this, String start, String sep, String end) {
            return $this.addString(new StringBuilder(), start, sep, end).toString();
        }

        public static String mkString(TraversableOnce traversableOnce, String str) {
            return traversableOnce.mkString("", str, "");
        }

        public static String mkString(TraversableOnce traversableOnce) {
            return traversableOnce.mkString("");
        }

        public static StringBuilder addString(TraversableOnce $this, StringBuilder b$1, String start, String sep$1, String end) {
            BooleanRef first$2 = new BooleanRef(true);
            b$1.append(start);
            $this.foreach(new TraversableOnce$$anonfun$addString$1($this, b$1, sep$1, first$2));
            b$1.append(end);
            return b$1;
        }
    }
}
