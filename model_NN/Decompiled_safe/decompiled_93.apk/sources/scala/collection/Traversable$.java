package scala.collection;

import scala.ScalaObject;
import scala.collection.generic.TraversableFactory;
import scala.collection.mutable.Builder;
import scala.collection.mutable.ListBuffer;
import scala.util.control.Breaks;

/* compiled from: Traversable.scala */
public final class Traversable$ extends TraversableFactory<Traversable> implements ScalaObject {
    public static final Traversable$ MODULE$ = null;
    private final Breaks breaks = new Breaks();

    static {
        new Traversable$();
    }

    private Traversable$() {
        MODULE$ = this;
    }

    public <A> Builder<A, Traversable<A>> newBuilder() {
        return new ListBuffer();
    }
}
