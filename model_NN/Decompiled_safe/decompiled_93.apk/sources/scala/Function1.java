package scala;

import scala.runtime.BoxesRunTime;

/* compiled from: Function1.scala */
public interface Function1<T1, R> extends ScalaObject {
    <A> Function1<T1, A> andThen(Function1<R, A> function1);

    R apply(Object obj);

    int apply$mcII$sp(int i);

    void apply$mcVI$sp(int i);

    void apply$mcVL$sp(long j);

    <A> Function1<A, R> compose(Function1<A, T1> function1);

    /* renamed from: scala.Function1$class  reason: invalid class name */
    /* compiled from: Function1.scala */
    public abstract class Cclass {
        public static void $init$(Function1 $this) {
        }

        public static double apply$mcDD$sp(Function1 $this, double v1) {
            return BoxesRunTime.unboxToDouble($this.apply(BoxesRunTime.boxToDouble(v1)));
        }

        public static double apply$mcDF$sp(Function1 $this, float v1) {
            return BoxesRunTime.unboxToDouble($this.apply(BoxesRunTime.boxToFloat(v1)));
        }

        public static double apply$mcDI$sp(Function1 $this, int v1) {
            return BoxesRunTime.unboxToDouble($this.apply(BoxesRunTime.boxToInteger(v1)));
        }

        public static double apply$mcDL$sp(Function1 $this, long v1) {
            return BoxesRunTime.unboxToDouble($this.apply(BoxesRunTime.boxToLong(v1)));
        }

        public static float apply$mcFD$sp(Function1 $this, double v1) {
            return BoxesRunTime.unboxToFloat($this.apply(BoxesRunTime.boxToDouble(v1)));
        }

        public static float apply$mcFF$sp(Function1 $this, float v1) {
            return BoxesRunTime.unboxToFloat($this.apply(BoxesRunTime.boxToFloat(v1)));
        }

        public static float apply$mcFI$sp(Function1 $this, int v1) {
            return BoxesRunTime.unboxToFloat($this.apply(BoxesRunTime.boxToInteger(v1)));
        }

        public static float apply$mcFL$sp(Function1 $this, long v1) {
            return BoxesRunTime.unboxToFloat($this.apply(BoxesRunTime.boxToLong(v1)));
        }

        public static int apply$mcID$sp(Function1 $this, double v1) {
            return BoxesRunTime.unboxToInt($this.apply(BoxesRunTime.boxToDouble(v1)));
        }

        public static int apply$mcIF$sp(Function1 $this, float v1) {
            return BoxesRunTime.unboxToInt($this.apply(BoxesRunTime.boxToFloat(v1)));
        }

        public static int apply$mcII$sp(Function1 $this, int v1) {
            return BoxesRunTime.unboxToInt($this.apply(BoxesRunTime.boxToInteger(v1)));
        }

        public static int apply$mcIL$sp(Function1 $this, long v1) {
            return BoxesRunTime.unboxToInt($this.apply(BoxesRunTime.boxToLong(v1)));
        }

        public static long apply$mcLD$sp(Function1 $this, double v1) {
            return BoxesRunTime.unboxToLong($this.apply(BoxesRunTime.boxToDouble(v1)));
        }

        public static long apply$mcLF$sp(Function1 $this, float v1) {
            return BoxesRunTime.unboxToLong($this.apply(BoxesRunTime.boxToFloat(v1)));
        }

        public static long apply$mcLI$sp(Function1 $this, int v1) {
            return BoxesRunTime.unboxToLong($this.apply(BoxesRunTime.boxToInteger(v1)));
        }

        public static long apply$mcLL$sp(Function1 $this, long v1) {
            return BoxesRunTime.unboxToLong($this.apply(BoxesRunTime.boxToLong(v1)));
        }

        public static void apply$mcVD$sp(Function1 $this, double v1) {
            $this.apply(BoxesRunTime.boxToDouble(v1));
        }

        public static void apply$mcVF$sp(Function1 $this, float v1) {
            $this.apply(BoxesRunTime.boxToFloat(v1));
        }

        public static void apply$mcVI$sp(Function1 $this, int v1) {
            $this.apply(BoxesRunTime.boxToInteger(v1));
        }

        public static void apply$mcVL$sp(Function1 $this, long v1) {
            $this.apply(BoxesRunTime.boxToLong(v1));
        }

        public static boolean apply$mcZD$sp(Function1 $this, double v1) {
            return BoxesRunTime.unboxToBoolean($this.apply(BoxesRunTime.boxToDouble(v1)));
        }

        public static boolean apply$mcZF$sp(Function1 $this, float v1) {
            return BoxesRunTime.unboxToBoolean($this.apply(BoxesRunTime.boxToFloat(v1)));
        }

        public static boolean apply$mcZI$sp(Function1 $this, int v1) {
            return BoxesRunTime.unboxToBoolean($this.apply(BoxesRunTime.boxToInteger(v1)));
        }

        public static boolean apply$mcZL$sp(Function1 $this, long v1) {
            return BoxesRunTime.unboxToBoolean($this.apply(BoxesRunTime.boxToLong(v1)));
        }

        public static String toString(Function1 $this) {
            return "<function1>";
        }

        public static Function1 compose(Function1 $this, Function1 g$1) {
            return new Function1$$anonfun$compose$1($this, g$1);
        }

        public static Function1 compose$mcDD$sp(Function1 $this, Function1 g) {
            return $this.compose(g);
        }

        public static Function1 compose$mcDF$sp(Function1 $this, Function1 g) {
            return $this.compose(g);
        }

        public static Function1 compose$mcDI$sp(Function1 $this, Function1 g) {
            return $this.compose(g);
        }

        public static Function1 compose$mcDL$sp(Function1 $this, Function1 g) {
            return $this.compose(g);
        }

        public static Function1 compose$mcFD$sp(Function1 $this, Function1 g) {
            return $this.compose(g);
        }

        public static Function1 compose$mcFF$sp(Function1 $this, Function1 g) {
            return $this.compose(g);
        }

        public static Function1 compose$mcFI$sp(Function1 $this, Function1 g) {
            return $this.compose(g);
        }

        public static Function1 compose$mcFL$sp(Function1 $this, Function1 g) {
            return $this.compose(g);
        }

        public static Function1 compose$mcID$sp(Function1 $this, Function1 g) {
            return $this.compose(g);
        }

        public static Function1 compose$mcIF$sp(Function1 $this, Function1 g) {
            return $this.compose(g);
        }

        public static Function1 compose$mcII$sp(Function1 $this, Function1 g) {
            return $this.compose(g);
        }

        public static Function1 compose$mcIL$sp(Function1 $this, Function1 g) {
            return $this.compose(g);
        }

        public static Function1 compose$mcLD$sp(Function1 $this, Function1 g) {
            return $this.compose(g);
        }

        public static Function1 compose$mcLF$sp(Function1 $this, Function1 g) {
            return $this.compose(g);
        }

        public static Function1 compose$mcLI$sp(Function1 $this, Function1 g) {
            return $this.compose(g);
        }

        public static Function1 compose$mcLL$sp(Function1 $this, Function1 g) {
            return $this.compose(g);
        }

        public static Function1 compose$mcVD$sp(Function1 $this, Function1 g) {
            return $this.compose(g);
        }

        public static Function1 compose$mcVF$sp(Function1 $this, Function1 g) {
            return $this.compose(g);
        }

        public static Function1 compose$mcVI$sp(Function1 $this, Function1 g) {
            return $this.compose(g);
        }

        public static Function1 compose$mcVL$sp(Function1 $this, Function1 g) {
            return $this.compose(g);
        }

        public static Function1 compose$mcZD$sp(Function1 $this, Function1 g) {
            return $this.compose(g);
        }

        public static Function1 compose$mcZF$sp(Function1 $this, Function1 g) {
            return $this.compose(g);
        }

        public static Function1 compose$mcZI$sp(Function1 $this, Function1 g) {
            return $this.compose(g);
        }

        public static Function1 compose$mcZL$sp(Function1 $this, Function1 g) {
            return $this.compose(g);
        }

        public static Function1 andThen(Function1 $this, Function1 g$2) {
            return new Function1$$anonfun$andThen$1($this, g$2);
        }

        public static Function1 andThen$mcDD$sp(Function1 $this, Function1 g) {
            return $this.andThen(g);
        }

        public static Function1 andThen$mcDF$sp(Function1 $this, Function1 g) {
            return $this.andThen(g);
        }

        public static Function1 andThen$mcDI$sp(Function1 $this, Function1 g) {
            return $this.andThen(g);
        }

        public static Function1 andThen$mcDL$sp(Function1 $this, Function1 g) {
            return $this.andThen(g);
        }

        public static Function1 andThen$mcFD$sp(Function1 $this, Function1 g) {
            return $this.andThen(g);
        }

        public static Function1 andThen$mcFF$sp(Function1 $this, Function1 g) {
            return $this.andThen(g);
        }

        public static Function1 andThen$mcFI$sp(Function1 $this, Function1 g) {
            return $this.andThen(g);
        }

        public static Function1 andThen$mcFL$sp(Function1 $this, Function1 g) {
            return $this.andThen(g);
        }

        public static Function1 andThen$mcID$sp(Function1 $this, Function1 g) {
            return $this.andThen(g);
        }

        public static Function1 andThen$mcIF$sp(Function1 $this, Function1 g) {
            return $this.andThen(g);
        }

        public static Function1 andThen$mcII$sp(Function1 $this, Function1 g) {
            return $this.andThen(g);
        }

        public static Function1 andThen$mcIL$sp(Function1 $this, Function1 g) {
            return $this.andThen(g);
        }

        public static Function1 andThen$mcLD$sp(Function1 $this, Function1 g) {
            return $this.andThen(g);
        }

        public static Function1 andThen$mcLF$sp(Function1 $this, Function1 g) {
            return $this.andThen(g);
        }

        public static Function1 andThen$mcLI$sp(Function1 $this, Function1 g) {
            return $this.andThen(g);
        }

        public static Function1 andThen$mcLL$sp(Function1 $this, Function1 g) {
            return $this.andThen(g);
        }

        public static Function1 andThen$mcVD$sp(Function1 $this, Function1 g) {
            return $this.andThen(g);
        }

        public static Function1 andThen$mcVF$sp(Function1 $this, Function1 g) {
            return $this.andThen(g);
        }

        public static Function1 andThen$mcVI$sp(Function1 $this, Function1 g) {
            return $this.andThen(g);
        }

        public static Function1 andThen$mcVL$sp(Function1 $this, Function1 g) {
            return $this.andThen(g);
        }

        public static Function1 andThen$mcZD$sp(Function1 $this, Function1 g) {
            return $this.andThen(g);
        }

        public static Function1 andThen$mcZF$sp(Function1 $this, Function1 g) {
            return $this.andThen(g);
        }

        public static Function1 andThen$mcZI$sp(Function1 $this, Function1 g) {
            return $this.andThen(g);
        }

        public static Function1 andThen$mcZL$sp(Function1 $this, Function1 g) {
            return $this.andThen(g);
        }
    }
}
