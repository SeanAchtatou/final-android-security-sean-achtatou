package scala;

/* compiled from: Predef.scala */
public final class Predef {

    /* compiled from: Predef.scala */
    public static final class ArrowAssoc<A> implements ScalaObject {
        public final A x;

        public ArrowAssoc(A x2) {
            this.x = x2;
        }

        public A x() {
            return this.x;
        }

        public <B> Tuple2<A, B> $minus$greater(B y) {
            return new Tuple2<>(x(), y);
        }
    }
}
