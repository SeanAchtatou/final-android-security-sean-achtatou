package scala;

/* compiled from: Function1.scala */
public interface Function1$mcII$sp extends Function1<Integer, Integer> {
    <A> Function1<Integer, A> andThen$mcII$sp(Function1<Integer, A> function1);

    int apply(int i);

    <A> Function1<A, Integer> compose$mcII$sp(Function1<A, Integer> function1);

    /* renamed from: scala.Function1$mcII$sp$class  reason: invalid class name */
    /* compiled from: Function1.scala */
    public abstract class Cclass {
        public static void $init$(Function1$mcII$sp $this) {
        }

        public static int apply(Function1$mcII$sp $this, int v1) {
            return $this.apply$mcII$sp(v1);
        }

        public static Function1 compose(Function1$mcII$sp $this, Function1 g) {
            return $this.compose$mcII$sp(g);
        }

        public static Function1 compose$mcII$sp(Function1$mcII$sp $this, Function1 g$7) {
            return new Function1$mcII$sp$$anonfun$compose$mcII$sp$1($this, g$7);
        }

        public static Function1 andThen(Function1$mcII$sp $this, Function1 g) {
            return $this.andThen$mcII$sp(g);
        }

        public static Function1 andThen$mcII$sp(Function1$mcII$sp $this, Function1 g$8) {
            return new Function1$mcII$sp$$anonfun$andThen$mcII$sp$1($this, g$8);
        }
    }
}
