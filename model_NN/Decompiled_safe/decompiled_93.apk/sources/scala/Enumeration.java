package scala;

import java.io.Serializable;
import java.util.NoSuchElementException;
import scala.Function1;
import scala.collection.Iterable;
import scala.collection.IterableLike;
import scala.collection.Iterator;
import scala.collection.Seq;
import scala.collection.Set;
import scala.collection.SetLike;
import scala.collection.Traversable;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.generic.Addable;
import scala.collection.generic.CanBuildFrom;
import scala.collection.generic.GenericCompanion;
import scala.collection.generic.GenericSetTemplate;
import scala.collection.generic.GenericTraversableTemplate;
import scala.collection.generic.Subtractable;
import scala.collection.immutable.BitSet;
import scala.collection.immutable.Iterable;
import scala.collection.immutable.List;
import scala.collection.immutable.Set;
import scala.collection.immutable.Stream;
import scala.collection.immutable.StringLike;
import scala.collection.immutable.StringOps;
import scala.collection.immutable.Traversable;
import scala.collection.mutable.ArrayOps;
import scala.collection.mutable.Buffer;
import scala.collection.mutable.Builder;
import scala.collection.mutable.HashMap;
import scala.collection.mutable.Map;
import scala.collection.mutable.StringBuilder;
import scala.math.Numeric;
import scala.math.Ordered;
import scala.reflect.ClassManifest;
import scala.runtime.BoxesRunTime;

/* compiled from: Enumeration.scala */
public abstract class Enumeration implements Serializable, ScalaObject {
    public static final long serialVersionUID = 8476000850333817230L;
    public /* synthetic */ Enumeration$ValueSet$ ValueSet$module;
    private int nextId;
    private Iterator<String> nextName;
    private final Map scala$Enumeration$$nmap;
    private int scala$Enumeration$$topId;
    private final Map scala$Enumeration$$vmap;
    private transient boolean scala$Enumeration$$vsetDefined;
    private transient ValueSet vset;

    public Enumeration(int initial, Seq<String> names) {
        this.scala$Enumeration$$vmap = new HashMap();
        this.vset = null;
        this.scala$Enumeration$$vsetDefined = false;
        this.scala$Enumeration$$nmap = new HashMap();
        this.nextId = initial;
        this.nextName = names.iterator();
        this.scala$Enumeration$$topId = initial;
    }

    public Enumeration() {
        this(0, Predef$.MODULE$.wrapRefArray((Object[]) new String[]{null}));
    }

    public String toString() {
        return (String) new ArrayOps.ofRef((Object[]) StringLike.Cclass.split(new StringOps((String) new ArrayOps.ofRef((Object[]) StringLike.Cclass.split(new StringOps(StringLike.Cclass.stripSuffix(new StringOps(getClass().getName()), "$")), '.')).last()), '$')).last();
    }

    public final Map scala$Enumeration$$vmap() {
        return this.scala$Enumeration$$vmap;
    }

    public final void scala$Enumeration$$vsetDefined_$eq(boolean z) {
        this.scala$Enumeration$$vsetDefined = z;
    }

    public final Map scala$Enumeration$$nmap() {
        return this.scala$Enumeration$$nmap;
    }

    public int nextId() {
        return this.nextId;
    }

    public void nextId_$eq(int i) {
        this.nextId = i;
    }

    public Iterator<String> nextName() {
        return this.nextName;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public final String scala$Enumeration$$nextNameOrElse(Function0 orElse) {
        return nextName().hasNext() ? nextName().next() : (String) orElse.apply();
    }

    public final int scala$Enumeration$$topId() {
        return this.scala$Enumeration$$topId;
    }

    public final void scala$Enumeration$$topId_$eq(int i) {
        this.scala$Enumeration$$topId = i;
    }

    public final Value apply(int x) {
        return (Value) this.scala$Enumeration$$vmap.apply(BoxesRunTime.boxToInteger(x));
    }

    public final Value Value() {
        return new Val(this, nextId(), nextName().hasNext() ? nextName().next() : null);
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    public final String scala$Enumeration$$nameOf(int i$2) {
        Object orElse;
        synchronized (this) {
            orElse = this.scala$Enumeration$$nmap.getOrElse(BoxesRunTime.boxToInteger(i$2), new Enumeration$$anonfun$scala$Enumeration$$nameOf$1(this, i$2));
        }
        return (String) orElse;
    }

    /* compiled from: Enumeration.scala */
    public abstract class Value implements Ordered<Value>, ScalaObject, Ordered {
        public static final long serialVersionUID = 7091335633555234129L;
        public final /* synthetic */ Enumeration $outer;
        private final Enumeration scala$Enumeration$$outerEnum;

        public abstract int id();

        public Value(Enumeration $outer2) {
            if ($outer2 == null) {
                throw new NullPointerException();
            }
            this.$outer = $outer2;
            Ordered.Cclass.$init$(this);
            this.scala$Enumeration$$outerEnum = $outer2;
        }

        public boolean $less(Object that) {
            return Ordered.Cclass.$less(this, that);
        }

        public int compareTo(Object that) {
            return Ordered.Cclass.compareTo(this, that);
        }

        public Enumeration scala$Enumeration$$outerEnum() {
            return this.scala$Enumeration$$outerEnum;
        }

        public int compare(Value that) {
            return id() - that.id();
        }

        public boolean equals(Object other) {
            if (!(other instanceof Value)) {
                return false;
            }
            Value value = (Value) other;
            if (scala$Enumeration$$outerEnum() == value.scala$Enumeration$$outerEnum() && id() == value.id()) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            return id();
        }
    }

    /* compiled from: Enumeration.scala */
    public class Val extends Value implements Serializable, ScalaObject {
        public static final long serialVersionUID = -3501153230598116017L;
        private final String name;
        public final int scala$Enumeration$Val$$i;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Val(Enumeration $outer, int i, String name2) {
            super($outer);
            this.scala$Enumeration$Val$$i = i;
            this.name = name2;
            Predef$.MODULE$.m1assert(!$outer.scala$Enumeration$$vmap().isDefinedAt(BoxesRunTime.boxToInteger(i)), new Enumeration$Val$$anonfun$2(this));
            $outer.scala$Enumeration$$vmap().update(BoxesRunTime.boxToInteger(i), this);
            $outer.scala$Enumeration$$vsetDefined_$eq(false);
            $outer.nextId_$eq(i + 1);
            if ($outer.nextId() > $outer.scala$Enumeration$$topId()) {
                $outer.scala$Enumeration$$topId_$eq($outer.nextId());
            }
        }

        public /* synthetic */ Enumeration scala$Enumeration$Val$$$outer() {
            return this.$outer;
        }

        public Val(Enumeration $outer, int i$1) {
            this($outer, i$1, $outer.scala$Enumeration$$nextNameOrElse(new Enumeration$Val$$anonfun$$init$$1($outer, i$1)));
        }

        public Val(Enumeration $outer) {
            this($outer, $outer.nextId());
        }

        public int id() {
            return this.scala$Enumeration$Val$$i;
        }

        public String toString() {
            String exceptionResult1;
            if (this.name != null) {
                return this.name;
            }
            try {
                exceptionResult1 = scala$Enumeration$Val$$$outer().scala$Enumeration$$nameOf(this.scala$Enumeration$Val$$i);
            } catch (NoSuchElementException e) {
                exceptionResult1 = new StringBuilder().append((Object) "<Invalid enum: no field for #").append(BoxesRunTime.boxToInteger(this.scala$Enumeration$Val$$i)).append((Object) ">").toString();
            }
            return exceptionResult1;
        }
    }

    /* compiled from: Enumeration.scala */
    public class ValueSet implements Set<Value>, SetLike<Value, ValueSet>, ScalaObject {
        public final /* synthetic */ Enumeration $outer;
        private final BitSet ids;

        public ValueSet(Enumeration $outer2, BitSet ids2) {
            this.ids = ids2;
            if ($outer2 == null) {
                throw new NullPointerException();
            }
            this.$outer = $outer2;
            TraversableOnce.Cclass.$init$(this);
            TraversableLike.Cclass.$init$(this);
            GenericTraversableTemplate.Cclass.$init$(this);
            Traversable.Cclass.$init$(this);
            Traversable.Cclass.$init$(this);
            IterableLike.Cclass.$init$(this);
            Iterable.Cclass.$init$(this);
            Iterable.Cclass.$init$(this);
            Function1.Cclass.$init$(this);
            GenericSetTemplate.Cclass.$init$(this);
            Addable.Cclass.$init$(this);
            Subtractable.Cclass.$init$(this);
            SetLike.Cclass.$init$(this);
            Set.Cclass.$init$(this);
            Set.Cclass.$init$(this);
        }

        public <B> B $div$colon(B z, Function2<B, Value, B> op) {
            return TraversableOnce.Cclass.$div$colon(this, z, op);
        }

        public <B, That> That $plus$plus(TraversableOnce<B> that, CanBuildFrom<ValueSet, B, That> bf) {
            return TraversableLike.Cclass.$plus$plus(this, that, bf);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [scala.collection.generic.Addable, scala.Enumeration$ValueSet] */
        public ValueSet $plus$plus(TraversableOnce<Value> xs) {
            return Addable.Cclass.$plus$plus(this, xs);
        }

        public StringBuilder addString(StringBuilder b, String start, String sep, String end) {
            return TraversableOnce.Cclass.addString(this, b, start, sep, end);
        }

        public <A> Function1<Value, A> andThen(Function1<Boolean, A> g) {
            return Function1.Cclass.andThen(this, g);
        }

        public boolean apply(Object elem) {
            return SetLike.Cclass.apply(this, elem);
        }

        public int apply$mcII$sp(int v1) {
            return Function1.Cclass.apply$mcII$sp(this, v1);
        }

        public void apply$mcVI$sp(int v1) {
            Function1.Cclass.apply$mcVI$sp(this, v1);
        }

        public void apply$mcVL$sp(long v1) {
            Function1.Cclass.apply$mcVL$sp(this, v1);
        }

        public boolean canEqual(Object that) {
            return IterableLike.Cclass.canEqual(this, that);
        }

        public GenericCompanion<scala.collection.immutable.Set> companion() {
            return Set.Cclass.companion(this);
        }

        public <A> Function1<A, Boolean> compose(Function1<A, Value> g) {
            return Function1.Cclass.compose(this, g);
        }

        public <B> void copyToArray(Object xs, int start) {
            TraversableOnce.Cclass.copyToArray(this, xs, start);
        }

        public <B> void copyToArray(Object xs, int start, int len) {
            IterableLike.Cclass.copyToArray(this, xs, start, len);
        }

        public Object drop(int n) {
            return TraversableLike.Cclass.drop(this, n);
        }

        public boolean equals(Object that) {
            return SetLike.Cclass.equals(this, that);
        }

        public boolean exists(Function1<Value, Boolean> p) {
            return IterableLike.Cclass.exists(this, p);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.Enumeration$ValueSet] */
        public ValueSet filter(Function1<Value, Boolean> p) {
            return TraversableLike.Cclass.filter(this, p);
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object, scala.Enumeration$ValueSet] */
        public ValueSet filterNot(Function1<Value, Boolean> p) {
            return TraversableLike.Cclass.filterNot(this, p);
        }

        public <B> B foldLeft(B z, Function2<B, Value, B> op) {
            return TraversableOnce.Cclass.foldLeft(this, z, op);
        }

        public boolean forall(Function1<Value, Boolean> p) {
            return IterableLike.Cclass.forall(this, p);
        }

        public <U> void foreach(Function1<Value, U> f) {
            IterableLike.Cclass.foreach(this, f);
        }

        public <B> Builder<B, scala.collection.immutable.Set<B>> genericBuilder() {
            return GenericTraversableTemplate.Cclass.genericBuilder(this);
        }

        public int hashCode() {
            return SetLike.Cclass.hashCode(this);
        }

        public Object head() {
            return IterableLike.Cclass.head(this);
        }

        public BitSet ids() {
            return this.ids;
        }

        public boolean isEmpty() {
            return SetLike.Cclass.isEmpty(this);
        }

        public final boolean isTraversableAgain() {
            return TraversableLike.Cclass.isTraversableAgain(this);
        }

        public Object last() {
            return TraversableLike.Cclass.last(this);
        }

        public <B, That> That map(Function1<Value, B> f, CanBuildFrom<ValueSet, B, That> bf) {
            return TraversableLike.Cclass.map(this, f, bf);
        }

        public String mkString() {
            return TraversableOnce.Cclass.mkString(this);
        }

        public String mkString(String sep) {
            return TraversableOnce.Cclass.mkString(this, sep);
        }

        public String mkString(String start, String sep, String end) {
            return TraversableOnce.Cclass.mkString(this, start, sep, end);
        }

        public Builder<Value, ValueSet> newBuilder() {
            return SetLike.Cclass.newBuilder(this);
        }

        public boolean nonEmpty() {
            return TraversableOnce.Cclass.nonEmpty(this);
        }

        public <B> B reduceLeft(Function2<B, Value, B> op) {
            return TraversableOnce.Cclass.reduceLeft(this, op);
        }

        public Object repr() {
            return TraversableLike.Cclass.repr(this);
        }

        public <B> boolean sameElements(scala.collection.Iterable<B> that) {
            return IterableLike.Cclass.sameElements(this, that);
        }

        public /* synthetic */ Enumeration scala$Enumeration$ValueSet$$$outer() {
            return this.$outer;
        }

        public int size() {
            return TraversableOnce.Cclass.size(this);
        }

        public Object slice(int from, int until) {
            return IterableLike.Cclass.slice(this, from, until);
        }

        public boolean subsetOf(scala.collection.Set<Value> that) {
            return SetLike.Cclass.subsetOf(this, that);
        }

        public <B> B sum(Numeric<B> num) {
            return TraversableOnce.Cclass.sum(this, num);
        }

        public Object tail() {
            return TraversableLike.Cclass.tail(this);
        }

        public scala.collection.Iterable<Value> thisCollection() {
            return IterableLike.Cclass.thisCollection(this);
        }

        public <B> Object toArray(ClassManifest<B> evidence$1) {
            return TraversableOnce.Cclass.toArray(this, evidence$1);
        }

        public <B> Buffer<B> toBuffer() {
            return TraversableOnce.Cclass.toBuffer(this);
        }

        public List<Value> toList() {
            return TraversableOnce.Cclass.toList(this);
        }

        public <B> scala.collection.immutable.Set<B> toSet() {
            return TraversableOnce.Cclass.toSet(this);
        }

        public Stream<Value> toStream() {
            return IterableLike.Cclass.toStream(this);
        }

        public String toString() {
            return SetLike.Cclass.toString(this);
        }

        public <A1, B, That> That zip(scala.collection.Iterable<B> that, CanBuildFrom<ValueSet, Tuple2<A1, B>, That> bf) {
            return IterableLike.Cclass.zip(this, that, bf);
        }

        public ValueSet empty() {
            return scala$Enumeration$ValueSet$$$outer().ValueSet().empty();
        }

        public boolean contains(Value v) {
            return ids().contains(v.id());
        }

        public ValueSet $plus(Value value) {
            return new ValueSet(scala$Enumeration$ValueSet$$$outer(), ids().$plus(value.id()));
        }

        public Iterator<Value> iterator() {
            return ids().iterator().map(new Enumeration$ValueSet$$anonfun$iterator$1(this));
        }

        public String stringPrefix() {
            return new StringBuilder().append((Object) String.valueOf(scala$Enumeration$ValueSet$$$outer())).append((Object) ".ValueSet").toString();
        }
    }

    public final Enumeration$ValueSet$ ValueSet() {
        if (this.ValueSet$module == null) {
            this.ValueSet$module = new Enumeration$ValueSet$(this);
        }
        return this.ValueSet$module;
    }
}
