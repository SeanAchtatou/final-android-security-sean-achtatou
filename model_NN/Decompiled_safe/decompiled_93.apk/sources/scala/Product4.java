package scala;

import scala.runtime.BoxesRunTime;

/* compiled from: Product4.scala */
public interface Product4<T1, T2, T3, T4> extends Product, ScalaObject {
    T1 _1();

    T2 _2();

    T3 _3();

    T4 _4();

    /* renamed from: scala.Product4$class  reason: invalid class name */
    /* compiled from: Product4.scala */
    public abstract class Cclass {
        public static void $init$(Product4 $this) {
        }

        public static int productArity(Product4 $this) {
            return 4;
        }

        public static Object productElement(Product4 $this, int n) throws IndexOutOfBoundsException {
            switch (n) {
                case 0:
                    return $this._1();
                case 1:
                    return $this._2();
                case 2:
                    return $this._3();
                case 3:
                    return $this._4();
                default:
                    throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(n).toString());
            }
        }
    }
}
