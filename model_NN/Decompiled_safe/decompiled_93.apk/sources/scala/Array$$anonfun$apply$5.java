package scala;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.IntRef;
import scala.runtime.ScalaRunTime$;

/* compiled from: Array.scala */
public final class Array$$anonfun$apply$5 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Object array$1;
    public final /* synthetic */ IntRef i$1;

    public Array$$anonfun$apply$5(Object obj, IntRef intRef) {
        this.array$1 = obj;
        this.i$1 = intRef;
    }

    public final void apply(T x) {
        ScalaRunTime$.MODULE$.array_update(this.array$1, this.i$1.elem, x);
        this.i$1.elem++;
    }
}
