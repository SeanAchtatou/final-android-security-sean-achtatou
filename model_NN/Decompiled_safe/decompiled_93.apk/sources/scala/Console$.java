package scala;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import scala.util.DynamicVariable;

/* compiled from: Console.scala */
public final class Console$ implements ScalaObject {
    public static final Console$ MODULE$ = null;
    private final DynamicVariable<PrintStream> errVar = new DynamicVariable<>(System.err);
    private final DynamicVariable<BufferedReader> inVar = new DynamicVariable<>(new BufferedReader(new InputStreamReader(System.in)));
    private final DynamicVariable<PrintStream> outVar = new DynamicVariable<>(System.out);

    static {
        new Console$();
    }

    private Console$() {
        MODULE$ = this;
    }

    private DynamicVariable<PrintStream> outVar() {
        return this.outVar;
    }

    private DynamicVariable<PrintStream> errVar() {
        return this.errVar;
    }

    public PrintStream err() {
        return errVar().value();
    }

    public void println(Object x) {
        outVar().value().println(x);
    }
}
