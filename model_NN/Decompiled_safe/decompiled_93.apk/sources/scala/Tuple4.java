package scala;

import scala.Product;
import scala.Product4;
import scala.collection.Iterator;
import scala.collection.mutable.StringBuilder;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime$;

/* compiled from: Tuple4.scala */
public class Tuple4<T1, T2, T3, T4> implements Product4<T1, T2, T3, T4>, ScalaObject, Product, ScalaObject {
    private final T1 _1;
    private final T2 _2;
    private final T3 _3;
    private final T4 _4;

    public Tuple4(T1 _12, T2 _22, T3 _32, T4 _42) {
        this._1 = _12;
        this._2 = _22;
        this._3 = _32;
        this._4 = _42;
        Product.Cclass.$init$(this);
        Product4.Cclass.$init$(this);
    }

    private final /* synthetic */ boolean gd1$1(Object obj, Object obj2, Object obj3, Object obj4) {
        Object _12 = _1();
        if (obj == _12 ? true : obj == null ? false : obj instanceof Number ? BoxesRunTime.equalsNumObject((Number) obj, _12) : obj instanceof Character ? BoxesRunTime.equalsCharObject((Character) obj, _12) : obj.equals(_12)) {
            Object _22 = _2();
            if (obj2 == _22 ? true : obj2 == null ? false : obj2 instanceof Number ? BoxesRunTime.equalsNumObject((Number) obj2, _22) : obj2 instanceof Character ? BoxesRunTime.equalsCharObject((Character) obj2, _22) : obj2.equals(_22)) {
                Object _32 = _3();
                if (obj3 == _32 ? true : obj3 == null ? false : obj3 instanceof Number ? BoxesRunTime.equalsNumObject((Number) obj3, _32) : obj3 instanceof Character ? BoxesRunTime.equalsCharObject((Character) obj3, _32) : obj3.equals(_32)) {
                    Object _42 = _4();
                    if (obj4 == _42 ? true : obj4 == null ? false : obj4 instanceof Number ? BoxesRunTime.equalsNumObject((Number) obj4, _42) : obj4 instanceof Character ? BoxesRunTime.equalsCharObject((Character) obj4, _42) : obj4.equals(_42)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public T1 _1() {
        return this._1;
    }

    public T2 _2() {
        return this._2;
    }

    public T3 _3() {
        return this._3;
    }

    public T4 _4() {
        return this._4;
    }

    public boolean canEqual(Object obj) {
        return obj instanceof Tuple4;
    }

    public boolean equals(Object obj) {
        boolean z;
        if (this != obj) {
            if (obj instanceof Tuple4) {
                Tuple4 tuple4 = (Tuple4) obj;
                z = gd1$1(tuple4._1(), tuple4._2(), tuple4._3(), tuple4._4()) ? ((Tuple4) obj).canEqual(this) : false;
            } else {
                z = false;
            }
            if (!z) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return ScalaRunTime$.MODULE$._hashCode(this);
    }

    public int productArity() {
        return Product4.Cclass.productArity(this);
    }

    public Object productElement(int n) throws IndexOutOfBoundsException {
        return Product4.Cclass.productElement(this, n);
    }

    public Iterator<Object> productIterator() {
        return Product.Cclass.productIterator(this);
    }

    public String productPrefix() {
        return "Tuple4";
    }

    public String toString() {
        return new StringBuilder().append((Object) "(").append(_1()).append((Object) ",").append(_2()).append((Object) ",").append(_3()).append((Object) ",").append(_4()).append((Object) ")").toString();
    }
}
