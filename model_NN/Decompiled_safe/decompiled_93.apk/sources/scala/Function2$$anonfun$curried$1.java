package scala;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;

/* compiled from: Function2.scala */
public final class Function2$$anonfun$curried$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Function2 $outer;

    public Function2$$anonfun$curried$1(Function2<T1, T2, R> $outer2) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
    }

    public final Function1<T2, R> apply(T1 x1$1) {
        return new Function2$$anonfun$curried$1$$anonfun$apply$1(this, x1$1);
    }
}
