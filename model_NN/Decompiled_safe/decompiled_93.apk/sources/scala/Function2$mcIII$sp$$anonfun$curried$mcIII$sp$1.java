package scala;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

/* compiled from: Function2.scala */
public final class Function2$mcIII$sp$$anonfun$curried$mcIII$sp$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Function2$mcIII$sp $outer;

    public Function2$mcIII$sp$$anonfun$curried$mcIII$sp$1(Function2$mcIII$sp $outer2) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return apply(BoxesRunTime.unboxToInt(v1));
    }

    public final Function1<Integer, Integer> apply(int x1$4) {
        return new Function2$mcIII$sp$$anonfun$curried$mcIII$sp$1$$anonfun$apply$4(this, x1$4);
    }
}
