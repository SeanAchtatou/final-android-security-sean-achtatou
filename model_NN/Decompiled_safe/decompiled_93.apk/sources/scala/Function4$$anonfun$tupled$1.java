package scala;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;

/* compiled from: Function4.scala */
public final class Function4$$anonfun$tupled$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Function4 $outer;

    public Function4$$anonfun$tupled$1(Function4<T1, T2, T3, T4, R> $outer2) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return apply((Tuple4) ((Tuple4) v1));
    }

    public final R apply(Tuple4<T1, T2, T3, T4> tuple4) {
        if (tuple4 != null) {
            return this.$outer.apply(tuple4._1(), tuple4._2(), tuple4._3(), tuple4._4());
        }
        throw new MatchError(tuple4);
    }
}
