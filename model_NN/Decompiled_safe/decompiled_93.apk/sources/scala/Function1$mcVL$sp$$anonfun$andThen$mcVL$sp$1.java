package scala;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

/* compiled from: Function1.scala */
public final class Function1$mcVL$sp$$anonfun$andThen$mcVL$sp$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Function1$mcVL$sp $outer;
    public final /* synthetic */ Function1 g$16;

    public Function1$mcVL$sp$$anonfun$andThen$mcVL$sp$1(Function1$mcVL$sp $outer2, Function1 function1) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.g$16 = function1;
    }

    public final A apply(long x) {
        Function1 function1 = this.g$16;
        this.$outer.apply(x);
        return function1.apply(BoxedUnit.UNIT);
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return apply(BoxesRunTime.unboxToLong(v1));
    }
}
