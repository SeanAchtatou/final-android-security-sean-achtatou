package scala;

/* compiled from: MatchError.scala */
public final class MatchError extends RuntimeException implements ScalaObject {
    public MatchError(String msg) {
        super(msg);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public MatchError(Object obj) {
        this(obj == null ? "null" : obj.toString());
    }
}
