package scala;

import scala.collection.mutable.StringBuilder;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime$;

/* compiled from: Option.scala */
public final class Some<A> extends Option<A> implements ScalaObject, Product, ScalaObject {
    public final A x;

    public Some(A x2) {
        this.x = x2;
    }

    private final /* synthetic */ boolean gd1$1(Object obj) {
        A a = this.x;
        return obj == a ? true : obj == null ? false : obj instanceof Number ? BoxesRunTime.equalsNumObject((Number) obj, a) : obj instanceof Character ? BoxesRunTime.equalsCharObject((Character) obj, a) : obj.equals(a);
    }

    public boolean canEqual(Object obj) {
        return obj instanceof Some;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (!(obj instanceof Some ? gd1$1(((Some) obj).x) ? this instanceof Some : false : false)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return ScalaRunTime$.MODULE$._hashCode(this);
    }

    public int productArity() {
        return 1;
    }

    public Object productElement(int i) {
        if (i == 0) {
            return this.x;
        }
        throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(i).toString());
    }

    public String productPrefix() {
        return "Some";
    }

    public String toString() {
        return productIterator().mkString(new StringBuilder().append((Object) "Some").append((Object) "(").toString(), ",", ")");
    }

    public A x() {
        return this.x;
    }

    public boolean isEmpty() {
        return false;
    }

    public A get() {
        return this.x;
    }
}
