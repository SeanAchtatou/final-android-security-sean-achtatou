package scala;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;

/* compiled from: Function1.scala */
public final class Function1$$anonfun$andThen$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    private final /* synthetic */ Function1 $outer;
    private final /* synthetic */ Function1 g$2;

    public Function1$$anonfun$andThen$1(Function1 $outer2, Function1<T1, R> function1) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.g$2 = function1;
    }

    public final A apply(T1 x) {
        return this.g$2.apply(this.$outer.apply(x));
    }
}
