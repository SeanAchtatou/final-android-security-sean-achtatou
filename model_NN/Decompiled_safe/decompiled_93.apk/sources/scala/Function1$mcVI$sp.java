package scala;

/* compiled from: Function1.scala */
public interface Function1$mcVI$sp extends Function1<Integer, Object> {
    <A> Function1<Integer, A> andThen$mcVI$sp(Function1<Object, A> function1);

    void apply(int i);

    <A> Function1<A, Object> compose$mcVI$sp(Function1<A, Integer> function1);

    /* renamed from: scala.Function1$mcVI$sp$class  reason: invalid class name */
    /* compiled from: Function1.scala */
    public abstract class Cclass {
        public static void $init$(Function1$mcVI$sp $this) {
        }

        public static void apply(Function1$mcVI$sp $this, int v1) {
            $this.apply$mcVI$sp(v1);
        }

        public static Function1 compose(Function1$mcVI$sp $this, Function1 g) {
            return $this.compose$mcVI$sp(g);
        }

        public static Function1 compose$mcVI$sp(Function1$mcVI$sp $this, Function1 g$3) {
            return new Function1$mcVI$sp$$anonfun$compose$mcVI$sp$1($this, g$3);
        }

        public static Function1 andThen(Function1$mcVI$sp $this, Function1 g) {
            return $this.andThen$mcVI$sp(g);
        }

        public static Function1 andThen$mcVI$sp(Function1$mcVI$sp $this, Function1 g$4) {
            return new Function1$mcVI$sp$$anonfun$andThen$mcVI$sp$1($this, g$4);
        }
    }
}
