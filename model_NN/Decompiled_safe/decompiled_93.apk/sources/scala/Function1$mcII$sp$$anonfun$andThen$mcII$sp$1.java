package scala;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

/* compiled from: Function1.scala */
public final class Function1$mcII$sp$$anonfun$andThen$mcII$sp$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Function1$mcII$sp $outer;
    public final /* synthetic */ Function1 g$8;

    public Function1$mcII$sp$$anonfun$andThen$mcII$sp$1(Function1$mcII$sp $outer2, Function1 function1) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.g$8 = function1;
    }

    public final A apply(int x) {
        return this.g$8.apply(BoxesRunTime.boxToInteger(this.$outer.apply(x)));
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return apply(BoxesRunTime.unboxToInt(v1));
    }
}
