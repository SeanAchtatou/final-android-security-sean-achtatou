package scala;

import scala.collection.IndexedSeq$;
import scala.collection.Iterable$;
import scala.collection.Iterator$;
import scala.collection.Seq$;
import scala.collection.Traversable$;
import scala.collection.immutable.C$colon$colon$;
import scala.collection.immutable.List$;
import scala.collection.immutable.Nil$;
import scala.collection.immutable.Range$;
import scala.collection.immutable.Stream$;
import scala.collection.immutable.Stream$$hash$colon$colon$;
import scala.collection.immutable.Vector$;
import scala.math.BigDecimal$;
import scala.math.BigInt$;
import scala.math.Numeric$;
import scala.math.Ordered$;
import scala.math.Ordering$;
import scala.xml.TopScope$;

/* compiled from: package.scala */
public final class package$ implements ScalaObject {
    public static final package$ MODULE$ = null;
    private final C$colon$colon$ $colon$colon = C$colon$colon$.MODULE$;
    private final Stream$$hash$colon$colon$ $hash$colon$colon = Stream$$hash$colon$colon$.MODULE$;
    private final TopScope$ $scope = TopScope$.MODULE$;
    private final BigDecimal$ BigDecimal = BigDecimal$.MODULE$;
    private final BigInt$ BigInt = BigInt$.MODULE$;
    private final Iterable$ Collection = Iterable();
    private final IndexedSeq$ IndexedSeq = IndexedSeq$.MODULE$;
    private final Iterable$ Iterable = Iterable$.MODULE$;
    private final Iterator$ Iterator = Iterator$.MODULE$;
    private final List$ List = List$.MODULE$;
    private final Nil$ Nil = Nil$.MODULE$;
    private final Numeric$ Numeric = Numeric$.MODULE$;
    private final Ordered$ Ordered = Ordered$.MODULE$;
    private final Ordering$ Ordering = Ordering$.MODULE$;
    private final IndexedSeq$ RandomAccessSeq = IndexedSeq$.MODULE$;
    private final Range$ Range = Range$.MODULE$;
    private final Seq$ Seq = Seq$.MODULE$;
    private final Seq$ Sequence = Seq$.MODULE$;
    private final Stream$ Stream = Stream$.MODULE$;
    private final Traversable$ Traversable = Traversable$.MODULE$;
    private final Vector$ Vector = Vector$.MODULE$;

    static {
        new package$();
    }

    private package$() {
        MODULE$ = this;
    }

    public Iterable$ Iterable() {
        return this.Iterable;
    }

    public Iterator$ Iterator() {
        return this.Iterator;
    }
}
