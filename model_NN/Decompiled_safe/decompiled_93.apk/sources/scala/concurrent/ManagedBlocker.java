package scala.concurrent;

/* compiled from: ManagedBlocker.scala */
public interface ManagedBlocker {
    boolean block();

    boolean isReleasable();
}
