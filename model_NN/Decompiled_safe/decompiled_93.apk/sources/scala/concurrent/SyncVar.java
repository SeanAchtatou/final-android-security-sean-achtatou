package scala.concurrent;

import scala.None$;
import scala.Option;
import scala.ScalaObject;

/* compiled from: SyncVar.scala */
public class SyncVar<A> implements ScalaObject {
    private Option<Throwable> exception = None$.MODULE$;
    private boolean isDefined = false;
    private A value;

    private boolean isDefined() {
        return this.isDefined;
    }

    private void isDefined_$eq(boolean z) {
        this.isDefined = z;
    }

    private A value() {
        return this.value;
    }

    private void value_$eq(A a) {
        this.value = a;
    }

    private Option<Throwable> exception() {
        return this.exception;
    }

    private void exception_$eq(Option<Throwable> option) {
        this.exception = option;
    }

    public A get() {
        A value2;
        synchronized (this) {
            while (!isDefined()) {
                wait();
            }
            if (exception().isEmpty()) {
                value2 = value();
            } else {
                throw exception().get();
            }
        }
        return value2;
    }

    public void set(A x) {
        synchronized (this) {
            value_$eq(x);
            isDefined_$eq(true);
            exception_$eq(None$.MODULE$);
            notifyAll();
        }
    }
}
