package scala.concurrent;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import scala.ScalaObject;

/* compiled from: ThreadPoolRunner.scala */
public interface ThreadPoolRunner extends ScalaObject {
    ExecutorService executor();

    /* renamed from: scala.concurrent.ThreadPoolRunner$class  reason: invalid class name */
    /* compiled from: ThreadPoolRunner.scala */
    public abstract class Cclass {
        public static void $init$(ThreadPoolRunner $this) {
        }

        public static void execute(ThreadPoolRunner $this, Callable task) {
            $this.executor().execute((Runnable) task);
        }

        public static void managedBlock(ThreadPoolRunner $this, ManagedBlocker blocker) {
            blocker.block();
        }
    }
}
