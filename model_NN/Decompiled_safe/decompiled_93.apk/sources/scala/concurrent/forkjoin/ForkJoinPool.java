package scala.concurrent.forkjoin;

import java.lang.Thread;
import java.lang.reflect.Field;
import java.security.AccessController;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.util.Collection;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.LockSupport;
import java.util.concurrent.locks.ReentrantLock;
import sun.misc.Unsafe;

public class ForkJoinPool {
    static final Unsafe _unsafe;
    public static final ForkJoinWorkerThreadFactory defaultForkJoinWorkerThreadFactory = new DefaultForkJoinWorkerThreadFactory();
    static final long eventCountOffset;
    private static final RuntimePermission modifyThreadPermission = new RuntimePermission("modifyThread");
    private static final AtomicInteger poolNumberGenerator = new AtomicInteger();
    static final long runControlOffset;
    static final long spareStackOffset;
    static final long syncStackOffset;
    static final long workerCountsOffset;
    private volatile long eventCount;
    private final ForkJoinWorkerThreadFactory factory;
    private volatile boolean locallyFifo;
    private volatile boolean maintainsParallelism;
    private volatile int maxPoolSize;
    private volatile int parallelism;
    private final int poolNumber;
    private volatile int runControl;
    private volatile WaitQueueNode spareStack;
    private final AtomicLong stealCount;
    private final LinkedTransferQueue<ForkJoinTask<?>> submissionQueue;
    private volatile WaitQueueNode syncStack;
    private final Condition termination;
    private Thread.UncaughtExceptionHandler ueh;
    private volatile int workerCounts;
    private final ReentrantLock workerLock;
    public volatile ForkJoinWorkerThread[] workers;

    public interface ForkJoinWorkerThreadFactory {
        ForkJoinWorkerThread newThread(ForkJoinPool forkJoinPool);
    }

    public interface ManagedBlocker {
        boolean block() throws InterruptedException;

        boolean isReleasable();
    }

    interface RunnableFuture extends Runnable {
    }

    static class DefaultForkJoinWorkerThreadFactory implements ForkJoinWorkerThreadFactory {
        DefaultForkJoinWorkerThreadFactory() {
        }

        public ForkJoinWorkerThread newThread(ForkJoinPool pool) {
            try {
                return new ForkJoinWorkerThread(pool);
            } catch (OutOfMemoryError e) {
                return null;
            }
        }
    }

    static {
        try {
            _unsafe = getUnsafe();
            eventCountOffset = fieldOffset("eventCount");
            workerCountsOffset = fieldOffset("workerCounts");
            runControlOffset = fieldOffset("runControl");
            syncStackOffset = fieldOffset("syncStack");
            spareStackOffset = fieldOffset("spareStack");
        } catch (Throwable th) {
            throw new RuntimeException("Could not initialize intrinsics", th);
        }
    }

    private static void checkPermission() {
        SecurityManager security = System.getSecurityManager();
        if (security != null) {
            security.checkPermission(modifyThreadPermission);
        }
    }

    private static int totalCountOf(int s) {
        return s >>> 16;
    }

    private static int runningCountOf(int s) {
        return 65535 & s;
    }

    private static int workerCountsFor(int t, int r) {
        return (t << 16) + r;
    }

    /* access modifiers changed from: package-private */
    public final void updateRunningCount(int delta) {
        int s;
        do {
            s = this.workerCounts;
        } while (!casWorkerCounts(s, s + delta));
    }

    private void updateWorkerCount(int delta) {
        int s;
        int d = delta + (delta << 16);
        do {
            s = this.workerCounts;
        } while (!casWorkerCounts(s, s + d));
    }

    private static int runStateOf(int c) {
        return c >>> 16;
    }

    private static int activeCountOf(int c) {
        return 65535 & c;
    }

    private static int runControlFor(int r, int a) {
        return (r << 16) + a;
    }

    /* access modifiers changed from: package-private */
    public final boolean tryIncrementActiveCount() {
        int c = this.runControl;
        return casRunControl(c, c + 1);
    }

    /* access modifiers changed from: package-private */
    public final boolean tryDecrementActiveCount() {
        int c = this.runControl;
        int nextc = c - 1;
        if (!casRunControl(c, nextc)) {
            return false;
        }
        if (canTerminateOnShutdown(nextc)) {
            terminateOnShutdown();
        }
        return true;
    }

    private static boolean canTerminateOnShutdown(int c) {
        return (((-c) & c) >>> 16) != 0;
    }

    private boolean transitionRunStateTo(int state) {
        int c;
        do {
            c = this.runControl;
            if (runStateOf(c) >= state) {
                return false;
            }
        } while (!casRunControl(c, runControlFor(state, activeCountOf(c))));
        return true;
    }

    public ForkJoinPool() {
        this(Runtime.getRuntime().availableProcessors(), defaultForkJoinWorkerThreadFactory);
    }

    public ForkJoinPool(int parallelism2, ForkJoinWorkerThreadFactory factory2) {
        if (parallelism2 <= 0 || parallelism2 > 32767) {
            throw new IllegalArgumentException();
        } else if (factory2 == null) {
            throw new NullPointerException();
        } else {
            checkPermission();
            this.factory = factory2;
            this.parallelism = parallelism2;
            this.maxPoolSize = 32767;
            this.maintainsParallelism = true;
            this.poolNumber = poolNumberGenerator.incrementAndGet();
            this.workerLock = new ReentrantLock();
            this.termination = this.workerLock.newCondition();
            this.stealCount = new AtomicLong();
            this.submissionQueue = new LinkedTransferQueue<>();
        }
    }

    private ForkJoinWorkerThread createWorker(int index) {
        Thread.UncaughtExceptionHandler h = this.ueh;
        ForkJoinWorkerThread w = this.factory.newThread(this);
        if (w != null) {
            w.poolIndex = index;
            w.setDaemon(true);
            w.setAsyncMode(this.locallyFifo);
            w.setName("ForkJoinPool-" + this.poolNumber + "-worker-" + index);
            if (h != null) {
                w.setUncaughtExceptionHandler(h);
            }
        }
        return w;
    }

    private static int arraySizeFor(int ps) {
        if (ps <= 1) {
            return 1;
        }
        return 1 << (32 - Integer.numberOfLeadingZeros(ps - 1));
    }

    public static ForkJoinWorkerThread[] copyOfWorkers(ForkJoinWorkerThread[] original, int newLength) {
        ForkJoinWorkerThread[] copy = new ForkJoinWorkerThread[newLength];
        System.arraycopy(original, 0, copy, 0, Math.min(newLength, original.length));
        return copy;
    }

    private ForkJoinWorkerThread[] ensureWorkerArrayCapacity(int newLength) {
        ForkJoinWorkerThread[] ws = this.workers;
        if (ws == null) {
            ForkJoinWorkerThread[] forkJoinWorkerThreadArr = new ForkJoinWorkerThread[arraySizeFor(newLength)];
            this.workers = forkJoinWorkerThreadArr;
            return forkJoinWorkerThreadArr;
        } else if (newLength <= ws.length) {
            return ws;
        } else {
            ForkJoinWorkerThread[] copyOfWorkers = copyOfWorkers(ws, arraySizeFor(newLength));
            this.workers = copyOfWorkers;
            return copyOfWorkers;
        }
    }

    private void tryShrinkWorkerArray() {
        ForkJoinWorkerThread[] ws = this.workers;
        if (ws != null) {
            int len = ws.length;
            int last = len - 1;
            while (last >= 0 && ws[last] == null) {
                last--;
            }
            int newLength = arraySizeFor(last + 1);
            if (newLength < len) {
                this.workers = copyOfWorkers(ws, newLength);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void ensureWorkerInitialization() {
        if (this.workers == null) {
            ReentrantLock lock = this.workerLock;
            lock.lock();
            try {
                if (this.workers == null) {
                    int ps = this.parallelism;
                    ForkJoinWorkerThread[] ws = ensureWorkerArrayCapacity(ps);
                    for (int i = 0; i < ps; i++) {
                        ForkJoinWorkerThread w = createWorker(i);
                        if (w != null) {
                            ws[i] = w;
                            w.start();
                            updateWorkerCount(1);
                        }
                    }
                }
            } finally {
                lock.unlock();
            }
        }
    }

    private void createAndStartAddedWorkers() {
        int k;
        resumeAllSpares();
        int ps = this.parallelism;
        ForkJoinWorkerThread[] ws = ensureWorkerArrayCapacity(ps);
        int len = ws.length;
        int k2 = 0;
        while (k2 < len) {
            if (ws[k2] != null) {
                k2++;
            } else {
                int s = this.workerCounts;
                int tc = totalCountOf(s);
                int rc = runningCountOf(s);
                if (rc < ps && tc < ps) {
                    if (casWorkerCounts(s, workerCountsFor(tc + 1, rc + 1))) {
                        ForkJoinWorkerThread w = createWorker(k2);
                        if (w != null) {
                            k = k2 + 1;
                            ws[k2] = w;
                            w.start();
                        } else {
                            updateWorkerCount(-1);
                            return;
                        }
                    } else {
                        k = k2;
                    }
                    k2 = k;
                } else {
                    return;
                }
            }
        }
    }

    private <T> void doSubmit(ForkJoinTask<T> task) {
        if (isShutdown()) {
            throw new RejectedExecutionException();
        }
        if (this.workers == null) {
            ensureWorkerInitialization();
        }
        this.submissionQueue.offer(task);
        signalIdleWorkers();
    }

    public void execute(Runnable task) {
        doSubmit(new AdaptedRunnable(task, null));
    }

    static final class AdaptedRunnable<T> extends ForkJoinTask<T> implements RunnableFuture<T> {
        T result;
        final T resultOnCompletion;
        final Runnable runnable;

        AdaptedRunnable(Runnable runnable2, T result2) {
            if (runnable2 == null) {
                throw new NullPointerException();
            }
            this.runnable = runnable2;
            this.resultOnCompletion = result2;
        }

        public T getRawResult() {
            return this.result;
        }

        public boolean exec() {
            this.runnable.run();
            this.result = this.resultOnCompletion;
            return true;
        }

        public void run() {
            invoke();
        }
    }

    /* JADX INFO: finally extract failed */
    public void setParallelism(int parallelism2) {
        checkPermission();
        if (parallelism2 <= 0 || parallelism2 > this.maxPoolSize) {
            throw new IllegalArgumentException();
        }
        ReentrantLock lock = this.workerLock;
        lock.lock();
        try {
            if (!isTerminating()) {
                int p = this.parallelism;
                this.parallelism = parallelism2;
                if (parallelism2 > p) {
                    createAndStartAddedWorkers();
                } else {
                    trimSpares();
                }
            }
            lock.unlock();
            signalIdleWorkers();
        } catch (Throwable th) {
            lock.unlock();
            throw th;
        }
    }

    public int getParallelism() {
        return this.parallelism;
    }

    public int getMaximumPoolSize() {
        return this.maxPoolSize;
    }

    public void setMaximumPoolSize(int newMax) {
        if (newMax < 0 || newMax > 32767) {
            throw new IllegalArgumentException();
        }
        this.maxPoolSize = newMax;
    }

    public boolean setAsyncMode(boolean async) {
        boolean oldMode = this.locallyFifo;
        this.locallyFifo = async;
        ForkJoinWorkerThread[] ws = this.workers;
        if (ws != null) {
            for (ForkJoinWorkerThread t : ws) {
                if (t != null) {
                    t.setAsyncMode(async);
                }
            }
        }
        return oldMode;
    }

    public boolean isQuiescent() {
        return activeCountOf(this.runControl) == 0;
    }

    public long getStealCount() {
        return this.stealCount.get();
    }

    private void updateStealCount(ForkJoinWorkerThread w) {
        int sc = w.getAndClearStealCount();
        if (sc != 0) {
            this.stealCount.addAndGet((long) sc);
        }
    }

    public long getQueuedTaskCount() {
        long count = 0;
        ForkJoinWorkerThread[] ws = this.workers;
        if (ws != null) {
            for (ForkJoinWorkerThread t : ws) {
                if (t != null) {
                    count += (long) t.getQueueSize();
                }
            }
        }
        return count;
    }

    public int getQueuedSubmissionCount() {
        return this.submissionQueue.size();
    }

    public boolean hasQueuedSubmissions() {
        return !this.submissionQueue.isEmpty();
    }

    /* access modifiers changed from: protected */
    public ForkJoinTask<?> pollSubmission() {
        return this.submissionQueue.poll();
    }

    /* access modifiers changed from: protected */
    public int drainTasksTo(Collection<ForkJoinTask<?>> c) {
        int n = this.submissionQueue.drainTo(c);
        ForkJoinWorkerThread[] ws = this.workers;
        if (ws != null) {
            for (ForkJoinWorkerThread w : ws) {
                if (w != null) {
                    n += w.drainTasksTo(c);
                }
            }
        }
        return n;
    }

    public String toString() {
        int ps = this.parallelism;
        int wc = this.workerCounts;
        int rc = this.runControl;
        long st = getStealCount();
        long qt = getQueuedTaskCount();
        return super.toString() + "[" + runStateToString(runStateOf(rc)) + ", parallelism = " + ps + ", size = " + totalCountOf(wc) + ", active = " + activeCountOf(rc) + ", running = " + runningCountOf(wc) + ", steals = " + st + ", tasks = " + qt + ", submissions = " + ((long) getQueuedSubmissionCount()) + "]";
    }

    private static String runStateToString(int rs) {
        switch (rs) {
            case 0:
                return "Running";
            case 1:
                return "Shutting down";
            case 2:
                return "Terminating";
            case 3:
                return "Terminated";
            default:
                throw new Error("Unknown run state");
        }
    }

    public void shutdown() {
        checkPermission();
        transitionRunStateTo(1);
        if (canTerminateOnShutdown(this.runControl)) {
            terminateOnShutdown();
        }
    }

    public boolean isTerminating() {
        return runStateOf(this.runControl) >= 2;
    }

    public boolean isShutdown() {
        return runStateOf(this.runControl) >= 1;
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: package-private */
    public final void workerTerminated(ForkJoinWorkerThread w) {
        updateStealCount(w);
        updateWorkerCount(-1);
        ReentrantLock lock = this.workerLock;
        lock.lock();
        try {
            ForkJoinWorkerThread[] ws = this.workers;
            if (ws != null) {
                int idx = w.poolIndex;
                if (idx >= 0 && idx < ws.length && ws[idx] == w) {
                    ws[idx] = null;
                }
                if (totalCountOf(this.workerCounts) == 0) {
                    terminate();
                    transitionRunStateTo(3);
                    this.termination.signalAll();
                } else if (!isTerminating()) {
                    tryShrinkWorkerArray();
                    tryResumeSpare(true);
                }
            }
            lock.unlock();
            signalIdleWorkers();
        } catch (Throwable th) {
            lock.unlock();
            throw th;
        }
    }

    private void terminate() {
        if (transitionRunStateTo(2)) {
            stopAllWorkers();
            resumeAllSpares();
            signalIdleWorkers();
            cancelQueuedSubmissions();
            cancelQueuedWorkerTasks();
            interruptUnterminatedWorkers();
            signalIdleWorkers();
        }
    }

    private void terminateOnShutdown() {
        if (!hasQueuedSubmissions() && canTerminateOnShutdown(this.runControl)) {
            terminate();
        }
    }

    private void cancelQueuedSubmissions() {
        while (true) {
            ForkJoinTask<?> task = pollSubmission();
            if (task != null) {
                task.cancel(false);
            } else {
                return;
            }
        }
    }

    private void cancelQueuedWorkerTasks() {
        ReentrantLock lock = this.workerLock;
        lock.lock();
        try {
            ForkJoinWorkerThread[] ws = this.workers;
            if (ws != null) {
                for (ForkJoinWorkerThread t : ws) {
                    if (t != null) {
                        t.cancelTasks();
                    }
                }
            }
        } finally {
            lock.unlock();
        }
    }

    private void stopAllWorkers() {
        ReentrantLock lock = this.workerLock;
        lock.lock();
        try {
            ForkJoinWorkerThread[] ws = this.workers;
            if (ws != null) {
                for (ForkJoinWorkerThread t : ws) {
                    if (t != null) {
                        t.shutdownNow();
                    }
                }
            }
        } finally {
            lock.unlock();
        }
    }

    private void interruptUnterminatedWorkers() {
        ReentrantLock lock = this.workerLock;
        lock.lock();
        try {
            ForkJoinWorkerThread[] ws = this.workers;
            if (ws != null) {
                for (ForkJoinWorkerThread t : ws) {
                    if (t != null && !t.isTerminated()) {
                        try {
                            t.interrupt();
                        } catch (SecurityException e) {
                        }
                    }
                }
            }
        } finally {
            lock.unlock();
        }
    }

    static final class WaitQueueNode {
        final long count;
        WaitQueueNode next;
        volatile ForkJoinWorkerThread thread;

        WaitQueueNode(long c, ForkJoinWorkerThread w) {
            this.count = c;
            this.thread = w;
        }

        /* access modifiers changed from: package-private */
        public boolean signal() {
            ForkJoinWorkerThread t = this.thread;
            if (t == null) {
                return false;
            }
            this.thread = null;
            LockSupport.unpark(t);
            return true;
        }

        /* access modifiers changed from: package-private */
        public void awaitSyncRelease(ForkJoinPool p) {
            while (this.thread != null && !p.syncIsReleasable(this)) {
                LockSupport.park(this);
            }
        }

        /* access modifiers changed from: package-private */
        public void awaitSpareRelease() {
            while (this.thread != null) {
                if (!Thread.interrupted()) {
                    LockSupport.park(this);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final long ensureSync() {
        long c = this.eventCount;
        while (true) {
            WaitQueueNode q = this.syncStack;
            if (q != null && q.count < c) {
                if (casBarrierStack(q, null)) {
                    do {
                        q.signal();
                        q = q.next;
                    } while (q != null);
                    break;
                }
            } else {
                return c;
            }
        }
        return c;
    }

    private void signalIdleWorkers() {
        long c;
        do {
            c = this.eventCount;
        } while (!casEventCount(c, 1 + c));
        ensureSync();
    }

    /* access modifiers changed from: package-private */
    public final void signalWork() {
        WaitQueueNode q;
        if (this.syncStack != null) {
            long c = this.eventCount;
            if (casEventCount(c, 1 + c) && (q = this.syncStack) != null && q.count <= c) {
                if (!casBarrierStack(q, q.next) || !q.signal()) {
                    ensureSync();
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void sync(ForkJoinWorkerThread w) {
        WaitQueueNode h;
        updateStealCount(w);
        while (!w.isShutdown() && !isTerminating() && !suspendIfSpare(w)) {
            long prev = w.lastEventCount;
            WaitQueueNode node = null;
            while (true) {
                if (this.eventCount != prev || ((h = this.syncStack) != null && h.count != prev)) {
                    break;
                }
                if (node == null) {
                    node = new WaitQueueNode(prev, w);
                }
                node.next = h;
                if (casBarrierStack(h, node)) {
                    node.awaitSyncRelease(this);
                    break;
                }
            }
            long ec = ensureSync();
            if (ec != prev) {
                w.lastEventCount = ec;
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean syncIsReleasable(WaitQueueNode node) {
        long prev = node.count;
        if (!Thread.interrupted() && node.thread != null && ((node.next != null || !ForkJoinWorkerThread.hasQueuedTasks(this.workers)) && this.eventCount == prev)) {
            return false;
        }
        if (node.thread != null) {
            node.thread = null;
            long ec = this.eventCount;
            if (prev <= ec) {
                casEventCount(ec, 1 + ec);
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public final boolean hasNewSyncEvent(ForkJoinWorkerThread w) {
        long lc = w.lastEventCount;
        long ec = ensureSync();
        if (ec == lc) {
            return false;
        }
        w.lastEventCount = ec;
        return true;
    }

    /* access modifiers changed from: package-private */
    public final boolean preJoin(ForkJoinTask<?> joinMe, boolean maintainParallelism) {
        boolean maintainParallelism2 = maintainParallelism & this.maintainsParallelism;
        boolean dec = false;
        while (true) {
            if (this.spareStack != null && tryResumeSpare(dec)) {
                break;
            }
            int counts = this.workerCounts;
            if (!dec) {
                int counts2 = counts - 1;
                dec = casWorkerCounts(counts, counts2);
                if (dec) {
                    counts = counts2;
                }
            }
            if (!needSpare(counts, maintainParallelism2)) {
                break;
            } else if (joinMe.status < 0) {
                return true;
            } else {
                if (tryAddSpare(counts)) {
                    break;
                }
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public final boolean preBlock(ManagedBlocker blocker, boolean maintainParallelism) {
        boolean maintainParallelism2 = maintainParallelism & this.maintainsParallelism;
        boolean dec = false;
        while (true) {
            if (this.spareStack != null && tryResumeSpare(dec)) {
                break;
            }
            int counts = this.workerCounts;
            if (!dec) {
                int counts2 = counts - 1;
                dec = casWorkerCounts(counts, counts2);
                if (dec) {
                    counts = counts2;
                }
            }
            if (!needSpare(counts, maintainParallelism2)) {
                break;
            } else if (blocker.isReleasable()) {
                return true;
            } else {
                if (tryAddSpare(counts)) {
                    break;
                }
            }
        }
        return false;
    }

    private boolean needSpare(int counts, boolean maintainParallelism) {
        int ps = this.parallelism;
        int rc = runningCountOf(counts);
        int tc = totalCountOf(counts);
        int totalSurplus = tc - ps;
        return tc < this.maxPoolSize && (rc == 0 || totalSurplus < 0 || (maintainParallelism && ps - rc > totalSurplus && ForkJoinWorkerThread.hasQueuedTasks(this.workers)));
    }

    private boolean tryAddSpare(int expectedCounts) {
        ReentrantLock lock = this.workerLock;
        int expectedRunning = runningCountOf(expectedCounts);
        int expectedTotal = totalCountOf(expectedCounts);
        boolean success = false;
        boolean locked = false;
        while (true) {
            try {
                int s = this.workerCounts;
                int tc = totalCountOf(s);
                int rc = runningCountOf(s);
                if (rc > expectedRunning || tc > expectedTotal) {
                    break;
                }
                if (!locked) {
                    locked = lock.tryLock();
                    if (!locked) {
                        break;
                    }
                }
                if (casWorkerCounts(s, workerCountsFor(tc + 1, rc + 1))) {
                    createAndStartSpare(tc);
                    success = true;
                    break;
                }
            } finally {
                if (locked) {
                    lock.unlock();
                }
            }
        }
        return success;
    }

    private void createAndStartSpare(int k) {
        ForkJoinWorkerThread w;
        ForkJoinWorkerThread[] ws = ensureWorkerArrayCapacity(k + 1);
        int len = ws.length;
        if (k < len && ws[k] != null) {
            k = 0;
            while (k < len && ws[k] != null) {
                k++;
            }
        }
        if (k >= len || isTerminating() || (w = createWorker(k)) == null) {
            updateWorkerCount(-1);
        } else {
            ws[k] = w;
            w.start();
        }
        signalIdleWorkers();
    }

    private boolean suspendIfSpare(ForkJoinWorkerThread w) {
        int s;
        WaitQueueNode waitQueueNode;
        WaitQueueNode node = null;
        do {
            int i = this.parallelism;
            s = this.workerCounts;
            if (i >= runningCountOf(s)) {
                return false;
            }
            if (node == null) {
                node = new WaitQueueNode(0, w);
            }
        } while (!casWorkerCounts(s, s - 1));
        do {
            waitQueueNode = this.spareStack;
            node.next = waitQueueNode;
        } while (!casSpareStack(waitQueueNode, node));
        node.awaitSpareRelease();
        return true;
    }

    private boolean tryResumeSpare(boolean updateCount) {
        WaitQueueNode q;
        do {
            q = this.spareStack;
            if (q == null) {
                return false;
            }
        } while (!casSpareStack(q, q.next));
        if (updateCount) {
            updateRunningCount(1);
        }
        q.signal();
        return true;
    }

    private boolean resumeAllSpares() {
        WaitQueueNode q;
        do {
            q = this.spareStack;
            if (q == null) {
                return false;
            }
        } while (!casSpareStack(q, null));
        do {
            updateRunningCount(1);
            q.signal();
            q = q.next;
        } while (q != null);
        return true;
    }

    private void trimSpares() {
        int surplus = totalCountOf(this.workerCounts) - this.parallelism;
        while (surplus > 0) {
            WaitQueueNode q = this.spareStack;
            if (q == null) {
                return;
            }
            if (casSpareStack(q, null)) {
                do {
                    updateRunningCount(1);
                    ForkJoinWorkerThread w = q.thread;
                    if (w != null && surplus > 0 && runningCountOf(this.workerCounts) > 0 && w.shutdown()) {
                        surplus--;
                    }
                    q.signal();
                    q = q.next;
                } while (q != null);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001a, code lost:
        if (r0.preBlock(r4, r5) == false) goto L_0x001c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void managedBlock(scala.concurrent.forkjoin.ForkJoinPool.ManagedBlocker r4, boolean r5) throws java.lang.InterruptedException {
        /*
            r3 = 1
            java.lang.Thread r1 = java.lang.Thread.currentThread()
            boolean r2 = r1 instanceof scala.concurrent.forkjoin.ForkJoinWorkerThread
            if (r2 == 0) goto L_0x0025
            scala.concurrent.forkjoin.ForkJoinWorkerThread r1 = (scala.concurrent.forkjoin.ForkJoinWorkerThread) r1
            scala.concurrent.forkjoin.ForkJoinPool r2 = r1.pool
            r0 = r2
        L_0x000e:
            boolean r2 = r4.isReleasable()
            if (r2 != 0) goto L_0x0024
            if (r0 == 0) goto L_0x001c
            boolean r2 = r0.preBlock(r4, r5)     // Catch:{ all -> 0x0028 }
            if (r2 != 0) goto L_0x001f
        L_0x001c:
            awaitBlocker(r4)     // Catch:{ all -> 0x0028 }
        L_0x001f:
            if (r0 == 0) goto L_0x0024
            r0.updateRunningCount(r3)
        L_0x0024:
            return
        L_0x0025:
            r2 = 0
            r0 = r2
            goto L_0x000e
        L_0x0028:
            r2 = move-exception
            if (r0 == 0) goto L_0x002e
            r0.updateRunningCount(r3)
        L_0x002e:
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.concurrent.forkjoin.ForkJoinPool.managedBlock(scala.concurrent.forkjoin.ForkJoinPool$ManagedBlocker, boolean):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:0:0x0000 A[LOOP:0: B:0:0x0000->B:3:0x000a, LOOP_START, MTH_ENTER_BLOCK] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void awaitBlocker(scala.concurrent.forkjoin.ForkJoinPool.ManagedBlocker r1) throws java.lang.InterruptedException {
        /*
        L_0x0000:
            boolean r0 = r1.isReleasable()
            if (r0 != 0) goto L_0x000c
            boolean r0 = r1.block()
            if (r0 == 0) goto L_0x0000
        L_0x000c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: scala.concurrent.forkjoin.ForkJoinPool.awaitBlocker(scala.concurrent.forkjoin.ForkJoinPool$ManagedBlocker):void");
    }

    private static Unsafe getUnsafe() throws Throwable {
        try {
            return Unsafe.getUnsafe();
        } catch (SecurityException e) {
            try {
                return (Unsafe) AccessController.doPrivileged(new PrivilegedExceptionAction<Unsafe>() {
                    public Unsafe run() throws Exception {
                        return ForkJoinPool.getUnsafePrivileged();
                    }
                });
            } catch (PrivilegedActionException e2) {
                throw e2.getCause();
            }
        }
    }

    /* access modifiers changed from: private */
    public static Unsafe getUnsafePrivileged() throws NoSuchFieldException, IllegalAccessException {
        Field declaredField = Unsafe.class.getDeclaredField("theUnsafe");
        declaredField.setAccessible(true);
        return (Unsafe) declaredField.get(null);
    }

    private static long fieldOffset(String str) throws NoSuchFieldException {
        return _unsafe.objectFieldOffset(ForkJoinPool.class.getDeclaredField(str));
    }

    private boolean casEventCount(long cmp, long val) {
        return _unsafe.compareAndSwapLong(this, eventCountOffset, cmp, val);
    }

    private boolean casWorkerCounts(int cmp, int val) {
        return _unsafe.compareAndSwapInt(this, workerCountsOffset, cmp, val);
    }

    private boolean casRunControl(int cmp, int val) {
        return _unsafe.compareAndSwapInt(this, runControlOffset, cmp, val);
    }

    private boolean casSpareStack(WaitQueueNode cmp, WaitQueueNode val) {
        return _unsafe.compareAndSwapObject(this, spareStackOffset, cmp, val);
    }

    private boolean casBarrierStack(WaitQueueNode cmp, WaitQueueNode val) {
        return _unsafe.compareAndSwapObject(this, syncStackOffset, cmp, val);
    }
}
