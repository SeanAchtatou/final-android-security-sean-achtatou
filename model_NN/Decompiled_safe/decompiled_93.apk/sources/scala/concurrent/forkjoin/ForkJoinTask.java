package scala.concurrent.forkjoin;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.security.AccessController;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import sun.misc.Unsafe;

public abstract class ForkJoinTask<V> implements Future<V>, Serializable {
    static final Unsafe _unsafe;
    static final Map<ForkJoinTask<?>, Throwable> exceptionMap = Collections.synchronizedMap(new WeakHashMap());
    static final long statusOffset;
    volatile int status;

    /* access modifiers changed from: protected */
    public abstract boolean exec();

    public abstract V getRawResult();

    static {
        Unsafe tmpUnsafe = null;
        long tmpStatusOffset = 0;
        try {
            tmpUnsafe = getUnsafe();
            tmpStatusOffset = fieldOffset("status");
        } catch (Throwable th) {
            Throwable e = th;
            if (!System.getProperty("java.vm.vendor").contains("Android")) {
                throw new RuntimeException("Could not initialize intrinsics", e);
            }
        }
        _unsafe = tmpUnsafe;
        statusOffset = tmpStatusOffset;
    }

    static ForkJoinWorkerThread getWorker() {
        Thread t = Thread.currentThread();
        if (t instanceof ForkJoinWorkerThread) {
            return (ForkJoinWorkerThread) t;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public final boolean casStatus(int cmp, int val) {
        return _unsafe.compareAndSwapInt(this, statusOffset, cmp, val);
    }

    static void rethrowException(Throwable ex) {
        if (ex != null) {
            _unsafe.throwException(ex);
        }
    }

    /* access modifiers changed from: package-private */
    public final void setCompletion(int completion) {
        int s;
        ForkJoinPool pool = getPool();
        if (pool != null) {
            do {
                s = this.status;
                if (s < 0) {
                    break;
                }
            } while (!casStatus(s, completion));
            if ((65535 & s) != 0) {
                int s2 = s & 32767;
                if (s2 != 0) {
                    pool.updateRunningCount(s2);
                }
                synchronized (this) {
                    notifyAll();
                }
                return;
            }
            return;
        }
        externallySetCompletion(completion);
    }

    private void externallySetCompletion(int completion) {
        int s;
        do {
            s = this.status;
            if (s < 0) {
                break;
            }
        } while (!casStatus(s, (65535 & s) | completion));
        synchronized (this) {
            notifyAll();
        }
    }

    /* access modifiers changed from: package-private */
    public final void setNormalCompletion() {
        if (!_unsafe.compareAndSwapInt(this, statusOffset, 0, -536870912)) {
            setCompletion(-536870912);
        }
    }

    private void doAwaitDone() {
        while (this.status >= 0) {
            try {
                synchronized (this) {
                    if (this.status >= 0) {
                        wait();
                    }
                }
            } catch (InterruptedException e) {
                onInterruptedWait();
                return;
            }
        }
    }

    private void doAwaitDone(long startTime, long nanos) {
        synchronized (this) {
            while (this.status >= 0) {
                try {
                    long nt = (nanos - System.nanoTime()) - startTime;
                    if (nt > 0) {
                        wait(nt / 1000000, (int) (nt % 1000000));
                    }
                } catch (InterruptedException e) {
                    onInterruptedWait();
                }
            }
        }
    }

    private int awaitDone(ForkJoinWorkerThread w, boolean maintainParallelism) {
        int s;
        ForkJoinPool pool = w == null ? null : w.pool;
        while (true) {
            s = this.status;
            if (s < 0) {
                break;
            }
            if (casStatus(s, pool == null ? 32768 | s : s + 1)) {
                if (pool == null || !pool.preJoin(this, maintainParallelism)) {
                    doAwaitDone();
                }
                s = this.status;
                if ((s & 32767) != 0) {
                    adjustPoolCountsOnUnblock(pool);
                }
            }
        }
        return s;
    }

    private int awaitDone(ForkJoinWorkerThread w, long nanos) {
        int s;
        ForkJoinPool pool = w == null ? null : w.pool;
        while (true) {
            s = this.status;
            if (s < 0) {
                break;
            }
            if (casStatus(s, pool == null ? 32768 | s : s + 1)) {
                long startTime = System.nanoTime();
                if (pool == null || !pool.preJoin(this, false)) {
                    doAwaitDone(startTime, nanos);
                }
                s = this.status;
                if (s >= 0) {
                    adjustPoolCountsOnCancelledWait(pool);
                    s = this.status;
                }
                if (s < 0 && (s & 32767) != 0) {
                    adjustPoolCountsOnUnblock(pool);
                }
            }
        }
        return s;
    }

    private void adjustPoolCountsOnUnblock(ForkJoinPool pool) {
        int s;
        int s2;
        do {
            s = this.status;
            if (s >= 0) {
                break;
            }
        } while (!casStatus(s, -536870912 & s));
        if (pool != null && (s2 = s & 32767) != 0) {
            pool.updateRunningCount(s2);
        }
    }

    private void adjustPoolCountsOnCancelledWait(ForkJoinPool pool) {
        int s;
        if (pool != null) {
            do {
                s = this.status;
                if (s < 0 || (s & 32767) == 0) {
                    return;
                }
            } while (!casStatus(s, s - 1));
            pool.updateRunningCount(1);
        }
    }

    private void onInterruptedWait() {
        ForkJoinWorkerThread w = getWorker();
        if (w == null) {
            Thread.currentThread().interrupt();
        } else if (w.isTerminating()) {
            cancelIgnoringExceptions();
        }
    }

    private void setDoneExceptionally(Throwable rex) {
        exceptionMap.put(this, rex);
        setCompletion(-1610612736);
    }

    private void reportException(int s) {
        int s2 = s & -536870912;
        if (s2 >= -536870912) {
            return;
        }
        if (s2 == -1073741824) {
            throw new CancellationException();
        }
        rethrowException(exceptionMap.get(this));
    }

    private V reportFutureResult() throws ExecutionException, InterruptedException {
        Throwable ex;
        int s = this.status & -536870912;
        if (s < -536870912) {
            if (s == -1073741824) {
                throw new CancellationException();
            } else if (s == -1610612736 && (ex = exceptionMap.get(this)) != null) {
                throw new ExecutionException(ex);
            } else if (Thread.interrupted()) {
                throw new InterruptedException();
            }
        }
        return getRawResult();
    }

    private V reportTimedFutureResult() throws InterruptedException, ExecutionException, TimeoutException {
        Throwable ex;
        int s = this.status & -536870912;
        if (s == -536870912) {
            return getRawResult();
        }
        if (s == -1073741824) {
            throw new CancellationException();
        } else if (s == -1610612736 && (ex = exceptionMap.get(this)) != null) {
            throw new ExecutionException(ex);
        } else if (Thread.interrupted()) {
            throw new InterruptedException();
        } else {
            throw new TimeoutException();
        }
    }

    private boolean tryExec() {
        try {
            if (!exec()) {
                return false;
            }
            setNormalCompletion();
            return true;
        } catch (Throwable rex) {
            setDoneExceptionally(rex);
            rethrowException(rex);
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public final void quietlyExec() {
        if (this.status >= 0) {
            try {
                if (exec()) {
                    setNormalCompletion();
                }
            } catch (Throwable rex) {
                setDoneExceptionally(rex);
            }
        }
    }

    private boolean tryQuietlyInvoke() {
        try {
            if (!exec()) {
                return false;
            }
            setNormalCompletion();
            return true;
        } catch (Throwable rex) {
            setDoneExceptionally(rex);
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public final void cancelIgnoringExceptions() {
        try {
            cancel(false);
        } catch (Throwable th) {
        }
    }

    public final void fork() {
        ((ForkJoinWorkerThread) Thread.currentThread()).pushTask(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: scala.concurrent.forkjoin.ForkJoinTask.awaitDone(scala.concurrent.forkjoin.ForkJoinWorkerThread, boolean):int
     arg types: [scala.concurrent.forkjoin.ForkJoinWorkerThread, int]
     candidates:
      scala.concurrent.forkjoin.ForkJoinTask.awaitDone(scala.concurrent.forkjoin.ForkJoinWorkerThread, long):int
      scala.concurrent.forkjoin.ForkJoinTask.awaitDone(scala.concurrent.forkjoin.ForkJoinWorkerThread, boolean):int */
    public final V join() {
        ForkJoinWorkerThread w = getWorker();
        if (w == null || this.status < 0 || !w.unpushTask(this) || !tryExec()) {
            reportException(awaitDone(w, true));
        }
        return getRawResult();
    }

    public final V invoke() {
        if (this.status < 0 || !tryExec()) {
            return join();
        }
        return getRawResult();
    }

    public final boolean isDone() {
        return this.status < 0;
    }

    public final boolean isCancelled() {
        return (this.status & -536870912) == -1073741824;
    }

    public boolean cancel(boolean mayInterruptIfRunning) {
        setCompletion(-1073741824);
        return (this.status & -536870912) == -1073741824;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: scala.concurrent.forkjoin.ForkJoinTask.awaitDone(scala.concurrent.forkjoin.ForkJoinWorkerThread, boolean):int
     arg types: [scala.concurrent.forkjoin.ForkJoinWorkerThread, int]
     candidates:
      scala.concurrent.forkjoin.ForkJoinTask.awaitDone(scala.concurrent.forkjoin.ForkJoinWorkerThread, long):int
      scala.concurrent.forkjoin.ForkJoinTask.awaitDone(scala.concurrent.forkjoin.ForkJoinWorkerThread, boolean):int */
    public final V get() throws InterruptedException, ExecutionException {
        ForkJoinWorkerThread w = getWorker();
        if (w == null || this.status < 0 || !w.unpushTask(this) || !tryQuietlyInvoke()) {
            awaitDone(w, true);
        }
        return reportFutureResult();
    }

    public final V get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
        ForkJoinWorkerThread w = getWorker();
        if (w == null || this.status < 0 || !w.unpushTask(this) || !tryQuietlyInvoke()) {
            awaitDone(w, unit.toNanos(timeout));
        }
        return reportTimedFutureResult();
    }

    public static ForkJoinPool getPool() {
        Thread t = Thread.currentThread();
        if (t instanceof ForkJoinWorkerThread) {
            return ((ForkJoinWorkerThread) t).pool;
        }
        return null;
    }

    private static Unsafe getUnsafe() throws Throwable {
        try {
            return Unsafe.getUnsafe();
        } catch (SecurityException e) {
            try {
                return (Unsafe) AccessController.doPrivileged(new PrivilegedExceptionAction<Unsafe>() {
                    public Unsafe run() throws Exception {
                        return ForkJoinTask.getUnsafePrivileged();
                    }
                });
            } catch (PrivilegedActionException e2) {
                throw e2.getCause();
            }
        }
    }

    /* access modifiers changed from: private */
    public static Unsafe getUnsafePrivileged() throws NoSuchFieldException, IllegalAccessException {
        Field declaredField = Unsafe.class.getDeclaredField("theUnsafe");
        declaredField.setAccessible(true);
        return (Unsafe) declaredField.get(null);
    }

    private static long fieldOffset(String str) throws NoSuchFieldException, Throwable {
        return getUnsafe().objectFieldOffset(ForkJoinTask.class.getDeclaredField(str));
    }
}
