package scala.concurrent.forkjoin;

import java.lang.reflect.Field;
import java.security.AccessController;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.util.Collection;
import java.util.concurrent.RejectedExecutionException;
import sun.misc.Unsafe;

public class ForkJoinWorkerThread extends Thread {
    static final Unsafe _unsafe;
    static final long baseOffset;
    static final long qBase;
    static final int qShift;
    static final long runStateOffset;
    static final long spOffset;
    private boolean active;
    private volatile int base;
    long lastEventCount;
    private boolean locallyFifo;
    final ForkJoinPool pool;
    int poolIndex;
    private ForkJoinTask<?>[] queue;
    private volatile int runState;
    private int seed;
    private volatile int sp;
    private int stealCount;

    protected ForkJoinWorkerThread(ForkJoinPool pool2) {
        if (pool2 == null) {
            throw new NullPointerException();
        }
        this.pool = pool2;
    }

    /* access modifiers changed from: package-private */
    public void setAsyncMode(boolean async) {
        this.locallyFifo = async;
    }

    /* access modifiers changed from: package-private */
    public final boolean isShutdown() {
        return this.runState >= 1;
    }

    /* access modifiers changed from: package-private */
    public final boolean isTerminating() {
        return this.runState >= 2;
    }

    /* access modifiers changed from: package-private */
    public final boolean isTerminated() {
        return this.runState == 3;
    }

    /* access modifiers changed from: package-private */
    public final boolean shutdown() {
        return transitionRunStateTo(1);
    }

    /* access modifiers changed from: package-private */
    public final boolean shutdownNow() {
        return transitionRunStateTo(2);
    }

    private boolean transitionRunStateTo(int state) {
        int s;
        do {
            s = this.runState;
            if (s >= state) {
                return false;
            }
        } while (!_unsafe.compareAndSwapInt(this, runStateOffset, s, state));
        return true;
    }

    private boolean tryActivate() {
        if (!this.active) {
            if (!this.pool.tryIncrementActiveCount()) {
                return false;
            }
            this.active = true;
        }
        return true;
    }

    private boolean tryInactivate() {
        if (this.active) {
            if (!this.pool.tryDecrementActiveCount()) {
                return false;
            }
            this.active = false;
        }
        return true;
    }

    private static int xorShift(int r) {
        int r2 = r ^ (r << 1);
        int r3 = r2 ^ (r2 >>> 3);
        return r3 ^ (r3 << 10);
    }

    public void run() {
        Throwable exception = null;
        try {
            onStart();
            this.pool.sync(this);
            mainLoop();
        } catch (Throwable th) {
            exception = th;
        } finally {
            onTermination(exception);
        }
    }

    private void mainLoop() {
        while (!isShutdown()) {
            ForkJoinTask<?> t = pollTask();
            if (t != null || (t = pollSubmission()) != null) {
                t.quietlyExec();
            } else if (tryInactivate()) {
                this.pool.sync(this);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        this.queue = new ForkJoinTask[8192];
        int p = this.poolIndex + 1;
        this.seed = (p << 8) + p + (p << 16) + (p << 24);
    }

    /* access modifiers changed from: protected */
    public void onTermination(Throwable exception) {
        while (exception == null && !this.pool.isTerminating() && this.base != this.sp) {
            try {
                ForkJoinTask<?> t = popTask();
                if (t != null) {
                    t.quietlyExec();
                }
            } catch (Throwable th) {
                exception = th;
            }
        }
        do {
            try {
            } catch (Throwable th2) {
                if (exception != null) {
                    ForkJoinTask.rethrowException(exception);
                }
                throw th2;
            }
        } while (!tryInactivate());
        cancelTasks();
        this.runState = 3;
        this.pool.workerTerminated(this);
        if (exception != null) {
            ForkJoinTask.rethrowException(exception);
        }
    }

    private static void setSlot(ForkJoinTask<?>[] q, int i, ForkJoinTask<?> t) {
        _unsafe.putOrderedObject(q, ((long) (i << qShift)) + qBase, t);
    }

    private static boolean casSlotNull(ForkJoinTask<?>[] q, int i, ForkJoinTask<?> t) {
        return _unsafe.compareAndSwapObject(q, ((long) (i << qShift)) + qBase, t, (Object) null);
    }

    private void storeSp(int s) {
        _unsafe.putOrderedInt(this, spOffset, s);
    }

    /* access modifiers changed from: package-private */
    public final void pushTask(ForkJoinTask<?> t) {
        ForkJoinTask<?>[] q = this.queue;
        int mask = q.length - 1;
        int s = this.sp;
        setSlot(q, s & mask, t);
        int s2 = s + 1;
        storeSp(s2);
        int s3 = s2 - this.base;
        if (s3 == 1) {
            this.pool.signalWork();
        } else if (s3 >= mask) {
            growQueue();
        }
    }

    /* access modifiers changed from: package-private */
    public final ForkJoinTask<?> deqTask() {
        ForkJoinTask<?>[] q;
        int i;
        ForkJoinTask<?> t;
        int i2 = this.sp;
        int b = this.base;
        if (i2 == b || (q = this.queue) == null || (t = q[(i = (q.length - 1) & b)]) == null || !casSlotNull(q, i, t)) {
            return null;
        }
        this.base = b + 1;
        return t;
    }

    /* access modifiers changed from: package-private */
    public final ForkJoinTask<?> popTask() {
        int s = this.sp;
        while (true) {
            if (s == this.base) {
                break;
            } else if (tryActivate()) {
                ForkJoinTask<?>[] q = this.queue;
                int i = (s - 1) & (q.length - 1);
                ForkJoinTask<?> t = q[i];
                if (t != null && casSlotNull(q, i, t)) {
                    storeSp(s - 1);
                    return t;
                }
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public final boolean unpushTask(ForkJoinTask<?> t) {
        ForkJoinTask<?>[] q = this.queue;
        int mask = q.length - 1;
        int s = this.sp - 1;
        if (!casSlotNull(q, s & mask, t)) {
            return false;
        }
        storeSp(s);
        return true;
    }

    private void growQueue() {
        ForkJoinTask<?>[] oldQ = this.queue;
        int oldSize = oldQ.length;
        int newSize = oldSize << 1;
        if (newSize > 268435456) {
            throw new RejectedExecutionException("Queue capacity exceeded");
        }
        ForkJoinTask<?>[] newQ = new ForkJoinTask[newSize];
        this.queue = newQ;
        int b = this.base;
        int bf = b + oldSize;
        int oldMask = oldSize - 1;
        int newMask = newSize - 1;
        do {
            int oldIndex = b & oldMask;
            ForkJoinTask<?> t = oldQ[oldIndex];
            if (t != null && !casSlotNull(oldQ, oldIndex, t)) {
                t = null;
            }
            setSlot(newQ, b & newMask, t);
            b++;
        } while (b != bf);
        this.pool.signalWork();
    }

    private ForkJoinTask<?> scan() {
        int mask;
        ForkJoinTask<?> t = null;
        int r = this.seed;
        do {
            ForkJoinWorkerThread[] ws = this.pool.workers;
            if (ws != null && (mask = ws.length - 1) > 0) {
                int idx = r;
                int probes = mask ^ -1;
                while (true) {
                    r = xorShift(r);
                    ForkJoinWorkerThread v = ws[mask & idx];
                    if (v == null || v.sp == v.base) {
                        if (probes > mask) {
                            break;
                        }
                        int probes2 = probes + 1;
                        idx = probes < 0 ? r : idx + 1;
                        probes = probes2;
                    } else if (tryActivate() && (t = v.deqTask()) != null) {
                        break;
                    }
                }
            }
        } while (this.pool.hasNewSyncEvent(this));
        this.seed = r;
        return t;
    }

    /* access modifiers changed from: package-private */
    public final ForkJoinTask<?> pollTask() {
        ForkJoinTask<?> t = this.locallyFifo ? deqTask() : popTask();
        if (t == null && (t = scan()) != null) {
            this.stealCount++;
        }
        return t;
    }

    private ForkJoinTask<?> pollSubmission() {
        ForkJoinTask<?> t;
        ForkJoinPool p = this.pool;
        while (p.hasQueuedSubmissions()) {
            if (tryActivate() && (t = p.pollSubmission()) != null) {
                return t;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public final void cancelTasks() {
        ForkJoinTask<?> t;
        while (this.base != this.sp && (t = deqTask()) != null) {
            t.cancelIgnoringExceptions();
        }
    }

    /* access modifiers changed from: package-private */
    public final int drainTasksTo(Collection<ForkJoinTask<?>> c) {
        ForkJoinTask<?> t;
        int n = 0;
        while (this.base != this.sp && (t = deqTask()) != null) {
            c.add(t);
            n++;
        }
        return n;
    }

    /* access modifiers changed from: package-private */
    public final int getAndClearStealCount() {
        int sc = this.stealCount;
        this.stealCount = 0;
        return sc;
    }

    static boolean hasQueuedTasks(ForkJoinWorkerThread[] ws) {
        if (ws != null) {
            for (int j = 0; j < 2; j++) {
                for (ForkJoinWorkerThread w : ws) {
                    if (w != null && w.sp != w.base) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public final int getQueueSize() {
        int n = this.sp - this.base;
        if (n < 0) {
            return 0;
        }
        return n;
    }

    private static Unsafe getUnsafe() throws Throwable {
        try {
            return Unsafe.getUnsafe();
        } catch (SecurityException e) {
            try {
                return (Unsafe) AccessController.doPrivileged(new PrivilegedExceptionAction<Unsafe>() {
                    public Unsafe run() throws Exception {
                        return ForkJoinWorkerThread.getUnsafePrivileged();
                    }
                });
            } catch (PrivilegedActionException e2) {
                throw e2.getCause();
            }
        }
    }

    /* access modifiers changed from: private */
    public static Unsafe getUnsafePrivileged() throws NoSuchFieldException, IllegalAccessException {
        Field declaredField = Unsafe.class.getDeclaredField("theUnsafe");
        declaredField.setAccessible(true);
        return (Unsafe) declaredField.get(null);
    }

    private static long fieldOffset(String str) throws NoSuchFieldException {
        return _unsafe.objectFieldOffset(ForkJoinWorkerThread.class.getDeclaredField(str));
    }

    static {
        try {
            _unsafe = getUnsafe();
            baseOffset = fieldOffset("base");
            spOffset = fieldOffset("sp");
            runStateOffset = fieldOffset("runState");
            qBase = (long) _unsafe.arrayBaseOffset(ForkJoinTask[].class);
            int arrayIndexScale = _unsafe.arrayIndexScale(ForkJoinTask[].class);
            if (((arrayIndexScale - 1) & arrayIndexScale) != 0) {
                throw new Error("data type scale not a power of two");
            }
            qShift = 31 - Integer.numberOfLeadingZeros(arrayIndexScale);
        } catch (Throwable th) {
            throw new RuntimeException("Could not initialize intrinsics", th);
        }
    }
}
