package scala.concurrent.forkjoin;

public abstract class RecursiveAction extends ForkJoinTask<Void> {
    /* access modifiers changed from: protected */
    public abstract void compute();

    public final Void getRawResult() {
        return null;
    }

    /* access modifiers changed from: protected */
    public final boolean exec() {
        compute();
        return true;
    }
}
