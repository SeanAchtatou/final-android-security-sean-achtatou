package scala.concurrent.forkjoin;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.security.AccessController;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.util.AbstractQueue;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.concurrent.locks.LockSupport;
import sun.misc.Unsafe;

public class LinkedTransferQueue<E> extends AbstractQueue<E> implements TransferQueue<E>, Serializable {
    static final int NCPUS = Runtime.getRuntime().availableProcessors();
    private static final Unsafe _unsafe;
    private static final long cleanMeOffset;
    private static final long headOffset;
    static final int maxTimedSpins = (NCPUS < 2 ? 0 : 32);
    static final int maxUntimedSpins = (maxTimedSpins * 16);
    private static final long tailOffset;
    private final transient PaddedAtomicReference<QNode> cleanMe = new PaddedAtomicReference<>(null);
    private final transient PaddedAtomicReference<QNode> head;
    private final transient PaddedAtomicReference<QNode> tail;

    static {
        try {
            _unsafe = getUnsafe();
            headOffset = fieldOffset("head");
            tailOffset = fieldOffset("tail");
            cleanMeOffset = fieldOffset("cleanMe");
        } catch (Throwable th) {
            throw new RuntimeException("Could not initialize intrinsics", th);
        }
    }

    static final class QNode extends AtomicReference<Object> {
        static final AtomicReferenceFieldUpdater<QNode, QNode> nextUpdater = AtomicReferenceFieldUpdater.newUpdater(QNode.class, QNode.class, "next");
        final boolean isData;
        volatile QNode next;
        volatile Thread waiter;

        QNode(Object item, boolean isData2) {
            super(item);
            this.isData = isData2;
        }

        /* access modifiers changed from: package-private */
        public final boolean casNext(QNode cmp, QNode val) {
            return nextUpdater.compareAndSet(this, cmp, val);
        }

        /* access modifiers changed from: package-private */
        public final void clearNext() {
            nextUpdater.lazySet(this, this);
        }
    }

    static final class PaddedAtomicReference<T> extends AtomicReference<T> {
        PaddedAtomicReference(T r) {
            super(r);
        }
    }

    private boolean advanceHead(QNode h, QNode nh) {
        if (h != this.head.get() || !this.head.compareAndSet(h, nh)) {
            return false;
        }
        h.clearNext();
        return true;
    }

    private Object xfer(Object e, int mode, long nanos) {
        boolean isData;
        Object x;
        if (e != null) {
            isData = true;
        } else {
            isData = false;
        }
        QNode s = null;
        PaddedAtomicReference<QNode> head2 = this.head;
        PaddedAtomicReference<QNode> tail2 = this.tail;
        while (true) {
            QNode t = (QNode) tail2.get();
            QNode h = (QNode) head2.get();
            if (t != null && (t == h || t.isData == isData)) {
                if (s == null) {
                    s = new QNode(e, isData);
                }
                QNode last = t.next;
                if (last != null) {
                    if (t == tail2.get()) {
                        tail2.compareAndSet(t, last);
                    }
                } else if (t.casNext(null, s)) {
                    tail2.compareAndSet(t, s);
                    return awaitFulfill(t, s, e, mode, nanos);
                }
            } else if (h != null) {
                QNode first = h.next;
                if (t == tail2.get() && first != null && advanceHead(h, first) && (x = first.get()) != first && first.compareAndSet(x, e)) {
                    LockSupport.unpark(first.waiter);
                    return isData ? e : x;
                }
            } else {
                continue;
            }
        }
    }

    private Object fulfill(Object e) {
        boolean isData;
        Object x;
        if (e != null) {
            isData = true;
        } else {
            isData = false;
        }
        PaddedAtomicReference<QNode> head2 = this.head;
        PaddedAtomicReference<QNode> tail2 = this.tail;
        while (true) {
            QNode t = (QNode) tail2.get();
            QNode h = (QNode) head2.get();
            if (t != null && (t == h || t.isData == isData)) {
                QNode last = t.next;
                if (t != tail2.get()) {
                    continue;
                } else if (last == null) {
                    return null;
                } else {
                    tail2.compareAndSet(t, last);
                }
            } else if (h != null) {
                QNode first = h.next;
                if (t == tail2.get() && first != null && advanceHead(h, first) && (x = first.get()) != first && first.compareAndSet(x, e)) {
                    LockSupport.unpark(first.waiter);
                    return isData ? e : x;
                }
            } else {
                continue;
            }
        }
    }

    private Object awaitFulfill(QNode pred, QNode s, Object e, int mode, long nanos) {
        long lastTime;
        Object x;
        if (mode == 0) {
            return null;
        }
        if (mode == 1) {
            lastTime = System.nanoTime();
        } else {
            lastTime = 0;
        }
        Thread w = Thread.currentThread();
        int spins = -1;
        while (true) {
            if (w.isInterrupted()) {
                s.compareAndSet(e, s);
            }
            x = s.get();
            if (x != e) {
                break;
            }
            if (mode == 1) {
                long now = System.nanoTime();
                nanos -= now - lastTime;
                lastTime = now;
                if (nanos <= 0) {
                    s.compareAndSet(e, s);
                }
            }
            if (spins < 0) {
                QNode h = (QNode) this.head.get();
                spins = (h == null || h.next != s) ? 0 : mode == 1 ? maxTimedSpins : maxUntimedSpins;
            }
            if (spins > 0) {
                spins--;
            } else if (s.waiter == null) {
                s.waiter = w;
            } else if (mode != 1) {
                LockSupport.park(this);
                s.waiter = null;
                spins = -1;
            } else if (nanos > 1000) {
                LockSupport.parkNanos(this, nanos);
                s.waiter = null;
                spins = -1;
            }
        }
        advanceHead(pred, s);
        if (x == s) {
            clean(pred, s);
            return null;
        } else if (x == null) {
            return e;
        } else {
            s.set(s);
            return x;
        }
    }

    private QNode getValidatedTail() {
        while (true) {
            QNode h = (QNode) this.head.get();
            QNode first = h.next;
            if (first == null || first.next != first) {
                QNode t = (QNode) this.tail.get();
                QNode last = t.next;
                if (t != this.tail.get()) {
                    continue;
                } else if (last == null) {
                    return t;
                } else {
                    this.tail.compareAndSet(t, last);
                }
            } else {
                advanceHead(h, first);
            }
        }
    }

    /* access modifiers changed from: private */
    public void clean(QNode pred, QNode s) {
        Thread w = s.waiter;
        if (w != null) {
            s.waiter = null;
            if (w != Thread.currentThread()) {
                LockSupport.unpark(w);
            }
        }
        if (pred != null) {
            while (pred.next == s) {
                QNode oldpred = reclean();
                if (s != getValidatedTail()) {
                    QNode sn = s.next;
                    if (sn == s || pred.casNext(s, sn)) {
                        return;
                    }
                } else if (oldpred == pred) {
                    return;
                } else {
                    if (oldpred == null && this.cleanMe.compareAndSet(null, pred)) {
                        return;
                    }
                }
            }
        }
    }

    private QNode reclean() {
        QNode pred;
        QNode s;
        QNode sn;
        while (true) {
            pred = (QNode) this.cleanMe.get();
            if (pred == null || (s = pred.next) == getValidatedTail()) {
                return pred;
            }
            if (s == null || s == pred || s.get() != s || (sn = s.next) == s || pred.casNext(s, sn)) {
                this.cleanMe.compareAndSet(pred, null);
            }
        }
        return pred;
    }

    public LinkedTransferQueue() {
        QNode dummy = new QNode(null, false);
        this.head = new PaddedAtomicReference<>(dummy);
        this.tail = new PaddedAtomicReference<>(dummy);
    }

    public void put(E e) throws InterruptedException {
        if (e == null) {
            throw new NullPointerException();
        } else if (Thread.interrupted()) {
            throw new InterruptedException();
        } else {
            xfer(e, 0, 0);
        }
    }

    public boolean offer(E e, long timeout, TimeUnit unit) throws InterruptedException {
        if (e == null) {
            throw new NullPointerException();
        } else if (Thread.interrupted()) {
            throw new InterruptedException();
        } else {
            xfer(e, 0, 0);
            return true;
        }
    }

    public boolean offer(E e) {
        if (e == null) {
            throw new NullPointerException();
        }
        xfer(e, 0, 0);
        return true;
    }

    public boolean add(E e) {
        if (e == null) {
            throw new NullPointerException();
        }
        xfer(e, 0, 0);
        return true;
    }

    public E take() throws InterruptedException {
        Object e = xfer(null, 2, 0);
        if (e != null) {
            return e;
        }
        Thread.interrupted();
        throw new InterruptedException();
    }

    public E poll(long timeout, TimeUnit unit) throws InterruptedException {
        Object e = xfer(null, 1, unit.toNanos(timeout));
        if (e != null || !Thread.interrupted()) {
            return e;
        }
        throw new InterruptedException();
    }

    public E poll() {
        return fulfill(null);
    }

    public int drainTo(Collection<? super E> c) {
        if (c == null) {
            throw new NullPointerException();
        } else if (c == this) {
            throw new IllegalArgumentException();
        } else {
            int n = 0;
            while (true) {
                E e = poll();
                if (e == null) {
                    return n;
                }
                c.add(e);
                n++;
            }
        }
    }

    public int drainTo(Collection<? super E> c, int maxElements) {
        if (c == null) {
            throw new NullPointerException();
        } else if (c == this) {
            throw new IllegalArgumentException();
        } else {
            int n = 0;
            while (n < maxElements) {
                E e = poll();
                if (e == null) {
                    break;
                }
                c.add(e);
                n++;
            }
            return n;
        }
    }

    /* access modifiers changed from: private */
    public QNode traversalHead() {
        QNode h;
        while (true) {
            QNode t = (QNode) this.tail.get();
            h = (QNode) this.head.get();
            if (!(h == null || t == null)) {
                QNode last = t.next;
                QNode first = h.next;
                if (t != this.tail.get()) {
                    continue;
                } else if (last != null) {
                    this.tail.compareAndSet(t, last);
                } else if (first == null || first.get() != first) {
                    return h;
                } else {
                    advanceHead(h, first);
                }
            }
            reclean();
        }
        return h;
    }

    public Iterator<E> iterator() {
        return new Itr();
    }

    class Itr implements Iterator<E> {
        QNode curr;
        QNode next;
        E nextItem;
        QNode pcurr;
        QNode pnext;
        QNode snext;

        Itr() {
            findNext();
        }

        /* access modifiers changed from: package-private */
        public void findNext() {
            while (true) {
                QNode pred = this.pnext;
                QNode q = this.next;
                if (pred == null || pred == q) {
                    pred = LinkedTransferQueue.this.traversalHead();
                    q = pred.next;
                }
                if (q == null || !q.isData) {
                    this.next = null;
                } else {
                    Object x = q.get();
                    QNode s = q.next;
                    if (x == null || q == x || q == s) {
                        this.pnext = q;
                        this.next = s;
                    } else {
                        this.nextItem = x;
                        this.snext = s;
                        this.pnext = pred;
                        this.next = q;
                        return;
                    }
                }
            }
            this.next = null;
        }

        public boolean hasNext() {
            return this.next != null;
        }

        public E next() {
            if (this.next == null) {
                throw new NoSuchElementException();
            }
            this.pcurr = this.pnext;
            this.curr = this.next;
            this.pnext = this.next;
            this.next = this.snext;
            E x = this.nextItem;
            findNext();
            return x;
        }

        public void remove() {
            QNode p = this.curr;
            if (p == null) {
                throw new IllegalStateException();
            }
            Object x = p.get();
            if (x != null && x != p && p.compareAndSet(x, p)) {
                LinkedTransferQueue.this.clean(this.pcurr, p);
            }
        }
    }

    public E peek() {
        while (true) {
            QNode p = traversalHead().next;
            if (p == null) {
                return null;
            }
            Object x = p.get();
            if (p != x) {
                if (!p.isData) {
                    return null;
                }
                if (x != null) {
                    return x;
                }
            }
        }
    }

    public boolean isEmpty() {
        while (true) {
            QNode p = traversalHead().next;
            if (p == null) {
                return true;
            }
            Object x = p.get();
            if (p != x) {
                if (!p.isData) {
                    return true;
                }
                if (x != null) {
                    return false;
                }
            }
        }
    }

    public int size() {
        int count = 0;
        QNode p = traversalHead().next;
        while (p != null && p.isData) {
            Object x = p.get();
            if (x != null && x != p && (count = count + 1) == Integer.MAX_VALUE) {
                break;
            }
            p = p.next;
        }
        return count;
    }

    public int remainingCapacity() {
        return Integer.MAX_VALUE;
    }

    public boolean remove(Object o) {
        if (o == null) {
            return false;
        }
        loop0:
        while (true) {
            QNode pred = traversalHead();
            while (true) {
                QNode q = pred.next;
                if (q != null && q.isData) {
                    if (q != pred) {
                        Object x = q.get();
                        if (x == null || x == q || !o.equals(x) || !q.compareAndSet(x, q)) {
                            pred = q;
                        } else {
                            clean(pred, q);
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    private static Unsafe getUnsafe() throws Throwable {
        try {
            return Unsafe.getUnsafe();
        } catch (SecurityException e) {
            try {
                return (Unsafe) AccessController.doPrivileged(new PrivilegedExceptionAction<Unsafe>() {
                    public Unsafe run() throws Exception {
                        return LinkedTransferQueue.getUnsafePrivileged();
                    }
                });
            } catch (PrivilegedActionException e2) {
                throw e2.getCause();
            }
        }
    }

    /* access modifiers changed from: private */
    public static Unsafe getUnsafePrivileged() throws NoSuchFieldException, IllegalAccessException {
        Field declaredField = Unsafe.class.getDeclaredField("theUnsafe");
        declaredField.setAccessible(true);
        return (Unsafe) declaredField.get(null);
    }

    private static long fieldOffset(String str) throws NoSuchFieldException {
        return _unsafe.objectFieldOffset(LinkedTransferQueue.class.getDeclaredField(str));
    }
}
