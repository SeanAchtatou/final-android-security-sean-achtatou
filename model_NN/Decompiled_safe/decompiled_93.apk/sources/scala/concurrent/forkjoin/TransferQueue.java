package scala.concurrent.forkjoin;

import java.util.concurrent.BlockingQueue;

public interface TransferQueue extends BlockingQueue {
}
