package scala;

import java.io.Serializable;
import java.lang.reflect.Method;
import scala.collection.mutable.ArrayOps;
import scala.runtime.AbstractFunction0;
import scala.runtime.BoxesRunTime;

/* compiled from: Enumeration.scala */
public final class Enumeration$$anonfun$scala$Enumeration$$nameOf$1 extends AbstractFunction0 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ Enumeration $outer;
    public final /* synthetic */ int i$2;

    public Enumeration$$anonfun$scala$Enumeration$$nameOf$1(Enumeration $outer2, int i) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.i$2 = i;
    }

    public final String apply() {
        Enumeration enumeration = this.$outer;
        new ArrayOps.ofRef((Object[]) ((Method[]) new ArrayOps.ofRef((Object[]) enumeration.getClass().getMethods()).filter(new Enumeration$$anonfun$1(enumeration)))).foreach(new Enumeration$$anonfun$scala$Enumeration$$populateNameMap$1(enumeration));
        return (String) this.$outer.scala$Enumeration$$nmap().apply(BoxesRunTime.boxToInteger(this.i$2));
    }
}
