package scala;

import scala.Product2$mcII$sp;

/* compiled from: Tuple2.scala */
public class Tuple2$mcII$sp extends Tuple2<Integer, Integer> implements Product2$mcII$sp {
    public final int _1$mcI$sp;
    public final int _2$mcI$sp;

    public int _1$mcI$sp() {
        return this._1$mcI$sp;
    }

    public int _2$mcI$sp() {
        return this._2$mcI$sp;
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Tuple2$mcII$sp(int _1$mcI$sp2, int _2$mcI$sp2) {
        super(null, null);
        this._1$mcI$sp = _1$mcI$sp2;
        this._2$mcI$sp = _2$mcI$sp2;
        Product2$mcII$sp.Cclass.$init$(this);
    }

    public int _1() {
        return _1$mcI$sp();
    }

    public int _2() {
        return _2$mcI$sp();
    }
}
