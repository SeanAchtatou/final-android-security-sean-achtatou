package scala;

/* compiled from: Function1.scala */
public interface Function1$mcVL$sp extends Function1<Long, Object> {
    <A> Function1<Long, A> andThen$mcVL$sp(Function1<Object, A> function1);

    void apply(long j);

    <A> Function1<A, Object> compose$mcVL$sp(Function1<A, Long> function1);

    /* renamed from: scala.Function1$mcVL$sp$class  reason: invalid class name */
    /* compiled from: Function1.scala */
    public abstract class Cclass {
        public static void $init$(Function1$mcVL$sp $this) {
        }

        public static void apply(Function1$mcVL$sp $this, long v1) {
            $this.apply$mcVL$sp(v1);
        }

        public static Function1 compose(Function1$mcVL$sp $this, Function1 g) {
            return $this.compose$mcVL$sp(g);
        }

        public static Function1 compose$mcVL$sp(Function1$mcVL$sp $this, Function1 g$15) {
            return new Function1$mcVL$sp$$anonfun$compose$mcVL$sp$1($this, g$15);
        }

        public static Function1 andThen(Function1$mcVL$sp $this, Function1 g) {
            return $this.andThen$mcVL$sp(g);
        }

        public static Function1 andThen$mcVL$sp(Function1$mcVL$sp $this, Function1 g$16) {
            return new Function1$mcVL$sp$$anonfun$andThen$mcVL$sp$1($this, g$16);
        }
    }
}
