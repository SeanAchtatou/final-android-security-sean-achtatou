package scala;

/* compiled from: Option.scala */
public final class Option$ implements ScalaObject {
    public static final Option$ MODULE$ = null;

    static {
        new Option$();
    }

    private Option$() {
        MODULE$ = this;
    }

    public <A> Option<A> apply(A x) {
        return x == null ? None$.MODULE$ : new Some(x);
    }
}
