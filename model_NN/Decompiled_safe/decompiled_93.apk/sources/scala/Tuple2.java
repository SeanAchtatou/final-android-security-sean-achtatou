package scala;

import scala.Product;
import scala.Product2;
import scala.collection.Iterator;
import scala.collection.mutable.StringBuilder;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime$;

/* compiled from: Tuple2.scala */
public class Tuple2<T1, T2> implements Product2<T1, T2>, ScalaObject, Product, ScalaObject {
    public final T1 _1;
    public final T2 _2;

    public Tuple2(T1 _12, T2 _22) {
        this._1 = _12;
        this._2 = _22;
        Product.Cclass.$init$(this);
        Product2.Cclass.$init$(this);
    }

    private final /* synthetic */ boolean gd1$1(Object obj, Object obj2) {
        Object _12 = _1();
        if (obj == _12 ? true : obj == null ? false : obj instanceof Number ? BoxesRunTime.equalsNumObject((Number) obj, _12) : obj instanceof Character ? BoxesRunTime.equalsCharObject((Character) obj, _12) : obj.equals(_12)) {
            Object _22 = _2();
            if (obj2 == _22 ? true : obj2 == null ? false : obj2 instanceof Number ? BoxesRunTime.equalsNumObject((Number) obj2, _22) : obj2 instanceof Character ? BoxesRunTime.equalsCharObject((Character) obj2, _22) : obj2.equals(_22)) {
                return true;
            }
        }
        return false;
    }

    public T1 _1() {
        return this._1;
    }

    public int _1$mcI$sp() {
        return BoxesRunTime.unboxToInt(_1());
    }

    public T2 _2() {
        return this._2;
    }

    public int _2$mcI$sp() {
        return BoxesRunTime.unboxToInt(_2());
    }

    public boolean canEqual(Object obj) {
        return obj instanceof Tuple2;
    }

    public boolean equals(Object obj) {
        boolean z;
        if (this != obj) {
            if (obj instanceof Tuple2) {
                Tuple2 tuple2 = (Tuple2) obj;
                z = gd1$1(tuple2._1(), tuple2._2()) ? ((Tuple2) obj).canEqual(this) : false;
            } else {
                z = false;
            }
            if (!z) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return ScalaRunTime$.MODULE$._hashCode(this);
    }

    public int productArity() {
        return Product2.Cclass.productArity(this);
    }

    public Object productElement(int n) throws IndexOutOfBoundsException {
        return Product2.Cclass.productElement(this, n);
    }

    public Iterator<Object> productIterator() {
        return Product.Cclass.productIterator(this);
    }

    public String productPrefix() {
        return "Tuple2";
    }

    public String toString() {
        return new StringBuilder().append((Object) "(").append(_1()).append((Object) ",").append(_2()).append((Object) ")").toString();
    }
}
