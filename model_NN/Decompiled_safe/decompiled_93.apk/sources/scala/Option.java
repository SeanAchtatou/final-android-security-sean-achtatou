package scala;

import scala.Product;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;
import scala.runtime.Null$;

/* compiled from: Option.scala */
public abstract class Option<A> implements Product, ScalaObject {
    public abstract A get();

    public abstract boolean isEmpty();

    public Option() {
        Product.Cclass.$init$(this);
    }

    public Iterator<Object> productIterator() {
        return Product.Cclass.productIterator(this);
    }

    public String productPrefix() {
        return Product.Cclass.productPrefix(this);
    }

    public <B> B getOrElse(Function0<B> function0) {
        return isEmpty() ? function0.apply() : get();
    }

    public <A1> A1 orNull(Predef$$less$colon$less<Null$, A1> ev$1) {
        return getOrElse(new Option$$anonfun$orNull$1(this, ev$1));
    }

    public <B> Option<B> map(Function1<A, B> f) {
        return isEmpty() ? None$.MODULE$ : new Some(f.apply(get()));
    }

    public boolean exists(Function1<A, Boolean> p) {
        return !isEmpty() && BoxesRunTime.unboxToBoolean(p.apply(get()));
    }
}
