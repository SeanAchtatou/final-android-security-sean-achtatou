package scala;

import java.io.Serializable;
import scala.runtime.AbstractFunction0;
import scala.runtime.BoxesRunTime;

/* compiled from: Enumeration.scala */
public final class Enumeration$Val$$anonfun$$init$$1 extends AbstractFunction0 implements Serializable {
    public static final long serialVersionUID = 0;
    public final /* synthetic */ int i$1;

    public Enumeration$Val$$anonfun$$init$$1(Enumeration $outer, int i) {
        this.i$1 = i;
    }

    public final String apply() {
        return BoxesRunTime.boxToInteger(this.i$1).toString();
    }
}
