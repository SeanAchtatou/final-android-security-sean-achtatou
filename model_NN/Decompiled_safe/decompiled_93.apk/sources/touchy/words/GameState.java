package touchy.words;

import java.util.Random;
import scala.collection.Seq;
import scala.collection.immutable.List;
import scala.collection.immutable.Map;
import scala.collection.mutable.Stack;

/* compiled from: Data.scala */
public final class GameState {
    public static final int ABOUT() {
        return GameState$.MODULE$.ABOUT();
    }

    public static final int PROMPT_NAME() {
        return GameState$.MODULE$.PROMPT_NAME();
    }

    public static final int TIPS() {
        return GameState$.MODULE$.TIPS();
    }

    public static final String TIPS_MSG() {
        return GameState$.MODULE$.TIPS_MSG();
    }

    public static final int TRY_AGAIN() {
        return GameState$.MODULE$.TRY_AGAIN();
    }

    public static final int YES_NO() {
        return GameState$.MODULE$.YES_NO();
    }

    public static final void addBonuses(String str, int i) {
        GameState$.MODULE$.addBonuses(str, i);
    }

    public static final String[] bonuses() {
        return GameState$.MODULE$.bonuses();
    }

    public static final void bonuses_$eq(String[] strArr) {
        GameState$.MODULE$.bonuses_$eq(strArr);
    }

    public static final void buildBonuses() {
        GameState$.MODULE$.buildBonuses();
    }

    public static final boolean debugging() {
        return GameState$.MODULE$.debugging();
    }

    public static final boolean free() {
        return GameState$.MODULE$.free();
    }

    public static final int letterDuration() {
        return GameState$.MODULE$.letterDuration();
    }

    public static final Map<String, Integer> letterOccurencies() {
        return GameState$.MODULE$.letterOccurencies();
    }

    public static final int letterOccurenciesTotal() {
        return GameState$.MODULE$.letterOccurenciesTotal();
    }

    public static final Map<String, Integer> letterPrices() {
        return GameState$.MODULE$.letterPrices();
    }

    public static final int lettersPerGame() {
        return GameState$.MODULE$.lettersPerGame();
    }

    public static final List<String> lettersRandom() {
        return GameState$.MODULE$.lettersRandom();
    }

    public static final int log(Object obj) {
        return GameState$.MODULE$.log(obj);
    }

    public static final List<Integer> multipliers() {
        return GameState$.MODULE$.multipliers();
    }

    public static final int numLetters() {
        return GameState$.MODULE$.numLetters();
    }

    public static final Random random() {
        return GameState$.MODULE$.random();
    }

    public static final String seqString(Seq<String> seq) {
        return GameState$.MODULE$.seqString(seq);
    }

    public static final int unitTime() {
        return GameState$.MODULE$.unitTime();
    }

    public static final Seq<String> vowels() {
        return GameState$.MODULE$.vowels();
    }

    public static final int wordScore(Stack<Letter> stack) {
        return GameState$.MODULE$.wordScore(stack);
    }

    public static final String wordToStr(Stack<Letter> stack) {
        return GameState$.MODULE$.wordToStr(stack);
    }
}
