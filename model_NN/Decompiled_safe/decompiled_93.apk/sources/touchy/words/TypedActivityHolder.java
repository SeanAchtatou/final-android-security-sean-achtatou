package touchy.words;

import android.app.Activity;
import scala.ScalaObject;

/* compiled from: TR.scala */
public interface TypedActivityHolder extends ScalaObject {
    Activity activity();

    <T> T findView(TypedResource<T> typedResource);

    /* renamed from: touchy.words.TypedActivityHolder$class  reason: invalid class name */
    /* compiled from: TR.scala */
    public abstract class Cclass {
        public static void $init$(TypedActivityHolder $this) {
        }

        public static Object findView(TypedActivityHolder $this, TypedResource tr) {
            return $this.activity().findViewById(tr.copy$default$1());
        }
    }
}
