package touchy.words;

import android.view.View;
import scala.Function1;

/* compiled from: Activity.scala */
public final class MainActivity$$anon$1 implements View.OnClickListener {
    private final /* synthetic */ Function1 f$1;

    public MainActivity$$anon$1(MainActivity $outer, Function1 function1) {
        this.f$1 = function1;
    }

    public void onClick(View v) {
        this.f$1.apply(v);
    }
}
