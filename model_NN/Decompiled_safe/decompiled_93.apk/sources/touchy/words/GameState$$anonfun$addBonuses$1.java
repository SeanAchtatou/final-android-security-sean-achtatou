package touchy.words;

import java.io.Serializable;
import scala.runtime.AbstractFunction1$mcVI$sp;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.IntRef;

/* compiled from: Data.scala */
public final class GameState$$anonfun$addBonuses$1 extends AbstractFunction1$mcVI$sp implements Serializable {
    public static final long serialVersionUID = 0;
    private final /* synthetic */ String bonus$1;
    private final /* synthetic */ IntRef x$14;

    public GameState$$anonfun$addBonuses$1(String str, IntRef intRef) {
        this.bonus$1 = str;
        this.x$14 = intRef;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        apply(BoxesRunTime.unboxToInt(v1));
        return BoxedUnit.UNIT;
    }

    public final void apply(int _) {
        apply$mcVI$sp(_);
    }

    public void apply$mcVI$sp(int v1) {
        while (true) {
            this.x$14.elem = GameState$.MODULE$.random().nextInt(GameState$.MODULE$.lettersPerGame());
            String str = GameState$.MODULE$.bonuses()[this.x$14.elem];
            if (str == null) {
                if ("" == 0) {
                    break;
                }
            } else if (str.equals("")) {
                break;
            }
        }
        GameState$.MODULE$.bonuses()[this.x$14.elem] = this.bonus$1;
    }
}
