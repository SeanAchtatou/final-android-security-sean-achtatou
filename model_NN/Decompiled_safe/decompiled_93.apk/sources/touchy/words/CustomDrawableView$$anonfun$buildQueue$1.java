package touchy.words;

import java.io.Serializable;
import scala.collection.mutable.Queue;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

/* compiled from: Custom.scala */
public final class CustomDrawableView$$anonfun$buildQueue$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    private final /* synthetic */ Queue queue$1;

    public CustomDrawableView$$anonfun$buildQueue$1(CustomDrawableView $outer, Queue queue) {
        this.queue$1 = queue;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return apply(BoxesRunTime.unboxToInt(v1));
    }

    /* Debug info: failed to restart local var, previous not found, register: 10 */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: scala.collection.LinearSeqOptimized.slice(int, int):Repr
     arg types: [int, int]
     candidates:
      scala.collection.mutable.MutableList.slice(int, int):java.lang.Object
      scala.collection.IterableLike.slice(int, int):Repr
      scala.collection.IterableLike.slice(int, int):Repr
      scala.collection.IterableLike.slice(int, int):Repr
      scala.collection.IterableLike.slice(int, int):Repr
      scala.collection.IterableLike.slice(int, int):Repr
      scala.collection.IterableLike.slice(int, int):Repr
      scala.collection.IterableLike.slice(int, int):Repr
      scala.collection.IterableLike.slice(int, int):Repr
      scala.collection.IterableLike.slice(int, int):Repr
      scala.collection.IterableLike.slice(int, int):Repr
      scala.collection.IterableLike.slice(int, int):Repr
      scala.collection.IterableLike.slice(int, int):Repr
      scala.collection.IterableLike.slice(int, int):Repr
      scala.collection.IterableLike.slice(int, int):Repr
      scala.collection.IterableLike.slice(int, int):Repr
      scala.collection.IterableLike.slice(int, int):Repr
      scala.collection.IterableLike.slice(int, int):Repr
      scala.collection.IterableLike.slice(int, int):Repr
      scala.collection.IterableLike.slice(int, int):Repr
      scala.collection.LinearSeqOptimized.slice(int, int):Repr */
    public final Queue<Letter> apply(int i) {
        int numV;
        if (this.queue$1.length() >= 5) {
            numV = this.queue$1.slice(this.queue$1.length() - 5, this.queue$1.length()).count(new CustomDrawableView$$anonfun$buildQueue$1$$anonfun$4(this));
        } else {
            numV = -1;
        }
        if (numV != -1 && numV > 2) {
            return (Queue) this.queue$1.$plus$eq((Object) new Letter(true, false, i));
        }
        if (numV != -1 && numV == 0) {
            return (Queue) this.queue$1.$plus$eq((Object) new Letter(false, true, i));
        }
        return (Queue) this.queue$1.$plus$eq((Object) new Letter(Letter$.MODULE$.init$default$1(), Letter$.MODULE$.init$default$2(), i));
    }
}
