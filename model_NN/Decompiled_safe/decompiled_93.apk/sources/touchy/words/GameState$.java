package touchy.words;

import android.util.Log;
import java.util.Random;
import scala.Array$;
import scala.Predef$;
import scala.ScalaObject;
import scala.Tuple2;
import scala.collection.Seq;
import scala.collection.Seq$;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.immutable.Iterable$;
import scala.collection.immutable.List;
import scala.collection.immutable.List$;
import scala.collection.immutable.Map;
import scala.collection.immutable.Range;
import scala.collection.mutable.Stack;
import scala.reflect.ClassManifest$;
import scala.runtime.BoxesRunTime;
import scala.runtime.IntRef;

/* compiled from: Data.scala */
public final class GameState$ implements ScalaObject {
    public static final GameState$ MODULE$ = null;
    private final int ABOUT = 3;
    private final int PROMPT_NAME = 5;
    private final int TIPS = 6;
    private final String TIPS_MSG = seqString(Predef$.MODULE$.wrapRefArray((Object[]) new String[]{"Create words using a total of 64 letters. Get more points depending on word length and which letters you use. Can you rank #1 on today's online high scores?", "", "* touch letter tiles to spell a word", "* touch the timer to get a new tile immediately", "* touch the backspace arrow to delete a letter", "* touch a red tile to remove it", "", "Remember:", "* longer words get bonus point multipliers", "* tiles run away if you don't use them in time. Touchy little things!"}));
    private final int TRY_AGAIN = 4;
    private final int YES_NO = 2;
    private String[] bonuses;
    private final boolean debugging = false;
    private final boolean free = false;
    private final int letterDuration;
    private final Map<String, Integer> letterOccurencies;
    private final int letterOccurenciesTotal;
    private final Map<String, Integer> letterPrices;
    private final int lettersPerGame;
    private final List<String> lettersRandom;
    private final List<Integer> multipliers;
    private final int numLetters = 9;
    private final Random random;
    private final int unitTime;
    private final Seq<String> vowels;

    static {
        new GameState$();
    }

    private GameState$() {
        MODULE$ = this;
        this.lettersPerGame = debugging() ? 9 : 64;
        this.unitTime = 500;
        this.letterDuration = 10000;
        this.letterPrices = (Map) Predef$.MODULE$.Map().apply(Predef$.MODULE$.wrapRefArray((Object[]) new Tuple2[]{Predef$.MODULE$.any2ArrowAssoc("J").$minus$greater(BoxesRunTime.boxToInteger(11)), Predef$.MODULE$.any2ArrowAssoc("Q").$minus$greater(BoxesRunTime.boxToInteger(11)), Predef$.MODULE$.any2ArrowAssoc("X").$minus$greater(BoxesRunTime.boxToInteger(11)), Predef$.MODULE$.any2ArrowAssoc("Z").$minus$greater(BoxesRunTime.boxToInteger(11)), Predef$.MODULE$.any2ArrowAssoc("V").$minus$greater(BoxesRunTime.boxToInteger(6)), Predef$.MODULE$.any2ArrowAssoc("F").$minus$greater(BoxesRunTime.boxToInteger(4)), Predef$.MODULE$.any2ArrowAssoc("K").$minus$greater(BoxesRunTime.boxToInteger(4)), Predef$.MODULE$.any2ArrowAssoc("W").$minus$greater(BoxesRunTime.boxToInteger(4)), Predef$.MODULE$.any2ArrowAssoc("Y").$minus$greater(BoxesRunTime.boxToInteger(3)), Predef$.MODULE$.any2ArrowAssoc("B").$minus$greater(BoxesRunTime.boxToInteger(3)), Predef$.MODULE$.any2ArrowAssoc("H").$minus$greater(BoxesRunTime.boxToInteger(3)), Predef$.MODULE$.any2ArrowAssoc("G").$minus$greater(BoxesRunTime.boxToInteger(2)), Predef$.MODULE$.any2ArrowAssoc("M").$minus$greater(BoxesRunTime.boxToInteger(2)), Predef$.MODULE$.any2ArrowAssoc("P").$minus$greater(BoxesRunTime.boxToInteger(2)), Predef$.MODULE$.any2ArrowAssoc("C").$minus$greater(BoxesRunTime.boxToInteger(2)), Predef$.MODULE$.any2ArrowAssoc("U").$minus$greater(BoxesRunTime.boxToInteger(2)), Predef$.MODULE$.any2ArrowAssoc("D").$minus$greater(BoxesRunTime.boxToInteger(2)), Predef$.MODULE$.any2ArrowAssoc("L").$minus$greater(BoxesRunTime.boxToInteger(2)), Predef$.MODULE$.any2ArrowAssoc("O").$minus$greater(BoxesRunTime.boxToInteger(1)), Predef$.MODULE$.any2ArrowAssoc("N").$minus$greater(BoxesRunTime.boxToInteger(1)), Predef$.MODULE$.any2ArrowAssoc("T").$minus$greater(BoxesRunTime.boxToInteger(1)), Predef$.MODULE$.any2ArrowAssoc("R").$minus$greater(BoxesRunTime.boxToInteger(1)), Predef$.MODULE$.any2ArrowAssoc("I").$minus$greater(BoxesRunTime.boxToInteger(1)), Predef$.MODULE$.any2ArrowAssoc("A").$minus$greater(BoxesRunTime.boxToInteger(1)), Predef$.MODULE$.any2ArrowAssoc("S").$minus$greater(BoxesRunTime.boxToInteger(1)), Predef$.MODULE$.any2ArrowAssoc("E").$minus$greater(BoxesRunTime.boxToInteger(1))}));
        this.letterOccurencies = (Map) Predef$.MODULE$.Map().apply(Predef$.MODULE$.wrapRefArray((Object[]) new Tuple2[]{Predef$.MODULE$.any2ArrowAssoc("J").$minus$greater(BoxesRunTime.boxToInteger(1)), Predef$.MODULE$.any2ArrowAssoc("Q").$minus$greater(BoxesRunTime.boxToInteger(1)), Predef$.MODULE$.any2ArrowAssoc("X").$minus$greater(BoxesRunTime.boxToInteger(1)), Predef$.MODULE$.any2ArrowAssoc("Z").$minus$greater(BoxesRunTime.boxToInteger(1)), Predef$.MODULE$.any2ArrowAssoc("V").$minus$greater(BoxesRunTime.boxToInteger(2)), Predef$.MODULE$.any2ArrowAssoc("F").$minus$greater(BoxesRunTime.boxToInteger(3)), Predef$.MODULE$.any2ArrowAssoc("K").$minus$greater(BoxesRunTime.boxToInteger(3)), Predef$.MODULE$.any2ArrowAssoc("W").$minus$greater(BoxesRunTime.boxToInteger(3)), Predef$.MODULE$.any2ArrowAssoc("Y").$minus$greater(BoxesRunTime.boxToInteger(4)), Predef$.MODULE$.any2ArrowAssoc("B").$minus$greater(BoxesRunTime.boxToInteger(5)), Predef$.MODULE$.any2ArrowAssoc("H").$minus$greater(BoxesRunTime.boxToInteger(5)), Predef$.MODULE$.any2ArrowAssoc("G").$minus$greater(BoxesRunTime.boxToInteger(6)), Predef$.MODULE$.any2ArrowAssoc("M").$minus$greater(BoxesRunTime.boxToInteger(6)), Predef$.MODULE$.any2ArrowAssoc("P").$minus$greater(BoxesRunTime.boxToInteger(6)), Predef$.MODULE$.any2ArrowAssoc("C").$minus$greater(BoxesRunTime.boxToInteger(7)), Predef$.MODULE$.any2ArrowAssoc("U").$minus$greater(BoxesRunTime.boxToInteger(7)), Predef$.MODULE$.any2ArrowAssoc("D").$minus$greater(BoxesRunTime.boxToInteger(8)), Predef$.MODULE$.any2ArrowAssoc("L").$minus$greater(BoxesRunTime.boxToInteger(10)), Predef$.MODULE$.any2ArrowAssoc("O").$minus$greater(BoxesRunTime.boxToInteger(11)), Predef$.MODULE$.any2ArrowAssoc("N").$minus$greater(BoxesRunTime.boxToInteger(11)), Predef$.MODULE$.any2ArrowAssoc("T").$minus$greater(BoxesRunTime.boxToInteger(11)), Predef$.MODULE$.any2ArrowAssoc("R").$minus$greater(BoxesRunTime.boxToInteger(13)), Predef$.MODULE$.any2ArrowAssoc("I").$minus$greater(BoxesRunTime.boxToInteger(14)), Predef$.MODULE$.any2ArrowAssoc("A").$minus$greater(BoxesRunTime.boxToInteger(15)), Predef$.MODULE$.any2ArrowAssoc("S").$minus$greater(BoxesRunTime.boxToInteger(15)), Predef$.MODULE$.any2ArrowAssoc("E").$minus$greater(BoxesRunTime.boxToInteger(21))}));
        this.letterOccurenciesTotal = BoxesRunTime.unboxToInt(((TraversableOnce) letterOccurencies().map(new GameState$$anonfun$3(), Iterable$.MODULE$.canBuildFrom())).reduceLeft(new GameState$$anonfun$1()));
        this.lettersRandom = (List) ((TraversableOnce) letterOccurencies().map(new GameState$$anonfun$4(), Iterable$.MODULE$.canBuildFrom())).reduceLeft(new GameState$$anonfun$5());
        this.multipliers = List$.MODULE$.apply((Seq) Predef$.MODULE$.wrapIntArray(new int[]{0, 1, 1, 1, 2, 3, 4, 5, 6, 7}));
        this.random = new Random();
        this.vowels = (Seq) Seq$.MODULE$.apply(Predef$.MODULE$.wrapRefArray((Object[]) new String[]{"A", "E", "I", "O", "U"}));
    }

    public int YES_NO() {
        return this.YES_NO;
    }

    public int ABOUT() {
        return this.ABOUT;
    }

    public int TRY_AGAIN() {
        return this.TRY_AGAIN;
    }

    public int PROMPT_NAME() {
        return this.PROMPT_NAME;
    }

    public int TIPS() {
        return this.TIPS;
    }

    public String TIPS_MSG() {
        return this.TIPS_MSG;
    }

    public int numLetters() {
        return this.numLetters;
    }

    public boolean free() {
        return this.free;
    }

    public boolean debugging() {
        return this.debugging;
    }

    public int lettersPerGame() {
        return this.lettersPerGame;
    }

    public int unitTime() {
        return this.unitTime;
    }

    public int letterDuration() {
        return this.letterDuration;
    }

    public Map<String, Integer> letterPrices() {
        return this.letterPrices;
    }

    public Map<String, Integer> letterOccurencies() {
        return this.letterOccurencies;
    }

    public int letterOccurenciesTotal() {
        return this.letterOccurenciesTotal;
    }

    public List<String> lettersRandom() {
        return this.lettersRandom;
    }

    public List<Integer> multipliers() {
        return this.multipliers;
    }

    public Random random() {
        return this.random;
    }

    public Seq<String> vowels() {
        return this.vowels;
    }

    public int log(Object x) {
        return Log.v("touchy.words.log", x.toString());
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public String seqString(Seq<String> x) {
        if (x.isEmpty()) {
            return "";
        }
        return (String) x.reduceLeft(new GameState$$anonfun$seqString$1());
    }

    public String[] bonuses() {
        return this.bonuses;
    }

    public void bonuses_$eq(String[] strArr) {
        this.bonuses = strArr;
    }

    public void buildBonuses() {
        bonuses_$eq((String[]) Array$.MODULE$.fill(lettersPerGame(), new GameState$$anonfun$buildBonuses$1(), ClassManifest$.MODULE$.classType(String.class)));
        addBonuses("double", 2);
        addBonuses("triple", 1);
    }

    public void addBonuses(String bonus$1, int size) {
        ((Range.ByOne) Predef$.MODULE$.intWrapper(1).to(size)).foreach$mVc$sp(new GameState$$anonfun$addBonuses$1(bonus$1, new IntRef(0)));
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public String wordToStr(Stack<Letter> q) {
        return q.isEmpty() ? "" : (String) ((TraversableOnce) ((TraversableLike) q.reverse()).map(new GameState$$anonfun$wordToStr$1(), Seq$.MODULE$.canBuildFrom())).reduceLeft(new GameState$$anonfun$wordToStr$2());
    }

    public int wordScore(Stack<Letter> word) {
        if (word.isEmpty()) {
            return 0;
        }
        return BoxesRunTime.unboxToInt(((TraversableOnce) word.map(new GameState$$anonfun$wordScore$2(), Seq$.MODULE$.canBuildFrom())).reduceLeft(new GameState$$anonfun$wordScore$1())) * BoxesRunTime.unboxToInt(multipliers().apply(word.length()));
    }
}
