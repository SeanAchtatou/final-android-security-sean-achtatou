package touchy.words;

import java.io.Serializable;
import scala.Tuple2;
import scala.collection.immutable.List;
import scala.collection.immutable.List$;
import scala.runtime.AbstractFunction1;

/* compiled from: Data.scala */
public final class GameState$$anonfun$4 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return apply((Tuple2<String, Integer>) ((Tuple2) v1));
    }

    public final List<String> apply(Tuple2<String, Integer> x$13) {
        return (List) List$.MODULE$.fill(x$13._2$mcI$sp(), new GameState$$anonfun$4$$anonfun$apply$1(this, x$13));
    }
}
