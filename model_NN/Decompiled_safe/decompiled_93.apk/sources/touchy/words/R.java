package touchy.words;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int about = 2130837504;
        public static final int app_icon = 2130837505;
        public static final int background = 2130837506;
        public static final int background_menu = 2130837507;
        public static final int contact = 2130837508;
        public static final int home = 2130837509;
        public static final int new_game = 2130837510;
        public static final int pause = 2130837511;
        public static final int scala_logo = 2130837512;
        public static final int share = 2130837513;
    }

    public static final class id {
        public static final int about = 2131165206;
        public static final int adView = 2131165189;
        public static final int cancel = 2131165210;
        public static final int contact = 2131165207;
        public static final int delusername = 2131165201;
        public static final int game = 2131165187;
        public static final int game_over_content = 2131165184;
        public static final int go_online = 2131165197;
        public static final int highscores = 2131165191;
        public static final int highscores_back = 2131165196;
        public static final int highscores_text = 2131165195;
        public static final int layout_highscores = 2131165194;
        public static final int layout_home = 2131165188;
        public static final int layout_settings = 2131165198;
        public static final int menu = 2131165205;
        public static final int new_game = 2131165190;
        public static final int ok = 2131165211;
        public static final int pause = 2131165204;
        public static final int reset = 2131165199;
        public static final int settings = 2131165192;
        public static final int settings_back = 2131165203;
        public static final int share = 2131165208;
        public static final int show_menu = 2131165185;
        public static final int sound = 2131165202;
        public static final int try_again = 2131165186;
        public static final int tutorial = 2131165200;
        public static final int username = 2131165209;
        public static final int version = 2131165193;
    }

    public static final class layout {
        public static final int game_over_dialog = 2130903040;
        public static final int layout_game = 2130903041;
        public static final int menu = 2130903042;
        public static final int username_dialog = 2130903043;
    }

    public static final class raw {
        public static final int lost_letter = 2130968576;
        public static final int no_letters = 2130968577;
        public static final int victory = 2130968578;
        public static final int words = 2130968579;
    }

    public static final class string {
        public static final int app_name = 2131034112;
    }

    public static final class style {
        public static final int CodeFont = 2131099648;
    }
}
