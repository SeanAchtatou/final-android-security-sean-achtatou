package touchy.words;

import android.view.View;
import touchy.words.TypedViewHolder;

/* compiled from: TR.scala */
public final class TypedResource$$anon$1 implements TypedViewHolder {
    private final /* synthetic */ View v$1;

    public TypedResource$$anon$1(View view) {
        this.v$1 = view;
        TypedViewHolder.Cclass.$init$(this);
    }

    public <T> T findView(TypedResource<T> tr) {
        return TypedViewHolder.Cclass.findView(this, tr);
    }

    public View view() {
        return this.v$1;
    }
}
