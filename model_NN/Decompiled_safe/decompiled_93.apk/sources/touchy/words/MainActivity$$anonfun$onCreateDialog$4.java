package touchy.words;

import android.app.Dialog;
import android.view.View;
import java.io.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxedUnit;

/* compiled from: Activity.scala */
public final class MainActivity$$anonfun$onCreateDialog$4 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    private final /* synthetic */ MainActivity $outer;
    private final /* synthetic */ Dialog dialog$2;

    public MainActivity$$anonfun$onCreateDialog$4(MainActivity $outer2, Dialog dialog) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.dialog$2 = dialog;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        apply((View) v1);
        return BoxedUnit.UNIT;
    }

    public final void apply(View v) {
        if (!(this.$outer.game() == null || this.$outer.game().activity() == null || this.$outer.game().activity().tracker() == null)) {
            this.$outer.game().gameMenu();
        }
        this.dialog$2.cancel();
    }
}
