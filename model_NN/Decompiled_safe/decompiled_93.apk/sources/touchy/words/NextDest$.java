package touchy.words;

import java.io.Serializable;
import scala.Function0;
import scala.Function1;
import scala.None$;
import scala.Option;
import scala.ScalaObject;
import scala.Some;
import scala.Tuple4;
import scala.collection.immutable.Map;
import scala.runtime.AbstractFunction4;

/* compiled from: Activity.scala */
public final /* synthetic */ class NextDest$ extends AbstractFunction4 implements Serializable, ScalaObject {
    public static final NextDest$ MODULE$ = null;

    static {
        new NextDest$();
    }

    private NextDest$() {
        MODULE$ = this;
    }

    public /* synthetic */ NextDest apply(String path, Map args, Function1 ok, Function0 error) {
        return new NextDest(path, args, ok, error);
    }

    public Object readResolve() {
        return MODULE$;
    }

    public /* synthetic */ Option unapply(NextDest x$0) {
        return x$0 == null ? None$.MODULE$ : new Some(new Tuple4(x$0.copy$default$1(), x$0.copy$default$2(), x$0.copy$default$3(), x$0.copy$default$4()));
    }
}
