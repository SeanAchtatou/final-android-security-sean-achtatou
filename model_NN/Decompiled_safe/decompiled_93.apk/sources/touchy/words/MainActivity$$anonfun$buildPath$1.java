package touchy.words;

import java.io.Serializable;
import scala.Tuple2;
import scala.collection.mutable.StringBuilder;
import scala.runtime.AbstractFunction1;

/* compiled from: Activity.scala */
public final class MainActivity$$anonfun$buildPath$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;

    public MainActivity$$anonfun$buildPath$1(MainActivity $outer) {
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return apply((Tuple2<String, String>) ((Tuple2) v1));
    }

    public final String apply(Tuple2<String, String> x) {
        return new StringBuilder().append((Object) x._1()).append((Object) "=").append((Object) x._2()).toString();
    }
}
