package touchy.words;

import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.ads.AdView;
import scala.ScalaObject;

/* compiled from: TR.scala */
public final class TR$ implements ScalaObject {
    public static final TR$ MODULE$ = null;
    private final TypedResource<AdView> adView = new TypedResource<>(R.id.adView);
    private final TypedResource<Button> cancel = new TypedResource<>(R.id.cancel);
    private final TypedResource<Button> delusername = new TypedResource<>(R.id.delusername);
    private final TypedResource<CustomDrawableView> game = new TypedResource<>(R.id.game);
    private final TypedResource<TextView> game_over_content = new TypedResource<>(R.id.game_over_content);
    private final TypedResource<Button> go_online = new TypedResource<>(R.id.go_online);
    private final TypedResource<Button> highscores = new TypedResource<>(R.id.highscores);
    private final TypedResource<Button> highscores_back = new TypedResource<>(R.id.highscores_back);
    private final TypedResource<TextView> highscores_text = new TypedResource<>(R.id.highscores_text);
    private final TypedResource<RelativeLayout> layout_highscores = new TypedResource<>(R.id.layout_highscores);
    private final TypedResource<RelativeLayout> layout_home = new TypedResource<>(R.id.layout_home);
    private final TypedResource<RelativeLayout> layout_settings = new TypedResource<>(R.id.layout_settings);
    private final TypedResource<Button> new_game = new TypedResource<>(R.id.new_game);
    private final TypedResource<Button> ok = new TypedResource<>(R.id.ok);
    private final TypedResource<Button> reset = new TypedResource<>(R.id.reset);
    private final TypedResource<Button> settings = new TypedResource<>(R.id.settings);
    private final TypedResource<Button> settings_back = new TypedResource<>(R.id.settings_back);
    private final TypedResource<Button> show_menu = new TypedResource<>(R.id.show_menu);
    private final TypedResource<CheckBox> sound = new TypedResource<>(R.id.sound);
    private final TypedResource<Button> try_again = new TypedResource<>(R.id.try_again);
    private final TypedResource<Button> tutorial = new TypedResource<>(R.id.tutorial);
    private final TypedResource<EditText> username = new TypedResource<>(R.id.username);
    private final TypedResource<Button> version = new TypedResource<>(R.id.version);

    static {
        new TR$();
    }

    private TR$() {
        MODULE$ = this;
    }

    public TypedResource<RelativeLayout> layout_home() {
        return this.layout_home;
    }

    public TypedResource<RelativeLayout> layout_settings() {
        return this.layout_settings;
    }

    public TypedResource<Button> settings() {
        return this.settings;
    }

    public TypedResource<Button> highscores() {
        return this.highscores;
    }

    public TypedResource<CustomDrawableView> game() {
        return this.game;
    }

    public TypedResource<Button> new_game() {
        return this.new_game;
    }

    public TypedResource<Button> ok() {
        return this.ok;
    }

    public TypedResource<Button> reset() {
        return this.reset;
    }

    public TypedResource<AdView> adView() {
        return this.adView;
    }

    public TypedResource<CheckBox> sound() {
        return this.sound;
    }

    public TypedResource<Button> version() {
        return this.version;
    }

    public TypedResource<Button> settings_back() {
        return this.settings_back;
    }

    public TypedResource<Button> tutorial() {
        return this.tutorial;
    }

    public TypedResource<RelativeLayout> layout_highscores() {
        return this.layout_highscores;
    }

    public TypedResource<Button> delusername() {
        return this.delusername;
    }

    public TypedResource<Button> highscores_back() {
        return this.highscores_back;
    }

    public TypedResource<TextView> game_over_content() {
        return this.game_over_content;
    }

    public TypedResource<Button> try_again() {
        return this.try_again;
    }

    public TypedResource<EditText> username() {
        return this.username;
    }

    public TypedResource<Button> show_menu() {
        return this.show_menu;
    }

    public TypedResource<Button> go_online() {
        return this.go_online;
    }

    public TypedResource<TextView> highscores_text() {
        return this.highscores_text;
    }

    public TypedResource<Button> cancel() {
        return this.cancel;
    }
}
