package touchy.words;

import java.io.Serializable;
import scala.collection.mutable.StringBuilder;
import scala.runtime.AbstractFunction2;

/* compiled from: Data.scala */
public final class GameState$$anonfun$wordToStr$2 extends AbstractFunction2 implements Serializable {
    public static final long serialVersionUID = 0;

    public final String apply(String str, String str2) {
        return new StringBuilder().append((Object) str).append((Object) str2).toString();
    }
}
