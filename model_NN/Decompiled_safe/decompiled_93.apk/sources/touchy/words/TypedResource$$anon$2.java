package touchy.words;

import android.app.Activity;
import touchy.words.TypedActivityHolder;

/* compiled from: TR.scala */
public final class TypedResource$$anon$2 implements TypedActivityHolder {
    private final /* synthetic */ Activity act$1;

    public TypedResource$$anon$2(Activity activity) {
        this.act$1 = activity;
        TypedActivityHolder.Cclass.$init$(this);
    }

    public Activity activity() {
        return this.act$1;
    }

    public <T> T findView(TypedResource<T> tr) {
        return TypedActivityHolder.Cclass.findView(this, tr);
    }
}
