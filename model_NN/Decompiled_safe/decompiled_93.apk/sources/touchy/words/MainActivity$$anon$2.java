package touchy.words;

import android.app.Dialog;
import android.view.KeyEvent;
import android.view.View;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

/* compiled from: Activity.scala */
public final class MainActivity$$anon$2 implements View.OnKeyListener {
    private final /* synthetic */ MainActivity $outer;
    private final /* synthetic */ Dialog dialog$1;

    public MainActivity$$anon$2(MainActivity $outer2, Dialog dialog) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.dialog$1 = dialog;
    }

    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (keyCode != 66) {
            return false;
        }
        if (event.getAction() != 0) {
            BoxesRunTime.boxToBoolean(this.$outer.getButton(R.id.ok, this.dialog$1).performClick());
        } else {
            BoxedUnit boxedUnit = BoxedUnit.UNIT;
        }
        return true;
    }
}
