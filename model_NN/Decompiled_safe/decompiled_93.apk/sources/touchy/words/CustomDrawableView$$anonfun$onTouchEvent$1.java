package touchy.words;

import java.io.Serializable;
import scala.MatchError;
import scala.Tuple2;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

/* compiled from: Custom.scala */
public final class CustomDrawableView$$anonfun$onTouchEvent$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    private final /* synthetic */ CustomDrawableView $outer;
    private final /* synthetic */ float x$27;
    private final /* synthetic */ float y$1;

    public CustomDrawableView$$anonfun$onTouchEvent$1(CustomDrawableView $outer2, float f, float f2) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.x$27 = f;
        this.y$1 = f2;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return apply((Tuple2<Letter, Integer>) ((Tuple2) v1));
    }

    /* Debug info: failed to restart local var, previous not found, register: 5 */
    public final Object apply(Tuple2<Letter, Integer> tuple2) {
        if (tuple2 == null) {
            throw new MatchError(tuple2);
        }
        int l = this.$outer.margin() + ((this.$outer.side() + this.$outer.margin()) * BoxesRunTime.unboxToInt(tuple2._2()));
        if (this.x$27 <= ((float) l) || this.x$27 >= ((float) (this.$outer.side() + l)) || this.y$1 <= ((float) this.$outer.cardTop()) || this.y$1 >= ((float) (this.$outer.cardTop() + this.$outer.cardH()))) {
            return BoxedUnit.UNIT;
        }
        return this.$outer.selectLetter(tuple2._1());
    }
}
