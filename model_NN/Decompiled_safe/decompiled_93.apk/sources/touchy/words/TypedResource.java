package touchy.words;

import android.app.Activity;
import android.view.View;
import scala.Product;
import scala.ScalaObject;
import scala.collection.Iterator;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime$;

/* compiled from: TR.scala */
public class TypedResource<T> implements ScalaObject, Product, ScalaObject {
    private final int id;

    public static final TypedActivityHolder activity2typed(Activity activity) {
        return TypedResource$.MODULE$.activity2typed(activity);
    }

    public static final TypedViewHolder view2typed(View view) {
        return TypedResource$.MODULE$.view2typed(view);
    }

    public TypedResource(int id2) {
        this.id = id2;
        Product.Cclass.$init$(this);
    }

    private final /* synthetic */ boolean gd1$1(int i) {
        return i == copy$default$1();
    }

    public boolean canEqual(Object obj) {
        return obj instanceof TypedResource;
    }

    public /* synthetic */ TypedResource copy(int id2) {
        return new TypedResource(id2);
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (!(obj instanceof TypedResource ? gd1$1(((TypedResource) obj).copy$default$1()) ? ((TypedResource) obj).canEqual(this) : false : false)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return ScalaRunTime$.MODULE$._hashCode(this);
    }

    /* renamed from: id */
    public int copy$default$1() {
        return this.id;
    }

    public int productArity() {
        return 1;
    }

    public Object productElement(int i) {
        if (i == 0) {
            return BoxesRunTime.boxToInteger(copy$default$1());
        }
        throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(i).toString());
    }

    public Iterator<Object> productElements() {
        return Product.Cclass.productElements(this);
    }

    public Iterator<Object> productIterator() {
        return Product.Cclass.productIterator(this);
    }

    public String productPrefix() {
        return "TypedResource";
    }

    public String toString() {
        return ScalaRunTime$.MODULE$._toString(this);
    }
}
