package touchy.words;

import android.view.Menu;
import android.view.MenuItem;
import java.io.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

/* compiled from: Activity.scala */
public final class MainActivity$$anonfun$onPrepareOptionsMenu$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    private final /* synthetic */ MainActivity $outer;
    private final /* synthetic */ Menu menu$1;
    private final /* synthetic */ boolean x$9;

    public MainActivity$$anonfun$onPrepareOptionsMenu$1(MainActivity $outer2, Menu menu, boolean z) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.menu$1 = menu;
        this.x$9 = z;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return apply(BoxesRunTime.unboxToInt(v1));
    }

    public final MenuItem apply(int id) {
        return ((MenuItem) this.$outer.getResource(id, this.menu$1)).setVisible(this.x$9);
    }
}
