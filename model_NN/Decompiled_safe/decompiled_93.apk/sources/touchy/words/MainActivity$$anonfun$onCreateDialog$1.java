package touchy.words;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.view.View;
import android.widget.EditText;
import java.io.Serializable;
import scala.None$;
import scala.Option;
import scala.Predef$;
import scala.Tuple2;
import scala.collection.immutable.List;
import scala.collection.immutable.Map;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxedUnit;

/* compiled from: Activity.scala */
public final class MainActivity$$anonfun$onCreateDialog$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    private final /* synthetic */ MainActivity $outer;
    public final /* synthetic */ Dialog dialog$1;
    private final /* synthetic */ EditText edit$1;

    public MainActivity$$anonfun$onCreateDialog$1(MainActivity $outer2, Dialog dialog, EditText editText) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.dialog$1 = dialog;
        this.edit$1 = editText;
    }

    public /* synthetic */ MainActivity touchy$words$MainActivity$$anonfun$$$outer() {
        return this.$outer;
    }

    public final Object apply(View v) {
        String username$1 = this.edit$1.getText().toString().trim();
        if (username$1.length() < 4 || username$1.length() > 15) {
            this.$outer.showToast("The username must be between 4 and 15 characters");
            return BoxedUnit.UNIT;
        }
        Option<List<String>> unapplySeq = Predef$.MODULE$.augmentString("[0-9a-zA-Z._-]+").r().unapplySeq(username$1);
        None$ none$ = None$.MODULE$;
        if (unapplySeq != null ? !unapplySeq.equals(none$) : none$ != null) {
            ProgressDialog progressDialog$1 = ProgressDialog.show(this.$outer, "", "Checking availability...", true, true);
            this.$outer.asyncGet("create-user", (Map) Predef$.MODULE$.Map().apply(Predef$.MODULE$.wrapRefArray((Object[]) new Tuple2[]{Predef$.MODULE$.any2ArrowAssoc("username").$minus$greater(username$1)})), new MainActivity$$anonfun$onCreateDialog$1$$anonfun$apply$5(this, username$1, progressDialog$1), new MainActivity$$anonfun$onCreateDialog$1$$anonfun$apply$2(this, progressDialog$1), this.$outer.asyncGet$default$5());
            return BoxedUnit.UNIT;
        }
        this.$outer.showToast("The username can only contains alphanumeric characters and ._-");
        return BoxedUnit.UNIT;
    }
}
