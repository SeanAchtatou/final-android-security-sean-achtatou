package touchy.words;

import android.view.View;
import android.widget.CheckBox;
import java.io.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxedUnit;

/* compiled from: Activity.scala */
public final class MainActivity$$anonfun$init$12 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    private final /* synthetic */ MainActivity $outer;
    private final /* synthetic */ CheckBox checkBox$1;

    public MainActivity$$anonfun$init$12(MainActivity $outer2, CheckBox checkBox) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.checkBox$1 = checkBox;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        apply((View) v1);
        return BoxedUnit.UNIT;
    }

    public final void apply(View v) {
        this.$outer.hasSound_$eq(this.checkBox$1.isChecked() ? "yes" : "no");
        this.$outer.saveSettings("sound", this.$outer.hasSound(), this.$outer.saveSettings$default$3(), this.$outer.saveSettings$default$4());
    }
}
