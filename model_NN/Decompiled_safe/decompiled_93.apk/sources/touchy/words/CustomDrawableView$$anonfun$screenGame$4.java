package touchy.words;

import android.graphics.Canvas;
import java.io.Serializable;
import scala.MatchError;
import scala.Tuple2;
import scala.collection.mutable.StringBuilder;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

/* compiled from: Custom.scala */
public final class CustomDrawableView$$anonfun$screenGame$4 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    private final /* synthetic */ CustomDrawableView $outer;
    private final /* synthetic */ Canvas canvas$1;

    public CustomDrawableView$$anonfun$screenGame$4(CustomDrawableView $outer2, Canvas canvas) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.canvas$1 = canvas;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        apply((Tuple2<String, Integer>) ((Tuple2) v1));
        return BoxedUnit.UNIT;
    }

    public final void apply(Tuple2<String, Integer> tuple2) {
        if (tuple2 == null) {
            throw new MatchError(tuple2);
        }
        this.canvas$1.drawText(new StringBuilder().append((Object) " ").append((Object) tuple2._1()).toString(), (float) ((this.$outer.w() * 62) / 100), (float) BoxesRunTime.unboxToInt(tuple2._2()), this.$outer.pHistory());
    }
}
