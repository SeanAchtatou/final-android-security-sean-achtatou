package touchy.words;

import android.content.res.Resources;
import java.io.InputStream;
import scala.Array$;
import scala.Predef$;
import scala.ScalaObject;
import scala.collection.immutable.Range;
import scala.reflect.Manifest$;
import scala.runtime.NonLocalReturnControl;

/* compiled from: Data.scala */
public final class Dict$ implements ScalaObject {
    public static final Dict$ MODULE$ = null;
    private final int range = 128;
    private int size = 0;
    private InputStream words = null;

    static {
        new Dict$();
    }

    private Dict$() {
        MODULE$ = this;
    }

    public InputStream words() {
        return this.words;
    }

    public void words_$eq(InputStream inputStream) {
        this.words = inputStream;
    }

    /* renamed from: size */
    public int exists$default$3() {
        return this.size;
    }

    public void size_$eq(int i) {
        this.size = i;
    }

    public int range() {
        return this.range;
    }

    public void init(Resources res) {
        if (words() == null) {
            words_$eq(res.openRawResource(R.raw.words));
            words().mark(10000000);
            while (words().available() != 0) {
                size_$eq(exists$default$3() + words().available());
                words().skip((long) words().available());
            }
        }
    }

    public String getword(int pivot$1, String words$1) {
        Object obj = new Object();
        try {
            ((Range.ByOne) Predef$.MODULE$.intWrapper(0).to(range())).foreach$mVc$sp(new Dict$$anonfun$getword$1(pivot$1, words$1, obj));
            return "";
        } catch (NonLocalReturnControl e) {
            if (e.key() == obj) {
                return (String) e.value();
            }
            throw e;
        }
    }

    public /* synthetic */ int exists$default$2() {
        return 0;
    }

    public boolean exists(String word, int start, int end) {
        int startsub;
        int i;
        while (true) {
            int pivot = ((end - start) / 2) + start;
            if (pivot - range() > 0) {
                startsub = pivot - range();
            } else {
                startsub = 0;
            }
            int endsub = range() + pivot > exists$default$3() ? exists$default$3() : range() + pivot;
            byte[] buffer = (byte[]) Array$.MODULE$.fill(endsub - startsub, new Dict$$anonfun$2(), Manifest$.MODULE$.Byte());
            words().reset();
            words().skip((long) startsub);
            words().read(buffer, 0, endsub - startsub);
            String block = new String(buffer);
            if (pivot - range() > 0) {
                i = range();
            } else {
                i = pivot;
            }
            String curWord = getword(i, block);
            if (block.indexOf(Predef$.MODULE$.augmentString("\n%s\n").format(Predef$.MODULE$.genericWrapArray(new Object[]{word}))) != -1) {
                return true;
            }
            if (pivot != start && pivot != end && end - start >= endsub - startsub) {
                if (Predef$.MODULE$.augmentString(word).$less(curWord)) {
                    end = pivot;
                } else {
                    start = pivot;
                }
            }
        }
        return false;
    }
}
