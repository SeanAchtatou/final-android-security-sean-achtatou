package touchy.words;

import android.app.Activity;
import android.view.View;
import java.io.Serializable;
import scala.None$;
import scala.Option;
import scala.ScalaObject;
import scala.Some;
import scala.runtime.BoxesRunTime;

/* compiled from: TR.scala */
public final class TypedResource$ implements Serializable, ScalaObject {
    public static final TypedResource$ MODULE$ = null;

    static {
        new TypedResource$();
    }

    public /* synthetic */ TypedResource apply(int id) {
        return new TypedResource(id);
    }

    public /* synthetic */ Option unapply(TypedResource x$0) {
        return x$0 == null ? None$.MODULE$ : new Some(BoxesRunTime.boxToInteger(x$0.copy$default$1()));
    }

    private TypedResource$() {
        MODULE$ = this;
    }

    public Object readResolve() {
        return MODULE$;
    }

    public TypedViewHolder view2typed(View v$1) {
        return new TypedResource$$anon$1(v$1);
    }

    public TypedActivityHolder activity2typed(Activity act$1) {
        return new TypedResource$$anon$2(act$1);
    }
}
