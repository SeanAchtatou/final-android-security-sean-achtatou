package touchy.words;

import scala.ScalaObject;

/* compiled from: TR.scala */
public interface TypedActivity extends ScalaObject, TypedActivityHolder {
    TypedActivity activity();

    /* renamed from: touchy.words.TypedActivity$class  reason: invalid class name */
    /* compiled from: TR.scala */
    public abstract class Cclass {
        public static void $init$(TypedActivity $this) {
        }

        public static TypedActivity activity(TypedActivity $this) {
            return $this;
        }
    }
}
