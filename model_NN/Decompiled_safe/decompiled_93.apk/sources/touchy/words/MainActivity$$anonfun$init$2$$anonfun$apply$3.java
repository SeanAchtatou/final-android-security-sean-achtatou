package touchy.words;

import android.view.View;
import java.io.Serializable;
import org.json.JSONObject;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxedUnit;

/* compiled from: Activity.scala */
public final class MainActivity$$anonfun$init$2$$anonfun$apply$3 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    private final /* synthetic */ MainActivity$$anonfun$init$2 $outer;
    private final /* synthetic */ JSONObject res$1;

    public MainActivity$$anonfun$init$2$$anonfun$apply$3(MainActivity$$anonfun$init$2 $outer2, JSONObject jSONObject) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.res$1 = jSONObject;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        apply((View) v1);
        return BoxedUnit.UNIT;
    }

    public final void apply(View v) {
        this.$outer.touchy$words$MainActivity$$anonfun$$$outer().openUrl(this.res$1.getString("url"));
    }
}
