package touchy.words;

import java.io.Serializable;
import scala.runtime.AbstractFunction1$mcII$sp;
import scala.runtime.BoxesRunTime;

/* compiled from: Custom.scala */
public final class CustomDrawableView$$anonfun$2 extends AbstractFunction1$mcII$sp implements Serializable {
    public static final long serialVersionUID = 0;

    public CustomDrawableView$$anonfun$2(CustomDrawableView $outer) {
    }

    public final int apply(int x) {
        return apply$mcII$sp(x);
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return BoxesRunTime.boxToInteger(apply(BoxesRunTime.unboxToInt(v1)));
    }

    public int apply$mcII$sp(int v1) {
        return (v1 * 16) + v1;
    }
}
