package touchy.words;

import android.content.DialogInterface;
import scala.runtime.BoxesRunTime;

/* compiled from: Activity.scala */
public final class MainActivity$$anon$4 implements DialogInterface.OnClickListener {
    private final /* synthetic */ MainActivity $outer;

    public MainActivity$$anon$4(MainActivity $outer2) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
    }

    public void onClick(DialogInterface dialog, int id) {
        this.$outer.saveSettings("tutorial", BoxesRunTime.boxToInteger(0), this.$outer.saveSettings$default$3(), this.$outer.saveSettings$default$4());
        this.$outer.game().beginGame();
    }
}
