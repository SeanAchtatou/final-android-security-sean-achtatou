package touchy.words;

import java.io.Serializable;
import scala.Predef$;
import scala.collection.Seq;
import scala.collection.Seq$;
import scala.runtime.AbstractFunction1;

/* compiled from: Custom.scala */
public final class CustomDrawableView$$anonfun$gameOver$2 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    private final /* synthetic */ CustomDrawableView $outer;

    public CustomDrawableView$$anonfun$gameOver$2(CustomDrawableView $outer2) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
    }

    /* Debug info: failed to restart local var, previous not found, register: 8 */
    public final void apply(Object x) {
        String y = (String) x;
        if (y != null && (y != null ? !y.equals("failed") : "failed" != 0)) {
            this.$outer.activity().showToast(y);
        }
        int index = y.indexOf("(");
        if (index != -1) {
            this.$outer.helpRight_$eq((Seq) Seq$.MODULE$.apply(Predef$.MODULE$.wrapRefArray((Object[]) new String[]{"Online ranking:", y.substring(0, index)})));
        }
    }
}
