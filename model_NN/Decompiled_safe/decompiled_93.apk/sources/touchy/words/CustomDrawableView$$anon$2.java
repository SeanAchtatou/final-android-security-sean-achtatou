package touchy.words;

import android.os.CountDownTimer;

/* compiled from: Custom.scala */
public final class CustomDrawableView$$anon$2 extends CountDownTimer {
    private final /* synthetic */ CustomDrawableView $outer;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CustomDrawableView$$anon$2(CustomDrawableView $outer2) {
        super(GameState$.MODULE$.debugging() ? 500 : 2000, 1000);
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
    }

    public void onTick(long n) {
    }

    public void onFinish() {
        this.$outer.activity().init();
        this.$outer.gameMenu();
    }
}
