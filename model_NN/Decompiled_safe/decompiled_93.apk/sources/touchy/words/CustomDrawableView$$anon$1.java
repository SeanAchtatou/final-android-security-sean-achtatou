package touchy.words;

import android.os.CountDownTimer;

/* compiled from: Custom.scala */
public final class CustomDrawableView$$anon$1 extends CountDownTimer {
    private final /* synthetic */ CustomDrawableView $outer;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CustomDrawableView$$anon$1(CustomDrawableView $outer2) {
        super((long) $outer2.counterDuration(), (long) GameState$.MODULE$.unitTime());
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
    }

    public void onTick(long n) {
        this.$outer.tickTime_$eq((float) n);
        this.$outer.invalidate();
    }

    public void onFinish() {
        this.$outer.startTimer(this.$outer.startTimer$default$1(), this.$outer.startTimer$default$2(), this.$outer.startTimer$default$3());
    }
}
