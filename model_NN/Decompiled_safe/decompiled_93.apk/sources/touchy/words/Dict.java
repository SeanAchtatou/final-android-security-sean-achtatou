package touchy.words;

import android.content.res.Resources;
import java.io.InputStream;

/* compiled from: Data.scala */
public final class Dict {
    public static final boolean exists(String str, int i, int i2) {
        return Dict$.MODULE$.exists(str, i, i2);
    }

    public static final int exists$default$2() {
        return Dict$.MODULE$.exists$default$2();
    }

    public static final int exists$default$3() {
        return Dict$.MODULE$.exists$default$3();
    }

    public static final String getword(int i, String str) {
        return Dict$.MODULE$.getword(i, str);
    }

    public static final void init(Resources resources) {
        Dict$.MODULE$.init(resources);
    }

    public static final int range() {
        return Dict$.MODULE$.range();
    }

    public static final int size() {
        return Dict$.MODULE$.exists$default$3();
    }

    public static final void size_$eq(int i) {
        Dict$.MODULE$.size_$eq(i);
    }

    public static final InputStream words() {
        return Dict$.MODULE$.words();
    }

    public static final void words_$eq(InputStream inputStream) {
        Dict$.MODULE$.words_$eq(inputStream);
    }
}
