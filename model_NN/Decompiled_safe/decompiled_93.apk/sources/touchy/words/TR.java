package touchy.words;

import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.ads.AdView;

/* compiled from: TR.scala */
public final class TR {
    public static final TypedResource<AdView> adView() {
        return TR$.MODULE$.adView();
    }

    public static final TypedResource<Button> cancel() {
        return TR$.MODULE$.cancel();
    }

    public static final TypedResource<Button> delusername() {
        return TR$.MODULE$.delusername();
    }

    public static final TypedResource<CustomDrawableView> game() {
        return TR$.MODULE$.game();
    }

    public static final TypedResource<TextView> game_over_content() {
        return TR$.MODULE$.game_over_content();
    }

    public static final TypedResource<Button> go_online() {
        return TR$.MODULE$.go_online();
    }

    public static final TypedResource<Button> highscores() {
        return TR$.MODULE$.highscores();
    }

    public static final TypedResource<Button> highscores_back() {
        return TR$.MODULE$.highscores_back();
    }

    public static final TypedResource<TextView> highscores_text() {
        return TR$.MODULE$.highscores_text();
    }

    public static final TypedResource<RelativeLayout> layout_highscores() {
        return TR$.MODULE$.layout_highscores();
    }

    public static final TypedResource<RelativeLayout> layout_home() {
        return TR$.MODULE$.layout_home();
    }

    public static final TypedResource<RelativeLayout> layout_settings() {
        return TR$.MODULE$.layout_settings();
    }

    public static final TypedResource<Button> new_game() {
        return TR$.MODULE$.new_game();
    }

    public static final TypedResource<Button> ok() {
        return TR$.MODULE$.ok();
    }

    public static final TypedResource<Button> reset() {
        return TR$.MODULE$.reset();
    }

    public static final TypedResource<Button> settings() {
        return TR$.MODULE$.settings();
    }

    public static final TypedResource<Button> settings_back() {
        return TR$.MODULE$.settings_back();
    }

    public static final TypedResource<Button> show_menu() {
        return TR$.MODULE$.show_menu();
    }

    public static final TypedResource<CheckBox> sound() {
        return TR$.MODULE$.sound();
    }

    public static final TypedResource<Button> try_again() {
        return TR$.MODULE$.try_again();
    }

    public static final TypedResource<Button> tutorial() {
        return TR$.MODULE$.tutorial();
    }

    public static final TypedResource<EditText> username() {
        return TR$.MODULE$.username();
    }

    public static final TypedResource<Button> version() {
        return TR$.MODULE$.version();
    }
}
