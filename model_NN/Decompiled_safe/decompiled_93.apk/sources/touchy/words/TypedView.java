package touchy.words;

import scala.ScalaObject;

/* compiled from: TR.scala */
public interface TypedView extends ScalaObject, TypedViewHolder {
    TypedView view();

    /* renamed from: touchy.words.TypedView$class  reason: invalid class name */
    /* compiled from: TR.scala */
    public abstract class Cclass {
        public static void $init$(TypedView $this) {
        }

        public static TypedView view(TypedView $this) {
            return $this;
        }
    }
}
