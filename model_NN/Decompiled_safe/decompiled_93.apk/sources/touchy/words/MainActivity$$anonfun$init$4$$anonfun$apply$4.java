package touchy.words;

import android.widget.TextView;
import java.io.Serializable;
import scala.runtime.AbstractFunction1;

/* compiled from: Activity.scala */
public final class MainActivity$$anonfun$init$4$$anonfun$apply$4 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    private final /* synthetic */ MainActivity$$anonfun$init$4 $outer;
    private final /* synthetic */ TextView text$1;

    public MainActivity$$anonfun$init$4$$anonfun$apply$4(MainActivity$$anonfun$init$4 $outer2, TextView textView) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.text$1 = textView;
    }

    public final void apply(Object x) {
        this.text$1.setText(this.$outer.touchy$words$MainActivity$$anonfun$$$outer().game().fullStats((String) x));
    }
}
