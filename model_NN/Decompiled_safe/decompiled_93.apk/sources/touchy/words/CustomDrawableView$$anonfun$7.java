package touchy.words;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

/* compiled from: Custom.scala */
public final class CustomDrawableView$$anonfun$7 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;

    public CustomDrawableView$$anonfun$7(CustomDrawableView $outer) {
    }

    public final int apply(Word word) {
        return word.price();
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return BoxesRunTime.boxToInteger(apply((Word) v1));
    }
}
