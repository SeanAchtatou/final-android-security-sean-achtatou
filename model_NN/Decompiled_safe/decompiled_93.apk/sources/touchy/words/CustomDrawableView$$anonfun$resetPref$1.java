package touchy.words;

import android.content.SharedPreferences;
import java.io.Serializable;
import scala.runtime.AbstractFunction1;

/* compiled from: Custom.scala */
public final class CustomDrawableView$$anonfun$resetPref$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    private final /* synthetic */ SharedPreferences.Editor e$2;

    public CustomDrawableView$$anonfun$resetPref$1(CustomDrawableView $outer, SharedPreferences.Editor editor) {
        this.e$2 = editor;
    }

    public final SharedPreferences.Editor apply(String name) {
        return this.e$2.putInt(name, 0);
    }
}
