package touchy.words;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

/* compiled from: Custom.scala */
public final class CustomDrawableView$$anonfun$buildQueue$1$$anonfun$4 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;

    public CustomDrawableView$$anonfun$buildQueue$1$$anonfun$4(CustomDrawableView$$anonfun$buildQueue$1 $outer) {
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return BoxesRunTime.boxToBoolean(apply((Letter) v1));
    }

    public final boolean apply(Letter x) {
        return GameState$.MODULE$.vowels().contains(x.c());
    }
}
