package touchy.words;

import android.app.Dialog;
import android.view.View;
import java.io.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxedUnit;

/* compiled from: Activity.scala */
public final class MainActivity$$anonfun$onCreateDialog$2 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    private final /* synthetic */ Dialog dialog$1;

    public MainActivity$$anonfun$onCreateDialog$2(MainActivity $outer, Dialog dialog) {
        this.dialog$1 = dialog;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        apply((View) v1);
        return BoxedUnit.UNIT;
    }

    public final void apply(View v) {
        this.dialog$1.cancel();
    }
}
