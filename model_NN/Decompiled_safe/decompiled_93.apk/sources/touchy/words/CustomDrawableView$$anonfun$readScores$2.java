package touchy.words;

import java.io.Serializable;
import scala.Predef$;
import scala.collection.immutable.Map;
import scala.runtime.AbstractFunction1;

/* compiled from: Custom.scala */
public final class CustomDrawableView$$anonfun$readScores$2 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;

    public CustomDrawableView$$anonfun$readScores$2(CustomDrawableView $outer) {
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return apply((Map<String, Object>) ((Map) v1));
    }

    public final String apply(Map<String, Object> x) {
        return Predef$.MODULE$.augmentString("%-10s %3s").format(Predef$.MODULE$.genericWrapArray(new Object[]{x.apply("name"), x.apply("score")}));
    }
}
