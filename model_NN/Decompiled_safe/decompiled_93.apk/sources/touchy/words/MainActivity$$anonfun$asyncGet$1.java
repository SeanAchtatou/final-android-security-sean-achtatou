package touchy.words;

import java.io.Serializable;
import scala.Function0;
import scala.Function1;
import scala.collection.immutable.Map;
import scala.runtime.AbstractFunction0;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

/* compiled from: Activity.scala */
public final class MainActivity$$anonfun$asyncGet$1 extends AbstractFunction0 implements Serializable {
    public static final long serialVersionUID = 0;
    private final /* synthetic */ MainActivity $outer;
    private final /* synthetic */ Map args$2;
    private final /* synthetic */ boolean auth$1;
    private final /* synthetic */ Function0 error$2;
    private final /* synthetic */ Function1 ok$2;
    private final /* synthetic */ String path$2;

    public MainActivity$$anonfun$asyncGet$1(MainActivity $outer2, String str, Map map, Function1 function1, Function0 function0, boolean z) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.path$2 = str;
        this.args$2 = map;
        this.ok$2 = function1;
        this.error$2 = function0;
        this.auth$1 = z;
    }

    public final Object apply() {
        try {
            if (this.$outer.hasInternet()) {
                if (this.auth$1 && this.$outer.logged() == 0) {
                    this.$outer.login();
                }
                this.$outer.onUiThread$1(this.ok$2, this.$outer.getUrl(this.path$2, this.args$2, this.$outer.getUrl$default$3()));
                return BoxedUnit.UNIT;
            }
            throw new Exception("No connectivity");
        } catch (Exception e) {
            this.$outer.failure$1(this.error$2);
            return BoxesRunTime.boxToInteger(GameState$.MODULE$.log(e));
        }
    }
}
