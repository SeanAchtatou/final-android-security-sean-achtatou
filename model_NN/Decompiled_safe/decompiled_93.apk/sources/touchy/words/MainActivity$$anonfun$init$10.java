package touchy.words;

import android.view.View;
import java.io.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

/* compiled from: Activity.scala */
public final class MainActivity$$anonfun$init$10 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    private final /* synthetic */ MainActivity $outer;

    public MainActivity$$anonfun$init$10(MainActivity $outer2) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        apply((View) v1);
        return BoxedUnit.UNIT;
    }

    public final void apply(View v) {
        this.$outer.saveSettings("tutorial", BoxesRunTime.boxToInteger(1), this.$outer.saveSettings$default$3(), this.$outer.saveSettings$default$4());
        this.$outer.showToast("Saved!");
    }
}
