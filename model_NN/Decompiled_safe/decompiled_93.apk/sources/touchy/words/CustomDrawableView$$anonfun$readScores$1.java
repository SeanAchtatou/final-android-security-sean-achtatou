package touchy.words;

import java.io.Serializable;
import org.json.JSONArray;
import org.json.JSONObject;
import scala.Predef$;
import scala.Tuple2;
import scala.collection.Seq;
import scala.collection.Seq$;
import scala.collection.immutable.Map;
import scala.runtime.AbstractFunction1$mcVI$sp;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.ObjectRef;

/* compiled from: Custom.scala */
public final class CustomDrawableView$$anonfun$readScores$1 extends AbstractFunction1$mcVI$sp implements Serializable {
    public static final long serialVersionUID = 0;
    private final /* synthetic */ JSONArray array$1;
    private final /* synthetic */ ObjectRef res$1;

    public CustomDrawableView$$anonfun$readScores$1(CustomDrawableView $outer, JSONArray jSONArray, ObjectRef objectRef) {
        this.array$1 = jSONArray;
        this.res$1 = objectRef;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        apply(BoxesRunTime.unboxToInt(v1));
        return BoxedUnit.UNIT;
    }

    public final void apply(int i) {
        apply$mcVI$sp(i);
    }

    public void apply$mcVI$sp(int v1) {
        JSONObject x = this.array$1.getJSONObject(v1);
        this.res$1.elem = (Seq) ((Seq) this.res$1.elem).$plus$plus(Seq$.MODULE$.apply(Predef$.MODULE$.wrapRefArray((Object[]) new Map[]{(Map) Predef$.MODULE$.Map().apply(Predef$.MODULE$.wrapRefArray((Object[]) new Tuple2[]{Predef$.MODULE$.any2ArrowAssoc("name").$minus$greater(x.get("name")), Predef$.MODULE$.any2ArrowAssoc("score").$minus$greater(x.get("score"))}))})), Seq$.MODULE$.canBuildFrom());
    }
}
