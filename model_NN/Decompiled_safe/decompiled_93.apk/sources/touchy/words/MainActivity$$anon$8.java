package touchy.words;

import scala.Function1;

/* compiled from: Activity.scala */
public final class MainActivity$$anon$8 implements Runnable {
    private final /* synthetic */ Function1 f$2;
    private final /* synthetic */ Object res$2;

    public MainActivity$$anon$8(MainActivity $outer, Function1 function1, Object obj) {
        this.f$2 = function1;
        this.res$2 = obj;
    }

    public void run() {
        this.f$2.apply(this.res$2);
    }
}
