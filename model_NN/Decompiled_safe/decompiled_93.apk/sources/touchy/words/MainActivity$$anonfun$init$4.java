package touchy.words;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.io.Serializable;
import scala.Function0;
import scala.Function1;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxedUnit;

/* compiled from: Activity.scala */
public final class MainActivity$$anonfun$init$4 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    private final /* synthetic */ MainActivity $outer;
    private final /* synthetic */ RelativeLayout layoutHS$1;

    public MainActivity$$anonfun$init$4(MainActivity $outer2, RelativeLayout relativeLayout) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.layoutHS$1 = relativeLayout;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        apply((View) v1);
        return BoxedUnit.UNIT;
    }

    public /* synthetic */ MainActivity touchy$words$MainActivity$$anonfun$$$outer() {
        return this.$outer;
    }

    public final void apply(View v) {
        this.$outer.layoutHome().setVisibility(4);
        this.layoutHS$1.setVisibility(0);
        TextView text$1 = (TextView) this.$outer.getResource(R.id.highscores_text, this.$outer.getResource$default$2());
        CustomDrawableView qual$1 = this.$outer.game();
        text$1.setText(qual$1.fullStats(qual$1.fullStats$default$1()));
        Function1 x$5 = new MainActivity$$anonfun$init$4$$anonfun$apply$4(this, text$1);
        Function0 x$6 = new MainActivity$$anonfun$init$4$$anonfun$apply$1(this, text$1);
        this.$outer.asyncGet("get-scores", this.$outer.asyncGet$default$2(), x$5, x$6, this.$outer.asyncGet$default$5());
    }
}
