package touchy.words;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

/* compiled from: Custom.scala */
public final class CustomDrawableView$$anonfun$screenGame$6 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;

    public CustomDrawableView$$anonfun$screenGame$6(CustomDrawableView $outer) {
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return BoxesRunTime.boxToBoolean(apply((Word) v1));
    }

    public final boolean apply(Word x) {
        String word = x.word();
        if (word != null ? !word.equals("") : "" != 0) {
            if (x.price() > 0) {
                return true;
            }
        }
        return false;
    }
}
