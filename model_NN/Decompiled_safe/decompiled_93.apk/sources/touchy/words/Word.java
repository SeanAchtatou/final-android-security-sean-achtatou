package touchy.words;

import scala.Predef$;
import scala.ScalaObject;
import scala.collection.mutable.Stack;
import scala.runtime.BoxesRunTime;

/* compiled from: Data.scala */
public class Word implements ScalaObject {
    private final String a;
    private final int price;
    private final String str;
    private final String word;

    public static final Stack init$default$1() {
        return Word$.MODULE$.init$default$1();
    }

    public static final String init$default$2() {
        return Word$.MODULE$.init$default$2();
    }

    public static final int init$default$3() {
        return Word$.MODULE$.init$default$3();
    }

    public Word(Stack<Letter> w, String action, int bonus) {
        int wordScore;
        String format;
        this.word = w == null ? "" : GameState$.MODULE$.wordToStr(w);
        this.a = action;
        if (action != null ? !action.equals("") : "" != 0) {
            wordScore = (action != null ? !action.equals("empty") : "empty" != 0) ? (action != null ? !action.equals("chain") : "chain" != 0) ? 0 : bonus : bonus;
        } else {
            wordScore = GameState$.MODULE$.wordScore(w);
        }
        this.price = wordScore;
        if (action != null ? action.equals("") : "" == 0) {
            format = Predef$.MODULE$.augmentString(" %-8s %9s").format(Predef$.MODULE$.genericWrapArray(new Object[]{word(), Predef$.MODULE$.augmentString("%sx%s = %2s").format(Predef$.MODULE$.genericWrapArray(new Object[]{BoxesRunTime.boxToInteger(price() / BoxesRunTime.unboxToInt(GameState$.MODULE$.multipliers().apply(word().length()))), GameState$.MODULE$.multipliers().apply(word().length()), BoxesRunTime.boxToInteger(price())}))}));
        } else if (action != null ? !action.equals("empty") : "empty" != 0) {
            format = (action != null ? !action.equals("chain") : "chain" != 0) ? (action != null ? !action.equals("unavail") : "unavail" != 0) ? (action != null ? !action.equals("lost") : "lost" != 0) ? "" : Predef$.MODULE$.augmentString(" %s is not used").format(Predef$.MODULE$.genericWrapArray(new Object[]{word()})) : Predef$.MODULE$.augmentString(" %s not avail.").format(Predef$.MODULE$.genericWrapArray(new Object[]{word()})) : Predef$.MODULE$.augmentString(" Word chain! +%s").format(Predef$.MODULE$.genericWrapArray(new Object[]{BoxesRunTime.boxToInteger(bonus)}));
        } else {
            format = Predef$.MODULE$.augmentString(" Empty row bonus %s").format(Predef$.MODULE$.genericWrapArray(new Object[]{BoxesRunTime.boxToInteger(bonus)}));
        }
        this.str = format;
    }

    public String word() {
        return this.word;
    }

    public String a() {
        return this.a;
    }

    public int price() {
        return this.price;
    }

    public String str() {
        return this.str;
    }

    public String toString() {
        return str();
    }
}
