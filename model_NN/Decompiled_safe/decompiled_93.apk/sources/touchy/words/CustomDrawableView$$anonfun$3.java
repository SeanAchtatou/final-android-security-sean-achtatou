package touchy.words;

import android.graphics.Canvas;
import java.io.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxedUnit;

/* compiled from: Custom.scala */
public final class CustomDrawableView$$anonfun$3 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    private final /* synthetic */ CustomDrawableView $outer;

    public CustomDrawableView$$anonfun$3(CustomDrawableView $outer2) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        apply((Canvas) v1);
        return BoxedUnit.UNIT;
    }

    public final void apply(Canvas canvas) {
        this.$outer.screenSplash(canvas);
    }
}
