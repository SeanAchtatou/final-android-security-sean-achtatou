package touchy.words;

import android.app.ProgressDialog;
import java.io.Serializable;
import scala.runtime.AbstractFunction1;

/* compiled from: Activity.scala */
public final class MainActivity$$anonfun$onCreateDialog$1$$anonfun$apply$5 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    private final /* synthetic */ MainActivity$$anonfun$onCreateDialog$1 $outer;
    private final /* synthetic */ ProgressDialog progressDialog$1;
    private final /* synthetic */ String username$1;

    public MainActivity$$anonfun$onCreateDialog$1$$anonfun$apply$5(MainActivity$$anonfun$onCreateDialog$1 $outer2, String str, ProgressDialog progressDialog) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.username$1 = str;
        this.progressDialog$1 = progressDialog;
    }

    public final void apply(Object x) {
        this.progressDialog$1.cancel();
        if (x != null ? !x.equals("failed") : "failed" != 0) {
            this.$outer.touchy$words$MainActivity$$anonfun$$$outer().saveSettings("username", this.username$1, "password", (String) x);
            this.$outer.touchy$words$MainActivity$$anonfun$$$outer().authGet(this.$outer.touchy$words$MainActivity$$anonfun$$$outer().nextDest().copy$default$1(), this.$outer.touchy$words$MainActivity$$anonfun$$$outer().nextDest().copy$default$2(), this.$outer.touchy$words$MainActivity$$anonfun$$$outer().nextDest().copy$default$3(), this.$outer.touchy$words$MainActivity$$anonfun$$$outer().nextDest().copy$default$4());
            this.$outer.dialog$1.cancel();
            return;
        }
        this.$outer.touchy$words$MainActivity$$anonfun$$$outer().showToast("This username already exists. Pick another one.");
    }
}
