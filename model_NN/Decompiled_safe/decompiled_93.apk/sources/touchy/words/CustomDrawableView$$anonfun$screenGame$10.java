package touchy.words;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

/* compiled from: Custom.scala */
public final class CustomDrawableView$$anonfun$screenGame$10 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;

    public CustomDrawableView$$anonfun$screenGame$10(CustomDrawableView $outer) {
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return apply(BoxesRunTime.unboxToChar(v1));
    }

    public final String apply(char c) {
        return BoxesRunTime.boxToCharacter(c).toString();
    }
}
