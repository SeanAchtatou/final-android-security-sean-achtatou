package touchy.words;

import scala.ScalaObject;
import scala.runtime.BoxesRunTime;

/* compiled from: Data.scala */
public class Letter implements ScalaObject {
    private final String bonus;
    private final String c;
    private final int price = BoxesRunTime.unboxToInt(GameState$.MODULE$.letterPrices().apply(c()));

    public static final boolean init$default$1() {
        return Letter$.MODULE$.init$default$1();
    }

    public static final boolean init$default$2() {
        return Letter$.MODULE$.init$default$2();
    }

    public static final int init$default$3() {
        return Letter$.MODULE$.init$default$3();
    }

    public Letter(boolean consonant, boolean vowel, int left) {
        this.c = getLetter(consonant, vowel);
        this.bonus = GameState$.MODULE$.bonuses()[left];
    }

    public String c() {
        return this.c;
    }

    public String getLetter(boolean consonant, boolean vowel) {
        String x = GameState$.MODULE$.lettersRandom().apply(GameState$.MODULE$.random().nextInt(GameState$.MODULE$.letterOccurenciesTotal()));
        if ((!consonant || !vowel) && (consonant || vowel)) {
            if (consonant) {
                while (GameState$.MODULE$.vowels().contains(x)) {
                    x = GameState$.MODULE$.lettersRandom().apply(GameState$.MODULE$.random().nextInt(GameState$.MODULE$.letterOccurenciesTotal()));
                }
            } else {
                while (!GameState$.MODULE$.vowels().contains(x)) {
                    x = GameState$.MODULE$.lettersRandom().apply(GameState$.MODULE$.random().nextInt(GameState$.MODULE$.letterOccurenciesTotal()));
                }
            }
        }
        return x;
    }

    public String toString() {
        return c();
    }

    public int price() {
        return this.price;
    }

    public String bonus() {
        return this.bonus;
    }
}
