package touchy.words;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;

/* compiled from: Data.scala */
public final class GameState$$anonfun$wordToStr$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;

    public final String apply(Letter letter) {
        return letter.c();
    }
}
