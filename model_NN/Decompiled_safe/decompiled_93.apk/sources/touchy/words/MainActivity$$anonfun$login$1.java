package touchy.words;

import java.io.Serializable;
import java.util.List;
import org.apache.http.cookie.Cookie;
import scala.runtime.AbstractFunction1$mcVI$sp;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

/* compiled from: Activity.scala */
public final class MainActivity$$anonfun$login$1 extends AbstractFunction1$mcVI$sp implements Serializable {
    public static final long serialVersionUID = 0;
    private final /* synthetic */ MainActivity $outer;
    private final /* synthetic */ List c$1;

    public MainActivity$$anonfun$login$1(MainActivity $outer2, List list) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.c$1 = list;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        apply(BoxesRunTime.unboxToInt(v1));
        return BoxedUnit.UNIT;
    }

    public final void apply(int i) {
        apply$mcVI$sp(i);
    }

    public void apply$mcVI$sp(int v1) {
        String name = ((Cookie) this.c$1.get(v1)).getName();
        if (name == null) {
            if ("sessionid" != 0) {
                return;
            }
        } else if (!name.equals("sessionid")) {
            return;
        }
        this.$outer.saveSettings("sessionid", ((Cookie) this.c$1.get(v1)).getValue(), this.$outer.saveSettings$default$3(), this.$outer.saveSettings$default$4());
    }
}
