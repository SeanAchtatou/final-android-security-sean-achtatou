package touchy.words;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BooleanRef;
import scala.runtime.BoxedUnit;

/* compiled from: Custom.scala */
public final class CustomDrawableView$$anonfun$validateWord$2 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    private final /* synthetic */ CustomDrawableView $outer;
    private final /* synthetic */ BooleanRef break$1;

    public CustomDrawableView$$anonfun$validateWord$2(CustomDrawableView $outer2, BooleanRef booleanRef) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.break$1 = booleanRef;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        apply((Word) v1);
        return BoxedUnit.UNIT;
    }

    public final void apply(Word w) {
        String word = w.word();
        if (word == null) {
            if ("" == 0) {
                return;
            }
        } else if (word.equals("")) {
            return;
        }
        if (!this.break$1.elem) {
            if (w.price() == 0 || w.word().length() <= 4 || this.$outer.history().apply(0).word().length() <= 4) {
                this.$outer.chain_$eq(0);
            } else {
                this.$outer.chain_$eq(this.$outer.chain() + 1);
                this.$outer.history().push(new Word(Word$.MODULE$.init$default$1(), "chain", this.$outer.history().apply(0).word().length() * this.$outer.chain()));
                this.$outer.score_$eq(this.$outer.history().apply(0).price() + this.$outer.score());
            }
            this.break$1.elem = true;
        }
    }
}
