package touchy.words;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

/* compiled from: Data.scala */
public final class GameState$$anonfun$wordScore$2 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;

    public final int apply(Letter x) {
        int i;
        int price = x.price();
        String bonus = x.bonus();
        if (bonus != null ? bonus.equals("double") : "double" == 0) {
            i = 2;
        } else if (bonus != null ? !bonus.equals("triple") : "triple" != 0) {
            i = 1;
        } else {
            i = 3;
        }
        return price * i;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return BoxesRunTime.boxToInteger(apply((Letter) v1));
    }
}
