package touchy.words;

import android.view.View;
import scala.ScalaObject;

/* compiled from: TR.scala */
public interface TypedViewHolder extends ScalaObject {
    <T> T findView(TypedResource<T> typedResource);

    View view();

    /* renamed from: touchy.words.TypedViewHolder$class  reason: invalid class name */
    /* compiled from: TR.scala */
    public abstract class Cclass {
        public static void $init$(TypedViewHolder $this) {
        }

        public static Object findView(TypedViewHolder $this, TypedResource tr) {
            return $this.view().findViewById(tr.copy$default$1());
        }
    }
}
