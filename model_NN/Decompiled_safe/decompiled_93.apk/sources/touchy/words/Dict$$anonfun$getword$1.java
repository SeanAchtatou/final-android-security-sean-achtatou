package touchy.words;

import java.io.Serializable;
import scala.Predef$;
import scala.runtime.AbstractFunction1$mcVI$sp;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.NonLocalReturnControl;

/* compiled from: Data.scala */
public final class Dict$$anonfun$getword$1 extends AbstractFunction1$mcVI$sp implements Serializable {
    public static final long serialVersionUID = 0;
    private final /* synthetic */ Object nonLocalReturnKey1$1;
    private final /* synthetic */ int pivot$1;
    private final /* synthetic */ String words$1;

    public Dict$$anonfun$getword$1(int i, String str, Object obj) {
        this.pivot$1 = i;
        this.words$1 = str;
        this.nonLocalReturnKey1$1 = obj;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        apply(BoxesRunTime.unboxToInt(v1));
        return BoxedUnit.UNIT;
    }

    public final void apply(int i) {
        apply$mcVI$sp(i);
    }

    public void apply$mcVI$sp(int v1) {
        if (Predef$.MODULE$.augmentString(this.words$1).apply(this.pivot$1 - v1) == 10) {
            int next = this.words$1.indexOf(10, this.pivot$1 + 1);
            if (next == -1) {
                throw new NonLocalReturnControl(this.nonLocalReturnKey1$1, "");
            }
            throw new NonLocalReturnControl(this.nonLocalReturnKey1$1, this.words$1.substring((this.pivot$1 - v1) + 1, next));
        } else if (this.pivot$1 - v1 <= 0) {
            int next2 = this.words$1.indexOf(10, this.pivot$1);
            if (next2 == -1) {
                throw new NonLocalReturnControl(this.nonLocalReturnKey1$1, "");
            }
            throw new NonLocalReturnControl(this.nonLocalReturnKey1$1, this.words$1.substring(0, next2));
        }
    }
}
