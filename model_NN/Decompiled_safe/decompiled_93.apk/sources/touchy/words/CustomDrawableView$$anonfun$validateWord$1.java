package touchy.words;

import java.io.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

/* compiled from: Custom.scala */
public final class CustomDrawableView$$anonfun$validateWord$1 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    private final /* synthetic */ CustomDrawableView $outer;

    public CustomDrawableView$$anonfun$validateWord$1(CustomDrawableView $outer2) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return BoxesRunTime.boxToBoolean(apply((Letter) v1));
    }

    public final boolean apply(Letter x) {
        return this.$outer.currentWord().contains(x);
    }
}
