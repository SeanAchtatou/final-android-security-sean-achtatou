package touchy.words;

import android.widget.Button;
import java.io.Serializable;
import org.json.JSONObject;
import scala.runtime.AbstractFunction1;

/* compiled from: Activity.scala */
public final class MainActivity$$anonfun$init$2 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    private final /* synthetic */ MainActivity $outer;

    public MainActivity$$anonfun$init$2(MainActivity $outer2) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
    }

    public /* synthetic */ MainActivity touchy$words$MainActivity$$anonfun$$$outer() {
        return this.$outer;
    }

    public final void apply(Object x) {
        try {
            JSONObject res$1 = (JSONObject) x;
            if (res$1.has("message")) {
                Button button = this.$outer.getButton(R.id.version, this.$outer.getButton$default$2());
                button.setVisibility(0);
                button.setText(res$1.getString("message"));
                this.$outer.setOnClick(button, new MainActivity$$anonfun$init$2$$anonfun$apply$3(this, res$1));
            }
        } catch (Exception e) {
            GameState$.MODULE$.log(e);
        }
    }
}
