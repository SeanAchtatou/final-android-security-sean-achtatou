package touchy.words;

import android.content.DialogInterface;

/* compiled from: Activity.scala */
public final class MainActivity$$anon$7 implements DialogInterface.OnClickListener {
    private final /* synthetic */ MainActivity $outer;

    public MainActivity$$anon$7(MainActivity $outer2) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
    }

    public void onClick(DialogInterface dialog, int id) {
        this.$outer.openUrl("http://www.touchywords.com");
        dialog.cancel();
    }
}
