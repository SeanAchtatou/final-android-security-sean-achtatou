package touchy.words;

import java.io.Serializable;
import scala.collection.immutable.List;
import scala.collection.immutable.List$;
import scala.runtime.AbstractFunction2;

/* compiled from: Data.scala */
public final class GameState$$anonfun$5 extends AbstractFunction2 implements Serializable {
    public static final long serialVersionUID = 0;

    public final /* bridge */ /* synthetic */ Object apply(Object v1, Object v2) {
        return apply((List<String>) ((List) v1), (List<String>) ((List) v2));
    }

    public final List<String> apply(List<String> list, List<String> list2) {
        return (List) list.$plus$plus(list2, List$.MODULE$.canBuildFrom());
    }
}
