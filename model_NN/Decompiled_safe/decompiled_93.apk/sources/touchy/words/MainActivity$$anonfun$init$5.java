package touchy.words;

import android.view.View;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import java.io.Serializable;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxedUnit;

/* compiled from: Activity.scala */
public final class MainActivity$$anonfun$init$5 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    private final /* synthetic */ MainActivity $outer;
    private final /* synthetic */ CheckBox checkBox$1;
    private final /* synthetic */ RelativeLayout layoutSettings$1;

    public MainActivity$$anonfun$init$5(MainActivity $outer2, RelativeLayout relativeLayout, CheckBox checkBox) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.layoutSettings$1 = relativeLayout;
        this.checkBox$1 = checkBox;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        apply((View) v1);
        return BoxedUnit.UNIT;
    }

    public final void apply(View v) {
        this.$outer.layoutHome().setVisibility(4);
        this.layoutSettings$1.setVisibility(0);
        CheckBox checkBox = this.checkBox$1;
        String hasSound = this.$outer.hasSound();
        checkBox.setChecked(hasSound != null ? hasSound.equals("yes") : "yes" == 0);
    }
}
