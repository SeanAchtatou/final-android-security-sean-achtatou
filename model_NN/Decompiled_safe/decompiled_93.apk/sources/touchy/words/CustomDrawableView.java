package touchy.words;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.CountDownTimer;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import java.util.Date;
import org.json.JSONArray;
import scala.Function1;
import scala.MatchError;
import scala.Predef$;
import scala.ScalaObject;
import scala.Tuple2;
import scala.Tuple2$mcII$sp;
import scala.collection.IterableLike;
import scala.collection.LinearSeqOptimized;
import scala.collection.Seq;
import scala.collection.Seq$;
import scala.collection.SeqLike;
import scala.collection.TraversableLike;
import scala.collection.TraversableOnce;
import scala.collection.immutable.IndexedSeq;
import scala.collection.immutable.IndexedSeq$;
import scala.collection.immutable.List$;
import scala.collection.immutable.Map;
import scala.collection.immutable.Range;
import scala.collection.immutable.StringOps;
import scala.collection.mutable.LinearSeq$;
import scala.collection.mutable.MutableList;
import scala.collection.mutable.Queue;
import scala.collection.mutable.Queue$;
import scala.collection.mutable.Stack;
import scala.collection.mutable.Stack$;
import scala.collection.mutable.StringBuilder;
import scala.math.Numeric$IntIsIntegral$;
import scala.math.Ordering$Int$;
import scala.runtime.BooleanRef;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;
import scala.runtime.ObjectRef;

/* compiled from: Custom.scala */
public class CustomDrawableView extends View implements ScalaObject {
    private final MainActivity activity;
    private Bitmap background;
    private Bitmap backgroundMenu;
    private int bonusTop;
    private int cardH;
    private int cardTop;
    private int chain;
    private final Context context;
    private int counterDuration;
    private final Stack<Letter> currentWord;
    private String debug = "";
    private final RectF deleteButton;
    private int h;
    private boolean hasPaused;
    private Seq<String> helpRight;
    private final Stack<Word> history;
    private final Queue<Letter> letters;
    private int lettersLeftLeft;
    private int lettersLeftTop;
    private Bitmap logo;
    private int margin;
    private final RectF okButton;
    private final Paint pCurWord;
    private final Paint pDebug;
    private final Paint pHistory;
    private final Paint pLetter;
    private final Paint pLetterBg;
    private final Paint pLetterBgPause;
    private final Paint pLetterBgSel;
    private final Paint pLettersLeft;
    private final Paint pPrice;
    private final Paint pScore;
    private final Paint pTimer;
    private final Paint pTimerWarn;
    private final Paint pUpcoming;
    private boolean paused;
    private boolean playing;
    private int score;
    private int scoreLeft;
    private int scoreTop;
    private Function1<Canvas, Object> screen;
    private int side;
    private float tickTime;
    private Date timeBegin;
    private CountDownTimer timer;
    private final RectF timerRect;
    private Queue<Letter> upcoming;
    private int w;
    private int wordLeft;
    private int wordLength;
    private int wordTop;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CustomDrawableView(Context context2, AttributeSet attributeSet) {
        super(context2, attributeSet);
        this.context = context2;
        this.activity = (MainActivity) context2;
        this.deleteButton = new RectF();
        this.okButton = new RectF();
        this.timerRect = new RectF();
        this.cardTop = 0;
        this.margin = 0;
        this.side = 0;
        this.cardH = 0;
        this.h = 0;
        this.w = 0;
        this.wordTop = 0;
        this.wordLeft = 0;
        this.scoreTop = 0;
        this.scoreLeft = 0;
        this.bonusTop = 0;
        this.lettersLeftLeft = 0;
        this.lettersLeftTop = 0;
        this.logo = null;
        this.backgroundMenu = null;
        this.background = null;
        this.pDebug = easyPaint("f0f0", easyPaint$default$2());
        this.pLetter = easyPaint(easyPaint$default$1(), easyPaint$default$2());
        this.pPrice = easyPaint("f000", easyPaint$default$2());
        this.pHistory = easyPaint("ffff", easyPaint$default$2());
        this.pScore = easyPaint("fff0", easyPaint$default$2());
        this.pLettersLeft = easyPaint("f999", easyPaint$default$2());
        this.pUpcoming = easyPaint("ffff", easyPaint$default$2());
        this.pCurWord = easyPaint("fff0", easyPaint$default$2());
        this.pTimer = easyPaint("fff0", easyPaint$default$2());
        this.pTimerWarn = easyPaint("ff00", easyPaint$default$2());
        this.pLetterBg = easyPaint("ecfc", easyPaint$default$2());
        this.pLetterBgPause = easyPaint("efff", easyPaint$default$2());
        this.pLetterBgSel = easyPaint("efcc", easyPaint$default$2());
        this.screen = new CustomDrawableView$$anonfun$3(this);
        this.letters = Queue$.MODULE$.apply(Predef$.MODULE$.wrapRefArray((Object[]) new Letter[0]));
        this.upcoming = Queue$.MODULE$.apply(Predef$.MODULE$.wrapRefArray((Object[]) new Letter[0]));
        this.currentWord = Stack$.MODULE$.apply(Predef$.MODULE$.wrapRefArray((Object[]) new Letter[0]));
        this.history = Stack$.MODULE$.apply(Predef$.MODULE$.wrapRefArray((Object[]) new Word[0]));
        this.tickTime = 0.0f;
        this.score = 0;
        this.playing = false;
        this.hasPaused = false;
        this.paused = false;
        this.helpRight = (Seq) Seq$.MODULE$.apply(Predef$.MODULE$.wrapRefArray((Object[]) new String[0]));
        this.chain = 0;
        this.wordLength = 0;
    }

    public String debug() {
        return this.debug;
    }

    public void debug_$eq(String str) {
        this.debug = str;
    }

    public MainActivity activity() {
        return this.activity;
    }

    public RectF deleteButton() {
        return this.deleteButton;
    }

    public RectF okButton() {
        return this.okButton;
    }

    public RectF timerRect() {
        return this.timerRect;
    }

    public int bonusTop() {
        return this.bonusTop;
    }

    public void bonusTop_$eq(int i) {
        this.bonusTop = i;
    }

    public int cardH() {
        return this.cardH;
    }

    public void cardH_$eq(int i) {
        this.cardH = i;
    }

    public int cardTop() {
        return this.cardTop;
    }

    public void cardTop_$eq(int i) {
        this.cardTop = i;
    }

    public int h() {
        return this.h;
    }

    public void h_$eq(int i) {
        this.h = i;
    }

    public int lettersLeftLeft() {
        return this.lettersLeftLeft;
    }

    public void lettersLeftLeft_$eq(int i) {
        this.lettersLeftLeft = i;
    }

    public int lettersLeftTop() {
        return this.lettersLeftTop;
    }

    public void lettersLeftTop_$eq(int i) {
        this.lettersLeftTop = i;
    }

    public int margin() {
        return this.margin;
    }

    public void margin_$eq(int i) {
        this.margin = i;
    }

    public int scoreLeft() {
        return this.scoreLeft;
    }

    public void scoreLeft_$eq(int i) {
        this.scoreLeft = i;
    }

    public int scoreTop() {
        return this.scoreTop;
    }

    public void scoreTop_$eq(int i) {
        this.scoreTop = i;
    }

    public int side() {
        return this.side;
    }

    public void side_$eq(int i) {
        this.side = i;
    }

    public int w() {
        return this.w;
    }

    public void w_$eq(int i) {
        this.w = i;
    }

    public int wordLeft() {
        return this.wordLeft;
    }

    public void wordLeft_$eq(int i) {
        this.wordLeft = i;
    }

    public int wordTop() {
        return this.wordTop;
    }

    public void wordTop_$eq(int i) {
        this.wordTop = i;
    }

    public Bitmap logo() {
        return this.logo;
    }

    public void logo_$eq(Bitmap bitmap) {
        this.logo = bitmap;
    }

    public Bitmap backgroundMenu() {
        return this.backgroundMenu;
    }

    public void backgroundMenu_$eq(Bitmap bitmap) {
        this.backgroundMenu = bitmap;
    }

    public Bitmap background() {
        return this.background;
    }

    public void background_$eq(Bitmap bitmap) {
        this.background = bitmap;
    }

    public Paint pDebug() {
        return this.pDebug;
    }

    public Paint pLetter() {
        return this.pLetter;
    }

    public Paint pPrice() {
        return this.pPrice;
    }

    public Paint pHistory() {
        return this.pHistory;
    }

    public Paint pScore() {
        return this.pScore;
    }

    public Paint pLettersLeft() {
        return this.pLettersLeft;
    }

    public Paint pUpcoming() {
        return this.pUpcoming;
    }

    public Paint pCurWord() {
        return this.pCurWord;
    }

    public Paint pTimer() {
        return this.pTimer;
    }

    public Paint pTimerWarn() {
        return this.pTimerWarn;
    }

    public Paint pLetterBg() {
        return this.pLetterBg;
    }

    public Paint pLetterBgPause() {
        return this.pLetterBgPause;
    }

    public Paint pLetterBgSel() {
        return this.pLetterBgSel;
    }

    public Function1<Canvas, Object> screen() {
        return this.screen;
    }

    public void screen_$eq(Function1<Canvas, Object> function1) {
        this.screen = function1;
    }

    public Queue<Letter> letters() {
        return this.letters;
    }

    public Queue<Letter> upcoming() {
        return this.upcoming;
    }

    public void upcoming_$eq(Queue<Letter> queue) {
        this.upcoming = queue;
    }

    public Stack<Letter> currentWord() {
        return this.currentWord;
    }

    public Stack<Word> history() {
        return this.history;
    }

    public CountDownTimer timer() {
        return this.timer;
    }

    public void timer_$eq(CountDownTimer countDownTimer) {
        this.timer = countDownTimer;
    }

    public float tickTime() {
        return this.tickTime;
    }

    public void tickTime_$eq(float f) {
        this.tickTime = f;
    }

    public int score() {
        return this.score;
    }

    public void score_$eq(int i) {
        this.score = i;
    }

    public int counterDuration() {
        return this.counterDuration;
    }

    public void counterDuration_$eq(int i) {
        this.counterDuration = i;
    }

    public Date timeBegin() {
        return this.timeBegin;
    }

    public void timeBegin_$eq(Date date) {
        this.timeBegin = date;
    }

    public boolean playing() {
        return this.playing;
    }

    public void playing_$eq(boolean z) {
        this.playing = z;
    }

    public boolean hasPaused() {
        return this.hasPaused;
    }

    public void hasPaused_$eq(boolean z) {
        this.hasPaused = z;
    }

    public boolean paused() {
        return this.paused;
    }

    public void paused_$eq(boolean z) {
        this.paused = z;
    }

    public Seq<String> helpRight() {
        return this.helpRight;
    }

    public void helpRight_$eq(Seq<String> seq) {
        this.helpRight = seq;
    }

    public int chain() {
        return this.chain;
    }

    public void chain_$eq(int i) {
        this.chain = i;
    }

    public int wordLength() {
        return this.wordLength;
    }

    public void wordLength_$eq(int i) {
        this.wordLength = i;
    }

    public /* synthetic */ boolean startTimer$default$1() {
        return false;
    }

    public /* synthetic */ boolean startTimer$default$2() {
        return true;
    }

    public /* synthetic */ int startTimer$default$3() {
        return 0;
    }

    public void startTimer(boolean renew, boolean add, int customDuration) {
        int duration;
        int i;
        if (renew) {
            timer().cancel();
        }
        if (add) {
            BoxesRunTime.boxToBoolean(addLetter());
        } else {
            BoxedUnit boxedUnit = BoxedUnit.UNIT;
        }
        if (upcoming().length() > 0 || letters().length() > 0) {
            if (letters().length() <= (GameState$.MODULE$.numLetters() * 1) / 2) {
                duration = (GameState$.MODULE$.letterDuration() * 1) / 5;
            } else {
                duration = (GameState$.MODULE$.letterDuration() * letters().length()) / GameState$.MODULE$.numLetters();
            }
            if (upcoming().length() != 0 || duration >= GameState$.MODULE$.letterDuration() / 2) {
                i = duration;
            } else {
                i = GameState$.MODULE$.letterDuration() / 2;
            }
            counterDuration_$eq(i);
            timer_$eq(new CustomDrawableView$$anon$1(this));
            timer().start();
            return;
        }
        gameOver(gameOver$default$1());
    }

    public void start() {
        if (GameState$.MODULE$.debugging()) {
            tests();
        }
        new CustomDrawableView$$anon$2(this).start();
    }

    public void tests() {
        System.setProperty("networkaddress.cache.ttl", "0");
        System.setProperty("networkaddress.cache.negative.ttl", "0");
    }

    public void gameMenu() {
        activity().track("/game-menu");
        if (playing()) {
            gameOver(false);
        }
        screen_$eq(new CustomDrawableView$$anonfun$gameMenu$1(this));
        activity().showHome();
    }

    public void newGame() {
        if (activity().settings().getInt("tutorial", 1) == 1) {
            activity().showDialog(GameState$.MODULE$.TIPS());
        } else {
            beginGame();
        }
    }

    public void beginGame() {
        activity().track("/begin-game");
        if (timer() != null) {
            timer().cancel();
        }
        chain_$eq(0);
        playing_$eq(true);
        GameState$.MODULE$.buildBonuses();
        activity().layoutHome().setVisibility(4);
        timeBegin_$eq(new Date());
        letters().clear();
        upcoming_$eq(buildQueue(GameState$.MODULE$.lettersPerGame()));
        currentWord().clear();
        score_$eq(0);
        history().clear();
        screen_$eq(new CustomDrawableView$$anonfun$beginGame$1(this));
        hasPaused_$eq(false);
        paused_$eq(false);
        startTimer(startTimer$default$1(), startTimer$default$2(), startTimer$default$3());
    }

    public Queue<Letter> buildQueue(int size) {
        Queue queue$1 = Queue$.MODULE$.apply(Predef$.MODULE$.wrapRefArray((Object[]) new Letter[0]));
        Predef$.MODULE$.intWrapper(0).until(size).foreach(new CustomDrawableView$$anonfun$buildQueue$1(this, queue$1));
        return queue$1;
    }

    public void pauseGame() {
        if (timer() != null) {
            timer().cancel();
        }
        hasPaused_$eq(true);
        paused_$eq(true);
    }

    public void resumeGame() {
        startTimer(startTimer$default$1(), startTimer$default$2(), startTimer$default$3());
        timeBegin_$eq(new Date());
        paused_$eq(false);
    }

    public /* synthetic */ boolean gameOver$default$1() {
        return true;
    }

    public void gameOver(boolean z) {
        Word word;
        if (timer() != null) {
            timer().cancel();
        }
        playing_$eq(false);
        tickTime_$eq(0.0f);
        if (z) {
            activity().track("/game-over");
            activity().showDialog(GameState$.MODULE$.TRY_AGAIN());
            if (activity().hasInternet()) {
                if (history().isEmpty()) {
                    word = new Word(Word$.MODULE$.init$default$1(), Word$.MODULE$.init$default$2(), Word$.MODULE$.init$default$3());
                } else {
                    word = (Word) ((TraversableLike) history().sortBy(new CustomDrawableView$$anonfun$5(this), Ordering$Int$.MODULE$)).last();
                }
                activity().showToast("Loading online ranking...");
                activity().authGet("add-score", (Map) Predef$.MODULE$.Map().apply(Predef$.MODULE$.wrapRefArray((Object[]) new Tuple2[]{Predef$.MODULE$.any2ArrowAssoc("s").$minus$greater(BoxesRunTime.boxToInteger(hideScore(score())).toString()), Predef$.MODULE$.any2ArrowAssoc("word").$minus$greater(word.word())})), new CustomDrawableView$$anonfun$gameOver$2(this), new CustomDrawableView$$anonfun$gameOver$1(this));
            }
        }
    }

    public void onDraw(Canvas canvas) {
        if (h() == 0) {
            h_$eq(canvas.getHeight());
            w_$eq(canvas.getWidth());
        }
        screen().apply(canvas);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 1 && playing()) {
            float rawX = motionEvent.getRawX();
            float rawY = motionEvent.getRawY();
            debug_$eq(new StringBuilder().append(rawX).append((Object) " ").append(BoxesRunTime.boxToFloat(rawY)).toString());
            if (!paused()) {
                ((IterableLike) letters().zip(Predef$.MODULE$.intWrapper(0).to(20), LinearSeq$.MODULE$.canBuildFrom())).foreach(new CustomDrawableView$$anonfun$onTouchEvent$1(this, rawX, rawY));
                if (okButton().contains(rawX, rawY)) {
                    validateWord();
                }
                if (deleteButton().contains(rawX, rawY)) {
                    deleteLetter();
                }
                if (timerRect().contains(rawX, rawY)) {
                    passTimer();
                }
            } else if (new RectF(0.0f, (float) cardTop(), (float) w(), (float) (cardTop() + cardH())).contains(rawX, rawY)) {
                resumeGame();
            }
            invalidate();
        }
        return true;
    }

    public void screenMenu(Canvas canvas) {
        if (backgroundMenu() == null) {
            backgroundMenu_$eq(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(this.context.getResources(), R.drawable.background_menu), w(), h(), true));
        }
        canvas.drawBitmap(backgroundMenu(), 0.0f, 0.0f, (Paint) null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: scala.collection.LinearSeqOptimized.slice(int, int):Repr
     arg types: [int, int]
     candidates:
      scala.collection.mutable.MutableList.slice(int, int):java.lang.Object
      scala.collection.IterableLike.slice(int, int):Repr
      scala.collection.IterableLike.slice(int, int):Repr
      scala.collection.IterableLike.slice(int, int):Repr
      scala.collection.IterableLike.slice(int, int):Repr
      scala.collection.IterableLike.slice(int, int):Repr
      scala.collection.IterableLike.slice(int, int):Repr
      scala.collection.IterableLike.slice(int, int):Repr
      scala.collection.IterableLike.slice(int, int):Repr
      scala.collection.IterableLike.slice(int, int):Repr
      scala.collection.IterableLike.slice(int, int):Repr
      scala.collection.IterableLike.slice(int, int):Repr
      scala.collection.IterableLike.slice(int, int):Repr
      scala.collection.IterableLike.slice(int, int):Repr
      scala.collection.IterableLike.slice(int, int):Repr
      scala.collection.IterableLike.slice(int, int):Repr
      scala.collection.IterableLike.slice(int, int):Repr
      scala.collection.IterableLike.slice(int, int):Repr
      scala.collection.IterableLike.slice(int, int):Repr
      scala.collection.IterableLike.slice(int, int):Repr
      scala.collection.LinearSeqOptimized.slice(int, int):Repr */
    public void screenGame(Canvas canvas) {
        Object obj;
        Paint paint;
        initProp(canvas);
        canvas.drawBitmap(background(), 0.0f, 0.0f, (Paint) null);
        int wordLength2 = wordLength() - currentWord().length();
        StringOps augmentString = Predef$.MODULE$.augmentString("%-9s %3d");
        Predef$ predef$ = Predef$.MODULE$;
        Object[] objArr = new Object[2];
        StringBuilder append = new StringBuilder().append((Object) GameState$.MODULE$.wordToStr(currentWord()));
        if (wordLength2 < 0) {
            wordLength2 = 0;
        }
        objArr[0] = append.append((Object) "_________".substring(0, wordLength2)).toString();
        objArr[1] = BoxesRunTime.boxToInteger(GameState$.MODULE$.wordScore(currentWord()));
        canvas.drawText(augmentString.format(predef$.genericWrapArray(objArr)), (float) wordLeft(), (float) wordTop(), pCurWord());
        canvas.drawText(Predef$.MODULE$.augmentString("%3d").format(Predef$.MODULE$.genericWrapArray(new Object[]{BoxesRunTime.boxToInteger(score())})), (float) scoreLeft(), (float) scoreTop(), pScore());
        MutableList mutableList = (MutableList) upcoming().slice(0, upcoming().length() > 3 ? 3 : upcoming().length());
        StringOps augmentString2 = Predef$.MODULE$.augmentString("Next: %s");
        Predef$ predef$2 = Predef$.MODULE$;
        Object[] objArr2 = new Object[1];
        if (mutableList.length() > 0) {
            obj = ((TraversableOnce) mutableList.map(new CustomDrawableView$$anonfun$screenGame$1(this), LinearSeq$.MODULE$.canBuildFrom())).reduceLeft(new CustomDrawableView$$anonfun$screenGame$2(this));
        } else {
            obj = "";
        }
        objArr2[0] = obj;
        canvas.drawText(augmentString2.format(predef$2.genericWrapArray(objArr2)), (float) ((w() * 31) / 40), (float) ((h() * 24) / 100), pUpcoming());
        int h2 = (h() * 63) / 100;
        if (history().length() == 0) {
            ((IterableLike) ((IterableLike) Seq$.MODULE$.apply(Predef$.MODULE$.wrapRefArray((Object[]) new String[]{"Local high score:", BoxesRunTime.boxToInteger(activity().settings().getInt("highScore", 0)).toString()}))).zip(Predef$.MODULE$.intWrapper(h2).to(h()).by(h() / 14), Seq$.MODULE$.canBuildFrom())).foreach(new CustomDrawableView$$anonfun$screenGame$3(this, canvas));
            ((IterableLike) helpRight().zip(Predef$.MODULE$.intWrapper(h2).to(h()).by(h() / 14), Seq$.MODULE$.canBuildFrom())).foreach(new CustomDrawableView$$anonfun$screenGame$4(this, canvas));
        } else {
            ((IterableLike) history().zip(Predef$.MODULE$.intWrapper(h2).to(h()).by(h() / 14), Seq$.MODULE$.canBuildFrom())).foreach(new CustomDrawableView$$anonfun$screenGame$5(this, canvas));
            ((LinearSeqOptimized) ((TraversableOnce) ((TraversableLike) ((SeqLike) ((SeqLike) history().filter(new CustomDrawableView$$anonfun$screenGame$6(this))).sortBy(new CustomDrawableView$$anonfun$screenGame$7(this), Ordering$Int$.MODULE$)).reverse()).map(new CustomDrawableView$$anonfun$screenGame$8(this), Seq$.MODULE$.canBuildFrom())).toList().$colon$colon(Predef$.MODULE$.augmentString(" Word chain: +%s").format(Predef$.MODULE$.genericWrapArray(new Object[]{((TraversableOnce) ((TraversableLike) history().filter(new CustomDrawableView$$anonfun$6(this))).map(new CustomDrawableView$$anonfun$7(this), Seq$.MODULE$.canBuildFrom())).sum(Numeric$IntIsIntegral$.MODULE$)}))).zip(Predef$.MODULE$.intWrapper(h2).to(h()).by(h() / 14), List$.MODULE$.canBuildFrom())).foreach(new CustomDrawableView$$anonfun$screenGame$9(this, canvas));
        }
        RectF timerRect2 = timerRect();
        float tickTime2 = (((float) 360) * tickTime()) / ((float) counterDuration());
        if (letters().length() == GameState$.MODULE$.numLetters() || upcoming().length() == 0) {
            paint = pTimerWarn();
        } else {
            paint = pTimer();
        }
        canvas.drawArc(timerRect2, 270.0f, tickTime2, true, paint);
        canvas.drawText(Predef$.MODULE$.augmentString("%2d").format(Predef$.MODULE$.genericWrapArray(new Object[]{BoxesRunTime.boxToInteger(upcoming().length())})), (float) lettersLeftLeft(), (float) lettersLeftTop(), pLettersLeft());
        if (paused()) {
            ((IterableLike) ((IterableLike) Predef$.MODULE$.augmentString("Resume").map(new CustomDrawableView$$anonfun$screenGame$10(this), Predef$.MODULE$.fallbackStringCanBuildFrom())).zip(Predef$.MODULE$.intWrapper(0).to(20), IndexedSeq$.MODULE$.canBuildFrom())).foreach(new CustomDrawableView$$anonfun$screenGame$11(this, canvas));
        } else {
            ((IterableLike) letters().zip(Predef$.MODULE$.intWrapper(0).to(20), LinearSeq$.MODULE$.canBuildFrom())).foreach(new CustomDrawableView$$anonfun$screenGame$12(this, canvas));
        }
        if (GameState$.MODULE$.debugging()) {
            canvas.drawText(debug(), 0.0f, 10.0f, pDebug());
        }
    }

    public void screenSplash(Canvas canvas) {
        if (logo() == null) {
            logo_$eq(BitmapFactory.decodeResource(this.context.getResources(), R.drawable.scala_logo));
        }
        Tuple2$mcII$sp tuple2$mcII$sp = new Tuple2$mcII$sp(logo().getHeight(), logo().getWidth());
        if (tuple2$mcII$sp == null) {
            throw new MatchError(tuple2$mcII$sp);
        }
        Tuple2$mcII$sp tuple2$mcII$sp2 = new Tuple2$mcII$sp(BoxesRunTime.unboxToInt(tuple2$mcII$sp._1()), BoxesRunTime.unboxToInt(tuple2$mcII$sp._2()));
        int _1$mcI$sp = tuple2$mcII$sp2._1$mcI$sp();
        canvas.drawBitmap(logo(), (float) ((w() - tuple2$mcII$sp2._2$mcI$sp()) / 2), (float) ((h() - _1$mcI$sp) / 2), (Paint) null);
    }

    public void passTimer() {
        startTimer(true, true, startTimer$default$3());
    }

    public void validateWord() {
        if (currentWord().length() > 0) {
            letters().dequeueAll(new CustomDrawableView$$anonfun$validateWord$1(this));
            String wordToStr = GameState$.MODULE$.wordToStr(currentWord());
            wordLength_$eq(0);
            if (Dict$.MODULE$.exists(wordToStr.toLowerCase(), Dict$.MODULE$.exists$default$2(), Dict$.MODULE$.exists$default$3())) {
                score_$eq(score() + GameState$.MODULE$.wordScore(currentWord()));
                history().push(new Word(currentWord(), Word$.MODULE$.init$default$2(), Word$.MODULE$.init$default$3()));
                if (history().apply(0).word().length() > 4) {
                    wordLength_$eq(history().apply(0).word().length());
                    ((IterableLike) history().slice(1, history().length())).foreach(new CustomDrawableView$$anonfun$validateWord$2(this, new BooleanRef(false)));
                }
                if (letters().length() == 0) {
                    history().push(new Word(Word$.MODULE$.init$default$1(), "empty", 10));
                    score_$eq(history().apply(0).price() + score());
                }
                BoxedUnit boxedUnit = BoxedUnit.UNIT;
            } else {
                activity().vibrator().vibrate(70);
                history().push(new Word(currentWord(), "unavail", Word$.MODULE$.init$default$3()));
            }
            currentWord().clear();
            startTimer(true, false, startTimer$default$3());
        }
    }

    public void deleteLetter() {
        if (currentWord().length() > 0) {
            currentWord().pop();
        }
    }

    public Object selectLetter(Letter letter) {
        if (!currentWord().contains(letter)) {
            return currentWord().push(letter);
        }
        ((Range.ByOne) Predef$.MODULE$.intWrapper(1).to(currentWord().indexOf(letter) + 1)).foreach(new CustomDrawableView$$anonfun$selectLetter$1(this));
        return BoxedUnit.UNIT;
    }

    public boolean addLetter() {
        if (letters().length() == GameState$.MODULE$.numLetters() || (upcoming().length() == 0 && letters().length() > 0)) {
            wordLength_$eq(0);
            Letter dequeue = letters().dequeue();
            history().push(new Word(Stack$.MODULE$.apply(Predef$.MODULE$.wrapRefArray((Object[]) new Letter[]{dequeue})), "lost", Word$.MODULE$.init$default$3()));
            if (currentWord().contains(dequeue)) {
                currentWord().clear();
            }
            activity().playSound("lost");
        }
        if (upcoming().length() == 0) {
            return false;
        }
        if (upcoming().length() == 1) {
            activity().playSound("no");
        }
        letters().$plus$eq((Object) upcoming().dequeue());
        return true;
    }

    public String gameStats() {
        Word word;
        if (history().isEmpty()) {
            word = new Word(Word$.MODULE$.init$default$1(), Word$.MODULE$.init$default$2(), Word$.MODULE$.init$default$3());
        } else {
            word = (Word) ((TraversableLike) history().sortBy(new CustomDrawableView$$anonfun$8(this), Ordering$Int$.MODULE$)).last();
        }
        Tuple2<Boolean, Boolean> saveStats = saveStats();
        if (saveStats == null) {
            throw new MatchError(saveStats);
        }
        Tuple2 tuple2 = new Tuple2(saveStats._1(), saveStats._2());
        boolean unboxToBoolean = BoxesRunTime.unboxToBoolean(tuple2._1());
        boolean unboxToBoolean2 = BoxesRunTime.unboxToBoolean(tuple2._2());
        SharedPreferences sharedPreferences = activity().settings();
        int i = sharedPreferences.getInt("scoreSum", 0) / sharedPreferences.getInt("games", 1);
        int i2 = sharedPreferences.getInt("highScore", 0);
        if (unboxToBoolean || unboxToBoolean2) {
            activity().playSound("victory");
        }
        return new StringBuilder().append((Object) (unboxToBoolean ? "NEW HIGH SCORE!\n" : "")).append((Object) (unboxToBoolean2 ? "NEW BEST WORD!\n" : "")).append((Object) ((unboxToBoolean || unboxToBoolean2) ? "\n" : "")).append((Object) GameState$.MODULE$.seqString(Predef$.MODULE$.wrapRefArray((Object[]) new String[]{Predef$.MODULE$.augmentString("Score: %s").format(Predef$.MODULE$.genericWrapArray(new Object[]{BoxesRunTime.boxToInteger(score())})), Predef$.MODULE$.augmentString("Top word: %s (%s)").format(Predef$.MODULE$.genericWrapArray(new Object[]{word.word(), BoxesRunTime.boxToInteger(word.price())})), "", Predef$.MODULE$.augmentString("Average score: %s").format(Predef$.MODULE$.genericWrapArray(new Object[]{BoxesRunTime.boxToInteger(i)})), Predef$.MODULE$.augmentString("High score: %s").format(Predef$.MODULE$.genericWrapArray(new Object[]{BoxesRunTime.boxToInteger(i2)})), ""}))).toString();
    }

    public /* synthetic */ String fullStats$default$1() {
        return "Loading online high scores...";
    }

    public String fullStats(String str) {
        SharedPreferences sharedPreferences = activity().settings();
        return GameState$.MODULE$.seqString(Predef$.MODULE$.wrapRefArray((Object[]) new String[]{Predef$.MODULE$.augmentString("High score: %s").format(Predef$.MODULE$.genericWrapArray(new Object[]{BoxesRunTime.boxToInteger(sharedPreferences.getInt("highScore", 0))})), "", Predef$.MODULE$.augmentString("Best word: %s (%s)").format(Predef$.MODULE$.genericWrapArray(new Object[]{sharedPreferences.getString("bestWord", ""), BoxesRunTime.boxToInteger(sharedPreferences.getInt("bestWordScore", 0))})), "", Predef$.MODULE$.augmentString("Average score: %s").format(Predef$.MODULE$.genericWrapArray(new Object[]{BoxesRunTime.boxToInteger(sharedPreferences.getInt("scoreSum", 0) / (sharedPreferences.getInt("games", 1) == 0 ? 1 : sharedPreferences.getInt("games", 1)))})), Predef$.MODULE$.augmentString("Word accuracy: %s%%").format(Predef$.MODULE$.genericWrapArray(new Object[]{BoxesRunTime.boxToInteger((sharedPreferences.getInt("validWords", 1) * 100) / (sharedPreferences.getInt("validWords", 0) + sharedPreferences.getInt("invalidWords", 1)))})), "", Predef$.MODULE$.augmentString("Number of games: %s").format(Predef$.MODULE$.genericWrapArray(new Object[]{BoxesRunTime.boxToInteger(sharedPreferences.getInt("games", 0))})), Predef$.MODULE$.augmentString("Time spent playing: %.2f hours").format(Predef$.MODULE$.genericWrapArray(new Object[]{BoxesRunTime.boxToDouble(((double) sharedPreferences.getInt("totalTime", 0)) / 3600.0d)})), "", str}));
    }

    public Tuple2<Boolean, Boolean> saveStats() {
        Word word;
        boolean z;
        boolean z2;
        if (history().isEmpty()) {
            word = new Word(Word$.MODULE$.init$default$1(), Word$.MODULE$.init$default$2(), Word$.MODULE$.init$default$3());
        } else {
            word = (Word) ((TraversableLike) history().sortBy(new CustomDrawableView$$anonfun$9(this), Ordering$Int$.MODULE$)).last();
        }
        SharedPreferences sharedPreferences = activity().settings();
        SharedPreferences.Editor edit = sharedPreferences.edit();
        int i = sharedPreferences.getInt("highScore", 0);
        int i2 = sharedPreferences.getInt("bestWordScore", 0);
        inc$1("games", inc$default$2$1(), sharedPreferences, edit);
        inc$1("validWords", ((SeqLike) history().filter(new CustomDrawableView$$anonfun$saveStats$1(this))).length(), sharedPreferences, edit);
        inc$1("invalidWords", ((SeqLike) history().filter(new CustomDrawableView$$anonfun$saveStats$2(this))).length(), sharedPreferences, edit);
        higher$1("highScore", score(), sharedPreferences, edit);
        inc$1("totalTime", ((int) (new Date().getTime() - timeBegin().getTime())) / 1000, sharedPreferences, edit);
        inc$1("scoreSum", score(), sharedPreferences, edit);
        higher$1("bestWordScore", word.price(), sharedPreferences, edit);
        if (word.price() >= sharedPreferences.getInt("bestWordScore", 0)) {
            edit.putString("bestWord", word.word());
        } else {
            BoxedUnit boxedUnit = BoxedUnit.UNIT;
        }
        edit.commit();
        if (i < sharedPreferences.getInt("highScore", 0)) {
            z = true;
        } else {
            z = false;
        }
        Boolean boxToBoolean = BoxesRunTime.boxToBoolean(z);
        if (i2 < sharedPreferences.getInt("bestWordScore", 0)) {
            z2 = true;
        } else {
            z2 = false;
        }
        return new Tuple2<>(boxToBoolean, BoxesRunTime.boxToBoolean(z2));
    }

    private final /* synthetic */ int inc$default$2$1() {
        return 1;
    }

    private final SharedPreferences.Editor inc$1(String x, int y, SharedPreferences sharedPreferences, SharedPreferences.Editor editor) {
        return editor.putInt(x, sharedPreferences.getInt(x, 0) + y);
    }

    private final Object higher$1(String x, int y, SharedPreferences sharedPreferences, SharedPreferences.Editor editor) {
        return y >= sharedPreferences.getInt(x, 0) ? editor.putInt(x, y) : BoxedUnit.UNIT;
    }

    public void resetPref() {
        SharedPreferences.Editor edit = activity().settings().edit();
        List$.MODULE$.apply((Seq) Predef$.MODULE$.wrapRefArray((Object[]) new String[]{"highScore", "totalTime", "games", "bestWordScore", "scoreSum", "validWords", "invalidWords"})).foreach(new CustomDrawableView$$anonfun$resetPref$1(this, edit));
        edit.putString("bestWord", "");
        edit.commit();
    }

    public void initProp(Canvas canvas) {
        if (cardTop() == 0) {
            Dict$.MODULE$.init(activity().getResources());
            background_$eq(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(this.context.getResources(), R.drawable.background), w(), h(), true));
            deleteButton().set(0.0f, (float) ((h() * 1) / 100), (float) ((w() * 2) / 10), (float) ((h() * 19) / 100));
            okButton().set((float) (w() - ((w() * 2) / 10)), (float) ((h() * 1) / 100), (float) w(), (float) ((h() * 19) / 100));
            cardTop_$eq((h() * 30) / 100);
            margin_$eq((w() * 1) / 150);
            side_$eq(((w() - margin()) / GameState$.MODULE$.numLetters()) - margin());
            cardH_$eq((side() * 140) / 100);
            int w2 = w() / 2;
            int side2 = side() / 2;
            int h2 = (h() * 79) / 100;
            timerRect().set((float) (w2 - side2), (float) h2, (float) (w2 + side2), (float) ((side2 * 2) + h2));
            pUpcoming().setTextSize((float) (h() / 24));
            pLettersLeft().setTextSize((float) (side() / 2));
            pHistory().setTextSize((float) (h() / 24));
            pLetter().setTextSize((float) (((double) side()) * 0.9d));
            pPrice().setTextSize((float) (side() / 4));
            pScore().setTextSize((float) ((side() * 5) / 8));
            pCurWord().setTextSize((float) (side() / 2));
            lettersLeftLeft_$eq(w2 - (textSize(pLettersLeft(), "__")._2$mcI$sp() / 2));
            lettersLeftTop_$eq(((side2 * 2) - textSize(pLettersLeft(), "0")._1$mcI$sp()) + h2);
            wordTop_$eq((h() * 15) / 100);
            wordLeft_$eq((w() - textSize(pCurWord(), Predef$.MODULE$.augmentString("%-9s %3d").format(Predef$.MODULE$.genericWrapArray(new Object[]{"a", BoxesRunTime.boxToInteger(1)})))._2$mcI$sp()) / 2);
            scoreTop_$eq((h() * 67) / 100);
            scoreLeft_$eq((w() - ((textSize(pScore(), "---")._2$mcI$sp() * 12) / 10)) / 2);
            bonusTop_$eq(cardTop() + 3 + textSize(pPrice(), "X")._1$mcI$sp());
        }
    }

    public String readScores(JSONArray jSONArray) {
        ObjectRef objectRef = new ObjectRef((Seq) Seq$.MODULE$.apply(Predef$.MODULE$.wrapRefArray((Object[]) new Map[0])));
        if (jSONArray == null) {
            return "No connectivity";
        }
        Predef$.MODULE$.intWrapper(0).until(jSONArray.length()).foreach$mVc$sp(new CustomDrawableView$$anonfun$readScores$1(this, jSONArray, objectRef));
        return GameState$.MODULE$.seqString((Seq) ((Seq) objectRef.elem).map(new CustomDrawableView$$anonfun$readScores$2(this), Seq$.MODULE$.canBuildFrom()));
    }

    public Tuple2<Integer, Integer> textSize(Paint p, String s) {
        Rect r = new Rect();
        p.getTextBounds(s, 0, s.length(), r);
        return new Tuple2$mcII$sp(r.bottom - r.top, r.right - r.left);
    }

    public int hideScore(int i) {
        return (i * i * 781) + 908;
    }

    public /* synthetic */ String easyPaint$default$1() {
        return "f000";
    }

    public /* synthetic */ float easyPaint$default$2() {
        return 10.0f;
    }

    public Paint easyPaint(String argb, float size) {
        IndexedSeq x = (IndexedSeq) ((TraversableLike) ((TraversableLike) Predef$.MODULE$.augmentString(argb).map(new CustomDrawableView$$anonfun$10(this), Predef$.MODULE$.fallbackStringCanBuildFrom())).map(new CustomDrawableView$$anonfun$1(this), IndexedSeq$.MODULE$.canBuildFrom())).map(new CustomDrawableView$$anonfun$2(this), IndexedSeq$.MODULE$.canBuildFrom());
        Paint p = new Paint(1);
        p.setARGB(BoxesRunTime.unboxToInt(x.apply(0)), BoxesRunTime.unboxToInt(x.apply(1)), BoxesRunTime.unboxToInt(x.apply(2)), BoxesRunTime.unboxToInt(x.apply(3)));
        p.setTextSize(size);
        p.setTypeface(Typeface.MONOSPACE);
        return p;
    }
}
