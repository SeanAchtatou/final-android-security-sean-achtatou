package touchy.words;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.SoundPool;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.zemariamm.appirater.AppiraterBase;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.protocol.BasicHttpContext;
import org.json.JSONTokener;
import scala.Array$;
import scala.Function0;
import scala.Function1;
import scala.Predef$;
import scala.ScalaObject;
import scala.Tuple2;
import scala.actors.Futures$;
import scala.collection.Seq;
import scala.collection.Seq$;
import scala.collection.SeqLike;
import scala.collection.immutable.List$;
import scala.collection.immutable.Map;
import scala.collection.mutable.StringBuilder;
import scala.reflect.ClassManifest$;
import scala.reflect.OptManifest;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

/* compiled from: Activity.scala */
public class MainActivity extends AppiraterBase implements ScalaObject {
    private AudioManager audioManager;
    private final DefaultHttpClient client = new DefaultHttpClient();
    private CustomDrawableView game;
    private String hasSound = "";
    private BasicHttpContext httpContext = new BasicHttpContext();
    private final boolean isEmulator = ((SeqLike) Seq$.MODULE$.apply(Predef$.MODULE$.wrapRefArray((Object[]) new String[]{"google_sdk", "sdk"}))).contains(Build.PRODUCT);
    private RelativeLayout layoutHome;
    private int logged = 0;
    private NextDest nextDest = null;
    private SharedPreferences settings;
    private SoundPool soundPool;
    private Map<String, Integer> sounds;
    private GoogleAnalyticsTracker tracker;
    private Vibrator vibrator;

    public RelativeLayout layoutHome() {
        return this.layoutHome;
    }

    public void layoutHome_$eq(RelativeLayout relativeLayout) {
        this.layoutHome = relativeLayout;
    }

    public CustomDrawableView game() {
        return this.game;
    }

    public void game_$eq(CustomDrawableView customDrawableView) {
        this.game = customDrawableView;
    }

    public SharedPreferences settings() {
        return this.settings;
    }

    public void settings_$eq(SharedPreferences sharedPreferences) {
        this.settings = sharedPreferences;
    }

    public boolean isEmulator() {
        return this.isEmulator;
    }

    public GoogleAnalyticsTracker tracker() {
        return this.tracker;
    }

    public void tracker_$eq(GoogleAnalyticsTracker googleAnalyticsTracker) {
        this.tracker = googleAnalyticsTracker;
    }

    public Vibrator vibrator() {
        return this.vibrator;
    }

    public void vibrator_$eq(Vibrator vibrator2) {
        this.vibrator = vibrator2;
    }

    public SoundPool soundPool() {
        return this.soundPool;
    }

    public void soundPool_$eq(SoundPool soundPool2) {
        this.soundPool = soundPool2;
    }

    public AudioManager audioManager() {
        return this.audioManager;
    }

    public void audioManager_$eq(AudioManager audioManager2) {
        this.audioManager = audioManager2;
    }

    public Map<String, Integer> sounds() {
        return this.sounds;
    }

    public void sounds_$eq(Map<String, Integer> map) {
        this.sounds = map;
    }

    public String hasSound() {
        return this.hasSound;
    }

    public void hasSound_$eq(String str) {
        this.hasSound = str;
    }

    public DefaultHttpClient client() {
        return this.client;
    }

    public BasicHttpContext httpContext() {
        return this.httpContext;
    }

    public void httpContext_$eq(BasicHttpContext basicHttpContext) {
        this.httpContext = basicHttpContext;
    }

    public int logged() {
        return this.logged;
    }

    public void logged_$eq(int i) {
        this.logged = i;
    }

    public NextDest nextDest() {
        return this.nextDest;
    }

    public void nextDest_$eq(NextDest nextDest2) {
        this.nextDest = nextDest2;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.layout_game);
        setVolumeControlStream(3);
        settings_$eq(getSharedPreferences("Prefs", 0));
        hasSound_$eq(settings().getString("sound", "yes"));
        vibrator_$eq((Vibrator) getSystemService("vibrator"));
        game_$eq((CustomDrawableView) getResource(R.id.game, getResource$default$2()));
        game().start();
    }

    public void onDestroy() {
        super.onDestroy();
        tracker().stop();
    }

    public void init() {
        layoutHome_$eq(getLayout(R.id.layout_home));
        RelativeLayout layout = getLayout(R.id.layout_highscores);
        RelativeLayout layout2 = getLayout(R.id.layout_settings);
        CheckBox checkBox = (CheckBox) getResource(R.id.sound, getResource$default$2());
        try {
            int i = getApplicationContext().getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), 0).versionCode;
            if (hasInternet()) {
                asyncGet("check-version", (Map) Predef$.MODULE$.Map().apply(Predef$.MODULE$.wrapRefArray((Object[]) new Tuple2[]{Predef$.MODULE$.any2ArrowAssoc("V").$minus$greater(BoxesRunTime.boxToInteger(i).toString())})), new MainActivity$$anonfun$init$2(this), new MainActivity$$anonfun$init$1(this), asyncGet$default$5());
            }
            BoxedUnit boxedUnit = BoxedUnit.UNIT;
        } catch (Exception e) {
            BoxesRunTime.boxToInteger(GameState$.MODULE$.log(e));
        }
        soundPool_$eq(new SoundPool(2, 3, 0));
        audioManager_$eq((AudioManager) getSystemService("audio"));
        sounds_$eq((Map) Predef$.MODULE$.Map().apply(Predef$.MODULE$.wrapRefArray((Object[]) new Tuple2[]{Predef$.MODULE$.any2ArrowAssoc("lost").$minus$greater(BoxesRunTime.boxToInteger(soundPool().load(this, R.raw.lost_letter, 1))), Predef$.MODULE$.any2ArrowAssoc("no").$minus$greater(BoxesRunTime.boxToInteger(soundPool().load(this, R.raw.no_letters, 1))), Predef$.MODULE$.any2ArrowAssoc("victory").$minus$greater(BoxesRunTime.boxToInteger(soundPool().load(this, R.raw.victory, 1)))})));
        setOnClick(getButton(R.id.new_game, getButton$default$2()), new MainActivity$$anonfun$init$3(this));
        setOnClick(getButton(R.id.highscores, getButton$default$2()), new MainActivity$$anonfun$init$4(this, layout));
        setOnClick(getButton(R.id.settings, getButton$default$2()), new MainActivity$$anonfun$init$5(this, layout2, checkBox));
        setOnClick(getButton(R.id.highscores_back, getButton$default$2()), new MainActivity$$anonfun$init$6(this));
        setOnClick(getButton(R.id.go_online, getButton$default$2()), new MainActivity$$anonfun$init$7(this));
        setOnClick(getButton(R.id.settings_back, getButton$default$2()), new MainActivity$$anonfun$init$8(this));
        setOnClick(getButton(R.id.reset, getButton$default$2()), new MainActivity$$anonfun$init$9(this));
        setOnClick(getButton(R.id.tutorial, getButton$default$2()), new MainActivity$$anonfun$init$10(this));
        setOnClick(getButton(R.id.delusername, getButton$default$2()), new MainActivity$$anonfun$init$11(this));
        setOnClick(checkBox, new MainActivity$$anonfun$init$12(this, checkBox));
        tracker_$eq(GoogleAnalyticsTracker.getInstance());
        tracker().start("UA-291257-19", 20, this);
        AdView adView = (AdView) getResource(R.id.adView, getResource$default$2());
        if (GameState$.MODULE$.free()) {
            AdRequest adRequest = new AdRequest();
            if (isEmulator()) {
                adRequest.addTestDevice(AdRequest.TEST_EMULATOR);
            }
            adView.loadAd(adRequest);
            return;
        }
        adView.stopLoading();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.layout.menu, menu);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean playing = game().playing();
        List$.MODULE$.apply((Seq) Predef$.MODULE$.wrapIntArray(new int[]{R.id.new_game, R.id.pause, R.id.menu})).foreach(new MainActivity$$anonfun$onPrepareOptionsMenu$1(this, menu, playing));
        List$.MODULE$.apply((Seq) Predef$.MODULE$.wrapIntArray(new int[]{R.id.about, R.id.contact, R.id.share})).foreach(new MainActivity$$anonfun$onPrepareOptionsMenu$2(this, menu, playing));
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.new_game /*2131165190*/:
                game().newGame();
                return true;
            case R.id.pause /*2131165204*/:
                if (game().paused()) {
                    game().resumeGame();
                } else {
                    game().pauseGame();
                }
                game().invalidate();
                return true;
            case R.id.menu /*2131165205*/:
                game().gameMenu();
                return true;
            case R.id.about /*2131165206*/:
                showDialog(GameState$.MODULE$.ABOUT());
                return true;
            case R.id.contact /*2131165207*/:
                Intent intent = new Intent("android.intent.action.SEND");
                intent.setType("plain/text");
                intent.putExtra("android.intent.extra.EMAIL", (String[]) Array$.MODULE$.apply(Predef$.MODULE$.wrapRefArray((Object[]) new String[]{"contact@touchywords.com"}), ClassManifest$.MODULE$.classType(String.class)));
                intent.putExtra("android.intent.extra.TEXT", "\n\nIf you are experiencing a problem, please describe the steps to reproduce it.");
                startActivity(Intent.createChooser(intent, "Send your email with:"));
                return true;
            case R.id.share /*2131165208*/:
                Intent intent2 = new Intent("android.intent.action.SEND");
                intent2.setType("plain/text");
                intent2.putExtra("android.intent.extra.SUBJECT", "Check this cool Android App");
                intent2.putExtra("android.intent.extra.TEXT", GameState$.MODULE$.seqString(Predef$.MODULE$.wrapRefArray((Object[]) new String[]{"Touchy Words is mix of Scrabble, Word Mole and Shuffled Row with a fun touch screen interface. I think you'll enjoy it.", "", "Install market://details?id=touchy.words", "Website http://www.touchywords.com"})));
                startActivity(Intent.createChooser(intent2, "Share"));
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    public Dialog onCreateDialog(int i) {
        if (i == GameState$.MODULE$.PROMPT_NAME()) {
            Dialog dialog = new Dialog(this);
            dialog.setCancelable(false);
            dialog.setContentView((int) R.layout.username_dialog);
            dialog.setTitle("Your name:");
            EditText editText = (EditText) getResource(R.id.username, dialog);
            editText.setOnKeyListener(new MainActivity$$anon$2(this, dialog));
            setOnClick(getButton(R.id.ok, dialog), new MainActivity$$anonfun$onCreateDialog$1(this, dialog, editText));
            setOnClick(getButton(R.id.cancel, dialog), new MainActivity$$anonfun$onCreateDialog$2(this, dialog));
            return dialog;
        } else if (i == GameState$.MODULE$.TIPS()) {
            return new AlertDialog.Builder(this).setMessage("Tips").setMessage(GameState$.MODULE$.TIPS_MSG()).setPositiveButton("OK", new MainActivity$$anon$3(this)).setNegativeButton("Don't show again", new MainActivity$$anon$4(this)).create();
        } else {
            if (i == GameState$.MODULE$.YES_NO()) {
                return new AlertDialog.Builder(this).setMessage("Are you sure?").setCancelable(false).setPositiveButton("Yes", new MainActivity$$anon$5(this)).setNegativeButton("No", new MainActivity$$anon$6(this)).create();
            }
            if (i == GameState$.MODULE$.ABOUT()) {
                return new AlertDialog.Builder(this).setCancelable(true).setMessage(GameState$.MODULE$.seqString(Predef$.MODULE$.wrapRefArray((Object[]) new String[]{"This game is mix of Scrabble, Word Mole and Shuffled Row with a fun touch screen interface. If you have suggestions, feel free to contact me."}))).setNegativeButton("Go to website", new MainActivity$$anon$7(this)).create();
            } else if (i != GameState$.MODULE$.TRY_AGAIN()) {
                return null;
            } else {
                Dialog dialog2 = new Dialog(this);
                dialog2.setCancelable(false);
                dialog2.setContentView((int) R.layout.game_over_dialog);
                dialog2.setTitle("Game Over");
                setOnClick(getButton(R.id.try_again, dialog2), new MainActivity$$anonfun$onCreateDialog$3(this, dialog2));
                setOnClick(getButton(R.id.show_menu, dialog2), new MainActivity$$anonfun$onCreateDialog$4(this, dialog2));
                return dialog2;
            }
        }
    }

    public void onPrepareDialog(int i, Dialog dialog) {
        if (i == GameState$.MODULE$.TRY_AGAIN()) {
            GameState$.MODULE$.log("onPrepareDialog");
            if (game() != null && game().history() != null && game().timeBegin() != null && settings() != null && settings().edit() != null) {
                ((TextView) getResource(R.id.game_over_content, dialog)).setText(game().gameStats());
                GameState$.MODULE$.log("game is initialized");
            }
        }
    }

    public void onBackPressed() {
        if (game() != null && game().playing()) {
            if (game().paused()) {
                game().gameMenu();
            } else {
                game().pauseGame();
            }
            game().invalidate();
        } else if (layoutHome() == null) {
        } else {
            if (layoutHome().getVisibility() == 0) {
                finish();
            } else {
                showHome();
            }
        }
    }

    public void onResume() {
        super.onResume();
    }

    public void onPause() {
        super.onPause();
        if (game().playing()) {
            game().pauseGame();
        }
    }

    public void login() {
        BasicCookieStore basicCookieStore = new BasicCookieStore();
        httpContext().setAttribute("http.cookie-store", basicCookieStore);
        String string = settings().getString("sessionid", "");
        if (string != null ? !string.equals("") : "" != 0) {
            BasicClientCookie basicClientCookie = new BasicClientCookie("sessionid", settings().getString("sessionid", ""));
            basicClientCookie.setDomain("touchywords.com");
            basicClientCookie.setPath("/");
            basicClientCookie.setExpiryDate(new Date(new Date().getTime() + (15552000 * ((long) 1000))));
            basicCookieStore.addCookie(basicClientCookie);
            Object url = getUrl("isloggedin", getUrl$default$2(), getUrl$default$3());
            if (url != null ? !url.equals("ok") : "ok" != 0) {
                saveSettings("sessionid", "", saveSettings$default$3(), saveSettings$default$4());
                login();
                return;
            }
            logged_$eq(1);
            return;
        }
        String str = (String) getUrl("device-login", (Map) Predef$.MODULE$.Map().apply(Predef$.MODULE$.wrapRefArray((Object[]) new Tuple2[]{Predef$.MODULE$.any2ArrowAssoc("username").$minus$greater(settings().getString("username", "")), Predef$.MODULE$.any2ArrowAssoc("password").$minus$greater(settings().getString("password", ""))})), getUrl$default$3());
        if (str == null) {
            if ("ok" != 0) {
                return;
            }
        } else if (!str.equals("ok")) {
            return;
        }
        logged_$eq(1);
        List cookies = basicCookieStore.getCookies();
        Predef$.MODULE$.intWrapper(0).until(cookies.size()).foreach$mVc$sp(new MainActivity$$anonfun$login$1(this, cookies));
    }

    public String getOutput(HttpResponse httpResponse) {
        InputStream content = httpResponse.getEntity().getContent();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[512];
        int i = 0;
        while (i != -1) {
            i = content.read(bArr);
            if (i != -1) {
                byteArrayOutputStream.write(bArr, 0, i);
            }
        }
        return new String(byteArrayOutputStream.toByteArray());
    }

    public <T> T getJson(String json) {
        return new JSONTokener(json).nextValue();
    }

    public String buildPath(String str, Map<String, String> map) {
        if (map.isEmpty()) {
            return str;
        }
        return Predef$.MODULE$.augmentString("%s?%s").format(Predef$.MODULE$.genericWrapArray(new Object[]{str, Predef$.MODULE$.refArrayOps((Object[]) Predef$.MODULE$.refArrayOps((Object[]) map.toArray(ClassManifest$.MODULE$.classType(Tuple2.class, ClassManifest$.MODULE$.classType(String.class), Predef$.MODULE$.wrapRefArray((Object[]) new OptManifest[]{ClassManifest$.MODULE$.classType(String.class)})))).map(new MainActivity$$anonfun$buildPath$1(this), Array$.MODULE$.canBuildFrom(ClassManifest$.MODULE$.classType(String.class)))).reduceLeft(new MainActivity$$anonfun$buildPath$2(this))}));
    }

    public /* synthetic */ Map getUrl$default$2() {
        return (Map) Predef$.MODULE$.Map().apply(Predef$.MODULE$.wrapRefArray((Object[]) new Tuple2[0]));
    }

    public /* synthetic */ String getUrl$default$3() {
        return "http://www.touchywords.com/";
    }

    public <T> T getUrl(String path, Map<String, String> args, String base) {
        try {
            String url = new StringBuilder().append((Object) base).append((Object) buildPath(path, args)).toString();
            HttpResponse resp = client().execute(new HttpGet(url), httpContext());
            String out = getOutput(resp);
            if (GameState$.MODULE$.debugging()) {
                GameState$.MODULE$.log(url);
                GameState$.MODULE$.log(resp.getStatusLine());
                BoxesRunTime.boxToInteger(GameState$.MODULE$.log(out));
            } else {
                BoxedUnit boxedUnit = BoxedUnit.UNIT;
            }
            return getJson(out);
        } catch (Exception e) {
            int exceptionResult2 = GameState$.MODULE$.log(e);
            return null;
        }
    }

    public boolean hasInternet() {
        return ((ConnectivityManager) getSystemService("connectivity")).getActiveNetworkInfo() != null;
    }

    public /* synthetic */ Map authGet$default$2() {
        return (Map) Predef$.MODULE$.Map().apply(Predef$.MODULE$.wrapRefArray((Object[]) new Tuple2[0]));
    }

    public /* synthetic */ Function1 authGet$default$3() {
        return new MainActivity$$anonfun$authGet$default$3$1(this);
    }

    public /* synthetic */ Function0 authGet$default$4() {
        return null;
    }

    public void authGet(String str, Map<String, String> map, Function1<Object, Object> function1, Function0<Object> function0) {
        String string = settings().getString("username", "");
        if (string != null ? !string.equals("") : "" != 0) {
            asyncGet(str, map, function1, function0, true);
            return;
        }
        nextDest_$eq(new NextDest(str, map, function1, function0));
        showDialog(GameState$.MODULE$.PROMPT_NAME());
    }

    public /* synthetic */ Map asyncGet$default$2() {
        return (Map) Predef$.MODULE$.Map().apply(Predef$.MODULE$.wrapRefArray((Object[]) new Tuple2[0]));
    }

    public /* synthetic */ Function1 asyncGet$default$3() {
        return new MainActivity$$anonfun$asyncGet$default$3$1(this);
    }

    public /* synthetic */ Function0 asyncGet$default$4() {
        return null;
    }

    public /* synthetic */ boolean asyncGet$default$5() {
        return false;
    }

    public final void onUiThread$1(Function1 f$2, Object res$2) {
        runOnUiThread(new MainActivity$$anon$8(this, f$2, res$2));
    }

    public final void failure$1(Function0 f$3) {
        runOnUiThread(new MainActivity$$anon$9(this, f$3));
    }

    public void asyncGet(String path$2, Map<String, String> args$2, Function1<Object, Object> ok$2, Function0<Object> error$2, boolean auth$1) {
        Futures$.MODULE$.future(new MainActivity$$anonfun$asyncGet$1(this, path$2, args$2, ok$2, error$2, auth$1));
    }

    public void playSound(String id) {
        String hasSound2 = hasSound();
        if (hasSound2 == null) {
            if ("yes" != 0) {
                return;
            }
        } else if (!hasSound2.equals("yes")) {
            return;
        }
        float volume = ((float) audioManager().getStreamVolume(3)) / ((float) audioManager().getStreamMaxVolume(3));
        soundPool().play(BoxesRunTime.unboxToInt(sounds().apply(id)), volume, volume, 1, 0, 1.0f);
    }

    public void track(String path) {
        if (!GameState$.MODULE$.debugging()) {
            tracker().trackPageView(path);
        }
    }

    public /* synthetic */ String saveSettings$default$3() {
        return "";
    }

    public /* synthetic */ String saveSettings$default$4() {
        return "";
    }

    public void saveSettings(String key1, Object value1, String key2, String value2) {
        SharedPreferences.Editor e = settings().edit();
        if (value1 instanceof String) {
            e.putString(key1, (String) value1);
        } else {
            e.putInt(key1, BoxesRunTime.unboxToInt(value1));
        }
        if (key2 != null ? !key2.equals("") : "" != 0) {
            e.putString(key2, value2);
        } else {
            BoxedUnit boxedUnit = BoxedUnit.UNIT;
        }
        e.commit();
    }

    public void openUrl(String str) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse(str));
        startActivity(intent);
    }

    public void showHome() {
        getLayout(R.id.layout_highscores).setVisibility(4);
        getLayout(R.id.layout_settings).setVisibility(4);
        layoutHome().setVisibility(0);
    }

    public void showToast(String str) {
        int i;
        Context applicationContext = getApplicationContext();
        if (str.length() < 10) {
            i = 0;
        } else {
            i = 1;
        }
        Toast.makeText(applicationContext, str, i).show();
    }

    public /* synthetic */ Object getResource$default$2() {
        return null;
    }

    public <T> T getResource(int id, Object parent) {
        if (parent == null) {
            return findViewById(id);
        }
        if (parent instanceof Dialog) {
            return ((Dialog) parent).findViewById(id);
        }
        return ((Menu) parent).findItem(id);
    }

    public RelativeLayout getLayout(int x) {
        return (RelativeLayout) getResource(x, getResource$default$2());
    }

    public /* synthetic */ Dialog getButton$default$2() {
        return null;
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public Button getButton(int x, Dialog dialog) {
        if (dialog == null) {
            return (Button) getResource(x, getResource$default$2());
        }
        return (Button) getResource(x, dialog);
    }

    public void setOnClick(Button b, Function1<View, Object> f$1) {
        b.setOnClickListener(new MainActivity$$anon$1(this, f$1));
    }
}
