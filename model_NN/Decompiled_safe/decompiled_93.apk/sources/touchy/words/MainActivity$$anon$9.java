package touchy.words;

import scala.Function0;

/* compiled from: Activity.scala */
public final class MainActivity$$anon$9 implements Runnable {
    private final /* synthetic */ MainActivity $outer;
    private final /* synthetic */ Function0 f$3;

    public MainActivity$$anon$9(MainActivity $outer2, Function0 function0) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.f$3 = function0;
    }

    public void run() {
        if (this.f$3 == null) {
            this.$outer.showToast("No connectivity");
        } else {
            this.f$3.apply$mcV$sp();
        }
    }
}
