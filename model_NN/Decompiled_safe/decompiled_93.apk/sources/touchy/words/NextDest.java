package touchy.words;

import java.io.Serializable;
import scala.Function0;
import scala.Function1;
import scala.Product;
import scala.ScalaObject;
import scala.Tuple4;
import scala.collection.Iterator;
import scala.collection.immutable.Map;
import scala.runtime.BoxesRunTime;
import scala.runtime.ScalaRunTime$;

/* compiled from: Activity.scala */
public class NextDest implements Serializable, Product, ScalaObject {
    private final Map<String, String> args;
    private final Function0<Object> error;
    private final Function1<Object, Object> ok;
    private final String path;

    public static final Function1<String, Function1<Map<String, String>, Function1<Function1<Object, Object>, Function1<Function0<Object>, NextDest>>>> curried() {
        return NextDest$.MODULE$.curried();
    }

    public static final Function1<String, Function1<Map<String, String>, Function1<Function1<Object, Object>, Function1<Function0<Object>, NextDest>>>> curry() {
        return NextDest$.MODULE$.curry();
    }

    public static final Function1<Tuple4<String, Map<String, String>, Function1<Object, Object>, Function0<Object>>, NextDest> tupled() {
        return NextDest$.MODULE$.tupled();
    }

    public NextDest(String path2, Map<String, String> args2, Function1<Object, Object> ok2, Function0<Object> error2) {
        this.path = path2;
        this.args = args2;
        this.ok = ok2;
        this.error = error2;
        Product.Cclass.$init$(this);
    }

    private final /* synthetic */ boolean gd1$1(String str, Map map, Function1 function1, Function0 function0) {
        String path2 = copy$default$1();
        if (str != null ? str.equals(path2) : path2 == null) {
            Map<String, String> args2 = copy$default$2();
            if (map != null ? map.equals(args2) : args2 == null) {
                Function1<Object, Object> ok2 = copy$default$3();
                if (function1 != null ? function1.equals(ok2) : ok2 == null) {
                    Function0<Object> error2 = copy$default$4();
                    return function0 != null ? function0.equals(error2) : error2 == null;
                }
            }
        }
    }

    /* renamed from: args */
    public Map<String, String> copy$default$2() {
        return this.args;
    }

    public boolean canEqual(Object obj) {
        return obj instanceof NextDest;
    }

    public /* synthetic */ NextDest copy(String path2, Map args2, Function1 ok2, Function0 error2) {
        return new NextDest(path2, args2, ok2, error2);
    }

    public boolean equals(Object obj) {
        boolean z;
        if (this != obj) {
            if (obj instanceof NextDest) {
                NextDest nextDest = (NextDest) obj;
                z = gd1$1(nextDest.copy$default$1(), nextDest.copy$default$2(), nextDest.copy$default$3(), nextDest.copy$default$4()) ? ((NextDest) obj).canEqual(this) : false;
            } else {
                z = false;
            }
            if (!z) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: error */
    public Function0<Object> copy$default$4() {
        return this.error;
    }

    public int hashCode() {
        return ScalaRunTime$.MODULE$._hashCode(this);
    }

    /* renamed from: ok */
    public Function1<Object, Object> copy$default$3() {
        return this.ok;
    }

    /* renamed from: path */
    public String copy$default$1() {
        return this.path;
    }

    public int productArity() {
        return 4;
    }

    public Object productElement(int i) {
        switch (i) {
            case 0:
                return copy$default$1();
            case 1:
                return copy$default$2();
            case 2:
                return copy$default$3();
            case 3:
                return copy$default$4();
            default:
                throw new IndexOutOfBoundsException(BoxesRunTime.boxToInteger(i).toString());
        }
    }

    public Iterator<Object> productElements() {
        return Product.Cclass.productElements(this);
    }

    public Iterator<Object> productIterator() {
        return Product.Cclass.productIterator(this);
    }

    public String productPrefix() {
        return "NextDest";
    }

    public String toString() {
        return ScalaRunTime$.MODULE$._toString(this);
    }
}
