package touchy.words;

import java.io.Serializable;
import scala.Tuple2;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxesRunTime;

/* compiled from: Data.scala */
public final class GameState$$anonfun$3 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;

    public final int apply(Tuple2<String, Integer> tuple2) {
        return tuple2._2$mcI$sp();
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        return BoxesRunTime.boxToInteger(apply((Tuple2<String, Integer>) ((Tuple2) v1)));
    }
}
