package touchy.words;

import android.graphics.Canvas;
import android.graphics.RectF;
import java.io.Serializable;
import scala.MatchError;
import scala.Tuple2;
import scala.runtime.AbstractFunction1;
import scala.runtime.BoxedUnit;
import scala.runtime.BoxesRunTime;

/* compiled from: Custom.scala */
public final class CustomDrawableView$$anonfun$screenGame$12 extends AbstractFunction1 implements Serializable {
    public static final long serialVersionUID = 0;
    private final /* synthetic */ CustomDrawableView $outer;
    private final /* synthetic */ Canvas canvas$1;

    public CustomDrawableView$$anonfun$screenGame$12(CustomDrawableView $outer2, Canvas canvas) {
        if ($outer2 == null) {
            throw new NullPointerException();
        }
        this.$outer = $outer2;
        this.canvas$1 = canvas;
    }

    public final /* bridge */ /* synthetic */ Object apply(Object v1) {
        apply((Tuple2<Letter, Integer>) ((Tuple2) v1));
        return BoxedUnit.UNIT;
    }

    public final void apply(Tuple2<Letter, Integer> tuple2) {
        if (tuple2 == null) {
            throw new MatchError(tuple2);
        }
        Letter letter = tuple2._1();
        int l = this.$outer.margin() + ((this.$outer.side() + this.$outer.margin()) * BoxesRunTime.unboxToInt(tuple2._2()));
        this.canvas$1.drawRoundRect(new RectF((float) l, (float) this.$outer.cardTop(), (float) (this.$outer.side() + l), (float) (this.$outer.cardTop() + this.$outer.cardH())), (float) (this.$outer.side() / 7), (float) (this.$outer.side() / 7), this.$outer.currentWord().contains(letter) ? this.$outer.pLetterBgSel() : this.$outer.pLetterBg());
        this.canvas$1.drawText(BoxesRunTime.boxToInteger(letter.price()).toString(), (float) (l + 2), (float) ((this.$outer.cardTop() + this.$outer.cardH()) - 3), this.$outer.pPrice());
        this.canvas$1.drawText(letter.bonus(), (float) (l + 2), (float) this.$outer.bonusTop(), this.$outer.pPrice());
        this.canvas$1.drawText(letter.c(), (float) (((this.$outer.side() - this.$outer.textSize(this.$outer.pLetter(), letter.c())._1$mcI$sp()) / 2) + l), (float) ((this.$outer.cardTop() + this.$outer.cardH()) - (this.$outer.cardH() / 4)), this.$outer.pLetter());
    }
}
