package defpackage;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Handler;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.google.ads.InterstitialAd;
import com.google.ads.util.AdUtil;
import com.google.ads.util.a;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.LinkedList;

/* renamed from: d  reason: default package */
public final class d {
    private static final Object a = new Object();
    private WeakReference<Activity> b;
    private Ad c;
    private AdListener d;
    private c e;
    private AdRequest f;
    private AdSize g;
    private f h;
    private String i;
    private g j;
    private h k;
    private Handler l;
    private long m;
    private boolean n;
    private boolean o;
    private SharedPreferences p;
    private long q;
    private x r;
    private LinkedList<String> s;
    private LinkedList<String> t;
    private int u = 4;

    public d(Activity activity, Ad ad, AdSize adSize, String str) {
        this.b = new WeakReference<>(activity);
        this.c = ad;
        this.g = adSize;
        this.i = str;
        this.h = new f();
        this.d = null;
        this.e = null;
        this.f = null;
        this.n = false;
        this.l = new Handler();
        this.q = 0;
        this.o = false;
        synchronized (a) {
            this.p = activity.getApplicationContext().getSharedPreferences("GoogleAdMobAdsPrefs", 0);
            long j2 = this.p.getLong("InterstitialTimeout" + str, -1);
            if (j2 < 0) {
                this.m = 5000;
            } else {
                this.m = j2;
            }
        }
        this.r = new x(this);
        this.s = new LinkedList<>();
        this.t = new LinkedList<>();
        a();
        AdUtil.h(activity.getApplicationContext());
    }

    private synchronized boolean z() {
        return this.e != null;
    }

    public final synchronized void a() {
        Activity e2 = e();
        if (e2 == null) {
            a.a("activity was null while trying to create an AdWebView.");
        } else {
            this.j = new g(e2.getApplicationContext(), this.g);
            this.j.setVisibility(8);
            if (this.c instanceof AdView) {
                this.k = new h(this, a.b, true, false);
            } else {
                this.k = new h(this, a.b, true, true);
            }
            this.j.setWebViewClient(this.k);
        }
    }

    public final synchronized void a(float f2) {
        this.q = (long) (1000.0f * f2);
    }

    public final synchronized void a(int i2) {
        this.u = i2;
    }

    public final void a(long j2) {
        synchronized (a) {
            SharedPreferences.Editor edit = this.p.edit();
            edit.putLong("InterstitialTimeout" + this.i, j2);
            edit.commit();
            this.m = j2;
        }
    }

    public final synchronized void a(AdRequest.ErrorCode errorCode) {
        this.e = null;
        if (this.c instanceof InterstitialAd) {
            if (errorCode == AdRequest.ErrorCode.NO_FILL) {
                this.h.n();
            } else if (errorCode == AdRequest.ErrorCode.NETWORK_ERROR) {
                this.h.l();
            }
        }
        a.c("onFailedToReceiveAd(" + errorCode + ")");
        if (this.d != null) {
            this.d.onFailedToReceiveAd(this.c, errorCode);
        }
    }

    public final synchronized void a(AdRequest adRequest) {
        if (z()) {
            a.e("loadAd called while the ad is already loading.");
        } else {
            Activity e2 = e();
            if (e2 == null) {
                a.e("activity is null while trying to load an ad.");
            } else if (AdUtil.c(e2.getApplicationContext()) && AdUtil.b(e2.getApplicationContext())) {
                this.n = false;
                this.f = adRequest;
                this.e = new c(this);
                this.e.execute(adRequest);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(String str) {
        this.s.add(str);
    }

    /* access modifiers changed from: package-private */
    public final synchronized void b(String str) {
        this.t.add(str);
    }

    public final synchronized void c() {
        if (this.o) {
            a.a("Disabling refreshing.");
            this.l.removeCallbacks(this.r);
            this.o = false;
        } else {
            a.a("Refreshing is already disabled.");
        }
    }

    public final synchronized void d() {
        if (!(this.c instanceof AdView)) {
            a.a("Tried to enable refreshing on something other than an AdView.");
        } else if (!this.o) {
            a.a("Enabling refreshing every " + this.q + " milliseconds.");
            this.l.postDelayed(this.r, this.q);
            this.o = true;
        } else {
            a.a("Refreshing is already enabled.");
        }
    }

    public final Activity e() {
        return this.b.get();
    }

    /* access modifiers changed from: package-private */
    public final Ad f() {
        return this.c;
    }

    public final synchronized c g() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public final String h() {
        return this.i;
    }

    public final synchronized g i() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public final synchronized h j() {
        return this.k;
    }

    public final AdSize k() {
        return this.g;
    }

    public final f l() {
        return this.h;
    }

    public final synchronized int m() {
        return this.u;
    }

    public final long n() {
        long j2;
        if (!(this.c instanceof InterstitialAd)) {
            return 60000;
        }
        synchronized (a) {
            j2 = this.m;
        }
        return j2;
    }

    public final synchronized boolean p() {
        return this.o;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void q() {
        this.e = null;
        this.n = true;
        this.j.setVisibility(0);
        this.h.c();
        if (this.c instanceof AdView) {
            u();
        }
        a.c("onReceiveAd()");
        if (this.d != null) {
            this.d.onReceiveAd(this.c);
        }
    }

    public final synchronized void r() {
        this.h.o();
        a.c("onDismissScreen()");
        if (this.d != null) {
            this.d.onDismissScreen(this.c);
        }
    }

    public final synchronized void s() {
        this.h.b();
        a.c("onPresentScreen()");
        if (this.d != null) {
            this.d.onPresentScreen(this.c);
        }
    }

    public final synchronized void t() {
        a.c("onLeaveApplication()");
        if (this.d != null) {
            this.d.onLeaveApplication(this.c);
        }
    }

    public final synchronized void u() {
        Activity activity = this.b.get();
        if (activity == null) {
            a.e("activity was null while trying to ping tracking URLs.");
        } else {
            Iterator<String> it = this.s.iterator();
            while (it.hasNext()) {
                new Thread(new w(it.next(), activity.getApplicationContext())).start();
            }
            this.s.clear();
        }
    }

    public final synchronized boolean v() {
        boolean z;
        boolean z2 = !this.t.isEmpty();
        Activity activity = this.b.get();
        if (activity == null) {
            a.e("activity was null while trying to ping click tracking URLs.");
            z = z2;
        } else {
            Iterator<String> it = this.t.iterator();
            while (it.hasNext()) {
                new Thread(new w(it.next(), activity.getApplicationContext())).start();
            }
            this.t.clear();
            z = z2;
        }
        return z;
    }

    public final synchronized void w() {
        if (this.f == null) {
            a.a("Tried to refresh before calling loadAd().");
        } else if (this.c instanceof AdView) {
            if (!((AdView) this.c).isShown() || !AdUtil.b()) {
                a.a("Not refreshing because the ad is not visible.");
            } else {
                a.c("Refreshing ad.");
                a(this.f);
            }
            this.l.postDelayed(this.r, this.q);
        } else {
            a.a("Tried to refresh an ad that wasn't an AdView.");
        }
    }

    public final synchronized void y() {
        if (this.e != null) {
            this.e.cancel(false);
            this.e = null;
        }
        this.j.stopLoading();
    }
}
