package cn.com.jit.android.ida.util.pki.keystore;

import cn.com.jit.android.ida.util.pki.pkcs.PKCS12;
import cn.com.jit.ida.util.pki.Parser;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.pkcs.PKCSObjectIdentifiers;
import cn.com.jit.ida.util.pki.asn1.x509.AlgorithmIdentifier;
import cn.com.jit.ida.util.pki.cert.X509Cert;
import cn.com.jit.ida.util.pki.cipher.JCrypto;
import cn.com.jit.ida.util.pki.cipher.JKey;
import cn.com.jit.ida.util.pki.cipher.JKeyPair;
import cn.com.jit.ida.util.pki.cipher.Mechanism;
import cn.com.jit.ida.util.pki.cipher.Session;
import cn.com.jit.ida.util.pki.cipher.param.GenKeyAttribute;
import cn.com.jit.ida.util.pki.encoders.Base64;
import cn.com.jit.ida.util.pki.keystore.KeyEntry;
import cn.com.jit.ida.util.pki.pkcs.P7B;
import cn.com.jit.ida.util.pki.pkcs.PKCS10;
import com.jianq.misc.StringEx;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;

public class KeyStoreManager {
    public static final String TRUST_CERT_ID = "trustcertid";
    public static final String tmpCertStr = "MIIC5DCCAk2gAwIBAgIQa6gl/TivVSqXP8dPnZucXDANBgkqhkiG9w0BAQUFADBFMQswCQYDVQQGEwJDTjEMMAoGA1UEChMDSklUMRcwFQYDVQQLEw53d3cuaml0LmNvbS5jbjEPMA0GA1UEAxMGSklUIENBMB4XDTA1MDgxOTA4MTgxOFoXDTI1MDgxNDA4MTgxOFowRTELMAkGA1UEBhMCQ04xDDAKBgNVBAoTA0pJVDEXMBUGA1UECxMOd3d3LmppdC5jb20uY24xDzANBgNVBAMTBkpJVCBDQTCBnzANBgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEApoZCiaMfa8RFFVKZrUP+9QWHNk8c/clRiJIKl2WrUYYnUrL4KVYt7rA8NJdUAA63OM6w4Bd6uVuudGz949p3AkjiY0tOQNMsdsQfZoCdCykvsOazzboY2zTSAzwFZUt33+4nCCsxrxLbqvVSVwuN2x9+VKa327u8+hbYXOq+/JkCAwEAAaOB1DCB0TAfBgNVHSMEGDAWgBTPVrW0/9er9lukYtIum4uhHyzQBzAMBgNVHRMEBTADAQH/MHQGA1UdHwRtMGswQKA+oDykOjA4MQswCQYDVQQGEwJDTjEMMAoGA1UEChMDSklUMQwwCgYDVQQLEwNDUkwxDTALBgNVBAMTBENSTDEwJ6AloCOGIWh0dHA6Ly9qaXRjcmwuaml0LmNvbS5jbi9jcmwxLmNybDALBgNVHQ8EBAMCAf4wHQYDVR0OBBYEFM9WtbT/16v2W6Ri0i6bi6EfLNAHMA0GCSqGSIb3DQEBBQUAA4GBAH3FBD56Hebdyp1dh85vOmbxF/AVckS5aXUSkGlKzXJDOIGtr3mPb0r4m6NSJowDwrYpT+RcezFDacg9o+uLuU/q/9LaI4qmFVP1xISx+LG0liUDNoiySMa4TbV45RpkNOxUasLmhlx6oWXRMxTKuiv357yf0M6zGCuiseqNv6P3";
    private boolean isUserPriKeyPW = false;
    private FileOutputStream privateFfous = null;
    private File privateFile = null;
    private FileInputStream privateFin = null;
    private String privateKeyPassWord = StringEx.Empty;
    private String sType = "JKS";

    public void setPrivateFileOutputStream(FileOutputStream data) {
        this.privateFfous = data;
    }

    public void setPrivateFileInputStream(FileInputStream data) {
        this.privateFin = data;
    }

    public void setPrivateFile(File data) {
        this.privateFile = data;
    }

    public void setPrivateKeyPassWord(String privateKeyPassWord2) {
        this.privateKeyPassWord = privateKeyPassWord2;
    }

    public void ClearPrivateKeyPassWord() {
        this.privateKeyPassWord = StringEx.Empty;
    }

    public void UserPrivateKeyPassWord() {
        this.isUserPriKeyPW = true;
    }

    private String genP10Request(String deviceType, int hardKeyId, String keyStorePath, String keyStorePassword, String subject, String keyType, int keySize) throws Exception {
        Mechanism genKeyM;
        String signAlg;
        File file;
        FileOutputStream fous;
        Session session = openSession(deviceType);
        if (keyType.equalsIgnoreCase(Mechanism.RSA)) {
            genKeyM = new Mechanism(Mechanism.RSA);
            signAlg = "SHA1withRSAEncryption";
        } else if (keyType.equalsIgnoreCase(Mechanism.DSA)) {
            genKeyM = new Mechanism(Mechanism.DSA);
            signAlg = "SHA1withDSA";
        } else if (keyType.equalsIgnoreCase(Mechanism.SM2)) {
            genKeyM = new Mechanism(Mechanism.SM2);
            signAlg = "SM3withSM2Encryption";
        } else {
            genKeyM = new Mechanism(Mechanism.ECDSA);
            signAlg = "SHA1withECDSA";
        }
        JCrypto.getInstance().initialize(JCrypto.JSOFT_LIB, null);
        if (deviceType.equalsIgnoreCase(JCrypto.JSJY05B_LIB)) {
            GenKeyAttribute attr = new GenKeyAttribute();
            attr.setKeyNum(hardKeyId);
            attr.setIsExport(false);
            genKeyM.setParam(attr);
        }
        JKeyPair keyPair = session.generateKeyPair(genKeyM, keySize);
        JKey pubKey = keyPair.getPublicKey();
        JKey prvKey = keyPair.getPrivateKey();
        byte[] p10Request = new PKCS10(session).generateCertificationRequestData_B64(signAlg, subject, pubKey, null, prvKey);
        if (this.privateFile == null) {
            file = new File(keyStorePath);
        } else {
            file = this.privateFile;
        }
        if (!file.exists() && !file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        file.createNewFile();
        KeyStore keyStore = KeyStore.getInstance(this.sType);
        keyStore.load(null, null);
        Certificate tmpCert = CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(Base64.decode("MIIC5DCCAk2gAwIBAgIQa6gl/TivVSqXP8dPnZucXDANBgkqhkiG9w0BAQUFADBFMQswCQYDVQQGEwJDTjEMMAoGA1UEChMDSklUMRcwFQYDVQQLEw53d3cuaml0LmNvbS5jbjEPMA0GA1UEAxMGSklUIENBMB4XDTA1MDgxOTA4MTgxOFoXDTI1MDgxNDA4MTgxOFowRTELMAkGA1UEBhMCQ04xDDAKBgNVBAoTA0pJVDEXMBUGA1UECxMOd3d3LmppdC5jb20uY24xDzANBgNVBAMTBkpJVCBDQTCBnzANBgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEApoZCiaMfa8RFFVKZrUP+9QWHNk8c/clRiJIKl2WrUYYnUrL4KVYt7rA8NJdUAA63OM6w4Bd6uVuudGz949p3AkjiY0tOQNMsdsQfZoCdCykvsOazzboY2zTSAzwFZUt33+4nCCsxrxLbqvVSVwuN2x9+VKa327u8+hbYXOq+/JkCAwEAAaOB1DCB0TAfBgNVHSMEGDAWgBTPVrW0/9er9lukYtIum4uhHyzQBzAMBgNVHRMEBTADAQH/MHQGA1UdHwRtMGswQKA+oDykOjA4MQswCQYDVQQGEwJDTjEMMAoGA1UEChMDSklUMQwwCgYDVQQLEwNDUkwxDTALBgNVBAMTBENSTDEwJ6AloCOGIWh0dHA6Ly9qaXRjcmwuaml0LmNvbS5jbi9jcmwxLmNybDALBgNVHQ8EBAMCAf4wHQYDVR0OBBYEFM9WtbT/16v2W6Ri0i6bi6EfLNAHMA0GCSqGSIb3DQEBBQUAA4GBAH3FBD56Hebdyp1dh85vOmbxF/AVckS5aXUSkGlKzXJDOIGtr3mPb0r4m6NSJowDwrYpT+RcezFDacg9o+uLuU/q/9LaI4qmFVP1xISx+LG0liUDNoiySMa4TbV45RpkNOxUasLmhlx6oWXRMxTKuiv357yf0M6zGCuiseqNv6P3".getBytes())));
        if (deviceType.equalsIgnoreCase(JCrypto.JSOFT_LIB)) {
            keyStore.setKeyEntry(getAlias(pubKey), Parser.convertPrivateKey(prvKey), this.isUserPriKeyPW ? this.privateKeyPassWord.toCharArray() : keyStorePassword.toCharArray(), new Certificate[]{tmpCert});
        } else {
            keyStore.setCertificateEntry(subject.toLowerCase(), tmpCert);
        }
        if (this.privateFfous == null) {
            fous = new FileOutputStream(file);
        } else {
            fous = this.privateFfous;
        }
        keyStore.store(fous, keyStorePassword.toCharArray());
        fous.flush();
        fous.close();
        return new String(p10Request);
    }

    public String genP10RequestWithSoftLib(String keyStorePath, String keyStorePassword, String subject, String keyType, int keySize) throws Exception {
        return genP10Request(JCrypto.JSOFT_LIB, 0, keyStorePath, keyStorePassword, subject, keyType, keySize);
    }

    public String genP10RequestWithHardLib(int hardKeyId, String keyStorePath, String keyStorePassword, String subject, String keyType, int keySize) throws Exception {
        return genP10Request(JCrypto.JSJY05B_LIB, hardKeyId, keyStorePath, keyStorePassword, subject, keyType, keySize);
    }

    public String genP10Request4UpdateWithHardLib(int hardKeyId, String keyStorePath, String keyStorePassword, String subject, String keyType, int keySize) throws Exception {
        return genP10Request4Update(JCrypto.JSJY05B_LIB, hardKeyId, keyStorePath, keyStorePassword, keyType, keySize, subject);
    }

    public String genP10Request4UpdateWithSoftLib(String keyStorePath, String keyStorePassword, String subject, String keyType, int keySize) throws Exception {
        return genP10Request4Update(JCrypto.JSOFT_LIB, 0, keyStorePath, keyStorePassword, keyType, keySize, subject);
    }

    public boolean delAlias(String keyStorePath, String keyStorePassword, String alias) throws Exception {
        File file;
        FileOutputStream fous;
        if (this.privateFile == null) {
            file = new File(keyStorePath);
        } else {
            file = this.privateFile;
        }
        if (!file.getParentFile().exists()) {
            throw new Exception("keyStore file [" + keyStorePath + "] doesn't exists. please select a correct keyStore file to execute update.");
        }
        KeyStore keyStore = openKeyStore(keyStorePath, keyStorePassword);
        keyStore.deleteEntry(alias);
        if (this.privateFfous == null) {
            fous = new FileOutputStream(file);
        } else {
            fous = this.privateFfous;
        }
        keyStore.store(fous, keyStorePassword.toCharArray());
        fous.flush();
        fous.close();
        return true;
    }

    private String genP10Request4Update(String deviceType, int hardKeyId, String keyStorePath, String keyStorePassword, String keyType, int keySize, String subject) throws Exception {
        Mechanism genKeyM;
        String signAlg;
        File file;
        FileOutputStream fous;
        Session session = openSession(deviceType);
        if (keyType.equalsIgnoreCase(Mechanism.RSA)) {
            genKeyM = new Mechanism(Mechanism.RSA);
            signAlg = "SHA1withRSAEncryption";
        } else if (keyType.equalsIgnoreCase(Mechanism.DSA)) {
            genKeyM = new Mechanism(Mechanism.DSA);
            signAlg = "SHA1withDSA";
        } else if (keyType.equalsIgnoreCase(Mechanism.SM2)) {
            genKeyM = new Mechanism(Mechanism.SM2);
            signAlg = "SM3withSM2Encryption";
        } else {
            genKeyM = new Mechanism(Mechanism.ECDSA);
            signAlg = "SHA1withECDSA";
        }
        if (deviceType.equalsIgnoreCase(JCrypto.JSJY05B_LIB)) {
            GenKeyAttribute attr = new GenKeyAttribute();
            attr.setKeyNum(hardKeyId);
            attr.setIsExport(false);
            genKeyM.setParam(attr);
        }
        JKeyPair keyPair = session.generateKeyPair(genKeyM, keySize);
        JKey pubKey = keyPair.getPublicKey();
        JKey prvKey = keyPair.getPrivateKey();
        byte[] p10Request = new PKCS10(session).generateCertificationRequestData_B64(signAlg, subject, pubKey, null, prvKey);
        if (this.privateFile == null) {
            file = new File(keyStorePath);
        } else {
            file = this.privateFile;
        }
        if (!file.getParentFile().exists()) {
            throw new Exception("keyStore file [" + keyStorePath + "] doesn't exists. please select a correct keyStore file to execute update.");
        }
        KeyStore keyStore = openKeyStore(keyStorePath, keyStorePassword);
        if (deviceType.equalsIgnoreCase(JCrypto.JSOFT_LIB)) {
            PrivateKey convertPrivateKey = Parser.convertPrivateKey(prvKey);
            CertificateFactory certFac = CertificateFactory.getInstance("X.509");
            byte[] tmpCertData = Base64.decode("MIIC5DCCAk2gAwIBAgIQa6gl/TivVSqXP8dPnZucXDANBgkqhkiG9w0BAQUFADBFMQswCQYDVQQGEwJDTjEMMAoGA1UEChMDSklUMRcwFQYDVQQLEw53d3cuaml0LmNvbS5jbjEPMA0GA1UEAxMGSklUIENBMB4XDTA1MDgxOTA4MTgxOFoXDTI1MDgxNDA4MTgxOFowRTELMAkGA1UEBhMCQ04xDDAKBgNVBAoTA0pJVDEXMBUGA1UECxMOd3d3LmppdC5jb20uY24xDzANBgNVBAMTBkpJVCBDQTCBnzANBgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEApoZCiaMfa8RFFVKZrUP+9QWHNk8c/clRiJIKl2WrUYYnUrL4KVYt7rA8NJdUAA63OM6w4Bd6uVuudGz949p3AkjiY0tOQNMsdsQfZoCdCykvsOazzboY2zTSAzwFZUt33+4nCCsxrxLbqvVSVwuN2x9+VKa327u8+hbYXOq+/JkCAwEAAaOB1DCB0TAfBgNVHSMEGDAWgBTPVrW0/9er9lukYtIum4uhHyzQBzAMBgNVHRMEBTADAQH/MHQGA1UdHwRtMGswQKA+oDykOjA4MQswCQYDVQQGEwJDTjEMMAoGA1UEChMDSklUMQwwCgYDVQQLEwNDUkwxDTALBgNVBAMTBENSTDEwJ6AloCOGIWh0dHA6Ly9qaXRjcmwuaml0LmNvbS5jbi9jcmwxLmNybDALBgNVHQ8EBAMCAf4wHQYDVR0OBBYEFM9WtbT/16v2W6Ri0i6bi6EfLNAHMA0GCSqGSIb3DQEBBQUAA4GBAH3FBD56Hebdyp1dh85vOmbxF/AVckS5aXUSkGlKzXJDOIGtr3mPb0r4m6NSJowDwrYpT+RcezFDacg9o+uLuU/q/9LaI4qmFVP1xISx+LG0liUDNoiySMa4TbV45RpkNOxUasLmhlx6oWXRMxTKuiv357yf0M6zGCuiseqNv6P3".getBytes());
            Certificate tmpCert = certFac.generateCertificate(new ByteArrayInputStream(tmpCertData));
            new X509Cert(tmpCertData);
            String alias = getAlias(pubKey);
            Enumeration emuAlias = keyStore.aliases();
            while (emuAlias.hasMoreElements()) {
                String aliasold = emuAlias.nextElement();
                if (keyStore.isKeyEntry(aliasold) && aliasold.equals(alias)) {
                    keyStore.deleteEntry(alias);
                }
            }
            keyStore.setKeyEntry(alias, convertPrivateKey, this.isUserPriKeyPW ? this.privateKeyPassWord.toCharArray() : keyStorePassword.toCharArray(), new Certificate[]{tmpCert});
        }
        if (this.privateFfous == null) {
            fous = new FileOutputStream(file);
        } else {
            fous = this.privateFfous;
        }
        keyStore.store(fous, keyStorePassword.toCharArray());
        fous.flush();
        fous.close();
        return new String(p10Request);
    }

    public KeyEntry addKeyCertWithPfx(String keyStorePath, String keyStorePassword, JKey prvKey, X509Cert[] certs) throws Exception {
        FileOutputStream fous;
        Mechanism signM;
        X509Cert[] trustCerts = null;
        X509Cert keyCert = null;
        if (certs != null) {
            if (certs.length >= 2) {
                trustCerts = new X509Cert[(certs.length - 1)];
                Session session = openSession(JCrypto.JSOFT_LIB);
                int m = 0;
                for (int i = 0; i < certs.length; i++) {
                    JKey pubKey = certs[i].getPublicKey();
                    if (pubKey.getKeyType().equals(JKey.RSA_PUB_KEY)) {
                        signM = new Mechanism("SHA1withRSAEncryption");
                    } else {
                        signM = new Mechanism("SHA1withECDSA");
                    }
                    byte[] sourceData = "JIT".getBytes();
                    if (session.verifySign(signM, pubKey, sourceData, session.sign(signM, prvKey, sourceData))) {
                        keyCert = certs[i];
                    } else {
                        trustCerts[m] = certs[i];
                        m++;
                    }
                }
            } else {
                keyCert = certs[0];
            }
        }
        KeyStore keyStore = openKeyStore(keyStorePath, keyStorePassword);
        String lowerCase = keyCert.getSubject().toLowerCase();
        Key key = Parser.convertPrivateKey(prvKey);
        Certificate javaCert = convert2JavaCert(keyCert);
        String alias = getAlias(keyCert.getPublicKey());
        Enumeration emuAlias = keyStore.aliases();
        while (emuAlias.hasMoreElements()) {
            String aliasold = emuAlias.nextElement();
            if (keyStore.isKeyEntry(aliasold) && aliasold.equals(alias)) {
                keyStore.deleteEntry(alias);
            }
        }
        keyStore.setKeyEntry(alias, key, this.isUserPriKeyPW ? this.privateKeyPassWord.toCharArray() : keyStorePassword.toCharArray(), new Certificate[]{javaCert});
        if (this.privateFfous == null) {
            fous = new FileOutputStream(keyStorePath);
        } else {
            fous = this.privateFfous;
        }
        keyStore.store(fous, keyStorePassword.toCharArray());
        fous.flush();
        fous.close();
        if (trustCerts != null) {
            setTrustCerts(keyStorePath, keyStorePassword, trustCerts);
        }
        KeyEntry keyEntry = new KeyEntry();
        keyEntry.setAilas(alias);
        keyEntry.setKey(prvKey);
        keyEntry.setCert(keyCert);
        return keyEntry;
    }

    private void setKeyCertWithPfx(String keyStorePath, String keyStorePassword, JKey prvKey, X509Cert[] certs) throws Exception {
        FileOutputStream fous;
        Mechanism signM;
        X509Cert[] trustCerts = null;
        X509Cert keyCert = null;
        if (certs != null) {
            if (certs.length >= 2) {
                trustCerts = new X509Cert[(certs.length - 1)];
                Session session = openSession(JCrypto.JSOFT_LIB);
                int m = 0;
                for (int i = 0; i < certs.length; i++) {
                    JKey pubKey = certs[i].getPublicKey();
                    if (pubKey.getKeyType().equals(JKey.RSA_PUB_KEY)) {
                        signM = new Mechanism("SHA1withRSAEncryption");
                    } else {
                        signM = new Mechanism("SHA1withECDSA");
                    }
                    byte[] sourceData = "JIT".getBytes();
                    if (session.verifySign(signM, pubKey, sourceData, session.sign(signM, prvKey, sourceData))) {
                        keyCert = certs[i];
                    } else {
                        trustCerts[m] = certs[i];
                        m++;
                    }
                }
            } else {
                keyCert = certs[0];
            }
        }
        KeyStore keyStore = openKeyStore(keyStorePath, keyStorePassword);
        Enumeration emuAlias = keyStore.aliases();
        while (emuAlias.hasMoreElements()) {
            keyStore.deleteEntry(emuAlias.nextElement());
        }
        String lowerCase = keyCert.getSubject().toLowerCase();
        keyStore.setKeyEntry(getAlias(keyCert), Parser.convertPrivateKey(prvKey), this.isUserPriKeyPW ? this.privateKeyPassWord.toCharArray() : keyStorePassword.toCharArray(), new Certificate[]{convert2JavaCert(keyCert)});
        if (this.privateFfous == null) {
            fous = new FileOutputStream(keyStorePath);
        } else {
            fous = this.privateFfous;
        }
        keyStore.store(fous, keyStorePassword.toCharArray());
        fous.flush();
        fous.close();
        if (trustCerts != null) {
            setTrustCerts(keyStorePath, keyStorePassword, trustCerts);
        }
    }

    public void genKeyStoreWithPfx(String keyStorePath, String keyStorePassword, String fileName, char[] pfxPassword) throws Exception {
        Session openSession = openSession(JCrypto.JSOFT_LIB);
        PKCS12 pKCS12 = new PKCS12();
        pKCS12.load(fileName);
        pKCS12.decrypt(pfxPassword);
        setKeyCertWithPfx(keyStorePath, keyStorePassword, pKCS12.getPrivateKey(), pKCS12.getCerts());
    }

    public KeyEntry addKeyStoreWithPfx(String keyStorePath, String keyStorePassword, String fileName, char[] pfxPassword) throws Exception {
        Session openSession = openSession(JCrypto.JSOFT_LIB);
        PKCS12 pKCS12 = new PKCS12();
        pKCS12.load(fileName);
        pKCS12.decrypt(pfxPassword);
        return addKeyCertWithPfx(keyStorePath, keyStorePassword, pKCS12.getPrivateKey(), pKCS12.getCerts());
    }

    public void genKeyStoreWithPfx(String keyStorePath, String keyStorePassword, byte[] pfxData, char[] pfxPassword) throws Exception {
        Session openSession = openSession(JCrypto.JSOFT_LIB);
        PKCS12 pKCS12 = new PKCS12();
        pKCS12.load(pfxData);
        pKCS12.decrypt(pfxPassword);
        setKeyCertWithPfx(keyStorePath, keyStorePassword, pKCS12.getPrivateKey(), pKCS12.getCerts());
    }

    public void genKeyStoreWithPfx(String keyStorePath, String keyStorePassword, InputStream ins, char[] pfxPassword) throws Exception {
        Session openSession = openSession(JCrypto.JSOFT_LIB);
        PKCS12 pKCS12 = new PKCS12();
        pKCS12.load(ins);
        pKCS12.decrypt(pfxPassword);
        setKeyCertWithPfx(keyStorePath, keyStorePassword, pKCS12.getPrivateKey(), pKCS12.getCerts());
    }

    public void setKeyCertWithSoftLib(String keyStorePath, String keyStorePassword, X509Cert keyCert) throws Exception {
        Mechanism signM;
        FileOutputStream fous;
        KeyStore keyStore = openKeyStore(keyStorePath, keyStorePassword);
        String alias = getAlias(keyCert.getPublicKey());
        Key key = keyStore.getKey(alias, this.isUserPriKeyPW ? this.privateKeyPassWord.toCharArray() : keyStorePassword.toCharArray());
        if (key == null) {
            throw new Exception("KeyStore doesn't contain key enry named [" + alias + "]");
        }
        JKey prvKey = new JKey(JKey.RSA_PRV_KEY, key.getEncoded());
        JKey pubKey = keyCert.getPublicKey();
        if (pubKey.getKeyType().equals(JKey.RSA_PUB_KEY)) {
            signM = new Mechanism("SHA1withRSAEncryption");
        } else {
            signM = new Mechanism("SHA1withECDSA");
        }
        byte[] data = "JIT".getBytes();
        Session session = openSession(JCrypto.JSOFT_LIB);
        if (!session.verifySign(signM, pubKey, data, session.sign(signM, prvKey, data))) {
            throw new Exception("verify certificate public key failure.");
        }
        keyStore.deleteEntry(alias);
        keyStore.setKeyEntry(alias, key, this.isUserPriKeyPW ? this.privateKeyPassWord.toCharArray() : keyStorePassword.toCharArray(), new Certificate[]{convert2JavaCert(keyCert)});
        if (this.privateFfous == null) {
            fous = new FileOutputStream(keyStorePath);
        } else {
            fous = this.privateFfous;
        }
        keyStore.store(fous, keyStorePassword.toCharArray());
        fous.flush();
        fous.close();
    }

    public void setKeyCertWithHardLib(int hardKeyId, String keyStorePath, String keyStorePassword, X509Cert keyCert) throws Exception {
        Mechanism genKeyM;
        Session session = openSession(JCrypto.JSJY05B_LIB);
        GenKeyAttribute attr = new GenKeyAttribute();
        attr.setKeyNum(hardKeyId);
        attr.setIsExport(false);
        JKey certPubKey = keyCert.getPublicKey();
        if (certPubKey.getKeyType().equals(JKey.RSA_PUB_KEY)) {
            genKeyM = new Mechanism(Mechanism.RSA);
        } else {
            genKeyM = new Mechanism(Mechanism.ECDSA);
        }
        genKeyM.setParam(attr);
        if (!Arrays.equals(session.generateKeyPair(genKeyM, 1024).getPublicKey().getKey(), certPubKey.getKey())) {
            throw new Exception("verify certificate public key failure.");
        }
        session.destroyCertObject(null, session.getCfgTag().getNoExportRSAKey(hardKeyId).getBytes());
        session.createCertObject(keyCert.getSubject().getBytes(), keyCert.getEncoded(), session.getCfgTag().getNoExportRSAKey(hardKeyId).getBytes());
        setTrustCert(keyStorePath, keyStorePassword, keyCert);
    }

    public void setKeyCertWithHardLib(int hardKeyId, String keyStorePath, String keyStorePassword, X509Cert keyCert, String cfgName) throws Exception {
        Mechanism genKeyM;
        Session session = JCrypto.getInstance().openSession(JCrypto.JSJY05B_LIB, cfgName);
        GenKeyAttribute attr = new GenKeyAttribute();
        attr.setKeyNum(hardKeyId);
        attr.setIsExport(false);
        JKey certPubKey = keyCert.getPublicKey();
        if (certPubKey.getKeyType().equals(JKey.RSA_PUB_KEY)) {
            genKeyM = new Mechanism(Mechanism.RSA);
        } else {
            genKeyM = new Mechanism(Mechanism.ECDSA);
        }
        genKeyM.setParam(attr);
        if (!Arrays.equals(session.generateKeyPair(genKeyM, 1024).getPublicKey().getKey(), certPubKey.getKey())) {
            throw new Exception("verify certificate public key failure.");
        }
        session.destroyCertObject(null, session.getCfgTag().getNoExportRSAKey(hardKeyId).getBytes());
        session.createCertObject(keyCert.getSubject().getBytes(), keyCert.getEncoded(), session.getCfgTag().getNoExportRSAKey(hardKeyId).getBytes());
        setTrustCert(keyStorePath, keyStorePassword, keyCert);
    }

    public void setTrustCertWithP7B(String keyStorePath, String keyStorePassword, String fileName) throws Exception {
        setTrustCerts(keyStorePath, keyStorePassword, new P7B().parseP7b(fileName));
    }

    public void setTrustCertWithP7B(String keyStorePath, String keyStorePassword, byte[] p7bData) throws Exception {
        setTrustCerts(keyStorePath, keyStorePassword, new P7B().parseP7b(p7bData));
    }

    public void setTrustCert(String keyStorePath, String keyStorePassword, X509Cert cert) throws Exception {
        FileOutputStream fous;
        KeyStore keyStore = openKeyStore(keyStorePath, keyStorePassword);
        String alias = getAlias(cert);
        if (keyStore.containsAlias(alias)) {
            keyStore.deleteEntry(alias);
        }
        keyStore.setCertificateEntry(alias, convert2JavaCert(cert));
        if (this.privateFfous == null) {
            fous = new FileOutputStream(keyStorePath);
        } else {
            fous = this.privateFfous;
        }
        keyStore.store(fous, keyStorePassword.toCharArray());
        fous.flush();
        fous.close();
    }

    public void setTrustCerts(String keyStorePath, String keyStorePassword, X509Cert[] certs) throws Exception {
        FileOutputStream fous;
        KeyStore keyStore = openKeyStore(keyStorePath, keyStorePassword);
        for (int i = 0; i < certs.length; i++) {
            String alias = getAlias(certs[i]);
            if (keyStore.containsAlias(alias)) {
                keyStore.deleteEntry(alias);
            }
            keyStore.setCertificateEntry(alias, convert2JavaCert(certs[i]));
        }
        if (this.privateFfous == null) {
            fous = new FileOutputStream(keyStorePath);
        } else {
            fous = this.privateFfous;
        }
        keyStore.store(fous, keyStorePassword.toCharArray());
        fous.flush();
        fous.close();
    }

    public Enumeration alias(String keyStorePath, String keyStorePassword) throws Exception {
        return openKeyStore(keyStorePath, keyStorePassword).aliases();
    }

    public X509Cert getCertEntry(String keyStorePath, String keyStorePassword, String alias) throws Exception {
        Certificate javaCert = openKeyStore(keyStorePath, keyStorePassword).getCertificate(alias);
        if (javaCert != null) {
            return new X509Cert(javaCert.getEncoded());
        }
        return null;
    }

    public KeyEntry getKeyEntry(String keyStorePath, String keyStorePassword) throws Exception {
        KeyEntry keyEntry = new KeyEntry();
        KeyStore keyStore = openKeyStore(keyStorePath, keyStorePassword);
        Enumeration emuAlias = keyStore.aliases();
        while (emuAlias.hasMoreElements()) {
            String alias = emuAlias.nextElement();
            if (keyStore.isKeyEntry(alias)) {
                JKey key = null;
                if (!this.isUserPriKeyPW) {
                    Key javaKey = keyStore.getKey(alias, keyStorePassword.toCharArray());
                    if (AlgorithmIdentifier.getInstance(((ASN1Sequence) Parser.writeBytes2DERObj(javaKey.getEncoded())).getObjectAt(1)).getObjectId().equals(PKCSObjectIdentifiers.rsaEncryption)) {
                        key = new JKey(JKey.RSA_PRV_KEY, javaKey.getEncoded());
                    } else {
                        key = new JKey(JKey.ECDSA_PRV_KEY, javaKey.getEncoded());
                    }
                }
                X509Cert cert = new X509Cert(keyStore.getCertificate(alias).getEncoded());
                keyEntry.setAilas(alias);
                keyEntry.setCert(cert);
                keyEntry.setKey(key);
            }
        }
        return keyEntry;
    }

    public JKey getJKey(String keyStorePath, String keyStorePassword, String alias) throws Exception {
        Key javaKey = openKeyStore(keyStorePath, keyStorePassword).getKey(alias, this.isUserPriKeyPW ? this.privateKeyPassWord.toCharArray() : keyStorePassword.toCharArray());
        if (AlgorithmIdentifier.getInstance(((ASN1Sequence) Parser.writeBytes2DERObj(javaKey.getEncoded())).getObjectAt(1)).getObjectId().equals(PKCSObjectIdentifiers.rsaEncryption)) {
            return new JKey(JKey.RSA_PRV_KEY, javaKey.getEncoded());
        }
        return new JKey(JKey.ECDSA_PRV_KEY, javaKey.getEncoded());
    }

    public List<KeyEntry> getTrustCertList(String keyStorePath, String keyStorePassword) throws Exception {
        List<KeyEntry> keyEntryList = new ArrayList<>();
        KeyStore keyStore = openKeyStore(keyStorePath, keyStorePassword);
        Enumeration emuAlias = keyStore.aliases();
        while (emuAlias.hasMoreElements()) {
            String alias = emuAlias.nextElement();
            if (keyStore.isCertificateEntry(alias)) {
                X509Cert cert = new X509Cert(keyStore.getCertificate(alias).getEncoded());
                KeyEntry keyEntry = new KeyEntry();
                keyEntry.setAilas(alias);
                keyEntry.setCert(cert);
                keyEntry.setKey(null);
                keyEntryList.add(keyEntry);
            }
        }
        return keyEntryList;
    }

    public List<KeyEntry> getKeyEntryList(String keyStorePath, String keyStorePassword) throws Exception {
        List<KeyEntry> keyEntryList = new ArrayList<>();
        KeyStore keyStore = openKeyStore(keyStorePath, keyStorePassword);
        Enumeration emuAlias = keyStore.aliases();
        while (emuAlias.hasMoreElements()) {
            String alias = emuAlias.nextElement();
            if (keyStore.isKeyEntry(alias)) {
                JKey key = null;
                if (!this.isUserPriKeyPW) {
                    Key javaKey = keyStore.getKey(alias, keyStorePassword.toCharArray());
                    if (AlgorithmIdentifier.getInstance(((ASN1Sequence) Parser.writeBytes2DERObj(javaKey.getEncoded())).getObjectAt(1)).getObjectId().equals(PKCSObjectIdentifiers.rsaEncryption)) {
                        key = new JKey(JKey.RSA_PRV_KEY, javaKey.getEncoded());
                    } else {
                        key = new JKey(JKey.ECDSA_PRV_KEY, javaKey.getEncoded());
                    }
                }
                X509Cert cert = new X509Cert(keyStore.getCertificate(alias).getEncoded());
                KeyEntry keyEntry = new KeyEntry();
                keyEntry.setAilas(alias);
                keyEntry.setCert(cert);
                keyEntry.setKey(key);
                keyEntryList.add(keyEntry);
            }
        }
        return keyEntryList;
    }

    private Certificate convert2JavaCert(X509Cert jitCert) throws Exception {
        return CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(jitCert.getEncoded()));
    }

    private Session openSession(String deviceType) throws Exception {
        JCrypto jcrypto = JCrypto.getInstance();
        jcrypto.initialize(deviceType, null);
        return jcrypto.openSession(deviceType);
    }

    private KeyStore openKeyStore(String keyStorePath, String keyStorePassword) throws Exception {
        File file;
        FileInputStream fin;
        KeyStore keyStore = KeyStore.getInstance(this.sType);
        if (this.privateFile == null) {
            file = new File(keyStorePath);
        } else {
            file = this.privateFile;
        }
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        if (!file.exists()) {
            file.createNewFile();
            keyStore.load(null, null);
        } else {
            if (this.privateFin != null) {
                fin = this.privateFin;
            } else {
                fin = new FileInputStream(keyStorePath);
            }
            keyStore.load(fin, keyStorePassword.toCharArray());
            fin.close();
        }
        return keyStore;
    }

    public String getAlias(X509Cert keyCert) throws Exception {
        return getAlias(keyCert.getPublicKey());
    }

    public String getAlias(JKey key) throws Exception {
        return new String(Base64.encode(openSession(JCrypto.JSOFT_LIB).digest(new Mechanism(Mechanism.SHA1), key.getKey())));
    }

    public void setStoreType(String type) throws Exception {
        this.sType = type;
    }
}
