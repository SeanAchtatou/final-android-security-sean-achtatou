package cn.com.jit.pnxclient.net;

import com.jianq.misc.StringEx;
import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.security.KeyStore;
import java.security.Provider;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Properties;
import java.util.prefs.Preferences;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

public class SSLContextFactory {
    private static final String IBMPKCS11 = "com.ibm.crypto.pkcs11impl.provider.IBMPKCS11Impl";
    private static final String KeyStoreTypeIBMPKCS11 = "PKCS11IMPLKS";
    private static final String KeyStoreTypePKCS11 = "PKCS11";
    private static final String P11_CFG_NAME = "name";
    private static final String SUNPKCS11 = "sun.security.pkcs11.SunPKCS11";
    private static SSLContext clientInstance = null;
    private static SSLContext serverInstance = null;

    public static SSLContext getSSLContextInstance(boolean serverMode, SSLConfig sslConfig) throws Exception {
        SSLContext createSSLServerContext;
        if (!serverMode) {
            return createSSLClientContext(sslConfig);
        }
        synchronized (SSLContextFactory.class) {
            createSSLServerContext = createSSLServerContext(sslConfig);
        }
        return createSSLServerContext;
    }

    private static SSLContext createSSLServerContext(SSLConfig sslConfig) throws Exception {
        SSLContext ctx;
        KeyManagerFactory kmf;
        KeyStore keyStore;
        char[] charArray;
        TrustManagerFactory tmf;
        KeyStore keyStore2;
        String vmVender = System.getProperty("java.vm.vendor");
        boolean kmfSoft = true;
        if (sslConfig.getKeyStoreType().startsWith(KeyStoreTypePKCS11)) {
            kmfSoft = false;
        }
        if (kmfSoft) {
            ctx = SSLContext.getInstance(sslConfig.getProtocol());
            KeyStore keyStore3 = KeyStore.getInstance(sslConfig.getKeyStoreType());
            String keyStorePath = sslConfig.getKeyStorePath();
            String keyStorePassword = sslConfig.getKeyStorePassword();
            FileInputStream keyStoreFileIns = new FileInputStream(keyStorePath);
            keyStore3.load(keyStoreFileIns, keyStorePassword.toCharArray());
            keyStoreFileIns.close();
            kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            kmf.init(keyStore3, keyStorePassword.toCharArray());
        } else {
            String pin = sslConfig.getKeyStorePassword();
            String configName = sslConfig.getCfgName();
            if (vmVender.toUpperCase().indexOf("SUN") != -1) {
                addSunProvider(configName);
                ctx = SSLContext.getInstance("SSL");
                kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
                keyStore = KeyStore.getInstance(KeyStoreTypePKCS11);
            } else if (vmVender.toUpperCase().indexOf("HEWLETT") != -1) {
                addSunProvider(configName);
                ctx = SSLContext.getInstance("SSL");
                kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
                keyStore = KeyStore.getInstance(KeyStoreTypePKCS11);
            } else {
                addIBMProvider(sslConfig.getHardDriver(), pin);
                ctx = SSLContext.getInstance("SSL", "IBMJSSE2");
                kmf = KeyManagerFactory.getInstance("IBMX509", "IBMJSSE2");
                keyStore = KeyStore.getInstance(KeyStoreTypeIBMPKCS11);
            }
            keyStore.load(null, pin != null ? pin.toCharArray() : StringEx.Empty.toCharArray());
            if (pin != null) {
                charArray = pin.toCharArray();
            } else {
                charArray = StringEx.Empty.toCharArray();
            }
            kmf.init(keyStore, charArray);
        }
        boolean tmfSoft = true;
        if (sslConfig.getTrustStoreType().startsWith(KeyStoreTypePKCS11)) {
            tmfSoft = false;
        }
        if (tmfSoft) {
            KeyStore trustStore = KeyStore.getInstance(sslConfig.getTrustStoreType());
            String trustStorePath = sslConfig.getTrustStorePath();
            String trustStorePassword = sslConfig.getTrustStorePassword();
            FileInputStream trustStoreFileIns = new FileInputStream(trustStorePath);
            trustStore.load(trustStoreFileIns, trustStorePassword.toCharArray());
            trustStoreFileIns.close();
            tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(trustStore);
        } else {
            String pin2 = sslConfig.getKeyStorePassword();
            if (vmVender.toUpperCase().indexOf("SUN") != -1) {
                tmf = TrustManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
                keyStore2 = KeyStore.getInstance(KeyStoreTypePKCS11);
            } else if (vmVender.toUpperCase().indexOf("HEWLETT") != -1) {
                tmf = TrustManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
                keyStore2 = KeyStore.getInstance(KeyStoreTypePKCS11);
            } else {
                tmf = TrustManagerFactory.getInstance("IbmX509", "IBMJSSE2");
                keyStore2 = KeyStore.getInstance(KeyStoreTypeIBMPKCS11);
            }
            keyStore2.load(null, pin2.toCharArray());
            tmf.init(keyStore2);
        }
        ctx.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
        return ctx;
    }

    private static SSLContext createSSLClientContext(SSLConfig sslConfig) throws Exception {
        String protocol = "TLS";
        if (sslConfig != null) {
            protocol = sslConfig.getProtocol();
        }
        SSLContext ctx = SSLContext.getInstance(protocol);
        ctx.init(null, new TrustManager[]{new TrustAnyTrustManager()}, null);
        return ctx;
    }

    public static Object createObject(Constructor constructor, Object[] arguments) throws Exception {
        try {
            return constructor.newInstance(arguments);
        } catch (InstantiationException e) {
            throw new Exception("construct security provider of PKCS11 error", e);
        } catch (IllegalAccessException e2) {
            throw new Exception("construct security provider of PKCS11 error", e2);
        } catch (IllegalArgumentException e3) {
            throw new Exception("construct security provider of PKCS11 error", e3);
        } catch (InvocationTargetException e4) {
            throw new Exception("construct security provider of PKCS11 error", e4);
        }
    }

    public static Provider addIBMProvider(String hardDriver, String pin) throws Exception {
        if (Security.getProvider("IBMPKCS11Impl") == null) {
            Class cls = Class.forName(IBMPKCS11);
            Preferences prefs = Preferences.userNodeForPackage(cls);
            prefs.put("IBMPKCSImpl DLL", hardDriver);
            if (pin == null) {
                pin = StringEx.Empty;
            }
            prefs.put("IBMPKCSImpl password", pin);
            Provider pl = (Provider) cls.newInstance();
            Security.addProvider(pl);
            prefs.remove("IBMPKCSImpl DLL");
            prefs.remove("IBMPKCSImpl password");
            return pl;
        } else if (Security.getProvider("IBMJSSE2") != null) {
            return null;
        } else {
            Provider px = (Provider) Class.forName("com.ibm.jsse2.IBMJSSEProvider2").newInstance();
            Security.addProvider(px);
            return px;
        }
    }

    public static Provider addSunProvider(String cfgName) throws Exception {
        if (isSunProviderExist(cfgName)) {
            return null;
        }
        Object[] strArgs = {cfgName};
        Provider p = (Provider) createObject(Class.forName(SUNPKCS11).getConstructor(String.class), strArgs);
        Security.addProvider(p);
        return p;
    }

    private static boolean isSunProviderExist(String cfgName) {
        if (Security.getProvider(getSunProviderName(cfgName)) != null) {
            return true;
        }
        return false;
    }

    public static String getSunProviderName(String cfgName) {
        if (!new File(cfgName).exists()) {
            return null;
        }
        Properties props = new Properties();
        try {
            FileInputStream fileInputStream = new FileInputStream(cfgName);
            try {
                props.load(fileInputStream);
                fileInputStream.close();
                String name = props.getProperty(P11_CFG_NAME);
                if (!name.startsWith("SunPKCS11-")) {
                    return "SunPKCS11-" + name;
                }
                return name;
            } catch (Throwable th) {
                e = th;
                e.printStackTrace();
                return null;
            }
        } catch (Throwable th2) {
            e = th2;
            e.printStackTrace();
            return null;
        }
    }

    private static class TrustAnyTrustManager implements X509TrustManager {
        private TrustAnyTrustManager() {
        }

        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[0];
        }
    }
}
