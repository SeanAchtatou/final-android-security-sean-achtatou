package cn.com.jit.pnxclient.net;

import java.io.File;

public class SSLConfig {
    private String cfgName;
    private String hardDriver;
    private String keyStorePassword;
    private String keyStorePath;
    private String keyStoreType;
    private String needClientAuth;
    private String protocol = "TLS";
    private long sslSessionTimeout;
    private String trustStorePassword;
    private String trustStorePath;
    private String trustStoreType;

    public String getHardDriver() {
        File file = new File(this.hardDriver);
        if (file.exists()) {
            return file.getAbsolutePath();
        }
        return null;
    }

    public void setHardDriver(String hardDriver2) {
        this.hardDriver = hardDriver2;
    }

    public String getCfgName() {
        File file = new File(this.cfgName);
        if (file.exists()) {
            return file.getAbsolutePath();
        }
        return null;
    }

    public void setCfgName(String cfgName2) {
        this.cfgName = cfgName2;
    }

    public long getSslSessionTimeout() {
        return this.sslSessionTimeout;
    }

    public void setSslSessionTimeout(long sslSessionTimeout2) {
        this.sslSessionTimeout = sslSessionTimeout2;
    }

    public String getKeyStorePassword() {
        return this.keyStorePassword;
    }

    public void setKeyStorePassword(String keyStorePassword2) {
        this.keyStorePassword = keyStorePassword2;
    }

    public String getKeyStorePath() {
        if (this.keyStorePath == null) {
            return null;
        }
        File f = new File(this.keyStorePath);
        if (f.exists()) {
            return f.getAbsolutePath();
        }
        return null;
    }

    public void setKeyStorePath(String keyStorePath2) {
        this.keyStorePath = keyStorePath2;
    }

    public String getKeyStoreType() {
        return this.keyStoreType;
    }

    public void setKeyStoreType(String keyStoreType2) {
        this.keyStoreType = keyStoreType2;
    }

    public String getProtocol() {
        return this.protocol;
    }

    public void setProtocol(String protocol2) {
        this.protocol = protocol2;
    }

    public String getTrustStorePassword() {
        return this.trustStorePassword;
    }

    public void setTrustStorePassword(String trustStorePassword2) {
        this.trustStorePassword = trustStorePassword2;
    }

    public String getTrustStorePath() {
        if (this.trustStorePath == null) {
            return null;
        }
        File f = new File(this.trustStorePath);
        if (f.exists()) {
            return f.getAbsolutePath();
        }
        return null;
    }

    public void setTrustStorePath(String trustStorePath2) {
        this.trustStorePath = trustStorePath2;
    }

    public String getTrustStoreType() {
        return this.trustStoreType;
    }

    public void setTrustStoreType(String trustStoreType2) {
        this.trustStoreType = trustStoreType2;
    }

    public String getNeedClientAuth() {
        return this.needClientAuth;
    }

    public void setNeedClientAuth(String needClientAuth2) {
        this.needClientAuth = needClientAuth2;
    }
}
