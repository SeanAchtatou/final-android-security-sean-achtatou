package cn.com.jit.pnxclient;

public interface PNXClientSupport {
    String getErrorCode();

    String getLastError();
}
