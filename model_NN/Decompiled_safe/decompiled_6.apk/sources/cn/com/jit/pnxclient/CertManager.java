package cn.com.jit.pnxclient;

import android.util.Log;
import cn.com.jit.android.ida.util.pki.keystore.KeyStoreManager;
import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.cert.X509Cert;
import cn.com.jit.ida.util.pki.cipher.JCrypto;
import cn.com.jit.ida.util.pki.cipher.JKey;
import cn.com.jit.ida.util.pki.cipher.Mechanism;
import cn.com.jit.ida.util.pki.keystore.KeyEntry;
import cn.com.jit.ida.util.pki.pkcs.P7B;
import cn.com.jit.ida.util.pki.pkcs.PKCS10;
import cn.com.jit.pnxclient.constant.MessageCode;
import cn.com.jit.pnxclient.constant.PNXConfigConstant;
import cn.com.jit.pnxclient.exception.PNXClientException;
import cn.com.jit.pnxclient.net.Gwtk;
import cn.com.jit.pnxclient.net.MessageAssembly;
import cn.com.jit.pnxclient.pojo.CertResponse;
import cn.com.jit.pnxclient.util.CommonUtil;
import com.jianq.net.JQBasicNetwork;
import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class CertManager extends BaseManager {
    private boolean g_boolean;

    protected CertManager() {
    }

    /* access modifiers changed from: protected */
    public List<KeyEntry> getCertList() throws PNXClientException {
        reset();
        if (!CommonUtil.isFileExist(PNXConfigConstant.KEYSTOREFILEPATH())) {
            return null;
        }
        KeyStoreManager ksm = new KeyStoreManager();
        try {
            ksm.setStoreType(PNXConfigConstant.STORETYPE_DEFAULT);
            ksm.UserPrivateKeyPassWord();
            List<KeyEntry> g_KeyEntryList = ksm.getKeyEntryList(PNXConfigConstant.KEYSTOREFILEPATH(), PNXConfigConstant.KEYSTOREPASSWORD);
            Log.i("OUTPARAM", g_KeyEntryList + " ");
            return g_KeyEntryList;
        } catch (IOException e) {
            setErrorCode(MessageCode.A00001);
            throw new PNXClientException(MessageCode.A00001, e);
        } catch (Exception e2) {
            setErrorCode(MessageCode.A00002);
            throw new PNXClientException(MessageCode.A00002, e2);
        }
    }

    private String genP10(KeyStoreManager ksm, String credential) throws Exception {
        boolean istempTF;
        String p10;
        try {
            if (!CommonUtil.isFileExist(PNXConfigConstant.KEYSTOREFILEPATH()) || ksm.getKeyEntryList(PNXConfigConstant.KEYSTOREFILEPATH(), PNXConfigConstant.KEYSTOREPASSWORD) == null) {
                istempTF = false;
            } else {
                istempTF = true;
            }
        } catch (EOFException e) {
            istempTF = false;
            new File(PNXConfigConstant.KEYSTOREFILEPATH()).delete();
        } catch (IOException e2) {
            istempTF = false;
            new File(PNXConfigConstant.KEYSTOREFILEPATH()).delete();
        } catch (Exception e3) {
            istempTF = false;
        }
        String p10Subject = "CN=" + credential + ",C=CN";
        if (istempTF) {
            p10 = ksm.genP10Request4UpdateWithSoftLib(PNXConfigConstant.KEYSTOREFILEPATH(), PNXConfigConstant.KEYSTOREPASSWORD, p10Subject, Mechanism.RSA, 1024);
        } else {
            p10 = ksm.genP10RequestWithSoftLib(PNXConfigConstant.KEYSTOREFILEPATH(), PNXConfigConstant.KEYSTOREPASSWORD, p10Subject, Mechanism.RSA, 1024);
        }
        if (p10 == null || p10.length() == 0) {
            this.g_boolean = false;
        } else {
            this.g_boolean = true;
        }
        return p10;
    }

    private String genP10Alias(KeyStoreManager ksm, String p10) throws Exception {
        JCrypto jcrypto = JCrypto.getInstance();
        jcrypto.initialize(JCrypto.JSOFT_LIB, null);
        PKCS10 pKCS10Parser = new PKCS10(jcrypto.openSession(JCrypto.JSOFT_LIB));
        pKCS10Parser.load(p10.getBytes());
        String p10alias = ksm.getAlias(pKCS10Parser.getPubKey());
        if (p10alias == null || p10alias.length() == 0) {
            this.g_boolean = false;
        } else {
            this.g_boolean = true;
        }
        return p10alias;
    }

    /* access modifiers changed from: protected */
    public boolean genCert(String credential, String pwd, String gatewayIp, int gatewayPort) throws PNXClientException {
        reset();
        this.g_boolean = false;
        if (credential == null || credential.length() == 0) {
            setErrorCode(MessageCode.A00102);
            throw new PNXClientException(MessageCode.A00102);
        }
        Log.i("cert Credential", credential);
        String p10alias = null;
        KeyStoreManager ksm = new KeyStoreManager();
        try {
            ksm.setStoreType(PNXConfigConstant.STORETYPE_DEFAULT);
            ksm.UserPrivateKeyPassWord();
            ksm.setPrivateKeyPassWord(pwd);
            String p10 = genP10(ksm, credential);
            if (!this.g_boolean) {
                setErrorCode(MessageCode.A00104);
                if (this.g_boolean || 0 == 0) {
                    return false;
                }
                try {
                    ksm.delAlias(PNXConfigConstant.KEYSTOREFILEPATH(), PNXConfigConstant.KEYSTOREPASSWORD, null);
                    return false;
                } catch (Exception e) {
                    Log.e("delete cert exception", e.toString(), e);
                    return false;
                }
            } else {
                p10alias = genP10Alias(ksm, p10);
                if (!this.g_boolean) {
                    setErrorCode(MessageCode.A00104);
                    if (this.g_boolean || p10alias == null) {
                        return false;
                    }
                    try {
                        ksm.delAlias(PNXConfigConstant.KEYSTOREFILEPATH(), PNXConfigConstant.KEYSTOREPASSWORD, p10alias);
                        return false;
                    } catch (Exception e2) {
                        Log.e("delete cert exception", e2.toString(), e2);
                        return false;
                    }
                } else {
                    byte[] req = MessageAssembly.creatCARequest(gatewayIp, credential, p10, null);
                    if (req == null || req.length == 0) {
                        this.g_boolean = false;
                        setErrorCode(MessageCode.A00104);
                        boolean z = this.g_boolean;
                        if (this.g_boolean || p10alias == null) {
                            return z;
                        }
                        try {
                            ksm.delAlias(PNXConfigConstant.KEYSTOREFILEPATH(), PNXConfigConstant.KEYSTOREPASSWORD, p10alias);
                            return z;
                        } catch (Exception e3) {
                            Log.e("delete cert exception", e3.toString(), e3);
                            return z;
                        }
                    } else {
                        Log.i("MessageAssembly.creatCARequest OUTPARAM ", new String(req, JQBasicNetwork.UTF_8));
                        byte[] retxml = Gwtk.connectGWServer(gatewayIp, gatewayPort, req, 1, null);
                        if (retxml == null || retxml.length == 0) {
                            this.g_boolean = false;
                            setErrorCode(MessageCode.A00103);
                            boolean z2 = this.g_boolean;
                            if (this.g_boolean || p10alias == null) {
                                return z2;
                            }
                            try {
                                ksm.delAlias(PNXConfigConstant.KEYSTOREFILEPATH(), PNXConfigConstant.KEYSTOREPASSWORD, p10alias);
                                return z2;
                            } catch (Exception e4) {
                                Log.e("delete cert exception", e4.toString(), e4);
                                return z2;
                            }
                        } else {
                            Log.i("Gwtk.connectCAServer INPARAM", " String gatewayIp=" + gatewayIp + ",String gatewayPort=" + gatewayPort + "  OUTPARAM=" + new String(retxml, JQBasicNetwork.UTF_8));
                            CertResponse certResponse = Gwtk.parserCertResponse(retxml);
                            if (!certResponse.getErrorcode().equals("0")) {
                                this.g_boolean = false;
                                setErrorCode(MessageCode.A00103);
                                throw new PNXClientException(MessageCode.A00103, "request cert fail：" + certResponse.getErrorcode() + "[" + certResponse.getErrormsg() + "]");
                            }
                            String p7b = certResponse.getP7b();
                            if (p7b == null || p7b.length() == 0) {
                                this.g_boolean = false;
                                setErrorCode(MessageCode.A00105);
                                boolean z3 = this.g_boolean;
                                if (this.g_boolean || p10alias == null) {
                                    return z3;
                                }
                                try {
                                    ksm.delAlias(PNXConfigConstant.KEYSTOREFILEPATH(), PNXConfigConstant.KEYSTOREPASSWORD, p10alias);
                                    return z3;
                                } catch (Exception e5) {
                                    Log.e("delete cert exception", e5.toString(), e5);
                                    return z3;
                                }
                            } else {
                                Log.i("Gwtk.getp7b OUTPARAM", p7b);
                                X509Cert[] certs = new P7B().parseP7b(p7b.getBytes());
                                if (certs.length < 2) {
                                    Log.w("genCert", "Server did not return the root certificate ");
                                }
                                for (int i = 0; i < certs.length; i++) {
                                    if (p10alias.equals(ksm.getAlias(certs[i].getPublicKey()))) {
                                        ksm.setKeyCertWithSoftLib(PNXConfigConstant.KEYSTOREFILEPATH(), PNXConfigConstant.KEYSTOREPASSWORD, certs[i]);
                                    } else {
                                        ksm.setTrustCert(PNXConfigConstant.KEYSTOREFILEPATH(), PNXConfigConstant.KEYSTOREPASSWORD, certs[i]);
                                    }
                                }
                                this.g_boolean = true;
                                if (!this.g_boolean && p10alias != null) {
                                    try {
                                        ksm.delAlias(PNXConfigConstant.KEYSTOREFILEPATH(), PNXConfigConstant.KEYSTOREPASSWORD, p10alias);
                                    } catch (Exception e6) {
                                        Log.e("delete cert exception", e6.toString(), e6);
                                    }
                                }
                                return this.g_boolean;
                            }
                        }
                    }
                }
            }
        } catch (Exception e7) {
            Log.e("exception ignored", e7.toString(), e7);
            this.g_boolean = false;
            setErrorCode(MessageCode.A00103);
            throw new PNXClientException(MessageCode.A00103, e7);
        } catch (Throwable th) {
            if (!this.g_boolean && p10alias != null) {
                try {
                    ksm.delAlias(PNXConfigConstant.KEYSTOREFILEPATH(), PNXConfigConstant.KEYSTOREPASSWORD, p10alias);
                } catch (Exception e8) {
                    Log.e("delete cert exception", e8.toString(), e8);
                }
            }
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public boolean deleteCert(String alias) throws PNXClientException {
        reset();
        KeyStoreManager ksm = new KeyStoreManager();
        try {
            ksm.setStoreType(PNXConfigConstant.STORETYPE_DEFAULT);
            ksm.UserPrivateKeyPassWord();
            return ksm.delAlias(PNXConfigConstant.KEYSTOREFILEPATH(), PNXConfigConstant.KEYSTOREPASSWORD, alias);
        } catch (Exception e) {
            Log.e("deleteCert exception", e.toString(), e);
            setErrorCode(MessageCode.A00301);
            throw new PNXClientException(MessageCode.A00301, e);
        }
    }

    /* access modifiers changed from: protected */
    public boolean changeCertPwd(String ailas, String oldPwd, String newPwd) throws PNXClientException {
        reset();
        KeyStoreManager ksm = new KeyStoreManager();
        try {
            ksm.setStoreType(PNXConfigConstant.STORETYPE_DEFAULT);
            ksm.UserPrivateKeyPassWord();
            ksm.setPrivateKeyPassWord(oldPwd);
            JKey prikey = ksm.getJKey(PNXConfigConstant.KEYSTOREFILEPATH(), PNXConfigConstant.KEYSTOREPASSWORD, ailas);
            X509Cert cert = ksm.getCertEntry(PNXConfigConstant.KEYSTOREFILEPATH(), PNXConfigConstant.KEYSTOREPASSWORD, ailas);
            ksm.setPrivateKeyPassWord(newPwd);
            ksm.addKeyCertWithPfx(PNXConfigConstant.KEYSTOREFILEPATH(), PNXConfigConstant.KEYSTOREPASSWORD, prikey, new X509Cert[]{cert});
            return true;
        } catch (PKIException e) {
            setErrorCode(MessageCode.A00401);
            Log.e("changeCertPwd exception[A00401]", e.toString(), e);
            throw new PNXClientException(MessageCode.A00401, e);
        } catch (Exception e2) {
            setErrorCode(MessageCode.A00402);
            Log.e("changeCertPwd exception[A00402]", e2.toString(), e2);
            throw new PNXClientException(MessageCode.A00402, e2);
        }
    }
}
