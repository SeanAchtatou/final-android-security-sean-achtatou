package cn.com.jit.pnxclient;

import com.jianq.misc.StringEx;

public class BaseManager {
    private String errorCode = StringEx.Empty;

    /* access modifiers changed from: protected */
    public String getErrorCode() {
        return this.errorCode;
    }

    /* access modifiers changed from: protected */
    public void setErrorCode(String errorCode2) {
        this.errorCode = errorCode2;
    }

    /* access modifiers changed from: protected */
    public void reset() {
        this.errorCode = StringEx.Empty;
    }
}
