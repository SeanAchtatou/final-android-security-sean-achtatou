package cn.com.jit.pnxclient.constant;

public class PNXConfigConstant {
    public static final String AUTHMODE_DEFAULT = "2";
    public static String CACHEDIR = null;
    public static final String CA_SERVICE_TYPE = "CERTMAKEBYIDENTIFIER";
    public static final String[] FILTERFILENAMEEXT = {".p12", ".pfx"};
    public static final String KEYSTOREPASSWORD = "jitbks";
    public static final String LOGFILENAME = "PNXClient.log";
    public static final int PNXSERVERPORT_DEFAULT = 443;
    public static final String RESP_SPLIT_2 = "\r\n";
    public static final String RESP_SPLIT_3 = ":";
    public static final String RESULT_SUCESS_CODE = "0";
    public static final String ROOTDIR = "/sdcard";
    public static final String STORETYPE_DEFAULT = "BKS";

    public static String LOGFILEPATH() {
        return CACHEDIR + "/log/";
    }

    public static String KEYSTOREFILEPATH() {
        return CACHEDIR + "/client.bks";
    }
}
