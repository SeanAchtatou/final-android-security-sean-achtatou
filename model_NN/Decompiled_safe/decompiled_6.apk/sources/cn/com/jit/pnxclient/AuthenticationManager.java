package cn.com.jit.pnxclient;

import android.util.Log;
import cn.com.jit.android.ida.util.pki.keystore.KeyStoreManager;
import cn.com.jit.ida.util.pki.cert.X509Cert;
import cn.com.jit.ida.util.pki.cipher.JCrypto;
import cn.com.jit.ida.util.pki.cipher.JKey;
import cn.com.jit.ida.util.pki.cipher.Mechanism;
import cn.com.jit.ida.util.pki.cipher.Session;
import cn.com.jit.ida.util.pki.cipher.param.P7Param;
import cn.com.jit.ida.util.pki.encoders.Base64;
import cn.com.jit.ida.util.pki.pkcs.PKCS7;
import cn.com.jit.pnxclient.constant.ApplicationConstant;
import cn.com.jit.pnxclient.constant.MessageCode;
import cn.com.jit.pnxclient.constant.PNXConfigConstant;
import cn.com.jit.pnxclient.exception.PNXClientException;
import cn.com.jit.pnxclient.net.Gwtk;
import cn.com.jit.pnxclient.net.MessageAssembly;
import cn.com.jit.pnxclient.pojo.AppInfo;
import cn.com.jit.pnxclient.pojo.AskInfo;
import cn.com.jit.pnxclient.pojo.AuthRequest;
import cn.com.jit.pnxclient.pojo.AuthResponse;
import cn.com.jit.pnxclient.pojo.CertPlugin;
import cn.com.jit.pnxclient.pojo.UserInfo;
import com.jianq.net.JQBasicNetwork;
import java.io.IOException;
import java.net.SocketTimeoutException;

public class AuthenticationManager extends BaseManager {
    private AuthResponse authResponse = null;

    protected AuthenticationManager() {
    }

    /* access modifiers changed from: protected */
    public byte[] getCertSignData(String ailas, String original, String certPWD) throws PNXClientException {
        reset();
        KeyStoreManager ksm = new KeyStoreManager();
        try {
            ksm.setStoreType(PNXConfigConstant.STORETYPE_DEFAULT);
            ksm.UserPrivateKeyPassWord();
            ksm.setPrivateKeyPassWord(certPWD);
            JKey prikey = ksm.getJKey(PNXConfigConstant.KEYSTOREFILEPATH(), PNXConfigConstant.KEYSTOREPASSWORD, ailas);
            X509Cert cert = ksm.getCertEntry(PNXConfigConstant.KEYSTOREFILEPATH(), PNXConfigConstant.KEYSTOREPASSWORD, ailas);
            JCrypto jcrypto = JCrypto.getInstance();
            jcrypto.initialize(JCrypto.JSOFT_LIB, null);
            Session session = jcrypto.openSession(JCrypto.JSOFT_LIB);
            Mechanism cryptoM = new Mechanism("SHA1withRSAEncryption");
            PKCS7 p7 = new PKCS7(session);
            P7Param param = new P7Param();
            param.SetSignParam(prikey, cryptoM, new X509Cert[]{cert}, null, null, null);
            byte[] signedData_ = p7.genP7_Sign(original.getBytes(), new P7Param[]{param});
            Log.i("P7 signedData OUTPARAM", new String(signedData_, JQBasicNetwork.UTF_8));
            return signedData_;
        } catch (IOException e) {
            Log.e("CertSign exception", e.toString(), e);
            setErrorCode(MessageCode.A00001);
            throw new PNXClientException(MessageCode.A00001, e);
        } catch (Exception e2) {
            Log.e("CertSign exception", e2.toString(), e2);
            setErrorCode(MessageCode.A00202);
            throw new PNXClientException(MessageCode.A00202, e2);
        }
    }

    /* access modifiers changed from: protected */
    public boolean authLogin(String gateway_ip, int gateway_port, String cert_ailas, String cert_pwd) throws PNXClientException {
        reset();
        try {
            String original = fetchOriginal(askGWServer(gateway_ip, gateway_port));
            String[] certSingData = getCertSingData(cert_ailas, original, cert_pwd);
            AuthRequest authreq = new AuthRequest();
            authreq.setAuthmode("2");
            authreq.setALGID(Mechanism.SHA1);
            authreq.setSIGNALGID(Mechanism.RSA);
            authreq.setCERTB64(certSingData[0]);
            authreq.setSINGDATA(certSingData[1]);
            authreq.setORIGINAL(original);
            authreq.setClientip(gateway_ip);
            UserInfo anthInfo = authGWServer(gateway_ip, gateway_port, authreq);
            if (anthInfo == null) {
                return true;
            }
            bulidAuthLoginResult(anthInfo);
            return true;
        } catch (SocketTimeoutException e) {
            Log.e("authLogin exception", e.toString(), e);
            setErrorCode(MessageCode.A00502);
            throw new PNXClientException(MessageCode.A00502, e);
        } catch (PNXClientException e2) {
            Log.e("authLogin exception", e2.toString(), e2);
            setErrorCode(e2.getErrorCode());
            throw new PNXClientException(e2);
        } catch (Exception e3) {
            Log.e("authLogin exception", e3.toString(), e3);
            setErrorCode(MessageCode.A00501);
            throw new PNXClientException(MessageCode.A00501, e3);
        }
    }

    /* access modifiers changed from: protected */
    public AuthResponse getAuthLoginResult() {
        return this.authResponse;
    }

    private AskInfo askGWServer(String gw_ip, int gw_port) throws Exception {
        byte[] askreq = MessageAssembly.createAskRequest(null);
        Log.i("MessageAssembly.createAskRequest OUTPARAM ", new String(askreq, JQBasicNetwork.UTF_8));
        Log.i("Gwtk.connectGWServer INPARAM: ", "gw_ip=" + gw_ip + ", gw_port=" + gw_port);
        byte[] ret = Gwtk.connectGWServer(gw_ip, gw_port, askreq, 1, null);
        Log.i("AskGWServer OUTPARAM: ", new String(ret, JQBasicNetwork.UTF_8));
        AskInfo ask = Gwtk.getAskInfo(ret);
        if (ask == null) {
            throw new PNXClientException(MessageCode.A00501);
        } else if ("0".equals(ask.getErrorcode())) {
            return ask;
        } else {
            throw new PNXClientException(MessageCode.A00501, "[" + ask.getErrorcode() + "] " + ask.getErrormsg());
        }
    }

    private UserInfo authGWServer(String gw_ip, int gw_port, AuthRequest authreq) throws Exception {
        byte[] request = MessageAssembly.createAuthRequest(authreq);
        Log.i("MessageAssembly.createAuthRequest OUTPARAM ", new String(request, JQBasicNetwork.UTF_8));
        byte[] retauth = Gwtk.connectGWServer(gw_ip, gw_port, request, 1, null);
        Log.i("AuthGWServer OUTPARAM: ", new String(retauth, JQBasicNetwork.UTF_8));
        UserInfo userinfo = Gwtk.getUserInfo(retauth);
        if (userinfo == null) {
            throw new PNXClientException(MessageCode.A00501);
        } else if ("0".equals(userinfo.getErrorcode())) {
            return userinfo;
        } else {
            throw new PNXClientException(MessageCode.A00501, "[" + userinfo.getErrorcode() + "] " + userinfo.getErrormsg());
        }
    }

    private String[] getCertSingData(String ailas, String original, String pwd) throws Exception {
        try {
            KeyStoreManager ksm = new KeyStoreManager();
            ksm.setStoreType(PNXConfigConstant.STORETYPE_DEFAULT);
            ksm.UserPrivateKeyPassWord();
            ksm.setPrivateKeyPassWord(pwd);
            JKey prikey = ksm.getJKey(PNXConfigConstant.KEYSTOREFILEPATH(), PNXConfigConstant.KEYSTOREPASSWORD, ailas);
            X509Cert cert = ksm.getCertEntry(PNXConfigConstant.KEYSTOREFILEPATH(), PNXConfigConstant.KEYSTOREPASSWORD, ailas);
            byte[] srcByte = Base64.decode(original);
            JCrypto jcrypto = JCrypto.getInstance();
            jcrypto.initialize(JCrypto.JSOFT_LIB, null);
            return new String[]{new String(Base64.encode(cert.getEncoded())), new String(Base64.encode(jcrypto.openSession(JCrypto.JSOFT_LIB).sign(new Mechanism("SHA1withRSAEncryption"), prikey, srcByte)))};
        } catch (IOException e) {
            throw new PNXClientException(MessageCode.A00001, e);
        } catch (Exception e2) {
            throw new PNXClientException(MessageCode.A00202, e2);
        }
    }

    private String fetchOriginal(AskInfo askInfo) throws PNXClientException {
        CertPlugin plugin = askInfo.getCertplugin();
        String original = null;
        if (plugin != null) {
            original = plugin.getOriginal();
        }
        if (original != null && original.length() != 0) {
            return original;
        }
        throw new PNXClientException(MessageCode.A00501);
    }

    private void bulidAuthLoginResult(UserInfo anthInfo) {
        this.authResponse = new AuthResponse();
        AppInfo[] appinfos = anthInfo.getAppinfos();
        for (AppInfo app : appinfos) {
            this.authResponse.put(app.getAlias(), ApplicationConstant.APP_ALIAS, app.getAlias());
            this.authResponse.put(app.getAlias(), ApplicationConstant.APP_NAME, app.getAppname());
            this.authResponse.put(app.getAlias(), ApplicationConstant.APP_FLAG, app.getAppflag());
            this.authResponse.put(app.getAlias(), ApplicationConstant.APP_ACCESS_TYPE, app.getAccess_type());
            this.authResponse.put(app.getAlias(), ApplicationConstant.APP_URL, app.getUrl());
            String attributes_str = app.getAttributes();
            if (attributes_str != null) {
                for (String attribute : attributes_str.split(PNXConfigConstant.RESP_SPLIT_2)) {
                    String[] key_value = attribute.split(PNXConfigConstant.RESP_SPLIT_3);
                    this.authResponse.put(app.getAlias(), key_value[0], key_value[1]);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void reset() {
        super.reset();
        this.authResponse = null;
    }
}
