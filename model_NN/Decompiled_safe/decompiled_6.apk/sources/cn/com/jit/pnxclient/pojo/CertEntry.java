package cn.com.jit.pnxclient.pojo;

import java.math.BigInteger;
import java.util.Date;

public class CertEntry {
    private String ailas;
    private String issuer;
    private Date notAfter;
    private Date notBefore;
    private BigInteger serialNumber;
    private String subject;

    public String getAilas() {
        return this.ailas;
    }

    public void setAilas(String ailas2) {
        this.ailas = ailas2;
    }

    public String getSubject() {
        return this.subject;
    }

    public void setSubject(String subject2) {
        this.subject = subject2;
    }

    public BigInteger getSerialNumber() {
        return this.serialNumber;
    }

    public void setSerialNumber(BigInteger serialNumber2) {
        this.serialNumber = serialNumber2;
    }

    public String getIssuer() {
        return this.issuer;
    }

    public void setIssuer(String issuer2) {
        this.issuer = issuer2;
    }

    public Date getNotBefore() {
        return this.notBefore;
    }

    public void setNotBefore(Date notBefore2) {
        this.notBefore = notBefore2;
    }

    public Date getNotAfter() {
        return this.notAfter;
    }

    public void setNotAfter(Date notAfter2) {
        this.notAfter = notAfter2;
    }
}
