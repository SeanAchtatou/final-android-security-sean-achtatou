package cn.com.jit.pnxclient;

import android.app.Activity;
import android.os.Environment;
import android.util.Log;
import cn.com.jit.pnxclient.constant.PNXConfigConstant;
import java.security.Security;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class PNXClientFactory {
    private static PNXClientFactory clientFactory = null;
    private AuthenticationSupport authenticationSupport = null;
    private CertManagerSupport certManagerSupport = null;

    private PNXClientFactory() {
    }

    public static PNXClientFactory getInstance(Activity activity) {
        if (PNXConfigConstant.CACHEDIR == null) {
            if ("mounted".equals(Environment.getExternalStorageState())) {
                Log.w("certificate store", "external storage available,certificate store will use the SD card");
                PNXConfigConstant.CACHEDIR = activity.getExternalFilesDir(null).getPath();
            } else {
                Log.w("certificate store", "external storage is not available, certificate store will use internal storage");
                PNXConfigConstant.CACHEDIR = activity.getFilesDir().getPath();
            }
        }
        if (Security.getProvider("BC") == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
        if (clientFactory == null) {
            clientFactory = new PNXClientFactory();
        }
        return clientFactory;
    }

    public CertManagerSupport createCertManagerSupport() {
        if (this.certManagerSupport == null) {
            this.certManagerSupport = new CertManagerSupport();
        }
        return this.certManagerSupport;
    }

    public AuthenticationSupport createAuthenticationSupport() {
        if (this.authenticationSupport == null) {
            this.authenticationSupport = new AuthenticationSupport();
        }
        return this.authenticationSupport;
    }
}
