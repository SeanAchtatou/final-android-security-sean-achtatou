package cn.com.jit.pnxclient.pojo;

import java.util.Arrays;

public class UserInfo {
    private AppInfo[] appinfos = null;
    private String authmode = null;
    private String auto_logon = null;
    private String certtype = null;
    private String clientlog = null;
    private String errorcode = null;
    private String errormsg = null;
    private String logon_as_need = null;
    private String serialnumber = null;
    private String token = null;
    private String update_period = null;
    private String username = null;

    public String getErrorcode() {
        return this.errorcode;
    }

    public void setErrorcode(String errorcode2) {
        this.errorcode = errorcode2;
    }

    public String getErrormsg() {
        return this.errormsg;
    }

    public void setErrormsg(String errormsg2) {
        this.errormsg = errormsg2;
    }

    public String getToken() {
        return this.token;
    }

    public void setToken(String token2) {
        this.token = token2;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username2) {
        this.username = username2;
    }

    public String getSerialnumber() {
        return this.serialnumber;
    }

    public void setSerialnumber(String serialnumber2) {
        this.serialnumber = serialnumber2;
    }

    public String getCerttype() {
        return this.certtype;
    }

    public void setCerttype(String certtype2) {
        this.certtype = certtype2;
    }

    public String getAuthmode() {
        return this.authmode;
    }

    public void setAuthmode(String authmode2) {
        this.authmode = authmode2;
    }

    public AppInfo[] getAppinfos() {
        return this.appinfos;
    }

    public void setAppinfos(AppInfo[] appinfos2) {
        this.appinfos = appinfos2;
    }

    public String getLogon_as_need() {
        return this.logon_as_need;
    }

    public void setLogon_as_need(String logonAsNeed) {
        this.logon_as_need = logonAsNeed;
    }

    public String getAuto_logon() {
        return this.auto_logon;
    }

    public void setAuto_logon(String autoLogon) {
        this.auto_logon = autoLogon;
    }

    public String getClientlog() {
        return this.clientlog;
    }

    public void setClientlog(String clientlog2) {
        this.clientlog = clientlog2;
    }

    public String getUpdate_period() {
        return this.update_period;
    }

    public void setUpdate_period(String updatePeriod) {
        this.update_period = updatePeriod;
    }

    public String toString() {
        return "UserInfo [appinfos=" + Arrays.toString(this.appinfos) + ", authmode=" + this.authmode + ", auto_logon=" + this.auto_logon + ", certtype=" + this.certtype + ", clientlog=" + this.clientlog + ", errorcode=" + this.errorcode + ", errormsg=" + this.errormsg + ", logon_as_need=" + this.logon_as_need + ", serialnumber=" + this.serialnumber + ", token=" + this.token + ", update_period=" + this.update_period + ", username=" + this.username + "]";
    }
}
