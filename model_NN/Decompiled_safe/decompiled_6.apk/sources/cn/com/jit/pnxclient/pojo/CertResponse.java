package cn.com.jit.pnxclient.pojo;

public class CertResponse {
    private String doubleEncryptedPrivateKey = null;
    private String doubleEncryptedSessionKey = null;
    private String doubleP7b = null;
    private String errorcode = null;
    private String errormsg = null;
    private String p7b = null;
    private String sessionKeyAlg = null;

    public String getErrorcode() {
        return this.errorcode;
    }

    public void setErrorcode(String errorcode2) {
        this.errorcode = errorcode2;
    }

    public String getErrormsg() {
        return this.errormsg;
    }

    public void setErrormsg(String errormsg2) {
        this.errormsg = errormsg2;
    }

    public String getP7b() {
        return this.p7b;
    }

    public void setP7b(String p7b2) {
        this.p7b = p7b2;
    }

    public String getDoubleP7b() {
        return this.doubleP7b;
    }

    public void setDoubleP7b(String doubleP7b2) {
        this.doubleP7b = doubleP7b2;
    }

    public String getDoubleEncryptedSessionKey() {
        return this.doubleEncryptedSessionKey;
    }

    public void setDoubleEncryptedSessionKey(String doubleEncryptedSessionKey2) {
        this.doubleEncryptedSessionKey = doubleEncryptedSessionKey2;
    }

    public String getSessionKeyAlg() {
        return this.sessionKeyAlg;
    }

    public void setSessionKeyAlg(String sessionKeyAlg2) {
        this.sessionKeyAlg = sessionKeyAlg2;
    }

    public String getDoubleEncryptedPrivateKey() {
        return this.doubleEncryptedPrivateKey;
    }

    public void setDoubleEncryptedPrivateKey(String doubleEncryptedPrivateKey2) {
        this.doubleEncryptedPrivateKey = doubleEncryptedPrivateKey2;
    }

    public String toString() {
        return "CertResponse [errorcode=" + this.errorcode + ", errormsg=" + this.errormsg + ", p7b=" + this.p7b + ", doubleP7b=" + this.doubleP7b + ", doubleEncryptedSessionKey=" + this.doubleEncryptedSessionKey + ", sessionKeyAlg=" + this.sessionKeyAlg + ", doubleEncryptedPrivateKey=" + this.doubleEncryptedPrivateKey + "]";
    }
}
