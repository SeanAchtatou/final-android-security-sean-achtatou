package cn.com.jit.pnxclient.constant;

public enum MessageCodeDec {
    A00001("证书读取错误！"),
    A00002("不能获取证书！"),
    A00101("证书密码为空 "),
    A00102("证书的授权码为空"),
    A00103("申请证书失败！"),
    A00104("产生申请数据失败！"),
    A00105("无法获得制证数据！"),
    A00201("生成签名数据发生错误！"),
    A00202("证书密码错误！"),
    A00301("删除证书失败！"),
    A00401("生成证书数据发生错误！"),
    A00402("修改证书密码失败！"),
    A00501("连接网关失败！"),
    A00502("连接超时！ ");
    
    private String DEC;

    private MessageCodeDec(String dec) {
        this.DEC = dec;
    }

    public static String getDec(String code) {
        return valueOf(code).DEC;
    }
}
