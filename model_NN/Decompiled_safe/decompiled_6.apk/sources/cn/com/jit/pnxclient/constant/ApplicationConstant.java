package cn.com.jit.pnxclient.constant;

public class ApplicationConstant {
    public static final String APP_ACCESS_TYPE = "access_type";
    public static String APP_ALIAS = "alias";
    public static final String APP_FLAG = "appflag";
    public static final String APP_NAME = "appname";
    public static final String APP_URL = "url";
}
