package cn.com.jit.pnxclient.pojo;

public class AuthRequest {
    private String ALGID = null;
    private String CERTB64 = null;
    private String ORIGINAL = null;
    private String PASSWORD = null;
    private String SIGNALGID = null;
    private String SINGDATA = null;
    private String USERNAME = null;
    private String authmode = null;
    private String clientip = null;
    private String hardwareext = null;
    private String hardwarefinger = null;

    public String getAuthmode() {
        return this.authmode;
    }

    public void setAuthmode(String authmode2) {
        this.authmode = authmode2;
    }

    public String getCERTB64() {
        return this.CERTB64;
    }

    public void setCERTB64(String cERTB64) {
        this.CERTB64 = cERTB64;
    }

    public String getORIGINAL() {
        return this.ORIGINAL;
    }

    public void setORIGINAL(String oRIGINAL) {
        this.ORIGINAL = oRIGINAL;
    }

    public String getALGID() {
        return this.ALGID;
    }

    public void setALGID(String aLGID) {
        this.ALGID = aLGID;
    }

    public String getSIGNALGID() {
        return this.SIGNALGID;
    }

    public void setSIGNALGID(String sIGNALGID) {
        this.SIGNALGID = sIGNALGID;
    }

    public String getSINGDATA() {
        return this.SINGDATA;
    }

    public void setSINGDATA(String sINGDATA) {
        this.SINGDATA = sINGDATA;
    }

    public String getUSERNAME() {
        return this.USERNAME;
    }

    public void setUSERNAME(String uSERNAME) {
        this.USERNAME = uSERNAME;
    }

    public String getPASSWORD() {
        return this.PASSWORD;
    }

    public void setPASSWORD(String pASSWORD) {
        this.PASSWORD = pASSWORD;
    }

    public String getHardwarefinger() {
        return this.hardwarefinger;
    }

    public void setHardwarefinger(String hardwarefinger2) {
        this.hardwarefinger = hardwarefinger2;
    }

    public String getHardwareext() {
        return this.hardwareext;
    }

    public void setHardwareext(String hardwareext2) {
        this.hardwareext = hardwareext2;
    }

    public String getClientip() {
        return this.clientip;
    }

    public void setClientip(String clientip2) {
        this.clientip = clientip2;
    }

    public String toString() {
        return "AuthRequest [ALGID=" + this.ALGID + ", CERTB64=" + this.CERTB64 + ", ORIGINAL=" + this.ORIGINAL + ", PASSWORD=" + this.PASSWORD + ", SIGNALGID=" + this.SIGNALGID + ", SINGDATA=" + this.SINGDATA + ", USERNAME=" + this.USERNAME + ", authmode=" + this.authmode + ", clientip=" + this.clientip + ", hardwareext=" + this.hardwareext + ", hardwarefinger=" + this.hardwarefinger + "]";
    }
}
