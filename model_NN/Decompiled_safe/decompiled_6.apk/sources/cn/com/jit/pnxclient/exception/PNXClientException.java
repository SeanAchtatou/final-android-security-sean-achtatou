package cn.com.jit.pnxclient.exception;

import cn.com.jit.pnxclient.constant.MessageCodeDec;

public class PNXClientException extends Exception {
    private static final long serialVersionUID = -2297398861298075800L;
    private String errorCode;
    private String errorDesc;

    public PNXClientException(String errorCode2) {
        super("[" + errorCode2 + "] " + MessageCodeDec.getDec(errorCode2));
        this.errorCode = errorCode2;
        this.errorDesc = MessageCodeDec.getDec(errorCode2);
    }

    public PNXClientException(PNXClientException e) {
        super("[" + e.getErrorCode() + "] " + e.getErrorDesc());
        this.errorCode = e.getErrorCode();
        this.errorDesc = e.getErrorDesc();
    }

    public PNXClientException(String errorCode2, Throwable t) {
        super("[" + errorCode2 + "] " + MessageCodeDec.getDec(errorCode2), t);
        this.errorCode = errorCode2;
        this.errorDesc = MessageCodeDec.getDec(errorCode2);
    }

    public PNXClientException(String errorCode2, String errorDesc2) {
        super("[" + errorCode2 + "] " + errorDesc2);
        this.errorCode = errorCode2;
        this.errorDesc = errorDesc2;
    }

    public PNXClientException(String errorCode2, String errorDesc2, Throwable t) {
        super("[" + errorCode2 + "] " + errorDesc2, t);
        this.errorCode = errorCode2;
        this.errorDesc = errorDesc2;
    }

    public String getErrorCode() {
        return this.errorCode;
    }

    public void setErrorCode(String errorCode2) {
        this.errorCode = errorCode2;
    }

    public String getErrorDesc() {
        return this.errorDesc;
    }

    public void setErrorDesc(String errorDesc2) {
        this.errorDesc = errorDesc2;
    }
}
