package cn.com.jit.pnxclient.util;

public class PatternUtil {
    public static boolean isInteger(String value) {
        try {
            Integer.parseInt(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean isIpAddress(String value) {
        int start = 0;
        int end = value.indexOf(46);
        int numBlocks = 0;
        while (start < value.length()) {
            if (end == -1) {
                end = value.length();
            }
            try {
                int block = Integer.parseInt(value.substring(start, end));
                if (block > 255 || block < 0) {
                    return false;
                }
                numBlocks++;
                start = end + 1;
                end = value.indexOf(46, start);
            } catch (NumberFormatException e) {
                return false;
            }
        }
        if (numBlocks == 4) {
            return true;
        }
        return false;
    }
}
