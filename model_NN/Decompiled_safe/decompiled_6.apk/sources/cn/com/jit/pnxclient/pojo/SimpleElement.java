package cn.com.jit.pnxclient.pojo;

import java.util.HashMap;
import java.util.LinkedList;

public class SimpleElement {
    private HashMap attributes = new HashMap();
    private LinkedList childElements = new LinkedList();
    private String tagName;
    private String text;

    public SimpleElement(String tagName2) {
        this.tagName = tagName2;
    }

    public String getTagName() {
        return this.tagName;
    }

    public void setTagName(String tagName2) {
        this.tagName = tagName2;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text2) {
        this.text = text2;
    }

    public String getAttribute(String name) {
        return (String) this.attributes.get(name);
    }

    public void setAttribute(String name, String value) {
        this.attributes.put(name, value);
    }

    public void addChildElement(SimpleElement element) {
        this.childElements.add(element);
    }

    public Object[] getChildElements() {
        return this.childElements.toArray();
    }
}
