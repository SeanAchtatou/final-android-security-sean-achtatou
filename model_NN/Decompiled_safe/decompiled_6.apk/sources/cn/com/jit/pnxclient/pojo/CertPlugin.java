package cn.com.jit.pnxclient.pojo;

import java.util.Arrays;

public class CertPlugin {
    private String authmode = null;
    private String authname = null;
    private String[] dnList = null;
    private String original = null;
    private String typeid = null;

    public String getAuthname() {
        return this.authname;
    }

    public void setAuthname(String authname2) {
        this.authname = authname2;
    }

    public String getAuthmode() {
        return this.authmode;
    }

    public void setAuthmode(String authmode2) {
        this.authmode = authmode2;
    }

    public String getTypeid() {
        return this.typeid;
    }

    public void setTypeid(String typeid2) {
        this.typeid = typeid2;
    }

    public String[] getDnList() {
        return this.dnList;
    }

    public void setDnList(String[] dnList2) {
        this.dnList = dnList2;
    }

    public String getOriginal() {
        return this.original;
    }

    public void setOriginal(String original2) {
        this.original = original2;
    }

    public String toString() {
        return "CertPlugin [authmode=" + this.authmode + ", authname=" + this.authname + ", dnList=" + Arrays.toString(this.dnList) + ", original=" + this.original + ", typeid=" + this.typeid + "]";
    }
}
