package cn.com.jit.pnxclient.util;

import cn.com.jit.pnxclient.pojo.SimpleElement;
import com.jianq.misc.StringEx;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Stack;

public class SimpleDOMParser {
    private static final int[] cdata_end = {93, 93, 62};
    private static final int[] cdata_start = {60, 33, 91, 67, 68, 65, 84, 65, 91};
    private SimpleElement currentElement = null;
    private Stack elements = new Stack();
    private Reader reader;

    public SimpleElement parse(Reader reader2) throws IOException {
        String tagName;
        String currentTag;
        int index;
        String attributeValue;
        this.reader = new BufferedReader(reader2);
        skipPrologs();
        while (true) {
            String currentTag2 = readTag().trim();
            if (currentTag2.startsWith("</")) {
                String tagName2 = currentTag2.substring(2, currentTag2.length() - 1);
                if (this.currentElement == null) {
                    throw new IOException("Got close tag '" + tagName2 + "' without open tag.");
                } else if (!tagName2.equals(this.currentElement.getTagName())) {
                    throw new IOException("Expected close tag for '" + this.currentElement.getTagName() + "' but got '" + tagName2 + "'.");
                } else if (this.elements.empty()) {
                    return this.currentElement;
                } else {
                    this.currentElement = (SimpleElement) this.elements.pop();
                }
            } else {
                int index2 = currentTag2.indexOf(" ");
                if (index2 >= 0) {
                    tagName = currentTag2.substring(1, index2);
                    currentTag = currentTag2.substring(index2 + 1);
                } else if (currentTag2.endsWith("/>")) {
                    tagName = currentTag2.substring(1, currentTag2.length() - 2);
                    currentTag = "/>";
                } else {
                    tagName = currentTag2.substring(1, currentTag2.length() - 1);
                    currentTag = StringEx.Empty;
                }
                SimpleElement element = new SimpleElement(tagName.trim());
                boolean isTagClosed = false;
                while (true) {
                    if (currentTag.length() <= 0) {
                        break;
                    }
                    String currentTag3 = currentTag.trim();
                    if (!currentTag3.equals("/>")) {
                        if (currentTag3.equals(">")) {
                            break;
                        }
                        int index3 = currentTag3.indexOf("=");
                        if (index3 < 0) {
                            throw new IOException("Invalid attribute for tag '" + tagName + "'.");
                        }
                        String attributeName = currentTag3.substring(0, index3);
                        String currentTag4 = currentTag3.substring(index3 + 1);
                        boolean isQuoted = true;
                        if (currentTag4.startsWith("\"")) {
                            index = currentTag4.indexOf(34, 1);
                        } else if (currentTag4.startsWith("'")) {
                            index = currentTag4.indexOf(39, 1);
                        } else {
                            isQuoted = false;
                            index = currentTag4.indexOf(32);
                            if (index < 0 && (index = currentTag4.indexOf(62)) < 0) {
                                index = currentTag4.indexOf(47);
                            }
                        }
                        if (index < 0) {
                            throw new IOException("Invalid attribute for tag '" + tagName + "'.");
                        }
                        if (isQuoted) {
                            attributeValue = currentTag4.substring(1, index);
                        } else {
                            attributeValue = currentTag4.substring(0, index);
                        }
                        element.setAttribute(attributeName.trim(), attributeValue.trim());
                        currentTag = currentTag4.substring(index + 1);
                    } else {
                        isTagClosed = true;
                        break;
                    }
                }
                if (!isTagClosed) {
                    element.setText(readText().trim());
                }
                if (this.currentElement != null) {
                    this.currentElement.addChildElement(element);
                }
                if (!isTagClosed) {
                    if (this.currentElement != null) {
                        this.elements.push(this.currentElement);
                    }
                    this.currentElement = element;
                } else if (this.currentElement == null) {
                    return element;
                }
            }
        }
    }

    private int peek() throws IOException {
        this.reader.mark(1);
        int result = this.reader.read();
        this.reader.reset();
        return result;
    }

    private void peek(int[] buffer) throws IOException {
        this.reader.mark(buffer.length);
        for (int i = 0; i < buffer.length; i++) {
            buffer[i] = this.reader.read();
        }
        this.reader.reset();
    }

    private void skipWhitespace() throws IOException {
        while (peek() != 60) {
            this.reader.read();
        }
    }

    private void skipProlog() throws IOException {
        this.reader.skip(2);
        while (true) {
            int next = peek();
            if (next == 62) {
                this.reader.read();
                return;
            } else if (next == 60) {
                skipProlog();
            } else {
                this.reader.read();
            }
        }
    }

    private void skipPrologs() throws IOException {
        while (true) {
            skipWhitespace();
            int[] next = new int[2];
            peek(next);
            if (next[0] != 60) {
                throw new IOException("Expected '<' but got '" + ((char) next[0]) + "'.");
            } else if (next[1] == 63 || next[1] == 33) {
                skipProlog();
            } else {
                return;
            }
        }
    }

    private String readTag() throws IOException {
        skipWhitespace();
        StringBuffer sb = new StringBuffer();
        int next = peek();
        if (next != 60) {
            throw new IOException("Expected ＜ but got " + ((char) next));
        }
        sb.append((char) this.reader.read());
        while (peek() != 62) {
            sb.append((char) this.reader.read());
        }
        sb.append((char) this.reader.read());
        return sb.toString();
    }

    private String readText() throws IOException {
        StringBuffer sb = new StringBuffer();
        int[] next = new int[cdata_start.length];
        peek(next);
        if (compareIntArrays(next, cdata_start)) {
            this.reader.skip((long) next.length);
            int[] buffer = new int[cdata_end.length];
            while (true) {
                peek(buffer);
                if (compareIntArrays(buffer, cdata_end)) {
                    break;
                }
                sb.append((char) this.reader.read());
            }
            this.reader.skip((long) buffer.length);
        } else {
            while (peek() != 60) {
                sb.append((char) this.reader.read());
            }
        }
        return sb.toString();
    }

    private boolean compareIntArrays(int[] a1, int[] a2) {
        if (a1.length != a2.length) {
            return false;
        }
        for (int i = 0; i < a1.length; i++) {
            if (a1[i] != a2[i]) {
                return false;
            }
        }
        return true;
    }

    public void destroy() {
        this.elements = null;
        this.reader = null;
    }
}
