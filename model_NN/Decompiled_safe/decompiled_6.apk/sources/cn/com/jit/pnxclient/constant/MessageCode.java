package cn.com.jit.pnxclient.constant;

public class MessageCode {
    public static final String A00001 = "A00001";
    public static final String A00002 = "A00002";
    public static final String A00101 = "A00101";
    public static final String A00102 = "A00102";
    public static final String A00103 = "A00103";
    public static final String A00104 = "A00104";
    public static final String A00105 = "A00105";
    public static final String A00201 = "A00201";
    public static final String A00202 = "A00202";
    public static final String A00301 = "A00301";
    public static final String A00401 = "A00401";
    public static final String A00402 = "A00402";
    public static final String A00501 = "A00501";
    public static final String A00502 = "A00502";
}
