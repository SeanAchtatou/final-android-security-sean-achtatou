package cn.com.jit.pnxclient.net;

import cn.com.jit.pnxclient.constant.PNXConfigConstant;
import cn.com.jit.pnxclient.pojo.AuthRequest;
import com.jianq.misc.StringEx;
import com.jianq.net.JQBasicNetwork;

public class MessageAssembly {
    public static String getAttachRequest(String singData, String appId) {
        StringBuffer requestXml = new StringBuffer();
        requestXml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        requestXml.append("<message>");
        requestXml.append("<head><version>1.0</version><serviceType>AuthenService</serviceType></head>");
        requestXml.append("<body>");
        requestXml.append("<appId>" + appId + "</appId>");
        requestXml.append("<authen><authCredential authMode=\"cert\">");
        requestXml.append("<attach>" + singData + "</attach>");
        requestXml.append("</authCredential></authen>");
        requestXml.append("<accessControl>true</accessControl>");
        requestXml.append("<attributes attributeType=\"all\" />");
        requestXml.append("</body>");
        requestXml.append("</message>");
        return requestXml.toString();
    }

    public static byte[] creatCARequest(String ip, String identifier, String p10, String doubleP10) throws Exception {
        if (identifier == null || p10 == null) {
            throw new Exception("in param is null");
        } else if (identifier.trim().equals(StringEx.Empty) || p10.trim().equals(StringEx.Empty)) {
            throw new Exception("in param is empty");
        } else {
            StringBuffer xml = new StringBuffer();
            xml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?><CertMakeByIdentifierRequest><head><reqType>CERTMAKEBYIDENTIFIER</reqType></head><identifier>");
            xml.append(identifier);
            xml.append("</identifier><p10>");
            xml.append(p10);
            xml.append("</p10><doubleP10>");
            if (doubleP10 != null) {
                xml.append(doubleP10);
            }
            xml.append("</doubleP10></CertMakeByIdentifierRequest>");
            byte[] head = getHead(ip, xml.toString().getBytes().length, PNXConfigConstant.CA_SERVICE_TYPE);
            byte[] message = new byte[(xml.toString().getBytes().length + head.length)];
            System.arraycopy(head, 0, message, 0, head.length);
            System.arraycopy(xml.toString().getBytes(), 0, message, head.length, xml.toString().getBytes().length);
            return message;
        }
    }

    private static byte[] getHead(String ip, int length, String type) throws Exception {
        StringBuffer buffer = new StringBuffer();
        buffer.append("POST /" + type + " HTTP/1.1\r\n");
        buffer.append("Host: " + ip + ":443\r\n");
        buffer.append("Service-Type: " + type + PNXConfigConstant.RESP_SPLIT_2);
        buffer.append("Accept: */*\r\n");
        buffer.append("User-Agent:  Jilin University Http Client/Android\r\n");
        buffer.append("Connection: close\r\n");
        buffer.append("Content-Length: " + length + PNXConfigConstant.RESP_SPLIT_2);
        buffer.append(PNXConfigConstant.RESP_SPLIT_2);
        return buffer.toString().getBytes(JQBasicNetwork.UTF_8);
    }

    public static byte[] createAskRequest(String hardwarefinger) throws Exception {
        StringBuffer buffer = new StringBuffer();
        buffer.append("GET /clientAsk HTTP/1.1\r\n");
        buffer.append("Host: 127.0.0.1:10001\r\n");
        buffer.append("Service-Type: clientAskService\r\n");
        buffer.append("Accept: */*\r\n");
        buffer.append("User-Agent:  Jilin University Http Client/Android\r\n");
        if (hardwarefinger != null && !StringEx.Empty.equals(hardwarefinger)) {
            buffer.append("hardwarefinger: " + hardwarefinger + PNXConfigConstant.RESP_SPLIT_2);
        }
        buffer.append("Connection: Keep-Alive\r\n");
        buffer.append(PNXConfigConstant.RESP_SPLIT_2);
        return buffer.toString().getBytes(JQBasicNetwork.UTF_8);
    }

    public static byte[] createAuthRequest(AuthRequest request) throws Exception {
        if (request == null) {
            throw new Exception("auth request is null!");
        }
        StringBuffer buffer = new StringBuffer();
        buffer.append("GET /clientAuth HTTP/1.1\r\n");
        buffer.append("Host: 127.0.0.1:10001\r\n");
        buffer.append("Service-Type: clientLoginService\r\n");
        buffer.append("Accept: */*\r\n");
        buffer.append("User-Agent:  Jilin University Http Client/Android\r\n");
        if (request.getAuthmode() != null) {
            buffer.append("authmode: " + request.getAuthmode() + PNXConfigConstant.RESP_SPLIT_2);
        }
        if (request.getCERTB64() != null) {
            buffer.append("CERTB64: " + request.getCERTB64() + PNXConfigConstant.RESP_SPLIT_2);
        }
        if (request.getORIGINAL() != null) {
            buffer.append("ORIGINAL: " + request.getORIGINAL() + PNXConfigConstant.RESP_SPLIT_2);
        }
        if (request.getALGID() != null) {
            buffer.append("ALGID: " + request.getALGID() + PNXConfigConstant.RESP_SPLIT_2);
        }
        if (request.getSIGNALGID() != null) {
            buffer.append("SIGNALGID: " + request.getSIGNALGID() + PNXConfigConstant.RESP_SPLIT_2);
        }
        if (request.getSINGDATA() != null) {
            buffer.append("SIGNDATA: " + request.getSINGDATA() + PNXConfigConstant.RESP_SPLIT_2);
        }
        if (request.getHardwarefinger() != null) {
            buffer.append("hardwarefinger: " + request.getHardwarefinger() + PNXConfigConstant.RESP_SPLIT_2);
        }
        if (request.getHardwareext() != null) {
            buffer.append("hardwareext: " + request.getHardwareext() + PNXConfigConstant.RESP_SPLIT_2);
        }
        if (request.getClientip() != null) {
            buffer.append("clientip: " + request.getClientip() + PNXConfigConstant.RESP_SPLIT_2);
        }
        buffer.append("Connection: Keep-Alive\r\n");
        buffer.append(PNXConfigConstant.RESP_SPLIT_2);
        return buffer.toString().getBytes(JQBasicNetwork.UTF_8);
    }
}
