package cn.com.jit.pnxclient.pojo;

public class AskInfo {
    private CertPlugin certplugin = null;
    private String errorcode = null;
    private String errormsg = null;
    private String gateway_name = null;
    private String version = null;

    public CertPlugin getCertplugin() {
        return this.certplugin;
    }

    public void setCertplugin(CertPlugin certplugin2) {
        this.certplugin = certplugin2;
    }

    public String getErrorcode() {
        return this.errorcode;
    }

    public void setErrorcode(String errorcode2) {
        this.errorcode = errorcode2;
    }

    public String getErrormsg() {
        return this.errormsg;
    }

    public void setErrormsg(String errormsg2) {
        this.errormsg = errormsg2;
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(String version2) {
        this.version = version2;
    }

    public String getGateway_name() {
        return this.gateway_name;
    }

    public void setGateway_name(String gatewayName) {
        this.gateway_name = gatewayName;
    }

    public String toString() {
        return "AskInfo [certplugin=" + this.certplugin + ", errorcode=" + this.errorcode + ", errormsg=" + this.errormsg + ", gateway_name=" + this.gateway_name + ", version=" + this.version + "]";
    }
}
