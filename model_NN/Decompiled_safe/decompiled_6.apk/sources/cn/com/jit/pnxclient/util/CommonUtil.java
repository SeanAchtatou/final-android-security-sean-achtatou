package cn.com.jit.pnxclient.util;

import android.os.Build;
import java.io.File;

public class CommonUtil {
    public static int getAndroidSDKVersion() {
        try {
            return Integer.valueOf(Build.VERSION.SDK_INT).intValue();
        } catch (NumberFormatException e) {
            return 7;
        }
    }

    public static boolean isFileExist(String path) {
        return new File(path).exists();
    }
}
