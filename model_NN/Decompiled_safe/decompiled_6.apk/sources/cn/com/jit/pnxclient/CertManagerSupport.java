package cn.com.jit.pnxclient;

import cn.com.jit.ida.util.pki.keystore.KeyEntry;
import cn.com.jit.pnxclient.constant.MessageCodeDec;
import cn.com.jit.pnxclient.constant.PNXConfigConstant;
import cn.com.jit.pnxclient.exception.PNXClientException;
import cn.com.jit.pnxclient.pojo.CertEntry;
import com.jianq.misc.StringEx;
import java.util.ArrayList;
import java.util.List;

public class CertManagerSupport implements PNXClientSupport {
    private CertManager certManager = new CertManager();

    protected CertManagerSupport() {
    }

    public String getErrorCode() {
        return this.certManager.getErrorCode();
    }

    public String getLastError() {
        String ecode = getErrorCode();
        if (ecode == null || ecode.length() <= 0) {
            return StringEx.Empty;
        }
        return MessageCodeDec.getDec(ecode);
    }

    public List<CertEntry> getCertList() throws PNXClientException {
        List<KeyEntry> keyEntryList = this.certManager.getCertList();
        if (keyEntryList == null) {
            return null;
        }
        List<CertEntry> certList = new ArrayList<>();
        for (KeyEntry keyEntry : keyEntryList) {
            CertEntry cert = new CertEntry();
            cert.setAilas(keyEntry.getAilas());
            cert.setIssuer(keyEntry.getCert().getIssuer());
            cert.setNotAfter(keyEntry.getCert().getNotAfter());
            cert.setNotBefore(keyEntry.getCert().getNotBefore());
            cert.setSerialNumber(keyEntry.getCert().getSerialNumber());
            cert.setSubject(keyEntry.getCert().getSubject());
            certList.add(cert);
        }
        return certList;
    }

    public boolean requestCert(String credential, String pwd, String gatewayIp) throws PNXClientException {
        return this.certManager.genCert(credential, pwd, gatewayIp, PNXConfigConstant.PNXSERVERPORT_DEFAULT);
    }

    public boolean deleteCert(String alias) throws PNXClientException {
        return this.certManager.deleteCert(alias);
    }

    public boolean changeCertPassword(String ailas, String oldPwd, String newPwd) throws PNXClientException {
        return this.certManager.changeCertPwd(ailas, oldPwd, newPwd);
    }
}
