package cn.com.jit.pnxclient.pojo;

public class AppInfo {
    private String access_type = null;
    private String accessctrl = null;
    private String alias = null;
    private String app_type = null;
    private String appdesc = null;
    private String appflag = null;
    private String appip = null;
    private String appname = null;
    private String apport = null;
    private String attributes = null;
    private String conn_timeout = null;
    private String deployment_type = null;
    private String hideflag = null;
    private String iphide = null;
    private String isdomain = null;
    private String openflag = null;
    private String plugin_name = null;
    private String protect_type = null;
    private String protocol_type = null;
    private String realip = null;
    private String role_replace = null;
    private String trans_mode = null;
    private String url = null;

    public String getProtocol_type() {
        return this.protocol_type;
    }

    public void setProtocol_type(String protocolType) {
        this.protocol_type = protocolType;
    }

    public String getAppname() {
        return this.appname;
    }

    public void setAppname(String appname2) {
        this.appname = appname2;
    }

    public String getAppflag() {
        return this.appflag;
    }

    public void setAppflag(String appflag2) {
        this.appflag = appflag2;
    }

    public String getAlias() {
        return this.alias;
    }

    public void setAlias(String alias2) {
        this.alias = alias2;
    }

    public String getAppdesc() {
        return this.appdesc;
    }

    public void setAppdesc(String appdesc2) {
        this.appdesc = appdesc2;
    }

    public String getProtect_type() {
        return this.protect_type;
    }

    public void setProtect_type(String protectType) {
        this.protect_type = protectType;
    }

    public String getDeployment_type() {
        return this.deployment_type;
    }

    public void setDeployment_type(String deploymentType) {
        this.deployment_type = deploymentType;
    }

    public String getApp_type() {
        return this.app_type;
    }

    public void setApp_type(String appType) {
        this.app_type = appType;
    }

    public String getAccess_type() {
        return this.access_type;
    }

    public void setAccess_type(String accessType) {
        this.access_type = accessType;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public String getConn_timeout() {
        return this.conn_timeout;
    }

    public void setConn_timeout(String connTimeout) {
        this.conn_timeout = connTimeout;
    }

    public String getPlugin_name() {
        return this.plugin_name;
    }

    public void setPlugin_name(String pluginName) {
        this.plugin_name = pluginName;
    }

    public String getTrans_mode() {
        return this.trans_mode;
    }

    public void setTrans_mode(String transMode) {
        this.trans_mode = transMode;
    }

    public String getIphide() {
        return this.iphide;
    }

    public void setIphide(String iphide2) {
        this.iphide = iphide2;
    }

    public String getRole_replace() {
        return this.role_replace;
    }

    public void setRole_replace(String roleReplace) {
        this.role_replace = roleReplace;
    }

    public String getAccessctrl() {
        return this.accessctrl;
    }

    public void setAccessctrl(String accessctrl2) {
        this.accessctrl = accessctrl2;
    }

    public String getOpenflag() {
        return this.openflag;
    }

    public void setOpenflag(String openflag2) {
        this.openflag = openflag2;
    }

    public String getHideflag() {
        return this.hideflag;
    }

    public void setHideflag(String hideflag2) {
        this.hideflag = hideflag2;
    }

    public String getAppip() {
        return this.appip;
    }

    public void setAppip(String appip2) {
        this.appip = appip2;
    }

    public String getApport() {
        return this.apport;
    }

    public void setApport(String apport2) {
        this.apport = apport2;
    }

    public String getIsdomain() {
        return this.isdomain;
    }

    public void setIsdomain(String isdomain2) {
        this.isdomain = isdomain2;
    }

    public String getRealip() {
        return this.realip;
    }

    public void setRealip(String realip2) {
        this.realip = realip2;
    }

    public String getAttributes() {
        return this.attributes;
    }

    public void setAttributes(String attributes2) {
        this.attributes = attributes2;
    }

    public String toString() {
        return "AppInfo [access_type=" + this.access_type + ", accessctrl=" + this.accessctrl + ", alias=" + this.alias + ", app_type=" + this.app_type + ", appdesc=" + this.appdesc + ", appflag=" + this.appflag + ", appip=" + this.appip + ", appname=" + this.appname + ", apport=" + this.apport + ", conn_timeout=" + this.conn_timeout + ", deployment_type=" + this.deployment_type + ", hideflag=" + this.hideflag + ", iphide=" + this.iphide + ", isdomain=" + this.isdomain + ", openflag=" + this.openflag + ", plugin_name=" + this.plugin_name + ", protect_type=" + this.protect_type + ", protocol_type=" + this.protocol_type + ", realip=" + this.realip + ", role_replace=" + this.role_replace + ", trans_mode=" + this.trans_mode + ", url=" + this.url + ", attributes=" + this.attributes + "]";
    }
}
