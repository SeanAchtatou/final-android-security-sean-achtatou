package cn.com.jit.pnxclient.pojo;

import java.util.HashMap;
import java.util.Map;

public class AuthResponse extends HashMap<String, Map> {
    private static final long serialVersionUID = -7968091588869240216L;

    public Map get(String key) {
        return (Map) super.get((Object) key);
    }

    public AuthResponse put(String ailas, String name, String value) {
        Map map = get(ailas);
        if (map == null) {
            map = new HashMap();
            super.put(ailas, map);
        }
        map.put(name, value);
        return this;
    }

    public String getAttribute(String ailas, String name) {
        Object value;
        Map map = get(ailas);
        if (map == null || (value = map.get(name)) == null) {
            return null;
        }
        return value.toString();
    }
}
