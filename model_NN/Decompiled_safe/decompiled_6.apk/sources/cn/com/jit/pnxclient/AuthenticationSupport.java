package cn.com.jit.pnxclient;

import android.util.Log;
import cn.com.jit.ida.util.pki.encoders.Base64;
import cn.com.jit.pnxclient.constant.MessageCode;
import cn.com.jit.pnxclient.constant.MessageCodeDec;
import cn.com.jit.pnxclient.constant.PNXConfigConstant;
import cn.com.jit.pnxclient.exception.PNXClientException;
import cn.com.jit.pnxclient.net.MessageAssembly;
import cn.com.jit.pnxclient.pojo.AuthResponse;
import com.jianq.misc.StringEx;

public class AuthenticationSupport implements PNXClientSupport {
    private AuthenticationManager authenticationManager = new AuthenticationManager();

    protected AuthenticationSupport() {
    }

    public String getErrorCode() {
        return this.authenticationManager.getErrorCode();
    }

    public String getLastError() {
        String ecode = getErrorCode();
        if (ecode == null || ecode.length() <= 0) {
            return StringEx.Empty;
        }
        return MessageCodeDec.getDec(ecode);
    }

    public String createAuthCredential(String ailas, String certPWD, String appId) throws PNXClientException {
        byte[] certSingData = this.authenticationManager.getCertSignData(ailas, System.currentTimeMillis() + StringEx.Empty, certPWD);
        if (certSingData == null) {
            Log.w(MessageCode.A00201, getLastError());
            throw new PNXClientException(MessageCode.A00201);
        }
        String authCredential = MessageAssembly.getAttachRequest(new String(Base64.encode(certSingData)), appId);
        Log.i("AuthCredential OUTPARAM ", authCredential);
        return authCredential;
    }

    public String createAuthCredential(String ailas, String certPWD, String appId, boolean rebase64) throws PNXClientException {
        String authCredential = createAuthCredential(ailas, certPWD, appId);
        if (rebase64) {
            return new String(Base64.encode(authCredential.getBytes()));
        }
        return authCredential;
    }

    public boolean authLogin(String gateway_ip, String cert_ailas, String cert_pwd) throws PNXClientException {
        return this.authenticationManager.authLogin(gateway_ip, PNXConfigConstant.PNXSERVERPORT_DEFAULT, cert_ailas, cert_pwd);
    }

    public String fetchApplicationAttribute(String app_ailas, String property_name) {
        AuthResponse authResponse = this.authenticationManager.getAuthLoginResult();
        if (authResponse == null) {
            return null;
        }
        return authResponse.getAttribute(app_ailas, property_name);
    }
}
