package cn.com.jit.pnxclient.net;

import cn.com.jit.ida.util.pki.encoders.Base64;
import cn.com.jit.pnxclient.constant.ApplicationConstant;
import cn.com.jit.pnxclient.pojo.AppInfo;
import cn.com.jit.pnxclient.pojo.AskInfo;
import cn.com.jit.pnxclient.pojo.CertPlugin;
import cn.com.jit.pnxclient.pojo.CertResponse;
import cn.com.jit.pnxclient.pojo.SimpleElement;
import cn.com.jit.pnxclient.pojo.UserInfo;
import cn.com.jit.pnxclient.util.SimpleDOMParser;
import com.jianq.net.JQBasicNetwork;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import javax.net.ssl.SSLSocket;

public class Gwtk {
    public static byte[] connectGWServer(String ip, int port, byte[] msg, int mode, SSLConfig sslconfig) throws Exception {
        return connectGWServer(ip, port, msg, mode, sslconfig, 10, 30);
    }

    public static byte[] connectGWServer(String ip, int port, byte[] msg, int mode, SSLConfig sslconfig, int conntimeout, int readtimeout) throws Exception {
        Socket socket;
        Socket socket2;
        InputStream in = null;
        OutputStream out = null;
        if (mode == 0) {
            try {
                socket2 = new Socket();
            } catch (Throwable th) {
                th = th;
            }
            try {
                socket2.connect(new InetSocketAddress(ip, port), conntimeout * 1000);
                socket2.setSoTimeout(readtimeout * 1000);
                socket = socket2;
            } catch (Throwable th2) {
                th = th2;
                try {
                    out.close();
                } catch (Exception e) {
                }
                try {
                    in.close();
                } catch (Exception e2) {
                }
                throw th;
            }
        } else if (mode == 1) {
            socket = SSLContextFactory.getSSLContextInstance(false, sslconfig).getSocketFactory().createSocket();
            socket.connect(new InetSocketAddress(ip, port), conntimeout * 1000);
            socket.setSoTimeout(readtimeout * 1000);
            ((SSLSocket) socket).setNeedClientAuth(false);
            ((SSLSocket) socket).setUseClientMode(true);
            ((SSLSocket) socket).startHandshake();
        } else {
            throw new Exception("mode value:" + mode + " is error!");
        }
        in = socket.getInputStream();
        out = socket.getOutputStream();
        out.write(msg);
        out.flush();
        byte[] ret = getBody(in);
        try {
            out.close();
        } catch (Exception e3) {
        }
        try {
            in.close();
        } catch (Exception e4) {
        }
        return ret;
    }

    private static byte[] getBody(InputStream input) throws Exception {
        if (input == null) {
            return null;
        }
        int tag = 0;
        boolean gotHeader = false;
        ByteArrayOutputStream bous = new ByteArrayOutputStream();
        while (true) {
            int hr = input.read();
            if (hr != -1) {
                if (hr == 10 || hr == 13) {
                    tag++;
                } else if (tag != 0) {
                    tag = 0;
                }
                bous.write(hr);
                if (tag == 4) {
                    gotHeader = true;
                    break;
                }
            } else {
                break;
            }
        }
        if (!gotHeader) {
            String retStr = new String(bous.toByteArray());
            System.out.println("HttpRequest data format error: couldn't find HTTP header::" + retStr);
            throw new Exception("HttpRequest data format error: couldn't find HTTP header::" + retStr);
        }
        int dataLen = 0;
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(bous.toByteArray()), JQBasicNetwork.UTF_8));
        while (true) {
            String str = bufferedReader.readLine();
            if (str != null) {
                if (str.startsWith("Content-Length:")) {
                    dataLen = Integer.parseInt(str.substring(15, str.length()).trim());
                    break;
                }
            } else {
                break;
            }
        }
        if (dataLen <= 0) {
            throw new Exception("data length: " + dataLen + " is inviable.");
        }
        byte[] response = new byte[dataLen];
        int readLen = input.read(response);
        if (readLen == -1) {
            throw new Exception("read data content error.");
        }
        while (readLen < dataLen) {
            byte[] tmp = new byte[(dataLen - readLen)];
            int readed = input.read(tmp);
            System.arraycopy(tmp, 0, response, readLen, readed);
            readLen += readed;
        }
        return response;
    }

    public static CertResponse parserCertResponse(byte[] xml) throws Exception {
        if (xml == null || xml.length == 0) {
            throw new Exception("return xml is null");
        }
        CertResponse response = new CertResponse();
        SimpleElement se = new SimpleDOMParser().parse(new BufferedReader(new InputStreamReader(new ByteArrayInputStream(xml), JQBasicNetwork.UTF_8)));
        if (se != null) {
            Object[] objse = se.getChildElements();
            if (objse == null) {
                throw new Exception("xml content is null");
            }
            for (Object obj : objse) {
                SimpleElement tse = (SimpleElement) obj;
                if (tse.getTagName().equalsIgnoreCase("head")) {
                    Object[] error = tse.getChildElements();
                    response.setErrorcode(((SimpleElement) error[0]).getText());
                    response.setErrormsg(((SimpleElement) error[1]).getText());
                    if (!"0".equals(response.getErrorcode())) {
                        break;
                    }
                }
                if (tse.getTagName().equalsIgnoreCase("p7b")) {
                    response.setP7b(tse.getText());
                }
                if (tse.getTagName().equalsIgnoreCase("doubleP7b")) {
                    response.setDoubleP7b(tse.getText());
                }
                if (tse.getTagName().equalsIgnoreCase("doubleEncryptedSessionKey")) {
                    response.setDoubleEncryptedSessionKey(tse.getText());
                }
                if (tse.getTagName().equalsIgnoreCase("sessionKeyAlg")) {
                    response.setSessionKeyAlg(tse.getText());
                }
                if (tse.getTagName().equalsIgnoreCase("doubleEncryptedPrivateKey")) {
                    response.setDoubleEncryptedPrivateKey(tse.getText());
                }
            }
        }
        return response;
    }

    public static AskInfo getAskInfo(byte[] xml) throws Exception {
        Object[] objsetting;
        SimpleElement se = new SimpleDOMParser().parse(new BufferedReader(new InputStreamReader(new ByteArrayInputStream(xml), JQBasicNetwork.UTF_8)));
        AskInfo askinfo = null;
        if (se != null) {
            askinfo = new AskInfo();
            Object[] objse = se.getChildElements();
            if (objse == null) {
                throw new Exception("xml content is null");
            }
            for (Object obj : objse) {
                SimpleElement tse = (SimpleElement) obj;
                if (tse.getTagName().equalsIgnoreCase("version")) {
                    askinfo.setVersion(tse.getText());
                }
                if (tse.getTagName().equalsIgnoreCase("gateway_name")) {
                    askinfo.setGateway_name(tse.getText());
                }
                if (tse.getTagName().equalsIgnoreCase("result")) {
                    String errorcode = tse.getAttribute("errorcode");
                    askinfo.setErrorcode(errorcode);
                    if (!"0".equals(errorcode)) {
                        askinfo.setErrormsg(((SimpleElement) tse.getChildElements()[0]).getText());
                    }
                }
                if (tse.getTagName().equalsIgnoreCase("authplugins")) {
                    Object[] objauth = tse.getChildElements();
                    for (Object obj2 : objauth) {
                        SimpleElement tmpse = (SimpleElement) obj2;
                        if (tmpse.getTagName().equalsIgnoreCase("plugin") && "2".equals(tmpse.getAttribute("authmode"))) {
                            CertPlugin plugin = new CertPlugin();
                            plugin.setAuthmode(tmpse.getAttribute("authmode"));
                            plugin.setTypeid(tmpse.getAttribute("typeid"));
                            Object[] objtmp = tmpse.getChildElements();
                            if (objtmp != null && objtmp.length > 0) {
                                for (Object obj3 : objtmp) {
                                    SimpleElement tmppluginse = (SimpleElement) obj3;
                                    if (tmppluginse.getTagName().equalsIgnoreCase("pluginconfig")) {
                                        Object[] objconf = new SimpleDOMParser().parse(new BufferedReader(new InputStreamReader(new ByteArrayInputStream(Base64.decode(tmppluginse.getText())), JQBasicNetwork.UTF_8))).getChildElements();
                                        if (objconf != null && objconf.length > 0) {
                                            for (Object obj4 : objconf) {
                                                SimpleElement tmpconfse = (SimpleElement) obj4;
                                                if (tmpconfse.getTagName().equalsIgnoreCase("settings") && (objsetting = tmpconfse.getChildElements()) != null && objsetting.length > 0) {
                                                    for (int n = 0; n < objsetting.length; n++) {
                                                        if (((SimpleElement) objsetting[n]).getTagName().equalsIgnoreCase("original")) {
                                                            plugin.setOriginal(((SimpleElement) objsetting[n]).getAttribute("val"));
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            askinfo.setCertplugin(plugin);
                        }
                    }
                }
            }
        }
        return askinfo;
    }

    public static UserInfo getUserInfo(byte[] xml) throws Exception {
        SimpleElement se = new SimpleDOMParser().parse(new BufferedReader(new InputStreamReader(new ByteArrayInputStream(xml), JQBasicNetwork.UTF_8)));
        UserInfo userinfo = null;
        if (se != null) {
            userinfo = new UserInfo();
            Object[] objse = se.getChildElements();
            if (objse == null) {
                throw new Exception("xml content is null");
            }
            for (Object obj : objse) {
                SimpleElement tse = (SimpleElement) obj;
                if (tse.getTagName().equalsIgnoreCase("result")) {
                    String errorcode = tse.getAttribute("errorcode");
                    userinfo.setErrorcode(errorcode);
                    if (!"0".equals(errorcode)) {
                        userinfo.setErrormsg(((SimpleElement) tse.getChildElements()[0]).getText());
                    }
                }
                if (tse.getTagName().equalsIgnoreCase("jit_settings")) {
                    Object[] objsetting = tse.getChildElements();
                    for (Object obj2 : objsetting) {
                        SimpleElement settingse = (SimpleElement) obj2;
                        if (settingse.getTagName().equalsIgnoreCase("token")) {
                            userinfo.setToken(settingse.getAttribute("val"));
                        }
                    }
                }
                if (tse.getTagName().equalsIgnoreCase("applist")) {
                    Object[] objapp = tse.getChildElements();
                    AppInfo[] appinfo = new AppInfo[objapp.length];
                    for (int j = 0; j < objapp.length; j++) {
                        appinfo[j] = new AppInfo();
                        SimpleElement appse = (SimpleElement) objapp[j];
                        if (appse.getTagName().equalsIgnoreCase("application")) {
                            appinfo[j].setAppname(appse.getAttribute(ApplicationConstant.APP_NAME));
                            appinfo[j].setAppflag(appse.getAttribute(ApplicationConstant.APP_FLAG));
                            appinfo[j].setAlias(appse.getAttribute("alias"));
                            appinfo[j].setUrl(appse.getAttribute(ApplicationConstant.APP_URL));
                            appinfo[j].setAccess_type(appse.getAttribute(ApplicationConstant.APP_ACCESS_TYPE));
                            Object[] appcs = appse.getChildElements();
                            for (Object obj3 : appcs) {
                                SimpleElement appce = (SimpleElement) obj3;
                                if (appce.getTagName().equalsIgnoreCase("attributes")) {
                                    appinfo[j].setAttributes(new String(Base64.decode(appce.getText()), JQBasicNetwork.UTF_8));
                                }
                            }
                        }
                    }
                    userinfo.setAppinfos(appinfo);
                }
            }
        }
        return userinfo;
    }
}
