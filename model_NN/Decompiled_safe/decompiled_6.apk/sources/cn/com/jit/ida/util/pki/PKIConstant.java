package cn.com.jit.ida.util.pki;

import cn.com.jit.ida.util.pki.asn1.pkcs.PKCSObjectIdentifiers;
import cn.com.jit.ida.util.pki.asn1.x9.X9ObjectIdentifiers;
import java.util.Hashtable;

public class PKIConstant {
    public static final String MD2_WITH_RSA = "MD2withRSAEncryption";
    public static final String MD5_WITH_RSA = "MD5withRSAEncryption";
    public static final String SHA1_WITH_DSA = "SHA1withDSA";
    public static final String SHA1_WITH_ECDSA = "SHA1withECDSA";
    public static final String SHA1_WITH_RSA = "SHA1withRSAEncryption";
    public static final String SHA224_WITH_ECDSA = "SHA224withECDSA";
    public static final String SHA224_WITH_RSA = "SHA224withRSAEncryption";
    public static final String SHA256_WITH_ECDSA = "SHA256withECDSA";
    public static final String SHA256_WITH_RSA = "SHA256withRSAEncryption";
    public static final String SHA384_WITH_RSA = "SHA384withRSAEncryption";
    public static final String SHA512_WITH_RSA = "SHA512withRSAEncryption";
    public static final String SM3_WITH_SM2 = "SM3withSM2Encryption";
    public static final Hashtable oid2SigAlgName = new Hashtable();
    public static final Hashtable sigAlgName2OID = new Hashtable();

    static {
        sigAlgName2OID.put("SHA1withRSAEncryption", PKCSObjectIdentifiers.sha1WithRSAEncryption);
        sigAlgName2OID.put("MD2withRSAEncryption", PKCSObjectIdentifiers.md2WithRSAEncryption);
        sigAlgName2OID.put("MD5withRSAEncryption", PKCSObjectIdentifiers.md5WithRSAEncryption);
        sigAlgName2OID.put("SHA1withECDSA", PKCSObjectIdentifiers.sha1WithECEncryption);
        sigAlgName2OID.put("SHA1withDSA", PKCSObjectIdentifiers.sha1WithDSA);
        sigAlgName2OID.put("SHA224withRSAEncryption", PKCSObjectIdentifiers.sha224WithRSAEncryption);
        sigAlgName2OID.put("SHA256withRSAEncryption", PKCSObjectIdentifiers.sha256WithRSAEncryption);
        sigAlgName2OID.put("SHA384withRSAEncryption", PKCSObjectIdentifiers.sha384WithRSAEncryption);
        sigAlgName2OID.put("SHA512withRSAEncryption", PKCSObjectIdentifiers.sha512WithRSAEncryption);
        sigAlgName2OID.put("SHA224withECDSA", X9ObjectIdentifiers.ecdsa_with_SHA224);
        sigAlgName2OID.put("SHA256withECDSA", X9ObjectIdentifiers.ecdsa_with_SHA256);
        sigAlgName2OID.put("SM3withSM2Encryption", PKCSObjectIdentifiers.sm2_with_sm3);
        oid2SigAlgName.put(PKCSObjectIdentifiers.sha1WithRSAEncryption, "SHA1withRSAEncryption");
        oid2SigAlgName.put(PKCSObjectIdentifiers.md2WithRSAEncryption, "MD2withRSAEncryption");
        oid2SigAlgName.put(PKCSObjectIdentifiers.md5WithRSAEncryption, "MD5withRSAEncryption");
        oid2SigAlgName.put(PKCSObjectIdentifiers.sha1WithECEncryption, "SHA1withECDSA");
        oid2SigAlgName.put(PKCSObjectIdentifiers.sha1WithDSA, "SHA1withDSA");
        oid2SigAlgName.put(PKCSObjectIdentifiers.sha224WithRSAEncryption, "SHA224withRSAEncryption");
        oid2SigAlgName.put(PKCSObjectIdentifiers.sha256WithRSAEncryption, "SHA256withRSAEncryption");
        oid2SigAlgName.put(PKCSObjectIdentifiers.sha384WithRSAEncryption, "SHA384withRSAEncryption");
        oid2SigAlgName.put(PKCSObjectIdentifiers.sha512WithRSAEncryption, "SHA512withRSAEncryption");
        oid2SigAlgName.put(X9ObjectIdentifiers.ecdsa_with_SHA224, "SHA224withECDSA");
        oid2SigAlgName.put(X9ObjectIdentifiers.ecdsa_with_SHA256, "SHA256withECDSA");
        oid2SigAlgName.put(PKCSObjectIdentifiers.sm2_with_sm3, "SM3withSM2Encryption");
    }
}
