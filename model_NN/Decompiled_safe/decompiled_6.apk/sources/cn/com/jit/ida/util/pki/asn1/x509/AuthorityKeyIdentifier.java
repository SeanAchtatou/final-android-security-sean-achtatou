package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1OctetString;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.DERTaggedObject;
import cn.com.jit.ida.util.pki.asn1.DERTags;
import de.innosystec.unrar.unpack.vm.VMCmdFlags;
import java.math.BigInteger;
import java.util.Enumeration;

public class AuthorityKeyIdentifier implements DEREncodable, DERTags {
    GeneralNames certissuer = null;
    DERInteger certserno = null;
    ASN1OctetString keyidentifier = null;

    public interface Digest {
        int doFinal(byte[] bArr, int i);

        String getAlgorithmName();

        int getDigestSize();

        void reset();

        void update(byte b);

        void update(byte[] bArr, int i, int i2);
    }

    public static AuthorityKeyIdentifier getInstance(ASN1TaggedObject obj, boolean explicit) {
        return getInstance(ASN1Sequence.getInstance(obj, explicit));
    }

    public static AuthorityKeyIdentifier getInstance(Object obj) {
        if (obj instanceof AuthorityKeyIdentifier) {
            return (AuthorityKeyIdentifier) obj;
        }
        if (obj instanceof ASN1Sequence) {
            return new AuthorityKeyIdentifier((ASN1Sequence) obj);
        }
        throw new IllegalArgumentException("unknown object in factory");
    }

    public AuthorityKeyIdentifier(ASN1Sequence seq) {
        Enumeration e = seq.getObjects();
        while (e.hasMoreElements()) {
            DERTaggedObject o = (DERTaggedObject) e.nextElement();
            switch (o.getTagNo()) {
                case 0:
                    this.keyidentifier = ASN1OctetString.getInstance(o, false);
                    break;
                case 1:
                    this.certissuer = GeneralNames.getInstance(o, false);
                    break;
                case 2:
                    this.certserno = DERInteger.getInstance(o, false);
                    break;
                default:
                    throw new IllegalArgumentException("illegal tag");
            }
        }
    }

    public AuthorityKeyIdentifier(SubjectPublicKeyInfo spki) {
        Digest digest = new SHA1Digest();
        byte[] resBuf = new byte[digest.getDigestSize()];
        byte[] bytes = spki.getPublicKeyData().getBytes();
        digest.update(bytes, 0, bytes.length);
        digest.doFinal(resBuf, 0);
        this.keyidentifier = new DEROctetString(resBuf);
    }

    public AuthorityKeyIdentifier(SubjectPublicKeyInfo spki, GeneralNames name) {
        Digest digest = new SHA1Digest();
        byte[] resBuf = new byte[digest.getDigestSize()];
        byte[] bytes = spki.getPublicKeyData().getBytes();
        digest.update(bytes, 0, bytes.length);
        digest.doFinal(resBuf, 0);
        this.keyidentifier = new DEROctetString(resBuf);
        this.certissuer = name;
    }

    public AuthorityKeyIdentifier(SubjectPublicKeyInfo spki, BigInteger serialNumber) {
        Digest digest = new SHA1Digest();
        byte[] resBuf = new byte[digest.getDigestSize()];
        byte[] bytes = spki.getPublicKeyData().getBytes();
        digest.update(bytes, 0, bytes.length);
        digest.doFinal(resBuf, 0);
        this.keyidentifier = new DEROctetString(resBuf);
        this.certserno = new DERInteger(serialNumber);
    }

    public AuthorityKeyIdentifier(SubjectPublicKeyInfo spki, GeneralNames name, BigInteger serialNumber) {
        Digest digest = new SHA1Digest();
        byte[] resBuf = new byte[digest.getDigestSize()];
        byte[] bytes = spki.getPublicKeyData().getBytes();
        digest.update(bytes, 0, bytes.length);
        digest.doFinal(resBuf, 0);
        this.keyidentifier = new DEROctetString(resBuf);
        this.certissuer = name;
        this.certserno = new DERInteger(serialNumber);
    }

    public byte[] getKeyIdentifier() {
        if (this.keyidentifier != null) {
            return this.keyidentifier.getOctets();
        }
        return null;
    }

    public GeneralNames getGeneralNames() {
        return this.certissuer;
    }

    public DERInteger getCertserno() {
        return this.certserno;
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        if (this.keyidentifier != null) {
            v.add(new DERTaggedObject(false, 0, this.keyidentifier));
        }
        if (this.certissuer != null) {
            v.add(new DERTaggedObject(false, 1, this.certissuer));
        }
        if (this.certserno != null) {
            v.add(new DERTaggedObject(false, 2, this.certserno));
        }
        return new DERSequence(v);
    }

    public String toString() {
        return "AuthorityKeyIdentifier: KeyID(" + this.keyidentifier.getOctets() + ")";
    }

    public abstract class GeneralDigest implements Digest {
        private long byteCount;
        private byte[] xBuf;
        private int xBufOff;

        /* access modifiers changed from: protected */
        public abstract void processBlock();

        /* access modifiers changed from: protected */
        public abstract void processLength(long j);

        /* access modifiers changed from: protected */
        public abstract void processWord(byte[] bArr, int i);

        protected GeneralDigest() {
            this.xBuf = new byte[4];
            this.xBufOff = 0;
        }

        protected GeneralDigest(GeneralDigest t) {
            this.xBuf = new byte[t.xBuf.length];
            System.arraycopy(t.xBuf, 0, this.xBuf, 0, t.xBuf.length);
            this.xBufOff = t.xBufOff;
            this.byteCount = t.byteCount;
        }

        public void update(byte in) {
            byte[] bArr = this.xBuf;
            int i = this.xBufOff;
            this.xBufOff = i + 1;
            bArr[i] = in;
            if (this.xBufOff == this.xBuf.length) {
                processWord(this.xBuf, 0);
                this.xBufOff = 0;
            }
            this.byteCount++;
        }

        public void update(byte[] in, int inOff, int len) {
            while (this.xBufOff != 0 && len > 0) {
                update(in[inOff]);
                inOff++;
                len--;
            }
            while (len > this.xBuf.length) {
                processWord(in, inOff);
                inOff += this.xBuf.length;
                len -= this.xBuf.length;
                this.byteCount += (long) this.xBuf.length;
            }
            while (len > 0) {
                update(in[inOff]);
                inOff++;
                len--;
            }
        }

        public void finish() {
            long bitLength = this.byteCount << 3;
            update(Byte.MIN_VALUE);
            while (this.xBufOff != 0) {
                update((byte) 0);
            }
            processLength(bitLength);
            processBlock();
        }

        public void reset() {
            this.byteCount = 0;
            this.xBufOff = 0;
            for (int i = 0; i < this.xBuf.length; i++) {
                this.xBuf[i] = 0;
            }
        }
    }

    public class SHA1Digest extends GeneralDigest {
        private static final int DIGEST_LENGTH = 20;
        private static final int Y1 = 1518500249;
        private static final int Y2 = 1859775393;
        private static final int Y3 = -1894007588;
        private static final int Y4 = -899497514;
        private int H1;
        private int H2;
        private int H3;
        private int H4;
        private int H5;
        private int[] X = new int[80];
        private int xOff;

        public SHA1Digest() {
            super();
            reset();
        }

        public SHA1Digest(SHA1Digest t) {
            super(t);
            this.H1 = t.H1;
            this.H2 = t.H2;
            this.H3 = t.H3;
            this.H4 = t.H4;
            this.H5 = t.H5;
            System.arraycopy(t.X, 0, this.X, 0, t.X.length);
            this.xOff = t.xOff;
        }

        public String getAlgorithmName() {
            return "SHA-1";
        }

        public int getDigestSize() {
            return 20;
        }

        /* access modifiers changed from: protected */
        public void processWord(byte[] in, int inOff) {
            int[] iArr = this.X;
            int i = this.xOff;
            this.xOff = i + 1;
            iArr[i] = ((in[inOff] & 255) << 24) | ((in[inOff + 1] & 255) << VMCmdFlags.VMCF_PROC) | ((in[inOff + 2] & 255) << 8) | (in[inOff + 3] & 255);
            if (this.xOff == 16) {
                processBlock();
            }
        }

        private void unpackWord(int word, byte[] out, int outOff) {
            out[outOff] = (byte) (word >>> 24);
            out[outOff + 1] = (byte) (word >>> 16);
            out[outOff + 2] = (byte) (word >>> 8);
            out[outOff + 3] = (byte) word;
        }

        /* access modifiers changed from: protected */
        public void processLength(long bitLength) {
            if (this.xOff > 14) {
                processBlock();
            }
            this.X[14] = (int) (bitLength >>> 32);
            this.X[15] = (int) (-1 & bitLength);
        }

        public int doFinal(byte[] out, int outOff) {
            finish();
            unpackWord(this.H1, out, outOff);
            unpackWord(this.H2, out, outOff + 4);
            unpackWord(this.H3, out, outOff + 8);
            unpackWord(this.H4, out, outOff + 12);
            unpackWord(this.H5, out, outOff + 16);
            reset();
            return 20;
        }

        public void reset() {
            super.reset();
            this.H1 = 1732584193;
            this.H2 = -271733879;
            this.H3 = -1732584194;
            this.H4 = 271733878;
            this.H5 = -1009589776;
            this.xOff = 0;
            for (int i = 0; i != this.X.length; i++) {
                this.X[i] = 0;
            }
        }

        private int f(int u, int v, int w) {
            return (u & v) | ((u ^ -1) & w);
        }

        private int h(int u, int v, int w) {
            return (u ^ v) ^ w;
        }

        private int g(int u, int v, int w) {
            return (u & v) | (u & w) | (v & w);
        }

        private int rotateLeft(int x, int n) {
            return (x << n) | (x >>> (32 - n));
        }

        /* access modifiers changed from: protected */
        public void processBlock() {
            for (int i = 16; i <= 79; i++) {
                this.X[i] = rotateLeft(((this.X[i - 3] ^ this.X[i - 8]) ^ this.X[i - 14]) ^ this.X[i - 16], 1);
            }
            int A = this.H1;
            int B = this.H2;
            int C = this.H3;
            int D = this.H4;
            int E = this.H5;
            for (int j = 0; j <= 19; j++) {
                int t = rotateLeft(A, 5) + f(B, C, D) + E + this.X[j] + Y1;
                E = D;
                D = C;
                C = rotateLeft(B, 30);
                B = A;
                A = t;
            }
            for (int j2 = 20; j2 <= 39; j2++) {
                int t2 = rotateLeft(A, 5) + h(B, C, D) + E + this.X[j2] + Y2;
                E = D;
                D = C;
                C = rotateLeft(B, 30);
                B = A;
                A = t2;
            }
            for (int j3 = 40; j3 <= 59; j3++) {
                int t3 = rotateLeft(A, 5) + g(B, C, D) + E + this.X[j3] + Y3;
                E = D;
                D = C;
                C = rotateLeft(B, 30);
                B = A;
                A = t3;
            }
            for (int j4 = 60; j4 <= 79; j4++) {
                int t4 = rotateLeft(A, 5) + h(B, C, D) + E + this.X[j4] + Y4;
                E = D;
                D = C;
                C = rotateLeft(B, 30);
                B = A;
                A = t4;
            }
            this.H1 += A;
            this.H2 += B;
            this.H3 += C;
            this.H4 += D;
            this.H5 += E;
            this.xOff = 0;
            for (int i2 = 0; i2 != this.X.length; i2++) {
                this.X[i2] = 0;
            }
        }
    }
}
