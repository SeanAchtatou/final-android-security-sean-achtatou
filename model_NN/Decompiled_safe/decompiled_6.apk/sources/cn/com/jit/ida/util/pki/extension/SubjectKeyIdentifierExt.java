package cn.com.jit.ida.util.pki.extension;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.Parser;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.x509.SubjectKeyIdentifier;
import cn.com.jit.ida.util.pki.asn1.x509.X509Extensions;
import cn.com.jit.ida.util.pki.cipher.JKey;

public class SubjectKeyIdentifierExt extends AbstractStandardExtension {
    private SubjectKeyIdentifier ski = null;
    private JKey subjectPublicKey = null;

    public SubjectKeyIdentifierExt(JKey subjectPublicKey2) {
        this.subjectPublicKey = subjectPublicKey2;
        this.OID = X509Extensions.SubjectKeyIdentifier.getId();
        this.critical = false;
    }

    public SubjectKeyIdentifierExt(DEROctetString obj) {
        this.ski = new SubjectKeyIdentifier(obj.getOctets());
    }

    public byte[] getSubKeyIdentifier() {
        if (this.ski != null) {
            return this.ski.getKeyIdentifier();
        }
        return null;
    }

    public String getOID() {
        return this.OID;
    }

    public void setCritical(boolean critical) {
        this.critical = critical;
    }

    public boolean getCritical() {
        return this.critical;
    }

    public JKey getSubjectPublicKey() {
        return this.subjectPublicKey;
    }

    public byte[] encode() throws PKIException {
        try {
            return new DEROctetString(new SubjectKeyIdentifier(Parser.key2SPKI(this.subjectPublicKey)).getDERObject()).getOctets();
        } catch (PKIException ex) {
            throw ex;
        }
    }
}
