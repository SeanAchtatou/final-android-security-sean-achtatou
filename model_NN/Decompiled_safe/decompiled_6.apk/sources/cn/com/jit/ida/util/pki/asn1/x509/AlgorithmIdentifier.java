package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.ASN1Encodable;
import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DERSequence;

public class AlgorithmIdentifier extends ASN1Encodable {
    private DERObjectIdentifier objectId;
    private DEREncodable parameters;
    private boolean parametersDefined;

    public static AlgorithmIdentifier getInstance(ASN1TaggedObject obj, boolean explicit) {
        return getInstance(ASN1Sequence.getInstance(obj, explicit));
    }

    public static AlgorithmIdentifier getInstance(Object obj) {
        if (obj instanceof AlgorithmIdentifier) {
            return (AlgorithmIdentifier) obj;
        }
        if (obj instanceof DERObjectIdentifier) {
            return new AlgorithmIdentifier((DERObjectIdentifier) obj);
        }
        if (obj instanceof String) {
            return new AlgorithmIdentifier((String) obj);
        }
        if (obj instanceof ASN1Sequence) {
            return new AlgorithmIdentifier((ASN1Sequence) obj);
        }
        throw new IllegalArgumentException("unknown object in factory");
    }

    public AlgorithmIdentifier(DERObjectIdentifier objectId2) {
        this.parametersDefined = false;
        this.objectId = objectId2;
    }

    public AlgorithmIdentifier(String objectId2) {
        this.parametersDefined = false;
        this.objectId = new DERObjectIdentifier(objectId2);
    }

    public AlgorithmIdentifier(DERObjectIdentifier objectId2, DEREncodable parameters2) {
        this.parametersDefined = false;
        this.parametersDefined = true;
        this.objectId = objectId2;
        this.parameters = parameters2;
    }

    public AlgorithmIdentifier(ASN1Sequence seq) {
        this.parametersDefined = false;
        this.objectId = (DERObjectIdentifier) seq.getObjectAt(0);
        if (seq.size() == 2) {
            this.parametersDefined = true;
            this.parameters = seq.getObjectAt(1);
            return;
        }
        this.parameters = null;
    }

    public DERObjectIdentifier getObjectId() {
        return this.objectId;
    }

    public DEREncodable getParameters() {
        return this.parameters;
    }

    public DERObject toASN1Object() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(this.objectId);
        if (this.parametersDefined) {
            v.add(this.parameters);
        }
        return new DERSequence(v);
    }
}
