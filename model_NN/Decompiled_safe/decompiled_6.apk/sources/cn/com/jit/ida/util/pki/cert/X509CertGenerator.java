package cn.com.jit.ida.util.pki.cert;

import cn.com.jit.ida.util.pki.PKIConstant;
import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.Parser;
import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.DERBitString;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DERNull;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.x509.AlgorithmIdentifier;
import cn.com.jit.ida.util.pki.asn1.x509.TBSCertificateStructure;
import cn.com.jit.ida.util.pki.asn1.x509.Time;
import cn.com.jit.ida.util.pki.asn1.x509.V3TBSCertificateGenerator;
import cn.com.jit.ida.util.pki.asn1.x509.X509Extension;
import cn.com.jit.ida.util.pki.asn1.x509.X509Extensions;
import cn.com.jit.ida.util.pki.asn1.x509.X509Name;
import cn.com.jit.ida.util.pki.cipher.JKey;
import cn.com.jit.ida.util.pki.cipher.Mechanism;
import cn.com.jit.ida.util.pki.cipher.Session;
import cn.com.jit.ida.util.pki.extension.Extension;
import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Vector;

public class X509CertGenerator {
    private String deviceName;
    private HashMap dnrules;
    private Hashtable extensionSet;
    private String issuer;
    private Mechanism mechanism;
    private Date notAfter;
    private Date notBefore;
    private JKey pubKey;
    private BigInteger serialNumber;
    private AlgorithmIdentifier sigAlg;
    private DERBitString signature;
    private String subject;
    private TBSCertificateStructure tbsCert;
    private V3TBSCertificateGenerator tbsCertGen;

    public X509CertGenerator() {
        this.mechanism = null;
        this.deviceName = null;
        this.tbsCertGen = null;
        this.sigAlg = null;
        this.subject = null;
        this.issuer = null;
        this.serialNumber = null;
        this.notBefore = null;
        this.notAfter = null;
        this.pubKey = null;
        this.signature = null;
        this.tbsCert = null;
        this.extensionSet = null;
        this.dnrules = null;
        this.tbsCertGen = new V3TBSCertificateGenerator();
        this.extensionSet = new Hashtable();
    }

    public void setSerialNumber(String strSerialNumber) throws PKIException {
        if (strSerialNumber == null) {
            throw new PKIException(PKIException.SN_NULL, PKIException.SN_NULL_DES);
        }
        this.serialNumber = new BigInteger(strSerialNumber, 16);
        this.tbsCertGen.setSerialNumber(new DERInteger(this.serialNumber));
    }

    public void setSerialNumber(BigInteger serialNumber2) throws PKIException {
        if (serialNumber2 == null) {
            throw new PKIException(PKIException.SN_NULL, PKIException.SN_NULL_DES);
        }
        this.serialNumber = serialNumber2;
        this.tbsCertGen.setSerialNumber(new DERInteger(serialNumber2));
    }

    public void setSubject(String subject2) throws PKIException {
        if (subject2 == null || subject2.trim().length() == 0) {
            throw new PKIException(PKIException.SUBJECT_NULL, PKIException.SUBJECT_NULL_DES);
        }
        this.subject = subject2;
        X509Name x509Sub = new X509Name(subject2);
        if (this.dnrules != null) {
            x509Sub.setRules(this.dnrules);
        }
        this.tbsCertGen.setSubject(x509Sub);
    }

    public void setSubject(X509Name x509SubjectName) throws PKIException {
        if (x509SubjectName == null) {
            throw new PKIException(PKIException.SUBJECT_NULL, PKIException.SUBJECT_NULL_DES);
        }
        this.subject = x509SubjectName.toString();
        if (this.subject.trim().length() == 0) {
            throw new PKIException(PKIException.SUBJECT_NULL, PKIException.SUBJECT_NULL_DES);
        }
        this.tbsCertGen.setSubject(x509SubjectName);
    }

    public void setIssuer(String issuer2) throws PKIException {
        if (issuer2 == null || issuer2.trim().length() == 0) {
            throw new PKIException(PKIException.ISSUER_NULL, PKIException.ISSUER_NULL_DES);
        }
        this.issuer = issuer2;
        X509Name x509Issuer = new X509Name(issuer2);
        if (this.dnrules != null) {
            x509Issuer.setRules(this.dnrules);
        }
        this.tbsCertGen.setIssuer(x509Issuer);
    }

    public void setIssuer(X509Name x509IssuerName) throws PKIException {
        if (x509IssuerName == null) {
            throw new PKIException(PKIException.ISSUER_NULL, PKIException.ISSUER_NULL_DES);
        }
        this.issuer = x509IssuerName.toString();
        if (this.issuer.trim().length() == 0) {
            throw new PKIException(PKIException.ISSUER_NULL, PKIException.ISSUER_NULL_DES);
        }
        this.tbsCertGen.setIssuer(x509IssuerName);
    }

    public void setNotBefore(Date notBefore2) throws PKIException {
        if (notBefore2 == null) {
            throw new PKIException(PKIException.NOT_BEFORE_NULL, PKIException.NOT_BEFORE_NULL_DES);
        }
        this.notBefore = notBefore2;
        this.tbsCertGen.setStartDate(new Time(notBefore2));
    }

    public void setNotAfter(Date notAfter2) throws PKIException {
        if (notAfter2 == null) {
            throw new PKIException(PKIException.NOT_AFTER_NULL, PKIException.NOT_AFTER_NULL_DES);
        }
        this.notAfter = notAfter2;
        this.tbsCertGen.setEndDate(new Time(notAfter2));
    }

    public void setPublicKey(JKey pubKey2) throws PKIException {
        if (pubKey2 == null) {
            throw new PKIException(PKIException.PUB_KEY_NULL, PKIException.PUB_KEY_NULL_DES);
        }
        this.pubKey = pubKey2;
        try {
            this.tbsCertGen.setSubjectPublicKeyInfo(Parser.key2SPKI(pubKey2));
        } catch (Exception ex) {
            throw new PKIException("8134", PKIException.KEY_SPKI_DES, ex);
        }
    }

    public void setSignatureAlg(String signatureAlgorithm) throws PKIException {
        DERObjectIdentifier oid;
        if (signatureAlgorithm == null) {
            throw new PKIException(PKIException.SIG_ALG_NULL, PKIException.SIG_ALG_NULL_DES);
        }
        if (signatureAlgorithm.equals("MD2withRSAEncryption")) {
            this.mechanism = new Mechanism("MD2withRSAEncryption");
        } else if (signatureAlgorithm.equals("MD5withRSAEncryption")) {
            this.mechanism = new Mechanism("MD5withRSAEncryption");
        } else if (signatureAlgorithm.equals("SHA1withRSAEncryption")) {
            this.mechanism = new Mechanism("SHA1withRSAEncryption");
        } else if (signatureAlgorithm.endsWith("SHA1withECDSA")) {
            this.mechanism = new Mechanism("SHA1withECDSA");
        } else if (signatureAlgorithm.endsWith("SHA1withDSA")) {
            this.mechanism = new Mechanism("SHA1withDSA");
        } else if (signatureAlgorithm.equals("SHA224withRSAEncryption")) {
            this.mechanism = new Mechanism("SHA224withRSAEncryption");
        } else if (signatureAlgorithm.equals("SHA256withRSAEncryption")) {
            this.mechanism = new Mechanism("SHA256withRSAEncryption");
        } else if (signatureAlgorithm.equals("SHA384withRSAEncryption")) {
            this.mechanism = new Mechanism("SHA384withRSAEncryption");
        } else if (signatureAlgorithm.equals("SHA512withRSAEncryption")) {
            this.mechanism = new Mechanism("SHA512withRSAEncryption");
        } else if (signatureAlgorithm.endsWith("SHA224withECDSA")) {
            this.mechanism = new Mechanism("SHA224withECDSA");
        } else if (signatureAlgorithm.endsWith("SHA256withECDSA")) {
            this.mechanism = new Mechanism("SHA256withECDSA");
        } else if (signatureAlgorithm.endsWith("SM3withSM2Encryption")) {
            this.mechanism = new Mechanism("SM3withSM2Encryption");
        } else {
            throw new PKIException(PKIException.NONSUPPORT_SIGALG, "不支持的签名算法: " + signatureAlgorithm);
        }
        if (signatureAlgorithm.endsWith("SM3withSM2Encryption")) {
            oid = new DERObjectIdentifier("1.2.156.10197.1.501");
        } else {
            oid = (DERObjectIdentifier) PKIConstant.sigAlgName2OID.get(signatureAlgorithm);
        }
        this.sigAlg = new AlgorithmIdentifier(oid, new DERNull());
        this.tbsCertGen.setSignature(this.sigAlg);
    }

    public void setIssuerUniqueID(byte[] issuerUniqueID) {
        if (issuerUniqueID != null) {
            this.tbsCertGen.setIssuerUniqueID(new DERBitString(issuerUniqueID));
        }
    }

    public void setSubjectUniqueID(byte[] subjectUniqueID) {
        if (subjectUniqueID != null) {
            this.tbsCertGen.setSubjectUniqueID(new DERBitString(subjectUniqueID));
        }
    }

    public void addExtensiond(Extension extension) throws PKIException {
        DERObjectIdentifier derOID = new DERObjectIdentifier(extension.getOID());
        boolean critical = extension.getCritical();
        try {
            this.extensionSet.put(derOID, new X509Extension(extension.getCritical(), new DEROctetString(extension.encode())));
        } catch (PKIException ex) {
            throw new PKIException(PKIException.EXTENSION_ENCODE, PKIException.EXTENSION_ENCODE_DES, ex);
        }
    }

    public void setExtensiond(Vector extensions) throws PKIException {
        int size = extensions.size();
        int i = 0;
        while (i < size) {
            Extension extension = (Extension) extensions.get(i);
            DERObjectIdentifier derOID = new DERObjectIdentifier(extension.getOID());
            boolean critical = extension.getCritical();
            try {
                this.extensionSet.put(derOID, new X509Extension(extension.getCritical(), new DEROctetString(extension.encode())));
                i++;
            } catch (PKIException ex) {
                throw new PKIException(PKIException.EXTENSION_ENCODE, PKIException.EXTENSION_ENCODE_DES, ex);
            }
        }
    }

    public void addExtension(String oid, boolean critical, byte[] derValue) throws PKIException {
        this.extensionSet.put(new DERObjectIdentifier(oid), new X509Extension(critical, new DEROctetString(derValue)));
    }

    public byte[] generateX509Cert(JKey priKey, Session session) throws PKIException {
        if (this.issuer == null || this.issuer.trim().length() == 0) {
            throw new PKIException(PKIException.ISSUER_NULL, PKIException.ISSUER_NULL_DES);
        } else if (this.subject == null || this.subject.trim().length() == 0) {
            throw new PKIException(PKIException.SUBJECT_NULL, PKIException.SUBJECT_NULL_DES);
        } else if (this.pubKey == null) {
            throw new PKIException(PKIException.PUB_KEY_NULL, PKIException.PUB_KEY_NULL_DES);
        } else if (this.sigAlg == null) {
            throw new PKIException(PKIException.SIG_ALG_NULL, PKIException.SIG_ALG_NULL_DES);
        } else if (this.serialNumber == null) {
            throw new PKIException(PKIException.SN_NULL, PKIException.SN_NULL_DES);
        } else if (this.notBefore == null) {
            throw new PKIException(PKIException.NOT_BEFORE_NULL, PKIException.NOT_BEFORE_NULL_DES);
        } else if (this.notAfter == null) {
            throw new PKIException(PKIException.NOT_AFTER_NULL, PKIException.NOT_AFTER_NULL_DES);
        } else {
            generateSignature(priKey, session);
            return constructCertificate();
        }
    }

    private void generateSignature(JKey priKey, Session session) throws PKIException {
        if (this.extensionSet.size() > 0) {
            this.tbsCertGen.setExtensions(new X509Extensions(this.extensionSet));
        }
        this.tbsCert = this.tbsCertGen.generateTBSCertificate();
        try {
            try {
                this.signature = new DERBitString(session.sign(this.mechanism, priKey, Parser.writeDERObj2Bytes(this.tbsCert.getDERObject())));
            } catch (Exception ex) {
                throw new PKIException("5", PKIException.SIGN_DES, ex);
            }
        } catch (Exception ex2) {
            throw new PKIException(PKIException.TBSCERT_BYTES, PKIException.TBSCERT_BYTES_DES, ex2);
        }
    }

    private byte[] constructCertificate() throws PKIException {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(this.tbsCert);
        v.add(this.sigAlg);
        v.add(this.signature);
        try {
            return Parser.writeDERObj2Bytes(new DERSequence(v).getDERObject());
        } catch (Exception ex) {
            throw new PKIException(PKIException.CERT_BYTES, PKIException.CERT_BYTES_DES, ex);
        }
    }

    public void SetDnRules(Map rules) {
        this.dnrules = (HashMap) rules;
    }
}
