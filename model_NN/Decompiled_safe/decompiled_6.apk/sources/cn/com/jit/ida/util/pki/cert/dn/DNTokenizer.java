package cn.com.jit.ida.util.pki.cert.dn;

public class DNTokenizer {
    private StringBuffer buf = new StringBuffer();
    private int index;
    private String oid;

    public DNTokenizer(String oid2) {
        this.oid = oid2;
        this.index = -1;
    }

    public boolean hasMoreTokens() {
        return this.index != this.oid.length();
    }

    public String nextToken() {
        char c;
        if (this.index == this.oid.length()) {
            return null;
        }
        int end = this.index + 1;
        this.buf.setLength(0);
        while (end != this.oid.length() && (c = this.oid.charAt(end)) != ',') {
            if (c == '\\') {
                char c1 = this.oid.charAt(end + 1);
                if (c1 == ',') {
                    this.buf.append(c1);
                    end++;
                } else {
                    this.buf.append(c);
                }
            } else {
                this.buf.append(c);
            }
            end++;
        }
        this.index = end;
        return this.buf.toString().trim();
    }
}
