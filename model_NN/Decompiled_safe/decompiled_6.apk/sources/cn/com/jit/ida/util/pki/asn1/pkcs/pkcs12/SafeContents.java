package cn.com.jit.ida.util.pki.asn1.pkcs.pkcs12;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.BERSequence;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERObject;

public class SafeContents implements DEREncodable {
    private SafeBag[] safeBag;

    public static SafeContents getInstance(Object o) {
        if (o == null || (o instanceof SafeContents)) {
            return (SafeContents) o;
        }
        if (o instanceof ASN1Sequence) {
            return new SafeContents((ASN1Sequence) o);
        }
        throw new IllegalArgumentException("unknown object in factory " + o.getClass().getName());
    }

    public SafeContents(SafeBag[] _safeBag) {
        this.safeBag = _safeBag;
    }

    public SafeContents(ASN1Sequence seq) {
        this.safeBag = new SafeBag[seq.size()];
        for (int i = 0; i < this.safeBag.length; i++) {
            this.safeBag[i] = SafeBag.getInstance(seq.getObjectAt(i));
        }
    }

    public SafeBag[] getSafeBag() {
        return this.safeBag;
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        for (SafeBag add : this.safeBag) {
            v.add(add);
        }
        return new BERSequence(v);
    }
}
