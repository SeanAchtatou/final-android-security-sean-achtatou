package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.ASN1OctetString;
import cn.com.jit.ida.util.pki.asn1.DERBoolean;

public class X509Extension {
    boolean critical;
    ASN1OctetString value;

    public X509Extension(DERBoolean critical2, ASN1OctetString value2) {
        this.critical = critical2.isTrue();
        this.value = value2;
    }

    public X509Extension(boolean critical2, ASN1OctetString value2) {
        this.critical = critical2;
        this.value = value2;
    }

    public boolean isCritical() {
        return this.critical;
    }

    public ASN1OctetString getValue() {
        return this.value;
    }

    public int hashCode() {
        if (isCritical()) {
            return getValue().hashCode();
        }
        return getValue().hashCode() ^ -1;
    }

    public boolean equals(Object o) {
        if (o == null || !(o instanceof X509Extension)) {
            return false;
        }
        X509Extension other = (X509Extension) o;
        if (!other.getValue().equals(getValue()) || other.isCritical() != isCritical()) {
            return false;
        }
        return true;
    }
}
