package cn.com.jit.ida.util.pki.extension;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.x509.NetscapeCertType;
import cn.com.jit.ida.util.pki.asn1.x509.X509Extensions;
import java.util.Vector;

public class NetscapeCertTypeExt extends AbstractStandardExtension {
    public static final String OBJECT_SIGNING = "objectSigning";
    public static final String OBJECT_SIGNING_CA = "objectSigningCA";
    public static final String SMIME = "smime";
    public static final String SMIME_CA = "smimeCA";
    public static final String SSL_CA = "sslCA";
    public static final String SSL_CLIENT = "sslClient";
    public static final String SSL_SERVER = "sslServer";
    private int certType = 0;
    private Vector certTypeVector = null;

    public NetscapeCertTypeExt() {
        this.OID = X509Extensions.NetscapeCertType.getId();
        this.critical = false;
        this.certType = 0;
        this.certTypeVector = new Vector();
    }

    public String getOID() {
        return this.OID;
    }

    public void setCritical(boolean critical) {
        this.critical = critical;
    }

    public boolean getCritical() {
        return this.critical;
    }

    public void addCertType(String certType2) {
        this.certTypeVector.add(certType2);
    }

    public void setCertType(Vector certTypes) {
        for (int i = 0; i < certTypes.size(); i++) {
            this.certTypeVector.add((String) certTypes.get(i));
        }
    }

    public byte[] encode() throws PKIException {
        if (this.certTypeVector.contains(SSL_CLIENT)) {
            this.certType |= 128;
        }
        if (this.certTypeVector.contains(SSL_SERVER)) {
            this.certType |= 64;
        }
        if (this.certTypeVector.contains(SMIME)) {
            this.certType |= 32;
        }
        if (this.certTypeVector.contains(OBJECT_SIGNING)) {
            this.certType |= 16;
        }
        if (this.certTypeVector.contains(SSL_CA)) {
            this.certType |= 4;
        }
        if (this.certTypeVector.contains(SMIME_CA)) {
            this.certType |= 2;
        }
        if (this.certTypeVector.contains(OBJECT_SIGNING_CA)) {
            this.certType |= 1;
        }
        return new DEROctetString(new NetscapeCertType(this.certType).getDERObject()).getOctets();
    }
}
