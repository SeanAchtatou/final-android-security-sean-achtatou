package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DERBitString;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import java.math.BigInteger;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

public class AttributeCertificateInfo implements DEREncodable {
    DERInteger attCertVersion;
    AttCertValidityPeriod attrCertValidityPeriod;
    Attributes attributes;
    X509Extensions extensions;
    Holder holder;
    AttCertIssuer issuer;
    DERBitString issuerUniqueID;
    DERInteger serialNumber;
    AlgorithmIdentifier signature;

    public AttributeCertificateInfo(ASN1Sequence seq) {
        this.attCertVersion = DERInteger.getInstance(seq.getObjectAt(0));
        this.holder = Holder.getInstance(seq.getObjectAt(1));
        this.issuer = AttCertIssuer.getInstance(seq.getObjectAt(2));
        this.signature = AlgorithmIdentifier.getInstance(seq.getObjectAt(3));
        this.serialNumber = DERInteger.getInstance(seq.getObjectAt(4));
        this.attrCertValidityPeriod = AttCertValidityPeriod.getInstance(seq.getObjectAt(5));
        this.attributes = Attributes.getInstance(seq.getObjectAt(6));
        for (int i = 7; i < seq.size(); i++) {
            DEREncodable obj = seq.getObjectAt(i);
            if (obj instanceof DERBitString) {
                this.issuerUniqueID = DERBitString.getInstance(seq.getObjectAt(i));
            } else if ((obj instanceof ASN1Sequence) || (obj instanceof X509Extensions)) {
                this.extensions = X509Extensions.getInstance(seq.getObjectAt(i));
            }
        }
    }

    public AttributeCertificateInfo(Element node) throws PKIException {
        NodeList nodelist = node.getChildNodes();
        if (nodelist.getLength() == 0) {
            throw new PKIException("XML内容缺失,AttributeCertificateInfo.AttributeCertificateInfo()");
        }
        for (int i = 0; i < nodelist.getLength(); i++) {
            Node cnode = nodelist.item(i);
            String sname = cnode.getNodeName();
            if (sname.equals("Version")) {
                try {
                    this.attCertVersion = new DERInteger(Integer.parseInt(((Text) cnode.getFirstChild()).getData().trim()));
                } catch (NumberFormatException e) {
                    throw new PKIException("NumberFormatException,AttributeCertificateInfo.AttributeCertificateInfo()", e);
                }
            }
            if (sname.equals("Holder")) {
                this.holder = new Holder(cnode);
            }
            if (sname.equals("Issuer")) {
                this.issuer = new AttCertIssuer(cnode);
            }
            if (sname.equals("Signature")) {
                NodeList nlsign = cnode.getChildNodes();
                if (nodelist.getLength() == 0) {
                    throw new PKIException("XML内容缺失,AttributeCertificateInfo.AttributeCertificateInfo");
                }
                DERObjectIdentifier oid = null;
                String nameoid = null;
                for (int j = 0; j < nlsign.getLength(); j++) {
                    Node ndSig = nlsign.item(j);
                    String sndsig = ndSig.getNodeName();
                    oid = sndsig.equals("OID") ? new DERObjectIdentifier(((Text) ndSig.getFirstChild()).getData().trim()) : oid;
                    if (sndsig.equals("name")) {
                        nameoid = ((Text) ndSig.getFirstChild()).getData().trim();
                    }
                }
                this.signature = new AlgorithmIdentifier(oid, new DEROctetString(nameoid.getBytes()));
            }
            if (sname.equals("serialNumber")) {
                try {
                    this.serialNumber = new DERInteger(new BigInteger(((Text) cnode.getFirstChild()).getData().trim(), 16));
                } catch (NumberFormatException e2) {
                    throw new PKIException("NumberFormatException,AttributeCertificateInfo.AttributeCertificateInfo()", e2);
                }
            }
            if (sname.equals("AttrCertValidityPeriod")) {
                this.attrCertValidityPeriod = new AttCertValidityPeriod(cnode);
            }
            if (sname.equals("Attributes")) {
                this.attributes = new Attributes(cnode);
            }
            if (sname.equals("issuerUniqueID")) {
                this.issuerUniqueID = new DERBitString(((Text) cnode.getFirstChild()).getData().trim().getBytes());
            }
            if (sname.equals("Extensions")) {
                this.extensions = new X509Extensions(cnode);
            }
        }
    }

    public DERInteger getAttCertVersion() {
        return this.attCertVersion;
    }

    public Holder getHolder() {
        return this.holder;
    }

    public AttCertIssuer getIssuer() {
        return this.issuer;
    }

    public AlgorithmIdentifier getSignature() {
        return this.signature;
    }

    public DERInteger getSerialNumber() {
        return this.serialNumber;
    }

    public AttCertValidityPeriod getAttrCertValidityPeriod() {
        return this.attrCertValidityPeriod;
    }

    public Attributes getAttributes() {
        return this.attributes;
    }

    public DERBitString getIssuerUniqueID() {
        return this.issuerUniqueID;
    }

    public X509Extensions getExtensions() {
        return this.extensions;
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(this.attCertVersion);
        v.add(this.holder);
        v.add(this.issuer);
        v.add(this.signature);
        v.add(this.serialNumber);
        v.add(this.attrCertValidityPeriod);
        v.add(this.attributes.getDERObject());
        if (this.issuerUniqueID != null) {
            v.add(this.issuerUniqueID);
        }
        if (this.extensions != null) {
            v.add(this.extensions);
        }
        return new DERSequence(v);
    }
}
