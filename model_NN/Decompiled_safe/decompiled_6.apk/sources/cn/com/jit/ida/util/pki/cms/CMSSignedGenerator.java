package cn.com.jit.ida.util.pki.cms;

import cn.com.jit.ida.util.pki.cipher.Mechanism;
import java.io.IOException;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.CertStore;
import java.security.cert.CertStoreException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.security.interfaces.DSAPrivateKey;
import java.security.interfaces.RSAPrivateKey;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.asn1.cms.CMSObjectIdentifiers;
import org.bouncycastle.asn1.cms.IssuerAndSerialNumber;
import org.bouncycastle.asn1.cms.SignerIdentifier;
import org.bouncycastle.asn1.cryptopro.CryptoProObjectIdentifiers;
import org.bouncycastle.asn1.nist.NISTObjectIdentifiers;
import org.bouncycastle.asn1.oiw.OIWObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.teletrust.TeleTrusTObjectIdentifiers;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.AttributeCertificate;
import org.bouncycastle.asn1.x509.TBSCertificateStructure;
import org.bouncycastle.asn1.x9.X9ObjectIdentifiers;
import org.bouncycastle.cms.CMSAttributeTableGenerator;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.SignerInformationStore;
import org.bouncycastle.jce.interfaces.GOST3410PrivateKey;
import org.bouncycastle.util.Selector;
import org.bouncycastle.x509.X509AttributeCertificate;
import org.bouncycastle.x509.X509Store;

public class CMSSignedGenerator {
    public static final String DATA = CMSObjectIdentifiers.data.getId();
    public static final String DIGEST_GOST3411 = CryptoProObjectIdentifiers.gostR3411.getId();
    public static final String DIGEST_MD5 = PKCSObjectIdentifiers.md5.getId();
    public static final String DIGEST_RIPEMD128 = TeleTrusTObjectIdentifiers.ripemd128.getId();
    public static final String DIGEST_RIPEMD160 = TeleTrusTObjectIdentifiers.ripemd160.getId();
    public static final String DIGEST_RIPEMD256 = TeleTrusTObjectIdentifiers.ripemd256.getId();
    public static final String DIGEST_SHA1 = OIWObjectIdentifiers.idSHA1.getId();
    public static final String DIGEST_SHA224 = NISTObjectIdentifiers.id_sha224.getId();
    public static final String DIGEST_SHA256 = NISTObjectIdentifiers.id_sha256.getId();
    public static final String DIGEST_SHA384 = NISTObjectIdentifiers.id_sha384.getId();
    public static final String DIGEST_SHA512 = NISTObjectIdentifiers.id_sha512.getId();
    private static final Map EC_ALGORITHMS = new HashMap();
    public static final String ENCRYPTION_DSA = X9ObjectIdentifiers.id_dsa_with_sha1.getId();
    public static final String ENCRYPTION_ECDSA = X9ObjectIdentifiers.ecdsa_with_SHA1.getId();
    private static final String ENCRYPTION_ECDSA_WITH_SHA1 = X9ObjectIdentifiers.ecdsa_with_SHA1.getId();
    private static final String ENCRYPTION_ECDSA_WITH_SHA224 = X9ObjectIdentifiers.ecdsa_with_SHA224.getId();
    private static final String ENCRYPTION_ECDSA_WITH_SHA256 = X9ObjectIdentifiers.ecdsa_with_SHA256.getId();
    private static final String ENCRYPTION_ECDSA_WITH_SHA384 = X9ObjectIdentifiers.ecdsa_with_SHA384.getId();
    private static final String ENCRYPTION_ECDSA_WITH_SHA512 = X9ObjectIdentifiers.ecdsa_with_SHA512.getId();
    public static final String ENCRYPTION_ECGOST3410 = CryptoProObjectIdentifiers.gostR3410_2001.getId();
    public static final String ENCRYPTION_GOST3410 = CryptoProObjectIdentifiers.gostR3410_94.getId();
    public static final String ENCRYPTION_RSA = PKCSObjectIdentifiers.rsaEncryption.getId();
    public static final String ENCRYPTION_RSA_PSS = PKCSObjectIdentifiers.id_RSASSA_PSS.getId();
    private static final Set NO_PARAMS = new HashSet();
    protected List _certs;
    protected List _crls;
    protected Map _digests;
    protected List _signers;
    protected final SecureRandom rand;

    static {
        NO_PARAMS.add(ENCRYPTION_DSA);
        NO_PARAMS.add(ENCRYPTION_ECDSA);
        NO_PARAMS.add(ENCRYPTION_ECDSA_WITH_SHA1);
        NO_PARAMS.add(ENCRYPTION_ECDSA_WITH_SHA224);
        NO_PARAMS.add(ENCRYPTION_ECDSA_WITH_SHA256);
        NO_PARAMS.add(ENCRYPTION_ECDSA_WITH_SHA384);
        NO_PARAMS.add(ENCRYPTION_ECDSA_WITH_SHA512);
        EC_ALGORITHMS.put(DIGEST_SHA1, ENCRYPTION_ECDSA_WITH_SHA1);
        EC_ALGORITHMS.put(DIGEST_SHA224, ENCRYPTION_ECDSA_WITH_SHA224);
        EC_ALGORITHMS.put(DIGEST_SHA256, ENCRYPTION_ECDSA_WITH_SHA256);
        EC_ALGORITHMS.put(DIGEST_SHA384, ENCRYPTION_ECDSA_WITH_SHA384);
        EC_ALGORITHMS.put(DIGEST_SHA512, ENCRYPTION_ECDSA_WITH_SHA512);
    }

    protected CMSSignedGenerator() {
        this(new SecureRandom());
    }

    protected CMSSignedGenerator(SecureRandom rand2) {
        this._certs = new ArrayList();
        this._crls = new ArrayList();
        this._signers = new ArrayList();
        this._digests = new HashMap();
        this.rand = rand2;
    }

    /* access modifiers changed from: protected */
    public String getEncOID(PrivateKey key, String digestOID) {
        if ((key instanceof RSAPrivateKey) || Mechanism.RSA.equalsIgnoreCase(key.getAlgorithm())) {
            return ENCRYPTION_RSA;
        }
        if ((key instanceof DSAPrivateKey) || Mechanism.DSA.equalsIgnoreCase(key.getAlgorithm())) {
            String encOID = ENCRYPTION_DSA;
            if (digestOID.equals(DIGEST_SHA1)) {
                return encOID;
            }
            throw new IllegalArgumentException("can't mix DSA with anything but SHA1");
        } else if (Mechanism.ECDSA.equalsIgnoreCase(key.getAlgorithm()) || "EC".equalsIgnoreCase(key.getAlgorithm())) {
            String encOID2 = (String) EC_ALGORITHMS.get(digestOID);
            if (encOID2 != null) {
                return encOID2;
            }
            throw new IllegalArgumentException("can't mix ECDSA with anything but SHA family digests");
        } else if ((key instanceof GOST3410PrivateKey) || "GOST3410".equalsIgnoreCase(key.getAlgorithm())) {
            return ENCRYPTION_GOST3410;
        } else {
            if ("ECGOST3410".equalsIgnoreCase(key.getAlgorithm())) {
                return ENCRYPTION_ECGOST3410;
            }
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public AlgorithmIdentifier getEncAlgorithmIdentifier(String encOid, Signature sig) throws IOException {
        if (NO_PARAMS.contains(encOid)) {
            return new AlgorithmIdentifier(new DERObjectIdentifier(encOid));
        }
        if (!encOid.equals(ENCRYPTION_RSA_PSS)) {
            return new AlgorithmIdentifier(new DERObjectIdentifier(encOid), new DERNull());
        }
        return new AlgorithmIdentifier(new DERObjectIdentifier(encOid), ASN1Object.fromByteArray(sig.getParameters().getEncoded()));
    }

    /* access modifiers changed from: protected */
    public Map getBaseParameters(DERObjectIdentifier contentType, AlgorithmIdentifier digAlgId, byte[] hash) {
        Map param = new HashMap();
        param.put(CMSAttributeTableGenerator.CONTENT_TYPE, contentType);
        param.put(CMSAttributeTableGenerator.DIGEST_ALGORITHM_IDENTIFIER, digAlgId);
        param.put(CMSAttributeTableGenerator.DIGEST, hash.clone());
        return param;
    }

    /* access modifiers changed from: protected */
    public ASN1Set getAttributeSet(AttributeTable attr) {
        if (attr != null) {
            return new DERSet(attr.toASN1EncodableVector());
        }
        return null;
    }

    public void addCertificatesAndCRLs(CertStore certStore) throws CertStoreException, CMSException {
        this._certs.addAll(CMSUtils.getCertificatesFromStore(certStore));
        this._crls.addAll(CMSUtils.getCRLsFromStore(certStore));
    }

    public void addAttributeCertificates(X509Store store) throws CMSException {
        try {
            for (X509AttributeCertificate attrCert : store.getMatches((Selector) null)) {
                this._certs.add(new DERTaggedObject(false, 2, AttributeCertificate.getInstance(ASN1Object.fromByteArray(attrCert.getEncoded()))));
            }
        } catch (IllegalArgumentException e) {
            throw new CMSException("error processing attribute certs", e);
        } catch (IOException e2) {
            throw new CMSException("error processing attribute certs", e2);
        }
    }

    public void addSigners(SignerInformationStore signerStore) {
        for (Object add : signerStore.getSigners()) {
            this._signers.add(add);
        }
    }

    public Map getGeneratedDigests() {
        return new HashMap(this._digests);
    }

    static SignerIdentifier getSignerIdentifier(X509Certificate cert) {
        try {
            TBSCertificateStructure tbs = CMSUtils.getTBSCertificateStructure(cert);
            return new SignerIdentifier(new IssuerAndSerialNumber(tbs.getIssuer(), tbs.getSerialNumber().getValue()));
        } catch (CertificateEncodingException e) {
            throw new IllegalArgumentException("can't extract TBS structure from this cert");
        }
    }

    static SignerIdentifier getSignerIdentifier(byte[] subjectKeyIdentifier) {
        return new SignerIdentifier(new DEROctetString(subjectKeyIdentifier));
    }

    static class DigOutputStream extends OutputStream {
        MessageDigest dig;

        public DigOutputStream(MessageDigest dig2) {
            this.dig = dig2;
        }

        public void write(byte[] b, int off, int len) throws IOException {
            this.dig.update(b, off, len);
        }

        public void write(int b) throws IOException {
            this.dig.update((byte) b);
        }
    }

    static class SigOutputStream extends OutputStream {
        private final Signature sig;

        public SigOutputStream(Signature sig2) {
            this.sig = sig2;
        }

        public void write(byte[] b, int off, int len) throws IOException {
            try {
                this.sig.update(b, off, len);
            } catch (SignatureException e) {
                IOException io = new IOException("signature problem: " + e);
                io.setStackTrace(e.getStackTrace());
                throw io;
            }
        }

        public void write(int b) throws IOException {
            try {
                this.sig.update((byte) b);
            } catch (SignatureException e) {
                IOException io = new IOException("signature problem: " + e);
                io.setStackTrace(e.getStackTrace());
                throw io;
            }
        }
    }
}
