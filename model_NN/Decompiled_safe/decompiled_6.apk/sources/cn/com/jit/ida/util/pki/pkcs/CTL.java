package cn.com.jit.ida.util.pki.pkcs;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.Parser;
import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1InputStream;
import cn.com.jit.ida.util.pki.asn1.ASN1OctetString;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1Set;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DERNull;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.DEROutputStream;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.DERSet;
import cn.com.jit.ida.util.pki.asn1.DERUTCTime;
import cn.com.jit.ida.util.pki.asn1.cms.Attribute;
import cn.com.jit.ida.util.pki.asn1.cms.AttributeTable;
import cn.com.jit.ida.util.pki.asn1.cms.CMSAttributes;
import cn.com.jit.ida.util.pki.asn1.cms.IssuerAndSerialNumber;
import cn.com.jit.ida.util.pki.asn1.cms.SignerIdentifier;
import cn.com.jit.ida.util.pki.asn1.cms.SignerInfo;
import cn.com.jit.ida.util.pki.asn1.cms.Time;
import cn.com.jit.ida.util.pki.asn1.pkcs.PKCSObjectIdentifiers;
import cn.com.jit.ida.util.pki.asn1.pkcs.pkcs7.ContentInfo;
import cn.com.jit.ida.util.pki.asn1.pkcs.pkcs7.SignedData;
import cn.com.jit.ida.util.pki.asn1.x509.AlgorithmIdentifier;
import cn.com.jit.ida.util.pki.asn1.x509.CRLEntry;
import cn.com.jit.ida.util.pki.asn1.x509.SubjectKeyIdentifier;
import cn.com.jit.ida.util.pki.asn1.x509.TBSCertificateStructure;
import cn.com.jit.ida.util.pki.asn1.x509.X509CertificateStructure;
import cn.com.jit.ida.util.pki.cert.X509Cert;
import cn.com.jit.ida.util.pki.cipher.JCrypto;
import cn.com.jit.ida.util.pki.cipher.JKey;
import cn.com.jit.ida.util.pki.cipher.Mechanism;
import cn.com.jit.ida.util.pki.cipher.Session;
import cn.com.jit.ida.util.pki.crl.X509CRL;
import cn.com.jit.ida.util.pki.encoders.Base64;
import cn.com.jit.ida.util.pki.extension.ExtendedKeyUsageExt;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;

public class CTL {
    private ASN1EncodableVector certs = new ASN1EncodableVector();
    private ASN1EncodableVector crls = new ASN1EncodableVector();
    private String ctlName = null;
    private DERObject digObj = null;
    private DERSet digSet = null;
    private ExtendedKeyUsageExt keyUsg = null;
    private Session session = null;
    private ArrayList signers = new ArrayList();
    private Date validDate = null;
    private X509Cert[] xcerts = null;
    private X509CRL[] xcrls = null;

    public CTL() {
    }

    public CTL(Session sess) {
        this.session = sess;
    }

    public void AddCert(X509Cert value) throws PKIException {
        this.certs.add(Parser.convertJITCertStruct2BCCertStruct(value.getCertStructure()));
        this.xcerts = new X509Cert[this.certs.size()];
        for (int i = 0; i < this.certs.size(); i++) {
            this.xcerts[i] = new X509Cert(Parser.writeDERObj2Bytes(this.certs.get(i)));
        }
    }

    public X509Cert[] getCert() throws PKIException {
        return this.xcerts;
    }

    public void AddCRL(X509CRL value) throws PKIException {
        this.crls.add(Parser.convertJITCertList2BCCertList(value.getCertificateList()));
        this.xcrls = new X509CRL[this.crls.size()];
        for (int i = 0; i < this.crls.size(); i++) {
            this.xcrls[i] = new X509CRL(Parser.writeDERObj2Bytes(this.crls.get(i)));
        }
    }

    public X509CRL[] getCRL() throws PKIException {
        return this.xcrls;
    }

    public void SetSess(Session sess) {
        this.session = sess;
    }

    public void AddSigner(JKey privatekey, X509Cert cert, Mechanism sign_Mechanism) {
        this.signers.add(new Signer(this.session, privatekey, cert, sign_Mechanism));
    }

    public void AddSigner(JKey privatekey, X509Cert cert, Mechanism sign_Mechanism, AttributeTable sAttr, AttributeTable unsAttr) {
        this.signers.add(new Signer(this.session, privatekey, cert, sign_Mechanism, sAttr, unsAttr));
    }

    public ArrayList getSigner() throws PKIException {
        return this.signers;
    }

    public void AddDig(Mechanism dig) throws PKIException {
        String digestTypeOID;
        if (dig.getMechanismType().equals(Mechanism.MD2)) {
            digestTypeOID = "1.2.840.113549.2.2";
        } else if (dig.getMechanismType().equals(Mechanism.MD5)) {
            digestTypeOID = "1.2.840.113549.2.5";
        } else if (dig.getMechanismType().equals(Mechanism.SHA1)) {
            digestTypeOID = "1.3.14.3.2.26";
        } else if (dig.getMechanismType().equals(Mechanism.SHA256)) {
            digestTypeOID = "2.16.840.1.101.3.4.2.1";
        } else if (dig.getMechanismType().equals(Mechanism.SHA384)) {
            digestTypeOID = "2.16.840.1.101.3.4.2.2";
        } else if (dig.getMechanismType().equals(Mechanism.SHA512)) {
            digestTypeOID = "2.16.840.1.101.3.4.2.2";
        } else if (dig.getMechanismType().equals(Mechanism.SHA224)) {
            digestTypeOID = "2.16.840.1.101.3.4.2.1";
        } else {
            StringBuffer error = new StringBuffer();
            error.append(PKIException.SIGN_DES);
            error.append(" ");
            error.append(PKIException.NOT_SUP_DES);
            error.append(" ");
            error.append(dig.getMechanismType());
            throw new PKIException("8122", error.toString());
        }
        AlgorithmIdentifier digAlgId = new AlgorithmIdentifier(new DERObjectIdentifier(digestTypeOID), new DERNull());
        this.digObj = digAlgId.toASN1Object();
        this.digSet = new DERSet(digAlgId);
    }

    public Mechanism getDig() throws PKIException {
        String digstr = AlgorithmIdentifier.getInstance(this.digObj).getObjectId().getId();
        if (digstr.equals("1.2.840.113549.2.2".toString())) {
            return new Mechanism(Mechanism.MD2);
        }
        if (digstr.equals("1.2.840.113549.2.5".toString())) {
            return new Mechanism(Mechanism.MD5);
        }
        if (digstr.equals("1.3.14.3.2.26".toString())) {
            return new Mechanism(Mechanism.SHA1);
        }
        if (digstr.equals("2.16.840.1.101.3.4.2.1".toString())) {
            return new Mechanism(Mechanism.SHA256);
        }
        if (digstr.equals("2.16.840.1.101.3.4.2.2".toString())) {
            return new Mechanism(Mechanism.SHA384);
        }
        if (digstr.equals("2.16.840.1.101.3.4.2.2".toString())) {
            return new Mechanism(Mechanism.SHA512);
        }
        if (digstr.equals("2.16.840.1.101.3.4.2.1".toString())) {
            return new Mechanism(Mechanism.SHA224);
        }
        StringBuffer error = new StringBuffer();
        error.append("2");
        error.append(" ");
        error.append(PKIException.NOT_SUP_DES);
        error.append(" ");
        error.append(digstr);
        throw new PKIException("8122", error.toString());
    }

    public void AddKeyUsg(ExtendedKeyUsageExt usg) throws PKIException {
        this.keyUsg = usg;
    }

    public ExtendedKeyUsageExt getKeyUsg() throws PKIException {
        return this.keyUsg;
    }

    public void SetName(String name) throws PKIException {
        this.ctlName = name;
    }

    public String getName() throws PKIException {
        return this.ctlName;
    }

    public void SetValidDate(Date date) throws PKIException {
        this.validDate = date;
    }

    public Date getValidDate() throws PKIException {
        return this.validDate;
    }

    public void parseCTL(ContentInfo contentInfo) throws PKIException {
        if (!contentInfo.getContentType().equals(PKCSObjectIdentifiers.signedData)) {
            throw new PKIException("8172", "解析P7B证书链结构失败 证书链类型不匹配 " + contentInfo.getContentType().getId());
        }
        SignedData signedData = SignedData.getInstance(contentInfo.getContent());
        ASN1Set certSet = signedData.getCertificates();
        if (certSet != null) {
            Enumeration enumeration = certSet.getObjects();
            while (enumeration.hasMoreElements()) {
                this.certs.add(Parser.convertJITCertStruct2BCCertStruct(new X509Cert(X509CertificateStructure.getInstance(enumeration.nextElement())).getCertStructure()));
            }
            this.xcerts = new X509Cert[this.certs.size()];
            for (int i = 0; i < this.certs.size(); i++) {
                this.xcerts[i] = new X509Cert(Parser.writeDERObj2Bytes(this.certs.get(i)));
            }
        }
        ASN1Set crlSet = signedData.getCRLs();
        if (crlSet != null) {
            Enumeration enumeration2 = crlSet.getObjects();
            while (enumeration2.hasMoreElements()) {
                this.crls.add(Parser.convertJITCertList2BCCertList(new X509CRL(Parser.writeDERObj2Bytes(new CRLEntry((ASN1Sequence) enumeration2.nextElement()).getDERObject())).getCertificateList()));
            }
            this.xcrls = new X509CRL[this.crls.size()];
            for (int i2 = 0; i2 < this.crls.size(); i2++) {
                this.xcrls[i2] = new X509CRL(Parser.writeDERObj2Bytes(this.crls.get(i2)));
            }
        }
        ASN1Set signersSet = signedData.getSignerInfos();
        if (signersSet != null) {
            Enumeration enumeration3 = signersSet.getObjects();
            this.signers.clear();
            while (enumeration3.hasMoreElements()) {
                this.signers.add(enumeration3.nextElement());
            }
        }
        ContentInfo cnt = signedData.getContentInfo();
        if (cnt != null) {
            DERSequence derseq = (DERSequence) new DERSequence(cnt.getContent()).getObjectAt(0);
            if (derseq.size() > 0) {
                this.keyUsg = new ExtendedKeyUsageExt(ASN1Sequence.getInstance(derseq.getObjectAt(0)));
            }
            if (derseq.size() > 1) {
                this.ctlName = new String(DEROctetString.getInstance(derseq.getObjectAt(1)).getOctets());
            }
            if (derseq.size() > 2) {
                this.validDate = Time.getInstance(derseq.getObjectAt(2)).getDate();
            }
            if (derseq.size() > 3) {
                DERSequence seq = (DERSequence) new DERSequence(derseq.getObjectAt(3).getDERObject()).getObjectAt(0);
                this.digObj = seq.getObjectAt(0).getDERObject();
                this.digSet = new DERSet(seq.getObjectAt(0).getDERObject());
            }
            if (derseq.size() > 4) {
            }
        }
    }

    public void parseCTL(String fileName) throws PKIException {
        try {
            FileInputStream fin = new FileInputStream(fileName);
            try {
                byte[] data = new byte[fin.available()];
                fin.read(data);
                fin.close();
                parseCTL(data);
            } catch (Exception e) {
                ex = e;
                throw new PKIException("8172", PKIException.PARSE_P7B_ERR_DES, ex);
            }
        } catch (Exception e2) {
            ex = e2;
            throw new PKIException("8172", PKIException.PARSE_P7B_ERR_DES, ex);
        }
    }

    private void parseCTL(InputStream ins) throws PKIException {
        try {
            byte[] content = new byte[ins.available()];
            ins.read(content);
            ins.close();
            parseCTL(content);
        } catch (Exception ex) {
            throw new PKIException("8172", PKIException.PARSE_P7B_ERR_DES, ex);
        }
    }

    public void parseCTL(byte[] data) throws PKIException {
        if (Parser.isBase64Encode(data)) {
            data = Base64.decode(Parser.convertBase64(data));
        }
        if (data[0] != 48) {
            throw new PKIException("8172", PKIException.PARSE_P7B_ERR_DES, new Exception("The P7B certification chain content error."));
        }
        try {
            parseCTL(ContentInfo.getInstance((ASN1Sequence) Parser.writeBytes2DERObj(data)));
        } catch (Exception ex) {
            throw new PKIException("8172", PKIException.PARSE_P7B_ERR_DES, ex);
        }
    }

    public ContentInfo generateCTL() throws PKIException, IOException {
        new DERSet();
        if (this.certs == null || this.signers == null) {
            return null;
        }
        DERSet certificates = new DERSet(this.certs);
        DERSet certrevlist = null;
        if (!(this.crls == null || this.crls.size() == 0)) {
            certrevlist = new DERSet(this.crls);
        }
        ASN1EncodableVector signerInfos = new ASN1EncodableVector();
        ASN1EncodableVector digestAlgs = new ASN1EncodableVector();
        Iterator it = this.signers.iterator();
        DERObjectIdentifier contentTypeOID = new DERObjectIdentifier(PKCSObjectIdentifiers.data.getId());
        while (it.hasNext()) {
            Signer signer = (Signer) it.next();
            digestAlgs.add(new AlgorithmIdentifier(new DERObjectIdentifier(signer.GetDigestTypeOID()), new DERNull()));
            signerInfos.add(signer.toSignerInfo(contentTypeOID, "hello word".getBytes(), true, false));
        }
        ASN1EncodableVector cnt = new ASN1EncodableVector();
        if (this.keyUsg != null) {
            cnt.add(Parser.writeBytes2DERObj(this.keyUsg.encode()));
        }
        if (this.ctlName != null) {
            cnt.add(new DEROctetString(this.ctlName.getBytes()));
        }
        if (this.validDate != null) {
            cnt.add(Parser.writeBytes2DERObj(new Time(this.validDate).getEncoded()));
        }
        if (this.digObj != null) {
            cnt.add(this.digObj);
        }
        ASN1EncodableVector digvec = new ASN1EncodableVector();
        Mechanism mech = getDig();
        if (this.xcerts != null) {
            for (int i = 0; i < this.xcerts.length; i++) {
                digvec.add(new DERSequence(new DEROctetString(this.session.digest(mech, this.xcerts[i].getEncoded()))).getDERObject());
            }
        }
        if (this.xcrls != null) {
            for (int i2 = 0; i2 < this.xcrls.length; i2++) {
                digvec.add(new DERSequence(new DEROctetString(this.session.digest(mech, this.xcrls[i2].getEncoded()))).getDERObject());
            }
        }
        if (!(this.xcerts == null && this.xcrls == null)) {
            cnt.add(new DERSequence(digvec));
        }
        return new ContentInfo(PKCSObjectIdentifiers.signedData, new SignedData(new DERInteger(1), this.digSet, new ContentInfo(PKCSObjectIdentifiers.CTLContentData, new DERSequence(cnt)), certificates, certrevlist, new DERSet(signerInfos)));
    }

    public void generateCTLFile(String fileName) throws PKIException, IOException {
        ContentInfo contentInfo = generateCTL();
        try {
            FileOutputStream fos = new FileOutputStream(fileName);
            DEROutputStream dos = new DEROutputStream(fos);
            dos.writeObject(contentInfo.getDERObject());
            dos.close();
            fos.close();
        } catch (Exception ex) {
            throw new PKIException("8171", PKIException.GEN_P7B_ERR_DES, ex);
        }
    }

    public byte[] generateCTLData_DER() throws PKIException, IOException {
        return Parser.writeDERObj2Bytes(generateCTL().getDERObject());
    }

    public byte[] generateCTLData_B64() throws PKIException, IOException {
        return Base64.encode(generateCTLData_DER());
    }

    private class Signer {
        X509Cert cert = null;
        JKey privateKey = null;
        AttributeTable sAttr = null;
        Session session = null;
        Mechanism sign_Mechanism = null;
        AttributeTable unsAttr = null;

        Signer(Session session2, JKey key, X509Cert cert2, Mechanism sign_Mechanism2) {
            this.session = session2;
            this.privateKey = key;
            this.cert = cert2;
            this.sign_Mechanism = sign_Mechanism2;
        }

        Signer(Session session2, JKey key, X509Cert cert2, Mechanism sign_Mechanism2, AttributeTable sAttr2, AttributeTable unsAttr2) {
            this.session = session2;
            this.privateKey = key;
            this.cert = cert2;
            this.sign_Mechanism = sign_Mechanism2;
            this.sAttr = sAttr2;
            this.unsAttr = unsAttr2;
        }

        /* access modifiers changed from: package-private */
        public AttributeTable getSignedAttributes() {
            return this.sAttr;
        }

        /* access modifiers changed from: package-private */
        public AttributeTable getUnsignedAttributes() {
            return this.unsAttr;
        }

        /* access modifiers changed from: package-private */
        public JKey getKey() {
            return this.privateKey;
        }

        /* access modifiers changed from: package-private */
        public X509Cert getCertificate() {
            return this.cert;
        }

        /* access modifiers changed from: package-private */
        public Mechanism getSignMechanism() {
            return this.sign_Mechanism;
        }

        /* access modifiers changed from: package-private */
        public String GetDigestTypeName() throws PKIException {
            if (this.sign_Mechanism.getMechanismType().equals("MD2withRSAEncryption")) {
                return Mechanism.MD2;
            }
            if (this.sign_Mechanism.getMechanismType().equals("MD5withRSAEncryption")) {
                return Mechanism.MD5;
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA1withRSAEncryption") || this.sign_Mechanism.getMechanismType().equals("SHA1withDSA")) {
                return Mechanism.SHA1;
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA256withRSAEncryption")) {
                return Mechanism.SHA256;
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA384withRSAEncryption")) {
                return Mechanism.SHA384;
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA512withRSAEncryption")) {
                return Mechanism.SHA512;
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA1withECDSA")) {
                return Mechanism.SHA1;
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA224withECDSA")) {
                return Mechanism.SHA224;
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA256withECDSA")) {
                return Mechanism.SHA256;
            }
            if (this.sign_Mechanism.getMechanismType().equals("SM3withSM2Encryption")) {
                return Mechanism.SM3;
            }
            StringBuffer error = new StringBuffer();
            error.append(PKIException.SIGN_DES);
            error.append(" ");
            error.append(PKIException.NOT_SUP_DES);
            error.append(" ");
            error.append(this.sign_Mechanism.getMechanismType());
            throw new PKIException("8125", error.toString());
        }

        /* access modifiers changed from: package-private */
        public String GetEncTypeName() throws PKIException {
            if (this.sign_Mechanism.getMechanismType().equals("MD2withRSAEncryption") || this.sign_Mechanism.getMechanismType().equals("MD5withRSAEncryption") || this.sign_Mechanism.getMechanismType().equals("SHA1withRSAEncryption")) {
                return Mechanism.RSA;
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA1withDSA") || this.sign_Mechanism.getMechanismType().equals(Mechanism.SHA224_DSA) || this.sign_Mechanism.getMechanismType().equals(Mechanism.SHA256_DSA)) {
                return Mechanism.DSA;
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA1withECDSA") || this.sign_Mechanism.getMechanismType().equals("SHA224withECDSA") || this.sign_Mechanism.getMechanismType().equals("SHA256withECDSA")) {
                return Mechanism.ECDSA;
            }
            if (this.sign_Mechanism.getMechanismType().equals("SM3withSM2Encryption")) {
                return Mechanism.SM2;
            }
            StringBuffer error = new StringBuffer();
            error.append(PKIException.SIGN_DES);
            error.append(" ");
            error.append(PKIException.NOT_SUP_DES);
            error.append(" ");
            error.append(this.sign_Mechanism.getMechanismType());
            throw new PKIException("8125", error.toString());
        }

        private String GetSignatureAlgTypeOID() {
            String SignatureAlgorithm = this.sign_Mechanism.getMechanismType();
            if (SignatureAlgorithm.equals("SHA1withRSAEncryption")) {
                return "1.2.840.113549.1.1.5";
            }
            if (SignatureAlgorithm.equals("SHA256withRSAEncryption")) {
                return "1.2.840.113549.1.1.11";
            }
            if (SignatureAlgorithm.equals("SHA384withRSAEncryption")) {
                return "1.2.840.113549.1.1.12";
            }
            if (SignatureAlgorithm.equals("SHA512withRSAEncryption")) {
                return "1.2.840.113549.1.1.13";
            }
            if (SignatureAlgorithm.equals("MD5withRSAEncryption")) {
                return "1.2.840.113549.1.1.4";
            }
            if (SignatureAlgorithm.equals("MD2withRSAEncryption")) {
                return "1.2.840.113549.1.1.2";
            }
            if (SignatureAlgorithm.equals("SHA1withECDSA")) {
                return "1.2.840.10045.4.1";
            }
            if (SignatureAlgorithm.equals("SHA224withECDSA")) {
                return "1.2.840.10045.4.3.1";
            }
            if (SignatureAlgorithm.equals("SHA256withECDSA")) {
                return "1.2.840.10045.4.3.2";
            }
            if (SignatureAlgorithm.equals("SHA1withDSA")) {
                return "1.2.840.10040.4.3";
            }
            if (SignatureAlgorithm.equals(Mechanism.SHA224_DSA)) {
                return "2.16.840.1.101.3.4.3.1";
            }
            if (SignatureAlgorithm.equals(Mechanism.SHA256_DSA)) {
                return "2.16.840.1.101.3.4.3.2";
            }
            if (SignatureAlgorithm.equals("SM3withSM2Encryption")) {
                return "1.2.156.10197.1.501";
            }
            return null;
        }

        /* access modifiers changed from: package-private */
        public String GetDigestTypeOID() throws PKIException {
            if (this.sign_Mechanism.getMechanismType().equals("MD2withRSAEncryption")) {
                return "1.2.840.113549.2.2";
            }
            if (this.sign_Mechanism.getMechanismType().equals("MD5withRSAEncryption")) {
                return "1.2.840.113549.2.5";
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA1withRSAEncryption") || this.sign_Mechanism.getMechanismType().equals("SHA1withDSA")) {
                return "1.3.14.3.2.26";
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA256withRSAEncryption")) {
                return "2.16.840.1.101.3.4.2.1";
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA384withRSAEncryption") || this.sign_Mechanism.getMechanismType().equals("SHA512withRSAEncryption")) {
                return "2.16.840.1.101.3.4.2.2";
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA1withECDSA")) {
                return "1.3.14.3.2.26";
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA224withECDSA")) {
                return "2.16.840.1.101.3.4.2.4";
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA256withECDSA")) {
                return "2.16.840.1.101.3.4.2.1";
            }
            if (this.sign_Mechanism.getMechanismType().equals(Mechanism.SHA224_DSA)) {
                return "2.16.840.1.101.3.4.2.4";
            }
            if (this.sign_Mechanism.getMechanismType().equals(Mechanism.SHA256_DSA)) {
                return "2.16.840.1.101.3.4.2.1";
            }
            StringBuffer error = new StringBuffer();
            error.append(PKIException.SIGN_DES);
            error.append(" ");
            error.append(PKIException.NOT_SUP_DES);
            error.append(" ");
            error.append(this.sign_Mechanism.getMechanismType());
            throw new PKIException("8125", error.toString());
        }

        /* access modifiers changed from: package-private */
        public String GetEncTypeOID() throws PKIException {
            if (this.sign_Mechanism.getMechanismType().equals("MD2withRSAEncryption") || this.sign_Mechanism.getMechanismType().equals("MD5withRSAEncryption") || this.sign_Mechanism.getMechanismType().equals("SHA1withRSAEncryption")) {
                return "1.2.840.113549.1.1.1";
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA1withDSA")) {
                return "1.2.840.10040.4.3";
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA256withRSAEncryption") || this.sign_Mechanism.getMechanismType().equals("SHA384withRSAEncryption") || this.sign_Mechanism.getMechanismType().equals("SHA512withRSAEncryption")) {
                return "1.2.840.113549.1.1.1";
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA1withECDSA")) {
                return "1.2.840.10045.4.1";
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA224withECDSA")) {
                return "1.2.840.10045.4.3.1";
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA256withECDSA")) {
                return "1.2.840.10045.4.3.2";
            }
            if (this.sign_Mechanism.getMechanismType().equals(Mechanism.SHA224_DSA)) {
                return "2.16.840.1.101.3.4.3.1";
            }
            if (this.sign_Mechanism.getMechanismType().equals(Mechanism.SHA256_DSA)) {
                return "2.16.840.1.101.3.4.3.2";
            }
            StringBuffer error = new StringBuffer();
            error.append(PKIException.SIGN_DES);
            error.append(" ");
            error.append(PKIException.NOT_SUP_DES);
            error.append(" ");
            error.append(this.sign_Mechanism.getMechanismType());
            throw new PKIException("8125", error.toString());
        }

        /* access modifiers changed from: package-private */
        public SignerInfo toSignerInfo(DERObjectIdentifier contentType, byte[] content, boolean addDefaultAttributes, boolean setIssuerAndSN) throws PKIException, IOException {
            AlgorithmIdentifier encAlgId;
            SignerInfo signerInfo;
            AlgorithmIdentifier digAlgId = new AlgorithmIdentifier(new DERObjectIdentifier(GetDigestTypeOID()), new DERNull());
            if (GetEncTypeOID().equals("1.2.840.10040.4.3")) {
                encAlgId = new AlgorithmIdentifier(new DERObjectIdentifier(GetEncTypeOID()));
            } else {
                encAlgId = new AlgorithmIdentifier(new DERObjectIdentifier(GetEncTypeOID()), new DERNull());
            }
            AlgorithmIdentifier digEncAlgId = new AlgorithmIdentifier(new DERObjectIdentifier(GetSignatureAlgTypeOID()), new DERNull());
            ASN1Set signedAttr = null;
            ASN1Set unsignedAttr = null;
            byte[] hash = this.session.digest(new Mechanism(GetDigestTypeName()), content);
            AttributeTable attr = getSignedAttributes();
            if (attr != null) {
                ASN1EncodableVector v = new ASN1EncodableVector();
                if (attr.get(CMSAttributes.contentType) == null) {
                    v.add(new Attribute(CMSAttributes.contentType, new DERSet(contentType)));
                } else {
                    v.add(attr.get(CMSAttributes.contentType));
                }
                if (attr.get(CMSAttributes.signingTime) == null) {
                    v.add(new Attribute(CMSAttributes.signingTime, new DERSet(new Time(new Date()))));
                } else {
                    v.add(attr.get(CMSAttributes.signingTime));
                }
                v.add(new Attribute(CMSAttributes.messageDigest, new DERSet(new DEROctetString(hash))));
                Hashtable ats = attr.toHashtable();
                ats.remove(CMSAttributes.contentType);
                ats.remove(CMSAttributes.signingTime);
                ats.remove(CMSAttributes.messageDigest);
                for (Object instance : ats.values()) {
                    v.add(Attribute.getInstance(instance));
                }
                signedAttr = new DERSet(v);
            } else if (addDefaultAttributes) {
                ASN1EncodableVector v2 = new ASN1EncodableVector();
                v2.add(new Attribute(CMSAttributes.contentType, new DERSet(contentType)));
                v2.add(new Attribute(CMSAttributes.signingTime, new DERSet(new DERUTCTime(new Date()))));
                v2.add(new Attribute(CMSAttributes.messageDigest, new DERSet(new DEROctetString(hash))));
                signedAttr = new DERSet(v2);
            }
            AttributeTable attr2 = getUnsignedAttributes();
            if (attr2 != null) {
                ASN1EncodableVector v3 = new ASN1EncodableVector();
                for (Object instance2 : attr2.toHashtable().values()) {
                    v3.add(Attribute.getInstance(instance2));
                }
                unsignedAttr = new DERSet(v3);
            }
            ByteArrayOutputStream bOut = new ByteArrayOutputStream();
            if (signedAttr != null) {
                DEROutputStream dEROutputStream = new DEROutputStream(bOut);
                dEROutputStream.writeObject(signedAttr);
                dEROutputStream.flush();
                dEROutputStream.close();
            } else {
                bOut.write(content);
            }
            ASN1OctetString encDigest = new DEROctetString(this.session.sign(this.sign_Mechanism, this.privateKey, bOut.toByteArray()));
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(this.cert.getTBSCertificate());
            ASN1InputStream aSN1InputStream = new ASN1InputStream(byteArrayInputStream);
            TBSCertificateStructure tbs = TBSCertificateStructure.getInstance(aSN1InputStream.readObject());
            if (setIssuerAndSN) {
                signerInfo = new SignerInfo(new SignerIdentifier(new IssuerAndSerialNumber(tbs.getIssuer(), tbs.getSerialNumber().getValue())), digAlgId, signedAttr, digEncAlgId, encDigest, unsignedAttr);
            } else {
                signerInfo = new SignerInfo(new SignerIdentifier((ASN1OctetString) new SubjectKeyIdentifier(this.cert.getCertStructure().getSubjectPublicKeyInfo()).getDERObject()), digAlgId, signedAttr, encAlgId, encDigest, unsignedAttr);
            }
            bOut.flush();
            bOut.close();
            aSN1InputStream.close();
            byteArrayInputStream.close();
            return signerInfo;
        }
    }

    public static void main(String[] args) {
        try {
            JCrypto cry = JCrypto.getInstance();
            cry.initialize(JCrypto.JSJY05B_LIB, null);
            CTL ctl = new CTL(cry.openSession(JCrypto.JSJY05B_LIB));
            try {
                CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
                FileInputStream fis = new FileInputStream("d:\\root.cer");
                X509Certificate cert = (X509Certificate) certFactory.generateCertificate(fis);
                fis.close();
                cert.getEncoded();
                X509Cert x509 = new X509Cert(cert.getEncoded());
                PKCS12 p12 = new PKCS12();
                p12.load(new FileInputStream("d:\\key080903.pfx"));
                p12.decrypt("sft".toCharArray());
                JKey prvKey = p12.getPrivateKey();
                X509Cert[] certs2 = p12.getCerts();
                Mechanism mechanism = new Mechanism("SHA1withRSAEncryption");
                ctl.AddCert(x509);
                ctl.AddDig(new Mechanism(Mechanism.SHA1));
                ctl.AddSigner(prvKey, certs2[0], mechanism);
                ctl.SetName("this is test");
                ctl.SetValidDate(new Date(System.currentTimeMillis() + 86400000));
                ExtendedKeyUsageExt keyusg = new ExtendedKeyUsageExt();
                keyusg.addExtendedKeyUsage(ExtendedKeyUsageExt.SERVER_AUTH);
                ctl.AddKeyUsg(keyusg);
                ctl.generateCTLFile("d:\\test.stl".toString());
                System.out.println("to generate success...");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception ex) {
            System.err.println(ex.toString());
        }
    }
}
