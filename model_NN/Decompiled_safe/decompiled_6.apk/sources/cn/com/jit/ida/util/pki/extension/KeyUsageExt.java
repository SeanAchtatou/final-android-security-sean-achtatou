package cn.com.jit.ida.util.pki.extension;

import cn.com.jit.ida.util.pki.asn1.DERBitString;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.x509.KeyUsage;
import cn.com.jit.ida.util.pki.asn1.x509.X509Extensions;
import java.util.Vector;

public class KeyUsageExt extends AbstractStandardExtension {
    public static final String CRL_SIGN = "cRLSign";
    public static final String DATA_ENCIPHERMENT = "dataEncipherment";
    public static final String DECIPHER_ONLY = "decipherOnly";
    public static final String DIGITAL_SIGNATURE = "digitalSignature";
    public static final String ENCIPHER_ONLY = "encipherOnly";
    public static final String KEY_AGREEMENT = "keyAgreement";
    public static final String KEY_CERT_SIGN = "keyCertSign";
    public static final String KEY_ENCIPHERMENT = "keyEncipherment";
    public static final String NON_REPUDIATION = "nonRepudiation";
    private int iKeyUsage;
    private KeyUsage keyUsage;
    private Vector keyUsageVector;

    public KeyUsageExt() {
        this.iKeyUsage = 0;
        this.keyUsageVector = null;
        this.keyUsage = null;
        this.iKeyUsage = 0;
        this.keyUsageVector = new Vector();
        this.critical = false;
        this.OID = X509Extensions.KeyUsage.getId();
    }

    public KeyUsageExt(DERBitString derBitString) {
        this.iKeyUsage = 0;
        this.keyUsageVector = null;
        this.keyUsage = null;
        this.keyUsage = new KeyUsage(derBitString);
    }

    public void setCritical(boolean critical) {
        this.critical = critical;
    }

    public boolean getCritical() {
        return this.critical;
    }

    public String getOID() {
        return this.OID;
    }

    public void addKeyUsage(String keyUsageItem) {
        this.keyUsageVector.add(keyUsageItem);
    }

    public void setKeyUsage(Vector keyUsageVector2) {
        for (int i = 0; i < keyUsageVector2.size(); i++) {
            this.keyUsageVector.add((String) keyUsageVector2.get(i));
        }
    }

    public Vector getKeyUsage() {
        return this.keyUsageVector;
    }

    public byte[] encode() {
        if (this.keyUsageVector.contains(DIGITAL_SIGNATURE)) {
            this.iKeyUsage |= 128;
        }
        if (this.keyUsageVector.contains(NON_REPUDIATION)) {
            this.iKeyUsage |= 64;
        }
        if (this.keyUsageVector.contains(KEY_ENCIPHERMENT)) {
            this.iKeyUsage |= 32;
        }
        if (this.keyUsageVector.contains(DATA_ENCIPHERMENT)) {
            this.iKeyUsage |= 16;
        }
        if (this.keyUsageVector.contains(KEY_AGREEMENT)) {
            this.iKeyUsage |= 8;
        }
        if (this.keyUsageVector.contains(KEY_CERT_SIGN)) {
            this.iKeyUsage |= 4;
        }
        if (this.keyUsageVector.contains(CRL_SIGN)) {
            this.iKeyUsage |= 2;
        }
        if (this.keyUsageVector.contains(ENCIPHER_ONLY)) {
            this.iKeyUsage |= 1;
        }
        if (this.keyUsageVector.contains(DECIPHER_ONLY)) {
            this.iKeyUsage |= 32768;
        }
        return new DEROctetString(new KeyUsage(this.iKeyUsage).getDERObject()).getOctets();
    }

    public boolean[] getKeyUsageForBoolean() {
        if (this.keyUsage == null) {
            return null;
        }
        return this.keyUsage.getKeyUsage();
    }

    public boolean getKeyUsageByName(String name) {
        String tempName = name.trim();
        boolean[] array = getKeyUsageForBoolean();
        if (array == null) {
            return false;
        }
        if (DIGITAL_SIGNATURE.equalsIgnoreCase(tempName)) {
            return array[0];
        }
        if (NON_REPUDIATION.equalsIgnoreCase(tempName)) {
            return array[1];
        }
        if (KEY_ENCIPHERMENT.equalsIgnoreCase(tempName)) {
            return array[2];
        }
        if (DATA_ENCIPHERMENT.equalsIgnoreCase(tempName)) {
            return array[3];
        }
        if (KEY_AGREEMENT.equalsIgnoreCase(tempName)) {
            return array[4];
        }
        if (KEY_CERT_SIGN.equalsIgnoreCase(tempName)) {
            return array[5];
        }
        if (CRL_SIGN.equalsIgnoreCase(tempName)) {
            return array[6];
        }
        if (ENCIPHER_ONLY.equalsIgnoreCase(tempName)) {
            return array[7];
        }
        if (DECIPHER_ONLY.equalsIgnoreCase(tempName)) {
            return array[8];
        }
        return false;
    }
}
