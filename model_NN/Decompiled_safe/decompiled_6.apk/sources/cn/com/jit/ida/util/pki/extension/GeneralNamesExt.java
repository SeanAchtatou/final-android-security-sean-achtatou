package cn.com.jit.ida.util.pki.extension;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject;
import cn.com.jit.ida.util.pki.asn1.DERBMPString;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DEREncodableVector;
import cn.com.jit.ida.util.pki.asn1.DERIA5String;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.DERPrintableString;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.DERT61String;
import cn.com.jit.ida.util.pki.asn1.DERTaggedObject;
import cn.com.jit.ida.util.pki.asn1.DERUTF8String;
import cn.com.jit.ida.util.pki.asn1.DERUniversalString;
import cn.com.jit.ida.util.pki.asn1.x509.GeneralName;
import cn.com.jit.ida.util.pki.asn1.x509.GeneralNames;
import cn.com.jit.ida.util.pki.asn1.x509.OtherName;
import cn.com.jit.ida.util.pki.asn1.x509.X509Name;
import cn.com.jit.ida.util.pki.encoders.Hex;

public class GeneralNamesExt extends AbstractStandardExtension {
    public static final int DIRECTORYNAME = 4;
    public static final int DNSNAME = 2;
    public static final int EDIPARTYNAME = 5;
    public static final int EDIPARTYNAME_BMPSTRING = 204;
    public static final int EDIPARTYNAME_PRINTABLESTRING = 201;
    public static final int EDIPARTYNAME_TELETEXSTRING = 200;
    public static final int EDIPARTYNAME_UNIVERSALSTRING = 202;
    public static final int EDIPARTYNAME_UTF8STRING = 203;
    public static final int IPADDRESS = 7;
    public static final int OTHERNAME_GUID = 101;
    public static final int OTHERNAME_UPN = 100;
    public static final String OTHER_NAME_GUID_OID = "1.23.456.789";
    public static final String OTHER_NAME_UPN_OID = "1.3.6.1.4.1.311.20.2.3";
    public static final int REGISTEREDID = 8;
    public static final int RFC822NAME = 1;
    public static final int TYPE_NONE = -1;
    public static final int UNIFORMRESOURCEIDENTIFIER = 6;
    public static final int X400ADDRESS = 3;
    private ASN1EncodableVector GeneralNames = null;
    private ASN1Sequence seqGeneralNames = null;

    public GeneralNamesExt() {
        this.critical = false;
    }

    public GeneralNamesExt(ASN1Sequence asn1Sequence) {
        this.seqGeneralNames = asn1Sequence;
    }

    public int getGeneralNameCount() {
        return this.seqGeneralNames.size();
    }

    public int getGeneralNameType(int index) {
        int type = ((ASN1TaggedObject) this.seqGeneralNames.getObjectAt(index)).getTagNo();
        if (type != 0) {
            return type;
        }
        if ("1.3.6.1.4.1.311.20.2.3".equals(OtherName.getInstance(((ASN1TaggedObject) this.seqGeneralNames.getObjectAt(index)).getObject()).getTypeID().getId())) {
            return 100;
        }
        return 101;
    }

    public String getGeneralName(int index) {
        return GeneralNameToString((ASN1TaggedObject) this.seqGeneralNames.getObjectAt(index));
    }

    public String getEDIPartyNameToNameAsSigner(int index) {
        String ret_String = null;
        try {
            ASN1Sequence tempseq = (ASN1Sequence) ((ASN1TaggedObject) this.seqGeneralNames.getObjectAt(index)).getObject();
            for (int i = 0; i < tempseq.size(); i++) {
                ASN1TaggedObject tempTagObj = (ASN1TaggedObject) tempseq.getObjectAt(i);
                if (tempTagObj.getTagNo() == 0) {
                    DERObject obj = tempTagObj.getObject();
                    if (obj instanceof DERT61String) {
                        ret_String = DERT61String.getInstance(obj).getString();
                    } else if (obj instanceof DERPrintableString) {
                        ret_String = DERPrintableString.getInstance(obj).getString();
                    } else if (obj instanceof DERUTF8String) {
                        ret_String = DERUTF8String.getInstance(obj).getString();
                    } else if (obj instanceof DERBMPString) {
                        ret_String = DERBMPString.getInstance(obj).getString();
                    }
                }
            }
        } catch (Exception e) {
        }
        return ret_String;
    }

    public String getEDIPartyNameToPartyName(int index) {
        String ret_String = null;
        try {
            ASN1Sequence tempseq = (ASN1Sequence) ((ASN1TaggedObject) this.seqGeneralNames.getObjectAt(index)).getObject();
            for (int i = 0; i < tempseq.size(); i++) {
                ASN1TaggedObject tempTagObj = (ASN1TaggedObject) tempseq.getObjectAt(i);
                if (tempTagObj.getTagNo() == 1) {
                    DERObject obj = tempTagObj.getObject();
                    if (obj instanceof DERT61String) {
                        ret_String = DERT61String.getInstance(obj).getString();
                    } else if (obj instanceof DERPrintableString) {
                        ret_String = DERPrintableString.getInstance(obj).getString();
                    } else if (obj instanceof DERUTF8String) {
                        ret_String = DERUTF8String.getInstance(obj).getString();
                    } else if (obj instanceof DERBMPString) {
                        ret_String = DERBMPString.getInstance(obj).getString();
                    }
                }
            }
        } catch (Exception e) {
        }
        return ret_String;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v2, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v5, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v7, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v8, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v9, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v10, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String GeneralNameToString(cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject r8) {
        /*
            r7 = this;
            r6 = 4
            r1 = 0
            int r5 = r8.getTagNo()
            switch(r5) {
                case 0: goto L_0x000a;
                case 1: goto L_0x0059;
                case 2: goto L_0x0059;
                case 3: goto L_0x0009;
                case 4: goto L_0x00cb;
                case 5: goto L_0x0009;
                case 6: goto L_0x0059;
                case 7: goto L_0x0066;
                case 8: goto L_0x00bd;
                default: goto L_0x0009;
            }
        L_0x0009:
            return r1
        L_0x000a:
            java.lang.String r5 = "1.3.6.1.4.1.311.20.2.3"
            cn.com.jit.ida.util.pki.asn1.DERObject r6 = r8.getObject()
            cn.com.jit.ida.util.pki.asn1.x509.OtherName r6 = cn.com.jit.ida.util.pki.asn1.x509.OtherName.getInstance(r6)
            cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier r6 = r6.getTypeID()
            java.lang.String r6 = r6.getId()
            boolean r5 = r5.equals(r6)
            if (r5 == 0) goto L_0x0037
            cn.com.jit.ida.util.pki.asn1.DERObject r5 = r8.getObject()
            cn.com.jit.ida.util.pki.asn1.x509.OtherName r5 = cn.com.jit.ida.util.pki.asn1.x509.OtherName.getInstance(r5)
            cn.com.jit.ida.util.pki.asn1.DEREncodable r5 = r5.getValue()
            cn.com.jit.ida.util.pki.asn1.DERUTF8String r5 = (cn.com.jit.ida.util.pki.asn1.DERUTF8String) r5
            cn.com.jit.ida.util.pki.asn1.DERUTF8String r5 = (cn.com.jit.ida.util.pki.asn1.DERUTF8String) r5
            java.lang.String r1 = r5.getString()
            goto L_0x0009
        L_0x0037:
            java.lang.String r1 = new java.lang.String
            cn.com.jit.ida.util.pki.asn1.DERObject r5 = r8.getObject()
            cn.com.jit.ida.util.pki.asn1.x509.OtherName r5 = cn.com.jit.ida.util.pki.asn1.x509.OtherName.getInstance(r5)
            cn.com.jit.ida.util.pki.asn1.DEREncodable r5 = r5.getValue()
            cn.com.jit.ida.util.pki.asn1.DERObject r5 = r5.getDERObject()
            cn.com.jit.ida.util.pki.asn1.DEROctetString r5 = (cn.com.jit.ida.util.pki.asn1.DEROctetString) r5
            cn.com.jit.ida.util.pki.asn1.DEROctetString r5 = (cn.com.jit.ida.util.pki.asn1.DEROctetString) r5
            byte[] r5 = r5.getOctets()
            byte[] r5 = cn.com.jit.ida.util.pki.encoders.Hex.encode(r5)
            r1.<init>(r5)
            goto L_0x0009
        L_0x0059:
            cn.com.jit.ida.util.pki.asn1.DERObject r5 = r8.getObject()
            cn.com.jit.ida.util.pki.asn1.DERIA5String r5 = cn.com.jit.ida.util.pki.asn1.DERIA5String.getInstance(r5)
            java.lang.String r1 = r5.getString()
            goto L_0x0009
        L_0x0066:
            java.lang.StringBuffer r4 = new java.lang.StringBuffer
            r4.<init>()
            cn.com.jit.ida.util.pki.asn1.DERObject r5 = r8.getObject()
            cn.com.jit.ida.util.pki.asn1.DEROctetString r5 = (cn.com.jit.ida.util.pki.asn1.DEROctetString) r5
            byte[] r2 = r5.getOctets()
            r3 = 0
            r0 = 0
        L_0x0077:
            if (r0 >= r6) goto L_0x0091
            byte r3 = r2[r0]
            if (r3 >= 0) goto L_0x007f
            int r3 = r3 + 256
        L_0x007f:
            java.lang.String r5 = java.lang.String.valueOf(r3)
            r4.append(r5)
            r5 = 3
            if (r0 >= r5) goto L_0x008e
            java.lang.String r5 = "."
            r4.append(r5)
        L_0x008e:
            int r0 = r0 + 1
            goto L_0x0077
        L_0x0091:
            int r5 = r2.length
            if (r5 <= r6) goto L_0x00b7
            java.lang.String r5 = "/"
            r4.append(r5)
            r0 = 4
        L_0x009a:
            int r5 = r2.length
            if (r0 >= r5) goto L_0x00b7
            byte r3 = r2[r0]
            if (r3 >= 0) goto L_0x00a3
            int r3 = r3 + 256
        L_0x00a3:
            java.lang.String r5 = java.lang.String.valueOf(r3)
            r4.append(r5)
            int r5 = r2.length
            int r5 = r5 + -1
            if (r0 >= r5) goto L_0x00b4
            java.lang.String r5 = "."
            r4.append(r5)
        L_0x00b4:
            int r0 = r0 + 1
            goto L_0x009a
        L_0x00b7:
            java.lang.String r1 = r4.toString()
            goto L_0x0009
        L_0x00bd:
            cn.com.jit.ida.util.pki.asn1.DERObject r5 = r8.getObject()
            cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier r5 = cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier.getInstance(r5)
            java.lang.String r1 = r5.getId()
            goto L_0x0009
        L_0x00cb:
            cn.com.jit.ida.util.pki.asn1.DERObject r5 = r8.getObject()
            cn.com.jit.ida.util.pki.asn1.x509.X509Name r5 = cn.com.jit.ida.util.pki.asn1.x509.X509Name.getInstance(r5)
            java.lang.String r1 = r5.toString()
            goto L_0x0009
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.com.jit.ida.util.pki.extension.GeneralNamesExt.GeneralNameToString(cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject):java.lang.String");
    }

    public void addRFC822Name(String value) throws PKIException {
        addGeneralName(1, value);
    }

    public void addDNSName(String value) throws PKIException {
        addGeneralName(2, value);
    }

    public void addDirectoryName(String value) throws PKIException {
        addGeneralName(4, value);
    }

    public void addUniformResourceIdentifier(String value) throws PKIException {
        addGeneralName(6, value);
    }

    public void addIPAddress(String value) throws PKIException {
        addGeneralName(7, value);
    }

    public void addRegisteredID(String value) throws PKIException {
        addGeneralName(8, value);
    }

    public void addOtherName_UPN(String value) throws PKIException {
        addGeneralName(100, value);
    }

    public void addOtherName_GUID(String value) throws PKIException {
        addGeneralName(101, value);
    }

    public void addEDIPartyName(int partyNameType, String partyName, int nameAssignerType, String nameAssigner) throws PKIException {
        if (partyName == null) {
            throw new PKIException(PKIException.EXT_ENCODING_DATA_NULL, PKIException.EXT_ENCODING_DATA_NULL_DES);
        }
        DEREncodableVector v = new DEREncodableVector();
        if (nameAssigner != null) {
            v.add(new DERTaggedObject(0, TypeToDEREncodable(nameAssignerType, nameAssigner)));
        }
        v.add(new DERTaggedObject(1, TypeToDEREncodable(partyNameType, partyName)));
        this.GeneralNames.add(new GeneralName(new DERSequence(v), 5));
    }

    public void addEDIPartyName(int partyNameType, byte[] partyName, int nameAssignerType, byte[] nameAssigner) throws PKIException {
        if (partyName == null) {
            throw new PKIException(PKIException.EXT_ENCODING_DATA_NULL, PKIException.EXT_ENCODING_DATA_NULL_DES);
        }
        DEREncodableVector v = new DEREncodableVector();
        if (nameAssigner != null) {
            v.add(new DERTaggedObject(0, TypeToDEREncodable(nameAssignerType, nameAssigner)));
        }
        v.add(new DERTaggedObject(1, TypeToDEREncodable(partyNameType, partyName)));
        this.GeneralNames.add(new GeneralName(new DERSequence(v), 5));
    }

    public void addEDIPartyName(int partyNameType, String partyName, int nameAssignerType, byte[] nameAssigner) throws PKIException {
        if (partyName == null) {
            throw new PKIException(PKIException.EXT_ENCODING_DATA_NULL, PKIException.EXT_ENCODING_DATA_NULL_DES);
        }
        DEREncodableVector v = new DEREncodableVector();
        if (nameAssigner != null) {
            v.add(new DERTaggedObject(0, TypeToDEREncodable(nameAssignerType, nameAssigner)));
        }
        v.add(new DERTaggedObject(1, TypeToDEREncodable(partyNameType, partyName)));
        this.GeneralNames.add(new GeneralName(new DERSequence(v), 5));
    }

    public void addEDIPartyName(int partyNameType, byte[] partyName, int nameAssignerType, String nameAssigner) throws PKIException {
        if (partyName == null) {
            throw new PKIException(PKIException.EXT_ENCODING_DATA_NULL, PKIException.EXT_ENCODING_DATA_NULL_DES);
        }
        DEREncodableVector v = new DEREncodableVector();
        if (nameAssigner != null) {
            v.add(new DERTaggedObject(0, TypeToDEREncodable(nameAssignerType, nameAssigner)));
        }
        v.add(new DERTaggedObject(1, TypeToDEREncodable(partyNameType, partyName)));
        this.GeneralNames.add(new GeneralName(new DERSequence(v), 5));
    }

    public void addGeneralName(int dataType, String value) throws PKIException {
        if (this.GeneralNames == null) {
            this.GeneralNames = new ASN1EncodableVector();
        }
        if (dataType == 100 || dataType == 101) {
            this.GeneralNames.add(new GeneralName(TypeConversion(dataType, value), 0));
        } else {
            this.GeneralNames.add(new GeneralName(TypeConversion(dataType, value), dataType));
        }
    }

    public byte[] encode() throws PKIException {
        if (this.GeneralNames != null) {
            return new DEROctetString(new GeneralNames(new DERSequence(this.GeneralNames)).getDERObject()).getOctets();
        }
        throw new PKIException(PKIException.EXT_ENCODING_DATA_NULL, PKIException.EXT_ENCODING_DATA_NULL_DES);
    }

    public DERObject getDERObject() throws PKIException {
        if (this.GeneralNames != null) {
            return new GeneralNames(new DERSequence(this.GeneralNames)).getDERObject();
        }
        throw new PKIException(PKIException.EXT_ENCODING_DATA_NULL, PKIException.EXT_ENCODING_DATA_NULL_DES);
    }

    public DERObject getASN1Object() {
        if (this.GeneralNames == null) {
            return new DERSequence();
        }
        return new DERSequence(this.GeneralNames);
    }

    public boolean getCritical() {
        return this.critical;
    }

    public String getOID() {
        return this.OID;
    }

    public void setCritical(boolean critical) {
        this.critical = critical;
    }

    private DEREncodable TypeToDEREncodable(int valueType, String value) throws PKIException {
        switch (valueType) {
            case 200:
                return new DERT61String(value);
            case 201:
                return new DERPrintableString(value);
            case 202:
                throw new PKIException(PKIException.EXT_ENCODING_DATATYPE_NOT, PKIException.EXT_ENCODING_DATATYPE_NOT_DES);
            case 203:
                return new DERUTF8String(value);
            case 204:
                return new DERBMPString(value);
            default:
                return null;
        }
    }

    private DEREncodable TypeToDEREncodable(int valueType, byte[] value) throws PKIException {
        switch (valueType) {
            case 200:
                return new DERT61String(value);
            case 201:
                return new DERPrintableString(value);
            case 202:
                return new DERUniversalString(value);
            case 203:
                return new DERUTF8String(new String(value));
            case 204:
                return new DERBMPString(value);
            default:
                return null;
        }
    }

    private DERObject TypeConversion(int valueType, String value) throws PKIException {
        DERObject derencodable = null;
        switch (valueType) {
            case 1:
            case 2:
            case 6:
                derencodable = new DERIA5String(value).getDERObject();
                break;
            case 3:
                throw new PKIException(PKIException.EXT_ENCODING_DATATYPE_NOT, PKIException.EXT_ENCODING_DATATYPE_NOT_DES);
            case 4:
                derencodable = new X509Name(value).getDERObject();
                break;
            case 7:
                derencodable = new DEROctetString(IP4Address(value)).getDERObject();
                break;
            case 8:
                derencodable = new DERObjectIdentifier(value).getDERObject();
                break;
            case 100:
                derencodable = new OtherName(new DERObjectIdentifier("1.3.6.1.4.1.311.20.2.3"), new DERTaggedObject(true, 0, new DERUTF8String(value))).getDERObject();
                break;
            case 101:
                derencodable = new OtherName(new DERObjectIdentifier("1.23.456.789"), new DERTaggedObject(true, 0, new DEROctetString(Hex.decode(value)))).getDERObject();
                break;
        }
        if (derencodable != null) {
            return derencodable;
        }
        try {
            throw new PKIException(PKIException.EXT_ENCODING_DATATYPE_NOT, PKIException.EXT_ENCODING_DATATYPE_NOT_DES);
        } catch (Exception ex) {
            throw new PKIException(PKIException.EXT_ENCODE_GENERALNAMES_ERR, PKIException.EXT_ENCODE_GENERALNAMES_ERR_DES, ex);
        }
    }

    private byte[] IP4Address(String value) {
        String[] allip = value.split("/");
        if (allip.length == 2) {
            byte[] ip4_byte = new byte[8];
            GenIP4Address(ip4_byte, allip[0]);
            byte[] ip4_mask = new byte[4];
            GenIP4Address(ip4_mask, allip[1]);
            System.arraycopy(ip4_mask, 0, ip4_byte, 4, 4);
            return ip4_byte;
        } else if (allip.length != 1) {
            return null;
        } else {
            byte[] ip4_byte2 = new byte[4];
            GenIP4Address(ip4_byte2, value);
            return ip4_byte2;
        }
    }

    private void GenIP4Address(byte[] ip4_byte, String value) {
        for (int i = 0; i < 4; i++) {
            int pos = value.indexOf(".");
            if (pos == -1) {
                ip4_byte[i] = Integer.valueOf(value).byteValue();
            } else {
                ip4_byte[i] = Integer.valueOf(value.substring(0, pos)).byteValue();
            }
            value = value.substring(pos + 1, value.length());
        }
    }
}
