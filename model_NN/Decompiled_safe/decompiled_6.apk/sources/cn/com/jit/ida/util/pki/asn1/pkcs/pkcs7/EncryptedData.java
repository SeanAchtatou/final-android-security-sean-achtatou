package cn.com.jit.ida.util.pki.asn1.pkcs.pkcs7;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERSequence;

public class EncryptedData implements DEREncodable {
    private EncryptedContentInfo encryptedContentInfo;
    private DERInteger version;

    public static EncryptedData getInstance(Object o) {
        if (o == null || (o instanceof EncryptedData)) {
            return (EncryptedData) o;
        }
        if (o instanceof ASN1Sequence) {
            return new EncryptedData((ASN1Sequence) o);
        }
        throw new IllegalArgumentException("unknown object in factory:" + o.getClass().getName());
    }

    public EncryptedData(DERInteger _version, EncryptedContentInfo _encryptedContentInfo) {
        this.version = _version;
        this.encryptedContentInfo = _encryptedContentInfo;
    }

    public EncryptedData(ASN1Sequence seq) {
        this.version = (DERInteger) seq.getObjectAt(0);
        this.encryptedContentInfo = EncryptedContentInfo.getInstance(seq.getObjectAt(1));
    }

    public DERInteger getVersion() {
        return this.version;
    }

    public EncryptedContentInfo getEncryptedContentInfo() {
        return this.encryptedContentInfo;
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(this.version);
        v.add(this.encryptedContentInfo);
        return new DERSequence(v);
    }
}
