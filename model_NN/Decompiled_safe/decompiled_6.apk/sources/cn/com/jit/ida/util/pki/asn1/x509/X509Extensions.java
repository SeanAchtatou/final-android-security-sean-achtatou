package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1OctetString;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject;
import cn.com.jit.ida.util.pki.asn1.DERBoolean;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.extension.AuthorityInformationAccessExt;
import cn.com.jit.ida.util.pki.extension.AuthorityKeyIdentifierExt;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class X509Extensions implements DEREncodable {
    public static final DERObjectIdentifier ACRLDPExtension = new DERObjectIdentifier("2.5.29.31");
    public static final DERObjectIdentifier ACTargeting = new DERObjectIdentifier("2.5.29.55");
    public static final DERObjectIdentifier AuditIdentity = new DERObjectIdentifier("1.3.6.1.5.5.7.1.4");
    public static final DERObjectIdentifier AuthorityInfoAccess = new DERObjectIdentifier("1.3.6.1.5.5.7.1.1");
    public static final DERObjectIdentifier AuthorityInformationAccess = new DERObjectIdentifier("1.3.6.1.5.5.7.1.1");
    public static final DERObjectIdentifier AuthorityKeyIdentifier = new DERObjectIdentifier("2.5.29.35");
    public static final DERObjectIdentifier BasicConstraints = new DERObjectIdentifier("2.5.29.19");
    public static final DERObjectIdentifier CERTIFICATE_TEMPLATE = new DERObjectIdentifier("1.3.6.1.4.1.311.20.2");
    public static final DERObjectIdentifier CRLDistributionPoints = new DERObjectIdentifier("2.5.29.31");
    public static final DERObjectIdentifier CRLNumber = new DERObjectIdentifier("2.5.29.20");
    public static final DERObjectIdentifier CertificateIssuer = new DERObjectIdentifier("2.5.29.29");
    public static final DERObjectIdentifier CertificatePolicies = new DERObjectIdentifier("2.5.29.32");
    public static final DERObjectIdentifier DeltaCRLIndicator = new DERObjectIdentifier("2.5.29.27");
    public static final DERObjectIdentifier ExtendedKeyUsage = new DERObjectIdentifier("2.5.29.37");
    public static final DERObjectIdentifier InhibitAnyPolicy = new DERObjectIdentifier("2.5.29.54");
    public static final DERObjectIdentifier InstructionCode = new DERObjectIdentifier("2.5.29.23");
    public static final DERObjectIdentifier InvalidityDate = new DERObjectIdentifier("2.5.29.24");
    public static final DERObjectIdentifier IssuerAlternativeName = new DERObjectIdentifier("2.5.29.18");
    public static final DERObjectIdentifier IssuingDistributionPoint = new DERObjectIdentifier("2.5.29.28");
    public static final DERObjectIdentifier JIT_CasteCode = new DERObjectIdentifier("1.2.156.10260.4.1.7");
    public static final DERObjectIdentifier JIT_ICRegistrationNumber = new DERObjectIdentifier("1.2.156.10260.4.1.3");
    public static final DERObjectIdentifier JIT_ICRegistrationNumberOld = new DERObjectIdentifier("1.2.86.11.7.4");
    public static final DERObjectIdentifier JIT_IdentifyCode = new DERObjectIdentifier("1.2.156.10260.4.1.1");
    public static final DERObjectIdentifier JIT_IdentifyCodeOld = new DERObjectIdentifier("1.2.86.11.7.1");
    public static final DERObjectIdentifier JIT_InsuranceNumber = new DERObjectIdentifier("1.2.156.10260.4.1.2");
    public static final DERObjectIdentifier JIT_InsuranceNumberOld = new DERObjectIdentifier("1.2.86.11.7.2");
    public static final DERObjectIdentifier JIT_OrganizationCode = new DERObjectIdentifier("1.2.156.10260.4.1.4");
    public static final DERObjectIdentifier JIT_OrganizationCodeOld = new DERObjectIdentifier("1.2.86.11.7.3");
    public static final DERObjectIdentifier JIT_StationCode = new DERObjectIdentifier("1.2.156.10260.4.1.6");
    public static final DERObjectIdentifier JIT_TaxationNumber = new DERObjectIdentifier("1.2.156.10260.4.1.5");
    public static final DERObjectIdentifier JIT_TaxationNumberOld = new DERObjectIdentifier("1.2.86.11.7.5");
    public static final DERObjectIdentifier KeyUsage = new DERObjectIdentifier("2.5.29.15");
    public static final DERObjectIdentifier NameConstraints = new DERObjectIdentifier("2.5.29.30");
    public static final DERObjectIdentifier NetscapeCertType = new DERObjectIdentifier("2.16.840.1.113730.1.1");
    public static final DERObjectIdentifier NoRevocation = new DERObjectIdentifier("2.5.29.56");
    public static final DERObjectIdentifier PolicyConstraints = new DERObjectIdentifier("2.5.29.36");
    public static final DERObjectIdentifier PolicyMappings = new DERObjectIdentifier("2.5.29.33");
    public static final DERObjectIdentifier PrivateKeyUsagePeriod = new DERObjectIdentifier("2.5.29.16");
    public static final DERObjectIdentifier ReasonCode = new DERObjectIdentifier("2.5.29.21");
    public static final DERObjectIdentifier SubjectAlternativeName = new DERObjectIdentifier("2.5.29.17");
    public static final DERObjectIdentifier SubjectDirectoryAttributes = new DERObjectIdentifier("2.5.29.9");
    public static final DERObjectIdentifier SubjectKeyIdentifier = new DERObjectIdentifier("2.5.29.14");
    private Hashtable extensions;
    private Vector ordering;

    public static X509Extensions getInstance(ASN1TaggedObject obj, boolean explicit) {
        return getInstance(ASN1Sequence.getInstance(obj, explicit));
    }

    public static X509Extensions getInstance(Object obj) {
        if (obj == null || (obj instanceof X509Extensions)) {
            return (X509Extensions) obj;
        }
        if (obj instanceof ASN1Sequence) {
            return new X509Extensions((ASN1Sequence) obj);
        }
        if (obj instanceof ASN1TaggedObject) {
            return getInstance(((ASN1TaggedObject) obj).getObject());
        }
        throw new IllegalArgumentException("illegal object in getInstance: " + obj.getClass().getName());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cn.com.jit.ida.util.pki.asn1.x509.X509Extension.<init>(boolean, cn.com.jit.ida.util.pki.asn1.ASN1OctetString):void
     arg types: [int, cn.com.jit.ida.util.pki.asn1.ASN1OctetString]
     candidates:
      cn.com.jit.ida.util.pki.asn1.x509.X509Extension.<init>(cn.com.jit.ida.util.pki.asn1.DERBoolean, cn.com.jit.ida.util.pki.asn1.ASN1OctetString):void
      cn.com.jit.ida.util.pki.asn1.x509.X509Extension.<init>(boolean, cn.com.jit.ida.util.pki.asn1.ASN1OctetString):void */
    public X509Extensions(ASN1Sequence seq) {
        this.extensions = new Hashtable();
        this.ordering = new Vector();
        Enumeration e = seq.getObjects();
        while (e.hasMoreElements()) {
            ASN1Sequence s = (ASN1Sequence) e.nextElement();
            if (s.size() == 3) {
                this.extensions.put(s.getObjectAt(0), new X509Extension((DERBoolean) s.getObjectAt(1), (ASN1OctetString) s.getObjectAt(2)));
            } else {
                this.extensions.put(s.getObjectAt(0), new X509Extension(false, (ASN1OctetString) s.getObjectAt(1)));
            }
            this.ordering.addElement(s.getObjectAt(0));
        }
    }

    public X509Extensions(Hashtable extensions2) {
        this(null, extensions2);
    }

    public X509Extensions(Vector ordering2, Hashtable extensions2) {
        Enumeration e;
        this.extensions = new Hashtable();
        this.ordering = new Vector();
        if (ordering2 == null) {
            e = extensions2.keys();
        } else {
            e = ordering2.elements();
        }
        while (e.hasMoreElements()) {
            this.ordering.addElement(e.nextElement());
        }
        Enumeration e2 = this.ordering.elements();
        while (e2.hasMoreElements()) {
            DERObjectIdentifier oid = (DERObjectIdentifier) e2.nextElement();
            this.extensions.put(oid, (X509Extension) extensions2.get(oid));
        }
    }

    public Enumeration oids() {
        return this.ordering.elements();
    }

    public X509Extension getExtension(DERObjectIdentifier oid) {
        return (X509Extension) this.extensions.get(oid);
    }

    public DERObject getDERObject() {
        ASN1EncodableVector vec = new ASN1EncodableVector();
        Enumeration e = this.ordering.elements();
        while (e.hasMoreElements()) {
            DERObjectIdentifier oid = (DERObjectIdentifier) e.nextElement();
            X509Extension ext = (X509Extension) this.extensions.get(oid);
            ASN1EncodableVector v = new ASN1EncodableVector();
            v.add(oid);
            if (ext.isCritical()) {
                v.add(new DERBoolean(true));
            }
            v.add(ext.getValue());
            vec.add(new DERSequence(v));
        }
        return new DERSequence(vec);
    }

    public int hashCode() {
        Enumeration e = this.extensions.keys();
        int hashCode = 0;
        while (e.hasMoreElements()) {
            Object o = e.nextElement();
            hashCode = (hashCode ^ o.hashCode()) ^ this.extensions.get(o).hashCode();
        }
        return hashCode;
    }

    public boolean equals(Object o) {
        if (o == null || !(o instanceof X509Extensions)) {
            return false;
        }
        Enumeration e1 = this.extensions.keys();
        Enumeration e2 = ((X509Extensions) o).extensions.keys();
        while (e1.hasMoreElements() && e2.hasMoreElements()) {
            if (!e1.nextElement().equals(e2.nextElement())) {
                return false;
            }
        }
        if (e1.hasMoreElements() || e2.hasMoreElements()) {
            return false;
        }
        return true;
    }

    public X509Extensions(Node nl) throws PKIException {
        this.extensions = new Hashtable();
        this.ordering = new Vector();
        NodeList nodelist = nl.getChildNodes();
        if (nodelist.getLength() == 0) {
            throw new PKIException("XML内容缺失,X509Extensions.X509Extensions(Node nl),Extensions没有子节点");
        }
        for (int i = 0; i < nodelist.getLength(); i++) {
            Node cnode = nodelist.item(i);
            String sname = cnode.getNodeName();
            if (sname.equals("AuthorityInfoAccess")) {
                DERObjectIdentifier identifier = new DERObjectIdentifier(AuthorityInfoAccess.getId());
                this.ordering.add(identifier);
                this.extensions.put(identifier, new AuthorityInformationAccessExt(cnode));
            }
            if (sname.equals("AuthorityKeyIdentifierExtension")) {
                this.extensions.put(new DERObjectIdentifier(AuthorityKeyIdentifier.getId()), new AuthorityKeyIdentifierExt(cnode));
            }
            if (sname.equals("ACTargetingExtension")) {
                this.extensions.put(new DERObjectIdentifier(ACTargeting.getId()), new ACTargetingExtension(cnode));
            }
            if (sname.equals("AuditIdentity")) {
                this.extensions.put(new DERObjectIdentifier(AuditIdentity.getId()), new AuditIdentity(cnode));
            }
            if (sname.equals("ACRLDPExtension")) {
                this.extensions.put(new DERObjectIdentifier(ACRLDPExtension.getId()), new ACRLDPExtension(cnode));
            }
            if (sname.equals("NoRevocationExtension")) {
                this.extensions.put(new DERObjectIdentifier(NoRevocation.getId()), new NoRevocationExtension(cnode));
            }
        }
    }
}
