package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.Parser;
import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.DERBitString;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.pkcs.PKCSObjectIdentifiers;
import cn.com.jit.ida.util.pki.cipher.JKey;
import cn.com.jit.ida.util.pki.cipher.Mechanism;
import cn.com.jit.ida.util.pki.cipher.Session;
import java.math.BigInteger;

public class AttributeCertificate implements DEREncodable {
    AttributeCertificateInfo acinfo;
    AlgorithmIdentifier signatureAlgorithm;
    DERBitString signatureValue;

    public AttributeCertificateInfo getAcinfo() {
        return this.acinfo;
    }

    public AlgorithmIdentifier getSignatureAlgorithm() {
        return this.signatureAlgorithm;
    }

    public DERBitString getSignatureValue() {
        return this.signatureValue;
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(this.acinfo);
        v.add(this.signatureAlgorithm);
        v.add(this.signatureValue);
        return new DERSequence(v);
    }

    public AttributeCertificate(byte[] data) throws PKIException {
        DERSequence seq = (DERSequence) Parser.writeBytes2DERObj(data);
        this.acinfo = new AttributeCertificateInfo((DERSequence) seq.getObjectAt(0));
        this.signatureAlgorithm = new AlgorithmIdentifier((DERSequence) seq.getObjectAt(1));
        this.signatureValue = (DERBitString) seq.getObjectAt(2);
    }

    public boolean verify(JKey pubKey, Session session) throws PKIException {
        Mechanism mechanism;
        DERObjectIdentifier oid = this.signatureAlgorithm.getObjectId();
        if (oid.equals(PKCSObjectIdentifiers.md2WithRSAEncryption)) {
            mechanism = new Mechanism("MD2withRSAEncryption");
        } else if (oid.equals(PKCSObjectIdentifiers.md5WithRSAEncryption)) {
            mechanism = new Mechanism("MD5withRSAEncryption");
        } else if (oid.equals(PKCSObjectIdentifiers.sha1WithRSAEncryption) || oid.equals(PKCSObjectIdentifiers.sha1WithRSAEncryption_v1)) {
            mechanism = new Mechanism("SHA1withRSAEncryption");
        } else if (oid.equals(PKCSObjectIdentifiers.sha1WithECEncryption)) {
            mechanism = new Mechanism("SHA1withECDSA");
        } else if (oid.equals(PKCSObjectIdentifiers.sha1WithDSA)) {
            mechanism = new Mechanism("SHA1withDSA");
        } else if (oid.equals(PKCSObjectIdentifiers.sm2_with_sm3)) {
            mechanism = new Mechanism("SM3withSM2Encryption");
        } else {
            throw new PKIException(PKIException.NONSUPPORT_SIGALG, "不支持的签名算法:" + oid.getId());
        }
        try {
            return session.verifySign(mechanism, pubKey, Parser.writeDERObj2Bytes(this.acinfo.getDERObject()), this.signatureValue.getBytes());
        } catch (Exception ex) {
            throw new PKIException("6", PKIException.VERIFY_SIGN_DES, ex);
        }
    }

    public int getVersion() {
        return this.acinfo.getAttCertVersion().getValue().intValue();
    }

    public BigInteger getSerialNumber() {
        return this.acinfo.getSerialNumber().getValue();
    }

    public byte[] getSignature() {
        return this.signatureValue.getBytes();
    }
}
