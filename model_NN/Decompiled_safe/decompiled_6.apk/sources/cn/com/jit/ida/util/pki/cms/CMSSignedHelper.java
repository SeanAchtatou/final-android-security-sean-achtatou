package cn.com.jit.ida.util.pki.cms;

import cn.com.jit.ida.util.pki.cipher.Mechanism;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.Signature;
import java.security.cert.CRLException;
import java.security.cert.CertStore;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.CollectionCertStoreParameters;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.DERObject;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.cryptopro.CryptoProObjectIdentifiers;
import org.bouncycastle.asn1.nist.NISTObjectIdentifiers;
import org.bouncycastle.asn1.oiw.OIWObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.teletrust.TeleTrusTObjectIdentifiers;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.X509ObjectIdentifiers;
import org.bouncycastle.asn1.x9.X9ObjectIdentifiers;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.x509.NoSuchStoreException;
import org.bouncycastle.x509.X509CollectionStoreParameters;
import org.bouncycastle.x509.X509Store;
import org.bouncycastle.x509.X509V2AttributeCertificate;

class CMSSignedHelper {
    static final CMSSignedHelper INSTANCE = new CMSSignedHelper();
    private static final Map digestAlgs = new HashMap();
    private static final Map digestAliases = new HashMap();
    private static final Map encryptionAlgs = new HashMap();

    CMSSignedHelper() {
    }

    static {
        addEntries(NISTObjectIdentifiers.dsa_with_sha224, Mechanism.SHA224, Mechanism.DSA);
        addEntries(NISTObjectIdentifiers.dsa_with_sha256, Mechanism.SHA256, Mechanism.DSA);
        addEntries(NISTObjectIdentifiers.dsa_with_sha384, Mechanism.SHA384, Mechanism.DSA);
        addEntries(NISTObjectIdentifiers.dsa_with_sha512, Mechanism.SHA512, Mechanism.DSA);
        addEntries(OIWObjectIdentifiers.dsaWithSHA1, Mechanism.SHA1, Mechanism.DSA);
        addEntries(OIWObjectIdentifiers.md4WithRSA, "MD4", Mechanism.RSA);
        addEntries(OIWObjectIdentifiers.md4WithRSAEncryption, "MD4", Mechanism.RSA);
        addEntries(OIWObjectIdentifiers.md5WithRSA, Mechanism.MD5, Mechanism.RSA);
        addEntries(OIWObjectIdentifiers.sha1WithRSA, Mechanism.SHA1, Mechanism.RSA);
        addEntries(PKCSObjectIdentifiers.md2WithRSAEncryption, Mechanism.MD2, Mechanism.RSA);
        addEntries(PKCSObjectIdentifiers.md4WithRSAEncryption, "MD4", Mechanism.RSA);
        addEntries(PKCSObjectIdentifiers.md5WithRSAEncryption, Mechanism.MD5, Mechanism.RSA);
        addEntries(PKCSObjectIdentifiers.sha1WithRSAEncryption, Mechanism.SHA1, Mechanism.RSA);
        addEntries(PKCSObjectIdentifiers.sha224WithRSAEncryption, Mechanism.SHA224, Mechanism.RSA);
        addEntries(PKCSObjectIdentifiers.sha256WithRSAEncryption, Mechanism.SHA256, Mechanism.RSA);
        addEntries(PKCSObjectIdentifiers.sha384WithRSAEncryption, Mechanism.SHA384, Mechanism.RSA);
        addEntries(PKCSObjectIdentifiers.sha512WithRSAEncryption, Mechanism.SHA512, Mechanism.RSA);
        addEntries(X9ObjectIdentifiers.ecdsa_with_SHA1, Mechanism.SHA1, Mechanism.ECDSA);
        addEntries(X9ObjectIdentifiers.ecdsa_with_SHA224, Mechanism.SHA224, Mechanism.ECDSA);
        addEntries(X9ObjectIdentifiers.ecdsa_with_SHA256, Mechanism.SHA256, Mechanism.ECDSA);
        addEntries(X9ObjectIdentifiers.ecdsa_with_SHA384, Mechanism.SHA384, Mechanism.ECDSA);
        addEntries(X9ObjectIdentifiers.ecdsa_with_SHA512, Mechanism.SHA512, Mechanism.ECDSA);
        addEntries(X9ObjectIdentifiers.id_dsa_with_sha1, Mechanism.SHA1, Mechanism.DSA);
        encryptionAlgs.put(X9ObjectIdentifiers.id_dsa.getId(), Mechanism.DSA);
        encryptionAlgs.put(PKCSObjectIdentifiers.rsaEncryption.getId(), Mechanism.RSA);
        encryptionAlgs.put("1.3.36.3.3.1", Mechanism.RSA);
        encryptionAlgs.put(X509ObjectIdentifiers.id_ea_rsa.getId(), Mechanism.RSA);
        encryptionAlgs.put(JITCMSSignedDataGenerator.ENCRYPTION_RSA_PSS, "RSAandMGF1");
        encryptionAlgs.put(CryptoProObjectIdentifiers.gostR3410_94.getId(), "GOST3410");
        encryptionAlgs.put(CryptoProObjectIdentifiers.gostR3410_2001.getId(), "ECGOST3410");
        encryptionAlgs.put("1.3.6.1.4.1.5849.1.6.2", "ECGOST3410");
        encryptionAlgs.put("1.3.6.1.4.1.5849.1.1.5", "GOST3410");
        digestAlgs.put(PKCSObjectIdentifiers.md2.getId(), Mechanism.MD2);
        digestAlgs.put(PKCSObjectIdentifiers.md4.getId(), "MD4");
        digestAlgs.put(PKCSObjectIdentifiers.md5.getId(), Mechanism.MD5);
        digestAlgs.put(OIWObjectIdentifiers.idSHA1.getId(), Mechanism.SHA1);
        digestAlgs.put(NISTObjectIdentifiers.id_sha224.getId(), Mechanism.SHA224);
        digestAlgs.put(NISTObjectIdentifiers.id_sha256.getId(), Mechanism.SHA256);
        digestAlgs.put(NISTObjectIdentifiers.id_sha384.getId(), Mechanism.SHA384);
        digestAlgs.put(NISTObjectIdentifiers.id_sha512.getId(), Mechanism.SHA512);
        digestAlgs.put(TeleTrusTObjectIdentifiers.ripemd128.getId(), "RIPEMD128");
        digestAlgs.put(TeleTrusTObjectIdentifiers.ripemd160.getId(), "RIPEMD160");
        digestAlgs.put(TeleTrusTObjectIdentifiers.ripemd256.getId(), "RIPEMD256");
        digestAlgs.put(CryptoProObjectIdentifiers.gostR3411.getId(), "GOST3411");
        digestAlgs.put("1.3.6.1.4.1.5849.1.2.1", "GOST3411");
        digestAliases.put(Mechanism.SHA1, new String[]{"SHA-1"});
        digestAliases.put(Mechanism.SHA224, new String[]{"SHA-224"});
        digestAliases.put(Mechanism.SHA256, new String[]{"SHA-256"});
        digestAliases.put(Mechanism.SHA384, new String[]{"SHA-384"});
        digestAliases.put(Mechanism.SHA512, new String[]{"SHA-512"});
    }

    private static void addEntries(DERObjectIdentifier alias, String digest, String encryption) {
        digestAlgs.put(alias.getId(), digest);
        encryptionAlgs.put(alias.getId(), encryption);
    }

    /* access modifiers changed from: package-private */
    public String getDigestAlgName(String digestAlgOID) {
        String algName = (String) digestAlgs.get(digestAlgOID);
        return algName != null ? algName : digestAlgOID;
    }

    /* access modifiers changed from: package-private */
    public String[] getDigestAliases(String algName) {
        String[] aliases = (String[]) digestAliases.get(algName);
        return aliases != null ? aliases : new String[0];
    }

    /* access modifiers changed from: package-private */
    public String getEncryptionAlgName(String encryptionAlgOID) {
        String algName = (String) encryptionAlgs.get(encryptionAlgOID);
        return algName != null ? algName : encryptionAlgOID;
    }

    /* access modifiers changed from: package-private */
    public MessageDigest getDigestInstance(String algorithm, Provider provider) throws NoSuchAlgorithmException {
        try {
            return createDigestInstance(algorithm, provider);
        } catch (NoSuchAlgorithmException e) {
            String[] aliases = getDigestAliases(algorithm);
            int i = 0;
            while (i != aliases.length) {
                try {
                    return createDigestInstance(aliases[i], provider);
                } catch (NoSuchAlgorithmException e2) {
                    i++;
                }
            }
            if (provider != null) {
                return getDigestInstance(algorithm, null);
            }
            throw e;
        }
    }

    private MessageDigest createDigestInstance(String algorithm, Provider provider) throws NoSuchAlgorithmException {
        if (provider != null) {
            return MessageDigest.getInstance(algorithm, provider);
        }
        return MessageDigest.getInstance(algorithm);
    }

    /* access modifiers changed from: package-private */
    public Signature getSignatureInstance(String algorithm, Provider provider) throws NoSuchAlgorithmException {
        if (provider != null) {
            return Signature.getInstance(algorithm, provider);
        }
        return Signature.getInstance(algorithm);
    }

    /* access modifiers changed from: package-private */
    public X509Store createAttributeStore(String type, Provider provider, ASN1Set certSet) throws NoSuchStoreException, CMSException {
        List certs = new ArrayList();
        if (certSet != null) {
            Enumeration e = certSet.getObjects();
            while (e.hasMoreElements()) {
                try {
                    ASN1TaggedObject dERObject = ((DEREncodable) e.nextElement()).getDERObject();
                    if (dERObject instanceof ASN1TaggedObject) {
                        ASN1TaggedObject tagged = dERObject;
                        if (tagged.getTagNo() == 2) {
                            certs.add(new X509V2AttributeCertificate(ASN1Sequence.getInstance(tagged, false).getEncoded()));
                        }
                    }
                } catch (IOException ex) {
                    throw new CMSException("can't re-encode attribute certificate!", ex);
                }
            }
        }
        try {
            return X509Store.getInstance("AttributeCertificate/" + type, new X509CollectionStoreParameters(certs), provider);
        } catch (IllegalArgumentException e2) {
            throw new CMSException("can't setup the X509Store", e2);
        }
    }

    /* access modifiers changed from: package-private */
    public X509Store createCertificateStore(String type, Provider provider, ASN1Set certSet) throws NoSuchStoreException, CMSException {
        List certs = new ArrayList();
        if (certSet != null) {
            addCertsFromSet(certs, certSet, provider);
        }
        try {
            return X509Store.getInstance("Certificate/" + type, new X509CollectionStoreParameters(certs), provider);
        } catch (IllegalArgumentException e) {
            throw new CMSException("can't setup the X509Store", e);
        }
    }

    /* access modifiers changed from: package-private */
    public X509Store createCRLsStore(String type, Provider provider, ASN1Set crlSet) throws NoSuchStoreException, CMSException {
        List crls = new ArrayList();
        if (crlSet != null) {
            addCRLsFromSet(crls, crlSet, provider);
        }
        try {
            return X509Store.getInstance("CRL/" + type, new X509CollectionStoreParameters(crls), provider);
        } catch (IllegalArgumentException e) {
            throw new CMSException("can't setup the X509Store", e);
        }
    }

    /* access modifiers changed from: package-private */
    public CertStore createCertStore(String type, Provider provider, ASN1Set certSet, ASN1Set crlSet) throws CMSException, NoSuchAlgorithmException {
        List certsAndcrls = new ArrayList();
        if (certSet != null) {
            addCertsFromSet(certsAndcrls, certSet, provider);
        }
        if (crlSet != null) {
            addCRLsFromSet(certsAndcrls, crlSet, provider);
        }
        if (provider == null) {
            return CertStore.getInstance(type, new CollectionCertStoreParameters(certsAndcrls));
        }
        try {
            return CertStore.getInstance(type, new CollectionCertStoreParameters(certsAndcrls), provider);
        } catch (InvalidAlgorithmParameterException e) {
            throw new CMSException("can't setup the CertStore", e);
        }
    }

    private void addCertsFromSet(List certs, ASN1Set certSet, Provider provider) throws CMSException {
        CertificateFactory cf;
        if (provider != null) {
            try {
                cf = CertificateFactory.getInstance("X.509", provider);
            } catch (CertificateException ex) {
                throw new CMSException("can't get certificate factory.", ex);
            }
        } else {
            cf = CertificateFactory.getInstance("X.509");
        }
        Enumeration e = certSet.getObjects();
        while (e.hasMoreElements()) {
            try {
                DERObject obj = ((DEREncodable) e.nextElement()).getDERObject();
                if (obj instanceof ASN1Sequence) {
                    certs.add(cf.generateCertificate(new ByteArrayInputStream(obj.getEncoded())));
                }
            } catch (IOException ex2) {
                throw new CMSException("can't re-encode certificate!", ex2);
            } catch (CertificateException ex3) {
                throw new CMSException("can't re-encode certificate!", ex3);
            }
        }
    }

    private void addCRLsFromSet(List crls, ASN1Set certSet, Provider provider) throws CMSException {
        CertificateFactory cf;
        if (provider != null) {
            try {
                cf = CertificateFactory.getInstance("X.509", provider);
            } catch (CertificateException ex) {
                throw new CMSException("can't get certificate factory.", ex);
            }
        } else {
            cf = CertificateFactory.getInstance("X.509");
        }
        Enumeration e = certSet.getObjects();
        while (e.hasMoreElements()) {
            try {
                crls.add(cf.generateCRL(new ByteArrayInputStream(((DEREncodable) e.nextElement()).getDERObject().getEncoded())));
            } catch (IOException ex2) {
                throw new CMSException("can't re-encode CRL!", ex2);
            } catch (CRLException ex3) {
                throw new CMSException("can't re-encode CRL!", ex3);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public AlgorithmIdentifier fixAlgID(AlgorithmIdentifier algId) {
        if (algId.getParameters() == null) {
            return new AlgorithmIdentifier(algId.getObjectId(), DERNull.INSTANCE);
        }
        return algId;
    }

    /* access modifiers changed from: package-private */
    public void setSigningEncryptionAlgorithmMapping(DERObjectIdentifier oid, String algorithmName) {
        encryptionAlgs.put(oid.getId(), algorithmName);
    }

    /* access modifiers changed from: package-private */
    public void setSigningDigestAlgorithmMapping(DERObjectIdentifier oid, String algorithmName) {
        digestAlgs.put(oid.getId(), algorithmName);
    }
}
