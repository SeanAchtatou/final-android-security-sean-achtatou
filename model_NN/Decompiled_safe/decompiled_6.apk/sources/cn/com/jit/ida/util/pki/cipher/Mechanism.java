package cn.com.jit.ida.util.pki.cipher;

public class Mechanism {
    public static final String AES_CBC = "AES/CBC/PKCS7Padding";
    public static final String AES_ECB = "AES/ECB/PKCS7Padding";
    public static final String AES_KEY = "AES";
    public static final String CAST5_CBC = "CAST5/CBC/PKCS7Padding";
    public static final String CAST5_ECB = "CAST5/ECB/PKCS7Padding";
    public static final String CAST5_KEY = "CAST5";
    public static final String DES3_CBC = "DESede/CBC/PKCS7Padding";
    public static final String DES3_ECB = "DESede/ECB/PKCS7Padding";
    public static final String DES3_KEY = "DESede";
    public static final String DES_CBC = "DES/CBC/PKCS7Padding";
    public static final String DES_ECB = "DES/ECB/PKCS7Padding";
    public static final String DES_ECB_NOPADDING = "DES/ECB/NOPADDING";
    public static final String DES_KEY = "DES";
    public static final String DSA = "DSA";
    public static final String ECDSA = "ECDSA";
    public static final String ECIES = "ECIES";
    public static final String HMAC_MD2 = "HMac-MD2";
    public static final String HMAC_MD5 = "HMac-MD5";
    public static final String HMAC_SHA1 = "HMac-SHA1";
    public static final String IDEA_CBC = "IDEA/CBC/PKCS7Padding";
    public static final String IDEA_ECB = "IDEA/ECB/PKCS7Padding";
    public static final String IDEA_KEY = "IDEA";
    public static final String MASTER_KEY = "MASTERKEY";
    public static final String MD2 = "MD2";
    public static final String MD2_RSA = "MD2withRSAEncryption";
    public static final String MD5 = "MD5";
    public static final String MD5_RSA = "MD5withRSAEncryption";
    public static final String PBE_2KEY = "PBEWITHSHAAND2-KEYTRIPLEDES-CBC";
    public static final String PBE_3KEY = "PBEWITHSHAAND3-KEYTRIPLEDES-CBC";
    public static final String PBE_KEY = "PBEWithMD5AndDES";
    public static final String PBE_MD5_DES = "PBEWithMD5AndDES";
    public static final String PBE_MD5_RC2 = "PBEWithMD5AndRC2";
    public static final String PBE_SHA1_2DES = "PBEWITHSHAAND2-KEYTRIPLEDES-CBC";
    public static final String PBE_SHA1_3DES = "PBEWITHSHAAND3-KEYTRIPLEDES-CBC";
    public static final String PBE_SHA1_DES = "PBEWithSHA1AndDES";
    public static final String PBE_SHA1_RC2 = "PBEWithSHA1AndRC2";
    public static final String RANDOM = "Random";
    public static final String RAW = "CKM_RSA_RAW";
    public static final String RC2_CBC = "RC2/CBC/PKCS7Padding";
    public static final String RC2_ECB = "RC2/ECB/PKCS7Padding";
    public static final String RC2_KEY = "RC2";
    public static final String RC4 = "RC4";
    public static final String RC4_KEY = "RC4";
    public static final String RSA = "RSA";
    public static final String RSA_PKCS = "RSA/ECB/PKCS1PADDING";
    public static final String SCB2_CBC = "SCB2_CBC";
    public static final String SCB2_ECB = "SCB2_ECB";
    public static final String SCB2_KEY = "SCB2";
    public static final String SF33_CBC = "SF33_CBC";
    public static final String SF33_ECB = "SF33_ECB";
    public static final String SF33_KEY = "SF33";
    public static final String SHA1 = "SHA1";
    public static final String SHA1_DSA = "SHA1withDSA";
    public static final String SHA1_EC_DSA = "SHA1withECDSA";
    public static final String SHA1_RSA = "SHA1withRSAEncryption";
    public static final String SHA224 = "SHA224";
    public static final String SHA224_DSA = "SHA224withDSA";
    public static final String SHA224_EC_DSA = "SHA224withECDSA";
    public static final String SHA224_RSA = "SHA224withRSAEncryption";
    public static final String SHA256 = "SHA256";
    public static final String SHA256_DSA = "SHA256withDSA";
    public static final String SHA256_EC_DSA = "SHA256withECDSA";
    public static final String SHA256_RSA = "SHA256withRSAEncryption";
    public static final String SHA384 = "SHA384";
    public static final String SHA384_RSA = "SHA384withRSAEncryption";
    public static final String SHA512 = "SHA512";
    public static final String SHA512_RSA = "SHA512withRSAEncryption";
    public static final String SM2 = "SM2";
    public static final String SM2_RAW = "SM2_RAW";
    public static final String SM3 = "SM3";
    public static final String SM3_SM2 = "SM3withSM2Encryption";
    public static final String SM4 = "SM4";
    public static final String SM4_CBC = "SM4_CBC";
    public static final String SM4_ECB = "SM4_ECB";
    public static final String SYMMETRY_KEY = "SYMMETRY";
    private boolean isPad = true;
    private String mechanismType;
    private Object param;

    public Mechanism(String mechanismType2, Object param2) {
        this.mechanismType = mechanismType2;
        this.param = param2;
    }

    public Mechanism(String mechanismType2) {
        this.mechanismType = mechanismType2;
        this.param = null;
    }

    public String getMechanismType() {
        return this.mechanismType;
    }

    public Object getParam() {
        return this.param;
    }

    public void setParam(Object param2) {
        this.param = param2;
    }

    public void setMechanismType(String mechanismType2) {
        this.mechanismType = mechanismType2;
    }

    public boolean isDigestabled() {
        if (MD2.equals(this.mechanismType) || MD5.equals(this.mechanismType) || SHA1.equals(this.mechanismType) || SHA224.equals(this.mechanismType) || SHA256.equals(this.mechanismType) || SHA384.equals(this.mechanismType) || SHA512.equals(this.mechanismType)) {
            return true;
        }
        return false;
    }

    public boolean isSignabled() {
        if ("MD2withRSAEncryption".equals(this.mechanismType) || "MD5withRSAEncryption".equals(this.mechanismType) || "SHA1withRSAEncryption".equals(this.mechanismType) || "SHA1withDSA".equals(this.mechanismType) || "SHA1withECDSA".equals(this.mechanismType) || SHA224_DSA.equals(this.mechanismType) || SHA256_DSA.equals(this.mechanismType) || "SHA224withECDSA".equals(this.mechanismType) || "SHA256withECDSA".equals(this.mechanismType) || "SHA224withRSAEncryption".equals(this.mechanismType) || "SHA256withRSAEncryption".equals(this.mechanismType) || "SHA384withRSAEncryption".equals(this.mechanismType) || "SHA512withRSAEncryption".equals(this.mechanismType) || RSA_PKCS.equals(this.mechanismType)) {
            return true;
        }
        return false;
    }

    public boolean isGenerateKeyPairabled() {
        if (RSA.equals(this.mechanismType) || DSA.equals(this.mechanismType) || ECIES.equals(this.mechanismType) || ECDSA.equals(this.mechanismType)) {
            return true;
        }
        return false;
    }

    public String toString() {
        return "Mechanism [isPad=" + this.isPad + ", mechanismType=" + this.mechanismType + ", param=" + this.param + "]";
    }

    public boolean isPad() {
        return this.isPad;
    }

    public void setPad(boolean isPad2) {
        this.isPad = isPad2;
    }
}
