package cn.com.jit.ida.util.pki.cipher;

import javax.crypto.Cipher;

public class JHandle {
    private long hardLibHandle;
    private Cipher softLibHandle;

    public JHandle(long hardLibHandle2, Cipher softLibHandle2) {
        this.hardLibHandle = hardLibHandle2;
        this.softLibHandle = softLibHandle2;
    }

    public long getHardLibHandle() {
        return this.hardLibHandle;
    }

    public Cipher getSoftLibHandle() {
        return this.softLibHandle;
    }
}
