package cn.com.jit.ida.util.pki.extension;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.x509.CertificateTemplate;
import cn.com.jit.ida.util.pki.asn1.x509.X509Extensions;

public class CertificateTemplateExt extends AbstractStandardExtension {
    private String template = null;

    public CertificateTemplateExt() {
        this.OID = X509Extensions.CERTIFICATE_TEMPLATE.getId();
        this.critical = false;
    }

    public String getOID() {
        return this.OID;
    }

    public void setCritical(boolean critical) {
        this.critical = critical;
    }

    public byte[] encode() throws PKIException {
        return new DEROctetString(new CertificateTemplate(this.template).getDERObject()).getOctets();
    }

    public boolean getCritical() {
        return this.critical;
    }

    public String getTemplate() {
        return this.template;
    }

    public void setTemplate(String template2) {
        this.template = template2;
    }
}
