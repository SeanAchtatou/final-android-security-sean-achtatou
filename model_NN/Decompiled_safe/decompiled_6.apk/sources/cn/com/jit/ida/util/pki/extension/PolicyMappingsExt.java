package cn.com.jit.ida.util.pki.extension;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.x509.PolicyMappings;
import cn.com.jit.ida.util.pki.asn1.x509.X509Extensions;
import java.util.Hashtable;

public class PolicyMappingsExt extends AbstractStandardExtension {
    private Hashtable mappings = null;
    private ASN1Sequence sep = null;

    public PolicyMappingsExt() {
        this.OID = X509Extensions.PolicyMappings.getId();
        this.critical = false;
        this.mappings = new Hashtable();
    }

    public PolicyMappingsExt(ASN1Sequence asn1Sequence) {
        this.sep = asn1Sequence;
    }

    public int getPolicyMappingCount() {
        if (this.sep != null) {
            return this.sep.size();
        }
        return this.mappings.size();
    }

    public String getIssuerDomainPolicy(int index) {
        return ((DERObjectIdentifier) ((ASN1Sequence) this.sep.getObjectAt(index)).getObjectAt(0)).getId();
    }

    public String getSubjectDomainPolicy(int index) {
        return ((DERObjectIdentifier) ((ASN1Sequence) this.sep.getObjectAt(index)).getObjectAt(1)).getId();
    }

    public String getOID() {
        return this.OID;
    }

    public void setCritical(boolean critical) {
        this.critical = critical;
    }

    public boolean getCritical() {
        return this.critical;
    }

    public void addPolicyMapping(String issuerDomainPolicy, String subjectDomainPolicy) {
        this.mappings.put(issuerDomainPolicy, subjectDomainPolicy);
    }

    public byte[] encode() throws PKIException {
        return new DEROctetString(new PolicyMappings(this.mappings).getDERObject()).getOctets();
    }
}
