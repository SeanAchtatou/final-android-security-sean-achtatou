package cn.com.jit.ida.util.pki.asn1;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Vector;

public abstract class ASN1Set extends DERObject {
    protected Vector set = new Vector();

    /* access modifiers changed from: package-private */
    public abstract void encode(DEROutputStream dEROutputStream) throws IOException;

    public static ASN1Set getInstance(Object obj) {
        if (obj == null || (obj instanceof ASN1Set)) {
            return (ASN1Set) obj;
        }
        throw new IllegalArgumentException("unknown object in getInstance");
    }

    public static ASN1Set getInstance(ASN1TaggedObject obj, boolean explicit) {
        if (explicit) {
            if (obj.isExplicit()) {
                return (ASN1Set) obj.getObject();
            }
            throw new IllegalArgumentException("object implicit - explicit expected.");
        } else if (obj.isExplicit()) {
            return new DERSet(obj.getObject());
        } else {
            if (obj.getObject() instanceof ASN1Set) {
                return (ASN1Set) obj.getObject();
            }
            ASN1EncodableVector v = new ASN1EncodableVector();
            if (obj.getObject() instanceof ASN1Sequence) {
                Enumeration e = ((ASN1Sequence) obj.getObject()).getObjects();
                while (e.hasMoreElements()) {
                    v.add((DEREncodable) e.nextElement());
                }
                return new DERSet(v);
            }
            throw new IllegalArgumentException("unknown object in getInstanceFromTagged");
        }
    }

    public Enumeration getObjects() {
        return this.set.elements();
    }

    public DEREncodable getObjectAt(int index) {
        return (DEREncodable) this.set.elementAt(index);
    }

    public int size() {
        return this.set.size();
    }

    public int hashCode() {
        Enumeration e = getObjects();
        int hashCode = 0;
        while (e.hasMoreElements()) {
            hashCode ^= e.nextElement().hashCode();
        }
        return hashCode;
    }

    public boolean equals(Object o) {
        if (o == null || !(o instanceof ASN1Set)) {
            return false;
        }
        ASN1Set other = (ASN1Set) o;
        if (size() != other.size()) {
            return false;
        }
        Enumeration s1 = getObjects();
        Enumeration s2 = other.getObjects();
        while (s1.hasMoreElements()) {
            if (!s1.nextElement().equals(s2.nextElement())) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void addObject(DEREncodable obj) {
        this.set.addElement(obj);
    }
}
