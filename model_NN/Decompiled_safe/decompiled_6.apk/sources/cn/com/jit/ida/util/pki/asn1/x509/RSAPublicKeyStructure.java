package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import java.math.BigInteger;
import java.util.Enumeration;

public class RSAPublicKeyStructure implements DEREncodable {
    private BigInteger modulus;
    private BigInteger publicExponent;

    public static RSAPublicKeyStructure getInstance(ASN1TaggedObject obj, boolean explicit) {
        return getInstance(ASN1Sequence.getInstance(obj, explicit));
    }

    public static RSAPublicKeyStructure getInstance(Object obj) {
        if (obj == null || (obj instanceof RSAPublicKeyStructure)) {
            return (RSAPublicKeyStructure) obj;
        }
        if (obj instanceof ASN1Sequence) {
            return new RSAPublicKeyStructure((ASN1Sequence) obj);
        }
        throw new IllegalArgumentException("Invalid RSAPublicKeyStructure: " + obj.getClass().getName());
    }

    public RSAPublicKeyStructure(BigInteger modulus2, BigInteger publicExponent2) {
        this.modulus = modulus2;
        this.publicExponent = publicExponent2;
    }

    public RSAPublicKeyStructure(ASN1Sequence seq) {
        Enumeration e = seq.getObjects();
        this.modulus = ((DERInteger) e.nextElement()).getPositiveValue();
        this.publicExponent = ((DERInteger) e.nextElement()).getPositiveValue();
    }

    public BigInteger getModulus() {
        return this.modulus;
    }

    public BigInteger getPublicExponent() {
        return this.publicExponent;
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(new DERInteger(getModulus()));
        v.add(new DERInteger(getPublicExponent()));
        return new DERSequence(v);
    }
}
