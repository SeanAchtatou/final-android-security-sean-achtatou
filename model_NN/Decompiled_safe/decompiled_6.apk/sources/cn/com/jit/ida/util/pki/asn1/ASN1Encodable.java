package cn.com.jit.ida.util.pki.asn1;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public abstract class ASN1Encodable implements DEREncodable {
    public abstract DERObject toASN1Object();

    public byte[] getEncoded() throws IOException {
        ByteArrayOutputStream bOut = new ByteArrayOutputStream();
        new ASN1OutputStream(bOut).writeObject(this);
        return bOut.toByteArray();
    }

    public int hashCode() {
        return getDERObject().hashCode();
    }

    public boolean equals(Object o) {
        if (o == null || !(o instanceof DEREncodable)) {
            return false;
        }
        return getDERObject().equals(((DEREncodable) o).getDERObject());
    }

    public DERObject getDERObject() {
        return toASN1Object();
    }
}
