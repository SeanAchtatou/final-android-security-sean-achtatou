package cn.com.jit.ida.util.pki.cms;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.cert.CRLException;
import java.security.cert.CertStore;
import java.security.cert.CertStoreException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.BEROctetStringGenerator;
import org.bouncycastle.asn1.BERSet;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.cms.ContentInfo;
import org.bouncycastle.asn1.x509.CertificateList;
import org.bouncycastle.asn1.x509.TBSCertificateStructure;
import org.bouncycastle.asn1.x509.X509CertificateStructure;
import org.bouncycastle.cms.CMSException;

class CMSUtils {
    private static final Runtime RUNTIME = Runtime.getRuntime();

    CMSUtils() {
    }

    static int getMaximumMemory() {
        long maxMem = RUNTIME.maxMemory();
        if (maxMem > 2147483647L) {
            return Integer.MAX_VALUE;
        }
        return (int) maxMem;
    }

    static ContentInfo readContentInfo(byte[] input) throws CMSException {
        return readContentInfo(new ASN1InputStream(input));
    }

    static ContentInfo readContentInfo(InputStream input) throws CMSException {
        return readContentInfo(new ASN1InputStream(input, getMaximumMemory()));
    }

    static List getCertificatesFromStore(CertStore certStore) throws CertStoreException, CMSException {
        List certs = new ArrayList();
        try {
            Iterator it = certStore.getCertificates(null).iterator();
            while (it.hasNext()) {
                certs.add(X509CertificateStructure.getInstance(ASN1Object.fromByteArray(((X509Certificate) it.next()).getEncoded())));
            }
            return certs;
        } catch (IllegalArgumentException e) {
            throw new CMSException("error processing certs", e);
        } catch (IOException e2) {
            throw new CMSException("error processing certs", e2);
        } catch (CertificateEncodingException e3) {
            throw new CMSException("error encoding certs", e3);
        }
    }

    static List getCRLsFromStore(CertStore certStore) throws CertStoreException, CMSException {
        List crls = new ArrayList();
        try {
            Iterator it = certStore.getCRLs(null).iterator();
            while (it.hasNext()) {
                crls.add(CertificateList.getInstance(ASN1Object.fromByteArray(((X509CRL) it.next()).getEncoded())));
            }
            return crls;
        } catch (IllegalArgumentException e) {
            throw new CMSException("error processing crls", e);
        } catch (IOException e2) {
            throw new CMSException("error processing crls", e2);
        } catch (CRLException e3) {
            throw new CMSException("error encoding crls", e3);
        }
    }

    static ASN1Set createBerSetFromList(List derObjects) {
        ASN1EncodableVector v = new ASN1EncodableVector();
        Iterator it = derObjects.iterator();
        while (it.hasNext()) {
            v.add((DEREncodable) it.next());
        }
        return new BERSet(v);
    }

    static ASN1Set createDerSetFromList(List derObjects) {
        ASN1EncodableVector v = new ASN1EncodableVector();
        Iterator it = derObjects.iterator();
        while (it.hasNext()) {
            v.add((DEREncodable) it.next());
        }
        return new DERSet(v);
    }

    static OutputStream createBEROctetOutputStream(OutputStream s, int tagNo, boolean isExplicit, int bufferSize) throws IOException {
        BEROctetStringGenerator octGen = new BEROctetStringGenerator(s, tagNo, isExplicit);
        if (bufferSize != 0) {
            return octGen.getOctetOutputStream(new byte[bufferSize]);
        }
        return octGen.getOctetOutputStream();
    }

    static TBSCertificateStructure getTBSCertificateStructure(X509Certificate cert) throws CertificateEncodingException {
        try {
            return TBSCertificateStructure.getInstance(ASN1Object.fromByteArray(cert.getTBSCertificate()));
        } catch (IOException e) {
            throw new CertificateEncodingException(e.toString());
        }
    }

    private static ContentInfo readContentInfo(ASN1InputStream in) throws CMSException {
        try {
            return ContentInfo.getInstance(in.readObject());
        } catch (IOException e) {
            throw new CMSException("IOException reading content.", e);
        } catch (ClassCastException e2) {
            throw new CMSException("Malformed content.", e2);
        } catch (IllegalArgumentException e3) {
            throw new CMSException("Malformed content.", e3);
        }
    }

    public static byte[] streamToByteArray(InputStream in) throws IOException {
        return Streams.readAll(in);
    }

    public static byte[] streamToByteArray(InputStream in, int limit) throws IOException {
        return Streams.readAllLimited(in, limit);
    }
}
