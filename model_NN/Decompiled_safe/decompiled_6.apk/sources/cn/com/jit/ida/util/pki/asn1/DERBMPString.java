package cn.com.jit.ida.util.pki.asn1;

import java.io.IOException;

public class DERBMPString extends DERObject implements DERString {
    String string;

    public static DERBMPString getInstance(Object obj) {
        if (obj == null || (obj instanceof DERBMPString)) {
            return (DERBMPString) obj;
        }
        if (obj instanceof ASN1OctetString) {
            return new DERBMPString(((ASN1OctetString) obj).getOctets());
        }
        if (obj instanceof ASN1TaggedObject) {
            return getInstance(((ASN1TaggedObject) obj).getObject());
        }
        throw new IllegalArgumentException("illegal object in getInstance: " + obj.getClass().getName());
    }

    public static DERBMPString getInstance(ASN1TaggedObject obj, boolean explicit) {
        return getInstance(obj.getObject());
    }

    public DERBMPString(byte[] string2) {
        char[] cs = new char[(string2.length / 2)];
        for (int i = 0; i != cs.length; i++) {
            cs[i] = (char) ((string2[i * 2] << 8) | (string2[(i * 2) + 1] & 255));
        }
        this.string = new String(cs);
    }

    public DERBMPString(String string2) {
        this.string = string2;
    }

    public String getString() {
        return this.string;
    }

    public int hashCode() {
        return getString().hashCode();
    }

    public boolean equals(Object o) {
        if (!(o instanceof DERBMPString)) {
            return false;
        }
        return getString().equals(((DERBMPString) o).getString());
    }

    /* access modifiers changed from: package-private */
    public void encode(DEROutputStream out) throws IOException {
        char[] c = this.string.toCharArray();
        byte[] b = new byte[(c.length * 2)];
        for (int i = 0; i != c.length; i++) {
            b[i * 2] = (byte) (c[i] >> 8);
            b[(i * 2) + 1] = (byte) c[i];
        }
        out.writeEncoded(30, b);
    }
}
