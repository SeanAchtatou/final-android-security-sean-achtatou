package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Set;
import cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERPrintableString;
import cn.com.jit.ida.util.pki.asn1.DERSet;
import cn.com.jit.ida.util.pki.asn1.DERTaggedObject;
import cn.com.jit.ida.util.pki.asn1.DERUTF8String;

public class JITIdentifyCode implements DEREncodable {
    private DERUTF8String militaryOfficerCardNumber = null;
    private DERPrintableString passportNumber = null;
    private DERPrintableString residenterCardNumber = null;
    private ASN1Set set = null;

    public JITIdentifyCode(ASN1Set set2) {
        this.set = set2;
        for (int i = 0; i < set2.size(); i++) {
            ASN1TaggedObject tagObj = (ASN1TaggedObject) set2.getObjectAt(i);
            switch (tagObj.getTagNo()) {
                case 0:
                    this.residenterCardNumber = DERPrintableString.getInstance(tagObj);
                    break;
                case 1:
                    this.militaryOfficerCardNumber = DERUTF8String.getInstance(tagObj);
                    break;
                case 2:
                    this.passportNumber = DERPrintableString.getInstance(tagObj);
                    break;
            }
        }
    }

    public JITIdentifyCode(DERPrintableString residenterCardNumber2, DERUTF8String militaryOfficerCardNumber2, DERPrintableString passportNumber2) {
        this.residenterCardNumber = residenterCardNumber2;
        this.militaryOfficerCardNumber = militaryOfficerCardNumber2;
        this.passportNumber = passportNumber2;
        ASN1EncodableVector v = new ASN1EncodableVector();
        if (!(residenterCardNumber2 == null || residenterCardNumber2.getString() == null)) {
            v.add(new DERTaggedObject(0, residenterCardNumber2));
        }
        if (!(militaryOfficerCardNumber2 == null || militaryOfficerCardNumber2.getString() == null)) {
            v.add(new DERTaggedObject(1, militaryOfficerCardNumber2));
        }
        if (!(passportNumber2 == null || passportNumber2.getString() == null)) {
            v.add(new DERTaggedObject(2, passportNumber2));
        }
        this.set = new DERSet(v);
    }

    public static JITIdentifyCode getInstance(Object obj) {
        if (obj == null || (obj instanceof JITIdentifyCode)) {
            return (JITIdentifyCode) obj;
        }
        if (obj instanceof ASN1Set) {
            return new JITIdentifyCode((ASN1Set) obj);
        }
        throw new IllegalArgumentException("Invalid JITIdentifyCode: " + obj.getClass().getName());
    }

    public DERPrintableString getResidenterCardNumber() {
        if (this.residenterCardNumber != null) {
            return this.residenterCardNumber;
        }
        return null;
    }

    public DERUTF8String getMilitaryOfficerCardNumber() {
        if (this.militaryOfficerCardNumber != null) {
            return this.militaryOfficerCardNumber;
        }
        return null;
    }

    public DERPrintableString getPassportNumber() {
        if (this.passportNumber != null) {
            return this.passportNumber;
        }
        return null;
    }

    public DERObject getDERObject() {
        return this.set;
    }
}
