package cn.com.jit.ida.util.pki.cipher.softsm;

import org.bouncycastle.crypto.Digest;

public abstract class GeneralDigest implements Digest {
    private int BYTE_LENGTH;
    private long byteCount;
    private byte[] xBuf;
    private int xBufOff;

    public abstract void ProcessBlock();

    public abstract void ProcessLength(long j);

    public abstract void ProcessWord(byte[] bArr, int i);

    public abstract int doFinal(byte[] bArr, int i);

    public abstract String getAlgorithmName();

    public abstract int getDigestSize();

    public abstract void update(byte[] bArr, int i, int i2);

    public GeneralDigest() {
        this.BYTE_LENGTH = 64;
        this.xBuf = new byte[4];
    }

    public GeneralDigest(GeneralDigest t) {
        this.BYTE_LENGTH = 64;
        this.xBuf = new byte[t.xBuf.length];
        System.arraycopy(t.xBuf, 0, this.xBuf, 0, t.xBuf.length);
        this.xBufOff = t.xBufOff;
        this.byteCount = t.byteCount;
    }

    public void update(byte input) {
        byte[] bArr = this.xBuf;
        int i = this.xBufOff;
        this.xBufOff = i + 1;
        bArr[i] = input;
        if (this.xBufOff == this.xBuf.length) {
            ProcessWord(this.xBuf, 0);
            this.xBufOff = 0;
        }
        this.byteCount++;
    }

    public void BlockUpdate(byte[] input, int inOff, int length) {
        while (this.xBufOff != 0 && length > 0) {
            update(input[inOff]);
            inOff++;
            length--;
        }
        while (length > this.xBuf.length) {
            ProcessWord(input, inOff);
            inOff += this.xBuf.length;
            length -= this.xBuf.length;
            this.byteCount += (long) this.xBuf.length;
        }
        while (length > 0) {
            update(input[inOff]);
            inOff++;
            length--;
        }
    }

    public void Finish() {
        long bitLength = this.byteCount << 3;
        update(Byte.MIN_VALUE);
        while (this.xBufOff != 0) {
            update((byte) 0);
        }
        ProcessLength(bitLength);
        ProcessBlock();
    }

    public void reset() {
        this.byteCount = 0;
        this.xBufOff = 0;
        for (int i = 0; i < this.xBuf.length; i++) {
            this.xBuf[i] = 0;
        }
    }

    public int GetByteLength() {
        return this.BYTE_LENGTH;
    }
}
