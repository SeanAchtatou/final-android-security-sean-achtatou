package cn.com.jit.ida.util.pki;

import java.io.ByteArrayInputStream;
import java.math.BigInteger;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERBitString;
import org.bouncycastle.asn1.DERInteger;
import org.bouncycastle.asn1.DERObject;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.asn1.x9.X9ObjectIdentifiers;
import org.bouncycastle.util.encoders.Hex;

public class ECDSAParser {
    public static byte[] customData2SoftPublicKey(byte[] eccPublicKey, byte[] ecParams) throws PKIException {
        byte[] pubKey = new byte[(eccPublicKey.length + 1)];
        pubKey[0] = 4;
        System.arraycopy(eccPublicKey, 0, pubKey, 1, eccPublicKey.length);
        return new SubjectPublicKeyInfo(new AlgorithmIdentifier(X9ObjectIdentifiers.id_ecPublicKey, new DERObjectIdentifier("1.2.156.10197.1.301")), pubKey).getDEREncoded();
    }

    public static byte[] softKey2HardCustomKey(byte[] softKey) throws PKIException {
        try {
            byte[] pubKeyData = new SubjectPublicKeyInfo((ASN1Sequence) new ASN1InputStream(new ByteArrayInputStream(softKey)).readObject()).getPublicKeyData().getBytes();
            byte[] pubKeyDataResult = new byte[(pubKeyData.length - 1)];
            System.arraycopy(pubKeyData, 1, pubKeyDataResult, 0, pubKeyDataResult.length);
            byte[] pubKeyData2 = Hex.encode(pubKeyDataResult);
            return null;
        } catch (Exception ex) {
            throw new PKIException("8137", PKIException.BYTES_DEROBJ_DES, ex);
        }
    }

    public static byte[] customData2SoftSignData(byte[] hardSignData) throws PKIException {
        int signDataLen = hardSignData.length / 2;
        byte[] b_r = new byte[signDataLen];
        byte[] b_s = new byte[signDataLen];
        System.arraycopy(hardSignData, 0, b_r, 0, signDataLen);
        System.arraycopy(hardSignData, signDataLen, b_s, 0, signDataLen);
        BigInteger r = new BigInteger(b_r);
        BigInteger s = new BigInteger(b_s);
        DERInteger d_r = new DERInteger(r);
        DERInteger d_s = new DERInteger(s);
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(d_r);
        v.add(d_s);
        return new DERSequence(v).getDEREncoded();
    }

    public static byte[] customData2SoftECPrivKey(byte[] EcPoint, byte[] ecValue) throws PKIException {
        DERBitString pubkey = new DERBitString(EcPoint);
        DERInteger version = new DERInteger(1);
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(version);
        if (ecValue != null) {
            v.add(new DEROctetString(ecValue));
        }
        v.add(pubkey);
        return new DERSequence(v).getDEREncoded();
    }

    public static byte[] sm2_softKey2HardCustomKey(byte[] softKey) throws PKIException {
        byte[] derecpoint;
        try {
            DERObject derObj = new ASN1InputStream(new ByteArrayInputStream(softKey)).readObject();
            new DERSequence(derObj);
            ASN1Sequence asn1 = DERSequence.getInstance(derObj);
            byte[] derprvkey = null;
            if (2 == asn1.size()) {
                derecpoint = asn1.getObjectAt(1).getBytes();
            } else {
                derprvkey = asn1.getObjectAt(1).getOctets();
                derecpoint = asn1.getObjectAt(2).getBytes();
            }
            byte[] derecpoint2 = Hex.encode(derecpoint);
            if (derprvkey != null) {
                derprvkey = Hex.encode(derprvkey);
            }
            if (derprvkey != null) {
                byte[] key = new byte[(derprvkey.length + 12 + derecpoint2.length)];
                System.arraycopy("2".getBytes(), 0, key, 0, "2".getBytes().length);
                byte[] derprvkeyLen = Integer.toString(derprvkey.length).getBytes();
                byte[] derecpointLen = Integer.toString(derecpoint2.length).getBytes();
                System.arraycopy(derprvkeyLen, 0, key, 2, derprvkeyLen.length);
                System.arraycopy(derprvkey, 0, key, 7, derprvkey.length);
                System.arraycopy(derecpointLen, 0, key, derprvkey.length + 7, derecpointLen.length);
                System.arraycopy(derecpoint2, 0, key, derprvkey.length + 12, derecpoint2.length);
                return key;
            }
            byte[] key2 = new byte[(derecpoint2.length + 12)];
            System.arraycopy("1".getBytes(), 0, key2, 0, "1".getBytes().length);
            byte[] derecpointLen2 = Integer.toString(derecpoint2.length).getBytes();
            System.arraycopy(derecpointLen2, 0, key2, 2, derecpointLen2.length);
            System.arraycopy(derecpoint2, 0, key2, 7, derecpoint2.length);
            return key2;
        } catch (Exception ex) {
            throw new PKIException("8137", PKIException.BYTES_DEROBJ_DES, ex);
        }
    }

    public static void main(String[] args) {
    }
}
