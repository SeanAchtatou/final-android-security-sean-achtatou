package cn.com.jit.ida.util.pki.extension;

import cn.com.jit.ida.util.pki.asn1.DERIA5String;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.x509.DistributionPoint;
import cn.com.jit.ida.util.pki.asn1.x509.DistributionPointName;
import cn.com.jit.ida.util.pki.asn1.x509.GeneralName;
import cn.com.jit.ida.util.pki.asn1.x509.GeneralNames;
import cn.com.jit.ida.util.pki.asn1.x509.ReasonFlags;
import cn.com.jit.ida.util.pki.asn1.x509.X509Name;
import com.jianq.misc.StringEx;
import java.util.Vector;

public class DistributionPointExt {
    public static final int DN_TYPE_NAME = 4;
    public static final String REASON_AA_COMPROMISE = "aaCompromise";
    public static final String REASON_AFFILIATION_CHANGED = "affiliationChanged";
    public static final String REASON_CA_COMPROMISE = "caCompromise";
    public static final String REASON_CERTIFICATE_HOLD = "certificateHold";
    public static final String REASON_CESSATION_OF_OPERATION = "cessationOfOperation";
    public static final String REASON_KEY_COMPROMISE = "keyCompromise";
    public static final String REASON_PRIVILEGE_WITHDRAWN = "privilegeWithDrawn";
    public static final String REASON_SUPERSEDED = "superSeded";
    public static final String REASON_UNUSED = "unUsed";
    public static final int URI_TYPE_NAME = 6;
    private String cRLIssuer;
    private String dpName;
    private int nameType;
    private int reason;
    private Vector reasonVector;

    public DistributionPointExt() {
        this.dpName = null;
        this.nameType = -1;
        this.reasonVector = null;
        this.cRLIssuer = null;
        this.reason = 0;
        this.reason = 0;
        this.reasonVector = new Vector();
    }

    public void setDistributionPointName(int nameType2, String distributionPointName) {
        this.nameType = nameType2;
        this.dpName = distributionPointName;
    }

    public String getDistributionPointName() {
        return this.dpName;
    }

    public void addReasonFlags(String reasonFlags) {
        this.reasonVector.add(reasonFlags);
    }

    public void setReasonFlags(Vector reasonFlags) {
        for (int i = 0; i < reasonFlags.size(); i++) {
            this.reasonVector.add((String) reasonFlags.get(i));
        }
    }

    public Vector getReasonFlags() {
        return this.reasonVector;
    }

    public void setCRLIssuer(String cRLIssuer2) {
        this.cRLIssuer = cRLIssuer2;
    }

    public String getCRLIssuer() {
        return this.cRLIssuer;
    }

    private void generateReason() {
        if (this.reasonVector.contains(REASON_UNUSED)) {
            this.reason |= 128;
        }
        if (this.reasonVector.contains(REASON_KEY_COMPROMISE)) {
            this.reason |= 64;
        }
        if (this.reasonVector.contains(REASON_CA_COMPROMISE)) {
            this.reason |= 32;
        }
        if (this.reasonVector.contains(REASON_AFFILIATION_CHANGED)) {
            this.reason |= 16;
        }
        if (this.reasonVector.contains(REASON_SUPERSEDED)) {
            this.reason |= 8;
        }
        if (this.reasonVector.contains(REASON_CESSATION_OF_OPERATION)) {
            this.reason |= 4;
        }
        if (this.reasonVector.contains(REASON_CERTIFICATE_HOLD)) {
            this.reason |= 2;
        }
        if (this.reasonVector.contains(REASON_PRIVILEGE_WITHDRAWN)) {
            this.reason |= 1;
        }
        if (this.reasonVector.contains(REASON_AA_COMPROMISE)) {
            this.reason |= 32768;
        }
    }

    /* access modifiers changed from: package-private */
    public DistributionPoint encode() {
        DistributionPointName distPoiontName = null;
        if (this.dpName != null && !this.dpName.equals(StringEx.Empty)) {
            GeneralNames genNames = null;
            if (this.nameType == 6) {
                genNames = new GeneralNames(new DERSequence(new GeneralName(new DERIA5String(this.dpName.getBytes()), 6)));
            } else if (this.nameType == 4) {
                genNames = new GeneralNames(new DERSequence(new GeneralName(new X509Name(this.dpName))));
            }
            distPoiontName = new DistributionPointName(0, genNames);
        }
        ReasonFlags reasonFlags = null;
        generateReason();
        if (this.reason != 0) {
            reasonFlags = new ReasonFlags(this.reason);
        }
        GeneralNames crlIssuerName = null;
        if (this.cRLIssuer != null && !this.cRLIssuer.equals(StringEx.Empty)) {
            crlIssuerName = new GeneralNames(new DERSequence(new GeneralName(new X509Name(this.cRLIssuer))));
        }
        return new DistributionPoint(distPoiontName, reasonFlags, crlIssuerName);
    }
}
