package cn.com.jit.ida.util.pki.encoders;

import android.support.v4.view.MotionEventCompat;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class Base64 {
    private static final byte[] decodingTable = new byte[128];
    private static final byte[] encodingTable = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};

    static {
        for (int i = 65; i <= 90; i++) {
            decodingTable[i] = (byte) (i - 65);
        }
        for (int i2 = 97; i2 <= 122; i2++) {
            decodingTable[i2] = (byte) ((i2 - 97) + 26);
        }
        for (int i3 = 48; i3 <= 57; i3++) {
            decodingTable[i3] = (byte) ((i3 - 48) + 52);
        }
        decodingTable[43] = 62;
        decodingTable[47] = 63;
    }

    public static byte[] encode(byte[] data) {
        byte[] bytes;
        int modulus = data.length % 3;
        if (modulus == 0) {
            bytes = new byte[((data.length * 4) / 3)];
        } else {
            bytes = new byte[(((data.length / 3) + 1) * 4)];
        }
        int dataLength = data.length - modulus;
        int i = 0;
        int j = 0;
        while (i < dataLength) {
            int a1 = data[i] & MotionEventCompat.ACTION_MASK;
            int a2 = data[i + 1] & MotionEventCompat.ACTION_MASK;
            int a3 = data[i + 2] & MotionEventCompat.ACTION_MASK;
            bytes[j] = encodingTable[(a1 >>> 2) & 63];
            bytes[j + 1] = encodingTable[((a1 << 4) | (a2 >>> 4)) & 63];
            bytes[j + 2] = encodingTable[((a2 << 2) | (a3 >>> 6)) & 63];
            bytes[j + 3] = encodingTable[a3 & 63];
            i += 3;
            j += 4;
        }
        switch (modulus) {
            case 1:
                int d1 = data[data.length - 1] & MotionEventCompat.ACTION_MASK;
                bytes[bytes.length - 4] = encodingTable[(d1 >>> 2) & 63];
                bytes[bytes.length - 3] = encodingTable[(d1 << 4) & 63];
                bytes[bytes.length - 2] = 61;
                bytes[bytes.length - 1] = 61;
                break;
            case 2:
                int d12 = data[data.length - 2] & MotionEventCompat.ACTION_MASK;
                int d2 = data[data.length - 1] & MotionEventCompat.ACTION_MASK;
                bytes[bytes.length - 4] = encodingTable[(d12 >>> 2) & 63];
                bytes[bytes.length - 3] = encodingTable[((d12 << 4) | (d2 >>> 4)) & 63];
                bytes[bytes.length - 2] = encodingTable[(d2 << 2) & 63];
                bytes[bytes.length - 1] = 61;
                break;
        }
        return bytes;
    }

    public static byte[] decode(byte[] data) {
        byte[] bytes;
        if (data[data.length - 2] == 61) {
            bytes = new byte[((((data.length / 4) - 1) * 3) + 1)];
        } else if (data[data.length - 1] == 61) {
            bytes = new byte[((((data.length / 4) - 1) * 3) + 2)];
        } else {
            bytes = new byte[((data.length / 4) * 3)];
        }
        int i = 0;
        int j = 0;
        while (i < data.length - 4) {
            byte b1 = decodingTable[data[i]];
            byte b2 = decodingTable[data[i + 1]];
            byte b3 = decodingTable[data[i + 2]];
            byte b4 = decodingTable[data[i + 3]];
            bytes[j] = (byte) ((b1 << 2) | (b2 >> 4));
            bytes[j + 1] = (byte) ((b2 << 4) | (b3 >> 2));
            bytes[j + 2] = (byte) ((b3 << 6) | b4);
            i += 4;
            j += 3;
        }
        if (data[data.length - 2] == 61) {
            bytes[bytes.length - 1] = (byte) ((decodingTable[data[data.length - 4]] << 2) | (decodingTable[data[data.length - 3]] >> 4));
        } else if (data[data.length - 1] == 61) {
            byte b12 = decodingTable[data[data.length - 4]];
            byte b22 = decodingTable[data[data.length - 3]];
            byte b32 = decodingTable[data[data.length - 2]];
            bytes[bytes.length - 2] = (byte) ((b12 << 2) | (b22 >> 4));
            bytes[bytes.length - 1] = (byte) ((b22 << 4) | (b32 >> 2));
        } else {
            byte b13 = decodingTable[data[data.length - 4]];
            byte b23 = decodingTable[data[data.length - 3]];
            byte b33 = decodingTable[data[data.length - 2]];
            byte b42 = decodingTable[data[data.length - 1]];
            bytes[bytes.length - 3] = (byte) ((b13 << 2) | (b23 >> 4));
            bytes[bytes.length - 2] = (byte) ((b23 << 4) | (b33 >> 2));
            bytes[bytes.length - 1] = (byte) ((b33 << 6) | b42);
        }
        return bytes;
    }

    private static boolean ignore(char c) {
        return c == 10 || c == 13 || c == 9 || c == ' ';
    }

    public static byte[] decode(String data) {
        ByteArrayOutputStream bOut = new ByteArrayOutputStream();
        try {
            decode(data, bOut);
            return bOut.toByteArray();
        } catch (IOException e) {
            throw new RuntimeException("exception decoding base64 string: " + e);
        }
    }

    public static int decode(String data, OutputStream out) throws IOException {
        int length = 0;
        int end = data.length();
        while (end > 0 && ignore(data.charAt(end - 1))) {
            end--;
        }
        for (int i = 0; i < end - 4; i += 4) {
            if (!ignore(data.charAt(i))) {
                byte b1 = decodingTable[data.charAt(i)];
                byte b2 = decodingTable[data.charAt(i + 1)];
                byte b3 = decodingTable[data.charAt(i + 2)];
                byte b4 = decodingTable[data.charAt(i + 3)];
                out.write((b1 << 2) | (b2 >> 4));
                out.write((b2 << 4) | (b3 >> 2));
                out.write((b3 << 6) | b4);
                length += 3;
            }
        }
        if (data.charAt(end - 2) == '=') {
            out.write((decodingTable[data.charAt(end - 4)] << 2) | (decodingTable[data.charAt(end - 3)] >> 4));
            return length + 1;
        } else if (data.charAt(end - 1) == '=') {
            byte b12 = decodingTable[data.charAt(end - 4)];
            byte b22 = decodingTable[data.charAt(end - 3)];
            byte b32 = decodingTable[data.charAt(end - 2)];
            out.write((b12 << 2) | (b22 >> 4));
            out.write((b22 << 4) | (b32 >> 2));
            return length + 2;
        } else {
            byte b13 = decodingTable[data.charAt(end - 4)];
            byte b23 = decodingTable[data.charAt(end - 3)];
            byte b33 = decodingTable[data.charAt(end - 2)];
            byte b42 = decodingTable[data.charAt(end - 1)];
            out.write((b13 << 2) | (b23 >> 4));
            out.write((b23 << 4) | (b33 >> 2));
            out.write((b33 << 6) | b42);
            return length + 3;
        }
    }

    public static void main(String[] args) {
        System.out.println(decode("1.1.4".toString().getBytes()));
    }
}
