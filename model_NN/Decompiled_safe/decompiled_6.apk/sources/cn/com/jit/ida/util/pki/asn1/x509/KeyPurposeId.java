package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;

public class KeyPurposeId extends DERObjectIdentifier {
    public static final KeyPurposeId anyExtendedKeyUsage = new KeyPurposeId(X509Extensions.ExtendedKeyUsage.getId() + ".0");
    private static final String id_kp = "1.3.6.1.5.5.7.3";
    public static final KeyPurposeId id_kp_OCSPSigning = new KeyPurposeId("1.3.6.1.5.5.7.3.9");
    public static final KeyPurposeId id_kp_clientAuth = new KeyPurposeId("1.3.6.1.5.5.7.3.2");
    public static final KeyPurposeId id_kp_codeSigning = new KeyPurposeId("1.3.6.1.5.5.7.3.3");
    public static final KeyPurposeId id_kp_emailProtection = new KeyPurposeId("1.3.6.1.5.5.7.3.4");
    public static final KeyPurposeId id_kp_encryptedFileSystem = new KeyPurposeId("1.3.6.1.4.1.311.10.3.4");
    public static final KeyPurposeId id_kp_ipsecEndSystem = new KeyPurposeId("1.3.6.1.5.5.7.3.5");
    public static final KeyPurposeId id_kp_ipsecTunnel = new KeyPurposeId("1.3.6.1.5.5.7.3.6");
    public static final KeyPurposeId id_kp_ipsecUser = new KeyPurposeId("1.3.6.1.5.5.7.3.7");
    public static final KeyPurposeId id_kp_serverAuth = new KeyPurposeId("1.3.6.1.5.5.7.3.1");
    public static final KeyPurposeId id_kp_smartcardlogon = new KeyPurposeId("1.3.6.1.4.1.311.20.2.2");
    public static final KeyPurposeId id_kp_timeStamping = new KeyPurposeId("1.3.6.1.5.5.7.3.8");

    private KeyPurposeId(String id) {
        super(id);
    }
}
