package cn.com.jit.ida.util.pki.extension;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.x509.CRLDistPoint;
import cn.com.jit.ida.util.pki.asn1.x509.DistributionPoint;
import cn.com.jit.ida.util.pki.asn1.x509.X509Extensions;

public class CRLDistPointExt extends AbstractStandardExtension {
    private ASN1Sequence crldistpointsep = null;
    private DistributionPointExt[] dps = null;

    public CRLDistPointExt() {
        this.OID = X509Extensions.CRLDistributionPoints.getId();
        this.critical = false;
    }

    public CRLDistPointExt(ASN1Sequence asn1Sequence) {
        this.OID = X509Extensions.CRLDistributionPoints.getId();
        this.critical = false;
        this.crldistpointsep = asn1Sequence;
    }

    public void setDistributionPointExts(DistributionPointExt[] distributionPointExts) {
        this.dps = distributionPointExts;
    }

    public DistributionPointExt[] getDistributionPointExts() {
        return this.dps;
    }

    public String getOID() {
        return this.OID;
    }

    public void setCritical(boolean critical) {
        this.critical = critical;
    }

    public boolean getCritical() {
        return this.critical;
    }

    public byte[] encode() throws PKIException {
        DistributionPoint[] dp = new DistributionPoint[this.dps.length];
        for (int i = 0; i < this.dps.length; i++) {
            dp[i] = this.dps[i].encode();
        }
        return new DEROctetString(new CRLDistPoint(dp).getDERObject()).getOctets();
    }
}
