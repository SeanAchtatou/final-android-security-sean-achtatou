package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.ASN1Encodable;
import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.DERBitString;
import cn.com.jit.ida.util.pki.asn1.DERGeneralizedTime;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.DERSet;

public class V2AttributeCertificateInfoGenerator {
    private AttCertValidityPeriod attrCertValidityPeriod;
    private Attributes attributes = new Attributes();
    private DERGeneralizedTime endDate;
    private X509Extensions extensions;
    private Holder holder;
    private AttCertIssuer issuer;
    private DERBitString issuerUniqueID;
    private DERInteger serialNumber;
    private AlgorithmIdentifier signature;
    private DERGeneralizedTime startDate;
    private DERInteger version = new DERInteger(1);

    public void setHolder(Holder holder2) {
        this.holder = holder2;
    }

    public void addAttribute(String oid, ASN1Encodable value) {
        this.attributes.addAttribute(new Attribute(new DERObjectIdentifier(oid), new DERSet(value)));
    }

    public void addAttribute(Attribute attribute) {
        this.attributes.addAttribute(attribute);
    }

    public void setSerialNumber(DERInteger serialNumber2) {
        this.serialNumber = serialNumber2;
    }

    public void setSignature(AlgorithmIdentifier signature2) {
        this.signature = signature2;
    }

    public void setIssuer(AttCertIssuer issuer2) {
        this.issuer = issuer2;
    }

    public void setStartDate(DERGeneralizedTime startDate2) {
        this.startDate = startDate2;
    }

    public void setEndDate(DERGeneralizedTime endDate2) {
        this.endDate = endDate2;
    }

    public void setIssuerUniqueID(DERBitString issuerUniqueID2) {
        this.issuerUniqueID = issuerUniqueID2;
    }

    public void setExtensions(X509Extensions extensions2) {
        this.extensions = extensions2;
    }

    public AttributeCertificateInfo generateAttributeCertificateInfo() {
        if (this.serialNumber == null || this.signature == null || this.issuer == null || this.startDate == null || this.endDate == null || this.holder == null || this.attributes == null) {
            throw new IllegalStateException("not all mandatory fields set in V2 AttributeCertificateInfo generator");
        }
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(this.version);
        v.add(this.holder);
        v.add(this.issuer);
        v.add(this.signature);
        v.add(this.serialNumber);
        v.add(new AttCertValidityPeriod(this.startDate, this.endDate));
        v.add(this.attributes.getDERObject());
        if (this.issuerUniqueID != null) {
            v.add(this.issuerUniqueID);
        }
        if (this.extensions != null) {
            v.add(this.extensions);
        }
        return new AttributeCertificateInfo(new DERSequence(v));
    }
}
