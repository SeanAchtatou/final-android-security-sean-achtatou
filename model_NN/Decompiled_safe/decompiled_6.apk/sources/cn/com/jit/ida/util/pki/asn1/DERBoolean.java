package cn.com.jit.ida.util.pki.asn1;

import java.io.IOException;

public class DERBoolean extends DERObject {
    public static final DERBoolean FALSE = new DERBoolean(false);
    public static final DERBoolean TRUE = new DERBoolean(true);
    byte value;

    public static DERBoolean getInstance(Object obj) {
        if (obj == null || (obj instanceof DERBoolean)) {
            return (DERBoolean) obj;
        }
        if (obj instanceof ASN1OctetString) {
            return new DERBoolean(((ASN1OctetString) obj).getOctets());
        }
        if (obj instanceof ASN1TaggedObject) {
            return getInstance(((ASN1TaggedObject) obj).getObject());
        }
        throw new IllegalArgumentException("illegal object in getInstance: " + obj.getClass().getName());
    }

    public static DERBoolean getInstance(boolean value2) {
        return value2 ? TRUE : FALSE;
    }

    public static DERBoolean getInstance(ASN1TaggedObject obj, boolean explicit) {
        return getInstance(obj.getObject());
    }

    public DERBoolean(byte[] value2) {
        this.value = value2[0];
    }

    public DERBoolean(boolean value2) {
        this.value = value2 ? (byte) -1 : 0;
    }

    public boolean isTrue() {
        return this.value != 0;
    }

    /* access modifiers changed from: package-private */
    public void encode(DEROutputStream out) throws IOException {
        out.writeEncoded(1, new byte[]{this.value});
    }

    public boolean equals(Object o) {
        if (o == null || !(o instanceof DERBoolean) || this.value != ((DERBoolean) o).value) {
            return false;
        }
        return true;
    }
}
