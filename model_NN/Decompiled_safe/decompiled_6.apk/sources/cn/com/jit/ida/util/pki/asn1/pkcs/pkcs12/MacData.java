package cn.com.jit.ida.util.pki.asn1.pkcs.pkcs12;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1OctetString;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.x509.DigestInfo;
import java.math.BigInteger;

public class MacData implements DEREncodable {
    private DigestInfo digInfo;
    private BigInteger iterationCount;
    private byte[] salt;

    public static MacData getInstance(Object obj) {
        if (obj instanceof MacData) {
            return (MacData) obj;
        }
        if (obj instanceof ASN1Sequence) {
            return new MacData((ASN1Sequence) obj);
        }
        throw new IllegalArgumentException("unknown object in factory " + obj.getClass().getName());
    }

    public MacData(ASN1Sequence seq) {
        this.digInfo = DigestInfo.getInstance(seq.getObjectAt(0));
        this.salt = ((ASN1OctetString) seq.getObjectAt(1)).getOctets();
        if (seq.size() == 3) {
            this.iterationCount = ((DERInteger) seq.getObjectAt(2)).getValue();
        } else {
            this.iterationCount = BigInteger.valueOf(1);
        }
    }

    public MacData(DigestInfo digInfo2, byte[] salt2, int iterationCount2) {
        this.digInfo = digInfo2;
        this.salt = salt2;
        this.iterationCount = BigInteger.valueOf((long) iterationCount2);
    }

    public DigestInfo getMac() {
        return this.digInfo;
    }

    public byte[] getSalt() {
        return this.salt;
    }

    public BigInteger getIterationCount() {
        return this.iterationCount;
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(this.digInfo);
        v.add(new DEROctetString(this.salt));
        v.add(new DERInteger(this.iterationCount));
        return new DERSequence(v);
    }
}
