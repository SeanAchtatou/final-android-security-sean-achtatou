package cn.com.jit.ida.util.pki.asn1;

import cn.com.jit.ida.util.pki.PKIException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Vector;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

public abstract class ASN1OctetString extends DERObject {
    byte[] string;

    /* access modifiers changed from: package-private */
    public abstract void encode(DEROutputStream dEROutputStream) throws IOException;

    public static ASN1OctetString getInstance(ASN1TaggedObject obj, boolean explicit) {
        return getInstance(obj.getObject());
    }

    public static ASN1OctetString getInstance(Object obj) {
        if (obj == null || (obj instanceof ASN1OctetString)) {
            return (ASN1OctetString) obj;
        }
        if (obj instanceof ASN1TaggedObject) {
            return getInstance(((ASN1TaggedObject) obj).getObject());
        }
        if (obj instanceof ASN1Sequence) {
            Vector v = new Vector();
            Enumeration e = ((ASN1Sequence) obj).getObjects();
            while (e.hasMoreElements()) {
                v.addElement(e.nextElement());
            }
            return new BERConstructedOctetString(v);
        }
        throw new IllegalArgumentException("illegal object in getInstance: " + obj.getClass().getName());
    }

    public ASN1OctetString(byte[] string2) {
        this.string = string2;
    }

    public ASN1OctetString(DEREncodable obj) {
        try {
            ByteArrayOutputStream bOut = new ByteArrayOutputStream();
            DEROutputStream dOut = new DEROutputStream(bOut);
            dOut.writeObject(obj);
            dOut.close();
            this.string = bOut.toByteArray();
        } catch (IOException e) {
            throw new IllegalArgumentException("Error processing object : " + e.toString());
        }
    }

    public byte[] getOctets() {
        return this.string;
    }

    public int hashCode() {
        byte[] b = getOctets();
        int value = 0;
        for (int i = 0; i != b.length; i++) {
            value ^= (b[i] & 255) << (i % 4);
        }
        return value;
    }

    public boolean equals(Object o) {
        if (o == null || !(o instanceof DEROctetString)) {
            return false;
        }
        byte[] b1 = ((DEROctetString) o).getOctets();
        byte[] b2 = getOctets();
        if (b1.length != b2.length) {
            return false;
        }
        for (int i = 0; i != b1.length; i++) {
            if (b1[i] != b2[i]) {
                return false;
            }
        }
        return true;
    }

    public ASN1OctetString(Node nl) throws PKIException {
        this.string = ((Text) nl.getFirstChild()).getData().trim().getBytes();
    }
}
