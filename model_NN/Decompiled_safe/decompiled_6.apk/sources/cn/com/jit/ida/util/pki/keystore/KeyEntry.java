package cn.com.jit.ida.util.pki.keystore;

import cn.com.jit.ida.util.pki.cert.X509Cert;
import cn.com.jit.ida.util.pki.cipher.JKey;

public class KeyEntry {
    private String ailas;
    private X509Cert cert;
    private JKey key;

    public X509Cert getCert() {
        return this.cert;
    }

    public void setCert(X509Cert cert2) {
        this.cert = cert2;
    }

    public JKey getKey() {
        return this.key;
    }

    public void setKey(JKey key2) {
        this.key = key2;
    }

    public String getAilas() {
        return this.ailas;
    }

    public void setAilas(String ailas2) {
        this.ailas = ailas2;
    }
}
