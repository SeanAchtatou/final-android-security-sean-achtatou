package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import java.util.Enumeration;
import java.util.Vector;

public class CertificatePolicies implements DEREncodable {
    static final DERObjectIdentifier anyPolicy = new DERObjectIdentifier("2.5.29.32.0");
    Vector policies;

    public static CertificatePolicies getInstance(ASN1TaggedObject obj, boolean explicit) {
        return getInstance(ASN1Sequence.getInstance(obj, explicit));
    }

    public static CertificatePolicies getInstance(Object obj) {
        if (obj instanceof CertificatePolicies) {
            return (CertificatePolicies) obj;
        }
        if (obj instanceof ASN1Sequence) {
            return new CertificatePolicies((ASN1Sequence) obj);
        }
        throw new IllegalArgumentException("unknown object in factory");
    }

    public CertificatePolicies(ASN1Sequence seq) {
        this.policies = new Vector();
        Enumeration e = seq.getObjects();
        while (e.hasMoreElements()) {
            this.policies.addElement((ASN1Sequence) e.nextElement());
        }
    }

    public CertificatePolicies(DERObjectIdentifier p) {
        this.policies = new Vector();
        this.policies.addElement(p);
    }

    public CertificatePolicies(String p) {
        this(new DERObjectIdentifier(p));
    }

    public void addPolicy(String p) {
        this.policies.addElement(new DERObjectIdentifier(p));
    }

    public String getPolicy(int nr) {
        if (this.policies.size() > nr) {
            return ((DERObjectIdentifier) this.policies.elementAt(nr)).getId();
        }
        return null;
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        for (int i = 0; i < this.policies.size(); i++) {
            v.add(new PolicyInformation((ASN1Sequence) this.policies.get(i)).toASN1Object());
        }
        return new DERSequence(v);
    }

    public String toString() {
        String p = null;
        for (int i = 0; i < this.policies.size(); i++) {
            if (p != null) {
                p = p + ", ";
            }
            p = p + ((DERObjectIdentifier) this.policies.elementAt(i)).getId();
        }
        return "CertificatePolicies: " + p;
    }
}
