package cn.com.jit.ida.util.pki.cms;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.Parser;
import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1InputStream;
import cn.com.jit.ida.util.pki.asn1.ASN1OctetString;
import cn.com.jit.ida.util.pki.asn1.ASN1OutputStream;
import cn.com.jit.ida.util.pki.asn1.ASN1Set;
import cn.com.jit.ida.util.pki.asn1.BERConstructedOctetString;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERNull;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.DEROutputStream;
import cn.com.jit.ida.util.pki.asn1.DERSet;
import cn.com.jit.ida.util.pki.asn1.DERUTCTime;
import cn.com.jit.ida.util.pki.asn1.cms.Attribute;
import cn.com.jit.ida.util.pki.asn1.cms.AttributeTable;
import cn.com.jit.ida.util.pki.asn1.cms.CMSAttributes;
import cn.com.jit.ida.util.pki.asn1.cms.ContentInfo;
import cn.com.jit.ida.util.pki.asn1.cms.IssuerAndSerialNumber;
import cn.com.jit.ida.util.pki.asn1.cms.SignedData;
import cn.com.jit.ida.util.pki.asn1.cms.SignerIdentifier;
import cn.com.jit.ida.util.pki.asn1.cms.SignerInfo;
import cn.com.jit.ida.util.pki.asn1.cms.Time;
import cn.com.jit.ida.util.pki.asn1.pkcs.PKCSObjectIdentifiers;
import cn.com.jit.ida.util.pki.asn1.x509.AlgorithmIdentifier;
import cn.com.jit.ida.util.pki.asn1.x509.SubjectKeyIdentifier;
import cn.com.jit.ida.util.pki.asn1.x509.TBSCertificateStructure;
import cn.com.jit.ida.util.pki.cert.X509Cert;
import cn.com.jit.ida.util.pki.cipher.JKey;
import cn.com.jit.ida.util.pki.cipher.Mechanism;
import cn.com.jit.ida.util.pki.cipher.Session;
import cn.com.jit.ida.util.pki.crl.X509CRL;
import cn.com.jit.ida.util.pki.encoders.Base64;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.PublicKey;
import java.security.cert.CertStore;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateFactory;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.CollectionCertStoreParameters;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

public class CMSSignedData {
    private ASN1EncodableVector certs = new ASN1EncodableVector();
    private ASN1EncodableVector clss = new ASN1EncodableVector();
    private byte[] msg = null;
    private Session session = null;
    private SignedData signedData = null;
    private ArrayList signers = new ArrayList();

    private CMSSignedData() {
    }

    public CMSSignedData(Session value) {
        this.session = value;
    }

    public void AddCert(X509Cert value) throws PKIException {
        this.certs.add(Parser.convertJITCertStruct2BCCertStruct(value.getCertStructure()));
    }

    public void AddCRL(X509CRL value) throws PKIException {
        this.clss.add(Parser.convertJITCertList2BCCertList(value.getCertificateList()));
    }

    public void AddSigner(JKey privatekey, X509Cert cert, Mechanism sign_Mechanism) {
        this.signers.add(new Signer(this.session, privatekey, cert, sign_Mechanism));
    }

    public void AddSigner(JKey privatekey, X509Cert cert, Mechanism sign_Mechanism, AttributeTable sAttr, AttributeTable unsAttr) {
        this.signers.add(new Signer(this.session, privatekey, cert, sign_Mechanism, sAttr, unsAttr));
    }

    public byte[] GetSignedDataForByte() throws PKIException, IOException {
        if (this.signedData == null) {
            return null;
        }
        ContentInfo contentInfo = new ContentInfo(PKCSObjectIdentifiers.signedData, this.signedData);
        ByteArrayOutputStream bOut = new ByteArrayOutputStream();
        DEROutputStream dOut = new DEROutputStream(bOut);
        dOut.writeObject(contentInfo);
        byte[] byteArray = bOut.toByteArray();
        dOut.flush();
        dOut.close();
        bOut.flush();
        bOut.close();
        return byteArray;
    }

    public InputStream GetSignedDataForInputStream() throws PKIException, IOException {
        return new ByteArrayInputStream(GetSignedDataForByte());
    }

    public SignedData GetSignedData() throws PKIException {
        return this.signedData;
    }

    public void GetSignedDataForFile(String FileName) throws PKIException, IOException {
        OutputStream fos = new FileOutputStream(FileName);
        fos.write(GetSignedDataForByte());
        fos.flush();
        fos.close();
    }

    public void Generate(byte[] content, boolean encapsulate, boolean setIssuerAndSN) throws PKIException, IOException {
        Generate(PKCSObjectIdentifiers.data.getId(), content, encapsulate, setIssuerAndSN);
    }

    public void Generate(byte[] content, boolean encapsulate, boolean addDefaultAttributes, boolean setIssuerAndSN) throws PKIException, IOException {
        Generate(PKCSObjectIdentifiers.data.getId(), content, encapsulate, addDefaultAttributes, setIssuerAndSN);
    }

    public void Generate(String signedContentType, byte[] content, boolean encapsulate, boolean setIssuerAndSN) throws PKIException, IOException {
        Generate(signedContentType, content, encapsulate, true, setIssuerAndSN);
    }

    public void Generate(String signedContentType, byte[] content, boolean encapsulate, boolean addDefaultAttributes, boolean setIssuerAndSN) throws PKIException, IOException {
        ContentInfo encInfo;
        if (this.msg == null || this.signedData == null) {
            ASN1EncodableVector digestAlgs = new ASN1EncodableVector();
            ASN1EncodableVector signerInfos = new ASN1EncodableVector();
            DERObjectIdentifier contentTypeOID = new DERObjectIdentifier(signedContentType);
            Iterator it = this.signers.iterator();
            while (it.hasNext()) {
                Signer signer = (Signer) it.next();
                digestAlgs.add(new AlgorithmIdentifier(new DERObjectIdentifier(signer.GetDigestTypeOID()), new DERNull()));
                signerInfos.add(signer.toSignerInfo(contentTypeOID, content, addDefaultAttributes, setIssuerAndSN));
            }
            DERSet certificates = new DERSet(this.certs);
            DERSet certrevlist = new DERSet(this.clss);
            if (encapsulate) {
                ByteArrayOutputStream bOut = new ByteArrayOutputStream();
                bOut.write(content);
                encInfo = new ContentInfo(contentTypeOID, new BERConstructedOctetString(bOut.toByteArray()));
                bOut.flush();
                bOut.close();
            } else {
                encInfo = new ContentInfo(contentTypeOID, null);
            }
            this.signedData = new SignedData(new DERSet(digestAlgs), encInfo, certificates, certrevlist, new DERSet(signerInfos));
            this.msg = content;
        }
    }

    public void load(byte[] data) throws PKIException {
        if (Parser.isBase64Encode(data)) {
            data = Base64.decode(Parser.convertBase64(data));
        }
        load(new ByteArrayInputStream(data));
    }

    public void load(InputStream ins) throws PKIException {
        ASN1InputStream ais = new ASN1InputStream(ins);
        try {
            this.signedData = SignedData.getInstance(ContentInfo.getInstance(ais.readObject()).getContent());
            ins.close();
            ais.close();
            this.msg = null;
        } catch (Exception ex) {
            throw new PKIException("8175", PKIException.PARSE_P7_SIGNEDDATA_ERR_DES, ex);
        }
    }

    public void load(SignedData signedData2) throws PKIException {
        this.signedData = signedData2;
        this.msg = null;
    }

    public void load(String fileName) throws PKIException {
        try {
            FileInputStream fin = new FileInputStream(fileName);
            try {
                byte[] data = new byte[fin.available()];
                fin.read(data);
                fin.close();
                load(data);
            } catch (Exception e) {
                ex = e;
                throw new PKIException("8175", PKIException.PARSE_P7_SIGNEDDATA_ERR_DES, ex);
            }
        } catch (Exception e2) {
            ex = e2;
            throw new PKIException("8175", PKIException.PARSE_P7_SIGNEDDATA_ERR_DES, ex);
        }
    }

    public byte[] getContent() throws PKIException {
        if (this.msg == null && this.signedData == null) {
            return null;
        }
        if (this.msg == null && this.signedData != null) {
            ContentInfo contentInfo = this.signedData.getEncapContentInfo();
            if (!contentInfo.getContentType().equals(PKCSObjectIdentifiers.data) && !contentInfo.getContentType().equals(PKCSObjectIdentifiers.id_ct_TSTInfo)) {
                this.msg = writeDERObj2Bytes(contentInfo.getContent());
            } else if (contentInfo.getContent() == null) {
                throw new PKIException("8175", "解析PKCS7签名数据包失败 解析PKCS7签名数据包失败", new Exception("no sourceData to be verify."));
            } else {
                this.msg = ((ASN1OctetString) contentInfo.getContent()).getOctets();
            }
            return this.msg;
        } else if (this.msg != null) {
            return this.msg;
        } else {
            return this.msg;
        }
    }

    public boolean verify(byte[] sourcedata, X509Cert cert, SignerInfo signerInfo) {
        try {
            return verifySignerInfo(sourcedata, (X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(cert.getEncoded())), signerInfo, true);
        } catch (CertificateException e) {
            return false;
        } catch (PKIException e2) {
            return false;
        }
    }

    public boolean verify(X509Cert[] certs2) {
        return verify(null, certs2);
    }

    public boolean verify(byte[] sourcedata, X509Cert[] certs2) {
        ArrayList CertList = new ArrayList();
        try {
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            for (int i = 0; i < certs2.length; i++) {
                if (certs2[i] != null) {
                    CertList.add(cf.generateCertificate(new ByteArrayInputStream(certs2[i].getEncoded())));
                }
            }
            return verifySignerInfo(sourcedata, CertList);
        } catch (PKIException e) {
            return false;
        } catch (CertificateException e2) {
            return false;
        }
    }

    public boolean verify(byte[] sourcedata) {
        ArrayList CertList = new ArrayList();
        ByteArrayOutputStream bOut = new ByteArrayOutputStream();
        ASN1OutputStream aOut = new ASN1OutputStream(bOut);
        ASN1Set setCerts = this.signedData.getCertificates();
        try {
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            if (setCerts != null) {
                Enumeration e = setCerts.getObjects();
                while (e.hasMoreElements()) {
                    aOut.writeObject(e.nextElement());
                    CertList.add(cf.generateCertificate(new ByteArrayInputStream(bOut.toByteArray())));
                    bOut.reset();
                }
            }
            aOut.flush();
            aOut.close();
            bOut.flush();
            bOut.close();
            return verifySignerInfo(sourcedata, CertList);
        } catch (IOException e2) {
            return false;
        } catch (CertificateException e3) {
            return false;
        }
    }

    public boolean verify() {
        return verify((byte[]) null);
    }

    public boolean verify(byte[] sourceData, Map subjectKeyMap, Map issuerAndSNMap, boolean isCheckTime) {
        X509Cert cert;
        ASN1Set SignerInfoset = this.signedData.getSignerInfos();
        int i = 0;
        while (i < SignerInfoset.size()) {
            SignerInfo signerInfo = SignerInfo.getInstance(SignerInfoset.getObjectAt(i));
            SignerIdentifier signerid = signerInfo.getSID();
            if (signerid.isTagged()) {
                cert = (X509Cert) issuerAndSNMap.get((String) subjectKeyMap.get(new String(Base64.encode(SubjectKeyIdentifier.getInstance(signerid.getId()).getKeyIdentifier()))));
            } else {
                IssuerAndSerialNumber iAnds = IssuerAndSerialNumber.getInstance(signerid.getId());
                cert = (X509Cert) issuerAndSNMap.get(iAnds.getName().toString() + iAnds.getSerialNumber().getValue().toString());
            }
            if (cert == null) {
                return false;
            }
            try {
                if (!verifySignerInfo(sourceData, (X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(cert.getEncoded())), signerInfo, isCheckTime)) {
                    return false;
                }
                i++;
            } catch (CertificateException e) {
                return false;
            } catch (PKIException e2) {
                return false;
            }
        }
        return true;
    }

    private boolean verifySignerInfo(byte[] sourceData, ArrayList CertList) {
        try {
            CertStore certStore = CertStore.getInstance("Collection", new CollectionCertStoreParameters(CertList));
            ASN1Set SignerInfoset = this.signedData.getSignerInfos();
            int i = 0;
            while (i < SignerInfoset.size()) {
                SignerInfo signerInfo = SignerInfo.getInstance(SignerInfoset.getObjectAt(i));
                SignerId sid = new SignerId();
                SignerIdentifier signerid = signerInfo.getSID();
                if (signerid.isTagged()) {
                    sid.setSubjectKeyIdentifier(ASN1OctetString.getInstance(signerid.getId()).getOctets());
                } else {
                    IssuerAndSerialNumber iAnds = IssuerAndSerialNumber.getInstance(signerid.getId());
                    ByteArrayOutputStream sidbOut = new ByteArrayOutputStream();
                    ASN1OutputStream sidaOut = new ASN1OutputStream(sidbOut);
                    try {
                        sidaOut.writeObject(iAnds.getName());
                        sid.setIssuer(sidbOut.toByteArray());
                        sidaOut.flush();
                        sidaOut.close();
                        sidbOut.flush();
                        sidbOut.close();
                        sid.setSerialNumber(iAnds.getSerialNumber().getValue());
                    } catch (IOException e) {
                        return false;
                    }
                }
                try {
                    if (!verifySignerInfo(sourceData, (X509Certificate) certStore.getCertificates(sid).iterator().next(), signerInfo, true)) {
                        return false;
                    }
                    i++;
                } catch (Exception e2) {
                    return false;
                }
            }
            return true;
        } catch (Exception e3) {
            return false;
        }
    }

    private boolean verifySignerInfo(byte[] sourceData, X509Certificate cert, SignerInfo signerInfo, boolean isCheckTime) {
        Attribute t;
        AttributeTable attr = new AttributeTable(signerInfo.getAuthenticatedAttributes());
        if (!(!isCheckTime || attr == null || (t = attr.get(CMSAttributes.signingTime)) == null)) {
            try {
                cert.checkValidity(Time.getInstance(t.getAttrValues().getObjectAt(0).getDERObject()).getDate());
            } catch (CertificateExpiredException | CertificateNotYetValidException e) {
                return false;
            }
        }
        return doVerify(cert.getPublicKey(), attr, GetSignMechanism(signerInfo), signerInfo.getAuthenticatedAttributes(), sourceData, signerInfo.getEncryptedDigest().getOctets());
    }

    private Mechanism GetSignMechanism(SignerInfo signerInfo) {
        String DigestEncryptionAlgorithm = signerInfo.getDigestEncryptionAlgorithm().getObjectId().getId();
        if (DigestEncryptionAlgorithm.equals("1.2.840.113549.1.1.5")) {
            return new Mechanism("SHA1withRSAEncryption");
        }
        if (DigestEncryptionAlgorithm.equals("1.2.840.113549.1.1.11")) {
            return new Mechanism("SHA256withRSAEncryption");
        }
        if (DigestEncryptionAlgorithm.equals("1.2.840.113549.1.1.12")) {
            return new Mechanism("SHA384withRSAEncryption");
        }
        if (DigestEncryptionAlgorithm.equals("1.2.840.113549.1.1.13")) {
            return new Mechanism("SHA512withRSAEncryption");
        }
        if (DigestEncryptionAlgorithm.equals("1.2.840.113549.1.1.4")) {
            return new Mechanism("MD5withRSAEncryption");
        }
        if (DigestEncryptionAlgorithm.equals("1.2.840.113549.1.1.2")) {
            return new Mechanism("MD2withRSAEncryption");
        }
        if (DigestEncryptionAlgorithm.equals("1.2.840.10045.4.1")) {
            return new Mechanism("SHA1withECDSA");
        }
        if (DigestEncryptionAlgorithm.equals("1.2.840.10045.4.3.1")) {
            return new Mechanism("SHA224withECDSA");
        }
        if (DigestEncryptionAlgorithm.equals("1.2.840.10045.4.3.2")) {
            return new Mechanism("SHA256withECDSA");
        }
        if (DigestEncryptionAlgorithm.equals("1.2.840.10040.4.3")) {
            return new Mechanism("SHA1withDSA");
        }
        if (DigestEncryptionAlgorithm.equals("2.16.840.1.101.3.4.3.1")) {
            return new Mechanism(Mechanism.SHA224_DSA);
        }
        if (DigestEncryptionAlgorithm.equals("2.16.840.1.101.3.4.3.2")) {
            return new Mechanism(Mechanism.SHA256_DSA);
        }
        return null;
    }

    private String GetDigestTypeName(Mechanism signmechanism) throws PKIException {
        if (signmechanism.getMechanismType().equals("MD2withRSAEncryption")) {
            return Mechanism.MD2;
        }
        if (signmechanism.getMechanismType().equals("MD5withRSAEncryption")) {
            return Mechanism.MD5;
        }
        if (signmechanism.getMechanismType().equals("SHA1withRSAEncryption") || signmechanism.getMechanismType().equals("SHA1withDSA")) {
            return Mechanism.SHA1;
        }
        if (signmechanism.getMechanismType().equals("SHA256withRSAEncryption")) {
            return Mechanism.SHA256;
        }
        if (signmechanism.getMechanismType().equals("SHA384withRSAEncryption")) {
            return Mechanism.SHA384;
        }
        if (signmechanism.getMechanismType().equals("SHA512withRSAEncryption")) {
            return Mechanism.SHA512;
        }
        if (signmechanism.getMechanismType().equals("SHA1withECDSA")) {
            return Mechanism.SHA1;
        }
        if (signmechanism.getMechanismType().equals("SHA224withECDSA")) {
            return Mechanism.SHA224;
        }
        if (signmechanism.getMechanismType().equals("SHA256withECDSA")) {
            return Mechanism.SHA256;
        }
        if (signmechanism.getMechanismType().equals("SHA1withDSA")) {
            return Mechanism.SHA1;
        }
        if (signmechanism.getMechanismType().equals(Mechanism.SHA224_DSA)) {
            return Mechanism.SHA224;
        }
        if (signmechanism.getMechanismType().equals(Mechanism.SHA256_DSA)) {
            return Mechanism.SHA256;
        }
        StringBuffer error = new StringBuffer();
        error.append(PKIException.SIGN_DES);
        error.append(" ");
        error.append(PKIException.NOT_SUP_DES);
        error.append(" ");
        error.append(signmechanism.getMechanismType());
        throw new PKIException("8125", error.toString());
    }

    private boolean doVerify(PublicKey key, AttributeTable signedAttrTable, Mechanism signmechanism, ASN1Set signedAttributes, byte[] sourceData, byte[] signdata) {
        if (sourceData == null) {
            if (this.msg != null) {
                sourceData = this.msg;
            } else {
                try {
                    sourceData = getContent();
                } catch (PKIException e) {
                    return false;
                }
            }
            if (sourceData == null) {
                return false;
            }
        }
        if (signmechanism == null) {
            return false;
        }
        try {
            JKey jKey = Parser.convertJITPublicKey(key);
            if (signedAttrTable == null) {
                return this.session.verifySign(signmechanism, jKey, sourceData, signdata);
            }
            byte[] digest = this.session.digest(new Mechanism(GetDigestTypeName(signmechanism)), sourceData);
            Attribute dig = signedAttrTable.get(CMSAttributes.messageDigest);
            Attribute type = signedAttrTable.get(CMSAttributes.contentType);
            if (dig == null) {
                return false;
            }
            if (type == null) {
                return false;
            }
            byte[] octets = ((ASN1OctetString) dig.getAttrValues().getObjectAt(0)).getOctets();
            if (!((DERObjectIdentifier) type.getAttrValues().getObjectAt(0)).equals(this.signedData.getEncapContentInfo().getContentType())) {
                return false;
            }
            ByteArrayOutputStream bOut = new ByteArrayOutputStream();
            DEROutputStream dOut = new DEROutputStream(bOut);
            dOut.writeObject(signedAttributes);
            byte[] bytedata = bOut.toByteArray();
            dOut.flush();
            dOut.close();
            bOut.flush();
            bOut.close();
            return this.session.verifySign(signmechanism, jKey, bytedata, signdata);
        } catch (PKIException e2) {
            return false;
        } catch (IOException e3) {
            return false;
        }
    }

    private static byte[] writeDERObj2Bytes(DEREncodable obj) throws PKIException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        DEROutputStream dos = new DEROutputStream(bos);
        try {
            dos.writeObject(obj);
            byte[] byteData = bos.toByteArray();
            dos.flush();
            dos.close();
            bos.flush();
            bos.close();
            return byteData;
        } catch (Exception ex) {
            throw new PKIException("8136", PKIException.DEROBJ_BYTES_DES, ex);
        }
    }

    private class Signer {
        X509Cert cert = null;
        JKey privateKey = null;
        AttributeTable sAttr = null;
        Session session = null;
        Mechanism sign_Mechanism = null;
        AttributeTable unsAttr = null;

        Signer(Session session2, JKey key, X509Cert cert2, Mechanism sign_Mechanism2) {
            this.session = session2;
            this.privateKey = key;
            this.cert = cert2;
            this.sign_Mechanism = sign_Mechanism2;
        }

        Signer(Session session2, JKey key, X509Cert cert2, Mechanism sign_Mechanism2, AttributeTable sAttr2, AttributeTable unsAttr2) {
            this.session = session2;
            this.privateKey = key;
            this.cert = cert2;
            this.sign_Mechanism = sign_Mechanism2;
            this.sAttr = sAttr2;
            this.unsAttr = unsAttr2;
        }

        /* access modifiers changed from: package-private */
        public AttributeTable getSignedAttributes() {
            return this.sAttr;
        }

        /* access modifiers changed from: package-private */
        public AttributeTable getUnsignedAttributes() {
            return this.unsAttr;
        }

        /* access modifiers changed from: package-private */
        public JKey getKey() {
            return this.privateKey;
        }

        /* access modifiers changed from: package-private */
        public X509Cert getCertificate() {
            return this.cert;
        }

        /* access modifiers changed from: package-private */
        public Mechanism getSignMechanism() {
            return this.sign_Mechanism;
        }

        /* access modifiers changed from: package-private */
        public String GetDigestTypeName() throws PKIException {
            if (this.sign_Mechanism.getMechanismType().equals("MD2withRSAEncryption")) {
                return Mechanism.MD2;
            }
            if (this.sign_Mechanism.getMechanismType().equals("MD5withRSAEncryption")) {
                return Mechanism.MD5;
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA1withRSAEncryption") || this.sign_Mechanism.getMechanismType().equals("SHA1withDSA")) {
                return Mechanism.SHA1;
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA256withRSAEncryption")) {
                return Mechanism.SHA256;
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA384withRSAEncryption")) {
                return Mechanism.SHA384;
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA512withRSAEncryption")) {
                return Mechanism.SHA512;
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA1withECDSA")) {
                return Mechanism.SHA1;
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA224withECDSA")) {
                return Mechanism.SHA224;
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA256withECDSA")) {
                return Mechanism.SHA256;
            }
            if (this.sign_Mechanism.getMechanismType().equals("SM3withSM2Encryption")) {
                return Mechanism.SM3;
            }
            StringBuffer error = new StringBuffer();
            error.append(PKIException.SIGN_DES);
            error.append(" ");
            error.append(PKIException.NOT_SUP_DES);
            error.append(" ");
            error.append(this.sign_Mechanism.getMechanismType());
            throw new PKIException("8125", error.toString());
        }

        /* access modifiers changed from: package-private */
        public String GetEncTypeName() throws PKIException {
            if (this.sign_Mechanism.getMechanismType().equals("MD2withRSAEncryption") || this.sign_Mechanism.getMechanismType().equals("MD5withRSAEncryption") || this.sign_Mechanism.getMechanismType().equals("SHA1withRSAEncryption")) {
                return Mechanism.RSA;
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA1withDSA") || this.sign_Mechanism.getMechanismType().equals(Mechanism.SHA224_DSA) || this.sign_Mechanism.getMechanismType().equals(Mechanism.SHA256_DSA)) {
                return Mechanism.DSA;
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA1withECDSA") || this.sign_Mechanism.getMechanismType().equals("SHA224withECDSA") || this.sign_Mechanism.getMechanismType().equals("SHA256withECDSA")) {
                return Mechanism.ECDSA;
            }
            if (this.sign_Mechanism.getMechanismType().equals("SM3withSM2Encryption")) {
                return Mechanism.SM2;
            }
            StringBuffer error = new StringBuffer();
            error.append(PKIException.SIGN_DES);
            error.append(" ");
            error.append(PKIException.NOT_SUP_DES);
            error.append(" ");
            error.append(this.sign_Mechanism.getMechanismType());
            throw new PKIException("8125", error.toString());
        }

        private String GetSignatureAlgTypeOID() {
            String SignatureAlgorithm = this.sign_Mechanism.getMechanismType();
            if (SignatureAlgorithm.equals("SHA1withRSAEncryption")) {
                return "1.2.840.113549.1.1.5";
            }
            if (SignatureAlgorithm.equals("SHA256withRSAEncryption")) {
                return "1.2.840.113549.1.1.11";
            }
            if (SignatureAlgorithm.equals("SHA384withRSAEncryption")) {
                return "1.2.840.113549.1.1.12";
            }
            if (SignatureAlgorithm.equals("SHA512withRSAEncryption")) {
                return "1.2.840.113549.1.1.13";
            }
            if (SignatureAlgorithm.equals("MD5withRSAEncryption")) {
                return "1.2.840.113549.1.1.4";
            }
            if (SignatureAlgorithm.equals("MD2withRSAEncryption")) {
                return "1.2.840.113549.1.1.2";
            }
            if (SignatureAlgorithm.equals("SHA1withECDSA")) {
                return "1.2.840.10045.4.1";
            }
            if (SignatureAlgorithm.equals("SHA224withECDSA")) {
                return "1.2.840.10045.4.3.1";
            }
            if (SignatureAlgorithm.equals("SHA256withECDSA")) {
                return "1.2.840.10045.4.3.2";
            }
            if (SignatureAlgorithm.equals("SHA1withDSA")) {
                return "1.2.840.10040.4.3";
            }
            if (SignatureAlgorithm.equals(Mechanism.SHA224_DSA)) {
                return "2.16.840.1.101.3.4.3.1";
            }
            if (SignatureAlgorithm.equals(Mechanism.SHA256_DSA)) {
                return "2.16.840.1.101.3.4.3.2";
            }
            if (SignatureAlgorithm.equals("SM3withSM2Encryption")) {
                return "1.2.156.10197.1.501";
            }
            return null;
        }

        /* access modifiers changed from: package-private */
        public String GetDigestTypeOID() throws PKIException {
            if (this.sign_Mechanism.getMechanismType().equals("MD2withRSAEncryption")) {
                return "1.2.840.113549.2.2";
            }
            if (this.sign_Mechanism.getMechanismType().equals("MD5withRSAEncryption")) {
                return "1.2.840.113549.2.5";
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA1withRSAEncryption") || this.sign_Mechanism.getMechanismType().equals("SHA1withDSA")) {
                return "1.3.14.3.2.26";
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA256withRSAEncryption")) {
                return "2.16.840.1.101.3.4.2.1";
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA384withRSAEncryption") || this.sign_Mechanism.getMechanismType().equals("SHA512withRSAEncryption")) {
                return "2.16.840.1.101.3.4.2.2";
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA1withECDSA")) {
                return "1.3.14.3.2.26";
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA224withECDSA")) {
                return "2.16.840.1.101.3.4.2.4";
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA256withECDSA")) {
                return "2.16.840.1.101.3.4.2.1";
            }
            if (this.sign_Mechanism.getMechanismType().equals(Mechanism.SHA224_DSA)) {
                return "2.16.840.1.101.3.4.2.4";
            }
            if (this.sign_Mechanism.getMechanismType().equals(Mechanism.SHA256_DSA)) {
                return "2.16.840.1.101.3.4.2.1";
            }
            StringBuffer error = new StringBuffer();
            error.append(PKIException.SIGN_DES);
            error.append(" ");
            error.append(PKIException.NOT_SUP_DES);
            error.append(" ");
            error.append(this.sign_Mechanism.getMechanismType());
            throw new PKIException("8125", error.toString());
        }

        /* access modifiers changed from: package-private */
        public String GetEncTypeOID() throws PKIException {
            if (this.sign_Mechanism.getMechanismType().equals("MD2withRSAEncryption") || this.sign_Mechanism.getMechanismType().equals("MD5withRSAEncryption") || this.sign_Mechanism.getMechanismType().equals("SHA1withRSAEncryption")) {
                return "1.2.840.113549.1.1.1";
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA1withDSA")) {
                return "1.2.840.10040.4.3";
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA256withRSAEncryption") || this.sign_Mechanism.getMechanismType().equals("SHA384withRSAEncryption") || this.sign_Mechanism.getMechanismType().equals("SHA512withRSAEncryption")) {
                return "1.2.840.113549.1.1.1";
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA1withECDSA")) {
                return "1.2.840.10045.4.1";
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA224withECDSA")) {
                return "1.2.840.10045.4.3.1";
            }
            if (this.sign_Mechanism.getMechanismType().equals("SHA256withECDSA")) {
                return "1.2.840.10045.4.3.2";
            }
            if (this.sign_Mechanism.getMechanismType().equals(Mechanism.SHA224_DSA)) {
                return "2.16.840.1.101.3.4.3.1";
            }
            if (this.sign_Mechanism.getMechanismType().equals(Mechanism.SHA256_DSA)) {
                return "2.16.840.1.101.3.4.3.2";
            }
            StringBuffer error = new StringBuffer();
            error.append(PKIException.SIGN_DES);
            error.append(" ");
            error.append(PKIException.NOT_SUP_DES);
            error.append(" ");
            error.append(this.sign_Mechanism.getMechanismType());
            throw new PKIException("8125", error.toString());
        }

        /* access modifiers changed from: package-private */
        public SignerInfo toSignerInfo(DERObjectIdentifier contentType, byte[] content, boolean addDefaultAttributes, boolean setIssuerAndSN) throws PKIException, IOException {
            AlgorithmIdentifier encAlgId;
            SignerInfo signerInfo;
            AlgorithmIdentifier digAlgId = new AlgorithmIdentifier(new DERObjectIdentifier(GetDigestTypeOID()), new DERNull());
            if (GetEncTypeOID().equals("1.2.840.10040.4.3")) {
                encAlgId = new AlgorithmIdentifier(new DERObjectIdentifier(GetEncTypeOID()));
            } else {
                encAlgId = new AlgorithmIdentifier(new DERObjectIdentifier(GetEncTypeOID()), new DERNull());
            }
            AlgorithmIdentifier digEncAlgId = new AlgorithmIdentifier(new DERObjectIdentifier(GetSignatureAlgTypeOID()), new DERNull());
            ASN1Set signedAttr = null;
            ASN1Set unsignedAttr = null;
            byte[] hash = this.session.digest(new Mechanism(GetDigestTypeName()), content);
            AttributeTable attr = getSignedAttributes();
            if (attr != null) {
                ASN1EncodableVector v = new ASN1EncodableVector();
                if (attr.get(CMSAttributes.contentType) == null) {
                    v.add(new Attribute(CMSAttributes.contentType, new DERSet(contentType)));
                } else {
                    v.add(attr.get(CMSAttributes.contentType));
                }
                if (attr.get(CMSAttributes.signingTime) == null) {
                    v.add(new Attribute(CMSAttributes.signingTime, new DERSet(new Time(new Date()))));
                } else {
                    v.add(attr.get(CMSAttributes.signingTime));
                }
                v.add(new Attribute(CMSAttributes.messageDigest, new DERSet(new DEROctetString(hash))));
                Hashtable ats = attr.toHashtable();
                ats.remove(CMSAttributes.contentType);
                ats.remove(CMSAttributes.signingTime);
                ats.remove(CMSAttributes.messageDigest);
                for (Object instance : ats.values()) {
                    v.add(Attribute.getInstance(instance));
                }
                signedAttr = new DERSet(v);
            } else if (addDefaultAttributes) {
                ASN1EncodableVector v2 = new ASN1EncodableVector();
                v2.add(new Attribute(CMSAttributes.contentType, new DERSet(contentType)));
                v2.add(new Attribute(CMSAttributes.signingTime, new DERSet(new DERUTCTime(new Date()))));
                v2.add(new Attribute(CMSAttributes.messageDigest, new DERSet(new DEROctetString(hash))));
                signedAttr = new DERSet(v2);
            }
            AttributeTable attr2 = getUnsignedAttributes();
            if (attr2 != null) {
                ASN1EncodableVector v3 = new ASN1EncodableVector();
                for (Object instance2 : attr2.toHashtable().values()) {
                    v3.add(Attribute.getInstance(instance2));
                }
                unsignedAttr = new DERSet(v3);
            }
            ByteArrayOutputStream bOut = new ByteArrayOutputStream();
            if (signedAttr != null) {
                DEROutputStream dEROutputStream = new DEROutputStream(bOut);
                dEROutputStream.writeObject(signedAttr);
                dEROutputStream.flush();
                dEROutputStream.close();
            } else {
                bOut.write(content);
            }
            ASN1OctetString encDigest = new DEROctetString(this.session.sign(this.sign_Mechanism, this.privateKey, bOut.toByteArray()));
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(this.cert.getTBSCertificate());
            ASN1InputStream aSN1InputStream = new ASN1InputStream(byteArrayInputStream);
            TBSCertificateStructure tbs = TBSCertificateStructure.getInstance(aSN1InputStream.readObject());
            if (setIssuerAndSN) {
                signerInfo = new SignerInfo(new SignerIdentifier(new IssuerAndSerialNumber(tbs.getIssuer(), tbs.getSerialNumber().getValue())), digAlgId, signedAttr, digEncAlgId, encDigest, unsignedAttr);
            } else {
                signerInfo = new SignerInfo(new SignerIdentifier((ASN1OctetString) new SubjectKeyIdentifier(this.cert.getCertStructure().getSubjectPublicKeyInfo()).getDERObject()), digAlgId, signedAttr, encAlgId, encDigest, unsignedAttr);
            }
            bOut.flush();
            bOut.close();
            aSN1InputStream.close();
            byteArrayInputStream.close();
            return signerInfo;
        }
    }

    private class SignerId extends X509CertSelector {
        private SignerId() {
        }

        public int hashCode() {
            int code = 0;
            if (getSerialNumber() != null) {
                code = 0 ^ getSerialNumber().hashCode();
            }
            if (getIssuerAsString() != null) {
                code ^= getIssuerAsString().hashCode();
            }
            byte[] subjectId = getSubjectKeyIdentifier();
            if (subjectId != null) {
                for (int i = 0; i != subjectId.length; i++) {
                    code ^= (subjectId[i] & 255) << (i % 4);
                }
            }
            return code;
        }

        public boolean equals(Object o) {
            byte[] otherId;
            if (!(o instanceof SignerId)) {
                return false;
            }
            SignerId id = (SignerId) o;
            if (id.getSerialNumber() != null && !id.getSerialNumber().equals(getSerialNumber())) {
                return false;
            }
            if (id.getIssuerAsString() != null && !id.getIssuerAsString().equals(getIssuerAsString())) {
                return false;
            }
            byte[] subjectId = getSubjectKeyIdentifier();
            if (subjectId == null || ((otherId = id.getSubjectKeyIdentifier()) != null && Arrays.equals(subjectId, otherId))) {
                return true;
            }
            return false;
        }
    }
}
