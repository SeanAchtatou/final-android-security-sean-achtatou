package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERGeneralizedTime;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.DERTaggedObject;

public class PrivateKeyUsagePeriod implements DEREncodable {
    private DERGeneralizedTime notAfter;
    private DERGeneralizedTime notBefore;
    private ASN1Sequence seq;

    public PrivateKeyUsagePeriod(ASN1Sequence seq2) {
        this.seq = seq2;
        for (int i = 0; i < seq2.size(); i++) {
            ASN1TaggedObject obj = (ASN1TaggedObject) seq2.getObjectAt(i);
            switch (obj.getTagNo()) {
                case 0:
                    this.notBefore = DERGeneralizedTime.getInstance(obj, true);
                    break;
                case 1:
                    this.notAfter = DERGeneralizedTime.getInstance(obj, true);
                    break;
                default:
                    throw new IllegalArgumentException("illegal tag");
            }
        }
    }

    public PrivateKeyUsagePeriod(DERGeneralizedTime notBefore2, DERGeneralizedTime notAfter2) {
        this.notBefore = notBefore2;
        this.notAfter = notAfter2;
    }

    public static PrivateKeyUsagePeriod getInstance(ASN1TaggedObject obj, boolean explicit) {
        return getInstance(ASN1Sequence.getInstance(obj, explicit));
    }

    public static PrivateKeyUsagePeriod getInstance(Object obj) {
        if (obj instanceof PrivateKeyUsagePeriod) {
            return (PrivateKeyUsagePeriod) obj;
        }
        if (obj instanceof ASN1Sequence) {
            return new PrivateKeyUsagePeriod((ASN1Sequence) obj);
        }
        throw new IllegalArgumentException("unknown object in factory");
    }

    public DERGeneralizedTime getNotBefore() {
        return this.notBefore;
    }

    public DERGeneralizedTime getNotAfter() {
        return this.notAfter;
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        if (this.notBefore != null) {
            v.add(new DERTaggedObject(0, this.notBefore));
        }
        if (this.notAfter != null) {
            v.add(new DERTaggedObject(1, this.notAfter));
        }
        return new DERSequence(v);
    }
}
