package cn.com.jit.ida.util.pki.asn1;

import de.innosystec.unrar.unpack.decode.Compress;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

public class BERInputStream extends DERInputStream {
    private DERObject END_OF_STREAM = new DERObject() {
        /* access modifiers changed from: package-private */
        public void encode(DEROutputStream out) throws IOException {
            throw new IOException("Eeek!");
        }
    };

    public BERInputStream(InputStream is) {
        super(is);
    }

    private byte[] readIndefiniteLengthFully() throws IOException {
        ByteArrayOutputStream bOut = new ByteArrayOutputStream();
        int b1 = read();
        while (true) {
            int b = read();
            if (b >= 0 && (b1 != 0 || b != 0)) {
                bOut.write(b1);
                b1 = b;
            }
        }
        return bOut.toByteArray();
    }

    private BERConstructedOctetString buildConstructedOctetString() throws IOException {
        Vector octs = new Vector();
        while (true) {
            DERObject o = readObject();
            if (o == this.END_OF_STREAM) {
                return new BERConstructedOctetString(octs);
            }
            octs.addElement(o);
        }
    }

    public DERObject readObject() throws IOException {
        int tag = read();
        if (tag == -1) {
            throw new EOFException();
        }
        int length = readLength();
        if (length < 0) {
            switch (tag) {
                case 5:
                    return null;
                case 36:
                    return buildConstructedOctetString();
                case Compress.DC20 /*48*/:
                    BERConstructedSequence seq = new BERConstructedSequence();
                    while (true) {
                        DERObject obj = readObject();
                        if (obj == this.END_OF_STREAM) {
                            return seq;
                        }
                        seq.addObject(obj);
                    }
                case 49:
                    ASN1EncodableVector v = new ASN1EncodableVector();
                    while (true) {
                        DERObject obj2 = readObject();
                        if (obj2 == this.END_OF_STREAM) {
                            return new BERSet(v);
                        }
                        v.add(obj2);
                    }
                default:
                    if ((tag & 128) == 0) {
                        throw new IOException("unknown BER object encountered");
                    } else if ((tag & 31) == 31) {
                        throw new IOException("unsupported high tag encountered");
                    } else if ((tag & 32) == 0) {
                        return new BERTaggedObject(false, tag & 31, new DEROctetString(readIndefiniteLengthFully()));
                    } else {
                        DERObject dObj = readObject();
                        if (dObj == this.END_OF_STREAM) {
                            return new DERTaggedObject(tag & 31);
                        }
                        DERObject next = readObject();
                        if (next == this.END_OF_STREAM) {
                            return new BERTaggedObject(tag & 31, dObj);
                        }
                        BERConstructedSequence seq2 = new BERConstructedSequence();
                        seq2.addObject(dObj);
                        do {
                            seq2.addObject(next);
                            next = readObject();
                        } while (next != this.END_OF_STREAM);
                        return new BERTaggedObject(false, tag & 31, seq2);
                    }
            }
        } else if (tag == 0 && length == 0) {
            return this.END_OF_STREAM;
        } else {
            byte[] bytes = new byte[length];
            readFully(bytes);
            return buildObject(tag, bytes);
        }
    }
}
