package cn.com.jit.ida.util.pki.asn1;

import android.support.v4.view.MotionEventCompat;
import cn.com.jit.ida.util.pki.PKIException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

public class DERObjectIdentifier extends DERObject {
    String identifier;

    public static DERObjectIdentifier getInstance(Object obj) {
        if (obj == null || (obj instanceof DERObjectIdentifier)) {
            return (DERObjectIdentifier) obj;
        }
        if (obj instanceof ASN1OctetString) {
            return new DERObjectIdentifier(((ASN1OctetString) obj).getOctets());
        }
        if (obj instanceof ASN1TaggedObject) {
            return getInstance(((ASN1TaggedObject) obj).getObject());
        }
        throw new IllegalArgumentException("illegal object in getInstance: " + obj.getClass().getName());
    }

    public static DERObjectIdentifier getInstance(ASN1TaggedObject obj, boolean explicit) {
        return getInstance(obj.getObject());
    }

    DERObjectIdentifier(byte[] bytes) {
        StringBuffer objId = new StringBuffer();
        int value = 0;
        boolean first = true;
        for (int i = 0; i != bytes.length; i++) {
            int b = bytes[i] & MotionEventCompat.ACTION_MASK;
            value = (value * 128) + (b & 127);
            if ((b & 128) == 0) {
                if (first) {
                    switch (value / 40) {
                        case 0:
                            objId.append('0');
                            break;
                        case 1:
                            objId.append('1');
                            value -= 40;
                            break;
                        default:
                            objId.append('2');
                            value -= 80;
                            break;
                    }
                    first = false;
                }
                objId.append('.');
                objId.append(Integer.toString(value));
                value = 0;
            }
        }
        this.identifier = objId.toString();
    }

    public DERObjectIdentifier(String identifier2) {
        this.identifier = identifier2;
    }

    public String getId() {
        return this.identifier;
    }

    private void writeField(OutputStream out, int fieldValue) throws IOException {
        if (fieldValue >= 128) {
            if (fieldValue >= 16384) {
                if (fieldValue >= 2097152) {
                    if (fieldValue >= 268435456) {
                        out.write((fieldValue >> 28) | 128);
                    }
                    out.write((fieldValue >> 21) | 128);
                }
                out.write((fieldValue >> 14) | 128);
            }
            out.write((fieldValue >> 7) | 128);
        }
        out.write(fieldValue & 127);
    }

    /* access modifiers changed from: package-private */
    public void encode(DEROutputStream out) throws IOException {
        OIDTokenizer tok = new OIDTokenizer(this.identifier);
        ByteArrayOutputStream bOut = new ByteArrayOutputStream();
        DEROutputStream dOut = new DEROutputStream(bOut);
        writeField(bOut, (Integer.parseInt(tok.nextToken()) * 40) + Integer.parseInt(tok.nextToken()));
        while (tok.hasMoreTokens()) {
            writeField(bOut, Integer.parseInt(tok.nextToken()));
        }
        dOut.close();
        out.writeEncoded(6, bOut.toByteArray());
    }

    public int hashCode() {
        return this.identifier.hashCode();
    }

    public boolean equals(Object o) {
        if (o == null || !(o instanceof DERObjectIdentifier)) {
            return false;
        }
        return this.identifier.equals(((DERObjectIdentifier) o).identifier);
    }

    public DERObjectIdentifier(Node nl) throws PKIException {
        this.identifier = ((Text) nl.getFirstChild()).getData().trim();
    }
}
