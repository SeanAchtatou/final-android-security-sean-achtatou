package cn.com.jit.ida.util.pki.asn1;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class DERApplicationSpecific extends DERObject {
    private byte[] octets;
    private int tag;

    public DERApplicationSpecific(int tag2, byte[] octets2) {
        this.tag = tag2;
        this.octets = octets2;
    }

    public DERApplicationSpecific(int tag2, DEREncodable object) throws IOException {
        this.tag = tag2 | 32;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        new DEROutputStream(baos).writeObject(object);
        this.octets = baos.toByteArray();
    }

    public boolean isConstructed() {
        return (this.tag & 32) != 0;
    }

    public byte[] getContents() {
        return this.octets;
    }

    public int getApplicationTag() {
        return this.tag & 31;
    }

    public DERObject getObject() throws IOException {
        return new ASN1InputStream(new ByteArrayInputStream(getContents())).readObject();
    }

    /* access modifiers changed from: package-private */
    public void encode(DEROutputStream out) throws IOException {
        out.writeEncoded(this.tag | 64, this.octets);
    }
}
