package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.DERTaggedObject;
import java.util.Enumeration;

public class NameConstraints implements DEREncodable {
    ASN1Sequence excluded;
    ASN1Sequence permitted;

    public NameConstraints(ASN1Sequence seq) {
        Enumeration e = seq.getObjects();
        while (e.hasMoreElements()) {
            ASN1TaggedObject o = (ASN1TaggedObject) e.nextElement();
            switch (o.getTagNo()) {
                case 0:
                    this.permitted = ASN1Sequence.getInstance(o, false);
                    break;
                case 1:
                    this.excluded = ASN1Sequence.getInstance(o, false);
                    break;
            }
        }
    }

    public ASN1Sequence getPermittedSubtrees() {
        return this.permitted;
    }

    public ASN1Sequence getExcludedSubtrees() {
        return this.excluded;
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        if (this.permitted != null) {
            v.add(new DERTaggedObject(false, 0, this.permitted));
        }
        if (this.excluded != null) {
            v.add(new DERTaggedObject(false, 1, this.excluded));
        }
        return new DERSequence(v);
    }
}
