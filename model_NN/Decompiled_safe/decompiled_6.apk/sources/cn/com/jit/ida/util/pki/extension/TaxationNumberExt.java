package cn.com.jit.ida.util.pki.extension;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.DERPrintableString;
import cn.com.jit.ida.util.pki.asn1.x509.JITTaxationNumeber;
import cn.com.jit.ida.util.pki.asn1.x509.X509Extensions;

public class TaxationNumberExt extends AbstractStandardExtension {
    private String taxationnumber = null;

    public TaxationNumberExt() {
        this.OID = X509Extensions.JIT_TaxationNumber.getId();
        this.critical = false;
    }

    public TaxationNumberExt(DERPrintableString obj) {
        this.taxationnumber = obj.getString();
    }

    public TaxationNumberExt(String value) {
        this.OID = X509Extensions.JIT_TaxationNumber.getId();
        this.critical = false;
        this.taxationnumber = value;
    }

    public void SetTaxationNumber(String value) {
        this.taxationnumber = value;
    }

    public String GetTaxationNumber() {
        return this.taxationnumber;
    }

    public byte[] encode() throws PKIException {
        if (this.taxationnumber != null) {
            return new DEROctetString(new JITTaxationNumeber(this.taxationnumber).getDERObject()).getOctets();
        }
        throw new PKIException(PKIException.EXT_ENCODING_DATA_NULL, PKIException.EXT_ENCODING_DATA_NULL_DES);
    }

    public boolean getCritical() {
        return this.critical;
    }

    public String getOID() {
        return this.OID;
    }

    public void setCritical(boolean critical) {
        this.critical = critical;
    }
}
