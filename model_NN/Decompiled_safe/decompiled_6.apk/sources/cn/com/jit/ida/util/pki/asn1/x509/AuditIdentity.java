package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.ASN1Encodable;
import cn.com.jit.ida.util.pki.asn1.DERBoolean;
import cn.com.jit.ida.util.pki.asn1.DEREncodableVector;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

public class AuditIdentity extends ASN1Encodable {
    private DERBoolean critical = new DERBoolean(true);
    private DERObjectIdentifier extnID = X509Extensions.AuditIdentity;
    private DEROctetString extnValue;

    public AuditIdentity(Node nl) throws PKIException {
        NodeList nodelist = nl.getChildNodes();
        if (nodelist.getLength() == 0) {
            throw new PKIException("XML内容缺失,AuditIdentity.AuditIdentity(),AuditIdentity没有子节点");
        }
        for (int i = 0; i < nodelist.getLength(); i++) {
            Node cnode = nodelist.item(i);
            if (cnode.getNodeName().equals("extenValue")) {
                this.extnValue = new DEROctetString(((Text) cnode.getFirstChild()).getData().trim().getBytes());
            }
        }
    }

    public DERObject toASN1Object() {
        DEREncodableVector seq = new DEREncodableVector();
        seq.add(this.extnID);
        seq.add(this.critical);
        seq.add(this.extnValue);
        return new DERSequence(seq);
    }

    public void decode(DERObject parm1) throws PKIException {
        throw new UnsupportedOperationException("Method decode() not yet implemented.");
    }
}
