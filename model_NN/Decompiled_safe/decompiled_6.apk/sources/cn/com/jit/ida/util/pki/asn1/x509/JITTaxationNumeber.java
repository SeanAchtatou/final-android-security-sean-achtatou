package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.DERPrintableString;

public class JITTaxationNumeber extends DERPrintableString {
    public JITTaxationNumeber(byte[] string) {
        super(string);
    }

    public JITTaxationNumeber(String string) {
        super(string);
    }

    public String getTaxationNumeber() {
        return getString();
    }
}
