package cn.com.jit.ida.util.pki.extension;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.DERPrintableString;
import cn.com.jit.ida.util.pki.asn1.x509.JITInsuranceNumber;
import cn.com.jit.ida.util.pki.asn1.x509.X509Extensions;

public class InsuranceNumberExt extends AbstractStandardExtension {
    private String insurancenumber = null;

    public InsuranceNumberExt() {
        this.OID = X509Extensions.JIT_InsuranceNumber.getId();
        this.critical = false;
    }

    public InsuranceNumberExt(DERPrintableString obj) {
        this.insurancenumber = obj.getString();
    }

    public InsuranceNumberExt(String value) {
        this.OID = X509Extensions.JIT_InsuranceNumber.getId();
        this.critical = false;
        this.insurancenumber = value;
    }

    public void SetInsuranceNumber(String value) {
        this.insurancenumber = value;
    }

    public String GetInsuranceNumber() {
        return this.insurancenumber;
    }

    public byte[] encode() throws PKIException {
        if (this.insurancenumber != null) {
            return new DEROctetString(new JITInsuranceNumber(this.insurancenumber).getDERObject()).getOctets();
        }
        throw new PKIException(PKIException.EXT_ENCODING_DATA_NULL, PKIException.EXT_ENCODING_DATA_NULL_DES);
    }

    public boolean getCritical() {
        return this.critical;
    }

    public String getOID() {
        return this.OID;
    }

    public void setCritical(boolean critical) {
        this.critical = critical;
    }
}
