package cn.com.jit.ida.util.pki.extension;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1SequenceExt;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DEREncodableVector;
import cn.com.jit.ida.util.pki.asn1.DERIA5String;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.DERTaggedObject;
import cn.com.jit.ida.util.pki.asn1.x509.GeneralSubtree;
import cn.com.jit.ida.util.pki.asn1.x509.NameConstraints;
import cn.com.jit.ida.util.pki.asn1.x509.X509Extensions;
import cn.com.jit.ida.util.pki.asn1.x509.X509Name;
import java.math.BigInteger;

public class NameConstraintsExt extends AbstractStandardExtension {
    public static final short DIRECTORYNAME = 4;
    public static final short DNSNAME = 2;
    public static final short IPADDRESS = 7;
    public static final short RFC822NAME = 1;
    public static final short UNIFORMRESOURCEIDENTIFIER = 6;
    public static final short X400ADDRESS = 3;
    private DEREncodableVector Excluded = new DEREncodableVector();
    private DEREncodableVector Permitted = new DEREncodableVector();
    private ASN1Sequence seqExcluded = null;
    private ASN1Sequence seqPermitted = null;

    public NameConstraintsExt() {
        this.OID = X509Extensions.NameConstraints.getId();
        this.critical = false;
    }

    public NameConstraintsExt(ASN1Sequence asn1Sequence) {
        for (int i = 0; i < asn1Sequence.size(); i++) {
            DERTaggedObject tempDERTaggedObject = (DERTaggedObject) asn1Sequence.getObjectAt(i).getDERObject();
            switch (tempDERTaggedObject.getTagNo()) {
                case 0:
                    this.seqPermitted = (ASN1Sequence) tempDERTaggedObject.getObject();
                    break;
                case 1:
                    this.seqExcluded = (ASN1Sequence) tempDERTaggedObject.getObject();
                    break;
            }
        }
        for (int i2 = 0; i2 < this.seqPermitted.size(); i2++) {
            this.Permitted.add(this.seqPermitted.getObjectAt(i2));
        }
        for (int i3 = 0; i3 < this.seqExcluded.size(); i3++) {
            this.Excluded.add(this.seqExcluded.getObjectAt(i3));
        }
    }

    public int getPermittedCount() {
        if (this.seqPermitted == null) {
            return 0;
        }
        return this.seqPermitted.size();
    }

    public int getExcludedCount() {
        if (this.seqExcluded == null) {
            return 0;
        }
        return this.seqExcluded.size();
    }

    public int getPermittedForBaseType(int index) {
        if (this.seqPermitted == null) {
            return 0;
        }
        return new GeneralSubtree((ASN1Sequence) this.seqPermitted.getObjectAt(index)).getBase().getTagNo();
    }

    public String getPermittedForBase(int index) {
        if (this.seqPermitted == null) {
            return null;
        }
        return GeneralNameToString(new GeneralSubtree((ASN1Sequence) this.seqPermitted.getObjectAt(index)).getBase());
    }

    public int getPermittedForMinmum(int index) {
        if (this.seqPermitted == null) {
            return 0;
        }
        return new GeneralSubtree((ASN1Sequence) this.seqPermitted.getObjectAt(index)).getMinimum().intValue();
    }

    public int getPermittedForMaxmum(int index) {
        if (this.seqPermitted == null) {
            return 0;
        }
        BigInteger max = new GeneralSubtree((ASN1Sequence) this.seqPermitted.getObjectAt(index)).getMaximum();
        if (max == null) {
            return -1;
        }
        return max.intValue();
    }

    public int getExcludedForBaseType(int index) {
        if (this.seqExcluded == null) {
            return 0;
        }
        return new GeneralSubtree((ASN1Sequence) this.seqExcluded.getObjectAt(index)).getBase().getTagNo();
    }

    public String getExcludedForBase(int index) {
        if (this.seqExcluded == null) {
            return null;
        }
        return GeneralNameToString(new GeneralSubtree((ASN1Sequence) this.seqExcluded.getObjectAt(index)).getBase());
    }

    public int getExcludedForMinmum(int index) {
        if (this.seqExcluded == null) {
            return 0;
        }
        return new GeneralSubtree((ASN1Sequence) this.seqExcluded.getObjectAt(index)).getMinimum().intValue();
    }

    public int getExcludedForMaxmum(int index) {
        if (this.seqExcluded == null) {
            return 0;
        }
        BigInteger max = new GeneralSubtree((ASN1Sequence) this.seqExcluded.getObjectAt(index)).getMaximum();
        if (max == null) {
            return -1;
        }
        return max.intValue();
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v2, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v5, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v7, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v8, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v9, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v10, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String GeneralNameToString(cn.com.jit.ida.util.pki.asn1.x509.GeneralName r8) {
        /*
            r7 = this;
            r6 = 4
            r1 = 0
            int r5 = r8.getTagNo()
            switch(r5) {
                case 1: goto L_0x000a;
                case 2: goto L_0x000a;
                case 3: goto L_0x0009;
                case 4: goto L_0x0076;
                case 5: goto L_0x0009;
                case 6: goto L_0x000a;
                case 7: goto L_0x0015;
                case 8: goto L_0x006b;
                default: goto L_0x0009;
            }
        L_0x0009:
            return r1
        L_0x000a:
            cn.com.jit.ida.util.pki.asn1.DEREncodable r5 = r8.getName()
            cn.com.jit.ida.util.pki.asn1.DERIA5String r5 = (cn.com.jit.ida.util.pki.asn1.DERIA5String) r5
            java.lang.String r1 = r5.getString()
            goto L_0x0009
        L_0x0015:
            java.lang.StringBuffer r4 = new java.lang.StringBuffer
            r4.<init>()
            cn.com.jit.ida.util.pki.asn1.DEREncodable r5 = r8.getName()
            cn.com.jit.ida.util.pki.asn1.DEROctetString r5 = (cn.com.jit.ida.util.pki.asn1.DEROctetString) r5
            byte[] r2 = r5.getOctets()
            r3 = 0
            r0 = 0
        L_0x0026:
            if (r0 >= r6) goto L_0x0040
            byte r3 = r2[r0]
            if (r3 >= 0) goto L_0x002e
            int r3 = r3 + 256
        L_0x002e:
            java.lang.String r5 = java.lang.String.valueOf(r3)
            r4.append(r5)
            r5 = 3
            if (r0 >= r5) goto L_0x003d
            java.lang.String r5 = "."
            r4.append(r5)
        L_0x003d:
            int r0 = r0 + 1
            goto L_0x0026
        L_0x0040:
            int r5 = r2.length
            if (r5 <= r6) goto L_0x0066
            java.lang.String r5 = "/"
            r4.append(r5)
            r0 = 4
        L_0x0049:
            int r5 = r2.length
            if (r0 >= r5) goto L_0x0066
            byte r3 = r2[r0]
            if (r3 >= 0) goto L_0x0052
            int r3 = r3 + 256
        L_0x0052:
            java.lang.String r5 = java.lang.String.valueOf(r3)
            r4.append(r5)
            int r5 = r2.length
            int r5 = r5 + -1
            if (r0 >= r5) goto L_0x0063
            java.lang.String r5 = "."
            r4.append(r5)
        L_0x0063:
            int r0 = r0 + 1
            goto L_0x0049
        L_0x0066:
            java.lang.String r1 = r4.toString()
            goto L_0x0009
        L_0x006b:
            cn.com.jit.ida.util.pki.asn1.DEREncodable r5 = r8.getName()
            cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier r5 = (cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier) r5
            java.lang.String r1 = r5.getId()
            goto L_0x0009
        L_0x0076:
            cn.com.jit.ida.util.pki.asn1.DEREncodable r5 = r8.getName()
            cn.com.jit.ida.util.pki.asn1.x509.X509Name r5 = cn.com.jit.ida.util.pki.asn1.x509.X509Name.getInstance(r5)
            java.lang.String r1 = r5.toString()
            goto L_0x0009
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.com.jit.ida.util.pki.extension.NameConstraintsExt.GeneralNameToString(cn.com.jit.ida.util.pki.asn1.x509.GeneralName):java.lang.String");
    }

    public void addPermitted(short valueType, String value, Integer max, Integer min) throws PKIException {
        this.Permitted.add(GenDerObject(valueType, value, max, min));
    }

    public void addExcluded(short valueType, String value, Integer max, Integer min) throws PKIException {
        this.Excluded.add(GenDerObject(valueType, value, max, min));
    }

    public byte[] encode() throws PKIException {
        if (this.Permitted.size() <= 0 || this.Excluded.size() <= 0) {
            throw new PKIException(PKIException.EXT_ENCODING_DATA_NULL, PKIException.EXT_ENCODING_DATA_NULL_DES);
        }
        ASN1SequenceExt NameConstraints = new ASN1SequenceExt();
        DERTaggedObject dto_Permitted = new DERTaggedObject(false, 0, new DERSequence(this.Permitted));
        DERTaggedObject dto_Excluded = new DERTaggedObject(false, 1, new DERSequence(this.Excluded));
        NameConstraints.addObject(dto_Permitted);
        NameConstraints.addObject(dto_Excluded);
        return new DEROctetString(new NameConstraints(NameConstraints).getDERObject()).getOctets();
    }

    public boolean getCritical() {
        return this.critical;
    }

    public String getOID() {
        return this.OID;
    }

    public void setCritical(boolean critical) {
        this.critical = critical;
    }

    private DERObject GenDerObject(short valueType, String value, Integer max, Integer min) throws PKIException {
        ASN1SequenceExt temp = new ASN1SequenceExt();
        temp.addObject(new DERTaggedObject(valueType, TypeConversion(valueType, value)));
        if (min != null) {
            temp.addObject(new DERTaggedObject(0, new DERInteger(min.intValue())));
        }
        if (max != null) {
            temp.addObject(new DERTaggedObject(1, new DERInteger(max.intValue())));
        }
        return new GeneralSubtree(temp).getDERObject();
    }

    private DEREncodable TypeConversion(short valueType, String value) throws PKIException {
        DEREncodable derencodable = null;
        switch (valueType) {
            case 1:
            case 2:
            case 6:
                derencodable = new DERIA5String(value);
                break;
            case 3:
                throw new PKIException(PKIException.EXT_ENCODING_DATATYPE_NOT, PKIException.EXT_ENCODING_DATATYPE_NOT_DES);
            case 4:
                derencodable = new X509Name(value);
                break;
            case 7:
                derencodable = new DEROctetString(IP4Address(value));
                break;
        }
        if (derencodable != null) {
            return derencodable;
        }
        throw new PKIException(PKIException.EXT_ENCODING_DATATYPE_NOT, PKIException.EXT_ENCODING_DATATYPE_NOT_DES);
    }

    private byte[] IP4Address(String value) {
        String[] allip = value.split("/");
        if (allip.length == 2) {
            byte[] ip4_byte = new byte[8];
            GenIP4Address(ip4_byte, allip[0]);
            byte[] ip4_mask = new byte[4];
            GenIP4Address(ip4_mask, allip[1]);
            System.arraycopy(ip4_mask, 0, ip4_byte, 4, 4);
            return ip4_byte;
        } else if (allip.length != 1) {
            return null;
        } else {
            byte[] ip4_byte2 = new byte[4];
            GenIP4Address(ip4_byte2, value);
            return ip4_byte2;
        }
    }

    private void GenIP4Address(byte[] ip4_byte, String value) {
        for (int i = 0; i < 4; i++) {
            int pos = value.indexOf(".");
            if (pos == -1) {
                ip4_byte[i] = Integer.valueOf(value).byteValue();
            } else {
                ip4_byte[i] = Integer.valueOf(value.substring(0, pos)).byteValue();
            }
            value = value.substring(pos + 1, value.length());
        }
    }
}
