package cn.com.jit.ida.util.pki.asn1.pkcs.pkcs7;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1Set;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.DERTaggedObject;
import java.util.Enumeration;

public class SignedAndEnvelopedData implements DEREncodable {
    private ASN1Set certificates;
    private ASN1Set crls;
    private ASN1Set digestAlgorithms;
    private EncryptedContentInfo encryptedContentInfo;
    private ASN1Set recipientInfos;
    private ASN1Set signerInfos;
    private DERInteger version;

    public static SignedAndEnvelopedData getInstance(Object o) {
        if (o == null || (o instanceof SignedAndEnvelopedData)) {
            return (SignedAndEnvelopedData) o;
        }
        if (o instanceof ASN1Sequence) {
            return new SignedAndEnvelopedData((ASN1Sequence) o);
        }
        throw new IllegalArgumentException("unknown object in factory:" + o.getClass().getName());
    }

    public SignedAndEnvelopedData(DERInteger _version, ASN1Set _recipientInfos, ASN1Set _digestAlgorithms, EncryptedContentInfo _encryptedContentInfo, ASN1Set _certificates, ASN1Set _crls, ASN1Set _signerInfos) {
        this.version = _version;
        this.recipientInfos = _recipientInfos;
        this.digestAlgorithms = _digestAlgorithms;
        this.encryptedContentInfo = _encryptedContentInfo;
        this.certificates = _certificates;
        this.crls = _crls;
        this.signerInfos = _signerInfos;
    }

    public SignedAndEnvelopedData(ASN1Sequence seq) {
        Enumeration e = seq.getObjects();
        this.version = (DERInteger) e.nextElement();
        this.recipientInfos = (ASN1Set) e.nextElement();
        this.digestAlgorithms = (ASN1Set) e.nextElement();
        this.encryptedContentInfo = EncryptedContentInfo.getInstance(e.nextElement());
        while (e.hasMoreElements()) {
            DERObject o = (DERObject) e.nextElement();
            if (o instanceof DERTaggedObject) {
                DERTaggedObject tagged = (DERTaggedObject) o;
                switch (tagged.getTagNo()) {
                    case 0:
                        this.certificates = ASN1Set.getInstance(tagged, false);
                        continue;
                    case 1:
                        this.crls = ASN1Set.getInstance(tagged, false);
                        continue;
                    default:
                        throw new IllegalArgumentException("unknown tag value " + tagged.getTagNo());
                }
            } else {
                this.signerInfos = (ASN1Set) o;
            }
        }
    }

    public DERInteger getVersin() {
        return this.version;
    }

    public ASN1Set getRecipientInfos() {
        return this.recipientInfos;
    }

    public ASN1Set getDigestAlgorithms() {
        return this.digestAlgorithms;
    }

    public EncryptedContentInfo getEncryptedContentInfo() {
        return this.encryptedContentInfo;
    }

    public ASN1Set getCertificates() {
        return this.certificates;
    }

    public ASN1Set getCrls() {
        return this.crls;
    }

    public ASN1Set getSignerInfos() {
        return this.signerInfos;
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(this.version);
        v.add(this.recipientInfos);
        v.add(this.digestAlgorithms);
        v.add(this.encryptedContentInfo);
        if (this.certificates != null) {
            v.add(new DERTaggedObject(false, 0, this.certificates));
        }
        if (this.crls != null) {
            v.add(new DERTaggedObject(false, 1, this.crls));
        }
        v.add(this.signerInfos);
        return new DERSequence(v);
    }
}
