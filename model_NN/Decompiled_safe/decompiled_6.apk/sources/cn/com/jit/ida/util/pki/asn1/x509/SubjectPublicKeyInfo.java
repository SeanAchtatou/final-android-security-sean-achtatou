package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject;
import cn.com.jit.ida.util.pki.asn1.DERBitString;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERInputStream;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Enumeration;

public class SubjectPublicKeyInfo implements DEREncodable {
    private AlgorithmIdentifier algId;
    private DERBitString keyData;

    public static SubjectPublicKeyInfo getInstance(ASN1TaggedObject obj, boolean explicit) {
        return getInstance(ASN1Sequence.getInstance(obj, explicit));
    }

    public static SubjectPublicKeyInfo getInstance(Object obj) {
        if (obj instanceof SubjectPublicKeyInfo) {
            return (SubjectPublicKeyInfo) obj;
        }
        if (obj instanceof ASN1Sequence) {
            return new SubjectPublicKeyInfo((ASN1Sequence) obj);
        }
        throw new IllegalArgumentException("unknown object in factory");
    }

    public SubjectPublicKeyInfo(AlgorithmIdentifier algId2, DEREncodable publicKey) {
        this.keyData = new DERBitString(publicKey);
        this.algId = algId2;
    }

    public SubjectPublicKeyInfo(AlgorithmIdentifier algId2, byte[] publicKey) {
        this.keyData = new DERBitString(publicKey);
        this.algId = algId2;
    }

    public SubjectPublicKeyInfo(ASN1Sequence seq) {
        Enumeration e = seq.getObjects();
        this.algId = AlgorithmIdentifier.getInstance(e.nextElement());
        this.keyData = (DERBitString) e.nextElement();
    }

    public AlgorithmIdentifier getAlgorithmId() {
        return this.algId;
    }

    public DERObject getPublicKey() throws IOException {
        return new DERInputStream(new ByteArrayInputStream(this.keyData.getBytes())).readObject();
    }

    public DERBitString getPublicKeyData() {
        return this.keyData;
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(this.algId);
        v.add(this.keyData);
        return new DERSequence(v);
    }
}
