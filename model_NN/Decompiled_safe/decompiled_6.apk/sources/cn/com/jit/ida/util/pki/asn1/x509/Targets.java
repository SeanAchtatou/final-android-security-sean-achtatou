package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.ASN1Encodable;
import cn.com.jit.ida.util.pki.asn1.DEREncodableVector;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Targets extends ASN1Encodable {
    private int choice;
    private DEREncodableVector valueVector = new DEREncodableVector();

    public Targets(Node nl) throws PKIException {
        NodeList nodelist = nl.getChildNodes();
        if (nodelist.getLength() == 0) {
            throw new PKIException("XML内容缺失, Targets.Targets(), Targets没有子节点");
        }
        String type = nl.getAttributes().item(0).getNodeValue();
        if (type.equals("name")) {
            this.choice = 1;
        }
        if (type.equals("group")) {
            this.choice = 2;
        }
        if (this.choice == 1) {
            for (int i = 0; i < nodelist.getLength(); i++) {
                Node cnode = nodelist.item(i);
                if (cnode.getNodeName().equals("target")) {
                    this.valueVector.add(new GeneralName(cnode));
                }
            }
        }
        if (this.choice == 2) {
            for (int i2 = 0; i2 < nodelist.getLength(); i2++) {
                Node cnode2 = nodelist.item(i2);
                if (cnode2.getNodeName().equals("target")) {
                    this.valueVector.add(new GeneralName(cnode2));
                }
            }
        }
    }

    public DERObject toASN1Object() {
        DEREncodableVector vec = new DEREncodableVector();
        if (this.choice == 1) {
            for (int i = 0; i < this.valueVector.size(); i++) {
                vec.add(((GeneralName) this.valueVector.get(i)).getDERObject());
            }
        }
        if (this.choice == 2) {
            for (int i2 = 0; i2 < this.valueVector.size(); i2++) {
                vec.add(((GeneralName) this.valueVector.get(i2)).getDERObject());
            }
        }
        return new DERSequence(vec);
    }

    public void decode(DERObject parm1) throws PKIException {
        throw new UnsupportedOperationException("Method decode() not yet implemented.");
    }
}
