package cn.com.jit.ida.util.pki.extension;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.ASN1Encodable;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1Set;
import cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject;
import cn.com.jit.ida.util.pki.asn1.DERBitString;
import cn.com.jit.ida.util.pki.asn1.DEREncodableVector;
import cn.com.jit.ida.util.pki.asn1.DERSet;
import cn.com.jit.ida.util.pki.asn1.x509.DistributionPoint;
import cn.com.jit.ida.util.pki.asn1.x509.DistributionPointName;
import cn.com.jit.ida.util.pki.asn1.x509.GeneralNames;
import cn.com.jit.ida.util.pki.asn1.x509.ReasonFlags;

public class DistributionPointsExt {
    public static final int DIRECTORYNAME = 4;
    public static final int DNSNAME = 2;
    public static final int EDIPARTYNAME = 5;
    public static final int EDIPARTYNAME_BMPSTRING = 204;
    public static final int EDIPARTYNAME_PRINTABLESTRING = 201;
    public static final int EDIPARTYNAME_TELETEXSTRING = 200;
    public static final int EDIPARTYNAME_UNIVERSALSTRING = 202;
    public static final int EDIPARTYNAME_UTF8STRING = 203;
    public static final int FULL_NAME = 0;
    public static final int IPADDRESS = 7;
    public static final int NAMERELATIVETOCRLISSUER_BMPSTRING = 304;
    public static final int NAMERELATIVETOCRLISSUER_PRINTABLESTRING = 301;
    public static final int NAMERELATIVETOCRLISSUER_TELETEXSTRING = 300;
    public static final int NAMERELATIVETOCRLISSUER_UNIVERSALSTRING = 302;
    public static final int NAMERELATIVETOCRLISSUER_UTF8STRING = 303;
    public static final int NAME_RELATIVE_TO_CRL_ISSUER = 1;
    public static final int OTHERNAME_GUID = 101;
    public static final int OTHERNAME_UPN = 100;
    public static final String OTHER_NAME_GUID_OID = "1.3.6.1.4.1.311.25.1";
    public static final String OTHER_NAME_UPN_OID = "1.3.6.1.4.1.311.20.2.3";
    public static final int REASON_AA_COMPROMISE = 32768;
    public static final int REASON_AFFILIATION_CHANGED = 16;
    public static final int REASON_CA_COMPROMISE = 32;
    public static final int REASON_CERTIFICATE_HOLD = 2;
    public static final int REASON_CESSATION_OF_OPERATION = 4;
    public static final int REASON_KEY_COMPROMISE = 64;
    public static final int REASON_PRIVILEGE_WITHDRAWN = 1;
    public static final int REASON_SUPERSEDED = 8;
    public static final int REASON_UNUSED = 128;
    public static final int REGISTEREDID = 8;
    public static final int RFC822NAME = 1;
    public static final int TYPE_NONE = -1;
    public static final int UNIFORMRESOURCEIDENTIFIER = 6;
    public static final int X400ADDRESS = 3;
    private GeneralNamesExt CRLIssuer = null;
    private GeneralNamesExt FullName = null;
    private DEREncodableVector NameRelativeToCRLIssuer = null;
    private boolean[] bool_reasons = new boolean[9];
    private int isFullNameOrNameRelativeToCRLIssuer = 0;
    private int reason = 0;

    public DistributionPointsExt() {
        for (int i = 0; i < 9; i++) {
            this.bool_reasons[i] = false;
        }
    }

    public DistributionPointsExt(ASN1Sequence asn1Sequence) {
        for (int z = 0; z < 9; z++) {
            this.bool_reasons[z] = false;
        }
        for (int i = 0; i != asn1Sequence.size(); i++) {
            ASN1TaggedObject t = (ASN1TaggedObject) asn1Sequence.getObjectAt(i);
            switch (t.getTagNo()) {
                case 0:
                    ASN1TaggedObject temp = (ASN1TaggedObject) t.getObject();
                    if (temp.getTagNo() != 0) {
                        this.isFullNameOrNameRelativeToCRLIssuer = 1;
                        ASN1Set set = ASN1Set.getInstance(temp, false);
                        this.NameRelativeToCRLIssuer = new DEREncodableVector();
                        for (int x = 0; x < set.size(); x++) {
                            this.NameRelativeToCRLIssuer.add(ASN1Sequence.getInstance(set.getObjectAt(x)));
                        }
                        break;
                    } else {
                        this.FullName = new GeneralNamesExt(ASN1Sequence.getInstance(temp, false));
                        this.isFullNameOrNameRelativeToCRLIssuer = 0;
                        break;
                    }
                case 1:
                    ReasonFlags reasons = new ReasonFlags(DERBitString.getInstance(t, false));
                    byte[] breasons = reasons.getBytes();
                    int length = (breasons.length * 8) - reasons.getPadBits();
                    for (int j = 0; j != length; j++) {
                        this.bool_reasons[j] = (breasons[j / 8] & (128 >>> (j % 8))) != 0;
                        if (this.bool_reasons[j]) {
                            if (j == 0) {
                                this.reason |= 128;
                            } else if (j == 1) {
                                this.reason |= 64;
                            } else if (j == 2) {
                                this.reason |= 32;
                            } else if (j == 3) {
                                this.reason |= 16;
                            } else if (j == 4) {
                                this.reason |= 8;
                            } else if (j == 5) {
                                this.reason |= 4;
                            } else if (j == 6) {
                                this.reason |= 2;
                            } else if (j == 7) {
                                this.reason |= 1;
                            } else if (j == 8) {
                                this.reason |= 32768;
                            }
                        }
                    }
                    break;
                case 2:
                    this.CRLIssuer = new GeneralNamesExt(ASN1Sequence.getInstance(t, false));
                    break;
            }
        }
    }

    public void setFullNameOrNameRelativeToCRLIssuerType(int type) {
        this.isFullNameOrNameRelativeToCRLIssuer = type;
    }

    public int getFullNameOrNameRelativeToCRLIssuerType() {
        return this.isFullNameOrNameRelativeToCRLIssuer;
    }

    public void addDistributionPointNameByFullName(int type, String value) throws PKIException {
        if (this.FullName == null) {
            this.FullName = new GeneralNamesExt();
        }
        addGeneralNames(this.FullName, type, value);
    }

    public void addDistributionPointNameByFullName(int partyNameType, String partyName, int nameAssignerType, String nameAssigner) throws PKIException {
        if (this.FullName == null) {
            this.FullName = new GeneralNamesExt();
        }
        this.FullName.addEDIPartyName(partyNameType, partyName, nameAssignerType, nameAssigner);
    }

    public int getDistributionPointNameByFullNameCount() {
        if (this.FullName == null) {
            return -1;
        }
        return this.FullName.getGeneralNameCount();
    }

    public int getDistributionPointNameByFullNameType(int index) {
        if (this.FullName == null) {
            return -1;
        }
        return this.FullName.getGeneralNameType(index);
    }

    public String getDistributionPointNameByFullName(int index) {
        if (this.FullName == null) {
            return null;
        }
        return this.FullName.getGeneralName(index);
    }

    public String getDistributionPointNameByFullNameToNameAsSigner(int index) {
        if (this.FullName == null) {
            return null;
        }
        return this.FullName.getEDIPartyNameToNameAsSigner(index);
    }

    public String getDistributionPointNameByFullNameToPartyName(int index) {
        if (this.FullName == null) {
            return null;
        }
        return this.FullName.getEDIPartyNameToPartyName(index);
    }

    public void addDistributionPointNameByNameRelativeToCRLIssuer(String OID, int type, String value) throws PKIException {
        if (this.NameRelativeToCRLIssuer == null) {
            this.NameRelativeToCRLIssuer = new DEREncodableVector();
        }
        AttributeTypeAndValueExt atttypeandvalue = new AttributeTypeAndValueExt();
        atttypeandvalue.setAttributeType(OID);
        atttypeandvalue.setAttributeValue(type, value);
        this.NameRelativeToCRLIssuer.add(atttypeandvalue.getObject());
    }

    public int getDistributionPointNameByNameRelativeToCRLIssuerCount() {
        return this.NameRelativeToCRLIssuer.size();
    }

    public String getDistributionPointNameByNameRelativeToCRLIssuerOID(int index) {
        return new AttributeTypeAndValueExt((ASN1Sequence) this.NameRelativeToCRLIssuer.get(index)).getAttributeType();
    }

    public String getDistributionPointNameByNameRelativeToCRLIssuerValue(int index) {
        return new AttributeTypeAndValueExt((ASN1Sequence) this.NameRelativeToCRLIssuer.get(index)).getAttributeValue();
    }

    public void addCRLIssuer(int partyNameType, String partyName, int nameAssignerType, String nameAssigner) throws PKIException {
        if (this.CRLIssuer == null) {
            this.CRLIssuer = new GeneralNamesExt();
        }
        this.CRLIssuer.addEDIPartyName(partyNameType, partyName, nameAssignerType, nameAssigner);
    }

    public void addCRLIssuer(int type, String value) throws PKIException {
        if (this.CRLIssuer == null) {
            this.CRLIssuer = new GeneralNamesExt();
        }
        addGeneralNames(this.CRLIssuer, type, value);
    }

    public int getCRLIssuerCount() {
        if (this.CRLIssuer == null) {
            return -1;
        }
        return this.CRLIssuer.getGeneralNameCount();
    }

    public int getCRLIssuerType(int index) {
        if (this.CRLIssuer == null) {
            return -1;
        }
        return this.CRLIssuer.getGeneralNameType(index);
    }

    public String getCRLIssuer(int index) {
        if (this.CRLIssuer == null) {
            return null;
        }
        return this.CRLIssuer.getGeneralName(index);
    }

    public String getCRLIssuerToNameAsSigner(int index) {
        if (this.CRLIssuer == null) {
            return null;
        }
        return this.CRLIssuer.getEDIPartyNameToNameAsSigner(index);
    }

    public String getCRLIssuerToPartyName(int index) {
        if (this.CRLIssuer == null) {
            return null;
        }
        return this.CRLIssuer.getEDIPartyNameToPartyName(index);
    }

    public void addReasonFlags(int Type) {
        switch (Type) {
            case 1:
                this.bool_reasons[7] = true;
                break;
            case 2:
                this.bool_reasons[6] = true;
                break;
            case 4:
                this.bool_reasons[5] = true;
                break;
            case 8:
                this.bool_reasons[4] = true;
                break;
            case 16:
                this.bool_reasons[3] = true;
                break;
            case 32:
                this.bool_reasons[2] = true;
                break;
            case 64:
                this.bool_reasons[1] = true;
                break;
            case 128:
                this.bool_reasons[0] = true;
                break;
            case 32768:
                this.bool_reasons[8] = true;
                break;
            default:
                return;
        }
        this.reason |= Type;
    }

    public boolean getReasonFlags(int type) {
        switch (type) {
            case 1:
                return this.bool_reasons[7];
            case 2:
                return this.bool_reasons[6];
            case 4:
                return this.bool_reasons[5];
            case 8:
                return this.bool_reasons[4];
            case 16:
                return this.bool_reasons[3];
            case 32:
                return this.bool_reasons[2];
            case 64:
                return this.bool_reasons[1];
            case 128:
                return this.bool_reasons[0];
            case 32768:
                return this.bool_reasons[8];
            default:
                return false;
        }
    }

    public ASN1Encodable getObject() throws PKIException {
        DistributionPointName distPoiontName;
        if (this.isFullNameOrNameRelativeToCRLIssuer == 0) {
            distPoiontName = new DistributionPointName(0, new GeneralNames((ASN1Sequence) this.FullName.getDERObject()));
        } else {
            distPoiontName = new DistributionPointName(1, new DERSet(this.NameRelativeToCRLIssuer));
        }
        return new DistributionPoint(distPoiontName, new ReasonFlags(this.reason), new GeneralNames((ASN1Sequence) this.CRLIssuer.getDERObject()));
    }

    private void addGeneralNames(GeneralNamesExt gne, int type, String value) throws PKIException {
        switch (type) {
            case 1:
                gne.addRFC822Name(value);
                return;
            case 2:
                gne.addDNSName(value);
                return;
            case 4:
                gne.addDirectoryName(value);
                return;
            case 6:
                gne.addUniformResourceIdentifier(value);
                return;
            case 7:
                gne.addIPAddress(value);
                return;
            case 8:
                gne.addRegisteredID(value);
                return;
            case 100:
                gne.addOtherName_UPN(value);
                return;
            case 101:
                gne.addOtherName_GUID(value);
                return;
            default:
                return;
        }
    }
}
