package cn.com.jit.ida.util.pki.asn1.pkcs.pkcs5;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.x509.AlgorithmIdentifier;

public class EncryptionScheme extends AlgorithmIdentifier {
    DERObject obj;
    DERObject objectId;

    public EncryptionScheme(ASN1Sequence seq) {
        super(seq);
        this.objectId = (DERObject) seq.getObjectAt(0);
        if (seq.size() == 2) {
            this.obj = (DERObject) seq.getObjectAt(1);
        } else {
            this.obj = null;
        }
    }

    public DERObject getObject() {
        return this.obj;
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(this.objectId);
        if (this.obj != null) {
            v.add(this.obj);
        }
        return new DERSequence(v);
    }
}
