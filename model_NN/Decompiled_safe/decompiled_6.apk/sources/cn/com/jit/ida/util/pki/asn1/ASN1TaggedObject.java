package cn.com.jit.ida.util.pki.asn1;

import java.io.IOException;

public abstract class ASN1TaggedObject extends DERObject {
    boolean empty;
    boolean explicit;
    DEREncodable obj;
    int tagNo;

    /* access modifiers changed from: package-private */
    public abstract void encode(DEROutputStream dEROutputStream) throws IOException;

    public static ASN1TaggedObject getInstance(ASN1TaggedObject obj2, boolean explicit2) {
        if (explicit2) {
            return (ASN1TaggedObject) obj2.getObject();
        }
        throw new IllegalArgumentException("implicitly tagged tagged object");
    }

    public ASN1TaggedObject(int tagNo2, DEREncodable obj2) {
        this.empty = false;
        this.explicit = true;
        this.obj = null;
        this.explicit = true;
        this.tagNo = tagNo2;
        this.obj = obj2;
    }

    public ASN1TaggedObject(boolean explicit2, int tagNo2, DEREncodable obj2) {
        this.empty = false;
        this.explicit = true;
        this.obj = null;
        this.explicit = explicit2;
        this.tagNo = tagNo2;
        this.obj = obj2;
    }

    public boolean equals(Object o) {
        if (o == null || !(o instanceof ASN1TaggedObject)) {
            return false;
        }
        ASN1TaggedObject other = (ASN1TaggedObject) o;
        if (this.tagNo != other.tagNo || this.empty != other.empty || this.explicit != other.explicit) {
            return false;
        }
        if (this.obj == null) {
            if (other.obj == null) {
                return true;
            }
            return false;
        } else if (!this.obj.equals(other.obj)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int code = this.tagNo;
        if (this.obj != null) {
            return code ^ this.obj.hashCode();
        }
        return code;
    }

    public int getTagNo() {
        return this.tagNo;
    }

    public boolean isExplicit() {
        return this.explicit;
    }

    public boolean isEmpty() {
        return this.empty;
    }

    public DERObject getObject() {
        if (this.obj != null) {
            return this.obj.getDERObject();
        }
        return null;
    }
}
