package cn.com.jit.ida.util.pki.cipher.softsm;

import android.support.v4.view.MotionEventCompat;
import java.math.BigInteger;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.util.encoders.Hex;

public class KeyExchange {
    private BigInteger _2w;
    private BigInteger _2w_1;
    private int ct = 1;
    private byte[] key = null;
    private int keyOff = 0;
    private BigInteger rD;
    private ECPoint rKey;
    private SM2 sm2;
    private SM3Digest sm3keybase = null;
    private BigInteger userD;
    private ECPoint userKey;

    public void Init(SM2 sm22, BigInteger userD2, ECPoint userKey2) {
        this.sm2 = sm22;
        this.userD = userD2;
        this.userKey = userKey2;
        AsymmetricCipherKeyPair keypair = sm22.ecc_key_pair_generator.generateKeyPair();
        this.rD = keypair.getPrivate().getD();
        this.rKey = keypair.getPublic().getQ();
        this._2w = BigInteger.ONE.shiftLeft((sm22.ecc_n.bitLength() / 2) - 1);
        this._2w_1 = this._2w.subtract(BigInteger.ONE);
    }

    public void Init_test_a(SM2 sm22, BigInteger userD2, ECPoint userKey2) {
        Init(sm22, userD2, userKey2);
        this.rD = new BigInteger("83A2C9C8B96E5AF70BD480B472409A9A327257F1EBB73F5B073354B248668563", 16);
        this.rKey = sm22.ecc_point_g.multiply(this.rD);
    }

    public void Init_test_b(SM2 sm22, BigInteger userD2, ECPoint userKey2) {
        Init(sm22, userD2, userKey2);
        this.rD = new BigInteger("33FE21940342161C55619C4A0C060293D543C80AF19748CE176D83477DE71C80", 16);
        this.rKey = sm22.ecc_point_g.multiply(this.rD);
    }

    public void DoA_1_3(SM2Result sm2Ret) {
        sm2Ret.keyra = this.rKey;
    }

    public void DoB_1_10(byte[] ida, byte[] idb, ECPoint keyusera, ECPoint keyra, SM2Result sm2Ret) {
        sm2Ret.keyrb = this.rKey;
        sm2Ret.sb = null;
        sm2Ret.s2 = null;
        byte[] za = null;
        byte[] zb = null;
        if (ida != null) {
            za = this.sm2.Sm2GetZ(ida, keyusera);
        }
        if (idb != null) {
            zb = this.sm2.Sm2GetZ(idb, this.userKey);
        }
        BigInteger x2_ = this._2w.add(this.rKey.getX().toBigInteger().and(this._2w_1));
        System.out.println("x2_ = \n" + x2_.toString(16));
        BigInteger tb = this.userD.add(this.rD.multiply(x2_)).mod(this.sm2.ecc_n);
        System.out.println("tb = \n" + tb.toString(16));
        BigInteger x1_ = this._2w.add(keyra.getX().toBigInteger().and(this._2w_1));
        System.out.println("x1_ = \n" + x1_.toString(16));
        ECPoint pa0 = keyra.multiply(x1_);
        System.out.println("pa0 (x,y) = ");
        System.out.println(pa0.getX().toBigInteger().toString(16));
        System.out.println(pa0.getY().toBigInteger().toString(16));
        ECPoint pa1 = keyusera.add(pa0);
        System.out.println("pa1 (x,y) = ");
        System.out.println(pa1.getX().toBigInteger().toString(16));
        System.out.println(pa1.getY().toBigInteger().toString(16));
        ECPoint pv = pa1.multiply(tb.multiply(this.sm2.ecc_bc_spec.getH()));
        System.out.println("pv (x,y) = ");
        System.out.println(pv.getX().toBigInteger().toString(16));
        System.out.println(pv.getY().toBigInteger().toString(16));
        if (!pv.isInfinity()) {
            this.sm3keybase = new SM3Digest();
            byte[] p = Util.byteconvert32(pv.getX().toBigInteger());
            this.sm3keybase.BlockUpdate(p, 0, p.length);
            byte[] p2 = Util.byteconvert32(pv.getY().toBigInteger());
            this.sm3keybase.BlockUpdate(p2, 0, p2.length);
            byte[] p3 = za;
            this.sm3keybase.BlockUpdate(p3, 0, p3.length);
            byte[] p4 = zb;
            this.sm3keybase.BlockUpdate(p4, 0, p4.length);
            this.ct = 1;
            this.key = new byte[32];
            this.keyOff = 0;
            NextKey();
            byte[] mdb = new byte[32];
            byte[] mdsb = new byte[32];
            byte[] mds2 = new byte[32];
            SM3Digest hashb = new SM3Digest();
            byte[] p5 = Util.byteconvert32(pv.getX().toBigInteger());
            hashb.BlockUpdate(p5, 0, p5.length);
            if (za != null) {
                hashb.BlockUpdate(za, 0, za.length);
            }
            if (zb != null) {
                hashb.BlockUpdate(zb, 0, zb.length);
            }
            byte[] p6 = Util.byteconvert32(keyra.getX().toBigInteger());
            hashb.BlockUpdate(p6, 0, p6.length);
            byte[] p7 = Util.byteconvert32(keyra.getY().toBigInteger());
            hashb.BlockUpdate(p7, 0, p7.length);
            byte[] p8 = Util.byteconvert32(this.rKey.getX().toBigInteger());
            hashb.BlockUpdate(p8, 0, p8.length);
            byte[] p9 = Util.byteconvert32(this.rKey.getY().toBigInteger());
            hashb.BlockUpdate(p9, 0, p9.length);
            hashb.doFinal(mdb, 0);
            System.out.println("hash1 = \n" + new String(Hex.encode(mdb)));
            hashb.reset();
            hashb.update((byte) 2);
            byte[] p10 = Util.byteconvert32(pv.getY().toBigInteger());
            hashb.BlockUpdate(p10, 0, p10.length);
            byte[] p11 = mdb;
            hashb.BlockUpdate(p11, 0, p11.length);
            hashb.doFinal(mdsb, 0);
            System.out.println("sb = \n" + new String(Hex.encode(mdsb)));
            hashb.reset();
            hashb.update((byte) 3);
            byte[] p12 = Util.byteconvert32(pv.getY().toBigInteger());
            hashb.BlockUpdate(p12, 0, p12.length);
            byte[] p13 = mdb;
            hashb.BlockUpdate(p13, 0, p13.length);
            hashb.doFinal(mds2, 0);
            System.out.println("s2 = \n" + new String(Hex.encode(mds2)));
            sm2Ret.s2 = mds2;
            sm2Ret.sb = mdsb;
        }
    }

    public void DoA_4_10(byte[] ida, byte[] idb, ECPoint keyuserb, ECPoint keyrb, SM2Result sm2Ret) {
        sm2Ret.keyra = this.rKey;
        sm2Ret.sa = null;
        sm2Ret.s1 = null;
        byte[] za = null;
        byte[] zb = null;
        if (ida != null) {
            za = this.sm2.Sm2GetZ(ida, this.userKey);
        }
        if (idb != null) {
            zb = this.sm2.Sm2GetZ(idb, keyuserb);
        }
        BigInteger x1_a = this._2w.add(this.rKey.getX().toBigInteger().and(this._2w_1));
        System.out.println("x1_a = \n" + x1_a.toString(16));
        BigInteger x2_a = this._2w.add(keyrb.getX().toBigInteger().and(this._2w_1));
        System.out.println("x2_a = \n" + x2_a.toString(16));
        BigInteger ta = this.userD.add(this.rD.multiply(x1_a)).mod(this.sm2.ecc_n);
        System.out.println("ta = \n" + ta.toString(16));
        ECPoint pb0 = keyrb.multiply(x2_a);
        System.out.println("pb0 (x,y) = ");
        System.out.println(pb0.getX().toBigInteger().toString(16));
        System.out.println(pb0.getY().toBigInteger().toString(16));
        ECPoint pb1 = keyuserb.add(pb0);
        System.out.println("pb1 (x,y) = ");
        System.out.println(pb1.getX().toBigInteger().toString(16));
        System.out.println(pb1.getY().toBigInteger().toString(16));
        ECPoint pu = pb1.multiply(ta.multiply(this.sm2.ecc_bc_spec.getH()));
        System.out.println("pu (x,y) = ");
        System.out.println(pu.getX().toBigInteger().toString(16));
        System.out.println(pu.getY().toBigInteger().toString(16));
        if (!pu.isInfinity()) {
            SM3Digest sm3keybase1 = new SM3Digest();
            byte[] p1 = Util.byteconvert32(pu.getX().toBigInteger());
            sm3keybase1.BlockUpdate(p1, 0, p1.length);
            byte[] p12 = Util.byteconvert32(pu.getY().toBigInteger());
            sm3keybase1.BlockUpdate(p12, 0, p12.length);
            byte[] p13 = za;
            sm3keybase1.BlockUpdate(p13, 0, p13.length);
            byte[] p14 = zb;
            sm3keybase1.BlockUpdate(p14, 0, p14.length);
            this.sm3keybase = new SM3Digest();
            byte[] p = Util.byteconvert32(pu.getX().toBigInteger());
            this.sm3keybase.BlockUpdate(p, 0, p.length);
            byte[] p2 = Util.byteconvert32(pu.getY().toBigInteger());
            this.sm3keybase.BlockUpdate(p2, 0, p2.length);
            byte[] p3 = za;
            this.sm3keybase.BlockUpdate(p3, 0, p3.length);
            byte[] p4 = zb;
            this.sm3keybase.BlockUpdate(p4, 0, p4.length);
            this.ct = 1;
            this.key = new byte[32];
            this.keyOff = 0;
            NextKey();
            byte[] mda = new byte[32];
            byte[] mdsa = new byte[32];
            byte[] mds1 = new byte[32];
            SM3Digest hasha = new SM3Digest();
            byte[] p5 = Util.byteconvert32(pu.getX().toBigInteger());
            hasha.BlockUpdate(p5, 0, p5.length);
            if (za != null) {
                hasha.BlockUpdate(za, 0, za.length);
            }
            if (zb != null) {
                hasha.BlockUpdate(zb, 0, zb.length);
            }
            byte[] p6 = Util.byteconvert32(this.rKey.getX().toBigInteger());
            hasha.BlockUpdate(p6, 0, p6.length);
            byte[] p7 = Util.byteconvert32(this.rKey.getY().toBigInteger());
            hasha.BlockUpdate(p7, 0, p7.length);
            byte[] p8 = Util.byteconvert32(keyrb.getX().toBigInteger());
            hasha.BlockUpdate(p8, 0, p8.length);
            byte[] p9 = Util.byteconvert32(keyrb.getY().toBigInteger());
            hasha.BlockUpdate(p9, 0, p9.length);
            hasha.doFinal(mda, 0);
            System.out.println("hash1 = \n" + new String(Hex.encode(mda)));
            hasha.reset();
            hasha.update((byte) 3);
            byte[] p10 = Util.byteconvert32(pu.getY().toBigInteger());
            hasha.BlockUpdate(p10, 0, p10.length);
            byte[] p11 = mda;
            hasha.BlockUpdate(p11, 0, p11.length);
            hasha.doFinal(mdsa, 0);
            System.out.println("sa = \n" + new String(Hex.encode(mdsa)));
            hasha.reset();
            hasha.update((byte) 2);
            byte[] p15 = Util.byteconvert32(pu.getY().toBigInteger());
            hasha.BlockUpdate(p15, 0, p15.length);
            byte[] p16 = mda;
            hasha.BlockUpdate(p16, 0, p16.length);
            hasha.doFinal(mds1, 0);
            System.out.println("s1 = \n" + new String(Hex.encode(mds1)));
            sm2Ret.s1 = mds1;
            sm2Ret.sa = mdsa;
        }
    }

    public void GetKey(byte[] keybuf) {
        for (int i = 0; i < keybuf.length; i++) {
            if (this.keyOff == this.key.length) {
                NextKey();
            }
            byte[] bArr = this.key;
            int i2 = this.keyOff;
            this.keyOff = i2 + 1;
            keybuf[i] = bArr[i2];
        }
    }

    private void NextKey() {
        SM3Digest sm3keycur = new SM3Digest(this.sm3keybase);
        sm3keycur.update((byte) ((this.ct >> 24) & MotionEventCompat.ACTION_MASK));
        sm3keycur.update((byte) ((this.ct >> 16) & MotionEventCompat.ACTION_MASK));
        sm3keycur.update((byte) ((this.ct >> 8) & MotionEventCompat.ACTION_MASK));
        sm3keycur.update((byte) (this.ct & MotionEventCompat.ACTION_MASK));
        sm3keycur.doFinal(this.key, 0);
        this.keyOff = 0;
        this.ct++;
    }
}
