package cn.com.jit.ida.util.pki.extension;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.ASN1Set;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.DERPrintableString;
import cn.com.jit.ida.util.pki.asn1.DERUTF8String;
import cn.com.jit.ida.util.pki.asn1.x509.JITIdentifyCode;
import cn.com.jit.ida.util.pki.asn1.x509.X509Extensions;

public class IdentifyCodeExt extends AbstractStandardExtension {
    private JITIdentifyCode jitIdentifyCode = null;
    private String militaryOfficerCardNumber = null;
    private String passportNumber = null;
    private String residenterCardNumber = null;

    public IdentifyCodeExt() {
        this.OID = X509Extensions.JIT_IdentifyCode.getId();
        this.critical = false;
    }

    public IdentifyCodeExt(ASN1Set obj) {
        this.jitIdentifyCode = new JITIdentifyCode(obj);
    }

    public IdentifyCodeExt(String residenterCardNumber2, String militaryOfficerCardNumber2, String passportNumber2) {
        this.OID = X509Extensions.JIT_IdentifyCode.getId();
        this.critical = false;
        this.residenterCardNumber = residenterCardNumber2;
        this.militaryOfficerCardNumber = militaryOfficerCardNumber2;
        this.passportNumber = passportNumber2;
    }

    public String getResidenterCardNumber() {
        if (this.jitIdentifyCode == null || this.jitIdentifyCode.getResidenterCardNumber() == null) {
            return null;
        }
        return this.jitIdentifyCode.getResidenterCardNumber().getString();
    }

    public String getMilitaryOfficerCardNumber() {
        if (this.jitIdentifyCode == null || this.jitIdentifyCode.getMilitaryOfficerCardNumber() == null) {
            return null;
        }
        return this.jitIdentifyCode.getMilitaryOfficerCardNumber().getString();
    }

    public String getPassportNumber() {
        if (this.jitIdentifyCode == null || this.jitIdentifyCode.getPassportNumber() == null) {
            return null;
        }
        return this.jitIdentifyCode.getPassportNumber().getString();
    }

    public void setResidenterCardNumber(String value) {
        this.residenterCardNumber = value;
    }

    public void setMilitaryOfficerCardNumber(String value) {
        this.militaryOfficerCardNumber = value;
    }

    public void setPassportNumber(String value) {
        this.passportNumber = value;
    }

    public byte[] encode() throws PKIException {
        if (this.residenterCardNumber != null || this.militaryOfficerCardNumber != null || this.passportNumber != null) {
            return new DEROctetString(new JITIdentifyCode(new DERPrintableString(this.residenterCardNumber), new DERUTF8String(this.militaryOfficerCardNumber), new DERPrintableString(this.passportNumber)).getDERObject()).getOctets();
        }
        throw new PKIException(PKIException.EXT_ENCODING_DATA_NULL, PKIException.EXT_ENCODING_DATA_NULL_DES);
    }

    public boolean getCritical() {
        return this.critical;
    }

    public String getOID() {
        return this.OID;
    }

    public void setCritical(boolean critical) {
        this.critical = critical;
    }
}
