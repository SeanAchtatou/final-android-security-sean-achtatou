package cn.com.jit.ida.util.pki.extension;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.x509.JITCasteCode;
import cn.com.jit.ida.util.pki.asn1.x509.X509Extensions;

public class CasteCodeExt extends AbstractStandardExtension {
    private String castecode = null;

    public CasteCodeExt() {
        this.OID = X509Extensions.JIT_CasteCode.getId();
        this.critical = false;
    }

    public CasteCodeExt(String value) {
        this.OID = X509Extensions.JIT_CasteCode.getId();
        this.critical = false;
        this.castecode = value;
    }

    public void SetCasteCode(String value) {
        this.castecode = value;
    }

    public byte[] encode() throws PKIException {
        if (this.castecode != null) {
            return new DEROctetString(new JITCasteCode(this.castecode).getDERObject()).getOctets();
        }
        throw new PKIException(PKIException.EXT_ENCODING_DATA_NULL, PKIException.EXT_ENCODING_DATA_NULL_DES);
    }

    public boolean getCritical() {
        return this.critical;
    }

    public String getOID() {
        return this.OID;
    }

    public void setCritical(boolean critical) {
        this.critical = critical;
    }
}
