package cn.com.jit.ida.util.pki.pkcs;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.Parser;
import cn.com.jit.ida.util.pki.asn1.ASN1InputStream;
import cn.com.jit.ida.util.pki.asn1.ASN1OctetString;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1Set;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DEREncodableVector;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.DEROutputStream;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.DERSet;
import cn.com.jit.ida.util.pki.asn1.pkcs.PKCSObjectIdentifiers;
import cn.com.jit.ida.util.pki.asn1.pkcs.RSAPrivateKeyStructure;
import cn.com.jit.ida.util.pki.asn1.pkcs.pkcs12.AuthenticatedSafe;
import cn.com.jit.ida.util.pki.asn1.pkcs.pkcs12.CertBag;
import cn.com.jit.ida.util.pki.asn1.pkcs.pkcs12.MacData;
import cn.com.jit.ida.util.pki.asn1.pkcs.pkcs12.PKCS12PBEParams;
import cn.com.jit.ida.util.pki.asn1.pkcs.pkcs12.Pfx;
import cn.com.jit.ida.util.pki.asn1.pkcs.pkcs12.SafeBag;
import cn.com.jit.ida.util.pki.asn1.pkcs.pkcs12.SafeContents;
import cn.com.jit.ida.util.pki.asn1.pkcs.pkcs7.ContentInfo;
import cn.com.jit.ida.util.pki.asn1.pkcs.pkcs7.EncryptedContentInfo;
import cn.com.jit.ida.util.pki.asn1.pkcs.pkcs7.EncryptedData;
import cn.com.jit.ida.util.pki.asn1.pkcs.pkcs8.PrivateKeyInfo;
import cn.com.jit.ida.util.pki.asn1.x509.AlgorithmIdentifier;
import cn.com.jit.ida.util.pki.asn1.x509.Attribute;
import cn.com.jit.ida.util.pki.asn1.x509.DigestInfo;
import cn.com.jit.ida.util.pki.asn1.x509.X509CertificateStructure;
import cn.com.jit.ida.util.pki.cert.X509Cert;
import cn.com.jit.ida.util.pki.cipher.JCrypto;
import cn.com.jit.ida.util.pki.cipher.JKey;
import cn.com.jit.ida.util.pki.cipher.Mechanism;
import cn.com.jit.ida.util.pki.cipher.Session;
import cn.com.jit.ida.util.pki.cipher.param.CBCParam;
import cn.com.jit.ida.util.pki.encoders.Base64;
import com.jianq.misc.StringEx;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.security.SecureRandom;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Vector;
import javax.crypto.EncryptedPrivateKeyInfo;
import javax.crypto.spec.PBEParameterSpec;
import org.bouncycastle.crypto.BufferedBlockCipher;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.digests.MD2Digest;
import org.bouncycastle.crypto.digests.MD5Digest;
import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.crypto.engines.RC2Engine;
import org.bouncycastle.crypto.generators.PKCS12ParametersGenerator;
import org.bouncycastle.crypto.macs.HMac;
import org.bouncycastle.crypto.modes.CBCBlockCipher;
import org.bouncycastle.crypto.modes.PaddedBlockCipher;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.crypto.params.RSAPrivateCrtKeyParameters;

public class PKCS12 {
    private static final int ITERATIONS = 2000;
    private CertBag[] certBags;
    private ContentInfo certContent;
    private boolean decrypted = false;
    private ContentInfo keyContent;
    private byte[] password;
    private Pfx pfx;
    private DEREncodable privateKeyInfo;
    private Session session = null;

    public PKCS12() {
        JCrypto jcrypto = JCrypto.getInstance();
        try {
            jcrypto.initialize(JCrypto.JSOFT_LIB, null);
            this.session = jcrypto.openSession(JCrypto.JSOFT_LIB);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.pfx = null;
        this.certBags = null;
        this.privateKeyInfo = null;
        this.keyContent = null;
        this.certContent = null;
        this.password = null;
        this.decrypted = false;
    }

    public void load(Pfx _pfx) {
        this.pfx = _pfx;
    }

    public void load(String fileName) throws PKIException {
        try {
            FileInputStream fin = new FileInputStream(fileName);
            byte[] data = new byte[fin.available()];
            fin.read(data);
            fin.close();
            load(data);
        } catch (Exception ex) {
            throw new PKIException("8176", PKIException.LOAD_P12_ERR_DES, ex);
        }
    }

    public void load(InputStream ins) throws PKIException {
        try {
            ASN1InputStream ais = new ASN1InputStream(ins);
            this.pfx = Pfx.getInstance(ais.readObject());
            ais.close();
            ins.close();
        } catch (Exception ex) {
            throw new PKIException("8176", PKIException.LOAD_P12_ERR_DES, ex);
        }
    }

    public void load(byte[] data) throws PKIException {
        if (Parser.isBase64Encode(data)) {
            data = Base64.decode(Parser.convertBase64(data));
        }
        try {
            ByteArrayInputStream bis = new ByteArrayInputStream(data);
            ASN1InputStream ais = new ASN1InputStream(bis);
            this.pfx = Pfx.getInstance(ais.readObject());
            ais.close();
            bis.close();
        } catch (Exception ex) {
            throw new PKIException("8176", PKIException.LOAD_P12_ERR_DES, ex);
        }
    }

    public void decrypt(char[] _password) throws PKIException {
        try {
            if (this.pfx == null) {
                throw new Exception("you must load Pfx first.");
            }
            this.password = PKCS12ParametersGenerator.PKCS12PasswordToBytes(_password);
            if (!verifyMac()) {
                throw new Exception("verifyMac faulture.");
            }
            ContentInfo[] contentInfo = AuthenticatedSafe.getInstance(oct2Seq(ASN1OctetString.getInstance(this.pfx.getAuthSafe().getContent()))).getContentInfo();
            for (int i = 0; i < contentInfo.length; i++) {
                if (contentInfo[i].getContentType().equals(PKCSObjectIdentifiers.data)) {
                    this.keyContent = contentInfo[i];
                } else if (contentInfo[i].getContentType().equals(PKCSObjectIdentifiers.encryptedData)) {
                    this.certContent = contentInfo[i];
                }
            }
            handleKeyContent(this.keyContent);
            handleCertContent(this.certContent);
            this.decrypted = true;
        } catch (Exception ex) {
            throw new PKIException("8177", PKIException.DECRYPT_P12_ERR_DES, ex);
        }
    }

    public JKey getPrivateKey() throws PKIException {
        try {
            if (!this.decrypted) {
                throw new Exception("pfx file hasn't been decrypted yet.");
            }
            ASN1Sequence s = (ASN1Sequence) this.privateKeyInfo;
            PrivateKeyInfo pki = new PrivateKeyInfo(s);
            PKCS8EncodedKeySpec p8KeySpec = new PKCS8EncodedKeySpec(Parser.writeDERObj2Bytes(s));
            if (pki.getAlgorithmId().getObjectId().equals(PKCSObjectIdentifiers.rsaEncryption)) {
                return new JKey(JKey.RSA_PRV_KEY, p8KeySpec.getEncoded());
            }
            return null;
        } catch (Exception ex) {
            throw new PKIException("8178", PKIException.P12_GETPRVKEY_ERR_DES, ex);
        }
    }

    public X509Cert[] getCerts() throws PKIException {
        try {
            if (!this.decrypted) {
                throw new Exception("pfx file hasn't been decrypted yet.");
            }
            Vector v = new Vector();
            for (int i = 0; i < this.certBags.length; i++) {
                DERObjectIdentifier certId = this.certBags[i].getCertId();
                if (certId.equals(PKCSObjectIdentifiers.x509certType)) {
                    v.add(new X509Cert(X509CertificateStructure.getInstance(oct2Seq(ASN1OctetString.getInstance(this.certBags[i].getCertValue())))));
                } else if (!certId.equals(PKCSObjectIdentifiers.sdsiCertType)) {
                    throw new Exception("not support certBag type, id=" + certId.getId());
                }
            }
            X509Cert[] certs = new X509Cert[v.size()];
            v.toArray(certs);
            return certs;
        } catch (Exception ex) {
            throw new PKIException("8179", PKIException.P12_GETPUBCERT_ERR_DES, ex);
        }
    }

    public X509Cert getCertificate() throws PKIException {
        Mechanism signM;
        X509Cert[] certs = getCerts();
        JKey prvKey = getPrivateKey();
        if (certs == null) {
            return null;
        }
        int m = 0;
        for (int i = 0; i < certs.length; i++) {
            JKey pubKey = certs[i].getPublicKey();
            if (pubKey.getKeyType().equals(JKey.RSA_PUB_KEY)) {
                signM = new Mechanism("SHA1withRSAEncryption");
            } else if (pubKey.getKeyType().equals(JKey.SM2_PUB_KEY)) {
                signM = new Mechanism("SM3withSM2Encryption");
            } else {
                signM = new Mechanism("SHA1withECDSA");
            }
            byte[] sourceData = "JIT".getBytes();
            if (this.session.verifySign(signM, pubKey, sourceData, this.session.sign(signM, prvKey, sourceData))) {
                m = i;
            }
        }
        return certs[m];
    }

    private boolean verifyMac() throws Exception {
        PKCS12ParametersGenerator p12gen;
        int keyLen;
        Mechanism macM;
        MacData macData = this.pfx.getMacData();
        DigestInfo digestInfo = macData.getMac();
        String oid = digestInfo.getAlgorithmId().getObjectId().getId();
        if (oid.equals("1.3.14.3.2.26")) {
            p12gen = new PKCS12ParametersGenerator(new SHA1Digest());
            keyLen = 160;
            macM = new Mechanism(Mechanism.HMAC_SHA1);
        } else if (oid.equals("1.2.840.113549.2.2")) {
            p12gen = new PKCS12ParametersGenerator(new MD2Digest());
            keyLen = 128;
            macM = new Mechanism(Mechanism.HMAC_MD2);
        } else if (oid.equals("1.2.840.113549.2.5")) {
            p12gen = new PKCS12ParametersGenerator(new MD5Digest());
            keyLen = 128;
            macM = new Mechanism(Mechanism.HMAC_MD5);
        } else {
            throw new Exception("not support digest algorithmIdentifier:" + oid);
        }
        p12gen.init(this.password, macData.getSalt(), macData.getIterationCount().intValue());
        return Parser.isEqualArray(this.session.mac(macM, new JKey("DESede", p12gen.generateDerivedMacParameters(keyLen).getKey()), ASN1OctetString.getInstance(this.pfx.getAuthSafe().getContent()).getOctets()), digestInfo.getDigest());
    }

    private void handleCertContent(ContentInfo certContent2) throws Exception {
        EncryptedContentInfo eci = EncryptedData.getInstance(certContent2.getContent()).getEncryptedContentInfo();
        PKCS12PBEParams pm = PKCS12PBEParams.getInstance(eci.getContentEncryptionAlgorithm().getParameters());
        byte[] salt = pm.getIV();
        int iterations = pm.getIterations().intValue();
        PKCS12ParametersGenerator p12gen = new PKCS12ParametersGenerator(new SHA1Digest());
        p12gen.init(this.password, salt, iterations);
        SafeBag[] safeBag = SafeContents.getInstance((ASN1Sequence) new ASN1InputStream(new ByteArrayInputStream(pbeDecrypt(eci.getContentEncryptionAlgorithm().getObjectId().getId(), p12gen, eci.getEncryptedContent().getOctets()))).readObject()).getSafeBag();
        Vector v = new Vector();
        for (int i = 0; i < safeBag.length; i++) {
            if (safeBag[i].getBagId().equals(PKCSObjectIdentifiers.certBag)) {
                CertBag cb = CertBag.getInstance(safeBag[i].getBagValue());
                ASN1Set set = safeBag[i].getBagAttributes();
                v.add(cb);
            }
        }
        this.certBags = new CertBag[v.size()];
        v.toArray(this.certBags);
    }

    private void handleKeyContent(ContentInfo keyContent2) throws Exception {
        SafeBag keyBag = SafeContents.getInstance(oct2Seq(ASN1OctetString.getInstance(keyContent2.getContent()))).getSafeBag()[0];
        if (keyBag.getBagId().equals(PKCSObjectIdentifiers.keyBag)) {
            this.privateKeyInfo = new PrivateKeyInfo((ASN1Sequence) keyBag.getBagValue());
        } else if (keyBag.getBagId().equals(PKCSObjectIdentifiers.pkcs8ShroudedKeyBag)) {
            EncryptedPrivateKeyInfo epki = new EncryptedPrivateKeyInfo(Parser.writeDERObj2Bytes(keyBag.getBagValue()));
            PBEParameterSpec pbeParamSpec = (PBEParameterSpec) epki.getAlgParameters().getParameterSpec(PBEParameterSpec.class);
            byte[] salt = pbeParamSpec.getSalt();
            int iterations = pbeParamSpec.getIterationCount();
            PKCS12ParametersGenerator p12gen = new PKCS12ParametersGenerator(new SHA1Digest());
            p12gen.init(this.password, salt, iterations);
            this.privateKeyInfo = (ASN1Sequence) new ASN1InputStream(new ByteArrayInputStream(pbeDecrypt(epki.getAlgParameters().getAlgorithm(), p12gen, epki.getEncryptedData()))).readObject();
            Attribute.getInstance(keyBag.getBagAttributes().getObjectAt(0));
        } else {
            throw new Exception("handle keyBag error. bagId = " + keyBag.getBagId().getId());
        }
    }

    private byte[] pbeDecrypt(String algName, PKCS12ParametersGenerator p12gen, byte[] en_data) throws Exception {
        if (algName.equals(PKCSObjectIdentifiers.pbeWithSHAAnd3DESCBC.getId())) {
            ParametersWithIV ivParam = p12gen.generateDerivedParameters(192, 64);
            byte[] iv = ivParam.getIV();
            JKey key = new JKey("DESede", ivParam.getParameters().getKey());
            CBCParam cbcParam = new CBCParam();
            cbcParam.setIv(iv);
            return this.session.decrypt(new Mechanism(Mechanism.DES3_CBC, cbcParam), key, en_data);
        } else if (algName.equals(PKCSObjectIdentifiers.pbeWithSHAAnd2DESCBC.getId())) {
            ParametersWithIV ivParam2 = p12gen.generateDerivedParameters(128, 64);
            byte[] iv2 = ivParam2.getIV();
            JKey key2 = new JKey("DESede", ivParam2.getParameters().getKey());
            CBCParam cbcParam2 = new CBCParam();
            cbcParam2.setIv(iv2);
            return this.session.decrypt(new Mechanism(Mechanism.DES3_CBC, cbcParam2), key2, en_data);
        } else if (algName.equals(PKCSObjectIdentifiers.pbeWithSHAAnd128RC2CBC.getId())) {
            return rc2doCipher(false, p12gen.generateDerivedParameters(128, 64), en_data);
        } else {
            if (algName.equals(PKCSObjectIdentifiers.pbeWithSHAAnd40RC2CBC.getId())) {
                return rc2doCipher(false, p12gen.generateDerivedParameters(40, 64), en_data);
            }
            throw new Exception("not support pkcs12pbe algorithm: " + algName);
        }
    }

    public Pfx generatePfx(JKey jprvKey, X509Cert x509cert, char[] _password) throws PKIException {
        X509CertificateStructure cert = x509cert.getCertStructure();
        this.password = PKCS12ParametersGenerator.PKCS12PasswordToBytes(_password);
        try {
            EncryptedPrivateKeyInfo epki = generateEPKI(jprvKey);
            DEROctetString dEROctetString = new DEROctetString(Parser.writeDERObj2Bytes(cert.getSerialNumber()));
            DEREncodableVector derV = new DEREncodableVector();
            derV.add(dEROctetString);
            Attribute attribute = new Attribute(PKCSObjectIdentifiers.pkcs_9_at_localKeyId, new DERSet(derV));
            DEREncodableVector derV2 = new DEREncodableVector();
            derV2.add(attribute);
            DERSet derSet = new DERSet(derV2);
            DEROctetString dEROctetString2 = new DEROctetString(Parser.writeDERObj2Bytes(new SafeContents(new SafeBag[]{new SafeBag(PKCSObjectIdentifiers.pkcs8ShroudedKeyBag, Parser.writeBytes2DERObj(epki.getEncoded()), derSet)}).getDERObject()));
            CertBag certBag = new CertBag(PKCSObjectIdentifiers.x509certType, new DEROctetString(Parser.writeDERObj2Bytes(cert.getDERObject())));
            EncryptedData encryptedData = encryptedCertContents(new SafeContents(new SafeBag[]{new SafeBag(PKCSObjectIdentifiers.certBag, certBag.getDERObject(), derSet)}));
            ContentInfo authSafe = new ContentInfo(PKCSObjectIdentifiers.data, new DEROctetString(Parser.writeDERObj2Bytes(new AuthenticatedSafe(new ContentInfo[]{new ContentInfo(PKCSObjectIdentifiers.data, dEROctetString2), new ContentInfo(PKCSObjectIdentifiers.encryptedData, encryptedData.getDERObject())}).getDERObject())));
            return new Pfx(authSafe, generateMacData(authSafe));
        } catch (Exception ex) {
            throw new PKIException("8170", PKIException.P12_GENERATE_ERR_DES, ex);
        }
    }

    public Pfx generatePfx(JKey jprvKey, X509Cert[] x509certs, char[] _password) throws PKIException {
        SafeBag sbag;
        X509CertificateStructure cert = x509certs[0].getCertStructure();
        this.password = PKCS12ParametersGenerator.PKCS12PasswordToBytes(_password);
        try {
            EncryptedPrivateKeyInfo epki = generateEPKI(jprvKey);
            DEROctetString dEROctetString = new DEROctetString(Parser.writeDERObj2Bytes(cert.getSerialNumber()));
            DEREncodableVector derV = new DEREncodableVector();
            derV.add(dEROctetString);
            Attribute attribute = new Attribute(PKCSObjectIdentifiers.pkcs_9_at_localKeyId, new DERSet(derV));
            DEREncodableVector derV2 = new DEREncodableVector();
            derV2.add(attribute);
            DERSet derSet = new DERSet(derV2);
            ContentInfo[] contentInfos = new ContentInfo[2];
            contentInfos[0] = new ContentInfo(PKCSObjectIdentifiers.data, new DEROctetString(Parser.writeDERObj2Bytes(new SafeContents(new SafeBag[]{new SafeBag(PKCSObjectIdentifiers.pkcs8ShroudedKeyBag, Parser.writeBytes2DERObj(epki.getEncoded()), derSet)}).getDERObject())));
            SafeBag[] certBags2 = new SafeBag[x509certs.length];
            for (int i = 0; i < x509certs.length; i++) {
                CertBag certBag = new CertBag(PKCSObjectIdentifiers.x509certType, new DEROctetString(Parser.writeDERObj2Bytes(x509certs[i].getCertStructure().getDERObject())));
                if (i == 0) {
                    sbag = new SafeBag(PKCSObjectIdentifiers.certBag, certBag.getDERObject(), derSet);
                } else {
                    sbag = new SafeBag(PKCSObjectIdentifiers.certBag, certBag.getDERObject());
                }
                certBags2[i] = sbag;
            }
            EncryptedData encryptedData = encryptedCertContents(new SafeContents(certBags2));
            contentInfos[1] = new ContentInfo(PKCSObjectIdentifiers.encryptedData, encryptedData.getDERObject());
            ContentInfo authSafe = new ContentInfo(PKCSObjectIdentifiers.data, new DEROctetString(Parser.writeDERObj2Bytes(new AuthenticatedSafe(contentInfos).getDERObject())));
            return new Pfx(authSafe, generateMacData(authSafe));
        } catch (Exception ex) {
            throw new PKIException("8170", PKIException.P12_GENERATE_ERR_DES, ex);
        }
    }

    public void generatePfxFile(JKey jprvKey, X509Cert cert, char[] _password, String fileName) throws PKIException {
        Pfx pfxObj = generatePfx(jprvKey, cert, _password);
        try {
            FileOutputStream fos = new FileOutputStream(fileName);
            DEROutputStream dos = new DEROutputStream(fos);
            dos.writeObject(pfxObj);
            dos.close();
            fos.close();
        } catch (Exception ex) {
            throw new PKIException("8170", PKIException.P12_GENERATE_ERR_DES, ex);
        }
    }

    public void generatePfxFile(JKey jprvKey, X509Cert[] certs, char[] _password, String fileName) throws PKIException {
        Pfx pfxObj = generatePfx(jprvKey, certs, _password);
        try {
            FileOutputStream fos = new FileOutputStream(fileName);
            DEROutputStream dos = new DEROutputStream(fos);
            dos.writeObject(pfxObj);
            dos.close();
            fos.close();
        } catch (Exception ex) {
            throw new PKIException("8170", PKIException.P12_GENERATE_ERR_DES, ex);
        }
    }

    public byte[] generatePfxData(JKey jprvKey, X509Cert cert, char[] _password) throws PKIException {
        return Parser.writeDERObj2Bytes(generatePfx(jprvKey, cert, _password).getDERObject());
    }

    public byte[] generatePfxData(JKey jprvKey, X509Cert[] certs, char[] _password) throws PKIException {
        return Parser.writeDERObj2Bytes(generatePfx(jprvKey, certs, _password).getDERObject());
    }

    private EncryptedData encryptedCertContents(DEREncodable safeContents) throws Exception {
        byte[] salt = new byte[8];
        new SecureRandom().nextBytes(salt);
        PKCS12ParametersGenerator p12gen = new PKCS12ParametersGenerator(new SHA1Digest());
        p12gen.init(this.password, salt, (int) ITERATIONS);
        DEROctetString octString = new DEROctetString(rc2doCipher(true, p12gen.generateDerivedParameters(40, 64), Parser.writeDERObj2Bytes(safeContents.getDERObject())));
        DEREncodableVector vector = new DEREncodableVector();
        DEROctetString de0 = new DEROctetString(salt);
        DERInteger deI = new DERInteger((int) ITERATIONS);
        vector.add(de0);
        vector.add(deI);
        return new EncryptedData(new DERInteger(0), new EncryptedContentInfo(PKCSObjectIdentifiers.data, new AlgorithmIdentifier(PKCSObjectIdentifiers.pbeWithSHAAnd40RC2CBC, new DERSequence(vector)), octString));
    }

    private MacData generateMacData(ContentInfo authSafe) throws Exception {
        byte[] salt = new byte[8];
        new SecureRandom().nextBytes(salt);
        PKCS12ParametersGenerator p12gen = new PKCS12ParametersGenerator(new SHA1Digest());
        p12gen.init(this.password, salt, (int) ITERATIONS);
        CipherParameters param = p12gen.generateDerivedMacParameters(160);
        byte[] da = ASN1OctetString.getInstance(authSafe.getContent()).getOctets();
        HMac mac = new HMac(new SHA1Digest());
        mac.init(param);
        mac.update(da, 0, da.length);
        byte[] hmac = new byte[mac.getMacSize()];
        mac.doFinal(hmac, 0);
        return new MacData(new DigestInfo(new AlgorithmIdentifier(new DERObjectIdentifier("1.3.14.3.2.26")), hmac), salt, ITERATIONS);
    }

    private EncryptedPrivateKeyInfo generateEPKI(JKey prvKey) throws Exception {
        byte[] keyData = prvKey.getKey();
        byte[] salt = new byte[8];
        new SecureRandom().nextBytes(salt);
        PKCS12ParametersGenerator pKCS12ParametersGenerator = new PKCS12ParametersGenerator(new SHA1Digest());
        pKCS12ParametersGenerator.init(this.password, salt, ITERATIONS);
        ParametersWithIV ivParam = pKCS12ParametersGenerator.generateDerivedParameters(192, 64);
        byte[] iv = ivParam.getIV();
        JKey encryptKey = new JKey("DESede", ivParam.getParameters().getKey());
        CBCParam cbcParam = new CBCParam();
        cbcParam.setIv(iv);
        DEROctetString deS = new DEROctetString(this.session.encrypt(new Mechanism(Mechanism.DES3_CBC, cbcParam), encryptKey, keyData));
        DEREncodableVector vector = new DEREncodableVector();
        DEROctetString de0 = new DEROctetString(salt);
        DERInteger deI = new DERInteger(ITERATIONS);
        vector.add(de0);
        vector.add(deI);
        AlgorithmIdentifier algId = new AlgorithmIdentifier(PKCSObjectIdentifiers.pbeWithSHAAnd3DESCBC, new DERSequence(vector));
        DEREncodableVector vector2 = new DEREncodableVector();
        vector2.add(algId);
        vector2.add(deS);
        return new EncryptedPrivateKeyInfo(Parser.writeDERObj2Bytes(new DERSequence(vector2)));
    }

    private DEREncodable generateRSAPriKeyInfo(CipherParameters param) {
        RSAPrivateCrtKeyParameters privateKey = (RSAPrivateCrtKeyParameters) param;
        return new PrivateKeyInfo(new AlgorithmIdentifier(PKCSObjectIdentifiers.rsaEncryption, null), new RSAPrivateKeyStructure(privateKey.getModulus(), privateKey.getPublicExponent(), privateKey.getExponent(), privateKey.getP(), privateKey.getQ(), privateKey.getDP(), privateKey.getDQ(), privateKey.getQInv()).getDERObject());
    }

    public void reset() {
        this.pfx = null;
        this.certBags = null;
        this.privateKeyInfo = null;
        this.keyContent = null;
        this.certContent = null;
        this.password = null;
        this.decrypted = false;
    }

    public Pfx getPfx() {
        return this.pfx;
    }

    private ASN1Sequence oct2Seq(ASN1OctetString oct) throws Exception {
        return (ASN1Sequence) new ASN1InputStream(new ByteArrayInputStream(oct.getOctets())).readObject();
    }

    private byte[] rc2doCipher(boolean isEncrypt, CipherParameters param, byte[] data) throws Exception {
        BufferedBlockCipher cipher = new PaddedBlockCipher(new CBCBlockCipher(new RC2Engine()));
        cipher.init(isEncrypt, param);
        byte[] out = new byte[cipher.getOutputSize(data.length)];
        int res = cipher.processBytes(data, 0, data.length, out, 0);
        int validLen = -1;
        if (res < out.length) {
            validLen = cipher.doFinal(out, res);
        }
        if (isEncrypt) {
            return out;
        }
        byte[] d = new byte[((out.length - cipher.getBlockSize()) + validLen)];
        System.arraycopy(out, 0, d, 0, d.length);
        return d;
    }

    public static void main(String[] args) {
        try {
            JCrypto.getInstance().initialize(JCrypto.JSOFT_LIB, null);
            PKCS12 p12 = new PKCS12();
            p12.load(new FileInputStream("d:/user.pfx"));
            p12.decrypt(StringEx.Empty.toCharArray());
            JKey prvKey = p12.getPrivateKey();
            X509Cert[] certs = p12.getCerts();
            System.out.println(certs.length);
            p12.generatePfxFile(prvKey, certs, "2222".toCharArray(), "c:/complex.pfx");
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
    }
}
