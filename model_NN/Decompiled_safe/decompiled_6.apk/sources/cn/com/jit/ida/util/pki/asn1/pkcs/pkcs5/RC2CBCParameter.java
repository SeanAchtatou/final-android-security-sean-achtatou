package cn.com.jit.ida.util.pki.asn1.pkcs.pkcs5;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1OctetString;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import java.math.BigInteger;

public class RC2CBCParameter implements DEREncodable {
    ASN1OctetString iv;
    DERInteger version;

    public static RC2CBCParameter getInstance(Object o) {
        if (o instanceof ASN1Sequence) {
            return new RC2CBCParameter((ASN1Sequence) o);
        }
        throw new IllegalArgumentException("unknown object in RC2CBCParameter factory");
    }

    public RC2CBCParameter(byte[] iv2) {
        this.version = null;
        this.iv = new DEROctetString(iv2);
    }

    public RC2CBCParameter(int parameterVersion, byte[] iv2) {
        this.version = new DERInteger(parameterVersion);
        this.iv = new DEROctetString(iv2);
    }

    public RC2CBCParameter(ASN1Sequence seq) {
        if (seq.size() == 1) {
            this.version = null;
            this.iv = (ASN1OctetString) seq.getObjectAt(0);
            return;
        }
        this.version = (DERInteger) seq.getObjectAt(0);
        this.iv = (ASN1OctetString) seq.getObjectAt(1);
    }

    public BigInteger getRC2ParameterVersion() {
        if (this.version == null) {
            return null;
        }
        return this.version.getValue();
    }

    public byte[] getIV() {
        return this.iv.getOctets();
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        if (this.version != null) {
            v.add(this.version);
        }
        v.add(this.iv);
        return new DERSequence(v);
    }
}
