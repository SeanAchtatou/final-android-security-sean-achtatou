package cn.com.jit.ida.util.pki.asn1;

import java.io.IOException;

public class DERNull extends ASN1Null {
    byte[] zeroBytes = new byte[0];

    /* access modifiers changed from: package-private */
    public void encode(DEROutputStream out) throws IOException {
        out.writeEncoded(5, this.zeroBytes);
    }

    public boolean equals(Object o) {
        if (o == null || !(o instanceof DERNull)) {
            return false;
        }
        return true;
    }
}
