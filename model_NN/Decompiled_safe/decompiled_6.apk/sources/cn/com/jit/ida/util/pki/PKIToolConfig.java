package cn.com.jit.ida.util.pki;

import cn.com.jit.ida.util.ini.ProFile;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.x509.X509Name;
import com.jianq.misc.StringEx;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class PKIToolConfig {
    private static HashMap dnrules = null;
    private static boolean reverseDN = true;
    private static String teletexstr = "default";
    private static boolean useUTF8String = true;
    private final String CERT_ZH_ENCODE = "X509CertZHEncode";
    private String DN_RULES = "DNRULES";
    private final String JNIUSE = "JNIUse";
    private final String MASTERKEY_ENC = "MasterKeyEnc";
    private final String MASTERKEY_TYPE = "MasterKeyType";
    private final String MASTERKEY_VALUE = "MasterKeyValue";
    private final String NOEXPORT_DSA_KEYTYPE = "NoExportDSAKeyType";
    private final String NOEXPORT_DSA_PRVKEYVALUE = "NoExportDSAPriKeyValue";
    private final String NOEXPORT_DSA_PUBKEYVALUE = "NoExportDSAPubKeyValue";
    private final String NOEXPORT_ECDSA_ENCKEYTYPE = "NoExportECDSAEncKeyType";
    private final String NOEXPORT_ECDSA_ENCPRVKEYVALUE = "NoExportECDSAEncPriKeyValue";
    private final String NOEXPORT_ECDSA_ENCPUBKEYVALUE = "NoExportECDSAEncPubKeyValue";
    private final String NOEXPORT_ECDSA_KEYTYPE = "NoExportECDSAKeyType";
    private final String NOEXPORT_ECDSA_PRVKEYVALUE = "NoExportECDSAPriKeyValue";
    private final String NOEXPORT_ECDSA_PUBKEYVALUE = "NoExportECDSAPubKeyValue";
    private final String NOEXPORT_RSA_KEYCOUNT = "NoExportRSAKeyCount";
    private final String NOEXPORT_RSA_KEYTYPE = "NoExportRSAKeyType";
    private final String NOEXPORT_RSA_KEYVALUE = "NoExportRSAKeyValue";
    private final String NOEXPORT_RSA_PRVKEYVALUE = "NoExportRSAPriKeyValue";
    private final String NOEXPORT_RSA_PUBKEYVALUE = "NoExportRSAPubKeyValue";
    private String NoExportDSAKeyType = null;
    private final String P11_CFG_DESCRIPTION = "description";
    private final String P11_CFG_LIBRARY = "library";
    private final String P11_CFG_NAME = "name";
    private final String P11_CFG_SLOTLISTINDEX = "slotListIndex";
    private final String P11_CONFIG = "P11Config";
    private final String P11_FILE = "P11File";
    private final String P11_PASSWORD = "P11Password";
    private String P11_SLOT = "P11Slot";
    private String PKITOOL = "PKITOOL";
    private final String REVERSE_DN = "ReverseDN";
    private String SEC_PUBLIC = "PUBLIC";
    private String SSF33ECB = null;
    private String SSF33Key = null;
    private String SSF33KeyGenerate = null;
    private final String SSF33_ECB = "SSF33ECB";
    private final String SSF33_KEY = "SSF33Key";
    private final String SSF33_KEYGEN = "SSF33KeyGenerate";
    private final String SUPPORT_KEY_SIZE = "SupportKeySize";
    private final String TELETEX_STRING = "TeletexString";
    private final String VERIFY_P10 = "VerifyP10Signature";
    private boolean exists1 = true;
    private boolean exists2 = true;
    private String jniUse = "JNI1";
    private String masterKeyEnc = null;
    private String masterKeyType = null;
    private String masterKeyValue = null;
    private String noExportDSAPriKeyValue = null;
    private String noExportDSAPubKeyValue = null;
    private String noExportECDSAEncKeyType = null;
    private String noExportECDSAEncPriKeyValue = null;
    private String noExportECDSAEncPubKeyValue = null;
    private String noExportECDSAKeyType = null;
    private String noExportECDSAPriKeyValue = null;
    private String noExportECDSAPubKeyValue = null;
    private String noExportRSAKeyCount = null;
    private String noExportRSAKeyType = null;
    private String noExportRSAKeyValue = null;
    private String noExportRSAPriKeyValue = null;
    private String noExportRSAPubKeyValue = null;
    private String p11Config = null;
    private String p11Description = null;
    private String p11File = null;
    private String p11Library = StringEx.Empty;
    private String p11Password = null;
    private String p11ProviderName = null;
    private String p11SlotListIndex = null;
    private String p11_slot = null;
    private String supportKeySize = null;
    private boolean verifyP10Signature = false;

    public void LoadOpt(String tag) throws PKIException {
        if (tag != null) {
            this.PKITOOL = tag;
        }
        String path1 = System.getProperty("PKIToolConfig");
        boolean usePath1 = true;
        if (path1 == null) {
            usePath1 = false;
            if (!new File("./pkitool.ini").exists()) {
                this.exists1 = false;
                System.out.println(System.getProperty("user.dir"));
                System.out.println("Couldn't find config file named 'pkitool.ini'. All the pkitool parameters will be set to default value.");
            }
        } else if (!new File(path1).exists()) {
            this.exists1 = false;
            System.out.println("Couldn't find config file named 'pkitool.ini'. All the pkitool parameters will be set to default value.");
        }
        if (this.exists1) {
            ProFile proFile = new ProFile();
            if (usePath1) {
                proFile.load(path1);
            } else {
                proFile.load("./pkitool.ini");
            }
            if ("true".equalsIgnoreCase(proFile.getValue(this.PKITOOL, "VerifyP10Signature"))) {
                this.verifyP10Signature = true;
            }
            String strReverseDN = proFile.getValue(this.PKITOOL, "ReverseDN");
            if (strReverseDN != null && "false".equalsIgnoreCase(strReverseDN)) {
                reverseDN = false;
            }
            String strUseUTF8 = proFile.getValue(this.PKITOOL, "X509CertZHEncode");
            if (strUseUTF8 != null && !"UTF8String".equalsIgnoreCase(strUseUTF8)) {
                useUTF8String = false;
            }
            this.jniUse = proFile.getValue(this.PKITOOL, "JNIUse");
            String strTeletex = proFile.getValue(this.PKITOOL, "TeletexString");
            if (strTeletex != null && !StringEx.Empty.equalsIgnoreCase(strTeletex)) {
                teletexstr = strTeletex;
            }
            if (tag.equals(this.PKITOOL)) {
                String strReverseDN2 = proFile.getValue(this.SEC_PUBLIC, "ReverseDN");
                if (strReverseDN2 != null && "false".equalsIgnoreCase(strReverseDN2)) {
                    reverseDN = false;
                }
                String strUseUTF82 = proFile.getValue(this.SEC_PUBLIC, "X509CertZHEncode");
                if (strUseUTF82 != null && !"UTF8String".equalsIgnoreCase(strUseUTF82)) {
                    useUTF8String = false;
                }
                String strTeletex2 = proFile.getValue(this.SEC_PUBLIC, "TeletexString");
                if (strTeletex2 != null && !StringEx.Empty.equalsIgnoreCase(strTeletex2)) {
                    teletexstr = strTeletex2;
                }
            }
            this.p11_slot = proFile.getValue(this.jniUse, this.P11_SLOT);
            String[] strRules = proFile.getKeys(this.DN_RULES);
            if (strRules == null) {
                dnrules = null;
            } else {
                dnrules = new HashMap();
                for (int i = 0; i < strRules.length; i++) {
                    String value = proFile.getValue(this.DN_RULES, strRules[i]);
                    dnrules.put((DERObjectIdentifier) X509Name.DefaultLookUp.get(strRules[i].toLowerCase()), value);
                }
            }
            this.p11File = proFile.getValue(this.jniUse, "P11File");
            if (this.p11File != null) {
                this.p11File.replaceAll("\\\\\\\\", "/");
            }
            this.p11Config = proFile.getValue(this.jniUse, "P11Config");
            this.p11Password = proFile.getValue(this.jniUse, "P11Password");
            this.supportKeySize = proFile.getValue(this.jniUse, "SupportKeySize");
            this.masterKeyEnc = proFile.getValue(this.jniUse, "MasterKeyEnc");
            this.masterKeyType = proFile.getValue(this.jniUse, "MasterKeyType");
            this.masterKeyValue = proFile.getValue(this.jniUse, "MasterKeyValue");
            this.SSF33Key = proFile.getValue(this.jniUse, "SSF33Key");
            this.SSF33KeyGenerate = proFile.getValue(this.jniUse, "SSF33KeyGenerate");
            this.SSF33ECB = proFile.getValue(this.jniUse, "SSF33ECB");
            this.noExportRSAKeyCount = proFile.getValue(this.jniUse, "NoExportRSAKeyCount");
            this.noExportRSAKeyType = proFile.getValue(this.jniUse, "NoExportRSAKeyType");
            this.noExportRSAKeyValue = proFile.getValue(this.jniUse, "NoExportRSAKeyValue");
            this.noExportRSAPubKeyValue = proFile.getValue(this.jniUse, "NoExportRSAPubKeyValue");
            this.noExportRSAPriKeyValue = proFile.getValue(this.jniUse, "NoExportRSAPriKeyValue");
            this.noExportECDSAKeyType = proFile.getValue(this.jniUse, "NoExportECDSAKeyType");
            this.noExportECDSAPubKeyValue = proFile.getValue(this.jniUse, "NoExportECDSAPubKeyValue");
            this.noExportECDSAPriKeyValue = proFile.getValue(this.jniUse, "NoExportECDSAPriKeyValue");
            this.NoExportDSAKeyType = proFile.getValue(this.jniUse, "NoExportDSAKeyType");
            this.noExportDSAPubKeyValue = proFile.getValue(this.jniUse, "NoExportDSAPubKeyValue");
            this.noExportDSAPriKeyValue = proFile.getValue(this.jniUse, "NoExportDSAPriKeyValue");
            this.noExportECDSAEncKeyType = proFile.getValue(this.jniUse, "NoExportECDSAEncKeyType");
            this.noExportECDSAEncPubKeyValue = proFile.getValue(this.jniUse, "NoExportECDSAEncPubKeyValue");
            this.noExportECDSAEncPriKeyValue = proFile.getValue(this.jniUse, "NoExportECDSAEncPriKeyValue");
            if (this.p11Config == null) {
                this.exists2 = false;
            } else if (!new File(this.p11Config).exists()) {
                this.exists2 = false;
            }
            if (this.exists2) {
                Properties props = new Properties();
                try {
                    props.load(new FileInputStream(this.p11Config));
                    this.p11ProviderName = props.getProperty("name");
                    this.p11Library = props.getProperty("library");
                    this.p11Description = props.getProperty("description");
                    this.p11SlotListIndex = props.getProperty("slotListIndex");
                    if (this.p11Library != null) {
                        this.p11Library.replaceAll("\\\\\\\\\\\\", "/");
                    }
                } catch (IOException e) {
                }
            }
        }
    }

    public boolean isReLoaded() throws PKIException {
        if (!this.exists1) {
            throw new PKIException("8599", "couldn't find 'pkitool.ini' file");
        } else if (this.exists2) {
            return this.p11Library.equals(this.p11File);
        } else {
            throw new PKIException("8599", "couldn't find '" + this.p11Config + "' file");
        }
    }

    public static boolean isReverseDN() {
        return reverseDN;
    }

    public static boolean isUseUTF8String() {
        return useUTF8String;
    }

    public boolean isVerifyP10Signature() {
        return this.verifyP10Signature;
    }

    public String getJniUse() {
        return this.jniUse;
    }

    public String getMasterKeyEnc() throws PKIException {
        if (this.exists1) {
            return this.masterKeyEnc;
        }
        throw new PKIException("8599", "couldn't find 'pkitool.ini' file");
    }

    public int getNoExportRSAKeyCount() throws PKIException {
        if (!this.exists1) {
            throw new PKIException("8599", "couldn't find 'pkitool.ini' file");
        }
        try {
            return Integer.parseInt(this.noExportRSAKeyCount);
        } catch (Exception e) {
            return -1;
        }
    }

    public String getNoExportRSAKeyType() throws PKIException {
        if (this.exists1) {
            return this.noExportRSAKeyType;
        }
        throw new PKIException("8599", "couldn't find 'pkitool.ini' file");
    }

    public String getNoExportECCKeyType() throws PKIException {
        if (this.exists1) {
            return this.noExportECDSAKeyType;
        }
        throw new PKIException("8599", "couldn't find 'pkitool.ini' file");
    }

    public String getNoExportECDSAPubKeyValue() throws PKIException {
        if (this.exists1) {
            return this.noExportECDSAPubKeyValue;
        }
        throw new PKIException("8599", "couldn't find 'pkitool.ini' file");
    }

    public String getNoExportECDSAPriKeyValue() throws PKIException {
        if (this.exists1) {
            return this.noExportECDSAPriKeyValue;
        }
        throw new PKIException("8599", "couldn't find 'pkitool.ini' file");
    }

    public String getNoExportDSAKeyType() throws PKIException {
        if (this.exists1) {
            return this.NoExportDSAKeyType;
        }
        throw new PKIException("8599", "couldn't find 'pkitool.ini' file");
    }

    public String getNoExportDSAPubKeyValue() throws PKIException {
        if (this.exists1) {
            return this.noExportDSAPubKeyValue;
        }
        throw new PKIException("8599", "couldn't find 'pkitool.ini' file");
    }

    public String getNoExportDSAPriKeyValue() throws PKIException {
        if (this.exists1) {
            return this.noExportDSAPriKeyValue;
        }
        throw new PKIException("8599", "couldn't find 'pkitool.ini' file");
    }

    public String getNoExportRSAKeyValue() throws PKIException {
        if (this.exists1) {
            return this.noExportRSAKeyValue;
        }
        throw new PKIException("8599", "couldn't find 'pkitool.ini' file");
    }

    public String getNoExportRSAPriKeyValue() throws PKIException {
        if (this.exists1) {
            return this.noExportRSAPriKeyValue;
        }
        throw new PKIException("8599", "couldn't find 'pkitool.ini' file");
    }

    public String getNoExportRSAPubKeyValue() throws PKIException {
        if (this.exists1) {
            return this.noExportRSAPubKeyValue;
        }
        throw new PKIException("8599", "couldn't find 'pkitool.ini' file");
    }

    public String getP11Config() throws PKIException {
        if (this.exists1) {
            return this.p11Config;
        }
        throw new PKIException("8599", "couldn't find 'pkitool.ini' file");
    }

    public String getP11File() throws PKIException {
        if (this.exists1) {
            return this.p11File;
        }
        throw new PKIException("8599", "couldn't find 'pkitool.ini' file");
    }

    public String getP11Password() throws PKIException {
        if (this.exists1) {
            return this.p11Password;
        }
        throw new PKIException("8599", "couldn't find 'pkitool.ini' file");
    }

    public String getSupportKeySize() throws PKIException {
        if (this.exists1) {
            return this.supportKeySize;
        }
        throw new PKIException("8599", "couldn't find 'pkitool.ini' file");
    }

    public String getSSF33ECB() throws PKIException {
        if (this.exists1) {
            return this.SSF33ECB;
        }
        throw new PKIException("8599", "couldn't find 'pkitool.ini' file");
    }

    public String getSSF33Key() throws PKIException {
        if (this.exists1) {
            return this.SSF33Key;
        }
        throw new PKIException("8599", "couldn't find 'pkitool.ini' file");
    }

    public String getSSF33KeyGenerate() throws PKIException {
        if (this.exists1) {
            return this.SSF33KeyGenerate;
        }
        throw new PKIException("8599", "couldn't find 'pkitool.ini' file");
    }

    public String getP11ProviderName() throws PKIException {
        if (this.exists2) {
            return "SunPKCS11-" + this.p11ProviderName;
        }
        throw new PKIException("8599", "couldn't find '" + this.p11Config + "' file");
    }

    public String getNoExportRSAKey(int keyID) throws PKIException {
        if (this.exists1) {
            return this.noExportRSAKeyValue + keyID;
        }
        throw new PKIException("8599", "couldn't find 'pkitool.ini' file");
    }

    public String getMasterKeyType() throws PKIException {
        if (this.exists1) {
            return this.masterKeyType;
        }
        throw new PKIException("8599", "couldn't find 'pkitool.ini' file");
    }

    public String getMasterKeyValue() throws PKIException {
        if (this.exists1) {
            return this.masterKeyValue;
        }
        throw new PKIException("8599", "couldn't find 'pkitool.ini' file");
    }

    public String getP11Description() throws PKIException {
        if (this.exists2) {
            return this.p11Description;
        }
        throw new PKIException("8599", "couldn't find '" + this.p11Config + "' file");
    }

    public String getP11Library() throws PKIException {
        if (this.exists2) {
            return this.p11Library;
        }
        throw new PKIException("8599", "couldn't find '" + this.p11Config + "' file");
    }

    public String getP11SlotListIndex() throws PKIException {
        if (this.exists2) {
            return this.p11SlotListIndex;
        }
        throw new PKIException("8599", "couldn't find '" + this.p11Config + "' file");
    }

    public static String getTeletexstr() {
        return teletexstr;
    }

    public String getNoExportECCEncKeyType() throws PKIException {
        if (this.exists1) {
            return this.noExportECDSAEncKeyType;
        }
        throw new PKIException("8599", "couldn't find 'pkitool.ini' file");
    }

    public String getNoExportECDSAEncPubKeyValue() throws PKIException {
        if (this.exists1) {
            return this.noExportECDSAEncPubKeyValue;
        }
        throw new PKIException("8599", "couldn't find 'pkitool.ini' file");
    }

    public String getNoExportECDSAEncPriKeyValue() throws PKIException {
        if (this.exists1) {
            return this.noExportECDSAEncPriKeyValue;
        }
        throw new PKIException("8599", "couldn't find 'pkitool.ini' file");
    }

    public String getSlot() throws PKIException {
        if (this.exists1) {
            return this.p11_slot;
        }
        throw new PKIException("8599", "couldn't find 'pkitool.ini' file");
    }

    public static Map getDnRules() {
        return dnrules;
    }
}
