package cn.com.jit.ida.util.pki.asn1.pkcs.pkcs5;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.pkcs.PKCSObjectIdentifiers;
import java.util.Enumeration;

public class PBES2Parameters implements PKCSObjectIdentifiers, DEREncodable {
    private KeyDerivationFunc func;
    private EncryptionScheme scheme;

    public PBES2Parameters(ASN1Sequence obj) {
        Enumeration e = obj.getObjects();
        ASN1Sequence funcSeq = (ASN1Sequence) e.nextElement();
        if (funcSeq.getObjectAt(0).equals(id_PBKDF2)) {
            this.func = new PBKDF2Params(funcSeq);
        } else {
            this.func = new KeyDerivationFunc(funcSeq);
        }
        this.scheme = new EncryptionScheme((ASN1Sequence) e.nextElement());
    }

    public KeyDerivationFunc getKeyDerivationFunc() {
        return this.func;
    }

    public EncryptionScheme getEncryptionScheme() {
        return this.scheme;
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(this.func);
        v.add(this.scheme);
        return new DERSequence(v);
    }
}
