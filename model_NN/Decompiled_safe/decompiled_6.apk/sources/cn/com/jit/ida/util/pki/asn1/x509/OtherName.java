package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.DERTaggedObject;

public class OtherName implements DEREncodable {
    private DERObjectIdentifier typeID = null;
    private DEREncodable value = null;

    public OtherName(ASN1Sequence seq) {
        this.typeID = DERObjectIdentifier.getInstance(seq.getObjectAt(0));
        this.value = ((DERTaggedObject) seq.getObjectAt(1)).getObject();
    }

    public OtherName(DERObjectIdentifier _typeID, DEREncodable _value) {
        this.typeID = _typeID;
        this.value = _value;
    }

    public static OtherName getInstance(ASN1TaggedObject o, boolean explicit) {
        return new OtherName(ASN1Sequence.getInstance(o, explicit));
    }

    public static OtherName getInstance(Object obj) {
        if (obj == null || (obj instanceof OtherName)) {
            return (OtherName) obj;
        }
        if (obj instanceof ASN1TaggedObject) {
            return getInstance((ASN1TaggedObject) obj, false);
        }
        if (obj instanceof ASN1Sequence) {
            return new OtherName(ASN1Sequence.getInstance(obj));
        }
        throw new IllegalArgumentException("unknown object in factory:" + obj.getClass().getName());
    }

    public DERObjectIdentifier getTypeID() {
        return this.typeID;
    }

    public DEREncodable getValue() {
        return this.value;
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(this.typeID);
        v.add(this.value);
        return new DERSequence(v);
    }
}
