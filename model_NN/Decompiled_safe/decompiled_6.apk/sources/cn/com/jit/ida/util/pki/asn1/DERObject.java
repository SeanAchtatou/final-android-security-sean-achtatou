package cn.com.jit.ida.util.pki.asn1;

import java.io.IOException;

public abstract class DERObject implements DERTags, DEREncodable {
    /* access modifiers changed from: package-private */
    public abstract void encode(DEROutputStream dEROutputStream) throws IOException;

    public DERObject getDERObject() {
        return this;
    }
}
