package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.DERTaggedObject;
import java.util.Enumeration;

public class CertificatePair implements DEREncodable {
    private X509CertificateStructure forward = null;
    private X509CertificateStructure reverse = null;

    public CertificatePair(X509CertificateStructure forward2, X509CertificateStructure reverse2) {
        this.forward = forward2;
        this.reverse = reverse2;
    }

    public CertificatePair(ASN1Sequence seq) {
        Enumeration e = seq.getObjects();
        while (e.hasMoreElements()) {
            ASN1TaggedObject obj = (ASN1TaggedObject) e.nextElement();
            switch (obj.getTagNo()) {
                case 0:
                    this.forward = X509CertificateStructure.getInstance(obj, true);
                    break;
                case 1:
                    this.reverse = X509CertificateStructure.getInstance(obj, true);
                    break;
                default:
                    throw new IllegalArgumentException("unknown tag in CertificatePair");
            }
        }
    }

    public X509CertificateStructure getForward() {
        return this.forward;
    }

    public X509CertificateStructure getReverse() {
        return this.reverse;
    }

    public static CertificatePair getInstance(Object obj) {
        if (obj instanceof CertificatePair) {
            return (CertificatePair) obj;
        }
        if (obj instanceof ASN1Sequence) {
            return new CertificatePair((ASN1Sequence) obj);
        }
        throw new IllegalArgumentException("unknown object in factory:" + obj.getClass().getName());
    }

    public static CertificatePair getInstance(ASN1TaggedObject tagObj, boolean explicit) {
        return getInstance(ASN1Sequence.getInstance(tagObj, explicit));
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        if (this.forward != null) {
            v.add(new DERTaggedObject(true, 0, this.forward));
        }
        if (this.reverse != null) {
            v.add(new DERTaggedObject(true, 1, this.reverse));
        }
        return new DERSequence(v);
    }
}
