package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.DERTaggedObject;

public class PolicyConstraints implements DEREncodable {
    private DERInteger inhibitPolicyMapping;
    private DERInteger requireExplicitPolicy;
    private ASN1Sequence seq;

    public PolicyConstraints(DERInteger requireExplicitPolicy2, DERInteger inhibitPolicyMapping2) {
        this.requireExplicitPolicy = requireExplicitPolicy2;
        this.inhibitPolicyMapping = inhibitPolicyMapping2;
    }

    public PolicyConstraints(ASN1Sequence seq2) {
        this.seq = seq2;
        for (int i = 0; i < seq2.size(); i++) {
            ASN1TaggedObject obj = (ASN1TaggedObject) seq2.getObjectAt(i);
            switch (obj.getTagNo()) {
                case 0:
                    this.requireExplicitPolicy = DERInteger.getInstance(obj, false);
                    break;
                case 1:
                    this.inhibitPolicyMapping = DERInteger.getInstance(obj, false);
                    break;
                default:
                    throw new IllegalArgumentException("illegal tag");
            }
        }
    }

    public static PolicyConstraints getInstance(ASN1TaggedObject obj, boolean explicit) {
        return getInstance(ASN1Sequence.getInstance(obj, explicit));
    }

    public static PolicyConstraints getInstance(Object obj) {
        if (obj instanceof PolicyConstraints) {
            return (PolicyConstraints) obj;
        }
        if (obj instanceof ASN1Sequence) {
            return new PolicyConstraints((ASN1Sequence) obj);
        }
        throw new IllegalArgumentException("unknown object in factory");
    }

    public DERInteger getRequireExplicitPolicy() {
        return this.requireExplicitPolicy;
    }

    public DERInteger getInhibitPolicyMapping() {
        return this.inhibitPolicyMapping;
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        if (this.requireExplicitPolicy != null) {
            v.add(new DERTaggedObject(false, 0, this.requireExplicitPolicy));
        }
        if (this.inhibitPolicyMapping != null) {
            v.add(new DERTaggedObject(false, 1, this.inhibitPolicyMapping));
        }
        return new DERSequence(v);
    }
}
