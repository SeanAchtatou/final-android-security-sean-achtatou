package cn.com.jit.ida.util.pki.asn1;

import cn.com.jit.pnxclient.constant.PNXConfigConstant;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.SimpleTimeZone;

public class DERGeneralizedTime extends DERObject {
    String time;

    public static DERGeneralizedTime getInstance(Object obj) {
        if (obj == null || (obj instanceof DERGeneralizedTime)) {
            return (DERGeneralizedTime) obj;
        }
        if (obj instanceof ASN1OctetString) {
            return new DERGeneralizedTime(((ASN1OctetString) obj).getOctets());
        }
        throw new IllegalArgumentException("illegal object in getInstance: " + obj.getClass().getName());
    }

    public static DERGeneralizedTime getInstance(ASN1TaggedObject obj, boolean explicit) {
        return getInstance(obj.getObject());
    }

    public DERGeneralizedTime(String time2) {
        this.time = time2;
    }

    public DERGeneralizedTime(Date time2) {
        SimpleDateFormat dateF = new SimpleDateFormat("yyyyMMddHHmmss'Z'");
        dateF.setTimeZone(new SimpleTimeZone(0, "Z"));
        this.time = dateF.format(time2);
    }

    DERGeneralizedTime(byte[] bytes) {
        char[] dateC = new char[bytes.length];
        for (int i = 0; i != dateC.length; i++) {
            dateC[i] = (char) (bytes[i] & 255);
        }
        this.time = new String(dateC);
    }

    public String getTime() {
        if (this.time.charAt(this.time.length() - 1) == 'Z') {
            return this.time.substring(0, this.time.length() - 1) + "GMT+00:00";
        }
        int signPos = this.time.length() - 5;
        char sign = this.time.charAt(signPos);
        if (sign == '-' || sign == '+') {
            return this.time.substring(0, signPos) + "GMT" + this.time.substring(signPos, signPos + 3) + PNXConfigConstant.RESP_SPLIT_3 + this.time.substring(signPos + 3);
        }
        int signPos2 = this.time.length() - 3;
        char sign2 = this.time.charAt(signPos2);
        if (sign2 == '-' || sign2 == '+') {
            return this.time.substring(0, signPos2) + "GMT" + this.time.substring(signPos2) + ":00";
        }
        return this.time;
    }

    public Date getDate() throws ParseException {
        SimpleDateFormat dateF = new SimpleDateFormat("yyyyMMddHHmmss'Z'");
        dateF.setTimeZone(new SimpleTimeZone(0, "Z"));
        return dateF.parse(this.time);
    }

    private byte[] getOctets() {
        char[] cs = this.time.toCharArray();
        byte[] bs = new byte[cs.length];
        for (int i = 0; i != cs.length; i++) {
            bs[i] = (byte) cs[i];
        }
        return bs;
    }

    /* access modifiers changed from: package-private */
    public void encode(DEROutputStream out) throws IOException {
        out.writeEncoded(24, getOctets());
    }

    public boolean equals(Object o) {
        if (o == null || !(o instanceof DERGeneralizedTime)) {
            return false;
        }
        return this.time.equals(((DERGeneralizedTime) o).time);
    }
}
