package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.DERBitString;

public class NetscapeCertType extends DERBitString {
    public static final int objectSigning = 16;
    public static final int objectSigningCA = 1;
    public static final int smime = 32;
    public static final int smimeCA = 2;
    public static final int sslCA = 4;
    public static final int sslClient = 128;
    public static final int sslServer = 64;

    public NetscapeCertType(int certType) {
        super(getBytes(certType), getPadBits(certType));
    }

    public NetscapeCertType(DERBitString certType) {
        super(certType.getBytes(), certType.getPadBits());
    }

    public String toString() {
        if (this.data.length == 1) {
            return "NetscapeCertType: 0x" + Integer.toHexString(this.data[0] & 255);
        }
        return "NetscapeCertType: 0x" + Integer.toHexString(((this.data[1] & 255) << 8) | (this.data[0] & 255));
    }
}
