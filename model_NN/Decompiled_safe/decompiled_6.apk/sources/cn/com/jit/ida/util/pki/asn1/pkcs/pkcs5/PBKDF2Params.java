package cn.com.jit.ida.util.pki.asn1.pkcs.pkcs5;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1OctetString;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import java.math.BigInteger;
import java.util.Enumeration;

public class PBKDF2Params extends KeyDerivationFunc {
    DERObjectIdentifier id;
    DERInteger iterationCount;
    DERInteger keyLength;
    ASN1OctetString octStr;

    PBKDF2Params(ASN1Sequence seq) {
        super(seq);
        Enumeration e = seq.getObjects();
        this.id = (DERObjectIdentifier) e.nextElement();
        Enumeration e2 = ((ASN1Sequence) e.nextElement()).getObjects();
        this.octStr = (ASN1OctetString) e2.nextElement();
        this.iterationCount = (DERInteger) e2.nextElement();
        if (e2.hasMoreElements()) {
            this.keyLength = (DERInteger) e2.nextElement();
        } else {
            this.keyLength = null;
        }
    }

    public byte[] getSalt() {
        return this.octStr.getOctets();
    }

    public BigInteger getIterationCount() {
        return this.iterationCount.getValue();
    }

    public BigInteger getKeyLength() {
        if (this.keyLength != null) {
            return this.keyLength.getValue();
        }
        return null;
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        ASN1EncodableVector subV = new ASN1EncodableVector();
        v.add(this.id);
        subV.add(this.octStr);
        subV.add(this.iterationCount);
        if (this.keyLength != null) {
            subV.add(this.keyLength);
        }
        v.add(new DERSequence(subV));
        return new DERSequence(v);
    }
}
