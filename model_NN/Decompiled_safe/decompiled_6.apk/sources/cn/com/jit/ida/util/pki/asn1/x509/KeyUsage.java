package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.DERBitString;

public class KeyUsage extends DERBitString {
    public static final int cRLSign = 2;
    public static final int dataEncipherment = 16;
    public static final int decipherOnly = 32768;
    public static final int digitalSignature = 128;
    public static final int encipherOnly = 1;
    public static final int keyAgreement = 8;
    public static final int keyCertSign = 4;
    public static final int keyEncipherment = 32;
    public static final int nonRepudiation = 64;

    public KeyUsage(int usage) {
        super(getBytes(usage), getPadBits(usage));
    }

    public KeyUsage(DERBitString usage) {
        super(usage.getBytes(), usage.getPadBits());
    }

    public String toString() {
        if (this.data.length == 1) {
            return "KeyUsage: 0x" + Integer.toHexString(this.data[0] & 255);
        }
        return "KeyUsage: 0x" + Integer.toHexString(((this.data[1] & 255) << 8) | (this.data[0] & 255));
    }

    public boolean[] getKeyUsage() {
        int i = 9;
        byte[] bKeyUsage = getBytes();
        int length = (bKeyUsage.length * 8) - getPadBits();
        if (length >= 9) {
            i = length;
        }
        boolean[] keyUsage = new boolean[i];
        for (int i2 = 0; i2 != length; i2++) {
            keyUsage[i2] = (bKeyUsage[i2 / 8] & (128 >>> (i2 % 8))) != 0;
        }
        return keyUsage;
    }
}
