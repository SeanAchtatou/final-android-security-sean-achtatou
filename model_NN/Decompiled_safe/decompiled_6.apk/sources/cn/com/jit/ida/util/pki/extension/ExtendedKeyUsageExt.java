package cn.com.jit.ida.util.pki.extension;

import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.x509.ExtendedKeyUsage;
import cn.com.jit.ida.util.pki.asn1.x509.KeyPurposeId;
import cn.com.jit.ida.util.pki.asn1.x509.X509Extensions;
import java.util.Vector;

public class ExtendedKeyUsageExt extends AbstractStandardExtension {
    public static final String ANY_EXTENED_KEYUSAGE = KeyPurposeId.anyExtendedKeyUsage.getId();
    public static final String CLIENT_AUTH = KeyPurposeId.id_kp_clientAuth.getId();
    public static final String CODE_SIGNING = KeyPurposeId.id_kp_codeSigning.getId();
    public static final String EMAIL_PROTECTION = KeyPurposeId.id_kp_emailProtection.getId();
    public static final String ENCRYPTED_FILE_SYSTEM = KeyPurposeId.id_kp_encryptedFileSystem.getId();
    public static final String IPSEC_END_SYSTEM = KeyPurposeId.id_kp_ipsecEndSystem.getId();
    public static final String IPSEC_TUNNEL = KeyPurposeId.id_kp_ipsecTunnel.getId();
    public static final String IPSEC_USER = KeyPurposeId.id_kp_ipsecUser.getId();
    public static final String OCSP_SIGNING = KeyPurposeId.id_kp_OCSPSigning.getId();
    public static final String SERVER_AUTH = KeyPurposeId.id_kp_serverAuth.getId();
    public static final String SMART_CARD_LOGON = KeyPurposeId.id_kp_smartcardlogon.getId();
    public static final String TIME_STAMPING = KeyPurposeId.id_kp_timeStamping.getId();
    private Vector extKeyUsageVector;

    public ExtendedKeyUsageExt() {
        this.extKeyUsageVector = null;
        this.critical = false;
        this.OID = X509Extensions.ExtendedKeyUsage.getId();
        this.extKeyUsageVector = new Vector();
    }

    public ExtendedKeyUsageExt(ASN1Sequence asn1Sequence) {
        this.extKeyUsageVector = null;
        this.extKeyUsageVector = new Vector();
        for (int i = 0; i < asn1Sequence.size(); i++) {
            this.extKeyUsageVector.add(((DERObjectIdentifier) asn1Sequence.getObjectAt(i)).getId());
        }
    }

    public int getExtendedKeyUsageCount() {
        return this.extKeyUsageVector.size();
    }

    public String getExtendedKeyUsage(int index) {
        return (String) this.extKeyUsageVector.get(index);
    }

    public String getOID() {
        return this.OID;
    }

    public void setCritical(boolean critical) {
        this.critical = critical;
    }

    public boolean getCritical() {
        return this.critical;
    }

    public void addExtendedKeyUsage(String extKeyUsage) {
        this.extKeyUsageVector.add(extKeyUsage);
    }

    public void setExtendedKeyUsage(Vector extKeyUsageVector2) {
        for (int i = 0; i < extKeyUsageVector2.size(); i++) {
            this.extKeyUsageVector.add((String) extKeyUsageVector2.get(i));
        }
    }

    public Vector getExtendedKeyUsage() {
        return this.extKeyUsageVector;
    }

    public byte[] encode() {
        int size = this.extKeyUsageVector.size();
        Vector tempVector = new Vector();
        for (int i = 0; i < size; i++) {
            tempVector.add(new DERObjectIdentifier((String) this.extKeyUsageVector.get(i)));
        }
        return new DEROctetString(new ExtendedKeyUsage(tempVector).getDERObject()).getOctets();
    }
}
