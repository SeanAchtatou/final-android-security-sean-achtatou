package cn.com.jit.ida.util.pki.asn1.pkcs.pkcs12;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.BERSequence;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.pkcs.pkcs7.ContentInfo;

public class AuthenticatedSafe implements DEREncodable {
    private ContentInfo[] info;

    public static AuthenticatedSafe getInstance(Object o) {
        if (o == null || (o instanceof AuthenticatedSafe)) {
            return (AuthenticatedSafe) o;
        }
        if (o instanceof ASN1Sequence) {
            return new AuthenticatedSafe((ASN1Sequence) o);
        }
        throw new IllegalArgumentException("unknown object in factory " + o.getClass().getName());
    }

    public AuthenticatedSafe(ASN1Sequence seq) {
        this.info = new ContentInfo[seq.size()];
        for (int i = 0; i != this.info.length; i++) {
            this.info[i] = ContentInfo.getInstance(seq.getObjectAt(i));
        }
    }

    public AuthenticatedSafe(ContentInfo[] info2) {
        this.info = info2;
    }

    public ContentInfo[] getContentInfo() {
        return this.info;
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        for (int i = 0; i != this.info.length; i++) {
            v.add(this.info[i]);
        }
        return new BERSequence(v);
    }
}
