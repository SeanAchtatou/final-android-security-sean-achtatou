package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.DERBitString;

public class ReasonFlags extends DERBitString {
    public static final int AA_COMPROMISE = 32768;
    public static final int AFFILIATION_CHANGED = 16;
    public static final int CA_COMPROMISE = 32;
    public static final int CERTIFICATE_HOLD = 2;
    public static final int CESSATION_OF_OPERATION = 4;
    public static final int KEY_COMPROMISE = 64;
    public static final int PRIVILEGE_WITHDRAWN = 1;
    public static final int SUPERSEDED = 8;
    public static final int UNUSED = 128;

    public ReasonFlags(int reasons) {
        super(getBytes(reasons), getPadBits(reasons));
    }

    public ReasonFlags(DERBitString reasons) {
        super(reasons.getBytes(), reasons.getPadBits());
    }
}
