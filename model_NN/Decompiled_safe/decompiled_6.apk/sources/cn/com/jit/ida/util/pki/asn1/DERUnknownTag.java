package cn.com.jit.ida.util.pki.asn1;

import java.io.IOException;

public class DERUnknownTag extends DERObject {
    byte[] data;
    int tag;

    public DERUnknownTag(int tag2, byte[] data2) {
        this.tag = tag2;
        this.data = data2;
    }

    public int getTag() {
        return this.tag;
    }

    public byte[] getData() {
        return this.data;
    }

    /* access modifiers changed from: package-private */
    public void encode(DEROutputStream out) throws IOException {
        out.writeEncoded(this.tag, this.data);
    }

    public boolean equals(Object o) {
        if (o == null || !(o instanceof DERUnknownTag)) {
            return false;
        }
        DERUnknownTag other = (DERUnknownTag) o;
        if (this.tag != other.tag || this.data.length != other.data.length) {
            return false;
        }
        for (int i = 0; i < this.data.length; i++) {
            if (this.data[i] != other.data[i]) {
                return false;
            }
        }
        return true;
    }
}
