package cn.com.jit.ida.util.pki.asn1;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class DERUTF8String extends DERObject implements DERString {
    String string;

    public static DERUTF8String getInstance(Object obj) {
        if (obj == null || (obj instanceof DERUTF8String)) {
            return (DERUTF8String) obj;
        }
        if (obj instanceof ASN1OctetString) {
            return new DERUTF8String(((ASN1OctetString) obj).getOctets());
        }
        if (obj instanceof ASN1TaggedObject) {
            return getInstance(((ASN1TaggedObject) obj).getObject());
        }
        throw new IllegalArgumentException("illegal object in getInstance: " + obj.getClass().getName());
    }

    public static DERUTF8String getInstance(ASN1TaggedObject obj, boolean explicit) {
        return getInstance(obj.getObject());
    }

    DERUTF8String(byte[] string2) {
        char ch;
        int i = 0;
        int length = 0;
        while (i < string2.length) {
            length++;
            if ((string2[i] & 224) == 224) {
                i += 3;
            } else if ((string2[i] & 192) == 192) {
                i += 2;
            } else {
                i++;
            }
        }
        char[] cs = new char[length];
        int i2 = 0;
        int length2 = 0;
        while (i2 < string2.length) {
            if ((string2[i2] & 224) == 224) {
                ch = (char) (((string2[i2] & 31) << 12) | ((string2[i2 + 1] & 63) << 6) | (string2[i2 + 2] & 63));
                i2 += 3;
            } else if ((string2[i2] & 192) == 192) {
                ch = (char) (((string2[i2] & 63) << 6) | (string2[i2 + 1] & 63));
                i2 += 2;
            } else {
                ch = (char) (string2[i2] & 255);
                i2++;
            }
            cs[length2] = ch;
            length2++;
        }
        this.string = new String(cs);
    }

    public DERUTF8String(String string2) {
        this.string = string2;
    }

    public String getString() {
        return this.string;
    }

    public int hashCode() {
        return getString().hashCode();
    }

    public boolean equals(Object o) {
        if (!(o instanceof DERUTF8String)) {
            return false;
        }
        return getString().equals(((DERUTF8String) o).getString());
    }

    /* access modifiers changed from: package-private */
    public void encode(DEROutputStream out) throws IOException {
        char[] c = this.string.toCharArray();
        ByteArrayOutputStream bOut = new ByteArrayOutputStream();
        for (int i = 0; i != c.length; i++) {
            char ch = c[i];
            if (ch < 128) {
                bOut.write(ch);
            } else if (ch < 2048) {
                bOut.write((ch >> 6) | 192);
                bOut.write((ch & '?') | 128);
            } else {
                bOut.write((ch >> 12) | 224);
                bOut.write(((ch >> 6) & 63) | 128);
                bOut.write((ch & '?') | 128);
            }
        }
        out.writeEncoded(12, bOut.toByteArray());
    }
}
