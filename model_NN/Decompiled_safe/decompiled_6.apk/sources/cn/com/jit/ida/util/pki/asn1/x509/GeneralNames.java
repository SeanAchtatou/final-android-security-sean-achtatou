package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject;
import cn.com.jit.ida.util.pki.asn1.DERBMPString;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DEREncodableVector;
import cn.com.jit.ida.util.pki.asn1.DERIA5String;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.DERPrintableString;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.DERT61String;
import cn.com.jit.ida.util.pki.asn1.DERTaggedObject;
import cn.com.jit.ida.util.pki.asn1.DERUTF8String;
import cn.com.jit.ida.util.pki.asn1.DERUniversalString;
import cn.com.jit.ida.util.pki.encoders.Hex;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class GeneralNames implements DEREncodable {
    public static final int DIRECTORYNAME = 4;
    public static final int DNSNAME = 2;
    public static final int EDIPARTYNAME = 5;
    public static final int EDIPARTYNAME_BMPSTRING = 204;
    public static final int EDIPARTYNAME_PRINTABLESTRING = 201;
    public static final int EDIPARTYNAME_TELETEXSTRING = 200;
    public static final int EDIPARTYNAME_UNIVERSALSTRING = 202;
    public static final int EDIPARTYNAME_UTF8STRING = 203;
    public static final int IPADDRESS = 7;
    public static final int OTHERNAME_GUID = 101;
    public static final int OTHERNAME_UPN = 100;
    public static final String OTHER_NAME_GUID_OID = "1.23.456.789";
    public static final String OTHER_NAME_UPN_OID = "1.3.6.1.4.1.311.20.2.3";
    public static final int REGISTEREDID = 8;
    public static final int RFC822NAME = 1;
    public static final int TYPE_NONE = -1;
    public static final int UNIFORMRESOURCEIDENTIFIER = 6;
    public static final int X400ADDRESS = 3;
    boolean isInsideImplicit = false;
    DEREncodableVector seq = new DEREncodableVector();

    public GeneralNames() {
    }

    public GeneralNames(Node nl) throws PKIException {
        NodeList nodelist = nl.getChildNodes();
        if (nodelist.getLength() == 0) {
            throw new PKIException("XML内容缺失,GeneralNames.GeneralNames(Node node)");
        }
        for (int i = 0; i < nodelist.getLength(); i++) {
            if (nodelist.item(i).getNodeName().equals("GeneralName")) {
                this.seq.add(new GeneralName(nodelist.item(i)).getDERObject());
            }
        }
    }

    public static GeneralNames getInstance(Object obj) {
        if (obj == null || (obj instanceof GeneralNames)) {
            return (GeneralNames) obj;
        }
        if (obj instanceof ASN1Sequence) {
            return new GeneralNames((ASN1Sequence) obj);
        }
        throw new IllegalArgumentException("illegal object in getInstance: " + obj.getClass().getName());
    }

    public static GeneralNames getInstance(ASN1TaggedObject obj, boolean explicit) {
        return getInstance(ASN1Sequence.getInstance(obj, explicit));
    }

    public GeneralNames(ASN1Sequence seq2) {
        this.seq = new DEREncodableVector(seq2);
    }

    public void markInsideImplicit(boolean isInsideImplicit2) {
        this.isInsideImplicit = isInsideImplicit2;
    }

    public DERObject getDERObject() {
        return new DERSequence(this.seq);
    }

    public GeneralName[] getNames() {
        GeneralName[] names = new GeneralName[this.seq.size()];
        for (int i = 0; i != this.seq.size(); i++) {
            names[i] = GeneralName.getInstance(this.seq.get(i));
        }
        return names;
    }

    public String toString() {
        StringBuffer buffer = new StringBuffer();
        if (this.seq != null) {
            GeneralName[] names = getNames();
            for (GeneralName name : names) {
                if (buffer != null && buffer.length() > 0) {
                    buffer.append("\n");
                }
                buffer.append(GeneralName.GeneralNameToString((DERTaggedObject) name.getDERObject()));
            }
        }
        return buffer.toString();
    }

    public void addRFC822Name(String value) throws PKIException {
        addGeneralName(1, value);
    }

    public void addDNSName(String value) throws PKIException {
        addGeneralName(2, value);
    }

    public void addDirectoryName(String value) throws PKIException {
        addGeneralName(4, value);
    }

    public void addUniformResourceIdentifier(String value) throws PKIException {
        addGeneralName(6, value);
    }

    public void addIPAddress(String value) throws PKIException {
        addGeneralName(7, value);
    }

    public void addRegisteredID(String value) throws PKIException {
        addGeneralName(8, value);
    }

    public void addOtherName_UPN(String value) throws PKIException {
        addGeneralName(100, value);
    }

    public void addOtherName_GUID(String value) throws PKIException {
        addGeneralName(101, value);
    }

    public void addEDIPartyName(int partyNameType, String partyName, int nameAssignerType, String nameAssigner) throws PKIException {
        if (partyName == null) {
            throw new PKIException(PKIException.EXT_ENCODING_DATA_NULL, PKIException.EXT_ENCODING_DATA_NULL_DES);
        }
        DEREncodableVector v = new DEREncodableVector();
        if (nameAssigner != null) {
            v.add(new DERTaggedObject(0, TypeToDEREncodable(nameAssignerType, nameAssigner)));
        }
        v.add(new DERTaggedObject(1, TypeToDEREncodable(partyNameType, partyName)));
        this.seq.add(new GeneralName(new DERSequence(v), 5));
    }

    public void addEDIPartyName(int partyNameType, byte[] partyName, int nameAssignerType, byte[] nameAssigner) throws PKIException {
        if (partyName == null) {
            throw new PKIException(PKIException.EXT_ENCODING_DATA_NULL, PKIException.EXT_ENCODING_DATA_NULL_DES);
        }
        DEREncodableVector v = new DEREncodableVector();
        if (nameAssigner != null) {
            v.add(new DERTaggedObject(0, TypeToDEREncodable(nameAssignerType, nameAssigner)));
        }
        v.add(new DERTaggedObject(1, TypeToDEREncodable(partyNameType, partyName)));
        this.seq.add(new GeneralName(new DERSequence(v), 5));
    }

    public void addEDIPartyName(int partyNameType, String partyName, int nameAssignerType, byte[] nameAssigner) throws PKIException {
        if (partyName == null) {
            throw new PKIException(PKIException.EXT_ENCODING_DATA_NULL, PKIException.EXT_ENCODING_DATA_NULL_DES);
        }
        DEREncodableVector v = new DEREncodableVector();
        if (nameAssigner != null) {
            v.add(new DERTaggedObject(0, TypeToDEREncodable(nameAssignerType, nameAssigner)));
        }
        v.add(new DERTaggedObject(1, TypeToDEREncodable(partyNameType, partyName)));
        this.seq.add(new GeneralName(new DERSequence(v), 5));
    }

    public void addEDIPartyName(int partyNameType, byte[] partyName, int nameAssignerType, String nameAssigner) throws PKIException {
        if (partyName == null) {
            throw new PKIException(PKIException.EXT_ENCODING_DATA_NULL, PKIException.EXT_ENCODING_DATA_NULL_DES);
        }
        DEREncodableVector v = new DEREncodableVector();
        if (nameAssigner != null) {
            v.add(new DERTaggedObject(0, TypeToDEREncodable(nameAssignerType, nameAssigner)));
        }
        v.add(new DERTaggedObject(1, TypeToDEREncodable(partyNameType, partyName)));
        this.seq.add(new GeneralName(new DERSequence(v), 5));
    }

    public void addGeneralName(int dataType, String value) throws PKIException {
        if (this.seq == null) {
            this.seq = new ASN1EncodableVector();
        }
        if (dataType == 100 || dataType == 101) {
            this.seq.add(new GeneralName(TypeConversion(dataType, value), 0));
        } else {
            this.seq.add(new GeneralName(TypeConversion(dataType, value), dataType));
        }
    }

    private DEREncodable TypeToDEREncodable(int valueType, String value) throws PKIException {
        switch (valueType) {
            case 200:
                return new DERT61String(value);
            case 201:
                return new DERPrintableString(value);
            case 202:
                throw new PKIException(PKIException.EXT_ENCODING_DATATYPE_NOT, PKIException.EXT_ENCODING_DATATYPE_NOT_DES);
            case 203:
                return new DERUTF8String(value);
            case 204:
                return new DERBMPString(value);
            default:
                return null;
        }
    }

    private DEREncodable TypeToDEREncodable(int valueType, byte[] value) throws PKIException {
        switch (valueType) {
            case 200:
                return new DERT61String(value);
            case 201:
                return new DERPrintableString(value);
            case 202:
                return new DERUniversalString(value);
            case 203:
                return new DERUTF8String(new String(value));
            case 204:
                return new DERBMPString(value);
            default:
                return null;
        }
    }

    private DERObject TypeConversion(int valueType, String value) throws PKIException {
        DERObject derencodable = null;
        switch (valueType) {
            case 1:
            case 2:
            case 6:
                derencodable = new DERIA5String(value).getDERObject();
                break;
            case 3:
                throw new PKIException(PKIException.EXT_ENCODING_DATATYPE_NOT, PKIException.EXT_ENCODING_DATATYPE_NOT_DES);
            case 4:
                derencodable = new X509Name(value).getDERObject();
                break;
            case 7:
                derencodable = new DEROctetString(IP4Address(value)).getDERObject();
                break;
            case 8:
                derencodable = new DERObjectIdentifier(value).getDERObject();
                break;
            case 100:
                derencodable = new OtherName(new DERObjectIdentifier("1.3.6.1.4.1.311.20.2.3"), new DERTaggedObject(true, 0, new DERUTF8String(value))).getDERObject();
                break;
            case 101:
                derencodable = new OtherName(new DERObjectIdentifier("1.23.456.789"), new DERTaggedObject(true, 0, new DEROctetString(Hex.decode(value)))).getDERObject();
                break;
        }
        if (derencodable != null) {
            return derencodable;
        }
        try {
            throw new PKIException(PKIException.EXT_ENCODING_DATATYPE_NOT, PKIException.EXT_ENCODING_DATATYPE_NOT_DES);
        } catch (Exception ex) {
            throw new PKIException(PKIException.EXT_ENCODE_GENERALNAMES_ERR, PKIException.EXT_ENCODE_GENERALNAMES_ERR_DES, ex);
        }
    }

    private byte[] IP4Address(String value) {
        String[] allip = value.split("/");
        if (allip.length == 2) {
            byte[] ip4_byte = new byte[8];
            GenIP4Address(ip4_byte, allip[0]);
            byte[] ip4_mask = new byte[4];
            GenIP4Address(ip4_mask, allip[1]);
            System.arraycopy(ip4_mask, 0, ip4_byte, 4, 4);
            return ip4_byte;
        } else if (allip.length != 1) {
            return null;
        } else {
            byte[] ip4_byte2 = new byte[4];
            GenIP4Address(ip4_byte2, value);
            return ip4_byte2;
        }
    }

    private void GenIP4Address(byte[] ip4_byte, String value) {
        for (int i = 0; i < 4; i++) {
            int pos = value.indexOf(".");
            if (pos == -1) {
                ip4_byte[i] = Integer.valueOf(value).byteValue();
            } else {
                ip4_byte[i] = Integer.valueOf(value.substring(0, pos)).byteValue();
            }
            value = value.substring(pos + 1, value.length());
        }
    }
}
