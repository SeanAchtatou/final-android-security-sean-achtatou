package cn.com.jit.ida.util.pki.cert.dn;

import cn.com.jit.ida.util.ini.ProFile;
import java.io.InputStream;

public class DNConfig {
    private static final String DNMAPPING = "DNMapping";
    private static final String ORDER = "Order";
    private static final String ORDERSTRING = "orderString";
    private static DNConfig instance = null;
    private ProFile proFile = null;

    private DNConfig() {
    }

    public static DNConfig getInstance() throws Exception {
        if (instance == null) {
            instance = new DNConfig();
            instance.init();
        }
        return instance;
    }

    private void init() {
        InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("cn/com/jit/ida/util/pki/cert/dn/dnConfig.ini");
        this.proFile = new ProFile();
        this.proFile.load(is);
    }

    public String getMapValue(String key) {
        return this.proFile.getValue(DNMAPPING, key);
    }

    public String getOrderValue() {
        return this.proFile.getValue(ORDER, ORDERSTRING);
    }
}
