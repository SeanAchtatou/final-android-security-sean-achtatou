package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.DEROutputStream;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.DERTaggedObject;
import cn.com.jit.ida.util.pki.asn1.DERUTCTime;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Vector;

public class V2TBSCertListGenerator {
    private Vector crlentries = null;
    X509Extensions extensions = null;
    X509Name issuer;
    Time nextUpdate = null;
    AlgorithmIdentifier signature;
    Time thisUpdate;
    DERInteger version = new DERInteger(1);

    public void setSignature(AlgorithmIdentifier signature2) {
        this.signature = signature2;
    }

    public void setIssuer(X509Name issuer2) {
        this.issuer = issuer2;
    }

    public void setThisUpdate(DERUTCTime thisUpdate2) {
        this.thisUpdate = new Time(thisUpdate2);
    }

    public void setNextUpdate(DERUTCTime nextUpdate2) {
        this.nextUpdate = new Time(nextUpdate2);
    }

    public void setThisUpdate(Time thisUpdate2) {
        this.thisUpdate = thisUpdate2;
    }

    public void setNextUpdate(Time nextUpdate2) {
        this.nextUpdate = nextUpdate2;
    }

    public void addCRLEntry(ASN1Sequence crlEntry) {
        if (this.crlentries == null) {
            this.crlentries = new Vector();
        }
        this.crlentries.addElement(crlEntry);
    }

    public void addCRLEntry(DERInteger userCertificate, DERUTCTime revocationDate, int reason) {
        addCRLEntry(userCertificate, new Time(revocationDate), reason);
    }

    public void addCRLEntry(DERInteger userCertificate, Time revocationDate, int reason) {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(userCertificate);
        v.add(revocationDate);
        if (reason != 0) {
            CRLReason rf = new CRLReason(reason);
            ByteArrayOutputStream bOut = new ByteArrayOutputStream();
            try {
                new DEROutputStream(bOut).writeObject(rf);
                byte[] value = bOut.toByteArray();
                ASN1EncodableVector v1 = new ASN1EncodableVector();
                v1.add(X509Extensions.ReasonCode);
                v1.add(new DEROctetString(value));
                v.add(new X509Extensions(new DERSequence(new DERSequence(v1))));
            } catch (IOException e) {
                throw new IllegalArgumentException("error encoding value: " + e);
            }
        }
        if (this.crlentries == null) {
            this.crlentries = new Vector();
        }
        this.crlentries.addElement(new DERSequence(v));
    }

    public void setExtensions(X509Extensions extensions2) {
        this.extensions = extensions2;
    }

    public TBSCertList generateTBSCertList() {
        if (this.signature == null || this.issuer == null || this.thisUpdate == null) {
            throw new IllegalStateException("Not all mandatory fields set in V2 TBSCertList generator.");
        }
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(this.version);
        v.add(this.signature);
        v.add(this.issuer);
        v.add(this.thisUpdate);
        if (this.nextUpdate != null) {
            v.add(this.nextUpdate);
        }
        if (this.crlentries != null) {
            ASN1EncodableVector certs = new ASN1EncodableVector();
            Enumeration it = this.crlentries.elements();
            while (it.hasMoreElements()) {
                certs.add((ASN1Sequence) it.nextElement());
            }
            v.add(new DERSequence(certs));
        }
        if (this.extensions != null) {
            v.add(new DERTaggedObject(0, this.extensions));
        }
        return new TBSCertList(new DERSequence(v));
    }
}
