package cn.com.jit.ida.util.pki.asn1.x509;

public class GroupAttribute extends Attribute {
    public GroupAttribute() {
        setTttrType(Attribute.GROUP_ATTRIBUTE_OID);
    }

    public IetfAttrSyntax createIetfAttrSyntax() {
        return new IetfAttrSyntax();
    }

    public void addIetfAttrSyntax(IetfAttrSyntax obj) {
        getAttrValues().add(obj.getDERObject());
    }
}
