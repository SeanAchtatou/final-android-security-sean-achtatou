package cn.com.jit.ida.util.pki.pkcs;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.Parser;
import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DEROutputStream;
import cn.com.jit.ida.util.pki.asn1.DERSet;
import cn.com.jit.ida.util.pki.asn1.pkcs.PKCSObjectIdentifiers;
import cn.com.jit.ida.util.pki.asn1.pkcs.pkcs7.ContentInfo;
import cn.com.jit.ida.util.pki.asn1.pkcs.pkcs7.SignedData;
import cn.com.jit.ida.util.pki.asn1.x509.X509CertificateStructure;
import cn.com.jit.ida.util.pki.cert.X509Cert;
import cn.com.jit.ida.util.pki.cipher.JCrypto;
import cn.com.jit.ida.util.pki.encoders.Base64;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Vector;

public class P7B {
    public X509Cert[] parseP7b(ContentInfo contentInfo) throws PKIException {
        if (!contentInfo.getContentType().equals(PKCSObjectIdentifiers.signedData)) {
            throw new PKIException("8172", "解析P7B证书链结构失败 证书链类型不匹配 " + contentInfo.getContentType().getId());
        }
        Enumeration enumeration = SignedData.getInstance(contentInfo.getContent()).getCertificates().getObjects();
        Vector v = new Vector();
        while (enumeration.hasMoreElements()) {
            v.add(new X509Cert(X509CertificateStructure.getInstance(enumeration.nextElement())));
        }
        X509Cert[] certs = new X509Cert[v.size()];
        v.toArray(certs);
        return certs;
    }

    public X509Cert[] parseP7b(String fileName) throws PKIException {
        try {
            FileInputStream fin = new FileInputStream(fileName);
            try {
                byte[] data = new byte[fin.available()];
                fin.read(data);
                fin.close();
                return parseP7b(data);
            } catch (Exception e) {
                ex = e;
                throw new PKIException("8172", PKIException.PARSE_P7B_ERR_DES, ex);
            }
        } catch (Exception e2) {
            ex = e2;
            throw new PKIException("8172", PKIException.PARSE_P7B_ERR_DES, ex);
        }
    }

    private X509Cert[] parseP7b(InputStream ins) throws PKIException {
        try {
            byte[] content = new byte[ins.available()];
            ins.read(content);
            ins.close();
            return parseP7b(content);
        } catch (Exception ex) {
            throw new PKIException("8172", PKIException.PARSE_P7B_ERR_DES, ex);
        }
    }

    public X509Cert[] parseP7b(byte[] data) throws PKIException {
        if (Parser.isBase64Encode(data)) {
            data = Base64.decode(Parser.convertBase64(data));
        }
        if (data[0] != 48) {
            throw new PKIException("8172", PKIException.PARSE_P7B_ERR_DES, new Exception("The P7B certification chain content error."));
        }
        try {
            return parseP7b(ContentInfo.getInstance((ASN1Sequence) Parser.writeBytes2DERObj(data)));
        } catch (Exception ex) {
            throw new PKIException("8172", PKIException.PARSE_P7B_ERR_DES, ex);
        }
    }

    public ContentInfo generateP7b(X509Cert[] certs) {
        ASN1EncodableVector v = new ASN1EncodableVector();
        for (X509Cert certStructure : certs) {
            v.add(certStructure.getCertStructure());
        }
        DERSet certSet = new DERSet(v);
        return new ContentInfo(PKCSObjectIdentifiers.signedData, new SignedData(new DERInteger(1), new DERSet(), new ContentInfo(PKCSObjectIdentifiers.data, null), certSet, null, new DERSet()));
    }

    public void generateP7bFile(X509Cert[] certs, String fileName) throws PKIException {
        ContentInfo contentInfo = generateP7b(certs);
        try {
            FileOutputStream fos = new FileOutputStream(fileName);
            DEROutputStream dos = new DEROutputStream(fos);
            dos.writeObject(contentInfo.getDERObject());
            dos.close();
            fos.close();
        } catch (Exception ex) {
            throw new PKIException("8171", PKIException.GEN_P7B_ERR_DES, ex);
        }
    }

    public byte[] generateP7bData_DER(X509Cert[] certs) throws PKIException {
        return Parser.writeDERObj2Bytes(generateP7b(certs).getDERObject());
    }

    public byte[] generateP7bData_B64(X509Cert[] certs) throws PKIException {
        return Base64.encode(generateP7bData_DER(certs));
    }

    public static void main(String[] args) {
        try {
            P7B p7b = new P7B();
            FileInputStream fis = new FileInputStream("d:\\1.p7b");
            byte[] data = new byte[fis.available()];
            fis.read(data, 0, data.length);
            X509Cert[] certs = p7b.parseP7b(data);
            X509Cert x509Cert = certs[0];
            System.out.println(certs.length);
            JCrypto jc = JCrypto.getInstance();
            jc.initialize(JCrypto.JSOFT_LIB, null);
            PKCS7SignedData signedData = new PKCS7SignedData(jc.openSession(JCrypto.JSOFT_LIB, null));
            signedData.load(data);
            boolean verifyP7SignedData = signedData.verifyP7SignedData("2c908d293966f5f7013967066b9d0004".getBytes(), x509Cert);
            byte[] b = "abc".getBytes();
            if (Parser.isBase64Encode(b)) {
                System.out.println(new String(Base64.decode(b)));
            }
        } catch (Exception ex) {
            System.err.println(ex.toString());
        }
    }
}
