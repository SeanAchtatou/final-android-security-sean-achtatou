package cn.com.jit.ida.util.pki.cert;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.DEREncodableVector;
import cn.com.jit.ida.util.pki.asn1.DERGeneralizedTime;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.DERSet;
import cn.com.jit.ida.util.pki.asn1.DERTaggedObject;
import cn.com.jit.ida.util.pki.asn1.DERUTF8String;
import cn.com.jit.ida.util.pki.asn1.x509.AttCertIssuer;
import cn.com.jit.ida.util.pki.asn1.x509.Attribute;
import cn.com.jit.ida.util.pki.asn1.x509.AttributeCertificateInfo;
import cn.com.jit.ida.util.pki.asn1.x509.ClearanceAttribute;
import cn.com.jit.ida.util.pki.asn1.x509.GeneralName;
import cn.com.jit.ida.util.pki.asn1.x509.GeneralNames;
import cn.com.jit.ida.util.pki.asn1.x509.GroupAttribute;
import cn.com.jit.ida.util.pki.asn1.x509.Holder;
import cn.com.jit.ida.util.pki.asn1.x509.RoleAttribute;
import cn.com.jit.ida.util.pki.asn1.x509.RoleSyntax;
import cn.com.jit.ida.util.pki.asn1.x509.V2X509AttCertGenerator;
import cn.com.jit.ida.util.pki.cipher.JKey;
import cn.com.jit.ida.util.pki.cipher.Session;
import java.io.File;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

public class X509AttCertGenerator extends V2X509AttCertGenerator {
    public void setHolder(String holder) throws PKIException {
        if (holder == null) {
            throw new PKIException(PKIException.HOLDER_NULL, PKIException.HOLDER_NULL_DES);
        }
        getV2AttCert().setHolder(Holder.getInstance(holder));
    }

    public void setIssuer(String issuer) throws PKIException {
        if (issuer == null) {
            throw new PKIException(PKIException.ISSUER_NULL, PKIException.ISSUER_NULL_DES);
        }
        getV2AttCert().setIssuer(AttCertIssuer.getInstance(issuer));
    }

    public void setSerialNumber(String strSerialNumber) throws PKIException {
        if (strSerialNumber == null) {
            throw new PKIException(PKIException.SN_NULL, PKIException.SN_NULL_DES);
        }
        getV2AttCert().setSerialNumber(new DERInteger(new BigInteger(strSerialNumber, 16)));
    }

    public void setSerialNumber(BigInteger serialNumber) throws PKIException {
        if (serialNumber == null) {
            throw new PKIException(PKIException.SN_NULL, PKIException.SN_NULL_DES);
        }
        getV2AttCert().setSerialNumber(new DERInteger(serialNumber));
    }

    public void setNotbefore(Date notBefore) throws PKIException {
        if (notBefore == null) {
            throw new PKIException(PKIException.NOT_BEFORE_NULL, PKIException.NOT_BEFORE_NULL_DES);
        }
        getV2AttCert().setStartDate(new DERGeneralizedTime(notBefore));
    }

    public void setNotafter(Date notafter) throws PKIException {
        if (notafter == null) {
            throw new PKIException(PKIException.NOT_AFTER_NULL, PKIException.NOT_AFTER_NULL_DES);
        }
        getV2AttCert().setEndDate(new DERGeneralizedTime(notafter));
    }

    public void setRoleAttribute(String roleAuthority, String roleName) throws PKIException {
        if (roleAuthority == null) {
            throw new PKIException(PKIException.ROLE_AUTHORITY_NULL, PKIException.ROLE_AUTHORITY_NULL_DES);
        } else if (roleName == null) {
            throw new PKIException(PKIException.ROLE_NAME_NULL, PKIException.ROLE_NAME_NULL_DES);
        } else {
            RoleSyntax role = new RoleSyntax(createAuthority(roleAuthority), createRoleName(roleName));
            DEREncodableVector vector = new DEREncodableVector();
            vector.add(role);
            addAttribute(new Attribute(new DERObjectIdentifier(Attribute.ROLE_ATTRIBUTE_OID), new DERSet(vector)));
        }
    }

    public RoleAttribute createRoleAttribute() {
        return new RoleAttribute();
    }

    public GroupAttribute createGroupAttribute() {
        return new GroupAttribute();
    }

    public ClearanceAttribute createClearanceAttribute() {
        return new ClearanceAttribute();
    }

    public void addAttribute(Attribute att) {
        getV2AttCert().addAttribute(att);
    }

    public List generateX509AttCerts(JKey priKey, Session session, Document requestDoc) throws PKIException {
        ArrayList certs = new ArrayList();
        Document resultDoc = translate(requestDoc);
        NodeList resultInfo = resultDoc.getDocumentElement().getElementsByTagName("AttributeCertificateInfo");
        for (int i = 0; i < resultInfo.getLength(); i++) {
            Element resultNode = (Element) resultInfo.item(i);
            addSerialNumber(resultNode, getSerialNumber(), resultDoc);
            setAttCertInfo(new AttributeCertificateInfo(resultNode));
            setSignatureAlg(V2X509AttCertGenerator.generateMechanisNameByOID(getAttCertInfo().getSignature().getObjectId().getId()));
            signCertInfo(priKey, session);
            certs.add(constructAttCertificate());
        }
        return certs;
    }

    private String getSerialNumber() {
        Random tRnd = new Random();
        long tLong1 = tRnd.nextLong();
        long tLong2 = tRnd.nextLong();
        String n1 = Long.toHexString(tLong1);
        return (n1 + Long.toHexString(tLong2)).toUpperCase();
    }

    private void addSerialNumber(Element root, String serialNumber, Document resultDoc) {
        Text serialText = resultDoc.createTextNode("serialNumberText");
        serialText.setData(serialNumber);
        ((Element) root.getElementsByTagName("serialNumber").item(0)).appendChild(serialText);
    }

    private Document translate(Document requestXML) throws PKIException {
        DOMSource xmlSoure = new DOMSource(requestXML);
        Source xslSoure = new StreamSource(new File("Template/TransRequest.xsl"));
        DOMResult domResult = new DOMResult();
        try {
            Transformer trans = TransformerFactory.newInstance().newTransformer(xslSoure);
            trans.setOutputProperty("encoding", "gb2312");
            trans.setOutputProperty("indent", "yes");
            try {
                trans.transform(xmlSoure, domResult);
                return (Document) domResult.getNode();
            } catch (TransformerException ex) {
                throw new PKIException("转换XML文档时出现错误", ex);
            }
        } catch (TransformerConfigurationException ex2) {
            throw new PKIException("读取转换文件xsl时出现错误", ex2);
        }
    }

    private GeneralNames createAuthority(String name) {
        DERUTF8String a = new DERUTF8String(name);
        DERObjectIdentifier id = new DERObjectIdentifier(RoleSyntax.JIT_ROLE_OTHER_NAME_ID);
        DERTaggedObject b = new DERTaggedObject(true, 0, a);
        DEREncodableVector vector = new DEREncodableVector();
        vector.add(id);
        vector.add(b);
        DERTaggedObject gname = new DERTaggedObject(true, 0, new DERSequence(vector));
        DEREncodableVector vector2 = new DEREncodableVector();
        vector2.add(gname);
        return GeneralNames.getInstance(new DERSequence(vector2));
    }

    private GeneralName createRoleName(String name) {
        DERUTF8String a = new DERUTF8String(name);
        DERObjectIdentifier id = new DERObjectIdentifier(RoleSyntax.JIT_ROLE_OTHER_NAME_ID);
        DERTaggedObject b = new DERTaggedObject(0, a);
        DEREncodableVector vector = new DEREncodableVector();
        vector.add(id);
        vector.add(b);
        return GeneralName.getInstance(new DERTaggedObject(true, 0, new DERSequence(vector)));
    }
}
