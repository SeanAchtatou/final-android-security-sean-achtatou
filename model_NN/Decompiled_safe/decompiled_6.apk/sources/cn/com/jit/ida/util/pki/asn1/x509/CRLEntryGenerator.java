package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DERSequence;

public class CRLEntryGenerator {
    private X509Extensions crlEntryExtensions = null;
    private Time revocationDate = null;
    private DERInteger userCertificate = null;

    public void setUserCertificate(DERInteger CertSerialNumber) {
        this.userCertificate = CertSerialNumber;
    }

    public void setRevocationDate(Time revocationDate2) {
        this.revocationDate = revocationDate2;
    }

    public void setCrlEntryExtensions(X509Extensions crlEntryExtensions2) {
        this.crlEntryExtensions = crlEntryExtensions2;
    }

    public CRLEntry generateCRLEntry() throws Exception {
        if (this.userCertificate == null || this.revocationDate == null) {
            throw new Exception("userCertificate and revocationDate must be set");
        }
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(this.userCertificate);
        v.add(this.revocationDate);
        if (this.crlEntryExtensions != null) {
            v.add(this.crlEntryExtensions);
        }
        return new CRLEntry(new DERSequence(v));
    }
}
