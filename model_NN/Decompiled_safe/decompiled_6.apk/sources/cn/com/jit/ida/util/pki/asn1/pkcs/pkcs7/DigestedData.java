package cn.com.jit.ida.util.pki.asn1.pkcs.pkcs7;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1OctetString;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.pkcs.PKCSObjectIdentifiers;
import cn.com.jit.ida.util.pki.asn1.x509.AlgorithmIdentifier;

public class DigestedData implements DEREncodable, PKCSObjectIdentifiers {
    private ContentInfo contentInfo;
    private ASN1OctetString digest;
    private AlgorithmIdentifier digestAlgorithm;
    private DERInteger version;

    public static DigestedData getInstance(Object o) {
        if (o == null || (o instanceof DigestedData)) {
            return (DigestedData) o;
        }
        if (o instanceof ASN1Sequence) {
            return new DigestedData((ASN1Sequence) o);
        }
        throw new IllegalArgumentException("unknown object in factory:" + o.getClass().getName());
    }

    public DigestedData(DERInteger _version, AlgorithmIdentifier _digestAlgorithm, ContentInfo _contentInfo, ASN1OctetString _digest) {
        this.version = _version;
        this.digestAlgorithm = _digestAlgorithm;
        this.contentInfo = _contentInfo;
        this.digest = _digest;
    }

    public DigestedData(ASN1Sequence seq) {
        this.version = (DERInteger) seq.getObjectAt(0);
        this.digestAlgorithm = AlgorithmIdentifier.getInstance(seq.getObjectAt(1));
        this.contentInfo = ContentInfo.getInstance(seq.getObjectAt(2));
        this.digest = ASN1OctetString.getInstance(seq.getObjectAt(3));
    }

    public DERInteger getVersion() {
        return this.version;
    }

    public AlgorithmIdentifier getDigestAlgorithm() {
        return this.digestAlgorithm;
    }

    public ContentInfo getContentInfo() {
        return this.contentInfo;
    }

    public ASN1OctetString getDigest() {
        return this.digest;
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(this.version);
        v.add(this.digestAlgorithm);
        v.add(this.contentInfo);
        v.add(this.digest);
        return new DERSequence(v);
    }
}
