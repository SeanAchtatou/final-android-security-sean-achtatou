package cn.com.jit.ida.util.pki.extension;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.x509.X509Extensions;

public class SubjectAltNameExt extends GeneralNamesExt {
    public static final String DNS_TYPE_NAME = "2";
    public static final String DN_TYPE_NAME = "4";
    public static final String URI_TYPE_NAME = "6";

    public SubjectAltNameExt() {
        this.OID = X509Extensions.SubjectAlternativeName.getId();
    }

    public SubjectAltNameExt(ASN1Sequence asn1Sequence) {
        super(asn1Sequence);
        this.OID = X509Extensions.SubjectAlternativeName.getId();
    }

    public void setOtherName(String nameOID, String otherName) throws PKIException {
        if (nameOID.equals("1.3.6.1.4.1.311.20.2.3")) {
            addGeneralName(100, otherName);
        } else if (nameOID.equals("1.23.456.789")) {
            addGeneralName(101, otherName);
        }
    }

    public void setName(String nameType, String name) throws PKIException {
        if (nameType.equals("2")) {
            addGeneralName(2, name);
        } else if (nameType.equals("6")) {
            addGeneralName(6, name);
        } else if (nameType.equals("4")) {
            addGeneralName(4, name);
        }
    }
}
