package cn.com.jit.ida.util.pki.asn1.cms;

import cn.com.jit.ida.util.pki.asn1.ASN1Encodable;
import cn.com.jit.ida.util.pki.asn1.ASN1OctetString;
import cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERTaggedObject;

public class SignerIdentifier extends ASN1Encodable {
    private DEREncodable id;

    public SignerIdentifier(IssuerAndSerialNumber id2) {
        this.id = id2;
    }

    public SignerIdentifier(ASN1OctetString id2) {
        this.id = new DERTaggedObject(false, 0, id2);
    }

    public SignerIdentifier(DERObject id2) {
        this.id = id2;
    }

    public static SignerIdentifier getInstance(Object o) {
        if (o == null || (o instanceof SignerIdentifier)) {
            return (SignerIdentifier) o;
        }
        if (o instanceof IssuerAndSerialNumber) {
            return new SignerIdentifier((IssuerAndSerialNumber) o);
        }
        if (o instanceof ASN1OctetString) {
            return new SignerIdentifier((ASN1OctetString) o);
        }
        if (o instanceof DERObject) {
            return new SignerIdentifier((DERObject) o);
        }
        throw new IllegalArgumentException("Illegal object in SignerIdentifier: " + o.getClass().getName());
    }

    public boolean isTagged() {
        return this.id instanceof ASN1TaggedObject;
    }

    public DEREncodable getId() {
        if (this.id instanceof ASN1TaggedObject) {
            return ASN1OctetString.getInstance((ASN1TaggedObject) this.id, false);
        }
        return this.id;
    }

    public DERObject toASN1Object() {
        return this.id.getDERObject();
    }
}
