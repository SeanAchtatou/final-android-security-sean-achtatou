package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERGeneralizedTime;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

public class AttCertValidityPeriod implements DEREncodable {
    DERGeneralizedTime notAfterTime;
    DERGeneralizedTime notBeforeTime;

    public static AttCertValidityPeriod getInstance(Object obj) {
        if (obj instanceof AttCertValidityPeriod) {
            return (AttCertValidityPeriod) obj;
        }
        if (obj instanceof ASN1Sequence) {
            return new AttCertValidityPeriod((ASN1Sequence) obj);
        }
        throw new IllegalArgumentException("unknown object in factory");
    }

    public AttCertValidityPeriod(DERGeneralizedTime notBeforeTime2, DERGeneralizedTime notAfterTime2) {
        this.notBeforeTime = notBeforeTime2;
        this.notAfterTime = notAfterTime2;
    }

    public AttCertValidityPeriod(ASN1Sequence seq) {
        this.notBeforeTime = (DERGeneralizedTime) seq.getObjectAt(0);
        this.notAfterTime = (DERGeneralizedTime) seq.getObjectAt(1);
    }

    public DERGeneralizedTime getNotBeforeTime() {
        return this.notBeforeTime;
    }

    public DERGeneralizedTime getNotAfterTime() {
        return this.notAfterTime;
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(this.notBeforeTime);
        v.add(this.notAfterTime);
        return new DERSequence(v);
    }

    public AttCertValidityPeriod(Node nl) throws PKIException {
        NodeList nodelist = nl.getChildNodes();
        if (nodelist.getLength() == 0) {
            throw new PKIException("XML内容缺失,AttrCertValidityPeriod.AttrCertValidityPeriod(),属性证书有效期没有子节点");
        }
        for (int i = 0; i < nodelist.getLength(); i++) {
            Node cnode = nodelist.item(i);
            String sname = cnode.getNodeName();
            if (sname.equals("notBefore")) {
                this.notBeforeTime = new DERGeneralizedTime(((Text) cnode.getFirstChild()).getData().trim());
            }
            if (sname.equals("notAfter")) {
                this.notAfterTime = new DERGeneralizedTime(((Text) cnode.getFirstChild()).getData().trim());
            }
        }
    }
}
