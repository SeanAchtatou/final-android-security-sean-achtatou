package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject;
import cn.com.jit.ida.util.pki.asn1.DERBitString;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.extension.GeneralNamesExt;
import java.math.BigInteger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

public class IssuerSerial implements DEREncodable {
    GeneralNamesExt issuer;
    DERBitString issuerUID;
    DERInteger serial;

    public IssuerSerial(ASN1Sequence seq) {
        this.issuer = new GeneralNamesExt((DERSequence) seq.getObjectAt(0));
        this.serial = (DERInteger) seq.getObjectAt(1);
        if (seq.size() == 3) {
            this.issuerUID = (DERBitString) seq.getObjectAt(2);
        }
    }

    public IssuerSerial(Node nl) throws PKIException {
        NodeList nodelist = nl.getChildNodes();
        if (nodelist.getLength() == 0) {
            throw new PKIException("XML内容缺失IssuerSerial", getClass().toString());
        }
        for (int i = 0; i < nodelist.getLength(); i++) {
            Node cnode = nodelist.item(i);
            if (cnode.getNodeName().equals("IssuerSerial")) {
                iniByXml(cnode);
            }
        }
    }

    private void iniByXml(Node nl) throws PKIException {
        NodeList nodelist = nl.getChildNodes();
        if (nodelist.getLength() == 0) {
            throw new PKIException("XML内容缺失IssuerSerial没有子节点");
        }
        for (int i = 0; i < nodelist.getLength(); i++) {
            Node cnode = nodelist.item(i);
            String sname = cnode.getNodeName();
            if (sname.equals("issuer")) {
                NodeList nodelist1 = cnode.getChildNodes();
                if (nodelist.getLength() == 0) {
                    throw new PKIException("XML内容缺失Issuer没有子节点");
                }
                this.issuer = new GeneralNamesExt();
                for (int j = 0; j < nodelist1.getLength(); j++) {
                    Node cnode1 = nodelist1.item(j);
                    if (cnode1.getNodeName().equals("GeneralName")) {
                        String type = cnode1.getAttributes().item(0).getNodeValue();
                        try {
                            if (type.equals("4")) {
                                this.issuer.addDirectoryName(((Text) cnode1.getFirstChild()).getData());
                            }
                            if (type.equals("7")) {
                                this.issuer.addIPAddress(((Text) cnode1.getFirstChild()).getData());
                            }
                            if (type.equals("2")) {
                                this.issuer.addDNSName(((Text) cnode1.getFirstChild()).getData());
                            }
                            if (type.equals("6")) {
                                this.issuer.addUniformResourceIdentifier(((Text) cnode1.getFirstChild()).getData());
                            }
                            if (type.equals("0")) {
                                this.issuer.addOtherName_UPN(((Text) cnode1.getFirstChild()).getData().trim());
                            }
                        } catch (IllegalArgumentException e) {
                            throw new PKIException("IllegalArgumentException,IssuerSerial.IssuerSerial()", e);
                        }
                    }
                }
            }
            if (sname.equals("serial")) {
                try {
                    this.serial = new DERInteger(new BigInteger(((Text) cnode.getFirstChild()).getData().trim(), 16));
                } catch (NumberFormatException e2) {
                    throw new PKIException("NumberFormatException,IssuerSerial.IssuerSerial()", e2);
                }
            }
            if (sname.equals("issuerUID")) {
                this.issuerUID = new DERBitString(((Text) cnode.getFirstChild()).getData().trim().getBytes());
            }
        }
    }

    public static IssuerSerial getInstance(ASN1TaggedObject obj, boolean explicit) {
        return getInstance(ASN1Sequence.getInstance(obj, explicit));
    }

    public static IssuerSerial getInstance(Object obj) {
        if (obj == null || (obj instanceof GeneralNames)) {
            return (IssuerSerial) obj;
        }
        if (obj instanceof ASN1Sequence) {
            return new IssuerSerial((ASN1Sequence) obj);
        }
        throw new IllegalArgumentException("illegal object in getInstance: " + obj.getClass().getName());
    }

    public GeneralNamesExt getIssuer() {
        return this.issuer;
    }

    public DERInteger getSerial() {
        return this.serial;
    }

    public DERBitString getIssuerUID() {
        return this.issuerUID;
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(this.issuer.getASN1Object());
        v.add(this.serial);
        if (this.issuerUID != null) {
            v.add(this.issuerUID);
        }
        return new DERSequence(v);
    }
}
