package cn.com.jit.ida.util.pki.cipher.softsm;

public class Sm4_Context {
    public boolean isPadding;
    public int mode;
    public long[] sk;

    public Sm4_Context() {
        this.mode = 0;
        this.sk = null;
        this.isPadding = true;
        this.mode = 0;
        this.isPadding = true;
        this.sk = new long[32];
    }
}
