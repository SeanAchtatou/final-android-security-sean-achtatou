package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.DERPrintableString;

public class JITOrganizationCode extends DERPrintableString {
    public JITOrganizationCode(byte[] string) {
        super(string);
    }

    public JITOrganizationCode(String string) {
        super(string);
    }

    public String getOrganizationCode() {
        return getString();
    }
}
