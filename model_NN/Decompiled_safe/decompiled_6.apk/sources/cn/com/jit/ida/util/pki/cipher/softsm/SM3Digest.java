package cn.com.jit.ida.util.pki.cipher.softsm;

import android.support.v4.view.MotionEventCompat;
import cn.com.jit.ida.util.pki.cipher.Mechanism;
import de.innosystec.unrar.unpack.vm.VMCmdFlags;
import org.bouncycastle.util.encoders.Hex;

public class SM3Digest extends GeneralDigest {
    private static final int[] X0 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    private static final int[] v0 = {1937774191, 1226093241, 388252375, -628488704, -1452330820, 372324522, -477237683, -1325724082};
    private int DIGEST_LENGTH = 32;
    private int T_00_15 = 2043430169;
    private int T_16_63 = 2055708042;
    private int[] X = new int[68];
    private int[] v = new int[8];
    private int[] v_ = new int[8];
    private int xOff;

    public String getAlgorithmName() {
        return Mechanism.SM3;
    }

    public int getDigestSize() {
        return this.DIGEST_LENGTH;
    }

    public SM3Digest() {
        reset();
    }

    public SM3Digest(SM3Digest t) {
        super(t);
        System.arraycopy(t.X, 0, this.X, 0, t.X.length);
        this.xOff = t.xOff;
        System.arraycopy(t.v, 0, this.v, 0, t.v.length);
    }

    public void reset() {
        super.reset();
        System.arraycopy(v0, 0, this.v, 0, v0.length);
        this.xOff = 0;
        System.arraycopy(X0, 0, this.X, 0, X0.length);
    }

    public void ProcessBlock() {
        int[] ww = this.X;
        int[] ww_ = new int[64];
        for (int i = 16; i < 68; i++) {
            ww[i] = (P1((ww[i - 16] ^ ww[i - 9]) ^ ROTATE(ww[i - 3], 15)) ^ ROTATE(ww[i - 13], 7)) ^ ww[i - 6];
        }
        for (int i2 = 0; i2 < 64; i2++) {
            ww_[i2] = ww[i2] ^ ww[i2 + 4];
        }
        int[] vv = this.v;
        int[] vv_ = this.v_;
        System.arraycopy(vv, 0, vv_, 0, v0.length);
        for (int i3 = 0; i3 < 16; i3++) {
            int aaa = ROTATE(vv_[0], 12);
            int SS1 = ROTATE(vv_[4] + aaa + ROTATE(this.T_00_15, i3), 7);
            int TT1 = FF_00_15(vv_[0], vv_[1], vv_[2]) + vv_[3] + (SS1 ^ aaa) + ww_[i3];
            int TT2 = GG_00_15(vv_[4], vv_[5], vv_[6]) + vv_[7] + SS1 + ww[i3];
            vv_[3] = vv_[2];
            vv_[2] = ROTATE(vv_[1], 9);
            vv_[1] = vv_[0];
            vv_[0] = TT1;
            vv_[7] = vv_[6];
            vv_[6] = ROTATE(vv_[5], 19);
            vv_[5] = vv_[4];
            vv_[4] = P0(TT2);
        }
        for (int i4 = 16; i4 < 64; i4++) {
            int aaa2 = ROTATE(vv_[0], 12);
            int SS12 = ROTATE(vv_[4] + aaa2 + ROTATE(this.T_16_63, i4), 7);
            int TT12 = FF_16_63(vv_[0], vv_[1], vv_[2]) + vv_[3] + (SS12 ^ aaa2) + ww_[i4];
            int TT22 = GG_16_63(vv_[4], vv_[5], vv_[6]) + vv_[7] + SS12 + ww[i4];
            vv_[3] = vv_[2];
            vv_[2] = ROTATE(vv_[1], 9);
            vv_[1] = vv_[0];
            vv_[0] = TT12;
            vv_[7] = vv_[6];
            vv_[6] = ROTATE(vv_[5], 19);
            vv_[5] = vv_[4];
            vv_[4] = P0(TT22);
        }
        for (int i5 = 0; i5 < 8; i5++) {
            vv[i5] = vv[i5] ^ vv_[i5];
        }
        this.xOff = 0;
        System.arraycopy(X0, 0, this.X, 0, X0.length);
    }

    public void ProcessWord(byte[] in_Renamed, int inOff) {
        int inOff2 = inOff + 1;
        int inOff3 = inOff2 + 1;
        this.X[this.xOff] = (in_Renamed[inOff] << 24) | ((in_Renamed[inOff2] & 255) << VMCmdFlags.VMCF_PROC) | ((in_Renamed[inOff3] & 255) << 8) | (in_Renamed[inOff3 + 1] & MotionEventCompat.ACTION_MASK);
        int i = this.xOff + 1;
        this.xOff = i;
        if (i == 16) {
            ProcessBlock();
        }
    }

    public void ProcessLength(long bitLength) {
        if (this.xOff > 14) {
            ProcessBlock();
        }
        this.X[14] = (int) SupportClass.URShift(bitLength, 32);
        this.X[15] = (int) (-1 & bitLength);
    }

    public static void IntToBigEndian(int n, byte[] bs, int off) {
        bs[off] = (byte) SupportClass.URShift(n, 24);
        int off2 = off + 1;
        bs[off2] = (byte) SupportClass.URShift(n, 16);
        int off3 = off2 + 1;
        bs[off3] = (byte) SupportClass.URShift(n, 8);
        bs[off3 + 1] = (byte) n;
    }

    public int doFinal(byte[] out_Renamed, int outOff) {
        Finish();
        for (int i = 0; i < 8; i++) {
            IntToBigEndian(this.v[i], out_Renamed, (i * 4) + outOff);
        }
        reset();
        return this.DIGEST_LENGTH;
    }

    private int ROTATE(int x, int n) {
        return (x << n) | SupportClass.URShift(x, 32 - n);
    }

    private int P0(int X2) {
        return (ROTATE(X2, 9) ^ X2) ^ ROTATE(X2, 17);
    }

    private int P1(int X2) {
        return (ROTATE(X2, 15) ^ X2) ^ ROTATE(X2, 23);
    }

    private int FF_00_15(int X2, int Y, int Z) {
        return (X2 ^ Y) ^ Z;
    }

    private int FF_16_63(int X2, int Y, int Z) {
        return (X2 & Y) | (X2 & Z) | (Y & Z);
    }

    private int GG_00_15(int X2, int Y, int Z) {
        return (X2 ^ Y) ^ Z;
    }

    private int GG_16_63(int X2, int Y, int Z) {
        return (X2 & Y) | ((X2 ^ -1) & Z);
    }

    public static void main(String[] args) {
        byte[] msg1 = "abc".getBytes();
        byte[] msg2 = "abcd".getBytes();
        byte[] md = new byte[32];
        SM3Digest sm3 = new SM3Digest();
        sm3.BlockUpdate(msg1, 0, msg1.length);
        sm3.doFinal(md, 0);
        System.out.println(new String(Hex.encode(md)));
        SM3Digest sm31 = new SM3Digest();
        byte[] md1 = new byte[32];
        for (int i = 0; i < 16; i++) {
            sm31.BlockUpdate(msg2, 0, msg2.length);
        }
        sm31.doFinal(md1, 0);
        System.out.println(new String(Hex.encode(md1)));
        SM3Digest sm3_ = new SM3Digest();
        sm3_.BlockUpdate(msg2, 0, msg2.length);
        SM3Digest sm32 = new SM3Digest(sm3_);
        for (int i2 = 1; i2 < 16; i2++) {
            sm32.BlockUpdate(msg2, 0, msg2.length);
        }
        sm32.doFinal(md, 0);
        System.out.println(new String(Hex.encode(md)));
    }

    public void update(byte[] in, int inOff, int len) {
    }
}
