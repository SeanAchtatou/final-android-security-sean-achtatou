package cn.com.jit.ida.util.pki.extension;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.x509.JITStationCode;
import cn.com.jit.ida.util.pki.asn1.x509.X509Extensions;

public class StationCodeExt extends AbstractStandardExtension {
    private String stationcode = null;

    public StationCodeExt() {
        this.OID = X509Extensions.JIT_StationCode.getId();
        this.critical = false;
    }

    public StationCodeExt(String value) {
        this.OID = X509Extensions.JIT_StationCode.getId();
        this.critical = false;
        this.stationcode = value;
    }

    public void SetStationCode(String value) {
        this.stationcode = value;
    }

    public byte[] encode() throws PKIException {
        if (this.stationcode != null) {
            return new DEROctetString(new JITStationCode(this.stationcode).getDERObject()).getOctets();
        }
        throw new PKIException(PKIException.EXT_ENCODING_DATA_NULL, PKIException.EXT_ENCODING_DATA_NULL_DES);
    }

    public boolean getCritical() {
        return this.critical;
    }

    public String getOID() {
        return this.OID;
    }

    public void setCritical(boolean critical) {
        this.critical = critical;
    }
}
