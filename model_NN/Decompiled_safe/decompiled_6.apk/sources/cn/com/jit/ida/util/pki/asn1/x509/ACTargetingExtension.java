package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.Parser;
import cn.com.jit.ida.util.pki.asn1.DERBoolean;
import cn.com.jit.ida.util.pki.asn1.DEREncodableVector;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.extension.AbstractStandardExtension;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ACTargetingExtension extends AbstractStandardExtension {
    private DERBoolean critical = new DERBoolean(true);
    private DERObjectIdentifier extnID = X509Extensions.ACTargeting;
    private DEROctetString extnValue;
    private DEREncodableVector targetsVec;

    public String getOID() {
        return this.extnID.getId();
    }

    public void setCritical(boolean critical2) {
        this.critical = new DERBoolean(critical2);
    }

    public boolean getCritical() {
        return this.critical.isTrue();
    }

    public ACTargetingExtension(Node nl) throws PKIException {
        NodeList nodelist = nl.getChildNodes();
        if (nodelist.getLength() == 0) {
            throw new PKIException("XML内容缺失,ACTargetingExtension.ACTargetingExtension(),属性证书定向扩展没有子节点");
        }
        this.targetsVec = new DEREncodableVector();
        for (int i = 0; i < nodelist.getLength(); i++) {
            Node cnode = nodelist.item(i);
            if (cnode.getNodeName().equals("Targets")) {
                this.targetsVec.add(new Targets(cnode).getDERObject());
            }
        }
    }

    public DERObject toASN1Object() throws PKIException {
        DEREncodableVector vec = new DEREncodableVector();
        vec.add(this.extnID);
        vec.add(this.critical);
        DEREncodableVector vec1 = new DEREncodableVector();
        for (int i = 0; i < this.targetsVec.size(); i++) {
            vec1.add(((Targets) this.targetsVec.get(i)).getDERObject());
        }
        this.extnValue = new DEROctetString(Parser.writeDERObj2Bytes(new DERSequence(vec1)));
        vec.add(this.extnValue);
        return new DERSequence(vec);
    }

    public byte[] encode() throws PKIException {
        return new DEROctetString(toASN1Object()).getOctets();
    }

    public void decode(DERObject parm1) throws PKIException {
        throw new UnsupportedOperationException("Method decode() not yet implemented.");
    }
}
