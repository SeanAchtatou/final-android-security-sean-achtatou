package cn.com.jit.ida.util.pki.extension;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.x509.PolicyConstraints;
import cn.com.jit.ida.util.pki.asn1.x509.X509Extensions;

public class PolicyConstraintsExt extends AbstractStandardExtension {
    private int inhibitPolicyMapping = -1;
    private int requireExplicitPolicy = -1;

    public PolicyConstraintsExt(int requireExplicitPolicy2, int inhibitPolicyMapping2) {
        this.OID = X509Extensions.PolicyConstraints.getId();
        this.critical = false;
        this.requireExplicitPolicy = requireExplicitPolicy2;
        this.inhibitPolicyMapping = inhibitPolicyMapping2;
    }

    public PolicyConstraintsExt(ASN1Sequence seq) {
        for (int i = 0; i < seq.size(); i++) {
            ASN1TaggedObject obj = (ASN1TaggedObject) seq.getObjectAt(i);
            switch (obj.getTagNo()) {
                case 0:
                    this.requireExplicitPolicy = DERInteger.getInstance(obj, false).getValue().intValue();
                    break;
                case 1:
                    this.inhibitPolicyMapping = DERInteger.getInstance(obj, false).getValue().intValue();
                    break;
                default:
                    throw new IllegalArgumentException("illegal tag");
            }
        }
    }

    public int getRequireExplicitPolicy() {
        return this.requireExplicitPolicy;
    }

    public int getInhibitPolicyMapping() {
        return this.inhibitPolicyMapping;
    }

    public String getOID() {
        return this.OID;
    }

    public void setCritical(boolean critical) {
        this.critical = critical;
    }

    public boolean getCritical() {
        return this.critical;
    }

    public byte[] encode() throws PKIException {
        DERInteger require = null;
        if (this.requireExplicitPolicy >= 0) {
            require = new DERInteger(this.requireExplicitPolicy);
        }
        DERInteger inhibit = null;
        if (this.inhibitPolicyMapping >= 0) {
            inhibit = new DERInteger(this.inhibitPolicyMapping);
        }
        return new DEROctetString(new PolicyConstraints(require, inhibit).getDERObject()).getOctets();
    }
}
