package cn.com.jit.ida.util.pki.cert.dn;

import cn.com.jit.ida.util.pki.asn1.DEROutputStream;
import cn.com.jit.ida.util.pki.asn1.x509.X509Name;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import javax.security.auth.x500.X500Principal;

public class DNTool {
    private HashMap dnrules = null;
    private X509Name x509 = null;

    public DNTool() {
    }

    public DNTool(String dn) {
        this.x509 = new X509Name(dn.trim());
        if (this.dnrules != null) {
            this.x509.setRules(this.dnrules);
        }
    }

    public void setRules(Map rules) {
        this.dnrules = (HashMap) rules;
    }

    public boolean dnCheck(String dn) throws Exception {
        try {
            X509Name name = new X509Name(dn.trim());
            if (this.dnrules != null) {
                name.setRules(this.dnrules);
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public List getDNInfo(String dnName) {
        if (this.x509 == null) {
            return null;
        }
        List list = new ArrayList();
        String upperCase = dnName.toUpperCase();
        if (this.dnrules != null) {
            this.x509.setRules(this.dnrules);
        }
        Vector values = this.x509.getValues();
        Vector oids = this.x509.getOIDs();
        Object oid = X509Name.DefaultLookUp.get(dnName.toLowerCase());
        if (oid == null) {
            return null;
        }
        for (int index = 0; index != oids.size(); index++) {
            if (oid.equals(oids.get(index))) {
                list.add((String) values.elementAt(index));
            }
        }
        return list;
    }

    public String dnSort(String dn) throws Exception {
        return DNConvert(dn);
    }

    public String DNConvert(String dn) throws Exception {
        X509Name name = new X509Name(dn.trim());
        this.x509 = name;
        if (this.dnrules != null) {
            name.setRules(this.dnrules);
            this.x509.setRules(this.dnrules);
        }
        return name.toString().trim();
    }

    public boolean equals(Object dn) {
        if (this == dn) {
            return true;
        }
        if (!(dn instanceof DNTool)) {
            return false;
        }
        return equals(dn.toString());
    }

    public boolean equals(String dn) {
        String tmp = null;
        try {
            tmp = DNConvert(dn);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tmp.equals(this.x509.toString().trim());
    }

    public String tostring() {
        if (this.x509 == null) {
            return null;
        }
        if (this.dnrules != null) {
            this.x509.setRules(this.dnrules);
        }
        return this.x509.toString().trim();
    }

    public String convertSun(String dn) throws Exception {
        X509Name name = new X509Name(dn.trim());
        this.x509 = name;
        if (this.dnrules != null) {
            name.setRules(this.dnrules);
            this.x509.setRules(this.dnrules);
        }
        ByteArrayOutputStream bOut = new ByteArrayOutputStream();
        DEROutputStream dOut = new DEROutputStream(bOut);
        dOut.writeObject(name.getDERObject());
        byte[] dnb = bOut.toByteArray();
        dOut.close();
        return new X500Principal(dnb).toString().trim();
    }

    public static void main(String[] args) throws Exception {
        Map regRuleClass = new HashMap();
        regRuleClass.put("O", "UTF8String");
        regRuleClass.put("OU", "IA5String");
        regRuleClass.put("CN", "PrintableString");
        new DNTool();
        DNTool dn = new DNTool("ou=#123\\,,c=cn,o=jit,cn=abc");
        System.out.println("source dn:" + "ou=#123\\,,c=cn,o=jit,cn=abc");
        System.out.println("this is new4 of rules:");
        System.out.println(dn.DNConvert("ou=#123\\,,c=cn,o=jit,cn=abc"));
        System.out.println("this is sun:");
        System.out.println(dn.convertSun("ou=#123\\,,c=cn,o=jit,cn=abc"));
    }
}
