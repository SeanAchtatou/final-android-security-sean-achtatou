package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.ASN1OctetString;
import cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERIA5String;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.DERTaggedObject;
import cn.com.jit.ida.util.pki.asn1.DERUTF8String;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

public class GeneralName implements DEREncodable {
    public static final String OTHER_NAME_UPN_OID = "1.3.6.1.4.1.311.20.2.3";
    public static final int uniformResourceIdentifier = 6;
    boolean isInsideImplicit = false;
    DEREncodable obj;
    int tag;

    public GeneralName(X509Name directoryName) {
        this.obj = directoryName;
        this.tag = 4;
    }

    public GeneralName(DERObject name, int tag2) {
        this.obj = name;
        this.tag = tag2;
    }

    public static GeneralName getInstance(Object obj2) {
        if (obj2 == null || (obj2 instanceof GeneralName)) {
            return (GeneralName) obj2;
        }
        if (obj2 instanceof ASN1TaggedObject) {
            ASN1TaggedObject tagObj = (ASN1TaggedObject) obj2;
            int tag2 = tagObj.getTagNo();
            switch (tag2) {
                case 0:
                    return new GeneralName(tagObj.getObject(), tag2);
                case 1:
                    return new GeneralName(DERIA5String.getInstance(tagObj, false), tag2);
                case 2:
                    return new GeneralName(DERIA5String.getInstance(tagObj, false), tag2);
                case 3:
                    throw new IllegalArgumentException("unknown tag: " + tag2);
                case 4:
                    return new GeneralName(tagObj.getObject(), tag2);
                case 5:
                    return new GeneralName(tagObj.getObject(), tag2);
                case 6:
                    return new GeneralName(DERIA5String.getInstance(tagObj, false), tag2);
                case 7:
                    return new GeneralName(ASN1OctetString.getInstance(tagObj, false), tag2);
                case 8:
                    return new GeneralName(DERObjectIdentifier.getInstance(tagObj, false), tag2);
            }
        }
        throw new IllegalArgumentException("unknown object in getInstance");
    }

    public static GeneralName getInstance(ASN1TaggedObject tagObj, boolean explicit) {
        return getInstance(ASN1TaggedObject.getInstance(tagObj, explicit));
    }

    public void markInsideImplicit(boolean isInsideImplicit2) {
        this.isInsideImplicit = isInsideImplicit2;
    }

    public int getTagNo() {
        return this.tag;
    }

    public DEREncodable getName() {
        return this.obj;
    }

    public String toString() {
        return GeneralNameToString((DERTaggedObject) getDERObject());
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v2, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v5, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v7, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v8, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v9, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v10, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String GeneralNameToString(cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject r7) {
        /*
            r6 = 4
            r1 = 0
            int r5 = r7.getTagNo()
            switch(r5) {
                case 0: goto L_0x000a;
                case 1: goto L_0x0071;
                case 2: goto L_0x0071;
                case 3: goto L_0x0009;
                case 4: goto L_0x00e3;
                case 5: goto L_0x0009;
                case 6: goto L_0x0071;
                case 7: goto L_0x007e;
                case 8: goto L_0x00d5;
                default: goto L_0x0009;
            }
        L_0x0009:
            return r1
        L_0x000a:
            java.lang.String r5 = "1.3.6.1.4.1.311.20.2.3"
            cn.com.jit.ida.util.pki.asn1.DERObject r6 = r7.getObject()
            cn.com.jit.ida.util.pki.asn1.x509.OtherName r6 = cn.com.jit.ida.util.pki.asn1.x509.OtherName.getInstance(r6)
            cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier r6 = r6.getTypeID()
            java.lang.String r6 = r6.getId()
            boolean r5 = r5.equals(r6)
            if (r5 != 0) goto L_0x003a
            java.lang.String r5 = "1.2.156.1995.1999.2"
            cn.com.jit.ida.util.pki.asn1.DERObject r6 = r7.getObject()
            cn.com.jit.ida.util.pki.asn1.x509.OtherName r6 = cn.com.jit.ida.util.pki.asn1.x509.OtherName.getInstance(r6)
            cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier r6 = r6.getTypeID()
            java.lang.String r6 = r6.getId()
            boolean r5 = r5.equals(r6)
            if (r5 == 0) goto L_0x004f
        L_0x003a:
            cn.com.jit.ida.util.pki.asn1.DERObject r5 = r7.getObject()
            cn.com.jit.ida.util.pki.asn1.x509.OtherName r5 = cn.com.jit.ida.util.pki.asn1.x509.OtherName.getInstance(r5)
            cn.com.jit.ida.util.pki.asn1.DEREncodable r5 = r5.getValue()
            cn.com.jit.ida.util.pki.asn1.DERUTF8String r5 = (cn.com.jit.ida.util.pki.asn1.DERUTF8String) r5
            cn.com.jit.ida.util.pki.asn1.DERUTF8String r5 = (cn.com.jit.ida.util.pki.asn1.DERUTF8String) r5
            java.lang.String r1 = r5.getString()
            goto L_0x0009
        L_0x004f:
            java.lang.String r1 = new java.lang.String
            cn.com.jit.ida.util.pki.asn1.DERObject r5 = r7.getObject()
            cn.com.jit.ida.util.pki.asn1.x509.OtherName r5 = cn.com.jit.ida.util.pki.asn1.x509.OtherName.getInstance(r5)
            cn.com.jit.ida.util.pki.asn1.DEREncodable r5 = r5.getValue()
            cn.com.jit.ida.util.pki.asn1.DERObject r5 = r5.getDERObject()
            cn.com.jit.ida.util.pki.asn1.DEROctetString r5 = (cn.com.jit.ida.util.pki.asn1.DEROctetString) r5
            cn.com.jit.ida.util.pki.asn1.DEROctetString r5 = (cn.com.jit.ida.util.pki.asn1.DEROctetString) r5
            byte[] r5 = r5.getOctets()
            byte[] r5 = cn.com.jit.ida.util.pki.encoders.Hex.encode(r5)
            r1.<init>(r5)
            goto L_0x0009
        L_0x0071:
            cn.com.jit.ida.util.pki.asn1.DERObject r5 = r7.getObject()
            cn.com.jit.ida.util.pki.asn1.DERIA5String r5 = cn.com.jit.ida.util.pki.asn1.DERIA5String.getInstance(r5)
            java.lang.String r1 = r5.getString()
            goto L_0x0009
        L_0x007e:
            java.lang.StringBuffer r4 = new java.lang.StringBuffer
            r4.<init>()
            cn.com.jit.ida.util.pki.asn1.DERObject r5 = r7.getObject()
            cn.com.jit.ida.util.pki.asn1.DEROctetString r5 = (cn.com.jit.ida.util.pki.asn1.DEROctetString) r5
            byte[] r2 = r5.getOctets()
            r3 = 0
            r0 = 0
        L_0x008f:
            if (r0 >= r6) goto L_0x00a9
            byte r3 = r2[r0]
            if (r3 >= 0) goto L_0x0097
            int r3 = r3 + 256
        L_0x0097:
            java.lang.String r5 = java.lang.String.valueOf(r3)
            r4.append(r5)
            r5 = 3
            if (r0 >= r5) goto L_0x00a6
            java.lang.String r5 = "."
            r4.append(r5)
        L_0x00a6:
            int r0 = r0 + 1
            goto L_0x008f
        L_0x00a9:
            int r5 = r2.length
            if (r5 <= r6) goto L_0x00cf
            java.lang.String r5 = "/"
            r4.append(r5)
            r0 = 4
        L_0x00b2:
            int r5 = r2.length
            if (r0 >= r5) goto L_0x00cf
            byte r3 = r2[r0]
            if (r3 >= 0) goto L_0x00bb
            int r3 = r3 + 256
        L_0x00bb:
            java.lang.String r5 = java.lang.String.valueOf(r3)
            r4.append(r5)
            int r5 = r2.length
            int r5 = r5 + -1
            if (r0 >= r5) goto L_0x00cc
            java.lang.String r5 = "."
            r4.append(r5)
        L_0x00cc:
            int r0 = r0 + 1
            goto L_0x00b2
        L_0x00cf:
            java.lang.String r1 = r4.toString()
            goto L_0x0009
        L_0x00d5:
            cn.com.jit.ida.util.pki.asn1.DERObject r5 = r7.getObject()
            cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier r5 = cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier.getInstance(r5)
            java.lang.String r1 = r5.getId()
            goto L_0x0009
        L_0x00e3:
            cn.com.jit.ida.util.pki.asn1.DERObject r5 = r7.getObject()
            cn.com.jit.ida.util.pki.asn1.x509.X509Name r5 = cn.com.jit.ida.util.pki.asn1.x509.X509Name.getInstance(r5)
            java.lang.String r1 = r5.toString()
            goto L_0x0009
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.com.jit.ida.util.pki.asn1.x509.GeneralName.GeneralNameToString(cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject):java.lang.String");
    }

    public DERObject getDERObject() {
        if (this.tag == 4) {
            return new DERTaggedObject(true, this.tag, this.obj);
        }
        return new DERTaggedObject(false, this.tag, this.obj);
    }

    public GeneralName(Node cnode) throws PKIException {
        String type = cnode.getAttributes().item(0).getNodeValue();
        try {
            if (type.equals("4")) {
                this.obj = new X509Name(((Text) cnode.getFirstChild()).getData()).getDERObject();
                this.tag = 4;
            }
            if (type.equals("7")) {
                this.obj = new DEROctetString(((Text) cnode.getFirstChild()).getData().getBytes()).getDERObject();
                this.tag = 7;
            }
            if (type.equals("6")) {
                this.obj = new DERIA5String(((Text) cnode.getFirstChild()).getData()).getDERObject();
                this.tag = 6;
            }
            if (type.equals("2")) {
                this.obj = new DERIA5String(((Text) cnode.getFirstChild()).getData()).getDERObject();
                this.tag = 2;
            }
            if (type.equals("0")) {
                this.obj = new OtherName(new DERObjectIdentifier("1.3.6.1.4.1.311.20.2.3"), new DERTaggedObject(true, 0, new DERUTF8String(((Text) cnode.getFirstChild()).getData().trim()))).getDERObject();
                this.tag = 0;
            }
        } catch (Exception e) {
            throw new PKIException("GeneralName构造函数错误", e);
        }
    }
}
