package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.ASN1Encodable;
import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DERSequence;

public class AccessDescription extends ASN1Encodable {
    GeneralName accessLocation;
    DERObjectIdentifier accessMethod;

    public AccessDescription() {
        this.accessMethod = null;
        this.accessLocation = null;
        this.accessMethod = null;
        this.accessLocation = null;
    }

    public AccessDescription(ASN1Sequence seq) {
        this.accessMethod = null;
        this.accessLocation = null;
        this.accessMethod = (DERObjectIdentifier) seq.getObjectAt(0);
        this.accessLocation = (GeneralName) seq.getObjectAt(1);
    }

    public DERObject toASN1Object() {
        ASN1EncodableVector accessDescription = new ASN1EncodableVector();
        accessDescription.add(this.accessMethod);
        accessDescription.add(this.accessLocation);
        return new DERSequence(accessDescription);
    }

    public GeneralName getAccessLocation() {
        return this.accessLocation;
    }

    public DERObjectIdentifier getAccessMethod() {
        return this.accessMethod;
    }

    public void setAccessLocation(GeneralName accessLocation2) {
        this.accessLocation = accessLocation2;
    }

    public void setAccessMethod(DERObjectIdentifier accessMethod2) {
        this.accessMethod = accessMethod2;
    }
}
