package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERGeneralizedTime;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERUTCTime;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.SimpleTimeZone;

public class Time implements DEREncodable {
    DERObject time;

    public static Time getInstance(ASN1TaggedObject obj, boolean explicit) {
        return getInstance(obj.getObject());
    }

    public Time(DERObject time2) {
        if ((time2 instanceof DERUTCTime) || (time2 instanceof DERGeneralizedTime)) {
            this.time = time2;
            return;
        }
        throw new IllegalArgumentException("unknown object passed to Time");
    }

    public Time(Date date) {
        SimpleTimeZone tz = new SimpleTimeZone(0, "Z");
        SimpleDateFormat dateF = new SimpleDateFormat("yyyyMMddHHmmss");
        dateF.setTimeZone(tz);
        String d = dateF.format(date) + "Z";
        int year = Integer.parseInt(d.substring(0, 4));
        if (year < 1950 || year > 2049) {
            this.time = new DERGeneralizedTime(d);
        } else {
            this.time = new DERUTCTime(d.substring(2));
        }
    }

    public static Time getInstance(Object obj) {
        if (obj instanceof Time) {
            return (Time) obj;
        }
        if (obj instanceof DERUTCTime) {
            return new Time((DERUTCTime) obj);
        }
        if (obj instanceof DERGeneralizedTime) {
            return new Time((DERGeneralizedTime) obj);
        }
        throw new IllegalArgumentException("unknown object in factory");
    }

    public String getTime() {
        if (this.time instanceof DERUTCTime) {
            return ((DERUTCTime) this.time).getAdjustedTime();
        }
        return ((DERGeneralizedTime) this.time).getTime();
    }

    public Date getDate() {
        return new SimpleDateFormat("yyyyMMddHHmmssz").parse(getTime(), new ParsePosition(0));
    }

    public DERObject getDERObject() {
        return this.time;
    }
}
