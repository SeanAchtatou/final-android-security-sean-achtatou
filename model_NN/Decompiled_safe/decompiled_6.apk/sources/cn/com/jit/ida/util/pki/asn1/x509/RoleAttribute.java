package cn.com.jit.ida.util.pki.asn1.x509;

public class RoleAttribute extends Attribute {
    public RoleAttribute() {
        setTttrType(Attribute.ROLE_ATTRIBUTE_OID);
    }

    public RoleSyntax createRoleSyntax() {
        return new RoleSyntax();
    }

    public void addRoleSyntax(RoleSyntax obj) {
        addAttValueObject(obj.getDERObject());
    }
}
