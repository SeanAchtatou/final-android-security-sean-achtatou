package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject;
import cn.com.jit.ida.util.pki.asn1.DERBoolean;
import cn.com.jit.ida.util.pki.asn1.DERObject;

public class IssuingDistributionPoint {
    private boolean indirectCRL;
    private boolean onlyContainsAttributeCerts;
    private boolean onlyContainsCACerts;
    private boolean onlyContainsUserCerts;
    private ASN1Sequence seq;

    public static IssuingDistributionPoint getInstance(ASN1TaggedObject obj, boolean explicit) {
        return getInstance(ASN1Sequence.getInstance(obj, explicit));
    }

    public static IssuingDistributionPoint getInstance(Object obj) {
        if (obj == null || (obj instanceof IssuingDistributionPoint)) {
            return (IssuingDistributionPoint) obj;
        }
        if (obj instanceof ASN1Sequence) {
            return new IssuingDistributionPoint((ASN1Sequence) obj);
        }
        throw new IllegalArgumentException("unknown object in factory");
    }

    public IssuingDistributionPoint(ASN1Sequence seq2) {
        this.seq = seq2;
        for (int i = 0; i != seq2.size(); i++) {
            ASN1TaggedObject o = (ASN1TaggedObject) seq2.getObjectAt(i);
            switch (o.getTagNo()) {
                case 0:
                case 3:
                case 1:
                    this.onlyContainsUserCerts = DERBoolean.getInstance(o, false).isTrue();
                    continue;
                case 2:
                    this.onlyContainsCACerts = DERBoolean.getInstance(o, false).isTrue();
                    continue;
                case 4:
                    this.indirectCRL = DERBoolean.getInstance(o, false).isTrue();
                    break;
                case 5:
                    break;
                default:
                    throw new IllegalArgumentException("unknown tag in IssuingDistributionPoint");
            }
            this.onlyContainsAttributeCerts = DERBoolean.getInstance(o, false).isTrue();
        }
    }

    public boolean onlyContainsUserCerts() {
        return this.onlyContainsUserCerts;
    }

    public boolean onlyContainsCACerts() {
        return this.onlyContainsCACerts;
    }

    public boolean isIndirectCRL() {
        return this.indirectCRL;
    }

    public boolean onlyContainsAttributeCerts() {
        return this.onlyContainsAttributeCerts;
    }

    public DERObject getDERObject() {
        return this.seq;
    }
}
