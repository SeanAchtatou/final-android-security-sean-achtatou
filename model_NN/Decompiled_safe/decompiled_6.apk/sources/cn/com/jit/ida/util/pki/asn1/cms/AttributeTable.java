package cn.com.jit.ida.util.pki.asn1.cms;

import cn.com.jit.ida.util.pki.asn1.ASN1Set;
import cn.com.jit.ida.util.pki.asn1.DEREncodableVector;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import java.util.Hashtable;

public class AttributeTable {
    private Hashtable attributes = new Hashtable();

    public AttributeTable(Hashtable attrs) {
        this.attributes = new Hashtable(attrs);
    }

    public AttributeTable(DEREncodableVector v) {
        for (int i = 0; i != v.size(); i++) {
            Attribute a = Attribute.getInstance(v.get(i));
            this.attributes.put(a.getAttrType(), a);
        }
    }

    public AttributeTable(ASN1Set s) {
        for (int i = 0; i != s.size(); i++) {
            Attribute a = Attribute.getInstance(s.getObjectAt(i));
            this.attributes.put(a.getAttrType(), a);
        }
    }

    public Attribute get(DERObjectIdentifier oid) {
        return (Attribute) this.attributes.get(oid);
    }

    public Hashtable toHashtable() {
        return new Hashtable(this.attributes);
    }
}
