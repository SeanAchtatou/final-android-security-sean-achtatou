package cn.com.jit.ida.util.pki.asn1.pkcs;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1Set;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DERSequence;

public class Attribute implements DEREncodable {
    private DERObjectIdentifier attrType;
    private ASN1Set attrValues;

    public static Attribute getInstance(Object o) {
        if (o == null || (o instanceof Attribute)) {
            return (Attribute) o;
        }
        if (o instanceof ASN1Sequence) {
            return new Attribute((ASN1Sequence) o);
        }
        throw new IllegalArgumentException("unknown object in factory");
    }

    public Attribute(ASN1Sequence seq) {
        this.attrType = (DERObjectIdentifier) seq.getObjectAt(0);
        this.attrValues = (ASN1Set) seq.getObjectAt(1);
    }

    public Attribute(DERObjectIdentifier attrType2, ASN1Set attrValues2) {
        this.attrType = attrType2;
        this.attrValues = attrValues2;
    }

    public DERObjectIdentifier getAttrType() {
        return this.attrType;
    }

    public ASN1Set getAttrValues() {
        return this.attrValues;
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(this.attrType);
        v.add(this.attrValues);
        return new DERSequence(v);
    }
}
