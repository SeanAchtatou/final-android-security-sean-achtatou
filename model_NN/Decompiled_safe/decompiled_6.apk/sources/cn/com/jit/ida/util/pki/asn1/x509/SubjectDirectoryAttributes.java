package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.pkcs.Attribute;

public class SubjectDirectoryAttributes implements DEREncodable {
    private Attribute[] attributes = null;

    public SubjectDirectoryAttributes(ASN1Sequence seq) {
        this.attributes = new Attribute[seq.size()];
        for (int i = 0; i < this.attributes.length; i++) {
            this.attributes[i] = new Attribute((ASN1Sequence) seq.getObjectAt(i));
        }
    }

    public SubjectDirectoryAttributes(Attribute[] attributes2) {
        this.attributes = attributes2;
    }

    public Attribute[] getAttributes() {
        return this.attributes;
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        for (Attribute dERObject : this.attributes) {
            v.add(dERObject.getDERObject());
        }
        return new DERSequence(v);
    }
}
