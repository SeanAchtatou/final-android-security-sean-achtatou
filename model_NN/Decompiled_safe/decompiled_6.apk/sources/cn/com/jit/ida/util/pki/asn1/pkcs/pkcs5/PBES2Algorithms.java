package cn.com.jit.ida.util.pki.asn1.pkcs.pkcs5;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.pkcs.PKCSObjectIdentifiers;
import cn.com.jit.ida.util.pki.asn1.x509.AlgorithmIdentifier;
import java.util.Enumeration;

public class PBES2Algorithms extends AlgorithmIdentifier implements PKCSObjectIdentifiers {
    private KeyDerivationFunc func;
    private DERObjectIdentifier objectId;
    private EncryptionScheme scheme;

    public PBES2Algorithms(ASN1Sequence obj) {
        super(obj);
        Enumeration e = obj.getObjects();
        this.objectId = (DERObjectIdentifier) e.nextElement();
        Enumeration e2 = ((ASN1Sequence) e.nextElement()).getObjects();
        ASN1Sequence funcSeq = (ASN1Sequence) e2.nextElement();
        if (funcSeq.getObjectAt(0).equals(id_PBKDF2)) {
            this.func = new PBKDF2Params(funcSeq);
        } else {
            this.func = new KeyDerivationFunc(funcSeq);
        }
        this.scheme = new EncryptionScheme((ASN1Sequence) e2.nextElement());
    }

    public DERObjectIdentifier getObjectId() {
        return this.objectId;
    }

    public KeyDerivationFunc getKeyDerivationFunc() {
        return this.func;
    }

    public EncryptionScheme getEncryptionScheme() {
        return this.scheme;
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        ASN1EncodableVector subV = new ASN1EncodableVector();
        v.add(this.objectId);
        subV.add(this.func);
        subV.add(this.scheme);
        v.add(new DERSequence(subV));
        return new DERSequence(v);
    }
}
