package cn.com.jit.ida.util.pki.asn1;

import java.io.IOException;

public abstract class ASN1Null extends DERObject {
    /* access modifiers changed from: package-private */
    public abstract void encode(DEROutputStream dEROutputStream) throws IOException;

    public int hashCode() {
        return 0;
    }

    public boolean equals(Object o) {
        if (o == null || !(o instanceof ASN1Null)) {
            return false;
        }
        return true;
    }
}
