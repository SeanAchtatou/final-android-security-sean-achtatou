package cn.com.jit.ida.util.pki;

import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DEREncodableVector;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DEROutputStream;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.pkcs.PKCSObjectIdentifiers;
import cn.com.jit.ida.util.pki.asn1.pkcs.RSAPrivateKeyStructure;
import cn.com.jit.ida.util.pki.asn1.x509.CertificateList;
import cn.com.jit.ida.util.pki.asn1.x509.RSAPublicKeyStructure;
import cn.com.jit.ida.util.pki.asn1.x509.X509CertificateStructure;
import cn.com.jit.ida.util.pki.cert.X509Cert;
import cn.com.jit.ida.util.pki.cipher.JKey;
import cn.com.jit.ida.util.pki.cipher.Mechanism;
import cn.com.jit.ida.util.pki.encoders.Hex;
import com.jianq.misc.StringEx;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.Key;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Vector;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.asn1.x9.X9ObjectIdentifiers;

public class Parser {
    private static final byte[] b64Code = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47, 61, 13, 10};

    public static byte[] customData2SoftPublicKey(byte[] eccPublicKey, byte[] ecParams) throws PKIException {
        return ECDSAParser.customData2SoftPublicKey(eccPublicKey, ecParams);
    }

    public static byte[] customDSAData2SoftPublicKey(byte[] dsaPublicKey) throws PKIException {
        return new SubjectPublicKeyInfo(new AlgorithmIdentifier(X9ObjectIdentifiers.id_dsa, (DEREncodable) null), dsaPublicKey).getDEREncoded();
    }

    public static SecretKey convertSecretKey(JKey jkey) throws PKIException {
        try {
            if (!jkey.getKeyType().equals("PBEWithMD5AndDES") && !jkey.getKeyType().equals("PBEWITHSHAAND2-KEYTRIPLEDES-CBC") && !jkey.getKeyType().equals("PBEWITHSHAAND3-KEYTRIPLEDES-CBC")) {
                return new SecretKeySpec(jkey.getKey(), jkey.getKeyType());
            }
            return SecretKeyFactory.getInstance(jkey.getKeyType(), "BC").generateSecret(new PBEKeySpec(new String(jkey.getKey()).toCharArray()));
        } catch (Exception ex) {
            throw new PKIException("8131", PKIException.COV_SYM_KEY_DES, ex);
        }
    }

    public static X509Certificate convertX509Cert2JavaCert(X509Cert cert) {
        try {
            return (X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(cert.getEncoded()));
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static PrivateKey convertPrivateKey(JKey jkey) throws PKIException {
        String keyType = jkey.getKeyType();
        if (keyType.equals(JKey.RSA_PRV_KEY) || keyType.equals(JKey.DSA_PRV_KEY) || keyType.equals(JKey.ECIES_PRV_KEY) || keyType.equals(JKey.SM2_PRV_KEY) || keyType.equals(JKey.ECDSA_PRV_KEY)) {
            try {
                String alg = keyType.substring(0, keyType.indexOf("_"));
                if (!Mechanism.ECIES.equals(alg) && !Mechanism.ECDSA.equals(alg) && !Mechanism.SM2.equals(alg)) {
                    return KeyFactory.getInstance(alg, "BC").generatePrivate(new PKCS8EncodedKeySpec(jkey.getKey()));
                }
                new PrivateKeyInfo(new ASN1InputStream(new ByteArrayInputStream(jkey.getKey())).readObject());
                return KeyFactory.getInstance(alg, "BC").generatePrivate(new PKCS8EncodedKeySpec(jkey.getKey()));
            } catch (Exception ex) {
                throw new PKIException("8132", PKIException.COV_PRV_KEY_DES, ex);
            }
        } else {
            throw new PKIException("8132", "私钥转换失败 密钥类型不合法 " + keyType);
        }
    }

    public static PublicKey convertPublicKey(JKey jkey) throws PKIException {
        String keyType = jkey.getKeyType();
        if (keyType.equals(JKey.RSA_PUB_KEY) || keyType.equals(JKey.DSA_PUB_KEY) || keyType.equals(JKey.ECIES_PUB_KEY) || keyType.equals(JKey.ECDSA_PUB_KEY)) {
            try {
                String alg = keyType.substring(0, keyType.indexOf("_"));
                if (!Mechanism.ECIES.equals(alg) && !Mechanism.ECDSA.equals(alg)) {
                    return KeyFactory.getInstance(alg, "BC").generatePublic(new X509EncodedKeySpec(jkey.getKey()));
                }
                return KeyFactory.getInstance(alg, "BC").generatePublic(new X509EncodedKeySpec(jkey.getKey()));
            } catch (Exception ex) {
                throw new PKIException("8133", PKIException.COV_PUB_KEY_DES, ex);
            }
        } else {
            throw new PKIException("8133", "公钥转换失败 密钥类型不合法 " + keyType);
        }
    }

    public static Key convertKey(JKey jkey) throws PKIException {
        String keyType = jkey.getKeyType();
        if (keyType.equals("DES") || keyType.equals("DESede") || keyType.equals("RC2") || keyType.equals("RC4") || keyType.equals("CAST5") || keyType.equals("IDEA") || keyType.equals("AES") || keyType.equals("PBEWithMD5AndDES") || keyType.equals("PBEWITHSHAAND2-KEYTRIPLEDES-CBC") || keyType.equals("PBEWITHSHAAND3-KEYTRIPLEDES-CBC")) {
            return convertSecretKey(jkey);
        }
        if (keyType.equals(JKey.RSA_PRV_KEY) || keyType.equals(JKey.DSA_PRV_KEY) || keyType.equals(JKey.ECIES_PRV_KEY) || keyType.equals(JKey.ECDSA_PRV_KEY)) {
            return convertPrivateKey(jkey);
        }
        if (keyType.equals(JKey.RSA_PUB_KEY) || keyType.equals(JKey.DSA_PUB_KEY) || keyType.equals(JKey.ECIES_PUB_KEY) || keyType.equals(JKey.ECDSA_PUB_KEY)) {
            return convertPublicKey(jkey);
        }
        throw new PKIException("8130", "密钥转换操作失败 密钥类型不合法 " + keyType);
    }

    public static cn.com.jit.ida.util.pki.asn1.x509.SubjectPublicKeyInfo key2SPKI(JKey key) throws PKIException {
        String keyType = key.getKeyType();
        if (keyType.equals(JKey.RSA_PUB_KEY) || keyType.equals(JKey.DSA_PUB_KEY) || keyType.equals(JKey.ECIES_PUB_KEY) || keyType.equals(JKey.SM2_PUB_KEY) || keyType.equals(JKey.ECDSA_PUB_KEY)) {
            try {
                return new cn.com.jit.ida.util.pki.asn1.x509.SubjectPublicKeyInfo((ASN1Sequence) new cn.com.jit.ida.util.pki.asn1.ASN1InputStream(new ByteArrayInputStream(key.getKey())).readObject());
            } catch (Exception ex) {
                ex.printStackTrace();
                throw new PKIException("8134", PKIException.KEY_SPKI_DES, ex);
            }
        } else {
            throw new PKIException("8134", "公钥转换为主题公钥信息失败 密钥类型不合法 " + keyType);
        }
    }

    public static JKey SPKI2Key(cn.com.jit.ida.util.pki.asn1.x509.SubjectPublicKeyInfo spki) throws PKIException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            new DEROutputStream(bos).writeObject(spki.getDERObject());
            byte[] key = bos.toByteArray();
            if (spki.getAlgorithmId().getObjectId().equals(PKCSObjectIdentifiers.rsaEncryption)) {
                return new JKey(JKey.RSA_PUB_KEY, key);
            }
            if (spki.getAlgorithmId().getObjectId().equals(PKCSObjectIdentifiers.ecEncryption)) {
                if (spki.getAlgorithmId().getParameters() != null) {
                    return new JKey(JKey.SM2_PUB_KEY, key);
                }
                return new JKey(JKey.ECDSA_PUB_KEY, key);
            } else if (spki.getAlgorithmId().getObjectId().equals(PKCSObjectIdentifiers.ecEncryption)) {
                return new JKey(JKey.ECIES_PUB_KEY, key);
            } else {
                if (spki.getAlgorithmId().getObjectId().equals(PKCSObjectIdentifiers.id_SM2PublicKey)) {
                    return new JKey(JKey.SM2_PUB_KEY, key);
                }
                return null;
            }
        } catch (Exception ex) {
            throw new PKIException("8135", PKIException.SPKI_KEY_DES, ex);
        }
    }

    public static byte[] hardKey2SoftKey(String keyType, byte[] hardKey) throws PKIException {
        byte[] key;
        if (keyType.equals(JKey.RSA_PUB_KEY)) {
            try {
                try {
                    key = writeDERObj2Bytes(new cn.com.jit.ida.util.pki.asn1.x509.SubjectPublicKeyInfo(new cn.com.jit.ida.util.pki.asn1.x509.AlgorithmIdentifier(PKCSObjectIdentifiers.rsaEncryption, null), hardKey));
                } catch (Exception e) {
                    ex = e;
                    throw new PKIException("8138", PKIException.COV_HARD_SOFT_KEY_DES, ex);
                }
            } catch (Exception e2) {
                ex = e2;
                throw new PKIException("8138", PKIException.COV_HARD_SOFT_KEY_DES, ex);
            }
        } else if (keyType.equals(JKey.RSA_PRV_KEY)) {
            try {
                try {
                    key = writeDERObj2Bytes(new cn.com.jit.ida.util.pki.asn1.pkcs.pkcs8.PrivateKeyInfo(new cn.com.jit.ida.util.pki.asn1.x509.AlgorithmIdentifier(PKCSObjectIdentifiers.rsaEncryption, null), new RSAPrivateKeyStructure((ASN1Sequence) writeBytes2DERObj(hardKey)).getDERObject()));
                } catch (Exception e3) {
                    ex = e3;
                    throw new PKIException("8138", PKIException.COV_HARD_SOFT_KEY_DES, ex);
                }
            } catch (Exception e4) {
                ex = e4;
                throw new PKIException("8138", PKIException.COV_HARD_SOFT_KEY_DES, ex);
            }
        } else {
            throw new PKIException("8138", "加密机密钥转换软库密钥失败 密钥类型不合法");
        }
        return key;
    }

    public static byte[] softKey2HardKey(String keyType, byte[] softKey) throws PKIException {
        if (keyType.equals(JKey.RSA_PUB_KEY)) {
            try {
                return new cn.com.jit.ida.util.pki.asn1.x509.SubjectPublicKeyInfo((ASN1Sequence) writeBytes2DERObj(softKey)).getPublicKeyData().getBytes();
            } catch (Exception ex) {
                throw new PKIException("8139", PKIException.COV_SOFT_HARD_KEY_DES, ex);
            }
        } else if (keyType.equals(JKey.RSA_PRV_KEY)) {
            try {
                return writeDERObj2Bytes(new RSAPrivateKeyStructure((ASN1Sequence) new cn.com.jit.ida.util.pki.asn1.pkcs.pkcs8.PrivateKeyInfo((ASN1Sequence) writeBytes2DERObj(softKey)).getPrivateKey()));
            } catch (Exception ex2) {
                throw new PKIException("8139", PKIException.COV_SOFT_HARD_KEY_DES, ex2);
            }
        } else {
            throw new PKIException("8139", "软库密钥转换加密机密钥失败 密钥类型不合法");
        }
    }

    public static byte[] softKey2HardCustomKey(String keyType, byte[] softKey) throws PKIException {
        byte[] modulus;
        byte[] privateExponent;
        byte[] publicExponent;
        byte[] prime1;
        byte[] prime2;
        byte[] exponent1;
        byte[] exponent2;
        byte[] coefficient;
        byte[] modulus2;
        byte[] publicExponent2;
        if (keyType.equals(JKey.ECDSA_PUB_KEY)) {
            return ECDSAParser.softKey2HardCustomKey(softKey);
        }
        if (keyType.equals(JKey.DSA_PUB_KEY)) {
            return DSAParser.softKey2HardCustomKey(softKey);
        }
        if (keyType.equals(JKey.RSA_PUB_KEY)) {
            byte[] modulus3 = null;
            byte[] publicExponent3 = null;
            try {
                RSAPublicKeyStructure pubKeyStructure = RSAPublicKeyStructure.getInstance(new cn.com.jit.ida.util.pki.asn1.x509.SubjectPublicKeyInfo((ASN1Sequence) writeBytes2DERObj(softKey)).getPublicKey());
                modulus3 = pubKeyStructure.getModulus().toByteArray();
                publicExponent3 = pubKeyStructure.getPublicExponent().toByteArray();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            if (modulus3[0] == 0) {
                byte[] modulus1 = new byte[(modulus3.length - 1)];
                modulus1[0] = 0;
                System.arraycopy(modulus3, 1, modulus1, 0, modulus3.length - 1);
                modulus2 = Hex.encode(modulus1);
            } else {
                modulus2 = Hex.encode(modulus3);
            }
            if (publicExponent3[0] == 0) {
                byte[] publicExponent1 = new byte[(publicExponent3.length - 1)];
                publicExponent1[0] = 0;
                System.arraycopy(publicExponent3, 1, publicExponent1, 0, publicExponent3.length - 1);
                publicExponent2 = Hex.encode(publicExponent1);
            } else {
                publicExponent2 = Hex.encode(publicExponent3);
            }
            byte[] key = new byte[(modulus2.length + 12 + publicExponent2.length)];
            System.arraycopy("2".getBytes(), 0, key, 0, "2".getBytes().length);
            byte[] modulusLen = Integer.toString(modulus2.length).getBytes();
            byte[] publicExponentLen = Integer.toString(publicExponent2.length).getBytes();
            System.arraycopy(modulusLen, 0, key, 2, modulusLen.length);
            System.arraycopy(modulus2, 0, key, 7, modulus2.length);
            System.arraycopy(publicExponentLen, 0, key, modulus2.length + 7, publicExponentLen.length);
            System.arraycopy(publicExponent2, 0, key, modulus2.length + 12, publicExponent2.length);
            return key;
        } else if (keyType.equals(JKey.RSA_PRV_KEY)) {
            RSAPrivateKeyStructure rSAPrivateKeyStructure = new RSAPrivateKeyStructure((ASN1Sequence) new cn.com.jit.ida.util.pki.asn1.pkcs.pkcs8.PrivateKeyInfo((ASN1Sequence) writeBytes2DERObj(softKey)).getPrivateKey());
            byte[] modulus4 = rSAPrivateKeyStructure.getModulus().toByteArray();
            byte[] privateExponent2 = rSAPrivateKeyStructure.getPrivateExponent().toByteArray();
            byte[] publicExponent4 = rSAPrivateKeyStructure.getPublicExponent().toByteArray();
            byte[] prime12 = rSAPrivateKeyStructure.getPrime1().toByteArray();
            byte[] prime22 = rSAPrivateKeyStructure.getPrime2().toByteArray();
            byte[] exponent12 = rSAPrivateKeyStructure.getExponent1().toByteArray();
            byte[] exponent22 = rSAPrivateKeyStructure.getExponent2().toByteArray();
            byte[] coefficient2 = rSAPrivateKeyStructure.getCoefficient().toByteArray();
            if (modulus4[0] == 0) {
                byte[] modulus12 = new byte[(modulus4.length - 1)];
                modulus12[0] = 0;
                System.arraycopy(modulus4, 1, modulus12, 0, modulus4.length - 1);
                modulus = Hex.encode(modulus12);
            } else {
                modulus = Hex.encode(modulus4);
            }
            if (privateExponent2[0] == 0) {
                byte[] privateExponent1 = new byte[(privateExponent2.length - 1)];
                privateExponent1[0] = 0;
                System.arraycopy(privateExponent2, 1, privateExponent1, 0, privateExponent2.length - 1);
                privateExponent = Hex.encode(privateExponent1);
            } else {
                privateExponent = Hex.encode(privateExponent2);
            }
            if (publicExponent4[0] == 0) {
                byte[] publicExponent12 = new byte[(publicExponent4.length - 1)];
                publicExponent12[0] = 0;
                System.arraycopy(publicExponent4, 1, publicExponent12, 0, publicExponent4.length - 1);
                publicExponent = Hex.encode(publicExponent12);
            } else {
                publicExponent = Hex.encode(publicExponent4);
            }
            if (prime12[0] == 0) {
                byte[] prime1_1 = new byte[(prime12.length - 1)];
                prime1_1[0] = 0;
                System.arraycopy(prime12, 1, prime1_1, 0, prime12.length - 1);
                prime1 = Hex.encode(prime1_1);
            } else {
                prime1 = Hex.encode(prime12);
            }
            if (prime22[0] == 0) {
                byte[] prime2_1 = new byte[(prime22.length - 1)];
                prime2_1[0] = 0;
                System.arraycopy(prime22, 1, prime2_1, 0, prime22.length - 1);
                prime2 = Hex.encode(prime2_1);
            } else {
                prime2 = Hex.encode(prime22);
            }
            if (exponent12[0] == 0) {
                byte[] exponent1_1 = new byte[(exponent12.length - 1)];
                exponent1_1[0] = 0;
                System.arraycopy(exponent12, 1, exponent1_1, 0, exponent12.length - 1);
                exponent1 = Hex.encode(exponent1_1);
            } else {
                exponent1 = Hex.encode(exponent12);
            }
            if (exponent22[0] == 0) {
                byte[] exponent2_1 = new byte[(exponent22.length - 1)];
                exponent2_1[0] = 0;
                System.arraycopy(exponent22, 1, exponent2_1, 0, exponent22.length - 1);
                exponent2 = Hex.encode(exponent2_1);
            } else {
                exponent2 = Hex.encode(exponent22);
            }
            if (coefficient2[0] == 0) {
                byte[] coefficient1 = new byte[(coefficient2.length - 1)];
                coefficient1[0] = 0;
                System.arraycopy(coefficient2, 1, coefficient1, 0, coefficient2.length - 1);
                coefficient = Hex.encode(coefficient1);
            } else {
                coefficient = Hex.encode(coefficient2);
            }
            byte[] key2 = new byte[(modulus.length + 42 + publicExponent.length + privateExponent.length + prime1.length + prime2.length + exponent1.length + exponent2.length + coefficient.length)];
            System.arraycopy("8".getBytes(), 0, key2, 0, "8".getBytes().length);
            byte[] modulusLen2 = Integer.toString(modulus.length).getBytes();
            byte[] privateExponentLen = Integer.toString(privateExponent.length).getBytes();
            byte[] publicExponentLen2 = Integer.toString(publicExponent.length).getBytes();
            byte[] prime1Len = Integer.toString(prime1.length).getBytes();
            byte[] prime2Len = Integer.toString(prime2.length).getBytes();
            byte[] exponent1Len = Integer.toString(exponent1.length).getBytes();
            byte[] exponent2Len = Integer.toString(exponent2.length).getBytes();
            byte[] coefficientLen = Integer.toString(coefficient.length).getBytes();
            System.arraycopy(modulusLen2, 0, key2, 2, modulusLen2.length);
            System.arraycopy(modulus, 0, key2, 7, modulus.length);
            System.arraycopy(privateExponentLen, 0, key2, modulus.length + 7, privateExponentLen.length);
            System.arraycopy(privateExponent, 0, key2, modulus.length + 12, privateExponent.length);
            System.arraycopy(publicExponentLen2, 0, key2, modulus.length + 12 + privateExponent.length, publicExponentLen2.length);
            System.arraycopy(publicExponent, 0, key2, modulus.length + 17 + privateExponent.length, publicExponent.length);
            System.arraycopy(prime1Len, 0, key2, modulus.length + 17 + privateExponent.length + publicExponent.length, prime1Len.length);
            System.arraycopy(prime1, 0, key2, modulus.length + 22 + privateExponent.length + publicExponent.length, prime1.length);
            System.arraycopy(prime2Len, 0, key2, modulus.length + 22 + privateExponent.length + publicExponent.length + prime1.length, prime2Len.length);
            System.arraycopy(prime2, 0, key2, modulus.length + 27 + privateExponent.length + publicExponent.length + prime1.length, prime2.length);
            System.arraycopy(exponent1Len, 0, key2, modulus.length + 27 + privateExponent.length + publicExponent.length + prime1.length + prime2.length, exponent1Len.length);
            System.arraycopy(exponent1, 0, key2, modulus.length + 32 + privateExponent.length + publicExponent.length + prime1.length + prime2.length, exponent1.length);
            System.arraycopy(exponent2Len, 0, key2, modulus.length + 32 + privateExponent.length + publicExponent.length + prime1.length + prime2.length + exponent1.length, exponent2Len.length);
            System.arraycopy(exponent2, 0, key2, modulus.length + 37 + privateExponent.length + publicExponent.length + prime1.length + prime2.length + exponent1.length, exponent2.length);
            System.arraycopy(coefficientLen, 0, key2, modulus.length + 37 + privateExponent.length + publicExponent.length + prime1.length + prime2.length + exponent1.length + exponent2.length, coefficientLen.length);
            System.arraycopy(coefficient, 0, key2, modulus.length + 42 + privateExponent.length + publicExponent.length + prime1.length + prime2.length + exponent1.length + exponent2.length, coefficient.length);
            return key2;
        } else if (keyType.equals(JKey.SM2_PRV_KEY)) {
            return ECDSAParser.sm2_softKey2HardCustomKey(softKey);
        } else {
            if (keyType.equals(JKey.SM2_PUB_KEY)) {
                return ECDSAParser.sm2_softKey2HardCustomKey(softKey);
            }
            throw new PKIException("8139", "软库密钥转换加密机密钥失败 密钥类型不合法");
        }
    }

    public static byte[] customData2SoftPublicKey(String keyType, byte[] modulus, byte[] exponent) throws PKIException {
        BigInteger b_modulus;
        BigInteger b_exponent;
        if (keyType.equals(JKey.RSA_PUB_KEY)) {
            if (modulus[0] < 0) {
                byte[] modulus1 = new byte[(modulus.length + 1)];
                modulus1[0] = 0;
                System.arraycopy(modulus, 0, modulus1, 1, modulus.length);
                b_modulus = new BigInteger(modulus1);
            } else {
                b_modulus = new BigInteger(modulus);
            }
            if (exponent[0] < 0) {
                byte[] exponent1 = new byte[(exponent.length + 1)];
                exponent1[0] = 0;
                System.arraycopy(exponent, 0, exponent1, 1, exponent.length);
                b_exponent = new BigInteger(exponent1);
            } else {
                b_exponent = new BigInteger(exponent);
            }
            return writeDERObj2Bytes(new cn.com.jit.ida.util.pki.asn1.x509.SubjectPublicKeyInfo(new cn.com.jit.ida.util.pki.asn1.x509.AlgorithmIdentifier(PKCSObjectIdentifiers.rsaEncryption, null), writeDERObj2Bytes(new RSAPublicKeyStructure(b_modulus, b_exponent).getDERObject())).getDERObject());
        }
        throw new PKIException("8138", "加密机密钥转换软库密钥失败 密钥类型不合法");
    }

    public static byte[] customData2SoftPublicKey(String keyType, byte[] DSA_P, byte[] DSA_Q, byte[] DSA_G, byte[] softKey) throws PKIException {
        if (keyType.equals(JKey.DSA_PUB_KEY)) {
            return DSAParser.customData2SoftPublicKey(DSA_P, DSA_Q, DSA_G, softKey);
        }
        throw new PKIException("8138", "加密机密钥转换软库密钥失败 密钥类型不合法");
    }

    public static byte[] customData2SoftPrivateKey(String keyType, byte[] modulus, byte[] privateExponent, byte[] publicExponent, byte[] prime1, byte[] prime2, byte[] exponent1, byte[] exponent2, byte[] coefficient) throws PKIException {
        BigInteger b_modulus;
        BigInteger b_privateExponent;
        BigInteger b_publicExponent;
        BigInteger b_prime1;
        BigInteger b_prime2;
        BigInteger b_exponent1;
        BigInteger b_exponent2;
        BigInteger b_coefficient;
        if (keyType.equals(JKey.RSA_PRV_KEY)) {
            if (modulus[0] < 0) {
                byte[] modulus1 = new byte[(modulus.length + 1)];
                modulus1[0] = 0;
                System.arraycopy(modulus, 0, modulus1, 1, modulus.length);
                b_modulus = new BigInteger(modulus1);
            } else {
                b_modulus = new BigInteger(modulus);
            }
            if (privateExponent[0] < 0) {
                byte[] privateExponent1 = new byte[(privateExponent.length + 1)];
                privateExponent1[0] = 0;
                System.arraycopy(privateExponent, 0, privateExponent1, 1, privateExponent.length);
                b_privateExponent = new BigInteger(privateExponent1);
            } else {
                b_privateExponent = new BigInteger(privateExponent);
            }
            if (publicExponent[0] < 0) {
                byte[] publicExponent1 = new byte[(publicExponent.length + 1)];
                publicExponent1[0] = 0;
                System.arraycopy(publicExponent, 0, publicExponent1, 1, publicExponent.length);
                b_publicExponent = new BigInteger(publicExponent1);
            } else {
                b_publicExponent = new BigInteger(publicExponent);
            }
            if (prime1[0] < 0) {
                byte[] prime1_1 = new byte[(prime1.length + 1)];
                prime1_1[0] = 0;
                System.arraycopy(prime1, 0, prime1_1, 1, prime1.length);
                b_prime1 = new BigInteger(prime1_1);
            } else {
                b_prime1 = new BigInteger(prime1);
            }
            if (prime2[0] < 0) {
                byte[] prime2_1 = new byte[(prime2.length + 1)];
                prime2_1[0] = 0;
                System.arraycopy(prime2, 0, prime2_1, 1, prime2.length);
                b_prime2 = new BigInteger(prime2_1);
            } else {
                b_prime2 = new BigInteger(prime2);
            }
            if (exponent1[0] < 0) {
                byte[] exponent1_1 = new byte[(exponent1.length + 1)];
                exponent1_1[0] = 0;
                System.arraycopy(exponent1, 0, exponent1_1, 1, exponent1.length);
                b_exponent1 = new BigInteger(exponent1_1);
            } else {
                b_exponent1 = new BigInteger(exponent1);
            }
            if (exponent2[0] < 0) {
                byte[] exponent2_1 = new byte[(exponent2.length + 1)];
                exponent2_1[0] = 0;
                System.arraycopy(exponent2, 0, exponent2_1, 1, exponent2.length);
                b_exponent2 = new BigInteger(exponent2_1);
            } else {
                b_exponent2 = new BigInteger(exponent2);
            }
            if (coefficient[0] < 0) {
                byte[] coefficient1 = new byte[(coefficient.length + 1)];
                coefficient1[0] = 0;
                System.arraycopy(coefficient, 0, coefficient1, 1, coefficient.length);
                b_coefficient = new BigInteger(coefficient1);
            } else {
                b_coefficient = new BigInteger(coefficient);
            }
            RSAPrivateKeyStructure priKeyStructure = new RSAPrivateKeyStructure(b_modulus, b_publicExponent, b_privateExponent, b_prime1, b_prime2, b_exponent1, b_exponent2, b_coefficient);
            return writeDERObj2Bytes(new cn.com.jit.ida.util.pki.asn1.pkcs.pkcs8.PrivateKeyInfo(new cn.com.jit.ida.util.pki.asn1.x509.AlgorithmIdentifier(PKCSObjectIdentifiers.rsaEncryption, null), priKeyStructure.getDERObject()).getDERObject());
        } else if (keyType.equals(JKey.SM2_PRV_KEY)) {
            return null;
        } else {
            throw new PKIException("8138", "加密机密钥转换软库密钥失败 密钥类型不合法");
        }
    }

    public static byte[] writeDERObj2Bytes(cn.com.jit.ida.util.pki.asn1.DEREncodable obj) throws PKIException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            new DEROutputStream(bos).writeObject(obj);
            return bos.toByteArray();
        } catch (Exception ex) {
            throw new PKIException("8136", PKIException.DEROBJ_BYTES_DES, ex);
        }
    }

    public static DERObject writeBytes2DERObj(byte[] data) throws PKIException {
        try {
            return new cn.com.jit.ida.util.pki.asn1.ASN1InputStream(new ByteArrayInputStream(data)).readObject();
        } catch (Exception ex) {
            throw new PKIException("8137", PKIException.BYTES_DEROBJ_DES, ex);
        }
    }

    public static byte[] convertBase64(byte[] data) {
        ByteArrayInputStream bis = new ByteArrayInputStream(data);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        while (true) {
            byte tmp = (byte) bis.read();
            if (tmp == -1) {
                return bos.toByteArray();
            }
            if (!(tmp == 10 || tmp == 13)) {
                bos.write(tmp);
            }
        }
    }

    public static boolean isBase64Encode(byte[] data) {
        if (data.length < 4) {
            return false;
        }
        for (int i = 0; i < data.length; i++) {
            boolean include = false;
            int j = 0;
            while (true) {
                if (j >= b64Code.length) {
                    break;
                } else if (data[i] == b64Code[j]) {
                    include = true;
                    break;
                } else {
                    j++;
                }
            }
            if (!include) {
                return false;
            }
        }
        return true;
    }

    public static int getRSAKeyLength(JKey jkey) throws PKIException {
        if (jkey.getKeyType().equals(JKey.RSA_PUB_KEY)) {
            return convertPublicKey(jkey).getModulus().bitLength();
        }
        throw new PKIException("8133", "公钥转换失败 密钥类型不合法 " + jkey.getKeyType());
    }

    public static int getKeyLength(JKey jkey) throws PKIException {
        if (jkey.getKeyType().equals(JKey.RSA_PUB_KEY)) {
            return convertPublicKey(jkey).getModulus().bitLength();
        }
        if (jkey.getKeyType().equals(JKey.ECDSA_PUB_KEY)) {
            return convertPublicKey(jkey).getParameters().getCurve().getFieldSize();
        }
        throw new PKIException("8133", "公钥转换失败 密钥类型不合法 " + jkey.getKeyType());
    }

    public static boolean isEqualArray(byte[] a, byte[] b) {
        if (a.length != b.length) {
            return false;
        }
        for (int i = 0; i < a.length; i++) {
            if (a[i] != b[i]) {
                return false;
            }
        }
        return true;
    }

    public static String ReverseDN(String srcDN) {
        Vector dnVector = new Vector();
        while (srcDN.indexOf(",") != -1) {
            dnVector.add(srcDN.substring(0, srcDN.indexOf(",")).trim());
            srcDN = srcDN.substring(srcDN.indexOf(",") + 1, srcDN.length()).trim();
        }
        dnVector.add(srcDN);
        String entryDN = StringEx.Empty;
        int tCount = 0;
        int tSize = dnVector.size();
        for (int i = 0; i < tSize; i++) {
            if (tCount == 0) {
                entryDN = dnVector.get(i).toString().trim() + entryDN;
            } else {
                entryDN = dnVector.get(i).toString().trim() + "," + entryDN;
            }
            tCount++;
        }
        return entryDN;
    }

    public static X509CertificateStructure convertJITCertStruct2BCCertStruct(X509CertificateStructure jitCertStruct) throws PKIException {
        try {
            return new X509CertificateStructure((ASN1Sequence) new cn.com.jit.ida.util.pki.asn1.ASN1InputStream(new ByteArrayInputStream(writeDERObj2Bytes(jitCertStruct.getDERObject()))).readObject());
        } catch (IOException ex) {
            throw new PKIException(StringEx.Empty, "convert JITX509CertStructure to BCX509CertStructure failture.", ex);
        }
    }

    public static CertificateList convertJITCertList2BCCertList(CertificateList jitCertList) throws PKIException {
        try {
            return new CertificateList((ASN1Sequence) new cn.com.jit.ida.util.pki.asn1.ASN1InputStream(new ByteArrayInputStream(writeDERObj2Bytes(jitCertList.getDERObject()))).readObject());
        } catch (IOException ex) {
            throw new PKIException(StringEx.Empty, "convert JITX509CertStructure to BCX509CertStructure failture.", ex);
        }
    }

    public static JKey convertJITPublicKey(PublicKey key) throws PKIException {
        if (key.getAlgorithm().indexOf(Mechanism.RSA) >= 0) {
            return new JKey(JKey.RSA_PUB_KEY, key.getEncoded());
        }
        if (key.getAlgorithm().indexOf("1.2.840.10045.2.1") >= 0) {
            return new JKey(JKey.ECDSA_PUB_KEY, key.getEncoded());
        }
        if (key.getAlgorithm().indexOf(Mechanism.ECDSA) >= 0) {
            return new JKey(JKey.ECDSA_PUB_KEY, key.getEncoded());
        }
        if (key.getAlgorithm().indexOf("EC") >= 0) {
            return new JKey(JKey.ECDSA_PUB_KEY, key.getEncoded());
        }
        throw new PKIException("8133", "公钥转换失败 密钥类型不合法 " + key.getAlgorithm());
    }

    public static byte[] cvtPKCS8PrvKeyToCryptoAPIKey(byte[] prikey, int keysize) throws PKIException {
        try {
            ASN1Sequence secq = (ASN1Sequence) new cn.com.jit.ida.util.pki.asn1.pkcs.pkcs8.PrivateKeyInfo((ASN1Sequence) new cn.com.jit.ida.util.pki.asn1.ASN1InputStream(new ByteArrayInputStream(prikey)).readObject()).getPrivateKey();
            int bigLen = keysize / 8;
            int smallLen = keysize / 16;
            byte[] totlePrivate = new byte[((keysize / 16) * 9)];
            byte[] derInt7Data = writeDERObj2Bytes((DERInteger) secq.getObjectAt(3));
            System.arraycopy(derInt7Data, derInt7Data.length - bigLen, totlePrivate, 0, bigLen);
            int len = 0 + bigLen;
            byte[] derInt6Data = writeDERObj2Bytes((DERInteger) secq.getObjectAt(8));
            System.arraycopy(derInt6Data, derInt6Data.length - smallLen, totlePrivate, len, smallLen);
            int len2 = len + smallLen;
            byte[] derInt5Data = writeDERObj2Bytes((DERInteger) secq.getObjectAt(7));
            System.arraycopy(derInt5Data, derInt5Data.length - smallLen, totlePrivate, len2, smallLen);
            int len3 = len2 + smallLen;
            byte[] derInt4Data = writeDERObj2Bytes((DERInteger) secq.getObjectAt(6));
            System.arraycopy(derInt4Data, derInt4Data.length - smallLen, totlePrivate, len3, smallLen);
            int len4 = len3 + smallLen;
            byte[] derInt3Data = writeDERObj2Bytes((DERInteger) secq.getObjectAt(5));
            System.arraycopy(derInt3Data, derInt3Data.length - smallLen, totlePrivate, len4, smallLen);
            int len5 = len4 + smallLen;
            byte[] derInt2Data = writeDERObj2Bytes((DERInteger) secq.getObjectAt(4));
            System.arraycopy(derInt2Data, derInt2Data.length - smallLen, totlePrivate, len5, smallLen);
            byte[] derInt1Data = writeDERObj2Bytes((DERInteger) secq.getObjectAt(1));
            System.arraycopy(derInt1Data, derInt1Data.length - bigLen, totlePrivate, len5 + smallLen, bigLen);
            byte[] privateKey = new byte[totlePrivate.length];
            for (int i = 0; i < totlePrivate.length; i++) {
                privateKey[i] = totlePrivate[(totlePrivate.length - i) - 1];
            }
            return privateKey;
        } catch (Exception ex) {
            throw new PKIException("0", "密钥转换操作失败 " + ex.toString());
        }
    }

    public static byte[] cvtCryptoAPIKeyToPKCS8PrvKey(byte[] cryptoKey, int keySize) throws PKIException {
        byte[] privateKey = new byte[cryptoKey.length];
        for (int i = 0; i < cryptoKey.length; i++) {
            privateKey[i] = cryptoKey[(cryptoKey.length - i) - 1];
        }
        try {
            ByteArrayInputStream bis = new ByteArrayInputStream(privateKey);
            int bigLen = keySize / 8;
            int smallLen = keySize / 16;
            byte[] der3Data = new byte[bigLen];
            bis.read(der3Data);
            DERInteger der3Int = cvtDERInt(der3Data);
            byte[] der8Data = new byte[smallLen];
            bis.read(der8Data);
            DERInteger der8Int = cvtDERInt(der8Data);
            byte[] der7Data = new byte[smallLen];
            bis.read(der7Data);
            DERInteger der7Int = cvtDERInt(der7Data);
            byte[] der6Data = new byte[smallLen];
            bis.read(der6Data);
            DERInteger der6Int = cvtDERInt(der6Data);
            byte[] der5Data = new byte[smallLen];
            bis.read(der5Data);
            DERInteger der5Int = cvtDERInt(der5Data);
            byte[] der4Data = new byte[smallLen];
            bis.read(der4Data);
            DERInteger der4Int = cvtDERInt(der4Data);
            byte[] der1Data = new byte[bigLen];
            bis.read(der1Data);
            DERInteger der1Int = cvtDERInt(der1Data);
            DERInteger der2Int = new DERInteger(new byte[]{1, 0, 1});
            DERInteger der0Int = new DERInteger(new byte[]{0});
            DEREncodableVector derV = new DEREncodableVector();
            derV.add(der0Int);
            derV.add(der1Int);
            derV.add(der2Int);
            derV.add(der3Int);
            derV.add(der4Int);
            derV.add(der5Int);
            derV.add(der6Int);
            derV.add(der7Int);
            derV.add(der8Int);
            DERSequence derSeq = new DERSequence(derV);
            return writeDERObj2Bytes(new cn.com.jit.ida.util.pki.asn1.pkcs.pkcs8.PrivateKeyInfo(new cn.com.jit.ida.util.pki.asn1.x509.AlgorithmIdentifier(PKCSObjectIdentifiers.rsaEncryption, null), new RSAPrivateKeyStructure(derSeq).getDERObject()));
        } catch (Exception ex) {
            throw new PKIException("0", "密钥转换操作失败 " + ex.toString());
        }
    }

    private static DERInteger cvtDERInt(byte[] src) {
        if (src[0] >= 0) {
            return new DERInteger(src);
        }
        byte[] tmp = new byte[(src.length + 1)];
        tmp[0] = 0;
        System.arraycopy(src, 0, tmp, 1, src.length);
        return new DERInteger(tmp);
    }
}
