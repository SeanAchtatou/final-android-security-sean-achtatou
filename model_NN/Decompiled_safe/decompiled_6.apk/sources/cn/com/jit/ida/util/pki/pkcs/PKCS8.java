package cn.com.jit.ida.util.pki.pkcs;

import cn.com.jit.ida.util.pki.Parser;
import cn.com.jit.ida.util.pki.asn1.ASN1OctetString;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DEREncodableVector;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.pkcs.PKCSObjectIdentifiers;
import cn.com.jit.ida.util.pki.asn1.pkcs.pkcs8.EncryptedPrivateKeyInfo;
import cn.com.jit.ida.util.pki.asn1.x509.AlgorithmIdentifier;
import cn.com.jit.ida.util.pki.cipher.JCrypto;
import cn.com.jit.ida.util.pki.cipher.JKey;
import cn.com.jit.ida.util.pki.cipher.Mechanism;
import cn.com.jit.ida.util.pki.cipher.Session;
import cn.com.jit.ida.util.pki.cipher.param.PBEParam;

public class PKCS8 {
    private Session session;

    public PKCS8() {
        try {
            JCrypto jcrypto = JCrypto.getInstance();
            jcrypto.initialize(JCrypto.JSOFT_LIB, null);
            this.session = jcrypto.openSession(JCrypto.JSOFT_LIB);
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
    }

    public byte[] generateEPKI(JKey prvKey, char[] password) throws Exception {
        if (prvKey.getKeyType() != JKey.RSA_PRV_KEY) {
            throw new Exception("Unsupported JKey type:" + prvKey.getKeyType());
        }
        byte[] keyData = prvKey.getKey();
        PBEParam pbeParam = new PBEParam();
        byte[] salt = pbeParam.getSalt();
        int iterations = pbeParam.getIterations();
        JKey pbeKey = this.session.generatePBEKey(new Mechanism("PBEWITHSHAAND3-KEYTRIPLEDES-CBC"), password);
        byte[] encryptedData = this.session.encrypt(new Mechanism("PBEWITHSHAAND3-KEYTRIPLEDES-CBC", pbeParam), pbeKey, keyData);
        DEREncodableVector derV = new DEREncodableVector();
        DEROctetString derO = new DEROctetString(salt);
        DERInteger derI = new DERInteger(iterations);
        derV.add(derO);
        derV.add(derI);
        return Parser.writeDERObj2Bytes(new EncryptedPrivateKeyInfo(new AlgorithmIdentifier(PKCSObjectIdentifiers.pbeWithSHAAnd3DESCBC, new DERSequence(derV)), encryptedData));
    }

    public JKey decodeEPKI(byte[] epkiData, char[] password) throws Exception {
        EncryptedPrivateKeyInfo epki = new EncryptedPrivateKeyInfo((ASN1Sequence) Parser.writeBytes2DERObj(epkiData));
        AlgorithmIdentifier algId = epki.getEncryptionAlgorithm();
        if (!algId.getObjectId().equals(PKCSObjectIdentifiers.pbeWithSHAAnd3DESCBC)) {
            throw new Exception("Unsupported encryption algorithm:" + algId.getObjectId().getId());
        }
        ASN1Sequence asn1Seq2 = (ASN1Sequence) algId.getParameters();
        byte[] salt = ((ASN1OctetString) asn1Seq2.getObjectAt(0)).getOctets();
        int iterations = ((DERInteger) asn1Seq2.getObjectAt(1)).getValue().intValue();
        PBEParam pbeParam = new PBEParam();
        pbeParam.setSalt(salt);
        pbeParam.setIterations(iterations);
        JKey pbeKey = this.session.generatePBEKey(new Mechanism("PBEWITHSHAAND3-KEYTRIPLEDES-CBC"), password);
        return new JKey(JKey.RSA_PRV_KEY, this.session.decrypt(new Mechanism("PBEWITHSHAAND3-KEYTRIPLEDES-CBC", pbeParam), pbeKey, epki.getEncryptedData()));
    }
}
