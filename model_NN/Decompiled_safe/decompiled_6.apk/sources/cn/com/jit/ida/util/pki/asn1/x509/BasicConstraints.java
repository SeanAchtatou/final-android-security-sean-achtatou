package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.ASN1Encodable;
import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject;
import cn.com.jit.ida.util.pki.asn1.DERBoolean;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import java.math.BigInteger;

public class BasicConstraints extends ASN1Encodable {
    DERBoolean cA;
    DERInteger pathLenConstraint;

    public static BasicConstraints getInstance(ASN1TaggedObject obj, boolean explicit) {
        return getInstance(ASN1Sequence.getInstance(obj, explicit));
    }

    public static BasicConstraints getInstance(Object obj) {
        if (obj == null || (obj instanceof BasicConstraints)) {
            return (BasicConstraints) obj;
        }
        if (obj instanceof ASN1Sequence) {
            return new BasicConstraints((ASN1Sequence) obj);
        }
        throw new IllegalArgumentException("unknown object in factory");
    }

    public BasicConstraints(ASN1Sequence seq) {
        this.cA = new DERBoolean(false);
        this.pathLenConstraint = null;
        if (seq.size() == 0) {
            this.cA = null;
            this.pathLenConstraint = null;
            return;
        }
        this.cA = (DERBoolean) seq.getObjectAt(0);
        if (seq.size() > 1) {
            this.pathLenConstraint = (DERInteger) seq.getObjectAt(1);
        }
    }

    public BasicConstraints(boolean cA2, int pathLenConstraint2) {
        this.cA = new DERBoolean(false);
        this.pathLenConstraint = null;
        if (cA2) {
            this.cA = new DERBoolean(cA2);
            this.pathLenConstraint = new DERInteger(pathLenConstraint2);
            return;
        }
        this.cA = null;
        this.pathLenConstraint = null;
    }

    public BasicConstraints(boolean cA2) {
        this.cA = new DERBoolean(false);
        this.pathLenConstraint = null;
        if (cA2) {
            this.cA = new DERBoolean(true);
        } else {
            this.cA = new DERBoolean(false);
        }
        this.pathLenConstraint = null;
    }

    public BasicConstraints(int pathLenConstraint2) {
        this.cA = new DERBoolean(false);
        this.pathLenConstraint = null;
        this.cA = new DERBoolean(true);
        this.pathLenConstraint = new DERInteger(pathLenConstraint2);
    }

    public boolean isCA() {
        return this.cA != null && this.cA.isTrue();
    }

    public BigInteger getPathLenConstraint() {
        if (this.pathLenConstraint != null) {
            return this.pathLenConstraint.getValue();
        }
        return null;
    }

    public DERObject toASN1Object() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        if (this.cA != null) {
            v.add(this.cA);
            if (this.pathLenConstraint != null) {
                v.add(this.pathLenConstraint);
            }
        }
        return new DERSequence(v);
    }

    public String toString() {
        if (this.pathLenConstraint != null) {
            return "BasicConstraints: isCa(" + isCA() + "), pathLenConstraint = " + this.pathLenConstraint.getValue();
        }
        if (this.cA == null) {
            return "BasicConstraints: isCa(false)";
        }
        return "BasicConstraints: isCa(" + isCA() + ")";
    }
}
