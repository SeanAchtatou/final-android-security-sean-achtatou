package cn.com.jit.ida.util.pki.extension;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject;
import cn.com.jit.ida.util.pki.asn1.DERIA5String;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.x509.AccessDescription;
import cn.com.jit.ida.util.pki.asn1.x509.AuthorityInformationAccess;
import cn.com.jit.ida.util.pki.asn1.x509.GeneralName;
import cn.com.jit.ida.util.pki.asn1.x509.X509Extensions;
import cn.com.jit.ida.util.pki.asn1.x509.X509Name;
import java.util.Vector;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

public class AuthorityInformationAccessExt extends AbstractStandardExtension {
    public static final int DN_TYPE_NAME = 4;
    public static final int URI_TYPE_NAME = 6;
    private AccessDescriptionExt accessDescriptionExt;
    private Vector accessDescriptionExts;
    private String accessLocation;
    private String accessMethod;
    private int nameType;

    public AuthorityInformationAccessExt() {
        this.nameType = -1;
        this.accessMethod = null;
        this.accessLocation = null;
        this.accessDescriptionExts = null;
        this.accessDescriptionExt = null;
        this.OID = X509Extensions.AuthorityInfoAccess.getId();
        this.critical = false;
        this.accessDescriptionExts = new Vector();
        this.accessDescriptionExt = new AccessDescriptionExt();
    }

    public AuthorityInformationAccessExt(ASN1Sequence asn1Sequence) {
        this.nameType = -1;
        this.accessMethod = null;
        this.accessLocation = null;
        this.accessDescriptionExts = null;
        this.accessDescriptionExt = null;
        this.accessDescriptionExts = new Vector();
        int size = asn1Sequence.size();
        for (int i = 0; i < size; i++) {
            ASN1Sequence seq = (ASN1Sequence) asn1Sequence.getObjectAt(i);
            AccessDescriptionExt accessDescription = new AccessDescriptionExt();
            accessDescription.setAccessMethod(((DERObjectIdentifier) seq.getObjectAt(0)).getId());
            accessDescription.setAccessLocation(GeneralName.GeneralNameToString((ASN1TaggedObject) seq.getObjectAt(1)));
            this.accessDescriptionExts.add(accessDescription);
        }
    }

    public AuthorityInformationAccessExt(Node nl) throws PKIException {
        this.nameType = -1;
        this.accessMethod = null;
        this.accessLocation = null;
        this.accessDescriptionExts = null;
        this.accessDescriptionExt = null;
        this.OID = X509Extensions.AuthorityInfoAccess.getId();
        this.critical = false;
        this.accessDescriptionExts = new Vector();
        NodeList nodelist = nl.getChildNodes();
        if (nodelist.getLength() == 0) {
            throw new PKIException("XML内容缺失,AuthorityInformationAccessExt.AuthorityInformationAccessExt(),AuthorityInfoAccess没有子节点");
        }
        for (int i = 0; i < nodelist.getLength(); i++) {
            Node cnode = nodelist.item(i);
            String oid = null;
            String gname = null;
            if (cnode.getNodeName().equals("AccessDescription")) {
                NodeList nodelist1 = cnode.getChildNodes();
                for (int j = 0; j < nodelist1.getLength(); j++) {
                    Node nodeauth = nodelist1.item(j);
                    String cname = nodeauth.getNodeName();
                    oid = cname.equals("accessMethod") ? ((Text) nodeauth.getFirstChild()).getData().trim() : oid;
                    if (cname.equals("accessLocation")) {
                        String nodeValue = nodeauth.getAttributes().item(0).getNodeValue();
                        try {
                            gname = ((Text) nodeauth.getFirstChild()).getData().trim();
                        } catch (IllegalArgumentException e) {
                            throw new PKIException("IllegalArgumentException,AuthorityInformationAccessExt.AuthorityInformationAccessExt(),构造accessLocation时出错", e);
                        }
                    }
                }
                this.accessDescriptionExt = new AccessDescriptionExt();
                try {
                    this.accessDescriptionExt.setAccessLocation(gname);
                    this.accessDescriptionExt.setAccessMethod(oid);
                    this.accessDescriptionExts.add(this.accessDescriptionExt);
                } catch (IllegalArgumentException e2) {
                    throw new PKIException("IllegalArgumentException,AuthorityInformationAccessExt.AuthorityInformationAccessExt(),构造AccessDescription时出错", e2);
                }
            }
        }
    }

    public void addAccessDescription(AccessDescriptionExt accessDescription) {
        this.accessDescriptionExts.add(accessDescription);
    }

    public AccessDescriptionExt[] getAccessDescription() {
        return (AccessDescriptionExt[]) this.accessDescriptionExts.toArray(new AccessDescriptionExt[this.accessDescriptionExts.size()]);
    }

    public void setAccessMethod(String accessMethod2) {
        this.accessDescriptionExt.setAccessMethod(accessMethod2);
    }

    public void setAccessLocation(int nameType2, String accessLocation2) {
        this.accessDescriptionExt.setAccessLocationType(nameType2);
        this.accessDescriptionExt.setAccessLocation(accessLocation2);
    }

    public String getOID() {
        return this.OID;
    }

    public void setCritical(boolean critical) {
        this.critical = critical;
    }

    public boolean getCritical() {
        return this.critical;
    }

    public String getAccessMethod() {
        return this.accessMethod;
    }

    public String getAccessLocation() {
        return this.accessLocation;
    }

    public AccessDescriptionExt getAccessDescriptionExt() {
        return this.accessDescriptionExt;
    }

    public void setAccessDescriptionExt(AccessDescriptionExt accessDescriptionExt2) {
        this.accessDescriptionExt = accessDescriptionExt2;
    }

    public byte[] encode() throws PKIException {
        Vector vAccessDesExt = new Vector();
        for (int i = 0; i < this.accessDescriptionExts.size(); i++) {
            AccessDescriptionExt accessDesExt = (AccessDescriptionExt) this.accessDescriptionExts.get(i);
            DERObjectIdentifier idAccessMethod = new DERObjectIdentifier(accessDesExt.getAccessMethod());
            GeneralName nameAccessLocation = null;
            if (accessDesExt.getAccessLocationType() == 6) {
                nameAccessLocation = new GeneralName(new DERIA5String(accessDesExt.getAccessLocation().getBytes()), 6);
            } else if (accessDesExt.getAccessLocationType() == 4) {
                nameAccessLocation = new GeneralName(new X509Name(accessDesExt.getAccessLocation()));
            }
            AccessDescription accessDes = new AccessDescription();
            accessDes.setAccessMethod(idAccessMethod);
            accessDes.setAccessLocation(nameAccessLocation);
            vAccessDesExt.add(accessDes);
        }
        return new DEROctetString(new AuthorityInformationAccess(vAccessDesExt).getDERObject()).getOctets();
    }
}
