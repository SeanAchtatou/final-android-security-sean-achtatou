package cn.com.jit.ida.util.pki.asn1.pkcs.pkcs7;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1OctetString;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject;
import cn.com.jit.ida.util.pki.asn1.BERSequence;
import cn.com.jit.ida.util.pki.asn1.BERTaggedObject;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.x509.AlgorithmIdentifier;

public class EncryptedContentInfo implements DEREncodable {
    private AlgorithmIdentifier contentEncryptionAlgorithm;
    private DERObjectIdentifier contentType;
    private ASN1OctetString encryptedContent;

    public EncryptedContentInfo(DERObjectIdentifier contentType2, AlgorithmIdentifier contentEncryptionAlgorithm2, ASN1OctetString encryptedContent2) {
        this.contentType = contentType2;
        this.contentEncryptionAlgorithm = contentEncryptionAlgorithm2;
        this.encryptedContent = encryptedContent2;
    }

    public EncryptedContentInfo(ASN1Sequence seq) {
        this.contentType = (DERObjectIdentifier) seq.getObjectAt(0);
        this.contentEncryptionAlgorithm = AlgorithmIdentifier.getInstance(seq.getObjectAt(1));
        if (seq.size() > 2) {
            this.encryptedContent = ASN1OctetString.getInstance((ASN1TaggedObject) seq.getObjectAt(2), false);
        }
    }

    public static EncryptedContentInfo getInstance(Object obj) {
        if (obj == null || (obj instanceof EncryptedContentInfo)) {
            return (EncryptedContentInfo) obj;
        }
        if (obj instanceof ASN1Sequence) {
            return new EncryptedContentInfo((ASN1Sequence) obj);
        }
        throw new IllegalArgumentException("Invalid EncryptedContentInfo: " + obj.getClass().getName());
    }

    public DERObjectIdentifier getContentType() {
        return this.contentType;
    }

    public AlgorithmIdentifier getContentEncryptionAlgorithm() {
        return this.contentEncryptionAlgorithm;
    }

    public ASN1OctetString getEncryptedContent() {
        return this.encryptedContent;
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(this.contentType);
        v.add(this.contentEncryptionAlgorithm);
        if (this.encryptedContent != null) {
            v.add(new BERTaggedObject(false, 0, this.encryptedContent));
        }
        return new BERSequence(v);
    }
}
