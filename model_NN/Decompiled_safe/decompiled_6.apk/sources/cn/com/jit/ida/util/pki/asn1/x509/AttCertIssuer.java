package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DEREncodableVector;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.DERTaggedObject;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class AttCertIssuer implements DEREncodable {
    DERObject choiceObj;
    V2Form v2form;

    public static AttCertIssuer getInstance(String issuer) {
        DEREncodableVector ve = new DEREncodableVector();
        ve.add(new GeneralName(new X509Name(issuer)).getDERObject());
        GeneralNames ns = new GeneralNames(new DERSequence(ve));
        V2Form form = new V2Form();
        form.setIssuerName(ns);
        return getInstance(form);
    }

    public static AttCertIssuer getInstance(Object obj) {
        if (obj instanceof AttCertIssuer) {
            return (AttCertIssuer) obj;
        }
        if (obj instanceof V2Form) {
            return new AttCertIssuer(V2Form.getInstance(obj));
        }
        if (obj instanceof GeneralNames) {
            return new AttCertIssuer((GeneralNames) obj);
        }
        if (obj instanceof ASN1TaggedObject) {
            return new AttCertIssuer(V2Form.getInstance((ASN1TaggedObject) obj, true));
        }
        if (obj instanceof ASN1Sequence) {
            return new AttCertIssuer(GeneralNames.getInstance(obj));
        }
        throw new IllegalArgumentException("unknown object in factory: " + obj.getClass());
    }

    public static AttCertIssuer getInstance(ASN1TaggedObject obj, boolean explicit) {
        return getInstance(obj.getObject());
    }

    public AttCertIssuer(GeneralNames names) {
        this.v2form = new V2Form(names);
        this.choiceObj = this.v2form.getDERObject();
    }

    public AttCertIssuer(V2Form v2Form) {
        this.v2form = v2Form;
        this.choiceObj = new DERTaggedObject(true, 0, this.v2form.getDERObject());
    }

    public DERObject getDERObject() {
        if (this.choiceObj == null && this.v2form != null) {
            this.choiceObj = new DERTaggedObject(true, 0, this.v2form.getDERObject());
        }
        return this.choiceObj;
    }

    public String[] toStringArr() {
        if (this.v2form == null) {
            return null;
        }
        GeneralNames issuerName = this.v2form.getIssuerName();
        String[] names = new String[issuerName.getNames().length];
        for (int i = 0; i < names.length; i++) {
            names[i] = issuerName.getNames()[i].toString();
        }
        return names;
    }

    public String toString() {
        String[] a;
        if (this.v2form == null || (a = toStringArr()) == null) {
            return null;
        }
        StringBuffer buf = new StringBuffer();
        for (String append : a) {
            if (buf.length() > 0) {
                buf.append("\n");
            }
            buf.append(append);
        }
        return buf.toString();
    }

    public AttCertIssuer(Node nl) throws PKIException {
        this.v2form = null;
        NodeList nodelist = nl.getChildNodes();
        if (nodelist.getLength() == 0) {
            throw new PKIException("XML内容缺失,AttCertIssuer.AttCertIssuer(),Issuer没有子节点");
        }
        for (int i = 0; i < nodelist.getLength(); i++) {
            Node cnode = nodelist.item(i);
            if (cnode.getNodeName().equals("V2Form")) {
                this.v2form = new V2Form(cnode);
            }
        }
    }
}
