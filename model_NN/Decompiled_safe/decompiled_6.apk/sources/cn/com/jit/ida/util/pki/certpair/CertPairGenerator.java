package cn.com.jit.ida.util.pki.certpair;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.Parser;
import cn.com.jit.ida.util.pki.asn1.x509.CertificatePair;
import cn.com.jit.ida.util.pki.asn1.x509.X509CertificateStructure;
import cn.com.jit.ida.util.pki.cert.X509Cert;
import cn.com.jit.ida.util.pki.encoders.Base64;
import java.io.InputStream;

public class CertPairGenerator {
    private X509CertificateStructure forward = null;
    private X509CertificateStructure reverse = null;

    public CertPairGenerator(X509Cert forward2, X509Cert reverse2) {
        if (forward2 != null) {
            this.forward = forward2.getCertStructure();
        }
        if (reverse2 != null) {
            this.reverse = reverse2.getCertStructure();
        }
    }

    public CertPairGenerator(byte[] forward2, byte[] reverse2) throws PKIException {
        if (forward2 != null && forward2.length > 0) {
            this.forward = new X509Cert(forward2).getCertStructure();
        }
        if (reverse2 != null && reverse2.length > 0) {
            this.reverse = new X509Cert(reverse2).getCertStructure();
        }
    }

    public CertPairGenerator(InputStream forward2, InputStream reverse2) throws PKIException {
        if (forward2 != null) {
            this.forward = new X509Cert(forward2).getCertStructure();
        }
        if (reverse2 != null) {
            this.reverse = new X509Cert(reverse2).getCertStructure();
        }
    }

    public byte[] generateCertPair() throws PKIException {
        try {
            return Parser.writeDERObj2Bytes(new CertificatePair(this.forward, this.reverse).getDERObject());
        } catch (PKIException ex) {
            throw new PKIException(PKIException.CERT_PAIR_BYTES_ERR, PKIException.CERT_PAIR_BYTES_DES, ex);
        }
    }

    public byte[] generateCertPair_BASE64() throws PKIException {
        try {
            return Base64.encode(Parser.writeDERObj2Bytes(new CertificatePair(this.forward, this.reverse).getDERObject()));
        } catch (PKIException ex) {
            throw new PKIException(PKIException.CERT_PAIR_BYTES_ERR, PKIException.CERT_PAIR_BYTES_DES, ex);
        }
    }
}
