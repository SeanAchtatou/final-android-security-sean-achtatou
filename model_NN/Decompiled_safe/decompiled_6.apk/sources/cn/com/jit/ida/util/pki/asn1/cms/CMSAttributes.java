package cn.com.jit.ida.util.pki.asn1.cms;

import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.pkcs.PKCSObjectIdentifiers;

public interface CMSAttributes {
    public static final DERObjectIdentifier contentType = PKCSObjectIdentifiers.pkcs_9_at_contentType;
    public static final DERObjectIdentifier counterSignature = PKCSObjectIdentifiers.pkcs_9_at_counterSignature;
    public static final DERObjectIdentifier messageDigest = PKCSObjectIdentifiers.pkcs_9_at_messageDigest;
    public static final DERObjectIdentifier signingTime = PKCSObjectIdentifiers.pkcs_9_at_signingTime;
}
