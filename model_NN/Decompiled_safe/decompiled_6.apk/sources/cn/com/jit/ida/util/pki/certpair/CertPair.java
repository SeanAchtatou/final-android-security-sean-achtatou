package cn.com.jit.ida.util.pki.certpair;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.Parser;
import cn.com.jit.ida.util.pki.asn1.ASN1InputStream;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.x509.CertificatePair;
import cn.com.jit.ida.util.pki.asn1.x509.X509CertificateStructure;
import cn.com.jit.ida.util.pki.cert.X509Cert;
import cn.com.jit.ida.util.pki.encoders.Base64;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class CertPair {
    private CertificatePair certPair = null;

    public CertPair(byte[] certPairData) throws PKIException {
        try {
            this.certPair = new CertificatePair((ASN1Sequence) new ASN1InputStream(new ByteArrayInputStream(parseData(certPairData))).readObject());
        } catch (Exception ex) {
            throw new PKIException(PKIException.INIT_CERT_PAIR_ERR, PKIException.INIT_CERT_PAIR_DES, ex);
        } catch (Throwable th) {
            throw new PKIException(PKIException.INIT_CERT_PAIR_ERR, PKIException.INIT_CERT_PAIR_DES);
        }
    }

    public CertPair(InputStream certPairIS) throws PKIException {
        try {
            int length = certPairIS.available();
            byte[] bCertPair = new byte[length];
            int readLen = certPairIS.read(bCertPair);
            while (readLen < length) {
                byte[] temp = new byte[(length - readLen)];
                int i = certPairIS.read(temp);
                System.arraycopy(temp, 0, bCertPair, readLen, i);
                readLen += i;
            }
            certPairIS.close();
            this.certPair = new CertificatePair((ASN1Sequence) new ASN1InputStream(new ByteArrayInputStream(parseData(bCertPair))).readObject());
        } catch (Exception ex) {
            throw new PKIException(PKIException.INIT_CERT_PAIR_ERR, PKIException.INIT_CERT_PAIR_DES, ex);
        } catch (Throwable th) {
            throw new PKIException(PKIException.INIT_CERT_PAIR_ERR, PKIException.INIT_CERT_PAIR_DES);
        }
    }

    public X509Cert getForward() {
        X509CertificateStructure certStructure = this.certPair.getForward();
        if (certStructure != null) {
            return new X509Cert(certStructure);
        }
        return null;
    }

    public X509Cert getReverse() {
        X509CertificateStructure certStructure = this.certPair.getReverse();
        if (certStructure != null) {
            return new X509Cert(certStructure);
        }
        return null;
    }

    private byte[] parseData(byte[] certPairData) throws PKIException {
        try {
            if (Parser.isBase64Encode(certPairData)) {
                return Base64.decode(Parser.convertBase64(certPairData));
            }
            return certPairData;
        } catch (Exception ex) {
            throw new PKIException(PKIException.INIT_CERT_PAIR_ERR, PKIException.INIT_CERT_PAIR_DES, ex);
        } catch (Throwable th) {
            throw new PKIException(PKIException.INIT_CERT_PAIR_ERR, PKIException.INIT_CERT_PAIR_DES);
        }
    }
}
