package cn.com.jit.ida.util.pki.asn1;

import java.io.IOException;

public class BERNull extends DERNull {
    /* access modifiers changed from: package-private */
    public void encode(DEROutputStream out) throws IOException {
        if ((out instanceof ASN1OutputStream) || (out instanceof BEROutputStream)) {
            out.write(5);
            out.write(0);
            out.write(0);
            return;
        }
        super.encode(out);
    }
}
