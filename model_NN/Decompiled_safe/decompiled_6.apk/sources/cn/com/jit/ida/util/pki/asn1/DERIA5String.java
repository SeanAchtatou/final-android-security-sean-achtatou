package cn.com.jit.ida.util.pki.asn1;

import java.io.IOException;

public class DERIA5String extends DERObject implements DERString {
    String string;

    public static DERIA5String getInstance(Object obj) {
        if (obj == null || (obj instanceof DERIA5String)) {
            return (DERIA5String) obj;
        }
        if (obj instanceof ASN1OctetString) {
            return new DERIA5String(((ASN1OctetString) obj).getOctets());
        }
        if (obj instanceof ASN1TaggedObject) {
            return getInstance(((ASN1TaggedObject) obj).getObject());
        }
        throw new IllegalArgumentException("illegal object in getInstance: " + obj.getClass().getName());
    }

    public static DERIA5String getInstance(ASN1TaggedObject obj, boolean explicit) {
        return getInstance(obj.getObject());
    }

    public DERIA5String(byte[] string2) {
        char[] cs = new char[string2.length];
        for (int i = 0; i != cs.length; i++) {
            cs[i] = (char) (string2[i] & 255);
        }
        this.string = new String(cs);
    }

    public DERIA5String(String string2) {
        this.string = string2;
    }

    public String getString() {
        return this.string;
    }

    public byte[] getOctets() {
        char[] cs = this.string.toCharArray();
        byte[] bs = new byte[cs.length];
        for (int i = 0; i != cs.length; i++) {
            bs[i] = (byte) cs[i];
        }
        return bs;
    }

    /* access modifiers changed from: package-private */
    public void encode(DEROutputStream out) throws IOException {
        out.writeEncoded(22, getOctets());
    }

    public int hashCode() {
        return getString().hashCode();
    }

    public boolean equals(Object o) {
        if (!(o instanceof DERIA5String)) {
            return false;
        }
        return getString().equals(((DERIA5String) o).getString());
    }
}
