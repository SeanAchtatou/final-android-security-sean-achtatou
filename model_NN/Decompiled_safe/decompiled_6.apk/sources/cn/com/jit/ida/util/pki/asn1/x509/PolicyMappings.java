package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.ASN1Encodable;
import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import java.util.Enumeration;
import java.util.Hashtable;

public class PolicyMappings extends ASN1Encodable {
    ASN1Sequence seq = null;

    public PolicyMappings(ASN1Sequence seq2) {
        this.seq = seq2;
    }

    public PolicyMappings(Hashtable mappings) {
        ASN1EncodableVector dev = new ASN1EncodableVector();
        Enumeration it = mappings.keys();
        while (it.hasMoreElements()) {
            String idp = (String) it.nextElement();
            ASN1EncodableVector dv = new ASN1EncodableVector();
            dv.add(new DERObjectIdentifier(idp));
            dv.add(new DERObjectIdentifier((String) mappings.get(idp)));
            dev.add(new DERSequence(dv));
        }
        this.seq = new DERSequence(dev);
    }

    public DERObject toASN1Object() {
        return this.seq;
    }
}
