package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.Parser;
import cn.com.jit.ida.util.pki.asn1.ASN1Encodable;
import cn.com.jit.ida.util.pki.asn1.DERBoolean;
import cn.com.jit.ida.util.pki.asn1.DEREncodableVector;
import cn.com.jit.ida.util.pki.asn1.DERNull;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import org.w3c.dom.Node;

public class NoRevocationExtension extends ASN1Encodable {
    private DERBoolean critical = new DERBoolean(false);
    private DERObjectIdentifier extnID = X509Extensions.NoRevocation;
    private DEROctetString extnValue;

    public NoRevocationExtension(Node nl) {
    }

    public DERObject toASN1Object() {
        DEREncodableVector vec = new DEREncodableVector();
        vec.add(this.extnID);
        vec.add(this.critical);
        byte[] arr = null;
        try {
            arr = Parser.writeDERObj2Bytes(new DERNull());
        } catch (PKIException e) {
            e.printStackTrace();
        }
        this.extnValue = new DEROctetString(arr);
        vec.add(this.extnValue);
        return new DERSequence(vec);
    }

    public void decode(DERObject parm1) throws PKIException {
        throw new UnsupportedOperationException("Method decode() not yet implemented.");
    }
}
