package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject;
import cn.com.jit.ida.util.pki.asn1.DERBitString;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.pkcs.PKCSObjectIdentifiers;

public class X509CertificateStructure implements DEREncodable, X509ObjectIdentifiers, PKCSObjectIdentifiers {
    ASN1Sequence seq;
    DERBitString sig;
    AlgorithmIdentifier sigAlgId;
    TBSCertificateStructure tbsCert;

    public static X509CertificateStructure getInstance(ASN1TaggedObject obj, boolean explicit) {
        return getInstance(ASN1Sequence.getInstance(obj, explicit));
    }

    public static X509CertificateStructure getInstance(Object obj) {
        if (obj instanceof X509CertificateStructure) {
            return (X509CertificateStructure) obj;
        }
        if (obj instanceof ASN1Sequence) {
            return new X509CertificateStructure((ASN1Sequence) obj);
        }
        throw new IllegalArgumentException("unknown object in factory");
    }

    public X509CertificateStructure(ASN1Sequence seq2) {
        this.seq = seq2;
        if (seq2.size() == 3) {
            this.tbsCert = TBSCertificateStructure.getInstance(seq2.getObjectAt(0));
            this.sigAlgId = AlgorithmIdentifier.getInstance(seq2.getObjectAt(1));
            this.sig = (DERBitString) seq2.getObjectAt(2);
            return;
        }
        throw new IllegalArgumentException("wrong format of cert structure");
    }

    public TBSCertificateStructure getTBSCertificate() {
        return this.tbsCert;
    }

    public int getVersion() {
        return this.tbsCert.getVersion();
    }

    public DERInteger getSerialNumber() {
        return this.tbsCert.getSerialNumber();
    }

    public X509Name getIssuer() {
        return this.tbsCert.getIssuer();
    }

    public Time getStartDate() {
        return this.tbsCert.getStartDate();
    }

    public Time getEndDate() {
        return this.tbsCert.getEndDate();
    }

    public X509Name getSubject() {
        return this.tbsCert.getSubject();
    }

    public SubjectPublicKeyInfo getSubjectPublicKeyInfo() {
        return this.tbsCert.getSubjectPublicKeyInfo();
    }

    public X509Extensions getExtensions() {
        return this.tbsCert.getExtensions();
    }

    public DERBitString getIssuerUniqueId() {
        return this.tbsCert.getIssuerUniqueId();
    }

    public DERBitString getSubjectUniqueId() {
        return this.tbsCert.getSubjectUniqueId();
    }

    public AlgorithmIdentifier getSignatureAlgorithm() {
        return this.sigAlgId;
    }

    public DERBitString getSignature() {
        return this.sig;
    }

    public DERObject getDERObject() {
        return this.seq;
    }
}
