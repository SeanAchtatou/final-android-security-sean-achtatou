package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.DERPrintableString;

public class JITInsuranceNumber extends DERPrintableString {
    public JITInsuranceNumber(byte[] string) {
        super(string);
    }

    public JITInsuranceNumber(String string) {
        super(string);
    }

    public String getInsuranceNumber() {
        return getString();
    }
}
