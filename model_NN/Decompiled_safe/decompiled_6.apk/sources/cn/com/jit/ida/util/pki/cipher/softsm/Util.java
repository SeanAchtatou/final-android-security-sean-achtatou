package cn.com.jit.ida.util.pki.cipher.softsm;

import cn.com.jit.ida.util.pki.ECDSAParser;
import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.Parser;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.cipher.JKey;
import cn.com.jit.ida.util.pki.cipher.JKeyPair;
import java.io.ByteArrayInputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERInteger;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.math.ec.ECPoint;

public class Util {
    public static byte[] byteconvert32(BigInteger b) {
        byte[] tmpd;
        if (b.toByteArray().length == 33) {
            tmpd = new byte[32];
            System.arraycopy(b.toByteArray(), 1, tmpd, 0, 32);
        } else if (b.toByteArray().length != 32) {
            return null;
        } else {
            tmpd = b.toByteArray();
        }
        return tmpd;
    }

    public static BigInteger byteconvertInteger(byte[] b) {
        if (b[0] >= 0) {
            return new BigInteger(b);
        }
        byte[] temp = new byte[(b.length + 1)];
        temp[0] = 0;
        System.arraycopy(b, 0, temp, 1, b.length);
        return new BigInteger(temp);
    }

    public static byte[] hardKey2SoftPubKey(JKey pubKey) throws Exception {
        try {
            return new SubjectPublicKeyInfo((ASN1Sequence) new ASN1InputStream(new ByteArrayInputStream(pubKey.getKey())).readObject()).getPublicKeyData().getBytes();
        } catch (Exception ex) {
            throw new PKIException("8137", PKIException.BYTES_DEROBJ_DES, ex);
        }
    }

    public static BigInteger hardKey2SoftPrivKey(JKey privKey) throws Exception {
        try {
            return byteconvertInteger(((ASN1Sequence) new ASN1InputStream(new ByteArrayInputStream(privKey.getKey())).readObject()).getObjectAt(1).getOctets());
        } catch (Exception ex) {
            throw new PKIException("8137", PKIException.BYTES_DEROBJ_DES, ex);
        }
    }

    public static byte[] soft2HardSignData(BigInteger r, BigInteger s) throws Exception {
        DERInteger d_r = new DERInteger(r);
        DERInteger d_s = new DERInteger(s);
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(d_r);
        v.add(d_s);
        return new DERSequence(v).getDEREncoded();
    }

    public static SM2Result Hard2softSignData(byte[] signdata) throws Exception {
        try {
            Enumeration e = ((ASN1Sequence) new ASN1InputStream(new ByteArrayInputStream(signdata)).readObject()).getObjects();
            BigInteger r = ((DERInteger) e.nextElement()).getValue();
            BigInteger s = ((DERInteger) e.nextElement()).getValue();
            SM2Result sm2result = new SM2Result();
            sm2result.r = r;
            sm2result.s = s;
            return sm2result;
        } catch (Exception ex) {
            throw new PKIException("8137", PKIException.BYTES_DEROBJ_DES, ex);
        }
    }

    public static JKeyPair getSM2Key(ECPublicKeyParameters pubparam, ECPrivateKeyParameters priparam) throws Exception {
        if (pubparam == null && priparam == null) {
            return null;
        }
        byte[] tmpq = null;
        JKey publicKey = null;
        JKey privateKey = null;
        if (pubparam != null) {
            ECPoint userKey = pubparam.getQ();
            tmpq = new byte[(userKey.getEncoded().length - 1)];
            System.arraycopy(userKey.getEncoded(), 1, tmpq, 0, userKey.getEncoded().length - 1);
            publicKey = new JKey(JKey.SM2_PUB_KEY, ECDSAParser.customData2SoftPublicKey(tmpq, null));
        }
        if (priparam != null) {
            byte[] tmpd = byteconvert32(priparam.getD());
            if (tmpq != null) {
                privateKey = new JKey(JKey.SM2_PRV_KEY, ECDSAParser.customData2SoftECPrivKey(tmpq, tmpd));
            }
        }
        return new JKeyPair(publicKey, privateKey);
    }

    public static byte[] encedDataEncode(ECPoint pubQ, byte[] dig, byte[] enc) throws Exception {
        cn.com.jit.ida.util.pki.asn1.DERInteger x = new cn.com.jit.ida.util.pki.asn1.DERInteger(pubQ.getX().toBigInteger());
        cn.com.jit.ida.util.pki.asn1.DERInteger y = new cn.com.jit.ida.util.pki.asn1.DERInteger(pubQ.getY().toBigInteger());
        DEROctetString derDig = new DEROctetString(dig);
        DEROctetString derEnc = new DEROctetString(enc);
        cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector v = new cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector();
        v.add(x);
        v.add(y);
        v.add(derDig);
        v.add(derEnc);
        return Parser.writeDERObj2Bytes(new cn.com.jit.ida.util.pki.asn1.DERSequence(v));
    }

    public static List getXY(byte[] encedData) throws Exception {
        cn.com.jit.ida.util.pki.asn1.DERSequence der = (cn.com.jit.ida.util.pki.asn1.DERSequence) Parser.writeBytes2DERObj(encedData);
        List ls = new ArrayList();
        ls.add(((cn.com.jit.ida.util.pki.asn1.DERInteger) der.getObjectAt(0)).getValue());
        ls.add(((cn.com.jit.ida.util.pki.asn1.DERInteger) der.getObjectAt(1)).getValue());
        return ls;
    }

    public static byte[] getEnc(byte[] encedData) throws Exception {
        return ((DEROctetString) ((cn.com.jit.ida.util.pki.asn1.DERSequence) Parser.writeBytes2DERObj(encedData)).getObjectAt(3)).getOctets();
    }

    public static BigInteger getPrivKeyD(JKey privKey) throws Exception {
        return byteconvertInteger(((DEROctetString) ((cn.com.jit.ida.util.pki.asn1.DERSequence) Parser.writeBytes2DERObj(privKey.getKey())).getObjectAt(1)).getOctets());
    }
}
