package cn.com.jit.ida.util.pki.asn1.pkcs.pkcs8;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1OctetString;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1Set;
import cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERInputStream;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.DERTaggedObject;
import cn.com.jit.ida.util.pki.asn1.pkcs.PKCSObjectIdentifiers;
import cn.com.jit.ida.util.pki.asn1.x509.AlgorithmIdentifier;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Enumeration;

public class PrivateKeyInfo implements PKCSObjectIdentifiers, DEREncodable {
    private AlgorithmIdentifier algId;
    private ASN1Set attributes;
    private DERObject privKey;
    private DERInteger version;

    public PrivateKeyInfo(AlgorithmIdentifier algId2, DERObject privateKey) {
        this.privKey = privateKey;
        this.algId = algId2;
    }

    public PrivateKeyInfo(AlgorithmIdentifier algId2, DERObject privateKey, ASN1Set attributes2) {
        this.privKey = privateKey;
        this.algId = algId2;
        this.attributes = attributes2;
    }

    public PrivateKeyInfo(ASN1Sequence seq) {
        Enumeration e = seq.getObjects();
        BigInteger version2 = ((DERInteger) e.nextElement()).getValue();
        if (version2.intValue() != 0) {
            throw new IllegalArgumentException("wrong version:" + version2.intValue() + " for private key info");
        }
        this.algId = new AlgorithmIdentifier((ASN1Sequence) e.nextElement());
        try {
            this.privKey = new DERInputStream(new ByteArrayInputStream(((ASN1OctetString) e.nextElement()).getOctets())).readObject();
            if (e.hasMoreElements()) {
                this.attributes = ASN1Set.getInstance((ASN1TaggedObject) e.nextElement(), false);
            }
        } catch (IOException e2) {
            throw new IllegalArgumentException("Error recoverying private key from sequence");
        }
    }

    public AlgorithmIdentifier getAlgorithmId() {
        return this.algId;
    }

    public DERObject getPrivateKey() {
        return this.privKey;
    }

    public ASN1Set getAttributes() {
        return this.attributes;
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(new DERInteger(0));
        v.add(this.algId);
        v.add(new DEROctetString(this.privKey));
        if (this.attributes != null) {
            v.add(new DERTaggedObject(false, 0, this.attributes));
        }
        return new DERSequence(v);
    }
}
