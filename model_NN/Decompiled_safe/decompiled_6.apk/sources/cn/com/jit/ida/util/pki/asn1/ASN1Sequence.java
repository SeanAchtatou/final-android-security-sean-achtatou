package cn.com.jit.ida.util.pki.asn1;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Vector;

public abstract class ASN1Sequence extends DERObject {
    private Vector seq = new Vector();

    /* access modifiers changed from: package-private */
    public abstract void encode(DEROutputStream dEROutputStream) throws IOException;

    public static ASN1Sequence getInstance(Object obj) {
        if (obj == null || (obj instanceof ASN1Sequence)) {
            return (ASN1Sequence) obj;
        }
        throw new IllegalArgumentException("unknown object in getInstance");
    }

    public static ASN1Sequence getInstance(ASN1TaggedObject obj, boolean explicit) {
        if (explicit) {
            if (obj.isExplicit()) {
                return (ASN1Sequence) obj.getObject();
            }
            throw new IllegalArgumentException("object implicit - explicit expected.");
        } else if (obj.isExplicit()) {
            if (obj instanceof BERTaggedObject) {
                return new BERSequence(obj.getObject());
            }
            return new DERSequence(obj.getObject());
        } else if (obj.getObject() instanceof ASN1Sequence) {
            return (ASN1Sequence) obj.getObject();
        } else {
            throw new IllegalArgumentException("unknown object in getInstanceFromTagged");
        }
    }

    public Enumeration getObjects() {
        return this.seq.elements();
    }

    public DEREncodable getObjectAt(int index) {
        return (DEREncodable) this.seq.elementAt(index);
    }

    public int size() {
        return this.seq.size();
    }

    public int hashCode() {
        Enumeration e = getObjects();
        int hashCode = 0;
        while (e.hasMoreElements()) {
            Object o = e.nextElement();
            if (o != null) {
                hashCode ^= o.hashCode();
            }
        }
        return hashCode;
    }

    public boolean equals(Object o) {
        if (o == null || !(o instanceof ASN1Sequence)) {
            return false;
        }
        ASN1Sequence other = (ASN1Sequence) o;
        if (size() != other.size()) {
            return false;
        }
        Enumeration s1 = getObjects();
        Enumeration s2 = other.getObjects();
        while (s1.hasMoreElements()) {
            Object o1 = s1.nextElement();
            Object o2 = s2.nextElement();
            if (o1 == null || o2 == null) {
                if (o1 != null || o2 != null) {
                    return false;
                }
            } else if (!o1.equals(o2)) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void addObject(DEREncodable obj) {
        this.seq.addElement(obj);
    }
}
