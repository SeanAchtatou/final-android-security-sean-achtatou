package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DEREncodableVector;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.DERTaggedObject;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Holder implements DEREncodable {
    IssuerSerial baseCertificateID;
    GeneralNames entityName;
    ObjectDigestInfo objectDigestInfo;

    public static Holder getInstance(Object obj) {
        if (obj instanceof Holder) {
            return (Holder) obj;
        }
        if (obj instanceof ASN1Sequence) {
            return new Holder((ASN1Sequence) obj);
        }
        throw new IllegalArgumentException("unknown object in factory");
    }

    public Holder() {
    }

    public Holder(Node node) throws PKIException {
        NodeList nodelist = node.getChildNodes();
        if (nodelist.getLength() == 0) {
            throw new PKIException("XML内容缺失", "Holder没有子节点");
        }
        for (int i = 0; i < nodelist.getLength(); i++) {
            Node cnode = nodelist.item(i);
            String sname = cnode.getNodeName();
            if (sname.equals("BasecertificateID")) {
                this.baseCertificateID = new IssuerSerial(cnode);
            }
            if (sname.equals("EntityName")) {
                this.entityName = new GeneralNames(cnode);
            }
            if (sname.equals("Objectdigestinfo")) {
                this.objectDigestInfo = new ObjectDigestInfo(cnode);
            }
        }
    }

    public Holder(ASN1Sequence seq) {
        for (int i = 0; i != seq.size(); i++) {
            ASN1TaggedObject tObj = (ASN1TaggedObject) seq.getObjectAt(i);
            switch (tObj.getTagNo()) {
                case 0:
                    this.baseCertificateID = IssuerSerial.getInstance(tObj, false);
                    break;
                case 1:
                    this.entityName = GeneralNames.getInstance(tObj.getObject());
                    break;
                case 2:
                    this.objectDigestInfo = ObjectDigestInfo.getInstance(tObj, false);
                    break;
                default:
                    throw new IllegalArgumentException("unknown tag in Holder");
            }
        }
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        if (this.baseCertificateID != null) {
            v.add(new DERTaggedObject(false, 0, this.baseCertificateID));
        }
        if (this.entityName != null) {
            v.add(new DERTaggedObject(true, 1, this.entityName.getDERObject()));
        }
        if (this.objectDigestInfo != null) {
            v.add(new DERTaggedObject(false, 2, this.objectDigestInfo));
        }
        return new DERSequence(v);
    }

    public static Holder getInstance(String holder) throws PKIException {
        DEREncodableVector ve = new DEREncodableVector();
        ve.add(new GeneralName(new X509Name(holder)).getDERObject());
        return new Holder(new DERSequence(new DERTaggedObject(true, 1, new DERSequence(ve)).getDERObject()));
    }

    public String toString() {
        if (this.entityName == null) {
            return null;
        }
        String[] a = toStringArr();
        StringBuffer buf = new StringBuffer();
        for (String append : a) {
            if (buf.length() > 0) {
                buf.append("\n");
            }
            buf.append(append);
        }
        return buf.toString();
    }

    public String[] toStringArr() {
        if (this.entityName == null) {
            return null;
        }
        String[] names = new String[this.entityName.getNames().length];
        for (int i = 0; i < names.length; i++) {
            names[i] = this.entityName.getNames()[i].toString();
        }
        return names;
    }

    public GeneralNames getEntityName() {
        return this.entityName;
    }

    public void setEntityName(GeneralNames entityName2) {
        this.entityName = entityName2;
    }
}
