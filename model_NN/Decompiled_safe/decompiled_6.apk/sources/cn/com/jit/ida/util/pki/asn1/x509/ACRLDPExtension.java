package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.Parser;
import cn.com.jit.ida.util.pki.asn1.ASN1Encodable;
import cn.com.jit.ida.util.pki.asn1.DERBoolean;
import cn.com.jit.ida.util.pki.asn1.DEREncodableVector;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

public class ACRLDPExtension extends ASN1Encodable {
    private DERBoolean critical = new DERBoolean(false);
    private DERObjectIdentifier extnID = X509Extensions.ACRLDPExtension;
    private DEROctetString extnValue;
    private DEREncodableVector syntax = new DEREncodableVector();

    public ACRLDPExtension(Node nl) throws PKIException {
        NodeList nodelist = nl.getChildNodes();
        if (nodelist.getLength() == 0) {
            throw new PKIException("XML内容缺失,ACRLDPExtension.ACRLDPExtension,ACRLDPExtension没有子节点");
        }
        for (int i = 0; i < nodelist.getLength(); i++) {
            Node cnode = nodelist.item(i);
            if (cnode.getNodeName().equals("DistributionPoint")) {
                DistributionPoint dsp = new DistributionPoint();
                NodeList nldp = cnode.getChildNodes();
                if (nodelist.getLength() == 0) {
                    throw new PKIException("XML内容缺失,ACRLDPExtension.ACRLDPExtension, DistributionPoint没有子节点");
                }
                for (int j = 0; j < nldp.getLength(); j++) {
                    Node dp = nldp.item(j);
                    String sdp = dp.getNodeName();
                    if (sdp.equals("DistributionPointName")) {
                        NodeList nlgn = dp.getChildNodes();
                        if (nodelist.getLength() != 0) {
                            int k = 0;
                            while (true) {
                                if (k >= nlgn.getLength()) {
                                    break;
                                }
                                Node ndgname = nlgn.item(k);
                                if (ndgname.getNodeName().equals("GeneralName")) {
                                    String type = ndgname.getAttributes().item(0).getNodeValue();
                                    GeneralName gname = new GeneralName(ndgname);
                                    dsp.setDistributionPoint(new DistributionPointName(Integer.parseInt(type), gname.getDERObject()));
                                    break;
                                }
                                k++;
                            }
                        } else {
                            throw new PKIException("XML内容缺失,ACRLDPExtension.ACRLDPExtension,DistributionPointName没有子节点");
                        }
                    }
                    if (sdp.equals("reason")) {
                        try {
                            dsp.setReasons(new ReasonFlags(Integer.parseInt(((Text) dp.getFirstChild()).getData().trim()) ^ 2));
                        } catch (NumberFormatException e) {
                            throw new PKIException("NumberFormatException, ACRLDPExtension.ACRLDPExtension, 构造reason出错", e);
                        }
                    }
                    if (sdp.equals("CRLIssuer")) {
                        GeneralNames gnames1 = new GeneralNames();
                        NodeList nlgn1 = dp.getChildNodes();
                        if (nodelist.getLength() == 0) {
                            throw new PKIException("XML内容缺失,ACRLDPExtension.ACRLDPExtension,CRLIssuer没有子节点");
                        }
                        for (int l = 0; l < nlgn1.getLength(); l++) {
                            Node ndgname1 = nlgn1.item(l);
                            if (ndgname1.getNodeName().equals("GeneralName")) {
                                String type1 = ndgname1.getAttributes().item(0).getNodeValue();
                                String sdpname1 = ((Text) ndgname1.getFirstChild()).getData();
                                gnames1.addGeneralName(Integer.parseInt(type1), sdpname1.trim());
                            }
                        }
                        dsp.setCRLIssuer(gnames1);
                    }
                }
                this.syntax.add(dsp);
            }
        }
    }

    public DERObject toASN1Object() {
        DEREncodableVector seq = new DEREncodableVector();
        seq.add(this.extnID);
        seq.add(this.critical);
        DEREncodableVector seq1 = new DEREncodableVector();
        for (int i = 0; i < this.syntax.size(); i++) {
            seq1.add(((DistributionPoint) this.syntax.get(i)).toASN1Object());
        }
        byte[] arr = null;
        try {
            arr = Parser.writeDERObj2Bytes(new DERSequence(seq1).getDERObject());
        } catch (PKIException e) {
            e.printStackTrace();
        }
        this.extnValue = new DEROctetString(arr);
        seq.add(this.extnValue);
        return new DERSequence(seq);
    }

    public void decode(DERObject parm1) throws PKIException {
        throw new UnsupportedOperationException("Method decode() not yet implemented.");
    }
}
