package cn.com.jit.ida.util.pki.cipher;

public class JKeyPair {
    private JKey prvKey;
    private JKey pubKey;

    public JKeyPair(JKey pubKey2, JKey prvKey2) {
        this.pubKey = pubKey2;
        this.prvKey = prvKey2;
    }

    public JKey getPublicKey() {
        return this.pubKey;
    }

    public JKey getPrivateKey() {
        return this.prvKey;
    }
}
