package cn.com.jit.ida.util.pki.asn1;

import de.innosystec.unrar.unpack.decode.Compress;
import java.io.ByteArrayInputStream;
import java.io.EOFException;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class DERInputStream extends FilterInputStream implements DERTags {
    public DERInputStream(InputStream is) {
        super(is);
    }

    /* access modifiers changed from: protected */
    public int readLength() throws IOException {
        int length = read();
        if (length < 0) {
            throw new IOException("EOF found when length expected");
        } else if (length == 128) {
            return -1;
        } else {
            if (length > 127) {
                int size = length & 127;
                length = 0;
                for (int i = 0; i < size; i++) {
                    int next = read();
                    if (next < 0) {
                        throw new IOException("EOF found reading length");
                    }
                    length = (length << 8) + next;
                }
            }
            return length;
        }
    }

    /* access modifiers changed from: protected */
    public void readFully(byte[] bytes) throws IOException {
        int left = bytes.length;
        if (left != 0) {
            while (left > 0) {
                int l = read(bytes, bytes.length - left, left);
                if (l < 0) {
                    throw new EOFException("unexpected end of stream");
                }
                left -= l;
            }
        }
    }

    /* access modifiers changed from: protected */
    public DERObject buildObject(int tag, byte[] bytes) throws IOException {
        switch (tag) {
            case 1:
                return new DERBoolean(bytes);
            case 2:
                return new DERInteger(bytes);
            case 3:
                byte b = bytes[0];
                byte[] data = new byte[(bytes.length - 1)];
                System.arraycopy(bytes, 1, data, 0, bytes.length - 1);
                return new DERBitString(data, b);
            case 4:
                return new DEROctetString(bytes);
            case 5:
                return null;
            case 6:
                return new DERObjectIdentifier(bytes);
            case 10:
                return new DEREnumerated(bytes);
            case 12:
                return new DERUTF8String(bytes);
            case 19:
                return new DERPrintableString(bytes);
            case 20:
                return new DERT61String(bytes);
            case 22:
                return new DERIA5String(bytes);
            case 23:
                return new DERUTCTime(bytes);
            case 24:
                return new DERGeneralizedTime(bytes);
            case 26:
                return new DERVisibleString(bytes);
            case 27:
                return new DERGeneralString(bytes);
            case 28:
                return new DERUniversalString(bytes);
            case 30:
                return new DERBMPString(bytes);
            case Compress.DC20 /*48*/:
                BERInputStream dIn = new BERInputStream(new ByteArrayInputStream(bytes));
                DERConstructedSequence seq = new DERConstructedSequence();
                while (true) {
                    try {
                        seq.addObject(dIn.readObject());
                    } catch (EOFException e) {
                        return seq;
                    }
                }
            case 49:
                BERInputStream dIn2 = new BERInputStream(new ByteArrayInputStream(bytes));
                ASN1EncodableVector v = new ASN1EncodableVector();
                while (true) {
                    try {
                        v.add(dIn2.readObject());
                    } catch (EOFException e2) {
                        return new DERConstructedSet(v);
                    }
                }
            default:
                if ((tag & 128) == 0) {
                    return new DERUnknownTag(tag, bytes);
                }
                if ((tag & 31) == 31) {
                    throw new IOException("unsupported high tag encountered");
                } else if (bytes.length == 0) {
                    if ((tag & 32) == 0) {
                        return new DERTaggedObject(false, tag & 31, new DERNull());
                    }
                    return new DERTaggedObject(false, tag & 31, new DERConstructedSequence());
                } else if ((tag & 32) == 0) {
                    return new DERTaggedObject(false, tag & 31, new DEROctetString(bytes));
                } else {
                    BERInputStream dIn3 = new BERInputStream(new ByteArrayInputStream(bytes));
                    DEREncodable dObj = dIn3.readObject();
                    if (dIn3.available() == 0) {
                        return new DERTaggedObject(tag & 31, dObj);
                    }
                    DERConstructedSequence seq2 = new DERConstructedSequence();
                    seq2.addObject(dObj);
                    while (true) {
                        try {
                            seq2.addObject(dIn3.readObject());
                        } catch (EOFException e3) {
                            return new DERTaggedObject(false, tag & 31, seq2);
                        }
                    }
                }
        }
    }

    public DERObject readObject() throws IOException {
        int tag = read();
        if (tag == -1) {
            throw new EOFException();
        }
        byte[] bytes = new byte[readLength()];
        readFully(bytes);
        return buildObject(tag, bytes);
    }
}
