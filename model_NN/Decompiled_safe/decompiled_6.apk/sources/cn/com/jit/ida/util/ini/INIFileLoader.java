package cn.com.jit.ida.util.ini;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.StringTokenizer;

public class INIFileLoader {
    private static final String SPLIT_STR = "=";
    private HashMap section;

    public HashMap loadIniFromFile(File file) throws Exception {
        FileInputStream fis = new FileInputStream(file);
        HashMap result = loadIniFromStream(fis);
        fis.close();
        return result;
    }

    public HashMap loadIniFromStream(InputStream is) throws Exception {
        InputStreamReader ireader = new InputStreamReader(is);
        BufferedReader breader = new BufferedReader(ireader);
        this.section = new HashMap();
        String strSection = null;
        String strKey = null;
        String strLine = readLineIgnoreBlank(breader);
        if (strLine == null) {
            return this.section;
        }
        if (!isSection(strLine)) {
            throw new Exception("INI file's format error.");
        }
        while (strLine != null) {
            if (isSection(strLine)) {
                strSection = getSection(strLine);
                this.section.put(strSection, new HashMap());
            } else if (isKey(strLine)) {
                strKey = getKey(strLine);
                ((HashMap) this.section.get(strSection)).put(strKey, getValue(strLine));
            } else if (isContinueValue(strLine)) {
                StringBuffer buf = new StringBuffer();
                buf.append((String) ((HashMap) this.section.get(strSection)).get(strKey));
                buf.append(strLine.substring(2, strLine.length()));
                ((HashMap) this.section.get(strSection)).remove(strKey);
                ((HashMap) this.section.get(strSection)).put(strKey, buf.toString());
            } else {
                throw new Exception("INI file load error.");
            }
            strLine = readLineIgnoreBlank(breader);
        }
        breader.close();
        ireader.close();
        return this.section;
    }

    private String getSection(String str) {
        if (str != null && str.indexOf("[") == 0 && str.endsWith("]")) {
            return new StringTokenizer(str, "[]").nextToken();
        }
        return null;
    }

    private String getKey(String str) {
        if (str == null || str.indexOf("->") == 0) {
            return null;
        }
        return str.substring(0, str.indexOf(SPLIT_STR));
    }

    private String getValue(String str) {
        if (str == null) {
            return null;
        }
        return str.substring(str.indexOf(SPLIT_STR) + 1, str.length());
    }

    private boolean isSection(String str) {
        return getSection(str) != null;
    }

    private boolean isKey(String str) {
        return getKey(str) != null;
    }

    private boolean isContinueValue(String str) {
        return str.indexOf("->") == 0;
    }

    private String readLineIgnoreBlank(BufferedReader in) throws Exception {
        String result;
        do {
            result = in.readLine();
            if (result == null) {
                return null;
            }
        } while (result.trim().length() == 0);
        return result.trim();
    }
}
