package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.DERPrintableString;

public class JITCasteCode extends DERPrintableString {
    public JITCasteCode(byte[] string) {
        super(string);
    }

    public JITCasteCode(String string) {
        super(string);
    }

    public String getCasteCode() {
        return getString();
    }
}
