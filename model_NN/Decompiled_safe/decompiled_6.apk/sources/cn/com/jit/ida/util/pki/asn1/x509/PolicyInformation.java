package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.ASN1Encodable;
import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import java.util.Enumeration;

public class PolicyInformation extends ASN1Encodable {
    private DERObjectIdentifier policyIdentifier;
    private ASN1Sequence policyQualifiers;

    public PolicyInformation(ASN1Sequence seq) {
        this.policyIdentifier = (DERObjectIdentifier) seq.getObjectAt(0);
        if (seq.size() > 1) {
            this.policyQualifiers = (ASN1Sequence) seq.getObjectAt(1);
        }
    }

    public PolicyInformation(DERObjectIdentifier policyIdentifier2) {
        this.policyIdentifier = policyIdentifier2;
    }

    public PolicyInformation(DERObjectIdentifier policyIdentifier2, ASN1Sequence policyQualifiers2) {
        this.policyIdentifier = policyIdentifier2;
        this.policyQualifiers = policyQualifiers2;
    }

    public static PolicyInformation getInstance(Object obj) {
        if (obj == null || (obj instanceof PolicyInformation)) {
            return (PolicyInformation) obj;
        }
        return new PolicyInformation(ASN1Sequence.getInstance(obj));
    }

    public DERObjectIdentifier getPolicyIdentifier() {
        return this.policyIdentifier;
    }

    public ASN1Sequence getPolicyQualifiers() {
        return this.policyQualifiers;
    }

    public DERObject toASN1Object() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(this.policyIdentifier);
        if (this.policyQualifiers != null) {
            Enumeration e = this.policyQualifiers.getObjects();
            ASN1EncodableVector v1 = new ASN1EncodableVector();
            while (e.hasMoreElements()) {
                v1.add(((PolicyQualifierInfo) e.nextElement()).toASN1Object());
            }
            v.add(new DERSequence(v1));
        }
        return new DERSequence(v);
    }
}
