package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.ASN1Encodable;
import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1Set;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DEREncodableVector;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.DERSet;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

public class Attribute extends ASN1Encodable {
    public static final String CLEARANCE_ATTRIBUTE_OID = "2.5.1.5.55";
    public static final String GROUP_ATTRIBUTE_OID = "2.5.24.4";
    public static final String ROLE_ATTRIBUTE_OID = "2.5.4.72";
    private DERObjectIdentifier attrType;
    private DEREncodableVector attrValues;

    public Attribute() {
        this.attrValues = new DEREncodableVector();
    }

    public static Attribute getInstance(Object o) {
        if (o == null || (o instanceof Attribute)) {
            return (Attribute) o;
        }
        if (o instanceof ASN1Sequence) {
            return new Attribute((ASN1Sequence) o);
        }
        throw new IllegalArgumentException("unknown object in factory");
    }

    public Attribute(ASN1Sequence seq) {
        this.attrType = (DERObjectIdentifier) seq.getObjectAt(0);
        this.attrValues = new DEREncodableVector((ASN1Set) seq.getObjectAt(1));
    }

    public Attribute(DERObjectIdentifier attrType2, ASN1Set attrValues2) {
        this.attrType = attrType2;
        this.attrValues = new DEREncodableVector(attrValues2);
    }

    public DERObjectIdentifier getAttrType() {
        return this.attrType;
    }

    public ASN1Set getAttrValuesSet() {
        return new DERSet(this.attrValues);
    }

    public DEREncodableVector getAttrValues() {
        return this.attrValues;
    }

    public DERObject toASN1Object() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(this.attrType);
        v.add(getAttrValuesSet());
        return new DERSequence(v);
    }

    public int getAttributeValuesSize() {
        return this.attrValues.size();
    }

    public ASN1Encodable getValueObjAt(int i) {
        return (ASN1Encodable) this.attrValues.get(i);
    }

    /* access modifiers changed from: protected */
    public void setTttrType(DERObjectIdentifier type) {
        this.attrType = type;
    }

    /* access modifiers changed from: protected */
    public void setTttrType(String oid) {
        this.attrType = new DERObjectIdentifier(oid);
    }

    public void setAttrValues(ASN1Set values) {
        this.attrValues = new DEREncodableVector(values);
    }

    public void setAttrValues(DEREncodableVector attrValues2) {
        this.attrValues = attrValues2;
    }

    /* access modifiers changed from: protected */
    public void addAttValueObject(DEREncodable obj) {
        if (this.attrValues == null) {
            this.attrValues = new DEREncodableVector();
        }
        this.attrValues.add(obj);
    }

    public Attribute(Node nl) throws PKIException {
        NodeList nodelist = nl.getChildNodes();
        if (nodelist.getLength() == 0) {
            throw new PKIException("XML内容缺失,Attribute.Attribute(),Attribute没有子节点");
        }
        for (int i = 0; i < nodelist.getLength(); i++) {
            Node cnode = nodelist.item(i);
            String sname = cnode.getNodeName();
            if (sname.equals("type")) {
                this.attrType = new DERObjectIdentifier(((Text) cnode.getFirstChild()).getData().trim());
            }
            if (sname.equals("attrvalues")) {
                this.attrValues = generateAttrvalues(cnode);
            }
        }
    }

    public DEREncodableVector generateAttrvalues(Node nl) throws PKIException {
        DEREncodableVector vector = new DEREncodableVector();
        NodeList nodelist = nl.getChildNodes();
        if (nodelist.getLength() == 0) {
            throw new PKIException("XML内容缺失,Attrvalues,属性值集合没有子节点");
        }
        for (int i = 0; i < nodelist.getLength(); i++) {
            Node cnode = nodelist.item(i);
            String sname = cnode.getNodeName();
            if (sname.equals("RoleAttribute")) {
                vector.add(new RoleSyntax(cnode));
            }
            if (sname.equals("ClearanceAttribute")) {
                vector.add(new ClearanceSyntax(cnode));
            }
            if (sname.equals("GroupAttribute")) {
                vector.add(new IetfAttrSyntax(cnode));
            }
        }
        return vector;
    }
}
