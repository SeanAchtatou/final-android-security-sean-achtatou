package cn.com.jit.ida.util.pki.cipher.softsm;

import android.support.v4.view.MotionEventCompat;
import java.math.BigInteger;
import java.security.SecureRandom;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.generators.ECKeyPairGenerator;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.crypto.params.ECKeyGenerationParameters;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.math.ec.ECCurve;
import org.bouncycastle.math.ec.ECFieldElement;
import org.bouncycastle.math.ec.ECPoint;

public class SM2 {
    public static final String[] sm2_param = {"FFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000FFFFFFFFFFFFFFFF", "FFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000FFFFFFFFFFFFFFFC", "28E9FA9E9D9F5E344D5A9E4BCF6509A7F39789F515AB8F92DDBCBD414D940E93", "FFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFF7203DF6B21C6052B53BBF40939D54123", "32C4AE2C1F1981195F9904466A39C9948FE30BBFF2660BE1715A4589334C74C7", "BC3736A2F4F6779C59BDCEE36B692153D0A9877CC62A474002DF32E52139F0A0"};
    public static final String[] sm2_test_param = {"8542D69E4C044F18E8B92435BF6FF7DE457283915C45517D722EDB8B08F1DFC3", "787968B4FA32C3FD2417842E73BBFEFF2F3C848B6831D7E0EC65228B3937E498", "63E4C6D3B23B0C849CF84241484BFE48F61D59A5B16BA06E6E12D1DA27C5249A", "8542D69E4C044F18E8B92435BF6FF7DD297720630485628D5AE74EE7C32E79B7", "421DEBD61B62EAB6746434EBC3CC315E32220B3BADD50BDC4C4E6C147FEDD43D", "0680512BCBB42C07D47349D2153B70C4E5D7FDFCBFA36EA1A85841B9E46E09A2"};
    public final BigInteger ecc_a;
    public final BigInteger ecc_b;
    public final ECDomainParameters ecc_bc_spec;
    public final ECCurve ecc_curve;
    public final BigInteger ecc_gx;
    public final BigInteger ecc_gy;
    public final ECKeyPairGenerator ecc_key_pair_generator;
    public final BigInteger ecc_n;
    public final BigInteger ecc_p;
    public String[] ecc_param = sm2_test_param;
    public final ECPoint ecc_point_g;
    public boolean sm2Test = false;

    public static SM2 Instance() {
        return new SM2(false);
    }

    public static SM2 InstanceTest() {
        return new SM2(true);
    }

    private SM2(boolean sm2Test2) {
        this.sm2Test = sm2Test2;
        if (sm2Test2) {
            this.ecc_param = sm2_test_param;
        } else {
            this.ecc_param = sm2_param;
        }
        this.ecc_p = new BigInteger(this.ecc_param[0], 16);
        this.ecc_a = new BigInteger(this.ecc_param[1], 16);
        this.ecc_b = new BigInteger(this.ecc_param[2], 16);
        this.ecc_n = new BigInteger(this.ecc_param[3], 16);
        this.ecc_gx = new BigInteger(this.ecc_param[4], 16);
        this.ecc_gy = new BigInteger(this.ecc_param[5], 16);
        ECFieldElement ecc_gx_fieldelement = new ECFieldElement.Fp(this.ecc_p, this.ecc_gx);
        ECFieldElement ecc_gy_fieldelement = new ECFieldElement.Fp(this.ecc_p, this.ecc_gy);
        this.ecc_curve = new ECCurve.Fp(this.ecc_p, this.ecc_a, this.ecc_b);
        this.ecc_point_g = new ECPoint.Fp(this.ecc_curve, ecc_gx_fieldelement, ecc_gy_fieldelement);
        this.ecc_bc_spec = new ECDomainParameters(this.ecc_curve, this.ecc_point_g, this.ecc_n);
        ECKeyGenerationParameters ecc_ecgenparam = new ECKeyGenerationParameters(this.ecc_bc_spec, new SecureRandom());
        this.ecc_key_pair_generator = new ECKeyPairGenerator();
        this.ecc_key_pair_generator.init(ecc_ecgenparam);
    }

    public byte[] Sm2GetZ(byte[] userId, ECPoint userKey) {
        SM3Digest sm3 = new SM3Digest();
        int len = userId.length * 8;
        sm3.update((byte) ((len >> 8) & MotionEventCompat.ACTION_MASK));
        sm3.update((byte) (len & MotionEventCompat.ACTION_MASK));
        sm3.BlockUpdate(userId, 0, userId.length);
        byte[] p = Util.byteconvert32(this.ecc_a);
        sm3.BlockUpdate(p, 0, p.length);
        byte[] p2 = Util.byteconvert32(this.ecc_b);
        sm3.BlockUpdate(p2, 0, p2.length);
        byte[] p3 = Util.byteconvert32(this.ecc_gx);
        sm3.BlockUpdate(p3, 0, p3.length);
        byte[] p4 = Util.byteconvert32(this.ecc_gy);
        sm3.BlockUpdate(p4, 0, p4.length);
        byte[] p5 = Util.byteconvert32(userKey.getX().toBigInteger());
        sm3.BlockUpdate(p5, 0, p5.length);
        byte[] p6 = Util.byteconvert32(userKey.getY().toBigInteger());
        sm3.BlockUpdate(p6, 0, p6.length);
        byte[] md = new byte[sm3.getDigestSize()];
        sm3.doFinal(md, 0);
        return md;
    }

    public void Sm2Sign(byte[] md, BigInteger userD, ECPoint userKey, SM2Result sm2Ret) {
        BigInteger k;
        ECPoint kp;
        BigInteger e = new BigInteger(1, md);
        while (true) {
            if (!this.sm2Test) {
                AsymmetricCipherKeyPair keypair = this.ecc_key_pair_generator.generateKeyPair();
                k = keypair.getPrivate().getD();
                kp = keypair.getPublic().getQ();
            } else {
                k = new BigInteger("6CB28D99385C175C94F94E934817663FC176D925DD72B727260DBAAE1FB2F96F", 16);
                kp = this.ecc_point_g.multiply(k);
            }
            BigInteger r = e.add(kp.getX().toBigInteger()).mod(this.ecc_n);
            if (!r.equals(BigInteger.ZERO) && !r.add(k).equals(this.ecc_n)) {
                BigInteger s = userD.add(BigInteger.ONE).modInverse(this.ecc_n).multiply(k.subtract(r.multiply(userD))).mod(this.ecc_n);
                if (!s.equals(BigInteger.ZERO)) {
                    sm2Ret.r = r;
                    sm2Ret.s = s;
                    return;
                }
            }
        }
    }

    public void Sm2Verify(byte[] md, ECPoint userKey, BigInteger r, BigInteger s, SM2Result sm2Ret) {
        sm2Ret.R = null;
        BigInteger e = new BigInteger(1, md);
        BigInteger t = r.add(s).mod(this.ecc_n);
        if (!t.equals(BigInteger.ZERO)) {
            sm2Ret.R = e.add(this.ecc_point_g.multiply(sm2Ret.s).add(userKey.multiply(t)).getX().toBigInteger()).mod(this.ecc_n);
        }
    }

    public static void main(String[] args) {
        BigInteger userD;
        ECPoint userKey;
        SM2 sm2 = Instance();
        boolean sm2Test2 = sm2.sm2Test;
        if (sm2Test2) {
            System.out.println("p = " + sm2.ecc_p.toString(16));
            System.out.println("a = " + sm2.ecc_a.toString(16));
            System.out.println("b = " + sm2.ecc_b.toString(16));
            System.out.println("n = " + sm2.ecc_n.toString(16));
            System.out.println("gx= " + sm2.ecc_gx.toString(16));
            System.out.println("gy= " + sm2.ecc_gy.toString(16));
            System.out.println("h = " + sm2.ecc_bc_spec.getH().toString(16));
        }
        if (!sm2Test2) {
            System.out.println("密钥对产生 开始时间 = " + System.currentTimeMillis());
            AsymmetricCipherKeyPair keypair = null;
            for (int i = 0; i < 1; i++) {
                keypair = sm2.ecc_key_pair_generator.generateKeyPair();
            }
            System.out.println("密钥对产生 结束时间 = " + System.currentTimeMillis());
            ECPrivateKeyParameters ecpriv = keypair.getPrivate();
            ECPublicKeyParameters ecpub = keypair.getPublic();
            System.out.println("密钥对(D,x,y)= \n" + ecpriv.getD().toString(16));
            System.out.println(ecpub.getQ().getX().toBigInteger().toString(16));
            System.out.println(ecpub.getQ().getY().toBigInteger().toString(16));
            userD = ecpriv.getD();
            userKey = ecpub.getQ();
        } else {
            userD = new BigInteger("128B2FA8BD433C6C068C8D803DFF79792A519A55171B1B650C23661D15897263", 16);
            System.out.println("测试密钥对(D,x,y) = \n" + userD.toString(16));
            userKey = sm2.ecc_point_g.multiply(userD);
            System.out.println(userKey.getX().toBigInteger().toString(16));
            System.out.println(userKey.getY().toBigInteger().toString(16));
        }
        SM2Result sm2Ret = new SM2Result();
        SM3Digest sm3 = new SM3Digest();
        byte[] z = sm2.Sm2GetZ("1234567812345678".getBytes(), userKey);
        sm3.BlockUpdate(z, 0, z.length);
        byte[] p = "message digest".getBytes();
        sm3.BlockUpdate(p, 0, p.length);
        byte[] md = new byte[32];
        sm3.doFinal(md, 0);
        sm2.Sm2Sign(md, userD, userKey, sm2Ret);
        sm2.Sm2Verify(md, userKey, sm2Ret.r, sm2Ret.s, sm2Ret);
        System.out.println("签名结果(r,s) = \n" + sm2Ret.r.toString(16));
        System.out.println(sm2Ret.s.toString(16));
        System.out.println("验签结果 R = \n" + sm2Ret.R.toString(16));
    }
}
