package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject;
import cn.com.jit.ida.util.pki.asn1.DERBitString;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERTaggedObject;
import cn.com.jit.ida.util.pki.asn1.pkcs.PKCSObjectIdentifiers;

public class TBSCertificateStructure implements DEREncodable, X509ObjectIdentifiers, PKCSObjectIdentifiers {
    Time endDate;
    X509Extensions extensions;
    X509Name issuer;
    DERBitString issuerUniqueId;
    ASN1Sequence seq;
    DERInteger serialNumber;
    AlgorithmIdentifier signature;
    Time startDate;
    X509Name subject;
    SubjectPublicKeyInfo subjectPublicKeyInfo;
    DERBitString subjectUniqueId;
    DERInteger version;

    public static TBSCertificateStructure getInstance(ASN1TaggedObject obj, boolean explicit) {
        return getInstance(ASN1Sequence.getInstance(obj, explicit));
    }

    public static TBSCertificateStructure getInstance(Object obj) {
        if (obj instanceof TBSCertificateStructure) {
            return (TBSCertificateStructure) obj;
        }
        if (obj instanceof ASN1Sequence) {
            return new TBSCertificateStructure((ASN1Sequence) obj);
        }
        throw new IllegalArgumentException("unknown object in factory");
    }

    public TBSCertificateStructure(ASN1Sequence seq2) {
        int seqStart = 0;
        this.seq = seq2;
        if (seq2.getObjectAt(0) instanceof DERTaggedObject) {
            this.version = DERInteger.getInstance(seq2.getObjectAt(0));
        } else {
            seqStart = -1;
            this.version = new DERInteger(0);
        }
        this.serialNumber = DERInteger.getInstance(seq2.getObjectAt(seqStart + 1));
        this.signature = AlgorithmIdentifier.getInstance(seq2.getObjectAt(seqStart + 2));
        this.issuer = X509Name.getInstance(seq2.getObjectAt(seqStart + 3));
        ASN1Sequence dates = (ASN1Sequence) seq2.getObjectAt(seqStart + 4);
        this.startDate = Time.getInstance(dates.getObjectAt(0));
        this.endDate = Time.getInstance(dates.getObjectAt(1));
        this.subject = X509Name.getInstance(seq2.getObjectAt(seqStart + 5));
        this.subjectPublicKeyInfo = SubjectPublicKeyInfo.getInstance(seq2.getObjectAt(seqStart + 6));
        for (int extras = (seq2.size() - (seqStart + 6)) - 1; extras > 0; extras--) {
            DERTaggedObject extra = (DERTaggedObject) seq2.getObjectAt(seqStart + 6 + extras);
            switch (extra.getTagNo()) {
                case 1:
                    this.issuerUniqueId = DERBitString.getInstance(extra);
                    break;
                case 2:
                    this.subjectUniqueId = DERBitString.getInstance(extra);
                    break;
                case 3:
                    this.extensions = X509Extensions.getInstance(extra);
                    break;
            }
        }
    }

    public int getVersion() {
        return this.version.getValue().intValue() + 1;
    }

    public DERInteger getVersionNumber() {
        return this.version;
    }

    public DERInteger getSerialNumber() {
        return this.serialNumber;
    }

    public AlgorithmIdentifier getSignature() {
        return this.signature;
    }

    public X509Name getIssuer() {
        return this.issuer;
    }

    public Time getStartDate() {
        return this.startDate;
    }

    public Time getEndDate() {
        return this.endDate;
    }

    public X509Name getSubject() {
        return this.subject;
    }

    public SubjectPublicKeyInfo getSubjectPublicKeyInfo() {
        return this.subjectPublicKeyInfo;
    }

    public DERBitString getIssuerUniqueId() {
        return this.issuerUniqueId;
    }

    public DERBitString getSubjectUniqueId() {
        return this.subjectUniqueId;
    }

    public X509Extensions getExtensions() {
        return this.extensions;
    }

    public DERObject getDERObject() {
        return this.seq;
    }
}
