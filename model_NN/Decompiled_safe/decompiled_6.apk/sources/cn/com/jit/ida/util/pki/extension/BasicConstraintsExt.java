package cn.com.jit.ida.util.pki.extension;

import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DERBoolean;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.x509.BasicConstraints;
import cn.com.jit.ida.util.pki.asn1.x509.X509Extensions;

public class BasicConstraintsExt extends AbstractStandardExtension {
    private boolean IsHavePathLenConstraint = false;
    private boolean isCA = false;
    private int pathLenConstraint = -1;

    public BasicConstraintsExt(boolean isCA2) {
        this.OID = X509Extensions.BasicConstraints.getId();
        this.critical = false;
        this.isCA = isCA2;
    }

    public BasicConstraintsExt(boolean isCA2, int pathLenConstraint2) {
        this.OID = X509Extensions.BasicConstraints.getId();
        this.critical = false;
        this.isCA = isCA2;
        this.pathLenConstraint = pathLenConstraint2;
        this.IsHavePathLenConstraint = true;
    }

    public BasicConstraintsExt(ASN1Sequence asn1Sequence) {
        if (asn1Sequence != null && asn1Sequence.size() != 0) {
            this.isCA = ((DERBoolean) asn1Sequence.getObjectAt(0)).isTrue();
            if (asn1Sequence.size() > 1) {
                this.IsHavePathLenConstraint = true;
                this.pathLenConstraint = ((DERInteger) asn1Sequence.getObjectAt(1)).getValue().intValue();
            }
        }
    }

    public boolean isHavePathLenConstraint() {
        return this.IsHavePathLenConstraint;
    }

    public boolean getIsCA() {
        return this.isCA;
    }

    public int getPathLenConstraint() {
        return this.pathLenConstraint;
    }

    public String getOID() {
        return this.OID;
    }

    public void setCritical(boolean critical) {
        this.critical = critical;
    }

    public boolean getCritical() {
        return this.critical;
    }

    public byte[] encode() {
        BasicConstraints bc;
        if (this.pathLenConstraint != -1) {
            bc = new BasicConstraints(this.isCA, this.pathLenConstraint);
        } else {
            bc = new BasicConstraints(this.isCA);
        }
        return new DEROctetString(bc.getDERObject()).getOctets();
    }
}
