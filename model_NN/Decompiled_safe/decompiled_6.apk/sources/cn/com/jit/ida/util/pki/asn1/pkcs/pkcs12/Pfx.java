package cn.com.jit.ida.util.pki.asn1.pkcs.pkcs12;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.BERSequence;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.pkcs.PKCSObjectIdentifiers;
import cn.com.jit.ida.util.pki.asn1.pkcs.pkcs7.ContentInfo;

public class Pfx implements DEREncodable, PKCSObjectIdentifiers {
    private ContentInfo contentInfo;
    private MacData macData = null;

    public static Pfx getInstance(Object o) {
        if (o == null || (o instanceof Pfx)) {
            return (Pfx) o;
        }
        if (o instanceof ASN1Sequence) {
            return new Pfx((ASN1Sequence) o);
        }
        throw new IllegalArgumentException("unknown object in factory " + o.getClass().getName());
    }

    public Pfx(ASN1Sequence seq) {
        if (((DERInteger) seq.getObjectAt(0)).getValue().intValue() != 3) {
            throw new IllegalArgumentException("wrong version for PFX PDU");
        }
        this.contentInfo = ContentInfo.getInstance(seq.getObjectAt(1));
        if (seq.size() == 3) {
            this.macData = MacData.getInstance(seq.getObjectAt(2));
        }
    }

    public Pfx(ContentInfo contentInfo2, MacData macData2) {
        this.contentInfo = contentInfo2;
        this.macData = macData2;
    }

    public ContentInfo getAuthSafe() {
        return this.contentInfo;
    }

    public MacData getMacData() {
        return this.macData;
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(new DERInteger(3));
        v.add(this.contentInfo);
        if (this.macData != null) {
            v.add(this.macData);
        }
        return new BERSequence(v);
    }
}
