package cn.com.jit.ida.util.pki.crl;

import cn.com.jit.ida.util.pki.PKIConstant;
import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.Parser;
import cn.com.jit.ida.util.pki.asn1.ASN1InputStream;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.pkcs.PKCSObjectIdentifiers;
import cn.com.jit.ida.util.pki.asn1.x509.CertificateList;
import cn.com.jit.ida.util.pki.asn1.x509.TBSCertList;
import cn.com.jit.ida.util.pki.asn1.x509.Time;
import cn.com.jit.ida.util.pki.asn1.x509.X509Name;
import cn.com.jit.ida.util.pki.cert.X509Cert;
import cn.com.jit.ida.util.pki.cipher.JCrypto;
import cn.com.jit.ida.util.pki.cipher.JKey;
import cn.com.jit.ida.util.pki.cipher.Mechanism;
import cn.com.jit.ida.util.pki.cipher.Session;
import cn.com.jit.ida.util.pki.encoders.Base64;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Date;

public class X509CRL {
    private CertificateList certList = null;
    private TBSCertList.CRLEntry[] crlEntries = null;

    public X509CRL(byte[] derCRL) throws PKIException {
        try {
            if (Parser.isBase64Encode(derCRL)) {
                derCRL = Parser.convertBase64(derCRL);
                byte[] derData = Base64.decode(derCRL);
            }
            this.certList = new CertificateList((ASN1Sequence) new ASN1InputStream(new ByteArrayInputStream(derCRL)).readObject());
            this.crlEntries = this.certList.getTBSCertList().getRevokedCertificates();
        } catch (Exception ex) {
            throw new PKIException(PKIException.INIT_CRL, PKIException.INIT_CRL_DES, ex);
        } catch (Throwable th) {
            throw new PKIException(PKIException.INIT_CRL, PKIException.INIT_CRL_DES);
        }
    }

    public X509CRL(CertificateList certList2) {
        this.certList = certList2;
        this.crlEntries = certList2.getTBSCertList().getRevokedCertificates();
    }

    public X509CRL(InputStream is) throws PKIException {
        try {
            this.certList = new CertificateList((ASN1Sequence) new ASN1InputStream(is).readObject());
            this.crlEntries = this.certList.getTBSCertList().getRevokedCertificates();
        } catch (Exception ex) {
            throw new PKIException(PKIException.INIT_CRL, PKIException.INIT_CRL_DES, ex);
        } catch (Throwable th) {
            throw new PKIException(PKIException.INIT_CRL, PKIException.INIT_CRL_DES);
        }
    }

    public CertificateList getCertificateList() {
        return this.certList;
    }

    public byte[] getEncoded() throws PKIException {
        try {
            return Parser.writeDERObj2Bytes(this.certList);
        } catch (Exception ex) {
            throw new PKIException(PKIException.ENCODED_CRL, PKIException.ENCODED_CRL_DES, ex);
        }
    }

    public int getVersion() {
        return this.certList.getVersion();
    }

    public String getIssuer() {
        return this.certList.getIssuer().toString().trim();
    }

    public X509Name getX509NameIssuer() {
        return this.certList.getIssuer();
    }

    public String getSignatureAlgName() {
        DERObjectIdentifier oid = this.certList.getSignatureAlgorithm().getObjectId();
        if (!PKIConstant.oid2SigAlgName.containsKey(oid)) {
            return getSignatureAlgOID();
        }
        return (String) PKIConstant.oid2SigAlgName.get(oid);
    }

    public String getSignatureAlgOID() {
        return this.certList.getSignatureAlgorithm().getObjectId().getId();
    }

    public Date getThisUpdate() {
        return this.certList.getThisUpdate().getDate();
    }

    public Date getNextUpdate() {
        Time nextUpdate = this.certList.getNextUpdate();
        if (nextUpdate == null) {
            return null;
        }
        return nextUpdate.getDate();
    }

    public byte[] getSignature() {
        return this.certList.getSignature().getBytes();
    }

    public byte[] getTBSCertList() throws PKIException {
        try {
            return Parser.writeDERObj2Bytes(this.certList.getTBSCertList().getDERObject());
        } catch (Exception e) {
            throw new PKIException(PKIException.TBSCRL_BYTES, PKIException.TBSCRL_BYTES_DES, e);
        }
    }

    public boolean isRevoke(String certSerialNumber) {
        if (this.crlEntries == null) {
            return false;
        }
        BigInteger sn = new BigInteger(certSerialNumber, 16);
        for (TBSCertList.CRLEntry userCertificate : this.crlEntries) {
            if (sn.equals(userCertificate.getUserCertificate().getValue())) {
                return true;
            }
        }
        return false;
    }

    public boolean isRevoke(BigInteger certSerialNumber) {
        if (this.crlEntries == null) {
            return false;
        }
        for (TBSCertList.CRLEntry userCertificate : this.crlEntries) {
            if (certSerialNumber.equals(userCertificate.getUserCertificate().getValue())) {
                return true;
            }
        }
        return false;
    }

    public boolean isRevoke(X509Cert cert) {
        if (this.crlEntries == null) {
            return false;
        }
        BigInteger sn = cert.getSerialNumber();
        for (TBSCertList.CRLEntry userCertificate : this.crlEntries) {
            if (sn.equals(userCertificate.getUserCertificate().getValue())) {
                return true;
            }
        }
        return false;
    }

    public boolean verify(JKey pubKey, Session session) throws PKIException {
        Mechanism mechanism;
        DERObjectIdentifier oid = this.certList.getSignatureAlgorithm().getObjectId();
        if (oid.equals(PKCSObjectIdentifiers.md2WithRSAEncryption)) {
            mechanism = new Mechanism("MD2withRSAEncryption");
        } else if (oid.equals(PKCSObjectIdentifiers.md5WithRSAEncryption)) {
            mechanism = new Mechanism("MD5withRSAEncryption");
        } else if (oid.equals(PKCSObjectIdentifiers.sha1WithRSAEncryption) || oid.equals(PKCSObjectIdentifiers.sha1WithRSAEncryption_v1)) {
            mechanism = new Mechanism("SHA1withRSAEncryption");
        } else if (oid.equals(PKCSObjectIdentifiers.sha1WithECEncryption)) {
            mechanism = new Mechanism("SHA1withECDSA");
        } else if (oid.equals(PKCSObjectIdentifiers.sha1WithDSA)) {
            mechanism = new Mechanism("SHA1withDSA");
        } else if (oid.equals(PKCSObjectIdentifiers.sm2_with_sm3)) {
            mechanism = new Mechanism("SM3withSM2Encryption");
        } else {
            throw new PKIException(PKIException.NONSUPPORT_SIGALG, "不支持的签名算法:" + oid.getId());
        }
        try {
            return session.verifySign(mechanism, pubKey, getTBSCertList(), getSignature());
        } catch (Exception ex) {
            throw new PKIException("6", PKIException.VERIFY_SIGN_DES, ex);
        }
    }

    public static void main(String[] args) {
        try {
            FileInputStream fin = new FileInputStream("D:/hebca.crl");
            byte[] crldata = new byte[fin.available()];
            fin.read(crldata);
            fin.close();
            X509CRL crl = new X509CRL(crldata);
            System.out.println("to read crl success...");
            JCrypto jcrypto = JCrypto.getInstance();
            jcrypto.initialize(JCrypto.JSOFT_LIB, null);
            jcrypto.initialize(JCrypto.JSJY05B_LIB, "PKITOOL");
            Session session = jcrypto.openSession(JCrypto.JSJY05B_LIB, "PKITOOL");
            CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
            FileInputStream fis = new FileInputStream("d:\\sm2.cer");
            fis.close();
            boolean verify = crl.verify(new X509Cert(((X509Certificate) certFactory.generateCertificate(fis)).getEncoded()).getPublicKey(), session);
            String issuer = crl.getIssuer();
            String signatureAlgName = crl.getSignatureAlgName();
            Date thisUpdate = crl.getThisUpdate();
            boolean isRevoke = crl.isRevoke("a11111111111111");
            System.out.println("to verify crl success...");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("to test error..." + e.getMessage());
        }
    }
}
