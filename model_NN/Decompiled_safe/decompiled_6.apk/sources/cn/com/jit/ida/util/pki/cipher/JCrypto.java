package cn.com.jit.ida.util.pki.cipher;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.PKIToolConfig;
import cn.com.jit.ida.util.pki.cipher.lib.JCaviumLib;
import cn.com.jit.ida.util.pki.cipher.lib.JHARDLib;
import cn.com.jit.ida.util.pki.cipher.lib.JSoftLib;
import java.security.Security;
import java.util.Hashtable;
import org.bouncycastle.asn1.x9.X9ObjectIdentifiers;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class JCrypto {
    public static final int CKC_ID_30 = 5;
    public static final int CKC_ID_56 = 6;
    public static final int CKC_ID_EKEY = 8;
    public static final int CKC_ID_MIKEY = 7;
    public static final int CKC_ID_SJY03Card = 3;
    public static final int CKC_ID_SJY05PC = 2;
    public static final int CKC_ID_SOFTCRY = 1;
    public static final int CKC_ID_TIMECOS = 4;
    public static final String JSJY05B_LIB = "JSJY05B_LIB";
    public static final String JSOFT_LIB = "JSOFT_LIB";
    private static JCrypto jCrypto = null;
    private Hashtable htable = new Hashtable();

    private JCrypto() {
    }

    public static synchronized JCrypto getInstance() {
        JCrypto jCrypto2;
        synchronized (JCrypto.class) {
            if (jCrypto == null) {
                jCrypto = new JCrypto();
                jCrypto2 = jCrypto;
            } else {
                jCrypto2 = jCrypto;
            }
        }
        return jCrypto2;
    }

    public synchronized boolean initialize(String deviceName, Object param) throws PKIException {
        if (param == null) {
            param = "PKITOOL";
        }
        try {
            String hash = deviceName + param.toString();
            if (!this.htable.containsKey(hash)) {
                if (deviceName.equals(JSOFT_LIB)) {
                    String vmVender = System.getProperty("java.vm.vendor");
                    String vmVersion = System.getProperty("java.vm.version");
                    if (vmVender.toUpperCase().indexOf("IBM") == -1 && vmVersion.indexOf("1.5") == -1) {
                        Security.insertProviderAt(new BouncyCastleProvider(), 2);
                    } else {
                        Security.addProvider(new BouncyCastleProvider());
                    }
                    Security.getProvider("BC").remove("Alg.Alias.KeyFactory." + X9ObjectIdentifiers.id_ecPublicKey);
                    PKIToolConfig CfgTag = new PKIToolConfig();
                    CfgTag.LoadOpt("PKITOOL");
                    JSoftLib soft = new JSoftLib();
                    soft.setCfgTag(CfgTag);
                    this.htable.put(hash, soft);
                } else if (deviceName.equals(JSJY05B_LIB)) {
                    PKIToolConfig CfgTag2 = new PKIToolConfig();
                    if (param == null) {
                        CfgTag2.LoadOpt("PKITOOL");
                    } else {
                        CfgTag2.LoadOpt(param.toString());
                    }
                    if (new String(CfgTag2.getJniUse()).equals("JNI8")) {
                        String vmVender2 = System.getProperty("java.vm.vendor");
                        String vmVersion2 = System.getProperty("java.vm.version");
                        if (vmVender2.toUpperCase().indexOf("IBM") == -1 && vmVersion2.indexOf("1.5") == -1) {
                            Security.insertProviderAt(new BouncyCastleProvider(), 2);
                        } else {
                            Security.addProvider(new BouncyCastleProvider());
                        }
                        this.htable.put(deviceName, new JCaviumLib());
                    } else {
                        JHARDLib lib = new JHARDLib();
                        lib.setCfgTag(CfgTag2);
                        if (param == null) {
                            lib.Initialize("PKITOOL");
                            this.htable.put(hash, lib);
                        } else {
                            lib.Initialize(param.toString());
                            this.htable.put(hash, lib);
                        }
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new PKIException("8100", "初始化加密设备失败 " + deviceName, ex);
        }
        return true;
    }

    public boolean finalize(String deviceName, Object param) throws PKIException {
        JHARDLib sjyLib;
        if (param == null) {
            param = "PKITOOL";
        }
        try {
            String hash = deviceName + param.toString();
            if (!deviceName.equals(JSOFT_LIB) && deviceName.equals(JSJY05B_LIB)) {
                if (param == null) {
                    sjyLib = (JHARDLib) this.htable.get(hash);
                } else {
                    sjyLib = (JHARDLib) this.htable.get(hash);
                }
                if (sjyLib != null) {
                    sjyLib.Finalize();
                }
                return true;
            }
            if (param == null) {
                this.htable.remove(hash);
            } else {
                this.htable.remove(hash);
            }
            return true;
        } catch (Exception ex) {
            throw new PKIException("8101", "卸载加密设备失败 " + deviceName, ex);
        }
    }

    public Session openSession(String deviceName) throws PKIException {
        return openSession(deviceName, null);
    }

    public Session openSession(String deviceName, String CfgName) throws PKIException {
        Session session;
        if (CfgName == null) {
            CfgName = "PKITOOL";
        }
        String hash = deviceName + CfgName;
        if (deviceName.equals(JSOFT_LIB)) {
            session = (Session) this.htable.get(hash);
        } else {
            session = (Session) this.htable.get(hash);
        }
        if (session != null) {
            return session;
        }
        throw new PKIException("8102", "加密会话未进行初始化 " + deviceName);
    }

    public static void main(String[] args) throws Exception {
        getInstance().initialize(JSJY05B_LIB, null);
        System.out.println("success...");
    }
}
