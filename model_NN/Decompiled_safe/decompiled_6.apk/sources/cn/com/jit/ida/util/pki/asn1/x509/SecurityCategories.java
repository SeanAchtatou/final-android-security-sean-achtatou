package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject;
import cn.com.jit.ida.util.pki.asn1.DEREncodableVector;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERSet;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class SecurityCategories {
    private DEREncodableVector set;

    public SecurityCategories() {
        this.set = null;
        this.set = new DEREncodableVector();
    }

    public static SecurityCategories getInstance(Object obj) {
        if (obj == null || (obj instanceof SecurityCategories)) {
            return (SecurityCategories) obj;
        }
        if (obj instanceof DERSet) {
            return new SecurityCategories((DERSet) obj);
        }
        throw new IllegalArgumentException("illegal object in getInstance: " + obj.getClass().getName());
    }

    public SecurityCategories(DERSet derset) {
        this.set = null;
        this.set = new DEREncodableVector(derset);
    }

    public SecurityCategories(DEREncodableVector vec) {
        this.set = null;
        this.set = vec;
    }

    public static SecurityCategories getInstance(ASN1TaggedObject obj, boolean explicit) {
        return getInstance(obj.getObject());
    }

    public DERObject getDERObject() {
        return new DERSet(this.set);
    }

    public int getSize() {
        return this.set.size();
    }

    public SecurityCategory getSecurityCategoryAt(int i) {
        return (SecurityCategory) this.set.get(i);
    }

    public SecurityCategory creatSecurityCategory() {
        SecurityCategory a = new SecurityCategory();
        addSecurityCategory(a);
        return a;
    }

    public void addSecurityCategory(SecurityCategory sec) {
        if (this.set == null) {
            this.set = new DEREncodableVector();
        }
        this.set.add(sec.getDERObject());
    }

    public SecurityCategories(Node nl) throws PKIException {
        this.set = null;
        NodeList nodelist = nl.getChildNodes();
        if (nodelist.getLength() == 0) {
            throw new PKIException("XML内容缺失,SecurityCategories.SecurityCategories(),SecurityCategories没有子节点");
        }
        for (int i = 0; i < nodelist.getLength(); i++) {
            Node cnode = nodelist.item(i);
            if (cnode.getNodeName().equals("SecurityCategory")) {
                this.set.add(new SecurityCategory(cnode));
            }
        }
    }
}
