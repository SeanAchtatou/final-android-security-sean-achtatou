package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject;
import cn.com.jit.ida.util.pki.asn1.DERBitString;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.DERTaggedObject;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

public class ClearanceSyntax implements DEREncodable {
    private DERBitString classlist;
    private DERObjectIdentifier policyID;
    private SecurityCategories seccat;

    public ClearanceSyntax() {
        this.seccat = new SecurityCategories();
    }

    public ClearanceSyntax(ASN1Sequence seq) {
        for (int i = 0; i != seq.size(); i++) {
            ASN1TaggedObject taggedObject = (ASN1TaggedObject) seq.getObjectAt(i);
            switch (taggedObject.getTagNo()) {
                case 0:
                    this.policyID = DERObjectIdentifier.getInstance(taggedObject, true);
                    break;
                case 1:
                    this.classlist = DERBitString.getInstance(taggedObject, true);
                    break;
                case 2:
                    this.seccat = SecurityCategories.getInstance(taggedObject, true);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown tag in RoleSyntax");
            }
        }
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(new DERTaggedObject(true, 0, this.policyID));
        v.add(new DERTaggedObject(true, 1, this.classlist));
        v.add(new DERTaggedObject(true, 2, this.seccat.getDERObject()));
        return new DERSequence(v);
    }

    public ClearanceSyntax(DERObjectIdentifier policyID2, DERBitString classlist2, SecurityCategories seccat2) {
        this.policyID = policyID2;
        this.classlist = classlist2;
        this.seccat = seccat2;
    }

    public DERBitString getClasslist() {
        return this.classlist;
    }

    public void setClasslist(DERBitString classlist2) {
        this.classlist = classlist2;
    }

    public DERObjectIdentifier getPolicyID() {
        return this.policyID;
    }

    public void setPolicyID(DERObjectIdentifier policyID2) {
        this.policyID = policyID2;
    }

    public SecurityCategories getSeccat() {
        return this.seccat;
    }

    public void setSeccat(SecurityCategories seccat2) {
        this.seccat = seccat2;
    }

    public SecurityCategory createSecurityCategory() {
        return this.seccat.creatSecurityCategory();
    }

    public void addSecurityCategory(SecurityCategory sec) {
        this.seccat.addSecurityCategory(sec);
    }

    public ClearanceSyntax(Node nl) throws PKIException {
        this.policyID = null;
        this.classlist = null;
        this.seccat = null;
        NodeList nodelist = nl.getChildNodes();
        if (nodelist.getLength() == 0) {
            throw new PKIException("XML内容缺失,ClearanceSyntax.ClearanceSyntax(),ClearanceAttribute没有子节点");
        }
        for (int i = 0; i < nodelist.getLength(); i++) {
            Node cnode = nodelist.item(i);
            String sname = cnode.getNodeName();
            if (sname.equals("PolicyId")) {
                this.policyID = new DERObjectIdentifier(((Text) cnode.getFirstChild()).getData().trim());
            }
            if (sname.equals("ClassList")) {
                StringBuffer stringbuffer = new StringBuffer(Integer.toBinaryString(Integer.parseInt(((Text) cnode.getFirstChild()).getData().trim()) ^ 2));
                stringbuffer.reverse();
                this.classlist = new DERBitString(stringbuffer.toString().getBytes());
            }
            if (sname.equals("SecurityCategories")) {
                this.seccat = new SecurityCategories(cnode);
            }
        }
    }
}
