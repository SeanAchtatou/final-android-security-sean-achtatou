package cn.com.jit.ida.util.pki.cipher.param;

import java.security.SecureRandom;

public class PBEParam {
    private int iterations;
    private byte[] salt = new byte[8];

    public PBEParam() {
        new SecureRandom().nextBytes(this.salt);
        this.iterations = 1000;
    }

    public int getIterations() {
        return this.iterations;
    }

    public byte[] getSalt() {
        return this.salt;
    }

    public void setSalt(byte[] salt2) {
        this.salt = salt2;
    }

    public void setIterations(int iterations2) {
        this.iterations = iterations2;
    }
}
