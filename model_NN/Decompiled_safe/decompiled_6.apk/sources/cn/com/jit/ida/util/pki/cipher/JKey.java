package cn.com.jit.ida.util.pki.cipher;

public class JKey {
    public static final String AES_KEY = "AES";
    public static final String CAST5_KEY = "CAST5";
    public static final String DES3_KEY = "DESede";
    public static final String DES_KEY = "DES";
    public static final String DSA_PRV_KEY = "DSA_Private";
    public static final String DSA_PRV_KEY_ID = "DSA_PrivateID";
    public static final String DSA_PUB_KEY = "DSA_Public";
    public static final String DSA_PUB_KEY_ID = "DSA_PublicID";
    public static final String ECDSA_PRV_KEY = "ECDSA_Private";
    public static final String ECDSA_PRV_KEY_ID = "ECDSA_PrivateID";
    public static final String ECDSA_PUB_KEY = "ECDSA_Public";
    public static final String ECDSA_PUB_KEY_ID = "ECDSA_PublicID";
    public static final String ECIES_PRV_KEY = "ECIES_Private";
    public static final String ECIES_PRV_KEY_ID = "EC_PrivateID";
    public static final String ECIES_PUB_KEY = "ECIES_Public";
    public static final String ECIES_PUB_KEY_ID = "EC_PublicID";
    public static final String IDEA_KEY = "IDEA";
    public static final String MASTER_KEY = "MASTERKEY";
    public static final String PBE_2KEY = "PBEWITHSHAAND2-KEYTRIPLEDES-CBC";
    public static final String PBE_3KEY = "PBEWITHSHAAND3-KEYTRIPLEDES-CBC";
    public static final String PBE_KEY = "PBEWithMD5AndDES";
    public static final String RC2_KEY = "RC2";
    public static final String RC4_KEY = "RC4";
    public static final String RSA_PRV_KEY = "RSA_Private";
    public static final String RSA_PRV_KEY_ID = "RSA_PrivateID";
    public static final String RSA_PUB_KEY = "RSA_Public";
    public static final String RSA_PUB_KEY_ID = "RSA_PublicID";
    public static final String SCB2_KEY = "SCB2";
    public static final String SM2_PRV_KEY = "SM2_Private";
    public static final String SM2_PRV_KEY_ID = "SM2_PrivateID";
    public static final String SM2_PUB_KEY = "SM2_Public";
    public static final String SM2_PUB_KEY_ID = "SM2_PublicID";
    public static final String SM4 = "SM4";
    public static final String SSF33_KEY = "SF33";
    public static final String SYMMETRY_KEY = "SYMMETRY";
    private byte[] key;
    private long keyID;
    private String keyType;

    public JKey(String keyType2, byte[] key2) {
        this.keyType = keyType2;
        this.key = key2;
        this.keyID = -1;
    }

    public JKey() {
    }

    public JKey(String keyType2, long keyID2) {
        this.keyType = keyType2;
        this.keyID = keyID2;
        this.key = null;
    }

    public byte[] getKey() {
        return this.key;
    }

    public long getKeyID() {
        return this.keyID;
    }

    public String getKeyType() {
        return this.keyType;
    }

    public void setKeyType(String keyType2) {
        this.keyType = keyType2;
    }

    public void setKey(byte[] key2) {
        this.key = key2;
    }

    public void setKeyID(long keyID2) {
        this.keyID = keyID2;
    }
}
