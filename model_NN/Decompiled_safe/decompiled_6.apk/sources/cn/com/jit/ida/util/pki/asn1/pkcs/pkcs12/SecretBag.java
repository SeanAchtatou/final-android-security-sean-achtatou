package cn.com.jit.ida.util.pki.asn1.pkcs.pkcs12;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.DERTaggedObject;

public class SecretBag implements DEREncodable {
    private DERObjectIdentifier secretTypeId;
    private DERObject secretValue;

    public static SecretBag getInstance(Object o) {
        if (o == null || (o instanceof SecretBag)) {
            return (SecretBag) o;
        }
        if (o instanceof ASN1Sequence) {
            return new SecretBag((ASN1Sequence) o);
        }
        throw new IllegalArgumentException("unknown object in factory " + o.getClass().getName());
    }

    public SecretBag(ASN1Sequence seq) {
        this.secretTypeId = (DERObjectIdentifier) seq.getObjectAt(0);
        this.secretValue = ((DERTaggedObject) seq.getObjectAt(1)).getObject();
    }

    public SecretBag(DERObjectIdentifier _secretTypeId, DERObject _secretValue) {
        this.secretTypeId = _secretTypeId;
        this.secretValue = _secretValue;
    }

    public DERObjectIdentifier getSecretId() {
        return this.secretTypeId;
    }

    public DERObject getSecretValue() {
        return this.secretValue;
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(this.secretTypeId);
        v.add(new DERTaggedObject(0, this.secretValue));
        return new DERSequence(v);
    }
}
