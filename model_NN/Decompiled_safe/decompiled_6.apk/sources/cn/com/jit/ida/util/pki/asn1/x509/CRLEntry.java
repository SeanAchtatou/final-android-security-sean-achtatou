package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DERObject;

public class CRLEntry implements DEREncodable {
    X509Extensions crlEntryExtensions;
    Time revocationDate;
    ASN1Sequence seq;
    DERInteger userCertificate;

    public CRLEntry(ASN1Sequence seq2) {
        this.seq = seq2;
        this.userCertificate = (DERInteger) seq2.getObjectAt(0);
        this.revocationDate = Time.getInstance(seq2.getObjectAt(1));
        if (seq2.size() == 3) {
            this.crlEntryExtensions = X509Extensions.getInstance(seq2.getObjectAt(2));
        }
    }

    public DERInteger getUserCertificate() {
        return this.userCertificate;
    }

    public Time getRevocationDate() {
        return this.revocationDate;
    }

    public X509Extensions getExtensions() {
        return this.crlEntryExtensions;
    }

    public DERObject getDERObject() {
        return this.seq;
    }
}
