package cn.com.jit.ida.util.pki.cert;

import cn.com.jit.ida.util.pki.PKIConstant;
import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.Parser;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1Set;
import cn.com.jit.ida.util.pki.asn1.DERBitString;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.x509.Attribute;
import cn.com.jit.ida.util.pki.asn1.x509.AttributeCertificate;
import cn.com.jit.ida.util.pki.asn1.x509.Attributes;
import cn.com.jit.ida.util.pki.asn1.x509.RoleSyntax;
import cn.com.jit.ida.util.pki.cipher.JKey;
import cn.com.jit.ida.util.pki.cipher.Session;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.text.ParseException;
import java.util.Date;

public class X509AttCert {
    private String[] AttributeType = null;
    private AttributeCertificate attcert = null;
    private Attributes atts = null;
    private RoleSyntax role = null;
    private String[] roleAuthorityAndRoleName = null;

    public X509AttCert(String FileName) throws FileNotFoundException, IOException, PKIException {
        FileInputStream fin = new FileInputStream(new File(FileName));
        byte[] data = new byte[fin.available()];
        fin.read(data);
        this.attcert = new AttributeCertificate(data);
    }

    public X509AttCert(byte[] data) throws PKIException {
        this.attcert = new AttributeCertificate(data);
    }

    public boolean verify(JKey pubKey, Session session) throws PKIException {
        return this.attcert.verify(pubKey, session);
    }

    public String getHolder() {
        return this.attcert.getAcinfo().getHolder().toString();
    }

    public String getIssuer() {
        return this.attcert.getAcinfo().getIssuer().toString();
    }

    public Date getNotBefore() throws PKIException {
        try {
            return this.attcert.getAcinfo().getAttrCertValidityPeriod().getNotBeforeTime().getDate();
        } catch (ParseException e) {
            throw new PKIException(PKIException.ATTRIBUTE_NOTBEFOREPARSE_ERROR, PKIException.ATTRIBUTE_NOTBEFOREPARSE_ERROR_DES, e);
        }
    }

    public Date getNotAfter() throws PKIException {
        try {
            return this.attcert.getAcinfo().getAttrCertValidityPeriod().getNotAfterTime().getDate();
        } catch (ParseException e) {
            throw new PKIException(PKIException.ATTRIBUTE_NOTAFTERPARSE_ERROR, PKIException.ATTRIBUTE_NOTAFTERPARSE_ERROR_DES, e);
        }
    }

    public int getVersion() {
        return this.attcert.getVersion();
    }

    public BigInteger getSerialNumber() {
        return this.attcert.getSerialNumber();
    }

    public String getStringSerialNumber() {
        return getSerialNumber().toString(16).toUpperCase();
    }

    public String getSignatureAlgName() {
        DERObjectIdentifier oid = this.attcert.getSignatureAlgorithm().getObjectId();
        if (!PKIConstant.oid2SigAlgName.containsKey(oid)) {
            return getSignatureAlgOID();
        }
        return (String) PKIConstant.oid2SigAlgName.get(oid);
    }

    public String getSignatureAlgOID() {
        return this.attcert.getSignatureAlgorithm().getObjectId().getId();
    }

    public byte[] getSignature() {
        return this.attcert.getSignature();
    }

    public byte[] getIssuerUniqueId() {
        DERBitString issuerUniqueId = this.attcert.getAcinfo().getIssuerUniqueID();
        if (issuerUniqueId != null) {
            return issuerUniqueId.getBytes();
        }
        return null;
    }

    public byte[] getEncoded() throws PKIException {
        try {
            return Parser.writeDERObj2Bytes(this.attcert);
        } catch (Exception ex) {
            throw new PKIException(PKIException.ENCODED_CERT, PKIException.ENCODED_CERT_DES, ex);
        }
    }

    public String getRoleAuthority() {
        getRoleAuthority_Name();
        if (this.roleAuthorityAndRoleName == null) {
            return null;
        }
        return this.roleAuthorityAndRoleName[0];
    }

    public String getRoleName() {
        getRoleAuthority_Name();
        if (this.roleAuthorityAndRoleName == null) {
            return null;
        }
        return this.roleAuthorityAndRoleName[1];
    }

    private void getRoleAuthority_Name() {
        if (this.roleAuthorityAndRoleName == null) {
            String[] re = new String[2];
            Attributes attributes = getAttributes();
            for (int i = 0; i < attributes.getSize(); i++) {
                Attribute att = attributes.getAttributeAt(i);
                if (att.getAttrType().getId().equals(Attribute.ROLE_ATTRIBUTE_OID)) {
                    ASN1Set set = att.getAttrValuesSet();
                    for (int j = 0; j < set.size(); j++) {
                        RoleSyntax role2 = new RoleSyntax((ASN1Sequence) ((DERSequence) set.getObjectAt(j)));
                        String[] ra = role2.getRoleAuthorityAsString();
                        if (ra != null && ra.length > 0) {
                            re[0] = role2.getRoleAuthorityAsString()[0];
                        }
                        re[1] = role2.getRoleNameAsString();
                    }
                }
            }
            this.roleAuthorityAndRoleName = re;
        }
    }

    public Attributes getAttributes() {
        if (this.atts == null) {
            this.atts = this.attcert.getAcinfo().getAttributes();
        }
        return this.atts;
    }

    public int getAttributeCount() {
        return getAttributes().getSize();
    }

    public Attribute getAttributeAt(int i) {
        return getAttributes().getAttributeAt(i);
    }

    public String getAttributeTypeAt(int j) {
        if (this.AttributeType == null) {
            Attributes atts2 = getAttributes();
            this.AttributeType = new String[atts2.getSize()];
            for (int i = 0; i < atts2.getSize(); i++) {
                this.AttributeType[i] = atts2.getAttributeAt(i).getAttrType().getId();
            }
        }
        return this.AttributeType[j];
    }
}
