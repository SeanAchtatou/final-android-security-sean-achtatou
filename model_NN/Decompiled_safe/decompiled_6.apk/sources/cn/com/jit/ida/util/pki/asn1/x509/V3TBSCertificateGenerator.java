package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.DERBitString;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.DERTaggedObject;
import cn.com.jit.ida.util.pki.asn1.DERUTCTime;

public class V3TBSCertificateGenerator {
    Time endDate;
    X509Extensions extensions;
    X509Name issuer;
    DERBitString issuerUniqueId;
    DERInteger serialNumber;
    AlgorithmIdentifier signature;
    Time startDate;
    X509Name subject;
    SubjectPublicKeyInfo subjectPublicKeyInfo;
    DERBitString subjectUniqueId;
    DERTaggedObject version = new DERTaggedObject(0, new DERInteger(2));

    public void setSerialNumber(DERInteger serialNumber2) {
        this.serialNumber = serialNumber2;
    }

    public void setSignature(AlgorithmIdentifier signature2) {
        this.signature = signature2;
    }

    public void setIssuer(X509Name issuer2) {
        this.issuer = issuer2;
    }

    public void setStartDate(DERUTCTime startDate2) {
        this.startDate = new Time(startDate2);
    }

    public void setStartDate(Time startDate2) {
        this.startDate = startDate2;
    }

    public void setEndDate(DERUTCTime endDate2) {
        this.endDate = new Time(endDate2);
    }

    public void setEndDate(Time endDate2) {
        this.endDate = endDate2;
    }

    public void setSubject(X509Name subject2) {
        this.subject = subject2;
    }

    public void setSubjectPublicKeyInfo(SubjectPublicKeyInfo pubKeyInfo) {
        this.subjectPublicKeyInfo = pubKeyInfo;
    }

    public void setIssuerUniqueID(DERBitString issuerUniqueId2) {
        this.issuerUniqueId = issuerUniqueId2;
    }

    public void setSubjectUniqueID(DERBitString subjectUniqueID) {
        this.subjectUniqueId = subjectUniqueID;
    }

    public void setExtensions(X509Extensions extensions2) {
        this.extensions = extensions2;
    }

    public TBSCertificateStructure generateTBSCertificate() {
        if (this.serialNumber == null || this.signature == null || this.issuer == null || this.startDate == null || this.endDate == null || this.subject == null || this.subjectPublicKeyInfo == null) {
            throw new IllegalStateException("not all mandatory fields set in V3 TBScertificate generator");
        }
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(this.version);
        v.add(this.serialNumber);
        v.add(this.signature);
        v.add(this.issuer);
        ASN1EncodableVector validity = new ASN1EncodableVector();
        validity.add(this.startDate);
        validity.add(this.endDate);
        v.add(new DERSequence(validity));
        v.add(this.subject);
        v.add(this.subjectPublicKeyInfo);
        if (this.issuerUniqueId != null) {
            v.add(new DERTaggedObject(false, 1, this.issuerUniqueId));
        }
        if (this.subjectUniqueId != null) {
            v.add(new DERTaggedObject(false, 2, this.subjectUniqueId));
        }
        if (this.extensions != null) {
            v.add(new DERTaggedObject(3, this.extensions));
        }
        return new TBSCertificateStructure(new DERSequence(v));
    }
}
