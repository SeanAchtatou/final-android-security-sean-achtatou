package cn.com.jit.ida.util.pki.asn1;

import java.io.IOException;

public class DERGeneralString extends DERObject implements DERString {
    private String string;

    public static DERGeneralString getInstance(Object obj) {
        if (obj == null || (obj instanceof DERGeneralString)) {
            return (DERGeneralString) obj;
        }
        if (obj instanceof ASN1OctetString) {
            return new DERGeneralString(((ASN1OctetString) obj).getOctets());
        }
        if (obj instanceof ASN1TaggedObject) {
            return getInstance(((ASN1TaggedObject) obj).getObject());
        }
        throw new IllegalArgumentException("illegal object in getInstance: " + obj.getClass().getName());
    }

    public static DERGeneralString getInstance(ASN1TaggedObject obj, boolean explicit) {
        return getInstance(obj.getObject());
    }

    public DERGeneralString(byte[] string2) {
        char[] cs = new char[string2.length];
        for (int i = 0; i != cs.length; i++) {
            cs[i] = (char) (string2[i] & 255);
        }
        this.string = new String(cs);
    }

    public DERGeneralString(String string2) {
        this.string = string2;
    }

    public String getString() {
        return this.string;
    }

    public byte[] getOctets() {
        char[] cs = this.string.toCharArray();
        byte[] bs = new byte[cs.length];
        for (int i = 0; i != cs.length; i++) {
            bs[i] = (byte) cs[i];
        }
        return bs;
    }

    /* access modifiers changed from: package-private */
    public void encode(DEROutputStream out) throws IOException {
        out.writeEncoded(27, getOctets());
    }

    public int hashCode() {
        return getString().hashCode();
    }

    public boolean equals(Object o) {
        if (!(o instanceof DERGeneralString)) {
            return false;
        }
        return getString().equals(((DERGeneralString) o).getString());
    }
}
