package cn.com.jit.ida.util.pki.extension;

import cn.com.jit.ida.util.pki.PKIException;

public interface Extension {
    byte[] encode() throws PKIException;

    boolean getCritical();

    String getOID();

    void setCritical(boolean z);
}
