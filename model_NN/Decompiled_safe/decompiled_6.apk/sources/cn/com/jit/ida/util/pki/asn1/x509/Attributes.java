package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DEREncodableVector;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Attributes implements DEREncodable {
    private DEREncodableVector atts;

    public static Attributes getInstance(Object o) {
        if (o == null || (o instanceof Attributes)) {
            return (Attributes) o;
        }
        if (o instanceof ASN1Sequence) {
            return new Attributes((ASN1Sequence) o);
        }
        throw new IllegalArgumentException("unknown object in factory");
    }

    public DERObject getDERObject() {
        return new DERSequence(this.atts);
    }

    public Attributes(ASN1Sequence seq) {
        this.atts = new DEREncodableVector();
        for (int i = 0; i < seq.size(); i++) {
            this.atts.add(seq.getObjectAt(i));
        }
    }

    public Attributes(DEREncodableVector ats) {
        this.atts = new DEREncodableVector();
        this.atts = ats;
    }

    public Attributes() {
        this.atts = new DEREncodableVector();
        this.atts = new DEREncodableVector();
    }

    public void addAttribute(Attribute att) {
        this.atts.add(att.getDERObject());
    }

    public DEREncodableVector getAttributes() {
        return this.atts;
    }

    public void setAttributes(DEREncodableVector atts2) {
        this.atts = atts2;
    }

    public int getSize() {
        return this.atts.size();
    }

    public Attribute getAttributeAt(int i) {
        Attribute att = new Attribute((DERSequence) this.atts.get(i));
        String oid = att.getAttrType().getId();
        Attribute reAtt = null;
        if (Attribute.ROLE_ATTRIBUTE_OID.equals(oid)) {
            reAtt = new RoleAttribute();
        }
        if (Attribute.CLEARANCE_ATTRIBUTE_OID.equals(oid)) {
            reAtt = new ClearanceAttribute();
        }
        if (Attribute.GROUP_ATTRIBUTE_OID.equals(oid)) {
            reAtt = new GroupAttribute();
        }
        reAtt.setAttrValues(att.getAttrValuesSet());
        return reAtt;
    }

    public Attributes(Node nl) throws PKIException {
        this.atts = new DEREncodableVector();
        NodeList nodelist = nl.getChildNodes();
        if (nodelist.getLength() == 0) {
            throw new PKIException("XML内容缺失,Attributes.Attributes(),属性集合没有子节点");
        }
        for (int i = 0; i < nodelist.getLength(); i++) {
            Node cnode = nodelist.item(i);
            if (cnode.getNodeName().equals("attribute")) {
                this.atts.add(new Attribute(cnode));
            }
        }
    }
}
