package cn.com.jit.ida.util.pki.asn1.pkcs.pkcs8;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1OctetString;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.pkcs.PKCSObjectIdentifiers;
import cn.com.jit.ida.util.pki.asn1.x509.AlgorithmIdentifier;
import java.util.Enumeration;

public class EncryptedPrivateKeyInfo implements PKCSObjectIdentifiers, DEREncodable {
    private ASN1OctetString encryptedData;
    private AlgorithmIdentifier encryptionAlgId;

    public EncryptedPrivateKeyInfo(ASN1Sequence seq) {
        Enumeration e = seq.getObjects();
        this.encryptionAlgId = new AlgorithmIdentifier((ASN1Sequence) e.nextElement());
        this.encryptedData = (ASN1OctetString) e.nextElement();
    }

    public EncryptedPrivateKeyInfo(AlgorithmIdentifier algId, byte[] encoding) {
        this.encryptionAlgId = algId;
        this.encryptedData = new DEROctetString(encoding);
    }

    public AlgorithmIdentifier getEncryptionAlgorithm() {
        return this.encryptionAlgId;
    }

    public byte[] getEncryptedData() {
        return this.encryptedData.getOctets();
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(this.encryptionAlgId);
        v.add(this.encryptedData);
        return new DERSequence(v);
    }
}
