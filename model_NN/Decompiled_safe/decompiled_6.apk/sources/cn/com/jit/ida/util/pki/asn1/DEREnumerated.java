package cn.com.jit.ida.util.pki.asn1;

import java.io.IOException;
import java.math.BigInteger;

public class DEREnumerated extends DERObject {
    byte[] bytes;

    public static DEREnumerated getInstance(Object obj) {
        if (obj == null || (obj instanceof DEREnumerated)) {
            return (DEREnumerated) obj;
        }
        if (obj instanceof ASN1OctetString) {
            return new DEREnumerated(((ASN1OctetString) obj).getOctets());
        }
        if (obj instanceof ASN1TaggedObject) {
            return getInstance(((ASN1TaggedObject) obj).getObject());
        }
        throw new IllegalArgumentException("illegal object in getInstance: " + obj.getClass().getName());
    }

    public static DEREnumerated getInstance(ASN1TaggedObject obj, boolean explicit) {
        return getInstance(obj.getObject());
    }

    public DEREnumerated(int value) {
        this.bytes = BigInteger.valueOf((long) value).toByteArray();
    }

    public DEREnumerated(BigInteger value) {
        this.bytes = value.toByteArray();
    }

    public DEREnumerated(byte[] bytes2) {
        this.bytes = bytes2;
    }

    public BigInteger getValue() {
        return new BigInteger(this.bytes);
    }

    /* access modifiers changed from: package-private */
    public void encode(DEROutputStream out) throws IOException {
        out.writeEncoded(10, this.bytes);
    }

    public boolean equals(Object o) {
        if (o == null || !(o instanceof DEREnumerated)) {
            return false;
        }
        DEREnumerated other = (DEREnumerated) o;
        if (this.bytes.length != other.bytes.length) {
            return false;
        }
        for (int i = 0; i != this.bytes.length; i++) {
            if (this.bytes[i] != other.bytes[i]) {
                return false;
            }
        }
        return true;
    }
}
