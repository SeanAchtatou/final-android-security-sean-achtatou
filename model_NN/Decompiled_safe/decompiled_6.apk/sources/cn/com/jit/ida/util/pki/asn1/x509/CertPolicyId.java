package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;

public class CertPolicyId extends DERObjectIdentifier {
    public CertPolicyId(String id) {
        super(id);
    }
}
