package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERGeneralizedTime;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERTaggedObject;
import cn.com.jit.ida.util.pki.asn1.DERUTCTime;

public class TBSCertList implements DEREncodable {
    X509Extensions crlExtensions;
    X509Name issuer;
    Time nextUpdate;
    CRLEntry[] revokedCertificates;
    ASN1Sequence seq;
    AlgorithmIdentifier signature;
    Time thisUpdate;
    DERInteger version;

    public class CRLEntry implements DEREncodable {
        X509Extensions crlEntryExtensions;
        Time revocationDate;
        ASN1Sequence seq;
        DERInteger userCertificate;

        public CRLEntry(ASN1Sequence seq2) {
            this.seq = seq2;
            this.userCertificate = (DERInteger) seq2.getObjectAt(0);
            this.revocationDate = Time.getInstance(seq2.getObjectAt(1));
            if (seq2.size() == 3) {
                this.crlEntryExtensions = X509Extensions.getInstance(seq2.getObjectAt(2));
            }
        }

        public DERInteger getUserCertificate() {
            return this.userCertificate;
        }

        public Time getRevocationDate() {
            return this.revocationDate;
        }

        public X509Extensions getExtensions() {
            return this.crlEntryExtensions;
        }

        public DERObject getDERObject() {
            return this.seq;
        }
    }

    public static TBSCertList getInstance(ASN1TaggedObject obj, boolean explicit) {
        return getInstance(ASN1Sequence.getInstance(obj, explicit));
    }

    public static TBSCertList getInstance(Object obj) {
        if (obj instanceof TBSCertList) {
            return (TBSCertList) obj;
        }
        if (obj instanceof ASN1Sequence) {
            return new TBSCertList((ASN1Sequence) obj);
        }
        throw new IllegalArgumentException("unknown object in factory");
    }

    public TBSCertList(ASN1Sequence seq2) {
        int seqPos;
        int seqPos2 = 0;
        this.seq = seq2;
        if (seq2.getObjectAt(0) instanceof DERInteger) {
            this.version = (DERInteger) seq2.getObjectAt(0);
            seqPos2 = 0 + 1;
        } else {
            this.version = new DERInteger(0);
        }
        int seqPos3 = seqPos2 + 1;
        this.signature = AlgorithmIdentifier.getInstance(seq2.getObjectAt(seqPos2));
        int seqPos4 = seqPos3 + 1;
        this.issuer = X509Name.getInstance(seq2.getObjectAt(seqPos3));
        int seqPos5 = seqPos4 + 1;
        this.thisUpdate = Time.getInstance(seq2.getObjectAt(seqPos4));
        if (seqPos5 >= seq2.size() || (!(seq2.getObjectAt(seqPos5) instanceof DERUTCTime) && !(seq2.getObjectAt(seqPos5) instanceof DERGeneralizedTime) && !(seq2.getObjectAt(seqPos5) instanceof Time))) {
            seqPos = seqPos5;
        } else {
            seqPos = seqPos5 + 1;
            this.nextUpdate = Time.getInstance(seq2.getObjectAt(seqPos5));
        }
        if (seqPos < seq2.size() && !(seq2.getObjectAt(seqPos) instanceof DERTaggedObject)) {
            int seqPos6 = seqPos + 1;
            ASN1Sequence certs = (ASN1Sequence) seq2.getObjectAt(seqPos);
            this.revokedCertificates = new CRLEntry[certs.size()];
            for (int i = 0; i < this.revokedCertificates.length; i++) {
                this.revokedCertificates[i] = new CRLEntry((ASN1Sequence) certs.getObjectAt(i));
            }
            seqPos = seqPos6;
        }
        if (seqPos < seq2.size() && (seq2.getObjectAt(seqPos) instanceof DERTaggedObject)) {
            int seqPos7 = seqPos + 1;
            this.crlExtensions = X509Extensions.getInstance(seq2.getObjectAt(seqPos));
        }
    }

    public int getVersion() {
        return this.version.getValue().intValue() + 1;
    }

    public DERInteger getVersionNumber() {
        return this.version;
    }

    public AlgorithmIdentifier getSignature() {
        return this.signature;
    }

    public X509Name getIssuer() {
        return this.issuer;
    }

    public Time getThisUpdate() {
        return this.thisUpdate;
    }

    public Time getNextUpdate() {
        return this.nextUpdate;
    }

    public CRLEntry[] getRevokedCertificates() {
        return this.revokedCertificates;
    }

    public X509Extensions getExtensions() {
        return this.crlExtensions;
    }

    public DERObject getDERObject() {
        return this.seq;
    }
}
