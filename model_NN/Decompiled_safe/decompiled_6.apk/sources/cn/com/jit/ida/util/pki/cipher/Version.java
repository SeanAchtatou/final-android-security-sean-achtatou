package cn.com.jit.ida.util.pki.cipher;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.cipher.lib.JHARDLib;
import com.jianq.misc.StringEx;
import com.jianq.net.JQBasicNetwork;
import java.net.URLDecoder;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

public class Version {
    public static String getVersion() {
        try {
            String path = URLDecoder.decode(Version.class.getProtectionDomain().getCodeSource().getLocation().getFile(), JQBasicNetwork.UTF_8);
            if (path == null) {
                return StringEx.Empty;
            }
            Manifest mani = new JarFile(path).getManifest();
            if (mani == null) {
                return StringEx.Empty;
            }
            Attributes attr = mani.getAttributes("JIT");
            if (attr == null) {
                return StringEx.Empty;
            }
            return attr.getValue(new Attributes.Name("JIT-Version"));
        } catch (Exception e) {
            e.printStackTrace();
            return StringEx.Empty;
        }
    }

    private static String parse(String path) throws Exception {
        int i;
        boolean needToChange = false;
        int numChars = path.length();
        if (numChars > 500) {
            i = numChars / 2;
        } else {
            i = numChars;
        }
        StringBuffer sb = new StringBuffer(i);
        int i2 = 0;
        byte[] bytes = null;
        while (i2 < numChars) {
            char c = path.charAt(i2);
            switch (c) {
                case '%':
                    if (bytes == null) {
                        try {
                            bytes = new byte[((numChars - i2) / 3)];
                        } catch (NumberFormatException e) {
                            throw new IllegalArgumentException("URLDecoder: Illegal hex characters in escape (%) pattern - " + e.getMessage());
                        }
                    }
                    int pos = 0;
                    while (i2 + 2 < numChars && c == '%') {
                        int pos2 = pos + 1;
                        bytes[pos] = (byte) Integer.parseInt(path.substring(i2 + 1, i2 + 3), 16);
                        i2 += 3;
                        if (i2 < numChars) {
                            c = path.charAt(i2);
                            pos = pos2;
                        } else {
                            pos = pos2;
                        }
                    }
                    if (i2 >= numChars || c != '%') {
                        sb.append(new String(bytes, 0, pos, JQBasicNetwork.UTF_8));
                        needToChange = true;
                        break;
                    } else {
                        throw new IllegalArgumentException("URLDecoder: Incomplete trailing escape (%) pattern");
                    }
                case '+':
                    sb.append(' ');
                    i2++;
                    needToChange = true;
                    break;
                default:
                    sb.append(c);
                    i2++;
                    break;
            }
        }
        return needToChange ? sb.toString() : path;
    }

    public static void main(String[] args) {
        System.out.println("PKITOOL version: " + getVersion());
        JCrypto jcrypto = JCrypto.getInstance();
        try {
            jcrypto.initialize(JCrypto.JSJY05B_LIB, null);
            System.out.println("SessionDll version: " + ((JHARDLib) jcrypto.openSession(JCrypto.JSJY05B_LIB, "PKITOOL")).getSessVersion());
            jcrypto.finalize(JCrypto.JSJY05B_LIB, null);
        } catch (PKIException e) {
            e.printStackTrace();
        }
    }
}
