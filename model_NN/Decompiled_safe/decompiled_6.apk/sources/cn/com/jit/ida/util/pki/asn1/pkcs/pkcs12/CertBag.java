package cn.com.jit.ida.util.pki.asn1.pkcs.pkcs12;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.DERTaggedObject;

public class CertBag implements DEREncodable {
    private DERObjectIdentifier certId;
    private DERObject certValue;

    public static CertBag getInstance(Object o) {
        if (o == null || (o instanceof CertBag)) {
            return (CertBag) o;
        }
        if (o instanceof ASN1Sequence) {
            return new CertBag((ASN1Sequence) o);
        }
        throw new IllegalArgumentException("unknown object in factory " + o.getClass().getName());
    }

    public CertBag(ASN1Sequence seq) {
        this.certId = (DERObjectIdentifier) seq.getObjectAt(0);
        this.certValue = ((DERTaggedObject) seq.getObjectAt(1)).getObject();
    }

    public CertBag(DERObjectIdentifier certId2, DERObject certValue2) {
        this.certId = certId2;
        this.certValue = certValue2;
    }

    public DERObjectIdentifier getCertId() {
        return this.certId;
    }

    public DERObject getCertValue() {
        return this.certValue;
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(this.certId);
        v.add(new DERTaggedObject(0, this.certValue));
        return new DERSequence(v);
    }
}
