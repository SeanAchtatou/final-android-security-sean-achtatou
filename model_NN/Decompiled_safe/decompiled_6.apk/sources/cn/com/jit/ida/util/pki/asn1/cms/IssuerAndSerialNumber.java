package cn.com.jit.ida.util.pki.asn1.cms;

import cn.com.jit.ida.util.pki.asn1.ASN1Encodable;
import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.x509.X509Name;
import java.math.BigInteger;

public class IssuerAndSerialNumber extends ASN1Encodable {
    X509Name name;
    DERInteger serialNumber;

    public static IssuerAndSerialNumber getInstance(Object obj) {
        if (obj instanceof IssuerAndSerialNumber) {
            return (IssuerAndSerialNumber) obj;
        }
        if (obj instanceof ASN1Sequence) {
            return new IssuerAndSerialNumber((ASN1Sequence) obj);
        }
        throw new IllegalArgumentException("Illegal object in IssuerAndSerialNumber: " + obj.getClass().getName());
    }

    public IssuerAndSerialNumber(ASN1Sequence seq) {
        this.name = X509Name.getInstance(seq.getObjectAt(0));
        this.serialNumber = (DERInteger) seq.getObjectAt(1);
    }

    public IssuerAndSerialNumber(X509Name name2, BigInteger serialNumber2) {
        this.name = name2;
        this.serialNumber = new DERInteger(serialNumber2);
    }

    public IssuerAndSerialNumber(X509Name name2, DERInteger serialNumber2) {
        this.name = name2;
        this.serialNumber = serialNumber2;
    }

    public X509Name getName() {
        return this.name;
    }

    public DERInteger getSerialNumber() {
        return this.serialNumber;
    }

    public DERObject toASN1Object() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(this.name);
        v.add(this.serialNumber);
        return new DERSequence(v);
    }
}
