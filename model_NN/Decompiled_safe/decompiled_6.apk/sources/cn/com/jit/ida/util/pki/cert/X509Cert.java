package cn.com.jit.ida.util.pki.cert;

import cn.com.jit.ida.util.pki.PKIConstant;
import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.Parser;
import cn.com.jit.ida.util.pki.asn1.ASN1InputStream;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1Set;
import cn.com.jit.ida.util.pki.asn1.DERBitString;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.DERPrintableString;
import cn.com.jit.ida.util.pki.asn1.pkcs.PKCSObjectIdentifiers;
import cn.com.jit.ida.util.pki.asn1.x509.X509CertificateStructure;
import cn.com.jit.ida.util.pki.asn1.x509.X509Extension;
import cn.com.jit.ida.util.pki.asn1.x509.X509Extensions;
import cn.com.jit.ida.util.pki.asn1.x509.X509Name;
import cn.com.jit.ida.util.pki.asn1.x9.X9ObjectIdentifiers;
import cn.com.jit.ida.util.pki.cipher.JKey;
import cn.com.jit.ida.util.pki.cipher.Mechanism;
import cn.com.jit.ida.util.pki.cipher.Session;
import cn.com.jit.ida.util.pki.encoders.Base64;
import cn.com.jit.ida.util.pki.extension.AbstractSelfDefExtension;
import cn.com.jit.ida.util.pki.extension.AuthorityInformationAccessExt;
import cn.com.jit.ida.util.pki.extension.AuthorityKeyIdentifierExt;
import cn.com.jit.ida.util.pki.extension.BasicConstraintsExt;
import cn.com.jit.ida.util.pki.extension.CRLDistributionPointsExt;
import cn.com.jit.ida.util.pki.extension.CertificatePoliciesExt;
import cn.com.jit.ida.util.pki.extension.ExtendedKeyUsageExt;
import cn.com.jit.ida.util.pki.extension.ICRegistrationNumberExt;
import cn.com.jit.ida.util.pki.extension.IdentifyCodeExt;
import cn.com.jit.ida.util.pki.extension.InsuranceNumberExt;
import cn.com.jit.ida.util.pki.extension.IssuerAlternativeNamesExt;
import cn.com.jit.ida.util.pki.extension.KeyUsageExt;
import cn.com.jit.ida.util.pki.extension.NameConstraintsExt;
import cn.com.jit.ida.util.pki.extension.OrganizationCodeExt;
import cn.com.jit.ida.util.pki.extension.PolicyConstraintsExt;
import cn.com.jit.ida.util.pki.extension.PolicyMappingsExt;
import cn.com.jit.ida.util.pki.extension.SelfDefExtension;
import cn.com.jit.ida.util.pki.extension.SubjectAltNameExt;
import cn.com.jit.ida.util.pki.extension.SubjectKeyIdentifierExt;
import cn.com.jit.ida.util.pki.extension.TaxationNumberExt;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Date;

public class X509Cert {
    private X509CertificateStructure cert = null;
    private final String end = "-----END CERTIFICATE-----";
    private final String head = "-----BEGIN CERTIFICATE-----";

    public X509Cert(byte[] certData) throws PKIException {
        initCert(parseCertData(certData));
    }

    public X509Cert(InputStream is) throws PKIException {
        try {
            int length = is.available();
            byte[] certIS = new byte[length];
            int readLen = is.read(certIS);
            while (readLen < length) {
                byte[] temp = new byte[(length - readLen)];
                int i = is.read(temp);
                System.arraycopy(temp, 0, certIS, readLen, i);
                readLen += i;
            }
            is.close();
            initCert(parseCertData(certIS));
        } catch (IOException ex) {
            throw new PKIException(PKIException.INIT_CERT, PKIException.INIT_CERT_DES, ex);
        }
    }

    private byte[] parseCertData(byte[] certData) throws PKIException {
        try {
            byte[] tempHead = new byte["-----BEGIN CERTIFICATE-----".length()];
            System.arraycopy(certData, 0, tempHead, 0, tempHead.length);
            if (Parser.isBase64Encode(certData)) {
                return Base64.decode(Parser.convertBase64(certData));
            }
            if (!Arrays.equals(tempHead, "-----BEGIN CERTIFICATE-----".getBytes())) {
                return certData;
            }
            byte[] withoutHead = new byte[(certData.length - "-----BEGIN CERTIFICATE-----".length())];
            System.arraycopy(certData, "-----BEGIN CERTIFICATE-----".length(), withoutHead, 0, withoutHead.length);
            byte[] withoutEnd = new byte[(withoutHead.length - ("-----END CERTIFICATE-----".length() + 2))];
            System.arraycopy(withoutHead, 0, withoutEnd, 0, withoutEnd.length);
            return Base64.decode(Parser.convertBase64(withoutEnd));
        } catch (Exception ex) {
            throw new PKIException(PKIException.INIT_CERT, PKIException.INIT_CERT_DES, ex);
        } catch (Throwable th) {
            throw new PKIException(PKIException.INIT_CERT, PKIException.INIT_CERT_DES);
        }
    }

    private void initCert(byte[] certData) throws PKIException {
        try {
            this.cert = new X509CertificateStructure((ASN1Sequence) new ASN1InputStream(new ByteArrayInputStream(certData)).readObject());
        } catch (Exception ex) {
            throw new PKIException(PKIException.INIT_CERT, PKIException.INIT_CERT_DES, ex);
        } catch (Throwable th) {
            throw new PKIException(PKIException.INIT_CERT, PKIException.INIT_CERT_DES);
        }
    }

    public X509Cert(X509CertificateStructure certStructure) {
        this.cert = certStructure;
    }

    public X509CertificateStructure getCertStructure() {
        return this.cert;
    }

    public byte[] getEncoded() throws PKIException {
        try {
            return Parser.writeDERObj2Bytes(this.cert);
        } catch (Exception ex) {
            throw new PKIException(PKIException.ENCODED_CERT, PKIException.ENCODED_CERT_DES, ex);
        }
    }

    public int getVersion() {
        return this.cert.getVersion();
    }

    public String getIssuer() {
        return this.cert.getIssuer().toString().trim();
    }

    public X509Name getX509NameIssuer() {
        return this.cert.getIssuer();
    }

    public String getSubject() {
        return this.cert.getSubject().toString().trim();
    }

    public X509Name getX509NameSubject() {
        return this.cert.getSubject();
    }

    public Date getNotBefore() {
        return this.cert.getStartDate().getDate();
    }

    public Date getNotAfter() {
        return this.cert.getEndDate().getDate();
    }

    public BigInteger getSerialNumber() {
        return this.cert.getSerialNumber().getValue();
    }

    public String getStringSerialNumber() {
        return this.cert.getSerialNumber().getValue().toString(16).toUpperCase();
    }

    public String getSignatureAlgName() {
        DERObjectIdentifier oid = this.cert.getSignatureAlgorithm().getObjectId();
        if (!PKIConstant.oid2SigAlgName.containsKey(oid)) {
            return getSignatureAlgOID();
        }
        return (String) PKIConstant.oid2SigAlgName.get(oid);
    }

    public String getSignatureAlgOID() {
        return this.cert.getSignatureAlgorithm().getObjectId().getId();
    }

    public JKey getPublicKey() throws PKIException {
        try {
            return Parser.SPKI2Key(this.cert.getSubjectPublicKeyInfo());
        } catch (Exception ex) {
            throw new PKIException("5", PKIException.SPKI_KEY_DES, ex);
        }
    }

    public byte[] getTBSCertificate() throws PKIException {
        try {
            return Parser.writeDERObj2Bytes(this.cert.getTBSCertificate().getDERObject());
        } catch (Exception ex) {
            throw new PKIException(PKIException.TBSCERT_BYTES, PKIException.TBSCERT_BYTES_DES, ex);
        }
    }

    public byte[] getSignature() {
        return this.cert.getSignature().getBytes();
    }

    public byte[] getSubjectUniqueId() {
        DERBitString subjectUniqueId = this.cert.getSubjectUniqueId();
        if (subjectUniqueId != null) {
            return subjectUniqueId.getBytes();
        }
        return null;
    }

    public byte[] getIssuerUniqueId() {
        DERBitString issuerUniqueId = this.cert.getSubjectUniqueId();
        if (issuerUniqueId != null) {
            return issuerUniqueId.getBytes();
        }
        return null;
    }

    public boolean verify(JKey pubKey, Session session) throws PKIException {
        Mechanism mechanism;
        DERObjectIdentifier oid = this.cert.getSignatureAlgorithm().getObjectId();
        if (oid.equals(PKCSObjectIdentifiers.md2WithRSAEncryption)) {
            mechanism = new Mechanism("MD2withRSAEncryption");
        } else if (oid.equals(PKCSObjectIdentifiers.md5WithRSAEncryption)) {
            mechanism = new Mechanism("MD5withRSAEncryption");
        } else if (oid.equals(PKCSObjectIdentifiers.sha1WithRSAEncryption) || oid.equals(PKCSObjectIdentifiers.sha1WithRSAEncryption_v1)) {
            mechanism = new Mechanism("SHA1withRSAEncryption");
        } else if (oid.equals(PKCSObjectIdentifiers.sha1WithECEncryption)) {
            mechanism = new Mechanism("SHA1withECDSA");
        } else if (oid.equals(PKCSObjectIdentifiers.sha1WithDSA)) {
            mechanism = new Mechanism("SHA1withDSA");
        } else if (oid.equals(PKCSObjectIdentifiers.sha224WithRSAEncryption)) {
            mechanism = new Mechanism("SHA224withRSAEncryption");
        } else if (oid.equals(PKCSObjectIdentifiers.sha256WithRSAEncryption)) {
            mechanism = new Mechanism("SHA256withRSAEncryption");
        } else if (oid.equals(PKCSObjectIdentifiers.sha384WithRSAEncryption)) {
            mechanism = new Mechanism("SHA384withRSAEncryption");
        } else if (oid.equals(PKCSObjectIdentifiers.sha512WithRSAEncryption)) {
            mechanism = new Mechanism("SHA512withRSAEncryption");
        } else if (oid.equals(X9ObjectIdentifiers.ecdsa_with_SHA224)) {
            mechanism = new Mechanism("SHA224withECDSA");
        } else if (oid.equals(X9ObjectIdentifiers.ecdsa_with_SHA256)) {
            mechanism = new Mechanism("SHA256withECDSA");
        } else if (oid.equals(PKCSObjectIdentifiers.sm2_with_sm3)) {
            mechanism = new Mechanism("SM3withSM2Encryption");
        } else {
            throw new PKIException(PKIException.NONSUPPORT_SIGALG, "不支持的签名算法:" + oid.getId());
        }
        try {
            return session.verifySign(mechanism, pubKey, getTBSCertificate(), getSignature());
        } catch (Exception ex) {
            throw new PKIException("6", PKIException.VERIFY_SIGN_DES, ex);
        }
    }

    public KeyUsageExt getKeyUsage() throws PKIException {
        try {
            DERBitString derBitStr = (DERBitString) getExtensionData(X509Extensions.KeyUsage);
            if (derBitStr == null) {
                return null;
            }
            return new KeyUsageExt(derBitStr);
        } catch (Exception e) {
            throw new PKIException(PKIException.CONSTRUCT_KEY_USAGE_ERR, PKIException.CONSTRUCT_KEY_USAGE_ERR_DES, e);
        }
    }

    public InsuranceNumberExt getInsuranceNumber() throws PKIException {
        try {
            DERPrintableString derPrtStr = (DERPrintableString) getExtensionData(X509Extensions.JIT_InsuranceNumber);
            if (derPrtStr == null) {
                return null;
            }
            return new InsuranceNumberExt(derPrtStr);
        } catch (Exception e) {
            throw new PKIException(PKIException.CONSTRUCT_INSURANCE_NUMBER_ERR, PKIException.CONSTRUCT_INSURANCE_NUMBER_ERR_DES, e);
        }
    }

    public ICRegistrationNumberExt getICRegistrationNumber() throws PKIException {
        try {
            DERPrintableString derPrtStr = (DERPrintableString) getExtensionData(X509Extensions.JIT_ICRegistrationNumber);
            if (derPrtStr == null) {
                return null;
            }
            return new ICRegistrationNumberExt(derPrtStr);
        } catch (Exception e) {
            throw new PKIException(PKIException.CONSTRUCT_ICREGISTRATION_NUMBER_ERR, PKIException.CONSTRUCT_ICREGISTRATION_NUMBER_ERR_DES, e);
        }
    }

    public TaxationNumberExt getTaxationNumber() throws PKIException {
        try {
            DERPrintableString derPrtStr = (DERPrintableString) getExtensionData(X509Extensions.JIT_TaxationNumber);
            if (derPrtStr == null) {
                return null;
            }
            return new TaxationNumberExt(derPrtStr);
        } catch (Exception e) {
            throw new PKIException(PKIException.CONSTRUCT_TAXATION_NUMBER_ERR, PKIException.CONSTRUCT_TAXATION_NUMBER_ERR_DES, e);
        }
    }

    public OrganizationCodeExt getOrganizationCode() throws PKIException {
        try {
            DERPrintableString derPrtStr = (DERPrintableString) getExtensionData(X509Extensions.JIT_OrganizationCode);
            if (derPrtStr == null) {
                return null;
            }
            return new OrganizationCodeExt(derPrtStr);
        } catch (Exception e) {
            throw new PKIException(PKIException.CONSTRUCT_ORGANIZATION_CODE_ERR, PKIException.CONSTRUCT_ORGANIZATION_CODE_ERR_DES, e);
        }
    }

    public IdentifyCodeExt getIdentifyCode() throws PKIException {
        try {
            ASN1Set asnSet = (ASN1Set) getExtensionData(X509Extensions.JIT_IdentifyCode);
            if (asnSet == null) {
                return null;
            }
            return new IdentifyCodeExt(asnSet);
        } catch (Exception e) {
            throw new PKIException(PKIException.CONSTRUCT_IDENTIFY_CODE_ERR, PKIException.CONSTRUCT_IDENTIFY_CODE_ERR_DES, e);
        }
    }

    public SelfDefExtension getSelfDefExtension(String oid) throws PKIException {
        DERObjectType derobjtype = new DERObjectType();
        try {
            DERObject asnSet = getSelfDefExtensionData(new DERObjectIdentifier(oid), derobjtype);
            if (asnSet == null) {
                return null;
            }
            return new SelfDefExtension(asnSet, derobjtype.GetType());
        } catch (Exception e) {
            throw new PKIException(PKIException.CONSTRUCT_SELFDEF_EXTENSION_ERR, PKIException.CONSTRUCT_SELFDEF_EXTENSION_ERR_DES, e);
        }
    }

    public SubjectKeyIdentifierExt getSubjectKeyIdentifier() throws PKIException {
        try {
            DEROctetString asnSet = (DEROctetString) getExtensionData(X509Extensions.SubjectKeyIdentifier);
            if (asnSet == null) {
                return null;
            }
            return new SubjectKeyIdentifierExt(asnSet);
        } catch (Exception e) {
            throw new PKIException(PKIException.CONSTRUCT_SUBJECT_KEY_IDENTIFIER_ERR, PKIException.CONSTRUCT_SUBJECT_KEY_IDENTIFIER_ERR_DES, e);
        }
    }

    public AuthorityKeyIdentifierExt getAuthorityKeyIdentifier() throws PKIException {
        try {
            ASN1Sequence asnSet = (ASN1Sequence) getExtensionData(X509Extensions.AuthorityKeyIdentifier);
            if (asnSet == null) {
                return null;
            }
            return new AuthorityKeyIdentifierExt(asnSet);
        } catch (Exception e) {
            throw new PKIException(PKIException.CONSTRUCT_SUBJECT_KEY_IDENTIFIER_ERR, PKIException.CONSTRUCT_SUBJECT_KEY_IDENTIFIER_ERR_DES, e);
        }
    }

    public BasicConstraintsExt getBasicConstraints() throws PKIException {
        try {
            ASN1Sequence asn1Sequence = (ASN1Sequence) getExtensionData(X509Extensions.BasicConstraints);
            if (asn1Sequence == null) {
                return null;
            }
            return new BasicConstraintsExt(asn1Sequence);
        } catch (Exception e) {
            throw new PKIException(PKIException.CONSTRUCT_BASIC_CONSTRAINTS_ERR, PKIException.CONSTRUCT_BASIC_CONSTRAINTS_ERR_DES, e);
        }
    }

    public PolicyConstraintsExt getPolicyConstraints() throws PKIException {
        try {
            ASN1Sequence asn1Sequence = (ASN1Sequence) getExtensionData(X509Extensions.PolicyConstraints);
            if (asn1Sequence == null) {
                return null;
            }
            return new PolicyConstraintsExt(asn1Sequence);
        } catch (Exception e) {
            throw new PKIException(PKIException.CONSTRUCT_POLICY_CONSTRAINTS_ERR, PKIException.CONSTRUCT_POLICY_CONSTRAINTS_ERR_DES, e);
        }
    }

    public CertificatePoliciesExt getCertificatePolicies() throws PKIException {
        try {
            ASN1Sequence asn1Sequence = (ASN1Sequence) getExtensionData(X509Extensions.CertificatePolicies);
            if (asn1Sequence == null) {
                return null;
            }
            return new CertificatePoliciesExt(asn1Sequence);
        } catch (Exception e) {
            throw new PKIException(PKIException.CONSTRUCT_CERTIFICATE_POLICIES_ERR, PKIException.CONSTRUCT_CERTIFICATE_POLICIES_ERR_DES, e);
        }
    }

    public PolicyMappingsExt getPolicyMappings() throws PKIException {
        try {
            ASN1Sequence asn1Sequence = (ASN1Sequence) getExtensionData(X509Extensions.PolicyMappings);
            if (asn1Sequence == null) {
                return null;
            }
            return new PolicyMappingsExt(asn1Sequence);
        } catch (Exception e) {
            throw new PKIException(PKIException.CONSTRUCT_POLICY_MAPPINGS_ERR, PKIException.CONSTRUCT_POLICY_MAPPINGS_ERR_DES, e);
        }
    }

    public ExtendedKeyUsageExt getExtendedKeyUsage() throws PKIException {
        try {
            ASN1Sequence asn1Sequence = (ASN1Sequence) getExtensionData(X509Extensions.ExtendedKeyUsage);
            if (asn1Sequence == null) {
                return null;
            }
            return new ExtendedKeyUsageExt(asn1Sequence);
        } catch (Exception e) {
            throw new PKIException(PKIException.CONSTRUCT_EXTENDED_KEY_USAGE_ERR, PKIException.CONSTRUCT_EXTENDED_KEY_USAGE_ERR_DES, e);
        }
    }

    public NameConstraintsExt getNameConstraints() throws PKIException {
        try {
            ASN1Sequence asn1Sequence = (ASN1Sequence) getExtensionData(X509Extensions.NameConstraints);
            if (asn1Sequence == null) {
                return null;
            }
            return new NameConstraintsExt(asn1Sequence);
        } catch (Exception e) {
            throw new PKIException(PKIException.CONSTRUCT_NAME_CONSTRAINTS_ERR, PKIException.CONSTRUCT_NAME_CONSTRAINTS_ERR_DES, e);
        }
    }

    public IssuerAlternativeNamesExt getIssuerAlternativeNames() throws PKIException {
        try {
            ASN1Sequence asn1Sequence = (ASN1Sequence) getExtensionData(X509Extensions.IssuerAlternativeName);
            if (asn1Sequence == null) {
                return null;
            }
            return new IssuerAlternativeNamesExt(asn1Sequence);
        } catch (Exception e) {
            throw new PKIException(PKIException.CONSTRUCT_ISSUER_ALTERNATIVE_NAMES_ERR, PKIException.CONSTRUCT_ISSUER_ALTERNATIVE_NAMES_ERR_DES, e);
        }
    }

    public SubjectAltNameExt getSubjectAltName() throws PKIException {
        try {
            ASN1Sequence asn1Sequence = (ASN1Sequence) getExtensionData(X509Extensions.SubjectAlternativeName);
            if (asn1Sequence == null) {
                return null;
            }
            return new SubjectAltNameExt(asn1Sequence);
        } catch (Exception e) {
            throw new PKIException(PKIException.CONSTRUCT_SUBJECT_ALT_NAME_ERR, PKIException.CONSTRUCT_SUBJECT_ALT_NAME_ERR_DES, e);
        }
    }

    public CRLDistributionPointsExt getCRLDistributionPoints() throws PKIException {
        try {
            ASN1Sequence asn1Sequence = (ASN1Sequence) getExtensionData(X509Extensions.CRLDistributionPoints);
            if (asn1Sequence == null) {
                return null;
            }
            return new CRLDistributionPointsExt(asn1Sequence);
        } catch (Exception e) {
            throw new PKIException(PKIException.CONSTRUCT_CRL_DIST_POINT_ERR, PKIException.CONSTRUCT_CRL_DIST_POINT_ERR_DES, e);
        }
    }

    public AuthorityInformationAccessExt getAuthorityInfoAccess() throws PKIException {
        try {
            ASN1Sequence asn1Sequence = (ASN1Sequence) getExtensionData(X509Extensions.AuthorityInfoAccess);
            if (asn1Sequence == null) {
                return null;
            }
            return new AuthorityInformationAccessExt(asn1Sequence);
        } catch (Exception e) {
            throw new PKIException(PKIException.CONSTRUCT_CRL_DIST_POINT_ERR, PKIException.CONSTRUCT_CRL_DIST_POINT_ERR_DES, e);
        }
    }

    private DERObject getExtensionData(DERObjectIdentifier oid) throws Exception {
        return ByteToDERObject(getExtensionByteData(oid));
    }

    private DERObject ByteToDERObject(byte[] Data) throws Exception {
        if (Data == null) {
            return null;
        }
        try {
            return new ASN1InputStream(new ByteArrayInputStream(Data)).readObject();
        } catch (Exception ex) {
            throw ex;
        }
    }

    private DERObject getSelfDefExtensionData(DERObjectIdentifier oid, DERObjectType type) throws Exception {
        byte[] data = getExtensionByteData(oid);
        if (data == null) {
            return null;
        }
        switch (data[0]) {
            case 1:
                type.SetType(AbstractSelfDefExtension.BOOLEAN);
                return ByteToDERObject(data);
            case 2:
                type.SetType(AbstractSelfDefExtension.INTEGER);
                return ByteToDERObject(data);
            case 12:
                type.SetType(AbstractSelfDefExtension.UTF8STRING);
                return ByteToDERObject(data);
            case 19:
                type.SetType(AbstractSelfDefExtension.PRINTABLESTRING);
                return ByteToDERObject(data);
            case 22:
                type.SetType(AbstractSelfDefExtension.IA5STRING);
                return ByteToDERObject(data);
            default:
                type.SetType(AbstractSelfDefExtension.USERDEFINED);
                return new DEROctetString(data);
        }
    }

    private byte[] getExtensionByteData(DERObjectIdentifier oid) throws Exception {
        X509Extension extension;
        X509Extensions extensions = this.cert.getTBSCertificate().getExtensions();
        if (extensions == null || (extension = extensions.getExtension(oid)) == null) {
            return null;
        }
        return extension.getValue().getOctets();
    }

    private class DERObjectType {
        private String Type;

        private DERObjectType() {
            this.Type = null;
        }

        public void SetType(String value) {
            this.Type = value;
        }

        public String GetType() {
            return this.Type;
        }
    }

    public static void main(String[] args) {
    }
}
