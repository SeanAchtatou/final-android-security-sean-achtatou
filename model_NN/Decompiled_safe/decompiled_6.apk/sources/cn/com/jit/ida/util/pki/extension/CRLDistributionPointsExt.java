package cn.com.jit.ida.util.pki.extension;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.x509.CRLDistPoint;
import cn.com.jit.ida.util.pki.asn1.x509.DistributionPoint;
import cn.com.jit.ida.util.pki.asn1.x509.X509Extensions;
import java.util.Vector;

public class CRLDistributionPointsExt extends AbstractStandardExtension {
    public static final int DIRECTORYNAME = 4;
    public static final int DNSNAME = 2;
    public static final int EDIPARTYNAME = 5;
    public static final int EDIPARTYNAME_BMPSTRING = 204;
    public static final int EDIPARTYNAME_PRINTABLESTRING = 201;
    public static final int EDIPARTYNAME_TELETEXSTRING = 200;
    public static final int EDIPARTYNAME_UNIVERSALSTRING = 202;
    public static final int EDIPARTYNAME_UTF8STRING = 203;
    public static final int FULL_NAME = 0;
    public static final int IPADDRESS = 7;
    public static final int NAMERELATIVETOCRLISSUER_BMPSTRING = 304;
    public static final int NAMERELATIVETOCRLISSUER_PRINTABLESTRING = 301;
    public static final int NAMERELATIVETOCRLISSUER_TELETEXSTRING = 300;
    public static final int NAMERELATIVETOCRLISSUER_UNIVERSALSTRING = 302;
    public static final int NAMERELATIVETOCRLISSUER_UTF8STRING = 303;
    public static final int NAME_RELATIVE_TO_CRL_ISSUER = 1;
    public static final int OTHERNAME_GUID = 101;
    public static final int OTHERNAME_UPN = 100;
    public static final String OTHER_NAME_GUID_OID = "1.3.6.1.4.1.311.25.1";
    public static final String OTHER_NAME_UPN_OID = "1.3.6.1.4.1.311.20.2.3";
    public static final int REASON_AA_COMPROMISE = 32768;
    public static final int REASON_AFFILIATION_CHANGED = 16;
    public static final int REASON_CA_COMPROMISE = 32;
    public static final int REASON_CERTIFICATE_HOLD = 2;
    public static final int REASON_CESSATION_OF_OPERATION = 4;
    public static final int REASON_KEY_COMPROMISE = 64;
    public static final int REASON_PRIVILEGE_WITHDRAWN = 1;
    public static final int REASON_SUPERSEDED = 8;
    public static final int REASON_UNUSED = 128;
    public static final int REGISTEREDID = 8;
    public static final int RFC822NAME = 1;
    public static final int TYPE_NONE = -1;
    public static final int UNIFORMRESOURCEIDENTIFIER = 6;
    public static final int X400ADDRESS = 3;
    private Vector crlDistPoints = null;

    public CRLDistributionPointsExt() {
        this.OID = X509Extensions.CRLDistributionPoints.getId();
        this.critical = false;
        this.crlDistPoints = new Vector();
    }

    public CRLDistributionPointsExt(ASN1Sequence asn1Sequence) {
        this.OID = X509Extensions.CRLDistributionPoints.getId();
        this.critical = false;
        this.crlDistPoints = new Vector();
        for (int i = 0; i < asn1Sequence.size(); i++) {
            this.crlDistPoints.add(new DistributionPointsExt((ASN1Sequence) asn1Sequence.getObjectAt(i)));
        }
    }

    public void addDistributionPoint() {
        this.crlDistPoints.add(new DistributionPointsExt());
    }

    public int getDistributionPointCount() {
        return this.crlDistPoints.size();
    }

    public DistributionPointsExt getDistributionPoint(int index) {
        return (DistributionPointsExt) this.crlDistPoints.get(index);
    }

    public byte[] encode() throws PKIException {
        int length = this.crlDistPoints.size();
        DistributionPoint[] dp = new DistributionPoint[length];
        for (int i = 0; i < length; i++) {
            dp[i] = (DistributionPoint) ((DistributionPointsExt) this.crlDistPoints.get(i)).getObject();
        }
        return new DEROctetString(new CRLDistPoint(dp).getDERObject()).getOctets();
    }

    public boolean getCritical() {
        return this.critical;
    }

    public String getOID() {
        return this.OID;
    }

    public void setCritical(boolean critical) {
        this.critical = critical;
    }
}
