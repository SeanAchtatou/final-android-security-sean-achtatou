package cn.com.jit.ida.util.pki.extension;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DERBMPString;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DEREncodableVector;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DERPrintableString;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.DERT61String;
import cn.com.jit.ida.util.pki.asn1.DERUTF8String;

public class AttributeTypeAndValueExt {
    public static final int BMPSTRING = 304;
    public static final int PRINTABLESTRING = 301;
    public static final int TELETEXSTRING = 300;
    public static final int UNIVERSALSTRING = 302;
    public static final int UTF8STRING = 303;
    private DEREncodableVector seqTypeAndValue;

    public AttributeTypeAndValueExt() {
        this.seqTypeAndValue = null;
        this.seqTypeAndValue = new DEREncodableVector();
    }

    public AttributeTypeAndValueExt(ASN1Sequence seq) {
        this.seqTypeAndValue = null;
        this.seqTypeAndValue = new DEREncodableVector();
        for (int i = 0; i < seq.size(); i++) {
            this.seqTypeAndValue.add(seq.getObjectAt(i));
        }
    }

    public ASN1Sequence getObject() {
        return new DERSequence(this.seqTypeAndValue);
    }

    public void setAttributeType(String OID) {
        this.seqTypeAndValue.add(new DERObjectIdentifier(OID));
    }

    public String getAttributeType() {
        return ((DERObjectIdentifier) this.seqTypeAndValue.get(0)).getId();
    }

    public void setAttributeValue(int type, String value) throws PKIException {
        this.seqTypeAndValue.add(TypeToDEREncodable(type, value));
    }

    public String getAttributeValue() {
        DEREncodable obj = this.seqTypeAndValue.get(1);
        if (obj instanceof DERT61String) {
            return DERT61String.getInstance(obj).getString();
        }
        if (obj instanceof DERPrintableString) {
            return DERPrintableString.getInstance(obj).getString();
        }
        if (obj instanceof DERUTF8String) {
            return DERUTF8String.getInstance(obj).getString();
        }
        if (obj instanceof DERBMPString) {
            return DERBMPString.getInstance(obj).getString();
        }
        return null;
    }

    private DEREncodable TypeToDEREncodable(int valueType, String value) throws PKIException {
        switch (valueType) {
            case 300:
                return new DERT61String(value);
            case 301:
                return new DERPrintableString(value);
            case 302:
                throw new PKIException(PKIException.EXT_ENCODING_DATATYPE_NOT, PKIException.EXT_ENCODING_DATATYPE_NOT_DES);
            case 303:
                return new DERUTF8String(value);
            case 304:
                return new DERBMPString(value);
            default:
                return null;
        }
    }
}
