package cn.com.jit.ida.util.pki.cipher.lib;

import cn.com.jit.ida.util.pki.ECDSAParser;
import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.PKIToolConfig;
import cn.com.jit.ida.util.pki.Parser;
import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1InputStream;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.BERSequence;
import cn.com.jit.ida.util.pki.asn1.DERBitString;
import cn.com.jit.ida.util.pki.asn1.DERNull;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.pkcs.PKCSObjectIdentifiers;
import cn.com.jit.ida.util.pki.asn1.x509.AlgorithmIdentifier;
import cn.com.jit.ida.util.pki.cipher.JHandle;
import cn.com.jit.ida.util.pki.cipher.JKey;
import cn.com.jit.ida.util.pki.cipher.JKeyPair;
import cn.com.jit.ida.util.pki.cipher.Mechanism;
import cn.com.jit.ida.util.pki.cipher.Session;
import cn.com.jit.ida.util.pki.cipher.param.CBCParam;
import cn.com.jit.ida.util.pki.cipher.param.GenKeyAttribute;
import cn.com.jit.ida.util.pki.cipher.softsm.Util;
import cn.com.jit.ida.util.pki.encoders.Hex;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class JHARDLib implements Session {
    private static final long AES_KEY_GEN = 4224;
    private static final String CKC_KEYTYPE_DSA_PRIVATEKEY = "8";
    private static final String CKC_KEYTYPE_DSA_PRIVATEKEYID = "128";
    private static final String CKC_KEYTYPE_DSA_PUBLICKEY = "4";
    private static final String CKC_KEYTYPE_DSA_PUBLICKEYID = "64";
    private static final String CKC_KEYTYPE_ECDSA_PRIVATEKEY = "257";
    private static final String CKC_KEYTYPE_ECDSA_PRIVATEKEYID = "259";
    private static final String CKC_KEYTYPE_ECDSA_PUBLICKEY = "256";
    private static final String CKC_KEYTYPE_ECDSA_PUBLICKEYID = "258";
    private static final String CKC_KEYTYPE_MASTERKEYID = "153";
    private static final String CKC_KEYTYPE_RSA_PRIVATEKEY = "2";
    private static final String CKC_KEYTYPE_RSA_PRIVATEKEYID = "32";
    private static final String CKC_KEYTYPE_RSA_PUBLICKEY = "1";
    private static final String CKC_KEYTYPE_RSA_PUBLICKEYID = "16";
    private static final String CKC_KEYTYPE_SM2_PRIVATEKEY = "-2147483391";
    private static final String CKC_KEYTYPE_SM2_PRIVATEKEYID = "-2147483389";
    private static final String CKC_KEYTYPE_SM2_PUBLICKEY = "-2147483392";
    private static final String CKC_KEYTYPE_SM2_PUBLICKEYID = "-2147483390";
    private static final String CKC_KEYTYPE_SYMMETRY = "145";
    private static final long CKM_AES_CBC = 4226;
    private static final long CKM_AES_ECB = 4225;
    private static final long CKM_RSA_RAW = -2130706431;
    private static final long CKM_SF33_CBC = -2147483598;
    private static final long CKM_SF33_ECB = -2147483599;
    private static final long DES3_CBC = 307;
    private static final long DES3_ECB = 306;
    private static final long DES3_KEY = 305;
    private static final long DES_CBC = 290;
    private static final long DES_ECB = 289;
    private static final long DES_KEY = 288;
    private static final long DSA = 16;
    private static final long DSA_SHA1 = 18;
    private static final long DSA_SHA224 = 19;
    private static final long DSA_SHA256 = 20;
    private static final long ECDSA = 4160;
    private static final long ECDSA_SHA1 = 4162;
    private static final long ECDSA_SHA224 = 4163;
    private static final long ECDSA_SHA256 = 4164;
    private static final String JIT_CKR_ARGUMENTS_BAD = "7";
    private static final String JIT_CKR_ATTRIBUTE_READ_ONLY = "16";
    private static final String JIT_CKR_ATTRIBUTE_SENSITIVE = "17";
    private static final String JIT_CKR_ATTRIBUTE_TYPE_INVALID = "18";
    private static final String JIT_CKR_ATTRIBUTE_VALUE_INVALID = "19";
    private static final String JIT_CKR_BUFFER_TOO_SMALL = "336";
    private static final String JIT_CKR_CANT_LOCK = "10";
    private static final String JIT_CKR_CRYPTOKI_ALREADY_INITIALIZED = "401";
    private static final String JIT_CKR_CRYPTOKI_NOT_INITIALIZED = "400";
    private static final String JIT_CKR_DATA_INVALID = "32";
    private static final String JIT_CKR_DATA_LEN_RANGE = "33";
    private static final String JIT_CKR_DEVICE_ERROR = "48";
    private static final String JIT_CKR_DEVICE_MEMORY = "49";
    private static final String JIT_CKR_DEVICE_REMOVED = "50";
    private static final String JIT_CKR_DOMAIN_PARAMS_INVALID = "304";
    private static final String JIT_CKR_ENCRYPTED_DATA_INVALID = "64";
    private static final String JIT_CKR_ENCRYPTED_DATA_LEN_RANGE = "65";
    private static final String JIT_CKR_FUNCTION_CANCELED = "80";
    private static final String JIT_CKR_FUNCTION_FAILED = "6";
    private static final String JIT_CKR_FUNCTION_NOT_PARALLEL = "81";
    private static final String JIT_CKR_FUNCTION_NOT_SUPPORTED = "84";
    private static final String JIT_CKR_GENERAL_ERROR = "5";
    private static final String JIT_CKR_HOST_MEMORY = "2";
    private static final String JIT_CKR_INFORMATION_SENSITIVE = "368";
    private static final String JIT_CKR_KEY_CHANGED = "101";
    private static final String JIT_CKR_KEY_FUNCTION_NOT_PERMITTED = " 104";
    private static final String JIT_CKR_KEY_HANDLE_INVALID = "96";
    private static final String JIT_CKR_KEY_INDIGESTIBLE = "103";
    private static final String JIT_CKR_KEY_NEEDED = "102";
    private static final String JIT_CKR_KEY_NOT_NEEDED = "100";
    private static final String JIT_CKR_KEY_NOT_WRAPPABLE = "105";
    private static final String JIT_CKR_KEY_SIZE_RANGE = "98";
    private static final String JIT_CKR_KEY_TYPE_INCONSISTENT = "99";
    private static final String JIT_CKR_KEY_UNEXTRACTABLE = "106";
    private static final String JIT_CKR_MECHANISM_INVALID = "112";
    private static final String JIT_CKR_MECHANISM_PARAM_INVALID = "113";
    private static final String JIT_CKR_MEMORYNOTNULL = "4098";
    private static final String JIT_CKR_MUTEX_BAD = "416";
    private static final String JIT_CKR_MUTEX_NOT_LOCKED = "417";
    private static final String JIT_CKR_NEED_TO_CREATE_THREADS = "9";
    private static final String JIT_CKR_NOTSUPPORT = "4099";
    private static final String JIT_CKR_NO_EVENT = "8";
    private static final String JIT_CKR_OBJECT_HANDLE_INVALID = "130";
    private static final String JIT_CKR_OK = "0";
    private static final String JIT_CKR_OPERATION_ACTIVE = "144";
    private static final String JIT_CKR_OPERATION_NOT_INITIALIZED = "145";
    private static final String JIT_CKR_OUTOFMEMORY = "4097";
    private static final String JIT_CKR_PIN_EXPIRED = "163";
    private static final String JIT_CKR_PIN_INCORRECT = "160";
    private static final String JIT_CKR_PIN_INVALID = "161";
    private static final String JIT_CKR_PIN_LEN_RANGE = "162";
    private static final String JIT_CKR_PIN_LOCKED = "164";
    private static final String JIT_CKR_RANDOM_NO_RNG = "289";
    private static final String JIT_CKR_RANDOM_SEED_NOT_SUPPORTED = "288";
    private static final String JIT_CKR_SAVED_STATE_INVALID = "352";
    private static final String JIT_CKR_SESSION_CLOSED = "176";
    private static final String JIT_CKR_SESSION_COUNT = "177";
    private static final String JIT_CKR_SESSION_EXISTS = "182";
    private static final String JIT_CKR_SESSION_HANDLE_INVALID = "179";
    private static final String JIT_CKR_SESSION_PARALLEL_NOT_SUPPORTED = "180";
    private static final String JIT_CKR_SESSION_READ_ONLY = "181";
    private static final String JIT_CKR_SESSION_READ_ONLY_EXISTS = "183";
    private static final String JIT_CKR_SESSION_READ_WRITE_SO_EXISTS = "184";
    private static final String JIT_CKR_SIGNATURE_INVALID = "192";
    private static final String JIT_CKR_SIGNATURE_LEN_RANGE = "193";
    private static final String JIT_CKR_SLOT_ID_INVALID = "3";
    private static final String JIT_CKR_STATE_UNSAVEABLE = "384";
    private static final String JIT_CKR_TEMPLATE_INCOMPLETE = "208";
    private static final String JIT_CKR_TEMPLATE_INCONSISTENT = "209";
    private static final String JIT_CKR_TOKEN_NOT_PRESENT = "224";
    private static final String JIT_CKR_TOKEN_NOT_RECOGNIZED = "225";
    private static final String JIT_CKR_TOKEN_WRITE_PROTECTED = "226";
    private static final String JIT_CKR_UNWRAPPING_KEY_HANDLE_INVALID = "240";
    private static final String JIT_CKR_UNWRAPPING_KEY_SIZE_RANGE = "241";
    private static final String JIT_CKR_UNWRAPPING_KEY_TYPE_INCONSISTENT = "242";
    private static final String JIT_CKR_USER_ALREADY_LOGGED_IN = "256";
    private static final String JIT_CKR_USER_ANOTHER_ALREADY_LOGGED_IN = " 260";
    private static final String JIT_CKR_USER_NOT_LOGGED_IN = "257";
    private static final String JIT_CKR_USER_PIN_NOT_INITIALIZED = "258";
    private static final String JIT_CKR_USER_TOO_MANY_TYPES = "261";
    private static final String JIT_CKR_USER_TYPE_INVALID = "259";
    private static final String JIT_CKR_WRAPPED_KEY_INVALID = "272";
    private static final String JIT_CKR_WRAPPED_KEY_LEN_RANGE = "274";
    private static final String JIT_CKR_WRAPPING_KEY_HANDLE_INVALID = "275";
    private static final String JIT_CKR_WRAPPING_KEY_SIZE_RANGE = "276";
    private static final String JIT_CKR_WRAPPING_KEY_TYPE_INCONSISTENT = "277";
    private static final String JIT_CREATEPRIVATEKEY_FAIL = "5385";
    private static final String JIT_CUSTOM_LENGTH_INVALID = "5121";
    private static final String JIT_CUSTOM_PRIVATEKEY_ERR = "4865";
    private static final String JIT_CUSTOM_PUBLICKEY_ERR = "4866";
    private static final String JIT_DECRYPTFINAL_FAIL = "5407";
    private static final String JIT_DECRYPTINIT_FAIL = "5405";
    private static final String JIT_DECRYPTLENGTH_NULL = "4612";
    private static final String JIT_DECRYPTUPDATE_FAIL = "5406";
    private static final String JIT_ENCRYPTFINAL_FAIL = "5404";
    private static final String JIT_ENCRYPTINIT_FAIL = "5386";
    private static final String JIT_ENCRYPTLENGTH_NULL = "4611";
    private static final String JIT_ENCRYPTUPDATE_FAIL = "5403";
    private static final String JIT_FUNCTIONLISTPTR_NULL = "4610";
    private static final String JIT_GEFUNCTIONLIST_FAIL = "4354";
    private static final String JIT_GETCERT_FAIL = "5384";
    private static final String JIT_GETKEYLENGTH_FAIL = "5380";
    private static final String JIT_GETKEY_FAIL = "5381";
    private static final String JIT_GETMASTERKEY_FAIL = "5379";
    private static final String JIT_GETPRIVATEKEY_FAIL = "5383";
    private static final String JIT_GETPUBLICKEY_FAIL = "5382";
    private static final String JIT_GETUNEXPORTPRIVATEKEY_FAIL = "5378";
    private static final String JIT_GETUNEXPORTPUBLICKEY_FAIL = "5377";
    private static final String JIT_HANDLE_NULL = "4609";
    private static final String JIT_KEYLENGTHE_INVALID = "5123";
    private static final String JIT_KEYTYPE_INVALID = "5122";
    private static final String JIT_LOADLIBRARY_FAIL = "4353";
    private static final String JIT_MECHANISM_INVALID = "5124";
    private static final String JIT_PARSE_FAIL = "4096";
    private static final long MD2 = 512;
    private static final long MD2_HMAC = 513;
    private static final long MD5 = 528;
    private static final long MD5_HMAC = 529;
    private static final long MD5_RSA_PKCS = 5;
    public static final String PROVIDER = "HARDLIB";
    private static final long RC2_CBC = 258;
    private static final long RC2_ECB = 257;
    private static final long RC2_KEY = 256;
    private static final long RC4 = 273;
    private static final long RC4_KEY = 272;
    private static final long RSA = 0;
    private static final long RSA_PKCS = 1;
    private static final long SCB2_CBC = -2147483631;
    private static final long SCB2_ECB = -2147483632;
    private static final long SCB2_KEY_GEN = -2147483633;
    private static final long SF33_KEY_GEN = -2147483600;
    private static final long SHA1 = 544;
    private static final long SHA1_HMAC = 545;
    private static final long SHA1_RSA_PKCS = 6;
    private static final long SHA224 = 592;
    private static final long SHA224_RSA_PKCS = 70;
    private static final long SHA256 = 608;
    private static final long SHA256_RSA_PKCS = 64;
    private static final long SHA384 = 624;
    private static final long SHA384_RSA_PKCS = 65;
    private static final long SHA512 = 640;
    private static final long SHA512_RSA_PKCS = 66;
    private static final long SW_SM2 = -2147450879;
    private static final long SW_SM2_RAW = -2147450368;
    private static final long SW_SM3 = -2147483643;
    private static final long SW_SM3_SM2 = -2147450624;
    private static final String[][] hardException = {new String[]{"0", "成功"}, new String[]{"2", "内存错误"}, new String[]{"3", "SLOTID无效"}, new String[]{"5", "一般的错误"}, new String[]{"6", "函数错误"}, new String[]{"7", "参数错误"}, new String[]{"8", "没有事件"}, new String[]{"9", "需要创建线程"}, new String[]{JIT_CKR_CANT_LOCK, "倾斜锁定"}, new String[]{"16", "属性只读"}, new String[]{JIT_CKR_ATTRIBUTE_SENSITIVE, "属性敏感"}, new String[]{JIT_CKR_ATTRIBUTE_TYPE_INVALID, "属性类型非法"}, new String[]{JIT_CKR_ATTRIBUTE_VALUE_INVALID, "属性值无效"}, new String[]{"32", "数据无效"}, new String[]{JIT_CKR_DATA_LEN_RANGE, "数据长度错误"}, new String[]{JIT_CKR_DEVICE_ERROR, "设备错误"}, new String[]{JIT_CKR_DEVICE_MEMORY, "设备内存出错"}, new String[]{JIT_CKR_DEVICE_REMOVED, "设备已删除"}, new String[]{"64", "加密的数据无效"}, new String[]{JIT_CKR_ENCRYPTED_DATA_LEN_RANGE, "加密的数据长度错误"}, new String[]{JIT_CKR_FUNCTION_CANCELED, "接口已取消"}, new String[]{JIT_CKR_FUNCTION_NOT_PARALLEL, "接口不能并行"}, new String[]{JIT_CKR_FUNCTION_NOT_SUPPORTED, "不支持此接口"}, new String[]{JIT_CKR_KEY_HANDLE_INVALID, "非法的密钥句柄"}, new String[]{JIT_CKR_KEY_SIZE_RANGE, "密钥的长度错误"}, new String[]{JIT_CKR_KEY_TYPE_INCONSISTENT, " 密钥类型矛盾"}, new String[]{JIT_CKR_KEY_NOT_NEEDED, "不需要附加的密钥"}, new String[]{JIT_CKR_KEY_CHANGED, "密钥已更改"}, new String[]{JIT_CKR_KEY_NEEDED, "需要附加密钥"}, new String[]{JIT_CKR_KEY_INDIGESTIBLE, "密钥不能被解析"}, new String[]{JIT_CKR_KEY_FUNCTION_NOT_PERMITTED, "接口不允许"}, new String[]{JIT_CKR_KEY_NOT_WRAPPABLE, "密钥不能打包"}, new String[]{JIT_CKR_KEY_UNEXTRACTABLE, "指定的私有或保密密钥不能被打包"}, new String[]{JIT_CKR_MECHANISM_INVALID, "无效的机制"}, new String[]{JIT_CKR_MECHANISM_PARAM_INVALID, "无效的机制参数"}, new String[]{JIT_CKR_OBJECT_HANDLE_INVALID, "非法的对象句柄"}, new String[]{JIT_CKR_OPERATION_ACTIVE, "操作已激活"}, new String[]{"145", "操作没有初始化"}, new String[]{JIT_CKR_PIN_INCORRECT, "错误的PIN码"}, new String[]{JIT_CKR_PIN_INVALID, "无效的PIN"}, new String[]{JIT_CKR_PIN_LEN_RANGE, "无效的PIN的长度"}, new String[]{JIT_CKR_PIN_EXPIRED, "PIN码已经过期"}, new String[]{JIT_CKR_PIN_LOCKED, "PIN码被锁定"}, new String[]{JIT_CKR_SESSION_CLOSED, "会话已经关闭"}, new String[]{JIT_CKR_SESSION_COUNT, "会话超出总数"}, new String[]{JIT_CKR_SESSION_HANDLE_INVALID, "无效的会话句柄"}, new String[]{JIT_CKR_SESSION_PARALLEL_NOT_SUPPORTED, "不支持并行"}, new String[]{JIT_CKR_SESSION_READ_ONLY, "会话只读"}, new String[]{JIT_CKR_SESSION_EXISTS, "会话已经存在"}, new String[]{JIT_CKR_SESSION_READ_ONLY_EXISTS, "会话已经只读"}, new String[]{JIT_CKR_SESSION_READ_WRITE_SO_EXISTS, "会话已经支持读写"}, new String[]{JIT_CKR_SIGNATURE_INVALID, "签名无效"}, new String[]{JIT_CKR_SIGNATURE_LEN_RANGE, "签名长度无效"}, new String[]{JIT_CKR_TEMPLATE_INCOMPLETE, "模板未完成"}, new String[]{JIT_CKR_TEMPLATE_INCONSISTENT, "模板不一致"}, new String[]{JIT_CKR_TOKEN_NOT_PRESENT, "TOKEN不存在"}, new String[]{JIT_CKR_TOKEN_NOT_RECOGNIZED, "TOKEN未识别"}, new String[]{JIT_CKR_TOKEN_WRITE_PROTECTED, "TOKEN写保护"}, new String[]{JIT_CKR_UNWRAPPING_KEY_HANDLE_INVALID, "无效的解包密钥句柄"}, new String[]{JIT_CKR_UNWRAPPING_KEY_SIZE_RANGE, "接驳阿密钥尺寸过大"}, new String[]{JIT_CKR_UNWRAPPING_KEY_TYPE_INCONSISTENT, "解包密钥类型不一致"}, new String[]{"256", "用户已登陆"}, new String[]{"257", "用户未登陆"}, new String[]{"258", "用户PIN码未初始化"}, new String[]{"259", "用户类型非法"}, new String[]{JIT_CKR_USER_ANOTHER_ALREADY_LOGGED_IN, "另外用户已登陆"}, new String[]{JIT_CKR_USER_TOO_MANY_TYPES, "用户类型太多"}, new String[]{JIT_CKR_WRAPPED_KEY_INVALID, "打包密钥无效"}, new String[]{JIT_CKR_WRAPPED_KEY_LEN_RANGE, "打包密钥过长"}, new String[]{JIT_CKR_WRAPPING_KEY_HANDLE_INVALID, "打包密钥的句柄无效"}, new String[]{JIT_CKR_WRAPPING_KEY_SIZE_RANGE, "打包密钥尺寸过大"}, new String[]{JIT_CKR_WRAPPING_KEY_TYPE_INCONSISTENT, "打包密钥类型不一致"}, new String[]{JIT_CKR_RANDOM_SEED_NOT_SUPPORTED, "不支持随机种子"}, new String[]{JIT_CKR_RANDOM_NO_RNG, "没有随机数字产生器"}, new String[]{JIT_CKR_DOMAIN_PARAMS_INVALID, "DOMAIN参数无效"}, new String[]{JIT_CKR_BUFFER_TOO_SMALL, "分配的空间小"}, new String[]{JIT_CKR_SAVED_STATE_INVALID, "保存的状态无效"}, new String[]{JIT_CKR_INFORMATION_SENSITIVE, "信息敏感"}, new String[]{JIT_CKR_STATE_UNSAVEABLE, "状态不能保存"}, new String[]{JIT_CKR_CRYPTOKI_NOT_INITIALIZED, "加密库未初始化"}, new String[]{JIT_CKR_CRYPTOKI_ALREADY_INITIALIZED, "加密库已经初始化"}, new String[]{JIT_CKR_MUTEX_BAD, "互斥体坏"}, new String[]{JIT_CKR_MUTEX_NOT_LOCKED, "互斥体未锁定"}, new String[]{JIT_PARSE_FAIL, "解析加密机返回数据失败"}, new String[]{JIT_CKR_OUTOFMEMORY, "内存不足"}, new String[]{JIT_CKR_MEMORYNOTNULL, "传入内存参数不为空"}, new String[]{JIT_CKR_NOTSUPPORT, "当前版本没有支持"}, new String[]{JIT_LOADLIBRARY_FAIL, "加载P11库失败"}, new String[]{JIT_GEFUNCTIONLIST_FAIL, "获取动态库函数列表失败"}, new String[]{JIT_HANDLE_NULL, "句柄参数为空"}, new String[]{JIT_FUNCTIONLISTPTR_NULL, "P11函数列表指针为空"}, new String[]{JIT_ENCRYPTLENGTH_NULL, "加密后长度为零"}, new String[]{JIT_DECRYPTLENGTH_NULL, "解密后长度为零"}, new String[]{JIT_CUSTOM_PRIVATEKEY_ERR, "获取到的自定义私钥信息错误"}, new String[]{JIT_CUSTOM_PUBLICKEY_ERR, "获取到的自定义公钥信息错误"}, new String[]{JIT_CUSTOM_LENGTH_INVALID, "获取到的自定义数据长度非法"}, new String[]{JIT_KEYTYPE_INVALID, "密钥类型不支持"}, new String[]{JIT_KEYLENGTHE_INVALID, "密钥长度不支持"}, new String[]{JIT_MECHANISM_INVALID, "算法不支持"}, new String[]{JIT_GETUNEXPORTPUBLICKEY_FAIL, "没有找到不可导出密钥对中的公钥"}, new String[]{JIT_GETUNEXPORTPRIVATEKEY_FAIL, "没有找到不可导出密钥对中的私钥"}, new String[]{JIT_GETMASTERKEY_FAIL, "获取对称密钥信息长度失败"}, new String[]{JIT_GETKEYLENGTH_FAIL, "获取对称密钥信息长度失败"}, new String[]{JIT_GETKEY_FAIL, "获取对称密钥信息失败"}, new String[]{JIT_GETPUBLICKEY_FAIL, "获取公钥信息失败"}, new String[]{JIT_GETPRIVATEKEY_FAIL, "获取私钥信息失败"}, new String[]{JIT_GETCERT_FAIL, "获取证书对象失败"}, new String[]{JIT_CREATEPRIVATEKEY_FAIL, "建立私钥对象失败"}, new String[]{JIT_ENCRYPTINIT_FAIL, "加密操作初始化失败"}, new String[]{JIT_ENCRYPTUPDATE_FAIL, PKIException.ENCRYPT_DES}, new String[]{JIT_ENCRYPTFINAL_FAIL, "加密操作结束失败"}, new String[]{JIT_DECRYPTINIT_FAIL, "解密操作初始化失败"}, new String[]{JIT_DECRYPTUPDATE_FAIL, PKIException.DECRYPT_DES}, new String[]{JIT_DECRYPTFINAL_FAIL, "解密操作结束失败"}};
    private PKIToolConfig CfgTag = null;
    private boolean isInit = false;
    private String tag = "PKITOOL";

    private native int DestroyKeyPair(long j, int i, byte[] bArr, byte[] bArr2, byte[] bArr3);

    private native byte[] decrypt(long j, int i, byte[] bArr, byte[] bArr2, byte[] bArr3, byte[] bArr4);

    private native byte[] destroyCert(byte[] bArr, byte[] bArr2, byte[] bArr3);

    private native byte[] destroyCerts(byte[] bArr, byte[] bArr2);

    private native byte[] digest(long j, byte[] bArr, byte[] bArr2);

    private native byte[] encrypt(long j, int i, byte[] bArr, byte[] bArr2, byte[] bArr3, byte[] bArr4);

    private native byte[] findKeyPair(long j, int i, byte[] bArr, byte[] bArr2, byte[] bArr3);

    private native byte[] generateKey(long j, int i, byte[] bArr);

    private native byte[] generateKeyPair(long j, int i, byte[] bArr);

    private native byte[] generateRandom(int i, byte[] bArr);

    private native byte[] getCert(byte[] bArr, byte[] bArr2);

    private native byte[] getVersion();

    private native byte[] importCert(byte[] bArr, byte[] bArr2, byte[] bArr3, byte[] bArr4);

    private native byte[] masterDecrypt(long j, int i, byte[] bArr, byte[] bArr2, byte[] bArr3, byte[] bArr4);

    private native byte[] masterEncrypt(long j, int i, byte[] bArr, byte[] bArr2, byte[] bArr3, byte[] bArr4);

    private native int p11Finalize(byte[] bArr);

    private native int p11Initialize(byte[] bArr, byte[] bArr2, int i);

    private native byte[] sign(long j, int i, byte[] bArr, byte[] bArr2, byte[] bArr3);

    private native byte[] updateKeyPair(long j, byte[] bArr, byte[] bArr2, int i, byte[] bArr3, byte[] bArr4, byte[] bArr5, byte[] bArr6);

    private native byte[] verifySign(long j, int i, byte[] bArr, byte[] bArr2, byte[] bArr3, byte[] bArr4);

    private native byte[] wrapKeyEnc(byte[] bArr, byte[] bArr2, long j, long j2, byte[] bArr3, byte[] bArr4, byte[] bArr5);

    public native byte[] decryptFinal(long j, long j2, byte[] bArr, byte[] bArr2);

    public native byte[] decryptInit(long j, int i, byte[] bArr, byte[] bArr2, byte[] bArr3);

    public native byte[] decryptUpdate(long j, long j2, byte[] bArr, byte[] bArr2);

    public native byte[] encryptFinal(long j, long j2, byte[] bArr, byte[] bArr2);

    public native byte[] encryptInit(long j, int i, byte[] bArr, byte[] bArr2, byte[] bArr3);

    public native byte[] encryptUpdate(long j, long j2, byte[] bArr, byte[] bArr2);

    public native byte[] getMRTDVision(byte[] bArr, byte[] bArr2);

    public JHARDLib() {
        System.loadLibrary("SessionDll");
    }

    public void Initialize() throws PKIException {
        Initialize("PKITOOL");
    }

    public void Initialize(String cfg) throws PKIException {
        this.tag = cfg;
        if (this.CfgTag == null) {
            this.CfgTag = new PKIToolConfig();
            this.CfgTag.LoadOpt(cfg);
        }
        byte[] libPath = new String(this.CfgTag.getP11File()).getBytes();
        int slot = 0;
        if (this.CfgTag.getSlot() != null) {
            slot = Integer.valueOf(this.CfgTag.getSlot()).intValue();
        }
        int errnum = p11Initialize(libPath, this.tag.getBytes(), slot);
        if (errnum != 0) {
            throw new PKIException("8110", PKIException.SYM_KEY_DES, new PKIException(Integer.toString(errnum), errorDesc(Integer.toString(errnum))));
        }
    }

    public void Finalize() throws PKIException {
        int errnum = p11Finalize(this.tag.getBytes());
        if (errnum != 0) {
            throw new PKIException("8110", PKIException.SYM_KEY_DES, new PKIException(Integer.toString(errnum), errorDesc(Integer.toString(errnum))));
        }
    }

    public JKey generateKey(Mechanism mechanism, int keyLength) throws PKIException {
        String mType = mechanism.getMechanismType();
        if (mType.equals("DES") || mType.equals("SF33") || mType.equals("DESede") || mType.equals("RC2") || mType.equals("RC4") || mType.equals("AES") || mType.equals("SCB2")) {
            ByteArrayInputStream in = new ByteArrayInputStream(generateKey(JavaToJNIMechanismType(mType), keyLength, this.tag.getBytes()));
            Properties recvData = new Properties();
            try {
                recvData.load(in);
                String error = recvData.getProperty("Error");
                String lType = recvData.getProperty("lType");
                String pKey = recvData.getProperty("pKey");
                if (pKey == null || !error.equals("0")) {
                    throw new PKIException("8110", PKIException.SYM_KEY_DES, new PKIException(error, errorDesc(error)));
                }
                new Hex();
                return new JKey(JNIToJavaKeyType(lType), Hex.decode(pKey));
            } catch (Exception e) {
                throw new PKIException("8110", PKIException.SYM_KEY_DES, new PKIException(JIT_PARSE_FAIL, errorDesc(JIT_PARSE_FAIL)));
            }
        } else {
            throw new PKIException("8110", "产生对称密钥操作失败 本操作不支持此种机制类型 " + mType);
        }
    }

    public boolean updateKeyPair(Mechanism mechanism, JKey pubKey, JKey prvKey, int hardPos) throws PKIException {
        String mType = mechanism.getMechanismType();
        if (!mType.equals(Mechanism.RSA) || !mType.equals(Mechanism.SM2)) {
            throw new PKIException("8198195", "替换密钥对失败 本操作不支持此种机制类型 " + mType);
        }
        ByteArrayInputStream in = new ByteArrayInputStream(updateKeyPair(JavaToJNIMechanismType(mType), Parser.softKey2HardCustomKey(pubKey.getKeyType(), pubKey.getKey()), Parser.softKey2HardCustomKey(prvKey.getKeyType(), prvKey.getKey()), hardPos, this.CfgTag.getNoExportRSAPubKeyValue().getBytes(), this.CfgTag.getNoExportRSAPriKeyValue().getBytes(), this.CfgTag.getNoExportRSAKeyValue().getBytes(), this.tag.getBytes()));
        Properties recvData = new Properties();
        try {
            recvData.load(in);
            String error = recvData.getProperty("Error");
            if (error.equals("0")) {
                return true;
            }
            throw new PKIException("8198195", PKIException.UPDATE_KEYPAIR_RES, new PKIException(error, errorDesc(error)));
        } catch (Exception e) {
            throw new PKIException("8198195", PKIException.UPDATE_KEYPAIR_RES, new PKIException(JIT_PARSE_FAIL, errorDesc(JIT_PARSE_FAIL)));
        }
    }

    public JKeyPair generateKeyPair(Mechanism mechanism, int keyLength) throws PKIException {
        byte[] pubKeyValue;
        byte[] priKeyValue;
        JKey publicKey;
        String mType = mechanism.getMechanismType();
        if (mType.equals(Mechanism.RSA) || mType.equals(Mechanism.ECDSA) || mType.equals(Mechanism.SM2) || mType.equals(Mechanism.DSA)) {
            long mechType = JavaToJNIMechanismType(mType);
            boolean isExport = true;
            int keyNum = 0;
            JKey publicKey2 = null;
            JKey privateKey = null;
            byte[] keyData = null;
            if (mechanism.getParam() != null) {
                GenKeyAttribute keyAtt = (GenKeyAttribute) mechanism.getParam();
                isExport = keyAtt.getIsExport();
                keyNum = keyAtt.getKeyNum();
                boolean isNotExportGenKey = keyAtt.getIsNotExportGenKey();
            }
            if (isExport) {
                if (keyLength == 0) {
                    keyLength = 1024;
                }
                keyData = generateKeyPair(mechType, keyLength, this.tag.getBytes());
            } else if (mType.equals(Mechanism.RSA)) {
                keyData = findKeyPair(mechType, Integer.parseInt(this.CfgTag.getNoExportRSAKeyType()), (this.CfgTag.getNoExportRSAPubKeyValue() + String.valueOf(keyNum)).getBytes(), (this.CfgTag.getNoExportRSAPriKeyValue() + String.valueOf(keyNum)).getBytes(), this.tag.getBytes());
            } else if (mType.equals(Mechanism.ECDSA)) {
                keyData = findKeyPair(mechType, Integer.parseInt(this.CfgTag.getNoExportECCKeyType()), (this.CfgTag.getNoExportECDSAPubKeyValue() + String.valueOf(keyNum)).getBytes(), (this.CfgTag.getNoExportECDSAPriKeyValue() + String.valueOf(keyNum)).getBytes(), this.tag.getBytes());
            } else if (mType.equals(Mechanism.SM2)) {
                int isSign = ((GenKeyAttribute) mechanism.getParam()).getIsSign();
                int keyType = Integer.parseInt(this.CfgTag.getNoExportECCKeyType());
                if (isSign == 1) {
                    pubKeyValue = (this.CfgTag.getNoExportECDSAPubKeyValue() + String.valueOf(keyNum)).getBytes();
                    priKeyValue = (this.CfgTag.getNoExportECDSAPriKeyValue() + String.valueOf(keyNum)).getBytes();
                } else {
                    pubKeyValue = (this.CfgTag.getNoExportECDSAEncPubKeyValue() + String.valueOf(keyNum)).getBytes();
                    priKeyValue = (this.CfgTag.getNoExportECDSAEncPriKeyValue() + String.valueOf(keyNum)).getBytes();
                }
                keyData = findKeyPair(mechType, keyType, pubKeyValue, priKeyValue, this.tag.getBytes());
            } else if (mType.equals(Mechanism.DSA)) {
                keyData = findKeyPair(mechType, Integer.parseInt(this.CfgTag.getNoExportDSAKeyType()), (this.CfgTag.getNoExportDSAPubKeyValue() + String.valueOf(keyNum)).getBytes(), (this.CfgTag.getNoExportDSAPriKeyValue() + String.valueOf(keyNum)).getBytes(), this.tag.getBytes());
            }
            if (mType.equals(Mechanism.RSA)) {
                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(keyData);
                Properties recvData = new Properties();
                try {
                    recvData.load(byteArrayInputStream);
                    String error = recvData.getProperty("Error");
                    String pubType = recvData.getProperty("pubType");
                    String pubKeyID = recvData.getProperty("pubKeyID");
                    String pubModulus = recvData.getProperty("Pub_Modulus");
                    String pubExponent = recvData.getProperty("Pub_Exponent");
                    if (!(pubKeyID == null && pubModulus == null && pubExponent == null) && error.equals("0")) {
                        String priType = recvData.getProperty("priType");
                        String priKeyID = recvData.getProperty("priKeyID");
                        String modulus = recvData.getProperty("Modulus");
                        String privateExponent = recvData.getProperty("PrivateExponent");
                        String publicExponent = recvData.getProperty("PublicExponent");
                        String prime1 = recvData.getProperty("Prime1");
                        String prime2 = recvData.getProperty("Prime2");
                        String exponent1 = recvData.getProperty("Exponent1");
                        String exponent2 = recvData.getProperty("Exponent2");
                        String coefficient = recvData.getProperty("Coefficient");
                        if (priKeyID == null && modulus == null && privateExponent == null && publicExponent == null && prime1 == null && prime2 == null && exponent1 == null && exponent2 == null && coefficient == null) {
                            throw new PKIException("8111", PKIException.KEY_PAIR_DES);
                        } else if (mType.equals(Mechanism.RSA)) {
                            if (pubKeyID == null) {
                                new Hex();
                                publicKey2 = new JKey(JNIToJavaKeyType(pubType), Parser.customData2SoftPublicKey(JNIToJavaKeyType(pubType), Hex.decode(pubModulus), Hex.decode(pubExponent)));
                            } else {
                                publicKey2 = new JKey(JNIToJavaKeyType(pubType), Long.parseLong(pubKeyID));
                            }
                            if (priKeyID == null) {
                                new Hex();
                                privateKey = new JKey(JNIToJavaKeyType(priType), Parser.customData2SoftPrivateKey(JNIToJavaKeyType(priType), Hex.decode(modulus), Hex.decode(privateExponent), Hex.decode(publicExponent), Hex.decode(prime1), Hex.decode(prime2), Hex.decode(exponent1), Hex.decode(exponent2), Hex.decode(coefficient)));
                            } else {
                                privateKey = new JKey(JNIToJavaKeyType(priType), Long.parseLong(priKeyID));
                            }
                        }
                    } else {
                        throw new PKIException("8111", PKIException.KEY_PAIR_DES, new PKIException(error, errorDesc(error)));
                    }
                } catch (Exception e) {
                    throw new PKIException("8111", PKIException.KEY_PAIR_DES, new PKIException(JIT_PARSE_FAIL, errorDesc(JIT_PARSE_FAIL)));
                }
            } else if (mType.equals(Mechanism.ECDSA) || mType.equals(Mechanism.SM2)) {
                ByteArrayInputStream byteArrayInputStream2 = new ByteArrayInputStream(keyData);
                Properties recvData2 = new Properties();
                try {
                    recvData2.load(byteArrayInputStream2);
                    String error2 = recvData2.getProperty("Error");
                    String pubType2 = recvData2.getProperty("pubType");
                    String pubKeyID2 = recvData2.getProperty("pubKeyID");
                    String pubEcPoint = recvData2.getProperty("Pub_ecPoint");
                    String pubECParameters = recvData2.getProperty("Pub_ecParma");
                    if (!(pubKeyID2 == null && pubEcPoint == null) && error2.equals("0")) {
                        String priType2 = recvData2.getProperty("priType");
                        String priKeyID2 = recvData2.getProperty("PriKeyID");
                        String modulus2 = recvData2.getProperty("pri_ecValue");
                        if (priKeyID2 == null && modulus2 == null) {
                            throw new PKIException("8111", PKIException.KEY_PAIR_DES);
                        } else if (mType.equals(Mechanism.ECDSA) || mType.equals(Mechanism.SM2)) {
                            if (pubKeyID2 == null) {
                                new Hex();
                                if (mType.equals(Mechanism.ECDSA)) {
                                    publicKey = new JKey(JNIToJavaKeyType(pubType2), Parser.customData2SoftPublicKey(Hex.decode(pubEcPoint), Hex.decode(pubECParameters)));
                                } else {
                                    publicKey = new JKey(JNIToJavaKeyType(pubType2), Parser.customData2SoftPublicKey(Hex.decode(pubEcPoint), null));
                                }
                            } else {
                                publicKey = new JKey(JNIToJavaKeyType(pubType2), Long.parseLong(pubKeyID2));
                            }
                            if (priKeyID2 == null) {
                                new Hex();
                                privateKey = new JKey(JNIToJavaKeyType(priType2), ECDSAParser.customData2SoftECPrivKey(Hex.decode(pubEcPoint), Hex.decode(modulus2)));
                            } else {
                                privateKey = new JKey(JNIToJavaKeyType(priType2), Long.parseLong(priKeyID2));
                            }
                        }
                    } else {
                        throw new PKIException("8111", PKIException.KEY_PAIR_DES, new PKIException(error2, errorDesc(error2)));
                    }
                } catch (Exception e2) {
                    throw new PKIException("8111", PKIException.KEY_PAIR_DES, new PKIException(JIT_PARSE_FAIL, errorDesc(JIT_PARSE_FAIL)));
                }
            } else if (mType.equals(Mechanism.DSA)) {
                ByteArrayInputStream byteArrayInputStream3 = new ByteArrayInputStream(keyData);
                Properties recvData3 = new Properties();
                try {
                    recvData3.load(byteArrayInputStream3);
                    String error3 = recvData3.getProperty("Error");
                    String pubType3 = recvData3.getProperty("pubType");
                    String pubKeyID3 = recvData3.getProperty("pubKeyID");
                    String DSA_P = recvData3.getProperty("DSA_P");
                    String DSA_Q = recvData3.getProperty("DSA_Q");
                    String DSA_G = recvData3.getProperty("DSA_G");
                    String pubEcPoint2 = recvData3.getProperty("Pub_ecPoint");
                    if (!(pubKeyID3 == null && pubEcPoint2 == null) && error3.equals("0")) {
                        String priType3 = recvData3.getProperty("priType");
                        String priKeyID3 = recvData3.getProperty("PriKeyID");
                        String modulus3 = recvData3.getProperty("pri_ecValue");
                        if (priKeyID3 == null && modulus3 == null) {
                            throw new PKIException("8111", PKIException.KEY_PAIR_DES);
                        } else if (mType.equals(Mechanism.DSA)) {
                            if (pubKeyID3 == null) {
                                new Hex();
                                publicKey2 = new JKey(JNIToJavaKeyType(pubType3), Parser.customData2SoftPublicKey(JNIToJavaKeyType(pubType3), Hex.decode(DSA_P), Hex.decode(DSA_Q), Hex.decode(DSA_G), Hex.decode(pubEcPoint2)));
                            }
                            if (priKeyID3 != null) {
                                privateKey = new JKey(JNIToJavaKeyType(priType3), Long.parseLong(priKeyID3));
                            }
                        }
                    } else {
                        throw new PKIException("8111", PKIException.KEY_PAIR_DES, new PKIException(error3, errorDesc(error3)));
                    }
                } catch (Exception e3) {
                    throw new PKIException("8111", PKIException.KEY_PAIR_DES, new PKIException(JIT_PARSE_FAIL, errorDesc(JIT_PARSE_FAIL)));
                }
            }
            return new JKeyPair(publicKey2, privateKey);
        }
        throw new PKIException("8111", "产生非对称密钥对失败 本操作不支持此种机制类型 " + mType);
    }

    public byte[] sign(Mechanism mechanism, JKey prvKey, byte[] sourceData) throws PKIException {
        byte[] signData;
        String mType = mechanism.getMechanismType();
        if (mType.equals("MD5withRSAEncryption") || mType.equals(Mechanism.RSA_PKCS) || mType.equals("SHA1withRSAEncryption") || mType.equals("SHA1withECDSA") || mType.equals("SHA224withECDSA") || mType.equals("SHA256withECDSA") || mType.equals("SHA1withDSA") || mType.equals(Mechanism.SHA224_DSA) || mType.equals(Mechanism.SHA256_DSA) || mType.equals("SM3withSM2Encryption") || mType.equals("SHA224withRSAEncryption") || mType.equals("SHA256withRSAEncryption") || mType.equals("SHA384withRSAEncryption") || mType.equals("SHA512withRSAEncryption")) {
            long mechType = JavaToJNIMechanismType(mType);
            if (prvKey.getKey() != null) {
                signData = sign(mechType, JavaToJNIKeyType(prvKey.getKeyType()), Parser.softKey2HardCustomKey(prvKey.getKeyType(), prvKey.getKey()), sourceData, this.tag.getBytes());
            } else {
                signData = sign(mechType, JavaToJNIKeyType(prvKey.getKeyType()), Long.toString(prvKey.getKeyID()).getBytes(), sourceData, this.tag.getBytes());
            }
            ByteArrayInputStream in = new ByteArrayInputStream(signData);
            Properties recvData = new Properties();
            try {
                recvData.load(in);
                String recvSign = recvData.getProperty("SignData");
                String error = recvData.getProperty("Error");
                if (!error.equals("0") || recvSign == null) {
                    throw new PKIException("8195", PKIException.SIGN_DES, new PKIException(error, errorDesc(error)));
                }
                new Hex();
                return Hex.decode(recvSign);
            } catch (Exception e) {
                throw new PKIException("8195", PKIException.SIGN_DES, new PKIException(JIT_PARSE_FAIL, errorDesc(JIT_PARSE_FAIL)));
            }
        } else {
            throw new PKIException("8195", "签名操作失败 本操作不支持此种机制类型 " + mType);
        }
    }

    public boolean verifySign(Mechanism mechanism, JKey pubKey, byte[] sourceData, byte[] signData) throws PKIException {
        String mType = mechanism.getMechanismType();
        if (mType.equals("MD5withRSAEncryption") || mType.equals("SHA1withRSAEncryption") || mType.equals("SHA1withECDSA") || mType.equals("SHA224withECDSA") || mType.equals("SHA256withECDSA") || mType.equals("SHA1withDSA") || mType.equals(Mechanism.SHA224_DSA) || mType.equals(Mechanism.SHA256_DSA) || mType.equals("SM3withSM2Encryption") || mType.equals("SHA224withRSAEncryption") || mType.equals("SHA256withRSAEncryption") || mType.equals("SHA384withRSAEncryption") || mType.equals("SHA512withRSAEncryption")) {
            ByteArrayInputStream in = new ByteArrayInputStream(verifySign(JavaToJNIMechanismType(mType), JavaToJNIKeyType(pubKey.getKeyType()), Parser.softKey2HardCustomKey(pubKey.getKeyType(), pubKey.getKey()), sourceData, signData, this.tag.getBytes()));
            Properties recvData = new Properties();
            try {
                recvData.load(in);
                String error = recvData.getProperty("Error");
                if (error.equals("0")) {
                    return true;
                }
                throw new PKIException("8196", PKIException.VERIFY_SIGN_DES, new PKIException(error, errorDesc(error)));
            } catch (Exception e) {
                throw new PKIException("8195", PKIException.SIGN_DES, new PKIException(JIT_PARSE_FAIL, errorDesc(JIT_PARSE_FAIL)));
            }
        } else {
            throw new PKIException("8196", "验证签名操作失败 本操作不支持此种机制类型 " + mType);
        }
    }

    public byte[] encrypt(Mechanism mechanism, JKey enKey, byte[] sourceData) throws PKIException {
        byte[] encData;
        String mType = mechanism.getMechanismType();
        if (mType.equals(Mechanism.SF33_ECB) || mType.equals(Mechanism.RSA_PKCS) || mType.equals(Mechanism.DES_CBC) || mType.equals(Mechanism.DES_ECB) || mType.equals(Mechanism.DES3_ECB) || mType.equals(Mechanism.SM2_RAW) || mType.equals(Mechanism.SF33_CBC) || mType.equals(Mechanism.AES_CBC) || mType.equals(Mechanism.AES_ECB) || mType.equals(Mechanism.DES3_CBC) || mType.equals(Mechanism.SCB2_CBC) || mType.equals(Mechanism.SCB2_ECB)) {
            long mechType = JavaToJNIMechanismType(mType);
            CBCParam cbcParam = (CBCParam) mechanism.getParam();
            byte[] cbcIv = null;
            if (cbcParam != null) {
                cbcIv = cbcParam.getIv();
            }
            if (mechanism.isPad()) {
                sourceData = getPaddingData(mechanism, sourceData);
            }
            if (mechType == RSA_PKCS) {
                if (enKey.getKey() == null) {
                    encData = encrypt(mechType, JavaToJNIKeyType(enKey.getKeyType()), Long.toString(enKey.getKeyID()).getBytes(), sourceData, cbcIv, this.tag.getBytes());
                } else {
                    encData = encrypt(mechType, JavaToJNIKeyType(enKey.getKeyType()), Parser.softKey2HardCustomKey(enKey.getKeyType(), enKey.getKey()), sourceData, cbcIv, this.tag.getBytes());
                }
            } else if (SW_SM2_RAW == mechType) {
                if (enKey.getKey() == null) {
                    encData = encrypt(mechType, JavaToJNIKeyType(enKey.getKeyType()), Long.toString(enKey.getKeyID()).getBytes(), sourceData, cbcIv, this.tag.getBytes());
                } else {
                    encData = encrypt(mechType, JavaToJNIKeyType(enKey.getKeyType()), Parser.softKey2HardCustomKey(enKey.getKeyType(), enKey.getKey()), sourceData, cbcIv, this.tag.getBytes());
                }
            } else if (enKey.getKeyType().equalsIgnoreCase("MASTERKEY")) {
                long parseLong = Long.parseLong(this.CfgTag.getMasterKeyEnc());
                encData = masterEncrypt(Long.parseLong(this.CfgTag.getMasterKeyEnc()), Integer.parseInt(this.CfgTag.getMasterKeyType()), this.CfgTag.getMasterKeyValue().getBytes(), sourceData, cbcIv, this.tag.getBytes());
            } else {
                encData = encrypt(mechType, JavaToJNIKeyType(enKey.getKeyType()), enKey.getKey(), sourceData, cbcIv, this.tag.getBytes());
            }
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(encData);
            Properties recvData = new Properties();
            try {
                recvData.load(byteArrayInputStream);
                String recvEnc = recvData.getProperty("EncData");
                String error = recvData.getProperty("Error");
                if (!error.equals("0") || recvEnc == null) {
                    throw new PKIException(PKIException.DATA_LOAD_FAIL, PKIException.ENCRYPT_DES, new PKIException(error, errorDesc(error)));
                }
                new Hex();
                return Hex.decode(recvEnc);
            } catch (Exception e) {
                throw new PKIException(PKIException.DATA_LOAD_FAIL, PKIException.ENCRYPT_DES, new PKIException(JIT_PARSE_FAIL, errorDesc(JIT_PARSE_FAIL)));
            }
        } else {
            throw new PKIException(PKIException.DATA_LOAD_FAIL, "加密操作失败 本操作不支持此种机制类型 " + mType);
        }
    }

    public byte[] decrypt(Mechanism mechanism, JKey deKey, byte[] encryptedData) throws PKIException {
        byte[] decData;
        String mType = mechanism.getMechanismType();
        if (mType.equals(Mechanism.SF33_ECB) || mType.equals(Mechanism.RSA_PKCS) || mType.equals(Mechanism.DES_ECB) || mType.equals(Mechanism.DES_CBC) || mType.equals(Mechanism.DES3_ECB) || mType.equals(Mechanism.DES3_CBC) || mType.equals(Mechanism.SCB2_CBC) || mType.equals(Mechanism.SCB2_ECB) || mType.equals(Mechanism.SM2_RAW) || mType.equals(Mechanism.SF33_CBC) || mType.equals(Mechanism.AES_CBC) || mType.equals(Mechanism.AES_ECB) || mType.equals(Mechanism.RAW)) {
            long mechType = JavaToJNIMechanismType(mType);
            CBCParam cbcParam = (CBCParam) mechanism.getParam();
            byte[] cbcIv = null;
            if (cbcParam != null) {
                cbcIv = cbcParam.getIv();
            }
            if (mechType == RSA_PKCS && deKey.getKey() == null) {
                decData = decrypt(mechType, JavaToJNIKeyType(deKey.getKeyType()), Long.toString(deKey.getKeyID()).getBytes(), encryptedData, cbcIv, this.tag.getBytes());
            } else if (mechType == RSA_PKCS && deKey.getKey() != null) {
                decData = decrypt(mechType, JavaToJNIKeyType(deKey.getKeyType()), Parser.softKey2HardCustomKey(deKey.getKeyType(), deKey.getKey()), encryptedData, cbcIv, this.tag.getBytes());
            } else if (deKey.getKeyType().equalsIgnoreCase("MASTERKEY")) {
                long parseLong = Long.parseLong(this.CfgTag.getMasterKeyEnc());
                decData = masterDecrypt(Long.parseLong(this.CfgTag.getMasterKeyEnc()), Integer.parseInt(this.CfgTag.getMasterKeyType()), this.CfgTag.getMasterKeyValue().getBytes(), encryptedData, cbcIv, this.tag.getBytes());
            } else if (mechType == CKM_RSA_RAW && deKey.getKey() != null) {
                decData = decrypt(mechType, JavaToJNIKeyType(deKey.getKeyType()), Parser.softKey2HardCustomKey(deKey.getKeyType(), deKey.getKey()), encryptedData, cbcIv, this.tag.getBytes());
            } else if (mechType == SW_SM2_RAW && deKey.getKey() == null) {
                decData = decrypt(mechType, JavaToJNIKeyType(deKey.getKeyType()), Long.toString(deKey.getKeyID()).getBytes(), encryptedData, cbcIv, this.tag.getBytes());
            } else if (mechType != SW_SM2_RAW || deKey.getKey() == null) {
                decData = decrypt(mechType, JavaToJNIKeyType(deKey.getKeyType()), deKey.getKey(), encryptedData, cbcIv, this.tag.getBytes());
            } else {
                decData = decrypt(mechType, JavaToJNIKeyType(deKey.getKeyType()), Parser.softKey2HardCustomKey(deKey.getKeyType(), deKey.getKey()), encryptedData, cbcIv, this.tag.getBytes());
            }
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(decData);
            Properties recvData = new Properties();
            try {
                recvData.load(byteArrayInputStream);
                String recvDec = recvData.getProperty("DecData");
                String error = recvData.getProperty("Error");
                if (!error.equals("0") || recvDec == null) {
                    throw new PKIException(PKIException.CREAT_CERT_OBJECT, PKIException.DECRYPT_DES, new PKIException(error, errorDesc(error)));
                }
                new Hex();
                byte[] dec = Hex.decode(recvDec);
                if (mechanism.isPad()) {
                    return paddingDrop(dec);
                }
                return dec;
            } catch (Exception e) {
                throw new PKIException(PKIException.CREAT_CERT_OBJECT, PKIException.DECRYPT_DES, new PKIException(JIT_PARSE_FAIL, errorDesc(JIT_PARSE_FAIL)));
            }
        } else {
            throw new PKIException(PKIException.CREAT_CERT_OBJECT, "解密操作失败 本操作不支持此种机制类型 " + mType);
        }
    }

    public boolean createCertObject(byte[] dn, byte[] certData, byte[] keyId) throws PKIException {
        ByteArrayInputStream in = new ByteArrayInputStream(importCert(dn, certData, keyId, this.tag.getBytes()));
        Properties recvData = new Properties();
        try {
            recvData.load(in);
            String error = recvData.getProperty("Error");
            if (error.equals("0")) {
                return true;
            }
            throw new PKIException("8198191", PKIException.CREAT_CERT_OBJECT_RES, new PKIException(error, errorDesc(error)));
        } catch (Exception e) {
            throw new PKIException("8198190", PKIException.DATA_LOAD_FAIL_RES, new PKIException(JIT_PARSE_FAIL, errorDesc(JIT_PARSE_FAIL)));
        }
    }

    public boolean destroyCertObject(byte[] dn, byte[] keyId) throws PKIException {
        byte[] keyData;
        if (dn == null) {
            keyData = destroyCerts(keyId, this.tag.getBytes());
        } else {
            keyData = destroyCert(dn, keyId, this.tag.getBytes());
        }
        ByteArrayInputStream in = new ByteArrayInputStream(keyData);
        Properties recvData = new Properties();
        try {
            recvData.load(in);
            String error = recvData.getProperty("Error");
            if (error.equals("0")) {
                return true;
            }
            if (error.equals(JIT_GETCERT_FAIL)) {
                return false;
            }
            throw new PKIException("8198193", PKIException.DESTROY_CERT_OBJECT_RES, new PKIException(error, errorDesc(error)));
        } catch (Exception e) {
            throw new PKIException("8198192", PKIException.CERT_DATA_FAIL_RES, new PKIException(JIT_PARSE_FAIL, errorDesc(JIT_PARSE_FAIL)));
        }
    }

    public byte[] getMrtdVision(byte[] fileName) throws PKIException {
        ByteArrayInputStream in = new ByteArrayInputStream(getMRTDVision(fileName, this.tag.getBytes()));
        Properties recvData = new Properties();
        try {
            recvData.load(in);
        } catch (Exception e) {
            new PKIException(JIT_PARSE_FAIL, errorDesc(JIT_PARSE_FAIL));
        }
        String error = recvData.getProperty("Error");
        String recvVersion = recvData.getProperty("Mrtd_version");
        if (error.equals("0")) {
            new Hex();
            return Hex.decode(recvVersion);
        }
        throw new PKIException(PKIException.JHARDLIB, new PKIException(error, errorDesc(error)));
    }

    public byte[] getCertObject(byte[] keyId) throws PKIException {
        ByteArrayInputStream in = new ByteArrayInputStream(getCert(keyId, this.tag.getBytes()));
        Properties recvData = new Properties();
        try {
            recvData.load(in);
            String error = recvData.getProperty("Error");
            String certObject = recvData.getProperty("certObject");
            if (!error.equals("0") || certObject == null) {
                throw new PKIException("8198194", PKIException.GET_CERT_OBJECT_RES, new PKIException(error, errorDesc(error)));
            }
            new Hex();
            return Hex.decode(certObject);
        } catch (Exception e) {
            throw new PKIException("8198194", PKIException.GET_CERT_OBJECT_RES, new PKIException(JIT_PARSE_FAIL, errorDesc(JIT_PARSE_FAIL)));
        }
    }

    public byte[] digest(Mechanism mechanism, byte[] sourceData) throws PKIException {
        String mType = mechanism.getMechanismType();
        if (mType.equals(Mechanism.MD2) || mType.equals(Mechanism.MD5) || mType.equals(Mechanism.SM3) || mType.equals(Mechanism.SHA1) || mType.equals(Mechanism.SHA224) || mType.equals(Mechanism.SHA256) || mType.equals(Mechanism.SHA384) || mType.equals(Mechanism.SHA512)) {
            ByteArrayInputStream in = new ByteArrayInputStream(digest(JavaToJNIMechanismType(mType), sourceData, this.tag.getBytes()));
            Properties recvData = new Properties();
            try {
                recvData.load(in);
                String recvEnc = recvData.getProperty("DigestData");
                if (!recvData.getProperty("Error").equals("0") || recvEnc == null) {
                    throw new PKIException(PKIException.CERT_DATA_FAIL, PKIException.DIGEST_DES);
                }
                new Hex();
                return Hex.decode(recvEnc);
            } catch (Exception ex) {
                throw new PKIException(PKIException.CERT_DATA_FAIL, PKIException.DIGEST_DES, ex);
            }
        } else {
            throw new PKIException(PKIException.CERT_DATA_FAIL, "文摘操作失败 本操作不支持此种机制类型 " + mType);
        }
    }

    public byte[] mac(Mechanism mechanism, JKey key, byte[] sourceData) throws PKIException {
        throw new UnsupportedOperationException("Method mac() not yet implemented.");
    }

    public boolean verifyMac(Mechanism mechanism, JKey key, byte[] sourceData, byte[] macData) throws PKIException {
        throw new UnsupportedOperationException("Method verifyMac() not yet implemented.");
    }

    public JKey generatePBEKey(Mechanism mechanism, char[] password) throws PKIException {
        throw new UnsupportedOperationException("Method generatePBEKey() not yet implemented.");
    }

    public byte[] generateRandom(Mechanism mechanism, int length) throws PKIException {
        if (!mechanism.getMechanismType().equals(Mechanism.RANDOM)) {
            throw new PKIException("8113", "产生随机数失败 本操作不支持此种机制类型 Random");
        }
        ByteArrayInputStream in = new ByteArrayInputStream(generateRandom(length, this.tag.getBytes()));
        Properties recvData = new Properties();
        try {
            recvData.load(in);
            String recvEnc = recvData.getProperty("RandomData");
            if (!recvData.getProperty("Error").equals("0") || recvEnc == null) {
                throw new PKIException(PKIException.DESTROY_CERT_OBJECT, PKIException.RANDOM_DES);
            }
            new Hex();
            return Hex.decode(recvEnc);
        } catch (Exception ex) {
            throw new PKIException(PKIException.DESTROY_CERT_OBJECT, PKIException.RANDOM_DES, ex);
        }
    }

    private static String errorDesc(String errCode) {
        String code = new String();
        int i = 0;
        while (true) {
            if (i >= hardException.length) {
                break;
            } else if (hardException[i][0].equals(errCode)) {
                code = hardException[i][1];
                break;
            } else {
                i++;
            }
        }
        if (code == null) {
            return "系统内部错误!!!";
        }
        return code;
    }

    private boolean isEqualArray(byte[] a, byte[] b) {
        if (a.length != b.length) {
            return false;
        }
        for (int i = 0; i < a.length; i++) {
            if (a[i] != b[i]) {
                return false;
            }
        }
        return true;
    }

    private String JNIToJavaKeyType(String keyType) throws PKIException {
        if (keyType.equalsIgnoreCase("145")) {
            return new String("SYMMETRY");
        }
        if (keyType.equalsIgnoreCase("1")) {
            return new String(JKey.RSA_PUB_KEY);
        }
        if (keyType.equalsIgnoreCase("2")) {
            return new String(JKey.RSA_PRV_KEY);
        }
        if (keyType.equalsIgnoreCase("16")) {
            return new String(JKey.RSA_PUB_KEY_ID);
        }
        if (keyType.equalsIgnoreCase("32")) {
            return new String(JKey.RSA_PRV_KEY_ID);
        }
        if (keyType.equalsIgnoreCase(CKC_KEYTYPE_MASTERKEYID)) {
            return new String("MASTERKEY");
        }
        if (keyType.equalsIgnoreCase("256")) {
            return new String(JKey.ECDSA_PUB_KEY);
        }
        if (keyType.equalsIgnoreCase("258")) {
            return new String(JKey.ECDSA_PUB_KEY_ID);
        }
        if (keyType.equalsIgnoreCase("257")) {
            return new String(JKey.ECDSA_PRV_KEY);
        }
        if (keyType.equalsIgnoreCase("259")) {
            return new String(JKey.ECDSA_PRV_KEY_ID);
        }
        if (keyType.equalsIgnoreCase("4")) {
            return new String(JKey.DSA_PUB_KEY);
        }
        if (keyType.equalsIgnoreCase("64")) {
            return new String(JKey.DSA_PUB_KEY_ID);
        }
        if (keyType.equalsIgnoreCase("8")) {
            return new String(JKey.DSA_PRV_KEY);
        }
        if (keyType.equalsIgnoreCase(CKC_KEYTYPE_DSA_PRIVATEKEYID)) {
            return new String(JKey.DSA_PRV_KEY_ID);
        }
        if (keyType.equalsIgnoreCase(CKC_KEYTYPE_SM2_PUBLICKEY)) {
            return new String(JKey.SM2_PUB_KEY);
        }
        if (keyType.equalsIgnoreCase(CKC_KEYTYPE_SM2_PUBLICKEYID)) {
            return new String(JKey.SM2_PUB_KEY_ID);
        }
        if (keyType.equalsIgnoreCase(CKC_KEYTYPE_SM2_PRIVATEKEY)) {
            return new String(JKey.SM2_PRV_KEY);
        }
        if (keyType.equalsIgnoreCase(CKC_KEYTYPE_SM2_PRIVATEKEYID)) {
            return new String(JKey.SM2_PRV_KEY_ID);
        }
        throw new PKIException(PKIException.DATA_LOAD_FAIL, PKIException.NOT_SUP_DES);
    }

    private int JavaToJNIKeyType(String keyType) throws PKIException {
        if (keyType.equalsIgnoreCase("SYMMETRY")) {
            return Integer.parseInt("145");
        }
        if (keyType.equalsIgnoreCase("DES")) {
            return Integer.parseInt("145");
        }
        if (keyType.equalsIgnoreCase("DESede")) {
            return Integer.parseInt("145");
        }
        if (keyType.equalsIgnoreCase("SCB2")) {
            return Integer.parseInt("145");
        }
        if (keyType.equalsIgnoreCase("SF33")) {
            return Integer.parseInt("145");
        }
        if (keyType.equalsIgnoreCase(JKey.RSA_PUB_KEY)) {
            return Integer.parseInt("1");
        }
        if (keyType.equalsIgnoreCase(JKey.RSA_PRV_KEY)) {
            return Integer.parseInt("2");
        }
        if (keyType.equalsIgnoreCase(JKey.RSA_PUB_KEY_ID)) {
            return Integer.parseInt("16");
        }
        if (keyType.equalsIgnoreCase(JKey.RSA_PRV_KEY_ID)) {
            return Integer.parseInt("32");
        }
        if (keyType.equalsIgnoreCase("MASTERKEY")) {
            return Integer.parseInt(CKC_KEYTYPE_MASTERKEYID);
        }
        if (keyType.equalsIgnoreCase(JKey.ECDSA_PRV_KEY)) {
            return Integer.parseInt("257");
        }
        if (keyType.equalsIgnoreCase(JKey.ECDSA_PRV_KEY_ID)) {
            return Integer.parseInt("259");
        }
        if (keyType.equalsIgnoreCase(JKey.ECDSA_PUB_KEY)) {
            return Integer.parseInt("256");
        }
        if (keyType.equalsIgnoreCase(JKey.ECDSA_PUB_KEY_ID)) {
            return Integer.parseInt("258");
        }
        if (keyType.equalsIgnoreCase(JKey.DSA_PRV_KEY)) {
            return Integer.parseInt("8");
        }
        if (keyType.equalsIgnoreCase(JKey.DSA_PRV_KEY_ID)) {
            return Integer.parseInt(CKC_KEYTYPE_DSA_PRIVATEKEYID);
        }
        if (keyType.equalsIgnoreCase(JKey.DSA_PUB_KEY)) {
            return Integer.parseInt("4");
        }
        if (keyType.equalsIgnoreCase(JKey.DSA_PUB_KEY_ID)) {
            return Integer.parseInt("64");
        }
        if (keyType.equalsIgnoreCase(JKey.SM2_PRV_KEY)) {
            return Integer.parseInt(CKC_KEYTYPE_SM2_PRIVATEKEY);
        }
        if (keyType.equalsIgnoreCase(JKey.SM2_PRV_KEY_ID)) {
            return Integer.parseInt(CKC_KEYTYPE_SM2_PRIVATEKEYID);
        }
        if (keyType.equalsIgnoreCase(JKey.SM2_PUB_KEY)) {
            return Integer.parseInt(CKC_KEYTYPE_SM2_PUBLICKEY);
        }
        if (keyType.equalsIgnoreCase(JKey.SM2_PUB_KEY_ID)) {
            return Integer.parseInt(CKC_KEYTYPE_SM2_PUBLICKEYID);
        }
        throw new PKIException(PKIException.DATA_LOAD_FAIL, PKIException.NOT_SUP_DES);
    }

    private long JavaToJNIMechanismType(String mechanismType) throws PKIException {
        if (mechanismType.equals(Mechanism.RSA)) {
            return RSA;
        }
        if (mechanismType.equals(Mechanism.RSA_PKCS)) {
            return RSA_PKCS;
        }
        if (mechanismType.equals(Mechanism.ECDSA)) {
            return ECDSA;
        }
        if (mechanismType.equals("SHA1withECDSA")) {
            return ECDSA_SHA1;
        }
        if (mechanismType.equalsIgnoreCase(Mechanism.DSA)) {
            return DSA;
        }
        if (mechanismType.equals(Mechanism.MD2)) {
            return MD2;
        }
        if (mechanismType.equals(Mechanism.MD5)) {
            return MD5;
        }
        if (mechanismType.equals("MD5withRSAEncryption")) {
            return MD5_RSA_PKCS;
        }
        if (mechanismType.equals(Mechanism.SHA1)) {
            return SHA1;
        }
        if (mechanismType.equals("SHA1withRSAEncryption")) {
            return SHA1_RSA_PKCS;
        }
        if (mechanismType.equals("DES")) {
            return DES_KEY;
        }
        if (mechanismType.equals(Mechanism.DES_ECB)) {
            return DES_ECB;
        }
        if (mechanismType.equals(Mechanism.DES_CBC)) {
            return DES_CBC;
        }
        if (mechanismType.equals("DESede")) {
            return DES3_KEY;
        }
        if (mechanismType.equals(Mechanism.DES3_ECB)) {
            return DES3_ECB;
        }
        if (mechanismType.equals(Mechanism.DES3_CBC)) {
            return DES3_CBC;
        }
        if (mechanismType.equals("RC2")) {
            return RC2_KEY;
        }
        if (mechanismType.equals(Mechanism.RC2_ECB)) {
            return RC2_ECB;
        }
        if (mechanismType.equals(Mechanism.RC2_CBC)) {
            return RC2_CBC;
        }
        if (mechanismType.equals("RC4")) {
            return RC4_KEY;
        }
        if (mechanismType.equals("RC4")) {
            return RC4;
        }
        if (mechanismType.equals("SF33")) {
            return SF33_KEY_GEN;
        }
        if (mechanismType.equals(Mechanism.SF33_ECB)) {
            return CKM_SF33_ECB;
        }
        if (mechanismType.equals("SCB2")) {
            return SCB2_KEY_GEN;
        }
        if (mechanismType.equals(Mechanism.SCB2_CBC)) {
            return SCB2_CBC;
        }
        if (mechanismType.equals(Mechanism.SCB2_ECB)) {
            return SCB2_ECB;
        }
        if (mechanismType.equals(Mechanism.RAW)) {
            return CKM_RSA_RAW;
        }
        if (mechanismType.equals(Mechanism.SHA224)) {
            return SHA224;
        }
        if (mechanismType.equals(Mechanism.SHA256)) {
            return SHA256;
        }
        if (mechanismType.equals(Mechanism.SHA384)) {
            return SHA384;
        }
        if (mechanismType.equals(Mechanism.SHA512)) {
            return SHA512;
        }
        if (mechanismType.equals("SHA224withRSAEncryption")) {
            return SHA224_RSA_PKCS;
        }
        if (mechanismType.equals("SHA256withRSAEncryption")) {
            return SHA256_RSA_PKCS;
        }
        if (mechanismType.equals("SHA384withRSAEncryption")) {
            return SHA384_RSA_PKCS;
        }
        if (mechanismType.equals("SHA512withRSAEncryption")) {
            return SHA512_RSA_PKCS;
        }
        if (mechanismType.equals("SHA1withDSA")) {
            return DSA_SHA1;
        }
        if (mechanismType.equals(Mechanism.SHA224_DSA)) {
            return DSA_SHA224;
        }
        if (mechanismType.equals(Mechanism.SHA256_DSA)) {
            return DSA_SHA256;
        }
        if (mechanismType.equals("SHA224withECDSA")) {
            return ECDSA_SHA224;
        }
        if (mechanismType.equals("SHA256withECDSA")) {
            return ECDSA_SHA256;
        }
        if (mechanismType.equals(Mechanism.SM2)) {
            return SW_SM2;
        }
        if (mechanismType.equals(Mechanism.SM2_RAW)) {
            return SW_SM2_RAW;
        }
        if (mechanismType.equals("SM3withSM2Encryption")) {
            return SW_SM3_SM2;
        }
        if (mechanismType.equals(Mechanism.SM3)) {
            return SW_SM3;
        }
        if (mechanismType.equals("AES")) {
            return AES_KEY_GEN;
        }
        if (mechanismType.equals(Mechanism.SF33_CBC)) {
            return CKM_SF33_CBC;
        }
        if (mechanismType.equals(Mechanism.AES_ECB)) {
            return CKM_AES_ECB;
        }
        if (mechanismType.equals(Mechanism.AES_CBC)) {
            return CKM_AES_CBC;
        }
        throw new PKIException(PKIException.DATA_LOAD_FAIL, PKIException.NOT_SUP_DES);
    }

    public boolean DestroyKeyPair(Mechanism mechanism) throws PKIException {
        String mType = mechanism.getMechanismType();
        if (!mType.equals(Mechanism.RSA)) {
            throw new PKIException("8111", "产生非对称密钥对失败 本操作不支持此种机制类型 " + mType);
        }
        long mechType = JavaToJNIMechanismType(mType);
        int keyNum = 0;
        if (mechanism.getParam() != null) {
            keyNum = ((GenKeyAttribute) mechanism.getParam()).getKeyNum();
        }
        return DestroyKeyPair(mechType, Integer.parseInt(this.CfgTag.getNoExportRSAKeyType()), new StringBuilder().append(this.CfgTag.getNoExportRSAPubKeyValue()).append(String.valueOf(keyNum)).toString().getBytes(), new StringBuilder().append(this.CfgTag.getNoExportRSAPriKeyValue()).append(String.valueOf(keyNum)).toString().getBytes(), this.tag.getBytes()) == 0;
    }

    public String Version() throws PKIException {
        ByteArrayInputStream in = new ByteArrayInputStream(getVersion());
        Properties recvData = new Properties();
        try {
            recvData.load(in);
            return recvData.getProperty("Version");
        } catch (Exception ex) {
            throw new PKIException(PKIException.DESTROY_CERT_OBJECT, PKIException.RANDOM_DES, ex);
        }
    }

    public static void main(String[] args) {
    }

    public byte[] digest(Mechanism mechanism, InputStream sourceData) throws PKIException {
        throw new PKIException(PKIException.CERT_DATA_FAIL, "文摘操作失败 digest() JHardLib didn't support Stream-Operation yet. ");
    }

    public byte[] mac(Mechanism mechanism, JKey key, InputStream sourceData) throws PKIException {
        throw new PKIException(PKIException.DESTROY_CERT_OBJECT, "MAC操作失败 mac() JHardLib didn't support Stream-Operation yet. ");
    }

    public boolean verifyMac(Mechanism mechanism, JKey key, InputStream sourceData, byte[] macData) throws PKIException {
        throw new PKIException(PKIException.GET_CERT_OBJECT, "验证MAC操作失败 verifyMac() JHardLib didn't support Stream-Operation yet. ");
    }

    public byte[] sign(Mechanism mechanism, JKey prvKey, InputStream sourceData) throws PKIException {
        throw new PKIException("8195", "签名操作失败 sign() JHardLib didn't support Stream-Operation yet. ");
    }

    public boolean verifySign(Mechanism mechanism, JKey pubKey, InputStream sourceData, byte[] signData) throws PKIException {
        throw new PKIException("8196", "验证签名操作失败 verifySign() JHardLib didn't support Stream-Operation yet. ");
    }

    public byte[] encrypt(Mechanism mechanism, JKey enKey, InputStream sourceData) throws PKIException {
        throw new PKIException(PKIException.DATA_LOAD_FAIL, "加密操作失败 encrypt() JHardLib didn't support Stream-Operation yet. ");
    }

    public byte[] decrypt(Mechanism mechanism, JKey deKey, InputStream encryptedData) throws PKIException {
        throw new PKIException(PKIException.CREAT_CERT_OBJECT, "解密操作失败 decrypt() JHardLib didn't support Stream-Operation yet. ");
    }

    public int encrypt(Mechanism mechanism, JKey enKey, InputStream sourceData, OutputStream out) throws PKIException {
        throw new PKIException(PKIException.DATA_LOAD_FAIL, "加密操作失败 encrypt() JHardLib didn't support Stream-Operation yet. ");
    }

    public int decrypt(Mechanism mechanism, JKey deKey, InputStream encryptedData, OutputStream out) throws PKIException {
        throw new PKIException(PKIException.CREAT_CERT_OBJECT, "解密操作失败 decrypt() JHardLib didn't support Stream-Operation yet. ");
    }

    public byte[] decryptFinal(JHandle handle, Mechanism mechanism, byte[] sourceData) throws PKIException {
        byte[] decData;
        String mType = mechanism.getMechanismType();
        if (mType.equals(Mechanism.SF33_ECB) || mType.equals(Mechanism.RSA_PKCS) || mType.equals(Mechanism.DES_ECB) || mType.equals(Mechanism.DES_CBC) || mType.equals(Mechanism.DES3_ECB) || mType.equals(Mechanism.DES3_CBC) || mType.equals(Mechanism.SM2_RAW) || mType.equals(Mechanism.SF33_CBC) || mType.equals(Mechanism.AES_CBC) || mType.equals(Mechanism.AES_ECB) || mType.equals(Mechanism.RAW)) {
            long mechType = JavaToJNIMechanismType(mType);
            if (mechType == RSA_PKCS) {
                decData = decryptFinal(handle.getHardLibHandle(), mechType, sourceData, this.tag.getBytes());
            } else if (mechType == RSA_PKCS) {
                decData = decryptFinal(handle.getHardLibHandle(), mechType, sourceData, this.tag.getBytes());
            } else if (mechType == CKM_RSA_RAW) {
                decData = decryptFinal(handle.getHardLibHandle(), mechType, sourceData, this.tag.getBytes());
            } else if (mechType == SW_SM2_RAW) {
                decData = decryptFinal(handle.getHardLibHandle(), mechType, sourceData, this.tag.getBytes());
            } else {
                decData = decryptFinal(handle.getHardLibHandle(), mechType, sourceData, this.tag.getBytes());
            }
            ByteArrayInputStream in = new ByteArrayInputStream(decData);
            Properties recvData = new Properties();
            try {
                recvData.load(in);
                String recvDec = recvData.getProperty("DecData");
                String error = recvData.getProperty("Error");
                if (!error.equals("0") || recvDec == null) {
                    throw new PKIException(PKIException.CREAT_CERT_OBJECT, PKIException.DECRYPT_DES, new PKIException(error, errorDesc(error)));
                }
                new Hex();
                byte[] dec = Hex.decode(recvDec);
                if (mechanism.isPad()) {
                    return paddingDrop(dec);
                }
                return dec;
            } catch (Exception e) {
                throw new PKIException(PKIException.CREAT_CERT_OBJECT, PKIException.DECRYPT_DES, new PKIException(JIT_PARSE_FAIL, errorDesc(JIT_PARSE_FAIL)));
            }
        } else {
            throw new PKIException(PKIException.CREAT_CERT_OBJECT, "解密操作失败 本操作不支持此种机制类型 " + mType);
        }
    }

    public JHandle decryptInit(Mechanism mechanism, JKey deKey) throws PKIException {
        byte[] decInit;
        String mType = mechanism.getMechanismType();
        if (mType.equals(Mechanism.SF33_ECB) || mType.equals(Mechanism.RSA_PKCS) || mType.equals(Mechanism.DES_ECB) || mType.equals(Mechanism.DES_CBC) || mType.equals(Mechanism.DES3_ECB) || mType.equals(Mechanism.DES3_CBC) || mType.equals(Mechanism.SM2_RAW) || mType.equals(Mechanism.SF33_CBC) || mType.equals(Mechanism.AES_CBC) || mType.equals(Mechanism.AES_ECB) || mType.equals(Mechanism.RAW)) {
            long mechType = JavaToJNIMechanismType(mType);
            CBCParam cbcParam = (CBCParam) mechanism.getParam();
            byte[] cbcIv = null;
            if (cbcParam != null) {
                cbcIv = cbcParam.getIv();
            }
            if (mechType == RSA_PKCS && deKey.getKey() == null) {
                decInit = decryptInit(mechType, JavaToJNIKeyType(deKey.getKeyType()), Long.toString(deKey.getKeyID()).getBytes(), cbcIv, this.tag.getBytes());
            } else if (mechType == RSA_PKCS && deKey.getKey() != null) {
                decInit = decryptInit(mechType, JavaToJNIKeyType(deKey.getKeyType()), Parser.softKey2HardCustomKey(deKey.getKeyType(), deKey.getKey()), cbcIv, this.tag.getBytes());
            } else if (mechType == CKM_RSA_RAW && deKey.getKey() != null) {
                decInit = decryptInit(mechType, JavaToJNIKeyType(deKey.getKeyType()), Parser.softKey2HardCustomKey(deKey.getKeyType(), deKey.getKey()), cbcIv, this.tag.getBytes());
            } else if (mechType != SW_SM2_RAW || deKey.getKey() == null) {
                decInit = decryptInit(mechType, JavaToJNIKeyType(deKey.getKeyType()), deKey.getKey(), cbcIv, this.tag.getBytes());
            } else {
                decInit = decryptInit(mechType, JavaToJNIKeyType(deKey.getKeyType()), Parser.softKey2HardCustomKey(deKey.getKeyType(), deKey.getKey()), cbcIv, this.tag.getBytes());
            }
            ByteArrayInputStream in = new ByteArrayInputStream(decInit);
            Properties recvData = new Properties();
            try {
                recvData.load(in);
                String recvDecInit = recvData.getProperty("session");
                String error = recvData.getProperty("Error");
                if (error.equals("0") && recvDecInit != null) {
                    return new JHandle(Long.parseLong(recvDecInit), null);
                }
                throw new PKIException(PKIException.CREAT_CERT_OBJECT, PKIException.DECRYPT_DES, new PKIException(error, errorDesc(error)));
            } catch (Exception e) {
                throw new PKIException(PKIException.CREAT_CERT_OBJECT, PKIException.DECRYPT_DES, new PKIException(JIT_PARSE_FAIL, errorDesc(JIT_PARSE_FAIL)));
            }
        } else {
            throw new PKIException(PKIException.CREAT_CERT_OBJECT, "解密操作失败 本操作不支持此种机制类型 " + mType);
        }
    }

    public byte[] decryptUpdate(JHandle handle, Mechanism mechanism, byte[] sourceData) throws PKIException {
        byte[] decData;
        String mType = mechanism.getMechanismType();
        if (mType.equals(Mechanism.SF33_ECB) || mType.equals(Mechanism.RSA_PKCS) || mType.equals(Mechanism.DES_ECB) || mType.equals(Mechanism.DES_CBC) || mType.equals(Mechanism.DES3_ECB) || mType.equals(Mechanism.DES3_CBC) || mType.equals(Mechanism.SM2_RAW) || mType.equals(Mechanism.SF33_CBC) || mType.equals(Mechanism.AES_CBC) || mType.equals(Mechanism.AES_ECB) || mType.equals(Mechanism.RAW)) {
            long mechType = JavaToJNIMechanismType(mType);
            if (mechType == RSA_PKCS) {
                decData = decryptUpdate(handle.getHardLibHandle(), mechType, sourceData, this.tag.getBytes());
            } else if (mechType == RSA_PKCS) {
                decData = decryptUpdate(handle.getHardLibHandle(), mechType, sourceData, this.tag.getBytes());
            } else if (mechType == CKM_RSA_RAW) {
                decData = decryptUpdate(handle.getHardLibHandle(), mechType, sourceData, this.tag.getBytes());
            } else if (mechType == SW_SM2_RAW) {
                decData = decryptUpdate(handle.getHardLibHandle(), mechType, sourceData, this.tag.getBytes());
            } else {
                decData = decryptUpdate(handle.getHardLibHandle(), mechType, sourceData, this.tag.getBytes());
            }
            ByteArrayInputStream in = new ByteArrayInputStream(decData);
            Properties recvData = new Properties();
            try {
                recvData.load(in);
                String recvDec = recvData.getProperty("DecData");
                String error = recvData.getProperty("Error");
                if (!error.equals("0") || recvDec == null) {
                    throw new PKIException(PKIException.CREAT_CERT_OBJECT, PKIException.DECRYPT_DES, new PKIException(error, errorDesc(error)));
                }
                new Hex();
                return Hex.decode(recvDec);
            } catch (Exception e) {
                throw new PKIException(PKIException.CREAT_CERT_OBJECT, PKIException.DECRYPT_DES, new PKIException(JIT_PARSE_FAIL, errorDesc(JIT_PARSE_FAIL)));
            }
        } else {
            throw new PKIException(PKIException.CREAT_CERT_OBJECT, "解密操作失败 本操作不支持此种机制类型 " + mType);
        }
    }

    public byte[] encryptFinal(JHandle handle, Mechanism mechanism, byte[] sourceData) throws PKIException {
        byte[] encData;
        String mType = mechanism.getMechanismType();
        if (mType.equals(Mechanism.SF33_ECB) || mType.equals(Mechanism.RSA_PKCS) || mType.equals(Mechanism.DES_CBC) || mType.equals(Mechanism.DES_ECB) || mType.equals(Mechanism.DES3_ECB) || mType.equals(Mechanism.SM2_RAW) || mType.equals(Mechanism.SF33_CBC) || mType.equals(Mechanism.AES_CBC) || mType.equals(Mechanism.AES_ECB) || mType.equals(Mechanism.DES3_CBC)) {
            long mechType = JavaToJNIMechanismType(mType);
            if (mechanism.isPad()) {
                sourceData = getPaddingData(mechanism, sourceData);
            }
            if (mechType == RSA_PKCS) {
                encData = encryptFinal(handle.getHardLibHandle(), mechType, sourceData, this.tag.getBytes());
            } else if (SW_SM2_RAW == mechType) {
                encData = encryptFinal(handle.getHardLibHandle(), mechType, sourceData, this.tag.getBytes());
            } else {
                encData = encryptFinal(handle.getHardLibHandle(), mechType, sourceData, this.tag.getBytes());
            }
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(encData);
            Properties recvData = new Properties();
            try {
                recvData.load(byteArrayInputStream);
                String recvEnc = recvData.getProperty("EncData");
                String error = recvData.getProperty("Error");
                if (!error.equals("0") || recvEnc == null) {
                    throw new PKIException(PKIException.DATA_LOAD_FAIL, PKIException.ENCRYPT_DES, new PKIException(error, errorDesc(error)));
                }
                new Hex();
                return Hex.decode(recvEnc);
            } catch (Exception e) {
                throw new PKIException(PKIException.DATA_LOAD_FAIL, PKIException.ENCRYPT_DES, new PKIException(JIT_PARSE_FAIL, errorDesc(JIT_PARSE_FAIL)));
            }
        } else {
            throw new PKIException(PKIException.DATA_LOAD_FAIL, "加密操作失败 本操作不支持此种机制类型 " + mType);
        }
    }

    public JHandle encryptInit(Mechanism mechanism, JKey enKey) throws PKIException {
        byte[] encInit;
        String mType = mechanism.getMechanismType();
        if (mType.equals(Mechanism.SF33_ECB) || mType.equals(Mechanism.RSA_PKCS) || mType.equals(Mechanism.DES_CBC) || mType.equals(Mechanism.DES_ECB) || mType.equals(Mechanism.DES3_ECB) || mType.equals(Mechanism.SM2_RAW) || mType.equals(Mechanism.SF33_CBC) || mType.equals(Mechanism.AES_CBC) || mType.equals(Mechanism.AES_ECB) || mType.equals(Mechanism.DES3_CBC)) {
            long mechType = JavaToJNIMechanismType(mType);
            CBCParam cbcParam = (CBCParam) mechanism.getParam();
            byte[] cbcIv = null;
            if (cbcParam != null) {
                cbcIv = cbcParam.getIv();
            }
            if (mechType == RSA_PKCS) {
                encInit = encryptInit(mechType, JavaToJNIKeyType(enKey.getKeyType()), Parser.softKey2HardCustomKey(enKey.getKeyType(), enKey.getKey()), cbcIv, this.tag.getBytes());
            } else if (mechType == SW_SM2_RAW) {
                encInit = encryptInit(mechType, JavaToJNIKeyType(enKey.getKeyType()), Parser.softKey2HardCustomKey(enKey.getKeyType(), enKey.getKey()), cbcIv, this.tag.getBytes());
            } else {
                encInit = encryptInit(mechType, JavaToJNIKeyType(enKey.getKeyType()), enKey.getKey(), cbcIv, this.tag.getBytes());
            }
            ByteArrayInputStream in = new ByteArrayInputStream(encInit);
            Properties recvData = new Properties();
            try {
                recvData.load(in);
                String recvEncInit = recvData.getProperty("session");
                String error = recvData.getProperty("Error");
                if (error.equals("0") && recvEncInit != null) {
                    return new JHandle(Long.parseLong(recvEncInit), null);
                }
                throw new PKIException(PKIException.DATA_LOAD_FAIL, PKIException.ENCRYPT_DES, new PKIException(error, errorDesc(error)));
            } catch (Exception e) {
                throw new PKIException(PKIException.DATA_LOAD_FAIL, PKIException.ENCRYPT_DES, new PKIException(JIT_PARSE_FAIL, errorDesc(JIT_PARSE_FAIL)));
            }
        } else {
            throw new PKIException(PKIException.DATA_LOAD_FAIL, "加密操作失败 本操作不支持此种机制类型 " + mType);
        }
    }

    public byte[] encryptUpdate(JHandle handle, Mechanism mechanism, byte[] sourceData) throws PKIException {
        byte[] encData;
        String mType = mechanism.getMechanismType();
        if (mType.equals(Mechanism.SF33_ECB) || mType.equals(Mechanism.RSA_PKCS) || mType.equals(Mechanism.DES_CBC) || mType.equals(Mechanism.DES_ECB) || mType.equals(Mechanism.DES3_ECB) || mType.equals(Mechanism.SM2_RAW) || mType.equals(Mechanism.SF33_CBC) || mType.equals(Mechanism.AES_CBC) || mType.equals(Mechanism.AES_ECB) || mType.equals(Mechanism.DES3_CBC)) {
            long mechType = JavaToJNIMechanismType(mType);
            if (mechType == RSA_PKCS) {
                encData = encryptUpdate(handle.getHardLibHandle(), mechType, sourceData, this.tag.getBytes());
            } else if (SW_SM2_RAW == mechType) {
                encData = encryptUpdate(handle.getHardLibHandle(), mechType, sourceData, this.tag.getBytes());
            } else {
                encData = encryptUpdate(handle.getHardLibHandle(), mechType, sourceData, this.tag.getBytes());
            }
            ByteArrayInputStream in = new ByteArrayInputStream(encData);
            Properties recvData = new Properties();
            try {
                recvData.load(in);
                String recvEnc = recvData.getProperty("EncData");
                String error = recvData.getProperty("Error");
                if (!error.equals("0") || recvEnc == null) {
                    throw new PKIException(PKIException.DATA_LOAD_FAIL, PKIException.ENCRYPT_DES, new PKIException(error, errorDesc(error)));
                }
                new Hex();
                return Hex.decode(recvEnc);
            } catch (Exception e) {
                throw new PKIException(PKIException.DATA_LOAD_FAIL, PKIException.ENCRYPT_DES, new PKIException(JIT_PARSE_FAIL, errorDesc(JIT_PARSE_FAIL)));
            }
        } else {
            throw new PKIException(PKIException.DATA_LOAD_FAIL, "加密操作失败 本操作不支持此种机制类型 " + mType);
        }
    }

    public String getCfgTagName() throws PKIException {
        return this.tag;
    }

    public void setCfgTag(PKIToolConfig cfg) throws PKIException {
        this.CfgTag = cfg;
    }

    public PKIToolConfig getCfgTag() throws PKIException {
        return this.CfgTag;
    }

    private Mechanism encMech2genMech(Mechanism encMech) throws PKIException {
        if (encMech == null) {
            return null;
        }
        String mech = encMech.getMechanismType();
        if (mech.equals(Mechanism.SF33_ECB) || mech.equals(Mechanism.SF33_CBC)) {
            return new Mechanism("SF33");
        }
        if (mech.equals(Mechanism.DES_ECB) || mech.equals(Mechanism.DES_CBC)) {
            return new Mechanism("DES");
        }
        if (mech.equals(Mechanism.DES3_ECB) || mech.equals(Mechanism.DES3_CBC)) {
            return new Mechanism("DESede");
        }
        if (mech.equals(Mechanism.AES_ECB) || mech.equals(Mechanism.AES_CBC)) {
            return new Mechanism("AES");
        }
        if (mech.equals(Mechanism.SCB2_ECB) || mech.equals(Mechanism.SCB2_CBC)) {
            return new Mechanism("SCB2");
        }
        if (mech.equals(Mechanism.RSA_PKCS)) {
            return new Mechanism(Mechanism.RSA);
        }
        if (mech.equals(Mechanism.SM2_RAW)) {
            return new Mechanism(Mechanism.SM2);
        }
        return null;
    }

    public List WrapKeyEnc(JKey pubKey, JKey sysKey, Mechanism pubEncMech, Mechanism sysEncMech, byte[] data) throws PKIException {
        byte[] encData;
        String pubEncType = pubEncMech.getMechanismType();
        String sysEnctype = sysEncMech.getMechanismType();
        if (!sysEnctype.equals(Mechanism.SF33_ECB) && !sysEnctype.equals(Mechanism.DES_CBC) && !sysEnctype.equals(Mechanism.DES_ECB) && !sysEnctype.equals(Mechanism.DES3_ECB) && !sysEnctype.equals(Mechanism.AES_ECB) && !sysEnctype.equals(Mechanism.AES_CBC) && !sysEnctype.equals(Mechanism.DES3_CBC) && !sysEnctype.equals(Mechanism.SCB2_CBC) && !sysEnctype.equals(Mechanism.SCB2_ECB) && !sysEnctype.equals(Mechanism.SM4_CBC) && !sysEnctype.equals(Mechanism.SM4_ECB)) {
            throw new PKIException(PKIException.DATA_LOAD_FAIL, "加密操作失败 本操作不支持此种机制类型 " + sysEnctype);
        } else if (pubEncType.equals(Mechanism.RSA_PKCS) || pubEncType.equals(Mechanism.SM2_RAW)) {
            CBCParam cbcParam = (CBCParam) sysEncMech.getParam();
            byte[] cbcIv = null;
            if (cbcParam != null) {
                cbcIv = cbcParam.getIv();
            }
            if (sysEncMech.isPad()) {
                data = getPaddingData(sysEncMech, data);
            }
            long lpubEncType = JavaToJNIMechanismType(pubEncType);
            long lsysEncType = JavaToJNIMechanismType(sysEnctype);
            if (sysKey == null) {
                encData = wrapKeyEnc(Parser.softKey2HardCustomKey(pubKey.getKeyType(), pubKey.getKey()), null, lpubEncType, lsysEncType, data, cbcIv, this.tag.getBytes());
            } else {
                encData = wrapKeyEnc(Parser.softKey2HardCustomKey(pubKey.getKeyType(), pubKey.getKey()), sysKey.getKey(), lpubEncType, lsysEncType, data, cbcIv, this.tag.getBytes());
            }
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(encData);
            Properties recvData = new Properties();
            try {
                recvData.load(byteArrayInputStream);
                String recvEnc = recvData.getProperty("EncData");
                String error = recvData.getProperty("Error");
                String outKey = recvData.getProperty("WrapedSysKey");
                ArrayList arrayList = new ArrayList();
                if (!error.equals("0") || recvEnc == null) {
                    throw new PKIException(PKIException.DATA_LOAD_FAIL, PKIException.ENCRYPT_DES, new PKIException(error, errorDesc(error)));
                }
                new Hex();
                arrayList.add(Hex.decode(recvEnc));
                arrayList.add(Hex.decode(outKey));
                return arrayList;
            } catch (Exception e) {
                throw new PKIException(PKIException.DATA_LOAD_FAIL, PKIException.ENCRYPT_DES, new PKIException(JIT_PARSE_FAIL, errorDesc(JIT_PARSE_FAIL)));
            }
        } else {
            throw new PKIException(PKIException.DATA_LOAD_FAIL, "加密操作失败 本操作不支持此种机制类型 " + pubEncType);
        }
    }

    private byte[] getPaddingData(Mechanism mech, byte[] data) throws PKIException {
        int pad;
        String mType = mech.getMechanismType();
        if (mType.equals(Mechanism.RSA_PKCS) || mType.equals(Mechanism.SM2_RAW) || mType.equals("RC4")) {
            return data;
        }
        if (mType.equals(Mechanism.SF33_ECB) || mType.equals(Mechanism.SF33_CBC) || mType.equals(Mechanism.SCB2_ECB) || mType.equals(Mechanism.SCB2_CBC) || mType.equals(Mechanism.AES_ECB) || mType.equals(Mechanism.AES_CBC)) {
            if (data.length % 16 == 0) {
                pad = 16;
            } else {
                pad = 16 - (data.length % 16);
            }
        } else if (data.length % 8 == 0) {
            pad = 8;
        } else {
            pad = 8 - (data.length % 8);
        }
        byte[] ret = new byte[(data.length + pad)];
        System.arraycopy(data, 0, ret, 0, data.length);
        for (int i = 0; i < pad; i++) {
            ret[data.length + i] = (byte) pad;
        }
        return ret;
    }

    private byte[] paddingDrop(byte[] data) throws PKIException {
        byte b = data[data.length - 1];
        byte[] ret = new byte[(data.length - b)];
        System.arraycopy(data, 0, ret, 0, data.length - b);
        return ret;
    }

    public String getSessVersion() throws PKIException {
        ByteArrayInputStream in = new ByteArrayInputStream(getMRTDVision(null, null));
        Properties recvData = new Properties();
        try {
            recvData.load(in);
        } catch (Exception e) {
            new PKIException(JIT_PARSE_FAIL, errorDesc(JIT_PARSE_FAIL));
        }
        String error = recvData.getProperty("Error");
        String recvVersion = recvData.getProperty("version");
        if (error.equals("0")) {
            return recvVersion;
        }
        throw new PKIException(PKIException.JHARDLIB, new PKIException(error, errorDesc(error)));
    }

    private byte[] getSM2EnvedKey(Mechanism sysEncMech, byte[] encedSysKey, byte[] pubkey, byte[] encedPriKey) throws PKIException {
        DERObjectIdentifier oid;
        if (sysEncMech.getMechanismType().equals(Mechanism.SM4_ECB)) {
            oid = PKCSObjectIdentifiers.gm_SM4;
        } else if (sysEncMech.getMechanismType().equals(Mechanism.SCB2_ECB)) {
            oid = PKCSObjectIdentifiers.gm_SM1;
        } else if (!sysEncMech.getMechanismType().equals(Mechanism.SF33_ECB)) {
            return null;
        } else {
            oid = PKCSObjectIdentifiers.gm_SSF33;
        }
        AlgorithmIdentifier algo = new AlgorithmIdentifier(oid, new DERNull());
        ASN1EncodableVector v = null;
        try {
            ASN1Sequence sysKey = (ASN1Sequence) new ASN1InputStream(new ByteArrayInputStream(encedSysKey)).readObject();
            DERBitString bitPriKey = new DERBitString(encedPriKey);
            DERBitString bitPubKey = new DERBitString(pubkey);
            ASN1EncodableVector v2 = new ASN1EncodableVector();
            try {
                v2.add(algo);
                v2.add(sysKey);
                v2.add(bitPubKey);
                v2.add(bitPriKey);
                v = v2;
            } catch (IOException e) {
                e = e;
                v = v2;
                e.printStackTrace();
                return Parser.writeDERObj2Bytes(new BERSequence(v));
            }
        } catch (IOException e2) {
            e = e2;
            e.printStackTrace();
            return Parser.writeDERObj2Bytes(new BERSequence(v));
        }
        return Parser.writeDERObj2Bytes(new BERSequence(v));
    }

    public byte[] WrapPriKey(JKey pubKey, JKey sysKey, Mechanism pubEncMech, Mechanism sysEncMech, JKey priKey) throws PKIException {
        try {
            byte[] eccsfpubk = Util.hardKey2SoftPubKey(pubKey);
            byte[] priData = Util.byteconvert32(Util.hardKey2SoftPrivKey(priKey));
            byte[] data = new byte[(priData.length + 32)];
            for (int i = 0; i < 32; i++) {
                data[i] = 0;
            }
            System.arraycopy(priData, 0, data, 32, 32);
            sysEncMech.setPad(false);
            List ls = WrapKeyEnc(pubKey, sysKey, pubEncMech, sysEncMech, data);
            return getSM2EnvedKey(sysEncMech, (byte[]) ls.get(1), eccsfpubk, (byte[]) ls.get(0));
        } catch (Exception ex) {
            throw new PKIException(PKIException.DATA_LOAD_FAIL, ex.getMessage());
        }
    }
}
