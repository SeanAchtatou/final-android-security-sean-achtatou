package cn.com.jit.ida.util.pki.asn1;

import de.innosystec.unrar.unpack.vm.VMCmdFlags;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class DERTaggedObject extends ASN1TaggedObject {
    public DERTaggedObject(int tagNo, DEREncodable obj) {
        super(tagNo, obj);
    }

    public DERTaggedObject(boolean explicit, int tagNo, DEREncodable obj) {
        super(explicit, tagNo, obj);
    }

    public DERTaggedObject(int tagNo) {
        super(false, tagNo, new DERSequence());
    }

    /* access modifiers changed from: package-private */
    public void encode(DEROutputStream out) throws IOException {
        if (!this.empty) {
            ByteArrayOutputStream bOut = new ByteArrayOutputStream();
            DEROutputStream dOut = new DEROutputStream(bOut);
            dOut.writeObject(this.obj);
            dOut.close();
            byte[] bytes = bOut.toByteArray();
            if (this.explicit) {
                out.writeEncoded(this.tagNo | 160, bytes);
                return;
            }
            if ((bytes[0] & VMCmdFlags.VMCF_USEFLAGS) != 0) {
                bytes[0] = (byte) (this.tagNo | 160);
            } else {
                bytes[0] = (byte) (this.tagNo | 128);
            }
            out.write(bytes);
            return;
        }
        out.writeEncoded(this.tagNo | 160, new byte[0]);
    }
}
