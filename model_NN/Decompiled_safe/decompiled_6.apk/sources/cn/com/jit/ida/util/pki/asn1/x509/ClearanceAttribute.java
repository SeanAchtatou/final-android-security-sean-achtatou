package cn.com.jit.ida.util.pki.asn1.x509;

public class ClearanceAttribute extends Attribute {
    public ClearanceAttribute() {
        setTttrType(Attribute.CLEARANCE_ATTRIBUTE_OID);
    }

    public ClearanceSyntax createClearanceSyntax() {
        return new ClearanceSyntax();
    }

    public void addClearanceSyntax(ClearanceSyntax obj) {
        addAttValueObject(obj.getDERObject());
    }
}
