package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.ASN1Encodable;
import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import java.util.Enumeration;
import java.util.Vector;

public class NoticeReference extends ASN1Encodable {
    ASN1Sequence noticeNumbers;
    DisplayText organization;

    public NoticeReference(String orgName, Vector numbers) {
        this.organization = new DisplayText(0, orgName);
        Object o = numbers.elementAt(0);
        ASN1EncodableVector av = new ASN1EncodableVector();
        if (o instanceof Integer) {
            Enumeration it = numbers.elements();
            while (it.hasMoreElements()) {
                av.add(new DERInteger(((Integer) it.nextElement()).intValue()));
            }
        }
        this.noticeNumbers = new DERSequence(av);
    }

    public NoticeReference(String orgName, ASN1Sequence numbers) {
        this.organization = new DisplayText(0, orgName);
        this.noticeNumbers = numbers;
    }

    public NoticeReference(int displayTextType, String orgName, ASN1Sequence numbers) {
        this.organization = new DisplayText(displayTextType, orgName);
        this.noticeNumbers = numbers;
    }

    public NoticeReference(ASN1Sequence as) {
        this.organization = DisplayText.getInstance(as.getObjectAt(0));
        this.noticeNumbers = (ASN1Sequence) as.getObjectAt(1);
    }

    public static NoticeReference getInstance(Object as) {
        if (as instanceof NoticeReference) {
            return (NoticeReference) as;
        }
        if (as instanceof ASN1Sequence) {
            return new NoticeReference((ASN1Sequence) as);
        }
        throw new IllegalArgumentException("unknown object in getInstance.");
    }

    public DERObject toASN1Object() {
        ASN1EncodableVector av = new ASN1EncodableVector();
        av.add(this.organization);
        av.add(this.noticeNumbers);
        return new DERSequence(av);
    }

    public String getOrganization() {
        return this.organization.getString();
    }

    public int getNoticeNumberCount() {
        return this.noticeNumbers.size();
    }

    public long getNoticeNumber(int index) {
        return ((DERInteger) this.noticeNumbers.getObjectAt(index)).getValue().longValue();
    }
}
