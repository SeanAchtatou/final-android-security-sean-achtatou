package cn.com.jit.ida.util.pki.cipher.lib;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.PKIToolConfig;
import cn.com.jit.ida.util.pki.Parser;
import cn.com.jit.ida.util.pki.cipher.JCrypto;
import cn.com.jit.ida.util.pki.cipher.JHandle;
import cn.com.jit.ida.util.pki.cipher.JKey;
import cn.com.jit.ida.util.pki.cipher.JKeyPair;
import cn.com.jit.ida.util.pki.cipher.Mechanism;
import cn.com.jit.ida.util.pki.cipher.Session;
import cn.com.jit.ida.util.pki.cipher.param.CBCParam;
import cn.com.jit.ida.util.pki.cipher.param.PBEParam;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.List;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEParameterSpec;
import org.bouncycastle.crypto.AsymmetricBlockCipher;
import org.bouncycastle.crypto.engines.RSAEngine;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.params.RSAPrivateCrtKeyParameters;
import org.bouncycastle.jce.provider.JCERSAPrivateCrtKey;
import org.bouncycastle.jce.provider.JCERSAPublicKey;

public class JCaviumLib implements Session {
    public static final String PROVIDER = "BC";
    private PKIToolConfig CfgTag = null;
    private String tag = "PKITOOL";

    public static native double nativeAllocContext();

    public static native long nativeFreeContext(double d);

    public static native long nativeRSASign(int i, byte[] bArr, int i2, byte[] bArr2, int i3, byte[] bArr3, int i4, byte[] bArr4, int[] iArr);

    public static native long nativeRSAVerify(int i, byte[] bArr, int i2, byte[] bArr2, int i3, byte[] bArr3, int i4, byte[] bArr4, int i5);

    public native long deactivateDriver();

    public native long nativeDigestProcess(byte[] bArr, int i, byte[] bArr2, int[] iArr, int i2);

    public native long nativeInitDriver();

    public JCaviumLib() {
        System.loadLibrary("cavium_native");
        if (nativeInitDriver() != 0) {
            System.out.println("FATAL ERROR : Failed to load native Cavium Driver ");
        }
    }

    public void finalize() {
        System.out.println("Deactivating Cavium Driver.");
        deactivateDriver();
    }

    public byte[] digest(Mechanism mechanism, byte[] sourceData) throws PKIException {
        String mType = mechanism.getMechanismType();
        if (!mechanism.isDigestabled()) {
            throw new PKIException("8122", "文摘操作失败 本操作不支持此种机制类型 " + mType);
        }
        try {
            MessageDigest m = MessageDigest.getInstance(mType, "BC");
            m.update(sourceData);
            return m.digest();
        } catch (Exception ex) {
            throw new PKIException("8122", PKIException.DIGEST_DES, ex);
        }
    }

    public byte[] mac(Mechanism mechanism, JKey key, byte[] sourceData) throws PKIException {
        String mType = mechanism.getMechanismType();
        if (mType.equals(Mechanism.HMAC_MD2) || mType.equals(Mechanism.HMAC_MD5) || mType.equals(Mechanism.HMAC_SHA1)) {
            try {
                Mac mac = Mac.getInstance(mechanism.getMechanismType(), "BC");
                mac.init(Parser.convertSecretKey(key));
                mac.update(sourceData);
                return mac.doFinal();
            } catch (Exception ex) {
                throw new PKIException("8123", PKIException.MAC_DES, ex);
            }
        } else {
            throw new PKIException("8123", "MAC操作失败 本操作不支持此种机制类型 " + mType);
        }
    }

    public boolean verifyMac(Mechanism mechanism, JKey key, byte[] sourceData, byte[] macData) throws PKIException {
        String mType = mechanism.getMechanismType();
        if (mType.equals(Mechanism.HMAC_MD2) || mType.equals(Mechanism.HMAC_MD5) || mType.equals(Mechanism.HMAC_SHA1)) {
            try {
                return Parser.isEqualArray(mac(mechanism, key, sourceData), macData);
            } catch (Exception ex) {
                throw new PKIException("8124", PKIException.VERIFY_MAC_DES, ex);
            }
        } else {
            throw new PKIException("8124", "验证MAC操作失败 本操作不支持此种机制类型 " + mType);
        }
    }

    public byte[] sign(Mechanism mechanism, JKey prvKey, byte[] sourceData) throws PKIException {
        int type;
        String mType = mechanism.getMechanismType();
        if (!mechanism.isSignabled()) {
            throw new PKIException("8125", "签名操作失败 本操作不支持此种机制类型 " + mType);
        }
        if (mType.equals("MD5withRSAEncryption")) {
            type = 1;
        } else if (mType.equals("SHA1withRSAEncryption")) {
            type = 2;
        } else if (mType.equals(Mechanism.RSA_PKCS)) {
            type = 6;
        } else {
            throw new PKIException("8125", "签名操作失败 本操作不支持此种机制类型 " + mType);
        }
        try {
            RSAPrivateKey prv = (RSAPrivateKey) Parser.convertPrivateKey(prvKey);
            byte[] out = new byte[(prv.getModulus().bitLength() / 8)];
            int[] outsize = new int[1];
            if (nativeRSASign(type, roundup8(prv.getPrivateExponent()), prv.getPrivateExponent().bitLength(), roundup8(prv.getModulus()), prv.getModulus().bitLength(), sourceData, sourceData.length, out, outsize) != 0) {
                throw new PKIException("8125", PKIException.SIGN_DES);
            } else if (out.length == outsize[0]) {
                return out;
            } else {
                byte[] result = new byte[outsize[0]];
                System.arraycopy(out, 0, result, 0, outsize[0]);
                return result;
            }
        } catch (Exception ex) {
            throw new PKIException("8125", PKIException.SIGN_DES, ex);
        }
    }

    public boolean verifySign(Mechanism mechanism, JKey pubKey, byte[] sourceData, byte[] signData) throws PKIException {
        int type;
        String mType = mechanism.getMechanismType();
        if (!mechanism.isSignabled()) {
            throw new PKIException("8126", "验证签名操作失败 本操作不支持此种机制类型 " + mType);
        }
        if (mType.equals("MD5withRSAEncryption")) {
            type = 1;
        } else if (mType.equals("SHA1withRSAEncryption")) {
            type = 2;
        } else if (mType.equals(Mechanism.RSA_PKCS)) {
            type = 6;
        } else {
            throw new PKIException("8126", "验证签名操作失败 本操作不支持此种机制类型 " + mType);
        }
        try {
            RSAPublicKey publicKey = (RSAPublicKey) Parser.convertPublicKey(pubKey);
            long error = nativeRSAVerify(type, roundup8(publicKey.getPublicExponent()), publicKey.getPublicExponent().bitLength(), roundup8(publicKey.getModulus()), publicKey.getModulus().bitLength(), signData, signData.length, sourceData, sourceData.length);
            if (error == 1) {
                return true;
            }
            if (error == 0) {
                return false;
            }
            throw new PKIException("8126", PKIException.VERIFY_SIGN_DES);
        } catch (Exception ex) {
            throw new PKIException("8126", PKIException.VERIFY_SIGN_DES, ex);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cn.com.jit.ida.util.pki.cipher.lib.JCaviumLib.doCipher(cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, boolean, byte[]):byte[]
     arg types: [cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, int, byte[]]
     candidates:
      cn.com.jit.ida.util.pki.cipher.lib.JCaviumLib.doCipher(cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, boolean, java.io.InputStream):byte[]
      cn.com.jit.ida.util.pki.cipher.lib.JCaviumLib.doCipher(cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, boolean, byte[]):byte[] */
    public byte[] encrypt(Mechanism mechanism, JKey enKey, byte[] sourceData) throws PKIException {
        try {
            return doCipher(mechanism, enKey, true, sourceData);
        } catch (Exception ex) {
            throw new PKIException("8120", PKIException.ENCRYPT_DES, ex);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cn.com.jit.ida.util.pki.cipher.lib.JCaviumLib.doCipher(cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, boolean, byte[]):byte[]
     arg types: [cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, int, byte[]]
     candidates:
      cn.com.jit.ida.util.pki.cipher.lib.JCaviumLib.doCipher(cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, boolean, java.io.InputStream):byte[]
      cn.com.jit.ida.util.pki.cipher.lib.JCaviumLib.doCipher(cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, boolean, byte[]):byte[] */
    public byte[] decrypt(Mechanism mechanism, JKey deKey, byte[] encryptedData) throws PKIException {
        try {
            return doCipher(mechanism, deKey, false, encryptedData);
        } catch (Exception ex) {
            throw new PKIException("8121", PKIException.DECRYPT_DES, ex);
        }
    }

    public JKey generateKey(Mechanism mechanism, int keyLength) throws PKIException {
        String mType = mechanism.getMechanismType();
        if (mType.equals("DES") || mType.equals("DESede") || mType.equals("RC2") || mType.equals("RC4") || mType.equals("CAST5") || mType.equals("IDEA") || mType.equals("AES")) {
            try {
                KeyGenerator keyGen = KeyGenerator.getInstance(mechanism.getMechanismType(), "BC");
                keyGen.init(keyLength);
                SecretKey key = keyGen.generateKey();
                return new JKey(key.getAlgorithm(), key.getEncoded());
            } catch (Exception ex) {
                throw new PKIException("8110", PKIException.SYM_KEY_DES, ex);
            }
        } else {
            throw new PKIException("8110", "产生对称密钥操作失败 本操作不支持此种机制类型 " + mType);
        }
    }

    public JKeyPair generateKeyPair(Mechanism mechanism, int keyLength) throws PKIException {
        JKey jPubKey;
        String mType = mechanism.getMechanismType();
        if (!mechanism.isGenerateKeyPairabled()) {
            throw new PKIException("8111", "产生非对称密钥对失败 本操作不支持此种机制类型 " + mType);
        }
        JKey jPubKey2 = null;
        JKey jPrvKey = null;
        try {
            KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance(mType, "BC");
            keyPairGen.initialize(keyLength, new SecureRandom());
            KeyPair keyPair = keyPairGen.generateKeyPair();
            PublicKey pubKey = keyPair.getPublic();
            PrivateKey prvKey = keyPair.getPrivate();
            byte[] pubKeyEncoded = pubKey.getEncoded();
            byte[] prvKeyEncoded = prvKey.getEncoded();
            if (mechanism.getMechanismType().equals(Mechanism.RSA)) {
                jPubKey = new JKey(JKey.RSA_PUB_KEY, pubKeyEncoded);
                try {
                    jPrvKey = new JKey(JKey.RSA_PRV_KEY, prvKeyEncoded);
                    jPubKey2 = jPubKey;
                } catch (Exception e) {
                    ex = e;
                    throw new PKIException("8111", PKIException.KEY_PAIR_DES, ex);
                }
            } else if (mechanism.getMechanismType().equals(Mechanism.DSA)) {
                jPubKey = new JKey(JKey.DSA_PUB_KEY, pubKeyEncoded);
                jPrvKey = new JKey(JKey.DSA_PRV_KEY, prvKeyEncoded);
                jPubKey2 = jPubKey;
            } else if (mechanism.getMechanismType().equals(Mechanism.ECDSA)) {
                jPubKey = new JKey(JKey.ECDSA_PUB_KEY, pubKeyEncoded);
                jPrvKey = new JKey(JKey.ECDSA_PRV_KEY, prvKeyEncoded);
                jPubKey2 = jPubKey;
            } else if (mechanism.getMechanismType().equals(Mechanism.ECIES)) {
                jPubKey = new JKey(JKey.ECIES_PUB_KEY, pubKeyEncoded);
                jPrvKey = new JKey(JKey.ECIES_PRV_KEY, prvKeyEncoded);
                jPubKey2 = jPubKey;
            }
            return new JKeyPair(jPubKey2, jPrvKey);
        } catch (Exception e2) {
            ex = e2;
            throw new PKIException("8111", PKIException.KEY_PAIR_DES, ex);
        }
    }

    public boolean DestroyKeyPair(Mechanism mechanism) throws PKIException {
        throw new UnsupportedOperationException("Method DestroyKeyPair() not yet implemented in JSOFT_LIB.");
    }

    public JKey generatePBEKey(Mechanism mechanism, char[] password) throws PKIException {
        String mType = mechanism.getMechanismType();
        if (!mType.equals("PBEWithMD5AndDES") && !mType.equals("PBEWITHSHAAND2-KEYTRIPLEDES-CBC") && !mType.equals("PBEWITHSHAAND3-KEYTRIPLEDES-CBC")) {
            if (mType.equalsIgnoreCase("PBE/PKCS5")) {
                mType = "PBEWithMD5AndDES";
            } else {
                throw new PKIException("8112", "产生PBE密钥失败 本操作不支持此种机制类型 " + mType);
            }
        }
        try {
            byte[] keyData = new String(password).getBytes();
            if (mType.equals("PBEWithMD5AndDES")) {
                return new JKey("PBEWithMD5AndDES", keyData);
            }
            if (mType.equals("PBEWITHSHAAND2-KEYTRIPLEDES-CBC")) {
                return new JKey("PBEWITHSHAAND2-KEYTRIPLEDES-CBC", keyData);
            }
            return new JKey("PBEWITHSHAAND3-KEYTRIPLEDES-CBC", keyData);
        } catch (Exception ex) {
            throw new PKIException("8112", PKIException.PBE_KEY_DES, ex);
        }
    }

    public byte[] generateRandom(Mechanism mechanism, int length) throws PKIException {
        String mType = mechanism.getMechanismType();
        if (!mechanism.getMechanismType().equals(Mechanism.RANDOM)) {
            throw new PKIException("8113", "产生随机数失败 本操作不支持此种机制类型 " + mType);
        }
        byte[] data = new byte[length];
        new SecureRandom().nextBytes(data);
        return data;
    }

    private byte[] doCipher(Mechanism mechanism, JKey jkey, boolean isEncrypt, byte[] data) throws Exception {
        int cipherMode;
        String mType = mechanism.getMechanismType();
        int rsaKeyLen = -1;
        if (mType.equalsIgnoreCase(Mechanism.RSA_PKCS)) {
            if (jkey.getKeyType().equals(JKey.RSA_PUB_KEY)) {
                rsaKeyLen = ((RSAPublicKey) Parser.convertPublicKey(jkey)).getModulus().bitLength();
            } else if (jkey.getKeyType().equals(JKey.RSA_PRV_KEY)) {
                rsaKeyLen = ((RSAPrivateKey) Parser.convertPrivateKey(jkey)).getModulus().bitLength();
            }
            if (rsaKeyLen > 2048) {
                return doCipher_RSA_ext(mechanism, jkey, isEncrypt, data);
            }
        }
        Cipher cipher = Cipher.getInstance(mType, "BC");
        if (isEncrypt) {
            cipherMode = 1;
        } else {
            cipherMode = 2;
        }
        if (mType.indexOf("PBE") != -1) {
            PBEParam pbeParam = (PBEParam) mechanism.getParam();
            if (pbeParam == null) {
                throw new PKIException("PBE参数为空");
            }
            cipher.init(cipherMode, Parser.convertKey(jkey), new PBEParameterSpec(pbeParam.getSalt(), pbeParam.getIterations()));
        } else if (mType.indexOf("CBC") != -1) {
            CBCParam cbcParam = (CBCParam) mechanism.getParam();
            if (cbcParam == null) {
                throw new PKIException("CBC参数为空");
            }
            cipher.init(cipherMode, Parser.convertKey(jkey), new IvParameterSpec(cbcParam.getIv()));
        } else {
            cipher.init(cipherMode, Parser.convertKey(jkey));
        }
        return cipher.doFinal(data);
    }

    public static void main(String[] args) {
        System.out.println("OK");
        try {
            JCrypto jcrypto = JCrypto.getInstance();
            jcrypto.initialize(JCrypto.JSOFT_LIB, null);
            Session session = jcrypto.openSession(JCrypto.JSOFT_LIB);
            Mechanism keyGen = new Mechanism("PBEWITHSHAAND3-KEYTRIPLEDES-CBC");
            Mechanism mechanism = new Mechanism("PBEWITHSHAAND3-KEYTRIPLEDES-CBC", new PBEParam());
            JKey key = session.generatePBEKey(keyGen, "HELLO".toCharArray());
            byte[] enData = session.encrypt(mechanism, key, "JIT公司测试".getBytes());
            System.out.println(new String(enData));
            System.out.println(new String(session.decrypt(mechanism, key, enData)));
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
    }

    private byte[] doCipher_RSA_ext(Mechanism mechanism, JKey jkey, boolean isEncrypt, byte[] data) throws Exception {
        RSAKeyParameters keyParams;
        AsymmetricBlockCipher eng = new RSAEngine();
        if (jkey.getKeyType().equals(JKey.RSA_PUB_KEY)) {
            JCERSAPublicKey pubKey = Parser.convertPublicKey(jkey);
            keyParams = new RSAKeyParameters(false, pubKey.getModulus(), pubKey.getPublicExponent());
        } else {
            JCERSAPrivateCrtKey prvKey = Parser.convertPrivateKey(jkey);
            keyParams = new RSAPrivateCrtKeyParameters(prvKey.getModulus(), prvKey.getPublicExponent(), prvKey.getPrivateExponent(), prvKey.getPrimeP(), prvKey.getPrimeQ(), prvKey.getPrimeExponentP(), prvKey.getPrimeExponentQ(), prvKey.getCrtCoefficient());
        }
        eng.init(isEncrypt, keyParams);
        return eng.processBlock(data, 0, data.length);
    }

    public byte[] digest(Mechanism mechanism, InputStream sourceData) throws PKIException {
        String mType = mechanism.getMechanismType();
        if (!mechanism.isDigestabled()) {
            throw new PKIException("8122", "文摘操作失败 本操作不支持此种机制类型 " + mType);
        }
        try {
            MessageDigest m = MessageDigest.getInstance(mType, "BC");
            byte[] buffer = new byte[1024];
            while (true) {
                int i = sourceData.read(buffer);
                if (i <= 0) {
                    return m.digest();
                }
                m.update(buffer, 0, i);
            }
        } catch (Exception ex) {
            throw new PKIException("8122", PKIException.DIGEST_DES, ex);
        }
    }

    public byte[] mac(Mechanism mechanism, JKey key, InputStream sourceData) throws PKIException {
        String mType = mechanism.getMechanismType();
        if (mType.equals(Mechanism.HMAC_MD2) || mType.equals(Mechanism.HMAC_MD5) || mType.equals(Mechanism.HMAC_SHA1)) {
            try {
                Mac mac = Mac.getInstance(mechanism.getMechanismType(), "BC");
                mac.init(Parser.convertSecretKey(key));
                byte[] buffer = new byte[1024];
                while (true) {
                    int i = sourceData.read(buffer);
                    if (i <= 0) {
                        return mac.doFinal();
                    }
                    mac.update(buffer, 0, i);
                }
            } catch (Exception ex) {
                throw new PKIException("8123", PKIException.MAC_DES, ex);
            }
        } else {
            throw new PKIException("8123", "MAC操作失败 本操作不支持此种机制类型 " + mType);
        }
    }

    public boolean verifyMac(Mechanism mechanism, JKey key, InputStream sourceData, byte[] macData) throws PKIException {
        String mType = mechanism.getMechanismType();
        if (mType.equals(Mechanism.HMAC_MD2) || mType.equals(Mechanism.HMAC_MD5) || mType.equals(Mechanism.HMAC_SHA1)) {
            try {
                return Parser.isEqualArray(mac(mechanism, key, sourceData), macData);
            } catch (Exception ex) {
                throw new PKIException("8124", PKIException.VERIFY_MAC_DES, ex);
            }
        } else {
            throw new PKIException("8124", "验证MAC操作失败 本操作不支持此种机制类型 " + mType);
        }
    }

    public byte[] sign(Mechanism mechanism, JKey prvKey, InputStream sourceData) throws PKIException {
        String mType = mechanism.getMechanismType();
        if (!mechanism.isSignabled()) {
            throw new PKIException("8125", "签名操作失败 本操作不支持此种机制类型 " + mType);
        }
        try {
            Signature signature = Signature.getInstance(mType, "BC");
            signature.initSign(Parser.convertPrivateKey(prvKey));
            byte[] buffer = new byte[1024];
            while (true) {
                int i = sourceData.read(buffer);
                if (i <= 0) {
                    return signature.sign();
                }
                signature.update(buffer, 0, i);
            }
        } catch (Exception ex) {
            throw new PKIException("8125", PKIException.SIGN_DES, ex);
        }
    }

    public boolean verifySign(Mechanism mechanism, JKey pubKey, InputStream sourceData, byte[] signData) throws PKIException {
        String mType = mechanism.getMechanismType();
        if (!mechanism.isSignabled()) {
            throw new PKIException("8126", "验证签名操作失败 本操作不支持此种机制类型 " + mType);
        }
        try {
            Signature signature = Signature.getInstance(mechanism.getMechanismType(), "BC");
            signature.initVerify(Parser.convertPublicKey(pubKey));
            byte[] buffer = new byte[1024];
            while (true) {
                int i = sourceData.read(buffer);
                if (i <= 0) {
                    return signature.verify(signData);
                }
                signature.update(buffer, 0, i);
            }
        } catch (Exception ex) {
            throw new PKIException("8126", PKIException.VERIFY_SIGN_DES, ex);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cn.com.jit.ida.util.pki.cipher.lib.JCaviumLib.doCipher(cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, boolean, java.io.InputStream):byte[]
     arg types: [cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, int, java.io.InputStream]
     candidates:
      cn.com.jit.ida.util.pki.cipher.lib.JCaviumLib.doCipher(cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, boolean, byte[]):byte[]
      cn.com.jit.ida.util.pki.cipher.lib.JCaviumLib.doCipher(cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, boolean, java.io.InputStream):byte[] */
    public byte[] encrypt(Mechanism mechanism, JKey enKey, InputStream sourceData) throws PKIException {
        try {
            return doCipher(mechanism, enKey, true, sourceData);
        } catch (Exception ex) {
            throw new PKIException("8120", PKIException.ENCRYPT_DES, ex);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cn.com.jit.ida.util.pki.cipher.lib.JCaviumLib.doCipher(cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, boolean, java.io.InputStream):byte[]
     arg types: [cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, int, java.io.InputStream]
     candidates:
      cn.com.jit.ida.util.pki.cipher.lib.JCaviumLib.doCipher(cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, boolean, byte[]):byte[]
      cn.com.jit.ida.util.pki.cipher.lib.JCaviumLib.doCipher(cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, boolean, java.io.InputStream):byte[] */
    public byte[] decrypt(Mechanism mechanism, JKey deKey, InputStream encryptedData) throws PKIException {
        try {
            return doCipher(mechanism, deKey, false, encryptedData);
        } catch (Exception ex) {
            throw new PKIException("8121", PKIException.DECRYPT_DES, ex);
        }
    }

    private byte[] doCipher(Mechanism mechanism, JKey jkey, boolean isEncrypt, InputStream data) throws Exception {
        int cipherMode;
        String mType = mechanism.getMechanismType();
        int rsaKeyLen = -1;
        if (mType.equalsIgnoreCase(Mechanism.RSA_PKCS)) {
            if (jkey.getKeyType().equals(JKey.RSA_PUB_KEY)) {
                rsaKeyLen = ((RSAPublicKey) Parser.convertPublicKey(jkey)).getModulus().bitLength();
            } else if (jkey.getKeyType().equals(JKey.RSA_PRV_KEY)) {
                rsaKeyLen = ((RSAPrivateKey) Parser.convertPrivateKey(jkey)).getModulus().bitLength();
            }
            if (rsaKeyLen > 2048) {
                byte[] bs = new byte[data.available()];
                data.read(bs);
                data.close();
                return doCipher_RSA_ext(mechanism, jkey, isEncrypt, bs);
            }
        }
        Cipher cipher = Cipher.getInstance(mType, "BC");
        if (isEncrypt) {
            cipherMode = 1;
        } else {
            cipherMode = 2;
        }
        if (mType.indexOf("CBC") != -1) {
            CBCParam cbcParam = (CBCParam) mechanism.getParam();
            if (cbcParam == null) {
                throw new PKIException("CBC参数为空");
            }
            cipher.init(cipherMode, Parser.convertKey(jkey), new IvParameterSpec(cbcParam.getIv()));
        } else if (mType.indexOf("PBE") != -1) {
            PBEParam pbeParam = (PBEParam) mechanism.getParam();
            if (pbeParam == null) {
                throw new PKIException("PBE参数为空");
            }
            cipher.init(cipherMode, Parser.convertKey(jkey), new PBEParameterSpec(pbeParam.getSalt(), pbeParam.getIterations()));
        } else {
            cipher.init(cipherMode, Parser.convertKey(jkey));
        }
        ByteArrayOutputStream bin = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        while (true) {
            int i = data.read(buffer);
            if (i > 0) {
                bin.write(cipher.update(buffer, 0, i));
            } else {
                bin.write(cipher.doFinal());
                return bin.toByteArray();
            }
        }
    }

    public byte[] decryptFinal(JHandle handle, Mechanism mechanism, byte[] sourceData) throws PKIException {
        try {
            return handle.getSoftLibHandle().doFinal(sourceData);
        } catch (Exception ex) {
            throw new PKIException("8121", PKIException.DECRYPT_DES, ex);
        }
    }

    public JHandle decryptInit(Mechanism mechanism, JKey deKey) throws PKIException {
        try {
            String mType = mechanism.getMechanismType();
            Cipher largeDataCipher = Cipher.getInstance(mType, "BC");
            if (mType.indexOf("PBE") != -1) {
                PBEParam pbeParam = (PBEParam) mechanism.getParam();
                if (pbeParam == null) {
                    throw new PKIException("PBE参数为空");
                }
                largeDataCipher.init(2, Parser.convertKey(deKey), new PBEParameterSpec(pbeParam.getSalt(), pbeParam.getIterations()));
            } else if (mType.indexOf("CBC") != -1) {
                CBCParam cbcParam = (CBCParam) mechanism.getParam();
                if (cbcParam == null) {
                    throw new PKIException("CBC参数为空");
                }
                largeDataCipher.init(2, Parser.convertKey(deKey), new IvParameterSpec(cbcParam.getIv()));
            } else {
                largeDataCipher.init(2, Parser.convertKey(deKey));
            }
            return new JHandle(0, largeDataCipher);
        } catch (Exception ex) {
            throw new PKIException("8120", PKIException.DECRYPT_DES, ex);
        }
    }

    public byte[] decryptUpdate(JHandle handle, Mechanism mechanism, byte[] sourceData) throws PKIException {
        try {
            return handle.getSoftLibHandle().update(sourceData);
        } catch (Exception ex) {
            throw new PKIException("8121", PKIException.DECRYPT_DES, ex);
        }
    }

    public byte[] encryptFinal(JHandle handle, Mechanism mechanism, byte[] sourceData) throws PKIException {
        try {
            return handle.getSoftLibHandle().doFinal(sourceData);
        } catch (Exception ex) {
            throw new PKIException("8121", PKIException.DECRYPT_DES, ex);
        }
    }

    public JHandle encryptInit(Mechanism mechanism, JKey enKey) throws PKIException {
        try {
            String mType = mechanism.getMechanismType();
            Cipher largeDataCipher = Cipher.getInstance(mType, "BC");
            if (mType.indexOf("PBE") != -1) {
                PBEParam pbeParam = (PBEParam) mechanism.getParam();
                if (pbeParam == null) {
                    throw new PKIException("PBE参数为空");
                }
                largeDataCipher.init(1, Parser.convertKey(enKey), new PBEParameterSpec(pbeParam.getSalt(), pbeParam.getIterations()));
            } else if (mType.indexOf("CBC") != -1) {
                CBCParam cbcParam = (CBCParam) mechanism.getParam();
                if (cbcParam == null) {
                    throw new PKIException("CBC参数为空");
                }
                largeDataCipher.init(1, Parser.convertKey(enKey), new IvParameterSpec(cbcParam.getIv()));
            } else {
                largeDataCipher.init(1, Parser.convertKey(enKey));
            }
            return new JHandle(0, largeDataCipher);
        } catch (Exception ex) {
            throw new PKIException("8120", PKIException.DECRYPT_DES, ex);
        }
    }

    public byte[] encryptUpdate(JHandle handle, Mechanism mechanism, byte[] sourceData) throws PKIException {
        try {
            return handle.getSoftLibHandle().update(sourceData);
        } catch (Exception ex) {
            throw new PKIException("8121", PKIException.DECRYPT_DES, ex);
        }
    }

    public boolean updateKeyPair(Mechanism mechanism, JKey pubKey, JKey prvKey, int hardPos) throws PKIException {
        throw new PKIException("8128195", "PKIERRORNO updateKeyPair() JSoftLib didn't support Stream-Operation yet. ");
    }

    public static byte[] roundup8(BigInteger bigInt) {
        int bytes = (bigInt.bitLength() + 7) / 8;
        byte[] out = new byte[bytes];
        System.arraycopy(bigInt.toByteArray(), bigInt.toByteArray().length - bytes, out, 0, bytes);
        return out;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cn.com.jit.ida.util.pki.cipher.lib.JCaviumLib.doCipher(cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, boolean, java.io.InputStream):byte[]
     arg types: [cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, int, java.io.InputStream]
     candidates:
      cn.com.jit.ida.util.pki.cipher.lib.JCaviumLib.doCipher(cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, boolean, byte[]):byte[]
      cn.com.jit.ida.util.pki.cipher.lib.JCaviumLib.doCipher(cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, boolean, java.io.InputStream):byte[] */
    public int encrypt(Mechanism mechanism, JKey enKey, InputStream sourceData, OutputStream out) throws PKIException {
        try {
            byte[] enc = doCipher(mechanism, enKey, true, sourceData);
            out.write(enc);
            return enc.length;
        } catch (Exception ex) {
            throw new PKIException("8120", PKIException.ENCRYPT_DES, ex);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cn.com.jit.ida.util.pki.cipher.lib.JCaviumLib.doCipher(cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, boolean, java.io.InputStream):byte[]
     arg types: [cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, int, java.io.InputStream]
     candidates:
      cn.com.jit.ida.util.pki.cipher.lib.JCaviumLib.doCipher(cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, boolean, byte[]):byte[]
      cn.com.jit.ida.util.pki.cipher.lib.JCaviumLib.doCipher(cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, boolean, java.io.InputStream):byte[] */
    public int decrypt(Mechanism mechanism, JKey deKey, InputStream encryptedData, OutputStream out) throws PKIException {
        try {
            return doCipher(mechanism, deKey, false, encryptedData).length;
        } catch (Exception ex) {
            throw new PKIException("8121", PKIException.DECRYPT_DES, ex);
        }
    }

    public String getCfgTagName() throws PKIException {
        return this.tag;
    }

    public void setCfgTag(PKIToolConfig cfg) throws PKIException {
        this.CfgTag = cfg;
    }

    public PKIToolConfig getCfgTag() throws PKIException {
        return this.CfgTag;
    }

    public boolean createCertObject(byte[] dn, byte[] certData, byte[] keyId) throws PKIException {
        return false;
    }

    public boolean destroyCertObject(byte[] dn, byte[] keyId) throws PKIException {
        return false;
    }

    public byte[] getCertObject(byte[] keyId) throws PKIException {
        return null;
    }

    public List WrapKeyEnc(JKey pubKey, JKey sysKey, Mechanism pubEncMech, Mechanism sysEncMech, byte[] data) throws PKIException {
        return null;
    }

    public byte[] WrapPriKey(JKey pubKey, JKey sysKey, Mechanism pubEncMech, Mechanism sysEncMech, JKey priKey) throws PKIException {
        return null;
    }
}
