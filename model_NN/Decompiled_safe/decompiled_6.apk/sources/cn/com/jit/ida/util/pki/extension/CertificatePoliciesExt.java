package cn.com.jit.ida.util.pki.extension;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1SequenceExt;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.x509.CertificatePolicies;
import cn.com.jit.ida.util.pki.asn1.x509.X509Extensions;
import java.util.Vector;

public class CertificatePoliciesExt extends AbstractStandardExtension {
    public static final int CONTENT_TYPE_BMPSTRING = 1;
    public static final int CONTENT_TYPE_IA5STRING = 0;
    public static final int CONTENT_TYPE_UTF8STRING = 2;
    public static final int CONTENT_TYPE_VISIBLESTRING = 3;
    Vector seq_Policyinformation = new Vector();

    public CertificatePoliciesExt() {
        this.OID = X509Extensions.CertificatePolicies.getId();
        this.critical = false;
    }

    public CertificatePoliciesExt(String value) {
        this.OID = X509Extensions.CertificatePolicies.getId();
        this.critical = false;
        addPolicy(value);
    }

    public CertificatePoliciesExt(ASN1Sequence asn1Sequence) {
        for (int i = 0; i < asn1Sequence.size(); i++) {
            this.seq_Policyinformation.add(new PolicyInformationExt((ASN1Sequence) asn1Sequence.getObjectAt(i)));
        }
    }

    public int GetPolicyCount() {
        return this.seq_Policyinformation.size();
    }

    public PolicyInformationExt getPolicy(int index) {
        return (PolicyInformationExt) this.seq_Policyinformation.get(index);
    }

    public void addPolicy(String value) {
        this.seq_Policyinformation.add(new PolicyInformationExt(value));
    }

    public String getOID() {
        return this.OID;
    }

    public void setCritical(boolean critical) {
        this.critical = critical;
    }

    public boolean getCritical() {
        return this.critical;
    }

    public byte[] encode() throws PKIException {
        ASN1SequenceExt tempASN1Seq = new ASN1SequenceExt();
        for (int i = 0; i < this.seq_Policyinformation.size(); i++) {
            tempASN1Seq.addObject(((PolicyInformationExt) this.seq_Policyinformation.get(i)).getSeqPolicyInformation());
        }
        return new DEROctetString(new CertificatePolicies(tempASN1Seq).getDERObject()).getOctets();
    }
}
