package cn.com.jit.ida.util.pki;

import cn.com.jit.ida.util.pki.encoders.Hex;
import java.io.ByteArrayInputStream;
import java.math.BigInteger;
import java.security.spec.DSAParameterSpec;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.DSAParameter;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.asn1.x9.X9ObjectIdentifiers;

public class DSAParser {
    public static byte[] softKey2HardCustomKey(byte[] softKey) throws PKIException {
        byte[] P;
        byte[] Q;
        byte[] G;
        try {
            SubjectPublicKeyInfo subjectPublicKeyInfo = new SubjectPublicKeyInfo((ASN1Sequence) new ASN1InputStream(new ByteArrayInputStream(softKey)).readObject());
            byte[] pubParams = subjectPublicKeyInfo.getAlgorithmId().getParameters().getDERObject().getDEREncoded();
            byte[] pubKeyData = subjectPublicKeyInfo.getPublicKeyData().getBytes();
            try {
                DSAParameter dSAParameter = new DSAParameter((ASN1Sequence) new ASN1InputStream(new ByteArrayInputStream(pubParams)).readObject());
                byte[] P2 = dSAParameter.getP().toByteArray();
                byte[] Q2 = dSAParameter.getQ().toByteArray();
                byte[] G2 = dSAParameter.getG().toByteArray();
                if (P2[0] == 0) {
                    byte[] P1 = new byte[(P2.length - 1)];
                    P1[0] = 0;
                    System.arraycopy(P2, 1, P1, 0, P2.length - 1);
                    P = Hex.encode(P1);
                } else {
                    P = Hex.encode(P2);
                }
                if (Q2[0] == 0) {
                    byte[] Q1 = new byte[(Q2.length - 1)];
                    Q1[0] = 0;
                    System.arraycopy(Q2, 1, Q1, 0, Q2.length - 1);
                    Q = Hex.encode(Q1);
                } else {
                    Q = Hex.encode(Q2);
                }
                if (G2[0] == 0) {
                    byte[] G1 = new byte[(G2.length - 1)];
                    G1[0] = 0;
                    System.arraycopy(G2, 1, G1, 0, G2.length - 1);
                    G = Hex.encode(G1);
                } else {
                    G = Hex.encode(G2);
                }
                byte[] pubKeyData2 = Hex.encode(pubKeyData);
                byte[] key = new byte[(P.length + 22 + Q.length + G.length + pubKeyData2.length)];
                System.arraycopy("4".getBytes(), 0, key, 0, "4".getBytes().length);
                byte[] PLen = Integer.toString(P.length).getBytes();
                byte[] QLen = Integer.toString(Q.length).getBytes();
                byte[] GLen = Integer.toString(G.length).getBytes();
                byte[] pubKeyDataLen = Integer.toString(pubKeyData2.length).getBytes();
                System.arraycopy(PLen, 0, key, 2, PLen.length);
                System.arraycopy(P, 0, key, 7, P.length);
                System.arraycopy(QLen, 0, key, P.length + 7, QLen.length);
                System.arraycopy(Q, 0, key, P.length + 12, Q.length);
                System.arraycopy(GLen, 0, key, P.length + 12 + Q.length, GLen.length);
                System.arraycopy(G, 0, key, P.length + 17 + Q.length, G.length);
                System.arraycopy(pubKeyDataLen, 0, key, P.length + 17 + Q.length + G.length, pubKeyDataLen.length);
                System.arraycopy(pubKeyData2, 0, key, P.length + 22 + Q.length + G.length, pubKeyData2.length);
                return key;
            } catch (Exception ex) {
                throw new PKIException("8137", PKIException.BYTES_DEROBJ_DES, ex);
            }
        } catch (Exception ex2) {
            throw new PKIException("8137", PKIException.BYTES_DEROBJ_DES, ex2);
        }
    }

    public static byte[] customData2SoftPublicKey(byte[] DSA_P, byte[] DSA_Q, byte[] DSA_G, byte[] softKey) throws PKIException {
        BigInteger b_DSA_P;
        BigInteger b_DSA_Q;
        BigInteger b_DSA_G;
        if (DSA_P[0] < 0) {
            byte[] DSA_P1 = new byte[(DSA_P.length + 1)];
            DSA_P1[0] = 0;
            System.arraycopy(DSA_P, 0, DSA_P1, 1, DSA_P.length);
            b_DSA_P = new BigInteger(DSA_P1);
        } else {
            b_DSA_P = new BigInteger(DSA_P);
        }
        if (DSA_Q[0] < 0) {
            byte[] DSA_Q1 = new byte[(DSA_Q.length + 1)];
            DSA_Q1[0] = 0;
            System.arraycopy(DSA_Q, 0, DSA_Q1, 1, DSA_Q.length);
            b_DSA_Q = new BigInteger(DSA_Q1);
        } else {
            b_DSA_Q = new BigInteger(DSA_Q);
        }
        if (DSA_G[0] < 0) {
            byte[] DSA_G1 = new byte[(DSA_G.length + 1)];
            DSA_G1[0] = 0;
            System.arraycopy(DSA_G, 0, DSA_G1, 1, DSA_G.length);
            b_DSA_G = new BigInteger(DSA_G1);
        } else {
            b_DSA_G = new BigInteger(DSA_G);
        }
        DSAParameterSpec dsaSpec = new DSAParameterSpec(b_DSA_P, b_DSA_Q, b_DSA_G);
        return new SubjectPublicKeyInfo(new AlgorithmIdentifier(X9ObjectIdentifiers.id_dsa, new DSAParameter(dsaSpec.getP(), dsaSpec.getQ(), dsaSpec.getG()).getDERObject()), softKey).getDEREncoded();
    }

    public static void main(String[] args) {
    }
}
