package cn.com.jit.ida.util.pki.cipher.param;

import java.security.SecureRandom;

public class CBCParam {
    private byte[] iv = new byte[8];

    public CBCParam() {
        new SecureRandom().nextBytes(this.iv);
    }

    public byte[] getIv() {
        return this.iv;
    }

    public void setIv(byte[] iv2) {
        this.iv = iv2;
    }
}
