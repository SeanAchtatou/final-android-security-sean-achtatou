package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject;
import cn.com.jit.ida.util.pki.asn1.DERBitString;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DEREnumerated;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

public class ObjectDigestInfo implements DEREncodable {
    AlgorithmIdentifier digestAlgorithm;
    DEREnumerated digestedObjectType;
    DERBitString objectDigest;
    DERObjectIdentifier otherObjectTypeID;

    public static ObjectDigestInfo getInstance(Object obj) {
        if (obj == null || (obj instanceof ObjectDigestInfo)) {
            return (ObjectDigestInfo) obj;
        }
        if (obj instanceof ASN1Sequence) {
            return new ObjectDigestInfo((ASN1Sequence) obj);
        }
        throw new IllegalArgumentException("illegal object in getInstance: " + obj.getClass().getName());
    }

    public static ObjectDigestInfo getInstance(ASN1TaggedObject obj, boolean explicit) {
        return getInstance(ASN1Sequence.getInstance(obj, explicit));
    }

    public ObjectDigestInfo(ASN1Sequence seq) {
        this.digestedObjectType = DEREnumerated.getInstance(seq.getObjectAt(0));
        int offset = 0;
        if (seq.size() == 4) {
            this.otherObjectTypeID = DERObjectIdentifier.getInstance(seq.getObjectAt(1));
            offset = 0 + 1;
        }
        this.digestAlgorithm = AlgorithmIdentifier.getInstance(seq.getObjectAt(offset + 1));
        this.objectDigest = new DERBitString(seq.getObjectAt(offset + 2));
    }

    public DEREnumerated getDigestedObjectType() {
        return this.digestedObjectType;
    }

    public DERObjectIdentifier getOtherObjectTypeID() {
        return this.otherObjectTypeID;
    }

    public AlgorithmIdentifier getDigestAlgorithm() {
        return this.digestAlgorithm;
    }

    public DERBitString getObjectDigest() {
        return this.objectDigest;
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(this.digestedObjectType);
        if (this.otherObjectTypeID != null) {
            v.add(this.otherObjectTypeID);
        }
        v.add(this.digestAlgorithm);
        v.add(this.objectDigest);
        return new DERSequence(v);
    }

    public ObjectDigestInfo(Node nl) throws PKIException {
        this.otherObjectTypeID = null;
        this.digestAlgorithm = null;
        this.objectDigest = null;
        NodeList nodelist = nl.getChildNodes();
        if (nodelist.getLength() == 0) {
            throw new PKIException("XML内容缺失,ObjectDigestInfo.ObjectDigestInfo(),ObjectDigestInfo没有子节点");
        }
        for (int i = 0; i < nodelist.getLength(); i++) {
            Node cnode = nodelist.item(i);
            String sname = cnode.getNodeName();
            if (sname.equals("otherObjectTypeID")) {
                this.otherObjectTypeID = new DERObjectIdentifier(((Text) cnode.getFirstChild()).getData().trim());
            }
            if (sname.equals("digestAlgorithm")) {
                NodeList nodelist1 = cnode.getChildNodes();
                if (nodelist1.getLength() == 0) {
                    throw new PKIException("XML内容缺失,ObjectDigestInfo.ObjectDigestInfo(),digestAlgorithm没有子节点");
                }
                DERObjectIdentifier oid = null;
                String nameoid = null;
                for (int j = 0; j < nodelist1.getLength(); j++) {
                    Node snode = nodelist1.item(j);
                    String snodename = snode.getNodeName();
                    oid = snodename.equals("OID") ? new DERObjectIdentifier(((Text) snode.getFirstChild()).getData().trim()) : oid;
                    if (snodename.equals("name")) {
                        nameoid = ((Text) snode.getFirstChild()).getData().trim();
                    }
                }
                this.digestAlgorithm = new AlgorithmIdentifier(oid, new DEROctetString(nameoid.getBytes()));
            }
            if (sname.equals("objectDigest")) {
                this.objectDigest = new DERBitString(((Text) cnode.getFirstChild()).getData().getBytes());
            }
        }
    }
}
