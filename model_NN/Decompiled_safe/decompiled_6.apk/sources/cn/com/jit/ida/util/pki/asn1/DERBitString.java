package cn.com.jit.ida.util.pki.asn1;

import android.support.v4.view.MotionEventCompat;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class DERBitString extends DERObject implements DERString {
    private static final char[] table = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    protected byte[] data;
    protected int padBits;

    protected static int getPadBits(int bitString) {
        int val = 0;
        int i = 3;
        while (true) {
            if (i < 0) {
                break;
            }
            if (i != 0) {
                if ((bitString >> (i * 8)) != 0) {
                    val = (bitString >> (i * 8)) & MotionEventCompat.ACTION_MASK;
                    break;
                }
            } else if (bitString != 0) {
                val = bitString & MotionEventCompat.ACTION_MASK;
                break;
            }
            i--;
        }
        if (val == 0) {
            return 7;
        }
        int bits = 1;
        while (true) {
            val <<= 1;
            if ((val & MotionEventCompat.ACTION_MASK) == 0) {
                return 8 - bits;
            }
            bits++;
        }
    }

    protected static byte[] getBytes(int bitString) {
        int bytes = 4;
        int i = 3;
        while (i >= 1 && ((MotionEventCompat.ACTION_MASK << (i * 8)) & bitString) == 0) {
            bytes--;
            i--;
        }
        byte[] result = new byte[bytes];
        for (int i2 = 0; i2 < bytes; i2++) {
            result[i2] = (byte) ((bitString >> (i2 * 8)) & MotionEventCompat.ACTION_MASK);
        }
        return result;
    }

    public static DERBitString getInstance(Object obj) {
        if (obj == null || (obj instanceof DERBitString)) {
            return (DERBitString) obj;
        }
        if (obj instanceof ASN1OctetString) {
            byte[] bytes = ((ASN1OctetString) obj).getOctets();
            byte b = bytes[0];
            byte[] data2 = new byte[(bytes.length - 1)];
            System.arraycopy(bytes, 1, data2, 0, bytes.length - 1);
            return new DERBitString(data2, b);
        } else if (obj instanceof ASN1TaggedObject) {
            return getInstance(((ASN1TaggedObject) obj).getObject());
        } else {
            throw new IllegalArgumentException("illegal object in getInstance: " + obj.getClass().getName());
        }
    }

    public static DERBitString getInstance(ASN1TaggedObject obj, boolean explicit) {
        return getInstance(obj.getObject());
    }

    protected DERBitString(byte data2, int padBits2) {
        this.data = new byte[1];
        this.data[0] = data2;
        this.padBits = padBits2;
    }

    public DERBitString(byte[] data2, int padBits2) {
        this.data = data2;
        this.padBits = padBits2;
    }

    public DERBitString(byte[] data2) {
        this(data2, 0);
    }

    public DERBitString(DEREncodable obj) {
        try {
            ByteArrayOutputStream bOut = new ByteArrayOutputStream();
            DEROutputStream dOut = new DEROutputStream(bOut);
            dOut.writeObject(obj);
            dOut.close();
            this.data = bOut.toByteArray();
            this.padBits = 0;
        } catch (IOException e) {
            throw new IllegalArgumentException("Error processing object : " + e.toString());
        }
    }

    public byte[] getBytes() {
        return this.data;
    }

    public int getPadBits() {
        return this.padBits;
    }

    /* access modifiers changed from: package-private */
    public void encode(DEROutputStream out) throws IOException {
        byte[] bytes = new byte[(getBytes().length + 1)];
        bytes[0] = (byte) getPadBits();
        System.arraycopy(getBytes(), 0, bytes, 1, bytes.length - 1);
        out.writeEncoded(3, bytes);
    }

    public int hashCode() {
        int value = 0;
        for (int i = 0; i != this.data.length; i++) {
            value ^= (this.data[i] & 255) << (i % 4);
        }
        return value;
    }

    public boolean equals(Object o) {
        if (o == null || !(o instanceof DERBitString)) {
            return false;
        }
        DERBitString other = (DERBitString) o;
        if (this.data.length != other.data.length) {
            return false;
        }
        for (int i = 0; i != this.data.length; i++) {
            if (this.data[i] != other.data[i]) {
                return false;
            }
        }
        if (this.padBits == other.padBits) {
            return true;
        }
        return false;
    }

    public String getString() {
        StringBuffer buf = new StringBuffer("#");
        ByteArrayOutputStream bOut = new ByteArrayOutputStream();
        try {
            new ASN1OutputStream(bOut).writeObject(this);
            byte[] string = bOut.toByteArray();
            for (int i = 0; i != string.length; i++) {
                buf.append(table[(string[i] >>> 4) % 15]);
                buf.append(table[string[i] & 15]);
            }
            return buf.toString();
        } catch (IOException e) {
            throw new RuntimeException("internal error encoding BitString");
        }
    }
}
