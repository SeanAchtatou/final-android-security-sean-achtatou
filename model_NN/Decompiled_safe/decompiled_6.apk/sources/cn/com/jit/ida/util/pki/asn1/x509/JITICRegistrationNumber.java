package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.DERPrintableString;

public class JITICRegistrationNumber extends DERPrintableString {
    public JITICRegistrationNumber(byte[] string) {
        super(string);
    }

    public JITICRegistrationNumber(String string) {
        super(string);
    }

    public String getICRegistrationNumber() {
        return getString();
    }
}
