package cn.com.jit.ida.util.pki.asn1;

public interface DERString {
    String getString();
}
