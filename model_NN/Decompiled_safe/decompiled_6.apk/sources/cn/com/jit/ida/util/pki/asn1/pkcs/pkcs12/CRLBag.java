package cn.com.jit.ida.util.pki.asn1.pkcs.pkcs12;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.DERTaggedObject;

public class CRLBag implements DEREncodable {
    private DERObjectIdentifier crlId;
    private DERObject crlValue;

    public static CRLBag getInstance(Object o) {
        if (o == null || (o instanceof CRLBag)) {
            return (CRLBag) o;
        }
        if (o instanceof ASN1Sequence) {
            return new CRLBag((ASN1Sequence) o);
        }
        throw new IllegalArgumentException("unknown object in factory " + o.getClass().getName());
    }

    public CRLBag(ASN1Sequence seq) {
        this.crlId = (DERObjectIdentifier) seq.getObjectAt(0);
        this.crlValue = ((DERTaggedObject) seq.getObjectAt(1)).getObject();
    }

    public CRLBag(DERObjectIdentifier _crlId, DERObject _crlValue) {
        this.crlId = _crlId;
        this.crlValue = _crlValue;
    }

    public DERObjectIdentifier getCrlId() {
        return this.crlId;
    }

    public DERObject getCrlValue() {
        return this.crlValue;
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(this.crlId);
        v.add(new DERTaggedObject(0, this.crlValue));
        return new DERSequence(v);
    }
}
