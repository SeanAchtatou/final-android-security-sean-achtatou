package cn.com.jit.ida.util.pki.cipher.param;

import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.cert.X509Cert;
import cn.com.jit.ida.util.pki.cipher.JKey;
import cn.com.jit.ida.util.pki.cipher.Mechanism;
import cn.com.jit.ida.util.pki.crl.X509CRL;
import java.util.List;

public class P7Param {
    private DERObject[] authData = null;
    private X509Cert[] certs = null;
    private X509CRL[] crls = null;
    private byte[] data = null;
    private List digInfo = null;
    private Mechanism encMech = null;
    private byte[] encedData = null;
    private Mechanism[] envMech = null;
    private JKey prvKey = null;
    private List[] recInfos = null;
    private List[] signInfos = null;
    private Mechanism signMech = null;
    private JKey sysKey = null;
    private DERObject[] unauthData = null;

    public void P7Param() {
    }

    public void SetSignParam(JKey prvKey2, Mechanism signMech2, X509Cert[] certs2, X509CRL[] crls2, DERObject[] authData2, DERObject[] unauthData2) {
        this.prvKey = prvKey2;
        this.signMech = signMech2;
        this.certs = certs2;
        this.crls = crls2;
    }

    public void SetEnvParam(JKey encKey, Mechanism encMech2, X509Cert[] certs2, Mechanism[] envMech2) {
        this.sysKey = encKey;
        this.encMech = encMech2;
        this.certs = certs2;
        this.envMech = envMech2;
    }

    public void SetSignEnvParam(JKey prvKey2, Mechanism signMech2, X509Cert[] certs2, X509CRL[] crls2, DERObject[] authData2, DERObject[] unauthData2, JKey encKey, Mechanism encMech2, Mechanism envMech2) {
        this.prvKey = prvKey2;
        this.signMech = signMech2;
        this.certs = certs2;
        this.crls = crls2;
        this.sysKey = encKey;
        this.encMech = encMech2;
        this.envMech = new Mechanism[]{envMech2};
    }

    public byte[] GetSource() {
        return this.data;
    }

    public X509Cert[] GetCerts() {
        return this.certs;
    }

    public X509CRL[] GetCrls() {
        return this.crls;
    }

    public List[] GetSignInfos() {
        return this.signInfos;
    }

    public List GetdigInfo() {
        return this.digInfo;
    }

    public byte[] GetEncedData() {
        return this.encedData;
    }

    public Mechanism GetEncMech() {
        return this.encMech;
    }

    public List[] GetRecInfos() {
        return this.recInfos;
    }

    public Mechanism[] GetEnvMech() {
        return this.envMech;
    }

    public void SetRecInfos(List[] ls) {
        this.recInfos = ls;
    }

    public void SetEncMech(String mech) {
        this.encMech = new Mechanism(mech);
    }

    public void SetEncedData(byte[] enc) {
        this.encedData = enc;
    }

    public void SetDigInfo(List ls) {
        this.digInfo = ls;
    }

    public void SetSignInfos(List[] ls) {
        this.signInfos = ls;
    }

    public void SetSource(byte[] data2) {
        this.data = data2;
    }

    public void SetCerts(X509Cert[] certs2) {
        this.certs = certs2;
    }

    public void SetCrls(X509CRL[] crls2) {
        this.crls = crls2;
    }

    public JKey GetPrvKey() {
        return this.prvKey;
    }

    public Mechanism GetSignMech() {
        return this.signMech;
    }

    public DERObject[] GetAuthData() {
        return this.authData;
    }

    public DERObject[] GetunAuthData() {
        return this.unauthData;
    }

    public JKey GetEncKey() {
        return this.sysKey;
    }
}
