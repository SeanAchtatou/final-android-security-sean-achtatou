package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1OctetString;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DEREncodableVector;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.DERTaggedObject;
import cn.com.jit.ida.util.pki.asn1.DERUTF8String;
import cn.com.jit.ida.util.pki.encoders.Hex;
import java.util.Enumeration;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

public class IetfAttrSyntax implements DEREncodable {
    public static final int VALUE_OCTETS = 1;
    public static final int VALUE_OID = 2;
    public static final int VALUE_UTF8 = 3;
    GeneralNames policyAuthority = null;
    int valueChoice = -1;
    DEREncodableVector values = new DEREncodableVector();

    public IetfAttrSyntax() {
    }

    public IetfAttrSyntax(ASN1Sequence seq) {
        int i = 0;
        if (seq.getObjectAt(0) instanceof ASN1TaggedObject) {
            this.policyAuthority = GeneralNames.getInstance(seq.getObjectAt(0).getDERObject());
            i = 0 + 1;
        } else if (seq.size() == 2) {
            this.policyAuthority = GeneralNames.getInstance(seq.getObjectAt(0));
            i = 0 + 1;
        }
        if (!(seq.getObjectAt(i) instanceof ASN1Sequence)) {
            throw new IllegalArgumentException("Non-IetfAttrSyntax encoding");
        }
        iniValues((ASN1Sequence) seq.getObjectAt(i));
    }

    private void iniValues(ASN1Sequence seq) {
        Enumeration e = seq.getObjects();
        while (e.hasMoreElements()) {
            addValue((DERObject) e.nextElement());
        }
    }

    public GeneralNames getPolicyAuthority() {
        return this.policyAuthority;
    }

    public int getValueType() {
        return this.valueChoice;
    }

    public Object[] getValues() {
        Object[] objArr;
        if (getValueType() == 1) {
            objArr = new ASN1OctetString[this.values.size()];
            for (int i = 0; i != objArr.length; i++) {
                objArr[i] = (ASN1OctetString) this.values.get(i);
            }
        } else if (getValueType() == 2) {
            objArr = new DERObjectIdentifier[this.values.size()];
            for (int i2 = 0; i2 != objArr.length; i2++) {
                objArr[i2] = (DERObjectIdentifier) this.values.get(i2);
            }
        } else {
            objArr = new DERUTF8String[this.values.size()];
            for (int i3 = 0; i3 != objArr.length; i3++) {
                objArr[i3] = (DERUTF8String) this.values.get(i3);
            }
        }
        return objArr;
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        if (this.policyAuthority != null) {
            v.add(new DERTaggedObject(0, this.policyAuthority.getDERObject()));
        }
        v.add(new DERSequence(this.values));
        return new DERSequence(v);
    }

    public IetfAttrSyntax(GeneralNames policyAuthority2, ASN1Sequence values2) {
        this.policyAuthority = policyAuthority2;
        iniValues(values2);
    }

    public IetfAttrSyntax(GeneralNames policyAuthority2, ASN1EncodableVector values2) {
        this.policyAuthority = policyAuthority2;
        this.values = values2;
    }

    public void setPolicyAuthority(GeneralNames policyAuthority2) {
        this.policyAuthority = policyAuthority2;
    }

    public void setValues(DEREncodableVector values2) {
        this.values = values2;
    }

    public void addValue(DERObject obj) {
        int type;
        if (obj instanceof DERObjectIdentifier) {
            type = 2;
        } else if (obj instanceof DERUTF8String) {
            type = 3;
        } else if (obj instanceof DEROctetString) {
            type = 1;
        } else {
            throw new IllegalArgumentException("Bad value type encoding IetfAttrSyntax");
        }
        if (this.valueChoice < 0) {
            this.valueChoice = type;
        }
        if (type != this.valueChoice) {
            throw new IllegalArgumentException("Mix of value types in IetfAttrSyntax");
        }
        this.values.add(obj);
    }

    public void addUTF8String(String str) {
        if (this.valueChoice < 0) {
            this.valueChoice = 3;
        }
        if (this.valueChoice != 3) {
            throw new IllegalArgumentException("Mix of value types in IetfAttrSyntax");
        }
        this.values.add(new DERUTF8String(str));
    }

    public void addOIDString(String str) {
        if (this.valueChoice < 0) {
            this.valueChoice = 2;
        }
        if (this.valueChoice != 2) {
            throw new IllegalArgumentException("Mix of value types in IetfAttrSyntax");
        }
        this.values.add(new DERObjectIdentifier(str));
    }

    public void addOctectString(byte[] octect) {
        if (this.valueChoice < 0) {
            this.valueChoice = 1;
        }
        if (this.valueChoice != 1) {
            throw new IllegalArgumentException("Mix of value types in IetfAttrSyntax");
        }
        this.values.add(new DEROctetString(octect));
    }

    public void addOctectString(String octectstr) {
        if (this.valueChoice < 0) {
            this.valueChoice = 1;
        }
        if (this.valueChoice != 1) {
            throw new IllegalArgumentException("Mix of value types in IetfAttrSyntax");
        }
        this.values.add(new DEROctetString(Hex.decode(octectstr)));
    }

    public IetfAttrSyntax(Node nl) throws PKIException {
        NodeList nodelist = nl.getChildNodes();
        if (nodelist.getLength() == 0) {
            throw new PKIException("XML内容缺失,IetfAttrSyntax.IetfAttrSyntax(),GroupAttribute没有子节点");
        }
        for (int i = 0; i < nodelist.getLength(); i++) {
            Node cnode = nodelist.item(i);
            String sname = cnode.getNodeName();
            if (sname.equals("PolicyAuthority")) {
                this.policyAuthority = new GeneralNames(cnode);
            }
            if (sname.equals("IetfValue")) {
                this.valueChoice = generalIetfValue(cnode, this.values);
            }
        }
    }

    public int generalIetfValue(Node nl, DEREncodableVector values2) throws PKIException {
        int choice = -1;
        NodeList nodelist = nl.getChildNodes();
        if (nodelist.getLength() == 0) {
            throw new PKIException("XML内容缺失,IetfAttrSyntax.generalIetfValue(),IetfValue没有子节点");
        }
        String type = nl.getAttributes().item(0).getNodeValue();
        if (type.equals("1")) {
            choice = 1;
        }
        if (type.equals("2")) {
            choice = 2;
        }
        if (type.equals("3")) {
            choice = 3;
        }
        if (choice == 1) {
            for (int i = 0; i < nodelist.getLength(); i++) {
                Node cnode = nodelist.item(i);
                if (cnode.getNodeName().equals("value")) {
                    values2.add(new DEROctetString(((Text) cnode.getFirstChild()).getData().trim().getBytes()));
                }
            }
        }
        if (choice == 2) {
            for (int i2 = 0; i2 < nodelist.getLength(); i2++) {
                Node cnode2 = nodelist.item(i2);
                if (cnode2.getNodeName().equals("value")) {
                    values2.add(new DERObjectIdentifier(((Text) cnode2.getFirstChild()).getData().trim()));
                }
            }
        }
        if (choice == 3) {
            for (int i3 = 0; i3 < nodelist.getLength(); i3++) {
                Node cnode3 = nodelist.item(i3);
                if (cnode3.getNodeName().equals("value")) {
                    values2.add(new DERUTF8String(((Text) cnode3.getFirstChild()).getData().trim()));
                }
            }
        }
        return choice;
    }
}
