package cn.com.jit.ida.util.pki.asn1.pkcs.pkcs12;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1Set;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.DERTaggedObject;

public class SafeBag implements DEREncodable {
    private ASN1Set bagAttributes;
    private DERObjectIdentifier bagId;
    private DERObject bagValue;

    public static SafeBag getInstance(Object o) {
        if (o == null || (o instanceof SafeBag)) {
            return (SafeBag) o;
        }
        if (o instanceof ASN1Sequence) {
            return new SafeBag((ASN1Sequence) o);
        }
        throw new IllegalArgumentException("unknown object in factory " + o.getClass().getName());
    }

    public SafeBag(DERObjectIdentifier oid, DERObject obj) {
        this.bagId = oid;
        this.bagValue = obj;
        this.bagAttributes = null;
    }

    public SafeBag(DERObjectIdentifier oid, DERObject obj, ASN1Set bagAttributes2) {
        this.bagId = oid;
        this.bagValue = obj;
        this.bagAttributes = bagAttributes2;
    }

    public SafeBag(ASN1Sequence seq) {
        this.bagId = (DERObjectIdentifier) seq.getObjectAt(0);
        this.bagValue = ((DERTaggedObject) seq.getObjectAt(1)).getObject();
        if (seq.size() == 3) {
            this.bagAttributes = (ASN1Set) seq.getObjectAt(2);
        }
    }

    public DERObjectIdentifier getBagId() {
        return this.bagId;
    }

    public DERObject getBagValue() {
        return this.bagValue;
    }

    public ASN1Set getBagAttributes() {
        return this.bagAttributes;
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(this.bagId);
        v.add(new DERTaggedObject(0, this.bagValue));
        if (this.bagAttributes != null) {
            v.add(this.bagAttributes);
        }
        return new DERSequence(v);
    }
}
