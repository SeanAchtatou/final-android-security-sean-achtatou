package cn.com.jit.ida.util.pki.cms;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.Parser;
import cn.com.jit.ida.util.pki.cipher.JKey;
import cn.com.jit.ida.util.pki.cipher.Mechanism;
import cn.com.jit.ida.util.pki.cipher.Session;
import cn.com.jit.ida.util.pki.cms.CMSSignedGenerator;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.BERConstructedOctetString;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.asn1.cms.CMSAttributes;
import org.bouncycastle.asn1.cms.CMSObjectIdentifiers;
import org.bouncycastle.asn1.cms.ContentInfo;
import org.bouncycastle.asn1.cms.SignedData;
import org.bouncycastle.asn1.cms.SignerIdentifier;
import org.bouncycastle.asn1.cms.SignerInfo;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.cms.CMSAttributeTableGenerator;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSProcessable;
import org.bouncycastle.cms.CMSProcessableByteArray;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.DefaultSignedAttributeTableGenerator;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.cms.SignerInformationStore;
import org.bouncycastle.cms.SimpleAttributeTableGenerator;

public class JITCMSSignedDataGenerator extends CMSSignedGenerator {
    Session session;
    List signerInfs = new ArrayList();

    private class SignerInf {
        private final AttributeTable baseSignedTable;
        private final String digestOID;
        private final String encOID;
        private PrivateKey key = null;
        private JKey prvkey;
        private final CMSAttributeTableGenerator sAttr;
        private final SignerIdentifier signerIdentifier;
        private final CMSAttributeTableGenerator unsAttr;

        SignerInf(JKey key2, SignerIdentifier signerIdentifier2, String digestOID2, String encOID2, CMSAttributeTableGenerator sAttr2, CMSAttributeTableGenerator unsAttr2, AttributeTable baseSignedTable2) {
            this.signerIdentifier = signerIdentifier2;
            this.digestOID = digestOID2;
            this.encOID = encOID2;
            this.sAttr = sAttr2;
            this.unsAttr = unsAttr2;
            this.baseSignedTable = baseSignedTable2;
            this.prvkey = key2;
        }

        /* access modifiers changed from: package-private */
        public AlgorithmIdentifier getDigestAlgorithmID() {
            return new AlgorithmIdentifier(new DERObjectIdentifier(this.digestOID), new DERNull());
        }

        /* access modifiers changed from: package-private */
        public SignerInfo toSignerInfo(DERObjectIdentifier contentType, CMSProcessable content, SecureRandom random, Session session, boolean addDefaultAttributes, boolean isCounterSignature) throws IOException, SignatureException, InvalidKeyException, NoSuchAlgorithmException, CertificateEncodingException, CMSException, PKIException {
            AttributeTable signed;
            byte[] tmp;
            this.key = Parser.convertPrivateKey(this.prvkey);
            AlgorithmIdentifier digAlgId = getDigestAlgorithmID();
            String digestName = CMSSignedHelper.INSTANCE.getDigestAlgName(this.digestOID);
            String signatureName = digestName + "with" + CMSSignedHelper.INSTANCE.getEncryptionAlgName(this.encOID);
            Signature sig = CMSSignedHelper.INSTANCE.getSignatureInstance(signatureName, null);
            MessageDigest dig = CMSSignedHelper.INSTANCE.getDigestInstance(digestName, null);
            AlgorithmIdentifier encAlgId = JITCMSSignedDataGenerator.this.getEncAlgorithmIdentifier(this.encOID, sig);
            if (content != null) {
                content.write(new CMSSignedGenerator.DigOutputStream(dig));
            }
            byte[] hash = dig.digest();
            JITCMSSignedDataGenerator.this._digests.put(this.digestOID, hash.clone());
            if (addDefaultAttributes) {
                signed = this.sAttr != null ? this.sAttr.getAttributes(Collections.unmodifiableMap(JITCMSSignedDataGenerator.this.getBaseParameters(contentType, digAlgId, hash))) : null;
            } else {
                signed = this.baseSignedTable;
            }
            ASN1Set signedAttr = null;
            if (signed != null) {
                if (isCounterSignature) {
                    Hashtable tmpSigned = signed.toHashtable();
                    tmpSigned.remove(CMSAttributes.contentType);
                    signed = new AttributeTable(tmpSigned);
                }
                signedAttr = JITCMSSignedDataGenerator.this.getAttributeSet(signed);
                tmp = signedAttr.getEncoded("DER");
            } else {
                ByteArrayOutputStream bOut = new ByteArrayOutputStream();
                if (content != null) {
                    content.write(bOut);
                }
                tmp = bOut.toByteArray();
            }
            byte[] sigBytes = session.sign(new Mechanism(GetSignMech(digestName, signatureName)), this.prvkey, tmp);
            ASN1Set unsignedAttr = null;
            if (this.unsAttr != null) {
                Map parameters = JITCMSSignedDataGenerator.this.getBaseParameters(contentType, digAlgId, hash);
                parameters.put(CMSAttributeTableGenerator.SIGNATURE, sigBytes.clone());
                unsignedAttr = JITCMSSignedDataGenerator.this.getAttributeSet(this.unsAttr.getAttributes(Collections.unmodifiableMap(parameters)));
            }
            return new SignerInfo(this.signerIdentifier, digAlgId, signedAttr, encAlgId, new DEROctetString(sigBytes), unsignedAttr);
        }

        /* access modifiers changed from: package-private */
        public String GetSignMech(String dig, String signame) {
            String tmp = dig + "with";
            String encmech = signame.substring(tmp.length());
            if (Mechanism.RSA.equals(encmech)) {
                return new String(tmp + "RSAEncryption");
            }
            if (Mechanism.DSA.equals(encmech)) {
                return new String(tmp + Mechanism.DSA);
            }
            if (Mechanism.ECDSA.equals(encmech)) {
                return new String(tmp + Mechanism.ECDSA);
            }
            return null;
        }
    }

    public JITCMSSignedDataGenerator(Session session2) {
        this.session = session2;
    }

    public JITCMSSignedDataGenerator(SecureRandom rand, Session session2) {
        super(rand);
        this.session = session2;
    }

    public void addSigner(JKey key, X509Certificate cert, String digestOID) throws IllegalArgumentException, PKIException {
        addSigner(key, cert, getEncOID(Parser.convertPrivateKey(key), digestOID), digestOID);
    }

    public void addSigner(JKey key, X509Certificate cert, String encryptionOID, String digestOID) throws IllegalArgumentException {
        this.signerInfs.add(new SignerInf(key, getSignerIdentifier(cert), digestOID, encryptionOID, new DefaultSignedAttributeTableGenerator(), null, null));
    }

    public void addSigner(JKey key, byte[] subjectKeyID, String digestOID) throws IllegalArgumentException, PKIException {
        addSigner(key, subjectKeyID, getEncOID(Parser.convertPrivateKey(key), digestOID), digestOID);
    }

    public void addSigner(JKey key, byte[] subjectKeyID, String encryptionOID, String digestOID) throws IllegalArgumentException {
        this.signerInfs.add(new SignerInf(key, getSignerIdentifier(subjectKeyID), digestOID, encryptionOID, new DefaultSignedAttributeTableGenerator(), null, null));
    }

    public void addSigner(JKey key, X509Certificate cert, String digestOID, AttributeTable signedAttr, AttributeTable unsignedAttr) throws IllegalArgumentException, PKIException {
        addSigner(key, cert, getEncOID(Parser.convertPrivateKey(key), digestOID), digestOID, signedAttr, unsignedAttr);
    }

    public void addSigner(JKey key, X509Certificate cert, String encryptionOID, String digestOID, AttributeTable signedAttr, AttributeTable unsignedAttr) throws IllegalArgumentException {
        this.signerInfs.add(new SignerInf(key, getSignerIdentifier(cert), digestOID, encryptionOID, new DefaultSignedAttributeTableGenerator(signedAttr), new SimpleAttributeTableGenerator(unsignedAttr), signedAttr));
    }

    public void addSigner(JKey key, byte[] subjectKeyID, String digestOID, AttributeTable signedAttr, AttributeTable unsignedAttr) throws IllegalArgumentException, PKIException {
        addSigner(key, subjectKeyID, digestOID, getEncOID(Parser.convertPrivateKey(key), digestOID), new DefaultSignedAttributeTableGenerator(signedAttr), new SimpleAttributeTableGenerator(unsignedAttr));
    }

    public void addSigner(JKey key, byte[] subjectKeyID, String encryptionOID, String digestOID, AttributeTable signedAttr, AttributeTable unsignedAttr) throws IllegalArgumentException {
        this.signerInfs.add(new SignerInf(key, getSignerIdentifier(subjectKeyID), digestOID, encryptionOID, new DefaultSignedAttributeTableGenerator(signedAttr), new SimpleAttributeTableGenerator(unsignedAttr), signedAttr));
    }

    public void addSigner(JKey key, X509Certificate cert, String digestOID, CMSAttributeTableGenerator signedAttrGen, CMSAttributeTableGenerator unsignedAttrGen) throws IllegalArgumentException, PKIException {
        addSigner(key, cert, getEncOID(Parser.convertPrivateKey(key), digestOID), digestOID, signedAttrGen, unsignedAttrGen);
    }

    public void addSigner(JKey key, X509Certificate cert, String encryptionOID, String digestOID, CMSAttributeTableGenerator signedAttrGen, CMSAttributeTableGenerator unsignedAttrGen) throws IllegalArgumentException {
        this.signerInfs.add(new SignerInf(key, getSignerIdentifier(cert), digestOID, encryptionOID, signedAttrGen, unsignedAttrGen, null));
    }

    public void addSigner(JKey key, byte[] subjectKeyID, String digestOID, CMSAttributeTableGenerator signedAttrGen, CMSAttributeTableGenerator unsignedAttrGen) throws IllegalArgumentException, PKIException {
        addSigner(key, subjectKeyID, digestOID, getEncOID(Parser.convertPrivateKey(key), digestOID), signedAttrGen, unsignedAttrGen);
    }

    public void addSigner(JKey key, byte[] subjectKeyID, String encryptionOID, String digestOID, CMSAttributeTableGenerator signedAttrGen, CMSAttributeTableGenerator unsignedAttrGen) throws IllegalArgumentException {
        this.signerInfs.add(new SignerInf(key, getSignerIdentifier(subjectKeyID), digestOID, encryptionOID, signedAttrGen, unsignedAttrGen, null));
    }

    public CMSSignedData generate(CMSProcessable content, Session session2) throws NoSuchAlgorithmException, CMSException, PKIException {
        return generate(content, false, session2);
    }

    public CMSSignedData generate(String eContentType, CMSProcessable content, boolean encapsulate, Session session2) throws NoSuchAlgorithmException, CMSException, PKIException {
        return generate(eContentType, content, encapsulate, session2, true);
    }

    public CMSSignedData generate(String eContentType, CMSProcessable content, boolean encapsulate, Session session2, boolean addDefaultAttributes) throws NoSuchAlgorithmException, CMSException, PKIException {
        DERObjectIdentifier contentTypeOID;
        ASN1EncodableVector digestAlgs = new ASN1EncodableVector();
        ASN1EncodableVector signerInfos = new ASN1EncodableVector();
        this._digests.clear();
        for (SignerInformation signer : this._signers) {
            digestAlgs.add(CMSSignedHelper.INSTANCE.fixAlgID(new AlgorithmIdentifier(signer.getDigestAlgOID())));
            signerInfos.add(signer.toSignerInfo());
        }
        boolean isCounterSignature = eContentType == null;
        if (isCounterSignature) {
            contentTypeOID = CMSObjectIdentifiers.data;
        } else {
            contentTypeOID = new DERObjectIdentifier(eContentType);
        }
        for (SignerInf signer2 : this.signerInfs) {
            try {
                digestAlgs.add(signer2.getDigestAlgorithmID());
                signerInfos.add(signer2.toSignerInfo(contentTypeOID, content, this.rand, session2, addDefaultAttributes, isCounterSignature));
            } catch (IOException e) {
                throw new CMSException("encoding error.", e);
            } catch (InvalidKeyException e2) {
                throw new CMSException("key inappropriate for signature.", e2);
            } catch (SignatureException e3) {
                throw new CMSException("error creating signature.", e3);
            } catch (CertificateEncodingException e4) {
                throw new CMSException("error creating sid.", e4);
            }
        }
        ASN1Set certificates = null;
        if (this._certs.size() != 0) {
            certificates = CMSUtils.createBerSetFromList(this._certs);
        }
        ASN1Set certrevlist = null;
        if (this._crls.size() != 0) {
            certrevlist = CMSUtils.createBerSetFromList(this._crls);
        }
        ASN1OctetString octs = null;
        if (encapsulate) {
            ByteArrayOutputStream bOut = new ByteArrayOutputStream();
            if (content != null) {
                try {
                    content.write(bOut);
                } catch (IOException e5) {
                    throw new CMSException("encapsulation error.", e5);
                }
            }
            octs = new BERConstructedOctetString(bOut.toByteArray());
        }
        return new CMSSignedData(content, new ContentInfo(CMSObjectIdentifiers.signedData, new SignedData(new DERSet(digestAlgs), new ContentInfo(contentTypeOID, octs), certificates, certrevlist, new DERSet(signerInfos))));
    }

    public CMSSignedData generate(CMSProcessable content, boolean encapsulate, Session session2) throws NoSuchAlgorithmException, CMSException, PKIException {
        return generate(DATA, content, encapsulate, session2);
    }

    public SignerInformationStore generateCounterSigners(SignerInformation signer, Session session2) throws NoSuchAlgorithmException, CMSException, PKIException {
        return generate(null, new CMSProcessableByteArray(signer.getSignature()), false, session2).getSignerInfos();
    }
}
