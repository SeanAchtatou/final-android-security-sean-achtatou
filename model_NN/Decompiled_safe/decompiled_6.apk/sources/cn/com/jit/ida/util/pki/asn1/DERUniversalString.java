package cn.com.jit.ida.util.pki.asn1;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class DERUniversalString extends DERObject implements DERString {
    private static final char[] table = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    private byte[] string;

    public static DERUniversalString getInstance(Object obj) {
        if (obj == null || (obj instanceof DERUniversalString)) {
            return (DERUniversalString) obj;
        }
        if (obj instanceof ASN1OctetString) {
            return new DERUniversalString(((ASN1OctetString) obj).getOctets());
        }
        throw new IllegalArgumentException("illegal object in getInstance: " + obj.getClass().getName());
    }

    public static DERUniversalString getInstance(ASN1TaggedObject obj, boolean explicit) {
        return getInstance(obj.getObject());
    }

    public DERUniversalString(byte[] string2) {
        this.string = string2;
    }

    public String getString() {
        StringBuffer buf = new StringBuffer("#");
        ByteArrayOutputStream bOut = new ByteArrayOutputStream();
        try {
            new ASN1OutputStream(bOut).writeObject(this);
            byte[] string2 = bOut.toByteArray();
            for (int i = 0; i != string2.length; i++) {
                buf.append(table[(string2[i] >>> 4) % 15]);
                buf.append(table[string2[i] & 15]);
            }
            return buf.toString();
        } catch (IOException e) {
            throw new RuntimeException("internal error encoding BitString");
        }
    }

    public byte[] getOctets() {
        return this.string;
    }

    /* access modifiers changed from: package-private */
    public void encode(DEROutputStream out) throws IOException {
        out.writeEncoded(28, getOctets());
    }

    public boolean equals(Object o) {
        if (o == null || !(o instanceof DERUniversalString)) {
            return false;
        }
        return getString().equals(((DERUniversalString) o).getString());
    }
}
