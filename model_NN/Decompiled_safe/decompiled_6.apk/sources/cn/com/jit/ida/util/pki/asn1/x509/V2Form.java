package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

public class V2Form implements DEREncodable {
    IssuerSerial baseCertificateID;
    GeneralNames issuerName;
    ObjectDigestInfo objectDigestInfo;

    public static V2Form getInstance(ASN1TaggedObject obj, boolean explicit) {
        return getInstance(ASN1Sequence.getInstance(obj, explicit));
    }

    public static V2Form getInstance(Object obj) {
        if (obj == null || (obj instanceof V2Form)) {
            return (V2Form) obj;
        }
        if (obj instanceof ASN1Sequence) {
            return new V2Form((ASN1Sequence) obj);
        }
        throw new IllegalArgumentException("unknown object in factory");
    }

    public V2Form() {
    }

    public V2Form(GeneralNames gns) {
        this.issuerName = gns;
    }

    public V2Form(ASN1Sequence seq) {
        int index = 0;
        if (!(seq.getObjectAt(0) instanceof ASN1TaggedObject)) {
            index = 0 + 1;
            this.issuerName = GeneralNames.getInstance(seq.getObjectAt(0));
        }
        for (int i = index; i != seq.size(); i++) {
            ASN1TaggedObject o = (ASN1TaggedObject) seq.getObjectAt(i);
            if (o.getTagNo() == 0) {
                this.baseCertificateID = IssuerSerial.getInstance(o, false);
            } else if (o.getTagNo() == 1) {
                this.objectDigestInfo = ObjectDigestInfo.getInstance(o, false);
            }
        }
    }

    public GeneralNames getIssuerName() {
        return this.issuerName;
    }

    public IssuerSerial getBaseCertificateID() {
        return this.baseCertificateID;
    }

    public ObjectDigestInfo getObjectDigestInfo() {
        return this.objectDigestInfo;
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        if (this.issuerName != null) {
            v.add(this.issuerName.getDERObject());
        }
        if (this.baseCertificateID != null) {
            v.add(this.baseCertificateID);
        }
        if (this.objectDigestInfo != null) {
            v.add(this.objectDigestInfo);
        }
        return new DERSequence(v);
    }

    public void setIssuerName(GeneralNames issuerName2) {
        this.issuerName = issuerName2;
    }

    public V2Form(Node nl) throws PKIException {
        NodeList nodelist = nl.getChildNodes();
        if (nodelist.getLength() == 0) {
            throw new PKIException("XML内容缺失,V2Form.V2Form(),V2Form没有子节点");
        }
        this.issuerName = new GeneralNames();
        for (int i = 0; i < nodelist.getLength(); i++) {
            Node cnode = nodelist.item(i);
            if (cnode.getNodeName().equals("issuerName")) {
                NodeList nodelist1 = cnode.getChildNodes();
                if (nodelist1.getLength() == 0) {
                    throw new PKIException("XML内容缺失,V2Form.V2Form(),issuerName没有子节点");
                }
                for (int j = 0; j < nodelist1.getLength(); j++) {
                    Node cnode1 = nodelist1.item(j);
                    if (cnode1.getNodeName().equals("GeneralName")) {
                        String type = cnode1.getAttributes().item(0).getNodeValue();
                        try {
                            if (type.equals("4")) {
                                this.issuerName.addDirectoryName(((Text) cnode1.getFirstChild()).getData());
                            }
                            if (type.equals("7")) {
                                this.issuerName.addIPAddress(((Text) cnode1.getFirstChild()).getData());
                            }
                            if (type.equals("2")) {
                                this.issuerName.addDNSName(((Text) cnode1.getFirstChild()).getData());
                            }
                            if (type.equals("6")) {
                                this.issuerName.addUniformResourceIdentifier(((Text) cnode1.getFirstChild()).getData());
                            }
                            if (type.equals("0")) {
                                this.issuerName.addOtherName_UPN(((Text) cnode1.getFirstChild()).getData().trim());
                            }
                        } catch (IllegalArgumentException e) {
                            throw new PKIException("IllegalArgumentException,V2Form.V2Form(),构造issuerName时出错", e);
                        }
                    }
                }
            }
        }
    }
}
