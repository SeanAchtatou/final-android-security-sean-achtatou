package cn.com.jit.ida.util.pki.cipher.softsm;

public class SupportClass {
    public static int URShift(int number, int bits) {
        if (number >= 0) {
            return number >> bits;
        }
        return (number >> bits) + (2 << (bits ^ -1));
    }

    public static int URShift(int number, long bits) {
        return URShift(number, (int) bits);
    }

    public static long URShift(long number, int bits) {
        if (number >= 0) {
            return number >> bits;
        }
        return (number >> bits) + (2 << (bits ^ -1));
    }

    public static long URShift(long number, long bits) {
        return URShift(number, (int) bits);
    }
}
