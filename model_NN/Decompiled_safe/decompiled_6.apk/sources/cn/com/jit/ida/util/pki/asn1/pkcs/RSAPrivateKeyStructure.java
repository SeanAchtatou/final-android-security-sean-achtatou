package cn.com.jit.ida.util.pki.asn1.pkcs;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import java.math.BigInteger;
import java.util.Enumeration;

public class RSAPrivateKeyStructure implements DEREncodable {
    private BigInteger coefficient;
    private BigInteger exponent1;
    private BigInteger exponent2;
    private BigInteger modulus;
    private BigInteger prime1;
    private BigInteger prime2;
    private BigInteger privateExponent;
    private BigInteger publicExponent;

    public RSAPrivateKeyStructure(BigInteger modulus2, BigInteger publicExponent2, BigInteger privateExponent2, BigInteger prime12, BigInteger prime22, BigInteger exponent12, BigInteger exponent22, BigInteger coefficient2) {
        this.modulus = modulus2;
        this.publicExponent = publicExponent2;
        this.privateExponent = privateExponent2;
        this.prime1 = prime12;
        this.prime2 = prime22;
        this.exponent1 = exponent12;
        this.exponent2 = exponent22;
        this.coefficient = coefficient2;
    }

    public RSAPrivateKeyStructure(ASN1Sequence seq) {
        Enumeration e = seq.getObjects();
        if (((DERInteger) e.nextElement()).getValue().intValue() != 0) {
            throw new IllegalArgumentException("wrong version for RSA private key");
        }
        this.modulus = ((DERInteger) e.nextElement()).getValue();
        this.publicExponent = ((DERInteger) e.nextElement()).getValue();
        this.privateExponent = ((DERInteger) e.nextElement()).getValue();
        this.prime1 = ((DERInteger) e.nextElement()).getValue();
        this.prime2 = ((DERInteger) e.nextElement()).getValue();
        this.exponent1 = ((DERInteger) e.nextElement()).getValue();
        this.exponent2 = ((DERInteger) e.nextElement()).getValue();
        this.coefficient = ((DERInteger) e.nextElement()).getValue();
    }

    public BigInteger getModulus() {
        return this.modulus;
    }

    public BigInteger getPublicExponent() {
        return this.publicExponent;
    }

    public BigInteger getPrivateExponent() {
        return this.privateExponent;
    }

    public BigInteger getPrime1() {
        return this.prime1;
    }

    public BigInteger getPrime2() {
        return this.prime2;
    }

    public BigInteger getExponent1() {
        return this.exponent1;
    }

    public BigInteger getExponent2() {
        return this.exponent2;
    }

    public BigInteger getCoefficient() {
        return this.coefficient;
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(new DERInteger(0));
        v.add(new DERInteger(getModulus()));
        v.add(new DERInteger(getPublicExponent()));
        v.add(new DERInteger(getPrivateExponent()));
        v.add(new DERInteger(getPrime1()));
        v.add(new DERInteger(getPrime2()));
        v.add(new DERInteger(getExponent1()));
        v.add(new DERInteger(getExponent2()));
        v.add(new DERInteger(getCoefficient()));
        return new DERSequence(v);
    }
}
