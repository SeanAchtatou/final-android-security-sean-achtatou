package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1OctetString;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.DERTaggedObject;
import cn.com.jit.ida.util.pki.encoders.Hex;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class SecurityCategory implements DEREncodable {
    private DERObjectIdentifier type;
    private ASN1OctetString value;

    public SecurityCategory() {
    }

    public static SecurityCategory getInstance(Object obj) {
        if (obj == null || (obj instanceof SecurityCategory)) {
            return (SecurityCategory) obj;
        }
        if (obj instanceof DERSequence) {
            return new SecurityCategory((DERSequence) obj);
        }
        throw new IllegalArgumentException("illegal object in getInstance: " + obj.getClass().getName());
    }

    public SecurityCategory(DERSequence seq) {
        this.type = (DERObjectIdentifier) ((DERTaggedObject) seq.getObjectAt(0)).getObject();
        this.value = (ASN1OctetString) seq.getObjectAt(1);
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(new DERTaggedObject(false, 0, this.type));
        v.add(new DERTaggedObject(true, 0, this.value));
        return new DERSequence(v);
    }

    public SecurityCategory(DERObjectIdentifier type2, ASN1OctetString value2) {
        this.type = type2;
        this.value = value2;
    }

    public DERObjectIdentifier getType() {
        return this.type;
    }

    public void setType(DERObjectIdentifier type2) {
        this.type = type2;
    }

    public void setType(String oid) {
        this.type = new DERObjectIdentifier(oid);
    }

    public ASN1OctetString getValue() {
        return this.value;
    }

    public void setValue(ASN1OctetString value2) {
        this.value = value2;
    }

    public void setValue(String value2) {
        this.value = new DEROctetString(Hex.decode(value2));
    }

    public void setValue(byte[] value2) {
        this.value = new DEROctetString(value2);
    }

    public SecurityCategory(Node nl) throws PKIException {
        NodeList nodelist = nl.getChildNodes();
        if (nodelist.getLength() == 0) {
            throw new PKIException("XML内容缺失,SecurityCategory.SecurityCategory(),SecurityCategory没有子节点");
        }
        for (int i = 0; i < nodelist.getLength(); i++) {
            Node cnode = nodelist.item(i);
            String sname = cnode.getNodeName();
            if (sname.equals("SecType")) {
                this.type = new DERObjectIdentifier(cnode);
            }
            if (sname.equals("SecValue")) {
                this.value = new DEROctetString(cnode);
            }
        }
    }
}
