package cn.com.jit.ida.util.pki.extension;

import cn.com.jit.ida.util.pki.PKIException;

public abstract class AbstractSelfDefExtension implements Extension {
    public static final String BOOLEAN = "Boolean";
    public static final String IA5STRING = "IA5 String";
    public static final String INTEGER = "Integer";
    public static final String PRINTABLESTRING = "Printable String";
    public static final String USERDEFINED = "User Defined";
    public static final String UTF8STRING = "UTF-8 String";
    protected String OID = null;
    protected boolean critical = false;

    public abstract String getEncoding();

    public abstract String getExtensionValue();

    public abstract void setEncoding(String str) throws PKIException;

    public abstract void setExtensionValue(String str) throws PKIException;

    public abstract void setOID(String str) throws PKIException;
}
