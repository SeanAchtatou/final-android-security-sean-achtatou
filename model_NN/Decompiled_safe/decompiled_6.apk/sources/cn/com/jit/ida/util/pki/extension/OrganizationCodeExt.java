package cn.com.jit.ida.util.pki.extension;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.DERPrintableString;
import cn.com.jit.ida.util.pki.asn1.x509.JITOrganizationCode;
import cn.com.jit.ida.util.pki.asn1.x509.X509Extensions;

public class OrganizationCodeExt extends AbstractStandardExtension {
    private String organizationcode = null;

    public OrganizationCodeExt() {
        this.OID = X509Extensions.JIT_OrganizationCode.getId();
        this.critical = false;
    }

    public OrganizationCodeExt(DERPrintableString obj) {
        this.organizationcode = obj.getString();
    }

    public OrganizationCodeExt(String value) {
        this.OID = X509Extensions.JIT_OrganizationCode.getId();
        this.critical = false;
        this.organizationcode = value;
    }

    public void SetOrganizationCode(String value) {
        this.organizationcode = value;
    }

    public String GetOrganizationCode() {
        return this.organizationcode;
    }

    public byte[] encode() throws PKIException {
        if (this.organizationcode != null) {
            return new DEROctetString(new JITOrganizationCode(this.organizationcode).getDERObject()).getOctets();
        }
        throw new PKIException(PKIException.EXT_ENCODING_DATA_NULL, PKIException.EXT_ENCODING_DATA_NULL_DES);
    }

    public boolean getCritical() {
        return this.critical;
    }

    public String getOID() {
        return this.OID;
    }

    public void setCritical(boolean critical) {
        this.critical = critical;
    }
}
