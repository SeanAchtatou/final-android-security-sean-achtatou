package cn.com.jit.ida.util.pki.cipher.softsm;

import java.math.BigInteger;
import org.bouncycastle.math.ec.ECPoint;

public class SM2Result {
    public BigInteger R;
    public ECPoint keyra;
    public ECPoint keyrb;
    public BigInteger r;
    public BigInteger s;
    public byte[] s1;
    public byte[] s2;
    public byte[] sa;
    public byte[] sb;
}
