package cn.com.jit.ida.util.pki.cipher.lib;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.PKIToolConfig;
import cn.com.jit.ida.util.pki.Parser;
import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1InputStream;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.BERSequence;
import cn.com.jit.ida.util.pki.asn1.DERBitString;
import cn.com.jit.ida.util.pki.asn1.DERNull;
import cn.com.jit.ida.util.pki.asn1.pkcs.PKCSObjectIdentifiers;
import cn.com.jit.ida.util.pki.asn1.x509.AlgorithmIdentifier;
import cn.com.jit.ida.util.pki.cipher.JCrypto;
import cn.com.jit.ida.util.pki.cipher.JHandle;
import cn.com.jit.ida.util.pki.cipher.JKey;
import cn.com.jit.ida.util.pki.cipher.JKeyPair;
import cn.com.jit.ida.util.pki.cipher.Mechanism;
import cn.com.jit.ida.util.pki.cipher.Session;
import cn.com.jit.ida.util.pki.cipher.param.CBCParam;
import cn.com.jit.ida.util.pki.cipher.param.PBEParam;
import cn.com.jit.ida.util.pki.cipher.softsm.Cipher;
import cn.com.jit.ida.util.pki.cipher.softsm.SM2;
import cn.com.jit.ida.util.pki.cipher.softsm.SM2Result;
import cn.com.jit.ida.util.pki.cipher.softsm.SM3Digest;
import cn.com.jit.ida.util.pki.cipher.softsm.SM4;
import cn.com.jit.ida.util.pki.cipher.softsm.Sm4_Context;
import cn.com.jit.ida.util.pki.cipher.softsm.Util;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.ArrayList;
import java.util.List;
import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEParameterSpec;
import org.bouncycastle.crypto.AsymmetricBlockCipher;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.engines.RSAEngine;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.params.RSAPrivateCrtKeyParameters;
import org.bouncycastle.jce.provider.JCERSAPrivateCrtKey;
import org.bouncycastle.jce.provider.JCERSAPublicKey;
import org.bouncycastle.math.ec.ECPoint;

public class JSoftLib implements Session {
    public static final String PROVIDER = "BC";
    private PKIToolConfig CfgTag = null;
    private String tag = JCrypto.JSOFT_LIB;

    public JSoftLib() {
        if (0 != 0) {
            System.out.println("FATAL ERROR : Failed to load native Cavium Driver ");
        }
    }

    public byte[] digest(Mechanism mechanism, byte[] sourceData) throws PKIException {
        String mType = mechanism.getMechanismType();
        if (!mechanism.isDigestabled() && !mType.equals(Mechanism.SM3)) {
            throw new PKIException("8122", "文摘操作失败 本操作不支持此种机制类型 " + mType);
        } else if (mType.equals(Mechanism.SM3)) {
            SM3Digest sm31 = new SM3Digest();
            byte[] md1 = new byte[32];
            sm31.BlockUpdate(sourceData, 0, sourceData.length);
            sm31.doFinal(md1, 0);
            return md1;
        } else {
            try {
                MessageDigest m = MessageDigest.getInstance(mType, "BC");
                m.update(sourceData);
                return m.digest();
            } catch (Exception ex) {
                throw new PKIException("8122", PKIException.DIGEST_DES, ex);
            }
        }
    }

    public byte[] mac(Mechanism mechanism, JKey key, byte[] sourceData) throws PKIException {
        String mType = mechanism.getMechanismType();
        if (mType.equals(Mechanism.HMAC_MD2) || mType.equals(Mechanism.HMAC_MD5) || mType.equals(Mechanism.HMAC_SHA1)) {
            try {
                Mac mac = Mac.getInstance(mechanism.getMechanismType(), "BC");
                mac.init(Parser.convertSecretKey(key));
                mac.update(sourceData);
                return mac.doFinal();
            } catch (Exception ex) {
                throw new PKIException("8123", PKIException.MAC_DES, ex);
            }
        } else {
            throw new PKIException("8123", "MAC操作失败 本操作不支持此种机制类型 " + mType);
        }
    }

    public boolean verifyMac(Mechanism mechanism, JKey key, byte[] sourceData, byte[] macData) throws PKIException {
        String mType = mechanism.getMechanismType();
        if (mType.equals(Mechanism.HMAC_MD2) || mType.equals(Mechanism.HMAC_MD5) || mType.equals(Mechanism.HMAC_SHA1)) {
            try {
                return Parser.isEqualArray(mac(mechanism, key, sourceData), macData);
            } catch (Exception ex) {
                throw new PKIException("8124", PKIException.VERIFY_MAC_DES, ex);
            }
        } else {
            throw new PKIException("8124", "验证MAC操作失败 本操作不支持此种机制类型 " + mType);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cn.com.jit.ida.util.pki.cipher.lib.JSoftLib.doCipher(cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, boolean, byte[]):byte[]
     arg types: [cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, int, byte[]]
     candidates:
      cn.com.jit.ida.util.pki.cipher.lib.JSoftLib.doCipher(cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, boolean, java.io.InputStream):byte[]
      cn.com.jit.ida.util.pki.cipher.lib.JSoftLib.doCipher(cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, boolean, byte[]):byte[] */
    public byte[] sign(Mechanism mechanism, JKey prvKey, byte[] sourceData) throws PKIException {
        String mType = mechanism.getMechanismType();
        if (!mechanism.isSignabled() && !mType.equals("SM3withSM2Encryption")) {
            throw new PKIException("8125", "签名操作失败 本操作不支持此种机制类型 " + mType);
        } else if (mType.equals(Mechanism.RSA_PKCS)) {
            try {
                return doCipher(mechanism, prvKey, true, sourceData);
            } catch (Exception ex) {
                throw new PKIException("8125", PKIException.SIGN_DES, ex);
            }
        } else if (mType.equals("SM3withSM2Encryption")) {
            SM2 sm2 = SM2.Instance();
            new Mechanism(Mechanism.SM3);
            try {
                BigInteger test_d = Util.hardKey2SoftPrivKey(prvKey);
                ECPoint test_p = sm2.ecc_point_g.multiply(test_d);
                byte[] md = getDigforSign(sourceData, test_p);
                SM2Result sm2Ret = new SM2Result();
                sm2.Sm2Sign(md, test_d, test_p, sm2Ret);
                return Util.soft2HardSignData(sm2Ret.r, sm2Ret.s);
            } catch (Exception e) {
                throw new PKIException("8125", PKIException.SIGN_DES, e);
            }
        } else {
            try {
                Signature signature = Signature.getInstance(mType, "BC");
                signature.initSign(Parser.convertPrivateKey(prvKey));
                signature.update(sourceData);
                return signature.sign();
            } catch (Exception ex2) {
                throw new PKIException("8125", PKIException.SIGN_DES, ex2);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cn.com.jit.ida.util.pki.cipher.lib.JSoftLib.doCipher(cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, boolean, byte[]):byte[]
     arg types: [cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, int, byte[]]
     candidates:
      cn.com.jit.ida.util.pki.cipher.lib.JSoftLib.doCipher(cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, boolean, java.io.InputStream):byte[]
      cn.com.jit.ida.util.pki.cipher.lib.JSoftLib.doCipher(cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, boolean, byte[]):byte[] */
    public boolean verifySign(Mechanism mechanism, JKey pubKey, byte[] sourceData, byte[] signData) throws PKIException {
        String mType = mechanism.getMechanismType();
        if (!mechanism.isSignabled() && !mType.equals("SM3withSM2Encryption")) {
            throw new PKIException("8126", "验证签名操作失败 本操作不支持此种机制类型 " + mType);
        } else if (mType.equals(Mechanism.RSA_PKCS)) {
            try {
                return isEqualArray(doCipher(mechanism, pubKey, false, signData), sourceData);
            } catch (Exception ex) {
                throw new PKIException("8125", PKIException.SIGN_DES, ex);
            }
        } else if (mType.equals("SM3withSM2Encryption")) {
            try {
                SM2 sm2 = SM2.Instance();
                ECPoint softpub = sm2.ecc_curve.decodePoint(Util.hardKey2SoftPubKey(pubKey));
                new Mechanism(Mechanism.SM3);
                byte[] md = getDigforSign(sourceData, softpub);
                SM2Result sm2Ret = Util.Hard2softSignData(signData);
                sm2.Sm2Verify(md, softpub, sm2Ret.r, sm2Ret.s, sm2Ret);
                return sm2Ret.r.equals(sm2Ret.R);
            } catch (Exception ex2) {
                throw new PKIException("8126", PKIException.VERIFY_SIGN_DES, ex2);
            }
        } else {
            try {
                Signature signature = Signature.getInstance(mechanism.getMechanismType(), "BC");
                signature.initVerify(Parser.convertPublicKey(pubKey));
                signature.update(sourceData);
                return signature.verify(signData);
            } catch (Exception ex3) {
                throw new PKIException("8126", PKIException.VERIFY_SIGN_DES, ex3);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cn.com.jit.ida.util.pki.cipher.lib.JSoftLib.doCipher(cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, boolean, byte[]):byte[]
     arg types: [cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, int, byte[]]
     candidates:
      cn.com.jit.ida.util.pki.cipher.lib.JSoftLib.doCipher(cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, boolean, java.io.InputStream):byte[]
      cn.com.jit.ida.util.pki.cipher.lib.JSoftLib.doCipher(cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, boolean, byte[]):byte[] */
    public byte[] encrypt(Mechanism mechanism, JKey enKey, byte[] sourceData) throws PKIException {
        String mType = mechanism.getMechanismType();
        if (mType.equals(Mechanism.SM2_RAW)) {
            byte[] source = new byte[sourceData.length];
            System.arraycopy(sourceData, 0, source, 0, sourceData.length);
            if (!enKey.getKeyType().equals(JKey.SM2_PUB_KEY)) {
                throw new PKIException("8120", PKIException.ENCRYPT_DES);
            }
            try {
                Cipher cipher = new Cipher();
                SM2 sm2 = SM2.Instance();
                ECPoint c1 = cipher.Init_enc(sm2, sm2.ecc_curve.decodePoint(Util.hardKey2SoftPubKey(enKey)));
                byte[] encoded = c1.getEncoded();
                cipher.Encrypt(source);
                byte[] c3 = new byte[32];
                cipher.Dofinal(c3);
                return Util.encedDataEncode(c1, c3, source);
            } catch (Exception ex) {
                throw new PKIException("8120", PKIException.ENCRYPT_DES, ex);
            }
        } else if (mType.equals(Mechanism.SM4_ECB) || mType.equals(Mechanism.SM4_CBC)) {
            Sm4_Context ctx = new Sm4_Context();
            SM4 sm4 = new SM4();
            ctx.isPadding = mechanism.isPad();
            try {
                sm4.sm4_setkey_enc(ctx, enKey.getKey());
                if (mType.equals(Mechanism.SM4_ECB)) {
                    return sm4.sm4_crypt_ecb(ctx, sourceData);
                }
                byte[] iv = null;
                CBCParam cbc = (CBCParam) mechanism.getParam();
                if (cbc != null) {
                    iv = cbc.getIv();
                }
                return sm4.sm4_crypt_cbc(ctx, iv, sourceData);
            } catch (Exception ex2) {
                throw new PKIException("8120", PKIException.ENCRYPT_DES, ex2);
            }
        } else {
            try {
                return doCipher(mechanism, enKey, true, sourceData);
            } catch (Exception ex3) {
                throw new PKIException("8120", PKIException.ENCRYPT_DES, ex3);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cn.com.jit.ida.util.pki.cipher.lib.JSoftLib.doCipher(cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, boolean, byte[]):byte[]
     arg types: [cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, int, byte[]]
     candidates:
      cn.com.jit.ida.util.pki.cipher.lib.JSoftLib.doCipher(cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, boolean, java.io.InputStream):byte[]
      cn.com.jit.ida.util.pki.cipher.lib.JSoftLib.doCipher(cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, boolean, byte[]):byte[] */
    public byte[] decrypt(Mechanism mechanism, JKey deKey, byte[] encryptedData) throws PKIException {
        byte[] output;
        String mType = mechanism.getMechanismType();
        if (mType.equals(Mechanism.SM2_RAW)) {
            if (!deKey.getKeyType().equals(JKey.SM2_PRV_KEY)) {
                throw new PKIException("8120", PKIException.ENCRYPT_DES);
            }
            try {
                byte[] enc = new byte[encryptedData.length];
                System.arraycopy(encryptedData, 0, enc, 0, encryptedData.length);
                Cipher cipher = new Cipher();
                SM2 sm2 = SM2.Instance();
                BigInteger prv = Util.getPrivKeyD(deKey);
                List ls = Util.getXY(enc);
                cipher.Init_dec(prv, sm2.ecc_curve.createPoint((BigInteger) ls.get(0), (BigInteger) ls.get(1), true));
                byte[] enc2 = Util.getEnc(enc);
                cipher.Decrypt(enc2);
                cipher.Dofinal(new byte[32]);
                return enc2;
            } catch (Exception ex) {
                throw new PKIException("8120", PKIException.ENCRYPT_DES, ex);
            }
        } else if (mType.equals(Mechanism.SM4_ECB) || mType.equals(Mechanism.SM4_CBC)) {
            Sm4_Context ctx = new Sm4_Context();
            SM4 sm4 = new SM4();
            ctx.isPadding = mechanism.isPad();
            try {
                sm4.sm4_setkey_dec(ctx, deKey.getKey());
                if (mType.equals(Mechanism.SM4_ECB)) {
                    output = sm4.sm4_crypt_ecb(ctx, encryptedData);
                } else {
                    byte[] iv = null;
                    CBCParam cbc = (CBCParam) mechanism.getParam();
                    if (cbc != null) {
                        iv = cbc.getIv();
                    }
                    output = sm4.sm4_crypt_cbc(ctx, iv, encryptedData);
                }
                return output;
            } catch (Exception ex2) {
                throw new PKIException("8120", PKIException.ENCRYPT_DES, ex2);
            }
        } else {
            try {
                return doCipher(mechanism, deKey, false, encryptedData);
            } catch (Exception ex3) {
                throw new PKIException("8121", PKIException.DECRYPT_DES, ex3);
            }
        }
    }

    public JKey generateKey(Mechanism mechanism, int keyLength) throws PKIException {
        String mType = mechanism.getMechanismType();
        if (mType.equals("DES") || mType.equals("DESede") || mType.equals("RC2") || mType.equals("RC4") || mType.equals("CAST5") || mType.equals("IDEA") || mType.equals("SM4") || mType.equals("AES")) {
            try {
                if (mType.equals("SM4")) {
                    return new JKey("SM4", generateRandom(new Mechanism(Mechanism.RANDOM), 16));
                }
                KeyGenerator keyGen = KeyGenerator.getInstance(mechanism.getMechanismType(), "BC");
                keyGen.init(keyLength);
                SecretKey key = keyGen.generateKey();
                return new JKey(key.getAlgorithm(), key.getEncoded());
            } catch (Exception ex) {
                throw new PKIException("8110", PKIException.SYM_KEY_DES, ex);
            }
        } else {
            throw new PKIException("8110", "产生对称密钥操作失败 本操作不支持此种机制类型 " + mType);
        }
    }

    public JKeyPair generateKeyPair(Mechanism mechanism, int keyLength) throws PKIException {
        JKey jPubKey;
        String mType = mechanism.getMechanismType();
        if (mechanism.isGenerateKeyPairabled() || mType.equals(Mechanism.SM2)) {
            JKey jPubKey2 = null;
            JKey jPrvKey = null;
            try {
                if (mType.equals(Mechanism.SM2)) {
                    AsymmetricCipherKeyPair keypair = SM2.Instance().ecc_key_pair_generator.generateKeyPair();
                    return Util.getSM2Key(keypair.getPublic(), keypair.getPrivate());
                }
                KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance(mType, "BC");
                keyPairGen.initialize(keyLength, new SecureRandom());
                KeyPair keyPair = keyPairGen.generateKeyPair();
                PublicKey pubKey = keyPair.getPublic();
                PrivateKey prvKey = keyPair.getPrivate();
                byte[] pubKeyEncoded = pubKey.getEncoded();
                byte[] prvKeyEncoded = prvKey.getEncoded();
                if (mechanism.getMechanismType().equals(Mechanism.RSA)) {
                    jPubKey = new JKey(JKey.RSA_PUB_KEY, pubKeyEncoded);
                    try {
                        jPrvKey = new JKey(JKey.RSA_PRV_KEY, prvKeyEncoded);
                        jPubKey2 = jPubKey;
                    } catch (Exception e) {
                        ex = e;
                        throw new PKIException("8111", PKIException.KEY_PAIR_DES, ex);
                    }
                } else if (mechanism.getMechanismType().equals(Mechanism.DSA)) {
                    jPubKey = new JKey(JKey.DSA_PUB_KEY, pubKeyEncoded);
                    jPrvKey = new JKey(JKey.DSA_PRV_KEY, prvKeyEncoded);
                    jPubKey2 = jPubKey;
                } else if (mechanism.getMechanismType().equals(Mechanism.ECDSA)) {
                    jPubKey = new JKey(JKey.ECDSA_PUB_KEY, pubKeyEncoded);
                    jPrvKey = new JKey(JKey.ECDSA_PRV_KEY, prvKeyEncoded);
                    jPubKey2 = jPubKey;
                } else if (mechanism.getMechanismType().equals(Mechanism.ECIES)) {
                    jPubKey = new JKey(JKey.ECIES_PUB_KEY, pubKeyEncoded);
                    jPrvKey = new JKey(JKey.ECIES_PRV_KEY, prvKeyEncoded);
                    jPubKey2 = jPubKey;
                }
                return new JKeyPair(jPubKey2, jPrvKey);
            } catch (Exception e2) {
                ex = e2;
            }
        } else {
            throw new PKIException("8111", "产生非对称密钥对失败 本操作不支持此种机制类型 " + mType);
        }
    }

    public boolean DestroyKeyPair(Mechanism mechanism) throws PKIException {
        throw new UnsupportedOperationException("Method DestroyKeyPair() not yet implemented in JSOFT_LIB.");
    }

    public JKey generatePBEKey(Mechanism mechanism, char[] password) throws PKIException {
        String mType = mechanism.getMechanismType();
        if (!mType.equals("PBEWithMD5AndDES") && !mType.equals("PBEWITHSHAAND2-KEYTRIPLEDES-CBC") && !mType.equals("PBEWITHSHAAND3-KEYTRIPLEDES-CBC")) {
            if (mType.equalsIgnoreCase("PBE/PKCS5")) {
                mType = "PBEWithMD5AndDES";
            } else {
                throw new PKIException("8112", "产生PBE密钥失败 本操作不支持此种机制类型 " + mType);
            }
        }
        try {
            byte[] keyData = new String(password).getBytes();
            if (mType.equals("PBEWithMD5AndDES")) {
                return new JKey("PBEWithMD5AndDES", keyData);
            }
            if (mType.equals("PBEWITHSHAAND2-KEYTRIPLEDES-CBC")) {
                return new JKey("PBEWITHSHAAND2-KEYTRIPLEDES-CBC", keyData);
            }
            return new JKey("PBEWITHSHAAND3-KEYTRIPLEDES-CBC", keyData);
        } catch (Exception ex) {
            throw new PKIException("8112", PKIException.PBE_KEY_DES, ex);
        }
    }

    public byte[] generateRandom(Mechanism mechanism, int length) throws PKIException {
        String mType = mechanism.getMechanismType();
        if (!mechanism.getMechanismType().equals(Mechanism.RANDOM)) {
            throw new PKIException("8113", "产生随机数失败 本操作不支持此种机制类型 " + mType);
        }
        byte[] data = new byte[length];
        new SecureRandom().nextBytes(data);
        return data;
    }

    private byte[] doCipher(Mechanism mechanism, JKey jkey, boolean isEncrypt, byte[] data) throws Exception {
        int cipherMode;
        String mType = mechanism.getMechanismType();
        int rsaKeyLen = -1;
        if (mType.equalsIgnoreCase(Mechanism.RSA_PKCS)) {
            if (jkey.getKeyType().equals(JKey.RSA_PUB_KEY)) {
                rsaKeyLen = ((RSAPublicKey) Parser.convertPublicKey(jkey)).getModulus().bitLength();
            } else if (jkey.getKeyType().equals(JKey.RSA_PRV_KEY)) {
                rsaKeyLen = ((RSAPrivateKey) Parser.convertPrivateKey(jkey)).getModulus().bitLength();
            }
            if (rsaKeyLen > 2048) {
                return doCipher_RSA_ext(mechanism, jkey, isEncrypt, data);
            }
        }
        javax.crypto.Cipher cipher = javax.crypto.Cipher.getInstance(mType, "BC");
        if (isEncrypt) {
            cipherMode = 1;
        } else {
            cipherMode = 2;
        }
        if (mType.indexOf("PBE") != -1) {
            PBEParam pbeParam = (PBEParam) mechanism.getParam();
            if (pbeParam == null) {
                throw new PKIException("PBE参数为空");
            }
            cipher.init(cipherMode, Parser.convertKey(jkey), new PBEParameterSpec(pbeParam.getSalt(), pbeParam.getIterations()));
        } else if (mType.indexOf("CBC") != -1) {
            CBCParam cbcParam = (CBCParam) mechanism.getParam();
            if (cbcParam == null) {
                throw new PKIException("CBC参数为空");
            }
            cipher.init(cipherMode, Parser.convertKey(jkey), new IvParameterSpec(cbcParam.getIv()));
        } else {
            cipher.init(cipherMode, Parser.convertKey(jkey));
        }
        return cipher.doFinal(data);
    }

    public static void main(String[] args) {
        System.out.println("OK");
        try {
            JCrypto jcrypto = JCrypto.getInstance();
            jcrypto.initialize(JCrypto.JSOFT_LIB, null);
            Session session = jcrypto.openSession(JCrypto.JSOFT_LIB);
            Mechanism keyGen = new Mechanism("PBEWITHSHAAND3-KEYTRIPLEDES-CBC");
            Mechanism mechanism = new Mechanism("PBEWITHSHAAND3-KEYTRIPLEDES-CBC", new PBEParam());
            JKey key = session.generatePBEKey(keyGen, "HELLO".toCharArray());
            byte[] enData = session.encrypt(mechanism, key, "JIT公司测试".getBytes());
            System.out.println(new String(enData));
            System.out.println(new String(session.decrypt(mechanism, key, enData)));
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
    }

    private byte[] doCipher_RSA_ext(Mechanism mechanism, JKey jkey, boolean isEncrypt, byte[] data) throws Exception {
        RSAKeyParameters keyParams;
        AsymmetricBlockCipher eng = new RSAEngine();
        if (jkey.getKeyType().equals(JKey.RSA_PUB_KEY)) {
            JCERSAPublicKey pubKey = Parser.convertPublicKey(jkey);
            keyParams = new RSAKeyParameters(false, pubKey.getModulus(), pubKey.getPublicExponent());
        } else {
            JCERSAPrivateCrtKey prvKey = Parser.convertPrivateKey(jkey);
            keyParams = new RSAPrivateCrtKeyParameters(prvKey.getModulus(), prvKey.getPublicExponent(), prvKey.getPrivateExponent(), prvKey.getPrimeP(), prvKey.getPrimeQ(), prvKey.getPrimeExponentP(), prvKey.getPrimeExponentQ(), prvKey.getCrtCoefficient());
        }
        eng.init(isEncrypt, keyParams);
        return eng.processBlock(data, 0, data.length);
    }

    public byte[] digest(Mechanism mechanism, InputStream sourceData) throws PKIException {
        String mType = mechanism.getMechanismType();
        if (!mechanism.isDigestabled()) {
            throw new PKIException("8122", "文摘操作失败 本操作不支持此种机制类型 " + mType);
        } else if (mType.equals(Mechanism.SM3)) {
            try {
                SM3Digest sm31 = new SM3Digest();
                byte[] md1 = new byte[32];
                byte[] buffer = new byte[1024];
                while (sourceData.read(buffer) > 0) {
                    sm31.BlockUpdate(buffer, 0, buffer.length);
                }
                sm31.doFinal(md1, 0);
                return md1;
            } catch (Exception ex) {
                throw new PKIException("8122", PKIException.DIGEST_DES, ex);
            }
        } else {
            try {
                MessageDigest m = MessageDigest.getInstance(mType, "BC");
                byte[] buffer2 = new byte[1024];
                while (true) {
                    int i = sourceData.read(buffer2);
                    if (i <= 0) {
                        return m.digest();
                    }
                    m.update(buffer2, 0, i);
                }
            } catch (Exception ex2) {
                throw new PKIException("8122", PKIException.DIGEST_DES, ex2);
            }
        }
    }

    public byte[] mac(Mechanism mechanism, JKey key, InputStream sourceData) throws PKIException {
        String mType = mechanism.getMechanismType();
        if (mType.equals(Mechanism.HMAC_MD2) || mType.equals(Mechanism.HMAC_MD5) || mType.equals(Mechanism.HMAC_SHA1)) {
            try {
                Mac mac = Mac.getInstance(mechanism.getMechanismType(), "BC");
                mac.init(Parser.convertSecretKey(key));
                byte[] buffer = new byte[1024];
                while (true) {
                    int i = sourceData.read(buffer);
                    if (i <= 0) {
                        return mac.doFinal();
                    }
                    mac.update(buffer, 0, i);
                }
            } catch (Exception ex) {
                throw new PKIException("8123", PKIException.MAC_DES, ex);
            }
        } else {
            throw new PKIException("8123", "MAC操作失败 本操作不支持此种机制类型 " + mType);
        }
    }

    public boolean verifyMac(Mechanism mechanism, JKey key, InputStream sourceData, byte[] macData) throws PKIException {
        String mType = mechanism.getMechanismType();
        if (mType.equals(Mechanism.HMAC_MD2) || mType.equals(Mechanism.HMAC_MD5) || mType.equals(Mechanism.HMAC_SHA1)) {
            try {
                return Parser.isEqualArray(mac(mechanism, key, sourceData), macData);
            } catch (Exception ex) {
                throw new PKIException("8124", PKIException.VERIFY_MAC_DES, ex);
            }
        } else {
            throw new PKIException("8124", "验证MAC操作失败 本操作不支持此种机制类型 " + mType);
        }
    }

    public byte[] sign(Mechanism mechanism, JKey prvKey, InputStream sourceData) throws PKIException {
        String mType = mechanism.getMechanismType();
        if (!mechanism.isSignabled()) {
            throw new PKIException("8125", "签名操作失败 本操作不支持此种机制类型 " + mType);
        } else if (mType.equals("SM3withSM2Encryption")) {
            JKey pub = (JKey) mechanism.getParam();
            SM2 sm2 = SM2.Instance();
            byte[] md = digest(new Mechanism(Mechanism.SM3), sourceData);
            try {
                byte[] eccsfpubk = Util.hardKey2SoftPubKey(pub);
                SM2Result sm2Ret = new SM2Result();
                sm2.Sm2Sign(md, Util.hardKey2SoftPrivKey(prvKey), sm2.ecc_curve.decodePoint(eccsfpubk), sm2Ret);
                return Util.soft2HardSignData(sm2Ret.r, sm2Ret.s);
            } catch (Exception e) {
                throw new PKIException("8125", PKIException.SIGN_DES, e);
            }
        } else {
            try {
                Signature signature = Signature.getInstance(mType, "BC");
                signature.initSign(Parser.convertPrivateKey(prvKey));
                byte[] buffer = new byte[1024];
                while (true) {
                    int i = sourceData.read(buffer);
                    if (i <= 0) {
                        return signature.sign();
                    }
                    signature.update(buffer, 0, i);
                }
            } catch (Exception ex) {
                throw new PKIException("8125", PKIException.SIGN_DES, ex);
            }
        }
    }

    public boolean verifySign(Mechanism mechanism, JKey pubKey, InputStream sourceData, byte[] signData) throws PKIException {
        String mType = mechanism.getMechanismType();
        if (!mechanism.isSignabled()) {
            throw new PKIException("8126", "验证签名操作失败 本操作不支持此种机制类型 " + mType);
        } else if (mType.equals("SM3withSM2Encryption")) {
            try {
                SM2 sm2 = SM2.Instance();
                ECPoint softpub = sm2.ecc_curve.decodePoint(Util.hardKey2SoftPubKey(pubKey));
                byte[] md = digest(new Mechanism(Mechanism.SM3), sourceData);
                SM2Result sm2Ret = Util.Hard2softSignData(signData);
                sm2.Sm2Verify(md, softpub, sm2Ret.r, sm2Ret.s, sm2Ret);
                return sm2Ret.r.equals(sm2Ret.R);
            } catch (Exception ex) {
                throw new PKIException("8126", PKIException.VERIFY_SIGN_DES, ex);
            }
        } else {
            try {
                Signature signature = Signature.getInstance(mechanism.getMechanismType(), "BC");
                signature.initVerify(Parser.convertPublicKey(pubKey));
                byte[] buffer = new byte[1024];
                while (true) {
                    int i = sourceData.read(buffer);
                    if (i <= 0) {
                        return signature.verify(signData);
                    }
                    signature.update(buffer, 0, i);
                }
            } catch (Exception ex2) {
                throw new PKIException("8126", PKIException.VERIFY_SIGN_DES, ex2);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cn.com.jit.ida.util.pki.cipher.lib.JSoftLib.doCipher(cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, boolean, java.io.InputStream):byte[]
     arg types: [cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, int, java.io.InputStream]
     candidates:
      cn.com.jit.ida.util.pki.cipher.lib.JSoftLib.doCipher(cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, boolean, byte[]):byte[]
      cn.com.jit.ida.util.pki.cipher.lib.JSoftLib.doCipher(cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, boolean, java.io.InputStream):byte[] */
    public byte[] encrypt(Mechanism mechanism, JKey enKey, InputStream sourceData) throws PKIException {
        String mType = mechanism.getMechanismType();
        if (mType.equals(Mechanism.SM2_RAW) || mType.equals(Mechanism.SM4_ECB) || mType.equals(Mechanism.SM4_CBC)) {
            try {
                byte[] data = new byte[sourceData.available()];
                sourceData.read(data);
                return encrypt(mechanism, enKey, data);
            } catch (Exception ex) {
                throw new PKIException("8120", PKIException.ENCRYPT_DES, ex);
            }
        } else {
            try {
                return doCipher(mechanism, enKey, true, sourceData);
            } catch (Exception ex2) {
                throw new PKIException("8120", PKIException.ENCRYPT_DES, ex2);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cn.com.jit.ida.util.pki.cipher.lib.JSoftLib.doCipher(cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, boolean, java.io.InputStream):byte[]
     arg types: [cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, int, java.io.InputStream]
     candidates:
      cn.com.jit.ida.util.pki.cipher.lib.JSoftLib.doCipher(cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, boolean, byte[]):byte[]
      cn.com.jit.ida.util.pki.cipher.lib.JSoftLib.doCipher(cn.com.jit.ida.util.pki.cipher.Mechanism, cn.com.jit.ida.util.pki.cipher.JKey, boolean, java.io.InputStream):byte[] */
    public byte[] decrypt(Mechanism mechanism, JKey deKey, InputStream encryptedData) throws PKIException {
        String mType = mechanism.getMechanismType();
        if (mType.equals(Mechanism.SM2_RAW) || mType.equals(Mechanism.SM4_ECB) || mType.equals(Mechanism.SM4_CBC)) {
            try {
                byte[] data = new byte[encryptedData.available()];
                encryptedData.read(data);
                return decrypt(mechanism, deKey, data);
            } catch (Exception ex) {
                throw new PKIException("8121", PKIException.DECRYPT_DES, ex);
            }
        } else {
            try {
                return doCipher(mechanism, deKey, false, encryptedData);
            } catch (Exception ex2) {
                throw new PKIException("8121", PKIException.DECRYPT_DES, ex2);
            }
        }
    }

    public int encrypt(Mechanism mechanism, JKey enKey, InputStream sourceData, OutputStream out) throws PKIException {
        int len = 0;
        String mType = mechanism.getMechanismType();
        if (mType.equals(Mechanism.SM2_RAW) || mType.equals(Mechanism.SM4_ECB) || mType.equals(Mechanism.SM4_CBC)) {
            try {
                byte[] buffer = new byte[1024];
                while (sourceData.read(buffer) > 0) {
                    byte[] enc = encrypt(mechanism, enKey, buffer);
                    out.write(enc);
                    len += enc.length;
                }
                return len;
            } catch (Exception ex) {
                throw new PKIException("8120", PKIException.ENCRYPT_DES, ex);
            }
        } else {
            try {
                return doCipher(mechanism, enKey, true, sourceData, out);
            } catch (Exception ex2) {
                throw new PKIException("8120", PKIException.ENCRYPT_DES, ex2);
            }
        }
    }

    public int decrypt(Mechanism mechanism, JKey deKey, InputStream encryptedData, OutputStream out) throws PKIException {
        int len = 0;
        String mType = mechanism.getMechanismType();
        if (mType.equals(Mechanism.SM2_RAW) || mType.equals(Mechanism.SM4_ECB) || mType.equals(Mechanism.SM4_CBC)) {
            try {
                byte[] buffer = new byte[1024];
                while (encryptedData.read(buffer) > 0) {
                    byte[] dec = decrypt(mechanism, deKey, buffer);
                    out.write(dec);
                    len += dec.length;
                }
                return len;
            } catch (Exception ex) {
                throw new PKIException("8120", PKIException.ENCRYPT_DES, ex);
            }
        } else {
            try {
                return doCipher(mechanism, deKey, false, encryptedData, out);
            } catch (Exception ex2) {
                throw new PKIException("8121", PKIException.DECRYPT_DES, ex2);
            }
        }
    }

    private byte[] doCipher(Mechanism mechanism, JKey jkey, boolean isEncrypt, InputStream data) throws Exception {
        int cipherMode;
        String mType = mechanism.getMechanismType();
        int rsaKeyLen = -1;
        if (mType.equalsIgnoreCase(Mechanism.RSA_PKCS)) {
            if (jkey.getKeyType().equals(JKey.RSA_PUB_KEY)) {
                rsaKeyLen = ((RSAPublicKey) Parser.convertPublicKey(jkey)).getModulus().bitLength();
            } else if (jkey.getKeyType().equals(JKey.RSA_PRV_KEY)) {
                rsaKeyLen = ((RSAPrivateKey) Parser.convertPrivateKey(jkey)).getModulus().bitLength();
            }
            if (rsaKeyLen > 2048) {
                byte[] bs = new byte[data.available()];
                data.read(bs);
                data.close();
                return doCipher_RSA_ext(mechanism, jkey, isEncrypt, bs);
            }
        }
        javax.crypto.Cipher cipher = javax.crypto.Cipher.getInstance(mType, "BC");
        if (isEncrypt) {
            cipherMode = 1;
        } else {
            cipherMode = 2;
        }
        if (mType.indexOf("CBC") != -1) {
            CBCParam cbcParam = (CBCParam) mechanism.getParam();
            if (cbcParam == null) {
                throw new PKIException("CBC参数为空");
            }
            cipher.init(cipherMode, Parser.convertKey(jkey), new IvParameterSpec(cbcParam.getIv()));
        } else if (mType.indexOf("PBE") != -1) {
            PBEParam pbeParam = (PBEParam) mechanism.getParam();
            if (pbeParam == null) {
                throw new PKIException("PBE参数为空");
            }
            cipher.init(cipherMode, Parser.convertKey(jkey), new PBEParameterSpec(pbeParam.getSalt(), pbeParam.getIterations()));
        } else {
            cipher.init(cipherMode, Parser.convertKey(jkey));
        }
        ByteArrayOutputStream bin = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        while (true) {
            int i = data.read(buffer);
            if (i > 0) {
                bin.write(cipher.update(buffer, 0, i));
            } else {
                bin.write(cipher.doFinal());
                return bin.toByteArray();
            }
        }
    }

    private int doCipher(Mechanism mechanism, JKey jkey, boolean isEncrypt, InputStream data, OutputStream out) throws Exception {
        int cipherMode;
        String mType = mechanism.getMechanismType();
        int rsaKeyLen = -1;
        if (mType.equalsIgnoreCase(Mechanism.RSA_PKCS)) {
            if (jkey.getKeyType().equals(JKey.RSA_PUB_KEY)) {
                rsaKeyLen = ((RSAPublicKey) Parser.convertPublicKey(jkey)).getModulus().bitLength();
            } else if (jkey.getKeyType().equals(JKey.RSA_PRV_KEY)) {
                rsaKeyLen = ((RSAPrivateKey) Parser.convertPrivateKey(jkey)).getModulus().bitLength();
            }
            if (rsaKeyLen > 2048) {
                byte[] bs = new byte[data.available()];
                data.read(bs);
                data.close();
                byte[] temp = doCipher_RSA_ext(mechanism, jkey, isEncrypt, bs);
                out.write(temp);
                return temp.length;
            }
        }
        javax.crypto.Cipher cipher = javax.crypto.Cipher.getInstance(mType, "BC");
        if (isEncrypt) {
            cipherMode = 1;
        } else {
            cipherMode = 2;
        }
        if (mType.indexOf("CBC") != -1) {
            CBCParam cbcParam = (CBCParam) mechanism.getParam();
            if (cbcParam == null) {
                throw new PKIException("CBC参数为空");
            }
            cipher.init(cipherMode, Parser.convertKey(jkey), new IvParameterSpec(cbcParam.getIv()));
        } else if (mType.indexOf("PBE") != -1) {
            PBEParam pbeParam = (PBEParam) mechanism.getParam();
            if (pbeParam == null) {
                throw new PKIException("PBE参数为空");
            }
            cipher.init(cipherMode, Parser.convertKey(jkey), new PBEParameterSpec(pbeParam.getSalt(), pbeParam.getIterations()));
        } else {
            cipher.init(cipherMode, Parser.convertKey(jkey));
        }
        byte[] buffer = new byte[1024];
        int datalen = 0;
        while (true) {
            int i = data.read(buffer);
            if (i > 0) {
                byte[] aa = cipher.update(buffer, 0, i);
                out.write(aa);
                datalen += aa.length;
            } else {
                byte[] last = cipher.doFinal();
                out.write(last);
                return datalen + last.length;
            }
        }
    }

    public byte[] decryptFinal(JHandle handle, Mechanism mechanism, byte[] sourceData) throws PKIException {
        try {
            return handle.getSoftLibHandle().doFinal(sourceData);
        } catch (Exception ex) {
            throw new PKIException("8121", PKIException.DECRYPT_DES, ex);
        }
    }

    public JHandle decryptInit(Mechanism mechanism, JKey deKey) throws PKIException {
        try {
            String mType = mechanism.getMechanismType();
            javax.crypto.Cipher largeDataCipher = javax.crypto.Cipher.getInstance(mType, "BC");
            if (mType.indexOf("PBE") != -1) {
                PBEParam pbeParam = (PBEParam) mechanism.getParam();
                if (pbeParam == null) {
                    throw new PKIException("PBE参数为空");
                }
                largeDataCipher.init(2, Parser.convertKey(deKey), new PBEParameterSpec(pbeParam.getSalt(), pbeParam.getIterations()));
            } else if (mType.indexOf("CBC") != -1) {
                CBCParam cbcParam = (CBCParam) mechanism.getParam();
                if (cbcParam == null) {
                    throw new PKIException("CBC参数为空");
                }
                largeDataCipher.init(2, Parser.convertKey(deKey), new IvParameterSpec(cbcParam.getIv()));
            } else {
                largeDataCipher.init(2, Parser.convertKey(deKey));
            }
            return new JHandle(0, largeDataCipher);
        } catch (Exception ex) {
            throw new PKIException("8120", PKIException.DECRYPT_DES, ex);
        }
    }

    public byte[] decryptUpdate(JHandle handle, Mechanism mechanism, byte[] sourceData) throws PKIException {
        try {
            return handle.getSoftLibHandle().update(sourceData);
        } catch (Exception ex) {
            throw new PKIException("8121", PKIException.DECRYPT_DES, ex);
        }
    }

    public byte[] encryptFinal(JHandle handle, Mechanism mechanism, byte[] sourceData) throws PKIException {
        try {
            return handle.getSoftLibHandle().doFinal(sourceData);
        } catch (Exception ex) {
            throw new PKIException("8121", PKIException.DECRYPT_DES, ex);
        }
    }

    public JHandle encryptInit(Mechanism mechanism, JKey enKey) throws PKIException {
        try {
            String mType = mechanism.getMechanismType();
            javax.crypto.Cipher largeDataCipher = javax.crypto.Cipher.getInstance(mType, "BC");
            if (mType.indexOf("PBE") != -1) {
                PBEParam pbeParam = (PBEParam) mechanism.getParam();
                if (pbeParam == null) {
                    throw new PKIException("PBE参数为空");
                }
                largeDataCipher.init(1, Parser.convertKey(enKey), new PBEParameterSpec(pbeParam.getSalt(), pbeParam.getIterations()));
            } else if (mType.indexOf("CBC") != -1) {
                CBCParam cbcParam = (CBCParam) mechanism.getParam();
                if (cbcParam == null) {
                    throw new PKIException("CBC参数为空");
                }
                largeDataCipher.init(1, Parser.convertKey(enKey), new IvParameterSpec(cbcParam.getIv()));
            } else {
                largeDataCipher.init(1, Parser.convertKey(enKey));
            }
            return new JHandle(0, largeDataCipher);
        } catch (Exception ex) {
            throw new PKIException("8120", PKIException.DECRYPT_DES, ex);
        }
    }

    public byte[] encryptUpdate(JHandle handle, Mechanism mechanism, byte[] sourceData) throws PKIException {
        try {
            return handle.getSoftLibHandle().update(sourceData);
        } catch (Exception ex) {
            throw new PKIException("8121", PKIException.DECRYPT_DES, ex);
        }
    }

    public boolean updateKeyPair(Mechanism mechanism, JKey pubKey, JKey prvKey, int hardPos) throws PKIException {
        throw new PKIException("8128195", "PKIERRORNO updateKeyPair() JSoftLib didn't support Stream-Operation yet. ");
    }

    private boolean isEqualArray(byte[] a, byte[] b) {
        if (a.length != b.length) {
            return false;
        }
        for (int i = 0; i < a.length; i++) {
            if (a[i] != b[i]) {
                return false;
            }
        }
        return true;
    }

    public String getCfgTagName() throws PKIException {
        return this.tag;
    }

    public void setCfgTag(PKIToolConfig cfg) throws PKIException {
        this.CfgTag = cfg;
    }

    public PKIToolConfig getCfgTag() throws PKIException {
        return this.CfgTag;
    }

    public boolean createCertObject(byte[] dn, byte[] certData, byte[] keyId) throws PKIException {
        return false;
    }

    public boolean destroyCertObject(byte[] dn, byte[] keyId) throws PKIException {
        return false;
    }

    public byte[] getCertObject(byte[] keyId) throws PKIException {
        return null;
    }

    public List WrapKeyEnc(JKey pubKey, JKey sysKey, Mechanism pubEncMech, Mechanism sysEncMech, byte[] data) throws PKIException {
        String pubEncType = pubEncMech.getMechanismType();
        String sysEnctype = sysEncMech.getMechanismType();
        if (!sysEnctype.equals(Mechanism.DES_CBC) && !sysEnctype.equals(Mechanism.DES_ECB) && !sysEnctype.equals(Mechanism.DES3_ECB) && !sysEnctype.equals(Mechanism.DES3_CBC) && !sysEnctype.equals(Mechanism.AES_ECB) && !sysEnctype.equals(Mechanism.AES_CBC) && !sysEnctype.equals(Mechanism.SM4_CBC) && !sysEnctype.equals(Mechanism.SM4_ECB)) {
            throw new PKIException(PKIException.DATA_LOAD_FAIL, "加密操作失败 本操作不支持此种机制类型 " + sysEnctype);
        } else if (pubEncType.equals(Mechanism.RSA_PKCS) || pubEncType.equals(Mechanism.SM2_RAW)) {
            CBCParam cbcParam = (CBCParam) sysEncMech.getParam();
            if (cbcParam != null) {
                byte[] cbcIv = cbcParam.getIv();
            }
            if (sysKey == null) {
                sysKey = generateKey(encMech2genMech(sysEncMech), getSysKeyLen(sysEncMech));
            }
            byte[] encData = encrypt(sysEncMech, sysKey, data);
            byte[] encedKey = encrypt(pubEncMech, pubKey, sysKey.getKey());
            List ls = new ArrayList();
            ls.add(encData);
            ls.add(encedKey);
            return ls;
        } else {
            throw new PKIException(PKIException.DATA_LOAD_FAIL, "加密操作失败 本操作不支持此种机制类型 " + pubEncType);
        }
    }

    private byte[] getDigforSign(byte[] source, ECPoint pub) throws PKIException {
        SM3Digest sm3 = new SM3Digest();
        byte[] z = SM2.Instance().Sm2GetZ("1234567812345678".getBytes(), pub);
        sm3.BlockUpdate(z, 0, z.length);
        sm3.BlockUpdate(source, 0, source.length);
        byte[] md = new byte[32];
        sm3.doFinal(md, 0);
        return md;
    }

    private Mechanism encMech2genMech(Mechanism encMech) throws PKIException {
        if (encMech == null) {
            return null;
        }
        String mech = encMech.getMechanismType();
        if (mech.equals(Mechanism.SF33_ECB) || mech.equals(Mechanism.SF33_CBC)) {
            return new Mechanism("SF33");
        }
        if (mech.equals(Mechanism.DES_ECB) || mech.equals(Mechanism.DES_CBC)) {
            return new Mechanism("DES");
        }
        if (mech.equals(Mechanism.DES3_ECB) || mech.equals(Mechanism.DES3_CBC)) {
            return new Mechanism("DESede");
        }
        if (mech.equals(Mechanism.AES_ECB) || mech.equals(Mechanism.AES_CBC)) {
            return new Mechanism("AES");
        }
        if (mech.equals(Mechanism.SCB2_ECB) || mech.equals(Mechanism.SCB2_CBC)) {
            return new Mechanism("SCB2");
        }
        if (mech.equals(Mechanism.SM4_ECB) || mech.equals(Mechanism.SM4_CBC)) {
            return new Mechanism("SM4");
        }
        if (mech.equals(Mechanism.RSA_PKCS)) {
            return new Mechanism(Mechanism.RSA);
        }
        if (mech.equals(Mechanism.SM2_RAW)) {
            return new Mechanism(Mechanism.SM2);
        }
        return null;
    }

    private int getSysKeyLen(Mechanism encMech) throws PKIException {
        if (encMech == null) {
            return 0;
        }
        String mech = encMech.getMechanismType();
        if (mech.equals(Mechanism.SF33_ECB) || mech.equals(Mechanism.SF33_CBC)) {
            return 128;
        }
        if (mech.equals(Mechanism.DES_ECB) || mech.equals(Mechanism.DES_CBC)) {
            return 64;
        }
        if (mech.equals(Mechanism.DES3_ECB) || mech.equals(Mechanism.DES3_CBC)) {
            return 192;
        }
        if (mech.equals(Mechanism.AES_ECB) || mech.equals(Mechanism.AES_CBC)) {
            return 128;
        }
        if (mech.equals(Mechanism.SCB2_ECB) || mech.equals(Mechanism.SCB2_CBC)) {
            return 128;
        }
        if (mech.equals(Mechanism.SM4_ECB) || mech.equals(Mechanism.SM4_CBC)) {
            return 128;
        }
        return 0;
    }

    private byte[] getSM2EnvedKey(Mechanism sysEncMech, byte[] encedSysKey, byte[] pubkey, byte[] encedPriKey) throws PKIException {
        if (!sysEncMech.getMechanismType().equals(Mechanism.SM4_ECB)) {
            return null;
        }
        AlgorithmIdentifier algo = new AlgorithmIdentifier(PKCSObjectIdentifiers.gm_SM4, new DERNull());
        ASN1EncodableVector v = null;
        try {
            ASN1Sequence sysKey = (ASN1Sequence) new ASN1InputStream(new ByteArrayInputStream(encedSysKey)).readObject();
            DERBitString bitPriKey = new DERBitString(encedPriKey);
            DERBitString bitPubKey = new DERBitString(pubkey);
            ASN1EncodableVector v2 = new ASN1EncodableVector();
            try {
                v2.add(algo);
                v2.add(sysKey);
                v2.add(bitPubKey);
                v2.add(bitPriKey);
                v = v2;
            } catch (IOException e) {
                e = e;
                v = v2;
                e.printStackTrace();
                return Parser.writeDERObj2Bytes(new BERSequence(v));
            }
        } catch (IOException e2) {
            e = e2;
            e.printStackTrace();
            return Parser.writeDERObj2Bytes(new BERSequence(v));
        }
        return Parser.writeDERObj2Bytes(new BERSequence(v));
    }

    public byte[] WrapPriKey(JKey pubKey, JKey sysKey, Mechanism pubEncMech, Mechanism sysEncMech, JKey priKey) throws PKIException {
        try {
            byte[] eccsfpubk = Util.hardKey2SoftPubKey(pubKey);
            byte[] priData = Util.byteconvert32(Util.hardKey2SoftPrivKey(priKey));
            byte[] data = new byte[(priData.length + 32)];
            for (int i = 0; i < 32; i++) {
                data[i] = 0;
            }
            System.arraycopy(priData, 0, data, 32, 32);
            sysEncMech.setPad(false);
            List ls = WrapKeyEnc(pubKey, sysKey, pubEncMech, sysEncMech, data);
            return getSM2EnvedKey(sysEncMech, (byte[]) ls.get(1), eccsfpubk, (byte[]) ls.get(0));
        } catch (Exception ex) {
            throw new PKIException(PKIException.DATA_LOAD_FAIL, ex.getMessage());
        }
    }
}
