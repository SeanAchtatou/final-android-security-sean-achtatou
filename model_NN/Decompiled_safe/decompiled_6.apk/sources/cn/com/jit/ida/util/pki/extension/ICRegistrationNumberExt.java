package cn.com.jit.ida.util.pki.extension;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.DERPrintableString;
import cn.com.jit.ida.util.pki.asn1.x509.JITICRegistrationNumber;
import cn.com.jit.ida.util.pki.asn1.x509.X509Extensions;

public class ICRegistrationNumberExt extends AbstractStandardExtension {
    private String icregistationnumber = null;

    public ICRegistrationNumberExt() {
        this.OID = X509Extensions.JIT_ICRegistrationNumber.getId();
        this.critical = false;
    }

    public ICRegistrationNumberExt(DERPrintableString obj) {
        this.icregistationnumber = obj.getString();
    }

    public ICRegistrationNumberExt(String value) {
        this.OID = X509Extensions.JIT_ICRegistrationNumber.getId();
        this.critical = false;
        this.icregistationnumber = value;
    }

    public void SetICRegistationNumber(String value) {
        this.icregistationnumber = value;
    }

    public String GetICRegistationNumber() {
        return this.icregistationnumber;
    }

    public byte[] encode() throws PKIException {
        if (this.icregistationnumber != null) {
            return new DEROctetString(new JITICRegistrationNumber(this.icregistationnumber).getDERObject()).getOctets();
        }
        throw new PKIException(PKIException.EXT_ENCODING_DATA_NULL, PKIException.EXT_ENCODING_DATA_NULL_DES);
    }

    public boolean getCritical() {
        return this.critical;
    }

    public String getOID() {
        return this.OID;
    }

    public void setCritical(boolean critical) {
        this.critical = critical;
    }
}
