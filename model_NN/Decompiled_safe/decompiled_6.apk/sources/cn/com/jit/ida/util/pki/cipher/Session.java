package cn.com.jit.ida.util.pki.cipher;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.PKIToolConfig;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public interface Session {
    boolean DestroyKeyPair(Mechanism mechanism) throws PKIException;

    List WrapKeyEnc(JKey jKey, JKey jKey2, Mechanism mechanism, Mechanism mechanism2, byte[] bArr) throws PKIException;

    byte[] WrapPriKey(JKey jKey, JKey jKey2, Mechanism mechanism, Mechanism mechanism2, JKey jKey3) throws PKIException;

    boolean createCertObject(byte[] bArr, byte[] bArr2, byte[] bArr3) throws PKIException;

    int decrypt(Mechanism mechanism, JKey jKey, InputStream inputStream, OutputStream outputStream) throws PKIException;

    byte[] decrypt(Mechanism mechanism, JKey jKey, InputStream inputStream) throws PKIException;

    byte[] decrypt(Mechanism mechanism, JKey jKey, byte[] bArr) throws PKIException;

    byte[] decryptFinal(JHandle jHandle, Mechanism mechanism, byte[] bArr) throws PKIException;

    JHandle decryptInit(Mechanism mechanism, JKey jKey) throws PKIException;

    byte[] decryptUpdate(JHandle jHandle, Mechanism mechanism, byte[] bArr) throws PKIException;

    boolean destroyCertObject(byte[] bArr, byte[] bArr2) throws PKIException;

    byte[] digest(Mechanism mechanism, InputStream inputStream) throws PKIException;

    byte[] digest(Mechanism mechanism, byte[] bArr) throws PKIException;

    int encrypt(Mechanism mechanism, JKey jKey, InputStream inputStream, OutputStream outputStream) throws PKIException;

    byte[] encrypt(Mechanism mechanism, JKey jKey, InputStream inputStream) throws PKIException;

    byte[] encrypt(Mechanism mechanism, JKey jKey, byte[] bArr) throws PKIException;

    byte[] encryptFinal(JHandle jHandle, Mechanism mechanism, byte[] bArr) throws PKIException;

    JHandle encryptInit(Mechanism mechanism, JKey jKey) throws PKIException;

    byte[] encryptUpdate(JHandle jHandle, Mechanism mechanism, byte[] bArr) throws PKIException;

    JKey generateKey(Mechanism mechanism, int i) throws PKIException;

    JKeyPair generateKeyPair(Mechanism mechanism, int i) throws PKIException;

    JKey generatePBEKey(Mechanism mechanism, char[] cArr) throws PKIException;

    byte[] generateRandom(Mechanism mechanism, int i) throws PKIException;

    byte[] getCertObject(byte[] bArr) throws PKIException;

    PKIToolConfig getCfgTag() throws PKIException;

    String getCfgTagName() throws PKIException;

    byte[] mac(Mechanism mechanism, JKey jKey, InputStream inputStream) throws PKIException;

    byte[] mac(Mechanism mechanism, JKey jKey, byte[] bArr) throws PKIException;

    void setCfgTag(PKIToolConfig pKIToolConfig) throws PKIException;

    byte[] sign(Mechanism mechanism, JKey jKey, InputStream inputStream) throws PKIException;

    byte[] sign(Mechanism mechanism, JKey jKey, byte[] bArr) throws PKIException;

    boolean updateKeyPair(Mechanism mechanism, JKey jKey, JKey jKey2, int i) throws PKIException;

    boolean verifyMac(Mechanism mechanism, JKey jKey, InputStream inputStream, byte[] bArr) throws PKIException;

    boolean verifyMac(Mechanism mechanism, JKey jKey, byte[] bArr, byte[] bArr2) throws PKIException;

    boolean verifySign(Mechanism mechanism, JKey jKey, InputStream inputStream, byte[] bArr) throws PKIException;

    boolean verifySign(Mechanism mechanism, JKey jKey, byte[] bArr, byte[] bArr2) throws PKIException;
}
