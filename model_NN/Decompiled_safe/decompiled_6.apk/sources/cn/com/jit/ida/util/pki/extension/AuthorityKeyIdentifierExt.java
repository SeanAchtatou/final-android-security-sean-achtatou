package cn.com.jit.ida.util.pki.extension;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.Parser;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.x509.AuthorityKeyIdentifier;
import cn.com.jit.ida.util.pki.asn1.x509.GeneralName;
import cn.com.jit.ida.util.pki.asn1.x509.GeneralNames;
import cn.com.jit.ida.util.pki.asn1.x509.SubjectPublicKeyInfo;
import cn.com.jit.ida.util.pki.asn1.x509.X509Extensions;
import cn.com.jit.ida.util.pki.asn1.x509.X509Name;
import cn.com.jit.ida.util.pki.cipher.JKey;
import com.jianq.misc.StringEx;
import java.math.BigInteger;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

public class AuthorityKeyIdentifierExt extends AbstractStandardExtension {
    private String certIssuer = null;
    private BigInteger certSerialNumber = null;
    private JKey pubKey = null;
    private AuthorityKeyIdentifier spki = null;

    public AuthorityKeyIdentifierExt(JKey pubKey2) {
        this.pubKey = pubKey2;
        this.critical = false;
        this.OID = X509Extensions.AuthorityKeyIdentifier.getId();
    }

    public AuthorityKeyIdentifierExt(Node nl) throws PKIException {
        this.critical = false;
        this.OID = X509Extensions.AuthorityKeyIdentifier.getId();
        NodeList nodelist = nl.getChildNodes();
        if (nodelist.getLength() == 0) {
            throw new PKIException("XML内容缺失,AuthorityKeyIdentifierExt.AuthorityKeyIdentifierExt(),AuthorityKeyIdentifierExtension没有子节点");
        }
        for (int i = 0; i < nodelist.getLength(); i++) {
            Node cnode = nodelist.item(i);
            String sname = cnode.getNodeName();
            if (sname.equals("keyIdentifier")) {
                this.OID = ((Text) cnode.getFirstChild()).getData().trim();
            }
            if (sname.equals("AuthorityCertIssuer")) {
                this.certIssuer = generalAuthorityCertIssuer(cnode);
            }
            if (sname.equals("AuthorityCertSerialNumber")) {
                this.certSerialNumber = new BigInteger(((Text) nl.getFirstChild()).getData().trim(), 16);
            }
        }
    }

    public AuthorityKeyIdentifierExt(ASN1Sequence asn1Sequence) {
        if (asn1Sequence.size() != 0) {
            this.spki = new AuthorityKeyIdentifier(asn1Sequence);
            GeneralNames issuer = this.spki.getGeneralNames();
            if (issuer != null) {
                this.certIssuer = issuer.toString();
            }
            if (this.spki.getCertserno() != null) {
                this.certSerialNumber = this.spki.getCertserno().getValue();
            }
        }
    }

    public byte[] getAuthorityKeyIdentifier() {
        return this.spki.getKeyIdentifier();
    }

    public String generalAuthorityCertIssuer(Node nl) throws PKIException {
        String value = null;
        NodeList nodelist = nl.getChildNodes();
        if (nodelist.getLength() == 0) {
            throw new PKIException("XML内容缺失,AuthorityKeyIdentifierExt.generalAuthorityCertIssuer(),AuthorityCertIssuer没有子节点");
        }
        for (int i = 0; i < nodelist.getLength(); i++) {
            Node cnode = nodelist.item(i);
            if (cnode.getNodeName().equals("GeneralName")) {
                NamedNodeMap attributes = cnode.getAttributes();
                String sname = ((Text) cnode.getFirstChild()).getData();
                if (value == null) {
                    value = sname.trim();
                } else {
                    value = value + ";" + sname.trim();
                }
            }
        }
        return value;
    }

    public JKey getAuthorityPublicKey() {
        return this.pubKey;
    }

    public void setAuthorityCertIssuer(String authorityCertIssuer) {
        this.certIssuer = authorityCertIssuer;
    }

    public String getAuthorityCertIssuer() {
        return this.certIssuer;
    }

    public void setAuthorityCertSerialNumber(BigInteger authorityCertSerialNumber) {
        this.certSerialNumber = authorityCertSerialNumber;
    }

    public BigInteger getAuthorityCertSerialNumber() {
        return this.certSerialNumber;
    }

    public void setAuthorityCertSerialNumber(int authorityCertSerialNumber) {
        this.certSerialNumber = new BigInteger(String.valueOf(authorityCertSerialNumber));
    }

    public void setAuthorityCertSerialNumber(String authorityCertSerialNumber) {
        this.certSerialNumber = new BigInteger(authorityCertSerialNumber);
    }

    public String getAuthorityCertSerialNumberStringType() {
        return this.certSerialNumber.toString();
    }

    public String getOID() {
        return this.OID;
    }

    public void setCritical(boolean critical) {
        this.critical = critical;
    }

    public boolean getCritical() {
        return this.critical;
    }

    public byte[] encode() throws PKIException {
        AuthorityKeyIdentifier authKeyID;
        try {
            SubjectPublicKeyInfo spki2 = Parser.key2SPKI(this.pubKey);
            GeneralNames name = null;
            if (this.certIssuer != null && !this.certIssuer.equals(StringEx.Empty)) {
                name = new GeneralNames(new DERSequence(new GeneralName(new X509Name(this.certIssuer))));
            }
            if (name == null && this.certSerialNumber == null) {
                authKeyID = new AuthorityKeyIdentifier(spki2);
            } else if (name == null) {
                authKeyID = new AuthorityKeyIdentifier(spki2, this.certSerialNumber);
            } else if (this.certSerialNumber == null) {
                authKeyID = new AuthorityKeyIdentifier(spki2, name);
            } else {
                authKeyID = new AuthorityKeyIdentifier(spki2, name, this.certSerialNumber);
            }
            return new DEROctetString(authKeyID.getDERObject()).getOctets();
        } catch (PKIException ex) {
            throw ex;
        }
    }
}
