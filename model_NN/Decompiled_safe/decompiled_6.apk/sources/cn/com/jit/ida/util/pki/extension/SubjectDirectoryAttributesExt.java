package cn.com.jit.ida.util.pki.extension;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.ASN1SequenceExt;
import cn.com.jit.ida.util.pki.asn1.DERBMPString;
import cn.com.jit.ida.util.pki.asn1.DERBitString;
import cn.com.jit.ida.util.pki.asn1.DERBoolean;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DEREncodableVector;
import cn.com.jit.ida.util.pki.asn1.DERGeneralizedTime;
import cn.com.jit.ida.util.pki.asn1.DERIA5String;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DERNumericString;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.DERPrintableString;
import cn.com.jit.ida.util.pki.asn1.DERSet;
import cn.com.jit.ida.util.pki.asn1.DERT61String;
import cn.com.jit.ida.util.pki.asn1.DERUTCTime;
import cn.com.jit.ida.util.pki.asn1.DERUTF8String;
import cn.com.jit.ida.util.pki.asn1.DERUniversalString;
import cn.com.jit.ida.util.pki.asn1.DERVisibleString;
import cn.com.jit.ida.util.pki.asn1.x509.SubjectDirectoryAttributes;
import cn.com.jit.ida.util.pki.asn1.x509.X509Extensions;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

public class SubjectDirectoryAttributesExt extends AbstractStandardExtension {
    public static final short BITSTRING = 2;
    public static final short BMPSTRING = 4;
    public static final short GENERALIZEDTIME = 1;
    public static final short IA5STRING = 5;
    public static final short NONE = -1;
    public static final short NUMERICSTRING = 6;
    public static final short OCTETSTRING = 7;
    public static final short PRINTABLESTRING = 3;
    public static final short T61STRING = 8;
    public static final short UNIVERSALSTRING = 9;
    public static final short UTCTIME = 0;
    public static final short UTF8STRING = 10;
    public static final short VISIBLESTRING = 11;
    private HashMap AttrMap = new HashMap();
    private boolean isHaveAttribute = false;

    public SubjectDirectoryAttributesExt() {
        this.OID = X509Extensions.SubjectDirectoryAttributes.getId();
        this.critical = false;
    }

    public void addSubjectDirectoryAttribute(String OID, String value, short type) throws PKIException {
        if (!this.isHaveAttribute) {
            this.isHaveAttribute = true;
        }
        addAttributes(new DERObjectIdentifier(OID), value, type);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cn.com.jit.ida.util.pki.extension.SubjectDirectoryAttributesExt.addAttributes(cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier, java.lang.Object, short):void
     arg types: [cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier, java.lang.Integer, int]
     candidates:
      cn.com.jit.ida.util.pki.extension.SubjectDirectoryAttributesExt.addAttributes(cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier, byte[], short):void
      cn.com.jit.ida.util.pki.extension.SubjectDirectoryAttributesExt.addAttributes(cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier, java.lang.Object, short):void */
    public void addSubjectDirectoryAttribute(String OID, Integer value) throws PKIException {
        if (!this.isHaveAttribute) {
            this.isHaveAttribute = true;
        }
        addAttributes(new DERObjectIdentifier(OID), (Object) value, (short) -1);
    }

    public void addSubjectDirectoryAttribute(String OID, byte[] value, short type) throws PKIException {
        if (!this.isHaveAttribute) {
            this.isHaveAttribute = true;
        }
        addAttributes(new DERObjectIdentifier(OID), value, type);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cn.com.jit.ida.util.pki.extension.SubjectDirectoryAttributesExt.addAttributes(cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier, java.lang.Object, short):void
     arg types: [cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier, java.lang.Boolean, int]
     candidates:
      cn.com.jit.ida.util.pki.extension.SubjectDirectoryAttributesExt.addAttributes(cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier, byte[], short):void
      cn.com.jit.ida.util.pki.extension.SubjectDirectoryAttributesExt.addAttributes(cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier, java.lang.Object, short):void */
    public void addSubjectDirectoryAttribute(String OID, Boolean value) throws PKIException {
        if (!this.isHaveAttribute) {
            this.isHaveAttribute = true;
        }
        addAttributes(new DERObjectIdentifier(OID), (Object) value, (short) -1);
    }

    public void addSubjectDirectoryAttribute(String OID, Date value, short type) throws PKIException {
        if (!this.isHaveAttribute) {
            this.isHaveAttribute = true;
        }
        addAttributes(new DERObjectIdentifier(OID), value, type);
    }

    public byte[] encode() throws PKIException {
        if (!this.isHaveAttribute) {
            throw new PKIException(PKIException.EXT_ENCODING_DATA_NULL, PKIException.EXT_ENCODING_DATA_NULL_DES);
        }
        ASN1SequenceExt subjectDirectoryAttributes = new ASN1SequenceExt();
        Iterator ValuesItr = this.AttrMap.values().iterator();
        for (DERObjectIdentifier addObject : this.AttrMap.keySet()) {
            ASN1SequenceExt seq_attr = new ASN1SequenceExt();
            seq_attr.addObject(addObject);
            seq_attr.addObject(new DERSet((DEREncodableVector) ValuesItr.next()));
            subjectDirectoryAttributes.addObject(seq_attr);
        }
        return new DEROctetString(new SubjectDirectoryAttributes(subjectDirectoryAttributes).getDERObject()).getOctets();
    }

    public boolean getCritical() {
        return this.critical;
    }

    public String getOID() {
        return this.OID;
    }

    public void setCritical(boolean critical) {
        this.critical = critical;
    }

    private void addAttributes(DERObjectIdentifier attrType, Object value, short type) throws PKIException {
        if (this.AttrMap.containsKey(attrType)) {
            ((DEREncodableVector) this.AttrMap.get(attrType)).add(TypeConversion(value, type));
            return;
        }
        DEREncodableVector derEncodableVector = new DEREncodableVector();
        derEncodableVector.add(TypeConversion(value, type));
        this.AttrMap.put(attrType, derEncodableVector);
    }

    private void addAttributes(DERObjectIdentifier attrType, byte[] value, short type) throws PKIException {
        if (this.AttrMap.containsKey(attrType)) {
            ((DEREncodableVector) this.AttrMap.get(attrType)).add(TypeConversion(value, type));
            return;
        }
        DEREncodableVector derEncodableVector = new DEREncodableVector();
        derEncodableVector.add(TypeConversion(value, type));
        this.AttrMap.put(attrType, derEncodableVector);
    }

    private DEREncodable TypeConversion(Object value, short Type) throws PKIException {
        DEREncodable derencodable = null;
        if (value instanceof Integer) {
            derencodable = new DERInteger(((Integer) value).intValue());
        } else if (value instanceof Boolean) {
            derencodable = new DERBoolean(((Boolean) value).booleanValue());
        } else if (value instanceof Date) {
            if (Type == 0) {
                derencodable = new DERUTCTime((Date) value);
            } else if (Type == 1) {
                derencodable = new DERGeneralizedTime((Date) value);
            }
        } else if (value instanceof String) {
            if (Type == 0) {
                derencodable = new DERUTCTime((String) value);
            } else if (Type == 1) {
                derencodable = new DERGeneralizedTime((String) value);
            } else if (Type == 3) {
                derencodable = new DERPrintableString((String) value);
            } else if (Type == 4) {
                derencodable = new DERBMPString((String) value);
            } else if (Type == 5) {
                derencodable = new DERIA5String((String) value);
            } else if (Type == 6) {
                derencodable = new DERNumericString((String) value);
            } else if (Type == 8) {
                derencodable = new DERT61String((String) value);
            } else if (Type == 10) {
                derencodable = new DERUTF8String((String) value);
            } else if (Type == 11) {
                derencodable = new DERVisibleString((String) value);
            }
        }
        if (derencodable != null) {
            return derencodable;
        }
        throw new PKIException(PKIException.EXT_ENCODING_DATATYPE_NOT, PKIException.EXT_ENCODING_DATATYPE_NOT_DES);
    }

    private DEREncodable TypeConversion(byte[] value, short Type) throws PKIException {
        DEREncodable derencodable = null;
        if (Type == 2) {
            derencodable = new DERBitString(value);
        } else if (Type == 3) {
            derencodable = new DERPrintableString(value);
        } else if (Type == 4) {
            derencodable = new DERBMPString(value);
        } else if (Type == 5) {
            derencodable = new DERIA5String(value);
        } else if (Type == 6) {
            derencodable = new DERNumericString(value);
        } else if (Type == 7) {
            derencodable = new DEROctetString(value);
        } else if (Type == 8) {
            derencodable = new DERT61String(value);
        } else if (Type == 9) {
            derencodable = new DERUniversalString(value);
        } else if (Type == 11) {
            derencodable = new DERVisibleString(value);
        }
        if (derencodable != null) {
            return derencodable;
        }
        throw new PKIException(PKIException.EXT_ENCODING_DATATYPE_NOT, PKIException.EXT_ENCODING_DATATYPE_NOT_DES);
    }
}
