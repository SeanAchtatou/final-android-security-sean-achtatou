package cn.com.jit.ida.util.pki.asn1;

import java.util.Vector;

public class DEREncodableVector {
    private Vector v = new Vector();

    public DEREncodableVector() {
    }

    public void add(DEREncodable obj) {
        this.v.addElement(obj);
    }

    public DEREncodable get(int i) {
        return (DEREncodable) this.v.elementAt(i);
    }

    public int size() {
        return this.v.size();
    }

    public DEREncodableVector(ASN1Sequence seq) {
        for (int i = 0; i < seq.size(); i++) {
            this.v.add(seq.getObjectAt(i));
        }
    }

    public DEREncodableVector(ASN1Set set) {
        for (int i = 0; i < set.size(); i++) {
            this.v.add(set.getObjectAt(i));
        }
    }
}
