package cn.com.jit.ida.util.pki.cipher.lib;

import cn.com.jit.ida.util.pki.PKIException;

public class JMRTDLib {
    private static JMRTDLib Instance = null;
    private static String pkiSessionDll = (System.getProperty("user.dir") + "/updatekeypair.dll");
    private static String sjyLibPath = (System.getProperty("user.dir") + "/lib/native/updatekey/pkilib.dll");

    private native int updateKeyPair(int i, int i2, int i3, String str);

    private JMRTDLib() {
        System.out.println("call load before");
        System.load(pkiSessionDll);
        System.out.println("call load after");
    }

    public static JMRTDLib getInstance() {
        if (Instance == null) {
            Instance = new JMRTDLib();
        }
        return Instance;
    }

    public boolean updateKeyPair(int iKeyNo, int keyType, int keyLen) throws PKIException {
        System.out.println("updateKeyPair: iKeyNo:" + iKeyNo + " keyLen:" + keyLen + " sjyLibPath" + sjyLibPath);
        int rec = updateKeyPair(iKeyNo, keyType, keyLen, sjyLibPath);
        System.out.println("return value:" + rec);
        if (rec == 0) {
            return true;
        }
        throw new PKIException(PKIException.UPDATEKEYPAIR_ERRORCODE, PKIException.UPDATEKEYPAIR_ERRORDESC);
    }

    public static void main(String[] args) {
        try {
            getInstance().updateKeyPair(1, 1024, 3);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
