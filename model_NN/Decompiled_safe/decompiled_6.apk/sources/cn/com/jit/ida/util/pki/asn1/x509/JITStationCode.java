package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.DERPrintableString;

public class JITStationCode extends DERPrintableString {
    public JITStationCode(byte[] string) {
        super(string);
    }

    public JITStationCode(String string) {
        super(string);
    }

    public String getStationCode() {
        return getString();
    }
}
