package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.DERInteger;
import java.math.BigInteger;

public class CRLNumber extends DERInteger {
    public CRLNumber(BigInteger number) {
        super(number);
    }

    public BigInteger getCRLNumber() {
        return getPositiveValue();
    }
}
