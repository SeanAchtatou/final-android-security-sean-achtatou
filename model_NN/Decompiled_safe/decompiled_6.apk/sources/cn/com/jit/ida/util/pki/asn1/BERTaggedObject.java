package cn.com.jit.ida.util.pki.asn1;

import java.io.IOException;
import java.util.Enumeration;

public class BERTaggedObject extends DERTaggedObject {
    public BERTaggedObject(int tagNo, DEREncodable obj) {
        super(tagNo, obj);
    }

    public BERTaggedObject(boolean explicit, int tagNo, DEREncodable obj) {
        super(explicit, tagNo, obj);
    }

    public BERTaggedObject(int tagNo) {
        super(false, tagNo, new BERConstructedSequence());
    }

    /* access modifiers changed from: package-private */
    public void encode(DEROutputStream out) throws IOException {
        Enumeration e;
        if ((out instanceof ASN1OutputStream) || (out instanceof BEROutputStream)) {
            out.write(this.tagNo | 160);
            out.write(128);
            if (!this.empty) {
                if (this.explicit) {
                    out.writeObject(this.obj);
                } else if (this.obj instanceof ASN1OctetString) {
                    if (this.obj instanceof BERConstructedOctetString) {
                        e = ((BERConstructedOctetString) this.obj).getObjects();
                    } else {
                        e = new BERConstructedOctetString(((ASN1OctetString) this.obj).getOctets()).getObjects();
                    }
                    while (e.hasMoreElements()) {
                        out.writeObject(e.nextElement());
                    }
                } else if (this.obj instanceof ASN1Sequence) {
                    Enumeration e2 = ((ASN1Sequence) this.obj).getObjects();
                    while (e2.hasMoreElements()) {
                        out.writeObject(e2.nextElement());
                    }
                } else if (this.obj instanceof ASN1Set) {
                    Enumeration e3 = ((ASN1Set) this.obj).getObjects();
                    while (e3.hasMoreElements()) {
                        out.writeObject(e3.nextElement());
                    }
                } else {
                    throw new RuntimeException("not implemented: " + this.obj.getClass().getName());
                }
            }
            out.write(0);
            out.write(0);
            return;
        }
        super.encode(out);
    }
}
