package cn.com.jit.ida.util.pki.cipher.param;

public class GenKeyAttribute {
    public boolean IsExport;
    public int KeyNum;
    private boolean isNotExportGenKey = false;
    public int nIsSign = 1;

    public boolean getIsNotExportGenKey() {
        return this.isNotExportGenKey;
    }

    public void SetIsNotExportGenKey(boolean value) {
        this.isNotExportGenKey = value;
    }

    public boolean getIsExport() {
        return this.IsExport;
    }

    public int getKeyNum() {
        return this.KeyNum;
    }

    public void setIsExport(boolean IsExport2) {
        this.IsExport = IsExport2;
    }

    public void setKeyNum(int KeyNum2) {
        this.KeyNum = KeyNum2;
    }

    public void setIsSign(int nIsSign2) {
        this.nIsSign = nIsSign2;
    }

    public int getIsSign() {
        return this.nIsSign;
    }
}
