package cn.com.jit.ida.util.pki.asn1;

import cn.com.jit.ida.util.pki.PKIException;
import java.io.IOException;
import org.w3c.dom.Node;

public class DEROctetString extends ASN1OctetString {
    public DEROctetString(byte[] string) {
        super(string);
    }

    public DEROctetString(DEREncodable obj) {
        super(obj);
    }

    /* access modifiers changed from: package-private */
    public void encode(DEROutputStream out) throws IOException {
        out.writeEncoded(4, this.string);
    }

    public DEROctetString(Node nl) throws PKIException {
        super(nl);
    }
}
