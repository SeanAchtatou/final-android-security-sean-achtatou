package cn.com.jit.ida.util.pki.extension;

import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.x509.X509Extensions;

public class IssuerAlternativeNamesExt extends GeneralNamesExt {
    public IssuerAlternativeNamesExt() {
        this.OID = X509Extensions.IssuerAlternativeName.getId();
    }

    public IssuerAlternativeNamesExt(ASN1Sequence asn1Sequence) {
        super(asn1Sequence);
        this.OID = X509Extensions.SubjectAlternativeName.getId();
    }
}
