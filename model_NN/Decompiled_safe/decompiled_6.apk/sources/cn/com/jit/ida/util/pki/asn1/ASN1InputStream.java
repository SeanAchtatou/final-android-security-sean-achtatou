package cn.com.jit.ida.util.pki.asn1;

import de.innosystec.unrar.unpack.decode.Compress;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

public class ASN1InputStream extends DERInputStream {
    private DERObject END_OF_STREAM = new DERObject() {
        /* access modifiers changed from: package-private */
        public void encode(DEROutputStream out) throws IOException {
            throw new IOException("Eeek!");
        }
    };
    boolean eofFound = false;
    long insLen = 0;

    public ASN1InputStream(InputStream is) {
        super(is);
        try {
            this.insLen = (long) is.available();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public int readLength() throws IOException {
        int length = read();
        if (length < 0) {
            throw new IOException("EOF found when length expected");
        } else if (length == 128) {
            return -1;
        } else {
            if (length > 127) {
                int size = length & 127;
                length = 0;
                for (int i = 0; i < size; i++) {
                    int next = read();
                    if (next < 0) {
                        throw new IOException("EOF found reading length");
                    }
                    length = (length << 8) + next;
                }
            }
            return length;
        }
    }

    /* access modifiers changed from: protected */
    public void readFully(byte[] bytes) throws IOException {
        int left = bytes.length;
        if (left != 0) {
            do {
                int len = read(bytes, bytes.length - left, left);
                if (len > 0) {
                    left -= len;
                } else if (left != 0) {
                    throw new EOFException("EOF encountered in middle of object");
                } else {
                    return;
                }
            } while (left != 0);
        }
    }

    /* access modifiers changed from: protected */
    public DERObject buildObject(int tag, byte[] bytes) throws IOException {
        if ((tag & 64) != 0) {
            return new DERApplicationSpecific(tag, bytes);
        }
        switch (tag) {
            case 1:
                return new DERBoolean(bytes);
            case 2:
                return new DERInteger(bytes);
            case 3:
                byte b = bytes[0];
                byte[] data = new byte[(bytes.length - 1)];
                System.arraycopy(bytes, 1, data, 0, bytes.length - 1);
                return new DERBitString(data, b);
            case 4:
                return new DEROctetString(bytes);
            case 5:
                return new DERNull();
            case 6:
                return new DERObjectIdentifier(bytes);
            case 10:
                return new DEREnumerated(bytes);
            case 12:
                return new DERUTF8String(bytes);
            case 19:
                return new DERPrintableString(bytes);
            case 20:
                return new DERT61String(bytes);
            case 22:
                return new DERIA5String(bytes);
            case 23:
                return new DERUTCTime(bytes);
            case 24:
                return new DERGeneralizedTime(bytes);
            case 26:
                return new DERVisibleString(bytes);
            case 27:
                return new DERGeneralString(bytes);
            case 28:
                return new DERUniversalString(bytes);
            case 30:
                return new DERBMPString(bytes);
            case Compress.DC20 /*48*/:
                ASN1InputStream aIn = new ASN1InputStream(new ByteArrayInputStream(bytes));
                ASN1EncodableVector v = new ASN1EncodableVector();
                for (DERObject obj = aIn.readObject(); obj != null; obj = aIn.readObject()) {
                    v.add(obj);
                }
                return new DERSequence(v);
            case 49:
                ASN1InputStream aIn2 = new ASN1InputStream(new ByteArrayInputStream(bytes));
                ASN1EncodableVector v2 = new ASN1EncodableVector();
                for (DERObject obj2 = aIn2.readObject(); obj2 != null; obj2 = aIn2.readObject()) {
                    v2.add(obj2);
                }
                return new DERSet(v2);
            default:
                if ((tag & 128) == 0) {
                    return new DERUnknownTag(tag, bytes);
                }
                int tagNo = tag & 31;
                if (tagNo == 31) {
                    int idx = 0;
                    int tagNo2 = 0;
                    while ((bytes[idx] & 128) != 0) {
                        tagNo2 = (tagNo2 | (bytes[idx] & 127)) << 7;
                        idx++;
                    }
                    tagNo = tagNo2 | (bytes[idx] & 127);
                    byte[] tmp = bytes;
                    bytes = new byte[(tmp.length - (idx + 1))];
                    System.arraycopy(tmp, idx + 1, bytes, 0, bytes.length);
                }
                if (bytes.length == 0) {
                    if ((tag & 32) == 0) {
                        return new DERTaggedObject(false, tagNo, new DERNull());
                    }
                    return new DERTaggedObject(false, tagNo, new DERSequence());
                } else if ((tag & 32) == 0) {
                    return new DERTaggedObject(false, tagNo, new DEROctetString(bytes));
                } else {
                    ASN1InputStream aIn3 = new ASN1InputStream(new ByteArrayInputStream(bytes));
                    DEREncodable dObj = aIn3.readObject();
                    if (aIn3.available() == 0) {
                        return new DERTaggedObject(tagNo, dObj);
                    }
                    ASN1EncodableVector v3 = new ASN1EncodableVector();
                    while (dObj != null) {
                        v3.add(dObj);
                        dObj = aIn3.readObject();
                    }
                    return new DERTaggedObject(false, tagNo, new DERSequence(v3));
                }
        }
    }

    private byte[] readIndefiniteLengthFully() throws IOException {
        ByteArrayOutputStream bOut = new ByteArrayOutputStream();
        int b1 = read();
        while (true) {
            int b = read();
            if (b >= 0 && (b1 != 0 || b != 0)) {
                bOut.write(b1);
                b1 = b;
            }
        }
        return bOut.toByteArray();
    }

    private BERConstructedOctetString buildConstructedOctetString() throws IOException {
        Vector octs = new Vector();
        while (true) {
            DERObject o = readObject();
            if (o == this.END_OF_STREAM) {
                return new BERConstructedOctetString(octs);
            }
            octs.addElement(o);
        }
    }

    public DERObject readObject() throws IOException {
        int tag = read();
        if (tag != -1) {
            int length = readLength();
            if (((long) length) >= this.insLen) {
                throw new IOException("data length invalid");
            } else if (length < 0) {
                switch (tag) {
                    case 5:
                        return new BERNull();
                    case 36:
                        return buildConstructedOctetString();
                    case Compress.DC20 /*48*/:
                        ASN1EncodableVector v = new ASN1EncodableVector();
                        while (true) {
                            DERObject obj = readObject();
                            if (obj == this.END_OF_STREAM) {
                                return new BERSequence(v);
                            }
                            v.add(obj);
                        }
                    case 49:
                        ASN1EncodableVector v2 = new ASN1EncodableVector();
                        while (true) {
                            DERObject obj2 = readObject();
                            if (obj2 == this.END_OF_STREAM) {
                                return new BERSet(v2);
                            }
                            v2.add(obj2);
                        }
                    default:
                        if ((tag & 128) != 0) {
                            int tagNo = tag & 31;
                            if (tagNo == 31) {
                                int b = read();
                                int tagNo2 = 0;
                                while (b >= 0 && (b & 128) != 0) {
                                    tagNo2 = (tagNo2 | (b & 127)) << 7;
                                    b = read();
                                }
                                tagNo = tagNo2 | (b & 127);
                            }
                            if ((tag & 32) == 0) {
                                return new BERTaggedObject(false, tagNo, new DEROctetString(readIndefiniteLengthFully()));
                            }
                            DERObject dObj = readObject();
                            if (dObj == this.END_OF_STREAM) {
                                return new DERTaggedObject(tagNo);
                            }
                            DERObject next = readObject();
                            if (next == this.END_OF_STREAM) {
                                return new BERTaggedObject(tagNo, dObj);
                            }
                            ASN1EncodableVector v3 = new ASN1EncodableVector();
                            v3.add(dObj);
                            do {
                                v3.add(next);
                                next = readObject();
                            } while (next != this.END_OF_STREAM);
                            return new BERTaggedObject(false, tagNo, new BERSequence(v3));
                        }
                        throw new IOException("unknown BER object encountered");
                }
            } else if (tag == 0 && length == 0) {
                return this.END_OF_STREAM;
            } else {
                byte[] bytes = new byte[length];
                readFully(bytes);
                return buildObject(tag, bytes);
            }
        } else if (this.eofFound) {
            throw new EOFException("attempt to read past end of file.");
        } else {
            this.eofFound = true;
            return null;
        }
    }
}
