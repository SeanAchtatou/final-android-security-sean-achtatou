package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.ASN1Encodable;
import cn.com.jit.ida.util.pki.asn1.DERBMPString;
import cn.com.jit.ida.util.pki.asn1.DERIA5String;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERString;
import cn.com.jit.ida.util.pki.asn1.DERUTF8String;
import cn.com.jit.ida.util.pki.asn1.DERVisibleString;

public class DisplayText extends ASN1Encodable {
    public static final int CONTENT_TYPE_BMPSTRING = 1;
    public static final int CONTENT_TYPE_IA5STRING = 0;
    public static final int CONTENT_TYPE_UTF8STRING = 2;
    public static final int CONTENT_TYPE_VISIBLESTRING = 3;
    public static final int DISPLAY_TEXT_MAXIMUM_SIZE = 200;
    int contentType;
    DERString contents;

    public DisplayText(int type, String text) {
        text = text.length() > 200 ? text.substring(0, 200) : text;
        this.contentType = type;
        switch (type) {
            case 0:
                this.contents = new DERIA5String(text);
                return;
            case 1:
                this.contents = new DERBMPString(text);
                return;
            case 2:
                this.contents = new DERUTF8String(text);
                return;
            case 3:
                this.contents = new DERVisibleString(text);
                return;
            default:
                this.contents = new DERUTF8String(text);
                return;
        }
    }

    public DisplayText(String text) {
        text = text.length() > 200 ? text.substring(0, 200) : text;
        this.contentType = 2;
        this.contents = new DERUTF8String(text);
    }

    public DisplayText(DERString de2) {
        this.contents = de2;
    }

    public static DisplayText getInstance(Object de2) {
        if (de2 instanceof DERString) {
            return new DisplayText((DERString) de2);
        }
        if (de2 instanceof DisplayText) {
            return (DisplayText) de2;
        }
        throw new IllegalArgumentException("illegal object in getInstance");
    }

    public DERObject toASN1Object() {
        return (DERObject) this.contents;
    }

    public String getString() {
        return this.contents.getString();
    }
}
