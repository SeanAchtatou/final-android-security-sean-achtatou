package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.ASN1Encodable;
import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.DERTaggedObject;
import cn.com.jit.ida.util.pki.asn1.DERUTF8String;
import com.jianq.misc.StringEx;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class RoleSyntax extends ASN1Encodable {
    public static final String JIT_ROLE_OTHER_NAME_ID = "1.2.156.1995.1999.2";
    private GeneralNames roleAuthority;
    private GeneralName roleName;

    public RoleSyntax() {
    }

    public static RoleSyntax getInstance(Object obj) {
        if (obj == null || (obj instanceof RoleSyntax)) {
            return (RoleSyntax) obj;
        }
        if (obj instanceof ASN1Sequence) {
            return new RoleSyntax((ASN1Sequence) obj);
        }
        throw new IllegalArgumentException("Unknown object in RoleSyntax factory.");
    }

    public RoleSyntax(GeneralNames roleAuthority2, GeneralName roleName2) {
        if (roleName2 == null) {
            throw new IllegalArgumentException("the role name MUST be non empty and MUST use the URI option of GeneralName");
        }
        this.roleAuthority = roleAuthority2;
        this.roleName = roleName2;
    }

    public RoleSyntax(GeneralName roleName2) {
        this(null, roleName2);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public RoleSyntax(String roleName2) {
        this(new GeneralName(new DERUTF8String(roleName2 == null ? StringEx.Empty : roleName2).getDERObject(), 6));
    }

    public RoleSyntax(ASN1Sequence seq) {
        for (int i = 0; i != seq.size(); i++) {
            ASN1TaggedObject taggedObject = (ASN1TaggedObject) seq.getObjectAt(i);
            switch (taggedObject.getTagNo()) {
                case 0:
                    this.roleAuthority = GeneralNames.getInstance(taggedObject.getObject());
                    break;
                case 1:
                    this.roleName = GeneralName.getInstance(taggedObject, true);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown tag in RoleSyntax");
            }
        }
    }

    public GeneralNames getRoleAuthority() {
        return this.roleAuthority;
    }

    public GeneralName getRoleName() {
        return this.roleName;
    }

    public String getRoleNameAsString() {
        return GeneralName.GeneralNameToString((ASN1TaggedObject) this.roleName.getDERObject());
    }

    public String[] getRoleAuthorityAsString() {
        if (this.roleAuthority == null) {
            return new String[0];
        }
        int count = this.roleAuthority.getNames().length;
        String[] namesString = new String[count];
        for (int i = 0; i < count; i++) {
            namesString[i] = this.roleAuthority.getNames()[i].toString();
        }
        return namesString;
    }

    public DERObject toASN1Object() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        if (this.roleAuthority != null) {
            v.add(new DERTaggedObject(true, 0, this.roleAuthority.getDERObject()));
        }
        v.add(new DERTaggedObject(true, 1, this.roleName));
        return new DERSequence(v);
    }

    public String toString() {
        StringBuffer buff = new StringBuffer("Name: " + getRoleNameAsString() + " - Auth: ");
        if (this.roleAuthority == null || this.roleAuthority.getNames().length == 0) {
            buff.append("N/A");
        } else {
            String[] names = getRoleAuthorityAsString();
            buff.append("[" + names[0]);
            for (int i = 1; i < names.length; i++) {
                buff.append(", " + names[i]);
            }
            buff.append("]");
        }
        return buff.toString();
    }

    public void setRoleAuthority(GeneralNames roleAuthority2) {
        this.roleAuthority = roleAuthority2;
    }

    public void setRoleName(GeneralName roleName2) {
        this.roleName = roleName2;
    }

    public RoleSyntax(Node nl) throws PKIException {
        NodeList nodelist = nl.getChildNodes();
        if (nodelist.getLength() == 0) {
            throw new PKIException("XML内容缺失,RoleAttribute.RoleAttribute(),RoleAttribute没有子节点");
        }
        for (int i = 0; i < nodelist.getLength(); i++) {
            Node cnode = nodelist.item(i);
            String sname = cnode.getNodeName();
            if (sname.equals("RoleAuthority")) {
                this.roleAuthority = new GeneralNames(cnode);
            }
            if (sname.equals("roleName")) {
                this.roleName = new GeneralName(cnode);
            }
        }
    }
}
