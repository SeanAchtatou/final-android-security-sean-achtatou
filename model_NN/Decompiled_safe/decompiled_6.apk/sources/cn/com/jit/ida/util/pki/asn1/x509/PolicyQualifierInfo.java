package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.ASN1Encodable;
import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERIA5String;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DERSequence;

public class PolicyQualifierInfo extends ASN1Encodable {
    DERObjectIdentifier policyQualifierId;
    DEREncodable qualifier;

    public PolicyQualifierInfo(DERObjectIdentifier policyQualifierId2, DEREncodable qualifier2) {
        this.policyQualifierId = policyQualifierId2;
        this.qualifier = qualifier2;
    }

    public PolicyQualifierInfo(String cps) {
        this.policyQualifierId = PolicyQualifierId.id_qt_cps;
        this.qualifier = new DERIA5String(cps);
    }

    public PolicyQualifierInfo(ASN1Sequence as) {
        this.policyQualifierId = (DERObjectIdentifier) as.getObjectAt(0);
        this.qualifier = as.getObjectAt(1);
    }

    public static PolicyQualifierInfo getInstance(Object as) {
        if (as instanceof PolicyQualifierInfo) {
            return (PolicyQualifierInfo) as;
        }
        if (as instanceof ASN1Sequence) {
            return new PolicyQualifierInfo((ASN1Sequence) as);
        }
        throw new IllegalArgumentException("unknown object in getInstance.");
    }

    public DERObject toASN1Object() {
        ASN1EncodableVector dev = new ASN1EncodableVector();
        dev.add(this.policyQualifierId);
        dev.add(this.qualifier);
        return new DERSequence(dev);
    }

    public String getPolicyQualifierId() {
        return this.policyQualifierId.getId();
    }

    public boolean isCPSType() {
        return this.policyQualifierId.getId().equals("1.3.6.1.5.5.7.2.1");
    }

    public String getQualifierForCPS() {
        return ((DERIA5String) this.qualifier).getString();
    }

    public UserNotice getQualifierForUserNotice() {
        return new UserNotice((ASN1Sequence) this.qualifier);
    }
}
