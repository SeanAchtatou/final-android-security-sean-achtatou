package cn.com.jit.ida.util.pki.asn1.pkcs.pkcs7;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1Set;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERSequence;

public class EnvelopedData implements DEREncodable {
    private EncryptedContentInfo encryptedContentInfo;
    private ASN1Set recipientInfos;
    private DERInteger version;

    public static EnvelopedData getInstance(Object o) {
        if (o == null || (o instanceof EnvelopedData)) {
            return (EnvelopedData) o;
        }
        if (o instanceof ASN1Sequence) {
            return new EnvelopedData((ASN1Sequence) o);
        }
        throw new IllegalArgumentException("unknown object in factory: " + o.getClass().getName());
    }

    public EnvelopedData(DERInteger _version, ASN1Set _recipientInfos, EncryptedContentInfo _encryptedContentInfo) {
        this.version = _version;
        this.recipientInfos = _recipientInfos;
        this.encryptedContentInfo = _encryptedContentInfo;
    }

    public EnvelopedData(ASN1Sequence seq) {
        this.version = (DERInteger) seq.getObjectAt(0);
        this.recipientInfos = (ASN1Set) seq.getObjectAt(1);
        this.encryptedContentInfo = EncryptedContentInfo.getInstance(seq.getObjectAt(2));
    }

    public DERInteger getVersion() {
        return this.version;
    }

    public ASN1Set getRecipientInfos() {
        return this.recipientInfos;
    }

    public EncryptedContentInfo getEncryptedContentInfo() {
        return this.encryptedContentInfo;
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(this.version);
        v.add(this.recipientInfos);
        v.add(this.encryptedContentInfo);
        return new DERSequence(v);
    }
}
