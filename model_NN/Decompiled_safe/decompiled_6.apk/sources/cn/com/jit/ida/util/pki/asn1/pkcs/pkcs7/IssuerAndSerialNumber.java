package cn.com.jit.ida.util.pki.asn1.pkcs.pkcs7;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.x509.X509Name;
import java.math.BigInteger;

public class IssuerAndSerialNumber implements DEREncodable {
    private DERInteger certSerialNumber;
    private X509Name name;

    public static IssuerAndSerialNumber getInstance(Object obj) {
        if (obj instanceof IssuerAndSerialNumber) {
            return (IssuerAndSerialNumber) obj;
        }
        if (obj instanceof ASN1Sequence) {
            return new IssuerAndSerialNumber((ASN1Sequence) obj);
        }
        throw new IllegalArgumentException("unknown object in factory");
    }

    public IssuerAndSerialNumber(ASN1Sequence seq) {
        this.name = X509Name.getInstance(seq.getObjectAt(0));
        this.certSerialNumber = (DERInteger) seq.getObjectAt(1);
    }

    public IssuerAndSerialNumber(X509Name name2, BigInteger certSerialNumber2) {
        this.name = name2;
        this.certSerialNumber = new DERInteger(certSerialNumber2);
    }

    public IssuerAndSerialNumber(X509Name name2, DERInteger certSerialNumber2) {
        this.name = name2;
        this.certSerialNumber = certSerialNumber2;
    }

    public X509Name getName() {
        return this.name;
    }

    public DERInteger getCertificateSerialNumber() {
        return this.certSerialNumber;
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(this.name);
        v.add(this.certSerialNumber);
        return new DERSequence(v);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof IssuerAndSerialNumber)) {
            return false;
        }
        IssuerAndSerialNumber tmp = getInstance(obj);
        if (!tmp.getCertificateSerialNumber().equals(getCertificateSerialNumber()) || !tmp.getName().equals(getName())) {
            return false;
        }
        return true;
    }
}
