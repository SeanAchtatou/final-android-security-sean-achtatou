package cn.com.jit.ida.util.pki.pkcs;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.Parser;
import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1InputStream;
import cn.com.jit.ida.util.pki.asn1.ASN1OctetString;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1Set;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.DERSet;
import cn.com.jit.ida.util.pki.asn1.pkcs.PKCSObjectIdentifiers;
import cn.com.jit.ida.util.pki.asn1.pkcs.pkcs7.ContentInfo;
import cn.com.jit.ida.util.pki.asn1.pkcs.pkcs7.DigestedData;
import cn.com.jit.ida.util.pki.asn1.pkcs.pkcs7.EncryptedContentInfo;
import cn.com.jit.ida.util.pki.asn1.pkcs.pkcs7.EncryptedData;
import cn.com.jit.ida.util.pki.asn1.pkcs.pkcs7.EnvelopedData;
import cn.com.jit.ida.util.pki.asn1.pkcs.pkcs7.IssuerAndSerialNumber;
import cn.com.jit.ida.util.pki.asn1.pkcs.pkcs7.RecipientInfo;
import cn.com.jit.ida.util.pki.asn1.pkcs.pkcs7.SignedAndEnvelopedData;
import cn.com.jit.ida.util.pki.asn1.pkcs.pkcs7.SignedData;
import cn.com.jit.ida.util.pki.asn1.pkcs.pkcs7.SignerInfo;
import cn.com.jit.ida.util.pki.asn1.x509.AlgorithmIdentifier;
import cn.com.jit.ida.util.pki.asn1.x509.CertificateList;
import cn.com.jit.ida.util.pki.asn1.x509.X509CertificateStructure;
import cn.com.jit.ida.util.pki.cert.X509Cert;
import cn.com.jit.ida.util.pki.cipher.JKey;
import cn.com.jit.ida.util.pki.cipher.Mechanism;
import cn.com.jit.ida.util.pki.cipher.Session;
import cn.com.jit.ida.util.pki.cipher.param.P7Param;
import cn.com.jit.ida.util.pki.crl.X509CRL;
import cn.com.jit.ida.util.pki.encoders.Base64;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class PKCS7 {
    public static final int PKCS7_CONTENT_INVALIDTYPE = 0;
    public static final int PKCS7_CONTENT_TYPE_DATA = 1;
    public static final int PKCS7_CONTENT_TYPE_DIGESTEDDATA = 5;
    public static final int PKCS7_CONTENT_TYPE_ENCRYPTEDDATA = 6;
    public static final int PKCS7_CONTENT_TYPE_ENVELOPEDDATA = 3;
    public static final int PKCS7_CONTENT_TYPE_SIGNEDANDENVELOPEDDATA = 4;
    public static final int PKCS7_CONTENT_TYPE_SIGNEDDATA = 2;
    private ContentInfo cntInfo = null;
    private byte[] data = null;
    private Boolean isSMP7 = false;
    private P7Param p7cnt = null;
    private Session session = null;

    public Boolean getIsSMP7() {
        return this.isSMP7;
    }

    public void setIsSMP7(Boolean isSMP72) {
        this.isSMP7 = isSMP72;
    }

    public PKCS7() {
    }

    public PKCS7(Session session2) {
        this.session = session2;
    }

    public void setSession(Session session2) {
        this.session = session2;
    }

    public PKCS7(ASN1Sequence seq) {
        this.cntInfo = new ContentInfo(seq);
    }

    public static PKCS7 getInstance(Object o) throws PKIException {
        if (o instanceof PKCS7) {
            return (PKCS7) o;
        }
        if (o instanceof ASN1Sequence) {
            return new PKCS7((ASN1Sequence) o);
        }
        throw new IllegalArgumentException("unknown object");
    }

    public int load(String fileName) throws PKIException {
        try {
            FileInputStream fin = new FileInputStream(fileName);
            byte[] data2 = new byte[fin.available()];
            fin.read(data2);
            fin.close();
            return load(data2);
        } catch (Exception ex) {
            throw new PKIException(PKIException.P7_LOAD_ERR, PKIException.P7_LOAD_ERR_DES, ex);
        }
    }

    public int load(InputStream ins) throws PKIException {
        try {
            ASN1InputStream ais = new ASN1InputStream(ins);
            byte[] data2 = new byte[ais.available()];
            ais.read(data2);
            ais.close();
            ins.close();
            return load(data2);
        } catch (Exception ex) {
            throw new PKIException(PKIException.P7_LOAD_ERR, PKIException.P7_LOAD_ERR_DES, ex);
        }
    }

    public int load(byte[] data2) throws PKIException {
        if (Parser.isBase64Encode(data2)) {
            data2 = Base64.decode(Parser.convertBase64(data2));
        }
        try {
            ByteArrayInputStream bis = new ByteArrayInputStream(data2);
            ASN1InputStream ais = new ASN1InputStream(bis);
            this.cntInfo = new ContentInfo((ASN1Sequence) ais.readObject());
            ais.close();
            bis.close();
            return ParserCnt(this.cntInfo);
        } catch (Exception ex) {
            throw new PKIException(PKIException.P7_LOAD_ERR, PKIException.P7_LOAD_ERR_DES, ex);
        }
    }

    public P7Param GetP7Cnt() throws PKIException {
        return this.p7cnt;
    }

    public int GetType() throws PKIException {
        if (this.cntInfo == null) {
            throw new PKIException(PKIException.P7_PARSER_ERR, PKIException.P7_PARSER_ERR_DES);
        }
        DERObjectIdentifier cntType = this.cntInfo.getContentType();
        if (cntType == null) {
            throw new PKIException(PKIException.P7_PARSER_ERR, PKIException.P7_PARSER_ERR_DES);
        } else if (cntType.equals(PKCSObjectIdentifiers.data) || cntType.equals(PKCSObjectIdentifiers.gm_PKCS7_data)) {
            return 1;
        } else {
            if (cntType.equals(PKCSObjectIdentifiers.signedData) || cntType.equals(PKCSObjectIdentifiers.gm_PKCS7_signedData)) {
                return 2;
            }
            if (cntType.equals(PKCSObjectIdentifiers.envelopedData) || cntType.equals(PKCSObjectIdentifiers.gm_PKCS7_envelopedData)) {
                return 3;
            }
            if (cntType.equals(PKCSObjectIdentifiers.signedAndEnvelopedData) || cntType.equals(PKCSObjectIdentifiers.gm_PKCS7_signedAndEnvelopedData)) {
                return 4;
            }
            if (cntType.equals(PKCSObjectIdentifiers.digestedData)) {
                return 5;
            }
            if (cntType.equals(PKCSObjectIdentifiers.encryptedData) || cntType.equals(PKCSObjectIdentifiers.gm_PKCS7_encryptedData)) {
                return 6;
            }
            return 0;
        }
    }

    public DERObjectIdentifier GetTypeOid(int lType) throws PKIException {
        if (this.isSMP7.booleanValue()) {
            if (1 == lType) {
                return PKCSObjectIdentifiers.gm_PKCS7_data;
            }
            if (2 == lType) {
                return PKCSObjectIdentifiers.gm_PKCS7_signedData;
            }
            if (3 == lType) {
                return PKCSObjectIdentifiers.gm_PKCS7_envelopedData;
            }
            if (4 == lType) {
                return PKCSObjectIdentifiers.gm_PKCS7_signedAndEnvelopedData;
            }
            if (5 == lType) {
                return PKCSObjectIdentifiers.digestedData;
            }
            if (6 == lType) {
                return PKCSObjectIdentifiers.gm_PKCS7_encryptedData;
            }
        } else if (1 == lType) {
            return PKCSObjectIdentifiers.data;
        } else {
            if (2 == lType) {
                return PKCSObjectIdentifiers.signedData;
            }
            if (3 == lType) {
                return PKCSObjectIdentifiers.envelopedData;
            }
            if (4 == lType) {
                return PKCSObjectIdentifiers.signedAndEnvelopedData;
            }
            if (5 == lType) {
                return PKCSObjectIdentifiers.digestedData;
            }
            if (6 == lType) {
                return PKCSObjectIdentifiers.encryptedData;
            }
        }
        return null;
    }

    public DERObject genP7_DataObj(byte[] data2) throws PKIException {
        this.cntInfo = GetCnt(GetTypeOid(1), data2);
        if (this.cntInfo == null) {
            return null;
        }
        return this.cntInfo.getDERObject();
    }

    public byte[] genP7_Data(byte[] data2) throws PKIException {
        return Parser.writeDERObj2Bytes(genP7_DataObj(data2));
    }

    public DERObject genP7_SignObj(byte[] data2, P7Param[] param) throws PKIException {
        if (this.session == null) {
            throw new PKIException(PKIException.P7_PARAM_ERR, PKIException.P7_PARAM_ERR_DES);
        }
        try {
            ContentInfo dataCnt = GetCnt(GetTypeOid(1), data2);
            ASN1EncodableVector vdigMech = new ASN1EncodableVector();
            ASN1EncodableVector vCert = new ASN1EncodableVector();
            ASN1EncodableVector vCrl = new ASN1EncodableVector();
            ASN1EncodableVector vSignInfo = new ASN1EncodableVector();
            for (int i = 0; i < param.length; i++) {
                if (param[i].GetPrvKey() == null || param[i].GetSignMech() == null) {
                    throw new PKIException(PKIException.P7_PARAM_ERR, PKIException.P7_PARAM_ERR_DES);
                }
                AlgorithmIdentifier algoDig = new AlgorithmIdentifier(Sign2DigOid(param[i].GetSignMech()));
                DERObject[] GetAuthData = param[i].GetAuthData();
                vdigMech.add(algoDig);
                byte[] sign = this.session.sign(param[i].GetSignMech(), param[i].GetPrvKey(), data2);
                if (sign == null) {
                    throw new PKIException(PKIException.P7_GENERATE_ERR, PKIException.P7_GENERATE_ERR_DES);
                }
                vSignInfo.add(GetSignerInfo(param[i], sign));
                X509Cert[] x509certs = param[i].GetCerts();
                int j = 0;
                while (x509certs != null && j < x509certs.length) {
                    vCert.add(x509certs[j].getCertStructure());
                    j++;
                }
                X509CRL[] x509crls = param[i].GetCrls();
                int k = 0;
                while (x509crls != null && k < x509crls.length) {
                    vCrl.add(x509crls[k].getCertificateList());
                    k++;
                }
            }
            DERSet algos = new DERSet(vdigMech);
            DERSet infos = new DERSet(vSignInfo);
            DERSet certs = null;
            if (vCert.size() != 0) {
                certs = new DERSet(vCert);
            }
            DERSet crls = null;
            if (vCrl.size() != 0) {
                crls = new DERSet(vCrl);
            }
            this.cntInfo = GetCnt(GetTypeOid(2), new SignedData(new DERInteger(1), algos, dataCnt, certs, crls, infos));
            if (this.cntInfo == null) {
                return null;
            }
            return this.cntInfo.getDERObject();
        } catch (Exception ex) {
            throw new PKIException(PKIException.P7_GENERATE_ERR, PKIException.P7_GENERATE_ERR_DES, ex);
        }
    }

    public byte[] genP7_Sign(byte[] data2, P7Param[] param) throws PKIException {
        return Parser.writeDERObj2Bytes(genP7_SignObj(data2, param));
    }

    public DERObject genP7_EnvObj(byte[] data2, P7Param param) throws PKIException {
        if (this.session == null) {
            throw new PKIException(PKIException.P7_PARAM_ERR, PKIException.P7_PARAM_ERR_DES);
        }
        X509Cert[] certs = param.GetCerts();
        Mechanism[] mechs = param.GetEnvMech();
        ASN1EncodableVector vInfo = new ASN1EncodableVector();
        for (int i = 0; i < certs.length; i++) {
            vInfo.add(GetRecInfo(certs[i], mechs[i], this.session.encrypt(mechs[i], certs[i].getPublicKey(), param.GetEncKey().getKey())));
        }
        DERSet infos = new DERSet(vInfo);
        DERInteger ver = new DERInteger(0);
        byte[] encedData = null;
        if (data2 != null) {
            encedData = this.session.encrypt(param.GetEncMech(), param.GetEncKey(), data2);
        }
        this.cntInfo = GetCnt(GetTypeOid(3), new EnvelopedData(ver, infos, GetEncedCntInfo(encedData, param.GetEncMech())));
        if (this.cntInfo == null) {
            return null;
        }
        return this.cntInfo.getDERObject();
    }

    public byte[] genP7_Env(byte[] data2, P7Param param) throws PKIException {
        return Parser.writeDERObj2Bytes(genP7_EnvObj(data2, param));
    }

    public byte[] genP7_SignEnv(byte[] data2, P7Param[] param) throws PKIException {
        return Parser.writeDERObj2Bytes(genP7_SignEnvObj(data2, param));
    }

    public DERObject genP7_SignEnvObj(byte[] data2, P7Param[] param) throws PKIException {
        if (this.session == null) {
            throw new PKIException(PKIException.P7_PARAM_ERR, PKIException.P7_PARAM_ERR_DES);
        }
        ASN1EncodableVector vdigMech = new ASN1EncodableVector();
        ASN1EncodableVector vCert = new ASN1EncodableVector();
        ASN1EncodableVector vCrl = new ASN1EncodableVector();
        ASN1EncodableVector vSignInfo = new ASN1EncodableVector();
        ASN1EncodableVector vInfo = new ASN1EncodableVector();
        for (int i = 0; i < param.length; i++) {
            if (param[i].GetPrvKey() == null || param[i].GetSignMech() == null) {
                throw new PKIException(PKIException.P7_PARAM_ERR, PKIException.P7_PARAM_ERR_DES);
            }
            AlgorithmIdentifier algoDig = new AlgorithmIdentifier(Sign2DigOid(param[i].GetSignMech()));
            DERObject[] GetAuthData = param[i].GetAuthData();
            vdigMech.add(algoDig);
            byte[] sign = this.session.sign(param[i].GetSignMech(), param[i].GetPrvKey(), data2);
            if (sign == null) {
                throw new PKIException(PKIException.P7_GENERATE_ERR, PKIException.P7_GENERATE_ERR_DES);
            }
            vSignInfo.add(GetSignerInfo(param[i], sign));
            X509Cert[] x509certs = param[i].GetCerts();
            Mechanism[] mechs = param[i].GetEnvMech();
            if (x509certs != null) {
                byte[] encedKey = this.session.encrypt(mechs[0], x509certs[0].getPublicKey(), param[i].GetEncKey().getKey());
                vInfo.add(GetRecInfo(x509certs[0], param[i].GetEncMech(), encedKey));
            }
            int j = 0;
            while (x509certs != null && j < x509certs.length) {
                vCert.add(x509certs[j].getCertStructure());
                j++;
            }
            X509CRL[] x509crls = param[i].GetCrls();
            int k = 0;
            while (x509crls != null && k < x509crls.length) {
                vCrl.add(x509crls[k].getCertificateList());
                k++;
            }
        }
        DERSet algos = new DERSet(vdigMech);
        DERSet infos = new DERSet(vSignInfo);
        DERSet certs = null;
        if (vCert.size() != 0) {
            certs = new DERSet(vCert);
        }
        DERSet crls = null;
        if (vCrl.size() != 0) {
            crls = new DERSet(vCrl);
        }
        this.cntInfo = GetCnt(GetTypeOid(4), new SignedAndEnvelopedData(new DERInteger(1), new DERSet(vInfo), algos, GetEncedCntInfo(this.session.encrypt(param[0].GetEncMech(), param[0].GetEncKey(), data2), param[0].GetEncMech()), certs, crls, infos));
        if (this.cntInfo == null) {
            return null;
        }
        return this.cntInfo.getDERObject();
    }

    public byte[] genP7_Dig(byte[] data2, Mechanism digMech) throws PKIException {
        return Parser.writeDERObj2Bytes(genP7_DigObj(data2, digMech));
    }

    public DERObject genP7_DigObj(byte[] data2, Mechanism digMech) throws PKIException {
        if (this.session == null) {
            throw new PKIException(PKIException.P7_PARAM_ERR, PKIException.P7_PARAM_ERR_DES);
        }
        ContentInfo dataCnt = GetCnt(GetTypeOid(1), data2);
        this.cntInfo = GetCnt(GetTypeOid(5), new DigestedData(new DERInteger(0), new AlgorithmIdentifier(GetDigOid(digMech)), dataCnt, new DEROctetString(this.session.digest(digMech, data2))));
        if (this.cntInfo == null) {
            return null;
        }
        return this.cntInfo.getDERObject();
    }

    public byte[] genP7_Enc(byte[] data2, JKey key, Mechanism encMech) throws PKIException {
        return Parser.writeDERObj2Bytes(genP7_EncObj(data2, key, encMech));
    }

    public DERObject genP7_EncObj(byte[] data2, JKey key, Mechanism encMech) throws PKIException {
        if (this.session == null) {
            throw new PKIException(PKIException.P7_PARAM_ERR, PKIException.P7_PARAM_ERR_DES);
        }
        this.cntInfo = GetCnt(GetTypeOid(6), new EncryptedData(new DERInteger(0), GetEncedCntInfo(this.session.encrypt(encMech, key, data2), encMech)));
        if (this.cntInfo == null) {
            return null;
        }
        return this.cntInfo.getDERObject();
    }

    private int GetType(ContentInfo info) throws PKIException {
        if (info == null) {
            throw new PKIException(PKIException.P7_PARSER_ERR, PKIException.P7_PARSER_ERR_DES);
        }
        DERObjectIdentifier cntType = info.getContentType();
        if (cntType == null) {
            throw new PKIException(PKIException.P7_PARSER_ERR, PKIException.P7_PARSER_ERR_DES);
        } else if (cntType.equals(PKCSObjectIdentifiers.data)) {
            return 1;
        } else {
            if (cntType.equals(PKCSObjectIdentifiers.signedData)) {
                return 2;
            }
            if (cntType.equals(PKCSObjectIdentifiers.envelopedData)) {
                return 3;
            }
            if (cntType.equals(PKCSObjectIdentifiers.signedAndEnvelopedData)) {
                return 4;
            }
            if (cntType.equals(PKCSObjectIdentifiers.digestedData)) {
                return 5;
            }
            if (cntType.equals(PKCSObjectIdentifiers.encryptedData)) {
                return 6;
            }
            return 0;
        }
    }

    private ContentInfo GetCnt(DERObjectIdentifier type, byte[] data2) throws PKIException {
        DEROctetString cnt = null;
        if (data2 != null) {
            cnt = new DEROctetString(data2);
        }
        return new ContentInfo(type, cnt);
    }

    private ContentInfo GetCnt(DERObjectIdentifier type, DEREncodable data2) throws PKIException {
        return new ContentInfo(type, data2);
    }

    private DERObjectIdentifier Sign2DigOid(Mechanism signMech) throws PKIException {
        if (signMech == null) {
            return null;
        }
        String mech = signMech.getMechanismType().toString();
        if (mech.equals("SHA1withRSAEncryption") || mech.equals("SHA1withDSA") || mech.equals("SHA1withECDSA")) {
            return PKCSObjectIdentifiers.sha1;
        }
        if (mech.equals("SHA224withRSAEncryption") || mech.equals(Mechanism.SHA224_DSA) || mech.equals("SHA224withECDSA")) {
            return PKCSObjectIdentifiers.sha224;
        }
        if (mech.equals("SHA256withRSAEncryption") || mech.equals(Mechanism.SHA256_DSA) || mech.equals("SHA256withECDSA")) {
            return PKCSObjectIdentifiers.sha256;
        }
        if (mech.equals("SHA384withRSAEncryption")) {
            return PKCSObjectIdentifiers.sha384;
        }
        if (mech.equals("SHA512withRSAEncryption")) {
            return PKCSObjectIdentifiers.sha512;
        }
        if (mech.equals("MD2withRSAEncryption")) {
            return PKCSObjectIdentifiers.md2;
        }
        if (mech.equals("MD5withRSAEncryption")) {
            return PKCSObjectIdentifiers.md5;
        }
        if (mech.equals("SM3withSM2Encryption")) {
            return PKCSObjectIdentifiers.SM3;
        }
        throw new PKIException(PKIException.P7_INVALIDMECHTYPE_ERR, "本操作不支持此种机制类型 " + mech);
    }

    private DERObjectIdentifier GetDigOid(Mechanism digMech) throws PKIException {
        if (digMech == null) {
            return null;
        }
        if (digMech.getMechanismType().equals(Mechanism.MD2)) {
            return PKCSObjectIdentifiers.md2;
        }
        if (digMech.getMechanismType().equals(Mechanism.MD5)) {
            return PKCSObjectIdentifiers.md5;
        }
        if (digMech.getMechanismType().equals(Mechanism.SM3)) {
            return PKCSObjectIdentifiers.SM3;
        }
        if (digMech.getMechanismType().equals(Mechanism.SHA1)) {
            return PKCSObjectIdentifiers.sha1;
        }
        if (digMech.getMechanismType().equals(Mechanism.SHA224)) {
            return PKCSObjectIdentifiers.sha224;
        }
        if (digMech.getMechanismType().equals(Mechanism.SHA256)) {
            return PKCSObjectIdentifiers.sha256;
        }
        if (digMech.getMechanismType().equals(Mechanism.SHA384)) {
            return PKCSObjectIdentifiers.sha384;
        }
        throw new PKIException(PKIException.P7_INVALIDMECHTYPE_ERR, "本操作不支持此种机制类型 " + digMech);
    }

    private DERObjectIdentifier GetEncOid(Mechanism EncMech) throws PKIException {
        if (EncMech == null) {
            return null;
        }
        String mech = EncMech.getMechanismType().toString();
        if (mech.equals(Mechanism.RSA_PKCS)) {
            return PKCSObjectIdentifiers.rsaEncryption;
        }
        if (mech.equals(Mechanism.SM2_RAW)) {
            return PKCSObjectIdentifiers.SM2;
        }
        if (mech.equals(Mechanism.DES3_ECB)) {
            return PKCSObjectIdentifiers.des3Encryption;
        }
        if (mech.equals(Mechanism.DES3_CBC)) {
            return PKCSObjectIdentifiers.des3CBCEncryption;
        }
        if (mech.equals(Mechanism.DES_CBC)) {
            return PKCSObjectIdentifiers.desCBCEncryption;
        }
        if (mech.equals(Mechanism.DES_ECB)) {
            return PKCSObjectIdentifiers.desEncryption;
        }
        if (mech.equals(Mechanism.SCB2_ECB)) {
            return PKCSObjectIdentifiers.scb2Encryption;
        }
        if (mech.equals(Mechanism.SCB2_CBC)) {
            return PKCSObjectIdentifiers.scb2CBCEncryption;
        }
        if (mech.equals(Mechanism.RC2_CBC)) {
            return PKCSObjectIdentifiers.rc2CBCEncryption;
        }
        if (mech.equals(Mechanism.RC2_ECB)) {
            return PKCSObjectIdentifiers.rc2Encryption;
        }
        throw new PKIException(PKIException.P7_INVALIDMECHTYPE_ERR, "本操作不支持此种机制类型 " + mech);
    }

    private DERObjectIdentifier Sign2EncOid(Mechanism signMech) throws PKIException {
        String mech = signMech.getMechanismType().toString();
        if (mech.equals("SHA1withRSAEncryption") || mech.equals("SHA224withRSAEncryption") || mech.equals("SHA256withRSAEncryption") || mech.equals("SHA384withRSAEncryption") || mech.equals("SHA512withRSAEncryption") || mech.equals("MD2withRSAEncryption") || mech.equals("MD5withRSAEncryption")) {
            return PKCSObjectIdentifiers.rsaEncryption;
        }
        if (mech.equals("SM3withSM2Encryption")) {
            return PKCSObjectIdentifiers.SM2;
        }
        throw new PKIException(PKIException.P7_INVALIDMECHTYPE_ERR, "本操作不支持此种机制类型 " + mech);
    }

    private IssuerAndSerialNumber GetIssuerSN(X509Cert cert) throws PKIException {
        if (cert == null) {
            return null;
        }
        X509CertificateStructure x509cert = cert.getCertStructure();
        return new IssuerAndSerialNumber(x509cert.getIssuer(), x509cert.getSerialNumber());
    }

    private SignerInfo GetSignerInfo(P7Param param, byte[] signData) throws PKIException {
        DERInteger ver = new DERInteger(1);
        AlgorithmIdentifier algoDig = new AlgorithmIdentifier(Sign2DigOid(param.GetSignMech()));
        AlgorithmIdentifier algoEnc = new AlgorithmIdentifier(Sign2EncOid(param.GetSignMech()));
        X509Cert[] certs = param.GetCerts();
        DERObject[] auth = param.GetAuthData();
        DERObject[] unauth = param.GetunAuthData();
        IssuerAndSerialNumber issuerSN = GetIssuerSN(certs[0]);
        ASN1EncodableVector vauthData = new ASN1EncodableVector();
        ASN1EncodableVector vunauthData = new ASN1EncodableVector();
        int i = 0;
        while (auth != null && i < auth.length) {
            vauthData.add(auth[i]);
            i++;
        }
        int i2 = 0;
        while (unauth != null && i2 < unauth.length) {
            vunauthData.add(unauth[i2]);
            i2++;
        }
        DERSet authDatas = null;
        if (auth != null) {
            authDatas = new DERSet(vauthData);
        }
        DERSet unauthDatas = null;
        if (unauth != null) {
            unauthDatas = new DERSet(vunauthData);
        }
        return new SignerInfo(ver, issuerSN, algoDig, authDatas, algoEnc, new DEROctetString(signData), unauthDatas);
    }

    private EncryptedContentInfo GetEncedCntInfo(byte[] encedData, Mechanism mech) throws PKIException {
        AlgorithmIdentifier algoEnc = new AlgorithmIdentifier(GetEncOid(mech));
        DEROctetString cnt = null;
        if (encedData != null) {
            cnt = new DEROctetString(encedData);
        }
        return new EncryptedContentInfo(GetTypeOid(1), algoEnc, cnt);
    }

    private RecipientInfo GetRecInfo(X509Cert cert, Mechanism mech, byte[] encedKey) throws PKIException {
        AlgorithmIdentifier algo = new AlgorithmIdentifier(GetEncOid(mech));
        return new RecipientInfo(new DERInteger(0), GetIssuerSN(cert), algo, new DEROctetString(encedKey));
    }

    private int ParserCnt(ContentInfo info) throws PKIException {
        this.p7cnt = new P7Param();
        int nType = GetType(info);
        switch (nType) {
            case 1:
                DEROctetString oct = (DEROctetString) info.getContent();
                if (oct != null) {
                    this.data = oct.getOctets();
                    this.p7cnt.SetSource(this.data);
                    break;
                } else {
                    throw new PKIException(PKIException.P7_PARSER_ERR, PKIException.P7_PARSER_ERR_DES);
                }
            case 2:
                SignedData signData = SignedData.getInstance(info.getContent());
                if (signData.getContentInfo().getContent() != null) {
                    this.p7cnt.SetSource(((ASN1OctetString) signData.getContentInfo().getContent()).getOctets());
                }
                this.p7cnt.SetCerts(GetCerts(signData.getCertificates()));
                if (GetCrls(signData.getCRLs()) != null) {
                    this.p7cnt.SetCrls(GetCrls(signData.getCRLs()));
                }
                this.p7cnt.SetSignInfos(parserSignInfos(signData.getSignerInfos()));
                break;
            case 3:
                EnvelopedData envData = EnvelopedData.getInstance(info.getContent());
                List ls = parserEncedInfo(envData.getEncryptedContentInfo());
                this.p7cnt.SetEncedData((byte[]) ls.get(0));
                this.p7cnt.SetEncMech((String) ls.get(1));
                this.p7cnt.SetRecInfos(parserRecInfos(envData.getRecipientInfos()));
                break;
            case 4:
                SignedAndEnvelopedData senvData = SignedAndEnvelopedData.getInstance(info.getContent());
                this.p7cnt.SetCerts(GetCerts(senvData.getCertificates()));
                if (GetCrls(senvData.getCrls()) != null) {
                    this.p7cnt.SetCrls(GetCrls(senvData.getCrls()));
                }
                this.p7cnt.SetSignInfos(parserSignInfos(senvData.getSignerInfos()));
                List ls2 = parserEncedInfo(senvData.getEncryptedContentInfo());
                this.p7cnt.SetEncedData((byte[]) ls2.get(0));
                this.p7cnt.SetEncMech((String) ls2.get(1));
                this.p7cnt.SetRecInfos(parserRecInfos(senvData.getRecipientInfos()));
                break;
            case 5:
                DigestedData digData = DigestedData.getInstance(info.getContent());
                this.p7cnt.SetSource(((ASN1OctetString) digData.getContentInfo().getContent()).getOctets());
                List ls3 = new ArrayList();
                ls3.add(digData.getDigest().getOctets());
                ls3.add(GetDigMech(digData.getDigestAlgorithm()));
                this.p7cnt.SetDigInfo(ls3);
                break;
            case 6:
                List ls4 = parserEncedInfo(EncryptedData.getInstance(info.getContent()).getEncryptedContentInfo());
                this.p7cnt.SetEncedData((byte[]) ls4.get(0));
                this.p7cnt.SetEncMech((String) ls4.get(1));
                break;
            default:
                throw new PKIException(PKIException.P7_LOAD_ERR, "载入P7对象错误无效的p7类型");
        }
        return nType;
    }

    private X509Cert[] GetCerts(ASN1Set derSet) {
        if (derSet == null) {
            return null;
        }
        DERSet set = (DERSet) derSet;
        X509Cert[] certs = new X509Cert[set.size()];
        for (int i = 0; i < set.size(); i++) {
            certs[i] = new X509Cert(X509CertificateStructure.getInstance(set.getObjectAt(i)));
        }
        return certs;
    }

    private X509CRL[] GetCrls(ASN1Set derSet) {
        if (derSet == null) {
            return null;
        }
        DERSet set = (DERSet) derSet;
        X509CRL[] crls = new X509CRL[set.size()];
        for (int i = 0; i < set.size(); i++) {
            crls[i] = new X509CRL(CertificateList.getInstance(set.getObjectAt(i)));
        }
        return crls;
    }

    private String GetSignMech(AlgorithmIdentifier dig, AlgorithmIdentifier enc) throws PKIException {
        if (dig == null || enc == null) {
            return null;
        }
        String strDig = dig.getObjectId().getId();
        String strEnc = enc.getObjectId().getId();
        if (strEnc.equals(PKCSObjectIdentifiers.rsaEncryption.getId())) {
            if (strDig.equals(PKCSObjectIdentifiers.sha1.getId())) {
                return "SHA1withRSAEncryption";
            }
            if (strDig.equals(PKCSObjectIdentifiers.sha224.getId())) {
                return "SHA224withRSAEncryption";
            }
            if (strDig.equals(PKCSObjectIdentifiers.sha256.getId())) {
                return "SHA256withRSAEncryption";
            }
            if (strDig.equals(PKCSObjectIdentifiers.sha384.getId())) {
                return "SHA384withRSAEncryption";
            }
            if (strDig.equals(PKCSObjectIdentifiers.sha512.getId())) {
                return "SHA512withRSAEncryption";
            }
            if (strDig.equals(PKCSObjectIdentifiers.md2.getId())) {
                return "MD2withRSAEncryption";
            }
            if (strDig.equals(PKCSObjectIdentifiers.md5.getId())) {
                return "MD5withRSAEncryption";
            }
            throw new PKIException(PKIException.P7_PARSER_ERR, PKIException.NOT_SUP_DES);
        } else if (!strEnc.equals(PKCSObjectIdentifiers.SM2.getId())) {
            throw new PKIException(PKIException.P7_PARSER_ERR, PKIException.NOT_SUP_DES);
        } else if (strDig.equals(PKCSObjectIdentifiers.SM3.getId())) {
            return "SM3withSM2Encryption";
        } else {
            throw new PKIException(PKIException.P7_PARSER_ERR, PKIException.NOT_SUP_DES);
        }
    }

    private String GetDigMech(AlgorithmIdentifier dig) throws PKIException {
        if (dig == null) {
            return null;
        }
        String strDig = dig.getObjectId().getId();
        if (strDig.equals(PKCSObjectIdentifiers.sha1.getId())) {
            return Mechanism.SHA1;
        }
        if (strDig.equals(PKCSObjectIdentifiers.sha224.getId())) {
            return Mechanism.SHA224;
        }
        if (strDig.equals(PKCSObjectIdentifiers.sha256.getId())) {
            return Mechanism.SHA256;
        }
        if (strDig.equals(PKCSObjectIdentifiers.sha384.getId())) {
            return Mechanism.SHA384;
        }
        if (strDig.equals(PKCSObjectIdentifiers.sha512.getId())) {
            return Mechanism.SHA512;
        }
        if (strDig.equals(PKCSObjectIdentifiers.SM3.getId())) {
            return Mechanism.SM3;
        }
        if (strDig.equals(PKCSObjectIdentifiers.md2.getId())) {
            return Mechanism.MD2;
        }
        if (strDig.equals(PKCSObjectIdentifiers.md5.getId())) {
            return Mechanism.MD5;
        }
        throw new PKIException(PKIException.P7_PARSER_ERR, PKIException.NOT_SUP_DES);
    }

    private List[] parserSignInfos(ASN1Set derSet) throws PKIException {
        DERSet set = (DERSet) derSet;
        List[] ls = new ArrayList[set.size()];
        for (int i = 0; i < set.size(); i++) {
            SignerInfo info = SignerInfo.getInstance(set.getObjectAt(i));
            if (info == null) {
                throw new PKIException(PKIException.P7_PARSER_ERR, PKIException.P7_PARSER_ERR_DES);
            }
            ls[i] = new ArrayList();
            ls[i].add(info.getEncryptedDigest().getOctets());
            AlgorithmIdentifier dig = info.getDigestAlgorithm();
            AlgorithmIdentifier enc = info.getDigestEncryptionAlgorithm();
            IssuerAndSerialNumber iSN = info.getIssuerAndSerialNumber();
            if (GetSignMech(dig, enc) == null || iSN.getName() == null || iSN.getCertificateSerialNumber() == null) {
                throw new PKIException(PKIException.P7_PARSER_ERR, PKIException.P7_PARSER_ERR_DES);
            }
            ls[i].add(GetSignMech(dig, enc));
            ls[i].add(iSN.getName().toString());
            ls[i].add(iSN.getCertificateSerialNumber().getValue().toString(16));
        }
        return ls;
    }

    private String GetEncMech(AlgorithmIdentifier enc) throws PKIException {
        if (enc == null) {
            return null;
        }
        String strEnc = enc.getObjectId().getId();
        if (strEnc.equals(PKCSObjectIdentifiers.des3Encryption.getId())) {
            return Mechanism.DES3_ECB;
        }
        if (strEnc.equals(PKCSObjectIdentifiers.des3CBCEncryption.getId())) {
            return Mechanism.DES3_CBC;
        }
        if (strEnc.equals(PKCSObjectIdentifiers.desEncryption.getId())) {
            return Mechanism.DES_ECB;
        }
        if (strEnc.equals(PKCSObjectIdentifiers.desCBCEncryption.getId())) {
            return Mechanism.DES3_CBC;
        }
        if (strEnc.equals(PKCSObjectIdentifiers.scb2Encryption.getId())) {
            return Mechanism.SCB2_ECB;
        }
        if (strEnc.equals(PKCSObjectIdentifiers.scb2CBCEncryption.getId())) {
            return Mechanism.SCB2_CBC;
        }
        if (strEnc.equals(PKCSObjectIdentifiers.SM2.getId())) {
            return Mechanism.SM2_RAW;
        }
        if (strEnc.equals(PKCSObjectIdentifiers.rsaEncryption.getId())) {
            return Mechanism.RSA_PKCS;
        }
        throw new PKIException(PKIException.P7_PARSER_ERR, PKIException.NOT_SUP_DES);
    }

    private List parserEncedInfo(EncryptedContentInfo info) throws PKIException {
        List ls = new ArrayList();
        ls.add(info.getEncryptedContent().getOctets());
        ls.add(GetEncMech(info.getContentEncryptionAlgorithm()));
        return ls;
    }

    private List[] parserRecInfos(ASN1Set derSet) throws PKIException {
        DERSet set = (DERSet) derSet;
        List[] ls = new ArrayList[set.size()];
        for (int i = 0; i < set.size(); i++) {
            RecipientInfo info = RecipientInfo.getInstance(set.getObjectAt(i));
            if (info == null) {
                throw new PKIException(PKIException.P7_PARSER_ERR, PKIException.P7_PARSER_ERR_DES);
            }
            ls[i] = new ArrayList();
            ls[i].add(info.getEncryptedKey().getOctets());
            ls[i].add(GetEncMech(info.getKeyEncryptionAlgorithm()));
            IssuerAndSerialNumber iSN = info.getIssuerAndSerialNumber();
            if (iSN.getName() == null || iSN.getCertificateSerialNumber() == null) {
                throw new PKIException(PKIException.P7_PARSER_ERR, PKIException.P7_PARSER_ERR_DES);
            }
            ls[i].add(iSN.getName().toString());
            ls[i].add(iSN.getCertificateSerialNumber().getValue().toString(16));
        }
        return ls;
    }

    public static void main(String[] args) {
    }
}
