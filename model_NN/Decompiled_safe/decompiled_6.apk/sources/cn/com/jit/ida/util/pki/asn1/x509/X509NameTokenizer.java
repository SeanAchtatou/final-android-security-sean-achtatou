package cn.com.jit.ida.util.pki.asn1.x509;

public class X509NameTokenizer {
    private StringBuffer buf = new StringBuffer();
    private int index;
    private String oid;

    public X509NameTokenizer(String oid2) {
        this.oid = oid2;
        this.index = -1;
    }

    public boolean hasMoreTokens() {
        return this.index != this.oid.length();
    }

    public String nextToken() {
        if (this.index == this.oid.length()) {
            return null;
        }
        int end = this.index + 1;
        boolean quoted = false;
        boolean escaped = false;
        this.buf.setLength(0);
        while (end != this.oid.length()) {
            char c = this.oid.charAt(end);
            if (c == '\"') {
                if (!escaped) {
                    quoted = !quoted;
                } else {
                    this.buf.append(c);
                }
                escaped = false;
            } else if (escaped || quoted) {
                this.buf.append(c);
                escaped = false;
            } else if (c == '\\') {
                escaped = true;
            } else if (c == ',') {
                break;
            } else {
                this.buf.append(c);
            }
            end++;
        }
        this.index = end;
        return this.buf.toString().trim();
    }
}
