package cn.com.jit.ida.util.pki.extension;

import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1SequenceExt;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.x509.NoticeReference;
import cn.com.jit.ida.util.pki.asn1.x509.PolicyQualifierId;
import cn.com.jit.ida.util.pki.asn1.x509.PolicyQualifierInfo;
import cn.com.jit.ida.util.pki.asn1.x509.UserNotice;
import java.util.Vector;

public class PolicyInformationExt {
    private String id = null;
    private ASN1SequenceExt seqPolicyQualifierInfo = new ASN1SequenceExt();

    private PolicyInformationExt() {
    }

    public PolicyInformationExt(String value) {
        this.id = value;
    }

    public PolicyInformationExt(ASN1Sequence asn1Sequence) {
        this.id = ((DERObjectIdentifier) asn1Sequence.getObjectAt(0)).getId();
        ASN1Sequence tempSequence = (ASN1Sequence) asn1Sequence.getObjectAt(1);
        for (int i = 0; i < tempSequence.size(); i++) {
            this.seqPolicyQualifierInfo.addObject(new PolicyQualifierInfo((ASN1Sequence) tempSequence.getObjectAt(i)));
        }
    }

    public String GetID() {
        return this.id;
    }

    public int GetPolicyQualifierInfoCout() {
        return this.seqPolicyQualifierInfo.size();
    }

    public PolicyQualifierInfo GetPolicyQualifierInfo(int index) {
        return (PolicyQualifierInfo) this.seqPolicyQualifierInfo.getObjectAt(index);
    }

    public void addPolicyQualifierinfo(String value) {
        this.seqPolicyQualifierInfo.addObject(new PolicyQualifierInfo(value));
    }

    public void addPolicyQualifierinfo(String organization, Vector noticeNumbers) {
        addPolicyQualifierinfo(organization, noticeNumbers, null);
    }

    public void addPolicyQualifierinfo(String organization, Vector noticeNumbers, String explicitText) {
        ASN1SequenceExt tempPolicyQualifierinfo = new ASN1SequenceExt();
        tempPolicyQualifierinfo.addObject(PolicyQualifierId.id_qt_unotice);
        tempPolicyQualifierinfo.addObject(new UserNotice(new NoticeReference(organization, noticeNumbers), explicitText));
        this.seqPolicyQualifierInfo.addObject(new PolicyQualifierInfo(tempPolicyQualifierinfo));
    }

    public void addPolicyQualifierinfo(int type, String organization, Vector noticeNumbers, String explicitText) {
        ASN1SequenceExt tempPolicyQualifierinfo = new ASN1SequenceExt();
        tempPolicyQualifierinfo.addObject(PolicyQualifierId.id_qt_unotice);
        tempPolicyQualifierinfo.addObject(new UserNotice(type, new NoticeReference(organization, noticeNumbers), explicitText));
        this.seqPolicyQualifierInfo.addObject(new PolicyQualifierInfo(tempPolicyQualifierinfo));
    }

    public ASN1SequenceExt getSeqPolicyInformation() {
        ASN1SequenceExt tempSeq = new ASN1SequenceExt();
        tempSeq.addObject(new DERObjectIdentifier(this.id));
        tempSeq.addObject(this.seqPolicyQualifierInfo);
        return tempSeq;
    }
}
