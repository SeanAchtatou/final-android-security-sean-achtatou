package cn.com.jit.ida.util.pki.asn1.pkcs.pkcs7;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.BERSequence;
import cn.com.jit.ida.util.pki.asn1.BERTaggedObject;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DERTaggedObject;
import cn.com.jit.ida.util.pki.asn1.pkcs.PKCSObjectIdentifiers;
import java.util.Enumeration;

public class ContentInfo implements DEREncodable, PKCSObjectIdentifiers {
    private DEREncodable content = null;
    private DERObjectIdentifier contentType = null;

    public static ContentInfo getInstance(Object obj) {
        if (obj instanceof ContentInfo) {
            return (ContentInfo) obj;
        }
        if (obj instanceof ASN1Sequence) {
            return new ContentInfo((ASN1Sequence) obj);
        }
        throw new IllegalArgumentException("unknown object in factory");
    }

    public ContentInfo(ASN1Sequence seq) {
        Enumeration e = seq.getObjects();
        this.contentType = (DERObjectIdentifier) e.nextElement();
        if (e.hasMoreElements()) {
            this.content = ((DERTaggedObject) e.nextElement()).getObject();
        }
    }

    public ContentInfo(DERObjectIdentifier contentType2) {
        this.contentType = contentType2;
        this.content = null;
    }

    public ContentInfo(DERObjectIdentifier contentType2, DEREncodable content2) {
        this.contentType = contentType2;
        this.content = content2;
    }

    public DERObjectIdentifier getContentType() {
        return this.contentType;
    }

    public DEREncodable getContent() {
        return this.content;
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(this.contentType);
        if (this.content != null) {
            v.add(new BERTaggedObject(0, this.content));
        }
        return new BERSequence(v);
    }
}
