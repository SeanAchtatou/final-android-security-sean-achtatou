package cn.com.jit.ida.util.pki.pkcs;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.Parser;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1Set;
import cn.com.jit.ida.util.pki.asn1.DERBitString;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DERPrintableString;
import cn.com.jit.ida.util.pki.asn1.DERUTF8String;
import cn.com.jit.ida.util.pki.asn1.pkcs.PKCSObjectIdentifiers;
import cn.com.jit.ida.util.pki.asn1.pkcs.pkcs10.CertificationRequest;
import cn.com.jit.ida.util.pki.asn1.pkcs.pkcs10.CertificationRequestInfo;
import cn.com.jit.ida.util.pki.asn1.x509.AlgorithmIdentifier;
import cn.com.jit.ida.util.pki.asn1.x509.X509Name;
import cn.com.jit.ida.util.pki.asn1.x9.X9ObjectIdentifiers;
import cn.com.jit.ida.util.pki.cipher.JCrypto;
import cn.com.jit.ida.util.pki.cipher.JKey;
import cn.com.jit.ida.util.pki.cipher.Mechanism;
import cn.com.jit.ida.util.pki.cipher.Session;
import cn.com.jit.ida.util.pki.encoders.Base64;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.Hashtable;

public class PKCS10 {
    public static boolean LoadedConfigFile = false;
    public static final String MD2_RSA = "MD2withRSAEncryption";
    public static final String MD5_RSA = "MD5withRSAEncryption";
    public static final String SHA1_DSA = "SHA1withDSA";
    public static final String SHA1_EC_DSA = "SHA1withECDSA";
    public static final String SHA1_RSA = "SHA1withRSAEncryption";
    public static final String SHA224_EC_DSA = "SHA224withECDSA";
    public static final String SHA256_EC_DSA = "SHA256withECDSA";
    public static final String SHA256_RSA = "SHA256withRSAEncryption";
    public static final String SHA384_RSA = "SHA384withRSAEncryption";
    public static final String SHA512_RSA = "SHA512withRSAEncryption";
    private static Hashtable algs = new Hashtable();
    private static Hashtable oids = new Hashtable();
    private ASN1Set attributes = null;
    private boolean needVerify = false;
    private JKey pubKey = null;
    private Session session = null;
    private String subject = null;
    private X509Name xSub = null;

    static {
        algs.put("SHA1withRSAEncryption", PKCSObjectIdentifiers.sha1WithRSAEncryption);
        algs.put("MD5withRSAEncryption", PKCSObjectIdentifiers.md5WithRSAEncryption);
        algs.put("MD2withRSAEncryption", PKCSObjectIdentifiers.md2WithRSAEncryption);
        algs.put("SHA1withDSA", PKCSObjectIdentifiers.sha1WithDSA);
        algs.put("SHA1withECDSA", PKCSObjectIdentifiers.sha1WithECEncryption);
        algs.put("SHA224withECDSA", X9ObjectIdentifiers.ecdsa_with_SHA224);
        algs.put("SHA256withECDSA", X9ObjectIdentifiers.ecdsa_with_SHA256);
        algs.put("SHA256withRSAEncryption", PKCSObjectIdentifiers.sha256WithRSAEncryption);
        algs.put("SHA384withRSAEncryption", PKCSObjectIdentifiers.sha384WithRSAEncryption);
        algs.put("SHA512withRSAEncryption", PKCSObjectIdentifiers.sha512WithRSAEncryption);
        algs.put("SM3withSM2Encryption", PKCSObjectIdentifiers.sm2_with_sm3);
        oids.put(PKCSObjectIdentifiers.sha1WithRSAEncryption, new Mechanism("SHA1withRSAEncryption"));
        oids.put(PKCSObjectIdentifiers.md5WithRSAEncryption, new Mechanism("MD5withRSAEncryption"));
        oids.put(PKCSObjectIdentifiers.md2WithRSAEncryption, new Mechanism("MD2withRSAEncryption"));
        oids.put(PKCSObjectIdentifiers.sha1WithDSA, new Mechanism("SHA1withDSA"));
        oids.put(PKCSObjectIdentifiers.sha1WithECEncryption, new Mechanism("SHA1withECDSA"));
        oids.put(X9ObjectIdentifiers.ecdsa_with_SHA224, new Mechanism("SHA224withECDSA"));
        oids.put(X9ObjectIdentifiers.ecdsa_with_SHA256, new Mechanism("SHA256withECDSA"));
        oids.put(PKCSObjectIdentifiers.sha256WithRSAEncryption, new Mechanism("SHA256withRSAEncryption"));
        oids.put(PKCSObjectIdentifiers.sha384WithRSAEncryption, new Mechanism("SHA384withRSAEncryption"));
        oids.put(PKCSObjectIdentifiers.sha512WithRSAEncryption, new Mechanism("SHA512withRSAEncryption"));
        oids.put(PKCSObjectIdentifiers.sm2_with_sm3, new Mechanism("SM3withSM2Encryption"));
    }

    public PKCS10(Session session2) {
        if (session2 == null) {
            try {
                this.session = JCrypto.getInstance().openSession(JCrypto.JSOFT_LIB);
            } catch (Exception e) {
            }
        } else {
            this.session = session2;
        }
    }

    public CertificationRequest generateCertificationRequest(String signatureAlgorithm, String subject2, JKey pubKey2, ASN1Set attributes2, JKey signingKey) throws PKIException {
        this.subject = subject2;
        this.pubKey = pubKey2;
        this.attributes = attributes2;
        X509Name p10Subject = new X509Name(subject2);
        DERObjectIdentifier signOID = (DERObjectIdentifier) algs.get(signatureAlgorithm);
        if (signOID == null) {
            throw new PKIException("8173", "产生P10申请书失败 不支持的签名算法 " + signatureAlgorithm);
        } else if (subject2 == null) {
            throw new PKIException("8173", "产生P10申请书失败 主题信息必须不为空");
        } else if (pubKey2 == null) {
            throw new PKIException("8173", "产生P10申请书失败 公钥必须不为空");
        } else if (signingKey == null) {
            throw new PKIException("8173", "产生P10申请书失败 签名私钥必须不为空");
        } else {
            AlgorithmIdentifier algID = new AlgorithmIdentifier(signOID, null);
            CertificationRequestInfo crqInfo = new CertificationRequestInfo(p10Subject, Parser.key2SPKI(pubKey2), attributes2);
            byte[] b_crqInfo = Parser.writeDERObj2Bytes(crqInfo.getDERObject());
            return new CertificationRequest(crqInfo, algID, new DERBitString(this.session.sign((Mechanism) oids.get(signOID), signingKey, b_crqInfo)));
        }
    }

    public byte[] generateCertificationRequestData_DER(String signatureAlgorithm, String subject2, JKey pubKey2, ASN1Set attributes2, JKey signingKey) throws PKIException {
        return Parser.writeDERObj2Bytes(generateCertificationRequest(signatureAlgorithm, subject2, pubKey2, attributes2, signingKey).getDERObject());
    }

    public byte[] generateCertificationRequestData_B64(String signatureAlgorithm, String subject2, JKey pubKey2, ASN1Set attributes2, JKey signingKey) throws PKIException {
        return Base64.encode(Parser.writeDERObj2Bytes(generateCertificationRequest(signatureAlgorithm, subject2, pubKey2, attributes2, signingKey).getDERObject()));
    }

    public void load(CertificationRequest certficationRequest) throws PKIException {
        DERObjectIdentifier signOID = certficationRequest.getSignatureAlgorithm().getObjectId();
        Mechanism signM = (Mechanism) oids.get(signOID);
        if (signM == null) {
            throw new PKIException("8173", "产生P10申请书失败 不支持的签名算法 " + signOID.toString());
        }
        byte[] signData = certficationRequest.getSignature().getBytes();
        CertificationRequestInfo crqInfo = certficationRequest.getCertificationRequestInfo();
        byte[] b_crqInfo = Parser.writeDERObj2Bytes(crqInfo.getDERObject());
        this.pubKey = Parser.SPKI2Key(crqInfo.getSubjectPublicKeyInfo());
        if (this.session.getCfgTag() != null) {
            this.needVerify = this.session.getCfgTag().isVerifyP10Signature();
        } else {
            this.needVerify = false;
        }
        if (!this.needVerify || this.session.verifySign(signM, this.pubKey, b_crqInfo, signData)) {
            this.xSub = crqInfo.getSubject();
            this.subject = crqInfo.getSubject().toString();
            this.attributes = crqInfo.getAttributes();
            return;
        }
        throw new PKIException("8174", "解析P10申请书失败 签名验证失败");
    }

    public void load(byte[] data) throws PKIException {
        BufferedReader bufferReader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(data)));
        try {
            if (bufferReader.readLine().toUpperCase().indexOf("BEGIN NEW CERTIFICATE REQUEST") != -1) {
                StringBuffer strBuffer = new StringBuffer();
                while (true) {
                    String str = bufferReader.readLine();
                    if (str == null || str.toUpperCase().indexOf("END NEW CERTIFICATE REQUEST") != -1) {
                        data = Base64.decode(Parser.convertBase64(strBuffer.toString().getBytes()));
                    } else {
                        strBuffer.append(str);
                    }
                }
                data = Base64.decode(Parser.convertBase64(strBuffer.toString().getBytes()));
            } else if (Parser.isBase64Encode(data)) {
                data = Base64.decode(Parser.convertBase64(data));
            }
            try {
                if (data[0] != 48) {
                    throw new Exception();
                }
                load(new CertificationRequest((ASN1Sequence) Parser.writeBytes2DERObj(data)));
            } catch (Exception e) {
                throw new PKIException("8174", PKIException.PARSE_P10_ERR_DES, new Exception("The PKCS10 CertificationRequest content error."));
            }
        } catch (IOException ex1) {
            throw new PKIException("8174", PKIException.PARSE_P10_ERR_DES, ex1);
        }
    }

    public ASN1Set getAttributes() {
        return this.attributes;
    }

    public JKey getPubKey() {
        return this.pubKey;
    }

    public String getSubject() {
        return this.subject;
    }

    public boolean checkSubRules() throws Exception {
        Enumeration e = ((ASN1Sequence) this.xSub.getDERObject()).getObjects();
        while (e.hasMoreElements()) {
            ASN1Set set = (ASN1Set) e.nextElement();
            int i = 0;
            while (true) {
                if (i < set.size()) {
                    ASN1Sequence s = (ASN1Sequence) set.getObjectAt(i);
                    if (X509Name.C.equals(s.getObjectAt(0))) {
                        if (!(s.getObjectAt(1) instanceof DERPrintableString)) {
                            return false;
                        }
                    } else if (!(s.getObjectAt(1) instanceof DERUTF8String)) {
                        return false;
                    }
                    i++;
                }
            }
        }
        return true;
    }

    public static void main(String[] args) {
        try {
            JCrypto jcrypto = JCrypto.getInstance();
            jcrypto.initialize(JCrypto.JSOFT_LIB, null);
            jcrypto.initialize(JCrypto.JSJY05B_LIB, null);
            Session session2 = jcrypto.openSession(JCrypto.JSJY05B_LIB);
            PKCS10 p10 = new PKCS10(null);
            FileInputStream fis = new FileInputStream("d:\\p10.req");
            byte[] data = new byte[fis.available()];
            fis.read(data);
            fis.close();
            p10.load(data);
            p10.checkSubRules();
            JKey pubKey2 = p10.getPubKey();
            System.out.println("PKCS10 success!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
