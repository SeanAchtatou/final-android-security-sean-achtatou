package cn.com.jit.ida.util.pki.asn1;

import cn.com.jit.pnxclient.constant.PNXConfigConstant;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.SimpleTimeZone;

public class DERUTCTime extends DERObject {
    String time;

    public static DERUTCTime getInstance(Object obj) {
        if (obj == null || (obj instanceof DERUTCTime)) {
            return (DERUTCTime) obj;
        }
        if (obj instanceof ASN1OctetString) {
            return new DERUTCTime(((ASN1OctetString) obj).getOctets());
        }
        throw new IllegalArgumentException("illegal object in getInstance: " + obj.getClass().getName());
    }

    public static DERUTCTime getInstance(ASN1TaggedObject obj, boolean explicit) {
        return getInstance(obj.getObject());
    }

    public DERUTCTime(String time2) {
        this.time = time2;
    }

    public DERUTCTime(Date time2) {
        SimpleDateFormat dateF = new SimpleDateFormat("yyMMddHHmmss'Z'");
        dateF.setTimeZone(new SimpleTimeZone(0, "Z"));
        this.time = dateF.format(time2);
    }

    DERUTCTime(byte[] bytes) {
        char[] dateC = new char[bytes.length];
        for (int i = 0; i != dateC.length; i++) {
            dateC[i] = (char) (bytes[i] & 255);
        }
        this.time = new String(dateC);
    }

    public String getTime() {
        if (this.time.length() == 11) {
            return this.time.substring(0, 10) + "00GMT+00:00";
        }
        if (this.time.length() == 13) {
            return this.time.substring(0, 12) + "GMT+00:00";
        }
        if (this.time.length() == 17) {
            return this.time.substring(0, 12) + "GMT" + this.time.substring(12, 15) + PNXConfigConstant.RESP_SPLIT_3 + this.time.substring(15, 17);
        }
        return this.time;
    }

    public String getAdjustedTime() {
        String d = getTime();
        if (d.charAt(0) < '5') {
            return "20" + d;
        }
        return "19" + d;
    }

    private byte[] getOctets() {
        char[] cs = this.time.toCharArray();
        byte[] bs = new byte[cs.length];
        for (int i = 0; i != cs.length; i++) {
            bs[i] = (byte) cs[i];
        }
        return bs;
    }

    /* access modifiers changed from: package-private */
    public void encode(DEROutputStream out) throws IOException {
        out.writeEncoded(23, getOctets());
    }

    public boolean equals(Object o) {
        if (o == null || !(o instanceof DERUTCTime)) {
            return false;
        }
        return this.time.equals(((DERUTCTime) o).time);
    }
}
