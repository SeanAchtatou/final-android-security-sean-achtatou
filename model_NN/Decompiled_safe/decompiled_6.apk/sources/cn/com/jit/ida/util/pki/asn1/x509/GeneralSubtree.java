package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.DERTaggedObject;
import java.math.BigInteger;

public class GeneralSubtree implements DEREncodable {
    private GeneralName base;
    private DERInteger maximum;
    private DERInteger minimum;

    public GeneralSubtree(ASN1Sequence seq) {
        this.base = GeneralName.getInstance(seq.getObjectAt(0));
        switch (seq.size()) {
            case 1:
                return;
            case 2:
                ASN1TaggedObject o = (ASN1TaggedObject) seq.getObjectAt(1);
                switch (o.getTagNo()) {
                    case 0:
                        this.minimum = DERInteger.getInstance(o, false);
                        return;
                    case 1:
                        this.maximum = DERInteger.getInstance(o, false);
                        return;
                    default:
                        throw new IllegalArgumentException("Bad tag number: " + o.getTagNo());
                }
            case 3:
                this.minimum = DERInteger.getInstance((ASN1TaggedObject) seq.getObjectAt(1), false);
                this.maximum = DERInteger.getInstance((ASN1TaggedObject) seq.getObjectAt(2), false);
                return;
            default:
                throw new IllegalArgumentException("Bad sequence size: " + seq.size());
        }
    }

    public static GeneralSubtree getInstance(ASN1TaggedObject o, boolean explicit) {
        return new GeneralSubtree(ASN1Sequence.getInstance(o, explicit));
    }

    public static GeneralSubtree getInstance(Object obj) {
        if (obj == null) {
            return null;
        }
        if (obj instanceof GeneralSubtree) {
            return (GeneralSubtree) obj;
        }
        return new GeneralSubtree(ASN1Sequence.getInstance(obj));
    }

    public GeneralName getBase() {
        return this.base;
    }

    public BigInteger getMinimum() {
        if (this.minimum == null) {
            return BigInteger.valueOf(0);
        }
        return this.minimum.getValue();
    }

    public BigInteger getMaximum() {
        if (this.maximum == null) {
            return null;
        }
        return this.maximum.getValue();
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(this.base);
        if (this.minimum != null) {
            v.add(new DERTaggedObject(false, 0, this.minimum));
        }
        if (this.maximum != null) {
            v.add(new DERTaggedObject(false, 1, this.maximum));
        }
        return new DERSequence(v);
    }
}
