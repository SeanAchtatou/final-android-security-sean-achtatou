package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.PKIConstant;
import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.Parser;
import cn.com.jit.ida.util.pki.asn1.ASN1Encodable;
import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.DERBitString;
import cn.com.jit.ida.util.pki.asn1.DERGeneralizedTime;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DERNull;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.DERSet;
import cn.com.jit.ida.util.pki.cipher.JKey;
import cn.com.jit.ida.util.pki.cipher.Mechanism;
import cn.com.jit.ida.util.pki.cipher.Session;
import org.w3c.dom.Element;

public class V2X509AttCertGenerator {
    private AttributeCertificateInfo attCertInfo;
    private String deviceName;
    private Mechanism mechanism;
    private JKey pubKey;
    private AlgorithmIdentifier sigAlg;
    private DERBitString signature;
    private V2AttributeCertificateInfoGenerator v2AttCert;

    public V2X509AttCertGenerator() {
        this.v2AttCert = null;
        this.attCertInfo = null;
        this.mechanism = null;
        this.deviceName = null;
        this.sigAlg = null;
        this.pubKey = null;
        this.signature = null;
        this.v2AttCert = new V2AttributeCertificateInfoGenerator();
    }

    public void setHolder(Holder holder) {
        this.v2AttCert.setHolder(holder);
    }

    public void addAttribute(String oid, ASN1Encodable value) {
        this.v2AttCert.addAttribute(new Attribute(new DERObjectIdentifier(oid), new DERSet(value)));
    }

    public void addAttribute(Attribute attribute) {
        this.v2AttCert.addAttribute(attribute);
    }

    public void setSerialNumber(DERInteger serialNumber) throws PKIException {
        if (serialNumber == null) {
            throw new PKIException(PKIException.SN_NULL, PKIException.SN_NULL_DES);
        }
        this.v2AttCert.setSerialNumber(serialNumber);
    }

    public void setSignatureAlg(String signatureAlgorithm) throws PKIException {
        if (signatureAlgorithm == null) {
            throw new PKIException(PKIException.SIG_ALG_NULL, PKIException.SIG_ALG_NULL_DES);
        }
        if (signatureAlgorithm.equals("MD2withRSAEncryption")) {
            this.mechanism = new Mechanism("MD2withRSAEncryption");
        } else if (signatureAlgorithm.equals("MD5withRSAEncryption")) {
            this.mechanism = new Mechanism("MD5withRSAEncryption");
        } else if (signatureAlgorithm.equals("SHA1withRSAEncryption")) {
            this.mechanism = new Mechanism("SHA1withRSAEncryption");
        } else if (signatureAlgorithm.endsWith("SHA1withECDSA")) {
            this.mechanism = new Mechanism("SHA1withECDSA");
        } else if (signatureAlgorithm.endsWith("SHA1withDSA")) {
            this.mechanism = new Mechanism("SHA1withDSA");
        } else if (signatureAlgorithm.endsWith("SM3withSM2Encryption")) {
            this.mechanism = new Mechanism("SM3withSM2Encryption");
        } else {
            throw new PKIException(PKIException.NONSUPPORT_SIGALG, "不支持的签名算法: " + signatureAlgorithm);
        }
        this.sigAlg = new AlgorithmIdentifier((DERObjectIdentifier) PKIConstant.sigAlgName2OID.get(signatureAlgorithm), new DERNull());
        this.v2AttCert.setSignature(this.sigAlg);
    }

    public void setIssuer(AttCertIssuer issuer) {
        this.v2AttCert.setIssuer(issuer);
    }

    public void setStartDate(DERGeneralizedTime startDate) {
        this.v2AttCert.setStartDate(startDate);
    }

    public void setEndDate(DERGeneralizedTime endDate) {
        this.v2AttCert.setEndDate(endDate);
    }

    public void setIssuerUniqueID(DERBitString issuerUniqueID) {
        this.v2AttCert.setIssuerUniqueID(issuerUniqueID);
    }

    public void setExtensions(X509Extensions extensions) {
        this.v2AttCert.setExtensions(extensions);
    }

    public byte[] generateX509AttCert(JKey priKey, Session session) throws PKIException {
        generateSignature(priKey, session);
        return constructAttCertificate();
    }

    public byte[] generateX509AttCert(JKey priKey, Session session, Element xmlnode) throws PKIException {
        this.attCertInfo = new AttributeCertificateInfo(xmlnode);
        setSignatureAlg(generateMechanisNameByOID(this.attCertInfo.getSignature().getObjectId().getId()));
        signCertInfo(priKey, session);
        return constructAttCertificate();
    }

    public static String generateMechanisNameByOID(String oid) {
        if ("1.2.840.113549.1.1.5".equals(oid)) {
            return "SHA1withRSAEncryption";
        }
        return null;
    }

    private void generateSignature(JKey priKey, Session session) throws PKIException {
        try {
            this.attCertInfo = this.v2AttCert.generateAttributeCertificateInfo();
            signCertInfo(priKey, session);
        } catch (Exception e) {
            throw new PKIException(PKIException.ATTRIBUTE_CERTCREAT_ERROR, "属性证书产生过程出现异常:" + e.getMessage());
        }
    }

    public void signCertInfo(JKey priKey, Session session) throws PKIException {
        try {
            try {
                this.signature = new DERBitString(session.sign(this.mechanism, priKey, Parser.writeDERObj2Bytes(this.attCertInfo.getDERObject())));
            } catch (Exception ex) {
                throw new PKIException("5", PKIException.SIGN_DES, ex);
            }
        } catch (Exception ex2) {
            throw new PKIException(PKIException.TBSCERT_BYTES, PKIException.TBSCERT_BYTES_DES, ex2);
        }
    }

    /* access modifiers changed from: protected */
    public byte[] constructAttCertificate() throws PKIException {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(this.attCertInfo);
        v.add(this.sigAlg);
        v.add(this.signature);
        try {
            return Parser.writeDERObj2Bytes(new DERSequence(v).getDERObject());
        } catch (Exception ex) {
            throw new PKIException(PKIException.CERT_BYTES, PKIException.CERT_BYTES_DES, ex);
        }
    }

    public void setIssuerUniqueID(byte[] issuerUniqueID) {
        if (issuerUniqueID != null) {
            this.v2AttCert.setIssuerUniqueID(new DERBitString(issuerUniqueID));
        }
    }

    public AttributeCertificateInfo getAttCertInfo() {
        return this.attCertInfo;
    }

    public void setAttCertInfo(AttributeCertificateInfo attCertInfo2) {
        this.attCertInfo = attCertInfo2;
    }

    public String getDeviceName() {
        return this.deviceName;
    }

    public void setDeviceName(String deviceName2) {
        this.deviceName = deviceName2;
    }

    public Mechanism getMechanism() {
        return this.mechanism;
    }

    public void setMechanism(Mechanism mechanism2) {
        this.mechanism = mechanism2;
    }

    public JKey getPubKey() {
        return this.pubKey;
    }

    public void setPubKey(JKey pubKey2) {
        this.pubKey = pubKey2;
    }

    public AlgorithmIdentifier getSigAlg() {
        return this.sigAlg;
    }

    public void setSigAlg(AlgorithmIdentifier sigAlg2) {
        this.sigAlg = sigAlg2;
    }

    public DERBitString getSignature() {
        return this.signature;
    }

    public void setSignature(DERBitString signature2) {
        this.signature = signature2;
    }

    public V2AttributeCertificateInfoGenerator getV2AttCert() {
        return this.v2AttCert;
    }

    public void setV2AttCert(V2AttributeCertificateInfoGenerator attCert) {
        this.v2AttCert = attCert;
    }
}
