package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public class ExtendedKeyUsage implements DEREncodable {
    ASN1Sequence seq;
    Hashtable usageTable = new Hashtable();

    public static ExtendedKeyUsage getInstance(ASN1TaggedObject obj, boolean explicit) {
        return getInstance(ASN1Sequence.getInstance(obj, explicit));
    }

    public static ExtendedKeyUsage getInstance(Object obj) {
        if (obj == null || (obj instanceof ExtendedKeyUsage)) {
            return (ExtendedKeyUsage) obj;
        }
        if (obj instanceof ASN1Sequence) {
            return new ExtendedKeyUsage((ASN1Sequence) obj);
        }
        throw new IllegalArgumentException("Invalid ExtendedKeyUsage: " + obj.getClass().getName());
    }

    public ExtendedKeyUsage(ASN1Sequence seq2) {
        this.seq = seq2;
        Enumeration e = seq2.getObjects();
        while (e.hasMoreElements()) {
            Object o = e.nextElement();
            this.usageTable.put(o, o);
        }
    }

    public ExtendedKeyUsage(Vector usages) {
        ASN1EncodableVector v = new ASN1EncodableVector();
        Enumeration e = usages.elements();
        while (e.hasMoreElements()) {
            DERObject o = (DERObject) e.nextElement();
            v.add(o);
            this.usageTable.put(o, o);
        }
        this.seq = new DERSequence(v);
    }

    public boolean hasKeyPurposeId(KeyPurposeId keyPurposeId) {
        return this.usageTable.get(keyPurposeId) != null;
    }

    public DERObject getDERObject() {
        return this.seq;
    }
}
