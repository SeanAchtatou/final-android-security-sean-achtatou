package cn.com.jit.ida.util.pki.pkcs;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.Parser;
import cn.com.jit.ida.util.pki.asn1.ASN1InputStream;
import cn.com.jit.ida.util.pki.asn1.ASN1OctetString;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.pkcs.PKCSObjectIdentifiers;
import cn.com.jit.ida.util.pki.asn1.pkcs.pkcs7.ContentInfo;
import cn.com.jit.ida.util.pki.asn1.pkcs.pkcs7.IssuerAndSerialNumber;
import cn.com.jit.ida.util.pki.asn1.pkcs.pkcs7.SignedData;
import cn.com.jit.ida.util.pki.asn1.pkcs.pkcs7.SignerInfo;
import cn.com.jit.ida.util.pki.asn1.x509.X509Name;
import cn.com.jit.ida.util.pki.cert.X509Cert;
import cn.com.jit.ida.util.pki.cipher.JCrypto;
import cn.com.jit.ida.util.pki.cipher.Mechanism;
import cn.com.jit.ida.util.pki.cipher.Session;
import cn.com.jit.ida.util.pki.encoders.Base64;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.InputStream;

public class PKCS7SignedData {
    private Session session = null;
    private SignedData signedData = null;

    public PKCS7SignedData(Session session2) {
        this.session = session2;
    }

    public void load(byte[] data) throws PKIException {
        if (Parser.isBase64Encode(data)) {
            data = Base64.decode(Parser.convertBase64(data));
        }
        load(new ByteArrayInputStream(data));
    }

    public void load(SignedData signedData2) throws PKIException {
        this.signedData = signedData2;
    }

    public void load(String fileName) throws PKIException {
        try {
            FileInputStream fin = new FileInputStream(fileName);
            try {
                byte[] data = new byte[fin.available()];
                fin.read(data);
                fin.close();
                load(data);
            } catch (Exception e) {
                ex = e;
                throw new PKIException("8175", PKIException.PARSE_P7_SIGNEDDATA_ERR_DES, ex);
            }
        } catch (Exception e2) {
            ex = e2;
            throw new PKIException("8175", PKIException.PARSE_P7_SIGNEDDATA_ERR_DES, ex);
        }
    }

    public void load(InputStream ins) throws PKIException {
        ASN1InputStream ais = new ASN1InputStream(ins);
        try {
            SignedData sd = SignedData.getInstance(ContentInfo.getInstance((ASN1Sequence) ais.readObject()).getContent());
            ins.close();
            ais.close();
            this.signedData = sd;
        } catch (Exception ex) {
            throw new PKIException("8175", PKIException.PARSE_P7_SIGNEDDATA_ERR_DES, ex);
        } catch (Throwable e) {
            throw new PKIException("8175", PKIException.PARSE_P7_SIGNEDDATA_ERR_DES, (Exception) e);
        }
    }

    public byte[] getContent(X509Cert cert) throws PKIException {
        byte[] sourceData;
        ContentInfo contentInfo = this.signedData.getContentInfo();
        if (!contentInfo.getContentType().equals(PKCSObjectIdentifiers.data) && !contentInfo.getContentType().equals(PKCSObjectIdentifiers.id_ct_TSTInfo)) {
            sourceData = Parser.writeDERObj2Bytes(contentInfo.getContent().getDERObject());
        } else if (contentInfo.getContent() == null) {
            throw new PKIException("8175", "解析PKCS7签名数据包失败 解析PKCS7签名数据包失败", new Exception("no sourceData to be verify."));
        } else {
            sourceData = ((ASN1OctetString) contentInfo.getContent()).getOctets();
        }
        if (verifySignerInfo(sourceData, SignerInfo.getInstance(this.signedData.getSignerInfos().getObjectAt(0)), cert)) {
            return sourceData;
        }
        throw new PKIException("8175", "解析PKCS7签名数据包失败 验证PKCS7签名失败");
    }

    public boolean verifySignerInfo(byte[] content, SignerInfo signerInfo, X509Cert cert) {
        Mechanism signM;
        try {
            String issuer = cert.getIssuer();
            if (!new IssuerAndSerialNumber(new X509Name(issuer), cert.getSerialNumber()).equals(signerInfo.getIssuerAndSerialNumber())) {
                return false;
            }
            DERObjectIdentifier encryptionId = signerInfo.getDigestEncryptionAlgorithm().getObjectId();
            if (!encryptionId.equals(PKCSObjectIdentifiers.rsaEncryption) && !encryptionId.equals(PKCSObjectIdentifiers.SM2)) {
                return false;
            }
            DERObjectIdentifier objectId = signerInfo.getDigestEncryptionAlgorithm().getObjectId();
            DERObjectIdentifier digestId = signerInfo.getDigestAlgorithm().getObjectId();
            if (digestId.equals(PKCSObjectIdentifiers.md2)) {
                signM = new Mechanism("MD2withRSAEncryption");
            } else if (digestId.equals(PKCSObjectIdentifiers.md5)) {
                signM = new Mechanism("MD5withRSAEncryption");
            } else if (digestId.equals(PKCSObjectIdentifiers.sha1)) {
                signM = new Mechanism("SHA1withRSAEncryption");
            } else if (digestId.equals(PKCSObjectIdentifiers.SM3) || digestId.equals(PKCSObjectIdentifiers.SM3PublicKey1) || digestId.equals(PKCSObjectIdentifiers.SM3PublicKey2)) {
                signM = new Mechanism("SM3withSM2Encryption");
            } else if (digestId.equals(PKCSObjectIdentifiers.sha224)) {
                signM = new Mechanism("SHA224withRSAEncryption");
            } else if (digestId.equals(PKCSObjectIdentifiers.sha256)) {
                signM = new Mechanism("SHA256withRSAEncryption");
            } else if (digestId.equals(PKCSObjectIdentifiers.sha384)) {
                signM = new Mechanism("SHA384withRSAEncryption");
            } else if (!digestId.equals(PKCSObjectIdentifiers.sha512)) {
                return false;
            } else {
                signM = new Mechanism("SHA512withRSAEncryption");
            }
            byte[] signature = signerInfo.getEncryptedDigest().getOctets();
            return this.session.verifySign(signM, cert.getPublicKey(), content, signature);
        } catch (Exception e) {
            return false;
        }
    }

    public boolean verifySignerInfo(InputStream content, SignerInfo signerInfo, X509Cert cert) {
        Mechanism signM;
        try {
            String issuer = cert.getIssuer();
            if (!new IssuerAndSerialNumber(new X509Name(issuer), cert.getSerialNumber()).equals(signerInfo.getIssuerAndSerialNumber()) || !signerInfo.getDigestEncryptionAlgorithm().getObjectId().equals(PKCSObjectIdentifiers.rsaEncryption)) {
                return false;
            }
            DERObjectIdentifier digestId = signerInfo.getDigestAlgorithm().getObjectId();
            if (digestId.equals(PKCSObjectIdentifiers.md2)) {
                signM = new Mechanism("MD2withRSAEncryption");
            } else if (digestId.equals(PKCSObjectIdentifiers.md5)) {
                signM = new Mechanism("MD5withRSAEncryption");
            } else if (digestId.equals(PKCSObjectIdentifiers.sha1)) {
                signM = new Mechanism("SHA1withRSAEncryption");
            } else if (digestId.equals(PKCSObjectIdentifiers.SM3) || digestId.equals(PKCSObjectIdentifiers.SM3PublicKey1) || digestId.equals(PKCSObjectIdentifiers.SM3PublicKey2)) {
                signM = new Mechanism("SM3withSM2Encryption");
            } else if (digestId.equals(PKCSObjectIdentifiers.sha224)) {
                signM = new Mechanism("SHA224withRSAEncryption");
            } else if (digestId.equals(PKCSObjectIdentifiers.sha256)) {
                signM = new Mechanism("SHA256withRSAEncryption");
            } else if (digestId.equals(PKCSObjectIdentifiers.sha384)) {
                signM = new Mechanism("SHA384withRSAEncryption");
            } else if (!digestId.equals(PKCSObjectIdentifiers.sha512)) {
                return false;
            } else {
                signM = new Mechanism("SHA512withRSAEncryption");
            }
            byte[] signature = signerInfo.getEncryptedDigest().getOctets();
            return this.session.verifySign(signM, cert.getPublicKey(), content, signature);
        } catch (Exception e) {
            return false;
        }
    }

    public boolean verifyP7SignedData(X509Cert cert) throws PKIException {
        byte[] sourceData;
        ContentInfo contentInfo = this.signedData.getContentInfo();
        if (!contentInfo.getContentType().equals(PKCSObjectIdentifiers.data) && !contentInfo.getContentType().equals(PKCSObjectIdentifiers.id_ct_TSTInfo)) {
            sourceData = Parser.writeDERObj2Bytes(contentInfo.getContent().getDERObject());
        } else if (contentInfo.getContent() == null) {
            throw new PKIException("8175", "解析PKCS7签名数据包失败 解析PKCS7签名数据包失败", new Exception("no sourceData to be verify."));
        } else {
            sourceData = ((ASN1OctetString) contentInfo.getContent()).getOctets();
        }
        return verifySignerInfo(sourceData, SignerInfo.getInstance(this.signedData.getSignerInfos().getObjectAt(0)), cert);
    }

    public boolean verifyP7SignedData(byte[] sourceData, X509Cert cert) throws PKIException {
        ContentInfo contentInfo = this.signedData.getContentInfo();
        return verifySignerInfo(sourceData, SignerInfo.getInstance(this.signedData.getSignerInfos().getObjectAt(0)), cert);
    }

    public boolean verifyP7SignedData(InputStream sourceData, X509Cert cert) throws PKIException {
        ContentInfo contentInfo = this.signedData.getContentInfo();
        return verifySignerInfo(sourceData, SignerInfo.getInstance(this.signedData.getSignerInfos().getObjectAt(0)), cert);
    }

    public static void main(String[] args) {
        JCrypto jcrypto = JCrypto.getInstance();
        try {
            jcrypto.initialize(JCrypto.JSOFT_LIB, null);
            PKCS7SignedData p7 = new PKCS7SignedData(jcrypto.openSession(JCrypto.JSOFT_LIB));
            p7.load("d:/1.p7b");
            X509Cert cert = new X509Cert(new FileInputStream("d:/1.cer"));
            p7.verifyP7SignedData("a".getBytes(), cert);
            System.out.println(new String(p7.getContent(cert)));
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
    }
}
