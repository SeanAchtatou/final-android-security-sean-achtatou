package cn.com.jit.ida.util.pki.crl;

import cn.com.jit.ida.util.pki.PKIConstant;
import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.Parser;
import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.DERBitString;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DERNull;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.x509.AlgorithmIdentifier;
import cn.com.jit.ida.util.pki.asn1.x509.TBSCertList;
import cn.com.jit.ida.util.pki.asn1.x509.Time;
import cn.com.jit.ida.util.pki.asn1.x509.V2TBSCertListGenerator;
import cn.com.jit.ida.util.pki.asn1.x509.X509Extension;
import cn.com.jit.ida.util.pki.asn1.x509.X509Extensions;
import cn.com.jit.ida.util.pki.asn1.x509.X509Name;
import cn.com.jit.ida.util.pki.cipher.JKey;
import cn.com.jit.ida.util.pki.cipher.Mechanism;
import cn.com.jit.ida.util.pki.cipher.Session;
import cn.com.jit.ida.util.pki.extension.Extension;
import com.jianq.misc.StringEx;
import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Vector;

public class X509CRLGenerator {
    public static final int AA_COMPROMISE = 10;
    public static final int AFFILIATION_CHANGED = 3;
    public static final int CA_COMPROMISE = 2;
    public static final int CERTIFICATE_HOLD = 6;
    public static final int CESSATION_OF_OPERATION = 5;
    public static final int KEY_COMPROMISE = 1;
    public static final int PRIVILEGE_WITHDRAWN = 9;
    public static final int REMOVE_FROM_CRL = 8;
    public static final int SUPERSEDED = 4;
    public static final int UNSPECIFIED = 0;
    private HashMap dnrules;
    private Hashtable extensionSet;
    private String issuerName;
    private Mechanism mechanism;
    private AlgorithmIdentifier sigAlg;
    private DERBitString signature;
    private TBSCertList tbsCRL;
    private V2TBSCertListGenerator tbsCRLGen;
    private Date thisUpdate;

    public X509CRLGenerator() {
        this.mechanism = null;
        this.tbsCRLGen = null;
        this.sigAlg = null;
        this.tbsCRL = null;
        this.signature = null;
        this.issuerName = null;
        this.thisUpdate = null;
        this.extensionSet = null;
        this.dnrules = null;
        this.tbsCRLGen = new V2TBSCertListGenerator();
        this.extensionSet = new Hashtable();
    }

    public void addRevokeCert(String certSerialNumber, Date revokeTime) {
        this.tbsCRLGen.addCRLEntry(new DERInteger(new BigInteger(certSerialNumber, 16)), new Time(revokeTime), 0);
    }

    public void addRevokeCert(String certSerialNumber, Date revokeTime, int reason) {
        this.tbsCRLGen.addCRLEntry(new DERInteger(new BigInteger(certSerialNumber, 16)), new Time(revokeTime), reason);
    }

    public void addRevokeCert(BigInteger certSerialNumber, Date revokeTime) {
        this.tbsCRLGen.addCRLEntry(new DERInteger(certSerialNumber), new Time(revokeTime), 0);
    }

    public void addRevokeCert(BigInteger certSerialNumber, Date revokeTime, int reason) {
        this.tbsCRLGen.addCRLEntry(new DERInteger(certSerialNumber), new Time(revokeTime), reason);
    }

    public void setIssuer(String issuerName2) {
        this.issuerName = issuerName2;
        X509Name x509Issuer = new X509Name(issuerName2);
        if (this.dnrules != null) {
            x509Issuer.setRules(this.dnrules);
        }
        this.tbsCRLGen.setIssuer(x509Issuer);
    }

    public void setIssuer(X509Name x509IssuerName) {
        this.issuerName = x509IssuerName.toString();
        this.tbsCRLGen.setIssuer(x509IssuerName);
    }

    public void setThisUpdate(Date thisUpdate2) {
        this.thisUpdate = thisUpdate2;
        this.tbsCRLGen.setThisUpdate(new Time(thisUpdate2));
    }

    public void setNextUpdate(Date nextUpdate) {
        this.tbsCRLGen.setNextUpdate(new Time(nextUpdate));
    }

    public void setSignatureAlg(String signatureAlgorithm) throws PKIException {
        if (signatureAlgorithm == null) {
            throw new PKIException(PKIException.SIG_ALG_NULL, PKIException.SIG_ALG_NULL_DES);
        }
        if (signatureAlgorithm.equals("MD2withRSAEncryption")) {
            this.mechanism = new Mechanism("MD2withRSAEncryption");
        } else if (signatureAlgorithm.equals("MD5withRSAEncryption")) {
            this.mechanism = new Mechanism("MD5withRSAEncryption");
        } else if (signatureAlgorithm.equals("SHA1withRSAEncryption")) {
            this.mechanism = new Mechanism("SHA1withRSAEncryption");
        } else if (signatureAlgorithm.equals("SHA224withRSAEncryption")) {
            this.mechanism = new Mechanism("SHA224withRSAEncryption");
        } else if (signatureAlgorithm.equals("SHA256withRSAEncryption")) {
            this.mechanism = new Mechanism("SHA256withRSAEncryption");
        } else if (signatureAlgorithm.equals("SHA384withRSAEncryption")) {
            this.mechanism = new Mechanism("SHA384withRSAEncryption");
        } else if (signatureAlgorithm.equals("SHA512withRSAEncryption")) {
            this.mechanism = new Mechanism("SHA512withRSAEncryption");
        } else if (signatureAlgorithm.equals("SHA224withECDSA")) {
            this.mechanism = new Mechanism("SHA224withECDSA");
        } else if (signatureAlgorithm.equals("SHA256withECDSA")) {
            this.mechanism = new Mechanism("SHA256withECDSA");
        } else if (signatureAlgorithm.endsWith("SHA1withECDSA")) {
            this.mechanism = new Mechanism("SHA1withECDSA");
        } else if (signatureAlgorithm.endsWith("SHA1withDSA")) {
            this.mechanism = new Mechanism("SHA1withDSA");
        } else if (signatureAlgorithm.endsWith("SM3withSM2Encryption")) {
            this.mechanism = new Mechanism("SM3withSM2Encryption");
        } else {
            throw new PKIException(PKIException.NONSUPPORT_SIGALG, "不支持的签名算法: " + signatureAlgorithm);
        }
        DERObjectIdentifier oid = (DERObjectIdentifier) PKIConstant.sigAlgName2OID.get(signatureAlgorithm);
        if (signatureAlgorithm.equals("SHA224withRSAEncryption") || signatureAlgorithm.equals("SHA256withRSAEncryption") || signatureAlgorithm.equals("SHA384withRSAEncryption") || signatureAlgorithm.equals("SHA512withRSAEncryption")) {
            this.sigAlg = new AlgorithmIdentifier(oid, new DERNull());
        } else {
            this.sigAlg = new AlgorithmIdentifier(oid);
        }
        this.tbsCRLGen.setSignature(this.sigAlg);
    }

    public void setExtension(Vector extensions) throws PKIException {
        int size = extensions.size();
        int i = 0;
        while (i < size) {
            Extension extension = (Extension) extensions.get(i);
            DERObjectIdentifier derOID = new DERObjectIdentifier(extension.getOID());
            boolean critical = extension.getCritical();
            try {
                this.extensionSet.put(derOID, new X509Extension(extension.getCritical(), new DEROctetString(extension.encode())));
                i++;
            } catch (PKIException ex) {
                throw new PKIException(PKIException.EXTENSION_ENCODE, PKIException.EXTENSION_ENCODE_DES, ex);
            }
        }
    }

    public void addExtension(Extension extension) throws PKIException {
        DERObjectIdentifier derOID = new DERObjectIdentifier(extension.getOID());
        boolean critical = extension.getCritical();
        try {
            this.extensionSet.put(derOID, new X509Extension(extension.getCritical(), new DEROctetString(extension.encode())));
        } catch (PKIException ex) {
            throw new PKIException(PKIException.EXTENSION_ENCODE, PKIException.EXTENSION_ENCODE_DES, ex);
        }
    }

    public byte[] generateCRL(JKey priKey, Session session) throws PKIException {
        if (this.issuerName == null || this.issuerName.equals(StringEx.Empty)) {
            throw new PKIException(PKIException.ISSUER_NULL, PKIException.ISSUER_NULL_DES);
        } else if (this.thisUpdate == null) {
            throw new PKIException(PKIException.THIS_UPDATE_NULL, PKIException.THIS_UPDATE_NULL_DES);
        } else if (this.sigAlg == null) {
            throw new PKIException(PKIException.SIG_ALG_NULL, PKIException.SIG_ALG_NULL_DES);
        } else {
            generateSignature(priKey, session);
            return constructCRL();
        }
    }

    private void generateSignature(JKey priKey, Session session) throws PKIException {
        if (this.extensionSet.size() > 0) {
            this.tbsCRLGen.setExtensions(new X509Extensions(this.extensionSet));
        }
        this.tbsCRL = this.tbsCRLGen.generateTBSCertList();
        try {
            try {
                this.signature = new DERBitString(session.sign(this.mechanism, priKey, Parser.writeDERObj2Bytes(this.tbsCRL.getDERObject())));
            } catch (Exception ex) {
                throw new PKIException("5", PKIException.SIGN_DES, ex);
            }
        } catch (Exception ex2) {
            throw new PKIException(PKIException.TBSCRL_BYTES, PKIException.TBSCRL_BYTES_DES, ex2);
        }
    }

    private byte[] constructCRL() throws PKIException {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(this.tbsCRL);
        v.add(this.sigAlg);
        v.add(this.signature);
        try {
            return Parser.writeDERObj2Bytes(new DERSequence(v).getDERObject());
        } catch (Exception ex) {
            throw new PKIException(PKIException.CRL_BYTES, PKIException.CRL_BYTES_DES, ex);
        }
    }

    public void SetDnRules(Map rules) {
        this.dnrules = (HashMap) rules;
    }
}
