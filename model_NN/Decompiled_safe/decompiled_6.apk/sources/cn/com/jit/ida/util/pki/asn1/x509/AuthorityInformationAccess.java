package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.ASN1Encodable;
import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import java.util.Enumeration;
import java.util.Vector;

public class AuthorityInformationAccess extends ASN1Encodable {
    public static final DERObjectIdentifier ID_AD_CA_ISSUERS = new DERObjectIdentifier("1.3.6.1.5.5.7.48.2");
    public static final DERObjectIdentifier ID_AD_OCSP = new DERObjectIdentifier("1.3.6.1.5.5.7.48.1");
    private GeneralName accessLocation;
    private DERObjectIdentifier accessMethod;
    private Vector vAccessDes;

    public AuthorityInformationAccess getInstance(Object obj) {
        if (obj instanceof AuthorityInformationAccess) {
            return (AuthorityInformationAccess) obj;
        }
        if (obj instanceof ASN1Sequence) {
            return new AuthorityInformationAccess((ASN1Sequence) obj);
        }
        throw new IllegalArgumentException("unknown object in factory");
    }

    public AuthorityInformationAccess(ASN1Sequence seq) {
        this.accessMethod = null;
        this.accessLocation = null;
        this.vAccessDes = null;
        this.vAccessDes = new Vector();
        Enumeration e = seq.getObjects();
        if (e.hasMoreElements()) {
            DERSequence vec = (DERSequence) e.nextElement();
            if (vec.size() != 2) {
                throw new IllegalArgumentException("wrong number of elements in inner sequence");
            }
            this.vAccessDes.add(new AccessDescription(vec));
        }
    }

    public AuthorityInformationAccess(Vector vAccessDes2) {
        this.accessMethod = null;
        this.accessLocation = null;
        this.vAccessDes = null;
        this.vAccessDes = vAccessDes2;
    }

    public AuthorityInformationAccess(DERObjectIdentifier oid, GeneralName location) {
        this.accessMethod = null;
        this.accessLocation = null;
        this.vAccessDes = null;
        this.accessMethod = oid;
        this.accessLocation = location;
    }

    public DERObjectIdentifier getAccessMethod() {
        return this.accessMethod;
    }

    public GeneralName getAccessLocation() {
        return this.accessLocation;
    }

    public DERObject toASN1Object() {
        ASN1EncodableVector vec = new ASN1EncodableVector();
        for (int i = 0; i < this.vAccessDes.size(); i++) {
            vec.add(((AccessDescription) this.vAccessDes.get(i)).toASN1Object());
        }
        return new DERSequence(vec);
    }

    public String toString() {
        return "AuthorityInformationAccess: Oid(" + this.accessMethod.getId() + ")";
    }
}
