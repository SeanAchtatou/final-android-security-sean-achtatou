package cn.com.jit.ida.util.pki.cipher.softsm;

import android.support.v4.view.MotionEventCompat;
import java.math.BigInteger;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.math.ec.ECPoint;

public class Cipher {
    private int ct = 1;
    private byte[] key = new byte[32];
    private byte keyOff = 0;
    private ECPoint p2;
    private SM3Digest sm3c3;
    private SM3Digest sm3keybase;

    private void Reset() {
        this.sm3keybase = new SM3Digest();
        this.sm3c3 = new SM3Digest();
        byte[] p = Util.byteconvert32(this.p2.getX().toBigInteger());
        this.sm3keybase.BlockUpdate(p, 0, p.length);
        this.sm3c3.BlockUpdate(p, 0, p.length);
        byte[] p3 = Util.byteconvert32(this.p2.getY().toBigInteger());
        this.sm3keybase.BlockUpdate(p3, 0, p3.length);
        this.ct = 1;
        NextKey();
    }

    private void NextKey() {
        SM3Digest sm3keycur = new SM3Digest(this.sm3keybase);
        sm3keycur.update((byte) ((this.ct >> 24) & MotionEventCompat.ACTION_MASK));
        sm3keycur.update((byte) ((this.ct >> 16) & MotionEventCompat.ACTION_MASK));
        sm3keycur.update((byte) ((this.ct >> 8) & MotionEventCompat.ACTION_MASK));
        sm3keycur.update((byte) (this.ct & MotionEventCompat.ACTION_MASK));
        sm3keycur.doFinal(this.key, 0);
        this.keyOff = 0;
        this.ct++;
    }

    public ECPoint Init_enc(SM2 sm2, ECPoint userKey) {
        BigInteger k;
        ECPoint c1;
        if (!sm2.sm2Test) {
            AsymmetricCipherKeyPair key2 = sm2.ecc_key_pair_generator.generateKeyPair();
            k = key2.getPrivate().getD();
            c1 = key2.getPublic().getQ();
        } else {
            k = new BigInteger("4C62EEFD6ECFC2B95B92FD6C3D9575148AFA17425546D49018E5388D49DD7B4F", 16);
            c1 = sm2.ecc_point_g.multiply(k);
        }
        this.p2 = userKey.multiply(k);
        Reset();
        return c1;
    }

    public void Encrypt(byte[] data) {
        this.sm3c3.BlockUpdate(data, 0, data.length);
        for (int i = 0; i < data.length; i++) {
            if (this.keyOff == this.key.length) {
                NextKey();
            }
            byte b = data[i];
            byte[] bArr = this.key;
            byte b2 = this.keyOff;
            this.keyOff = (byte) (b2 + 1);
            data[i] = (byte) (b ^ bArr[b2]);
        }
    }

    public void Init_dec(BigInteger userD, ECPoint c1) {
        this.p2 = c1.multiply(userD);
        Reset();
    }

    public void Decrypt(byte[] data) {
        for (int i = 0; i < data.length; i++) {
            if (this.keyOff == this.key.length) {
                NextKey();
            }
            byte b = data[i];
            byte[] bArr = this.key;
            byte b2 = this.keyOff;
            this.keyOff = (byte) (b2 + 1);
            data[i] = (byte) (b ^ bArr[b2]);
        }
        this.sm3c3.BlockUpdate(data, 0, data.length);
    }

    public void Dofinal(byte[] c3) {
        byte[] p = Util.byteconvert32(this.p2.getY().toBigInteger());
        this.sm3c3.BlockUpdate(p, 0, p.length);
        this.sm3c3.doFinal(c3, 0);
        Reset();
    }
}
