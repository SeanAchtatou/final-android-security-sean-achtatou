package cn.com.jit.ida.util.pki.extension;

import cn.com.jit.ida.util.pki.asn1.x509.AuthorityInformationAccess;

public class AccessDescriptionExt {
    public static final int DN_TYPE_NAME = 4;
    public static final String METHOD_CA_ISSUERS = AuthorityInformationAccess.ID_AD_CA_ISSUERS.getId();
    public static final String METHOD_OCSP = AuthorityInformationAccess.ID_AD_OCSP.getId();
    public static final int URI_TYPE_NAME = 6;
    private String accessLocation;
    private int accessLocationType;
    private String accessMethod;

    public AccessDescriptionExt() {
        this.accessMethod = null;
        this.accessLocationType = 6;
        this.accessLocation = null;
        this.accessMethod = null;
        this.accessLocation = null;
        this.accessLocationType = 6;
    }

    public String getAccessLocation() {
        return this.accessLocation;
    }

    public String getAccessMethod() {
        return this.accessMethod;
    }

    public void setAccessLocation(String accessLocation2) {
        this.accessLocation = accessLocation2;
    }

    public void setAccessMethod(String accessMethod2) {
        this.accessMethod = accessMethod2;
    }

    public int getAccessLocationType() {
        return this.accessLocationType;
    }

    public void setAccessLocationType(int accessLocationType2) {
        this.accessLocationType = accessLocationType2;
    }
}
