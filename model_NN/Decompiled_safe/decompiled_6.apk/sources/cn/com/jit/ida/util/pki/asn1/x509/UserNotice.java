package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.ASN1Encodable;
import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.DERString;

public class UserNotice extends ASN1Encodable {
    DisplayText explicitText = null;
    NoticeReference noticeRef = null;

    public DERObject toASN1Object() {
        ASN1EncodableVector av = new ASN1EncodableVector();
        av.add(this.noticeRef);
        av.add(this.explicitText);
        return new DERSequence(av);
    }

    public UserNotice(NoticeReference noticeRef2, DisplayText explicitText2) {
        this.noticeRef = noticeRef2;
        this.explicitText = explicitText2;
    }

    public UserNotice(NoticeReference noticeRef2, String str) {
        this.noticeRef = noticeRef2;
        this.explicitText = new DisplayText(3, str);
    }

    public UserNotice(int type, NoticeReference noticeRef2, String str) {
        this.noticeRef = noticeRef2;
        this.explicitText = new DisplayText(type, str);
    }

    public UserNotice(ASN1Sequence as) {
        this.noticeRef = NoticeReference.getInstance(ASN1Sequence.getInstance(as.getObjectAt(0)));
        if (as.size() > 1) {
            this.explicitText = DisplayText.getInstance((DERString) as.getObjectAt(1));
        }
    }

    public boolean isHaveExplicitText() {
        return this.explicitText != null;
    }

    public String getExplicitText() {
        return this.explicitText.getString();
    }

    public NoticeReference getNoticeReference() {
        return this.noticeRef;
    }
}
