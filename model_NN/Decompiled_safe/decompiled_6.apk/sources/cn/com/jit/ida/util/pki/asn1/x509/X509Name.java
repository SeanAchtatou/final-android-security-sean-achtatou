package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.PKIToolConfig;
import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1InputStream;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.ASN1Set;
import cn.com.jit.ida.util.pki.asn1.ASN1TaggedObject;
import cn.com.jit.ida.util.pki.asn1.DERBMPString;
import cn.com.jit.ida.util.pki.asn1.DERBitString;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERIA5String;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DERObjectIdentifier;
import cn.com.jit.ida.util.pki.asn1.DERPrintableString;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.DERSet;
import cn.com.jit.ida.util.pki.asn1.DERString;
import cn.com.jit.ida.util.pki.asn1.DERT61String;
import cn.com.jit.ida.util.pki.asn1.DERUTF8String;
import cn.com.jit.ida.util.pki.asn1.DERVisibleString;
import cn.com.jit.ida.util.pki.asn1.pkcs.PKCSObjectIdentifiers;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Vector;

public class X509Name implements DEREncodable {
    public static final DERObjectIdentifier BUSINESS_CATEGORY = new DERObjectIdentifier("2.5.4.15");
    public static final DERObjectIdentifier C = new DERObjectIdentifier("2.5.4.6");
    public static final DERObjectIdentifier CN = new DERObjectIdentifier("2.5.4.3");
    public static final DERObjectIdentifier COUNTRY_OF_CITIZENSHIP = new DERObjectIdentifier("1.3.6.1.5.5.7.9.4");
    public static final DERObjectIdentifier COUNTRY_OF_RESIDENCE = new DERObjectIdentifier("1.3.6.1.5.5.7.9.5");
    public static final DERObjectIdentifier DATE_OF_BIRTH = new DERObjectIdentifier("1.3.6.1.5.5.7.9.1");
    public static final DERObjectIdentifier DC = new DERObjectIdentifier("0.9.2342.19200300.100.1.25");
    public static final DERObjectIdentifier DN_QUALIFIER = new DERObjectIdentifier("2.5.4.46");
    public static final Hashtable DefaultLookUp = new Hashtable();
    public static boolean DefaultReverse = PKIToolConfig.isReverseDN();
    public static final Hashtable DefaultSymbols = new Hashtable();
    public static final DERObjectIdentifier E = EmailAddress;
    public static final DERObjectIdentifier EmailAddress = PKCSObjectIdentifiers.pkcs_9_at_emailAddress;
    private static final Boolean FALSE = new Boolean(false);
    public static final DERObjectIdentifier GENDER = new DERObjectIdentifier("1.3.6.1.5.5.7.9.3");
    public static final DERObjectIdentifier GENERATION = new DERObjectIdentifier("2.5.4.44");
    public static final DERObjectIdentifier GIVENNAME = new DERObjectIdentifier("2.5.4.42");
    public static final DERObjectIdentifier INITIALS = new DERObjectIdentifier("2.5.4.43");
    public static final DERObjectIdentifier L = new DERObjectIdentifier("2.5.4.7");
    public static final DERObjectIdentifier NAME = X509ObjectIdentifiers.id_at_name;
    public static final DERObjectIdentifier NAME_AT_BIRTH = new DERObjectIdentifier("1.3.36.8.3.14");
    public static final DERObjectIdentifier O = new DERObjectIdentifier("2.5.4.10");
    public static final Hashtable OIDLookUp = DefaultSymbols;
    public static final DERObjectIdentifier OU = new DERObjectIdentifier("2.5.4.11");
    public static final DERObjectIdentifier PLACE_OF_BIRTH = new DERObjectIdentifier("1.3.6.1.5.5.7.9.2");
    public static final DERObjectIdentifier POSTAL_ADDRESS = new DERObjectIdentifier("2.5.4.16");
    public static final DERObjectIdentifier POSTAL_CODE = new DERObjectIdentifier("2.5.4.17");
    public static final DERObjectIdentifier PSEUDONYM = new DERObjectIdentifier("2.5.4.65");
    public static final Hashtable RFC1779Symbols = new Hashtable();
    public static final Hashtable RFC2253Symbols = new Hashtable();
    public static final DERObjectIdentifier SERIALNUMBER = SN;
    public static final DERObjectIdentifier SN = new DERObjectIdentifier("2.5.4.5");
    public static final DERObjectIdentifier ST = new DERObjectIdentifier("2.5.4.8");
    public static final DERObjectIdentifier STREET = new DERObjectIdentifier("2.5.4.9");
    public static final DERObjectIdentifier SURNAME = new DERObjectIdentifier("2.5.4.4");
    public static final Hashtable SymbolLookUp = DefaultLookUp;
    public static final DERObjectIdentifier T = new DERObjectIdentifier("2.5.4.12");
    public static final DERObjectIdentifier TELEPHONE_NUMBER = X509ObjectIdentifiers.id_at_telephoneNumber;
    private static final Boolean TRUE = new Boolean(true);
    public static final DERObjectIdentifier UID = new DERObjectIdentifier("0.9.2342.19200300.100.1.1");
    public static final DERObjectIdentifier UNIQUE_IDENTIFIER = new DERObjectIdentifier("2.5.4.45");
    public static final DERObjectIdentifier UnstructuredAddress = PKCSObjectIdentifiers.pkcs_9_at_unstructuredAddress;
    public static final DERObjectIdentifier UnstructuredName = PKCSObjectIdentifiers.pkcs_9_at_unstructuredName;
    public static boolean UseUTF8String = PKIToolConfig.isUseUTF8String();
    private static final Map regRuleClass = new HashMap();
    private Vector added;
    private Map dnrules;
    private Vector ordering;
    private ASN1Sequence seq;
    private Vector values;

    static {
        DefaultSymbols.put(C, "C");
        DefaultSymbols.put(O, "O");
        DefaultSymbols.put(T, "T");
        DefaultSymbols.put(OU, "OU");
        DefaultSymbols.put(CN, "CN");
        DefaultSymbols.put(L, "L");
        DefaultSymbols.put(ST, "ST");
        DefaultSymbols.put(SN, "SERIALNUMBER");
        DefaultSymbols.put(EmailAddress, "E");
        DefaultSymbols.put(DC, "DC");
        DefaultSymbols.put(UID, "UID");
        DefaultSymbols.put(STREET, "STREET");
        DefaultSymbols.put(SURNAME, "SURNAME");
        DefaultSymbols.put(GIVENNAME, "GIVENNAME");
        DefaultSymbols.put(INITIALS, "INITIALS");
        DefaultSymbols.put(GENERATION, "GENERATION");
        DefaultSymbols.put(UnstructuredAddress, "unstructuredAddress");
        DefaultSymbols.put(UnstructuredName, "unstructuredName");
        DefaultSymbols.put(UNIQUE_IDENTIFIER, "UniqueIdentifier");
        DefaultSymbols.put(DN_QUALIFIER, "DN");
        DefaultSymbols.put(PSEUDONYM, "Pseudonym");
        DefaultSymbols.put(POSTAL_ADDRESS, "PostalAddress");
        DefaultSymbols.put(NAME_AT_BIRTH, "NameAtBirth");
        DefaultSymbols.put(COUNTRY_OF_CITIZENSHIP, "CountryOfCitizenship");
        DefaultSymbols.put(COUNTRY_OF_RESIDENCE, "CountryOfResidence");
        DefaultSymbols.put(GENDER, "Gender");
        DefaultSymbols.put(PLACE_OF_BIRTH, "PlaceOfBirth");
        DefaultSymbols.put(DATE_OF_BIRTH, "DateOfBirth");
        DefaultSymbols.put(POSTAL_CODE, "PostalCode");
        DefaultSymbols.put(BUSINESS_CATEGORY, "BusinessCategory");
        DefaultSymbols.put(TELEPHONE_NUMBER, "TelephoneNumber");
        DefaultSymbols.put(NAME, "Name");
        RFC2253Symbols.put(C, "C");
        RFC2253Symbols.put(O, "O");
        RFC2253Symbols.put(OU, "OU");
        RFC2253Symbols.put(CN, "CN");
        RFC2253Symbols.put(L, "L");
        RFC2253Symbols.put(ST, "ST");
        RFC2253Symbols.put(STREET, "STREET");
        RFC2253Symbols.put(DC, "DC");
        RFC2253Symbols.put(UID, "UID");
        RFC1779Symbols.put(C, "C");
        RFC1779Symbols.put(O, "O");
        RFC1779Symbols.put(OU, "OU");
        RFC1779Symbols.put(CN, "CN");
        RFC1779Symbols.put(L, "L");
        RFC1779Symbols.put(ST, "ST");
        RFC1779Symbols.put(STREET, "STREET");
        DefaultLookUp.put("c", C);
        DefaultLookUp.put("o", O);
        DefaultLookUp.put("t", T);
        DefaultLookUp.put("ou", OU);
        DefaultLookUp.put("cn", CN);
        DefaultLookUp.put("l", L);
        DefaultLookUp.put("st", ST);
        DefaultLookUp.put("sn", SN);
        DefaultLookUp.put("serialnumber", SN);
        DefaultLookUp.put("street", STREET);
        DefaultLookUp.put("emailaddress", E);
        DefaultLookUp.put("dc", DC);
        DefaultLookUp.put("e", E);
        DefaultLookUp.put("uid", UID);
        DefaultLookUp.put("surname", SURNAME);
        DefaultLookUp.put("givenname", GIVENNAME);
        DefaultLookUp.put("initials", INITIALS);
        DefaultLookUp.put("generation", GENERATION);
        DefaultLookUp.put("unstructuredaddress", UnstructuredAddress);
        DefaultLookUp.put("unstructuredname", UnstructuredName);
        DefaultLookUp.put("uniqueidentifier", UNIQUE_IDENTIFIER);
        DefaultLookUp.put("dn", DN_QUALIFIER);
        DefaultLookUp.put("pseudonym", PSEUDONYM);
        DefaultLookUp.put("postaladdress", POSTAL_ADDRESS);
        DefaultLookUp.put("nameofbirth", NAME_AT_BIRTH);
        DefaultLookUp.put("countryofcitizenship", COUNTRY_OF_CITIZENSHIP);
        DefaultLookUp.put("countryofresidence", COUNTRY_OF_RESIDENCE);
        DefaultLookUp.put("gender", GENDER);
        DefaultLookUp.put("placeofbirth", PLACE_OF_BIRTH);
        DefaultLookUp.put("dateofbirth", DATE_OF_BIRTH);
        DefaultLookUp.put("postalcode", POSTAL_CODE);
        DefaultLookUp.put("businesscategory", BUSINESS_CATEGORY);
        DefaultLookUp.put("telephonenumber", TELEPHONE_NUMBER);
        DefaultLookUp.put("name", NAME);
        DefaultLookUp.put("s", ST);
        DefaultLookUp.put("email", E);
        DefaultLookUp.put("title", T);
        DefaultLookUp.put("g", GIVENNAME);
        regRuleClass.put("UTF8String".toUpperCase(), DERUTF8String.class);
        regRuleClass.put("IA5String".toUpperCase(), DERIA5String.class);
        regRuleClass.put("BitString".toUpperCase(), DERBitString.class);
        regRuleClass.put("BMPString".toUpperCase(), DERBMPString.class);
        regRuleClass.put("61String".toUpperCase(), DERT61String.class);
        regRuleClass.put("VisibleString".toUpperCase(), DERVisibleString.class);
        regRuleClass.put("PrintableString".toUpperCase(), DERPrintableString.class);
    }

    public static X509Name getInstance(ASN1TaggedObject obj, boolean explicit) {
        return getInstance(ASN1Sequence.getInstance(obj, explicit));
    }

    public static X509Name getInstance(Object obj) {
        if (obj == null || (obj instanceof X509Name)) {
            return (X509Name) obj;
        }
        if (obj instanceof ASN1Sequence) {
            return new X509Name((ASN1Sequence) obj);
        }
        throw new IllegalArgumentException("unknown object in factory");
    }

    public X509Name(ASN1Sequence seq2) {
        Boolean bool;
        this.dnrules = null;
        this.ordering = new Vector();
        this.values = new Vector();
        this.added = new Vector();
        this.seq = seq2;
        Enumeration e = seq2.getObjects();
        while (e.hasMoreElements()) {
            ASN1Set set = (ASN1Set) e.nextElement();
            for (int i = 0; i < set.size(); i++) {
                ASN1Sequence s = (ASN1Sequence) set.getObjectAt(i);
                this.ordering.addElement(s.getObjectAt(0));
                this.values.addElement(((DERString) s.getObjectAt(1)).getString());
                Vector vector = this.added;
                if (i != 0) {
                    bool = new Boolean(true);
                } else {
                    bool = new Boolean(false);
                }
                vector.addElement(bool);
            }
        }
    }

    public X509Name(Hashtable attributes) {
        this((Vector) null, attributes);
    }

    public X509Name(Vector ordering2, Hashtable attributes) {
        this.dnrules = null;
        this.ordering = new Vector();
        this.values = new Vector();
        this.added = new Vector();
        if (ordering2 != null) {
            for (int i = 0; i != ordering2.size(); i++) {
                this.ordering.addElement(ordering2.elementAt(i));
                this.added.addElement(new Boolean(false));
            }
        } else {
            Enumeration e = attributes.keys();
            while (e.hasMoreElements()) {
                this.ordering.addElement(e.nextElement());
                this.added.addElement(new Boolean(false));
            }
        }
        for (int i2 = 0; i2 != this.ordering.size(); i2++) {
            DERObjectIdentifier oid = (DERObjectIdentifier) this.ordering.elementAt(i2);
            if (attributes.get(oid) == null) {
                throw new IllegalArgumentException("No attribute for object id - " + oid.getId() + " - passed to distinguished name");
            }
            this.values.addElement(attributes.get(oid));
        }
    }

    public X509Name(Vector oids, Vector values2) {
        this.dnrules = null;
        this.ordering = new Vector();
        this.values = new Vector();
        this.added = new Vector();
        if (oids.size() != values2.size()) {
            throw new IllegalArgumentException("oids vector must be same length as values.");
        }
        for (int i = 0; i < oids.size(); i++) {
            this.ordering.addElement(oids.elementAt(i));
            this.values.addElement(values2.elementAt(i));
            this.added.addElement(new Boolean(false));
        }
    }

    public X509Name(String dirName) {
        this(DefaultReverse, DefaultLookUp, dirName);
    }

    public X509Name(boolean reverse, String dirName) {
        this(reverse, DefaultLookUp, dirName);
    }

    public X509Name(boolean reverse, Hashtable lookUp, String dirName) {
        DERObjectIdentifier oid;
        this.dnrules = null;
        this.ordering = new Vector();
        this.values = new Vector();
        this.added = new Vector();
        boolean reverse2 = DefaultReverse;
        if (dirName == null) {
            throw new IllegalArgumentException("badly formated directory string(dnstring is null)");
        }
        X509NameTokenizer nTok = new X509NameTokenizer(dirName.trim());
        while (nTok.hasMoreTokens()) {
            String token = nTok.nextToken();
            int index = token.indexOf(61);
            if (index == -1) {
                throw new IllegalArgumentException("badly formated directory string");
            }
            String name = token.substring(0, index).trim();
            String value = token.substring(index + 1).trim();
            if (name.toUpperCase().startsWith("OID.")) {
                oid = new DERObjectIdentifier(name.substring(4));
            } else if (name.charAt(0) < '0' || name.charAt(0) > '9') {
                oid = (DERObjectIdentifier) lookUp.get(name.toLowerCase());
                if (oid == null) {
                    throw new IllegalArgumentException("Unknown object id - " + name + " - passed to distinguished name");
                }
            } else {
                oid = new DERObjectIdentifier(name);
            }
            this.ordering.addElement(oid);
            this.values.addElement(value);
            this.added.addElement(new Boolean(false));
        }
        if (reverse2) {
            Vector o = new Vector();
            Vector v = new Vector();
            for (int i = this.ordering.size() - 1; i >= 0; i--) {
                o.addElement(this.ordering.elementAt(i));
                v.addElement(this.values.elementAt(i));
                this.added.addElement(new Boolean(false));
            }
            this.ordering = o;
            this.values = v;
        }
    }

    public Vector getOIDs() {
        Vector v = new Vector();
        for (int i = 0; i != this.ordering.size(); i++) {
            v.addElement(this.ordering.elementAt(i));
        }
        return v;
    }

    public Vector getValues() {
        Vector v = new Vector();
        for (int i = 0; i != this.values.size(); i++) {
            v.addElement(this.values.elementAt(i));
        }
        return v;
    }

    private boolean canBePrintable(String str) {
        for (int i = str.length() - 1; i >= 0; i--) {
            if (str.charAt(i) > 127) {
                return false;
            }
        }
        return true;
    }

    public DERObject getDERObject() {
        if (this.seq == null) {
            ASN1EncodableVector vec = new ASN1EncodableVector();
            HashMap rules = (HashMap) PKIToolConfig.getDnRules();
            if (this.dnrules != null) {
                rules = (HashMap) this.dnrules;
            }
            for (int i = 0; i != this.ordering.size(); i++) {
                ASN1EncodableVector v = new ASN1EncodableVector();
                DERObjectIdentifier oid = (DERObjectIdentifier) this.ordering.elementAt(i);
                v.add(oid);
                String str = (String) this.values.elementAt(i);
                if (str.charAt(0) == '#') {
                    String str2 = str.toLowerCase();
                    byte[] data = new byte[(str2.length() / 2)];
                    int nEndTag = 0;
                    for (int index = 0; index != data.length; index++) {
                        char left = str2.charAt((index * 2) + 1);
                        char right = 'a';
                        if ((index * 2) + 2 < data.length) {
                            right = str2.charAt((index * 2) + 2);
                        } else {
                            nEndTag = 1;
                        }
                        if (left < 'a') {
                            data[index] = (byte) ((left - '0') << 4);
                        } else {
                            data[index] = (byte) (((left - 'a') + 10) << 4);
                        }
                        if (nEndTag == 0) {
                            if (right < 'a') {
                                data[index] = (byte) (data[index] | ((byte) (right - '0')));
                            } else {
                                data[index] = (byte) (data[index] | ((byte) ((right - 'a') + 10)));
                            }
                        }
                    }
                    try {
                        v.add(new ASN1InputStream(new ByteArrayInputStream(data)).readObject());
                    } catch (IOException e) {
                        throw new RuntimeException("bad object in '#' string");
                    }
                } else if (rules != null) {
                    String rule = (String) rules.get(oid);
                    Class ruleClass = null;
                    if (rule != null) {
                        ruleClass = (Class) regRuleClass.get(rule.toUpperCase());
                    }
                    if (ruleClass != null) {
                        try {
                            ASN1EncodableVector aSN1EncodableVector = v;
                            aSN1EncodableVector.add((DEREncodable) ruleClass.getConstructor(String.class).newInstance(str));
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                    } else {
                        v.add(new DERUTF8String(str));
                    }
                } else if (oid.equals(EmailAddress)) {
                    v.add(new DERIA5String(str));
                } else if (canBePrintable(str)) {
                    v.add(new DERPrintableString(str));
                } else if (UseUTF8String) {
                    v.add(new DERUTF8String(str));
                } else {
                    v.add(new DERBMPString(str));
                }
                vec.add(new DERSet(new DERSequence(v)));
            }
            this.seq = new DERSequence(vec);
        }
        return this.seq;
    }

    public boolean equals(Object _obj, boolean inOrder) {
        if (_obj == this) {
            return true;
        }
        if (!inOrder) {
            return equals(_obj);
        }
        if (_obj == null || !(_obj instanceof X509Name)) {
            return false;
        }
        X509Name _oxn = (X509Name) _obj;
        int _orderingSize = this.ordering.size();
        if (_orderingSize != _oxn.ordering.size()) {
            return false;
        }
        for (int i = 0; i < _orderingSize; i++) {
            String _oid = ((DERObjectIdentifier) this.ordering.elementAt(i)).getId();
            String _val = (String) this.values.elementAt(i);
            String _oOID = ((DERObjectIdentifier) _oxn.ordering.elementAt(i)).getId();
            String _oVal = (String) _oxn.values.elementAt(i);
            if (_oid.equals(_oOID)) {
                String _val2 = _val.trim().toLowerCase();
                String _oVal2 = _oVal.trim().toLowerCase();
                if (_val2.equals(_oVal2)) {
                    continue;
                } else {
                    StringBuffer v1 = new StringBuffer();
                    StringBuffer v2 = new StringBuffer();
                    if (_val2.length() != 0) {
                        char c1 = _val2.charAt(0);
                        v1.append(c1);
                        for (int k = 1; k < _val2.length(); k++) {
                            char c2 = _val2.charAt(k);
                            if (c1 != ' ' || c2 != ' ') {
                                v1.append(c2);
                            }
                            c1 = c2;
                        }
                    }
                    if (_oVal2.length() != 0) {
                        char c12 = _oVal2.charAt(0);
                        v2.append(c12);
                        for (int k2 = 1; k2 < _oVal2.length(); k2++) {
                            char c22 = _oVal2.charAt(k2);
                            if (c12 != ' ' || c22 != ' ') {
                                v2.append(c22);
                            }
                            c12 = c22;
                        }
                    }
                    if (!v1.toString().equals(v2.toString())) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public boolean equals(Object _obj) {
        if (_obj == this) {
            return true;
        }
        if (_obj == 0 || !(_obj instanceof X509Name)) {
            return false;
        }
        X509Name _oxn = _obj;
        if (getDERObject().equals(_oxn.getDERObject())) {
            return true;
        }
        int _orderingSize = this.ordering.size();
        if (_orderingSize != _oxn.ordering.size()) {
            return false;
        }
        boolean[] _indexes = new boolean[_orderingSize];
        for (int i = 0; i < _orderingSize; i++) {
            boolean _found = false;
            String _oid = ((DERObjectIdentifier) this.ordering.elementAt(i)).getId();
            String _val = (String) this.values.elementAt(i);
            int j = 0;
            while (true) {
                if (j >= _orderingSize) {
                    break;
                }
                if (!_indexes[j]) {
                    String _oOID = ((DERObjectIdentifier) _oxn.ordering.elementAt(j)).getId();
                    String _oVal = (String) _oxn.values.elementAt(j);
                    if (_oid.equals(_oOID)) {
                        _val = _val.trim().toLowerCase();
                        String _oVal2 = _oVal.trim().toLowerCase();
                        if (_val.equals(_oVal2)) {
                            _indexes[j] = true;
                            _found = true;
                            break;
                        }
                        StringBuffer v1 = new StringBuffer();
                        StringBuffer v2 = new StringBuffer();
                        if (_val.length() != 0) {
                            char c1 = _val.charAt(0);
                            v1.append(c1);
                            for (int k = 1; k < _val.length(); k++) {
                                char c2 = _val.charAt(k);
                                if (c1 != ' ' || c2 != ' ') {
                                    v1.append(c2);
                                }
                                c1 = c2;
                            }
                        }
                        if (_oVal2.length() != 0) {
                            char c12 = _oVal2.charAt(0);
                            v2.append(c12);
                            for (int k2 = 1; k2 < _oVal2.length(); k2++) {
                                char c22 = _oVal2.charAt(k2);
                                if (c12 != ' ' || c22 != ' ') {
                                    v2.append(c22);
                                }
                                c12 = c22;
                            }
                        }
                        if (v1.toString().equals(v2.toString())) {
                            _indexes[j] = true;
                            _found = true;
                            break;
                        }
                    } else {
                        continue;
                    }
                }
                j++;
            }
            if (!_found) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        Enumeration e = ((ASN1Sequence) getDERObject()).getObjects();
        int hashCode = 0;
        while (e.hasMoreElements()) {
            hashCode ^= e.nextElement().hashCode();
        }
        return hashCode;
    }

    private void appendValue(StringBuffer buf, Hashtable oidSymbols, DERObjectIdentifier oid, String value) {
        String sym = (String) oidSymbols.get(oid);
        if (sym != null) {
            buf.append(sym);
        } else {
            buf.append(oid.getId());
        }
        buf.append("=");
        int length = buf.length();
        String temp = value.trim();
        boolean nTag = false;
        int i = 0;
        while (true) {
            if (i >= temp.length()) {
                break;
            } else if (temp.charAt(i) == ',' || temp.charAt(i) == '\"' || temp.charAt(i) == '\\' || temp.charAt(i) == '+' || temp.charAt(i) == '<' || temp.charAt(i) == '>' || temp.charAt(i) == ';') {
                nTag = true;
            } else {
                i++;
            }
        }
        if (nTag) {
            value = "\"" + temp + "\"";
        }
        buf.append(value);
        int length2 = buf.length();
    }

    public String toString(boolean reverse, Hashtable oidSymbols) {
        StringBuffer buf = new StringBuffer();
        boolean first = true;
        if (reverse) {
            for (int i = this.ordering.size() - 1; i >= 0; i--) {
                if (first) {
                    first = false;
                } else if (((Boolean) this.added.elementAt(i + 1)).booleanValue()) {
                    buf.append(" + ");
                } else {
                    buf.append(",");
                }
                appendValue(buf, oidSymbols, (DERObjectIdentifier) this.ordering.elementAt(i), (String) this.values.elementAt(i));
            }
        } else {
            for (int i2 = 0; i2 < this.ordering.size(); i2++) {
                if (first) {
                    first = false;
                } else if (((Boolean) this.added.elementAt(i2)).booleanValue()) {
                    buf.append(" + ");
                } else {
                    buf.append(",");
                }
                appendValue(buf, oidSymbols, (DERObjectIdentifier) this.ordering.elementAt(i2), (String) this.values.elementAt(i2));
            }
        }
        return buf.toString().trim();
    }

    public String toString() {
        return toString(DefaultReverse, DefaultSymbols);
    }

    public Vector getValues(DERObjectIdentifier paramDERObjectIdentifier) {
        Vector localVector = new Vector();
        for (int i = 0; i != this.values.size(); i++) {
            if (this.ordering.elementAt(i).equals(paramDERObjectIdentifier)) {
                String str = (String) this.values.elementAt(i);
                if (str.length() > 2 && str.charAt(0) == '\\' && str.charAt(1) == '#') {
                    localVector.addElement(str.substring(1));
                } else {
                    localVector.addElement(str);
                }
            }
        }
        return localVector;
    }

    public void setRules(Map rules) {
        HashMap map = (HashMap) rules;
        if (map != null) {
            this.dnrules = new HashMap();
            for (String key : map.keySet()) {
                DERObjectIdentifier oid = (DERObjectIdentifier) DefaultLookUp.get(key.toLowerCase());
                this.dnrules.put(oid, (String) map.get(key));
            }
        }
    }
}
