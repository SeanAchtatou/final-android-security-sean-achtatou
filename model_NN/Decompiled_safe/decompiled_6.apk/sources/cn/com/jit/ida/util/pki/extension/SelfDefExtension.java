package cn.com.jit.ida.util.pki.extension;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.DERBoolean;
import cn.com.jit.ida.util.pki.asn1.DERIA5String;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.DERPrintableString;
import cn.com.jit.ida.util.pki.asn1.DERUTF8String;
import com.jianq.misc.StringEx;
import java.math.BigInteger;

public class SelfDefExtension extends AbstractSelfDefExtension {
    private String encType = null;
    private String encoding = null;
    private byte[] extensionBytes = null;
    private String extensionValue = null;

    public SelfDefExtension() {
        this.critical = false;
    }

    public SelfDefExtension(DERObject obj, String type) {
        if (type == null) {
            this.extensionValue = null;
            return;
        }
        if (type.equals(AbstractSelfDefExtension.BOOLEAN)) {
            if (((DERBoolean) obj).isTrue()) {
                this.extensionValue = "true";
            } else {
                this.extensionValue = "false";
            }
        } else if (type.equals(AbstractSelfDefExtension.IA5STRING)) {
            this.extensionValue = ((DERIA5String) obj).getString();
            this.extensionBytes = ((DERIA5String) obj).getOctets();
        } else if (type.equals(AbstractSelfDefExtension.INTEGER)) {
            this.extensionValue = String.valueOf(((DERInteger) obj).getValue().longValue());
            this.extensionBytes = ((DERInteger) obj).getValue().toByteArray();
        } else if (type.equals(AbstractSelfDefExtension.PRINTABLESTRING)) {
            this.extensionValue = ((DERPrintableString) obj).getString();
            this.extensionBytes = ((DERPrintableString) obj).getOctets();
        } else if (type.equals(AbstractSelfDefExtension.USERDEFINED)) {
            this.extensionValue = new String(((DEROctetString) obj).getOctets());
            this.extensionBytes = ((DEROctetString) obj).getOctets();
        } else if (type.equals(AbstractSelfDefExtension.UTF8STRING)) {
            this.extensionValue = ((DERUTF8String) obj).getString();
            this.extensionBytes = ((DERUTF8String) obj).getString().getBytes();
        } else {
            this.extensionValue = null;
            return;
        }
        this.encType = type;
    }

    public void setEncoding(String encoding2) throws PKIException {
        if (encoding2 == null || encoding2.equals(StringEx.Empty)) {
            throw new PKIException("8189", PKIException.ILLEGAL_ARGUMENT_DES);
        }
        this.encoding = encoding2;
    }

    public String getEncoding() {
        return this.encoding;
    }

    public void setOID(String oid) throws PKIException {
        if (oid == null || oid.equals(StringEx.Empty)) {
            throw new PKIException("8189", PKIException.ILLEGAL_ARGUMENT_DES);
        }
        this.OID = oid;
    }

    public String getOID() {
        return this.OID;
    }

    public void setCritical(boolean critical) {
        this.critical = critical;
    }

    public boolean getCritical() {
        return this.critical;
    }

    public void setExtensionValue(String value) throws PKIException {
        if (value == null || value.equals(StringEx.Empty)) {
            throw new PKIException("8189", PKIException.ILLEGAL_ARGUMENT_DES);
        }
        this.extensionValue = value;
    }

    public String getExtensionValue() {
        return this.extensionValue;
    }

    public byte[] encode() throws PKIException {
        if (this.encoding == null) {
            throw new PKIException(PKIException.SELF_EXT_ENCODING_NULL, PKIException.SELF_EXT_ENCODING_NULL_DES);
        } else if (this.encoding.equals(AbstractSelfDefExtension.BOOLEAN)) {
            return new DEROctetString(new DERBoolean(Boolean.valueOf(this.extensionValue).booleanValue()).getDERObject()).getOctets();
        } else {
            if (this.encoding.equals(AbstractSelfDefExtension.IA5STRING)) {
                return new DEROctetString(new DERIA5String(this.extensionValue).getDERObject()).getOctets();
            }
            if (this.encoding.equals(AbstractSelfDefExtension.INTEGER)) {
                return new DEROctetString(new DERInteger(new BigInteger(this.extensionValue)).getDERObject()).getOctets();
            }
            if (this.encoding.equals(AbstractSelfDefExtension.PRINTABLESTRING)) {
                return new DEROctetString(new DERPrintableString(this.extensionValue).getDERObject()).getOctets();
            }
            if (this.encoding.equals(AbstractSelfDefExtension.USERDEFINED)) {
                return new DEROctetString(this.extensionValue.getBytes()).getOctets();
            }
            if (this.encoding.equals(AbstractSelfDefExtension.UTF8STRING)) {
                return new DEROctetString(new DERUTF8String(this.extensionValue).getDERObject()).getOctets();
            }
            throw new PKIException(PKIException.SELF_EXT_ENCODING_UNSUP, PKIException.SELF_EXT_ENCODING_UNSUP_DES);
        }
    }

    public String getEncType() {
        return this.encType;
    }

    public byte[] getExtensionBytes() {
        return this.extensionBytes;
    }

    public static void main(String[] args) throws PKIException {
        SelfDefExtension aaa = new SelfDefExtension();
        try {
            aaa.setExtensionValue("士大夫".toString());
            aaa.setEncoding(AbstractSelfDefExtension.USERDEFINED);
            byte[] value = aaa.encode();
            System.out.println("encode data is:");
            System.out.println(value);
            SelfDefExtension bbb = new SelfDefExtension(new DEROctetString(value), AbstractSelfDefExtension.USERDEFINED.toString());
            String newvalue = bbb.getExtensionValue();
            byte[] bytValue = bbb.getExtensionBytes();
            System.out.println("decode data is:");
            System.out.println(newvalue);
            System.out.println(bytValue);
        } catch (Exception e) {
            throw new PKIException(PKIException.CONSTRUCT_SELFDEF_EXTENSION_ERR, PKIException.CONSTRUCT_SELFDEF_EXTENSION_ERR_DES, e);
        }
    }
}
