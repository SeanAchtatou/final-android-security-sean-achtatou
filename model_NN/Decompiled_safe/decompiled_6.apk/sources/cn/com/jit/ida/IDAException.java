package cn.com.jit.ida;

public class IDAException extends Exception {
    protected String errCode = null;
    protected String errDesc = null;
    protected String errDescEx = null;
    protected Exception history = null;

    public IDAException(String _errCode) {
        this.errCode = _errCode;
    }

    public IDAException(String _errCode, Exception _history) {
        this.errCode = _errCode;
        this.history = _history;
    }

    public IDAException(String _errCode, String _errDesc) {
        this.errCode = _errCode;
        this.errDesc = _errDesc;
    }

    public IDAException(String _errCode, String _errDesc, Exception _history) {
        this.errCode = _errCode;
        this.errDesc = _errDesc;
        this.history = _history;
    }

    public String getErrCode() {
        return this.errCode;
    }

    public String getErrDesc() {
        return this.errDesc;
    }

    public Exception getHistory() {
        return this.history;
    }

    public String toString() {
        StringBuffer exString = new StringBuffer();
        exString.append(this.errCode + ": ");
        if (this.errDesc != null) {
            exString.append(getErrDescEx() + "\n");
        }
        if (this.history != null) {
            exString.append(this.history.toString());
        }
        return exString.toString();
    }

    public void appendMsg(String msg) {
        this.errDescEx = msg;
    }

    public String getErrDescEx() {
        if (this.errDescEx == null) {
            return this.errDesc;
        }
        return this.errDesc + "  " + this.errDescEx;
    }

    public String getMessage() {
        StringBuffer Msg = new StringBuffer(4);
        Msg.append("错误码：");
        Msg.append(this.errCode);
        Msg.append("  错误信息：");
        Msg.append(this.errDesc);
        if (this.errDescEx != null) {
            Msg.append("(" + this.errDescEx + ")");
        }
        return Msg.toString();
    }
}
