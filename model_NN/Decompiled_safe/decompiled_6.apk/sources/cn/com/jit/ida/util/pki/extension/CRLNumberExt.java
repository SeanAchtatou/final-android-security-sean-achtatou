package cn.com.jit.ida.util.pki.extension;

import cn.com.jit.ida.util.pki.PKIException;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.x509.CRLNumber;
import cn.com.jit.ida.util.pki.asn1.x509.X509Extensions;
import java.math.BigInteger;

public class CRLNumberExt extends AbstractStandardExtension {
    private BigInteger crlnumber = null;

    public CRLNumberExt() {
        this.OID = X509Extensions.CRLNumber.getId();
        this.critical = false;
    }

    public CRLNumberExt(BigInteger value) {
        this.OID = X509Extensions.CRLNumber.getId();
        this.critical = false;
        this.crlnumber = value;
    }

    public CRLNumberExt(String value) {
        this.OID = X509Extensions.CRLNumber.getId();
        this.critical = false;
        this.crlnumber = new BigInteger(value);
    }

    public CRLNumberExt(int value) {
        this.OID = X509Extensions.CRLNumber.getId();
        this.critical = false;
        this.crlnumber = new BigInteger(String.valueOf(value));
    }

    public void SetCRLNumber(BigInteger value) {
        this.crlnumber = value;
    }

    public void SetCRLNumber(String value) {
        this.crlnumber = new BigInteger(value);
    }

    public void SetCRLNumber(int value) {
        this.crlnumber = new BigInteger(String.valueOf(value));
    }

    public byte[] encode() throws PKIException {
        if (this.crlnumber != null) {
            return new DEROctetString(new CRLNumber(this.crlnumber).getDERObject()).getOctets();
        }
        throw new PKIException(PKIException.EXT_ENCODING_CRLNUMBER_NULL, PKIException.EXT_ENCODING_CRLNUMBER_NULL_DES);
    }

    public boolean getCritical() {
        return this.critical;
    }

    public String getOID() {
        return this.OID;
    }

    public void setCritical(boolean critical) {
        this.critical = critical;
    }
}
