package cn.com.jit.ida.util.pki.asn1.x509;

import cn.com.jit.ida.util.pki.asn1.DERBMPString;

public class CertificateTemplate extends DERBMPString {
    public CertificateTemplate(String string) {
        super(string);
    }

    public CertificateTemplate(byte[] string) {
        super(string);
    }
}
