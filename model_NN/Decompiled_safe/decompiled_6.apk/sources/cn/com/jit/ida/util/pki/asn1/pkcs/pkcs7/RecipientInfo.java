package cn.com.jit.ida.util.pki.asn1.pkcs.pkcs7;

import cn.com.jit.ida.util.pki.asn1.ASN1EncodableVector;
import cn.com.jit.ida.util.pki.asn1.ASN1OctetString;
import cn.com.jit.ida.util.pki.asn1.ASN1Sequence;
import cn.com.jit.ida.util.pki.asn1.DEREncodable;
import cn.com.jit.ida.util.pki.asn1.DERInteger;
import cn.com.jit.ida.util.pki.asn1.DERObject;
import cn.com.jit.ida.util.pki.asn1.DEROctetString;
import cn.com.jit.ida.util.pki.asn1.DERSequence;
import cn.com.jit.ida.util.pki.asn1.x509.AlgorithmIdentifier;

public class RecipientInfo implements DEREncodable {
    private ASN1OctetString encryptedKey;
    private IssuerAndSerialNumber issuerAndSerialNumber;
    private AlgorithmIdentifier keyEncryptionAlgorithm;
    private DERInteger version;

    public static RecipientInfo getInstance(Object o) {
        if (o == null || (o instanceof RecipientInfo)) {
            return (RecipientInfo) o;
        }
        if (o instanceof ASN1Sequence) {
            return new RecipientInfo((ASN1Sequence) o);
        }
        throw new IllegalArgumentException("unknown object in factory:" + o.getClass().getName());
    }

    public RecipientInfo(DERInteger _version, IssuerAndSerialNumber _issuerAndSerialNumber, AlgorithmIdentifier _keyEncryptionAlgorithm, ASN1OctetString _encryptedKey) {
        this.version = _version;
        this.issuerAndSerialNumber = _issuerAndSerialNumber;
        this.keyEncryptionAlgorithm = _keyEncryptionAlgorithm;
        this.encryptedKey = _encryptedKey;
    }

    public RecipientInfo(ASN1Sequence seq) {
        this.version = (DERInteger) seq.getObjectAt(0);
        this.issuerAndSerialNumber = IssuerAndSerialNumber.getInstance(seq.getObjectAt(1));
        this.keyEncryptionAlgorithm = AlgorithmIdentifier.getInstance(seq.getObjectAt(2));
        this.encryptedKey = DEROctetString.getInstance(seq.getObjectAt(3));
    }

    public DERInteger getVersion() {
        return this.version;
    }

    public IssuerAndSerialNumber getIssuerAndSerialNumber() {
        return this.issuerAndSerialNumber;
    }

    public AlgorithmIdentifier getKeyEncryptionAlgorithm() {
        return this.keyEncryptionAlgorithm;
    }

    public ASN1OctetString getEncryptedKey() {
        return this.encryptedKey;
    }

    public DERObject getDERObject() {
        ASN1EncodableVector v = new ASN1EncodableVector();
        v.add(this.version);
        v.add(this.issuerAndSerialNumber);
        v.add(this.keyEncryptionAlgorithm);
        v.add(this.encryptedKey);
        return new DERSequence(v);
    }
}
