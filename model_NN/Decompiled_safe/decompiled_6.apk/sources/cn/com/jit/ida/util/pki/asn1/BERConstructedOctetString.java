package cn.com.jit.ida.util.pki.asn1;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Vector;

public class BERConstructedOctetString extends DEROctetString {
    private Vector octs;

    private static byte[] toBytes(Vector octs2) {
        ByteArrayOutputStream bOut = new ByteArrayOutputStream();
        int i = 0;
        while (i != octs2.size()) {
            try {
                bOut.write(((DEROctetString) octs2.elementAt(i)).getOctets());
                i++;
            } catch (IOException e) {
                throw new RuntimeException("exception converting octets " + e.toString());
            }
        }
        return bOut.toByteArray();
    }

    public BERConstructedOctetString(byte[] string) {
        super(string);
    }

    public BERConstructedOctetString(Vector octs2) {
        super(toBytes(octs2));
        this.octs = octs2;
    }

    public BERConstructedOctetString(DERObject obj) {
        super(obj);
    }

    public BERConstructedOctetString(DEREncodable obj) {
        super(obj.getDERObject());
    }

    public byte[] getOctets() {
        return this.string;
    }

    public Enumeration getObjects() {
        if (this.octs == null) {
            return generateOcts().elements();
        }
        return this.octs.elements();
    }

    private Vector generateOcts() {
        int start = 0;
        Vector vec = new Vector();
        for (int end = 0; end + 1 < this.string.length; end++) {
            if (this.string[end] == 0 && this.string[end + 1] == 0) {
                byte[] nStr = new byte[((end - start) + 1)];
                System.arraycopy(this.string, start, nStr, 0, nStr.length);
                vec.addElement(new DEROctetString(nStr));
                start = end + 1;
            }
        }
        byte[] nStr2 = new byte[(this.string.length - start)];
        System.arraycopy(this.string, start, nStr2, 0, nStr2.length);
        vec.addElement(new DEROctetString(nStr2));
        return vec;
    }

    public void encode(DEROutputStream out) throws IOException {
        if ((out instanceof ASN1OutputStream) || (out instanceof BEROutputStream)) {
            out.write(36);
            out.write(128);
            if (this.octs != null) {
                for (int i = 0; i != this.octs.size(); i++) {
                    out.writeObject(this.octs.elementAt(i));
                }
            } else {
                int start = 0;
                for (int end = 0; end + 1 < this.string.length; end++) {
                    if (this.string[end] == 0 && this.string[end + 1] == 0) {
                        byte[] nStr = new byte[((end - start) + 1)];
                        System.arraycopy(this.string, start, nStr, 0, nStr.length);
                        out.writeObject(new DEROctetString(nStr));
                        start = end + 1;
                    }
                }
                byte[] nStr2 = new byte[(this.string.length - start)];
                System.arraycopy(this.string, start, nStr2, 0, nStr2.length);
                out.writeObject(new DEROctetString(nStr2));
            }
            out.write(0);
            out.write(0);
            return;
        }
        super.encode(out);
    }
}
