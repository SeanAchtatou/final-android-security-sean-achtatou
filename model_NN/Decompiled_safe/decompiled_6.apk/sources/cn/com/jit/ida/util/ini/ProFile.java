package cn.com.jit.ida.util.ini;

import cn.com.jit.pnxclient.constant.PNXConfigConstant;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.HashMap;

public class ProFile {
    private HashMap content;
    private INIFileLoader loader;

    public ProFile() {
        this.content = null;
        this.loader = null;
        this.content = new HashMap();
    }

    public void load(String filePath) {
        File file = new File(filePath);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (Exception ex) {
                System.out.println(ex.toString());
            }
        }
        this.loader = new INIFileLoader();
        try {
            this.content = this.loader.loadIniFromFile(file);
        } catch (Exception ex1) {
            System.out.println(ex1.toString());
        }
    }

    public void load(InputStream ins) {
        this.loader = new INIFileLoader();
        try {
            this.content = this.loader.loadIniFromStream(ins);
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
    }

    public String[] getSections() {
        return convertObj(this.content.keySet().toArray());
    }

    public String[] getKeys(String section) {
        if (!isExistsSection(section)) {
            return null;
        }
        return convertObj(((HashMap) this.content.get(section)).keySet().toArray());
    }

    public String getValue(String section, String key) {
        if (!isExistsKey(section, key)) {
            return null;
        }
        return convert((String) ((HashMap) this.content.get(section)).get(key));
    }

    public void storeValue(String section, String key, String value) {
        if (isExistsSection(section)) {
            if (isExistsKey(section, key)) {
                ((HashMap) this.content.get(section)).remove(key);
            }
            ((HashMap) this.content.get(section)).put(key, value);
            return;
        }
        createSection(section);
        ((HashMap) this.content.get(section)).put(key, value);
    }

    public void writeToFile(String fileName) throws Exception {
        FileWriter fwriter = new FileWriter(fileName);
        String[] sections = getSections();
        for (int i = 0; i < sections.length; i++) {
            fwriter.write("[" + sections[i] + "]");
            fwriter.write(PNXConfigConstant.RESP_SPLIT_2);
            String[] keys = getKeys(sections[i]);
            if (keys != null) {
                for (int j = 0; j < keys.length; j++) {
                    fwriter.write(keys[j] + "=" + ((String) ((HashMap) this.content.get(sections[i])).get(keys[j])));
                    fwriter.write(PNXConfigConstant.RESP_SPLIT_2);
                }
            }
            fwriter.write(PNXConfigConstant.RESP_SPLIT_2);
        }
        fwriter.close();
    }

    public void writeToOutputStream(OutputStream ous) throws Exception {
        PrintWriter writer = new PrintWriter(ous);
        String[] sections = getSections();
        for (int i = 0; i < sections.length; i++) {
            writer.write("[" + sections[i] + "]");
            writer.write(PNXConfigConstant.RESP_SPLIT_2);
            String[] keys = getKeys(sections[i]);
            if (keys != null) {
                for (int j = 0; j < keys.length; j++) {
                    writer.write(keys[j] + "=" + ((String) ((HashMap) this.content.get(sections[i])).get(keys[j])));
                    writer.write(PNXConfigConstant.RESP_SPLIT_2);
                }
            }
            writer.write(PNXConfigConstant.RESP_SPLIT_2);
        }
        writer.close();
    }

    public void createSection(String section) {
        this.content.put(section, new HashMap());
    }

    public boolean isExistsSection(String section) {
        return this.content.containsKey(section);
    }

    public boolean isExistsKey(String section, String key) {
        if (!isExistsSection(section)) {
            return false;
        }
        return ((HashMap) this.content.get(section)).containsKey(key);
    }

    private String[] convertObj(Object[] obj) {
        if (obj.length == 0) {
            return null;
        }
        String[] s = new String[obj.length];
        for (int i = 0; i < s.length; i++) {
            s[i] = obj[i].toString();
        }
        return s;
    }

    private String convert(String str) {
        StringBuffer sb = new StringBuffer();
        while (true) {
            int index = str.indexOf(">");
            if (index != -1) {
                sb.append(str.substring(0, index));
                str = str.substring(index + 1);
            } else {
                sb.append(str);
                return sb.toString().trim();
            }
        }
    }

    public void clear() {
        this.loader = new INIFileLoader();
    }

    public static void main(String[] args) {
        ProFile pf = new ProFile();
        pf.storeValue("Student Name", "001", "Lijiandfgsdfgsdfgdsfgdsfgdfsgsdfgsdfgsdfgsdgasdfasdfasdfasdddddddddddddddddddfffffffffffffffffffaweeeeeeeeeeeeeeeeeeefffffffffffffffffffasdf");
        pf.storeValue("Student Name", "002", "Zhaozhiwei");
        pf.storeValue("Student Age", "lijian", "23");
        pf.storeValue("Student Age", "tangbin", "23");
        System.out.println((int) "\n".getBytes()[0]);
        try {
            pf.writeToFile("c:/111.ini");
        } catch (Exception e) {
        }
    }
}
