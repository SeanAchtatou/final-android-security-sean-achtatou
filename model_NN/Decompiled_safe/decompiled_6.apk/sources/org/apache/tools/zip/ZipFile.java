package org.apache.tools.zip;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.zip.CRC32;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;
import java.util.zip.ZipException;

public class ZipFile {
    private static final int BYTE_SHIFT = 8;
    private static final int CFD_LOCATOR_OFFSET = 16;
    private static final int CFH_LEN = 42;
    private static final int HASH_SIZE = 509;
    private static final long LFH_OFFSET_FOR_FILENAME_LENGTH = 26;
    private static final int MAX_EOCD_SIZE = 65557;
    private static final int MIN_EOCD_SIZE = 22;
    private static final int NIBLET_MASK = 15;
    private static final int POS_0 = 0;
    private static final int POS_1 = 1;
    private static final int POS_2 = 2;
    private static final int POS_3 = 3;
    private static final int SHORT = 2;
    private static final int WORD = 4;
    /* access modifiers changed from: private */
    public RandomAccessFile archive;
    private String encoding;
    private final Map entries;
    private final Map nameMap;
    private final boolean useUnicodeExtraFields;
    private final ZipEncoding zipEncoding;

    private static final class OffsetEntry {
        /* access modifiers changed from: private */
        public long dataOffset;
        /* access modifiers changed from: private */
        public long headerOffset;

        private OffsetEntry() {
            this.headerOffset = -1;
            this.dataOffset = -1;
        }

        /* synthetic */ OffsetEntry(OffsetEntry offsetEntry) {
            this();
        }
    }

    public ZipFile(File f) throws IOException {
        this(f, (String) null);
    }

    public ZipFile(String name) throws IOException {
        this(new File(name), (String) null);
    }

    public ZipFile(String name, String encoding2) throws IOException {
        this(new File(name), encoding2, true);
    }

    public ZipFile(File f, String encoding2) throws IOException {
        this(f, encoding2, true);
    }

    public ZipFile(File f, String encoding2, boolean useUnicodeExtraFields2) throws IOException {
        this.entries = new HashMap((int) HASH_SIZE);
        this.nameMap = new HashMap((int) HASH_SIZE);
        this.encoding = null;
        this.encoding = encoding2;
        this.zipEncoding = ZipEncodingHelper.getZipEncoding(encoding2);
        this.useUnicodeExtraFields = useUnicodeExtraFields2;
        this.archive = new RandomAccessFile(f, "r");
        boolean success = false;
        try {
            resolveLocalFileHeaderData(populateFromCentralDirectory());
            success = true;
        } finally {
            if (!success) {
                try {
                    this.archive.close();
                } catch (IOException e) {
                }
            }
        }
    }

    public String getEncoding() {
        return this.encoding;
    }

    public void close() throws IOException {
        this.archive.close();
    }

    public static void closeQuietly(ZipFile zipfile) {
        if (zipfile != null) {
            try {
                zipfile.close();
            } catch (IOException e) {
            }
        }
    }

    public Enumeration getEntries() {
        return Collections.enumeration(this.entries.keySet());
    }

    public ZipEntry getEntry(String name) {
        return (ZipEntry) this.nameMap.get(name);
    }

    public InputStream getInputStream(ZipEntry ze) throws IOException, ZipException {
        OffsetEntry offsetEntry = (OffsetEntry) this.entries.get(ze);
        if (offsetEntry == null) {
            return null;
        }
        BoundedInputStream bis = new BoundedInputStream(offsetEntry.dataOffset, ze.getCompressedSize());
        switch (ze.getMethod()) {
            case 0:
                return bis;
            case 8:
                bis.addDummy();
                final Inflater inflater = new Inflater(true);
                return new InflaterInputStream(bis, inflater) {
                    public void close() throws IOException {
                        super.close();
                        inflater.end();
                    }
                };
            default:
                throw new ZipException("Found unsupported compression method " + ze.getMethod());
        }
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 124 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.Map populateFromCentralDirectory() throws java.io.IOException {
        /*
            r27 = this;
            java.util.HashMap r15 = new java.util.HashMap
            r15.<init>()
            r27.positionAtCentralDirectory()
            r25 = 42
            r0 = r25
            byte[] r4 = new byte[r0]
            r25 = 4
            r0 = r25
            byte[] r0 = new byte[r0]
            r20 = r0
            r0 = r27
            java.io.RandomAccessFile r0 = r0.archive
            r25 = r0
            r0 = r25
            r1 = r20
            r0.readFully(r1)
            long r18 = org.apache.tools.zip.ZipLong.getValue(r20)
            byte[] r25 = org.apache.tools.zip.ZipOutputStream.CFH_SIG
            long r5 = org.apache.tools.zip.ZipLong.getValue(r25)
            int r25 = (r18 > r5 ? 1 : (r18 == r5 ? 0 : -1))
            if (r25 == 0) goto L_0x0185
            boolean r25 = r27.startsWithLocalFileHeader()
            if (r25 == 0) goto L_0x0185
            java.io.IOException r25 = new java.io.IOException
            java.lang.String r26 = "central directory is empty, can't expand corrupt archive."
            r25.<init>(r26)
            throw r25
        L_0x003f:
            r0 = r27
            java.io.RandomAccessFile r0 = r0.archive
            r25 = r0
            r0 = r25
            r0.readFully(r4)
            r16 = 0
            org.apache.tools.zip.ZipEntry r24 = new org.apache.tools.zip.ZipEntry
            r24.<init>()
            r0 = r16
            int r23 = org.apache.tools.zip.ZipShort.getValue(r4, r0)
            int r16 = r16 + 2
            int r25 = r23 >> 8
            r25 = r25 & 15
            r24.setPlatform(r25)
            int r16 = r16 + 2
            r0 = r16
            int r13 = org.apache.tools.zip.ZipShort.getValue(r4, r0)
            r0 = r13 & 2048(0x800, float:2.87E-42)
            r25 = r0
            if (r25 == 0) goto L_0x018a
            r14 = 1
        L_0x006f:
            if (r14 == 0) goto L_0x018d
            org.apache.tools.zip.ZipEncoding r9 = org.apache.tools.zip.ZipEncodingHelper.UTF8_ZIP_ENCODING
        L_0x0073:
            int r16 = r16 + 2
            r0 = r16
            int r25 = org.apache.tools.zip.ZipShort.getValue(r4, r0)
            r24.setMethod(r25)
            int r16 = r16 + 2
            r0 = r16
            long r25 = org.apache.tools.zip.ZipLong.getValue(r4, r0)
            long r21 = dosToJavaTime(r25)
            r0 = r24
            r1 = r21
            r0.setTime(r1)
            int r16 = r16 + 4
            r0 = r16
            long r25 = org.apache.tools.zip.ZipLong.getValue(r4, r0)
            r24.setCrc(r25)
            int r16 = r16 + 4
            r0 = r16
            long r25 = org.apache.tools.zip.ZipLong.getValue(r4, r0)
            r24.setCompressedSize(r25)
            int r16 = r16 + 4
            r0 = r16
            long r25 = org.apache.tools.zip.ZipLong.getValue(r4, r0)
            r24.setSize(r25)
            int r16 = r16 + 4
            r0 = r16
            int r12 = org.apache.tools.zip.ZipShort.getValue(r4, r0)
            int r16 = r16 + 2
            r0 = r16
            int r10 = org.apache.tools.zip.ZipShort.getValue(r4, r0)
            int r16 = r16 + 2
            r0 = r16
            int r8 = org.apache.tools.zip.ZipShort.getValue(r4, r0)
            int r16 = r16 + 2
            int r16 = r16 + 2
            r0 = r16
            int r25 = org.apache.tools.zip.ZipShort.getValue(r4, r0)
            r24.setInternalAttributes(r25)
            int r16 = r16 + 2
            r0 = r16
            long r25 = org.apache.tools.zip.ZipLong.getValue(r4, r0)
            r24.setExternalAttributes(r25)
            int r16 = r16 + 4
            byte[] r11 = new byte[r12]
            r0 = r27
            java.io.RandomAccessFile r0 = r0.archive
            r25 = r0
            r0 = r25
            r0.readFully(r11)
            java.lang.String r25 = r9.decode(r11)
            r24.setName(r25)
            org.apache.tools.zip.ZipFile$OffsetEntry r17 = new org.apache.tools.zip.ZipFile$OffsetEntry
            r25 = 0
            r0 = r17
            r1 = r25
            r0.<init>(r1)
            r0 = r16
            long r25 = org.apache.tools.zip.ZipLong.getValue(r4, r0)
            r0 = r17
            r1 = r25
            r0.headerOffset = r1
            r0 = r27
            java.util.Map r0 = r0.entries
            r25 = r0
            r0 = r25
            r1 = r24
            r2 = r17
            r0.put(r1, r2)
            r0 = r27
            java.util.Map r0 = r0.nameMap
            r25 = r0
            java.lang.String r26 = r24.getName()
            r0 = r25
            r1 = r26
            r2 = r24
            r0.put(r1, r2)
            byte[] r3 = new byte[r10]
            r0 = r27
            java.io.RandomAccessFile r0 = r0.archive
            r25 = r0
            r0 = r25
            r0.readFully(r3)
            r0 = r24
            r0.setCentralDirectoryExtra(r3)
            byte[] r7 = new byte[r8]
            r0 = r27
            java.io.RandomAccessFile r0 = r0.archive
            r25 = r0
            r0 = r25
            r0.readFully(r7)
            java.lang.String r25 = r9.decode(r7)
            r24.setComment(r25)
            r0 = r27
            java.io.RandomAccessFile r0 = r0.archive
            r25 = r0
            r0 = r25
            r1 = r20
            r0.readFully(r1)
            long r18 = org.apache.tools.zip.ZipLong.getValue(r20)
            if (r14 != 0) goto L_0x0185
            r0 = r27
            boolean r0 = r0.useUnicodeExtraFields
            r25 = r0
            if (r25 == 0) goto L_0x0185
            org.apache.tools.zip.ZipFile$NameAndComment r25 = new org.apache.tools.zip.ZipFile$NameAndComment
            r26 = 0
            r0 = r25
            r1 = r26
            r0.<init>(r11, r7, r1)
            r0 = r24
            r1 = r25
            r15.put(r0, r1)
        L_0x0185:
            int r25 = (r18 > r5 ? 1 : (r18 == r5 ? 0 : -1))
            if (r25 == 0) goto L_0x003f
            return r15
        L_0x018a:
            r14 = 0
            goto L_0x006f
        L_0x018d:
            r0 = r27
            org.apache.tools.zip.ZipEncoding r9 = r0.zipEncoding
            goto L_0x0073
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.tools.zip.ZipFile.populateFromCentralDirectory():java.util.Map");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    private void positionAtCentralDirectory() throws IOException {
        boolean found = false;
        long off = this.archive.length() - 22;
        long stopSearching = Math.max(0L, this.archive.length() - 65557);
        if (off >= 0) {
            byte[] sig = ZipOutputStream.EOCD_SIG;
            while (true) {
                if (off >= stopSearching) {
                    this.archive.seek(off);
                    int curr = this.archive.read();
                    if (curr != -1) {
                        if (curr == sig[0] && this.archive.read() == sig[1] && this.archive.read() == sig[2] && this.archive.read() == sig[3]) {
                            found = true;
                            break;
                        }
                        off--;
                    } else {
                        break;
                    }
                } else {
                    break;
                }
            }
        }
        if (!found) {
            throw new ZipException("archive is not a ZIP archive");
        }
        this.archive.seek(16 + off);
        byte[] cfdOffset = new byte[4];
        this.archive.readFully(cfdOffset);
        this.archive.seek(ZipLong.getValue(cfdOffset));
    }

    private void resolveLocalFileHeaderData(Map entriesWithoutUTF8Flag) throws IOException {
        Enumeration e = Collections.enumeration(new HashSet(this.entries.keySet()));
        while (e.hasMoreElements()) {
            ZipEntry ze = (ZipEntry) e.nextElement();
            OffsetEntry offsetEntry = (OffsetEntry) this.entries.get(ze);
            long offset = offsetEntry.headerOffset;
            this.archive.seek(LFH_OFFSET_FOR_FILENAME_LENGTH + offset);
            byte[] b = new byte[2];
            this.archive.readFully(b);
            int fileNameLen = ZipShort.getValue(b);
            this.archive.readFully(b);
            int extraFieldLen = ZipShort.getValue(b);
            int lenToSkip = fileNameLen;
            while (lenToSkip > 0) {
                int skipped = this.archive.skipBytes(lenToSkip);
                if (skipped <= 0) {
                    throw new RuntimeException("failed to skip file name in local file header");
                }
                lenToSkip -= skipped;
            }
            byte[] localExtraData = new byte[extraFieldLen];
            this.archive.readFully(localExtraData);
            ze.setExtra(localExtraData);
            offsetEntry.dataOffset = LFH_OFFSET_FOR_FILENAME_LENGTH + offset + 2 + 2 + ((long) fileNameLen) + ((long) extraFieldLen);
            if (entriesWithoutUTF8Flag.containsKey(ze)) {
                this.entries.remove(ze);
                setNameAndCommentFromExtraFields(ze, (NameAndComment) entriesWithoutUTF8Flag.get(ze));
                this.entries.put(ze, offsetEntry);
            }
        }
    }

    protected static Date fromDosTime(ZipLong zipDosTime) {
        return new Date(dosToJavaTime(zipDosTime.getValue()));
    }

    private static long dosToJavaTime(long dosTime) {
        Calendar cal = Calendar.getInstance();
        cal.set(1, ((int) ((dosTime >> 25) & 127)) + 1980);
        cal.set(2, ((int) ((dosTime >> 21) & 15)) - 1);
        cal.set(5, ((int) (dosTime >> 16)) & 31);
        cal.set(11, ((int) (dosTime >> 11)) & 31);
        cal.set(12, ((int) (dosTime >> 5)) & 63);
        cal.set(13, ((int) (dosTime << 1)) & 62);
        return cal.getTime().getTime();
    }

    /* access modifiers changed from: protected */
    public String getString(byte[] bytes) throws ZipException {
        try {
            return ZipEncodingHelper.getZipEncoding(this.encoding).decode(bytes);
        } catch (IOException ex) {
            throw new ZipException("Failed to decode name: " + ex.getMessage());
        }
    }

    private boolean startsWithLocalFileHeader() throws IOException {
        this.archive.seek(0);
        byte[] start = new byte[4];
        this.archive.readFully(start);
        for (int i = 0; i < start.length; i++) {
            if (start[i] != ZipOutputStream.LFH_SIG[i]) {
                return false;
            }
        }
        return true;
    }

    private void setNameAndCommentFromExtraFields(ZipEntry ze, NameAndComment nc) {
        String newComment;
        String originalName = ze.getName();
        String newName = getUnicodeStringIfOriginalMatches((UnicodePathExtraField) ze.getExtraField(UnicodePathExtraField.UPATH_ID), nc.name);
        if (newName != null && !originalName.equals(newName)) {
            ze.setName(newName);
            this.nameMap.remove(originalName);
            this.nameMap.put(newName, ze);
        }
        if (nc.comment != null && nc.comment.length > 0 && (newComment = getUnicodeStringIfOriginalMatches((UnicodeCommentExtraField) ze.getExtraField(UnicodeCommentExtraField.UCOM_ID), nc.comment)) != null) {
            ze.setComment(newComment);
        }
    }

    private String getUnicodeStringIfOriginalMatches(AbstractUnicodeExtraField f, byte[] orig) {
        if (f == null) {
            return null;
        }
        CRC32 crc32 = new CRC32();
        crc32.update(orig);
        if (crc32.getValue() != f.getNameCRC32()) {
            return null;
        }
        try {
            return ZipEncodingHelper.UTF8_ZIP_ENCODING.decode(f.getUnicodeName());
        } catch (IOException e) {
            return null;
        }
    }

    private class BoundedInputStream extends InputStream {
        private boolean addDummyByte = false;
        private long loc;
        private long remaining;

        BoundedInputStream(long start, long remaining2) {
            this.remaining = remaining2;
            this.loc = start;
        }

        public int read() throws IOException {
            int read;
            long j = this.remaining;
            this.remaining = j - 1;
            if (j > 0) {
                synchronized (ZipFile.this.archive) {
                    RandomAccessFile access$0 = ZipFile.this.archive;
                    long j2 = this.loc;
                    this.loc = j2 + 1;
                    access$0.seek(j2);
                    read = ZipFile.this.archive.read();
                }
                return read;
            } else if (!this.addDummyByte) {
                return -1;
            } else {
                this.addDummyByte = false;
                return 0;
            }
        }

        public int read(byte[] b, int off, int len) throws IOException {
            int ret;
            if (this.remaining <= 0) {
                if (!this.addDummyByte) {
                    return -1;
                }
                this.addDummyByte = false;
                b[off] = 0;
                return 1;
            } else if (len <= 0) {
                return 0;
            } else {
                if (((long) len) > this.remaining) {
                    len = (int) this.remaining;
                }
                synchronized (ZipFile.this.archive) {
                    ZipFile.this.archive.seek(this.loc);
                    ret = ZipFile.this.archive.read(b, off, len);
                }
                if (ret <= 0) {
                    return ret;
                }
                this.loc += (long) ret;
                this.remaining -= (long) ret;
                return ret;
            }
        }

        /* access modifiers changed from: package-private */
        public void addDummy() {
            this.addDummyByte = true;
        }
    }

    private static final class NameAndComment {
        /* access modifiers changed from: private */
        public final byte[] comment;
        /* access modifiers changed from: private */
        public final byte[] name;

        private NameAndComment(byte[] name2, byte[] comment2) {
            this.name = name2;
            this.comment = comment2;
        }

        /* synthetic */ NameAndComment(byte[] bArr, byte[] bArr2, NameAndComment nameAndComment) {
            this(bArr, bArr2);
        }
    }
}
