package org.apache.tools.zip;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;
import java.util.HashMap;
import java.util.Map;

abstract class ZipEncodingHelper {
    private static final byte[] HEX_DIGITS = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70};
    static final String UTF8 = "UTF8";
    static final ZipEncoding UTF8_ZIP_ENCODING = new FallbackZipEncoding(UTF8);
    private static final String UTF_DASH_8 = "utf-8";
    private static final Map simpleEncodings = new HashMap();

    ZipEncodingHelper() {
    }

    private static class SimpleEncodingHolder {
        private Simple8BitZipEncoding encoding;
        private final char[] highChars;

        SimpleEncodingHolder(char[] highChars2) {
            this.highChars = highChars2;
        }

        public synchronized Simple8BitZipEncoding getEncoding() {
            if (this.encoding == null) {
                this.encoding = new Simple8BitZipEncoding(this.highChars);
            }
            return this.encoding;
        }
    }

    static {
        SimpleEncodingHolder cp437 = new SimpleEncodingHolder(new char[]{199, 252, 233, 226, 228, 224, 229, 231, 234, 235, 232, 239, 238, 236, 196, 197, 201, 230, 198, 244, 246, 242, 251, 249, 255, 214, 220, 162, 163, 165, 8359, 402, 225, 237, 243, 250, 241, 209, 170, 186, 191, 8976, 172, 189, 188, 161, 171, 187, 9617, 9618, 9619, 9474, 9508, 9569, 9570, 9558, 9557, 9571, 9553, 9559, 9565, 9564, 9563, 9488, 9492, 9524, 9516, 9500, 9472, 9532, 9566, 9567, 9562, 9556, 9577, 9574, 9568, 9552, 9580, 9575, 9576, 9572, 9573, 9561, 9560, 9554, 9555, 9579, 9578, 9496, 9484, 9608, 9604, 9612, 9616, 9600, 945, 223, 915, 960, 931, 963, 181, 964, 934, 920, 937, 948, 8734, 966, 949, 8745, 8801, 177, 8805, 8804, 8992, 8993, 247, 8776, 176, 8729, 183, 8730, 8319, 178, 9632, 160});
        simpleEncodings.put("CP437", cp437);
        simpleEncodings.put("Cp437", cp437);
        simpleEncodings.put("cp437", cp437);
        simpleEncodings.put("IBM437", cp437);
        simpleEncodings.put("ibm437", cp437);
        SimpleEncodingHolder cp850 = new SimpleEncodingHolder(new char[]{199, 252, 233, 226, 228, 224, 229, 231, 234, 235, 232, 239, 238, 236, 196, 197, 201, 230, 198, 244, 246, 242, 251, 249, 255, 214, 220, 248, 163, 216, 215, 402, 225, 237, 243, 250, 241, 209, 170, 186, 191, 174, 172, 189, 188, 161, 171, 187, 9617, 9618, 9619, 9474, 9508, 193, 194, 192, 169, 9571, 9553, 9559, 9565, 162, 165, 9488, 9492, 9524, 9516, 9500, 9472, 9532, 227, 195, 9562, 9556, 9577, 9574, 9568, 9552, 9580, 164, 240, 208, 202, 203, 200, 305, 205, 206, 207, 9496, 9484, 9608, 9604, 166, 204, 9600, 211, 223, 212, 210, 245, 213, 181, 254, 222, 218, 219, 217, 253, 221, 175, 180, 173, 177, 8215, 190, 182, 167, 247, 184, 176, 168, 183, 185, 179, 178, 9632, 160});
        simpleEncodings.put("CP850", cp850);
        simpleEncodings.put("Cp850", cp850);
        simpleEncodings.put("cp850", cp850);
        simpleEncodings.put("IBM850", cp850);
        simpleEncodings.put("ibm850", cp850);
    }

    static ByteBuffer growBuffer(ByteBuffer b, int newCapacity) {
        b.limit(b.position());
        b.rewind();
        int c2 = b.capacity() * 2;
        if (c2 >= newCapacity) {
            newCapacity = c2;
        }
        ByteBuffer on = ByteBuffer.allocate(newCapacity);
        on.put(b);
        return on;
    }

    static void appendSurrogate(ByteBuffer bb, char c) {
        bb.put((byte) 37);
        bb.put((byte) 85);
        bb.put(HEX_DIGITS[(c >> 12) & 15]);
        bb.put(HEX_DIGITS[(c >> 8) & 15]);
        bb.put(HEX_DIGITS[(c >> 4) & 15]);
        bb.put(HEX_DIGITS[c & 15]);
    }

    static ZipEncoding getZipEncoding(String name) {
        if (isUTF8(name)) {
            return UTF8_ZIP_ENCODING;
        }
        if (name == null) {
            return new FallbackZipEncoding();
        }
        SimpleEncodingHolder h = (SimpleEncodingHolder) simpleEncodings.get(name);
        if (h != null) {
            return h.getEncoding();
        }
        try {
            return new NioZipEncoding(Charset.forName(name));
        } catch (UnsupportedCharsetException e) {
            return new FallbackZipEncoding(name);
        }
    }

    static boolean isUTF8(String encoding) {
        if (encoding == null) {
            encoding = System.getProperty("file.encoding");
        }
        return UTF8.equalsIgnoreCase(encoding) || UTF_DASH_8.equalsIgnoreCase(encoding);
    }
}
