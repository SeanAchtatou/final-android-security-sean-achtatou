package org.bouncycastle.sasn1;

import java.io.ByteArrayInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;

public class Asn1InputStream {
    private boolean _eofFound;
    InputStream _in;
    private int _limit;

    public Asn1InputStream(InputStream inputStream) {
        this._in = inputStream;
        this._limit = Integer.MAX_VALUE;
    }

    public Asn1InputStream(InputStream inputStream, int i) {
        this._in = inputStream;
        this._limit = i;
    }

    public Asn1InputStream(byte[] bArr) {
        this._in = new ByteArrayInputStream(bArr);
        this._limit = bArr.length;
    }

    private int readLength() throws IOException {
        int i = 0;
        int read = this._in.read();
        if (read < 0) {
            throw new IOException("EOF found when length expected");
        } else if (read == 128) {
            return -1;
        } else {
            if (read <= 127) {
                return read;
            }
            int i2 = read & 127;
            if (i2 > 4) {
                throw new IOException("DER length more than 4 bytes");
            }
            int i3 = 0;
            while (i < i2) {
                int read2 = this._in.read();
                if (read2 < 0) {
                    throw new IOException("EOF found reading length");
                }
                i++;
                i3 = read2 + (i3 << 8);
            }
            if (i3 < 0) {
                throw new IOException("corrupted stream - negative length found");
            } else if (i3 < this._limit) {
                return i3;
            } else {
                throw new IOException("corrupted stream - out of bounds length found");
            }
        }
    }

    /* access modifiers changed from: package-private */
    public InputStream getParentStream() {
        return this._in;
    }

    public Asn1Object readObject() throws IOException {
        int i;
        int i2 = 0;
        int read = this._in.read();
        if (read != -1) {
            if (this._in instanceof IndefiniteLengthInputStream) {
                ((IndefiniteLengthInputStream) this._in).setEofOn00(false);
            }
            int i3 = read & -33;
            if ((read & 128) != 0) {
                i = read & 31;
                if (i == 31) {
                    int read2 = this._in.read();
                    while (read2 >= 0 && (read2 & 128) != 0) {
                        i2 = ((read2 & 127) | i2) << 7;
                        read2 = this._in.read();
                    }
                    if (read2 < 0) {
                        this._eofFound = true;
                        throw new EOFException("EOF encountered inside tag value.");
                    }
                    i = (read2 & 127) | i2;
                }
            } else {
                i = i3;
            }
            int readLength = readLength();
            if (readLength < 0) {
                IndefiniteLengthInputStream indefiniteLengthInputStream = new IndefiniteLengthInputStream(this._in);
                switch (i3) {
                    case 4:
                        return new BerOctetString(read, indefiniteLengthInputStream);
                    case 5:
                        return new Asn1Null(read);
                    case 16:
                        return new BerSequence(read, indefiniteLengthInputStream);
                    case 17:
                        return new BerSet(read, indefiniteLengthInputStream);
                    default:
                        return new Asn1TaggedObject(read, i, indefiniteLengthInputStream);
                }
            } else {
                DefiniteLengthInputStream definiteLengthInputStream = new DefiniteLengthInputStream(this._in, readLength);
                switch (i3) {
                    case 2:
                        return new Asn1Integer(read, definiteLengthInputStream.toByteArray());
                    case 4:
                        return new DerOctetString(read, definiteLengthInputStream.toByteArray());
                    case 5:
                        return new Asn1Null(read);
                    case 6:
                        return new Asn1ObjectIdentifier(read, definiteLengthInputStream.toByteArray());
                    case 16:
                        return new DerSequence(read, definiteLengthInputStream.toByteArray());
                    case 17:
                        return new DerSet(read, definiteLengthInputStream.toByteArray());
                    default:
                        return new Asn1TaggedObject(read, i, definiteLengthInputStream);
                }
            }
        } else if (this._eofFound) {
            throw new EOFException("attempt to read past end of file.");
        } else {
            this._eofFound = true;
            return null;
        }
    }
}
