package org.bouncycastle.sasn1;

import java.io.IOException;

public class DerSequence extends DerObject implements Asn1Sequence {
    private Asn1InputStream _aIn;

    DerSequence(int i, byte[] bArr) {
        super(i, 16, bArr);
        this._aIn = new Asn1InputStream(bArr);
    }

    public Asn1Object readObject() throws IOException {
        return this._aIn.readObject();
    }
}
