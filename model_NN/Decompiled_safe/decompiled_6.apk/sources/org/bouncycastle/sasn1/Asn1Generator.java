package org.bouncycastle.sasn1;

import java.io.OutputStream;

public abstract class Asn1Generator {
    protected OutputStream _out;

    public Asn1Generator(OutputStream outputStream) {
        this._out = outputStream;
    }

    public abstract OutputStream getRawOutputStream();
}
