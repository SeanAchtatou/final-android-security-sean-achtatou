package org.bouncycastle.sasn1;

import java.io.IOException;

public class DerSet extends DerObject implements Asn1Set {
    private Asn1InputStream _aIn;

    DerSet(int i, byte[] bArr) {
        super(i, 17, bArr);
        this._aIn = new Asn1InputStream(bArr);
    }

    public Asn1Object readObject() throws IOException {
        return this._aIn.readObject();
    }
}
