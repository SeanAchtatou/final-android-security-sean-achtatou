package org.bouncycastle.sasn1;

import java.io.InputStream;

public abstract class Asn1Object {
    protected int _baseTag;
    protected InputStream _contentStream;
    protected int _tagNumber;

    protected Asn1Object(int i, int i2, InputStream inputStream) {
        this._baseTag = i;
        this._tagNumber = i2;
        this._contentStream = inputStream;
    }

    public InputStream getRawContentStream() {
        return this._contentStream;
    }

    public int getTagNumber() {
        return this._tagNumber;
    }

    public boolean isConstructed() {
        return (this._baseTag & 32) != 0;
    }
}
