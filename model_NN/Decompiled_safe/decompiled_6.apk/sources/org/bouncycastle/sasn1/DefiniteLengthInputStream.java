package org.bouncycastle.sasn1;

import java.io.IOException;
import java.io.InputStream;

class DefiniteLengthInputStream extends LimitedInputStream {
    private int _length;

    DefiniteLengthInputStream(InputStream inputStream, int i) {
        super(inputStream);
        this._length = i;
    }

    public int read() throws IOException {
        int i = this._length;
        this._length = i - 1;
        if (i > 0) {
            return this._in.read();
        }
        setParentEofDetect(true);
        return -1;
    }
}
