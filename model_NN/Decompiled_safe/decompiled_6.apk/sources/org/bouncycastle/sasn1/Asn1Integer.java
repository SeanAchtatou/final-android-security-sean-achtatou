package org.bouncycastle.sasn1;

import java.io.IOException;
import java.math.BigInteger;

public class Asn1Integer extends DerObject {
    private BigInteger _value;

    protected Asn1Integer(int i, byte[] bArr) throws IOException {
        super(i, 2, bArr);
        this._value = new BigInteger(bArr);
    }

    public Asn1Integer(long j) {
        this(BigInteger.valueOf(j));
    }

    public Asn1Integer(BigInteger bigInteger) {
        super(0, 2, bigInteger.toByteArray());
        this._value = bigInteger;
    }

    public BigInteger getValue() {
        return this._value;
    }
}
