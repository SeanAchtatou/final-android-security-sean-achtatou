package org.bouncycastle.sasn1;

import java.io.IOException;
import java.io.OutputStream;

public class BerSequenceGenerator extends BerGenerator {
    public BerSequenceGenerator(OutputStream outputStream) throws IOException {
        super(outputStream);
        writeBerHeader(48);
    }

    public BerSequenceGenerator(OutputStream outputStream, int i, boolean z) throws IOException {
        super(outputStream, i, z);
        writeBerHeader(48);
    }

    public void addObject(DerObject derObject) throws IOException {
        this._out.write(derObject.getEncoded());
    }

    public void close() throws IOException {
        writeBerEnd();
    }
}
