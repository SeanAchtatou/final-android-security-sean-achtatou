package org.bouncycastle.sasn1;

import java.io.IOException;
import java.io.InputStream;

public class Asn1TaggedObject extends Asn1Object {
    protected Asn1TaggedObject(int i, int i2, InputStream inputStream) {
        super(i, i2, inputStream);
    }

    public Asn1Object getObject(int i, boolean z) throws IOException {
        if (z) {
            return new Asn1InputStream(getRawContentStream()).readObject();
        }
        switch (i) {
            case 4:
                return getRawContentStream() instanceof IndefiniteLengthInputStream ? new BerOctetString(32, getRawContentStream()) : isConstructed() ? new DerOctetString(32, ((DefiniteLengthInputStream) getRawContentStream()).toByteArray()) : new DerOctetString(0, ((DefiniteLengthInputStream) getRawContentStream()).toByteArray());
            case 16:
                return getRawContentStream() instanceof IndefiniteLengthInputStream ? new BerSequence(32, getRawContentStream()) : new DerSequence(32, ((DefiniteLengthInputStream) getRawContentStream()).toByteArray());
            case 17:
                return getRawContentStream() instanceof IndefiniteLengthInputStream ? new BerSet(32, getRawContentStream()) : new DerSet(32, ((DefiniteLengthInputStream) getRawContentStream()).toByteArray());
            default:
                throw new RuntimeException("implicit tagging not implemented");
        }
    }
}
