package org.bouncycastle.sasn1;

import java.io.IOException;
import java.io.InputStream;

class IndefiniteLengthInputStream extends LimitedInputStream {
    private int _b1;
    private int _b2;
    private boolean _eofOn00 = true;
    private boolean _eofReached = false;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    IndefiniteLengthInputStream(InputStream inputStream) throws IOException {
        super(inputStream);
        boolean z = true;
        this._b1 = inputStream.read();
        this._b2 = inputStream.read();
        this._eofReached = this._b2 >= 0 ? false : z;
    }

    /* access modifiers changed from: package-private */
    public void checkForEof() throws IOException {
        if (this._eofOn00 && this._b1 == 0 && this._b2 == 0) {
            this._eofReached = true;
            setParentEofDetect(true);
        }
    }

    public int read() throws IOException {
        checkForEof();
        if (this._eofReached) {
            return -1;
        }
        int read = this._in.read();
        if (read < 0) {
            this._eofReached = true;
            return -1;
        }
        int i = this._b1;
        this._b1 = this._b2;
        this._b2 = read;
        return i;
    }

    /* access modifiers changed from: package-private */
    public void setEofOn00(boolean z) {
        this._eofOn00 = z;
    }
}
