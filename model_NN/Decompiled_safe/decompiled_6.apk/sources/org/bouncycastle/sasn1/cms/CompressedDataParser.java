package org.bouncycastle.sasn1.cms;

import java.io.IOException;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.sasn1.Asn1Integer;
import org.bouncycastle.sasn1.Asn1Sequence;
import org.bouncycastle.sasn1.DerSequence;

public class CompressedDataParser {
    private AlgorithmIdentifier _compressionAlgorithm;
    private ContentInfoParser _encapContentInfo;
    private Asn1Integer _version;

    public CompressedDataParser(Asn1Sequence asn1Sequence) throws IOException {
        this._version = (Asn1Integer) asn1Sequence.readObject();
        this._compressionAlgorithm = AlgorithmIdentifier.getInstance(new ASN1InputStream(((DerSequence) asn1Sequence.readObject()).getEncoded()).readObject());
        this._encapContentInfo = new ContentInfoParser((Asn1Sequence) asn1Sequence.readObject());
    }

    public AlgorithmIdentifier getCompressionAlgorithmIdentifier() {
        return this._compressionAlgorithm;
    }

    public ContentInfoParser getEncapContentInfo() {
        return this._encapContentInfo;
    }

    public Asn1Integer getVersion() {
        return this._version;
    }
}
