package org.bouncycastle.sasn1.cms;

import java.io.IOException;
import org.bouncycastle.sasn1.Asn1Object;
import org.bouncycastle.sasn1.Asn1ObjectIdentifier;
import org.bouncycastle.sasn1.Asn1Sequence;
import org.bouncycastle.sasn1.Asn1TaggedObject;

public class ContentInfoParser {
    private Asn1TaggedObject content;
    private Asn1ObjectIdentifier contentType;

    public ContentInfoParser(Asn1Sequence asn1Sequence) throws IOException {
        this.contentType = (Asn1ObjectIdentifier) asn1Sequence.readObject();
        this.content = (Asn1TaggedObject) asn1Sequence.readObject();
    }

    public Asn1Object getContent(int i) throws IOException {
        if (this.content != null) {
            return this.content.getObject(i, true);
        }
        return null;
    }

    public Asn1ObjectIdentifier getContentType() {
        return this.contentType;
    }
}
