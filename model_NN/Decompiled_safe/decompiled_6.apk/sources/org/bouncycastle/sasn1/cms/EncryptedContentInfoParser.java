package org.bouncycastle.sasn1.cms;

import java.io.IOException;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.sasn1.Asn1Object;
import org.bouncycastle.sasn1.Asn1ObjectIdentifier;
import org.bouncycastle.sasn1.Asn1Sequence;
import org.bouncycastle.sasn1.Asn1TaggedObject;
import org.bouncycastle.sasn1.DerSequence;

public class EncryptedContentInfoParser {
    private AlgorithmIdentifier _contentEncryptionAlgorithm;
    private Asn1ObjectIdentifier _contentType;
    private Asn1TaggedObject _encryptedContent;

    public EncryptedContentInfoParser(Asn1Sequence asn1Sequence) throws IOException {
        this._contentType = (Asn1ObjectIdentifier) asn1Sequence.readObject();
        this._contentEncryptionAlgorithm = AlgorithmIdentifier.getInstance(new ASN1InputStream(((DerSequence) asn1Sequence.readObject()).getEncoded()).readObject());
        this._encryptedContent = (Asn1TaggedObject) asn1Sequence.readObject();
    }

    public AlgorithmIdentifier getContentEncryptionAlgorithm() {
        return this._contentEncryptionAlgorithm;
    }

    public Asn1ObjectIdentifier getContentType() {
        return this._contentType;
    }

    public Asn1Object getEncryptedContent(int i) throws IOException {
        return this._encryptedContent.getObject(i, false);
    }
}
