package org.bouncycastle.sasn1.cms;

import java.io.IOException;
import org.bouncycastle.sasn1.Asn1Integer;
import org.bouncycastle.sasn1.Asn1Object;
import org.bouncycastle.sasn1.Asn1Sequence;
import org.bouncycastle.sasn1.Asn1Set;
import org.bouncycastle.sasn1.Asn1TaggedObject;

public class EnvelopedDataParser {
    private Asn1Object _nextObject;
    private Asn1Sequence _seq;
    private Asn1Integer _version;

    public EnvelopedDataParser(Asn1Sequence asn1Sequence) throws IOException {
        this._seq = asn1Sequence;
        this._version = (Asn1Integer) asn1Sequence.readObject();
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: org.bouncycastle.sasn1.Asn1TaggedObject.getTagNumber():int in method: org.bouncycastle.sasn1.cms.EnvelopedDataParser.getCertificates():org.bouncycastle.sasn1.Asn1Set, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: org.bouncycastle.sasn1.Asn1TaggedObject.getTagNumber():int
        	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
        	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
        	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:540)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public org.bouncycastle.sasn1.Asn1Set getCertificates() {
        /*
            r4 = this;
            r1 = 0
            org.bouncycastle.sasn1.Asn1Sequence r0 = r4._seq
            org.bouncycastle.sasn1.Asn1Object r0 = r0.readObject()
            r4._nextObject = r0
            org.bouncycastle.sasn1.Asn1Object r0 = r4._nextObject
            boolean r0 = r0 instanceof org.bouncycastle.sasn1.Asn1TaggedObject
            if (r0 == 0) goto L_0x0029
            org.bouncycastle.sasn1.Asn1Object r0 = r4._nextObject
            org.bouncycastle.sasn1.Asn1TaggedObject r0 = (org.bouncycastle.sasn1.Asn1TaggedObject) r0
            int r0 = r0.getTagNumber()
            if (r0 != 0) goto L_0x0029
            org.bouncycastle.sasn1.Asn1Object r0 = r4._nextObject
            org.bouncycastle.sasn1.Asn1TaggedObject r0 = (org.bouncycastle.sasn1.Asn1TaggedObject) r0
            r2 = 17
            r3 = 0
            org.bouncycastle.sasn1.Asn1Object r0 = r0.getObject(r2, r3)
            org.bouncycastle.sasn1.Asn1Set r0 = (org.bouncycastle.sasn1.Asn1Set) r0
            r4._nextObject = r1
        L_0x0028:
            return r0
        L_0x0029:
            r0 = r1
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: org.bouncycastle.sasn1.cms.EnvelopedDataParser.getCertificates():org.bouncycastle.sasn1.Asn1Set");
    }

    public Asn1Set getCrls() throws IOException {
        if (this._nextObject == null) {
            this._nextObject = this._seq.readObject();
        }
        if (!(this._nextObject instanceof Asn1TaggedObject) || ((Asn1TaggedObject) this._nextObject).getTagNumber() != 1) {
            return null;
        }
        Asn1Set asn1Set = (Asn1Set) ((Asn1TaggedObject) this._nextObject).getObject(17, false);
        this._nextObject = null;
        return asn1Set;
    }

    public EncryptedContentInfoParser getEncryptedContentInfo() throws IOException {
        return new EncryptedContentInfoParser((Asn1Sequence) this._seq.readObject());
    }

    public Asn1Set getRecipientInfos() throws IOException {
        return (Asn1Set) this._seq.readObject();
    }

    public Asn1Set getUnprotectedAttrs() throws IOException {
        Asn1Object readObject = this._seq.readObject();
        if (readObject != null) {
            return (Asn1Set) ((Asn1TaggedObject) readObject).getObject(17, false);
        }
        return null;
    }

    public Asn1Integer getVersion() {
        return this._version;
    }
}
