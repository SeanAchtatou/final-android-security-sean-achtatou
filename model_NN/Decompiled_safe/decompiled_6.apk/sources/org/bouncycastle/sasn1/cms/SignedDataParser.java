package org.bouncycastle.sasn1.cms;

import java.io.IOException;
import org.bouncycastle.sasn1.Asn1Integer;
import org.bouncycastle.sasn1.Asn1Object;
import org.bouncycastle.sasn1.Asn1Sequence;
import org.bouncycastle.sasn1.Asn1Set;
import org.bouncycastle.sasn1.Asn1TaggedObject;

public class SignedDataParser {
    private boolean _certsCalled;
    private boolean _crlsCalled;
    private Asn1Object _nextObject;
    private Asn1Sequence _seq;
    private Asn1Integer _version;

    public SignedDataParser(Asn1Sequence asn1Sequence) throws IOException {
        this._seq = asn1Sequence;
        this._version = (Asn1Integer) asn1Sequence.readObject();
    }

    public Asn1Set getCertificates() throws IOException {
        this._certsCalled = true;
        this._nextObject = this._seq.readObject();
        if (!(this._nextObject instanceof Asn1TaggedObject) || ((Asn1TaggedObject) this._nextObject).getTagNumber() != 0) {
            return null;
        }
        Asn1Set asn1Set = (Asn1Set) ((Asn1TaggedObject) this._nextObject).getObject(17, false);
        this._nextObject = null;
        return asn1Set;
    }

    public Asn1Set getCrls() throws IOException {
        if (!this._certsCalled) {
            throw new IOException("getCerts() has not been called.");
        }
        this._crlsCalled = true;
        if (this._nextObject == null) {
            this._nextObject = this._seq.readObject();
        }
        if (!(this._nextObject instanceof Asn1TaggedObject) || ((Asn1TaggedObject) this._nextObject).getTagNumber() != 1) {
            return null;
        }
        Asn1Set asn1Set = (Asn1Set) ((Asn1TaggedObject) this._nextObject).getObject(17, false);
        this._nextObject = null;
        return asn1Set;
    }

    public Asn1Set getDigestAlgorithms() throws IOException {
        return (Asn1Set) this._seq.readObject();
    }

    public ContentInfoParser getEncapContentInfo() throws IOException {
        return new ContentInfoParser((Asn1Sequence) this._seq.readObject());
    }

    public Asn1Set getSignerInfos() throws IOException {
        if (!this._certsCalled || !this._crlsCalled) {
            throw new IOException("getCerts() and/or getCrls() has not been called.");
        }
        if (this._nextObject == null) {
            this._nextObject = this._seq.readObject();
        }
        return (Asn1Set) this._nextObject;
    }

    public Asn1Integer getVersion() {
        return this._version;
    }
}
