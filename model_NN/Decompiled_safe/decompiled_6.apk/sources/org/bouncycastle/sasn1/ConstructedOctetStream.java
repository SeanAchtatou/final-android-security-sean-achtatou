package org.bouncycastle.sasn1;

import java.io.IOException;
import java.io.InputStream;

class ConstructedOctetStream extends InputStream {
    private final Asn1InputStream _aIn;
    private InputStream _currentStream;
    private boolean _first = true;

    ConstructedOctetStream(InputStream inputStream) {
        this._aIn = new Asn1InputStream(inputStream);
    }

    public int read() throws IOException {
        if (this._first) {
            Asn1OctetString asn1OctetString = (Asn1OctetString) this._aIn.readObject();
            if (asn1OctetString == null) {
                return -1;
            }
            this._first = false;
            this._currentStream = asn1OctetString.getOctetStream();
        } else if (this._currentStream == null) {
            return -1;
        }
        int read = this._currentStream.read();
        if (read >= 0) {
            return read;
        }
        Asn1OctetString asn1OctetString2 = (Asn1OctetString) this._aIn.readObject();
        if (asn1OctetString2 == null) {
            this._currentStream = null;
            return -1;
        }
        this._currentStream = asn1OctetString2.getOctetStream();
        return this._currentStream.read();
    }
}
