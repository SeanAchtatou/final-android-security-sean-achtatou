package org.bouncycastle.sasn1;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class BerGenerator extends Asn1Generator {
    private boolean _isExplicit;
    private int _tagNo;
    private boolean _tagged;

    protected BerGenerator(OutputStream outputStream) {
        super(outputStream);
        this._tagged = false;
    }

    public BerGenerator(OutputStream outputStream, int i, boolean z) {
        super(outputStream);
        this._tagged = false;
        this._tagged = true;
        this._isExplicit = z;
        this._tagNo = i;
    }

    private void writeHdr(int i) throws IOException {
        this._out.write(i);
        this._out.write(128);
    }

    public OutputStream getRawOutputStream() {
        return this._out;
    }

    /* access modifiers changed from: protected */
    public void writeBerBody(InputStream inputStream) throws IOException {
        while (true) {
            int read = inputStream.read();
            if (read >= 0) {
                this._out.write(read);
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void writeBerEnd() throws IOException {
        this._out.write(0);
        this._out.write(0);
        if (this._tagged && this._isExplicit) {
            this._out.write(0);
            this._out.write(0);
        }
    }

    /* access modifiers changed from: protected */
    public void writeBerHeader(int i) throws IOException {
        int i2 = this._tagNo | 128;
        if (!this._tagged) {
            writeHdr(i);
        } else if (this._isExplicit) {
            writeHdr(i2 | 32);
            writeHdr(i);
        } else if ((i & 32) != 0) {
            writeHdr(i2 | 32);
        } else {
            writeHdr(i2);
        }
    }
}
