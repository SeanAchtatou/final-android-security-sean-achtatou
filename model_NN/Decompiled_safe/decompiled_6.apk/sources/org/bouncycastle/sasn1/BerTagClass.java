package org.bouncycastle.sasn1;

class BerTagClass {
    public static final int APPLICATION = 64;
    public static final int UNIVERSAL = 0;

    BerTagClass() {
    }
}
