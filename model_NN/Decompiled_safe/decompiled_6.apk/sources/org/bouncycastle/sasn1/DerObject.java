package org.bouncycastle.sasn1;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class DerObject extends Asn1Object {
    private byte[] _content;

    private class BasicDerGenerator extends DerGenerator {
        protected BasicDerGenerator(OutputStream outputStream) {
            super(outputStream);
        }

        public OutputStream getRawOutputStream() {
            return this._out;
        }
    }

    DerObject(int i, int i2, byte[] bArr) {
        super(i, i2, null);
        this._content = bArr;
    }

    /* access modifiers changed from: package-private */
    public void encode(OutputStream outputStream) throws IOException {
        new BasicDerGenerator(outputStream).writeDerEncoded(this._baseTag | this._tagNumber, this._content);
    }

    public byte[] getEncoded() throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        encode(byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public InputStream getRawContentStream() {
        return new ByteArrayInputStream(this._content);
    }

    public int getTagNumber() {
        return this._tagNumber;
    }
}
