package org.bouncycastle.sasn1;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigInteger;

public class Asn1ObjectIdentifier extends DerObject {
    private String _oid;

    private static class OIDTokenizer {
        private int index = 0;
        private String oid;

        public OIDTokenizer(String str) {
            this.oid = str;
        }

        public boolean hasMoreTokens() {
            return this.index != -1;
        }

        public String nextToken() {
            if (this.index == -1) {
                return null;
            }
            int indexOf = this.oid.indexOf(46, this.index);
            if (indexOf == -1) {
                String substring = this.oid.substring(this.index);
                this.index = -1;
                return substring;
            }
            String substring2 = this.oid.substring(this.index, indexOf);
            this.index = indexOf + 1;
            return substring2;
        }
    }

    Asn1ObjectIdentifier(int i, byte[] bArr) throws IOException {
        super(i, 6, bArr);
        StringBuffer stringBuffer = new StringBuffer();
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
        boolean z = true;
        long j = 0;
        BigInteger bigInteger = null;
        while (true) {
            int read = byteArrayInputStream.read();
            if (read < 0) {
                this._oid = stringBuffer.toString();
                return;
            } else if (j < 36028797018963968L) {
                j = (j * 128) + ((long) (read & 127));
                if ((read & 128) == 0) {
                    if (z) {
                        switch (((int) j) / 40) {
                            case 0:
                                stringBuffer.append('0');
                                break;
                            case 1:
                                stringBuffer.append('1');
                                j -= 40;
                                break;
                            default:
                                stringBuffer.append('2');
                                j -= 80;
                                break;
                        }
                        z = false;
                    }
                    stringBuffer.append('.');
                    stringBuffer.append(j);
                    j = 0;
                }
            } else {
                bigInteger = (bigInteger == null ? BigInteger.valueOf(j) : bigInteger).shiftLeft(7).or(BigInteger.valueOf((long) (read & 127)));
                if ((read & 128) == 0) {
                    stringBuffer.append('.');
                    stringBuffer.append(bigInteger);
                    bigInteger = null;
                    j = 0;
                }
            }
        }
    }

    public Asn1ObjectIdentifier(String str) throws IllegalArgumentException {
        super(0, 6, toByteArray(str));
        this._oid = str;
    }

    private static byte[] toByteArray(String str) throws IllegalArgumentException {
        OIDTokenizer oIDTokenizer = new OIDTokenizer(str);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            writeField(byteArrayOutputStream, (long) ((Integer.parseInt(oIDTokenizer.nextToken()) * 40) + Integer.parseInt(oIDTokenizer.nextToken())));
            while (oIDTokenizer.hasMoreTokens()) {
                String nextToken = oIDTokenizer.nextToken();
                if (nextToken.length() < 18) {
                    writeField(byteArrayOutputStream, Long.parseLong(nextToken));
                } else {
                    writeField(byteArrayOutputStream, new BigInteger(nextToken));
                }
            }
            return byteArrayOutputStream.toByteArray();
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("exception parsing field value: " + e.getMessage());
        } catch (IOException e2) {
            throw new IllegalArgumentException("exception converting to bytes: " + e2.getMessage());
        }
    }

    private static void writeField(OutputStream outputStream, long j) throws IOException {
        if (j >= 128) {
            if (j >= 16384) {
                if (j >= 2097152) {
                    if (j >= 268435456) {
                        if (j >= 34359738368L) {
                            if (j >= 4398046511104L) {
                                if (j >= 562949953421312L) {
                                    if (j >= 72057594037927936L) {
                                        outputStream.write(((int) (j >> 56)) | 128);
                                    }
                                    outputStream.write(((int) (j >> 49)) | 128);
                                }
                                outputStream.write(((int) (j >> 42)) | 128);
                            }
                            outputStream.write(((int) (j >> 35)) | 128);
                        }
                        outputStream.write(((int) (j >> 28)) | 128);
                    }
                    outputStream.write(((int) (j >> 21)) | 128);
                }
                outputStream.write(((int) (j >> 14)) | 128);
            }
            outputStream.write(((int) (j >> 7)) | 128);
        }
        outputStream.write(((int) j) & 127);
    }

    private static void writeField(OutputStream outputStream, BigInteger bigInteger) throws IOException {
        int bitLength = (bigInteger.bitLength() + 6) / 7;
        if (bitLength == 0) {
            outputStream.write(0);
            return;
        }
        byte[] bArr = new byte[bitLength];
        for (int i = bitLength - 1; i >= 0; i--) {
            bArr[i] = (byte) ((bigInteger.intValue() & 127) | 128);
            bigInteger = bigInteger.shiftRight(7);
        }
        int i2 = bitLength - 1;
        bArr[i2] = (byte) (bArr[i2] & Byte.MAX_VALUE);
        outputStream.write(bArr);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Asn1ObjectIdentifier)) {
            return false;
        }
        return this._oid.equals(((Asn1ObjectIdentifier) obj)._oid);
    }

    public int hashCode() {
        return this._oid.hashCode();
    }

    public String toString() {
        return this._oid;
    }
}
