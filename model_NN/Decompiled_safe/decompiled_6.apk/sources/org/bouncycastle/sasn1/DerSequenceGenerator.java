package org.bouncycastle.sasn1;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class DerSequenceGenerator extends DerGenerator {
    private final ByteArrayOutputStream _bOut = new ByteArrayOutputStream();

    public DerSequenceGenerator(OutputStream outputStream) throws IOException {
        super(outputStream);
    }

    public DerSequenceGenerator(OutputStream outputStream, int i, boolean z) throws IOException {
        super(outputStream, i, z);
    }

    public void addObject(DerObject derObject) throws IOException {
        this._bOut.write(derObject.getEncoded());
    }

    public void close() throws IOException {
        writeDerEncoded(48, this._bOut.toByteArray());
    }

    public OutputStream getRawOutputStream() {
        return this._bOut;
    }
}
