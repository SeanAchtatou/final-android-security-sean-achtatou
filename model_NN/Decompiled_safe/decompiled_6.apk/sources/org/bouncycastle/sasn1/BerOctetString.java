package org.bouncycastle.sasn1;

import java.io.InputStream;

public class BerOctetString extends Asn1Object implements Asn1OctetString {
    protected BerOctetString(int i, InputStream inputStream) {
        super(i, 4, inputStream);
    }

    public InputStream getOctetStream() {
        return isConstructed() ? new ConstructedOctetStream(getRawContentStream()) : getRawContentStream();
    }
}
