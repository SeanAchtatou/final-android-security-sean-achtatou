package org.bouncycastle.sasn1;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

abstract class LimitedInputStream extends InputStream {
    protected final InputStream _in;

    LimitedInputStream(InputStream inputStream) {
        this._in = inputStream;
    }

    /* access modifiers changed from: package-private */
    public InputStream getUnderlyingStream() {
        return this._in;
    }

    /* access modifiers changed from: protected */
    public void setParentEofDetect(boolean z) {
        if (this._in instanceof IndefiniteLengthInputStream) {
            ((IndefiniteLengthInputStream) this._in).setEofOn00(z);
        }
    }

    /* access modifiers changed from: package-private */
    public byte[] toByteArray() throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while (true) {
            int read = read();
            if (read < 0) {
                return byteArrayOutputStream.toByteArray();
            }
            byteArrayOutputStream.write(read);
        }
    }
}
