package org.bouncycastle.sasn1;

import java.io.IOException;
import java.io.InputStream;

public class BerSequence extends Asn1Object implements Asn1Sequence {
    private Asn1InputStream _aIn;

    protected BerSequence(int i, InputStream inputStream) {
        super(i, 16, inputStream);
        this._aIn = new Asn1InputStream(inputStream);
    }

    public Asn1Object readObject() throws IOException {
        return this._aIn.readObject();
    }
}
