package org.bouncycastle.sasn1;

import java.io.IOException;
import java.io.OutputStream;
import org.bouncycastle.asn1.DEROctetString;

public class BerOctetStringGenerator extends BerGenerator {

    private class BerOctetStream extends OutputStream {
        private byte[] _buf;

        private BerOctetStream() {
            this._buf = new byte[1];
        }

        public void close() throws IOException {
            BerOctetStringGenerator.this.writeBerEnd();
        }

        public void write(int i) throws IOException {
            this._buf[0] = (byte) i;
            BerOctetStringGenerator.this._out.write(new DEROctetString(this._buf).getEncoded());
        }

        public void write(byte[] bArr) throws IOException {
            BerOctetStringGenerator.this._out.write(new DEROctetString(bArr).getEncoded());
        }

        public void write(byte[] bArr, int i, int i2) throws IOException {
            byte[] bArr2 = new byte[i2];
            System.arraycopy(bArr, i, bArr2, 0, i2);
            BerOctetStringGenerator.this._out.write(new DEROctetString(bArr2).getEncoded());
        }
    }

    private class BufferedBerOctetStream extends OutputStream {
        private byte[] _buf;
        private int _off = 0;

        BufferedBerOctetStream(byte[] bArr) {
            this._buf = bArr;
        }

        public void close() throws IOException {
            if (this._off != 0) {
                byte[] bArr = new byte[this._off];
                System.arraycopy(this._buf, 0, bArr, 0, this._off);
                BerOctetStringGenerator.this._out.write(new DEROctetString(bArr).getEncoded());
            }
            BerOctetStringGenerator.this.writeBerEnd();
        }

        public void write(int i) throws IOException {
            byte[] bArr = this._buf;
            int i2 = this._off;
            this._off = i2 + 1;
            bArr[i2] = (byte) i;
            if (this._off == this._buf.length) {
                BerOctetStringGenerator.this._out.write(new DEROctetString(this._buf).getEncoded());
                this._off = 0;
            }
        }
    }

    public BerOctetStringGenerator(OutputStream outputStream) throws IOException {
        super(outputStream);
        writeBerHeader(36);
    }

    public BerOctetStringGenerator(OutputStream outputStream, int i, boolean z) throws IOException {
        super(outputStream, i, z);
        writeBerHeader(36);
    }

    public OutputStream getOctetOutputStream() {
        return new BerOctetStream();
    }

    public OutputStream getOctetOutputStream(byte[] bArr) {
        return new BufferedBerOctetStream(bArr);
    }
}
