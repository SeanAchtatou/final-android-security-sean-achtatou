package org.bouncycastle.sasn1;

import java.io.InputStream;

public class DerOctetString extends DerObject implements Asn1OctetString {
    protected DerOctetString(int i, byte[] bArr) {
        super(i, 4, bArr);
    }

    public InputStream getOctetStream() {
        return isConstructed() ? new ConstructedOctetStream(getRawContentStream()) : getRawContentStream();
    }
}
