package org.bouncycastle.sasn1;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public abstract class DerGenerator extends Asn1Generator {
    private boolean _isExplicit;
    private int _tagNo;
    private boolean _tagged;

    protected DerGenerator(OutputStream outputStream) {
        super(outputStream);
        this._tagged = false;
    }

    public DerGenerator(OutputStream outputStream, int i, boolean z) {
        super(outputStream);
        this._tagged = false;
        this._tagged = true;
        this._isExplicit = z;
        this._tagNo = i;
    }

    private void writeLength(OutputStream outputStream, int i) throws IOException {
        if (i > 127) {
            int i2 = 1;
            int i3 = i;
            while (true) {
                i3 >>>= 8;
                if (i3 == 0) {
                    break;
                }
                i2++;
            }
            outputStream.write((byte) (i2 | 128));
            for (int i4 = (i2 - 1) * 8; i4 >= 0; i4 -= 8) {
                outputStream.write((byte) (i >> i4));
            }
            return;
        }
        outputStream.write((byte) i);
    }

    /* access modifiers changed from: package-private */
    public void writeDerEncoded(int i, byte[] bArr) throws IOException {
        if (this._tagged) {
            int i2 = this._tagNo | 128;
            if (this._isExplicit) {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                writeDerEncoded(byteArrayOutputStream, i, bArr);
                writeDerEncoded(this._out, this._tagNo | 32 | 128, byteArrayOutputStream.toByteArray());
            } else if ((i & 32) != 0) {
                writeDerEncoded(this._out, i2 | 32, bArr);
            } else {
                writeDerEncoded(this._out, i2, bArr);
            }
        } else {
            writeDerEncoded(this._out, i, bArr);
        }
    }

    /* access modifiers changed from: package-private */
    public void writeDerEncoded(OutputStream outputStream, int i, InputStream inputStream) throws IOException {
        outputStream.write(i);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while (true) {
            int read = inputStream.read();
            if (read >= 0) {
                byteArrayOutputStream.write(read);
            } else {
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                writeLength(outputStream, byteArray.length);
                outputStream.write(byteArray);
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void writeDerEncoded(OutputStream outputStream, int i, byte[] bArr) throws IOException {
        outputStream.write(i);
        writeLength(outputStream, bArr.length);
        outputStream.write(bArr);
    }
}
