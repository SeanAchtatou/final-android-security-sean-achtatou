package org.bouncycastle.cms;

import java.io.IOException;
import java.security.AlgorithmParameters;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import javax.crypto.Cipher;
import javax.crypto.KeyAgreement;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERGeneralizedTime;
import org.bouncycastle.asn1.DERInteger;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.cms.IssuerAndSerialNumber;
import org.bouncycastle.asn1.cms.KEKIdentifier;
import org.bouncycastle.asn1.cms.KEKRecipientInfo;
import org.bouncycastle.asn1.cms.KeyAgreeRecipientIdentifier;
import org.bouncycastle.asn1.cms.KeyAgreeRecipientInfo;
import org.bouncycastle.asn1.cms.KeyTransRecipientInfo;
import org.bouncycastle.asn1.cms.OriginatorIdentifierOrKey;
import org.bouncycastle.asn1.cms.OriginatorPublicKey;
import org.bouncycastle.asn1.cms.OtherKeyAttribute;
import org.bouncycastle.asn1.cms.PasswordRecipientInfo;
import org.bouncycastle.asn1.cms.RecipientEncryptedKey;
import org.bouncycastle.asn1.cms.RecipientIdentifier;
import org.bouncycastle.asn1.cms.RecipientInfo;
import org.bouncycastle.asn1.kisa.KISAObjectIdentifiers;
import org.bouncycastle.asn1.nist.NISTObjectIdentifiers;
import org.bouncycastle.asn1.ntt.NTTObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PBKDF2Params;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.asn1.x509.TBSCertificateStructure;
import org.bouncycastle.asn1.x9.X9ObjectIdentifiers;
import org.bouncycastle.jce.PrincipalUtil;

public class CMSEnvelopedGenerator {
    public static final String AES128_CBC = NISTObjectIdentifiers.id_aes128_CBC.getId();
    public static final String AES128_WRAP = NISTObjectIdentifiers.id_aes128_wrap.getId();
    public static final String AES192_CBC = NISTObjectIdentifiers.id_aes192_CBC.getId();
    public static final String AES192_WRAP = NISTObjectIdentifiers.id_aes192_wrap.getId();
    public static final String AES256_CBC = NISTObjectIdentifiers.id_aes256_CBC.getId();
    public static final String AES256_WRAP = NISTObjectIdentifiers.id_aes256_wrap.getId();
    public static final String CAMELLIA128_CBC = NTTObjectIdentifiers.id_camellia128_cbc.getId();
    public static final String CAMELLIA128_WRAP = NTTObjectIdentifiers.id_camellia128_wrap.getId();
    public static final String CAMELLIA192_CBC = NTTObjectIdentifiers.id_camellia192_cbc.getId();
    public static final String CAMELLIA192_WRAP = NTTObjectIdentifiers.id_camellia192_wrap.getId();
    public static final String CAMELLIA256_CBC = NTTObjectIdentifiers.id_camellia256_cbc.getId();
    public static final String CAMELLIA256_WRAP = NTTObjectIdentifiers.id_camellia256_wrap.getId();
    public static final String CAST5_CBC = "1.2.840.113533.7.66.10";
    public static final String DES_EDE3_CBC = PKCSObjectIdentifiers.des_EDE3_CBC.getId();
    public static final String DES_EDE3_WRAP = PKCSObjectIdentifiers.id_alg_CMS3DESwrap.getId();
    public static final String ECDH_SHA1KDF = X9ObjectIdentifiers.dhSinglePass_stdDH_sha1kdf_scheme.getId();
    /* access modifiers changed from: private */
    public static final CMSEnvelopedHelper HELPER = CMSEnvelopedHelper.INSTANCE;
    public static final String IDEA_CBC = "1.3.6.1.4.1.188.7.1.1.2";
    public static final String RC2_CBC = PKCSObjectIdentifiers.RC2_CBC.getId();
    public static final String SEED_CBC = KISAObjectIdentifiers.id_seedCBC.getId();
    public static final String SEED_WRAP = KISAObjectIdentifiers.id_npki_app_cmsSeed_wrap.getId();
    final SecureRandom rand;
    final List recipientInfs;

    protected class RecipientInf {
        X509Certificate cert;
        AlgorithmIdentifier derivationAlg;
        AlgorithmIdentifier keyEncAlg;
        OriginatorIdentifierOrKey originator;
        PublicKey pubKey;
        SecretKey secKey;
        KEKIdentifier secKeyId;
        ASN1OctetString subKeyId;
        ASN1OctetString ukm;

        RecipientInf(PublicKey publicKey, ASN1OctetString aSN1OctetString) {
            this.pubKey = publicKey;
            this.subKeyId = aSN1OctetString;
            try {
                this.keyEncAlg = SubjectPublicKeyInfo.getInstance(ASN1Object.fromByteArray(publicKey.getEncoded())).getAlgorithmId();
            } catch (IOException e) {
                throw new IllegalArgumentException("can't extract key algorithm from this key");
            }
        }

        RecipientInf(X509Certificate x509Certificate) {
            this.cert = x509Certificate;
            this.pubKey = x509Certificate.getPublicKey();
            try {
                this.keyEncAlg = TBSCertificateStructure.getInstance(ASN1Object.fromByteArray(x509Certificate.getTBSCertificate())).getSubjectPublicKeyInfo().getAlgorithmId();
            } catch (IOException e) {
                throw new IllegalArgumentException("can't extract key algorithm from this cert");
            } catch (CertificateEncodingException e2) {
                throw new IllegalArgumentException("can't extract tbs structure from this cert");
            }
        }

        public RecipientInf(SecretKey secretKey, String str, String str2, OriginatorIdentifierOrKey originatorIdentifierOrKey, X509Certificate x509Certificate) {
            ASN1EncodableVector aSN1EncodableVector = new ASN1EncodableVector();
            aSN1EncodableVector.add(new DERObjectIdentifier(str2));
            aSN1EncodableVector.add(DERNull.INSTANCE);
            this.secKey = secretKey;
            this.keyEncAlg = new AlgorithmIdentifier(new DERObjectIdentifier(str), new DERSequence(aSN1EncodableVector));
            this.originator = originatorIdentifierOrKey;
            this.cert = x509Certificate;
        }

        RecipientInf(SecretKey secretKey, KEKIdentifier kEKIdentifier) {
            DERObjectIdentifier dERObjectIdentifier;
            DERObjectIdentifier dERObjectIdentifier2;
            this.secKey = secretKey;
            this.secKeyId = kEKIdentifier;
            if (secretKey.getAlgorithm().startsWith("DES")) {
                this.keyEncAlg = new AlgorithmIdentifier(new DERObjectIdentifier("1.2.840.113549.1.9.16.3.6"), new DERNull());
            } else if (secretKey.getAlgorithm().startsWith("RC2")) {
                this.keyEncAlg = new AlgorithmIdentifier(new DERObjectIdentifier("1.2.840.113549.1.9.16.3.7"), new DERInteger(58));
            } else if (secretKey.getAlgorithm().startsWith("AES")) {
                int length = secretKey.getEncoded().length * 8;
                if (length == 128) {
                    dERObjectIdentifier2 = NISTObjectIdentifiers.id_aes128_wrap;
                } else if (length == 192) {
                    dERObjectIdentifier2 = NISTObjectIdentifiers.id_aes192_wrap;
                } else if (length == 256) {
                    dERObjectIdentifier2 = NISTObjectIdentifiers.id_aes256_wrap;
                } else {
                    throw new IllegalArgumentException("illegal keysize in AES");
                }
                this.keyEncAlg = new AlgorithmIdentifier(dERObjectIdentifier2);
            } else if (secretKey.getAlgorithm().startsWith("SEED")) {
                this.keyEncAlg = new AlgorithmIdentifier(KISAObjectIdentifiers.id_npki_app_cmsSeed_wrap);
            } else if (secretKey.getAlgorithm().startsWith("Camellia")) {
                int length2 = secretKey.getEncoded().length * 8;
                if (length2 == 128) {
                    dERObjectIdentifier = NTTObjectIdentifiers.id_camellia128_wrap;
                } else if (length2 == 192) {
                    dERObjectIdentifier = NTTObjectIdentifiers.id_camellia192_wrap;
                } else if (length2 == 256) {
                    dERObjectIdentifier = NTTObjectIdentifiers.id_camellia256_wrap;
                } else {
                    throw new IllegalArgumentException("illegal keysize in Camellia");
                }
                this.keyEncAlg = new AlgorithmIdentifier(dERObjectIdentifier);
            } else {
                throw new IllegalArgumentException("unknown algorithm");
            }
        }

        public RecipientInf(SecretKey secretKey, AlgorithmIdentifier algorithmIdentifier) {
            this.secKey = secretKey;
            this.derivationAlg = algorithmIdentifier;
        }

        /* access modifiers changed from: package-private */
        public RecipientInfo toRecipientInfo(SecretKey secretKey, SecureRandom secureRandom, String str) throws IOException, GeneralSecurityException {
            DEROctetString dEROctetString;
            if (this.pubKey != null) {
                Cipher createAsymmetricCipher = CMSEnvelopedGenerator.HELPER.createAsymmetricCipher(this.keyEncAlg.getObjectId().getId(), str);
                try {
                    createAsymmetricCipher.init(3, this.pubKey, secureRandom);
                    dEROctetString = new DEROctetString(createAsymmetricCipher.wrap(secretKey));
                } catch (GeneralSecurityException e) {
                    createAsymmetricCipher.init(1, this.pubKey, secureRandom);
                    dEROctetString = new DEROctetString(createAsymmetricCipher.doFinal(secretKey.getEncoded()));
                } catch (IllegalStateException e2) {
                    createAsymmetricCipher.init(1, this.pubKey, secureRandom);
                    dEROctetString = new DEROctetString(createAsymmetricCipher.doFinal(secretKey.getEncoded()));
                } catch (UnsupportedOperationException e3) {
                    createAsymmetricCipher.init(1, secretKey, secureRandom);
                    dEROctetString = new DEROctetString(createAsymmetricCipher.doFinal(secretKey.getEncoded()));
                }
                if (this.cert == null) {
                    return new RecipientInfo(new KeyTransRecipientInfo(new RecipientIdentifier(this.subKeyId), this.keyEncAlg, dEROctetString));
                }
                TBSCertificateStructure instance = TBSCertificateStructure.getInstance(new ASN1InputStream(this.cert.getTBSCertificate()).readObject());
                return new RecipientInfo(new KeyTransRecipientInfo(new RecipientIdentifier(new IssuerAndSerialNumber(instance.getIssuer(), instance.getSerialNumber().getValue())), this.keyEncAlg, dEROctetString));
            } else if (this.originator != null) {
                Cipher createAsymmetricCipher2 = CMSEnvelopedGenerator.HELPER.createAsymmetricCipher(DERObjectIdentifier.getInstance(ASN1Sequence.getInstance(this.keyEncAlg.getParameters()).getObjectAt(0)).getId(), str);
                createAsymmetricCipher2.init(3, this.secKey, secureRandom);
                return new RecipientInfo(new KeyAgreeRecipientInfo(this.originator, this.ukm, this.keyEncAlg, new DERSequence(new RecipientEncryptedKey(new KeyAgreeRecipientIdentifier(new IssuerAndSerialNumber(PrincipalUtil.getIssuerX509Principal(this.cert), this.cert.getSerialNumber())), new DEROctetString(createAsymmetricCipher2.wrap(secretKey))))));
            } else if (this.derivationAlg != null) {
                Cipher createAsymmetricCipher3 = CMSEnvelopedGenerator.HELPER.createAsymmetricCipher(CMSEnvelopedGenerator.HELPER.getRFC3211WrapperName(this.secKey.getAlgorithm()), str);
                createAsymmetricCipher3.init(3, this.secKey, secureRandom);
                DEROctetString dEROctetString2 = new DEROctetString(createAsymmetricCipher3.wrap(secretKey));
                ASN1EncodableVector aSN1EncodableVector = new ASN1EncodableVector();
                aSN1EncodableVector.add(new DERObjectIdentifier(this.secKey.getAlgorithm()));
                aSN1EncodableVector.add(new DEROctetString(createAsymmetricCipher3.getIV()));
                this.keyEncAlg = new AlgorithmIdentifier(PKCSObjectIdentifiers.id_alg_PWRI_KEK, new DERSequence(aSN1EncodableVector));
                return new RecipientInfo(new PasswordRecipientInfo(this.derivationAlg, this.keyEncAlg, dEROctetString2));
            } else {
                Cipher createAsymmetricCipher4 = CMSEnvelopedGenerator.HELPER.createAsymmetricCipher(this.keyEncAlg.getObjectId().getId(), str);
                createAsymmetricCipher4.init(3, this.secKey, secureRandom);
                return new RecipientInfo(new KEKRecipientInfo(this.secKeyId, this.keyEncAlg, new DEROctetString(createAsymmetricCipher4.wrap(secretKey))));
            }
        }
    }

    public CMSEnvelopedGenerator() {
        this(new SecureRandom());
    }

    public CMSEnvelopedGenerator(SecureRandom secureRandom) {
        this.recipientInfs = new ArrayList();
        this.rand = secureRandom;
    }

    public void addKEKRecipient(SecretKey secretKey, byte[] bArr) {
        this.recipientInfs.add(new RecipientInf(secretKey, new KEKIdentifier(bArr, (DERGeneralizedTime) null, (OtherKeyAttribute) null)));
    }

    public void addKeyAgreementRecipient(String str, PrivateKey privateKey, PublicKey publicKey, X509Certificate x509Certificate, String str2, String str3) throws NoSuchProviderException, NoSuchAlgorithmException, InvalidKeyException {
        KeyAgreement instance = KeyAgreement.getInstance(str, str3);
        instance.init(privateKey, this.rand);
        instance.doPhase(x509Certificate.getPublicKey(), true);
        try {
            SubjectPublicKeyInfo instance2 = SubjectPublicKeyInfo.getInstance(ASN1Object.fromByteArray(publicKey.getEncoded()));
            this.recipientInfs.add(new RecipientInf(instance.generateSecret(str2), str, str2, new OriginatorIdentifierOrKey(new OriginatorPublicKey(new AlgorithmIdentifier(instance2.getAlgorithmId().getObjectId(), new DERNull()), instance2.getPublicKeyData().getBytes())), x509Certificate));
        } catch (IOException e) {
            throw new InvalidKeyException("cannot extract originator public key: " + e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.bouncycastle.cms.CMSEnvelopedGenerator.RecipientInf.<init>(org.bouncycastle.cms.CMSEnvelopedGenerator, java.security.PublicKey, org.bouncycastle.asn1.ASN1OctetString):void
     arg types: [org.bouncycastle.cms.CMSEnvelopedGenerator, java.security.PublicKey, org.bouncycastle.asn1.DEROctetString]
     candidates:
      org.bouncycastle.cms.CMSEnvelopedGenerator.RecipientInf.<init>(org.bouncycastle.cms.CMSEnvelopedGenerator, javax.crypto.SecretKey, org.bouncycastle.asn1.cms.KEKIdentifier):void
      org.bouncycastle.cms.CMSEnvelopedGenerator.RecipientInf.<init>(org.bouncycastle.cms.CMSEnvelopedGenerator, javax.crypto.SecretKey, org.bouncycastle.asn1.x509.AlgorithmIdentifier):void
      org.bouncycastle.cms.CMSEnvelopedGenerator.RecipientInf.<init>(org.bouncycastle.cms.CMSEnvelopedGenerator, java.security.PublicKey, org.bouncycastle.asn1.ASN1OctetString):void */
    public void addKeyTransRecipient(PublicKey publicKey, byte[] bArr) throws IllegalArgumentException {
        this.recipientInfs.add(new RecipientInf(publicKey, (ASN1OctetString) new DEROctetString(bArr)));
    }

    public void addKeyTransRecipient(X509Certificate x509Certificate) throws IllegalArgumentException {
        this.recipientInfs.add(new RecipientInf(x509Certificate));
    }

    public void addPasswordRecipient(CMSPBEKey cMSPBEKey, String str) {
        this.recipientInfs.add(new RecipientInf(new SecretKeySpec(cMSPBEKey.getEncoded(str), str), new AlgorithmIdentifier(PKCSObjectIdentifiers.id_PBKDF2, new PBKDF2Params(cMSPBEKey.getSalt(), cMSPBEKey.getIterationCount()))));
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.security.AlgorithmParameters generateParameters(java.lang.String r6, javax.crypto.SecretKey r7, java.lang.String r8) throws java.security.NoSuchProviderException, org.bouncycastle.cms.CMSException {
        /*
            r5 = this;
            java.security.AlgorithmParameterGenerator r0 = java.security.AlgorithmParameterGenerator.getInstance(r6, r8)     // Catch:{ NoSuchAlgorithmException -> 0x004e }
            java.lang.String r1 = org.bouncycastle.cms.CMSEnvelopedGenerator.RC2_CBC     // Catch:{ NoSuchAlgorithmException -> 0x004e }
            boolean r1 = r6.equals(r1)     // Catch:{ NoSuchAlgorithmException -> 0x004e }
            if (r1 == 0) goto L_0x002f
            r1 = 8
            byte[] r1 = new byte[r1]     // Catch:{ NoSuchAlgorithmException -> 0x004e }
            java.security.SecureRandom r2 = r5.rand     // Catch:{ NoSuchAlgorithmException -> 0x004e }
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ NoSuchAlgorithmException -> 0x004e }
            r2.setSeed(r3)     // Catch:{ NoSuchAlgorithmException -> 0x004e }
            java.security.SecureRandom r2 = r5.rand     // Catch:{ NoSuchAlgorithmException -> 0x004e }
            r2.nextBytes(r1)     // Catch:{ NoSuchAlgorithmException -> 0x004e }
            javax.crypto.spec.RC2ParameterSpec r2 = new javax.crypto.spec.RC2ParameterSpec     // Catch:{ InvalidAlgorithmParameterException -> 0x0034 }
            byte[] r3 = r7.getEncoded()     // Catch:{ InvalidAlgorithmParameterException -> 0x0034 }
            int r3 = r3.length     // Catch:{ InvalidAlgorithmParameterException -> 0x0034 }
            int r3 = r3 * 8
            r2.<init>(r3, r1)     // Catch:{ InvalidAlgorithmParameterException -> 0x0034 }
            java.security.SecureRandom r1 = r5.rand     // Catch:{ InvalidAlgorithmParameterException -> 0x0034 }
            r0.init(r2, r1)     // Catch:{ InvalidAlgorithmParameterException -> 0x0034 }
        L_0x002f:
            java.security.AlgorithmParameters r0 = r0.generateParameters()     // Catch:{ NoSuchAlgorithmException -> 0x004e }
        L_0x0033:
            return r0
        L_0x0034:
            r0 = move-exception
            org.bouncycastle.cms.CMSException r1 = new org.bouncycastle.cms.CMSException     // Catch:{ NoSuchAlgorithmException -> 0x004e }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ NoSuchAlgorithmException -> 0x004e }
            r2.<init>()     // Catch:{ NoSuchAlgorithmException -> 0x004e }
            java.lang.String r3 = "parameters generation error: "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ NoSuchAlgorithmException -> 0x004e }
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ NoSuchAlgorithmException -> 0x004e }
            java.lang.String r2 = r2.toString()     // Catch:{ NoSuchAlgorithmException -> 0x004e }
            r1.<init>(r2, r0)     // Catch:{ NoSuchAlgorithmException -> 0x004e }
            throw r1     // Catch:{ NoSuchAlgorithmException -> 0x004e }
        L_0x004e:
            r0 = move-exception
            r0 = 0
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: org.bouncycastle.cms.CMSEnvelopedGenerator.generateParameters(java.lang.String, javax.crypto.SecretKey, java.lang.String):java.security.AlgorithmParameters");
    }

    /* access modifiers changed from: protected */
    public AlgorithmIdentifier getAlgorithmIdentifier(String str, AlgorithmParameters algorithmParameters) throws IOException {
        return new AlgorithmIdentifier(new DERObjectIdentifier(str), algorithmParameters != null ? new ASN1InputStream(algorithmParameters.getEncoded("ASN.1")).readObject() : new DERNull());
    }
}
