package org.bouncycastle.cms;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertStore;
import java.security.cert.CertStoreException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Generator;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1OctetStringParser;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.ASN1SetParser;
import org.bouncycastle.asn1.ASN1StreamParser;
import org.bouncycastle.asn1.BEROctetStringGenerator;
import org.bouncycastle.asn1.BERSequenceGenerator;
import org.bouncycastle.asn1.BERSetParser;
import org.bouncycastle.asn1.BERTaggedObject;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.DERObject;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.cms.CMSObjectIdentifiers;
import org.bouncycastle.asn1.cms.ContentInfoParser;
import org.bouncycastle.asn1.cms.SignedDataParser;
import org.bouncycastle.asn1.cms.SignerInfo;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.util.io.Streams;
import org.bouncycastle.x509.NoSuchStoreException;
import org.bouncycastle.x509.X509Store;

public class CMSSignedDataParser extends CMSContentInfoParser {
    private static final CMSSignedHelper HELPER = CMSSignedHelper.INSTANCE;
    private X509Store _attributeStore;
    private ASN1Set _certSet;
    private CertStore _certStore;
    private X509Store _certificateStore;
    private ASN1Set _crlSet;
    private X509Store _crlStore;
    private Map _digests;
    private boolean _isCertCrlParsed;
    private CMSTypedStream _signedContent;
    private SignedDataParser _signedData;
    private SignerInformationStore _signerInfoStore;

    public CMSSignedDataParser(InputStream inputStream) throws CMSException {
        this((CMSTypedStream) null, inputStream);
    }

    public CMSSignedDataParser(CMSTypedStream cMSTypedStream, InputStream inputStream) throws CMSException {
        super(inputStream);
        try {
            this._signedContent = cMSTypedStream;
            this._signedData = SignedDataParser.getInstance(this._contentInfo.getContent(16));
            this._digests = new HashMap();
            ASN1SetParser digestAlgorithms = this._signedData.getDigestAlgorithms();
            while (true) {
                DEREncodable readObject = digestAlgorithms.readObject();
                if (readObject == null) {
                    break;
                }
                try {
                    String digestAlgName = HELPER.getDigestAlgName(AlgorithmIdentifier.getInstance(readObject.getDERObject()).getObjectId().toString());
                    this._digests.put(digestAlgName, HELPER.getDigestInstance(digestAlgName, null));
                } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
                }
            }
            ContentInfoParser encapContentInfo = this._signedData.getEncapContentInfo();
            ASN1OctetStringParser content = encapContentInfo.getContent(4);
            if (content != null) {
                CMSTypedStream cMSTypedStream2 = new CMSTypedStream(encapContentInfo.getContentType().getId(), content.getOctetStream());
                if (this._signedContent == null) {
                    this._signedContent = cMSTypedStream2;
                } else {
                    cMSTypedStream2.drain();
                }
            }
            if (this._digests.isEmpty()) {
                throw new CMSException("no digests could be created for message.");
            }
        } catch (IOException e2) {
            throw new CMSException("io exception: " + e2.getMessage(), e2);
        }
    }

    public CMSSignedDataParser(CMSTypedStream cMSTypedStream, byte[] bArr) throws CMSException {
        this(cMSTypedStream, new ByteArrayInputStream(bArr));
    }

    public CMSSignedDataParser(byte[] bArr) throws CMSException {
        this(new ByteArrayInputStream(bArr));
    }

    private static ASN1Set getASN1Set(ASN1SetParser aSN1SetParser) {
        if (aSN1SetParser == null) {
            return null;
        }
        return ASN1Set.getInstance(aSN1SetParser.getDERObject());
    }

    private static AlgorithmIdentifier makeAlgId(String str, byte[] bArr) throws IOException {
        return bArr != null ? new AlgorithmIdentifier(new DERObjectIdentifier(str), makeObj(bArr)) : new AlgorithmIdentifier(new DERObjectIdentifier(str), new DERNull());
    }

    private static DERObject makeObj(byte[] bArr) throws IOException {
        if (bArr == null) {
            return null;
        }
        return new ASN1InputStream(bArr).readObject();
    }

    private static void pipeOctetString(ASN1OctetStringParser aSN1OctetStringParser, OutputStream outputStream) throws IOException {
        OutputStream octetOutputStream = new BEROctetStringGenerator(outputStream, 0, true).getOctetOutputStream();
        Streams.pipeAll(aSN1OctetStringParser.getOctetStream(), octetOutputStream);
        octetOutputStream.close();
    }

    private void populateCertCrlSets() throws CMSException {
        if (!this._isCertCrlParsed) {
            this._isCertCrlParsed = true;
            try {
                this._certSet = getASN1Set(this._signedData.getCertificates());
                this._crlSet = getASN1Set(this._signedData.getCrls());
            } catch (IOException e) {
                throw new CMSException("problem parsing cert/crl sets", e);
            }
        }
    }

    public static OutputStream replaceCertificatesAndCRLs(InputStream inputStream, CertStore certStore, OutputStream outputStream) throws CMSException, IOException {
        SignedDataParser instance = SignedDataParser.getInstance(new ContentInfoParser(new ASN1StreamParser(inputStream, CMSUtils.getMaximumMemory()).readObject()).getContent(16));
        BERSequenceGenerator bERSequenceGenerator = new BERSequenceGenerator(outputStream);
        bERSequenceGenerator.addObject(CMSObjectIdentifiers.signedData);
        BERSequenceGenerator bERSequenceGenerator2 = new BERSequenceGenerator(bERSequenceGenerator.getRawOutputStream(), 0, true);
        bERSequenceGenerator2.addObject(instance.getVersion());
        bERSequenceGenerator2.getRawOutputStream().write(instance.getDigestAlgorithms().getDERObject().getEncoded());
        ContentInfoParser encapContentInfo = instance.getEncapContentInfo();
        BERSequenceGenerator bERSequenceGenerator3 = new BERSequenceGenerator(bERSequenceGenerator2.getRawOutputStream());
        bERSequenceGenerator3.addObject(encapContentInfo.getContentType());
        ASN1OctetStringParser content = encapContentInfo.getContent(4);
        if (content != null) {
            pipeOctetString(content, bERSequenceGenerator3.getRawOutputStream());
        }
        bERSequenceGenerator3.close();
        getASN1Set(instance.getCertificates());
        getASN1Set(instance.getCrls());
        try {
            ASN1Set createBerSetFromList = CMSUtils.createBerSetFromList(CMSUtils.getCertificatesFromStore(certStore));
            if (createBerSetFromList.size() > 0) {
                bERSequenceGenerator2.getRawOutputStream().write(new DERTaggedObject(false, 0, createBerSetFromList).getEncoded());
            }
            try {
                ASN1Set createBerSetFromList2 = CMSUtils.createBerSetFromList(CMSUtils.getCRLsFromStore(certStore));
                if (createBerSetFromList2.size() > 0) {
                    bERSequenceGenerator2.getRawOutputStream().write(new DERTaggedObject(false, 1, createBerSetFromList2).getEncoded());
                }
                bERSequenceGenerator2.getRawOutputStream().write(instance.getSignerInfos().getDERObject().getEncoded());
                bERSequenceGenerator2.close();
                bERSequenceGenerator.close();
                return outputStream;
            } catch (CertStoreException e) {
                throw new CMSException("error getting crls from certStore", e);
            }
        } catch (CertStoreException e2) {
            throw new CMSException("error getting certs from certStore", e2);
        }
    }

    public static OutputStream replaceSigners(InputStream inputStream, SignerInformationStore signerInformationStore, OutputStream outputStream) throws CMSException, IOException {
        SignedDataParser instance = SignedDataParser.getInstance(new ContentInfoParser(new ASN1StreamParser(inputStream, CMSUtils.getMaximumMemory()).readObject()).getContent(16));
        BERSequenceGenerator bERSequenceGenerator = new BERSequenceGenerator(outputStream);
        bERSequenceGenerator.addObject(CMSObjectIdentifiers.signedData);
        BERSequenceGenerator bERSequenceGenerator2 = new BERSequenceGenerator(bERSequenceGenerator.getRawOutputStream(), 0, true);
        bERSequenceGenerator2.addObject(instance.getVersion());
        instance.getDigestAlgorithms().getDERObject();
        ASN1EncodableVector aSN1EncodableVector = new ASN1EncodableVector();
        for (SignerInformation signerInformation : signerInformationStore.getSigners()) {
            aSN1EncodableVector.add(makeAlgId(signerInformation.getDigestAlgOID(), signerInformation.getDigestAlgParams()));
        }
        bERSequenceGenerator2.getRawOutputStream().write(new DERSet(aSN1EncodableVector).getEncoded());
        ContentInfoParser encapContentInfo = instance.getEncapContentInfo();
        BERSequenceGenerator bERSequenceGenerator3 = new BERSequenceGenerator(bERSequenceGenerator2.getRawOutputStream());
        bERSequenceGenerator3.addObject(encapContentInfo.getContentType());
        ASN1OctetStringParser content = encapContentInfo.getContent(4);
        if (content != null) {
            pipeOctetString(content, bERSequenceGenerator3.getRawOutputStream());
        }
        bERSequenceGenerator3.close();
        writeSetToGeneratorTagged(bERSequenceGenerator2, instance.getCertificates(), 0);
        writeSetToGeneratorTagged(bERSequenceGenerator2, instance.getCrls(), 1);
        ASN1EncodableVector aSN1EncodableVector2 = new ASN1EncodableVector();
        for (SignerInformation signerInfo : signerInformationStore.getSigners()) {
            aSN1EncodableVector2.add(signerInfo.toSignerInfo());
        }
        bERSequenceGenerator2.getRawOutputStream().write(new DERSet(aSN1EncodableVector2).getEncoded());
        bERSequenceGenerator2.close();
        bERSequenceGenerator.close();
        return outputStream;
    }

    private static void writeSetToGeneratorTagged(ASN1Generator aSN1Generator, ASN1SetParser aSN1SetParser, int i) throws IOException {
        ASN1Set aSN1Set = getASN1Set(aSN1SetParser);
        if (aSN1Set != null) {
            aSN1Generator.getRawOutputStream().write((aSN1SetParser instanceof BERSetParser ? new BERTaggedObject(false, i, aSN1Set) : new DERTaggedObject(false, i, aSN1Set)).getEncoded());
        }
    }

    public X509Store getAttributeCertificates(String str, String str2) throws NoSuchStoreException, NoSuchProviderException, CMSException {
        if (this._attributeStore == null) {
            populateCertCrlSets();
            this._attributeStore = HELPER.createAttributeStore(str, str2, this._certSet);
        }
        return this._attributeStore;
    }

    public X509Store getCRLs(String str, String str2) throws NoSuchStoreException, NoSuchProviderException, CMSException {
        if (this._crlStore == null) {
            populateCertCrlSets();
            this._crlStore = HELPER.createCRLsStore(str, str2, this._crlSet);
        }
        return this._crlStore;
    }

    public X509Store getCertificates(String str, String str2) throws NoSuchStoreException, NoSuchProviderException, CMSException {
        if (this._certificateStore == null) {
            populateCertCrlSets();
            this._certificateStore = HELPER.createCertificateStore(str, str2, this._certSet);
        }
        return this._certificateStore;
    }

    public CertStore getCertificatesAndCRLs(String str, String str2) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        if (this._certStore == null) {
            populateCertCrlSets();
            this._certStore = HELPER.createCertStore(str, str2, this._certSet, this._crlSet);
        }
        return this._certStore;
    }

    public CMSTypedStream getSignedContent() {
        if (this._signedContent == null) {
            return null;
        }
        InputStream contentStream = this._signedContent.getContentStream();
        DigestInputStream digestInputStream = contentStream;
        for (MessageDigest digestInputStream2 : this._digests.values()) {
            digestInputStream = new DigestInputStream(digestInputStream, digestInputStream2);
        }
        return new CMSTypedStream(this._signedContent.getContentType(), digestInputStream);
    }

    public SignerInformationStore getSignerInfos() throws CMSException {
        if (this._signerInfoStore == null) {
            populateCertCrlSets();
            ArrayList arrayList = new ArrayList();
            HashMap hashMap = new HashMap();
            for (Object next : this._digests.keySet()) {
                hashMap.put(next, ((MessageDigest) this._digests.get(next)).digest());
            }
            try {
                ASN1SetParser signerInfos = this._signedData.getSignerInfos();
                while (true) {
                    DEREncodable readObject = signerInfos.readObject();
                    if (readObject == null) {
                        break;
                    }
                    SignerInfo instance = SignerInfo.getInstance(readObject.getDERObject());
                    arrayList.add(new SignerInformation(instance, new DERObjectIdentifier(this._signedContent.getContentType()), null, new BaseDigestCalculator((byte[]) hashMap.get(HELPER.getDigestAlgName(instance.getDigestAlgorithm().getObjectId().getId())))));
                }
                this._signerInfoStore = new SignerInformationStore(arrayList);
            } catch (IOException e) {
                throw new CMSException("io exception: " + e.getMessage(), e);
            }
        }
        return this._signerInfoStore;
    }

    public int getVersion() {
        return this._signedData.getVersion().getValue().intValue();
    }
}
