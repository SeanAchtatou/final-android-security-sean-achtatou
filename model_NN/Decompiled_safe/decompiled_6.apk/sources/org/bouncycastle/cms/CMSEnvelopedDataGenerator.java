package org.bouncycastle.cms;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.AlgorithmParameters;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.BERConstructedOctetString;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.cms.ContentInfo;
import org.bouncycastle.asn1.cms.EncryptedContentInfo;
import org.bouncycastle.asn1.cms.EnvelopedData;
import org.bouncycastle.asn1.cms.OriginatorInfo;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.cms.CMSEnvelopedGenerator;

public class CMSEnvelopedDataGenerator extends CMSEnvelopedGenerator {
    public CMSEnvelopedDataGenerator() {
    }

    public CMSEnvelopedDataGenerator(SecureRandom secureRandom) {
        super(secureRandom);
    }

    private CMSEnvelopedData generate(CMSProcessable cMSProcessable, String str, KeyGenerator keyGenerator, String str2) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        String name = keyGenerator.getProvider().getName();
        ASN1EncodableVector aSN1EncodableVector = new ASN1EncodableVector();
        try {
            Cipher symmetricCipher = CMSEnvelopedHelper.INSTANCE.getSymmetricCipher(str, name);
            SecretKey generateKey = keyGenerator.generateKey();
            AlgorithmParameters generateParameters = generateParameters(str, generateKey, name);
            symmetricCipher.init(1, generateKey, generateParameters, this.rand);
            if (generateParameters == null) {
                generateParameters = symmetricCipher.getParameters();
            }
            AlgorithmIdentifier algorithmIdentifier = getAlgorithmIdentifier(str, generateParameters);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            CipherOutputStream cipherOutputStream = new CipherOutputStream(byteArrayOutputStream, symmetricCipher);
            cMSProcessable.write(cipherOutputStream);
            cipherOutputStream.close();
            BERConstructedOctetString bERConstructedOctetString = new BERConstructedOctetString(byteArrayOutputStream.toByteArray());
            for (CMSEnvelopedGenerator.RecipientInf recipientInfo : this.recipientInfs) {
                try {
                    aSN1EncodableVector.add(recipientInfo.toRecipientInfo(generateKey, this.rand, str2));
                } catch (IOException e) {
                    throw new CMSException("encoding error.", e);
                } catch (InvalidKeyException e2) {
                    throw new CMSException("key inappropriate for algorithm.", e2);
                } catch (GeneralSecurityException e3) {
                    throw new CMSException("error making encrypted content.", e3);
                }
            }
            return new CMSEnvelopedData(new ContentInfo(PKCSObjectIdentifiers.envelopedData, new EnvelopedData((OriginatorInfo) null, new DERSet(aSN1EncodableVector), new EncryptedContentInfo(PKCSObjectIdentifiers.data, algorithmIdentifier, bERConstructedOctetString), (ASN1Set) null)));
        } catch (InvalidKeyException e4) {
            throw new CMSException("key invalid in message.", e4);
        } catch (NoSuchPaddingException e5) {
            throw new CMSException("required padding not supported.", e5);
        } catch (InvalidAlgorithmParameterException e6) {
            throw new CMSException("algorithm parameters invalid.", e6);
        } catch (IOException e7) {
            throw new CMSException("exception decoding algorithm parameters.", e7);
        }
    }

    public CMSEnvelopedData generate(CMSProcessable cMSProcessable, String str, int i, String str2) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        KeyGenerator createSymmetricKeyGenerator = CMSEnvelopedHelper.INSTANCE.createSymmetricKeyGenerator(str, str2);
        createSymmetricKeyGenerator.init(i, this.rand);
        return generate(cMSProcessable, str, createSymmetricKeyGenerator, str2);
    }

    public CMSEnvelopedData generate(CMSProcessable cMSProcessable, String str, String str2) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        KeyGenerator createSymmetricKeyGenerator = CMSEnvelopedHelper.INSTANCE.createSymmetricKeyGenerator(str, str2);
        createSymmetricKeyGenerator.init(this.rand);
        return generate(cMSProcessable, str, createSymmetricKeyGenerator, str2);
    }
}
