package org.bouncycastle.cms;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

public class CMSProcessableFile implements CMSProcessable {
    private static final int DEFAULT_BUF_SIZE = 32768;
    private final byte[] _buf;
    private final File _file;

    public CMSProcessableFile(File file) {
        this(file, 32768);
    }

    public CMSProcessableFile(File file, int i) {
        this._file = file;
        this._buf = new byte[i];
    }

    public Object getContent() {
        return this._file;
    }

    public void write(OutputStream outputStream) throws IOException, CMSException {
        FileInputStream fileInputStream = new FileInputStream(this._file);
        while (true) {
            int read = fileInputStream.read(this._buf, 0, this._buf.length);
            if (read > 0) {
                outputStream.write(this._buf, 0, read);
            } else {
                fileInputStream.close();
                return;
            }
        }
    }
}
