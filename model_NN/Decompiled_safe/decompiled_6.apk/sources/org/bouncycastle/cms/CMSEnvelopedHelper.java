package org.bouncycastle.cms;

import java.io.IOException;
import java.security.AlgorithmParameters;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.HashMap;
import java.util.Map;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;

class CMSEnvelopedHelper {
    private static final Map BASE_CIPHER_NAMES = new HashMap();
    private static final Map CIPHER_ALG_NAMES = new HashMap();
    static final CMSEnvelopedHelper INSTANCE = new CMSEnvelopedHelper();
    private static final Map KEYSIZES = new HashMap();

    static {
        KEYSIZES.put(CMSEnvelopedGenerator.DES_EDE3_CBC, new Integer(192));
        KEYSIZES.put(CMSEnvelopedGenerator.AES128_CBC, new Integer(128));
        KEYSIZES.put(CMSEnvelopedGenerator.AES192_CBC, new Integer(192));
        KEYSIZES.put(CMSEnvelopedGenerator.AES256_CBC, new Integer(256));
        BASE_CIPHER_NAMES.put(CMSEnvelopedGenerator.DES_EDE3_CBC, "DESEDE");
        BASE_CIPHER_NAMES.put(CMSEnvelopedGenerator.AES128_CBC, "AES");
        BASE_CIPHER_NAMES.put(CMSEnvelopedGenerator.AES192_CBC, "AES");
        BASE_CIPHER_NAMES.put(CMSEnvelopedGenerator.AES256_CBC, "AES");
        CIPHER_ALG_NAMES.put(CMSEnvelopedGenerator.DES_EDE3_CBC, "DESEDE/CBC/PKCS5Padding");
        CIPHER_ALG_NAMES.put(CMSEnvelopedGenerator.AES128_CBC, "AES/CBC/PKCS5Padding");
        CIPHER_ALG_NAMES.put(CMSEnvelopedGenerator.AES192_CBC, "AES/CBC/PKCS5Padding");
        CIPHER_ALG_NAMES.put(CMSEnvelopedGenerator.AES256_CBC, "AES/CBC/PKCS5Padding");
    }

    CMSEnvelopedHelper() {
    }

    private AlgorithmParameters createAlgorithmParams(String str, String str2) throws NoSuchProviderException, NoSuchAlgorithmException {
        return str2 != null ? AlgorithmParameters.getInstance(str, str2) : AlgorithmParameters.getInstance(str);
    }

    private Cipher createCipher(String str, String str2) throws NoSuchProviderException, NoSuchAlgorithmException, NoSuchPaddingException {
        return str2 != null ? Cipher.getInstance(str, str2) : Cipher.getInstance(str);
    }

    private KeyGenerator createKeyGenerator(String str, String str2) throws NoSuchProviderException, NoSuchAlgorithmException {
        return str2 != null ? KeyGenerator.getInstance(str, str2) : KeyGenerator.getInstance(str);
    }

    private String getAsymmetricEncryptionAlgName(String str) {
        return PKCSObjectIdentifiers.rsaEncryption.getId().equals(str) ? "RSA/ECB/PKCS1Padding" : str;
    }

    /* access modifiers changed from: package-private */
    public AlgorithmParameters createAlgorithmParameters(String str, String str2) throws NoSuchProviderException, NoSuchAlgorithmException {
        try {
            return createAlgorithmParams(str, str2);
        } catch (NoSuchAlgorithmException e) {
            try {
                String str3 = (String) BASE_CIPHER_NAMES.get(str);
                if (str3 != null) {
                    return createAlgorithmParams(str3, str2);
                }
            } catch (NoSuchAlgorithmException e2) {
            }
            throw e;
        }
    }

    /* access modifiers changed from: package-private */
    public Cipher createAsymmetricCipher(String str, String str2) throws NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException {
        try {
            return createCipher(str, str2);
        } catch (NoSuchAlgorithmException e) {
            return createCipher(getAsymmetricEncryptionAlgName(str), str2);
        }
    }

    /* access modifiers changed from: package-private */
    public KeyGenerator createSymmetricKeyGenerator(String str, String str2) throws NoSuchProviderException, NoSuchAlgorithmException {
        try {
            return createKeyGenerator(str, str2);
        } catch (NoSuchAlgorithmException e) {
            try {
                String str3 = (String) BASE_CIPHER_NAMES.get(str);
                if (str3 != null) {
                    return createKeyGenerator(str3, str2);
                }
            } catch (NoSuchAlgorithmException e2) {
            }
            if (str2 != null) {
                return createSymmetricKeyGenerator(str, null);
            }
            throw e;
        }
    }

    /* access modifiers changed from: package-private */
    public AlgorithmParameters getEncryptionAlgorithmParameters(String str, byte[] bArr, String str2) throws CMSException, NoSuchProviderException {
        if (bArr == null) {
            return null;
        }
        try {
            AlgorithmParameters createAlgorithmParameters = createAlgorithmParameters(str, str2);
            createAlgorithmParameters.init(bArr, "ASN.1");
            return createAlgorithmParameters;
        } catch (NoSuchAlgorithmException e) {
            throw new CMSException("can't find parameters for algorithm", e);
        } catch (IOException e2) {
            throw new CMSException("can't find parse parameters", e2);
        }
    }

    /* access modifiers changed from: package-private */
    public int getKeySize(String str) {
        Integer num = (Integer) KEYSIZES.get(str);
        if (num != null) {
            return num.intValue();
        }
        throw new IllegalArgumentException("no keysize for " + str);
    }

    /* access modifiers changed from: package-private */
    public String getRFC3211WrapperName(String str) {
        String str2 = (String) BASE_CIPHER_NAMES.get(str);
        if (str2 != null) {
            return str2 + "RFC3211Wrap";
        }
        throw new IllegalArgumentException("no name for " + str);
    }

    /* access modifiers changed from: package-private */
    public Cipher getSymmetricCipher(String str, String str2) throws NoSuchProviderException, NoSuchAlgorithmException, NoSuchPaddingException {
        try {
            return createCipher(str, str2);
        } catch (NoSuchAlgorithmException e) {
            try {
                return createCipher((String) CIPHER_ALG_NAMES.get(str), str2);
            } catch (NoSuchAlgorithmException e2) {
                if (str2 != null) {
                    return getSymmetricCipher(str, null);
                }
                throw e;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public String getSymmetricCipherName(String str) {
        String str2 = (String) BASE_CIPHER_NAMES.get(str);
        return str2 != null ? str2 : str;
    }
}
