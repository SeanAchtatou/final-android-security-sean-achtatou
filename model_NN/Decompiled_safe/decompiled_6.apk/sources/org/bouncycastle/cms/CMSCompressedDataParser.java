package org.bouncycastle.cms;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.InflaterInputStream;
import org.bouncycastle.asn1.cms.CompressedDataParser;
import org.bouncycastle.asn1.cms.ContentInfoParser;

public class CMSCompressedDataParser extends CMSContentInfoParser {
    public CMSCompressedDataParser(InputStream inputStream) throws CMSException {
        super(inputStream);
    }

    public CMSCompressedDataParser(byte[] bArr) throws CMSException {
        this(new ByteArrayInputStream(bArr));
    }

    public CMSTypedStream getContent() throws CMSException {
        try {
            ContentInfoParser encapContentInfo = new CompressedDataParser(this._contentInfo.getContent(16)).getEncapContentInfo();
            return new CMSTypedStream(encapContentInfo.getContentType().toString(), new InflaterInputStream(encapContentInfo.getContent(4).getOctetStream()));
        } catch (IOException e) {
            throw new CMSException("IOException reading compressed content.", e);
        }
    }
}
