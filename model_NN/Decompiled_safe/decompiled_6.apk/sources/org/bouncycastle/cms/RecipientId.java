package org.bouncycastle.cms;

import java.math.BigInteger;
import java.security.cert.X509CertSelector;
import org.bouncycastle.util.Arrays;

public class RecipientId extends X509CertSelector {
    byte[] keyIdentifier = null;

    private boolean equalsObj(Object obj, Object obj2) {
        return obj != null ? obj.equals(obj2) : obj2 == null;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof RecipientId)) {
            return false;
        }
        RecipientId recipientId = (RecipientId) obj;
        return Arrays.areEqual(this.keyIdentifier, recipientId.keyIdentifier) && Arrays.areEqual(getSubjectKeyIdentifier(), recipientId.getSubjectKeyIdentifier()) && equalsObj(getSerialNumber(), recipientId.getSerialNumber()) && equalsObj(getIssuerAsString(), recipientId.getIssuerAsString());
    }

    public byte[] getKeyIdentifier() {
        return this.keyIdentifier;
    }

    public int hashCode() {
        int hashCode = Arrays.hashCode(this.keyIdentifier) ^ Arrays.hashCode(getSubjectKeyIdentifier());
        BigInteger serialNumber = getSerialNumber();
        if (serialNumber != null) {
            hashCode ^= serialNumber.hashCode();
        }
        String issuerAsString = getIssuerAsString();
        return issuerAsString != null ? hashCode ^ issuerAsString.hashCode() : hashCode;
    }

    public void setKeyIdentifier(byte[] bArr) {
        this.keyIdentifier = bArr;
    }
}
