package org.bouncycastle.cms;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class SignerInformationStore {
    private Map table = new HashMap();

    public SignerInformationStore(Collection collection) {
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            SignerInformation signerInformation = (SignerInformation) it.next();
            SignerId sid = signerInformation.getSID();
            if (this.table.get(sid) == null) {
                this.table.put(sid, signerInformation);
            } else {
                Object obj = this.table.get(sid);
                if (obj instanceof List) {
                    ((List) obj).add(signerInformation);
                } else {
                    ArrayList arrayList = new ArrayList();
                    arrayList.add(obj);
                    arrayList.add(signerInformation);
                    this.table.put(sid, arrayList);
                }
            }
        }
    }

    public SignerInformation get(SignerId signerId) {
        Object obj = this.table.get(signerId);
        return obj instanceof List ? (SignerInformation) ((List) obj).get(0) : (SignerInformation) obj;
    }

    public Collection getSigners() {
        ArrayList arrayList = new ArrayList(this.table.size());
        for (Object next : this.table.values()) {
            if (next instanceof List) {
                arrayList.addAll((List) next);
            } else {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    public Collection getSigners(SignerId signerId) {
        Object obj = this.table.get(signerId);
        return obj instanceof List ? new ArrayList((List) obj) : obj != null ? Collections.singletonList(obj) : new ArrayList();
    }

    public int size() {
        Iterator it = this.table.values().iterator();
        int i = 0;
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            Object next = it.next();
            i = next instanceof List ? ((List) next).size() + i2 : i2 + 1;
        }
    }
}
