package org.bouncycastle.cms;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.AlgorithmParameters;
import java.security.NoSuchProviderException;
import java.util.ArrayList;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.asn1.cms.ContentInfo;
import org.bouncycastle.asn1.cms.EncryptedContentInfo;
import org.bouncycastle.asn1.cms.EnvelopedData;
import org.bouncycastle.asn1.cms.KEKRecipientInfo;
import org.bouncycastle.asn1.cms.KeyAgreeRecipientInfo;
import org.bouncycastle.asn1.cms.KeyTransRecipientInfo;
import org.bouncycastle.asn1.cms.PasswordRecipientInfo;
import org.bouncycastle.asn1.cms.RecipientInfo;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;

public class CMSEnvelopedData {
    ContentInfo contentInfo;
    private AlgorithmIdentifier encAlg;
    RecipientInformationStore recipientInfoStore;
    private ASN1Set unprotectedAttributes;

    public CMSEnvelopedData(InputStream inputStream) throws CMSException {
        this(CMSUtils.readContentInfo(inputStream));
    }

    public CMSEnvelopedData(ContentInfo contentInfo2) throws CMSException {
        this.contentInfo = contentInfo2;
        EnvelopedData instance = EnvelopedData.getInstance(contentInfo2.getContent());
        EncryptedContentInfo encryptedContentInfo = instance.getEncryptedContentInfo();
        this.encAlg = encryptedContentInfo.getContentEncryptionAlgorithm();
        ASN1Set recipientInfos = instance.getRecipientInfos();
        ArrayList arrayList = new ArrayList();
        byte[] octets = encryptedContentInfo.getEncryptedContent().getOctets();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 != recipientInfos.size()) {
                RecipientInfo instance2 = RecipientInfo.getInstance(recipientInfos.getObjectAt(i2));
                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(octets);
                KeyTransRecipientInfo info = instance2.getInfo();
                if (info instanceof KeyTransRecipientInfo) {
                    arrayList.add(new KeyTransRecipientInformation(info, this.encAlg, byteArrayInputStream));
                } else if (info instanceof KEKRecipientInfo) {
                    arrayList.add(new KEKRecipientInformation((KEKRecipientInfo) info, this.encAlg, byteArrayInputStream));
                } else if (info instanceof KeyAgreeRecipientInfo) {
                    arrayList.add(new KeyAgreeRecipientInformation((KeyAgreeRecipientInfo) info, this.encAlg, byteArrayInputStream));
                } else if (info instanceof PasswordRecipientInfo) {
                    arrayList.add(new PasswordRecipientInformation((PasswordRecipientInfo) info, this.encAlg, byteArrayInputStream));
                }
                i = i2 + 1;
            } else {
                this.recipientInfoStore = new RecipientInformationStore(arrayList);
                this.unprotectedAttributes = instance.getUnprotectedAttrs();
                return;
            }
        }
    }

    public CMSEnvelopedData(byte[] bArr) throws CMSException {
        this(CMSUtils.readContentInfo(bArr));
    }

    private byte[] encodeObj(DEREncodable dEREncodable) throws IOException {
        if (dEREncodable != null) {
            return dEREncodable.getDERObject().getEncoded();
        }
        return null;
    }

    public ContentInfo getContentInfo() {
        return this.contentInfo;
    }

    public byte[] getEncoded() throws IOException {
        return this.contentInfo.getEncoded();
    }

    public String getEncryptionAlgOID() {
        return this.encAlg.getObjectId().getId();
    }

    public byte[] getEncryptionAlgParams() {
        try {
            return encodeObj(this.encAlg.getParameters());
        } catch (Exception e) {
            throw new RuntimeException("exception getting encryption parameters " + e);
        }
    }

    public AlgorithmParameters getEncryptionAlgorithmParameters(String str) throws CMSException, NoSuchProviderException {
        return CMSEnvelopedHelper.INSTANCE.getEncryptionAlgorithmParameters(getEncryptionAlgOID(), getEncryptionAlgParams(), str);
    }

    public RecipientInformationStore getRecipientInfos() {
        return this.recipientInfoStore;
    }

    public AttributeTable getUnprotectedAttributes() {
        if (this.unprotectedAttributes == null) {
            return null;
        }
        return new AttributeTable(this.unprotectedAttributes);
    }
}
