package org.bouncycastle.cms;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;

public class CMSTypedStream {
    private static final int BUF_SIZ = 32768;
    private final int _bufSize;
    private final InputStream _in;
    private final String _oid;

    private class FullReaderStream extends InputStream {
        InputStream _stream;

        FullReaderStream(InputStream inputStream, int i) {
            this._stream = new BufferedInputStream(inputStream, i);
        }

        public void close() throws IOException {
            this._stream.close();
        }

        public int read() throws IOException {
            return this._stream.read();
        }

        public int read(byte[] bArr, int i, int i2) throws IOException {
            int i3 = 0;
            while (i2 != 0) {
                int read = this._stream.read(bArr, i, i2);
                if (read <= 0) {
                    break;
                }
                i += read;
                i2 -= read;
                i3 += read;
            }
            if (i3 > 0) {
                return i3;
            }
            return -1;
        }
    }

    public CMSTypedStream(InputStream inputStream) {
        this(PKCSObjectIdentifiers.data.getId(), inputStream, 32768);
    }

    public CMSTypedStream(String str, InputStream inputStream) {
        this(str, inputStream, 32768);
    }

    public CMSTypedStream(String str, InputStream inputStream, int i) {
        this._oid = str;
        this._bufSize = i;
        this._in = new FullReaderStream(inputStream, i);
    }

    public void drain() throws IOException {
        byte[] bArr = new byte[this._bufSize];
        do {
        } while (this._in.read(bArr, 0, bArr.length) > 0);
        this._in.close();
    }

    public InputStream getContentStream() {
        return this._in;
    }

    public String getContentType() {
        return this._oid;
    }
}
