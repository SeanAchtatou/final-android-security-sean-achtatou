package org.bouncycastle.cms;

import cn.com.jit.ida.util.pki.cipher.Mechanism;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Signature;
import java.security.cert.CRLException;
import java.security.cert.CertStore;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.CollectionCertStoreParameters;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.DERObject;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.cryptopro.CryptoProObjectIdentifiers;
import org.bouncycastle.asn1.nist.NISTObjectIdentifiers;
import org.bouncycastle.asn1.oiw.OIWObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.teletrust.TeleTrusTObjectIdentifiers;
import org.bouncycastle.asn1.x509.X509ObjectIdentifiers;
import org.bouncycastle.asn1.x9.X9ObjectIdentifiers;
import org.bouncycastle.x509.NoSuchStoreException;
import org.bouncycastle.x509.X509CollectionStoreParameters;
import org.bouncycastle.x509.X509Store;
import org.bouncycastle.x509.X509V2AttributeCertificate;

class CMSSignedHelper {
    static final CMSSignedHelper INSTANCE = new CMSSignedHelper();
    private static final Map digestAlgs = new HashMap();
    private static final Map digestAliases = new HashMap();
    private static final Map encryptionAlgs = new HashMap();

    static {
        addEntries(NISTObjectIdentifiers.dsa_with_sha224, Mechanism.SHA224, Mechanism.DSA);
        addEntries(NISTObjectIdentifiers.dsa_with_sha256, Mechanism.SHA256, Mechanism.DSA);
        addEntries(NISTObjectIdentifiers.dsa_with_sha384, Mechanism.SHA384, Mechanism.DSA);
        addEntries(NISTObjectIdentifiers.dsa_with_sha512, Mechanism.SHA512, Mechanism.DSA);
        addEntries(OIWObjectIdentifiers.dsaWithSHA1, Mechanism.SHA1, Mechanism.DSA);
        addEntries(OIWObjectIdentifiers.md4WithRSA, "MD4", Mechanism.RSA);
        addEntries(OIWObjectIdentifiers.md4WithRSAEncryption, "MD4", Mechanism.RSA);
        addEntries(OIWObjectIdentifiers.md5WithRSA, Mechanism.MD5, Mechanism.RSA);
        addEntries(OIWObjectIdentifiers.sha1WithRSA, Mechanism.SHA1, Mechanism.RSA);
        addEntries(PKCSObjectIdentifiers.md2WithRSAEncryption, Mechanism.MD2, Mechanism.RSA);
        addEntries(PKCSObjectIdentifiers.md4WithRSAEncryption, "MD4", Mechanism.RSA);
        addEntries(PKCSObjectIdentifiers.md5WithRSAEncryption, Mechanism.MD5, Mechanism.RSA);
        addEntries(PKCSObjectIdentifiers.sha1WithRSAEncryption, Mechanism.SHA1, Mechanism.RSA);
        addEntries(PKCSObjectIdentifiers.sha224WithRSAEncryption, Mechanism.SHA224, Mechanism.RSA);
        addEntries(PKCSObjectIdentifiers.sha256WithRSAEncryption, Mechanism.SHA256, Mechanism.RSA);
        addEntries(PKCSObjectIdentifiers.sha384WithRSAEncryption, Mechanism.SHA384, Mechanism.RSA);
        addEntries(PKCSObjectIdentifiers.sha512WithRSAEncryption, Mechanism.SHA512, Mechanism.RSA);
        addEntries(X9ObjectIdentifiers.ecdsa_with_SHA1, Mechanism.SHA1, Mechanism.ECDSA);
        addEntries(X9ObjectIdentifiers.ecdsa_with_SHA224, Mechanism.SHA224, Mechanism.ECDSA);
        addEntries(X9ObjectIdentifiers.ecdsa_with_SHA256, Mechanism.SHA256, Mechanism.ECDSA);
        addEntries(X9ObjectIdentifiers.ecdsa_with_SHA384, Mechanism.SHA384, Mechanism.ECDSA);
        addEntries(X9ObjectIdentifiers.ecdsa_with_SHA512, Mechanism.SHA512, Mechanism.ECDSA);
        addEntries(X9ObjectIdentifiers.id_dsa_with_sha1, Mechanism.SHA1, Mechanism.DSA);
        encryptionAlgs.put(X9ObjectIdentifiers.id_dsa.getId(), Mechanism.DSA);
        encryptionAlgs.put(PKCSObjectIdentifiers.rsaEncryption.getId(), Mechanism.RSA);
        encryptionAlgs.put("1.3.36.3.3.1", Mechanism.RSA);
        encryptionAlgs.put(X509ObjectIdentifiers.id_ea_rsa.getId(), Mechanism.RSA);
        encryptionAlgs.put(CMSSignedDataGenerator.ENCRYPTION_RSA_PSS, "RSAandMGF1");
        encryptionAlgs.put(CryptoProObjectIdentifiers.gostR3410_94.getId(), "GOST3410");
        encryptionAlgs.put(CryptoProObjectIdentifiers.gostR3410_2001.getId(), "ECGOST3410");
        encryptionAlgs.put("1.3.6.1.4.1.5849.1.6.2", "ECGOST3410");
        encryptionAlgs.put("1.3.6.1.4.1.5849.1.1.5", "GOST3410");
        digestAlgs.put(PKCSObjectIdentifiers.md2.getId(), Mechanism.MD2);
        digestAlgs.put(PKCSObjectIdentifiers.md4.getId(), "MD4");
        digestAlgs.put(PKCSObjectIdentifiers.md5.getId(), Mechanism.MD5);
        digestAlgs.put(OIWObjectIdentifiers.idSHA1.getId(), Mechanism.SHA1);
        digestAlgs.put(NISTObjectIdentifiers.id_sha224.getId(), Mechanism.SHA224);
        digestAlgs.put(NISTObjectIdentifiers.id_sha256.getId(), Mechanism.SHA256);
        digestAlgs.put(NISTObjectIdentifiers.id_sha384.getId(), Mechanism.SHA384);
        digestAlgs.put(NISTObjectIdentifiers.id_sha512.getId(), Mechanism.SHA512);
        digestAlgs.put(TeleTrusTObjectIdentifiers.ripemd128.getId(), "RIPEMD128");
        digestAlgs.put(TeleTrusTObjectIdentifiers.ripemd160.getId(), "RIPEMD160");
        digestAlgs.put(TeleTrusTObjectIdentifiers.ripemd256.getId(), "RIPEMD256");
        digestAlgs.put(CryptoProObjectIdentifiers.gostR3411.getId(), "GOST3411");
        digestAlgs.put("1.3.6.1.4.1.5849.1.2.1", "GOST3411");
        digestAliases.put(Mechanism.SHA1, new String[]{"SHA-1"});
        digestAliases.put(Mechanism.SHA224, new String[]{"SHA-224"});
        digestAliases.put(Mechanism.SHA256, new String[]{"SHA-256"});
        digestAliases.put(Mechanism.SHA384, new String[]{"SHA-384"});
        digestAliases.put(Mechanism.SHA512, new String[]{"SHA-512"});
    }

    CMSSignedHelper() {
    }

    private void addCRLsFromSet(List list, ASN1Set aSN1Set, String str) throws NoSuchProviderException, CMSException {
        CertificateFactory instance;
        if (str != null) {
            try {
                instance = CertificateFactory.getInstance("X.509", str);
            } catch (CertificateException e) {
                throw new CMSException("can't get certificate factory.", e);
            }
        } else {
            instance = CertificateFactory.getInstance("X.509");
        }
        Enumeration objects = aSN1Set.getObjects();
        while (objects.hasMoreElements()) {
            try {
                list.add(instance.generateCRL(new ByteArrayInputStream(((DEREncodable) objects.nextElement()).getDERObject().getEncoded())));
            } catch (IOException e2) {
                throw new CMSException("can't re-encode CRL!", e2);
            } catch (CRLException e3) {
                throw new CMSException("can't re-encode CRL!", e3);
            }
        }
    }

    private void addCertsFromSet(List list, ASN1Set aSN1Set, String str) throws NoSuchProviderException, CMSException {
        CertificateFactory instance;
        if (str != null) {
            try {
                instance = CertificateFactory.getInstance("X.509", str);
            } catch (CertificateException e) {
                throw new CMSException("can't get certificate factory.", e);
            }
        } else {
            instance = CertificateFactory.getInstance("X.509");
        }
        Enumeration objects = aSN1Set.getObjects();
        while (objects.hasMoreElements()) {
            try {
                DERObject dERObject = ((DEREncodable) objects.nextElement()).getDERObject();
                if (dERObject instanceof ASN1Sequence) {
                    list.add(instance.generateCertificate(new ByteArrayInputStream(dERObject.getEncoded())));
                }
            } catch (IOException e2) {
                throw new CMSException("can't re-encode certificate!", e2);
            } catch (CertificateException e3) {
                throw new CMSException("can't re-encode certificate!", e3);
            }
        }
    }

    private static void addEntries(DERObjectIdentifier dERObjectIdentifier, String str, String str2) {
        digestAlgs.put(dERObjectIdentifier.getId(), str);
        encryptionAlgs.put(dERObjectIdentifier.getId(), str2);
    }

    private boolean anyCertHasTypeOther() {
        return false;
    }

    private boolean anyCertHasV1Attribute() {
        return false;
    }

    private boolean anyCertHasV2Attribute() {
        return false;
    }

    private boolean anyCrlHasTypeOther() {
        return false;
    }

    private MessageDigest createDigestInstance(String str, String str2) throws NoSuchAlgorithmException, NoSuchProviderException {
        return str2 != null ? MessageDigest.getInstance(str, str2) : MessageDigest.getInstance(str);
    }

    /* access modifiers changed from: package-private */
    public X509Store createAttributeStore(String str, String str2, ASN1Set aSN1Set) throws NoSuchStoreException, NoSuchProviderException, CMSException {
        ArrayList arrayList = new ArrayList();
        if (aSN1Set != null) {
            Enumeration objects = aSN1Set.getObjects();
            while (objects.hasMoreElements()) {
                try {
                    ASN1TaggedObject dERObject = ((DEREncodable) objects.nextElement()).getDERObject();
                    if (dERObject instanceof ASN1TaggedObject) {
                        ASN1TaggedObject aSN1TaggedObject = dERObject;
                        if (aSN1TaggedObject.getTagNo() == 2) {
                            arrayList.add(new X509V2AttributeCertificate(ASN1Sequence.getInstance(aSN1TaggedObject, false).getEncoded()));
                        }
                    }
                } catch (IOException e) {
                    throw new CMSException("can't re-encode attribute certificate!", e);
                }
            }
        }
        try {
            return X509Store.getInstance("AttributeCertificate/" + str, new X509CollectionStoreParameters(arrayList), str2);
        } catch (IllegalArgumentException e2) {
            throw new CMSException("can't setup the X509Store", e2);
        }
    }

    /* access modifiers changed from: package-private */
    public X509Store createCRLsStore(String str, String str2, ASN1Set aSN1Set) throws NoSuchStoreException, NoSuchProviderException, CMSException {
        ArrayList arrayList = new ArrayList();
        if (aSN1Set != null) {
            addCRLsFromSet(arrayList, aSN1Set, str2);
        }
        try {
            return X509Store.getInstance("CRL/" + str, new X509CollectionStoreParameters(arrayList), str2);
        } catch (IllegalArgumentException e) {
            throw new CMSException("can't setup the X509Store", e);
        }
    }

    /* access modifiers changed from: package-private */
    public CertStore createCertStore(String str, String str2, ASN1Set aSN1Set, ASN1Set aSN1Set2) throws NoSuchProviderException, CMSException, NoSuchAlgorithmException {
        ArrayList arrayList = new ArrayList();
        if (aSN1Set != null) {
            addCertsFromSet(arrayList, aSN1Set, str2);
        }
        if (aSN1Set2 != null) {
            addCRLsFromSet(arrayList, aSN1Set2, str2);
        }
        if (str2 == null) {
            return CertStore.getInstance(str, new CollectionCertStoreParameters(arrayList));
        }
        try {
            return CertStore.getInstance(str, new CollectionCertStoreParameters(arrayList), str2);
        } catch (InvalidAlgorithmParameterException e) {
            throw new CMSException("can't setup the CertStore", e);
        }
    }

    /* access modifiers changed from: package-private */
    public X509Store createCertificateStore(String str, String str2, ASN1Set aSN1Set) throws NoSuchStoreException, NoSuchProviderException, CMSException {
        ArrayList arrayList = new ArrayList();
        if (aSN1Set != null) {
            addCertsFromSet(arrayList, aSN1Set, str2);
        }
        try {
            return X509Store.getInstance("Certificate/" + str, new X509CollectionStoreParameters(arrayList), str2);
        } catch (IllegalArgumentException e) {
            throw new CMSException("can't setup the X509Store", e);
        }
    }

    /* access modifiers changed from: package-private */
    public String getDigestAlgName(String str) {
        String str2 = (String) digestAlgs.get(str);
        return str2 != null ? str2 : str;
    }

    /* access modifiers changed from: package-private */
    public String[] getDigestAliases(String str) {
        String[] strArr = (String[]) digestAliases.get(str);
        return strArr != null ? strArr : new String[0];
    }

    /* access modifiers changed from: package-private */
    public MessageDigest getDigestInstance(String str, String str2) throws NoSuchProviderException, NoSuchAlgorithmException {
        try {
            return createDigestInstance(str, str2);
        } catch (NoSuchAlgorithmException e) {
            String[] digestAliases2 = getDigestAliases(str);
            int i = 0;
            while (i != digestAliases2.length) {
                try {
                    return createDigestInstance(digestAliases2[i], str2);
                } catch (NoSuchAlgorithmException e2) {
                    i++;
                }
            }
            if (str2 != null) {
                return getDigestInstance(str, null);
            }
            throw e;
        }
    }

    /* access modifiers changed from: package-private */
    public String getEncryptionAlgName(String str) {
        String str2 = (String) encryptionAlgs.get(str);
        return str2 != null ? str2 : str;
    }

    /* access modifiers changed from: package-private */
    public Signature getSignatureInstance(String str, String str2) throws NoSuchProviderException, NoSuchAlgorithmException {
        return str2 != null ? Signature.getInstance(str, str2) : Signature.getInstance(str);
    }
}
