package org.bouncycastle.cms;

import java.io.IOException;
import java.io.InputStream;
import java.security.cert.CRL;
import java.security.cert.CRLException;
import java.security.cert.CertStore;
import java.security.cert.CertStoreException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.BERSet;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.cms.ContentInfo;
import org.bouncycastle.asn1.x509.CertificateList;
import org.bouncycastle.asn1.x509.X509CertificateStructure;
import org.bouncycastle.util.io.Streams;

class CMSUtils {
    private static final Runtime RUNTIME = Runtime.getRuntime();

    CMSUtils() {
    }

    static ASN1Set createBerSetFromList(List list) {
        ASN1EncodableVector aSN1EncodableVector = new ASN1EncodableVector();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            aSN1EncodableVector.add((DEREncodable) it.next());
        }
        return new BERSet(aSN1EncodableVector);
    }

    static ASN1Set createDerSetFromList(List list) {
        ASN1EncodableVector aSN1EncodableVector = new ASN1EncodableVector();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            aSN1EncodableVector.add((DEREncodable) it.next());
        }
        return new DERSet(aSN1EncodableVector);
    }

    static List getCRLsFromStore(CertStore certStore) throws CertStoreException, CMSException {
        ArrayList arrayList = new ArrayList();
        try {
            Iterator<? extends CRL> it = certStore.getCRLs(null).iterator();
            while (it.hasNext()) {
                arrayList.add(CertificateList.getInstance(ASN1Object.fromByteArray(((X509CRL) it.next()).getEncoded())));
            }
            return arrayList;
        } catch (IllegalArgumentException e) {
            throw new CMSException("error processing crls", e);
        } catch (IOException e2) {
            throw new CMSException("error processing crls", e2);
        } catch (CRLException e3) {
            throw new CMSException("error encoding crls", e3);
        }
    }

    static List getCertificatesFromStore(CertStore certStore) throws CertStoreException, CMSException {
        ArrayList arrayList = new ArrayList();
        try {
            Iterator<? extends Certificate> it = certStore.getCertificates(null).iterator();
            while (it.hasNext()) {
                arrayList.add(X509CertificateStructure.getInstance(ASN1Object.fromByteArray(((X509Certificate) it.next()).getEncoded())));
            }
            return arrayList;
        } catch (IllegalArgumentException e) {
            throw new CMSException("error processing certs", e);
        } catch (IOException e2) {
            throw new CMSException("error processing certs", e2);
        } catch (CertificateEncodingException e3) {
            throw new CMSException("error encoding certs", e3);
        }
    }

    static int getMaximumMemory() {
        long maxMemory = RUNTIME.maxMemory();
        if (maxMemory > 2147483647L) {
            return Integer.MAX_VALUE;
        }
        return (int) maxMemory;
    }

    static ContentInfo readContentInfo(InputStream inputStream) throws CMSException {
        return readContentInfo(new ASN1InputStream(inputStream, getMaximumMemory()));
    }

    private static ContentInfo readContentInfo(ASN1InputStream aSN1InputStream) throws CMSException {
        try {
            return ContentInfo.getInstance(aSN1InputStream.readObject());
        } catch (IOException e) {
            throw new CMSException("IOException reading content.", e);
        } catch (ClassCastException e2) {
            throw new CMSException("Malformed content.", e2);
        } catch (IllegalArgumentException e3) {
            throw new CMSException("Malformed content.", e3);
        }
    }

    static ContentInfo readContentInfo(byte[] bArr) throws CMSException {
        return readContentInfo(new ASN1InputStream(bArr));
    }

    public static byte[] streamToByteArray(InputStream inputStream) throws IOException {
        return Streams.readAll(inputStream);
    }
}
