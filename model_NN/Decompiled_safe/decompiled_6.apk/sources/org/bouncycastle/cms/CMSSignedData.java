package org.bouncycastle.cms;

import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertStore;
import java.security.cert.CertStoreException;
import java.util.ArrayList;
import java.util.Map;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.BERSequence;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.DERObject;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.cms.ContentInfo;
import org.bouncycastle.asn1.cms.SignedData;
import org.bouncycastle.asn1.cms.SignerInfo;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.x509.NoSuchStoreException;
import org.bouncycastle.x509.X509Store;

public class CMSSignedData {
    private static final CMSSignedHelper HELPER = CMSSignedHelper.INSTANCE;
    X509Store attributeStore;
    CertStore certStore;
    X509Store certificateStore;
    ContentInfo contentInfo;
    X509Store crlStore;
    private Map hashes;
    CMSProcessable signedContent;
    SignedData signedData;
    SignerInformationStore signerInfoStore;

    public CMSSignedData(InputStream inputStream) throws CMSException {
        this(CMSUtils.readContentInfo(inputStream));
    }

    public CMSSignedData(Map map, ContentInfo contentInfo2) {
        this.hashes = map;
        this.contentInfo = contentInfo2;
        this.signedData = SignedData.getInstance(this.contentInfo.getContent());
    }

    public CMSSignedData(Map map, byte[] bArr) throws CMSException {
        this(map, CMSUtils.readContentInfo(bArr));
    }

    public CMSSignedData(ContentInfo contentInfo2) {
        this.contentInfo = contentInfo2;
        this.signedData = SignedData.getInstance(this.contentInfo.getContent());
        if (this.signedData.getEncapContentInfo().getContent() != null) {
            this.signedContent = new CMSProcessableByteArray(this.signedData.getEncapContentInfo().getContent().getOctets());
        } else {
            this.signedContent = null;
        }
    }

    public CMSSignedData(CMSProcessable cMSProcessable, InputStream inputStream) throws CMSException {
        this(cMSProcessable, CMSUtils.readContentInfo((InputStream) new ASN1InputStream(inputStream)));
    }

    public CMSSignedData(CMSProcessable cMSProcessable, ContentInfo contentInfo2) {
        this.signedContent = cMSProcessable;
        this.contentInfo = contentInfo2;
        this.signedData = SignedData.getInstance(this.contentInfo.getContent());
    }

    public CMSSignedData(CMSProcessable cMSProcessable, byte[] bArr) throws CMSException {
        this(cMSProcessable, CMSUtils.readContentInfo(bArr));
    }

    private CMSSignedData(CMSSignedData cMSSignedData) {
        this.signedData = cMSSignedData.signedData;
        this.contentInfo = cMSSignedData.contentInfo;
        this.signedContent = cMSSignedData.signedContent;
        this.certStore = cMSSignedData.certStore;
        this.signerInfoStore = cMSSignedData.signerInfoStore;
    }

    public CMSSignedData(byte[] bArr) throws CMSException {
        this(CMSUtils.readContentInfo(bArr));
    }

    private static AlgorithmIdentifier makeAlgId(String str, byte[] bArr) throws IOException {
        return bArr != null ? new AlgorithmIdentifier(new DERObjectIdentifier(str), makeObj(bArr)) : new AlgorithmIdentifier(new DERObjectIdentifier(str), new DERNull());
    }

    private static DERObject makeObj(byte[] bArr) throws IOException {
        if (bArr == null) {
            return null;
        }
        return new ASN1InputStream(bArr).readObject();
    }

    public static CMSSignedData replaceCertificatesAndCRLs(CMSSignedData cMSSignedData, CertStore certStore2) throws CMSException {
        CMSSignedData cMSSignedData2 = new CMSSignedData(cMSSignedData);
        cMSSignedData2.certStore = certStore2;
        try {
            ASN1Set createBerSetFromList = CMSUtils.createBerSetFromList(CMSUtils.getCertificatesFromStore(certStore2));
            if (createBerSetFromList.size() == 0) {
                createBerSetFromList = null;
            }
            try {
                ASN1Set createBerSetFromList2 = CMSUtils.createBerSetFromList(CMSUtils.getCRLsFromStore(certStore2));
                if (createBerSetFromList2.size() == 0) {
                    createBerSetFromList2 = null;
                }
                cMSSignedData2.signedData = new SignedData(cMSSignedData.signedData.getDigestAlgorithms(), cMSSignedData.signedData.getEncapContentInfo(), createBerSetFromList, createBerSetFromList2, cMSSignedData.signedData.getSignerInfos());
                cMSSignedData2.contentInfo = new ContentInfo(cMSSignedData2.contentInfo.getContentType(), cMSSignedData2.signedData);
                return cMSSignedData2;
            } catch (CertStoreException e) {
                throw new CMSException("error getting crls from certStore", e);
            }
        } catch (CertStoreException e2) {
            throw new CMSException("error getting certs from certStore", e2);
        }
    }

    public static CMSSignedData replaceSigners(CMSSignedData cMSSignedData, SignerInformationStore signerInformationStore) {
        CMSSignedData cMSSignedData2 = new CMSSignedData(cMSSignedData);
        cMSSignedData2.signerInfoStore = signerInformationStore;
        ASN1EncodableVector aSN1EncodableVector = new ASN1EncodableVector();
        ASN1EncodableVector aSN1EncodableVector2 = new ASN1EncodableVector();
        for (SignerInformation signerInformation : signerInformationStore.getSigners()) {
            try {
                aSN1EncodableVector.add(makeAlgId(signerInformation.getDigestAlgOID(), signerInformation.getDigestAlgParams()));
                aSN1EncodableVector2.add(signerInformation.toSignerInfo());
            } catch (IOException e) {
                throw new RuntimeException("encoding error.", e);
            }
        }
        DERSet dERSet = new DERSet(aSN1EncodableVector);
        DERSet dERSet2 = new DERSet(aSN1EncodableVector2);
        ASN1Sequence dERObject = cMSSignedData.signedData.getDERObject();
        ASN1EncodableVector aSN1EncodableVector3 = new ASN1EncodableVector();
        aSN1EncodableVector3.add(dERObject.getObjectAt(0));
        aSN1EncodableVector3.add(dERSet);
        for (int i = 2; i != dERObject.size() - 1; i++) {
            aSN1EncodableVector3.add(dERObject.getObjectAt(i));
        }
        aSN1EncodableVector3.add(dERSet2);
        cMSSignedData2.signedData = SignedData.getInstance(new BERSequence(aSN1EncodableVector3));
        cMSSignedData2.contentInfo = new ContentInfo(cMSSignedData2.contentInfo.getContentType(), cMSSignedData2.signedData);
        return cMSSignedData2;
    }

    public X509Store getAttributeCertificates(String str, String str2) throws NoSuchStoreException, NoSuchProviderException, CMSException {
        if (this.attributeStore == null) {
            this.attributeStore = HELPER.createAttributeStore(str, str2, this.signedData.getCertificates());
        }
        return this.attributeStore;
    }

    public X509Store getCRLs(String str, String str2) throws NoSuchStoreException, NoSuchProviderException, CMSException {
        if (this.crlStore == null) {
            this.crlStore = HELPER.createCRLsStore(str, str2, this.signedData.getCRLs());
        }
        return this.crlStore;
    }

    public X509Store getCertificates(String str, String str2) throws NoSuchStoreException, NoSuchProviderException, CMSException {
        if (this.certificateStore == null) {
            this.certificateStore = HELPER.createCertificateStore(str, str2, this.signedData.getCertificates());
        }
        return this.certificateStore;
    }

    public CertStore getCertificatesAndCRLs(String str, String str2) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        if (this.certStore == null) {
            this.certStore = HELPER.createCertStore(str, str2, this.signedData.getCertificates(), this.signedData.getCRLs());
        }
        return this.certStore;
    }

    public ContentInfo getContentInfo() {
        return this.contentInfo;
    }

    public byte[] getEncoded() throws IOException {
        return this.contentInfo.getEncoded();
    }

    public CMSProcessable getSignedContent() {
        return this.signedContent;
    }

    public String getSignedContentTypeOID() {
        return this.signedData.getEncapContentInfo().getContentType().getId();
    }

    public SignerInformationStore getSignerInfos() {
        if (this.signerInfoStore == null) {
            ASN1Set signerInfos = this.signedData.getSignerInfos();
            ArrayList arrayList = new ArrayList();
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 == signerInfos.size()) {
                    break;
                }
                if (this.hashes == null) {
                    arrayList.add(new SignerInformation(SignerInfo.getInstance(signerInfos.getObjectAt(i2)), this.signedData.getEncapContentInfo().getContentType(), this.signedContent, null));
                } else {
                    SignerInfo instance = SignerInfo.getInstance(signerInfos.getObjectAt(i2));
                    arrayList.add(new SignerInformation(instance, this.signedData.getEncapContentInfo().getContentType(), null, new BaseDigestCalculator((byte[]) this.hashes.get(instance.getDigestAlgorithm().getObjectId().getId()))));
                }
                i = i2 + 1;
            }
            this.signerInfoStore = new SignerInformationStore(arrayList);
        }
        return this.signerInfoStore;
    }

    public int getVersion() {
        return this.signedData.getVersion().getValue().intValue();
    }
}
