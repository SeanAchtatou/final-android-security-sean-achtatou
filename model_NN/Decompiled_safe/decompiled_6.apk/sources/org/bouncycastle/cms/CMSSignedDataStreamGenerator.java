package org.bouncycastle.cms;

import java.io.IOException;
import java.io.OutputStream;
import java.security.DigestOutputStream;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.BEROctetStringGenerator;
import org.bouncycastle.asn1.BERSequenceGenerator;
import org.bouncycastle.asn1.BERTaggedObject;
import org.bouncycastle.asn1.DERInteger;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.DERObject;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.asn1.cms.CMSObjectIdentifiers;
import org.bouncycastle.asn1.cms.IssuerAndSerialNumber;
import org.bouncycastle.asn1.cms.SignerIdentifier;
import org.bouncycastle.asn1.cms.SignerInfo;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.TBSCertificateStructure;

public class CMSSignedDataStreamGenerator extends CMSSignedGenerator {
    private int _bufferSize;
    private List _messageDigests = new ArrayList();
    /* access modifiers changed from: private */
    public List _signerInfs = new ArrayList();

    private class CmsSignedDataOutputStream extends OutputStream {
        private DERObjectIdentifier _contentOID;
        private BERSequenceGenerator _eiGen;
        private OutputStream _out;
        private BERSequenceGenerator _sGen;
        private BERSequenceGenerator _sigGen;

        public CmsSignedDataOutputStream(OutputStream outputStream, String str, BERSequenceGenerator bERSequenceGenerator, BERSequenceGenerator bERSequenceGenerator2, BERSequenceGenerator bERSequenceGenerator3) {
            this._out = outputStream;
            this._contentOID = new DERObjectIdentifier(str);
            this._sGen = bERSequenceGenerator;
            this._sigGen = bERSequenceGenerator2;
            this._eiGen = bERSequenceGenerator3;
        }

        public void close() throws IOException {
            this._out.close();
            this._eiGen.close();
            CMSSignedDataStreamGenerator.this._digests.clear();
            if (CMSSignedDataStreamGenerator.this._certs.size() != 0) {
                this._sigGen.getRawOutputStream().write(new BERTaggedObject(false, 0, CMSUtils.createBerSetFromList(CMSSignedDataStreamGenerator.this._certs)).getEncoded());
            }
            if (CMSSignedDataStreamGenerator.this._crls.size() != 0) {
                this._sigGen.getRawOutputStream().write(new BERTaggedObject(false, 1, CMSUtils.createBerSetFromList(CMSSignedDataStreamGenerator.this._crls)).getEncoded());
            }
            ASN1EncodableVector aSN1EncodableVector = new ASN1EncodableVector();
            for (SignerInformation signerInfo : CMSSignedDataStreamGenerator.this._signers) {
                aSN1EncodableVector.add(signerInfo.toSignerInfo());
            }
            for (SignerInf signerInfo2 : CMSSignedDataStreamGenerator.this._signerInfs) {
                try {
                    aSN1EncodableVector.add(signerInfo2.toSignerInfo(this._contentOID));
                } catch (IOException e) {
                    throw new IOException("encoding error." + e);
                } catch (SignatureException e2) {
                    throw new IOException("error creating signature." + e2);
                } catch (CertificateEncodingException e3) {
                    throw new IOException("error creating sid." + e3);
                }
            }
            this._sigGen.getRawOutputStream().write(new DERSet(aSN1EncodableVector).getEncoded());
            this._sigGen.close();
            this._sGen.close();
        }

        public void write(int i) throws IOException {
            this._out.write(i);
        }

        public void write(byte[] bArr) throws IOException {
            this._out.write(bArr);
        }

        public void write(byte[] bArr, int i, int i2) throws IOException {
            this._out.write(bArr, i, i2);
        }
    }

    private class NullOutputStream extends OutputStream {
        private NullOutputStream() {
        }

        public void write(int i) throws IOException {
        }

        public void write(byte[] bArr) throws IOException {
        }

        public void write(byte[] bArr, int i, int i2) throws IOException {
        }
    }

    private class SignerInf {
        X509Certificate _cert;
        MessageDigest _digest;
        String _digestOID;
        String _encOID;
        PrivateKey _key;
        CMSAttributeTableGenerator _sAttr;
        Signature _signature;
        CMSAttributeTableGenerator _unsAttr;

        SignerInf(PrivateKey privateKey, X509Certificate x509Certificate, String str, String str2, CMSAttributeTableGenerator cMSAttributeTableGenerator, CMSAttributeTableGenerator cMSAttributeTableGenerator2, MessageDigest messageDigest, Signature signature) {
            this._key = privateKey;
            this._cert = x509Certificate;
            this._digestOID = str;
            this._encOID = str2;
            this._sAttr = cMSAttributeTableGenerator;
            this._unsAttr = cMSAttributeTableGenerator2;
            this._digest = messageDigest;
            this._signature = signature;
        }

        /* access modifiers changed from: package-private */
        public X509Certificate getCertificate() {
            return this._cert;
        }

        /* access modifiers changed from: package-private */
        public String getDigestAlgOID() {
            return this._digestOID;
        }

        /* access modifiers changed from: package-private */
        public byte[] getDigestAlgParams() {
            return null;
        }

        /* access modifiers changed from: package-private */
        public String getEncryptionAlgOID() {
            return this._encOID;
        }

        /* access modifiers changed from: package-private */
        public PrivateKey getKey() {
            return this._key;
        }

        /* access modifiers changed from: package-private */
        public SignerInfo toSignerInfo(DERObjectIdentifier dERObjectIdentifier) throws IOException, SignatureException, CertificateEncodingException {
            AttributeTable attributeTable = null;
            AlgorithmIdentifier algorithmIdentifier = new AlgorithmIdentifier(new DERObjectIdentifier(getDigestAlgOID()), new DERNull());
            AlgorithmIdentifier encAlgorithmIdentifier = CMSSignedDataStreamGenerator.this.getEncAlgorithmIdentifier(getEncryptionAlgOID());
            byte[] digest = this._digest.digest();
            CMSSignedDataStreamGenerator.this._digests.put(this._digestOID, digest.clone());
            ASN1Set attributeSet = CMSSignedDataStreamGenerator.this.getAttributeSet(this._sAttr != null ? this._sAttr.getAttributes(Collections.unmodifiableMap(CMSSignedDataStreamGenerator.this.getBaseParameters(dERObjectIdentifier, algorithmIdentifier, digest))) : null);
            if (attributeSet != null) {
                this._signature.update(attributeSet.getEncoded("DER"));
                DEROctetString dEROctetString = new DEROctetString(this._signature.sign());
                Map baseParameters = CMSSignedDataStreamGenerator.this.getBaseParameters(dERObjectIdentifier, algorithmIdentifier, digest);
                baseParameters.put(CMSAttributeTableGenerator.SIGNATURE, dEROctetString.getOctets().clone());
                if (this._unsAttr != null) {
                    attributeTable = this._unsAttr.getAttributes(Collections.unmodifiableMap(baseParameters));
                }
                ASN1Set attributeSet2 = CMSSignedDataStreamGenerator.this.getAttributeSet(attributeTable);
                TBSCertificateStructure instance = TBSCertificateStructure.getInstance(new ASN1InputStream(getCertificate().getTBSCertificate()).readObject());
                return new SignerInfo(new SignerIdentifier(new IssuerAndSerialNumber(instance.getIssuer(), instance.getSerialNumber().getValue())), algorithmIdentifier, attributeSet, encAlgorithmIdentifier, dEROctetString, attributeSet2);
            }
            throw new RuntimeException("signatures without signed attributes not implemented.");
        }
    }

    private class TeeOutputStream extends OutputStream {
        private OutputStream s1;
        private OutputStream s2;

        public TeeOutputStream(OutputStream outputStream, OutputStream outputStream2) {
            this.s1 = outputStream;
            this.s2 = outputStream2;
        }

        public void close() throws IOException {
            this.s1.close();
            this.s2.close();
        }

        public void write(int i) throws IOException {
            this.s1.write(i);
            this.s2.write(i);
        }

        public void write(byte[] bArr) throws IOException {
            this.s1.write(bArr);
            this.s2.write(bArr);
        }

        public void write(byte[] bArr, int i, int i2) throws IOException {
            this.s1.write(bArr, i, i2);
            this.s2.write(bArr, i, i2);
        }
    }

    public CMSSignedDataStreamGenerator() {
    }

    public CMSSignedDataStreamGenerator(SecureRandom secureRandom) {
        super(secureRandom);
    }

    private DERInteger calculateVersion(String str) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        boolean z6;
        boolean z7 = false;
        if (this._certs != null) {
            z3 = false;
            z2 = false;
            z = false;
            for (Object next : this._certs) {
                if (next instanceof ASN1TaggedObject) {
                    ASN1TaggedObject aSN1TaggedObject = (ASN1TaggedObject) next;
                    if (aSN1TaggedObject.getTagNo() == 1) {
                        z4 = z3;
                        z6 = z;
                        z5 = true;
                    } else if (aSN1TaggedObject.getTagNo() == 2) {
                        z4 = true;
                        z5 = z2;
                        z6 = z;
                    } else if (aSN1TaggedObject.getTagNo() == 3) {
                        z4 = z3;
                        z5 = z2;
                        z6 = true;
                    }
                    z = z6;
                    z2 = z5;
                    z3 = z4;
                }
                z4 = z3;
                z5 = z2;
                z6 = z;
                z = z6;
                z2 = z5;
                z3 = z4;
            }
        } else {
            z3 = false;
            z2 = false;
            z = false;
        }
        if (z) {
            return new DERInteger(5);
        }
        if (this._crls != null && !z) {
            for (Object obj : this._crls) {
                if (obj instanceof ASN1TaggedObject) {
                    z7 = true;
                }
            }
        }
        return z7 ? new DERInteger(5) : z3 ? new DERInteger(4) : z2 ? new DERInteger(3) : str.equals(DATA) ? checkForVersion3(this._signers) ? new DERInteger(3) : new DERInteger(1) : new DERInteger(3);
    }

    private boolean checkForVersion3(List list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            if (SignerInfo.getInstance(((SignerInformation) it.next()).toSignerInfo()).getVersion().getValue().intValue() == 3) {
                return true;
            }
        }
        return false;
    }

    private AlgorithmIdentifier makeAlgId(String str, byte[] bArr) throws IOException {
        return bArr != null ? new AlgorithmIdentifier(new DERObjectIdentifier(str), makeObj(bArr)) : new AlgorithmIdentifier(new DERObjectIdentifier(str), new DERNull());
    }

    private DERObject makeObj(byte[] bArr) throws IOException {
        if (bArr == null) {
            return null;
        }
        return new ASN1InputStream(bArr).readObject();
    }

    public void addSigner(PrivateKey privateKey, X509Certificate x509Certificate, String str, String str2) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        addSigner(privateKey, x509Certificate, str, new DefaultSignedAttributeTableGenerator(), (CMSAttributeTableGenerator) null, str2);
    }

    public void addSigner(PrivateKey privateKey, X509Certificate x509Certificate, String str, AttributeTable attributeTable, AttributeTable attributeTable2, String str2) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        addSigner(privateKey, x509Certificate, str, new DefaultSignedAttributeTableGenerator(attributeTable), new SimpleAttributeTableGenerator(attributeTable2), str2);
    }

    public void addSigner(PrivateKey privateKey, X509Certificate x509Certificate, String str, CMSAttributeTableGenerator cMSAttributeTableGenerator, CMSAttributeTableGenerator cMSAttributeTableGenerator2, String str2) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException {
        String encOID = getEncOID(privateKey, str);
        String digestAlgName = CMSSignedHelper.INSTANCE.getDigestAlgName(str);
        Signature signatureInstance = CMSSignedHelper.INSTANCE.getSignatureInstance(digestAlgName + "with" + CMSSignedHelper.INSTANCE.getEncryptionAlgName(encOID), str2);
        MessageDigest digestInstance = CMSSignedHelper.INSTANCE.getDigestInstance(digestAlgName, str2);
        signatureInstance.initSign(privateKey, this.rand);
        this._signerInfs.add(new SignerInf(privateKey, x509Certificate, str, encOID, cMSAttributeTableGenerator, cMSAttributeTableGenerator2, digestInstance, signatureInstance));
        this._messageDigests.add(digestInstance);
    }

    public OutputStream open(OutputStream outputStream) throws IOException {
        return open(outputStream, false);
    }

    public OutputStream open(OutputStream outputStream, String str, boolean z) throws IOException {
        return open(outputStream, str, z, null);
    }

    public OutputStream open(OutputStream outputStream, String str, boolean z, OutputStream outputStream2) throws IOException {
        OutputStream nullOutputStream;
        BERSequenceGenerator bERSequenceGenerator = new BERSequenceGenerator(outputStream);
        bERSequenceGenerator.addObject(CMSObjectIdentifiers.signedData);
        BERSequenceGenerator bERSequenceGenerator2 = new BERSequenceGenerator(bERSequenceGenerator.getRawOutputStream(), 0, true);
        bERSequenceGenerator2.addObject(calculateVersion(str));
        ASN1EncodableVector aSN1EncodableVector = new ASN1EncodableVector();
        for (SignerInformation signerInformation : this._signers) {
            aSN1EncodableVector.add(makeAlgId(signerInformation.getDigestAlgOID(), signerInformation.getDigestAlgParams()));
        }
        for (SignerInf signerInf : this._signerInfs) {
            aSN1EncodableVector.add(makeAlgId(signerInf.getDigestAlgOID(), signerInf.getDigestAlgParams()));
        }
        bERSequenceGenerator2.getRawOutputStream().write(new DERSet(aSN1EncodableVector).getEncoded());
        BERSequenceGenerator bERSequenceGenerator3 = new BERSequenceGenerator(bERSequenceGenerator2.getRawOutputStream());
        bERSequenceGenerator3.addObject(new DERObjectIdentifier(str));
        if (z) {
            BEROctetStringGenerator bEROctetStringGenerator = new BEROctetStringGenerator(bERSequenceGenerator3.getRawOutputStream(), 0, true);
            OutputStream octetOutputStream = this._bufferSize != 0 ? bEROctetStringGenerator.getOctetOutputStream(new byte[this._bufferSize]) : bEROctetStringGenerator.getOctetOutputStream();
            nullOutputStream = outputStream2 != null ? new TeeOutputStream(outputStream2, octetOutputStream) : octetOutputStream;
        } else {
            nullOutputStream = outputStream2 != null ? outputStream2 : new NullOutputStream();
        }
        DigestOutputStream digestOutputStream = nullOutputStream;
        for (MessageDigest digestOutputStream2 : this._messageDigests) {
            digestOutputStream = new DigestOutputStream(digestOutputStream, digestOutputStream2);
        }
        return new CmsSignedDataOutputStream(digestOutputStream, str, bERSequenceGenerator, bERSequenceGenerator2, bERSequenceGenerator3);
    }

    public OutputStream open(OutputStream outputStream, boolean z) throws IOException {
        return open(outputStream, DATA, z);
    }

    public OutputStream open(OutputStream outputStream, boolean z, OutputStream outputStream2) throws IOException {
        return open(outputStream, DATA, z, outputStream2);
    }

    public void setBufferSize(int i) {
        this._bufferSize = i;
    }
}
