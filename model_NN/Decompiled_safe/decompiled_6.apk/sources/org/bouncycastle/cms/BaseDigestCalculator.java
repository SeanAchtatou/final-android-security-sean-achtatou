package org.bouncycastle.cms;

import org.bouncycastle.util.Arrays;

class BaseDigestCalculator implements DigestCalculator {
    private final byte[] digest;

    BaseDigestCalculator(byte[] bArr) {
        this.digest = bArr;
    }

    public byte[] getDigest() {
        return Arrays.clone(this.digest);
    }
}
