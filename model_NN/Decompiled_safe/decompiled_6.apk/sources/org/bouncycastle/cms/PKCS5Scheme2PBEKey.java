package org.bouncycastle.cms;

import org.bouncycastle.crypto.PBEParametersGenerator;
import org.bouncycastle.crypto.generators.PKCS5S2ParametersGenerator;

public class PKCS5Scheme2PBEKey extends CMSPBEKey {
    public PKCS5Scheme2PBEKey(char[] cArr, byte[] bArr, int i) {
        super(cArr, bArr, i);
    }

    /* access modifiers changed from: package-private */
    public byte[] getEncoded(String str) {
        PKCS5S2ParametersGenerator pKCS5S2ParametersGenerator = new PKCS5S2ParametersGenerator();
        pKCS5S2ParametersGenerator.init(PBEParametersGenerator.PKCS5PasswordToBytes(getPassword()), getSalt(), getIterationCount());
        return pKCS5S2ParametersGenerator.generateDerivedParameters(CMSEnvelopedHelper.INSTANCE.getKeySize(str)).getKey();
    }
}
