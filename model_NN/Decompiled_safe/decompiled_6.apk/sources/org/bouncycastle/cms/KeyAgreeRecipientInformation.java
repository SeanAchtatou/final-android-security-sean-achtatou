package org.bouncycastle.cms;

import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import javax.crypto.Cipher;
import javax.crypto.KeyAgreement;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.cms.IssuerAndSerialNumber;
import org.bouncycastle.asn1.cms.KeyAgreeRecipientInfo;
import org.bouncycastle.asn1.cms.RecipientEncryptedKey;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;

public class KeyAgreeRecipientInformation extends RecipientInformation {
    private ASN1OctetString _encryptedKey;
    private KeyAgreeRecipientInfo _info;

    public KeyAgreeRecipientInformation(KeyAgreeRecipientInfo keyAgreeRecipientInfo, AlgorithmIdentifier algorithmIdentifier, InputStream inputStream) {
        super(algorithmIdentifier, AlgorithmIdentifier.getInstance(keyAgreeRecipientInfo.getKeyEncryptionAlgorithm()), inputStream);
        this._info = keyAgreeRecipientInfo;
        try {
            RecipientEncryptedKey instance = RecipientEncryptedKey.getInstance(this._info.getRecipientEncryptedKeys().getObjectAt(0));
            IssuerAndSerialNumber issuerAndSerialNumber = instance.getIdentifier().getIssuerAndSerialNumber();
            this._rid = new RecipientId();
            this._rid.setIssuer(issuerAndSerialNumber.getName().getEncoded());
            this._rid.setSerialNumber(issuerAndSerialNumber.getSerialNumber().getValue());
            this._encryptedKey = instance.getEncryptedKey();
        } catch (IOException e) {
            throw new IllegalArgumentException("invalid rid in KeyAgreeRecipientInformation");
        }
    }

    public CMSTypedStream getContentStream(Key key, String str) throws CMSException, NoSuchProviderException {
        try {
            X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(new SubjectPublicKeyInfo(PrivateKeyInfo.getInstance(ASN1Object.fromByteArray(key.getEncoded())).getAlgorithmId(), this._info.getOriginator().getOriginatorKey().getPublicKey().getBytes()).getEncoded());
            KeyFactory instance = KeyFactory.getInstance(this._keyEncAlg.getObjectId().getId(), str);
            KeyAgreement instance2 = KeyAgreement.getInstance(this._keyEncAlg.getObjectId().getId(), str);
            instance2.init(key);
            instance2.doPhase(instance.generatePublic(x509EncodedKeySpec), true);
            String id = DERObjectIdentifier.getInstance(ASN1Sequence.getInstance(this._keyEncAlg.getParameters()).getObjectAt(0)).getId();
            SecretKey generateSecret = instance2.generateSecret(id);
            Cipher instance3 = Cipher.getInstance(id, str);
            instance3.init(4, generateSecret);
            return getContentFromSessionKey(instance3.unwrap(this._encryptedKey.getOctets(), this._encAlg.getObjectId().getId(), 3), str);
        } catch (NoSuchAlgorithmException e) {
            throw new CMSException("can't find algorithm.", e);
        } catch (InvalidKeyException e2) {
            throw new CMSException("key invalid in message.", e2);
        } catch (InvalidKeySpecException e3) {
            throw new CMSException("originator key spec invalid.", e3);
        } catch (NoSuchPaddingException e4) {
            throw new CMSException("required padding not supported.", e4);
        } catch (Exception e5) {
            throw new CMSException("originator key invalid.", e5);
        }
    }
}
