package org.bouncycastle.cms;

import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import org.bouncycastle.asn1.cms.KEKRecipientInfo;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;

public class KEKRecipientInformation extends RecipientInformation {
    private AlgorithmIdentifier _encAlg;
    private KEKRecipientInfo _info;

    public KEKRecipientInformation(KEKRecipientInfo kEKRecipientInfo, AlgorithmIdentifier algorithmIdentifier, InputStream inputStream) {
        super(algorithmIdentifier, AlgorithmIdentifier.getInstance(kEKRecipientInfo.getKeyEncryptionAlgorithm()), inputStream);
        this._info = kEKRecipientInfo;
        this._encAlg = algorithmIdentifier;
        this._rid = new RecipientId();
        this._rid.setKeyIdentifier(kEKRecipientInfo.getKekid().getKeyIdentifier().getOctets());
    }

    public CMSTypedStream getContentStream(Key key, String str) throws CMSException, NoSuchProviderException {
        try {
            byte[] octets = this._info.getEncryptedKey().getOctets();
            Cipher instance = Cipher.getInstance(this._keyEncAlg.getObjectId().getId(), str);
            instance.init(4, key);
            return getContentFromSessionKey(instance.unwrap(octets, this._encAlg.getObjectId().getId(), 3), str);
        } catch (NoSuchAlgorithmException e) {
            throw new CMSException("can't find algorithm.", e);
        } catch (InvalidKeyException e2) {
            throw new CMSException("key invalid in message.", e2);
        } catch (NoSuchPaddingException e3) {
            throw new CMSException("required padding not supported.", e3);
        }
    }
}
