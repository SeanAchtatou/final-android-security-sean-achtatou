package org.bouncycastle.cms;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.AlgorithmParameters;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import org.bouncycastle.asn1.ASN1Null;
import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;

public abstract class RecipientInformation {
    protected InputStream _data;
    protected AlgorithmIdentifier _encAlg;
    protected AlgorithmIdentifier _keyEncAlg;
    protected RecipientId _rid = new RecipientId();

    protected RecipientInformation(AlgorithmIdentifier algorithmIdentifier, AlgorithmIdentifier algorithmIdentifier2, InputStream inputStream) {
        this._encAlg = algorithmIdentifier;
        this._keyEncAlg = algorithmIdentifier2;
        this._data = inputStream;
    }

    private byte[] encodeObj(DEREncodable dEREncodable) throws IOException {
        if (dEREncodable != null) {
            return dEREncodable.getDERObject().getEncoded();
        }
        return null;
    }

    public byte[] getContent(Key key, String str) throws CMSException, NoSuchProviderException {
        try {
            if (this._data instanceof ByteArrayInputStream) {
                this._data.reset();
            }
            return CMSUtils.streamToByteArray(getContentStream(key, str).getContentStream());
        } catch (IOException e) {
            throw new RuntimeException("unable to parse internal stream: " + e);
        }
    }

    /* access modifiers changed from: protected */
    public CMSTypedStream getContentFromSessionKey(Key key, String str) throws CMSException, NoSuchProviderException {
        String id = this._encAlg.getObjectId().getId();
        try {
            Cipher symmetricCipher = CMSEnvelopedHelper.INSTANCE.getSymmetricCipher(id, str);
            ASN1Object parameters = this._encAlg.getParameters();
            if (parameters != null && !(parameters instanceof ASN1Null)) {
                AlgorithmParameters createAlgorithmParameters = CMSEnvelopedHelper.INSTANCE.createAlgorithmParameters(id, symmetricCipher.getProvider().getName());
                createAlgorithmParameters.init(parameters.getEncoded(), "ASN.1");
                symmetricCipher.init(2, key, createAlgorithmParameters);
            } else if (id.equals(CMSEnvelopedDataGenerator.DES_EDE3_CBC) || id.equals("1.3.6.1.4.1.188.7.1.1.2") || id.equals("1.2.840.113533.7.66.10")) {
                symmetricCipher.init(2, key, new IvParameterSpec(new byte[8]));
            } else {
                symmetricCipher.init(2, key);
            }
            return new CMSTypedStream(new CipherInputStream(this._data, symmetricCipher));
        } catch (NoSuchAlgorithmException e) {
            throw new CMSException("can't find algorithm.", e);
        } catch (InvalidKeyException e2) {
            throw new CMSException("key invalid in message.", e2);
        } catch (NoSuchPaddingException e3) {
            throw new CMSException("required padding not supported.", e3);
        } catch (InvalidAlgorithmParameterException e4) {
            throw new CMSException("algorithm parameters invalid.", e4);
        } catch (IOException e5) {
            throw new CMSException("error decoding algorithm parameters.", e5);
        }
    }

    public abstract CMSTypedStream getContentStream(Key key, String str) throws CMSException, NoSuchProviderException;

    public String getKeyEncryptionAlgOID() {
        return this._keyEncAlg.getObjectId().getId();
    }

    public byte[] getKeyEncryptionAlgParams() {
        try {
            return encodeObj(this._keyEncAlg.getParameters());
        } catch (Exception e) {
            throw new RuntimeException("exception getting encryption parameters " + e);
        }
    }

    public AlgorithmParameters getKeyEncryptionAlgorithmParameters(String str) throws CMSException, NoSuchProviderException {
        try {
            byte[] encodeObj = encodeObj(this._keyEncAlg.getParameters());
            if (encodeObj == null) {
                return null;
            }
            AlgorithmParameters createAlgorithmParameters = CMSEnvelopedHelper.INSTANCE.createAlgorithmParameters(getKeyEncryptionAlgOID(), str);
            createAlgorithmParameters.init(encodeObj, "ASN.1");
            return createAlgorithmParameters;
        } catch (NoSuchAlgorithmException e) {
            throw new CMSException("can't find parameters for algorithm", e);
        } catch (IOException e2) {
            throw new CMSException("can't find parse parameters", e2);
        }
    }

    public RecipientId getRID() {
        return this._rid;
    }
}
