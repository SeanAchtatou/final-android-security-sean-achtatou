package org.bouncycastle.cms;

import cn.com.jit.ida.util.pki.cipher.Mechanism;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Enumeration;
import javax.crypto.Cipher;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Null;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.cms.Attribute;
import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.asn1.cms.CMSAttributes;
import org.bouncycastle.asn1.cms.IssuerAndSerialNumber;
import org.bouncycastle.asn1.cms.SignerIdentifier;
import org.bouncycastle.asn1.cms.SignerInfo;
import org.bouncycastle.asn1.cms.Time;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.DigestInfo;
import org.bouncycastle.cms.CMSSignedDataGenerator;

public class SignerInformation {
    private CMSProcessable content;
    private DERObjectIdentifier contentType;
    private AlgorithmIdentifier digestAlgorithm;
    private DigestCalculator digestCalculator;
    private AlgorithmIdentifier encryptionAlgorithm;
    private SignerInfo info;
    private byte[] resultDigest;
    private SignerId sid = new SignerId();
    private byte[] signature;
    private ASN1Set signedAttributes;
    private ASN1Set unsignedAttributes;

    SignerInformation(SignerInfo signerInfo, DERObjectIdentifier dERObjectIdentifier, CMSProcessable cMSProcessable, DigestCalculator digestCalculator2) {
        this.info = signerInfo;
        this.contentType = dERObjectIdentifier;
        try {
            SignerIdentifier sid2 = signerInfo.getSID();
            if (sid2.isTagged()) {
                this.sid.setSubjectKeyIdentifier(ASN1OctetString.getInstance(sid2.getId()).getOctets());
            } else {
                IssuerAndSerialNumber instance = IssuerAndSerialNumber.getInstance(sid2.getId());
                this.sid.setIssuer(instance.getName().getEncoded());
                this.sid.setSerialNumber(instance.getSerialNumber().getValue());
            }
            this.digestAlgorithm = signerInfo.getDigestAlgorithm();
            this.signedAttributes = signerInfo.getAuthenticatedAttributes();
            this.unsignedAttributes = signerInfo.getUnauthenticatedAttributes();
            this.encryptionAlgorithm = signerInfo.getDigestEncryptionAlgorithm();
            this.signature = signerInfo.getEncryptedDigest().getOctets();
            this.content = cMSProcessable;
            this.digestCalculator = digestCalculator2;
        } catch (IOException e) {
            throw new IllegalArgumentException("invalid sid in SignerInfo");
        }
    }

    public static SignerInformation addCounterSigners(SignerInformation signerInformation, SignerInformationStore signerInformationStore) {
        SignerInfo signerInfo = signerInformation.info;
        AttributeTable unsignedAttributes2 = signerInformation.getUnsignedAttributes();
        ASN1EncodableVector aSN1EncodableVector = unsignedAttributes2 != null ? unsignedAttributes2.toASN1EncodableVector() : new ASN1EncodableVector();
        ASN1EncodableVector aSN1EncodableVector2 = new ASN1EncodableVector();
        for (SignerInformation signerInfo2 : signerInformationStore.getSigners()) {
            aSN1EncodableVector2.add(signerInfo2.toSignerInfo());
        }
        aSN1EncodableVector.add(new Attribute(CMSAttributes.counterSignature, new DERSet(aSN1EncodableVector2)));
        return new SignerInformation(new SignerInfo(signerInfo.getSID(), signerInfo.getDigestAlgorithm(), signerInfo.getAuthenticatedAttributes(), signerInfo.getDigestEncryptionAlgorithm(), signerInfo.getEncryptedDigest(), new DERSet(aSN1EncodableVector)), signerInformation.contentType, signerInformation.content, null);
    }

    private DigestInfo derDecode(byte[] bArr) throws IOException, CMSException {
        if (bArr[0] != 48) {
            throw new IOException("not a digest info object");
        }
        DigestInfo digestInfo = new DigestInfo(new ASN1InputStream(bArr).readObject());
        if (digestInfo.getEncoded().length == bArr.length) {
            return digestInfo;
        }
        throw new CMSException("malformed RSA signature");
    }

    private boolean doVerify(PublicKey publicKey, AttributeTable attributeTable, String str) throws CMSException, NoSuchAlgorithmException, NoSuchProviderException {
        byte[] digest;
        String digestAlgName = CMSSignedHelper.INSTANCE.getDigestAlgName(getDigestAlgOID());
        Signature signatureInstance = CMSSignedHelper.INSTANCE.getSignatureInstance(digestAlgName + "with" + CMSSignedHelper.INSTANCE.getEncryptionAlgName(getEncryptionAlgOID()), str);
        MessageDigest digestInstance = CMSSignedHelper.INSTANCE.getDigestInstance(digestAlgName, str);
        try {
            signatureInstance.initVerify(publicKey);
            if (this.signedAttributes != null) {
                if (this.content != null) {
                    this.content.write(new CMSSignedDataGenerator.DigOutputStream(digestInstance));
                    digest = digestInstance.digest();
                } else {
                    digest = this.digestCalculator != null ? this.digestCalculator.getDigest() : null;
                }
                this.resultDigest = digest;
                Attribute attribute = attributeTable.get(CMSAttributes.messageDigest);
                Attribute attribute2 = attributeTable.get(CMSAttributes.contentType);
                if (attribute == null) {
                    throw new SignatureException("no hash for content found in signed attributes");
                } else if (attribute2 != null || this.contentType.equals(CMSAttributes.counterSignature)) {
                    ASN1OctetString dERObject = attribute.getAttrValues().getObjectAt(0).getDERObject();
                    if (dERObject instanceof ASN1OctetString) {
                        if (!MessageDigest.isEqual(digest, dERObject.getOctets())) {
                            throw new SignatureException("content hash found in signed attributes different");
                        }
                    } else if ((dERObject instanceof DERNull) && digest != null) {
                        throw new SignatureException("NULL hash found in signed attributes when one expected");
                    }
                    if (attribute2 == null || attribute2.getAttrValues().getObjectAt(0).equals(this.contentType)) {
                        signatureInstance.update(getEncodedSignedAttributes());
                    } else {
                        throw new SignatureException("contentType in signed attributes different");
                    }
                } else {
                    throw new SignatureException("no content type id found in signed attributes");
                }
            } else if (this.content != null) {
                this.content.write(new CMSSignedDataGenerator.SigOutputStream(signatureInstance));
                this.content.write(new CMSSignedDataGenerator.DigOutputStream(digestInstance));
                this.resultDigest = digestInstance.digest();
            } else {
                this.resultDigest = this.digestCalculator.getDigest();
                return verifyDigest(this.resultDigest, publicKey, getSignature(), str);
            }
            return signatureInstance.verify(getSignature());
        } catch (InvalidKeyException e) {
            throw new CMSException("key not appropriate to signature in message.", e);
        } catch (IOException e2) {
            throw new CMSException("can't process mime object to create signature.", e2);
        } catch (SignatureException e3) {
            throw new CMSException("invalid signature format in message: " + e3.getMessage(), e3);
        }
    }

    private byte[] encodeObj(DEREncodable dEREncodable) throws IOException {
        if (dEREncodable != null) {
            return dEREncodable.getDERObject().getEncoded();
        }
        return null;
    }

    private boolean isNull(DEREncodable dEREncodable) {
        return (dEREncodable instanceof ASN1Null) || dEREncodable == null;
    }

    public static SignerInformation replaceUnsignedAttributes(SignerInformation signerInformation, AttributeTable attributeTable) {
        SignerInfo signerInfo = signerInformation.info;
        return new SignerInformation(new SignerInfo(signerInfo.getSID(), signerInfo.getDigestAlgorithm(), signerInfo.getAuthenticatedAttributes(), signerInfo.getDigestEncryptionAlgorithm(), signerInfo.getEncryptedDigest(), attributeTable != null ? new DERSet(attributeTable.toASN1EncodableVector()) : null), signerInformation.contentType, signerInformation.content, null);
    }

    private boolean verifyDigest(byte[] bArr, PublicKey publicKey, byte[] bArr2, String str) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        String encryptionAlgName = CMSSignedHelper.INSTANCE.getEncryptionAlgName(getEncryptionAlgOID());
        try {
            if (encryptionAlgName.equals(Mechanism.RSA)) {
                Cipher instance = str != null ? Cipher.getInstance("RSA/ECB/PKCS1Padding", str) : Cipher.getInstance("RSA/ECB/PKCS1Padding");
                instance.init(2, publicKey);
                DigestInfo derDecode = derDecode(instance.doFinal(bArr2));
                if (derDecode.getAlgorithmId().getObjectId().equals(this.digestAlgorithm.getObjectId()) && isNull(derDecode.getAlgorithmId().getParameters())) {
                    return MessageDigest.isEqual(bArr, derDecode.getDigest());
                }
                return false;
            } else if (encryptionAlgName.equals(Mechanism.DSA)) {
                Signature instance2 = str != null ? Signature.getInstance("NONEwithDSA", str) : Signature.getInstance("NONEwithDSA");
                instance2.initVerify(publicKey);
                instance2.update(bArr);
                return instance2.verify(bArr2);
            } else {
                throw new CMSException("algorithm: " + encryptionAlgName + " not supported in base signatures.");
            }
        } catch (GeneralSecurityException e) {
            throw new CMSException("Exception processing signature: " + e, e);
        } catch (IOException e2) {
            throw new CMSException("Exception decoding signature: " + e2, e2);
        }
    }

    public byte[] getContentDigest() {
        if (this.resultDigest != null) {
            return (byte[]) this.resultDigest.clone();
        }
        throw new IllegalStateException("method can only be called after verify.");
    }

    public SignerInformationStore getCounterSignatures() {
        AttributeTable unsignedAttributes2 = getUnsignedAttributes();
        if (unsignedAttributes2 == null) {
            return new SignerInformationStore(new ArrayList(0));
        }
        ArrayList arrayList = new ArrayList();
        Attribute attribute = unsignedAttributes2.get(CMSAttributes.counterSignature);
        if (attribute != null) {
            ASN1Set attrValues = attribute.getAttrValues();
            arrayList = new ArrayList(attrValues.size());
            Enumeration objects = attrValues.getObjects();
            while (objects.hasMoreElements()) {
                SignerInfo instance = SignerInfo.getInstance(objects.nextElement());
                arrayList.add(new SignerInformation(instance, CMSAttributes.counterSignature, null, new CounterSignatureDigestCalculator(CMSSignedHelper.INSTANCE.getDigestAlgName(instance.getDigestAlgorithm().getObjectId().getId()), null, getSignature())));
            }
        }
        return new SignerInformationStore(arrayList);
    }

    public String getDigestAlgOID() {
        return this.digestAlgorithm.getObjectId().getId();
    }

    public byte[] getDigestAlgParams() {
        try {
            return encodeObj(this.digestAlgorithm.getParameters());
        } catch (Exception e) {
            throw new RuntimeException("exception getting digest parameters " + e);
        }
    }

    public byte[] getEncodedSignedAttributes() throws IOException {
        if (this.signedAttributes != null) {
            return this.signedAttributes.getEncoded("DER");
        }
        return null;
    }

    public String getEncryptionAlgOID() {
        return this.encryptionAlgorithm.getObjectId().getId();
    }

    public byte[] getEncryptionAlgParams() {
        try {
            return encodeObj(this.encryptionAlgorithm.getParameters());
        } catch (Exception e) {
            throw new RuntimeException("exception getting encryption parameters " + e);
        }
    }

    public SignerId getSID() {
        return this.sid;
    }

    public byte[] getSignature() {
        return (byte[]) this.signature.clone();
    }

    public AttributeTable getSignedAttributes() {
        if (this.signedAttributes == null) {
            return null;
        }
        return new AttributeTable(this.signedAttributes);
    }

    public AttributeTable getUnsignedAttributes() {
        if (this.unsignedAttributes == null) {
            return null;
        }
        return new AttributeTable(this.unsignedAttributes);
    }

    public int getVersion() {
        return this.info.getVersion().getValue().intValue();
    }

    public SignerInfo toSignerInfo() {
        return this.info;
    }

    public boolean verify(PublicKey publicKey, String str) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        return doVerify(publicKey, getSignedAttributes(), str);
    }

    public boolean verify(X509Certificate x509Certificate, String str) throws NoSuchAlgorithmException, NoSuchProviderException, CertificateExpiredException, CertificateNotYetValidException, CMSException {
        AttributeTable signedAttributes2 = getSignedAttributes();
        if (signedAttributes2 != null) {
            ASN1EncodableVector all = signedAttributes2.getAll(CMSAttributes.signingTime);
            switch (all.size()) {
                case 0:
                    break;
                default:
                    throw new CMSException("The SignedAttributes in a signerInfo MUST NOT include multiple instances of the signing-time attribute");
                case 1:
                    ASN1Set attrValues = all.get(0).getAttrValues();
                    if (attrValues.size() == 1) {
                        x509Certificate.checkValidity(Time.getInstance(attrValues.getObjectAt(0).getDERObject()).getDate());
                        break;
                    } else {
                        throw new CMSException("A signing-time attribute MUST have a single attribute value");
                    }
            }
        }
        return doVerify(x509Certificate.getPublicKey(), signedAttributes2, str);
    }
}
