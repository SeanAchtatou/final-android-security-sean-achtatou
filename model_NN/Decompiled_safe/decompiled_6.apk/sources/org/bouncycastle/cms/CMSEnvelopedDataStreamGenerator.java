package org.bouncycastle.cms;

import java.io.IOException;
import java.io.OutputStream;
import java.security.AlgorithmParameters;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.BEROctetStringGenerator;
import org.bouncycastle.asn1.BERSequenceGenerator;
import org.bouncycastle.asn1.BERSet;
import org.bouncycastle.asn1.DERInteger;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.cms.CMSObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.cms.CMSEnvelopedGenerator;

public class CMSEnvelopedDataStreamGenerator extends CMSEnvelopedGenerator {
    private boolean _berEncodeRecipientSet;
    private int _bufferSize;
    private Object _originatorInfo = null;
    private Object _unprotectedAttributes = null;

    private class CmsEnvelopedDataOutputStream extends OutputStream {
        private BERSequenceGenerator _cGen;
        private BERSequenceGenerator _eiGen;
        private BERSequenceGenerator _envGen;
        private CipherOutputStream _out;

        public CmsEnvelopedDataOutputStream(CipherOutputStream cipherOutputStream, BERSequenceGenerator bERSequenceGenerator, BERSequenceGenerator bERSequenceGenerator2, BERSequenceGenerator bERSequenceGenerator3) {
            this._out = cipherOutputStream;
            this._cGen = bERSequenceGenerator;
            this._envGen = bERSequenceGenerator2;
            this._eiGen = bERSequenceGenerator3;
        }

        public void close() throws IOException {
            this._out.close();
            this._eiGen.close();
            this._envGen.close();
            this._cGen.close();
        }

        public void write(int i) throws IOException {
            this._out.write(i);
        }

        public void write(byte[] bArr) throws IOException {
            this._out.write(bArr);
        }

        public void write(byte[] bArr, int i, int i2) throws IOException {
            this._out.write(bArr, i, i2);
        }
    }

    public CMSEnvelopedDataStreamGenerator() {
    }

    public CMSEnvelopedDataStreamGenerator(SecureRandom secureRandom) {
        super(secureRandom);
    }

    private DERInteger getVersion() {
        return (this._originatorInfo == null && this._unprotectedAttributes == null) ? new DERInteger(0) : new DERInteger(2);
    }

    private OutputStream open(OutputStream outputStream, String str, KeyGenerator keyGenerator, String str2) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        String name = keyGenerator.getProvider().getName();
        SecretKey generateKey = keyGenerator.generateKey();
        AlgorithmParameters generateParameters = generateParameters(str, generateKey, name);
        ASN1EncodableVector aSN1EncodableVector = new ASN1EncodableVector();
        for (CMSEnvelopedGenerator.RecipientInf recipientInfo : this.recipientInfs) {
            try {
                aSN1EncodableVector.add(recipientInfo.toRecipientInfo(generateKey, this.rand, str2));
            } catch (IOException e) {
                throw new CMSException("encoding error.", e);
            } catch (InvalidKeyException e2) {
                throw new CMSException("key inappropriate for algorithm.", e2);
            } catch (GeneralSecurityException e3) {
                throw new CMSException("error making encrypted content.", e3);
            }
        }
        return open(outputStream, str, generateKey, generateParameters, aSN1EncodableVector, name);
    }

    public OutputStream open(OutputStream outputStream, String str, int i, String str2) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException, IOException {
        KeyGenerator createSymmetricKeyGenerator = CMSEnvelopedHelper.INSTANCE.createSymmetricKeyGenerator(str, str2);
        createSymmetricKeyGenerator.init(i, this.rand);
        return open(outputStream, str, createSymmetricKeyGenerator, str2);
    }

    public OutputStream open(OutputStream outputStream, String str, String str2) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException, IOException {
        KeyGenerator createSymmetricKeyGenerator = CMSEnvelopedHelper.INSTANCE.createSymmetricKeyGenerator(str, str2);
        createSymmetricKeyGenerator.init(this.rand);
        return open(outputStream, str, createSymmetricKeyGenerator, str2);
    }

    /* access modifiers changed from: protected */
    public OutputStream open(OutputStream outputStream, String str, SecretKey secretKey, AlgorithmParameters algorithmParameters, ASN1EncodableVector aSN1EncodableVector, String str2) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        try {
            BERSequenceGenerator bERSequenceGenerator = new BERSequenceGenerator(outputStream);
            bERSequenceGenerator.addObject(CMSObjectIdentifiers.envelopedData);
            BERSequenceGenerator bERSequenceGenerator2 = new BERSequenceGenerator(bERSequenceGenerator.getRawOutputStream(), 0, true);
            bERSequenceGenerator2.addObject(getVersion());
            if (this._berEncodeRecipientSet) {
                bERSequenceGenerator2.getRawOutputStream().write(new BERSet(aSN1EncodableVector).getEncoded());
            } else {
                bERSequenceGenerator2.getRawOutputStream().write(new DERSet(aSN1EncodableVector).getEncoded());
            }
            Cipher symmetricCipher = CMSEnvelopedHelper.INSTANCE.getSymmetricCipher(str, str2);
            symmetricCipher.init(1, secretKey, algorithmParameters, this.rand);
            BERSequenceGenerator bERSequenceGenerator3 = new BERSequenceGenerator(bERSequenceGenerator2.getRawOutputStream());
            bERSequenceGenerator3.addObject(PKCSObjectIdentifiers.data);
            if (algorithmParameters == null) {
                algorithmParameters = symmetricCipher.getParameters();
            }
            bERSequenceGenerator3.getRawOutputStream().write(getAlgorithmIdentifier(str, algorithmParameters).getEncoded());
            BEROctetStringGenerator bEROctetStringGenerator = new BEROctetStringGenerator(bERSequenceGenerator3.getRawOutputStream(), 0, false);
            return new CmsEnvelopedDataOutputStream(this._bufferSize != 0 ? new CipherOutputStream(bEROctetStringGenerator.getOctetOutputStream(new byte[this._bufferSize]), symmetricCipher) : new CipherOutputStream(bEROctetStringGenerator.getOctetOutputStream(), symmetricCipher), bERSequenceGenerator, bERSequenceGenerator2, bERSequenceGenerator3);
        } catch (InvalidKeyException e) {
            throw new CMSException("key invalid in message.", e);
        } catch (NoSuchPaddingException e2) {
            throw new CMSException("required padding not supported.", e2);
        } catch (InvalidAlgorithmParameterException e3) {
            throw new CMSException("algorithm parameters invalid.", e3);
        } catch (IOException e4) {
            throw new CMSException("exception decoding algorithm parameters.", e4);
        }
    }

    public void setBEREncodeRecipients(boolean z) {
        this._berEncodeRecipientSet = z;
    }

    public void setBufferSize(int i) {
        this._bufferSize = i;
    }
}
