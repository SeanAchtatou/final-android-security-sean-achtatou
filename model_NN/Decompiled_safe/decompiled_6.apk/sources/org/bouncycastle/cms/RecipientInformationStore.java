package org.bouncycastle.cms;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class RecipientInformationStore {
    private Map table = new HashMap();

    public RecipientInformationStore(Collection collection) {
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            RecipientInformation recipientInformation = (RecipientInformation) it.next();
            RecipientId rid = recipientInformation.getRID();
            if (this.table.get(rid) == null) {
                this.table.put(rid, recipientInformation);
            } else {
                Object obj = this.table.get(rid);
                if (obj instanceof List) {
                    ((List) obj).add(recipientInformation);
                } else {
                    ArrayList arrayList = new ArrayList();
                    arrayList.add(obj);
                    arrayList.add(recipientInformation);
                    this.table.put(rid, arrayList);
                }
            }
        }
    }

    public RecipientInformation get(RecipientId recipientId) {
        Object obj = this.table.get(recipientId);
        return obj instanceof List ? (RecipientInformation) ((List) obj).get(0) : (RecipientInformation) obj;
    }

    public Collection getRecipients() {
        ArrayList arrayList = new ArrayList(this.table.size());
        for (Object next : this.table.values()) {
            if (next instanceof List) {
                arrayList.addAll((List) next);
            } else {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    public Collection getRecipients(RecipientId recipientId) {
        Object obj = this.table.get(recipientId);
        return obj instanceof List ? new ArrayList((List) obj) : obj != null ? Collections.singletonList(obj) : new ArrayList();
    }

    public int size() {
        Iterator it = this.table.values().iterator();
        int i = 0;
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            Object next = it.next();
            i = next instanceof List ? ((List) next).size() + i2 : i2 + 1;
        }
    }
}
