package org.bouncycastle.cms;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

interface DigestCalculator {
    byte[] getDigest() throws NoSuchProviderException, NoSuchAlgorithmException;
}
