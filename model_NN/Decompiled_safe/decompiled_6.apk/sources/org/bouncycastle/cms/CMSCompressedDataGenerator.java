package org.bouncycastle.cms;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.DeflaterOutputStream;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.BERConstructedOctetString;
import org.bouncycastle.asn1.DERObject;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.cms.CMSObjectIdentifiers;
import org.bouncycastle.asn1.cms.CompressedData;
import org.bouncycastle.asn1.cms.ContentInfo;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;

public class CMSCompressedDataGenerator {
    public static final String ZLIB = "1.2.840.113549.1.9.16.3.8";

    private AlgorithmIdentifier makeAlgId(String str, byte[] bArr) throws IOException {
        return bArr != null ? new AlgorithmIdentifier(new DERObjectIdentifier(str), makeObj(bArr)) : new AlgorithmIdentifier(new DERObjectIdentifier(str));
    }

    private DERObject makeObj(byte[] bArr) throws IOException {
        if (bArr == null) {
            return null;
        }
        return new ASN1InputStream(new ByteArrayInputStream(bArr)).readObject();
    }

    public CMSCompressedData generate(CMSProcessable cMSProcessable, String str) throws CMSException {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            DeflaterOutputStream deflaterOutputStream = new DeflaterOutputStream(byteArrayOutputStream);
            cMSProcessable.write(deflaterOutputStream);
            deflaterOutputStream.close();
            return new CMSCompressedData(new ContentInfo(CMSObjectIdentifiers.compressedData, new CompressedData(makeAlgId(str, null), new ContentInfo(CMSObjectIdentifiers.data, new BERConstructedOctetString(byteArrayOutputStream.toByteArray())))));
        } catch (IOException e) {
            throw new CMSException("exception encoding data.", e);
        }
    }
}
