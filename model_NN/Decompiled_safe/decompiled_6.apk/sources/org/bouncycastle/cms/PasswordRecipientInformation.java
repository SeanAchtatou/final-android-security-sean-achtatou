package org.bouncycastle.cms;

import java.io.InputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.cms.PasswordRecipientInfo;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;

public class PasswordRecipientInformation extends RecipientInformation {
    private AlgorithmIdentifier _encAlg;
    private PasswordRecipientInfo _info;

    public PasswordRecipientInformation(PasswordRecipientInfo passwordRecipientInfo, AlgorithmIdentifier algorithmIdentifier, InputStream inputStream) {
        super(algorithmIdentifier, AlgorithmIdentifier.getInstance(passwordRecipientInfo.getKeyEncryptionAlgorithm()), inputStream);
        this._info = passwordRecipientInfo;
        this._encAlg = algorithmIdentifier;
        this._rid = new RecipientId();
    }

    public CMSTypedStream getContentStream(Key key, String str) throws CMSException, NoSuchProviderException {
        try {
            ASN1Sequence parameters = AlgorithmIdentifier.getInstance(this._info.getKeyEncryptionAlgorithm()).getParameters();
            byte[] octets = this._info.getEncryptedKey().getOctets();
            String id = DERObjectIdentifier.getInstance(parameters.getObjectAt(0)).getId();
            Cipher instance = Cipher.getInstance(CMSEnvelopedHelper.INSTANCE.getRFC3211WrapperName(id), str);
            instance.init(4, new SecretKeySpec(((CMSPBEKey) key).getEncoded(id), id), new IvParameterSpec(ASN1OctetString.getInstance(parameters.getObjectAt(1)).getOctets()));
            return getContentFromSessionKey(instance.unwrap(octets, this._encAlg.getObjectId().getId(), 3), str);
        } catch (NoSuchAlgorithmException e) {
            throw new CMSException("can't find algorithm.", e);
        } catch (InvalidKeyException e2) {
            throw new CMSException("key invalid in message.", e2);
        } catch (NoSuchPaddingException e3) {
            throw new CMSException("required padding not supported.", e3);
        } catch (InvalidAlgorithmParameterException e4) {
            throw new CMSException("invalid iv.", e4);
        }
    }
}
