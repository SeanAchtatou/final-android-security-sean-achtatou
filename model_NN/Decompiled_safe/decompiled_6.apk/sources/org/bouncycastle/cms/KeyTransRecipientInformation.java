package org.bouncycastle.cms;

import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.ProviderException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import org.bouncycastle.asn1.ASN1OctetString;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.cms.IssuerAndSerialNumber;
import org.bouncycastle.asn1.cms.KeyTransRecipientInfo;
import org.bouncycastle.asn1.cms.RecipientIdentifier;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;

public class KeyTransRecipientInformation extends RecipientInformation {
    private KeyTransRecipientInfo _info;

    public KeyTransRecipientInformation(KeyTransRecipientInfo keyTransRecipientInfo, AlgorithmIdentifier algorithmIdentifier, InputStream inputStream) {
        super(algorithmIdentifier, AlgorithmIdentifier.getInstance(keyTransRecipientInfo.getKeyEncryptionAlgorithm()), inputStream);
        this._info = keyTransRecipientInfo;
        this._rid = new RecipientId();
        RecipientIdentifier recipientIdentifier = keyTransRecipientInfo.getRecipientIdentifier();
        try {
            if (recipientIdentifier.isTagged()) {
                this._rid.setSubjectKeyIdentifier(ASN1OctetString.getInstance(recipientIdentifier.getId()).getOctets());
                return;
            }
            IssuerAndSerialNumber instance = IssuerAndSerialNumber.getInstance(recipientIdentifier.getId());
            this._rid.setIssuer(instance.getName().getEncoded());
            this._rid.setSerialNumber(instance.getSerialNumber().getValue());
        } catch (IOException e) {
            throw new IllegalArgumentException("invalid rid in KeyTransRecipientInformation");
        }
    }

    private String getExchangeEncryptionAlgorithmName(DERObjectIdentifier dERObjectIdentifier) {
        return PKCSObjectIdentifiers.rsaEncryption.equals(dERObjectIdentifier) ? "RSA/ECB/PKCS1Padding" : dERObjectIdentifier.getId();
    }

    public CMSTypedStream getContentStream(Key key, String str) throws CMSException, NoSuchProviderException {
        Key secretKeySpec;
        byte[] octets = this._info.getEncryptedKey().getOctets();
        String exchangeEncryptionAlgorithmName = getExchangeEncryptionAlgorithmName(this._keyEncAlg.getObjectId());
        String symmetricCipherName = CMSEnvelopedHelper.INSTANCE.getSymmetricCipherName(this._encAlg.getObjectId().getId());
        try {
            Cipher symmetricCipher = CMSEnvelopedHelper.INSTANCE.getSymmetricCipher(exchangeEncryptionAlgorithmName, str);
            try {
                symmetricCipher.init(4, key);
                secretKeySpec = symmetricCipher.unwrap(octets, symmetricCipherName, 3);
            } catch (GeneralSecurityException e) {
                symmetricCipher.init(2, key);
                secretKeySpec = new SecretKeySpec(symmetricCipher.doFinal(octets), symmetricCipherName);
            } catch (IllegalStateException e2) {
                symmetricCipher.init(2, key);
                secretKeySpec = new SecretKeySpec(symmetricCipher.doFinal(octets), symmetricCipherName);
            } catch (UnsupportedOperationException e3) {
                symmetricCipher.init(2, key);
                secretKeySpec = new SecretKeySpec(symmetricCipher.doFinal(octets), symmetricCipherName);
            } catch (ProviderException e4) {
                symmetricCipher.init(2, key);
                secretKeySpec = new SecretKeySpec(symmetricCipher.doFinal(octets), symmetricCipherName);
            }
            return getContentFromSessionKey(secretKeySpec, str);
        } catch (NoSuchAlgorithmException e5) {
            throw new CMSException("can't find algorithm.", e5);
        } catch (InvalidKeyException e6) {
            throw new CMSException("key invalid in message.", e6);
        } catch (NoSuchPaddingException e7) {
            throw new CMSException("required padding not supported.", e7);
        } catch (IllegalBlockSizeException e8) {
            throw new CMSException("illegal blocksize in message.", e8);
        } catch (BadPaddingException e9) {
            throw new CMSException("bad padding in message.", e9);
        }
    }
}
