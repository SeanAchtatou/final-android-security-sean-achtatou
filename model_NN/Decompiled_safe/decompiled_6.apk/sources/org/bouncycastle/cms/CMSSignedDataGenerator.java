package org.bouncycastle.cms;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.BERConstructedOctetString;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.DERNull;
import org.bouncycastle.asn1.DERObject;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.asn1.cms.CMSAttributes;
import org.bouncycastle.asn1.cms.CMSObjectIdentifiers;
import org.bouncycastle.asn1.cms.ContentInfo;
import org.bouncycastle.asn1.cms.IssuerAndSerialNumber;
import org.bouncycastle.asn1.cms.SignedData;
import org.bouncycastle.asn1.cms.SignerIdentifier;
import org.bouncycastle.asn1.cms.SignerInfo;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.TBSCertificateStructure;

public class CMSSignedDataGenerator extends CMSSignedGenerator {
    List signerInfs = new ArrayList();

    static class DigOutputStream extends OutputStream {
        MessageDigest dig;

        public DigOutputStream(MessageDigest messageDigest) {
            this.dig = messageDigest;
        }

        public void write(int i) throws IOException {
            this.dig.update((byte) i);
        }

        public void write(byte[] bArr, int i, int i2) throws IOException {
            this.dig.update(bArr, i, i2);
        }
    }

    static class SigOutputStream extends OutputStream {
        Signature sig;

        public SigOutputStream(Signature signature) {
            this.sig = signature;
        }

        public void write(int i) throws IOException {
            try {
                this.sig.update((byte) i);
            } catch (SignatureException e) {
                throw new IOException("signature problem: " + e);
            }
        }

        public void write(byte[] bArr, int i, int i2) throws IOException {
            try {
                this.sig.update(bArr, i, i2);
            } catch (SignatureException e) {
                throw new IOException("signature problem: " + e);
            }
        }
    }

    private class SignerInf {
        AttributeTable baseSignedTable;
        X509Certificate cert;
        String digestOID;
        String encOID;
        PrivateKey key;
        CMSAttributeTableGenerator sAttr;
        CMSAttributeTableGenerator unsAttr;

        SignerInf(PrivateKey privateKey, X509Certificate x509Certificate, String str, String str2) {
            this.key = privateKey;
            this.cert = x509Certificate;
            this.digestOID = str;
            this.encOID = str2;
        }

        SignerInf(PrivateKey privateKey, X509Certificate x509Certificate, String str, String str2, CMSAttributeTableGenerator cMSAttributeTableGenerator, CMSAttributeTableGenerator cMSAttributeTableGenerator2, AttributeTable attributeTable) {
            this.key = privateKey;
            this.cert = x509Certificate;
            this.digestOID = str;
            this.encOID = str2;
            this.sAttr = cMSAttributeTableGenerator;
            this.unsAttr = cMSAttributeTableGenerator2;
            this.baseSignedTable = attributeTable;
        }

        /* access modifiers changed from: package-private */
        public X509Certificate getCertificate() {
            return this.cert;
        }

        /* access modifiers changed from: package-private */
        public String getDigestAlgOID() {
            return this.digestOID;
        }

        /* access modifiers changed from: package-private */
        public byte[] getDigestAlgParams() {
            return null;
        }

        /* access modifiers changed from: package-private */
        public String getEncryptionAlgOID() {
            return this.encOID;
        }

        /* access modifiers changed from: package-private */
        public PrivateKey getKey() {
            return this.key;
        }

        /* access modifiers changed from: package-private */
        public CMSAttributeTableGenerator getSignedAttributes() {
            return this.sAttr;
        }

        /* access modifiers changed from: package-private */
        public CMSAttributeTableGenerator getUnsignedAttributes() {
            return this.unsAttr;
        }

        /* access modifiers changed from: package-private */
        public SignerInfo toSignerInfo(DERObjectIdentifier dERObjectIdentifier, CMSProcessable cMSProcessable, SecureRandom secureRandom, String str, boolean z, boolean z2) throws IOException, SignatureException, InvalidKeyException, NoSuchProviderException, NoSuchAlgorithmException, CertificateEncodingException, CMSException {
            byte[] bArr;
            AttributeTable attributeTable;
            byte[] byteArray;
            AttributeTable attributeTable2 = null;
            AlgorithmIdentifier algorithmIdentifier = new AlgorithmIdentifier(new DERObjectIdentifier(getDigestAlgOID()), new DERNull());
            AlgorithmIdentifier encAlgorithmIdentifier = CMSSignedDataGenerator.this.getEncAlgorithmIdentifier(getEncryptionAlgOID());
            String digestAlgName = CMSSignedHelper.INSTANCE.getDigestAlgName(this.digestOID);
            Signature signatureInstance = CMSSignedHelper.INSTANCE.getSignatureInstance(digestAlgName + "with" + CMSSignedHelper.INSTANCE.getEncryptionAlgName(this.encOID), str);
            MessageDigest digestInstance = CMSSignedHelper.INSTANCE.getDigestInstance(digestAlgName, str);
            if (cMSProcessable != null) {
                cMSProcessable.write(new DigOutputStream(digestInstance));
                byte[] digest = digestInstance.digest();
                CMSSignedDataGenerator.this._digests.put(this.digestOID, digest.clone());
                bArr = digest;
            } else {
                bArr = null;
            }
            if (z) {
                attributeTable = this.sAttr != null ? this.sAttr.getAttributes(Collections.unmodifiableMap(CMSSignedDataGenerator.this.getBaseParameters(dERObjectIdentifier, algorithmIdentifier, bArr))) : null;
            } else {
                attributeTable = this.baseSignedTable;
            }
            if (z2) {
                Hashtable hashtable = attributeTable.toHashtable();
                hashtable.remove(CMSAttributes.contentType);
                attributeTable = new AttributeTable(hashtable);
            }
            ASN1Set attributeSet = CMSSignedDataGenerator.this.getAttributeSet(attributeTable);
            if (attributeSet != null) {
                byteArray = attributeSet.getEncoded("DER");
            } else {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                cMSProcessable.write(byteArrayOutputStream);
                byteArray = byteArrayOutputStream.toByteArray();
            }
            signatureInstance.initSign(this.key, secureRandom);
            signatureInstance.update(byteArray);
            DEROctetString dEROctetString = new DEROctetString(signatureInstance.sign());
            Map baseParameters = CMSSignedDataGenerator.this.getBaseParameters(dERObjectIdentifier, algorithmIdentifier, bArr);
            baseParameters.put(CMSAttributeTableGenerator.SIGNATURE, dEROctetString.getOctets().clone());
            if (this.unsAttr != null) {
                attributeTable2 = this.unsAttr.getAttributes(Collections.unmodifiableMap(baseParameters));
            }
            ASN1Set attributeSet2 = CMSSignedDataGenerator.this.getAttributeSet(attributeTable2);
            TBSCertificateStructure instance = TBSCertificateStructure.getInstance(new ASN1InputStream(new ByteArrayInputStream(getCertificate().getTBSCertificate())).readObject());
            return new SignerInfo(new SignerIdentifier(new IssuerAndSerialNumber(instance.getIssuer(), instance.getSerialNumber().getValue())), algorithmIdentifier, attributeSet, encAlgorithmIdentifier, dEROctetString, attributeSet2);
        }
    }

    public CMSSignedDataGenerator() {
    }

    public CMSSignedDataGenerator(SecureRandom secureRandom) {
        super(secureRandom);
    }

    private AlgorithmIdentifier makeAlgId(String str, byte[] bArr) throws IOException {
        return bArr != null ? new AlgorithmIdentifier(new DERObjectIdentifier(str), makeObj(bArr)) : new AlgorithmIdentifier(new DERObjectIdentifier(str), new DERNull());
    }

    private DERObject makeObj(byte[] bArr) throws IOException {
        if (bArr == null) {
            return null;
        }
        return new ASN1InputStream(new ByteArrayInputStream(bArr)).readObject();
    }

    public void addSigner(PrivateKey privateKey, X509Certificate x509Certificate, String str) throws IllegalArgumentException {
        PrivateKey privateKey2 = privateKey;
        X509Certificate x509Certificate2 = x509Certificate;
        String str2 = str;
        this.signerInfs.add(new SignerInf(privateKey2, x509Certificate2, str2, getEncOID(privateKey, str), new DefaultSignedAttributeTableGenerator(), null, null));
    }

    public void addSigner(PrivateKey privateKey, X509Certificate x509Certificate, String str, AttributeTable attributeTable, AttributeTable attributeTable2) throws IllegalArgumentException {
        PrivateKey privateKey2 = privateKey;
        X509Certificate x509Certificate2 = x509Certificate;
        String str2 = str;
        this.signerInfs.add(new SignerInf(privateKey2, x509Certificate2, str2, getEncOID(privateKey, str), new DefaultSignedAttributeTableGenerator(attributeTable), new SimpleAttributeTableGenerator(attributeTable2), attributeTable));
    }

    public void addSigner(PrivateKey privateKey, X509Certificate x509Certificate, String str, CMSAttributeTableGenerator cMSAttributeTableGenerator, CMSAttributeTableGenerator cMSAttributeTableGenerator2) throws IllegalArgumentException {
        this.signerInfs.add(new SignerInf(privateKey, x509Certificate, str, getEncOID(privateKey, str), cMSAttributeTableGenerator, cMSAttributeTableGenerator2, null));
    }

    public CMSSignedData generate(String str, CMSProcessable cMSProcessable, boolean z, String str2) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        return generate(str, cMSProcessable, z, str2, true);
    }

    public CMSSignedData generate(String str, CMSProcessable cMSProcessable, boolean z, String str2, boolean z2) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        DERObjectIdentifier dERObjectIdentifier;
        boolean z3;
        ContentInfo contentInfo;
        ASN1EncodableVector aSN1EncodableVector = new ASN1EncodableVector();
        ASN1EncodableVector aSN1EncodableVector2 = new ASN1EncodableVector();
        this._digests.clear();
        for (SignerInformation signerInformation : this._signers) {
            try {
                aSN1EncodableVector.add(makeAlgId(signerInformation.getDigestAlgOID(), signerInformation.getDigestAlgParams()));
                aSN1EncodableVector2.add(signerInformation.toSignerInfo());
            } catch (IOException e) {
                throw new CMSException("encoding error.", e);
            }
        }
        if (str != null) {
            dERObjectIdentifier = new DERObjectIdentifier(str);
            z3 = false;
        } else {
            dERObjectIdentifier = CMSObjectIdentifiers.data;
            z3 = true;
        }
        for (SignerInf signerInf : this.signerInfs) {
            try {
                aSN1EncodableVector.add(makeAlgId(signerInf.getDigestAlgOID(), signerInf.getDigestAlgParams()));
                aSN1EncodableVector2.add(signerInf.toSignerInfo(dERObjectIdentifier, cMSProcessable, this.rand, str2, z2, z3));
            } catch (IOException e2) {
                throw new CMSException("encoding error.", e2);
            } catch (InvalidKeyException e3) {
                throw new CMSException("key inappropriate for signature.", e3);
            } catch (SignatureException e4) {
                throw new CMSException("error creating signature.", e4);
            } catch (CertificateEncodingException e5) {
                throw new CMSException("error creating sid.", e5);
            }
        }
        ASN1Set aSN1Set = null;
        if (this._certs.size() != 0) {
            aSN1Set = CMSUtils.createBerSetFromList(this._certs);
        }
        ASN1Set aSN1Set2 = null;
        if (this._crls.size() != 0) {
            aSN1Set2 = CMSUtils.createBerSetFromList(this._crls);
        }
        if (z) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                cMSProcessable.write(byteArrayOutputStream);
                contentInfo = new ContentInfo(dERObjectIdentifier, new BERConstructedOctetString(byteArrayOutputStream.toByteArray()));
            } catch (IOException e6) {
                throw new CMSException("encapsulation error.", e6);
            }
        } else {
            contentInfo = new ContentInfo(dERObjectIdentifier, (DEREncodable) null);
        }
        return new CMSSignedData(cMSProcessable, new ContentInfo(PKCSObjectIdentifiers.signedData, new SignedData(new DERSet(aSN1EncodableVector), contentInfo, aSN1Set, aSN1Set2, new DERSet(aSN1EncodableVector2))));
    }

    public CMSSignedData generate(CMSProcessable cMSProcessable, String str) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        return generate(cMSProcessable, false, str);
    }

    public CMSSignedData generate(CMSProcessable cMSProcessable, boolean z, String str) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        return generate(DATA, cMSProcessable, z, str);
    }

    public SignerInformationStore generateCounterSigners(SignerInformation signerInformation, String str) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
        return generate(null, new CMSProcessableByteArray(signerInformation.getSignature()), false, str).getSignerInfos();
    }
}
