package org.bouncycastle.cms;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

class CounterSignatureDigestCalculator implements DigestCalculator {
    private final String alg;
    private final byte[] data;
    private final String provider;

    CounterSignatureDigestCalculator(String str, String str2, byte[] bArr) {
        this.alg = str;
        this.provider = str2;
        this.data = bArr;
    }

    public byte[] getDigest() throws NoSuchProviderException, NoSuchAlgorithmException {
        return CMSSignedHelper.INSTANCE.getDigestInstance(this.alg, this.provider).digest(this.data);
    }
}
