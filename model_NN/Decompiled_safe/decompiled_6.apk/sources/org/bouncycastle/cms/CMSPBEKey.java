package org.bouncycastle.cms;

import javax.crypto.interfaces.PBEKey;

public abstract class CMSPBEKey implements PBEKey {
    private int iterationCount;
    private char[] password;
    private byte[] salt;

    public CMSPBEKey(char[] cArr, byte[] bArr, int i) {
        this.password = cArr;
        this.salt = bArr;
        this.iterationCount = i;
    }

    public String getAlgorithm() {
        return "PKCS5S2";
    }

    public byte[] getEncoded() {
        return null;
    }

    /* access modifiers changed from: package-private */
    public abstract byte[] getEncoded(String str);

    public String getFormat() {
        return "RAW";
    }

    public int getIterationCount() {
        return this.iterationCount;
    }

    public char[] getPassword() {
        return this.password;
    }

    public byte[] getSalt() {
        return this.salt;
    }
}
