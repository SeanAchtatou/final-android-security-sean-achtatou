package org.bouncycastle.cms;

import java.security.cert.X509CertSelector;
import org.bouncycastle.util.Arrays;

public class SignerId extends X509CertSelector {
    private boolean equalsObj(Object obj, Object obj2) {
        return obj != null ? obj.equals(obj2) : obj2 == null;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof SignerId)) {
            return false;
        }
        SignerId signerId = (SignerId) obj;
        return Arrays.areEqual(getSubjectKeyIdentifier(), signerId.getSubjectKeyIdentifier()) && equalsObj(getSerialNumber(), signerId.getSerialNumber()) && equalsObj(getIssuerAsString(), signerId.getIssuerAsString());
    }

    public int hashCode() {
        int hashCode = Arrays.hashCode(getSubjectKeyIdentifier());
        if (getSerialNumber() != null) {
            hashCode ^= getSerialNumber().hashCode();
        }
        return getIssuerAsString() != null ? hashCode ^ getIssuerAsString().hashCode() : hashCode;
    }
}
