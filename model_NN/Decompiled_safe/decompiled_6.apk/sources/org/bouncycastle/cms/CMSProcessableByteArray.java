package org.bouncycastle.cms;

import java.io.IOException;
import java.io.OutputStream;

public class CMSProcessableByteArray implements CMSProcessable {
    private byte[] bytes;

    public CMSProcessableByteArray(byte[] bArr) {
        this.bytes = bArr;
    }

    public Object getContent() {
        return this.bytes.clone();
    }

    public void write(OutputStream outputStream) throws IOException, CMSException {
        outputStream.write(this.bytes);
    }
}
