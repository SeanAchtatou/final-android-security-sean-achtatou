package org.bouncycastle.cms;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.AlgorithmParameters;
import java.security.NoSuchProviderException;
import java.util.ArrayList;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1SequenceParser;
import org.bouncycastle.asn1.ASN1SetParser;
import org.bouncycastle.asn1.DEREncodable;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.asn1.cms.EncryptedContentInfoParser;
import org.bouncycastle.asn1.cms.EnvelopedDataParser;
import org.bouncycastle.asn1.cms.KEKRecipientInfo;
import org.bouncycastle.asn1.cms.KeyAgreeRecipientInfo;
import org.bouncycastle.asn1.cms.KeyTransRecipientInfo;
import org.bouncycastle.asn1.cms.PasswordRecipientInfo;
import org.bouncycastle.asn1.cms.RecipientInfo;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;

public class CMSEnvelopedDataParser extends CMSContentInfoParser {
    private boolean _attrNotRead;
    private AlgorithmIdentifier _encAlg;
    EnvelopedDataParser _envelopedData;
    RecipientInformationStore _recipientInfoStore;
    private AttributeTable _unprotectedAttributes;

    public CMSEnvelopedDataParser(InputStream inputStream) throws CMSException, IOException {
        super(inputStream);
        this._attrNotRead = true;
        this._envelopedData = new EnvelopedDataParser(this._contentInfo.getContent(16));
        ASN1SetParser recipientInfos = this._envelopedData.getRecipientInfos();
        ArrayList<RecipientInfo> arrayList = new ArrayList<>();
        while (true) {
            DEREncodable readObject = recipientInfos.readObject();
            if (readObject == null) {
                break;
            }
            arrayList.add(RecipientInfo.getInstance(readObject.getDERObject()));
        }
        EncryptedContentInfoParser encryptedContentInfo = this._envelopedData.getEncryptedContentInfo();
        this._encAlg = encryptedContentInfo.getContentEncryptionAlgorithm();
        ArrayList arrayList2 = new ArrayList();
        InputStream octetStream = encryptedContentInfo.getEncryptedContent(4).getOctetStream();
        for (RecipientInfo info : arrayList) {
            KeyTransRecipientInfo info2 = info.getInfo();
            if (info2 instanceof KeyTransRecipientInfo) {
                arrayList2.add(new KeyTransRecipientInformation(info2, this._encAlg, octetStream));
            } else if (info2 instanceof KEKRecipientInfo) {
                arrayList2.add(new KEKRecipientInformation((KEKRecipientInfo) info2, this._encAlg, octetStream));
            } else if (info2 instanceof KeyAgreeRecipientInfo) {
                arrayList2.add(new KeyAgreeRecipientInformation((KeyAgreeRecipientInfo) info2, this._encAlg, octetStream));
            } else if (info2 instanceof PasswordRecipientInfo) {
                arrayList2.add(new PasswordRecipientInformation((PasswordRecipientInfo) info2, this._encAlg, octetStream));
            }
        }
        this._recipientInfoStore = new RecipientInformationStore(arrayList2);
    }

    public CMSEnvelopedDataParser(byte[] bArr) throws CMSException, IOException {
        this(new ByteArrayInputStream(bArr));
    }

    private byte[] encodeObj(DEREncodable dEREncodable) throws IOException {
        if (dEREncodable != null) {
            return dEREncodable.getDERObject().getEncoded();
        }
        return null;
    }

    public String getEncryptionAlgOID() {
        return this._encAlg.getObjectId().toString();
    }

    public byte[] getEncryptionAlgParams() {
        try {
            return encodeObj(this._encAlg.getParameters());
        } catch (Exception e) {
            throw new RuntimeException("exception getting encryption parameters " + e);
        }
    }

    public AlgorithmParameters getEncryptionAlgorithmParameters(String str) throws CMSException, NoSuchProviderException {
        return CMSEnvelopedHelper.INSTANCE.getEncryptionAlgorithmParameters(getEncryptionAlgOID(), getEncryptionAlgParams(), str);
    }

    public RecipientInformationStore getRecipientInfos() {
        return this._recipientInfoStore;
    }

    public AttributeTable getUnprotectedAttributes() throws IOException {
        if (this._unprotectedAttributes == null && this._attrNotRead) {
            ASN1SetParser unprotectedAttrs = this._envelopedData.getUnprotectedAttrs();
            this._attrNotRead = false;
            if (unprotectedAttrs != null) {
                ASN1EncodableVector aSN1EncodableVector = new ASN1EncodableVector();
                while (true) {
                    ASN1SequenceParser readObject = unprotectedAttrs.readObject();
                    if (readObject == null) {
                        break;
                    }
                    aSN1EncodableVector.add(readObject.getDERObject());
                }
                this._unprotectedAttributes = new AttributeTable(new DERSet(aSN1EncodableVector));
            }
        }
        return this._unprotectedAttributes;
    }
}
