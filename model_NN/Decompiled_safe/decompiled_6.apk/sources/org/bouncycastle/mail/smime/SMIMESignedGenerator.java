package org.bouncycastle.mail.smime;

import java.io.IOException;
import java.io.OutputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.cert.CertStore;
import java.security.cert.CertStoreException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.activation.CommandMap;
import javax.activation.MailcapCommandMap;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.ContentType;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.apache.http.entity.mime.MIME;
import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.asn1.cryptopro.CryptoProObjectIdentifiers;
import org.bouncycastle.asn1.nist.NISTObjectIdentifiers;
import org.bouncycastle.asn1.oiw.OIWObjectIdentifiers;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.teletrust.TeleTrusTObjectIdentifiers;
import org.bouncycastle.asn1.x9.X9ObjectIdentifiers;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSSignedDataStreamGenerator;
import org.bouncycastle.cms.SignerInformationStore;
import org.bouncycastle.mail.smime.SMIMEUtil;
import org.bouncycastle.mail.smime.util.CRLFOutputStream;
import org.bouncycastle.x509.X509Store;

public class SMIMESignedGenerator extends SMIMEGenerator {
    private static final String CERTIFICATE_MANAGEMENT_CONTENT = "application/pkcs7-mime; name=smime.p7c; smime-type=certs-only";
    private static final String DETACHED_SIGNATURE_TYPE = "application/pkcs7-signature; name=smime.p7s; smime-type=signed-data";
    public static final String DIGEST_GOST3411 = CryptoProObjectIdentifiers.gostR3411.getId();
    public static final String DIGEST_MD5 = PKCSObjectIdentifiers.md5.getId();
    public static final String DIGEST_RIPEMD128 = TeleTrusTObjectIdentifiers.ripemd128.getId();
    public static final String DIGEST_RIPEMD160 = TeleTrusTObjectIdentifiers.ripemd160.getId();
    public static final String DIGEST_RIPEMD256 = TeleTrusTObjectIdentifiers.ripemd256.getId();
    public static final String DIGEST_SHA1 = OIWObjectIdentifiers.idSHA1.getId();
    public static final String DIGEST_SHA224 = NISTObjectIdentifiers.id_sha224.getId();
    public static final String DIGEST_SHA256 = NISTObjectIdentifiers.id_sha256.getId();
    public static final String DIGEST_SHA384 = NISTObjectIdentifiers.id_sha384.getId();
    public static final String DIGEST_SHA512 = NISTObjectIdentifiers.id_sha512.getId();
    private static final String ENCAPSULATED_SIGNED_CONTENT_TYPE = "application/pkcs7-mime; name=smime.p7m; smime-type=signed-data";
    public static final String ENCRYPTION_DSA = X9ObjectIdentifiers.id_dsa_with_sha1.getId();
    public static final String ENCRYPTION_ECDSA = X9ObjectIdentifiers.ecdsa_with_SHA1.getId();
    public static final String ENCRYPTION_ECGOST3410 = CryptoProObjectIdentifiers.gostR3410_2001.getId();
    public static final String ENCRYPTION_GOST3410 = CryptoProObjectIdentifiers.gostR3410_94.getId();
    public static final String ENCRYPTION_RSA = PKCSObjectIdentifiers.rsaEncryption.getId();
    public static final String ENCRYPTION_RSA_PSS = PKCSObjectIdentifiers.id_RSASSA_PSS.getId();
    /* access modifiers changed from: private */
    public List _attributeCerts;
    /* access modifiers changed from: private */
    public List _certStores;
    /* access modifiers changed from: private */
    public final String _defaultContentTransferEncoding;
    /* access modifiers changed from: private */
    public Map _digests;
    /* access modifiers changed from: private */
    public List _oldSigners;
    /* access modifiers changed from: private */
    public List _signers;

    private class ContentSigner implements SMIMEStreamingProcessor {
        private final MimeBodyPart _content;
        private final boolean _encapsulate;
        private final String _provider;

        ContentSigner(MimeBodyPart mimeBodyPart, boolean z, String str) {
            this._content = mimeBodyPart;
            this._encapsulate = z;
            this._provider = str;
        }

        private void writeBodyPart(OutputStream outputStream, MimeBodyPart mimeBodyPart) throws IOException, MessagingException {
            if (mimeBodyPart.getContent() instanceof Multipart) {
                Multipart multipart = (Multipart) mimeBodyPart.getContent();
                String str = "--" + new ContentType(multipart.getContentType()).getParameter("boundary");
                SMIMEUtil.LineOutputStream lineOutputStream = new SMIMEUtil.LineOutputStream(outputStream);
                Enumeration allHeaderLines = mimeBodyPart.getAllHeaderLines();
                while (allHeaderLines.hasMoreElements()) {
                    lineOutputStream.writeln((String) allHeaderLines.nextElement());
                }
                lineOutputStream.writeln();
                SMIMEUtil.outputPreamble(lineOutputStream, mimeBodyPart, str);
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 < multipart.getCount()) {
                        lineOutputStream.writeln(str);
                        writeBodyPart(outputStream, multipart.getBodyPart(i2));
                        lineOutputStream.writeln();
                        i = i2 + 1;
                    } else {
                        lineOutputStream.writeln(str + "--");
                        return;
                    }
                }
            } else {
                if (SMIMEUtil.isCanonicalisationRequired(mimeBodyPart, SMIMESignedGenerator.this._defaultContentTransferEncoding)) {
                    outputStream = new CRLFOutputStream(outputStream);
                }
                mimeBodyPart.writeTo(outputStream);
            }
        }

        /* access modifiers changed from: protected */
        public CMSSignedDataStreamGenerator getGenerator() throws CMSException, CertStoreException, InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException {
            CMSSignedDataStreamGenerator cMSSignedDataStreamGenerator = new CMSSignedDataStreamGenerator();
            for (CertStore addCertificatesAndCRLs : SMIMESignedGenerator.this._certStores) {
                cMSSignedDataStreamGenerator.addCertificatesAndCRLs(addCertificatesAndCRLs);
            }
            for (X509Store addAttributeCertificates : SMIMESignedGenerator.this._attributeCerts) {
                cMSSignedDataStreamGenerator.addAttributeCertificates(addAttributeCertificates);
            }
            for (Signer signer : SMIMESignedGenerator.this._signers) {
                cMSSignedDataStreamGenerator.addSigner(signer.getKey(), signer.getCert(), signer.getDigestOID(), signer.getSignedAttr(), signer.getUnsignedAttr(), this._provider);
            }
            cMSSignedDataStreamGenerator.addSigners(new SignerInformationStore(SMIMESignedGenerator.this._oldSigners));
            return cMSSignedDataStreamGenerator;
        }

        public void write(OutputStream outputStream) throws IOException {
            try {
                CMSSignedDataStreamGenerator generator = getGenerator();
                OutputStream open = generator.open(outputStream, this._encapsulate);
                if (this._content != null) {
                    if (!this._encapsulate) {
                        writeBodyPart(open, this._content);
                    } else {
                        this._content.writeTo(open);
                    }
                }
                open.close();
                Map unused = SMIMESignedGenerator.this._digests = generator.getGeneratedDigests();
            } catch (MessagingException e) {
                throw new IOException(e.toString());
            } catch (NoSuchAlgorithmException e2) {
                throw new IOException(e2.toString());
            } catch (NoSuchProviderException e3) {
                throw new IOException(e3.toString());
            } catch (CMSException e4) {
                throw new IOException(e4.toString());
            } catch (InvalidKeyException e5) {
                throw new IOException(e5.toString());
            } catch (CertStoreException e6) {
                throw new IOException(e6.toString());
            }
        }
    }

    private class Signer {
        final X509Certificate cert;
        final String digestOID;
        final PrivateKey key;
        final AttributeTable signedAttr;
        final AttributeTable unsignedAttr;

        Signer(PrivateKey privateKey, X509Certificate x509Certificate, String str, AttributeTable attributeTable, AttributeTable attributeTable2) {
            this.key = privateKey;
            this.cert = x509Certificate;
            this.digestOID = str;
            this.signedAttr = attributeTable;
            this.unsignedAttr = attributeTable2;
        }

        public X509Certificate getCert() {
            return this.cert;
        }

        public String getDigestOID() {
            return this.digestOID;
        }

        public PrivateKey getKey() {
            return this.key;
        }

        public AttributeTable getSignedAttr() {
            return this.signedAttr;
        }

        public AttributeTable getUnsignedAttr() {
            return this.unsignedAttr;
        }
    }

    static {
        MailcapCommandMap defaultCommandMap = CommandMap.getDefaultCommandMap();
        defaultCommandMap.addMailcap("application/pkcs7-signature;; x-java-content-handler=org.bouncycastle.mail.smime.handlers.pkcs7_signature");
        defaultCommandMap.addMailcap("application/pkcs7-mime;; x-java-content-handler=org.bouncycastle.mail.smime.handlers.pkcs7_mime");
        defaultCommandMap.addMailcap("application/x-pkcs7-signature;; x-java-content-handler=org.bouncycastle.mail.smime.handlers.x_pkcs7_signature");
        defaultCommandMap.addMailcap("application/x-pkcs7-mime;; x-java-content-handler=org.bouncycastle.mail.smime.handlers.x_pkcs7_mime");
        defaultCommandMap.addMailcap("multipart/signed;; x-java-content-handler=org.bouncycastle.mail.smime.handlers.multipart_signed");
        CommandMap.setDefaultCommandMap(defaultCommandMap);
    }

    public SMIMESignedGenerator() {
        this._certStores = new ArrayList();
        this._signers = new ArrayList();
        this._oldSigners = new ArrayList();
        this._attributeCerts = new ArrayList();
        this._digests = new HashMap();
        this._defaultContentTransferEncoding = "7bit";
    }

    public SMIMESignedGenerator(String str) {
        this._certStores = new ArrayList();
        this._signers = new ArrayList();
        this._oldSigners = new ArrayList();
        this._attributeCerts = new ArrayList();
        this._digests = new HashMap();
        this._defaultContentTransferEncoding = str;
    }

    private void addHashHeader(StringBuffer stringBuffer, List list) {
        int i = 0;
        Iterator it = list.iterator();
        HashSet<String> hashSet = new HashSet<>();
        while (it.hasNext()) {
            Signer signer = (Signer) it.next();
            if (signer.getDigestOID().equals(DIGEST_SHA1)) {
                hashSet.add("sha1");
            } else if (signer.getDigestOID().equals(DIGEST_MD5)) {
                hashSet.add("md5");
            } else if (signer.getDigestOID().equals(DIGEST_SHA224)) {
                hashSet.add("sha224");
            } else if (signer.getDigestOID().equals(DIGEST_SHA256)) {
                hashSet.add("sha256");
            } else if (signer.getDigestOID().equals(DIGEST_SHA384)) {
                hashSet.add("sha384");
            } else if (signer.getDigestOID().equals(DIGEST_SHA512)) {
                hashSet.add("sha512");
            } else if (signer.getDigestOID().equals(DIGEST_GOST3411)) {
                hashSet.add("gostr3411-94");
            } else {
                hashSet.add("unknown");
            }
        }
        for (String str : hashSet) {
            if (i != 0) {
                stringBuffer.append(',');
            } else if (hashSet.size() != 1) {
                stringBuffer.append("; micalg=\"");
            } else {
                stringBuffer.append("; micalg=");
            }
            stringBuffer.append(str);
            i++;
        }
        if (i != 0 && hashSet.size() != 1) {
            stringBuffer.append('\"');
        }
    }

    private MimeMultipart make(MimeBodyPart mimeBodyPart, String str) throws NoSuchAlgorithmException, NoSuchProviderException, SMIMEException {
        try {
            MimeBodyPart mimeBodyPart2 = new MimeBodyPart();
            mimeBodyPart2.setContent(new ContentSigner(mimeBodyPart, false, str), DETACHED_SIGNATURE_TYPE);
            mimeBodyPart2.addHeader("Content-Type", DETACHED_SIGNATURE_TYPE);
            mimeBodyPart2.addHeader(MIME.CONTENT_DISPOSITION, "attachment; filename=\"smime.p7s\"");
            mimeBodyPart2.addHeader("Content-Description", "S/MIME Cryptographic Signature");
            mimeBodyPart2.addHeader(MIME.CONTENT_TRANSFER_ENC, this.encoding);
            StringBuffer stringBuffer = new StringBuffer("signed; protocol=\"application/pkcs7-signature\"");
            addHashHeader(stringBuffer, this._signers);
            MimeMultipart mimeMultipart = new MimeMultipart(stringBuffer.toString());
            mimeMultipart.addBodyPart(mimeBodyPart);
            mimeMultipart.addBodyPart(mimeBodyPart2);
            return mimeMultipart;
        } catch (MessagingException e) {
            throw new SMIMEException("exception putting multi-part together.", e);
        }
    }

    private MimeBodyPart makeEncapsulated(MimeBodyPart mimeBodyPart, String str) throws NoSuchAlgorithmException, NoSuchProviderException, SMIMEException {
        try {
            MimeBodyPart mimeBodyPart2 = new MimeBodyPart();
            mimeBodyPart2.setContent(new ContentSigner(mimeBodyPart, true, str), ENCAPSULATED_SIGNED_CONTENT_TYPE);
            mimeBodyPart2.addHeader("Content-Type", ENCAPSULATED_SIGNED_CONTENT_TYPE);
            mimeBodyPart2.addHeader(MIME.CONTENT_DISPOSITION, "attachment; filename=\"smime.p7m\"");
            mimeBodyPart2.addHeader("Content-Description", "S/MIME Cryptographic Signed Data");
            mimeBodyPart2.addHeader(MIME.CONTENT_TRANSFER_ENC, this.encoding);
            return mimeBodyPart2;
        } catch (MessagingException e) {
            throw new SMIMEException("exception putting body part together.", e);
        }
    }

    public void addAttributeCertificates(X509Store x509Store) throws CMSException {
        this._attributeCerts.add(x509Store);
    }

    public void addCertificatesAndCRLs(CertStore certStore) throws CertStoreException, SMIMEException {
        this._certStores.add(certStore);
    }

    public void addSigner(PrivateKey privateKey, X509Certificate x509Certificate, String str) throws IllegalArgumentException {
        this._signers.add(new Signer(privateKey, x509Certificate, str, null, null));
    }

    public void addSigner(PrivateKey privateKey, X509Certificate x509Certificate, String str, AttributeTable attributeTable, AttributeTable attributeTable2) throws IllegalArgumentException {
        this._signers.add(new Signer(privateKey, x509Certificate, str, attributeTable, attributeTable2));
    }

    public void addSigners(SignerInformationStore signerInformationStore) {
        for (Object add : signerInformationStore.getSigners()) {
            this._oldSigners.add(add);
        }
    }

    public MimeMultipart generate(MimeBodyPart mimeBodyPart, String str) throws NoSuchAlgorithmException, NoSuchProviderException, SMIMEException {
        return make(makeContentBodyPart(mimeBodyPart), str);
    }

    public MimeMultipart generate(MimeMessage mimeMessage, String str) throws NoSuchAlgorithmException, NoSuchProviderException, SMIMEException {
        try {
            mimeMessage.saveChanges();
            return make(makeContentBodyPart(mimeMessage), str);
        } catch (MessagingException e) {
            throw new SMIMEException("unable to save message", e);
        }
    }

    public MimeBodyPart generateCertificateManagement(String str) throws SMIMEException, NoSuchProviderException {
        try {
            MimeBodyPart mimeBodyPart = new MimeBodyPart();
            mimeBodyPart.setContent(new ContentSigner(null, true, str), CERTIFICATE_MANAGEMENT_CONTENT);
            mimeBodyPart.addHeader("Content-Type", CERTIFICATE_MANAGEMENT_CONTENT);
            mimeBodyPart.addHeader(MIME.CONTENT_DISPOSITION, "attachment; filename=\"smime.p7c\"");
            mimeBodyPart.addHeader("Content-Description", "S/MIME Certificate Management Message");
            mimeBodyPart.addHeader(MIME.CONTENT_TRANSFER_ENC, this.encoding);
            return mimeBodyPart;
        } catch (MessagingException e) {
            throw new SMIMEException("exception putting body part together.", e);
        }
    }

    public MimeBodyPart generateEncapsulated(MimeBodyPart mimeBodyPart, String str) throws NoSuchAlgorithmException, NoSuchProviderException, SMIMEException {
        return makeEncapsulated(makeContentBodyPart(mimeBodyPart), str);
    }

    public MimeBodyPart generateEncapsulated(MimeMessage mimeMessage, String str) throws NoSuchAlgorithmException, NoSuchProviderException, SMIMEException {
        try {
            mimeMessage.saveChanges();
            return makeEncapsulated(makeContentBodyPart(mimeMessage), str);
        } catch (MessagingException e) {
            throw new SMIMEException("unable to save message", e);
        }
    }

    public Map getGeneratedDigests() {
        return new HashMap(this._digests);
    }
}
