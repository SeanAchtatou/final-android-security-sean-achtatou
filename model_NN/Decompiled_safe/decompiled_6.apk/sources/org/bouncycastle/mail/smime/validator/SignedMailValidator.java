package org.bouncycastle.mail.smime.validator;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.PublicKey;
import java.security.cert.CertPath;
import java.security.cert.CertStore;
import java.security.cert.CertStoreException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateFactory;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.PKIXParameters;
import java.security.cert.TrustAnchor;
import java.security.cert.X509CertSelector;
import java.security.cert.X509Certificate;
import java.security.interfaces.DSAPublicKey;
import java.security.interfaces.RSAPublicKey;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import javax.mail.Part;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1TaggedObject;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.DERObject;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.cms.Attribute;
import org.bouncycastle.asn1.cms.AttributeTable;
import org.bouncycastle.asn1.cms.CMSAttributes;
import org.bouncycastle.asn1.cms.Time;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x509.AuthorityKeyIdentifier;
import org.bouncycastle.asn1.x509.ExtendedKeyUsage;
import org.bouncycastle.asn1.x509.KeyPurposeId;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.cms.SignerInformationStore;
import org.bouncycastle.i18n.ErrorBundle;
import org.bouncycastle.i18n.filter.TrustedInput;
import org.bouncycastle.i18n.filter.UntrustedInput;
import org.bouncycastle.jce.PrincipalUtil;
import org.bouncycastle.jce.X509Principal;
import org.bouncycastle.mail.smime.SMIMESigned;
import org.bouncycastle.x509.CertPathReviewerException;
import org.bouncycastle.x509.PKIXCertPathReviewer;

public class SignedMailValidator {
    private static final Class DEFAULT_CERT_PATH_REVIEWER = PKIXCertPathReviewer.class;
    private static final String EXT_KEY_USAGE = X509Extensions.ExtendedKeyUsage.getId();
    private static final String RESOURCE_NAME = "org.bouncycastle.mail.smime.validator.SignedMailValidatorMessages";
    private static final String SUBJECT_ALTERNATIVE_NAME = X509Extensions.SubjectAlternativeName.getId();
    private static final long THIRTY_YEARS_IN_MILLI_SEC = 946728000000L;
    private static final int shortKeyLength = 512;
    private Class certPathReviewerClass;
    private CertStore certs;
    private String[] fromAddresses;
    private Map results;
    private SignerInformationStore signers;

    public class ValidationResult {
        private List errors;
        private List notifications;
        private PKIXCertPathReviewer review;
        private boolean signVerified;
        private List userProvidedCerts;

        ValidationResult(PKIXCertPathReviewer pKIXCertPathReviewer, boolean z, List list, List list2, List list3) {
            this.review = pKIXCertPathReviewer;
            this.errors = list;
            this.notifications = list2;
            this.signVerified = z;
            this.userProvidedCerts = list3;
        }

        public CertPath getCertPath() {
            if (this.review != null) {
                return this.review.getCertPath();
            }
            return null;
        }

        public PKIXCertPathReviewer getCertPathReview() {
            return this.review;
        }

        public List getErrors() {
            return this.errors;
        }

        public List getNotifications() {
            return this.notifications;
        }

        public List getUserProvidedCerts() {
            return this.userProvidedCerts;
        }

        public boolean isValidSignature() {
            return this.review != null && this.signVerified && this.review.isValidCertPath() && this.errors.isEmpty();
        }

        public boolean isVerifiedSignature() {
            return this.signVerified;
        }
    }

    public SignedMailValidator(MimeMessage mimeMessage, PKIXParameters pKIXParameters) throws SignedMailValidatorException {
        this(mimeMessage, pKIXParameters, DEFAULT_CERT_PATH_REVIEWER);
    }

    /* JADX WARN: Type inference failed for: r2v3, types: [java.lang.Throwable, org.bouncycastle.mail.smime.validator.SignedMailValidatorException] */
    /* JADX WARN: Type inference failed for: r0v4, types: [java.lang.Throwable, org.bouncycastle.mail.smime.validator.SignedMailValidatorException] */
    /* JADX WARN: Type inference failed for: r1v3, types: [java.lang.Throwable, org.bouncycastle.mail.smime.validator.SignedMailValidatorException] */
    public SignedMailValidator(MimeMessage mimeMessage, PKIXParameters pKIXParameters, Class cls) throws SignedMailValidatorException {
        SMIMESigned sMIMESigned;
        this.certPathReviewerClass = cls;
        if (!DEFAULT_CERT_PATH_REVIEWER.isAssignableFrom(cls)) {
            throw new IllegalArgumentException("certPathReviewerClass is not a subclass of " + DEFAULT_CERT_PATH_REVIEWER.getName());
        }
        try {
            if (mimeMessage.isMimeType("multipart/signed")) {
                sMIMESigned = new SMIMESigned((MimeMultipart) mimeMessage.getContent());
            } else if (mimeMessage.isMimeType("application/pkcs7-mime") || mimeMessage.isMimeType("application/x-pkcs7-mime")) {
                sMIMESigned = new SMIMESigned((Part) mimeMessage);
            } else {
                throw new SignedMailValidatorException(new ErrorBundle(RESOURCE_NAME, "SignedMailValidator.noSignedMessage"));
            }
            this.certs = sMIMESigned.getCertificatesAndCRLs("Collection", "BC");
            this.signers = sMIMESigned.getSignerInfos();
            InternetAddress[] from = mimeMessage.getFrom();
            this.fromAddresses = new String[from.length];
            for (int i = 0; i < from.length; i++) {
                this.fromAddresses[i] = from[i].getAddress();
            }
            this.results = new HashMap();
            validateSignatures(pKIXParameters);
        } catch (Exception e) {
            if (e instanceof SignedMailValidatorException) {
                throw ((SignedMailValidatorException) e);
            }
            throw new SignedMailValidatorException(new ErrorBundle(RESOURCE_NAME, "SignedMailValidator.exceptionReadingMessage", new Object[]{e.getMessage(), e, e.getClass().getName()}), e);
        }
    }

    static String addressesToString(Object[] objArr) {
        if (objArr == null) {
            return "null";
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append('[');
        for (int i = 0; i != objArr.length; i++) {
            stringBuffer.append(String.valueOf(objArr[i]));
            if (i == objArr.length - 1) {
                stringBuffer.append(", ");
            }
        }
        return stringBuffer.append(']').toString();
    }

    public static CertPath createCertPath(X509Certificate x509Certificate, Set set, List list) throws GeneralSecurityException {
        return (CertPath) createCertPath(x509Certificate, set, list, null)[0];
    }

    public static Object[] createCertPath(X509Certificate x509Certificate, Set set, List list, List list2) throws GeneralSecurityException {
        X509Certificate x509Certificate2;
        boolean z;
        X509Certificate x509Certificate3;
        boolean z2;
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        ArrayList arrayList = new ArrayList();
        linkedHashSet.add(x509Certificate);
        arrayList.add(new Boolean(true));
        X509Certificate x509Certificate4 = null;
        boolean z3 = false;
        X509Certificate x509Certificate5 = x509Certificate;
        while (x509Certificate5 != null && !z3) {
            Iterator it = set.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                TrustAnchor trustAnchor = (TrustAnchor) it.next();
                X509Certificate trustedCert = trustAnchor.getTrustedCert();
                if (trustedCert != null) {
                    if (trustedCert.getSubjectX500Principal().equals(x509Certificate5.getIssuerX500Principal())) {
                        try {
                            x509Certificate5.verify(trustedCert.getPublicKey(), "BC");
                            x509Certificate4 = trustedCert;
                            z3 = true;
                            break;
                        } catch (Exception e) {
                        }
                    } else {
                        continue;
                    }
                } else if (trustAnchor.getCAName().equals(x509Certificate5.getIssuerX500Principal().getName())) {
                    try {
                        x509Certificate5.verify(trustAnchor.getCAPublicKey(), "BC");
                        z3 = true;
                        break;
                    } catch (Exception e2) {
                    }
                } else {
                    continue;
                }
            }
            if (!z3) {
                X509CertSelector x509CertSelector = new X509CertSelector();
                try {
                    x509CertSelector.setSubject(x509Certificate5.getIssuerX500Principal().getEncoded());
                    byte[] extensionValue = x509Certificate5.getExtensionValue(X509Extensions.AuthorityKeyIdentifier.getId());
                    if (extensionValue != null) {
                        try {
                            AuthorityKeyIdentifier instance = AuthorityKeyIdentifier.getInstance(getObject(extensionValue));
                            if (instance.getKeyIdentifier() != null) {
                                x509CertSelector.setSubjectKeyIdentifier(new DEROctetString(instance.getKeyIdentifier()).getDEREncoded());
                            }
                        } catch (IOException e3) {
                        }
                    }
                    x509Certificate3 = findNextCert(list, x509CertSelector, linkedHashSet);
                    if (x509Certificate3 != null || list2 == null) {
                        z2 = false;
                    } else {
                        x509Certificate3 = findNextCert(list2, x509CertSelector, linkedHashSet);
                        z2 = true;
                    }
                    if (x509Certificate3 != null) {
                        linkedHashSet.add(x509Certificate3);
                        arrayList.add(new Boolean(z2));
                    }
                } catch (IOException e4) {
                    throw new IllegalStateException(e4.toString());
                }
            } else {
                x509Certificate3 = x509Certificate5;
            }
            x509Certificate5 = x509Certificate3;
        }
        if (z3) {
            if (x509Certificate4 == null || !x509Certificate4.getSubjectX500Principal().equals(x509Certificate4.getIssuerX500Principal())) {
                X509CertSelector x509CertSelector2 = new X509CertSelector();
                try {
                    x509CertSelector2.setSubject(x509Certificate5.getIssuerX500Principal().getEncoded());
                    x509CertSelector2.setIssuer(x509Certificate5.getIssuerX500Principal().getEncoded());
                    X509Certificate findNextCert = findNextCert(list, x509CertSelector2, linkedHashSet);
                    if (findNextCert != null || list2 == null) {
                        x509Certificate2 = findNextCert;
                        z = false;
                    } else {
                        x509Certificate2 = findNextCert(list2, x509CertSelector2, linkedHashSet);
                        z = true;
                    }
                    if (x509Certificate2 != null) {
                        try {
                            x509Certificate5.verify(x509Certificate2.getPublicKey(), "BC");
                            linkedHashSet.add(x509Certificate2);
                            arrayList.add(new Boolean(z));
                        } catch (GeneralSecurityException e5) {
                        }
                    }
                } catch (IOException e6) {
                    throw new IllegalStateException(e6.toString());
                }
            } else {
                linkedHashSet.add(x509Certificate4);
                arrayList.add(new Boolean(false));
            }
        }
        return new Object[]{CertificateFactory.getInstance("X.509", "BC").generateCertPath(new ArrayList(linkedHashSet)), arrayList};
    }

    private static List findCerts(List list, X509CertSelector x509CertSelector) throws CertStoreException {
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            arrayList.addAll(((CertStore) it.next()).getCertificates(x509CertSelector));
        }
        return arrayList;
    }

    private static X509Certificate findNextCert(List list, X509CertSelector x509CertSelector, Set set) throws CertStoreException {
        Iterator it = findCerts(list, x509CertSelector).iterator();
        boolean z = false;
        X509Certificate x509Certificate = null;
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            x509Certificate = (X509Certificate) it.next();
            if (!set.contains(x509Certificate)) {
                z = true;
                break;
            }
        }
        if (z) {
            return x509Certificate;
        }
        return null;
    }

    public static Set getEmailAddresses(X509Certificate x509Certificate) throws IOException, CertificateEncodingException {
        int i = 0;
        HashSet hashSet = new HashSet();
        X509Principal subjectX509Principal = PrincipalUtil.getSubjectX509Principal(x509Certificate);
        Vector oIDs = subjectX509Principal.getOIDs();
        Vector values = subjectX509Principal.getValues();
        int i2 = 0;
        while (true) {
            if (i2 >= oIDs.size()) {
                break;
            } else if (oIDs.get(i2).equals(X509Principal.EmailAddress)) {
                hashSet.add(((String) values.get(i2)).toLowerCase());
                break;
            } else {
                i2++;
            }
        }
        byte[] extensionValue = x509Certificate.getExtensionValue(SUBJECT_ALTERNATIVE_NAME);
        if (extensionValue != null) {
            DERSequence object = getObject(extensionValue);
            while (true) {
                int i3 = i;
                if (i3 >= object.size()) {
                    break;
                }
                ASN1TaggedObject objectAt = object.getObjectAt(i3);
                if (objectAt.getTagNo() == 1) {
                    hashSet.add(DERIA5String.getInstance(objectAt, true).getString().toLowerCase());
                }
                i = i3 + 1;
            }
        }
        return hashSet;
    }

    private static DERObject getObject(byte[] bArr) throws IOException {
        return new ASN1InputStream(new ASN1InputStream(bArr).readObject().getOctets()).readObject();
    }

    public static Date getSignatureTime(SignerInformation signerInformation) {
        Attribute attribute;
        AttributeTable signedAttributes = signerInformation.getSignedAttributes();
        if (signedAttributes == null || (attribute = signedAttributes.get(CMSAttributes.signingTime)) == null) {
            return null;
        }
        return Time.getInstance(attribute.getAttrValues().getObjectAt(0).getDERObject()).getDate();
    }

    /* access modifiers changed from: protected */
    public void checkSignerCert(X509Certificate x509Certificate, List list, List list2) {
        boolean z;
        PublicKey publicKey = x509Certificate.getPublicKey();
        int bitLength = publicKey instanceof RSAPublicKey ? ((RSAPublicKey) publicKey).getModulus().bitLength() : publicKey instanceof DSAPublicKey ? ((DSAPublicKey) publicKey).getParams().getP().bitLength() : -1;
        if (bitLength != -1 && bitLength <= 512) {
            list2.add(new ErrorBundle(RESOURCE_NAME, "SignedMailValidator.shortSigningKey", new Object[]{new Integer(bitLength)}));
        }
        if (x509Certificate.getNotAfter().getTime() - x509Certificate.getNotBefore().getTime() > THIRTY_YEARS_IN_MILLI_SEC) {
            list2.add(new ErrorBundle(RESOURCE_NAME, "SignedMailValidator.longValidity", new Object[]{new TrustedInput(x509Certificate.getNotBefore()), new TrustedInput(x509Certificate.getNotAfter())}));
        }
        boolean[] keyUsage = x509Certificate.getKeyUsage();
        if (keyUsage != null && !keyUsage[0] && !keyUsage[1]) {
            list.add(new ErrorBundle(RESOURCE_NAME, "SignedMailValidator.signingNotPermitted"));
        }
        try {
            byte[] extensionValue = x509Certificate.getExtensionValue(EXT_KEY_USAGE);
            if (extensionValue != null) {
                ExtendedKeyUsage instance = ExtendedKeyUsage.getInstance(getObject(extensionValue));
                if (!instance.hasKeyPurposeId(KeyPurposeId.anyExtendedKeyUsage) && !instance.hasKeyPurposeId(KeyPurposeId.id_kp_emailProtection)) {
                    list.add(new ErrorBundle(RESOURCE_NAME, "SignedMailValidator.extKeyUsageNotPermitted"));
                }
            }
        } catch (Exception e) {
            list.add(new ErrorBundle(RESOURCE_NAME, "SignedMailValidator.extKeyUsageError", new Object[]{e.getMessage(), e, e.getClass().getName()}));
        }
        try {
            Set emailAddresses = getEmailAddresses(x509Certificate);
            if (emailAddresses.isEmpty()) {
                list.add(new ErrorBundle(RESOURCE_NAME, "SignedMailValidator.noEmailInCert"));
                return;
            }
            int i = 0;
            while (true) {
                if (i >= this.fromAddresses.length) {
                    z = false;
                    break;
                } else if (emailAddresses.contains(this.fromAddresses[i].toLowerCase())) {
                    z = true;
                    break;
                } else {
                    i++;
                }
            }
            if (!z) {
                list.add(new ErrorBundle(RESOURCE_NAME, "SignedMailValidator.emailFromCertMismatch", new Object[]{new UntrustedInput(addressesToString(this.fromAddresses)), new UntrustedInput(emailAddresses)}));
            }
        } catch (Exception e2) {
            list.add(new ErrorBundle(RESOURCE_NAME, "SignedMailValidator.certGetEmailError", new Object[]{e2.getMessage(), e2, e2.getClass().getName()}));
        }
    }

    public CertStore getCertsAndCRLs() {
        return this.certs;
    }

    public SignerInformationStore getSignerInformationStore() {
        return this.signers;
    }

    /* JADX WARN: Type inference failed for: r1v2, types: [java.lang.Throwable, org.bouncycastle.mail.smime.validator.SignedMailValidatorException] */
    public ValidationResult getValidationResult(SignerInformation signerInformation) throws SignedMailValidatorException {
        if (!this.signers.getSigners(signerInformation.getSID()).isEmpty()) {
            return (ValidationResult) this.results.get(signerInformation);
        }
        throw new SignedMailValidatorException(new ErrorBundle(RESOURCE_NAME, "SignedMailValidator.wrongSigner"));
    }

    /* access modifiers changed from: protected */
    public void validateSignatures(PKIXParameters pKIXParameters) {
        PKIXParameters pKIXParameters2 = (PKIXParameters) pKIXParameters.clone();
        pKIXParameters2.addCertStore(this.certs);
        for (SignerInformation signerInformation : this.signers.getSigners()) {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            X509Certificate x509Certificate = null;
            try {
                Iterator it = findCerts(pKIXParameters2.getCertStores(), signerInformation.getSID()).iterator();
                x509Certificate = it.hasNext() ? (X509Certificate) it.next() : null;
            } catch (CertStoreException e) {
                arrayList.add(new ErrorBundle(RESOURCE_NAME, "SignedMailValidator.exceptionRetrievingSignerCert", new Object[]{e.getMessage(), e, e.getClass().getName()}));
            }
            if (x509Certificate != null) {
                boolean z = false;
                try {
                    z = signerInformation.verify(x509Certificate.getPublicKey(), "BC");
                    if (!z) {
                        arrayList.add(new ErrorBundle(RESOURCE_NAME, "SignedMailValidator.signatureNotVerified"));
                    }
                } catch (Exception e2) {
                    arrayList.add(new ErrorBundle(RESOURCE_NAME, "SignedMailValidator.exceptionVerifyingSignature", new Object[]{e2.getMessage(), e2, e2.getClass().getName()}));
                }
                checkSignerCert(x509Certificate, arrayList, arrayList2);
                AttributeTable signedAttributes = signerInformation.getSignedAttributes();
                if (!(signedAttributes == null || signedAttributes.get(PKCSObjectIdentifiers.id_aa_receiptRequest) == null)) {
                    arrayList2.add(new ErrorBundle(RESOURCE_NAME, "SignedMailValidator.signedReceiptRequest"));
                }
                Date signatureTime = getSignatureTime(signerInformation);
                if (signatureTime == null) {
                    arrayList.add(new ErrorBundle(RESOURCE_NAME, "SignedMailValidator.noSigningTime"));
                    signatureTime = new Date();
                } else {
                    try {
                        x509Certificate.checkValidity(signatureTime);
                    } catch (CertificateExpiredException e3) {
                        arrayList.add(new ErrorBundle(RESOURCE_NAME, "SignedMailValidator.certExpired", new Object[]{new TrustedInput(signatureTime), new TrustedInput(x509Certificate.getNotAfter())}));
                    } catch (CertificateNotYetValidException e4) {
                        arrayList.add(new ErrorBundle(RESOURCE_NAME, "SignedMailValidator.certNotYetValid", new Object[]{new TrustedInput(signatureTime), new TrustedInput(x509Certificate.getNotBefore())}));
                    }
                }
                pKIXParameters2.setDate(signatureTime);
                try {
                    ArrayList arrayList3 = new ArrayList();
                    arrayList3.add(this.certs);
                    Object[] createCertPath = createCertPath(x509Certificate, pKIXParameters2.getTrustAnchors(), pKIXParameters.getCertStores(), arrayList3);
                    CertPath certPath = (CertPath) createCertPath[0];
                    List list = (List) createCertPath[1];
                    PKIXCertPathReviewer pKIXCertPathReviewer = (PKIXCertPathReviewer) this.certPathReviewerClass.newInstance();
                    pKIXCertPathReviewer.init(certPath, pKIXParameters2);
                    if (!pKIXCertPathReviewer.isValidCertPath()) {
                        arrayList.add(new ErrorBundle(RESOURCE_NAME, "SignedMailValidator.certPathInvalid"));
                    }
                    this.results.put(signerInformation, new ValidationResult(pKIXCertPathReviewer, z, arrayList, arrayList2, list));
                } catch (IllegalAccessException e5) {
                    throw new IllegalArgumentException("Cannot instantiate object of type " + this.certPathReviewerClass.getName() + ": " + e5.getMessage());
                } catch (InstantiationException e6) {
                    throw new IllegalArgumentException("Cannot instantiate object of type " + this.certPathReviewerClass.getName() + ": " + e6.getMessage());
                } catch (GeneralSecurityException e7) {
                    arrayList.add(new ErrorBundle(RESOURCE_NAME, "SignedMailValidator.exceptionCreateCertPath", new Object[]{e7.getMessage(), e7, e7.getClass().getName()}));
                    this.results.put(signerInformation, new ValidationResult(null, z, arrayList, arrayList2, null));
                } catch (CertPathReviewerException e8) {
                    arrayList.add(e8.getErrorMessage());
                    this.results.put(signerInformation, new ValidationResult(null, z, arrayList, arrayList2, null));
                }
            } else {
                arrayList.add(new ErrorBundle(RESOURCE_NAME, "SignedMailValidator.noSignerCert"));
                this.results.put(signerInformation, new ValidationResult(null, false, arrayList, arrayList2, null));
            }
        }
    }
}
