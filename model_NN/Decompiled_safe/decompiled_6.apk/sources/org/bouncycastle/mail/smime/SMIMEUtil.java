package org.bouncycastle.mail.smime;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.ContentType;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import org.apache.http.entity.mime.MIME;
import org.bouncycastle.asn1.cms.IssuerAndSerialNumber;
import org.bouncycastle.cms.CMSTypedStream;
import org.bouncycastle.jce.PrincipalUtil;
import org.bouncycastle.mail.smime.util.CRLFOutputStream;
import org.bouncycastle.mail.smime.util.FileBackedMimeBodyPart;

public class SMIMEUtil {
    private static final int BUF_SIZE = 32760;

    static class LineOutputStream extends FilterOutputStream {
        private static byte[] newline = new byte[2];

        static {
            newline[0] = 13;
            newline[1] = 10;
        }

        public LineOutputStream(OutputStream outputStream) {
            super(outputStream);
        }

        private static byte[] getBytes(String str) {
            char[] charArray = str.toCharArray();
            int length = charArray.length;
            byte[] bArr = new byte[length];
            for (int i = 0; i < length; i++) {
                bArr[i] = (byte) charArray[i];
            }
            return bArr;
        }

        public void writeln() throws MessagingException {
            try {
                this.out.write(newline);
            } catch (Exception e) {
                throw new MessagingException("IOException", e);
            }
        }

        public void writeln(String str) throws MessagingException {
            try {
                this.out.write(getBytes(str));
                this.out.write(newline);
            } catch (Exception e) {
                throw new MessagingException("IOException", e);
            }
        }
    }

    private static class WriteOnceFileBackedMimeBodyPart extends FileBackedMimeBodyPart {
        public WriteOnceFileBackedMimeBodyPart(InputStream inputStream, File file) throws MessagingException, IOException {
            super(inputStream, file);
        }

        public void writeTo(OutputStream outputStream) throws MessagingException, IOException {
            super.writeTo(outputStream);
            dispose();
        }
    }

    public static IssuerAndSerialNumber createIssuerAndSerialNumberFor(X509Certificate x509Certificate) throws CertificateParsingException {
        try {
            return new IssuerAndSerialNumber(PrincipalUtil.getIssuerX509Principal(x509Certificate), x509Certificate.getSerialNumber());
        } catch (Exception e) {
            throw new CertificateParsingException("exception extracting issuer and serial number: " + e);
        }
    }

    static boolean isCanonicalisationRequired(MimeBodyPart mimeBodyPart, String str) throws MessagingException {
        String[] header = mimeBodyPart.getHeader(MIME.CONTENT_TRANSFER_ENC);
        if (header != null) {
            str = header[0];
        }
        return !str.equalsIgnoreCase(MIME.ENC_BINARY);
    }

    static void outputBodyPart(OutputStream outputStream, BodyPart bodyPart, String str) throws MessagingException, IOException {
        if (bodyPart instanceof MimeBodyPart) {
            MimeBodyPart mimeBodyPart = (MimeBodyPart) bodyPart;
            String[] header = mimeBodyPart.getHeader(MIME.CONTENT_TRANSFER_ENC);
            if (mimeBodyPart.getContent() instanceof MimeMultipart) {
                MimeMultipart mimeMultipart = (MimeMultipart) bodyPart.getContent();
                String str2 = "--" + new ContentType(mimeMultipart.getContentType()).getParameter("boundary");
                LineOutputStream lineOutputStream = new LineOutputStream(outputStream);
                Enumeration allHeaderLines = mimeBodyPart.getAllHeaderLines();
                while (allHeaderLines.hasMoreElements()) {
                    lineOutputStream.writeln((String) allHeaderLines.nextElement());
                }
                lineOutputStream.writeln();
                outputPreamble(lineOutputStream, mimeBodyPart, str2);
                for (int i = 0; i < mimeMultipart.getCount(); i++) {
                    lineOutputStream.writeln(str2);
                    BodyPart bodyPart2 = mimeMultipart.getBodyPart(i);
                    outputBodyPart(outputStream, bodyPart2, str);
                    if (!(bodyPart2.getContent() instanceof MimeMultipart)) {
                        lineOutputStream.writeln();
                    }
                }
                lineOutputStream.writeln(str2 + "--");
                return;
            }
            if (header != null) {
                str = header[0];
            }
            if (str.equalsIgnoreCase("base64") || str.equalsIgnoreCase("quoted-printable")) {
                LineOutputStream lineOutputStream2 = new LineOutputStream(outputStream);
                Enumeration allHeaderLines2 = mimeBodyPart.getAllHeaderLines();
                while (allHeaderLines2.hasMoreElements()) {
                    lineOutputStream2.writeln((String) allHeaderLines2.nextElement());
                }
                lineOutputStream2.writeln();
                lineOutputStream2.flush();
                InputStream rawInputStream = mimeBodyPart.getRawInputStream();
                CRLFOutputStream cRLFOutputStream = new CRLFOutputStream(outputStream);
                byte[] bArr = new byte[BUF_SIZE];
                while (true) {
                    int read = rawInputStream.read(bArr, 0, bArr.length);
                    if (read > 0) {
                        cRLFOutputStream.write(bArr, 0, read);
                    } else {
                        cRLFOutputStream.flush();
                        return;
                    }
                }
            } else {
                if (!str.equalsIgnoreCase(MIME.ENC_BINARY)) {
                    outputStream = new CRLFOutputStream(outputStream);
                }
                bodyPart.writeTo(outputStream);
                outputStream.flush();
            }
        } else {
            if (!str.equalsIgnoreCase(MIME.ENC_BINARY)) {
                outputStream = new CRLFOutputStream(outputStream);
            }
            bodyPart.writeTo(outputStream);
            outputStream.flush();
        }
    }

    static void outputPreamble(LineOutputStream lineOutputStream, MimeBodyPart mimeBodyPart, String str) throws MessagingException, IOException {
        String readLine;
        try {
            InputStream rawInputStream = mimeBodyPart.getRawInputStream();
            while (true) {
                readLine = readLine(rawInputStream);
                if (readLine == null || readLine.equals(str)) {
                    rawInputStream.close();
                } else {
                    lineOutputStream.writeln(readLine);
                }
            }
            rawInputStream.close();
            if (readLine == null) {
                throw new MessagingException("no boundary found");
            }
        } catch (MessagingException e) {
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x001c  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x001a A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String readLine(java.io.InputStream r3) throws java.io.IOException {
        /*
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
        L_0x0005:
            int r1 = r3.read()
            if (r1 < 0) goto L_0x0018
            r2 = 10
            if (r1 == r2) goto L_0x0018
            r2 = 13
            if (r1 == r2) goto L_0x0005
            char r1 = (char) r1
            r0.append(r1)
            goto L_0x0005
        L_0x0018:
            if (r1 >= 0) goto L_0x001c
            r0 = 0
        L_0x001b:
            return r0
        L_0x001c:
            java.lang.String r0 = r0.toString()
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: org.bouncycastle.mail.smime.SMIMEUtil.readLine(java.io.InputStream):java.lang.String");
    }

    public static MimeBodyPart toMimeBodyPart(InputStream inputStream) throws SMIMEException {
        try {
            return new MimeBodyPart(inputStream);
        } catch (MessagingException e) {
            throw new SMIMEException("exception creating body part.", e);
        }
    }

    public static MimeBodyPart toMimeBodyPart(byte[] bArr) throws SMIMEException {
        return toMimeBodyPart(new ByteArrayInputStream(bArr));
    }

    public static FileBackedMimeBodyPart toMimeBodyPart(CMSTypedStream cMSTypedStream) throws SMIMEException {
        try {
            return toMimeBodyPart(cMSTypedStream, File.createTempFile("bcMail", ".mime"));
        } catch (IOException e) {
            throw new SMIMEException("IOException creating tmp file:" + e.getMessage(), e);
        }
    }

    public static FileBackedMimeBodyPart toMimeBodyPart(CMSTypedStream cMSTypedStream, File file) throws SMIMEException {
        try {
            return new FileBackedMimeBodyPart(cMSTypedStream.getContentStream(), file);
        } catch (IOException e) {
            throw new SMIMEException("can't save content to file: " + e, e);
        } catch (MessagingException e2) {
            throw new SMIMEException("can't create part: " + e2, e2);
        }
    }

    static FileBackedMimeBodyPart toWriteOnceBodyPart(CMSTypedStream cMSTypedStream) throws SMIMEException {
        try {
            return new WriteOnceFileBackedMimeBodyPart(cMSTypedStream.getContentStream(), File.createTempFile("bcMail", ".mime"));
        } catch (IOException e) {
            throw new SMIMEException("IOException creating tmp file:" + e.getMessage(), e);
        } catch (MessagingException e2) {
            throw new SMIMEException("can't create part: " + e2, e2);
        }
    }
}
