package org.bouncycastle.mail.smime;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import javax.crypto.KeyGenerator;
import javax.mail.Header;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import org.bouncycastle.cms.CMSEnvelopedGenerator;
import org.bouncycastle.util.Strings;

public class SMIMEGenerator {
    private static Map BASE_CIPHER_NAMES = new HashMap();
    protected String encoding = "base64";
    protected boolean useBase64 = true;

    static {
        BASE_CIPHER_NAMES.put(CMSEnvelopedGenerator.DES_EDE3_CBC, "DESEDE");
        BASE_CIPHER_NAMES.put(CMSEnvelopedGenerator.AES128_CBC, "AES");
        BASE_CIPHER_NAMES.put(CMSEnvelopedGenerator.AES192_CBC, "AES");
        BASE_CIPHER_NAMES.put(CMSEnvelopedGenerator.AES256_CBC, "AES");
    }

    protected SMIMEGenerator() {
    }

    private KeyGenerator createKeyGenerator(String str, String str2) throws NoSuchProviderException, NoSuchAlgorithmException {
        return str2 != null ? KeyGenerator.getInstance(str, str2) : KeyGenerator.getInstance(str);
    }

    private void extractHeaders(MimeBodyPart mimeBodyPart, MimeMessage mimeMessage) throws MessagingException {
        Enumeration allHeaders = mimeMessage.getAllHeaders();
        while (allHeaders.hasMoreElements()) {
            Header header = (Header) allHeaders.nextElement();
            mimeBodyPart.setHeader(header.getName(), header.getValue());
        }
    }

    /* access modifiers changed from: protected */
    public KeyGenerator createSymmetricKeyGenerator(String str, String str2) throws NoSuchProviderException, NoSuchAlgorithmException {
        try {
            return createKeyGenerator(str, str2);
        } catch (NoSuchAlgorithmException e) {
            try {
                String str3 = (String) BASE_CIPHER_NAMES.get(str);
                if (str3 != null) {
                    return createKeyGenerator(str3, str2);
                }
            } catch (NoSuchAlgorithmException e2) {
            }
            if (str2 != null) {
                return createSymmetricKeyGenerator(str, null);
            }
            throw e;
        }
    }

    /* access modifiers changed from: protected */
    public MimeBodyPart makeContentBodyPart(MimeBodyPart mimeBodyPart) throws SMIMEException {
        try {
            MimeMessage mimeMessage = new MimeMessage((Session) null);
            Enumeration allHeaders = mimeBodyPart.getAllHeaders();
            mimeMessage.setDataHandler(mimeBodyPart.getDataHandler());
            while (allHeaders.hasMoreElements()) {
                Header header = (Header) allHeaders.nextElement();
                mimeMessage.setHeader(header.getName(), header.getValue());
            }
            mimeMessage.saveChanges();
            Enumeration allHeaders2 = mimeMessage.getAllHeaders();
            while (allHeaders2.hasMoreElements()) {
                Header header2 = (Header) allHeaders2.nextElement();
                if (Strings.toLowerCase(header2.getName()).startsWith("content-")) {
                    mimeBodyPart.setHeader(header2.getName(), header2.getValue());
                }
            }
            return mimeBodyPart;
        } catch (MessagingException e) {
            throw new SMIMEException("exception saving message state.", e);
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0036, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003e, code lost:
        throw new org.bouncycastle.mail.smime.SMIMEException("exception saving message state.", r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x003f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0047, code lost:
        throw new org.bouncycastle.mail.smime.SMIMEException("exception getting message content.", r0);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x003f A[ExcHandler: IOException (r0v1 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:1:0x0007] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public javax.mail.internet.MimeBodyPart makeContentBodyPart(javax.mail.internet.MimeMessage r4) throws org.bouncycastle.mail.smime.SMIMEException {
        /*
            r3 = this;
            javax.mail.internet.MimeBodyPart r0 = new javax.mail.internet.MimeBodyPart
            r0.<init>()
            java.lang.String r1 = "Message-Id"
            r4.removeHeader(r1)     // Catch:{ MessagingException -> 0x0036, IOException -> 0x003f }
            java.lang.String r1 = "Mime-Version"
            r4.removeHeader(r1)     // Catch:{ MessagingException -> 0x0036, IOException -> 0x003f }
            java.lang.Object r1 = r4.getContent()     // Catch:{ MessagingException -> 0x0026, IOException -> 0x003f }
            boolean r1 = r1 instanceof javax.mail.Multipart     // Catch:{ MessagingException -> 0x0026, IOException -> 0x003f }
            if (r1 == 0) goto L_0x0027
            java.io.InputStream r1 = r4.getRawInputStream()     // Catch:{ MessagingException -> 0x0026, IOException -> 0x003f }
            java.lang.String r2 = r4.getContentType()     // Catch:{ MessagingException -> 0x0026, IOException -> 0x003f }
            r0.setContent(r1, r2)     // Catch:{ MessagingException -> 0x0026, IOException -> 0x003f }
            r3.extractHeaders(r0, r4)     // Catch:{ MessagingException -> 0x0026, IOException -> 0x003f }
        L_0x0025:
            return r0
        L_0x0026:
            r1 = move-exception
        L_0x0027:
            java.lang.Object r1 = r4.getContent()     // Catch:{ MessagingException -> 0x0036, IOException -> 0x003f }
            java.lang.String r2 = r4.getContentType()     // Catch:{ MessagingException -> 0x0036, IOException -> 0x003f }
            r0.setContent(r1, r2)     // Catch:{ MessagingException -> 0x0036, IOException -> 0x003f }
            r3.extractHeaders(r0, r4)     // Catch:{ MessagingException -> 0x0036, IOException -> 0x003f }
            goto L_0x0025
        L_0x0036:
            r0 = move-exception
            org.bouncycastle.mail.smime.SMIMEException r1 = new org.bouncycastle.mail.smime.SMIMEException
            java.lang.String r2 = "exception saving message state."
            r1.<init>(r2, r0)
            throw r1
        L_0x003f:
            r0 = move-exception
            org.bouncycastle.mail.smime.SMIMEException r1 = new org.bouncycastle.mail.smime.SMIMEException
            java.lang.String r2 = "exception getting message content."
            r1.<init>(r2, r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: org.bouncycastle.mail.smime.SMIMEGenerator.makeContentBodyPart(javax.mail.internet.MimeMessage):javax.mail.internet.MimeBodyPart");
    }

    public void setContentTransferEncoding(String str) {
        this.encoding = str;
        this.useBase64 = Strings.toLowerCase(str).equals("base64");
    }
}
