package org.bouncycastle.mail.smime;

import java.io.IOException;
import java.io.OutputStream;
import java.security.AlgorithmParameters;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import javax.activation.CommandMap;
import javax.activation.MailcapCommandMap;
import javax.crypto.SecretKey;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import org.apache.http.entity.mime.MIME;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.cms.CMSEnvelopedDataGenerator;
import org.bouncycastle.cms.CMSEnvelopedDataStreamGenerator;
import org.bouncycastle.cms.CMSException;

public class SMIMEEnvelopedGenerator extends SMIMEGenerator {
    public static final String AES128_CBC = CMSEnvelopedDataGenerator.AES128_CBC;
    public static final String AES128_WRAP = CMSEnvelopedDataGenerator.AES128_WRAP;
    public static final String AES192_CBC = CMSEnvelopedDataGenerator.AES192_CBC;
    public static final String AES256_CBC = CMSEnvelopedDataGenerator.AES256_CBC;
    public static final String AES256_WRAP = CMSEnvelopedDataGenerator.AES256_WRAP;
    public static final String CAMELLIA128_CBC = CMSEnvelopedDataGenerator.CAMELLIA128_CBC;
    public static final String CAMELLIA128_WRAP = CMSEnvelopedDataGenerator.CAMELLIA128_WRAP;
    public static final String CAMELLIA192_CBC = CMSEnvelopedDataGenerator.CAMELLIA192_CBC;
    public static final String CAMELLIA192_WRAP = CMSEnvelopedDataGenerator.CAMELLIA192_WRAP;
    public static final String CAMELLIA256_CBC = CMSEnvelopedDataGenerator.CAMELLIA256_CBC;
    public static final String CAMELLIA256_WRAP = CMSEnvelopedDataGenerator.CAMELLIA256_WRAP;
    public static final String CAST5_CBC = "1.2.840.113533.7.66.10";
    public static final String DES_EDE3_CBC = CMSEnvelopedDataGenerator.DES_EDE3_CBC;
    public static final String DES_EDE3_WRAP = CMSEnvelopedDataGenerator.DES_EDE3_WRAP;
    public static final String ECDH_SHA1KDF = CMSEnvelopedDataGenerator.ECDH_SHA1KDF;
    private static final String ENCRYPTED_CONTENT_TYPE = "application/pkcs7-mime; name=\"smime.p7m\"; smime-type=enveloped-data";
    public static final String IDEA_CBC = "1.3.6.1.4.1.188.7.1.1.2";
    public static final String RC2_CBC = CMSEnvelopedDataGenerator.RC2_CBC;
    public static final String SEED_CBC = CMSEnvelopedDataGenerator.SEED_CBC;
    public static final String SEED_WRAP = CMSEnvelopedDataGenerator.SEED_WRAP;
    /* access modifiers changed from: private */
    public EnvelopedGenerator fact = new EnvelopedGenerator();

    private class ContentEncryptor implements SMIMEStreamingProcessor {
        private final MimeBodyPart _content;
        private final String _encryptionOid;
        private boolean _firstTime = true;
        private final int _keySize;
        private final String _provider;

        ContentEncryptor(MimeBodyPart mimeBodyPart, String str, int i, String str2) {
            this._content = mimeBodyPart;
            this._encryptionOid = str;
            this._keySize = i;
            this._provider = str2;
        }

        public void write(OutputStream outputStream) throws IOException {
            OutputStream regenerate;
            try {
                if (this._firstTime) {
                    regenerate = this._keySize == 0 ? SMIMEEnvelopedGenerator.this.fact.open(outputStream, this._encryptionOid, this._provider) : SMIMEEnvelopedGenerator.this.fact.open(outputStream, this._encryptionOid, this._keySize, this._provider);
                    this._firstTime = false;
                } else {
                    regenerate = SMIMEEnvelopedGenerator.this.fact.regenerate(outputStream, this._provider);
                }
                this._content.writeTo(regenerate);
                regenerate.close();
            } catch (MessagingException e) {
                throw new WrappingIOException(e.toString(), e);
            } catch (NoSuchAlgorithmException e2) {
                throw new WrappingIOException(e2.toString(), e2);
            } catch (NoSuchProviderException e3) {
                throw new WrappingIOException(e3.toString(), e3);
            } catch (CMSException e4) {
                throw new WrappingIOException(e4.toString(), e4);
            }
        }
    }

    private class EnvelopedGenerator extends CMSEnvelopedDataStreamGenerator {
        private SecretKey _encKey;
        private String _encryptionOID;
        private AlgorithmParameters _params;
        private ASN1EncodableVector _recipientInfos;

        private EnvelopedGenerator() {
        }

        /* access modifiers changed from: protected */
        public OutputStream open(OutputStream outputStream, String str, SecretKey secretKey, AlgorithmParameters algorithmParameters, ASN1EncodableVector aSN1EncodableVector, String str2) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
            this._encryptionOID = str;
            this._encKey = secretKey;
            this._params = algorithmParameters;
            this._recipientInfos = aSN1EncodableVector;
            return super.open(outputStream, str, secretKey, algorithmParameters, aSN1EncodableVector, str2);
        }

        /* access modifiers changed from: package-private */
        public OutputStream regenerate(OutputStream outputStream, String str) throws NoSuchAlgorithmException, NoSuchProviderException, CMSException {
            return super.open(outputStream, this._encryptionOID, this._encKey, this._params, this._recipientInfos, str);
        }
    }

    private static class WrappingIOException extends IOException {
        private Throwable cause;

        WrappingIOException(String str, Throwable th) {
            super(str);
            this.cause = th;
        }

        public Throwable getCause() {
            return this.cause;
        }
    }

    static {
        MailcapCommandMap defaultCommandMap = CommandMap.getDefaultCommandMap();
        defaultCommandMap.addMailcap("application/pkcs7-mime;; x-java-content-handler=org.bouncycastle.mail.smime.handlers.pkcs7_mime");
        defaultCommandMap.addMailcap("application/x-pkcs7-mime;; x-java-content-handler=org.bouncycastle.mail.smime.handlers.x_pkcs7_mime");
        CommandMap.setDefaultCommandMap(defaultCommandMap);
    }

    private MimeBodyPart make(MimeBodyPart mimeBodyPart, String str, int i, String str2) throws NoSuchAlgorithmException, NoSuchProviderException, SMIMEException {
        createSymmetricKeyGenerator(str, str2);
        try {
            MimeBodyPart mimeBodyPart2 = new MimeBodyPart();
            mimeBodyPart2.setContent(new ContentEncryptor(mimeBodyPart, str, i, str2), ENCRYPTED_CONTENT_TYPE);
            mimeBodyPart2.addHeader("Content-Type", ENCRYPTED_CONTENT_TYPE);
            mimeBodyPart2.addHeader(MIME.CONTENT_DISPOSITION, "attachment; filename=\"smime.p7m\"");
            mimeBodyPart2.addHeader("Content-Description", "S/MIME Encrypted Message");
            mimeBodyPart2.addHeader(MIME.CONTENT_TRANSFER_ENC, this.encoding);
            return mimeBodyPart2;
        } catch (MessagingException e) {
            throw new SMIMEException("exception putting multi-part together.", e);
        }
    }

    public void addKEKRecipient(SecretKey secretKey, byte[] bArr) throws IllegalArgumentException {
        this.fact.addKEKRecipient(secretKey, bArr);
    }

    public void addKeyAgreementRecipient(String str, PrivateKey privateKey, PublicKey publicKey, X509Certificate x509Certificate, String str2, String str3) throws NoSuchProviderException, NoSuchAlgorithmException, InvalidKeyException {
        this.fact.addKeyAgreementRecipient(str, privateKey, publicKey, x509Certificate, str2, str3);
    }

    public void addKeyTransRecipient(PublicKey publicKey, byte[] bArr) throws IllegalArgumentException {
        this.fact.addKeyTransRecipient(publicKey, bArr);
    }

    public void addKeyTransRecipient(X509Certificate x509Certificate) throws IllegalArgumentException {
        this.fact.addKeyTransRecipient(x509Certificate);
    }

    public MimeBodyPart generate(MimeBodyPart mimeBodyPart, String str, int i, String str2) throws NoSuchAlgorithmException, NoSuchProviderException, SMIMEException {
        return make(makeContentBodyPart(mimeBodyPart), str, i, str2);
    }

    public MimeBodyPart generate(MimeBodyPart mimeBodyPart, String str, String str2) throws NoSuchAlgorithmException, NoSuchProviderException, SMIMEException {
        return make(makeContentBodyPart(mimeBodyPart), str, 0, str2);
    }

    public MimeBodyPart generate(MimeMessage mimeMessage, String str, int i, String str2) throws NoSuchAlgorithmException, NoSuchProviderException, SMIMEException {
        try {
            mimeMessage.saveChanges();
            return make(makeContentBodyPart(mimeMessage), str, i, str2);
        } catch (MessagingException e) {
            throw new SMIMEException("unable to save message", e);
        }
    }

    public MimeBodyPart generate(MimeMessage mimeMessage, String str, String str2) throws NoSuchAlgorithmException, NoSuchProviderException, SMIMEException {
        try {
            mimeMessage.saveChanges();
            return make(makeContentBodyPart(mimeMessage), str, 0, str2);
        } catch (MessagingException e) {
            throw new SMIMEException("unable to save message", e);
        }
    }

    public void setBerEncodeRecipients(boolean z) {
        this.fact.setBEREncodeRecipients(z);
    }
}
