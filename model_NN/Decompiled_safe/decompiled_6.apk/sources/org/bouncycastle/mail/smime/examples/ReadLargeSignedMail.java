package org.bouncycastle.mail.smime.examples;

import java.security.cert.CertStore;
import java.security.cert.X509Certificate;
import javax.mail.Authenticator;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.mail.smime.SMIMESignedParser;
import org.bouncycastle.mail.smime.util.SharedFileInputStream;

public class ReadLargeSignedMail {
    public static void main(String[] strArr) throws Exception {
        MimeMessage mimeMessage = new MimeMessage(Session.getDefaultInstance(System.getProperties(), (Authenticator) null), new SharedFileInputStream("signed.message"));
        if (mimeMessage.isMimeType("multipart/signed")) {
            SMIMESignedParser sMIMESignedParser = new SMIMESignedParser((MimeMultipart) mimeMessage.getContent());
            System.out.println("Status:");
            verify(sMIMESignedParser);
        } else if (mimeMessage.isMimeType("application/pkcs7-mime")) {
            SMIMESignedParser sMIMESignedParser2 = new SMIMESignedParser((Part) mimeMessage);
            System.out.println("Status:");
            verify(sMIMESignedParser2);
        } else {
            System.err.println("Not a signed message!");
        }
    }

    private static void verify(SMIMESignedParser sMIMESignedParser) throws Exception {
        CertStore certificatesAndCRLs = sMIMESignedParser.getCertificatesAndCRLs("Collection", "BC");
        for (SignerInformation signerInformation : sMIMESignedParser.getSignerInfos().getSigners()) {
            if (signerInformation.verify((X509Certificate) certificatesAndCRLs.getCertificates(signerInformation.getSID()).iterator().next(), "BC")) {
                System.out.println("signature verified");
            } else {
                System.out.println("signature failed!");
            }
        }
    }
}
