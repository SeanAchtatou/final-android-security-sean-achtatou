package org.bouncycastle.mail.smime.examples;

import cn.com.jit.ida.util.pki.cipher.Mechanism;
import java.io.FileInputStream;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;
import java.security.Security;
import java.security.cert.CertStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.CollectionCertStoreParameters;
import java.security.cert.PKIXParameters;
import java.security.cert.TrustAnchor;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import javax.mail.Authenticator;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import javax.security.auth.x500.X500Principal;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.i18n.ErrorBundle;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.mail.smime.validator.SignedMailValidator;
import org.bouncycastle.x509.PKIXCertPathReviewer;
import org.bouncycastle.x509.extension.X509ExtensionUtil;

public class ValidateSignedMail {
    public static final int DETAIL = 3;
    private static final String RESOURCE_NAME = "org.bouncycastle.mail.smime.validator.SignedMailValidatorMessages";
    public static final int SUMMARY = 2;
    public static final int TEXT = 1;
    public static final int TITLE = 0;
    static int dbgLvl = 3;
    public static final boolean useCaCerts = false;

    private static TrustAnchor getDummyTrustAnchor() throws Exception {
        X500Principal x500Principal = new X500Principal("CN=Dummy Trust Anchor");
        KeyPairGenerator instance = KeyPairGenerator.getInstance(Mechanism.RSA, "BC");
        instance.initialize(1024, new SecureRandom());
        return new TrustAnchor(x500Principal, instance.generateKeyPair().getPublic(), (byte[]) null);
    }

    protected static TrustAnchor getTrustAnchor(String str) throws Exception {
        X509Certificate loadCert = loadCert(str);
        if (loadCert == null) {
            return null;
        }
        byte[] extensionValue = loadCert.getExtensionValue(X509Extensions.NameConstraints.getId());
        return extensionValue != null ? new TrustAnchor(loadCert, X509ExtensionUtil.fromExtensionValue(extensionValue).getDEREncoded()) : new TrustAnchor(loadCert, null);
    }

    protected static X509CRL loadCRL(String str) {
        try {
            return (X509CRL) CertificateFactory.getInstance("X.509", "BC").generateCRL(new FileInputStream(str));
        } catch (Exception e) {
            System.out.println("crlfile \"" + str + "\" not found - classpath is " + System.getProperty("java.class.path"));
            return null;
        }
    }

    protected static X509Certificate loadCert(String str) {
        try {
            return (X509Certificate) CertificateFactory.getInstance("X.509", "BC").generateCertificate(new FileInputStream(str));
        } catch (Exception e) {
            System.out.println("certfile \"" + str + "\" not found - classpath is " + System.getProperty("java.class.path"));
            return null;
        }
    }

    public static void main(String[] strArr) throws Exception {
        Security.addProvider(new BouncyCastleProvider());
        MimeMessage mimeMessage = new MimeMessage(Session.getDefaultInstance(System.getProperties(), (Authenticator) null), new FileInputStream("signed.message"));
        HashSet hashSet = new HashSet();
        TrustAnchor trustAnchor = getTrustAnchor("trustanchor");
        if (trustAnchor == null) {
            System.out.println("no trustanchor file found, using a dummy trustanchor");
            trustAnchor = getDummyTrustAnchor();
        }
        hashSet.add(trustAnchor);
        PKIXParameters pKIXParameters = new PKIXParameters(hashSet);
        ArrayList arrayList = new ArrayList();
        X509CRL loadCRL = loadCRL("crl.file");
        if (loadCRL != null) {
            arrayList.add(loadCRL);
        }
        pKIXParameters.addCertStore(CertStore.getInstance("Collection", new CollectionCertStoreParameters(arrayList), "BC"));
        pKIXParameters.setRevocationEnabled(true);
        verifySignedMail(mimeMessage, pKIXParameters);
    }

    public static void verifySignedMail(MimeMessage mimeMessage, PKIXParameters pKIXParameters) throws Exception {
        Locale locale = Locale.ENGLISH;
        SignedMailValidator signedMailValidator = new SignedMailValidator(mimeMessage, pKIXParameters);
        for (SignerInformation validationResult : signedMailValidator.getSignerInformationStore().getSigners()) {
            SignedMailValidator.ValidationResult validationResult2 = signedMailValidator.getValidationResult(validationResult);
            if (validationResult2.isValidSignature()) {
                System.out.println(new ErrorBundle(RESOURCE_NAME, "SignedMailValidator.sigValid").getText(locale));
            } else {
                System.out.println(new ErrorBundle(RESOURCE_NAME, "SignedMailValidator.sigInvalid").getText(locale));
                System.out.println("Errors:");
                for (ErrorBundle errorBundle : validationResult2.getErrors()) {
                    if (dbgLvl == 3) {
                        System.out.println("\t\t" + errorBundle.getDetail(locale));
                    } else {
                        System.out.println("\t\t" + errorBundle.getText(locale));
                    }
                }
            }
            if (!validationResult2.getNotifications().isEmpty()) {
                System.out.println("Notifications:");
                for (ErrorBundle errorBundle2 : validationResult2.getNotifications()) {
                    if (dbgLvl == 3) {
                        System.out.println("\t\t" + errorBundle2.getDetail(locale));
                    } else {
                        System.out.println("\t\t" + errorBundle2.getText(locale));
                    }
                }
            }
            PKIXCertPathReviewer certPathReview = validationResult2.getCertPathReview();
            if (certPathReview != null) {
                if (certPathReview.isValidCertPath()) {
                    System.out.println("Certificate path valid");
                } else {
                    System.out.println("Certificate path invalid");
                }
                System.out.println("\nCertificate path validation results:");
                System.out.println("Errors:");
                for (ErrorBundle errorBundle3 : certPathReview.getErrors(-1)) {
                    if (dbgLvl == 3) {
                        System.out.println("\t\t" + errorBundle3.getDetail(locale));
                    } else {
                        System.out.println("\t\t" + errorBundle3.getText(locale));
                    }
                }
                System.out.println("Notifications:");
                for (ErrorBundle text : certPathReview.getNotifications(-1)) {
                    System.out.println("\t" + text.getText(locale));
                }
                Iterator<? extends Certificate> it = certPathReview.getCertPath().getCertificates().iterator();
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (!it.hasNext()) {
                        break;
                    }
                    X509Certificate x509Certificate = (X509Certificate) it.next();
                    System.out.println("\nCertificate " + i2 + "\n========");
                    System.out.println("Issuer: " + x509Certificate.getIssuerDN().getName());
                    System.out.println("Subject: " + x509Certificate.getSubjectDN().getName());
                    System.out.println("\tErrors:");
                    for (ErrorBundle errorBundle4 : certPathReview.getErrors(i2)) {
                        if (dbgLvl == 3) {
                            System.out.println("\t\t" + errorBundle4.getDetail(locale));
                        } else {
                            System.out.println("\t\t" + errorBundle4.getText(locale));
                        }
                    }
                    System.out.println("\tNotifications:");
                    for (ErrorBundle errorBundle5 : certPathReview.getNotifications(i2)) {
                        if (dbgLvl == 3) {
                            System.out.println("\t\t" + errorBundle5.getDetail(locale));
                        } else {
                            System.out.println("\t\t" + errorBundle5.getText(locale));
                        }
                    }
                    i = i2 + 1;
                }
            }
        }
    }
}
