package org.bouncycastle.mail.smime.examples;

import java.io.FileInputStream;
import java.security.cert.CertStore;
import java.security.cert.X509Certificate;
import javax.mail.Authenticator;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.mail.smime.SMIMESigned;

public class ReadSignedMail {
    public static void main(String[] strArr) throws Exception {
        MimeMessage mimeMessage = new MimeMessage(Session.getDefaultInstance(System.getProperties(), (Authenticator) null), new FileInputStream("signed.message"));
        if (mimeMessage.isMimeType("multipart/signed")) {
            SMIMESigned sMIMESigned = new SMIMESigned((MimeMultipart) mimeMessage.getContent());
            MimeBodyPart content = sMIMESigned.getContent();
            System.out.println("Content:");
            Object content2 = content.getContent();
            if (content2 instanceof String) {
                System.out.println((String) content2);
            } else if (content2 instanceof Multipart) {
                Multipart multipart = (Multipart) content2;
                int count = multipart.getCount();
                for (int i = 0; i < count; i++) {
                    Object content3 = multipart.getBodyPart(i).getContent();
                    System.out.println("Part " + i);
                    System.out.println("---------------------------");
                    if (content3 instanceof String) {
                        System.out.println((String) content3);
                    } else {
                        System.out.println("can't print...");
                    }
                }
            }
            System.out.println("Status:");
            verify(sMIMESigned);
        } else if (mimeMessage.isMimeType("application/pkcs7-mime") || mimeMessage.isMimeType("application/x-pkcs7-mime")) {
            SMIMESigned sMIMESigned2 = new SMIMESigned((Part) mimeMessage);
            MimeBodyPart content4 = sMIMESigned2.getContent();
            System.out.println("Content:");
            Object content5 = content4.getContent();
            if (content5 instanceof String) {
                System.out.println((String) content5);
            }
            System.out.println("Status:");
            verify(sMIMESigned2);
        } else {
            System.err.println("Not a signed message!");
        }
    }

    private static void verify(SMIMESigned sMIMESigned) throws Exception {
        CertStore certificatesAndCRLs = sMIMESigned.getCertificatesAndCRLs("Collection", "BC");
        for (SignerInformation signerInformation : sMIMESigned.getSignerInfos().getSigners()) {
            if (signerInformation.verify((X509Certificate) certificatesAndCRLs.getCertificates(signerInformation.getSID()).iterator().next(), "BC")) {
                System.out.println("signature verified");
            } else {
                System.out.println("signature failed!");
            }
        }
    }
}
