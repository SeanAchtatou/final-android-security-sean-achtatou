package org.bouncycastle.mail.smime.util;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class CRLFOutputStream extends FilterOutputStream {
    protected static byte[] newline = new byte[2];
    protected int lastb = -1;

    static {
        newline[0] = 13;
        newline[1] = 10;
    }

    public CRLFOutputStream(OutputStream outputStream) {
        super(outputStream);
    }

    public void write(int i) throws IOException {
        if (i == 13) {
            this.out.write(newline);
        } else if (i != 10) {
            this.out.write(i);
        } else if (this.lastb != 13) {
            this.out.write(newline);
        }
        this.lastb = i;
    }

    public void write(byte[] bArr) throws IOException {
        write(bArr, 0, bArr.length);
    }

    public void write(byte[] bArr, int i, int i2) throws IOException {
        for (int i3 = i; i3 != i + i2; i3++) {
            write(bArr[i3]);
        }
    }

    public void writeln() throws IOException {
        this.out.write(newline);
    }
}
