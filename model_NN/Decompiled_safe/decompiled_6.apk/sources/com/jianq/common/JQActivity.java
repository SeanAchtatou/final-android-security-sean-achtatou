package com.jianq.common;

import android.app.Activity;
import android.os.Bundle;

public class JQActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setRequestedOrientation(1);
        JQApplication.getInst().addActivity(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        JQApplication.getInst().remove(this);
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onExit() {
        JQApplication.getInst().exit();
    }

    /* access modifiers changed from: protected */
    public void onExitAndStart() {
        JQApplication.getInst().restart();
    }
}
