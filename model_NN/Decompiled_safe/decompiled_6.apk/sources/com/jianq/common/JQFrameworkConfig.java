package com.jianq.common;

import android.content.Context;
import android.content.res.XmlResourceParser;
import android.util.Log;
import android.util.Xml;
import cn.com.jit.pnxclient.constant.PNXConfigConstant;
import com.jianq.misc.StringEx;
import com.jianq.net.JQBasicNetwork;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

public class JQFrameworkConfig {
    private static JQFrameworkConfig frameworkConfig;
    private Map<String, String> configValue = new HashMap();

    public JQFrameworkConfig(Context context, int xmlConfig) {
        buildConfig(context, xmlConfig);
    }

    public JQFrameworkConfig(InputStream inputStream) {
        buildConfig(inputStream);
    }

    public static JQFrameworkConfig getInst(InputStream inputStream) {
        if (frameworkConfig == null) {
            frameworkConfig = new JQFrameworkConfig(inputStream);
        }
        return frameworkConfig;
    }

    public static JQFrameworkConfig getInst() {
        if (frameworkConfig == null) {
            frameworkConfig = new JQFrameworkConfig(null, -1);
        }
        return frameworkConfig;
    }

    public static JQFrameworkConfig getInst(Context ctx, int cfg) {
        if (frameworkConfig == null) {
            frameworkConfig = new JQFrameworkConfig(ctx, cfg);
        }
        return frameworkConfig;
    }

    public String getHost() {
        return getConfigValue("host");
    }

    public String getPort() {
        return getConfigValue("port");
    }

    public String getLogPath() {
        return getConfigValue("logPath");
    }

    public String getLogName() {
        return getConfigValue("logname");
    }

    public String getConfigValue(String key) {
        if (key == null || key.equals(StringEx.Empty) || !this.configValue.containsKey(key.toLowerCase())) {
            return null;
        }
        return this.configValue.get(key.toLowerCase());
    }

    public String getDomain() {
        return "http://" + getHost() + PNXConfigConstant.RESP_SPLIT_3 + getPort() + "/";
    }

    public Map<String, String> getConfigValue() {
        return this.configValue;
    }

    public void setConfigValue(Map<String, String> configValue2) {
        this.configValue = configValue2;
    }

    public void put(String key, String value) {
        this.configValue.put(key.toLowerCase(), value);
    }

    private void buildConfig(Context context, int xmlConfig) {
        XmlResourceParser xrp = context.getResources().getXml(xmlConfig);
        String tn = StringEx.Empty;
        while (xrp.getEventType() != 1) {
            try {
                if (xrp.getEventType() == 2) {
                    tn = xrp.getName();
                }
                if (xrp.getEventType() == 4) {
                    this.configValue.put(tn.toLowerCase(), xrp.getText());
                }
                xrp.next();
            } catch (XmlPullParserException e) {
                Log.e(toString(), new StringBuilder().append(e.getDetail()).toString());
                e.printStackTrace();
                return;
            } catch (IOException e2) {
                Log.e(toString(), e2.getMessage());
                e2.printStackTrace();
                return;
            } finally {
                xrp.close();
            }
        }
    }

    private void buildConfig(InputStream inputStream) {
        XmlPullParser pullParser = Xml.newPullParser();
        try {
            pullParser.setInput(inputStream, JQBasicNetwork.UTF_8);
            for (int event = pullParser.getEventType(); event != 1; event = pullParser.next()) {
                switch (event) {
                    case 2:
                        if (!pullParser.getName().toLowerCase().equals("config")) {
                            String tn = pullParser.getName();
                            this.configValue.put(tn.toLowerCase(), pullParser.nextText());
                            break;
                        }
                        break;
                }
            }
            try {
                inputStream.close();
            } catch (IOException e) {
                Log.e(toString(), e.getMessage());
                e.printStackTrace();
            }
        } catch (XmlPullParserException e2) {
            Log.e(toString(), new StringBuilder().append(e2.getDetail()).toString());
            e2.printStackTrace();
            try {
                inputStream.close();
            } catch (IOException e3) {
                Log.e(toString(), e3.getMessage());
                e3.printStackTrace();
            }
        } catch (IOException e4) {
            Log.e(toString(), e4.getMessage());
            e4.printStackTrace();
            try {
                inputStream.close();
            } catch (IOException e5) {
                Log.e(toString(), e5.getMessage());
                e5.printStackTrace();
            }
        } catch (Throwable th) {
            try {
                inputStream.close();
            } catch (IOException e6) {
                Log.e(toString(), e6.getMessage());
                e6.printStackTrace();
            }
            throw th;
        }
    }

    public void save(Context context) throws IllegalArgumentException, IllegalStateException, IOException {
        FileOutputStream outStream = new FileOutputStream(new File(context.getFilesDir(), "config.xml"));
        XmlSerializer serializer = Xml.newSerializer();
        serializer.setOutput(outStream, JQBasicNetwork.UTF_8);
        serializer.startDocument(JQBasicNetwork.UTF_8, true);
        serializer.startTag(null, "config");
        for (Map.Entry<String, String> item : this.configValue.entrySet()) {
            serializer.startTag(null, (String) item.getKey());
            serializer.text((String) item.getValue());
            serializer.endTag(null, (String) item.getKey());
        }
        serializer.endTag(null, "config");
        serializer.endDocument();
        outStream.flush();
        outStream.close();
    }
}
