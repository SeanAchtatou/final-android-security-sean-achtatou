package com.jianq.common;

import java.util.HashMap;
import java.util.Map;

public class JQObjectStores {
    private static JQObjectStores objStores;
    private Map<String, Object> stores = new HashMap();

    private JQObjectStores() {
    }

    public static JQObjectStores getInst() {
        if (objStores == null) {
            objStores = new JQObjectStores();
        }
        return objStores;
    }

    public void putObject(String key, Object value) {
        this.stores.put(key, value);
    }

    public Object getObject(String key) {
        if (this.stores.containsKey(key)) {
            return this.stores.get(key);
        }
        return null;
    }

    public boolean removeObject(String key) {
        if (!this.stores.containsKey(key)) {
            return false;
        }
        this.stores.remove(key);
        return true;
    }

    public void removeAll() {
        if (this.stores.isEmpty()) {
            this.stores.clear();
        }
    }
}
