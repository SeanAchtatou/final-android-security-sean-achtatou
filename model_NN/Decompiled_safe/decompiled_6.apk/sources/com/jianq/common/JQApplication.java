package com.jianq.common;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.util.Log;
import com.jianq.exception.JQCrashHandler;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class JQApplication extends Application {
    private static final String TAG = "JQApplication";
    private static JQApplication application;
    private List<Activity> activityStack = new LinkedList();

    public void onCreate() {
        super.onCreate();
        JQDeviceInformation.init(getBaseContext());
        try {
            File f = new File(getApplicationContext().getFilesDir(), "config.xml");
            if (f.exists()) {
                JQFrameworkConfig.getInst(new FileInputStream(f));
            } else {
                JQFrameworkConfig.getInst(getBaseContext().getResources().getAssets().open("config.xml"));
                JQFrameworkConfig.getInst().save(getBaseContext());
            }
            JQFileEndings.init(getBaseContext().getResources().getAssets().open("fileendings.xml"));
        } catch (IOException e) {
            Log.e(TAG, "找不到assets/目录下的config.xml或者fileendings.xml配置文件", e);
            e.printStackTrace();
        }
        JQCrashHandler.getInst().init(getApplicationContext());
        application = this;
    }

    public static JQApplication getInst() {
        return application;
    }

    public void addActivity(Activity activity) {
        this.activityStack.add(activity);
    }

    public void remove(Activity activity) {
        if (this.activityStack.contains(activity)) {
            this.activityStack.remove(activity);
        }
    }

    public void restart() {
        for (Activity act : this.activityStack) {
            act.finish();
        }
        Intent i = getBaseContext().getPackageManager().getLaunchIntentForPackage(getBaseContext().getPackageName());
        i.addFlags(67108864);
        startActivity(i);
    }

    public void exit() {
        for (int i = this.activityStack.size() - 1; i > -1; i--) {
            this.activityStack.get(i).finish();
        }
        System.exit(0);
    }
}
