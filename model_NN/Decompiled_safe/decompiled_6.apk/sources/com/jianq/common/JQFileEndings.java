package com.jianq.common;

import android.util.Log;
import android.util.Xml;
import com.jianq.misc.StringEx;
import com.jianq.net.JQBasicNetwork;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class JQFileEndings {
    private static Map<String, String[]> fileEndMap;

    public static void init(InputStream inputStream) {
        fileEndMap = new HashMap();
        buildConfig(inputStream);
    }

    public static String[] getFileEndingsByName(String name) {
        return fileEndMap.get(name);
    }

    private static void buildConfig(InputStream inputStream) {
        XmlPullParser pullParser = Xml.newPullParser();
        try {
            pullParser.setInput(inputStream, JQBasicNetwork.UTF_8);
            String tn = StringEx.Empty;
            List<String> tv = new ArrayList<>();
            for (int event = pullParser.getEventType(); event != 1; event = pullParser.next()) {
                switch (event) {
                    case 2:
                        if (pullParser.getName().toLowerCase().equals("array")) {
                            tn = pullParser.getAttributeValue(0);
                        }
                        if (pullParser.getName().toLowerCase().equals("item")) {
                            tv.add(pullParser.nextText());
                            break;
                        }
                        break;
                    case 3:
                        if (pullParser.getName().toLowerCase().equals("array")) {
                            fileEndMap.put(tn, (String[]) tv.toArray(new String[tv.size()]));
                            tv.clear();
                            break;
                        }
                        break;
                }
            }
            try {
                inputStream.close();
            } catch (IOException e) {
                Log.e("JQFileEndings", e.getMessage());
                e.printStackTrace();
            }
        } catch (XmlPullParserException e2) {
            Log.e("JQFileEndings", new StringBuilder().append(e2.getDetail()).toString());
            e2.printStackTrace();
            try {
                inputStream.close();
            } catch (IOException e3) {
                Log.e("JQFileEndings", e3.getMessage());
                e3.printStackTrace();
            }
        } catch (IOException e4) {
            Log.e("JQFileEndings", e4.getMessage());
            e4.printStackTrace();
            try {
                inputStream.close();
            } catch (IOException e5) {
                Log.e("JQFileEndings", e5.getMessage());
                e5.printStackTrace();
            }
        } catch (Throwable th) {
            try {
                inputStream.close();
            } catch (IOException e6) {
                Log.e("JQFileEndings", e6.getMessage());
                e6.printStackTrace();
            }
            throw th;
        }
    }
}
