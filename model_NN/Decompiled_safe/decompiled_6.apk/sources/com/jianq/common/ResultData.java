package com.jianq.common;

import com.jianq.misc.StringEx;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class ResultData {
    public int error = 0;
    private List<Throwable> errorList = new ArrayList();
    private String errorMessage = StringEx.Empty;
    public boolean isCancel = false;
    private boolean isError = false;
    private String resultString;
    public int what;

    public List<Throwable> getErrors() {
        return this.errorList;
    }

    public void addError(Throwable ex) {
        this.errorList.add(ex);
    }

    public byte[] getData() {
        return this.resultString.getBytes();
    }

    public String getStringData() {
        return this.resultString;
    }

    public JSONObject getJsonData() throws JSONException {
        return new JSONObject(this.resultString);
    }

    public Document getXmlData() throws SAXException, IOException, ParserConfigurationException {
        return DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(this.resultString)));
    }

    public boolean hasError() {
        return this.isError;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public void setIsError(boolean isError2) {
        this.isError = isError2;
    }

    public void setErrorMessage(String errorMessage2) {
        this.errorMessage = errorMessage2;
    }

    public void setResultString(String resultString2) {
        this.resultString = resultString2;
    }
}
