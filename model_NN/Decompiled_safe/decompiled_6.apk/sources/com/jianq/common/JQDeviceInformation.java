package com.jianq.common;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;

public class JQDeviceInformation {
    private static String IMEI;
    private static String OS = "android";
    private static String SDCardPath;
    private static DisplayMetrics mDisplayMetrics;
    private static String model;
    private static boolean sdCardExist;
    private static int sdk;
    private static String version;

    public static void init(Context context) {
        if (context != null) {
            TelephonyManager tm = (TelephonyManager) context.getSystemService("phone");
            sdk = Build.VERSION.SDK_INT;
            IMEI = tm.getDeviceId();
            version = tm.getDeviceSoftwareVersion();
            sdCardExist = Environment.getExternalStorageState().equals("mounted");
            if (sdCardExist) {
                SDCardPath = new StringBuilder().append(Environment.getExternalStorageDirectory()).toString();
            }
            switch (tm.getPhoneType()) {
                case 0:
                    model = "PHONE_TYPE_CDMA";
                    return;
                case 1:
                    model = "PHONE_TYPE_GSM";
                    return;
                case 2:
                    model = "PHONE_TYPE_CDMA";
                    return;
                default:
                    return;
            }
        }
    }

    public static String getIMEI() {
        return IMEI;
    }

    public static String getOS() {
        return OS;
    }

    public static int getSdk() {
        return sdk;
    }

    public static String getVersion() {
        return version;
    }

    public static String getModel() {
        return model;
    }

    public static String getSDCardPath() {
        return SDCardPath;
    }

    public static boolean hasSDCard() {
        return sdCardExist;
    }

    public static int getScreenHeight(Activity act) {
        if (mDisplayMetrics == null) {
            mDisplayMetrics = new DisplayMetrics();
            act.getWindowManager().getDefaultDisplay().getMetrics(mDisplayMetrics);
        }
        return mDisplayMetrics.heightPixels;
    }

    public static int getScreenWidth(Activity act) {
        if (mDisplayMetrics == null) {
            mDisplayMetrics = new DisplayMetrics();
            act.getWindowManager().getDefaultDisplay().getMetrics(mDisplayMetrics);
        }
        return mDisplayMetrics.widthPixels;
    }
}
