package com.jianq.net;

import com.jianq.common.JQKeyValue;
import com.jianq.misc.StringEx;
import java.util.ArrayList;
import java.util.List;

public class JQParameter {
    private List<JQKeyValue> nameValueList = new ArrayList();

    public JQParameter() {
    }

    public JQParameter(List<JQKeyValue> keyValue) {
        this.nameValueList = keyValue;
    }

    public void addParam(String name, String value) {
        this.nameValueList.add(new JQKeyValue(name, value));
    }

    public String toString() {
        String params = StringEx.Empty;
        if (this.nameValueList.size() <= 0) {
            return params;
        }
        for (JQKeyValue keyValue : this.nameValueList) {
            params = String.valueOf(params) + keyValue.toString() + "&";
        }
        return params.substring(0, params.lastIndexOf("&"));
    }
}
