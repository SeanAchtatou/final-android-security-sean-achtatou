package com.jianq.net;

import android.content.Context;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;

public class JQXMasDownloader extends JQHttpDownloader {
    private String postEnc;
    private String postParam;

    public JQXMasDownloader(Context ctx) {
        super(ctx);
    }

    /* access modifiers changed from: protected */
    public InputStream getInputStreamFromUrl(String urlStr) throws MalformedURLException, IOException {
        int indexOf;
        int lastIndexOf = urlStr.lastIndexOf("/") + 1;
        if (urlStr.indexOf("?") == -1) {
            indexOf = urlStr.length();
        } else {
            indexOf = urlStr.indexOf("?");
        }
        String fName = urlStr.substring(lastIndexOf, indexOf);
        String urlStr2 = urlStr.replace(fName, URLEncoder.encode(fName));
        Log.i("HttpDownloader", "urlStr-->" + urlStr2);
        HttpResponse response = JQBasicNetwork.getInstance().getResponse(urlStr2, this.postParam, this.postEnc);
        if (response == null || response.getStatusLine().getStatusCode() != 200) {
            return null;
        }
        HttpEntity httpEntity = response.getEntity();
        InputStream inputStream = httpEntity.getContent();
        this.fileTotalSize = (int) httpEntity.getContentLength();
        sendMessage(0);
        return inputStream;
    }

    public void setPostEncoding(String enc) {
        this.postEnc = enc;
    }

    public void downloadFile(String url, String postParam2, String filePath, boolean isOpen) {
        this.postParam = postParam2;
        downloadFile(url, filePath, isOpen);
    }
}
