package com.jianq.net;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;
import cn.com.jit.pnxclient.constant.PNXConfigConstant;
import com.jianq.helper.JQOpenFileHelper;
import com.jianq.util.JQFileUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

public class JQHttpDownloader {
    protected static final int DOWNLOADED = 2;
    protected static final int DOWNLOADING = 1;
    protected static final int DOWNLOAD_ERROR = -1;
    protected static final int PREV_DOWNLOAD = 3;
    protected static final int START_DOWNLOAD = 0;
    protected Context ctx;
    protected ProgressDialog downloadDialog;
    protected int downloadedSize = 0;
    protected String errorMsg;
    protected File file;
    protected int fileTotalSize;
    protected String fullFilePath;
    protected boolean isOpen;
    protected Handler mainThreadHandler = new Handler(Looper.getMainLooper()) {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case -1:
                    if (JQHttpDownloader.this.downloadDialog.isShowing()) {
                        JQHttpDownloader.this.downloadDialog.dismiss();
                    }
                    Toast.makeText(JQHttpDownloader.this.ctx, JQHttpDownloader.this.errorMsg, 0).show();
                    if (JQHttpDownloader.this.file != null) {
                        JQFileUtils.delete(JQHttpDownloader.this.file.toString());
                        break;
                    }
                    break;
                case 0:
                    JQHttpDownloader.this.downloadDialog.setMax(JQHttpDownloader.this.fileTotalSize);
                    break;
                case 1:
                    JQHttpDownloader.this.downloadDialog.setProgress(JQHttpDownloader.this.downloadedSize);
                    break;
                case 2:
                    JQHttpDownloader.this.downloadDialog.dismiss();
                    if (JQHttpDownloader.this.file.exists()) {
                        Toast.makeText(JQHttpDownloader.this.ctx, "文件下载完成", 0).show();
                        if (JQHttpDownloader.this.isOpen) {
                            new JQOpenFileHelper(JQHttpDownloader.this.ctx).openFile(JQHttpDownloader.this.file);
                            break;
                        }
                    }
                    break;
                case 3:
                    JQHttpDownloader.this.downloadDialog.show();
                    break;
            }
            super.handleMessage(msg);
        }
    };
    protected URL url = null;

    public JQHttpDownloader(Context ctx2) {
        this.ctx = ctx2;
    }

    /* access modifiers changed from: protected */
    public void sendMessage(int flag) {
        Message msg = this.mainThreadHandler.obtainMessage();
        msg.what = flag;
        this.mainThreadHandler.sendMessage(msg);
    }

    /* access modifiers changed from: protected */
    public void write2SDFromInput(InputStream input) {
        FileOutputStream output = null;
        Log.i("HttpDownloader", this.fullFilePath);
        try {
            FileOutputStream output2 = new FileOutputStream(this.file);
            try {
                byte[] buffer = new byte[4096];
                while (true) {
                    int size = input.read(buffer);
                    if (size != -1) {
                        if (!this.file.exists()) {
                            break;
                        }
                        output2.write(buffer, 0, size);
                        this.downloadedSize += size;
                        sendMessage(1);
                    } else {
                        break;
                    }
                }
                output2.flush();
                sendMessage(2);
                try {
                    output2.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e2) {
                e = e2;
                output = output2;
                try {
                    e.printStackTrace();
                    try {
                        output.close();
                    } catch (Exception e3) {
                        e3.printStackTrace();
                    }
                } catch (Throwable th) {
                    th = th;
                    try {
                        output.close();
                    } catch (Exception e4) {
                        e4.printStackTrace();
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                output = output2;
                output.close();
                throw th;
            }
        } catch (Exception e5) {
            e = e5;
            e.printStackTrace();
            output.close();
        }
    }

    /* access modifiers changed from: protected */
    public void startDownload(final String urlStr) {
        this.downloadDialog = new ProgressDialog(this.ctx);
        this.downloadDialog.setProgressStyle(1);
        this.downloadDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                JQHttpDownloader.this.errorMsg = "下载已经取消...";
                JQHttpDownloader.this.sendMessage(-1);
            }
        });
        sendMessage(3);
        new Thread(new Runnable() {
            public void run() {
                InputStream inputStream = null;
                try {
                    inputStream = JQHttpDownloader.this.getInputStreamFromUrl(urlStr);
                    JQHttpDownloader.this.write2SDFromInput(inputStream);
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (Exception e) {
                            JQHttpDownloader.this.errorMsg = "URL出错，请检查能否正常访问到下载URL";
                            JQHttpDownloader.this.sendMessage(-1);
                            Log.d("HttpDownloader.URL", urlStr);
                        }
                    }
                } catch (Exception e2) {
                    JQHttpDownloader.this.errorMsg = "URL出错，请检查能否正常访问到下载URL";
                    JQHttpDownloader.this.sendMessage(-1);
                    Log.d("HttpDownloader.URL", urlStr);
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (Exception e3) {
                            JQHttpDownloader.this.errorMsg = "URL出错，请检查能否正常访问到下载URL";
                            JQHttpDownloader.this.sendMessage(-1);
                            Log.d("HttpDownloader.URL", urlStr);
                        }
                    }
                } catch (Throwable th) {
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (Exception e4) {
                            JQHttpDownloader.this.errorMsg = "URL出错，请检查能否正常访问到下载URL";
                            JQHttpDownloader.this.sendMessage(-1);
                            Log.d("HttpDownloader.URL", urlStr);
                        }
                    }
                    throw th;
                }
            }
        }).start();
    }

    /* access modifiers changed from: protected */
    public InputStream getInputStreamFromUrl(String urlStr) throws MalformedURLException, IOException {
        String fName = urlStr.substring(urlStr.lastIndexOf("/") + 1, urlStr.indexOf("?") == -1 ? urlStr.length() : urlStr.indexOf("?"));
        String urlStr2 = urlStr.replace(fName, URLEncoder.encode(fName));
        Log.i("HttpDownloader", "urlStr-->" + urlStr2);
        this.url = new URL(urlStr2);
        URLConnection urlConn = this.url.openConnection();
        urlConn.connect();
        InputStream inputStream = urlConn.getInputStream();
        this.fileTotalSize = urlConn.getContentLength();
        sendMessage(0);
        return inputStream;
    }

    public void downloadFile(String url2, String filePath, boolean isOpen2) {
        this.isOpen = isOpen2;
        this.fullFilePath = filePath;
        File file2 = new File(filePath);
        if (file2.exists()) {
            this.file = file2;
            if (isOpen2) {
                Toast.makeText(this.ctx, "文件已经存在，系统将自动为您打开", 0).show();
                new JQOpenFileHelper(this.ctx).openFile(file2);
                return;
            }
            Toast.makeText(this.ctx, "该文件已经存在...", 0).show();
        } else if (file2.isDirectory()) {
            this.errorMsg = "存储对象必须是文件，请联系相关开发人员";
            sendMessage(-1);
        } else {
            if (!new File(file2.getParent()).exists()) {
                JQFileUtils.createDir(file2.getParent());
            }
            try {
                this.file = JQFileUtils.createFile(filePath);
                startDownload(url2);
            } catch (IOException e) {
                this.errorMsg = "创建文件失败";
                sendMessage(-1);
            }
        }
    }

    public String downloadText(String urlStr) {
        StringBuffer sb = new StringBuffer();
        BufferedReader buffer = null;
        try {
            this.url = new URL(urlStr);
            HttpURLConnection urlConn = (HttpURLConnection) this.url.openConnection();
            BufferedReader buffer2 = new BufferedReader(new InputStreamReader(urlConn.getInputStream(), "GBK"));
            while (true) {
                try {
                    String line = buffer2.readLine();
                    if (line == null) {
                        break;
                    }
                    sb.append(String.valueOf(line) + PNXConfigConstant.RESP_SPLIT_2);
                } catch (Exception e) {
                    e = e;
                    buffer = buffer2;
                } catch (Throwable th) {
                    th = th;
                    buffer = buffer2;
                    try {
                        buffer.close();
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    throw th;
                }
            }
            urlConn.disconnect();
            try {
                buffer2.close();
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        } catch (Exception e4) {
            e = e4;
            try {
                e.printStackTrace();
                try {
                    buffer.close();
                } catch (Exception e5) {
                    e5.printStackTrace();
                }
                return sb.toString();
            } catch (Throwable th2) {
                th = th2;
                buffer.close();
                throw th;
            }
        }
        return sb.toString();
    }
}
