package com.jianq.net;

import android.util.Log;
import com.jianq.misc.StringEx;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

public class JQBasicNetwork {
    public static final String ASCII = "ASCII";
    public static final String CHARSET_PARAM = "; charset=";
    public static final String CHUNK_CODING = "chunked";
    public static final String CONN_CLOSE = "Close";
    public static final String CONN_DIRECTIVE = "Connection";
    public static final String CONN_KEEP_ALIVE = "Keep-Alive";
    public static final String CONTENT_ENCODING = "Content-Encoding";
    public static final String CONTENT_LEN = "Content-Length";
    public static final String CONTENT_TYPE = "Content-Type";
    public static final int CR = 13;
    public static final String DATE_HEADER = "Date";
    public static final String DEFAULT_CONTENT_CHARSET = "ISO-8859-1";
    public static final String DEFAULT_CONTENT_TYPE = "application/octet-stream";
    public static final String DEFAULT_PROTOCOL_CHARSET = "US-ASCII";
    public static final String EXPECT_CONTINUE = "100-continue";
    public static final String EXPECT_DIRECTIVE = "Expect";
    public static final int HT = 9;
    public static final String IDENTITY_CODING = "identity";
    public static final String ISO_8859_1 = "ISO-8859-1";
    public static final int LF = 10;
    public static final String OCTET_STREAM_TYPE = "application/octet-stream";
    public static final String PLAIN_TEXT_TYPE = "text/plain";
    public static final String SERVER_HEADER = "Server";
    public static final int SP = 32;
    public static final String TARGET_HOST = "Host";
    protected static final int TIMEOUT = 60000;
    public static final String TRANSFER_ENCODING = "Transfer-Encoding";
    public static final String USER_AGENT = "User-Agent";
    public static final String US_ASCII = "US-ASCII";
    public static final String UTF_16 = "UTF-16";
    public static final String UTF_8 = "UTF-8";
    protected static volatile JQBasicNetwork basicNetwork;
    private String cookie;
    protected HttpClient httpClient;
    protected HttpEntity httpEntity;
    protected HttpPost httpPost;
    protected HttpResponse response;
    private String xmasSessionId = null;

    protected JQBasicNetwork() {
        HttpParams httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParams, TIMEOUT);
        HttpConnectionParams.setSoTimeout(httpParams, TIMEOUT);
        this.httpClient = new DefaultHttpClient(httpParams);
    }

    public static JQBasicNetwork getInstance() {
        if (basicNetwork == null) {
            synchronized (JQBasicNetwork.class) {
                if (basicNetwork == null) {
                    basicNetwork = new JQBasicNetwork();
                }
            }
        }
        return basicNetwork;
    }

    public HttpResponse getResponse(String requestUrl, List<NameValuePair> parameter) throws UnsupportedEncodingException, ClientProtocolException, IOException {
        return getResponse(requestUrl, parameter, (String) null);
    }

    public HttpResponse getResponse(String requestUrl, List<NameValuePair> parameter, String encoding) throws UnsupportedEncodingException, ClientProtocolException, IOException {
        String paras = StringEx.Empty;
        for (NameValuePair pair : parameter) {
            paras = String.valueOf(paras) + pair.getName() + "=" + pair.getValue() + "&";
        }
        if (!paras.equals(StringEx.Empty)) {
            paras = paras.substring(0, paras.lastIndexOf("&"));
        }
        return getResponse(requestUrl, paras, encoding);
    }

    public HttpResponse getResponse(String requestUrl, String parameter, String encoding) throws UnsupportedEncodingException, ClientProtocolException, IOException, IllegalArgumentException {
        Log.d("BasicNetwork.requestUrl", requestUrl);
        Log.d("BasicNetwork.parameter", parameter);
        this.httpPost = new HttpPost(requestUrl);
        String enc = UTF_8;
        if (!(encoding == null || encoding == StringEx.Empty)) {
            enc = encoding;
        }
        if (this.cookie != null && !this.cookie.equals(StringEx.Empty)) {
            this.httpPost.setHeader("Cookie", this.cookie);
        }
        if (this.xmasSessionId != null && !this.xmasSessionId.equals(StringEx.Empty)) {
            this.httpPost.setHeader("xmas-session", this.xmasSessionId);
        }
        this.httpEntity = new StringEntity(parameter, enc);
        this.httpPost.setEntity(this.httpEntity);
        this.response = this.httpClient.execute(this.httpPost);
        List<Cookie> cookies = this.httpClient.getCookieStore().getCookies();
        if (this.cookie == null && cookies.size() > 0) {
            this.cookie = StringEx.Empty;
            for (Cookie c : cookies) {
                this.cookie = String.valueOf(this.cookie) + c.getName() + "=" + c.getValue() + "; ";
            }
            if (!this.cookie.equals(StringEx.Empty)) {
                this.cookie = this.cookie.substring(0, this.cookie.lastIndexOf(";"));
            }
            Log.d("BasicNetwork.cookie", this.cookie);
        }
        return this.response;
    }

    public HttpResponse getUploadResponse(String requestUrl, String parameter, String[] filenames, String fileType, String encoding) throws ClientProtocolException, IOException {
        StringBody stringBody;
        FileBody fileBody;
        this.httpPost = new HttpPost(requestUrl);
        if (this.cookie != null && !this.cookie.equals(StringEx.Empty)) {
            this.httpPost.setHeader("Cookie", this.cookie);
        }
        if (this.xmasSessionId != null && !this.xmasSessionId.equals(StringEx.Empty)) {
            this.httpPost.setHeader("xmas-session", this.xmasSessionId);
        }
        MultipartEntity reqEntity = new MultipartEntity();
        for (int i = 0; i < filenames.length; i++) {
            if (encoding == null || encoding.equals(StringEx.Empty)) {
                fileBody = new FileBody(new File(filenames[i]), fileType);
            } else {
                fileBody = new FileBody(new File(filenames[i]), fileType, encoding);
            }
            reqEntity.addPart("upFile", fileBody);
        }
        if (encoding == null || encoding.equals(StringEx.Empty)) {
            stringBody = new StringBody(parameter);
        } else {
            stringBody = new StringBody(parameter, Charset.forName(encoding));
        }
        reqEntity.addPart("xmas-json", stringBody);
        this.httpPost.setEntity(reqEntity);
        this.response = this.httpClient.execute(this.httpPost);
        List<Cookie> cookies = this.httpClient.getCookieStore().getCookies();
        if (this.cookie == null && cookies.size() > 0) {
            this.cookie = StringEx.Empty;
            for (Cookie c : cookies) {
                this.cookie = String.valueOf(this.cookie) + c.getName() + "=" + c.getValue() + "; ";
            }
            if (!this.cookie.equals(StringEx.Empty)) {
                this.cookie = this.cookie.substring(0, this.cookie.lastIndexOf(";"));
            }
            Log.d("BasicNetwork.cookie", this.cookie);
        }
        return this.response;
    }

    public void setXMasSessionId(String sessionId) {
        this.xmasSessionId = sessionId;
    }

    public String getXMasSessionId() {
        return this.xmasSessionId;
    }

    public void cancelHttpPost() {
        this.httpPost.abort();
    }

    public static void reset() {
        basicNetwork = new JQBasicNetwork();
    }
}
