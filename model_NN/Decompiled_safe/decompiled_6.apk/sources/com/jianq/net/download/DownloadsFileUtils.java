package com.jianq.net.download;

import android.content.Context;
import android.text.TextUtils;
import com.jianq.util.DeviceUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.Properties;
import java.util.regex.Pattern;

public class DownloadsFileUtils {
    private static NumberFormat mFileSizeNumberFormat = NumberFormat.getNumberInstance();

    private static File detectPath(String filePath, String fileName) throws IOException {
        if (filePath == null || fileName == null) {
            return null;
        }
        File file = new File(filePath);
        if (!file.exists()) {
            file.mkdirs();
        }
        File file2 = new File(String.valueOf(filePath) + File.separator + fileName);
        if (file2.exists()) {
            return file2;
        }
        file2.createNewFile();
        return file2;
    }

    public static File detectPath(Context context, String filePath) throws IOException, RuntimeException {
        if (!DeviceUtils.checkSDCard()) {
            throw new RuntimeException("sdcard is unavailable");
        } else if (!isFilePath(filePath)) {
            throw new RuntimeException("It is not valid file path: " + filePath);
        } else {
            int divide = filePath.lastIndexOf(File.separator);
            return detectPath(filePath.substring(0, divide), filePath.substring(File.separator.length() + divide));
        }
    }

    public static boolean isExistsFile(Context context, String filePath) throws RuntimeException {
        if (!DeviceUtils.checkSDCard()) {
            throw new RuntimeException("sdcard is unavailable");
        } else if (isFilePath(filePath)) {
            return new File(filePath).exists();
        } else {
            throw new RuntimeException("It is not valid file path: " + filePath);
        }
    }

    public static boolean write(Context context, String filePath, Properties pro, String comment) throws FileNotFoundException, IOException, RuntimeException {
        if (detectPath(context, filePath) == null) {
            return false;
        }
        pro.store(new FileOutputStream(filePath), comment);
        return true;
    }

    public static Properties read(Context context, String filePath) throws FileNotFoundException, IOException, RuntimeException {
        if (!DeviceUtils.checkSDCard()) {
            throw new RuntimeException("Sdcard is unavailable");
        } else if (!isFilePath(filePath)) {
            throw new RuntimeException("It is not valid file path: " + filePath);
        } else {
            Properties pro = new Properties();
            pro.load(new FileInputStream(filePath));
            return pro;
        }
    }

    public static boolean deleteFile(Context context, String filePath) throws RuntimeException {
        if (!DeviceUtils.checkSDCard()) {
            throw new RuntimeException("sdcard is unavailable");
        } else if (!isFilePath(filePath)) {
            throw new RuntimeException("It is not valid file path: " + filePath);
        } else {
            File file = new File(filePath);
            if (!file.exists()) {
                return false;
            }
            if (!file.isFile()) {
                return deleteDirectory(context, filePath);
            }
            file.delete();
            return true;
        }
    }

    private static boolean deleteDirectory(Context context, String sPath) {
        if (!sPath.endsWith(File.separator)) {
            sPath = String.valueOf(sPath) + File.separator;
        }
        File dirFile = new File(sPath);
        if (!dirFile.exists() || !dirFile.isDirectory()) {
            return false;
        }
        boolean flag = true;
        File[] files = dirFile.listFiles();
        for (int i = 0; i < files.length; i++) {
            if (files[i].isFile()) {
                flag = deleteFile(context, files[i].getAbsolutePath());
                if (!flag) {
                    break;
                }
            } else {
                flag = deleteDirectory(context, files[i].getAbsolutePath());
                if (!flag) {
                    break;
                }
            }
        }
        if (!flag || !dirFile.delete()) {
            return false;
        }
        return true;
    }

    public static boolean fileRename(Context context, String oldPath, String newPath) throws RuntimeException {
        if (!DeviceUtils.checkSDCard()) {
            throw new RuntimeException("sdcard is unavailable");
        } else if (!isFilePath(oldPath)) {
            throw new RuntimeException("It is not valid file path: " + oldPath);
        } else if (isFilePath(newPath)) {
            return new File(oldPath).renameTo(new File(newPath));
        } else {
            throw new RuntimeException("It is not valid file path: " + newPath);
        }
    }

    public static boolean isFilePath(String filePath) {
        if (TextUtils.isEmpty(filePath)) {
            return false;
        }
        return Pattern.compile("(^//.|^/|^[a-zA-Z])?:?/.+(/$)?").matcher(filePath.replaceAll("//", "/").trim()).matches();
    }

    public static String getFileSizeFriendly(long fileSize) {
        String suffix = "bytes";
        double size = (double) fileSize;
        mFileSizeNumberFormat.setMaximumFractionDigits(2);
        if (fileSize >= 1024 && fileSize < 1048576) {
            size = ((double) fileSize) / 1024.0d;
            suffix = "KB";
        } else if (fileSize >= 1048576 && fileSize < 1073741824) {
            size = ((double) fileSize) / 1048576.0d;
            suffix = "MB";
        } else if (fileSize >= 1073741824) {
            size = ((double) fileSize) / 1.073741824E9d;
            suffix = "GB";
        }
        return String.valueOf(mFileSizeNumberFormat.format(size)) + " " + suffix;
    }
}
