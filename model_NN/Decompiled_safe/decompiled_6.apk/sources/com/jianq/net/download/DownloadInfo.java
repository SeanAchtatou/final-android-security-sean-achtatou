package com.jianq.net.download;

public class DownloadInfo {
    public static final int STATUS_DOWNLOAD = 2;
    public static final int STATUS_END = 3;
    public static final int STATUS_EXCEPTION = -1;
    public static final int STATUS_LINK = 1;
    public static final String SUFFIX_CONFIG = ".cfg";
    public static final String SUFFIX_FILE = ".td";
    public String configPath;
    public int contentLength;
    public int downloadLength;
    public String fileName;
    public String finalPath;
    public boolean isFileAgain;
    public boolean isFileExist;
    public boolean isLife = true;
    public int priority;
    public int status;
    public String tempPath;
    public String url;
}
