package com.jianq.net.download;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.widget.ProgressBar;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;
import com.jianq.R;
import com.jianq.log.JQLogger;
import java.text.NumberFormat;

public class DownloadManager implements OnDownloadListener {
    private static final int NOTIFY_ID = 999;
    private JQLogger LOG = JQLogger.getLogger(DownloadManager.class, true);
    private boolean isShowNotification = true;
    private Context mContext;
    private ProgressDialog mDownloadProgressDialog;
    private DownloadTask mDownloadTask;
    private Intent mIntentWhenCompleted;
    private NotificationManager mNotifyMananger;
    private TextView mPercentage;
    private TextView mProgessText;
    private ProgressBar mProgressBar;
    private NumberFormat mProgressPercentFormat;
    private Notification mProgressUpdateNotification;

    public DownloadManager(Context ctx, boolean isShowNotification2) {
        this.mContext = ctx;
        this.mProgressPercentFormat = NumberFormat.getPercentInstance();
        this.mProgressPercentFormat.setMaximumFractionDigits(0);
        this.isShowNotification = isShowNotification2;
        this.mIntentWhenCompleted = new Intent("android.intent.action.VIEW");
        this.mIntentWhenCompleted.setFlags(268435456);
    }

    public void startDownload(String downloadUrl, String savePath) {
        this.mDownloadTask = new DownloadTask(this.mContext, true, this);
        this.mDownloadTask.download(downloadUrl, savePath);
    }

    public void setIntentWhenCompleted(Intent intent) {
        this.mIntentWhenCompleted = intent;
        this.mIntentWhenCompleted.setFlags(268435456);
    }

    public void stop() {
        if (this.mDownloadTask != null && !this.mDownloadTask.isCancelled()) {
            this.mDownloadTask.stop();
            this.mDownloadTask.cancel(true);
            this.mDownloadTask = null;
        }
        if (this.mNotifyMananger != null) {
            this.mNotifyMananger.cancel(NOTIFY_ID);
        }
    }

    public void onException(Exception e) {
        this.LOG.e(e.getMessage());
        Toast.makeText(this.mContext, this.mContext.getString(R.string.txt_download_exc, e.getMessage()), 1).show();
    }

    public void onPreExecute(DownloadInfo info) {
        this.mDownloadProgressDialog = new ProgressDialog(this.mContext);
        this.mDownloadProgressDialog.setProgressStyle(1);
        this.mDownloadProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                DownloadManager.this.stop();
            }
        });
        this.mDownloadProgressDialog.setTitle(R.string.downloading);
        this.mDownloadProgressDialog.show();
        this.mDownloadProgressDialog.setContentView(R.layout.jq_progress_dialog_downloading);
        this.mPercentage = (TextView) this.mDownloadProgressDialog.findViewById(R.id.percentage);
        this.mProgressBar = (ProgressBar) this.mDownloadProgressDialog.findViewById(R.id.progress);
        this.mProgessText = (TextView) this.mDownloadProgressDialog.findViewById(R.id.message);
        this.mProgressBar.setMax(100);
        if (this.isShowNotification) {
            initNotification();
        }
    }

    private void initNotification() {
        this.mProgressUpdateNotification = new Notification();
        this.mProgressUpdateNotification.contentView = new RemoteViews(this.mContext.getPackageName(), R.layout.jq_progress_dialog_downloading);
        this.mProgressUpdateNotification.flags |= 16;
        this.mProgressUpdateNotification.icon = R.drawable.jq_downloading;
        this.mProgressUpdateNotification.contentIntent = PendingIntent.getActivity(this.mContext, 0, this.mIntentWhenCompleted, 0);
        this.mNotifyMananger = (NotificationManager) this.mContext.getSystemService("notification");
        this.mNotifyMananger.notify(NOTIFY_ID, this.mProgressUpdateNotification);
    }

    public void onProgressUpdate(DownloadInfo info) {
        if (info != null && info.contentLength > 0) {
            String message = String.valueOf(DownloadsFileUtils.getFileSizeFriendly((long) info.downloadLength)) + "/" + DownloadsFileUtils.getFileSizeFriendly((long) info.contentLength);
            if (info.downloadLength == info.contentLength) {
                message = this.mContext.getString(R.string.download_completed);
            }
            this.mProgessText.setText(message);
            double percent = ((double) info.downloadLength) / ((double) info.contentLength);
            SpannableString tmp = new SpannableString(this.mProgressPercentFormat.format(percent));
            tmp.setSpan(new StyleSpan(1), 0, tmp.length(), 33);
            this.mPercentage.setText(tmp);
            this.mProgressBar.setProgress((int) (percent * 100.0d));
            if (this.mProgressUpdateNotification != null) {
                this.mProgressUpdateNotification.contentView.setProgressBar(R.id.progress, 100, (int) (percent * 100.0d), false);
                this.mProgressUpdateNotification.contentView.setTextViewText(R.id.message, message);
                this.mProgressUpdateNotification.contentView.setTextViewText(R.id.percentage, tmp);
                this.mNotifyMananger.notify(NOTIFY_ID, this.mProgressUpdateNotification);
            }
        }
    }

    public void onPostExecute(DownloadInfo info) {
        this.mDownloadProgressDialog.dismiss();
    }
}
