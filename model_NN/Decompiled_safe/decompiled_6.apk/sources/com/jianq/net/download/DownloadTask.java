package com.jianq.net.download;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import com.jianq.misc.StringEx;

@SuppressLint({"HandlerLeak"})
public class DownloadTask extends AsyncTask<DownloadInfo, DownloadInfo, DownloadInfo> {
    public static final String CFG_COMMENT = "Only support  ISO–8859–1, please do not use Chinese.";
    private static final String LOG_TAG = "DownloadTask";
    private boolean isResume;
    private Context mContext;
    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (DownloadTask.this.mListener != null && msg.obj != null && (msg.obj instanceof Exception)) {
                DownloadTask.this.mListener.onException((Exception) msg.obj);
            }
        }
    };
    private DownloadInfo mInfo;
    /* access modifiers changed from: private */
    public OnDownloadListener mListener;

    public DownloadTask(Context context, boolean isResume2, OnDownloadListener listener) {
        this.mContext = context;
        this.mListener = listener;
        this.isResume = isResume2;
    }

    public DownloadInfo getDownloadInfo() {
        return this.mInfo;
    }

    public void stop() {
        this.mInfo.isLife = false;
    }

    public void download(String url, String filePath) {
        if (this.mContext == null) {
            Log.w(LOG_TAG, StringEx.Empty, new RuntimeException("Context is null"));
        } else if (TextUtils.isEmpty(url) || TextUtils.isEmpty(filePath)) {
            Log.w(LOG_TAG, StringEx.Empty, new RuntimeException("Url or filePath is NULL."));
        } else if (!DownloadsFileUtils.isFilePath(filePath)) {
            Log.w(LOG_TAG, StringEx.Empty, new RuntimeException("It is not valid file path: " + filePath));
        } else {
            this.mInfo = new DownloadInfo();
            this.mInfo.url = url;
            this.mInfo.finalPath = filePath;
            execute(this.mInfo);
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        if (this.mListener != null) {
            this.mListener.onPreExecute(this.mInfo);
        }
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(DownloadInfo... values) {
        if (this.mListener != null) {
            this.mListener.onProgressUpdate(values[0]);
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(DownloadInfo result) {
        if (this.mListener != null) {
            this.mListener.onPostExecute(result);
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:110:?, code lost:
        return r7;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.jianq.net.download.DownloadInfo doInBackground(com.jianq.net.download.DownloadInfo... r19) {
        /*
            r18 = this;
            r0 = r18
            android.content.Context r14 = r0.mContext
            if (r14 != 0) goto L_0x0016
            java.lang.String r14 = "DownloadTask"
            java.lang.String r15 = ""
            java.lang.RuntimeException r16 = new java.lang.RuntimeException
            java.lang.String r17 = "Context is null"
            r16.<init>(r17)
            android.util.Log.w(r14, r15, r16)
            r7 = 0
        L_0x0015:
            return r7
        L_0x0016:
            if (r19 == 0) goto L_0x0022
            r0 = r19
            int r14 = r0.length
            if (r14 == 0) goto L_0x0022
            r14 = 0
            r14 = r19[r14]
            if (r14 != 0) goto L_0x0032
        L_0x0022:
            java.lang.String r14 = "DownloadTask"
            java.lang.String r15 = ""
            java.lang.NullPointerException r16 = new java.lang.NullPointerException
            java.lang.String r17 = "params is null."
            r16.<init>(r17)
            android.util.Log.w(r14, r15, r16)
            r7 = 0
            goto L_0x0015
        L_0x0032:
            r8 = 0
            r4 = 0
            r14 = 0
            r7 = r19[r14]
            r14 = 1
            r7.status = r14
            r14 = 1
            com.jianq.net.download.DownloadInfo[] r14 = new com.jianq.net.download.DownloadInfo[r14]
            r15 = 0
            r14[r15] = r7
            r0 = r18
            r0.publishProgress(r14)
            r0 = r18
            android.content.Context r14 = r0.mContext     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            com.jianq.net.HttpConnectionUtils.stuffDownloadInfo(r14, r7)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r14 = r7.finalPath     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r15 = java.io.File.separator     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            boolean r14 = r14.endsWith(r15)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            if (r14 != 0) goto L_0x006d
            java.lang.String r14 = r7.finalPath     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r14 = java.lang.String.valueOf(r14)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            r15.<init>(r14)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r14 = java.io.File.separator     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.StringBuilder r14 = r15.append(r14)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r14 = r14.toString()     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            r7.finalPath = r14     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
        L_0x006d:
            java.lang.String r14 = r7.finalPath     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r14 = java.lang.String.valueOf(r14)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            r15.<init>(r14)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r14 = r7.fileName     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.StringBuilder r14 = r15.append(r14)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r14 = r14.toString()     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            r7.finalPath = r14     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r15 = r7.finalPath     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r15 = java.lang.String.valueOf(r15)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            r14.<init>(r15)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r15 = ".td"
            java.lang.StringBuilder r14 = r14.append(r15)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r14 = r14.toString()     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            r7.tempPath = r14     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r15 = r7.tempPath     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r15 = java.lang.String.valueOf(r15)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            r14.<init>(r15)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r15 = ".cfg"
            java.lang.StringBuilder r14 = r14.append(r15)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r14 = r14.toString()     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            r7.configPath = r14     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            r0 = r18
            android.content.Context r14 = r0.mContext     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r15 = r7.configPath     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            boolean r14 = com.jianq.net.download.DownloadsFileUtils.isExistsFile(r14, r15)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            if (r14 == 0) goto L_0x01be
            r0 = r18
            boolean r14 = r0.isResume     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            if (r14 == 0) goto L_0x0108
            r0 = r18
            android.content.Context r14 = r0.mContext     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r15 = r7.configPath     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.util.Properties r11 = com.jianq.net.download.DownloadsFileUtils.read(r14, r15)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r14 = r7.fileName     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r15 = "_KEY_FILENAME_NETWORK"
            java.lang.String r15 = r11.getProperty(r15)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            boolean r14 = r14.equalsIgnoreCase(r15)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            if (r14 == 0) goto L_0x0108
            int r14 = r7.contentLength     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r14 = java.lang.String.valueOf(r14)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r15 = "_KEY_LENGTH_CONTENT"
            java.lang.String r15 = r11.getProperty(r15)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            boolean r14 = r14.equalsIgnoreCase(r15)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            if (r14 == 0) goto L_0x0108
            r0 = r18
            android.content.Context r14 = r0.mContext     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r15 = r7.tempPath     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            boolean r14 = com.jianq.net.download.DownloadsFileUtils.isExistsFile(r14, r15)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            if (r14 == 0) goto L_0x0108
            java.lang.String r14 = "_KEY_LENGTH_DOWNLOAD"
            java.lang.String r15 = "0"
            java.lang.String r14 = r11.getProperty(r14, r15)     // Catch:{ NumberFormatException -> 0x0197 }
            int r14 = java.lang.Integer.parseInt(r14)     // Catch:{ NumberFormatException -> 0x0197 }
            r7.downloadLength = r14     // Catch:{ NumberFormatException -> 0x0197 }
        L_0x0108:
            r0 = r18
            android.content.Context r14 = r0.mContext     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            org.apache.http.client.HttpClient r4 = com.jianq.net.HttpClientUtils.buildHttpClient(r14)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            org.apache.http.client.methods.HttpGet r5 = new org.apache.http.client.methods.HttpGet     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r14 = r7.url     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            r5.<init>(r14)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r14 = "Range"
            java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r16 = "bytes="
            r15.<init>(r16)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            int r0 = r7.downloadLength     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            r16 = r0
            java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r16 = "-"
            java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            int r0 = r7.contentLength     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            r16 = r0
            java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r15 = r15.toString()     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            r5.addHeader(r14, r15)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            org.apache.http.HttpResponse r12 = r4.execute(r5)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            org.apache.http.StatusLine r14 = r12.getStatusLine()     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            int r14 = r14.getStatusCode()     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            r15 = 200(0xc8, float:2.8E-43)
            if (r14 == r15) goto L_0x01ea
            org.apache.http.StatusLine r14 = r12.getStatusLine()     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            int r14 = r14.getStatusCode()     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            r15 = 206(0xce, float:2.89E-43)
            if (r14 == r15) goto L_0x01ea
            java.lang.RuntimeException r14 = new java.lang.RuntimeException     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r16 = "Response StatusCode:"
            r15.<init>(r16)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            org.apache.http.StatusLine r16 = r12.getStatusLine()     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            int r16 = r16.getStatusCode()     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r15 = r15.toString()     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            r14.<init>(r15)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            throw r14     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
        L_0x0176:
            r2 = move-exception
            android.os.Message r9 = new android.os.Message     // Catch:{ all -> 0x01f8 }
            r9.<init>()     // Catch:{ all -> 0x01f8 }
            r9.obj = r2     // Catch:{ all -> 0x02d1 }
            r0 = r18
            android.os.Handler r14 = r0.mHandler     // Catch:{ all -> 0x02d1 }
            r14.sendMessage(r9)     // Catch:{ all -> 0x02d1 }
            if (r4 == 0) goto L_0x02d5
            org.apache.http.conn.ClientConnectionManager r14 = r4.getConnectionManager()
            if (r14 == 0) goto L_0x02d5
            org.apache.http.conn.ClientConnectionManager r14 = r4.getConnectionManager()
            r14.shutdown()
            r8 = r9
            goto L_0x0015
        L_0x0197:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            goto L_0x0108
        L_0x019d:
            r2 = move-exception
            android.os.Message r9 = new android.os.Message     // Catch:{ all -> 0x01f8 }
            r9.<init>()     // Catch:{ all -> 0x01f8 }
            r9.obj = r2     // Catch:{ all -> 0x02d1 }
            r0 = r18
            android.os.Handler r14 = r0.mHandler     // Catch:{ all -> 0x02d1 }
            r14.sendMessage(r9)     // Catch:{ all -> 0x02d1 }
            if (r4 == 0) goto L_0x02d5
            org.apache.http.conn.ClientConnectionManager r14 = r4.getConnectionManager()
            if (r14 == 0) goto L_0x02d5
            org.apache.http.conn.ClientConnectionManager r14 = r4.getConnectionManager()
            r14.shutdown()
            r8 = r9
            goto L_0x0015
        L_0x01be:
            r0 = r18
            android.content.Context r14 = r0.mContext     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r15 = r7.configPath     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            com.jianq.net.download.DownloadsFileUtils.detectPath(r14, r15)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            goto L_0x0108
        L_0x01c9:
            r2 = move-exception
            android.os.Message r9 = new android.os.Message     // Catch:{ all -> 0x01f8 }
            r9.<init>()     // Catch:{ all -> 0x01f8 }
            r9.obj = r2     // Catch:{ all -> 0x02d1 }
            r0 = r18
            android.os.Handler r14 = r0.mHandler     // Catch:{ all -> 0x02d1 }
            r14.sendMessage(r9)     // Catch:{ all -> 0x02d1 }
            if (r4 == 0) goto L_0x02d5
            org.apache.http.conn.ClientConnectionManager r14 = r4.getConnectionManager()
            if (r14 == 0) goto L_0x02d5
            org.apache.http.conn.ClientConnectionManager r14 = r4.getConnectionManager()
            r14.shutdown()
            r8 = r9
            goto L_0x0015
        L_0x01ea:
            org.apache.http.HttpEntity r3 = r12.getEntity()     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            if (r3 != 0) goto L_0x0209
            java.lang.RuntimeException r14 = new java.lang.RuntimeException     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r15 = "No HttpEntity from the response."
            r14.<init>(r15)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            throw r14     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
        L_0x01f8:
            r14 = move-exception
        L_0x01f9:
            if (r4 == 0) goto L_0x0208
            org.apache.http.conn.ClientConnectionManager r15 = r4.getConnectionManager()
            if (r15 == 0) goto L_0x0208
            org.apache.http.conn.ClientConnectionManager r15 = r4.getConnectionManager()
            r15.shutdown()
        L_0x0208:
            throw r14
        L_0x0209:
            java.io.InputStream r6 = r3.getContent()     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            r10 = 0
            r14 = 1024(0x400, float:1.435E-42)
            byte[] r1 = new byte[r14]     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.io.RandomAccessFile r13 = new java.io.RandomAccessFile     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            r0 = r18
            android.content.Context r14 = r0.mContext     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r15 = r7.tempPath     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.io.File r14 = com.jianq.net.download.DownloadsFileUtils.detectPath(r14, r15)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r15 = "rwd"
            r13.<init>(r14, r15)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            int r14 = r7.downloadLength     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            long r14 = (long) r14     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            r13.seek(r14)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.util.Properties r11 = new java.util.Properties     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            r11.<init>()     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            r14 = 2
            r7.status = r14     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
        L_0x0231:
            r14 = 0
            r15 = 1024(0x400, float:1.435E-42)
            int r10 = r6.read(r1, r14, r15)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            r14 = -1
            if (r10 != r14) goto L_0x0271
        L_0x023b:
            r13.close()     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            r6.close()     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            int r14 = r7.downloadLength     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            int r15 = r7.contentLength     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            if (r14 != r15) goto L_0x0260
            r0 = r18
            android.content.Context r14 = r0.mContext     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r15 = r7.configPath     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            com.jianq.net.download.DownloadsFileUtils.deleteFile(r14, r15)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            r0 = r18
            android.content.Context r14 = r0.mContext     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r15 = r7.tempPath     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r0 = r7.finalPath     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            r16 = r0
            com.jianq.net.download.DownloadsFileUtils.fileRename(r14, r15, r16)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            r14 = 3
            r7.status = r14     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
        L_0x0260:
            if (r4 == 0) goto L_0x0015
            org.apache.http.conn.ClientConnectionManager r14 = r4.getConnectionManager()
            if (r14 == 0) goto L_0x0015
            org.apache.http.conn.ClientConnectionManager r14 = r4.getConnectionManager()
            r14.shutdown()
            goto L_0x0015
        L_0x0271:
            boolean r14 = r7.isLife     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            if (r14 != 0) goto L_0x028e
            r0 = r18
            boolean r14 = r0.isResume     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            if (r14 != 0) goto L_0x023b
            r0 = r18
            android.content.Context r14 = r0.mContext     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r15 = r7.configPath     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            com.jianq.net.download.DownloadsFileUtils.deleteFile(r14, r15)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            r0 = r18
            android.content.Context r14 = r0.mContext     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r15 = r7.tempPath     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            com.jianq.net.download.DownloadsFileUtils.deleteFile(r14, r15)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            goto L_0x023b
        L_0x028e:
            r14 = 0
            r13.write(r1, r14, r10)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            int r14 = r7.downloadLength     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            int r14 = r14 + r10
            r7.downloadLength = r14     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r14 = "_KEY_FILENAME_NETWORK"
            java.lang.String r15 = r7.fileName     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            r11.put(r14, r15)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r14 = "_KEY_LENGTH_DOWNLOAD"
            int r15 = r7.downloadLength     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r15 = java.lang.String.valueOf(r15)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            r11.put(r14, r15)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r14 = "_KEY_LENGTH_CONTENT"
            int r15 = r7.contentLength     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r15 = java.lang.String.valueOf(r15)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            r11.put(r14, r15)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            r0 = r18
            android.content.Context r14 = r0.mContext     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r15 = r7.configPath     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            java.lang.String r16 = "Only support  ISO–8859–1, please do not use Chinese."
            r0 = r16
            com.jianq.net.download.DownloadsFileUtils.write(r14, r15, r11, r0)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            r11.clear()     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            r14 = 1
            com.jianq.net.download.DownloadInfo[] r14 = new com.jianq.net.download.DownloadInfo[r14]     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            r15 = 0
            r14[r15] = r7     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            r0 = r18
            r0.publishProgress(r14)     // Catch:{ MalformedURLException -> 0x0176, RuntimeException -> 0x019d, IOException -> 0x01c9 }
            goto L_0x0231
        L_0x02d1:
            r14 = move-exception
            r8 = r9
            goto L_0x01f9
        L_0x02d5:
            r8 = r9
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jianq.net.download.DownloadTask.doInBackground(com.jianq.net.download.DownloadInfo[]):com.jianq.net.download.DownloadInfo");
    }
}
