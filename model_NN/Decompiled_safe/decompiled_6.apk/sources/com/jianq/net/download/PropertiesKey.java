package com.jianq.net.download;

public class PropertiesKey {
    public static final String KEY_FILENAME_NETWORK = "_KEY_FILENAME_NETWORK";
    public static final String KEY_LENGTH_CONTENT = "_KEY_LENGTH_CONTENT";
    public static final String KEY_LENGTH_DOWNLOAD = "_KEY_LENGTH_DOWNLOAD";
}
