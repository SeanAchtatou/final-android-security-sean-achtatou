package com.jianq.net.download;

public interface OnDownloadListener {
    void onException(Exception exc);

    void onPostExecute(DownloadInfo downloadInfo);

    void onPreExecute(DownloadInfo downloadInfo);

    void onProgressUpdate(DownloadInfo downloadInfo);
}
