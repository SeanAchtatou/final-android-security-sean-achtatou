package com.jianq.net;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.net.wifi.WifiManager;
import com.jianq.util.DeviceUtils;
import java.io.IOException;
import javax.net.ssl.SSLHandshakeException;
import org.apache.http.HttpHost;
import org.apache.http.NoHttpResponseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HttpContext;

public class HttpClientUtils {
    private static void stuffHttpRequestRetryHandler(DefaultHttpClient httpClient) {
        httpClient.setHttpRequestRetryHandler(new HttpRequestRetryHandler() {
            public boolean retryRequest(IOException exception, int executionCount, HttpContext context) {
                if (executionCount >= 3) {
                    return false;
                }
                if (exception instanceof NoHttpResponseException) {
                    return true;
                }
                if (exception instanceof SSLHandshakeException) {
                }
                return false;
            }
        });
    }

    public static HttpClient buildHttpClient(Context context) throws RuntimeException {
        Cursor mCursor;
        if (!DeviceUtils.isNetworkConnected(context)) {
            throw new RuntimeException("Network is not available!");
        }
        DefaultHttpClient httpClient = new DefaultHttpClient();
        if (!((WifiManager) context.getSystemService("wifi")).isWifiEnabled() && (mCursor = context.getContentResolver().query(Uri.parse("content://telephony/carriers/preferapn"), null, null, null, null)) != null) {
            mCursor.moveToNext();
            String proxyStr = mCursor.getString(mCursor.getColumnIndex("proxy"));
            if (proxyStr != null && proxyStr.trim().length() > 0) {
                httpClient.getParams().setParameter("http.route.default-proxy", new HttpHost(proxyStr, 80));
            }
        }
        HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 60000);
        HttpConnectionParams.setSoTimeout(httpClient.getParams(), 30000);
        stuffHttpRequestRetryHandler(httpClient);
        return httpClient;
    }
}
