package com.jianq.net;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;
import cn.com.jit.pnxclient.constant.PNXConfigConstant;
import com.jianq.helper.JQOpenFileHelper;
import com.jianq.util.JQFileUtils;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

public class JQHttpDownloader2 {
    private static final int DOWNLOADED = 2;
    private static final int DOWNLOADING = 1;
    private static final int DOWNLOAD_ERROR = -1;
    private static final int PREV_CHECK = 10;
    private static final int START_DOWNLOAD = 0;
    /* access modifiers changed from: private */
    public Context ctx;
    /* access modifiers changed from: private */
    public ProgressDialog downloadDialog;
    /* access modifiers changed from: private */
    public long downloadedSize = 0;
    /* access modifiers changed from: private */
    public String errorMsg;
    /* access modifiers changed from: private */
    public File file;
    /* access modifiers changed from: private */
    public long fileTotalSize;
    private String fullFilePath;
    /* access modifiers changed from: private */
    public boolean isCancel = false;
    /* access modifiers changed from: private */
    public boolean isContinue = false;
    /* access modifiers changed from: private */
    public boolean isOpen;
    private Handler mainThreadHandler = new Handler(Looper.getMainLooper()) {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case -1:
                    if (JQHttpDownloader2.this.downloadDialog.isShowing()) {
                        JQHttpDownloader2.this.downloadDialog.dismiss();
                    }
                    Toast.makeText(JQHttpDownloader2.this.ctx, JQHttpDownloader2.this.errorMsg, 0).show();
                    if (JQHttpDownloader2.this.file != null) {
                        JQFileUtils.delete(JQHttpDownloader2.this.file.toString());
                        break;
                    }
                    break;
                case 0:
                    JQHttpDownloader2.this.downloadDialog.show();
                    JQHttpDownloader2.this.downloadDialog.setMax((int) JQHttpDownloader2.this.fileTotalSize);
                    break;
                case 1:
                    JQHttpDownloader2.this.downloadDialog.setProgress((int) JQHttpDownloader2.this.downloadedSize);
                    break;
                case 2:
                    JQHttpDownloader2.this.downloadDialog.dismiss();
                    if (!JQHttpDownloader2.this.isCancel) {
                        Toast.makeText(JQHttpDownloader2.this.ctx, "文件下载完成", 0).show();
                        if (JQHttpDownloader2.this.isOpen) {
                            new JQOpenFileHelper(JQHttpDownloader2.this.ctx).openFile(JQHttpDownloader2.this.file);
                            break;
                        }
                    }
                    break;
            }
            super.handleMessage(msg);
        }
    };
    private URL url = null;

    public JQHttpDownloader2(Context ctx2) {
        this.ctx = ctx2;
    }

    /* access modifiers changed from: private */
    public void sendMessage(int flag) {
        Message msg = this.mainThreadHandler.obtainMessage();
        msg.what = flag;
        this.mainThreadHandler.sendMessage(msg);
    }

    /* access modifiers changed from: private */
    public void write2SDFromInput(InputStream input) {
        RandomAccessFile output = null;
        Log.i("HttpDownloader", this.fullFilePath);
        try {
            RandomAccessFile output2 = new RandomAccessFile(this.file, "rw");
            try {
                output2.seek(this.downloadedSize);
                byte[] buffer = new byte[4096];
                while (true) {
                    int size = input.read(buffer);
                    if (size != -1) {
                        if (this.isCancel) {
                            break;
                        }
                        output2.write(buffer, 0, size);
                        this.downloadedSize += (long) size;
                        sendMessage(1);
                    } else {
                        break;
                    }
                }
                sendMessage(2);
                try {
                    output2.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e2) {
                e = e2;
                output = output2;
                try {
                    e.printStackTrace();
                    try {
                        output.close();
                    } catch (Exception e3) {
                        e3.printStackTrace();
                    }
                } catch (Throwable th) {
                    th = th;
                    try {
                        output.close();
                    } catch (Exception e4) {
                        e4.printStackTrace();
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                output = output2;
                output.close();
                throw th;
            }
        } catch (Exception e5) {
            e = e5;
            e.printStackTrace();
            output.close();
        }
    }

    private void startDownload(final String urlStr, final String filePath) {
        this.downloadDialog = new ProgressDialog(this.ctx);
        this.downloadDialog.setProgressStyle(1);
        this.downloadDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                Toast.makeText(JQHttpDownloader2.this.ctx, "下载已经取消...", 0).show();
                JQHttpDownloader2.this.isCancel = true;
                dialog.dismiss();
            }
        });
        new Thread(new Runnable() {
            public void run() {
                File sfile = new File(filePath);
                if (sfile.exists()) {
                    JQHttpDownloader2.this.file = sfile;
                    long localSize = sfile.length();
                    Log.d("localFileSize", new StringBuilder().append(localSize).toString());
                    if (localSize < JQHttpDownloader2.this.getRemoteFileSize(urlStr)) {
                        JQHttpDownloader2.this.isContinue = true;
                        JQHttpDownloader2.this.downloadedSize = localSize;
                    } else if (JQHttpDownloader2.this.isOpen) {
                        JQHttpDownloader2.this.sendMessage(2);
                        return;
                    } else {
                        Toast.makeText(JQHttpDownloader2.this.ctx, "该文件已经存在...", 0).show();
                        return;
                    }
                } else if (sfile.isDirectory()) {
                    JQHttpDownloader2.this.errorMsg = "存储对象必须是文件，请联系相关开发人员";
                    JQHttpDownloader2.this.sendMessage(-1);
                    return;
                } else {
                    if (!new File(sfile.getParent()).exists()) {
                        JQFileUtils.createDir(sfile.getParent());
                    }
                    try {
                        JQHttpDownloader2.this.file = JQFileUtils.createFile(sfile.toString());
                    } catch (IOException e) {
                        JQHttpDownloader2.this.errorMsg = "创建文件失败";
                        JQHttpDownloader2.this.sendMessage(-1);
                        return;
                    }
                }
                InputStream inputStream = null;
                try {
                    inputStream = JQHttpDownloader2.this.getInputStreamFromUrl(urlStr);
                    JQHttpDownloader2.this.sendMessage(0);
                    JQHttpDownloader2.this.write2SDFromInput(inputStream);
                    try {
                        inputStream.close();
                    } catch (Exception e2) {
                        JQHttpDownloader2.this.errorMsg = "URL出错，请检查能否正常访问到下载URL";
                        JQHttpDownloader2.this.sendMessage(-1);
                        Log.d("HttpDownloader.URL", urlStr);
                    }
                } catch (Exception e3) {
                    JQHttpDownloader2.this.errorMsg = "URL出错，请检查能否正常访问到下载URL";
                    JQHttpDownloader2.this.sendMessage(-1);
                    Log.d("HttpDownloader.URL", urlStr);
                    try {
                        inputStream.close();
                    } catch (Exception e4) {
                        JQHttpDownloader2.this.errorMsg = "URL出错，请检查能否正常访问到下载URL";
                        JQHttpDownloader2.this.sendMessage(-1);
                        Log.d("HttpDownloader.URL", urlStr);
                    }
                } catch (Throwable th) {
                    try {
                        inputStream.close();
                    } catch (Exception e5) {
                        JQHttpDownloader2.this.errorMsg = "URL出错，请检查能否正常访问到下载URL";
                        JQHttpDownloader2.this.sendMessage(-1);
                        Log.d("HttpDownloader.URL", urlStr);
                    }
                    throw th;
                }
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public InputStream getInputStreamFromUrl(String urlStr) throws MalformedURLException, IOException {
        String fName = urlStr.substring(urlStr.lastIndexOf("/") + 1, urlStr.indexOf("?") == -1 ? urlStr.length() : urlStr.indexOf("?"));
        String urlStr2 = urlStr.replace(fName, URLEncoder.encode(fName));
        Log.i("HttpDownloader", "urlStr-->" + urlStr2);
        this.url = new URL(urlStr2);
        URLConnection urlConn = this.url.openConnection();
        InputStream inputStream = new BufferedInputStream(urlConn.getInputStream());
        this.fileTotalSize = (long) urlConn.getContentLength();
        return inputStream;
    }

    public void downloadFile(String url2, String filePath, boolean isOpen2) {
        this.isOpen = isOpen2;
        this.fullFilePath = filePath;
        startDownload(url2, filePath);
    }

    public String downloadText(String urlStr) {
        StringBuffer sb = new StringBuffer();
        BufferedReader buffer = null;
        try {
            this.url = new URL(urlStr);
            HttpURLConnection urlConn = (HttpURLConnection) this.url.openConnection();
            BufferedReader buffer2 = new BufferedReader(new InputStreamReader(urlConn.getInputStream(), "GBK"));
            while (true) {
                try {
                    String line = buffer2.readLine();
                    if (line == null) {
                        break;
                    }
                    sb.append(String.valueOf(line) + PNXConfigConstant.RESP_SPLIT_2);
                } catch (Exception e) {
                    e = e;
                    buffer = buffer2;
                } catch (Throwable th) {
                    th = th;
                    buffer = buffer2;
                    try {
                        buffer.close();
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    throw th;
                }
            }
            urlConn.disconnect();
            try {
                buffer2.close();
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        } catch (Exception e4) {
            e = e4;
            try {
                e.printStackTrace();
                try {
                    buffer.close();
                } catch (Exception e5) {
                    e5.printStackTrace();
                }
                return sb.toString();
            } catch (Throwable th2) {
                th = th2;
                buffer.close();
                throw th;
            }
        }
        return sb.toString();
    }

    public long getRemoteFileSize(String urlStr) {
        long size = 0;
        try {
            String fName = urlStr.substring(urlStr.lastIndexOf("/") + 1, urlStr.indexOf("?") == -1 ? urlStr.length() : urlStr.indexOf("?"));
            HttpURLConnection urlConn = (HttpURLConnection) new URL(urlStr.replace(fName, URLEncoder.encode(fName))).openConnection();
            urlConn.connect();
            size = (long) urlConn.getContentLength();
            urlConn.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        Log.d("remoteFileSize", new StringBuilder().append(size).toString());
        return size;
    }
}
