package com.jianq.net;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.widget.Toast;
import com.jianq.ui.JQCustomDialog;
import com.jianq.util.JQUtils;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;

public class JQXMasUploader {
    private static final int PRE_UPLOAD = 4;
    private static final int UPLOADCANCEL = 5;
    private static final int UPLOADED = 2;
    private static final int UPLOADING = 1;
    private static final int UPLOAD_ERROR = 0;
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public String errorMsg = "上传文件过程中出现错误...";
    private Handler handler = new Handler(Looper.getMainLooper()) {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    JQCustomDialog.getInst().cancelProgressDialog();
                    Toast.makeText(JQXMasUploader.this.context, JQXMasUploader.this.errorMsg, 0).show();
                    break;
                case 2:
                    JQCustomDialog.getInst().cancelProgressDialog();
                    Toast.makeText(JQXMasUploader.this.context, "上传完成...", 0).show();
                    JQXMasUploader.this.listener.uploadCallBack(JQXMasUploader.this.returnParameters);
                    break;
                case 4:
                    JQCustomDialog.getInst().showProgressDialog(JQXMasUploader.this.context, "正在上传中...");
                    JQCustomDialog.getInst().setOnCancelListener(JQXMasUploader.this.uploadCancelListener);
                    break;
                case 5:
                    JQCustomDialog.getInst().cancelProgressDialog();
                    Toast.makeText(JQXMasUploader.this.context, "上传取消...", 0).show();
                    break;
            }
            super.handleMessage(msg);
        }
    };
    /* access modifiers changed from: private */
    public UploadCallbackListener listener;
    /* access modifiers changed from: private */
    public String returnParameters;
    /* access modifiers changed from: private */
    public DialogInterface.OnCancelListener uploadCancelListener = new DialogInterface.OnCancelListener() {
        public void onCancel(DialogInterface dialog) {
            JQBasicNetwork.getInstance().cancelHttpPost();
            JQXMasUploader.this.sendMessage(5);
        }
    };

    public interface UploadCallbackListener {
        void uploadCallBack(String str);
    }

    public JQXMasUploader(Context context2) {
        this.context = context2;
    }

    public void upload(String requestUrl, String parameter, String[] files, String fileType, String encoding) {
        sendMessage(4);
        final String str = requestUrl;
        final String str2 = parameter;
        final String[] strArr = files;
        final String str3 = fileType;
        final String str4 = encoding;
        new Thread(new Runnable() {
            public void run() {
                HttpEntity result;
                if (JQUtils.isNetworkAvailable(JQXMasUploader.this.context)) {
                    try {
                        HttpResponse response = JQBasicNetwork.getInstance().getUploadResponse(str, str2, strArr, str3, str4);
                        int stateCode = response.getStatusLine().getStatusCode();
                        StringBuffer sb = new StringBuffer();
                        if (stateCode == 200 && (result = response.getEntity()) != null) {
                            InputStream is = result.getContent();
                            BufferedReader br = new BufferedReader(new InputStreamReader(is));
                            while (true) {
                                String tempLine = br.readLine();
                                if (tempLine == null) {
                                    break;
                                }
                                sb.append(tempLine);
                            }
                            if (br != null) {
                                br.close();
                            }
                            if (is != null) {
                                is.close();
                            }
                        }
                        JQBasicNetwork.getInstance().cancelHttpPost();
                        JQXMasUploader.this.returnParameters = sb.toString();
                        JQXMasUploader.this.sendMessage(2);
                    } catch (ClientProtocolException e) {
                        e.printStackTrace();
                        JQXMasUploader.this.sendMessage(0);
                    } catch (IOException e2) {
                        e2.printStackTrace();
                        JQXMasUploader.this.sendMessage(0);
                    }
                } else {
                    JQXMasUploader.this.errorMsg = "无网络连接，请确认网络已经连接";
                    JQXMasUploader.this.sendMessage(0);
                }
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public void sendMessage(int flag) {
        this.handler.sendMessage(this.handler.obtainMessage(flag));
    }

    public void setUploadCallbackListener(UploadCallbackListener listener2) {
        this.listener = listener2;
    }
}
