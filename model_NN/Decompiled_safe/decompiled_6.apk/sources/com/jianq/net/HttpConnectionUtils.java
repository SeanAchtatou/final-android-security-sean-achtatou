package com.jianq.net;

import android.content.Context;
import android.text.TextUtils;
import com.jianq.net.download.DownloadInfo;
import com.jianq.util.DeviceUtils;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HttpConnectionUtils {
    public static HttpURLConnection buildHttpURLConnection(Context context, String url) throws MalformedURLException, IOException, RuntimeException {
        if (!DeviceUtils.isNetworkConnected(context)) {
            throw new RuntimeException("Network is not available!");
        }
        HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
        conn.setConnectTimeout(30000);
        conn.setReadTimeout(5000);
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Accept", "image/gif, image/jpeg, image/pjpeg, image/pjpeg, application/x-shockwave-flash, application/xaml+xml, application/vnd.ms-xpsdocument, application/x-ms-xbap, application/x-ms-application, application/vnd.ms-excel, application/vnd.ms-powerpoint, application/msword, */*");
        conn.setRequestProperty("Accept-Language", "zh-CN");
        conn.setRequestProperty("Referer", url);
        conn.setRequestProperty("Charset", JQBasicNetwork.UTF_8);
        conn.setRequestProperty(JQBasicNetwork.USER_AGENT, "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.2; Trident/4.0; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)");
        conn.setRequestProperty(JQBasicNetwork.CONN_DIRECTIVE, JQBasicNetwork.CONN_KEEP_ALIVE);
        return conn;
    }

    private static String getFileName(String url, HttpURLConnection conn, String tmpName) throws MalformedURLException, IOException {
        String filename = url.substring(url.lastIndexOf(File.separator) + 1);
        if (TextUtils.isEmpty(filename)) {
            int i = 0;
            while (true) {
                String mine = conn.getHeaderField(i);
                if (mine == null) {
                    filename = tmpName;
                    break;
                }
                if ("content-disposition".equals(conn.getHeaderFieldKey(i).toLowerCase(Locale.US))) {
                    Matcher m = Pattern.compile(".*filename=(.*)").matcher(mine.toLowerCase(Locale.US));
                    if (m.find()) {
                        return m.group(1);
                    }
                }
                i++;
            }
        } else if (-1 != filename.indexOf("?")) {
            filename = filename.substring(0, filename.indexOf("?"));
        }
        return filename;
    }

    public static void stuffDownloadInfo(Context context, DownloadInfo info) throws RuntimeException, IOException, MalformedURLException {
        if (info == null || info.url == null) {
            throw new RuntimeException("Parameter DownloadInfo is NULL.");
        }
        HttpURLConnection conn = buildHttpURLConnection(context, info.url);
        conn.connect();
        if (conn.getResponseCode() == 200) {
            info.fileName = getFileName(info.url, conn, "TmpFile.zip");
            info.contentLength = conn.getContentLength();
            if (info.contentLength <= 0) {
                throw new RuntimeException("Unkown file size");
            }
            conn.disconnect();
            return;
        }
        throw new RuntimeException("Server no response");
    }
}
