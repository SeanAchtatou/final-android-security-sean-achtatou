package com.jianq.net;

import android.content.Context;
import android.text.TextUtils;
import cn.com.jit.pnxclient.constant.PNXConfigConstant;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManagerFactory;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

public class JQSslNetwork extends JQBasicNetwork {
    private static final String CLIENT_AGREEMENT = "TLS";
    private static final String CLIENT_KEY_MANAGER = "X509";
    private static final String CLIENT_TRUST_MANAGER = "X509";
    private StringBuilder mErrorMsgBuilder = new StringBuilder();
    private KeyStore mTrustKeyStore;

    public JQSslNetwork(Context ctx, int trustKeyStoreRawFileResourceId, String keystorePassword) throws Exception {
        if (!initKeyStoreParams(ctx, trustKeyStoreRawFileResourceId, keystorePassword)) {
            throw new Exception("failed to init keystore: " + getErrorInfo());
        }
        this.httpClient = getSslHttpClient();
        HttpParams httpParams = this.httpClient.getParams();
        HttpConnectionParams.setConnectionTimeout(httpParams, 60000);
        HttpConnectionParams.setSoTimeout(httpParams, 60000);
    }

    public String getErrorInfo() {
        return this.mErrorMsgBuilder.toString();
    }

    private boolean initKeyStoreParams(Context ctx, int trustKeyStoreRawFileResourceId, String keystorePassword) {
        try {
            this.mTrustKeyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            InputStream kis = ctx.getResources().openRawResource(trustKeyStoreRawFileResourceId);
            if (kis != null) {
                this.mTrustKeyStore.load(kis, keystorePassword.toCharArray());
            }
            kis.close();
            return true;
        } catch (KeyStoreException e) {
            e.printStackTrace();
            this.mErrorMsgBuilder.append(String.valueOf(e.getMessage()) + PNXConfigConstant.RESP_SPLIT_2);
            return false;
        } catch (NoSuchAlgorithmException e2) {
            e2.printStackTrace();
            this.mErrorMsgBuilder.append(String.valueOf(e2.getMessage()) + PNXConfigConstant.RESP_SPLIT_2);
            return false;
        } catch (CertificateException e3) {
            e3.printStackTrace();
            this.mErrorMsgBuilder.append(String.valueOf(e3.getMessage()) + PNXConfigConstant.RESP_SPLIT_2);
            return false;
        } catch (IOException e4) {
            e4.printStackTrace();
            this.mErrorMsgBuilder.append(String.valueOf(e4.getMessage()) + PNXConfigConstant.RESP_SPLIT_2);
            return false;
        }
    }

    public HttpClient getSslHttpClient() throws Exception {
        SSLSocketFactory sf = new SSLSocketFactory(this.mTrustKeyStore);
        sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        HttpParams params = new BasicHttpParams();
        HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(params, JQBasicNetwork.UTF_8);
        SchemeRegistry registry = new SchemeRegistry();
        registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        registry.register(new Scheme("https", sf, (int) PNXConfigConstant.PNXSERVERPORT_DEFAULT));
        return new DefaultHttpClient(new ThreadSafeClientConnManager(params, registry), params);
    }

    public HttpsURLConnection openConnection(String httpsUrl) {
        if (TextUtils.isEmpty(httpsUrl) || !httpsUrl.toLowerCase().startsWith("https")) {
            this.mErrorMsgBuilder.append("httpsUrl is invalid, can not be empty and should be started with https pls check!");
            return null;
        }
        try {
            SSLContext sslContext = SSLContext.getInstance(CLIENT_AGREEMENT);
            KeyManagerFactory instance = KeyManagerFactory.getInstance("X509");
            TrustManagerFactory trustManager = TrustManagerFactory.getInstance("X509");
            if (this.mTrustKeyStore != null) {
                trustManager.init(this.mTrustKeyStore);
            } else {
                this.mErrorMsgBuilder.append("mTrustKeyStore is NULL! Please check initKeyStoreParams(), make sure it returns true!");
            }
            sslContext.init(null, trustManager.getTrustManagers(), null);
            HttpsURLConnection connection = (HttpsURLConnection) new URL(httpsUrl).openConnection();
            connection.setSSLSocketFactory(sslContext.getSocketFactory());
            connection.setHostnameVerifier(new TrustAnyHostnameVerifier());
            return connection;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            this.mErrorMsgBuilder.append(String.valueOf(e.getMessage()) + PNXConfigConstant.RESP_SPLIT_2);
            return null;
        } catch (MalformedURLException e2) {
            e2.printStackTrace();
            this.mErrorMsgBuilder.append(String.valueOf(e2.getMessage()) + PNXConfigConstant.RESP_SPLIT_2);
            return null;
        } catch (IOException e3) {
            e3.printStackTrace();
            this.mErrorMsgBuilder.append(String.valueOf(e3.getMessage()) + PNXConfigConstant.RESP_SPLIT_2);
            return null;
        } catch (KeyManagementException e4) {
            e4.printStackTrace();
            this.mErrorMsgBuilder.append(String.valueOf(e4.getMessage()) + PNXConfigConstant.RESP_SPLIT_2);
            return null;
        } catch (KeyStoreException e5) {
            e5.printStackTrace();
            this.mErrorMsgBuilder.append(String.valueOf(e5.getMessage()) + PNXConfigConstant.RESP_SPLIT_2);
            return null;
        }
    }

    class TrustAnyHostnameVerifier implements HostnameVerifier {
        TrustAnyHostnameVerifier() {
        }

        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    }
}
