package com.jianq.exception;

import android.content.Context;
import android.os.Build;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;
import cn.com.jit.pnxclient.constant.PNXConfigConstant;
import com.jianq.common.JQApplication;
import com.jianq.common.JQDeviceInformation;
import com.jianq.common.JQFrameworkConfig;
import com.jianq.util.JQFileUtils;
import com.jianq.util.JQUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class JQCrashHandler implements Thread.UncaughtExceptionHandler {
    private static JQCrashHandler INSTANCE = new JQCrashHandler();
    private static final String TAG = "JQCrashHandler";
    /* access modifiers changed from: private */
    public Context context;
    private Thread.UncaughtExceptionHandler defaultHandler;
    private Map<String, String> infos = new HashMap();

    private JQCrashHandler() {
    }

    public static JQCrashHandler getInst() {
        return INSTANCE;
    }

    public void init(Context context2) {
        this.context = context2;
        this.defaultHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    public void uncaughtException(Thread thread, Throwable ex) {
        if (handleException(ex) || this.defaultHandler == null) {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                Log.e(TAG, "error :", e);
            }
        } else {
            this.defaultHandler.uncaughtException(thread, ex);
        }
        JQApplication.getInst().exit();
    }

    private boolean handleException(Throwable ex) {
        if (ex == null) {
            return false;
        }
        new Thread() {
            public void run() {
                Looper.prepare();
                Toast.makeText(JQCrashHandler.this.context, "程序出现异常，即将退出...", 0).show();
                Looper.loop();
            }
        }.start();
        collectDeviceInfo(this.context);
        saveCrashInfo2File(ex);
        Log.e("Application", "errors", ex);
        return true;
    }

    private void collectDeviceInfo(Context ctx) {
        try {
            this.infos.put("versionName", JQUtils.getVersionName(ctx));
            this.infos.put("versionCode", String.valueOf(JQUtils.getVersionCode(ctx)));
        } catch (Exception e) {
            Log.e(TAG, "an error occured when collect crash info", e);
        }
        for (Field field : Build.class.getDeclaredFields()) {
            try {
                field.setAccessible(true);
                this.infos.put(field.getName(), field.get(null).toString());
                Log.d(TAG, String.valueOf(field.getName()) + PNXConfigConstant.RESP_SPLIT_3 + field.get(null));
            } catch (Exception e2) {
                Log.e(TAG, "an error occured when collect crash info", e2);
            }
        }
    }

    private String saveCrashInfo2File(Throwable ex) {
        StringBuffer sb = new StringBuffer();
        for (Map.Entry<String, String> entry : this.infos.entrySet()) {
            sb.append(String.valueOf((String) entry.getKey()) + "=" + ((String) entry.getValue()) + PNXConfigConstant.RESP_SPLIT_2);
        }
        Writer writer = new StringWriter();
        PrintWriter printWriter = new PrintWriter(writer);
        ex.printStackTrace(printWriter);
        for (Throwable cause = ex.getCause(); cause != null; cause = cause.getCause()) {
            cause.printStackTrace(printWriter);
        }
        printWriter.close();
        sb.append(writer.toString().replace("\t", PNXConfigConstant.RESP_SPLIT_2));
        try {
            String fileName = "error-" + System.currentTimeMillis() + "-" + JQFrameworkConfig.getInst().getLogName();
            if (!JQDeviceInformation.hasSDCard()) {
                return fileName;
            }
            String path = String.valueOf(JQDeviceInformation.getSDCardPath()) + JQFrameworkConfig.getInst().getLogPath();
            if (!new File(path).exists()) {
                JQFileUtils.createDir(path);
            }
            Log.d(TAG, "Error save to:" + path + "/" + fileName);
            FileOutputStream fos = new FileOutputStream(JQFileUtils.createFile(String.valueOf(path) + "/" + fileName));
            fos.write(sb.toString().getBytes());
            fos.close();
            return String.valueOf(path) + "/" + fileName;
        } catch (IOException e) {
            Log.e(TAG, "an error occured while writing file...", e);
            return null;
        }
    }
}
