package com.jianq.log;

import android.os.Environment;
import android.util.Log;
import cn.com.jit.pnxclient.constant.PNXConfigConstant;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import com.jianq.misc.StringEx;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.ref.SoftReference;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class JQLogger {
    public static final String LOG_FILE_PATH_DEFAULT = "%1$s/jianq.com/log/%2$s.log";
    private static final String TAG = "JQLogger";
    private static HashMap<String, SoftReference<JQLogger>> mCacheLogger = new HashMap<>();
    private static List<LogConfig> mConfigs = null;
    private static LogLevel mCurrentLogLevel = LogLevel.LOG_LEVEL_DEBUG;
    private static SimpleDateFormat mDateFormatter = new SimpleDateFormat("yyyy-MM-dd");
    private static SimpleDateFormat mTimeFormatter = new SimpleDateFormat("MM-dd HH:mm:ss");
    private String mDefaultTag;
    private LogLevel mLevel;
    private OutputStreamWriter mWriter;

    static {
        initConfig();
    }

    private JQLogger(String defaultTag, LogLevel level) {
        this.mDefaultTag = defaultTag;
        this.mLevel = level;
    }

    public String getDefultTag() {
        return this.mDefaultTag;
    }

    public void setLogWriter(OutputStreamWriter writer) {
        this.mWriter = writer;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException} */
    public static JQLogger getLogger(Class<?> cls, boolean isLog2File) {
        JQLogger logger;
        String tag = cls.getSimpleName();
        Package pkg = cls.getPackage();
        String pkgName = StringEx.Empty;
        if (pkg != null) {
            pkgName = pkg.getName();
        }
        if (!mCacheLogger.containsKey(tag)) {
            logger = newInstance2Cache(tag, pkgName);
        } else {
            logger = mCacheLogger.get(tag).get();
            if (logger == null) {
                logger = newInstance2Cache(tag, pkgName);
            }
        }
        if (isLog2File && "mounted".equals(Environment.getExternalStorageState())) {
            String filePath = String.format(LOG_FILE_PATH_DEFAULT, Environment.getExternalStorageDirectory().getPath(), mDateFormatter.format(new Date()));
            try {
                File logFile = new File(filePath);
                if (!logFile.getParentFile().exists()) {
                    logFile.getParentFile().mkdirs();
                }
                if (logger.mWriter == null) {
                    logger.mWriter = new FileWriter(filePath, true);
                }
            } catch (IOException e) {
                e.printStackTrace();
                Log.e(JQLogger.class.getSimpleName(), e.getMessage());
            }
        }
        return logger;
    }

    public static JQLogger getLogger(Class<?> cls) {
        return getLogger(cls, false);
    }

    private static JQLogger newInstance2Cache(String tag, String pkgName) {
        JQLogger logger;
        LogConfig config = null;
        if (mConfigs != null) {
            Iterator<LogConfig> it = mConfigs.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                LogConfig cfg = it.next();
                if (tag.equals(cfg.filter)) {
                    config = cfg;
                    break;
                } else if (!StringEx.isNullOrEmpty(cfg.filter) && cfg.filter.endsWith(".*")) {
                    if (pkgName.startsWith(cfg.filter.substring(0, cfg.filter.lastIndexOf(46)))) {
                        config = cfg;
                    }
                }
            }
        }
        if (config != null) {
            logger = new JQLogger(tag, config.logLevel);
        } else {
            logger = new JQLogger(tag, mCurrentLogLevel);
        }
        mCacheLogger.put(tag, new SoftReference<>(logger));
        return logger;
    }

    private static void initConfig() {
        if ("mounted".equals(Environment.getExternalStorageState())) {
            File logConfigFlie = new File(String.valueOf(Environment.getExternalStorageDirectory().getPath()) + "/jianq.com/logger.json");
            if (logConfigFlie.exists()) {
                try {
                    mConfigs = (List) new Gson().fromJson(new FileReader(logConfigFlie), new TypeToken<List<LogConfig>>() {
                    }.getType());
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    Log.e(TAG, "failed to load Logger.json on SDCard.", e);
                } catch (JsonParseException je) {
                    Log.e(TAG, "failed to load Logger.json on SDCard.", je);
                }
            }
        }
    }

    public static void setGlobalLogLevel(LogLevel logLevel) {
        mCurrentLogLevel = logLevel;
    }

    public static LogLevel getGlobalLogLevel() {
        return mCurrentLogLevel;
    }

    public void e(String msg) {
        e(this.mDefaultTag, msg);
    }

    public void w(String msg) {
        w(this.mDefaultTag, msg);
    }

    public void d(String msg) {
        d(this.mDefaultTag, msg);
    }

    public void w(String tag, String msg) {
        if (mCurrentLogLevel.getValue() >= LogLevel.LOG_LEVEL_WARN.getValue()) {
            Log.w(tag, msg);
            writeFile("W", tag, msg);
        }
    }

    private void writeFile(String level, String tag, String msg) {
        String line = String.valueOf(level) + PNXConfigConstant.RESP_SPLIT_3 + tag + "\t" + mTimeFormatter.format(new Date()) + "\t" + msg;
        if (this.mWriter != null) {
            try {
                this.mWriter.write(line);
                this.mWriter.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void d(String tag, String msg) {
        if (mCurrentLogLevel.getValue() >= LogLevel.LOG_LEVEL_DEBUG.getValue()) {
            Log.d(tag, msg);
            writeFile("D", tag, msg);
        }
    }

    public void e(String tag, String msg) {
        if (mCurrentLogLevel.getValue() >= LogLevel.LOG_LEVEL_ERROR.getValue()) {
            Log.e(tag, msg);
            writeFile("E", tag, msg);
        }
    }

    public void e(String tag, String msg, Throwable throwable) {
        if (mCurrentLogLevel.getValue() >= LogLevel.LOG_LEVEL_ERROR.getValue()) {
            Log.e(tag, msg, throwable);
            writeFile("E", tag, String.valueOf(msg) + "--->" + throwable.getMessage());
        }
    }

    public void i(String tag, String msg) {
        if (mCurrentLogLevel.getValue() >= LogLevel.LOG_LEVEL_INFO.getValue()) {
            Log.i(tag, msg);
            writeFile("I", tag, msg);
        }
    }

    public void r(String message) {
    }

    public LogLevel getLogLevel() {
        return this.mLevel;
    }

    public void setLogLevel(LogLevel level) {
        this.mLevel = level;
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        super.finalize();
        if (this.mWriter != null) {
            try {
                this.mWriter.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            this.mWriter = null;
        }
    }
}
