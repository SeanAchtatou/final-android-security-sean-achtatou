package com.jianq.log;

import com.jianq.net.JQBasicNetwork;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;

public class LogReporter {
    private static byte[] readStream(InputStream is) throws Exception {
        byte[] buffer = new byte[4096];
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        while (true) {
            int len = is.read(buffer);
            if (len == -1) {
                is.close();
                return bos.toByteArray();
            }
            bos.write(buffer, 0, len);
        }
    }

    private static void sendData(String strurl, byte[] data, Map<String, String> requestParams) throws Exception {
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL(strurl).openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestMethod("POST");
            conn.setUseCaches(false);
            conn.setInstanceFollowRedirects(true);
            conn.setConnectTimeout(6000);
            conn.setRequestProperty("Content-Type", "text/html;charset=UTF-8");
            conn.setRequestProperty(JQBasicNetwork.CONN_DIRECTIVE, JQBasicNetwork.CONN_KEEP_ALIVE);
            conn.setRequestProperty("Charset", JQBasicNetwork.UTF_8);
            for (Map.Entry<String, String> entry : requestParams.entrySet()) {
                conn.setRequestProperty((String) entry.getKey(), (String) entry.getValue());
            }
            DataOutputStream dos = new DataOutputStream(conn.getOutputStream());
            dos.write(data);
            dos.flush();
            dos.close();
            if (conn.getResponseCode() == 200) {
                conn.getInputStream();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String makeUrl(String strurl, Map<String, String> requestParams) throws UnsupportedEncodingException {
        if (requestParams == null || requestParams.isEmpty()) {
            return strurl;
        }
        StringBuilder params = new StringBuilder();
        for (Map.Entry<String, String> entry : requestParams.entrySet()) {
            params.append((String) entry.getKey());
            params.append("=");
            params.append(URLEncoder.encode((String) entry.getValue(), JQBasicNetwork.UTF_8));
            params.append("&");
        }
        if (params.length() > 0) {
            params.deleteCharAt(params.length() - 1);
        }
        return params.toString();
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0027 A[SYNTHETIC, Splitter:B:15:0x0027] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0033 A[SYNTHETIC, Splitter:B:21:0x0033] */
    /* JADX WARNING: Removed duplicated region for block: B:33:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean sendCrash(android.content.Context r7, java.lang.String r8, java.util.Map<java.lang.String, java.lang.String> r9, java.lang.String r10) {
        /*
            r5 = 0
            boolean r6 = com.jianq.util.DeviceUtils.isWifiOk(r7)
            if (r6 != 0) goto L_0x0008
        L_0x0007:
            return r5
        L_0x0008:
            r3 = 0
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x0021 }
            r2.<init>(r10)     // Catch:{ Exception -> 0x0021 }
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0021 }
            r4.<init>(r2)     // Catch:{ Exception -> 0x0021 }
            byte[] r0 = readStream(r4)     // Catch:{ Exception -> 0x0044, all -> 0x0041 }
            sendData(r8, r0, r9)     // Catch:{ Exception -> 0x0044, all -> 0x0041 }
            if (r4 == 0) goto L_0x001f
            r4.close()     // Catch:{ IOException -> 0x003c }
        L_0x001f:
            r5 = 1
            goto L_0x0007
        L_0x0021:
            r1 = move-exception
        L_0x0022:
            r1.printStackTrace()     // Catch:{ all -> 0x0030 }
            if (r3 == 0) goto L_0x0007
            r3.close()     // Catch:{ IOException -> 0x002b }
            goto L_0x0007
        L_0x002b:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0007
        L_0x0030:
            r5 = move-exception
        L_0x0031:
            if (r3 == 0) goto L_0x0036
            r3.close()     // Catch:{ IOException -> 0x0037 }
        L_0x0036:
            throw r5
        L_0x0037:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0036
        L_0x003c:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x001f
        L_0x0041:
            r5 = move-exception
            r3 = r4
            goto L_0x0031
        L_0x0044:
            r1 = move-exception
            r3 = r4
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jianq.log.LogReporter.sendCrash(android.content.Context, java.lang.String, java.util.Map, java.lang.String):boolean");
    }
}
