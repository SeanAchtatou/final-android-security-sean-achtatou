package com.jianq;

public final class R {

    public static final class color {
        public static final int gray = 2131165185;
        public static final int white = 2131165184;
    }

    public static final class drawable {
        public static final int jq_downloading = 2130837779;
        public static final int jq_file_dialog_file = 2130837780;
        public static final int jq_file_dialog_folder = 2130837781;
        public static final int jq_loading_flower1 = 2130837782;
        public static final int jq_loading_flower2 = 2130837783;
        public static final int jq_loading_marquee = 2130837784;
        public static final int jq_logo = 2130837785;
        public static final int jq_progress_bg = 2130837786;
        public static final int jq_progress_dialog_bg_blue_01 = 2130837787;
        public static final int jq_progress_dialog_bg_blue_02 = 2130837788;
        public static final int jq_progress_dialog_bg_light = 2130837789;
        public static final int jq_progress_flower2_1 = 2130837790;
        public static final int jq_progress_flower2_10 = 2130837791;
        public static final int jq_progress_flower2_11 = 2130837792;
        public static final int jq_progress_flower2_12 = 2130837793;
        public static final int jq_progress_flower2_13 = 2130837794;
        public static final int jq_progress_flower2_2 = 2130837795;
        public static final int jq_progress_flower2_3 = 2130837796;
        public static final int jq_progress_flower2_4 = 2130837797;
        public static final int jq_progress_flower2_5 = 2130837798;
        public static final int jq_progress_flower2_6 = 2130837799;
        public static final int jq_progress_flower2_7 = 2130837800;
        public static final int jq_progress_flower2_8 = 2130837801;
        public static final int jq_progress_flower2_9 = 2130837802;
        public static final int jq_progress_flower_1_1 = 2130837803;
        public static final int jq_progress_flower_1_10 = 2130837804;
        public static final int jq_progress_flower_1_11 = 2130837805;
        public static final int jq_progress_flower_1_2 = 2130837806;
        public static final int jq_progress_flower_1_3 = 2130837807;
        public static final int jq_progress_flower_1_4 = 2130837808;
        public static final int jq_progress_flower_1_5 = 2130837809;
        public static final int jq_progress_flower_1_6 = 2130837810;
        public static final int jq_progress_flower_1_7 = 2130837811;
        public static final int jq_progress_flower_1_8 = 2130837812;
        public static final int jq_progress_flower_1_9 = 2130837813;
        public static final int jq_progress_marquee_1 = 2130837814;
        public static final int jq_progress_marquee_10 = 2130837815;
        public static final int jq_progress_marquee_11 = 2130837816;
        public static final int jq_progress_marquee_12 = 2130837817;
        public static final int jq_progress_marquee_13 = 2130837818;
        public static final int jq_progress_marquee_14 = 2130837819;
        public static final int jq_progress_marquee_2 = 2130837820;
        public static final int jq_progress_marquee_3 = 2130837821;
        public static final int jq_progress_marquee_4 = 2130837822;
        public static final int jq_progress_marquee_5 = 2130837823;
        public static final int jq_progress_marquee_6 = 2130837824;
        public static final int jq_progress_marquee_7 = 2130837825;
        public static final int jq_progress_marquee_8 = 2130837826;
        public static final int jq_progress_marquee_9 = 2130837827;
    }

    public static final class id {
        public static final int body = 2131493266;
        public static final int fdButtonCancel = 2131493240;
        public static final int fdButtonCreate = 2131493241;
        public static final int fdButtonNew = 2131493235;
        public static final int fdButtonSelect = 2131493236;
        public static final int fdEditTextFile = 2131493239;
        public static final int fdLinearLayoutCreate = 2131493237;
        public static final int fdLinearLayoutList = 2131493233;
        public static final int fdLinearLayoutSelect = 2131493234;
        public static final int fdrowimage = 2131493260;
        public static final int fdrowtext = 2131493261;
        public static final int message = 2131493264;
        public static final int path = 2131493242;
        public static final int percentage = 2131493265;
        public static final int progress = 2131493263;
        public static final int relativeLayout01 = 2131493232;
        public static final int textViewFilename = 2131493238;
        public static final int title = 2131493262;
    }

    public static final class layout {
        public static final int ja_file_dialog = 2130903097;
        public static final int jq_file_dialog_row = 2130903100;
        public static final int jq_progress_dialog_downloading = 2130903101;
        public static final int jq_progress_dialog_flower1 = 2130903102;
        public static final int jq_progress_dialog_flower2 = 2130903103;
        public static final int jq_progress_dialog_marquee = 2130903104;
    }

    public static final class string {
        public static final int app_name = 2131230720;
        public static final int cancel = 2131230741;
        public static final int cant_read_folder = 2131230744;
        public static final int confirm = 2131230740;
        public static final int create = 2131230748;
        public static final int download_completed = 2131230728;
        public static final int downloading = 2131230726;
        public static final int err = 2131230750;
        public static final int exc_context_null = 2131230736;
        public static final int exc_error_filepath = 2131230737;
        public static final int exc_network_no = 2131230731;
        public static final int exc_params_missing = 2131230735;
        public static final int exc_params_null = 2131230734;
        public static final int exc_sdcard_unavailable = 2131230738;
        public static final int exc_server_no_file = 2131230733;
        public static final int exc_server_no_response = 2131230732;
        public static final int exc_unkown_file_size = 2131230730;
        public static final int file_download = 2131230727;
        public static final int file_name = 2131230747;
        public static final int loading_please_waiting = 2131230723;
        public static final int location = 2131230743;
        public static final int menu_settings = 2131230721;
        public static final int name_tmpfile = 2131230739;
        public static final int nnew = 2131230745;
        public static final int no_data = 2131230749;
        public static final int select = 2131230746;
        public static final int title_activity_main = 2131230722;
        public static final int txt_comment = 2131230742;
        public static final int txt_download_exc = 2131230729;
        public static final int txt_download_exist = 2131230725;
        public static final int txt_file_exist = 2131230724;
    }

    public static final class style {
        public static final int AppTheme = 2131296256;
        public static final int ProgressDialogStyleFlower1 = 2131296257;
        public static final int ProgressDialogStyleFlower2 = 2131296258;
        public static final int ProgressDialogStyleMarquee = 2131296259;
    }
}
