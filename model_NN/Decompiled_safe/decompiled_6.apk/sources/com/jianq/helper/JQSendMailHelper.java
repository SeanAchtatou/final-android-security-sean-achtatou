package com.jianq.helper;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import com.jianq.common.ResultData;
import com.jianq.misc.StringEx;
import com.jianq.ui.JQCustomDialog;
import com.jianq.ui.JQUICallBack;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;

public class JQSendMailHelper {
    /* access modifiers changed from: private */
    public Context context;
    private List<String> mBcctoList;
    private List<String> mCctoList;
    private String mContent;
    private String mFromAddress;
    private int mImport;
    private String mProgressMessage;
    private List<String> mSendtoList;
    private String mSubject;
    private String mSubmitUrl;
    public JQUICallBack sendMailCallBack;

    static class MailImport {
        static final int HIGHT = 2;
        static final int LOWER = 0;
        static final int NORMAL = 1;

        MailImport() {
        }
    }

    public JQSendMailHelper(Context context2) {
        this(context2, StringEx.Empty, StringEx.Empty, StringEx.Empty);
    }

    public JQSendMailHelper(Context context2, String to, String subject, String content) {
        this.mImport = 1;
        this.sendMailCallBack = new JQUICallBack() {
            public void callBack(ResultData result) {
                if (result.hasError()) {
                    JQCustomDialog.getInst().showAlertDialog(JQSendMailHelper.this.context, "警告", "网络错误", "确定", 0, null);
                    return;
                }
                Log.d("SendMail", result.getStringData());
                try {
                    JQCustomDialog.getInst().showAlertDialog(JQSendMailHelper.this.context, "提示", result.getJsonData().getString("message"), "确定", 0, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            ((Activity) JQSendMailHelper.this.context).finish();
                        }
                    });
                } catch (JSONException e) {
                    JQCustomDialog.getInst().showAlertDialog(JQSendMailHelper.this.context, "提示", "邮件发送失败", "确定", 0, null);
                }
            }
        };
        this.context = context2;
        this.mSendtoList = new ArrayList();
        this.mCctoList = new ArrayList();
        this.mBcctoList = new ArrayList();
        for (String address : to.split("[;,]")) {
            addSendtoAddress(address);
        }
        setSubject(subject);
        setContent(content);
    }

    public void addSendtoAddress(String mailAddress) {
        if (mailAddress != null && !mailAddress.equals(StringEx.Empty)) {
            this.mSendtoList.add(mailAddress);
        }
    }

    public void addCctoAddress(String mailAddress) {
        if (mailAddress != null && !mailAddress.equals(StringEx.Empty)) {
            this.mCctoList.add(mailAddress);
        }
    }

    public void addBcctoAddress(String mailAddress) {
        if (mailAddress != null && !mailAddress.equals(StringEx.Empty)) {
            this.mBcctoList.add(mailAddress);
        }
    }

    public void clearSendtoAddress() {
        this.mSendtoList.clear();
    }

    public void clearCctoAddress() {
        this.mCctoList.clear();
    }

    public void clearBcctoAddress() {
        this.mBcctoList.clear();
    }

    public void setImport(int imp) {
        this.mImport = imp;
    }

    public void setSubject(String subject) {
        this.mSubject = subject;
    }

    public void setContent(String content) {
        this.mContent = content;
    }

    public void setProgressMessage(String progressMessage) {
        this.mProgressMessage = progressMessage;
    }

    public void setSubmitUrl(String submitUrl) {
        this.mSubmitUrl = submitUrl;
    }

    public void setFromAddress(String mailAddress) {
        this.mFromAddress = mailAddress;
    }

    public void submit() {
        String sendto = StringEx.Empty;
        String ccto = StringEx.Empty;
        String bccto = StringEx.Empty;
        for (String address : this.mSendtoList) {
            sendto = String.valueOf(sendto) + address + ";";
        }
        for (String address2 : this.mCctoList) {
            ccto = String.valueOf(ccto) + address2 + ";";
        }
        for (String address3 : this.mBcctoList) {
            bccto = String.valueOf(bccto) + address3 + ";";
        }
        if (sendto.length() > 0) {
            sendto = sendto.substring(0, sendto.lastIndexOf(";"));
        }
        if (ccto.length() > 0) {
            ccto = ccto.substring(0, ccto.lastIndexOf(";"));
        }
        if (bccto.length() > 0) {
            bccto = bccto.substring(0, bccto.lastIndexOf(";"));
        }
        JQNetworkHelper helper = new JQNetworkHelper(this.context);
        helper.progress = true;
        helper.setProgressMessage(this.mProgressMessage);
        helper.open("post", this.mSubmitUrl);
        helper.addParam("sendto", sendto);
        helper.addParam("ccto", ccto);
        helper.addParam("bccto", bccto);
        helper.addParam("from", this.mFromAddress == null ? StringEx.Empty : this.mFromAddress);
        helper.addParam("importance", new StringBuilder(String.valueOf(this.mImport)).toString());
        helper.addParam("subject", this.mSubject);
        helper.addParam("body", this.mContent);
        helper.setCallBack(this.sendMailCallBack);
        helper.send();
    }
}
