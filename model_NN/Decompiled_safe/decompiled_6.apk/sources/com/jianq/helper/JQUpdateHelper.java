package com.jianq.helper;

import android.content.Context;
import android.content.DialogInterface;
import com.jianq.misc.StringEx;
import com.jianq.net.download.DownloadManager;
import com.jianq.ui.JQCustomDialog;
import java.util.regex.Pattern;

public class JQUpdateHelper {
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public DownloadManager mDownloader;

    public JQUpdateHelper(Context context2) {
        this.context = context2;
    }

    private boolean checkVersion(String version) {
        if (Pattern.compile("[0-9]{1,4}\\.[0-9]{1,4}\\.[0-9]{1,4}").matcher(version).find()) {
            return true;
        }
        return false;
    }

    public boolean checkUpdate(String serverVersion, String clientVersion) {
        if (!checkVersion(serverVersion) || !checkVersion(clientVersion) || parseVersion(serverVersion) <= parseVersion(clientVersion)) {
            return false;
        }
        return true;
    }

    public void update(final String downloadUrl, final String saveFolder, DialogInterface.OnClickListener onCancelListener) {
        JQCustomDialog.getInst().showUpdateConfirmDialog(this.context, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                JQUpdateHelper.this.mDownloader = new DownloadManager(JQUpdateHelper.this.context, true);
                JQUpdateHelper.this.mDownloader.startDownload(downloadUrl, saveFolder);
            }
        }, onCancelListener);
    }

    public void destory() {
        if (this.mDownloader != null) {
            this.mDownloader.stop();
        }
    }

    private int parseVersion(String version) {
        String versionString = StringEx.Empty;
        String[] arr = version.split("\\.");
        for (int i = 0; i < arr.length; i++) {
            String s = arr[i];
            while (s.length() < 4) {
                s = "0" + s;
            }
            arr[i] = s;
        }
        for (int i2 = 0; i2 < arr.length; i2++) {
            versionString = String.valueOf(versionString) + arr[i2];
        }
        return Integer.parseInt(versionString);
    }
}
