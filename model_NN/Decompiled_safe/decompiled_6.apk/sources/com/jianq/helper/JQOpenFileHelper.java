package com.jianq.helper;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;
import com.jianq.common.JQFileEndings;
import com.jianq.ui.FileDialogActivity;
import com.jianq.util.JQOpenFiles;
import java.io.File;

public class JQOpenFileHelper {
    private Context context;

    public JQOpenFileHelper(Context context2) {
        this.context = context2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void openDirectory(File dir, int selectionMode) {
        if (dir.exists() && dir.isDirectory()) {
            Intent intent = new Intent(this.context, FileDialogActivity.class);
            intent.putExtra(FileDialogActivity.START_PATH, dir.getPath());
            intent.putExtra(FileDialogActivity.CAN_SELECT_DIR, false);
            intent.putExtra(FileDialogActivity.SELECTION_MODE, selectionMode);
            this.context.startActivity(intent);
        }
    }

    public void openDirectory(File dir) {
        openDirectory(dir, 2);
    }

    public void openFile(File currentPath) {
        Log.d("OpenFileHelper", currentPath.toString());
        if (currentPath == null || !currentPath.isFile()) {
            showMessage("对不起，这不是文件！");
            return;
        }
        String fileName = currentPath.toString();
        try {
            if (checkEndsWithInStringArray(fileName, JQFileEndings.getFileEndingsByName("fileEndingImage"))) {
                this.context.startActivity(JQOpenFiles.getImageFileIntent(currentPath));
            } else if (checkEndsWithInStringArray(fileName, JQFileEndings.getFileEndingsByName("fileEndingWebText"))) {
                this.context.startActivity(JQOpenFiles.getHtmlFileIntent(currentPath));
            } else if (checkEndsWithInStringArray(fileName, JQFileEndings.getFileEndingsByName("fileEndingPackage"))) {
                this.context.startActivity(JQOpenFiles.getApkFileIntent(currentPath));
            } else if (checkEndsWithInStringArray(fileName, JQFileEndings.getFileEndingsByName("fileEndingAudio"))) {
                this.context.startActivity(JQOpenFiles.getAudioFileIntent(currentPath));
            } else if (checkEndsWithInStringArray(fileName, JQFileEndings.getFileEndingsByName("fileEndingVideo"))) {
                this.context.startActivity(JQOpenFiles.getVideoFileIntent(currentPath));
            } else if (checkEndsWithInStringArray(fileName, JQFileEndings.getFileEndingsByName("fileEndingText"))) {
                this.context.startActivity(JQOpenFiles.getTextFileIntent(currentPath));
            } else if (checkEndsWithInStringArray(fileName, JQFileEndings.getFileEndingsByName("fileEndingPdf"))) {
                this.context.startActivity(JQOpenFiles.getPdfFileIntent(currentPath));
            } else if (checkEndsWithInStringArray(fileName, JQFileEndings.getFileEndingsByName("fileEndingWord"))) {
                this.context.startActivity(JQOpenFiles.getWordFileIntent(currentPath));
            } else if (checkEndsWithInStringArray(fileName, JQFileEndings.getFileEndingsByName("fileEndingExcel"))) {
                this.context.startActivity(JQOpenFiles.getExcelFileIntent(currentPath));
            } else if (checkEndsWithInStringArray(fileName, JQFileEndings.getFileEndingsByName("fileEndingPPT"))) {
                this.context.startActivity(JQOpenFiles.getPPTFileIntent(currentPath));
            } else {
                showMessage("无法打开，请安装相应的软件！");
            }
        } catch (ActivityNotFoundException e) {
            showMessage("无法打开，请安装相应的软件！");
        }
    }

    private void showMessage(String msg) {
        Toast.makeText(this.context, msg, 1).show();
    }

    private boolean checkEndsWithInStringArray(String checkItsEnd, String[] fileEndings) {
        for (String aEnd : fileEndings) {
            if (checkItsEnd.endsWith(aEnd)) {
                return true;
            }
        }
        return false;
    }
}
