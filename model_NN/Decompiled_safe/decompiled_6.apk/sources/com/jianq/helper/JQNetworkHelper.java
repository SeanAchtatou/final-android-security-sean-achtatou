package com.jianq.helper;

import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;
import com.jianq.common.ResultData;
import com.jianq.misc.StringEx;
import com.jianq.net.JQBasicNetwork;
import com.jianq.ui.JQCustomDialog;
import com.jianq.ui.JQUICallBack;
import com.jianq.util.JQUtils;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

public class JQNetworkHelper extends AsyncTask<String, String, ResultData> {
    public static final int CONNECTION_SERVER_ERROR = -2;
    public static final int HOST_ERROR = -6;
    public static final int NO_NETWORK = -1;
    public static final int PARAMETER_ERROR = -3;
    public static final int PARSE_JSON_ERROR = -4;
    public static final int RESPONSE_ERROR = -5;
    private Context context;
    private int flag = -10066329;
    /* access modifiers changed from: private */
    public boolean isCancel = false;
    private List<NameValuePair> nameValueList = new ArrayList();
    private String postEncoding;
    public boolean progress = true;
    private String progressMessage = null;
    private String reqUrl;
    private String resultEncoding;
    private JQUICallBack uicb;
    private String urlMethod = "post";

    public JQNetworkHelper(Context context2) {
        this.context = context2;
    }

    public void open(String urlMethod2, String reqUrl2) {
        this.urlMethod = urlMethod2;
        this.reqUrl = reqUrl2;
    }

    public void setCallBack(JQUICallBack uicb2) {
        this.uicb = uicb2;
    }

    public void setCallBack(JQUICallBack uicb2, int flag2) {
        this.uicb = uicb2;
        this.flag = flag2;
    }

    public void setPostEncoding(String enc) {
        this.postEncoding = enc;
    }

    public void setResultEncoding(String enc) {
        this.resultEncoding = enc;
    }

    public void setProgressMessage(String msgStr) {
        this.progressMessage = msgStr;
    }

    public void addParam(String name, String value) {
        this.nameValueList.add(new BasicNameValuePair(name, value));
    }

    public void send(String params) {
        execute(params);
    }

    public void send() {
        String params = StringEx.Empty;
        if (this.nameValueList.size() > 0) {
            for (NameValuePair nameValue : this.nameValueList) {
                params = String.valueOf(params) + nameValue.getName() + "=" + nameValue.getValue() + "&";
            }
            params = params.substring(0, params.lastIndexOf("&"));
        }
        send(params);
    }

    private String getResultString(HttpResponse response) throws IllegalStateException, IOException {
        if (response == null) {
            return StringEx.Empty;
        }
        StringBuffer result = new StringBuffer();
        if (response.getStatusLine().getStatusCode() == 200) {
            HttpEntity httpEntity = response.getEntity();
            if (this.resultEncoding != null) {
                result.append(EntityUtils.toString(httpEntity, this.resultEncoding));
            } else {
                result.append(EntityUtils.toString(httpEntity));
            }
        }
        return result.toString();
    }

    /* access modifiers changed from: protected */
    public ResultData doInBackground(String... params) {
        String para = params[0];
        HttpResponse response = null;
        ResultData resData = new ResultData();
        String resultString = StringEx.Empty;
        if (JQUtils.isNetworkAvailable(this.context)) {
            try {
                response = JQBasicNetwork.getInstance().getResponse(this.reqUrl, para, this.postEncoding);
            } catch (UnsupportedEncodingException e) {
                Log.e("JQNetworkHelper.doInbackground", "HTTP设置请求参数错误");
                resData.setIsError(true);
                resData.setErrorMessage("服务器请求参数错误");
                resData.error = -3;
                resData.addError(e);
            } catch (ClientProtocolException e2) {
                Log.e("JQNetworkHelper.doInbackground", "HttpClient执行错误,请确认服务器是否正确");
                resData.setIsError(true);
                resData.setErrorMessage("未找到服务器..");
                resData.error = -2;
                resData.addError(e2);
            } catch (IOException e3) {
                Log.e("JQNetworkHelper.doInbackground", "HttpClient执行错误，请确认服务器是否存在");
                resData.setIsError(true);
                resData.setErrorMessage("IOException错误，请检查参数或者访问的服务器是否正确..");
                resData.error = -2;
                resData.addError(e3);
            } catch (IllegalArgumentException e4) {
                Log.e("JQNetworkHelper.doInbackground", "HttpClient执行错误，请确认服务器是否存在");
                resData.setIsError(true);
                resData.setErrorMessage("IllegalArgumentException错误，请检查参数或者访问的服务器是否正确..");
                resData.error = -6;
                resData.addError(e4);
            }
            try {
                resultString = getResultString(response);
            } catch (IllegalStateException e5) {
                Log.e("JQNetworkHelper.doInbackground", "HttpResponse解析错误，请检查返回信息是否正确");
                resData.setIsError(true);
                resData.error = -5;
                resData.setErrorMessage("IllegalStateException错误：Http请求返回的信息不正确");
                resData.addError(e5);
            } catch (IOException e6) {
                Log.e("JQNetworkHelper.doInbackground", "HttpResponse解析错误，请检查返回信息是否正确");
                resData.setIsError(true);
                resData.error = -5;
                resData.setErrorMessage("IOException错误：HttpResponse请求内容有错误，请检查是否有返回内容。");
                resData.addError(e6);
            }
        } else {
            resData.setIsError(true);
            resData.error = -1;
            resData.setErrorMessage("网络没连接");
        }
        resData.setResultString(resultString);
        resData.what = this.flag;
        return resData;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(ResultData result) {
        if (!this.isCancel) {
            JQCustomDialog.getInst().cancelProgressDialog();
            this.uicb.callBack(result);
        }
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        if (this.progress) {
            JQCustomDialog.getInst().showProgressDialog(this.context, this.progressMessage);
            JQCustomDialog.getInst().setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialog) {
                    JQNetworkHelper.this.isCancel = true;
                    JQBasicNetwork.getInstance().cancelHttpPost();
                    dialog.dismiss();
                }
            });
        }
    }

    public JQCustomDialog getLoadingProgressDialog() {
        return JQCustomDialog.getInst();
    }
}
