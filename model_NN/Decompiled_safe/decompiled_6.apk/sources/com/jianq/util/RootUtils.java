package com.jianq.util;

import java.io.DataOutputStream;

public class RootUtils {
    private Boolean can_su;
    private SH su = new SH("su");

    public boolean canSU() {
        return canSU(false);
    }

    private boolean canSU(boolean force_check) {
        if (this.can_su == null || force_check) {
            this.can_su = Boolean.valueOf(this.su.runWaitFor("id").success());
        }
        return this.can_su.booleanValue();
    }

    private class CommandResult {
        public final Integer exit_value;

        CommandResult(Integer exit_value_in) {
            this.exit_value = exit_value_in;
        }

        public boolean success() {
            return this.exit_value != null && this.exit_value.intValue() == 0;
        }
    }

    private class SH {
        private String SHELL = "sh";

        public SH(String SHELL_in) {
            this.SHELL = SHELL_in;
        }

        public Process run(String s) {
            try {
                Process process = Runtime.getRuntime().exec(this.SHELL);
                DataOutputStream toProcess = new DataOutputStream(process.getOutputStream());
                toProcess.writeBytes("exec " + s + "\n");
                toProcess.flush();
                return process;
            } catch (Exception e) {
                return null;
            }
        }

        /* access modifiers changed from: private */
        public CommandResult runWaitFor(String s) {
            Process process = run(s);
            Integer exit_value = null;
            if (process != null) {
                try {
                    exit_value = Integer.valueOf(process.waitFor());
                } catch (InterruptedException e) {
                }
            }
            return new CommandResult(exit_value);
        }
    }
}
