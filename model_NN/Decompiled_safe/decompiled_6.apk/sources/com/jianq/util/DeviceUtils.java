package com.jianq.util;

import android.app.Activity;
import android.app.KeyguardManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import cn.com.jit.pnxclient.constant.PNXConfigConstant;
import com.jianq.misc.StringEx;

public class DeviceUtils {
    public static float Density = 0.0f;
    public static int ScreenHeightPixels = 0;
    public static int ScreenWidthPixels = 0;
    private static final String TAG = "DeviceUtils";
    private static DisplayMetrics dm;

    public static void loadScreenInfo(Activity activity) {
        if (dm == null) {
            dm = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        }
        ScreenWidthPixels = getScreenHeight();
        ScreenHeightPixels = getScreenWidth();
        Density = getDensity();
    }

    public static int getScreenWidth() {
        if (dm != null) {
            return dm.widthPixels;
        }
        throw new NullPointerException("DisplayMetrics is NULL, should invoke DeviceHelper.loadScreenInfo(activity) first!");
    }

    public static int getScreenWidth(Activity activity) {
        if (dm == null) {
            dm = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        }
        return dm.widthPixels;
    }

    public static int getScreenHeight(Activity activity) {
        if (dm == null) {
            dm = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        }
        return dm.heightPixels;
    }

    public static int getScreenHeight() {
        if (dm != null) {
            return dm.heightPixels;
        }
        throw new NullPointerException("DisplayMetrics is NULL, should invoke DeviceHelper.loadScreenInfo(activity) first!");
    }

    public static float getDensity() {
        if (dm != null) {
            return dm.density;
        }
        throw new NullPointerException("DisplayMetrics is NULL, should invoke DeviceHelper.loadScreenInfo(activity) first!");
    }

    public static String logScreenInfo(Activity activity) {
        if (dm == null) {
            dm = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        }
        String infoStr = "ScreenInfo [width=" + dm.widthPixels + ", height=" + dm.heightPixels + ", density=" + dm.density + ", densityDpi=" + dm.densityDpi + ", xDpi=" + dm.xdpi + ", yDpi=" + dm.ydpi + ", scaledDensity=" + dm.scaledDensity + "]";
        Log.d(TAG, infoStr);
        return infoStr;
    }

    public static boolean isScreenLocked(Context context) {
        if (((KeyguardManager) context.getSystemService("keyguard")).inKeyguardRestrictedInputMode()) {
            return true;
        }
        return false;
    }

    public static DisplayMetrics getDisplayMetrics() {
        if (dm != null) {
            return dm;
        }
        throw new NullPointerException("DisplayMetrics is NULL, should invoke DeviceHelper.loadScreenInfo(activity) first!");
    }

    public static boolean isWifiOk(Context ctx) {
        WifiManager wm;
        if (ctx == null || (wm = (WifiManager) ctx.getSystemService("wifi")) == null || !wm.isWifiEnabled()) {
            return false;
        }
        return true;
    }

    public static String getCpuInfo() {
        String mCpuInfo = shellExce(new String[]{"/system/bin/cat", "/proc/cpuinfo"});
        if (mCpuInfo != null) {
            mCpuInfo = mCpuInfo.replaceAll("\t", StringEx.Empty).replaceAll("\n", StringEx.Empty).replaceAll(" ", StringEx.Empty);
        }
        Log.d(TAG, "getCpuInfo mCpuInfo : " + mCpuInfo);
        return mCpuInfo;
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x004d A[SYNTHETIC, Splitter:B:29:0x004d] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0052 A[Catch:{ IOException -> 0x0056 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String shellExce(java.lang.String[] r9) {
        /*
            r7 = 0
            java.lang.ProcessBuilder r5 = new java.lang.ProcessBuilder
            r5.<init>(r9)
            r4 = 0
            r3 = 0
            r0 = 0
            java.lang.Process r4 = r5.start()     // Catch:{ Exception -> 0x0064, all -> 0x004a }
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0064, all -> 0x004a }
            r1.<init>()     // Catch:{ Exception -> 0x0064, all -> 0x004a }
            java.io.InputStream r3 = r4.getInputStream()     // Catch:{ Exception -> 0x0037, all -> 0x0061 }
            r6 = -1
        L_0x0017:
            r8 = -1
            int r6 = r3.read()     // Catch:{ Exception -> 0x0037, all -> 0x0061 }
            if (r8 != r6) goto L_0x0033
            java.lang.String r7 = new java.lang.String     // Catch:{ Exception -> 0x0037, all -> 0x0061 }
            byte[] r8 = r1.toByteArray()     // Catch:{ Exception -> 0x0037, all -> 0x0061 }
            r7.<init>(r8)     // Catch:{ Exception -> 0x0037, all -> 0x0061 }
            if (r1 == 0) goto L_0x002c
            r1.close()     // Catch:{ IOException -> 0x005b }
        L_0x002c:
            if (r3 == 0) goto L_0x005f
            r3.close()     // Catch:{ IOException -> 0x005b }
            r0 = r1
        L_0x0032:
            return r7
        L_0x0033:
            r1.write(r6)     // Catch:{ Exception -> 0x0037, all -> 0x0061 }
            goto L_0x0017
        L_0x0037:
            r2 = move-exception
            r0 = r1
        L_0x0039:
            r7 = 0
            if (r0 == 0) goto L_0x003f
            r0.close()     // Catch:{ IOException -> 0x0045 }
        L_0x003f:
            if (r3 == 0) goto L_0x0032
            r3.close()     // Catch:{ IOException -> 0x0045 }
            goto L_0x0032
        L_0x0045:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0032
        L_0x004a:
            r8 = move-exception
        L_0x004b:
            if (r0 == 0) goto L_0x0050
            r0.close()     // Catch:{ IOException -> 0x0056 }
        L_0x0050:
            if (r3 == 0) goto L_0x0055
            r3.close()     // Catch:{ IOException -> 0x0056 }
        L_0x0055:
            throw r8
        L_0x0056:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0055
        L_0x005b:
            r2 = move-exception
            r2.printStackTrace()
        L_0x005f:
            r0 = r1
            goto L_0x0032
        L_0x0061:
            r8 = move-exception
            r0 = r1
            goto L_0x004b
        L_0x0064:
            r2 = move-exception
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jianq.util.DeviceUtils.shellExce(java.lang.String[]):java.lang.String");
    }

    public static String getPeerid(Context context) {
        WifiManager wm = (WifiManager) context.getSystemService("wifi");
        if (wm == null || wm.getConnectionInfo() == null) {
            return null;
        }
        return (String.valueOf(wm.getConnectionInfo().getMacAddress()) + "004V").replaceAll(PNXConfigConstant.RESP_SPLIT_3, StringEx.Empty).replaceAll(",", StringEx.Empty).replaceAll("[.]", StringEx.Empty).toUpperCase();
    }

    public static boolean checkSDCard() {
        return Environment.getExternalStorageState().equals("mounted");
    }

    public static boolean isNetworkConnected(Context context) {
        NetworkInfo info;
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivity == null || (info = connectivity.getActiveNetworkInfo()) == null || !info.isConnected() || info.getState() != NetworkInfo.State.CONNECTED) {
            return false;
        }
        return true;
    }
}
