package com.jianq.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.jianq.misc.StringEx;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.regex.Pattern;
import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipFile;
import org.apache.tools.zip.ZipOutputStream;

public class ZipRarUtils {
    private static final int BUFFEREDSIZE = 1024;

    public static void unzip(String zipFileName, String targetPath2Place) throws Exception {
        try {
            new File(targetPath2Place).mkdirs();
            File f = new File(zipFileName);
            ZipFile zipFile = new ZipFile(zipFileName, "gbk");
            if (f.exists() || f.length() > 0) {
                String strPath = new File(targetPath2Place).getAbsolutePath();
                Enumeration e = zipFile.getEntries();
                while (e.hasMoreElements()) {
                    ZipEntry zipEnt = (ZipEntry) e.nextElement();
                    String gbkPath = zipEnt.getName();
                    if (zipEnt.isDirectory()) {
                        new File(String.valueOf(strPath) + File.separator + gbkPath).mkdirs();
                    } else {
                        BufferedInputStream bis = new BufferedInputStream(zipFile.getInputStream(zipEnt));
                        String gbkPath2 = zipEnt.getName();
                        String strtemp = String.valueOf(strPath) + File.separator + gbkPath2;
                        String strsubdir = gbkPath2;
                        for (int i = 0; i < strsubdir.length(); i++) {
                            if (strsubdir.substring(i, i + 1).equalsIgnoreCase("/")) {
                                File subdir = new File(String.valueOf(strPath) + File.separator + strsubdir.substring(0, i));
                                if (!subdir.exists()) {
                                    subdir.mkdir();
                                }
                            }
                        }
                        FileOutputStream fos = new FileOutputStream(strtemp);
                        BufferedOutputStream bos = new BufferedOutputStream(fos);
                        while (true) {
                            int c = bis.read();
                            if (c == -1) {
                                break;
                            }
                            bos.write((byte) c);
                        }
                        bos.close();
                        fos.close();
                    }
                }
                return;
            }
            throw new FileNotFoundException("The to be unzipped doesn't exist!");
        } catch (Exception e2) {
            e2.printStackTrace();
            throw e2;
        }
    }

    public static void zip(String inputFilename, String zipFilename) throws IOException {
        zip(new File(inputFilename), zipFilename);
    }

    public static void zip(File inputFile, String zipFilename) throws IOException {
        ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFilename));
        try {
            zip(inputFile, out, StringEx.Empty);
            out.close();
        } catch (IOException e) {
            throw e;
        } catch (Throwable th) {
            out.close();
            throw th;
        }
    }

    private static void zip(File inputFile, ZipOutputStream out, String base) throws IOException {
        if (inputFile.isDirectory()) {
            File[] inputFiles = inputFile.listFiles();
            out.putNextEntry(new ZipEntry(String.valueOf(base) + "/"));
            String base2 = base.length() == 0 ? StringEx.Empty : String.valueOf(base) + "/";
            for (int i = 0; i < inputFiles.length; i++) {
                zip(inputFiles[i], out, String.valueOf(base2) + inputFiles[i].getName());
            }
            return;
        }
        if (base.length() > 0) {
            out.putNextEntry(new ZipEntry(base));
        } else {
            out.putNextEntry(new ZipEntry(inputFile.getName()));
        }
        FileInputStream in = new FileInputStream(inputFile);
        try {
            byte[] by = new byte[1024];
            while (true) {
                int c = in.read(by);
                if (c == -1) {
                    in.close();
                    return;
                }
                out.write(by, 0, c);
            }
        } catch (IOException e) {
            throw e;
        } catch (Throwable th) {
            in.close();
            throw th;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:42:0x00ea A[SYNTHETIC, Splitter:B:42:0x00ea] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00f5 A[SYNTHETIC, Splitter:B:47:0x00f5] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0068 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean unrar(java.lang.String r16, java.lang.String r17) throws java.lang.Exception {
        /*
            r4 = 1
            java.io.File r10 = new java.io.File
            r0 = r16
            r10.<init>(r0)
            boolean r13 = r10.isFile()
            if (r13 != 0) goto L_0x0016
            java.lang.IllegalArgumentException r13 = new java.lang.IllegalArgumentException
            java.lang.String r14 = "The given file path to unRAR is not a file!"
            r13.<init>(r14)
            throw r13
        L_0x0016:
            boolean r13 = r10.exists()
            if (r13 != 0) goto L_0x0024
            java.io.FileNotFoundException r13 = new java.io.FileNotFoundException
            java.lang.String r14 = "The given file to unRAR is not exist!"
            r13.<init>(r14)
            throw r13
        L_0x0024:
            java.lang.String r13 = "\\"
            java.lang.String r14 = "/"
            r0 = r17
            java.lang.String r17 = r0.replace(r13, r14)
            java.lang.String r13 = "/"
            r0 = r17
            boolean r13 = r0.endsWith(r13)
            if (r13 != 0) goto L_0x004b
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            java.lang.String r14 = java.lang.String.valueOf(r17)
            r13.<init>(r14)
            java.lang.String r14 = "/"
            java.lang.StringBuilder r13 = r13.append(r14)
            java.lang.String r17 = r13.toString()
        L_0x004b:
            java.io.File r9 = new java.io.File
            r0 = r17
            r9.<init>(r0)
            boolean r13 = r9.exists()
            if (r13 != 0) goto L_0x005b
            r9.mkdir()
        L_0x005b:
            de.innosystec.unrar.Archive r1 = new de.innosystec.unrar.Archive
            r1.<init>(r10)
            java.util.List r6 = r1.getFileHeaders()
            java.util.Iterator r13 = r6.iterator()
        L_0x0068:
            boolean r14 = r13.hasNext()
            if (r14 != 0) goto L_0x006f
            return r4
        L_0x006f:
            java.lang.Object r5 = r13.next()
            de.innosystec.unrar.rarfile.FileHeader r5 = (de.innosystec.unrar.rarfile.FileHeader) r5
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            java.lang.String r15 = java.lang.String.valueOf(r17)
            r14.<init>(r15)
            java.lang.String r15 = r5.getFileNameW()
            java.lang.String r15 = r15.trim()
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r12 = r14.toString()
            boolean r14 = existZH(r12)
            if (r14 != 0) goto L_0x00ad
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            java.lang.String r15 = java.lang.String.valueOf(r17)
            r14.<init>(r15)
            java.lang.String r15 = r5.getFileNameString()
            java.lang.String r15 = r15.trim()
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r12 = r14.toString()
        L_0x00ad:
            java.lang.String r14 = "\\"
            java.lang.String r15 = "/"
            java.lang.String r12 = r12.replace(r14, r15)
            java.io.File r11 = new java.io.File
            r11.<init>(r12)
            boolean r14 = r5.isDirectory()
            if (r14 != 0) goto L_0x00f9
            java.io.File r3 = r11.getParentFile()
            if (r3 == 0) goto L_0x00cf
            boolean r14 = r3.exists()
            if (r14 != 0) goto L_0x00cf
            r3.mkdirs()
        L_0x00cf:
            r7 = 0
            java.io.FileOutputStream r8 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x00e3 }
            r8.<init>(r12)     // Catch:{ Exception -> 0x00e3 }
            r1.extractFile(r5, r8)     // Catch:{ Exception -> 0x0103, all -> 0x0100 }
            r8.flush()     // Catch:{ Exception -> 0x0103, all -> 0x0100 }
            if (r8 == 0) goto L_0x0068
            r8.close()     // Catch:{ Exception -> 0x00e1 }
            goto L_0x0068
        L_0x00e1:
            r14 = move-exception
            goto L_0x0068
        L_0x00e3:
            r2 = move-exception
        L_0x00e4:
            r2.printStackTrace()     // Catch:{ all -> 0x00f2 }
            r4 = 0
            if (r7 == 0) goto L_0x0068
            r7.close()     // Catch:{ Exception -> 0x00ef }
            goto L_0x0068
        L_0x00ef:
            r14 = move-exception
            goto L_0x0068
        L_0x00f2:
            r13 = move-exception
        L_0x00f3:
            if (r7 == 0) goto L_0x00f8
            r7.close()     // Catch:{ Exception -> 0x00fe }
        L_0x00f8:
            throw r13
        L_0x00f9:
            r11.mkdir()
            goto L_0x0068
        L_0x00fe:
            r14 = move-exception
            goto L_0x00f8
        L_0x0100:
            r13 = move-exception
            r7 = r8
            goto L_0x00f3
        L_0x0103:
            r2 = move-exception
            r7 = r8
            goto L_0x00e4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jianq.util.ZipRarUtils.unrar(java.lang.String, java.lang.String):boolean");
    }

    public static boolean existZH(String str) {
        return Pattern.compile("[\\u4e00-\\u9fa5]").matcher(str).find();
    }

    public static void openZip(Context ctx, String filePath) {
        Intent intent = new Intent("com.estrongs.action.PICK_DIRECTORY");
        intent.setData(Uri.fromFile(new File(filePath)));
        ctx.startActivity(intent);
    }
}
