package com.jianq.util;

import android.util.Log;
import cn.com.jit.pnxclient.constant.PNXConfigConstant;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class JQFileUtils {
    public static File createDir(String dirPath) {
        File file = new File(dirPath);
        if (!file.exists() && !file.mkdirs()) {
            return null;
        }
        return file;
    }

    public static File createFile(String filePath) throws IOException {
        File file = new File(filePath);
        if ((!file.exists() || !file.isFile()) && !file.createNewFile()) {
            return null;
        }
        return file;
    }

    public static boolean delete(String filePath) {
        Log.d("DeleteFile", filePath);
        if (!new File(filePath).exists()) {
            return false;
        }
        deleteFileOrDir(filePath);
        return true;
    }

    private static void deleteFileOrDir(String path) {
        File file = new File(path);
        if (file.isFile()) {
            file.delete();
            return;
        }
        for (File f : file.listFiles()) {
            deleteFileOrDir(f.getAbsolutePath());
        }
        file.delete();
    }

    public static boolean isExist(String filePath) {
        return new File(filePath).exists();
    }

    public static String getStringFromInputStream(InputStream iStream, String encoding) throws IOException {
        InputStreamReader isr;
        StringBuffer sb = new StringBuffer();
        if (encoding == null) {
            isr = new InputStreamReader(iStream);
        } else {
            isr = new InputStreamReader(iStream, encoding);
        }
        BufferedReader br = new BufferedReader(isr);
        while (true) {
            String line = br.readLine();
            if (line == null) {
                br.close();
                isr.close();
                iStream.close();
                return sb.toString();
            }
            sb.append(String.valueOf(line) + PNXConfigConstant.RESP_SPLIT_2);
        }
    }

    public static String getStringFromInputStream(InputStream iStream) throws IOException {
        return getStringFromInputStream(iStream, null);
    }
}
