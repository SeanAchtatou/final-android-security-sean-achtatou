package com.jianq.util;

import android.support.v4.view.MotionEventCompat;
import cn.com.jit.ida.util.pki.cipher.Mechanism;
import com.jianq.misc.StringEx;
import com.jianq.net.JQBasicNetwork;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import net.iharder.Base64;

public class JQEncrypt {
    private static byte[] exchangeBytes(byte[] inputbytes) throws Exception {
        byte[] inputbytes2 = shuffle(inputbytes);
        for (int i = 0; i < inputbytes2.length; i++) {
            inputbytes2[i] = exchangeByte(inputbytes2[i]);
        }
        return inputbytes2;
    }

    private static byte[] shuffle(byte[] inputbytes) throws Exception {
        int step_num = inputbytes.length / 2;
        for (int i = 0; i < step_num; i++) {
            byte buf = inputbytes[(i * 2) + 1];
            inputbytes[(i * 2) + 1] = inputbytes[i * 2];
            inputbytes[i * 2] = buf;
        }
        return inputbytes;
    }

    private static byte exchangeByte(byte b) {
        byte[] mask = {65, 77, 87, 99, 51, 110, 68, 53, 103, 83};
        byte r = b;
        for (int i = 0; i < mask.length; i++) {
            if (b == mask[i]) {
                r = mask[(mask.length - i) - 1];
            }
        }
        return r;
    }

    public static byte[] encrypt(byte[] inputbytes) throws Exception {
        return exchangeBytes(Base64.encodeBytesToBytes(GzipUtil.compress(inputbytes)));
    }

    public static byte[] decrypt(byte[] inputbytes) throws Exception {
        return GzipUtil.uncompress(Base64.decode(exchangeBytes(inputbytes)));
    }

    public static String encrypt(String input) throws Exception {
        try {
            return new String(encrypt(input.getBytes(JQBasicNetwork.UTF_8)), JQBasicNetwork.UTF_8);
        } catch (IOException e) {
            return StringEx.Empty;
        }
    }

    public static String decrypt(String input) throws Exception {
        try {
            return new String(decrypt(input.getBytes(JQBasicNetwork.UTF_8)), JQBasicNetwork.UTF_8);
        } catch (IOException e) {
            return StringEx.Empty;
        }
    }

    public static void main(String[] args) throws Exception {
        System.out.println("src=" + "据新华社电昨日，重庆市第三中级人民法院对重庆彭水县大学生村官任建宇因转发微博、发帖被劳教而提起行政诉讼一案作出宣判，认为任建宇的起诉超过法定起诉期限，因此裁定驳回其起诉。宣判前一日，重庆市劳教委以处理不当为由撤销了对任建宇的劳动教养决定，任建宇已解除限制人身自由的强制措施。");
        byte[] encryptBytes = encrypt("据新华社电昨日，重庆市第三中级人民法院对重庆彭水县大学生村官任建宇因转发微博、发帖被劳教而提起行政诉讼一案作出宣判，认为任建宇的起诉超过法定起诉期限，因此裁定驳回其起诉。宣判前一日，重庆市劳教委以处理不当为由撤销了对任建宇的劳动教养决定，任建宇已解除限制人身自由的强制措施。".getBytes(JQBasicNetwork.UTF_8));
        System.out.println("enc=" + new String(encryptBytes, JQBasicNetwork.UTF_8));
        System.out.println("dec=" + new String(decrypt(encryptBytes), JQBasicNetwork.UTF_8));
    }

    public static String toMD5(String input) {
        try {
            MessageDigest md5 = MessageDigest.getInstance(Mechanism.MD5);
            md5.update(input.getBytes());
            byte[] data = md5.digest();
            StringBuffer sb = new StringBuffer();
            if (data != null) {
                for (byte b : data) {
                    String code = Integer.toHexString(b & MotionEventCompat.ACTION_MASK);
                    if (code.length() < 2) {
                        sb.append(0);
                    }
                    sb.append(code);
                }
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }
}
