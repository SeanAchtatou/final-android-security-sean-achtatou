package com.jianq.util;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

public class AppUtils {
    public static boolean isAppInstalled(Context ctx, String packageName) {
        ApplicationInfo appInfo = null;
        try {
            appInfo = ctx.getPackageManager().getApplicationInfo(packageName, 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if (appInfo != null) {
            return true;
        }
        return false;
    }
}
