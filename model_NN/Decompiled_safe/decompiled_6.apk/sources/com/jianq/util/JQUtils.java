package com.jianq.util;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;
import java.text.SimpleDateFormat;

public class JQUtils {
    public static String getFormatTime() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Long.valueOf(System.currentTimeMillis()));
    }

    public static int getVersionCode(Context context) throws PackageManager.NameNotFoundException {
        return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
    }

    public static String getVersionName(Context context) throws PackageManager.NameNotFoundException {
        return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
    }

    public static int getStatusBarHeight(Activity activity) {
        Rect rect = new Rect();
        activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
        return rect.top;
    }

    public static void setFullScreen(Activity activity) {
        activity.requestWindowFeature(1);
        activity.getWindow().setFlags(1024, 1024);
    }

    public static void hideInputMethod(View view, Activity activity) {
        ((InputMethodManager) activity.getSystemService("input_method")).hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void showInputMethod(View view, Activity activity) {
        ((InputMethodManager) activity.getSystemService("input_method")).showSoftInput(view, 1);
    }

    public static void openMap(Context context, float x, float y) {
        context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("geo:" + x + "," + y)));
    }

    public static void doCall(Context context, String number) {
        context.startActivity(new Intent("android.intent.action.CALL", Uri.parse("tel:" + number)));
    }

    public static void openSMS(Context context, String number, String content) {
        Intent it = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:" + number));
        it.putExtra("sms_body", content);
        context.startActivity(it);
    }

    public static void openEmail(Context context, String[] sendTo, String[] ccTo, String subject, String content) {
        Intent it = new Intent("android.intent.action.SEND");
        it.putExtra("android.intent.extra.EMAIL", sendTo);
        it.putExtra("android.intent.extra.CC", ccTo);
        it.putExtra("android.intent.extra.TEXT", content);
        it.putExtra("android.intent.extra.SUBJECT", subject);
        it.setType("message/rfc822");
        context.startActivity(Intent.createChooser(it, "Choose Email Client"));
    }

    public static void openApplicationByPackageName(Context context, String packageName, String packageAndMainActivity) {
        Intent intent = new Intent();
        intent.setComponent(new ComponentName(packageName, packageAndMainActivity));
        intent.setAction("android.intent.action.VIEW");
        context.startActivity(intent);
    }

    public static boolean isNetworkAvailable(Context context) {
        NetworkInfo networkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (networkInfo == null) {
            Toast.makeText(context, "网络连接不可用，请重新设置。", 0).show();
            return false;
        } else if (networkInfo.isAvailable()) {
            return true;
        } else {
            Toast.makeText(context, "当前网络未连接...", 0).show();
            return false;
        }
    }

    public static int dip2px(Context context, float dpValue) {
        return (int) ((dpValue * context.getResources().getDisplayMetrics().density) + 0.5f);
    }

    public static int px2dip(Context context, float pxValue) {
        return (int) ((pxValue / context.getResources().getDisplayMetrics().density) + 0.5f);
    }
}
