package com.jianq.misc;

public class StringEx {
    public static final String Empty = "";

    public static boolean isNullOrEmpty(String str) {
        return str == null || Empty.equals(str.trim());
    }

    public static String insertWhitespace(String src) {
        String newStr = src;
        if (isNullOrEmpty(src)) {
            return newStr;
        }
        if (src.length() == 1) {
            return " " + src + " ";
        }
        if (src.length() == 2) {
            return String.valueOf(src.substring(0, 1)) + " " + src.substring(1);
        }
        return newStr;
    }
}
