package com.jianq.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import com.jianq.R;

public class JQCustomDialog {
    public static JQCustomDialog customDialog;
    private int mLayoutResId = -1;
    private DialogInterface.OnCancelListener mOnCancelListener;
    private ProgressDialog progressDialog = null;

    public static JQCustomDialog getInst() {
        if (customDialog == null) {
            customDialog = new JQCustomDialog();
        }
        return customDialog;
    }

    public JQCustomDialog setContentView(int layoutResId) {
        this.mLayoutResId = layoutResId;
        return this;
    }

    public void showProgressDialog(Context context) {
        showProgressDialog(context, context.getString(R.string.loading_please_waiting));
    }

    public void showProgressDialog(Context context, String message) {
        if (message == null) {
            showProgressDialog(context);
            return;
        }
        if (this.progressDialog != null) {
            this.progressDialog.dismiss();
        }
        this.progressDialog = new ProgressDialog(context);
        this.progressDialog.setMessage(message);
        this.progressDialog.setIndeterminate(true);
        this.progressDialog.setCancelable(true);
        this.progressDialog.setCanceledOnTouchOutside(false);
        if (this.mOnCancelListener != null) {
            this.progressDialog.setOnCancelListener(this.mOnCancelListener);
        }
        this.progressDialog.show();
        if (this.mLayoutResId != -1) {
            this.progressDialog.setContentView(this.mLayoutResId);
        }
    }

    public JQCustomDialog setOnCancelListener(DialogInterface.OnCancelListener listener) {
        this.mOnCancelListener = listener;
        return this;
    }

    public void cancelProgressDialog() {
        if (this.progressDialog != null && this.progressDialog.isShowing()) {
            this.progressDialog.dismiss();
        }
    }

    public void showAlertDialog(Context context, String title, String msg, String buttonText, int iconid, DialogInterface.OnClickListener listener) {
        String btnText = "确定";
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("信息");
        if (iconid > 0) {
            builder.setIcon(iconid);
        }
        if (title != null) {
            builder.setTitle(title);
        }
        if (msg != null) {
            builder.setMessage(msg);
        }
        if (buttonText != null) {
            btnText = buttonText;
        }
        builder.setPositiveButton(btnText, listener);
        builder.create().show();
    }

    public void showConfirmDialog(Context context, String title, String msg, String lStr, String rStr, int iconid, DialogInterface.OnClickListener listener) {
        String lBtnStr = "确定";
        String rBtnStr = "取消";
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("提示信息");
        if (iconid > 0) {
            builder.setIcon(iconid);
        }
        if (title != null) {
            builder.setTitle(title);
        }
        if (msg != null) {
            builder.setMessage(msg);
        }
        if (lStr != null && lStr.length() > 0) {
            lBtnStr = lStr;
        }
        if (rStr != null && rStr.length() > 0) {
            rBtnStr = rStr;
        }
        builder.setPositiveButton(lBtnStr, listener);
        builder.setNegativeButton(rBtnStr, listener);
        builder.create().show();
    }

    public void showConfirmDialog(Context context, String title, String msg, String lStr, String rStr, int iconid, DialogInterface.OnClickListener lListener, DialogInterface.OnClickListener rListener) {
        String lBtnStr = "确定";
        String rBtnStr = "取消";
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("提示信息");
        if (iconid > 0) {
            builder.setIcon(iconid);
        }
        if (title != null) {
            builder.setTitle(title);
        }
        if (msg != null) {
            builder.setMessage(msg);
        }
        if (lStr != null && lStr.length() > 0) {
            lBtnStr = lStr;
        }
        if (rStr != null && rStr.length() > 0) {
            rBtnStr = rStr;
        }
        builder.setPositiveButton(lBtnStr, lListener);
        builder.setNegativeButton(rBtnStr, rListener);
        builder.create().show();
    }

    public void showUpdateConfirmDialog(Context context, DialogInterface.OnClickListener leftListener, DialogInterface.OnClickListener rightListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setTitle("更新提醒");
        builder.setIcon(17301659);
        builder.setMessage("发现新版本，是否升级？");
        builder.setPositiveButton("现在升级", leftListener);
        builder.setNegativeButton("下次提醒", rightListener);
        builder.create().show();
    }
}
