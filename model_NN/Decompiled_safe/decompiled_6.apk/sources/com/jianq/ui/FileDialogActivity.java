package com.jianq.ui;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.jianq.R;
import com.jianq.helper.JQOpenFileHelper;
import com.jianq.misc.StringEx;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

public class FileDialogActivity extends ListActivity {
    public static final String CAN_SELECT_DIR = "CAN_SELECT_DIR";
    public static final String CAN_SELECT_OTHER_DIR = "CAN_SELECT_OTHER_DIR";
    public static final String FORMAT_FILTER = "FORMAT_FILTER";
    public static final String INTENT_ACTION = "com.jianq.OPEN_FILE_DIALOG";
    private static final String ITEM_IMAGE = "image";
    private static final String ITEM_KEY = "key";
    public static final String RESULT_PATH = "RESULT_PATH";
    private static final String ROOT = "/";
    public static final String SELECTION_MODE = "SELECTION_MODE";
    public static final String START_PATH = "START_PATH";
    private boolean canSelectDir = false;
    private boolean canSelectOtherDir = false;
    /* access modifiers changed from: private */
    public String currentPath = ROOT;
    private String[] formatFilter = null;
    private InputMethodManager inputManager;
    private HashMap<String, Integer> lastPositions = new HashMap<>();
    private LinearLayout layoutCreate;
    private LinearLayout layoutSelect;
    /* access modifiers changed from: private */
    public EditText mFileName;
    private ArrayList<HashMap<String, Object>> mList;
    private String mStartPath;
    private TextView myPath;
    private String parentPath;
    private List<String> path = null;
    private Button selectButton;
    /* access modifiers changed from: private */
    public File selectedFile;
    private int selectionMode = 0;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setResult(0, getIntent());
        setContentView(R.layout.ja_file_dialog);
        this.myPath = (TextView) findViewById(R.id.path);
        this.mFileName = (EditText) findViewById(R.id.fdEditTextFile);
        this.inputManager = (InputMethodManager) getSystemService("input_method");
        this.selectButton = (Button) findViewById(R.id.fdButtonSelect);
        this.selectButton.setEnabled(false);
        this.selectButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (FileDialogActivity.this.selectedFile != null) {
                    FileDialogActivity.this.getIntent().putExtra(FileDialogActivity.RESULT_PATH, FileDialogActivity.this.selectedFile.getPath());
                    FileDialogActivity.this.setResult(-1, FileDialogActivity.this.getIntent());
                    FileDialogActivity.this.finish();
                }
            }
        });
        Button newButton = (Button) findViewById(R.id.fdButtonNew);
        newButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FileDialogActivity.this.setCreateVisible(v);
                FileDialogActivity.this.mFileName.setText(StringEx.Empty);
                FileDialogActivity.this.mFileName.requestFocus();
            }
        });
        this.selectionMode = getIntent().getIntExtra(SELECTION_MODE, 0);
        this.formatFilter = getIntent().getStringArrayExtra(FORMAT_FILTER);
        this.canSelectDir = getIntent().getBooleanExtra(CAN_SELECT_DIR, false);
        this.canSelectOtherDir = getIntent().getBooleanExtra(CAN_SELECT_OTHER_DIR, false);
        if (this.selectionMode == 1) {
            newButton.setEnabled(false);
        }
        this.layoutSelect = (LinearLayout) findViewById(R.id.fdLinearLayoutSelect);
        this.layoutCreate = (LinearLayout) findViewById(R.id.fdLinearLayoutCreate);
        this.layoutCreate.setVisibility(8);
        if (this.selectionMode == 2) {
            this.layoutSelect.setVisibility(8);
        }
        ((Button) findViewById(R.id.fdButtonCancel)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FileDialogActivity.this.setSelectVisible(v);
            }
        });
        ((Button) findViewById(R.id.fdButtonCreate)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (FileDialogActivity.this.mFileName.getText().length() > 0) {
                    FileDialogActivity.this.getIntent().putExtra(FileDialogActivity.RESULT_PATH, String.valueOf(FileDialogActivity.this.currentPath) + FileDialogActivity.ROOT + ((Object) FileDialogActivity.this.mFileName.getText()));
                    FileDialogActivity.this.setResult(-1, FileDialogActivity.this.getIntent());
                    FileDialogActivity.this.finish();
                }
            }
        });
        this.mStartPath = getIntent().getStringExtra(START_PATH);
        this.mStartPath = this.mStartPath != null ? this.mStartPath : ROOT;
        if (this.canSelectDir) {
            this.selectedFile = new File(this.mStartPath);
            this.selectButton.setEnabled(true);
        }
        getDir(this.mStartPath);
    }

    private void getDir(String dirPath) {
        boolean useAutoSelection = dirPath.length() < this.currentPath.length();
        Integer position = this.lastPositions.get(this.parentPath);
        getDirImpl(dirPath);
        if (position != null && useAutoSelection) {
            getListView().setSelection(position.intValue());
        }
    }

    private void getDirImpl(String dirPath) {
        this.currentPath = dirPath;
        ArrayList arrayList = new ArrayList();
        this.path = new ArrayList();
        this.mList = new ArrayList<>();
        File f = new File(this.currentPath);
        File[] files = f.listFiles();
        if (files == null) {
            this.currentPath = ROOT;
            f = new File(this.currentPath);
            files = f.listFiles();
        }
        this.myPath.setText(((Object) getText(R.string.location)) + ": " + this.currentPath);
        if (!this.currentPath.equals(ROOT)) {
            arrayList.add(ROOT);
            addItem(ROOT, R.drawable.jq_file_dialog_folder);
            this.path.add(ROOT);
            arrayList.add("../");
            addItem("../", R.drawable.jq_file_dialog_folder);
            this.path.add(f.getParent());
            this.parentPath = f.getParent();
        }
        TreeMap<String, String> dirsMap = new TreeMap<>();
        TreeMap<String, String> dirsPathMap = new TreeMap<>();
        TreeMap<String, String> filesMap = new TreeMap<>();
        TreeMap<String, String> filesPathMap = new TreeMap<>();
        for (File file : files) {
            if (file.isDirectory()) {
                String dirName = file.getName();
                dirsMap.put(dirName, dirName);
                dirsPathMap.put(dirName, file.getPath());
            } else {
                String fileName = file.getName();
                String fileNameLwr = fileName.toLowerCase();
                if (this.formatFilter != null) {
                    boolean contains = false;
                    int i = 0;
                    while (true) {
                        if (i >= this.formatFilter.length) {
                            break;
                        } else if (fileNameLwr.endsWith(this.formatFilter[i].toLowerCase())) {
                            contains = true;
                            break;
                        } else {
                            i++;
                        }
                    }
                    if (contains) {
                        filesMap.put(fileName, fileName);
                        filesPathMap.put(fileName, file.getPath());
                    }
                } else {
                    filesMap.put(fileName, fileName);
                    filesPathMap.put(fileName, file.getPath());
                }
            }
        }
        arrayList.addAll(dirsMap.tailMap(StringEx.Empty).values());
        arrayList.addAll(filesMap.tailMap(StringEx.Empty).values());
        this.path.addAll(dirsPathMap.tailMap(StringEx.Empty).values());
        this.path.addAll(filesPathMap.tailMap(StringEx.Empty).values());
        SimpleAdapter fileList = new SimpleAdapter(this, this.mList, R.layout.jq_file_dialog_row, new String[]{ITEM_KEY, ITEM_IMAGE}, new int[]{R.id.fdrowtext, R.id.fdrowimage});
        for (String dir : dirsMap.tailMap(StringEx.Empty).values()) {
            addItem(dir, R.drawable.jq_file_dialog_folder);
        }
        for (String file2 : filesMap.tailMap(StringEx.Empty).values()) {
            addItem(file2, R.drawable.jq_file_dialog_file);
        }
        fileList.notifyDataSetChanged();
        setListAdapter(fileList);
    }

    private void addItem(String fileName, int imageId) {
        HashMap<String, Object> item = new HashMap<>();
        item.put(ITEM_KEY, fileName);
        item.put(ITEM_IMAGE, Integer.valueOf(imageId));
        this.mList.add(item);
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        File file = new File(this.path.get(position));
        setSelectVisible(v);
        if (file.isDirectory()) {
            this.selectButton.setEnabled(false);
            if (file.canRead()) {
                this.lastPositions.put(this.currentPath, Integer.valueOf(position));
                getDir(this.path.get(position));
                if (this.canSelectDir) {
                    this.selectedFile = file;
                    v.setSelected(true);
                    this.selectButton.setEnabled(true);
                    return;
                }
                return;
            }
            new AlertDialog.Builder(this).setIcon(R.drawable.jq_logo).setTitle("[" + file.getName() + "] " + ((Object) getText(R.string.cant_read_folder))).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            }).show();
            return;
        }
        this.selectedFile = file;
        v.setSelected(true);
        this.selectButton.setEnabled(true);
        new JQOpenFileHelper(this).openFile(this.selectedFile);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        this.selectButton.setEnabled(false);
        if (this.layoutCreate.getVisibility() == 0) {
            this.layoutCreate.setVisibility(8);
            if (this.selectionMode != 2) {
                this.layoutSelect.setVisibility(0);
            } else {
                this.layoutSelect.setVisibility(8);
            }
        } else if (this.currentPath.equals(ROOT) || this.currentPath.equals(this.mStartPath)) {
            return super.onKeyDown(keyCode, event);
        } else {
            getDir(this.parentPath);
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void setCreateVisible(View v) {
        this.layoutCreate.setVisibility(0);
        this.layoutSelect.setVisibility(8);
        this.inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
        this.selectButton.setEnabled(false);
    }

    /* access modifiers changed from: private */
    public void setSelectVisible(View v) {
        this.layoutCreate.setVisibility(8);
        if (this.selectionMode != 2) {
            this.layoutSelect.setVisibility(0);
        } else {
            this.layoutSelect.setVisibility(8);
        }
        this.inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
        this.selectButton.setEnabled(false);
    }
}
