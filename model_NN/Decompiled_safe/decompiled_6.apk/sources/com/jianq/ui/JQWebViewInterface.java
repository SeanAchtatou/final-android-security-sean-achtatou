package com.jianq.ui;

public interface JQWebViewInterface {
    void jqDownload(String str, String str2);

    void jqListener(String str, String str2, String str3);

    void jqPost(String str, String str2);

    void jqSelect(String str, String str2);

    void jqSelectPeople(String str, String str2);

    void jqSkipout();
}
