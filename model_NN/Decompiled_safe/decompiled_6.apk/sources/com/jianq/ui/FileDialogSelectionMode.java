package com.jianq.ui;

public class FileDialogSelectionMode {
    public static final int MODE_CREATE = 0;
    public static final int MODE_OPEN = 1;
    public static final int MODE_READ_ONLY = 2;
}
