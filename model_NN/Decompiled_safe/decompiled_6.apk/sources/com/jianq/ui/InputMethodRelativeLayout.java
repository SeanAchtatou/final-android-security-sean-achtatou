package com.jianq.ui;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import com.jianq.util.DeviceUtils;

public class InputMethodRelativeLayout extends RelativeLayout {
    private OnResizeListener mResizeListener;
    private int mVerticalOffet;
    private int mVisiableHeight = -1;

    public interface OnResizeListener {
        void onResize(int i, int i2, int i3, int i4);
    }

    public InputMethodRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (context instanceof Activity) {
            this.mVerticalOffet = DeviceUtils.getScreenHeight((Activity) context) / 2;
        } else {
            this.mVerticalOffet = 240;
        }
    }

    public void setOnResizeListener(OnResizeListener listener) {
        this.mResizeListener = listener;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        if (this.mVisiableHeight == -1) {
            this.mVisiableHeight = h;
        }
        if (oldh > h) {
            scrollTo(0, this.mVerticalOffet);
        }
        if (oldh > 0 && h == this.mVisiableHeight) {
            scrollTo(0, 0);
        }
        if (this.mResizeListener != null) {
            this.mResizeListener.onResize(w, h, oldw, oldh);
        }
        super.onSizeChanged(w, h, oldw, oldh);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, this.mVerticalOffet + heightMeasureSpec);
    }

    public boolean isInEditMode() {
        return true;
    }
}
