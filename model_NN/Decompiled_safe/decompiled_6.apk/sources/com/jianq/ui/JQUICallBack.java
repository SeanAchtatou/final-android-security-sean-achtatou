package com.jianq.ui;

import com.jianq.common.ResultData;

public interface JQUICallBack {
    void callBack(ResultData resultData);
}
