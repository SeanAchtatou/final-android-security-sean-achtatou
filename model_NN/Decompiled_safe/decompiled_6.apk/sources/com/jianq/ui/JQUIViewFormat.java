package com.jianq.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.jianq.common.JQDeviceInformation;
import com.jianq.common.JQFrameworkConfig;

public class JQUIViewFormat {
    /* access modifiers changed from: private */
    public Context context;

    public JQUIViewFormat(Context context2) {
        this.context = context2;
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    public WebView formatWebView(JQWebViewInterface contextImplementsJQWebViewInterface, WebView webView) {
        WebView view = webView;
        view.getSettings().setJavaScriptEnabled(true);
        view.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        view.addJavascriptInterface(new JQObject(contextImplementsJQWebViewInterface), "jq");
        view.setWebChromeClient(new WebChromeClient() {
            public boolean onJsConfirm(WebView view, String url, String message, final JsResult result) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(JQUIViewFormat.this.context).setTitle("提示").setIcon(17301659).setMessage(message).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        result.confirm();
                    }
                }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        result.cancel();
                    }
                });
                dialog.setCancelable(false);
                dialog.create();
                dialog.show();
                return true;
            }

            public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(JQUIViewFormat.this.context).setTitle("提示").setIcon(17301659).setMessage(message).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        result.confirm();
                    }
                });
                dialog.setCancelable(false);
                dialog.create();
                dialog.show();
                return true;
            }
        });
        view.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return true;
            }
        });
        return view;
    }

    class JQObject {
        private String CPath = "file:///android_asset/";
        private String callBackFunction;
        private String callOperate;
        private String callParam;
        private String fontSize;
        private String imei = JQDeviceInformation.getIMEI();
        private String os = "android";
        private String screenHeight;
        private String screenWidth;
        private String server = JQFrameworkConfig.getInst().getConfigValue("host");
        private String serverPort = JQFrameworkConfig.getInst().getConfigValue("port");
        private JQWebViewInterface webViewInterface;

        public JQObject(JQWebViewInterface webViewInterface2) {
            this.screenWidth = new StringBuilder().append(JQDeviceInformation.getScreenWidth((Activity) JQUIViewFormat.this.context)).toString();
            this.screenHeight = new StringBuilder().append(JQDeviceInformation.getScreenHeight((Activity) JQUIViewFormat.this.context)).toString();
            this.fontSize = JQFrameworkConfig.getInst().getConfigValue("fontSize") != null ? JQFrameworkConfig.getInst().getConfigValue("fontSize") : "18px";
            this.webViewInterface = webViewInterface2;
        }

        public String getServer() {
            return this.server;
        }

        public String getServerPort() {
            return this.serverPort;
        }

        public String getImei() {
            return this.imei;
        }

        public String getOs() {
            return this.os;
        }

        public String getCPath() {
            return this.CPath;
        }

        public String getScreenWidth() {
            return this.screenWidth;
        }

        public String getScreenHeight() {
            return this.screenHeight;
        }

        public String getFontSize() {
            return this.fontSize;
        }

        public String getCallOperate() {
            return this.callOperate;
        }

        public void setCallOperate(String callOperate2) {
            this.callOperate = callOperate2;
        }

        public String getCallParam() {
            return this.callParam;
        }

        public void setCallParam(String callParam2) {
            this.callParam = callParam2;
        }

        public String getCallBackFunction() {
            return this.callBackFunction;
        }

        public void setCallBackFunction(String callBackFunction2) {
            this.callBackFunction = callBackFunction2;
        }

        public void call(String operate, String param, String callBackFn) {
            if (operate != null) {
                setCallOperate(operate);
            }
            if (param != null) {
                setCallParam(param);
            }
            if (callBackFn != null) {
                setCallBackFunction(callBackFn);
            }
            Log.d("call.operate", operate);
            if (operate.equalsIgnoreCase("JQDownload")) {
                this.webViewInterface.jqDownload(param, callBackFn);
            } else if (operate.equalsIgnoreCase("JQSelect")) {
                this.webViewInterface.jqSelect(param, callBackFn);
            } else if (operate.equalsIgnoreCase("JQPost")) {
                this.webViewInterface.jqPost(param, callBackFn);
            } else if (operate.equalsIgnoreCase("JQSelectPeople")) {
                this.webViewInterface.jqSelectPeople(param, callBackFn);
            } else if (operate.equalsIgnoreCase("JQSkipout")) {
                this.webViewInterface.jqSkipout();
            } else {
                this.webViewInterface.jqListener(operate, param, callBackFn);
            }
        }

        public void call(String operate) {
            call(operate, null);
        }

        public void call(String operate, String param) {
            call(operate, param, null);
        }
    }
}
