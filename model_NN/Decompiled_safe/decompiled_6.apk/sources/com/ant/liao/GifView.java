package com.ant.liao;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import java.io.InputStream;

public class GifView extends View implements GifAction {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$ant$liao$GifView$GifImageType;
    private GifImageType animationType;
    /* access modifiers changed from: private */
    public Bitmap currentImage;
    private DrawThread drawThread;
    /* access modifiers changed from: private */
    public GifDecoder gifDecoder;
    /* access modifiers changed from: private */
    public boolean isRun;
    /* access modifiers changed from: private */
    public boolean pause;
    private Rect rect;
    /* access modifiers changed from: private */
    public Handler redrawHandler;
    private int showHeight;
    private int showWidth;

    static /* synthetic */ int[] $SWITCH_TABLE$com$ant$liao$GifView$GifImageType() {
        int[] iArr = $SWITCH_TABLE$com$ant$liao$GifView$GifImageType;
        if (iArr == null) {
            iArr = new int[GifImageType.values().length];
            try {
                iArr[GifImageType.COVER.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[GifImageType.SYNC_DECODER.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[GifImageType.WAIT_FINISH.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$ant$liao$GifView$GifImageType = iArr;
        }
        return iArr;
    }

    public enum GifImageType {
        WAIT_FINISH(0),
        SYNC_DECODER(1),
        COVER(2);
        
        final int nativeInt;

        private GifImageType(int i) {
            this.nativeInt = i;
        }
    }

    public GifView(Context context) {
        super(context);
        this.gifDecoder = null;
        this.currentImage = null;
        this.isRun = true;
        this.pause = false;
        this.showWidth = -1;
        this.showHeight = -1;
        this.rect = null;
        this.drawThread = null;
        this.animationType = GifImageType.SYNC_DECODER;
        this.redrawHandler = new Handler() {
            public void handleMessage(Message msg) {
                GifView.this.invalidate();
            }
        };
    }

    public GifView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public GifView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.gifDecoder = null;
        this.currentImage = null;
        this.isRun = true;
        this.pause = false;
        this.showWidth = -1;
        this.showHeight = -1;
        this.rect = null;
        this.drawThread = null;
        this.animationType = GifImageType.SYNC_DECODER;
        this.redrawHandler = new Handler() {
            public void handleMessage(Message msg) {
                GifView.this.invalidate();
            }
        };
    }

    private void setGifDecoderImage(byte[] gif) {
        if (this.gifDecoder != null) {
            this.gifDecoder.free();
            this.gifDecoder = null;
        }
        this.gifDecoder = new GifDecoder(gif, this);
        this.gifDecoder.start();
    }

    private void setGifDecoderImage(InputStream is) {
        if (this.gifDecoder != null) {
            this.gifDecoder.free();
            this.gifDecoder = null;
        }
        this.gifDecoder = new GifDecoder(is, this);
        this.gifDecoder.start();
    }

    public void setGifImage(byte[] gif) {
        setGifDecoderImage(gif);
    }

    public void setGifImage(InputStream is) {
        setGifDecoderImage(is);
    }

    public void setGifImage(int resId) {
        setGifDecoderImage(getResources().openRawResource(resId));
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.gifDecoder != null) {
            if (this.currentImage == null) {
                this.currentImage = this.gifDecoder.getImage();
            }
            if (this.currentImage != null) {
                int saveCount = canvas.getSaveCount();
                canvas.save();
                canvas.translate((float) getPaddingLeft(), (float) getPaddingTop());
                if (this.showWidth == -1) {
                    canvas.drawBitmap(this.currentImage, 0.0f, 0.0f, (Paint) null);
                } else {
                    canvas.drawBitmap(this.currentImage, (Rect) null, this.rect, (Paint) null);
                }
                canvas.restoreToCount(saveCount);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int w;
        int h;
        int pleft = getPaddingLeft();
        int pright = getPaddingRight();
        int ptop = getPaddingTop();
        int pbottom = getPaddingBottom();
        if (this.gifDecoder == null) {
            w = 1;
            h = 1;
        } else {
            w = this.gifDecoder.width;
            h = this.gifDecoder.height;
        }
        setMeasuredDimension(resolveSize(Math.max(w + pleft + pright, getSuggestedMinimumWidth()), widthMeasureSpec), resolveSize(Math.max(h + ptop + pbottom, getSuggestedMinimumHeight()), heightMeasureSpec));
    }

    public void showCover() {
        if (this.gifDecoder != null) {
            this.pause = true;
            this.currentImage = this.gifDecoder.getImage();
            invalidate();
        }
    }

    public void showAnimation() {
        if (this.pause) {
            this.pause = false;
        }
    }

    public void setGifImageType(GifImageType type) {
        if (this.gifDecoder == null) {
            this.animationType = type;
        }
    }

    public void setShowDimension(int width, int height) {
        if (width > 0 && height > 0) {
            this.showWidth = width;
            this.showHeight = height;
            this.rect = new Rect();
            this.rect.left = 0;
            this.rect.top = 0;
            this.rect.right = width;
            this.rect.bottom = height;
        }
    }

    public void parseOk(boolean parseStatus, int frameIndex) {
        if (!parseStatus) {
            return;
        }
        if (this.gifDecoder != null) {
            switch ($SWITCH_TABLE$com$ant$liao$GifView$GifImageType()[this.animationType.ordinal()]) {
                case 1:
                    if (frameIndex != -1) {
                        return;
                    }
                    if (this.gifDecoder.getFrameCount() > 1) {
                        new DrawThread(this, null).start();
                        return;
                    } else {
                        reDraw();
                        return;
                    }
                case 2:
                    if (frameIndex == 1) {
                        this.currentImage = this.gifDecoder.getImage();
                        reDraw();
                        return;
                    } else if (frameIndex == -1) {
                        reDraw();
                        return;
                    } else if (this.drawThread == null) {
                        this.drawThread = new DrawThread(this, null);
                        this.drawThread.start();
                        return;
                    } else {
                        return;
                    }
                case 3:
                    if (frameIndex == 1) {
                        this.currentImage = this.gifDecoder.getImage();
                        reDraw();
                        return;
                    } else if (frameIndex != -1) {
                        return;
                    } else {
                        if (this.gifDecoder.getFrameCount() <= 1) {
                            reDraw();
                            return;
                        } else if (this.drawThread == null) {
                            this.drawThread = new DrawThread(this, null);
                            this.drawThread.start();
                            return;
                        } else {
                            return;
                        }
                    }
                default:
                    return;
            }
        } else {
            Log.e("gif", "parse error");
        }
    }

    private void reDraw() {
        if (this.redrawHandler != null) {
            this.redrawHandler.sendMessage(this.redrawHandler.obtainMessage());
        }
    }

    private class DrawThread extends Thread {
        private DrawThread() {
        }

        /* synthetic */ DrawThread(GifView gifView, DrawThread drawThread) {
            this();
        }

        /* JADX WARNING: CFG modification limit reached, blocks count: 115 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r6 = this;
                com.ant.liao.GifView r4 = com.ant.liao.GifView.this
                com.ant.liao.GifDecoder r4 = r4.gifDecoder
                if (r4 != 0) goto L_0x0043
            L_0x0008:
                return
            L_0x0009:
                com.ant.liao.GifView r4 = com.ant.liao.GifView.this
                boolean r4 = r4.pause
                if (r4 != 0) goto L_0x004c
                com.ant.liao.GifView r4 = com.ant.liao.GifView.this
                com.ant.liao.GifDecoder r4 = r4.gifDecoder
                com.ant.liao.GifFrame r0 = r4.next()
                com.ant.liao.GifView r4 = com.ant.liao.GifView.this
                android.graphics.Bitmap r5 = r0.image
                r4.currentImage = r5
                int r4 = r0.delay
                long r2 = (long) r4
                com.ant.liao.GifView r4 = com.ant.liao.GifView.this
                android.os.Handler r4 = r4.redrawHandler
                if (r4 == 0) goto L_0x0008
                com.ant.liao.GifView r4 = com.ant.liao.GifView.this
                android.os.Handler r4 = r4.redrawHandler
                android.os.Message r1 = r4.obtainMessage()
                com.ant.liao.GifView r4 = com.ant.liao.GifView.this
                android.os.Handler r4 = r4.redrawHandler
                r4.sendMessage(r1)
                android.os.SystemClock.sleep(r2)
            L_0x0043:
                com.ant.liao.GifView r4 = com.ant.liao.GifView.this
                boolean r4 = r4.isRun
                if (r4 != 0) goto L_0x0009
                goto L_0x0008
            L_0x004c:
                r4 = 10
                android.os.SystemClock.sleep(r4)
                goto L_0x0043
            */
            throw new UnsupportedOperationException("Method not decompiled: com.ant.liao.GifView.DrawThread.run():void");
        }
    }
}
