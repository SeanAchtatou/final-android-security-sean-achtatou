package com.google.gson;

import com.google.gson.internal.C$Gson$Types;
import java.lang.reflect.Type;
import java.util.Map;

final class MapTypeAdapter extends BaseMapTypeAdapter {
    MapTypeAdapter() {
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.gson.JsonElement serialize(java.util.Map r11, java.lang.reflect.Type r12, com.google.gson.JsonSerializationContext r13) {
        /*
            r10 = this;
            com.google.gson.JsonObject r4 = new com.google.gson.JsonObject
            r4.<init>()
            r0 = 0
            boolean r8 = r12 instanceof java.lang.reflect.ParameterizedType
            if (r8 == 0) goto L_0x0015
            java.lang.Class r5 = com.google.gson.internal.C$Gson$Types.getRawType(r12)
            java.lang.reflect.Type[] r8 = com.google.gson.internal.C$Gson$Types.getMapKeyAndValueTypes(r12, r5)
            r9 = 1
            r0 = r8[r9]
        L_0x0015:
            java.util.Set r8 = r11.entrySet()
            java.util.Iterator r3 = r8.iterator()
        L_0x001d:
            boolean r8 = r3.hasNext()
            if (r8 == 0) goto L_0x004c
            java.lang.Object r2 = r3.next()
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2
            java.lang.Object r6 = r2.getValue()
            if (r6 != 0) goto L_0x003f
            com.google.gson.JsonNull r7 = com.google.gson.JsonNull.createJsonNull()
        L_0x0033:
            java.lang.Object r8 = r2.getKey()
            java.lang.String r8 = java.lang.String.valueOf(r8)
            r4.add(r8, r7)
            goto L_0x001d
        L_0x003f:
            if (r0 != 0) goto L_0x004a
            java.lang.Class r1 = r6.getClass()
        L_0x0045:
            com.google.gson.JsonElement r7 = serialize(r13, r6, r1)
            goto L_0x0033
        L_0x004a:
            r1 = r0
            goto L_0x0045
        L_0x004c:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.gson.MapTypeAdapter.serialize(java.util.Map, java.lang.reflect.Type, com.google.gson.JsonSerializationContext):com.google.gson.JsonElement");
    }

    public Map deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Map<Object, Object> map = constructMapType(typeOfT, context);
        Type[] keyAndValueTypes = C$Gson$Types.getMapKeyAndValueTypes(typeOfT, C$Gson$Types.getRawType(typeOfT));
        for (Map.Entry<String, JsonElement> entry : json.getAsJsonObject().entrySet()) {
            map.put(context.deserialize(new JsonPrimitive((String) entry.getKey()), keyAndValueTypes[0]), context.deserialize((JsonElement) entry.getValue(), keyAndValueTypes[1]));
        }
        return map;
    }

    public String toString() {
        return MapTypeAdapter.class.getSimpleName();
    }
}
