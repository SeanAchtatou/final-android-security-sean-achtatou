package com.google.gson;

import com.google.gson.ObjectNavigator;
import com.google.gson.internal.C$Gson$Preconditions;
import java.lang.reflect.Type;

abstract class JsonDeserializationVisitor<T> implements ObjectNavigator.Visitor {
    protected boolean constructed = false;
    protected final JsonDeserializationContext context;
    protected final ParameterizedTypeHandlerMap<JsonDeserializer<?>> deserializers;
    protected final FieldNamingStrategy2 fieldNamingPolicy;
    protected final JsonElement json;
    protected final ObjectConstructor objectConstructor;
    protected final ObjectNavigator objectNavigator;
    protected T target;
    protected final Type targetType;

    /* access modifiers changed from: protected */
    public abstract T constructTarget();

    JsonDeserializationVisitor(JsonElement json2, Type targetType2, ObjectNavigator objectNavigator2, FieldNamingStrategy2 fieldNamingPolicy2, ObjectConstructor objectConstructor2, ParameterizedTypeHandlerMap<JsonDeserializer<?>> deserializers2, JsonDeserializationContext context2) {
        this.targetType = targetType2;
        this.objectNavigator = objectNavigator2;
        this.fieldNamingPolicy = fieldNamingPolicy2;
        this.objectConstructor = objectConstructor2;
        this.deserializers = deserializers2;
        this.json = (JsonElement) C$Gson$Preconditions.checkNotNull(json2);
        this.context = context2;
    }

    public T getTarget() {
        if (!this.constructed) {
            this.target = constructTarget();
            this.constructed = true;
        }
        return this.target;
    }

    public void start(ObjectTypePair node) {
    }

    public void end(ObjectTypePair node) {
    }

    public final boolean visitUsingCustomHandler(ObjectTypePair objTypePair) {
        Pair<JsonDeserializer<?>, ObjectTypePair> pair = objTypePair.getMatchingHandler(this.deserializers);
        if (pair == null) {
            return false;
        }
        this.target = invokeCustomDeserializer(this.json, pair);
        this.constructed = true;
        return true;
    }

    /* access modifiers changed from: protected */
    public Object invokeCustomDeserializer(JsonElement element, Pair<JsonDeserializer<?>, ObjectTypePair> pair) {
        if (element == null || element.isJsonNull()) {
            return null;
        }
        return ((JsonDeserializer) pair.first).deserialize(element, ((ObjectTypePair) pair.second).type, this.context);
    }

    /* access modifiers changed from: package-private */
    public final Object visitChildAsObject(Type childType, JsonElement jsonChild) {
        return visitChild(childType, new JsonObjectDeserializationVisitor<>(jsonChild, childType, this.objectNavigator, this.fieldNamingPolicy, this.objectConstructor, this.deserializers, this.context));
    }

    /* access modifiers changed from: package-private */
    public final Object visitChildAsArray(Type childType, JsonArray jsonChild) {
        return visitChild(childType, new JsonArrayDeserializationVisitor<>(jsonChild.getAsJsonArray(), childType, this.objectNavigator, this.fieldNamingPolicy, this.objectConstructor, this.deserializers, this.context));
    }

    private Object visitChild(Type type, JsonDeserializationVisitor<?> childVisitor) {
        this.objectNavigator.accept(new ObjectTypePair(null, type, false), childVisitor);
        return childVisitor.getTarget();
    }
}
