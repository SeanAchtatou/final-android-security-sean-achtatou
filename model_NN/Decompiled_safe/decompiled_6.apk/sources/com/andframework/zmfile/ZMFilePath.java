package com.andframework.zmfile;

import android.os.Environment;
import com.jianq.misc.StringEx;

public class ZMFilePath {
    public static String APPROOT = ".sys";
    private static final String ASSETROOT = "file:///android_asset";
    public static final byte ASSET_ROOT_TYPE = 2;
    public static final byte CARD_ROOT_TYPE = 0;
    public static final String FILE_SEPARATOR = System.getProperty("file.separator");
    private static final String PHONEROOT = Environment.getDataDirectory().getPath();
    public static final byte PHONE_ROOT_TYPE = 1;
    private static final String SDCARDROOT = Environment.getExternalStorageDirectory().getPath();
    private StringBuffer filePath;
    private boolean isUseAppRoot;
    private byte rootType;

    public ZMFilePath(byte irootType, boolean iisUseAppRoot) {
        this.rootType = 0;
        this.isUseAppRoot = true;
        this.filePath = new StringBuffer();
        this.rootType = irootType;
        this.isUseAppRoot = iisUseAppRoot;
        if (this.rootType == 0) {
            if (!ZMFile.isSDCardExist()) {
                this.filePath.append(PHONEROOT);
            } else {
                this.filePath.append(SDCARDROOT);
            }
            this.filePath.append(FILE_SEPARATOR);
        } else if (this.rootType == 1) {
            this.filePath.append(PHONEROOT);
            this.filePath.append(FILE_SEPARATOR);
        } else if (this.rootType == 2) {
            this.filePath.append(ASSETROOT);
            this.filePath.append(FILE_SEPARATOR);
        } else {
            this.filePath.append(SDCARDROOT);
            this.filePath.append(FILE_SEPARATOR);
        }
        if (this.isUseAppRoot) {
            this.filePath.append(APPROOT);
            this.filePath.append(FILE_SEPARATOR);
        }
    }

    public ZMFilePath() {
        this((byte) 0, true);
    }

    public String pushPathNode(String pathNode) {
        if (pathNode != null && !pathNode.trim().equals(StringEx.Empty)) {
            this.filePath.append(pathNode);
            this.filePath.append(FILE_SEPARATOR);
        }
        return this.filePath.toString();
    }

    public boolean deletePathNode(String pathNode) {
        if (pathNode != null && !pathNode.trim().equals(StringEx.Empty)) {
            String temStr = this.filePath.toString();
            if (temStr.contains(String.valueOf(pathNode) + FILE_SEPARATOR)) {
                this.filePath = new StringBuffer(temStr.replace(String.valueOf(pathNode) + FILE_SEPARATOR, StringEx.Empty));
                return true;
            } else if (temStr.contains(pathNode)) {
                this.filePath = new StringBuffer(temStr.replace(pathNode, StringEx.Empty));
                return true;
            }
        }
        return false;
    }

    public String addFileName(String fileName) {
        this.filePath.append(fileName);
        return this.filePath.toString();
    }

    public String pullFilePath() {
        return this.filePath.toString();
    }

    public String toString() {
        return this.filePath.toString();
    }
}
