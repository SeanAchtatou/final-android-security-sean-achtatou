package com.andframework.zmfile;

import android.os.Environment;
import android.os.StatFs;
import java.io.File;
import java.io.FileInputStream;
import java.io.RandomAccessFile;

public final class ZMFile {
    public static final int read(String fileFullPath, byte[] pBuf, int pos, int size, int skip) {
        File file = new File(fileFullPath);
        if (file.exists()) {
            try {
                FileInputStream fis = new FileInputStream(file);
                fis.skip((long) skip);
                int readSize = fis.read(pBuf, pos, size);
                fis.close();
                return readSize;
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return 0;
    }

    public static final byte[] readAll(String fileFullPath) {
        File file = new File(fileFullPath);
        if (file.exists()) {
            try {
                FileInputStream fis = new FileInputStream(file);
                byte[] buff = new byte[fis.available()];
                fis.read(buff);
                fis.close();
                return buff;
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return null;
    }

    public static final boolean write(String fileFullPath, byte[] pBuf, int pos, int size, int skip) {
        File file = new File(fileFullPath);
        RandomAccessFile randomAccessFile = null;
        try {
            if (file.exists()) {
                randomAccessFile = new RandomAccessFile(file, "rw");
            } else {
                File p = file.getParentFile();
                boolean isOk = false;
                if (p != null && !p.exists()) {
                    isOk = p.mkdirs();
                } else if (p != null && p.exists()) {
                    isOk = true;
                }
                if (isOk && file.createNewFile()) {
                    randomAccessFile = new RandomAccessFile(file, "rw");
                }
            }
            if (randomAccessFile != null) {
                randomAccessFile.seek((long) skip);
                randomAccessFile.write(pBuf, pos, size);
                randomAccessFile.close();
                return true;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public static final int getSize(String fileFullPath) {
        try {
            File file = new File(fileFullPath);
            if (file.exists()) {
                return (int) file.length();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    public static final boolean fileExist(String fileFullPath) {
        if (new File(fileFullPath).exists()) {
            return true;
        }
        return false;
    }

    public static final boolean delFile(String filePath) {
        if (filePath == null) {
            return false;
        }
        return new File(filePath).delete();
    }

    public static final boolean delete(File dir) {
        if (dir.isDirectory()) {
            File[] listFiles = dir.listFiles();
            for (File delete : listFiles) {
                delete(delete);
            }
        }
        return dir.delete();
    }

    public static final void deleteAll(String filePath) {
        File dir = new File(filePath);
        if (dir.isDirectory()) {
            File[] listFiles = dir.listFiles();
            for (File delete : listFiles) {
                delete(delete);
            }
            return;
        }
        dir.delete();
    }

    public static final boolean createDirectory(String fileFullPath) {
        File file = new File(fileFullPath);
        boolean isOk = file.exists();
        if (!isOk) {
            return file.mkdirs();
        }
        return isOk;
    }

    public static final File[] getDirectory(String filePath) {
        File dir = new File(filePath);
        if (dir.isDirectory()) {
            return dir.listFiles();
        }
        return null;
    }

    public static final long phone_storage_free() {
        StatFs stat = new StatFs(Environment.getDataDirectory().getPath());
        return (long) (stat.getAvailableBlocks() * stat.getBlockSize());
    }

    public static final long phone_storage_used() {
        StatFs stat = new StatFs(Environment.getDataDirectory().getPath());
        return (long) ((stat.getBlockCount() - stat.getAvailableBlocks()) * stat.getBlockSize());
    }

    public static final long phone_storage_total() {
        StatFs stat = new StatFs(Environment.getDataDirectory().getPath());
        return (long) (stat.getBlockCount() * stat.getBlockSize());
    }

    public static final long sd_card_free() {
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        return (long) (stat.getAvailableBlocks() * stat.getBlockSize());
    }

    public static final long sd_card_used() {
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        return (long) ((stat.getBlockCount() - stat.getAvailableBlocks()) * stat.getBlockSize());
    }

    public static final long sd_card_total() {
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        return (long) (stat.getBlockCount() * stat.getBlockSize());
    }

    public static final boolean isSDCardExist() {
        if (Environment.getExternalStorageState().equals("mounted")) {
            return true;
        }
        return false;
    }
}
