package com.andframework.ui;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import com.andframework.business.LoadImageTsk;
import com.andframework.business.PicDownTaskManager;
import com.andframework.util.Util;
import com.andframework.zmfile.ZMFile;
import com.andframework.zmfile.ZMFilePath;
import com.jianq.misc.StringEx;

public class CustomImageView extends ImageView implements View.OnTouchListener {
    public static final byte SCALE_BIG = 1;
    public static final byte SCALE_SMALL = 0;
    private Bitmap bitmap;
    private float downX;
    private float downY;
    private boolean isFlipMove;
    private boolean isScale;
    private int maxBottom = this.maxY;
    private int maxLeft = (this.maxX * -1);
    private int maxRight = this.maxX;
    private int maxTop = (this.maxY * -1);
    private int maxX = 0;
    private int maxY = 0;
    private OnScaleFinish onScaleFinish;
    private float scale = 1.25f;
    private int screenHeight = 0;
    private int screenWidth = 0;
    private int scrollByX;
    private int scrollByY;
    private int totalX;
    private int totalY;
    float varscale = 1.0f;

    public interface OnScaleFinish {
        void OnFinish(byte b);
    }

    public CustomImageView(Context context) {
        super(context);
        Display display = ((Activity) context).getWindowManager().getDefaultDisplay();
        this.screenWidth = display.getWidth();
        this.screenHeight = display.getHeight();
    }

    public CustomImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Display display = ((Activity) context).getWindowManager().getDefaultDisplay();
        this.screenWidth = display.getWidth();
        this.screenHeight = display.getHeight();
    }

    public CustomImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        Display display = ((Activity) context).getWindowManager().getDefaultDisplay();
        this.screenWidth = display.getWidth();
        this.screenHeight = display.getHeight();
    }

    public void setDefaultImage(int resid) {
        setImageResource(resid);
    }

    public void setDefaultImage(Bitmap bitmap2) {
        setImageBitmap(bitmap2);
    }

    public void setScale(boolean isScale2) {
        this.isScale = isScale2;
    }

    public void setFlipMove(boolean isFlipMove2) {
        this.isFlipMove = isFlipMove2;
        if (isFlipMove2) {
            setOnTouchListener(this);
        } else {
            setOnTouchListener(null);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public void setImageBitmap(Bitmap bp) {
        if (this.isScale) {
            this.bitmap = bp;
            if (this.bitmap == null || this.screenWidth >= this.bitmap.getWidth()) {
                super.setImageBitmap(this.bitmap);
            } else {
                float sc = ((float) this.screenWidth) / ((float) this.bitmap.getWidth());
                this.varscale = sc;
                Matrix matrix = new Matrix();
                matrix.postScale(sc, sc);
                super.setImageBitmap(Bitmap.createBitmap(this.bitmap, 0, 0, this.bitmap.getWidth(), this.bitmap.getHeight(), matrix, true));
            }
            if (this.bitmap != null && this.bitmap.getWidth() < this.screenWidth && this.onScaleFinish != null) {
                this.onScaleFinish.OnFinish((byte) 0);
                return;
            }
            return;
        }
        super.setImageBitmap(bp);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public void scale(byte type) {
        if (this.isScale && this.bitmap != null) {
            if (type == 1) {
                int bmpWidth = this.bitmap.getWidth();
                int bmpHeight = this.bitmap.getHeight();
                int drawWidth = ((BitmapDrawable) getDrawable()).getBitmap().getWidth();
                if (bmpWidth < this.screenWidth || drawWidth < bmpWidth) {
                    if (bmpWidth >= this.screenWidth || drawWidth < bmpWidth * 2) {
                        this.varscale = this.varscale * this.scale;
                        Matrix matrix = new Matrix();
                        matrix.postScale(this.varscale, this.varscale);
                        Bitmap bmp = Bitmap.createBitmap(this.bitmap, 0, 0, bmpWidth, bmpHeight, matrix, true);
                        super.setImageBitmap(bmp);
                        if (bmpWidth < this.screenWidth && bmp.getWidth() >= this.screenWidth * 2 && this.onScaleFinish != null) {
                            this.onScaleFinish.OnFinish((byte) 1);
                        }
                    } else if (this.onScaleFinish != null) {
                        this.onScaleFinish.OnFinish((byte) 1);
                    }
                } else if (this.onScaleFinish != null) {
                    this.onScaleFinish.OnFinish((byte) 1);
                }
            } else if (type == 0) {
                int bmpWidth2 = this.bitmap.getWidth();
                int bmpHeight2 = this.bitmap.getHeight();
                int drawWidth2 = ((BitmapDrawable) getDrawable()).getBitmap().getWidth();
                if (bmpWidth2 >= this.screenWidth || ((double) drawWidth2) > ((double) bmpWidth2) * 0.75d) {
                    if (bmpWidth2 < this.screenWidth || ((double) drawWidth2) > ((double) this.screenWidth) * 0.75d) {
                        this.varscale = this.varscale / this.scale;
                        Matrix matrix2 = new Matrix();
                        matrix2.postScale(this.varscale, this.varscale);
                        Bitmap bmp2 = Bitmap.createBitmap(this.bitmap, 0, 0, bmpWidth2, bmpHeight2, matrix2, true);
                        super.setImageBitmap(bmp2);
                        if (((double) bmp2.getWidth()) <= ((double) bmpWidth2) * 0.75d && this.onScaleFinish != null) {
                            this.onScaleFinish.OnFinish((byte) 0);
                        }
                    } else if (this.onScaleFinish != null) {
                        this.onScaleFinish.OnFinish((byte) 0);
                    }
                } else if (this.onScaleFinish != null) {
                    this.onScaleFinish.OnFinish((byte) 0);
                }
            }
        }
    }

    public void setImageFromeUrl(String url) {
        byte[] tmpPic;
        if (url == null || !url.startsWith("http") || url.trim().equals(StringEx.Empty)) {
            Log.i("setImageFromeUrl >", "imageurl null");
            return;
        }
        String picname = Util.urlToFileName(url);
        ZMFilePath fp = new ZMFilePath((byte) 0, true);
        fp.pushPathNode("tmppic");
        fp.addFileName(picname);
        if (!ZMFile.fileExist(fp.toString()) || (tmpPic = ZMFile.readAll(fp.toString())) == null) {
            LoadImageTsk lit = new LoadImageTsk();
            lit.picUrl = url;
            lit.setImageView(this);
            PicDownTaskManager.getInst().postTask(lit);
            return;
        }
        Log.i("file exit>", String.valueOf(tmpPic.length) + "'");
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inScaled = false;
        Bitmap bp = BitmapFactory.decodeByteArray(tmpPic, 0, tmpPic.length, opts);
        if (bp != null) {
            Log.i("CustomImageView>setImageFromeUrl>local w-h", String.valueOf(bp.getWidth()) + "-" + bp.getHeight());
        }
        setImageBitmap(bp);
    }

    public boolean onTouch(View view, MotionEvent event) {
        switch (event.getAction()) {
            case 0:
                this.downX = event.getX();
                this.downY = event.getY();
                return true;
            case 1:
            default:
                return true;
            case 2:
                float currentX = event.getX();
                float currentY = event.getY();
                this.scrollByX = (int) (this.downX - currentX);
                this.scrollByY = (int) (this.downY - currentY);
                if (currentX > this.downX) {
                    if (this.totalX == this.maxLeft) {
                        this.scrollByX = 0;
                    }
                    if (this.totalX > this.maxLeft) {
                        this.totalX += this.scrollByX;
                    }
                    if (this.totalX < this.maxLeft) {
                        this.scrollByX = this.maxLeft - (this.totalX - this.scrollByX);
                        this.totalX = this.maxLeft;
                    }
                }
                if (currentX < this.downX) {
                    if (this.totalX == this.maxRight) {
                        this.scrollByX = 0;
                    }
                    if (this.totalX < this.maxRight) {
                        this.totalX += this.scrollByX;
                    }
                    if (this.totalX > this.maxRight) {
                        this.scrollByX = this.maxRight - (this.totalX - this.scrollByX);
                        this.totalX = this.maxRight;
                    }
                }
                if (currentY > this.downY) {
                    if (this.totalY == this.maxTop) {
                        this.scrollByY = 0;
                    }
                    if (this.totalY > this.maxTop) {
                        this.totalY += this.scrollByY;
                    }
                    if (this.totalY < this.maxTop) {
                        this.scrollByY = this.maxTop - (this.totalY - this.scrollByY);
                        this.totalY = this.maxTop;
                    }
                }
                if (currentY < this.downY) {
                    if (this.totalY == this.maxBottom) {
                        this.scrollByY = 0;
                    }
                    if (this.totalY < this.maxBottom) {
                        this.totalY += this.scrollByY;
                    }
                    if (this.totalY > this.maxBottom) {
                        this.scrollByY = this.maxBottom - (this.totalY - this.scrollByY);
                        this.totalY = this.maxBottom;
                    }
                }
                scrollBy(this.scrollByX, this.scrollByY);
                this.downX = currentX;
                this.downY = currentY;
                return true;
        }
    }

    public void onDraw(Canvas canvas) {
        Bitmap mBitmap;
        super.onDraw(canvas);
        BitmapDrawable mDrawable = (BitmapDrawable) getDrawable();
        if (mDrawable != null && (mBitmap = mDrawable.getBitmap()) != null && this.isFlipMove) {
            this.maxX = mBitmap.getWidth();
            this.maxY = mBitmap.getHeight();
            this.maxLeft = this.maxX * -1;
            this.maxRight = this.maxX;
            this.maxTop = this.maxY * -1;
            this.maxBottom = this.maxY;
        }
    }

    public void setOnScaleFinish(OnScaleFinish onScaleFinish2) {
        this.onScaleFinish = onScaleFinish2;
    }
}
