package com.andframework.ui;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import com.baosteelOnline.R;
import java.util.ArrayList;

public class ScrollView extends LinearLayout {
    /* access modifiers changed from: private */
    public View.OnClickListener cl;
    private Activity context;
    /* access modifiers changed from: private */
    public Button currentClick;
    private LinearLayout scrollPanel;

    public ScrollView(Context context2) {
        super(context2);
        this.context = (Activity) context2;
        initGUI();
    }

    public ScrollView(Context context2, AttributeSet attrs) {
        super(context2, attrs);
        this.context = (Activity) context2;
        initGUI();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.andframework.ui.ScrollView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void initGUI() {
        View view = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate((int) R.layout.activity_cyclic_goods_bid_hall_page, (ViewGroup) this, true);
        this.scrollPanel = (LinearLayout) view.findViewById(R.color.mistyrose);
        ((HorizontalScrollView) view.findViewById(R.color.blanchedalmond)).setHorizontalScrollBarEnabled(false);
    }

    public void setItemClickListener(View.OnClickListener listener) {
        this.cl = listener;
    }

    public void buildUIFromTexts(ArrayList<TextAndTag> textAndTag) {
        if (textAndTag != null && textAndTag.size() != 0) {
            TextAndTag[] textAndTags = new TextAndTag[textAndTag.size()];
            for (int i = 0; i < textAndTag.size(); i++) {
                textAndTags[i] = textAndTag.get(i);
            }
            buildUIFromTexts(textAndTags);
        }
    }

    public void buildUIFromTexts(TextAndTag[] textAndTag) {
        if (textAndTag != null && textAndTag.length != 0) {
            for (int i = 0; i < textAndTag.length; i++) {
                Button tmpButton = new Button(this.context);
                tmpButton.setText(textAndTag[i].text);
                tmpButton.setTag(textAndTag[i].obj);
                tmpButton.setBackgroundColor(0);
                tmpButton.setTextSize(1, 16.0f);
                tmpButton.setTextColor(-1);
                tmpButton.setPadding(tmpButton.getPaddingLeft(), 0, tmpButton.getPaddingRight(), 0);
                tmpButton.setGravity(17);
                if (i == 0) {
                    this.currentClick = tmpButton;
                    this.currentClick.setBackgroundResource(R.drawable.bg1);
                    this.currentClick.setTextColor(Color.parseColor("#0099cc"));
                }
                this.scrollPanel.addView(tmpButton);
                tmpButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        ScrollView.this.currentClick.setTextColor(-1);
                        ScrollView.this.currentClick.setBackgroundColor(0);
                        ScrollView.this.currentClick = (Button) v;
                        ScrollView.this.currentClick.setBackgroundResource(R.drawable.bg1);
                        ScrollView.this.currentClick.setTextColor(Color.parseColor("#0099cc"));
                        if (ScrollView.this.cl != null) {
                            ScrollView.this.cl.onClick(v);
                        }
                    }
                });
            }
        }
    }

    public class TextAndTag {
        public Object obj;
        public String text;

        public TextAndTag() {
        }
    }
}
