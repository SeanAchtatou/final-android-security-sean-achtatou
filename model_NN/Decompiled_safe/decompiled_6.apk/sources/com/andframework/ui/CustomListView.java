package com.andframework.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.andframework.util.Util;
import java.util.Date;
import java.util.LinkedList;

public class CustomListView extends ListView implements AbsListView.OnScrollListener {
    private static final int DONE = 3;
    private static final int GETMOREING = 5;
    private static final int LOADING = 4;
    private static final int PULL_To_REFRESH = 1;
    private static final int RATIO = 3;
    private static final int REFRESHING = 2;
    private static final int RELEASE_To_REFRESH = 0;
    private static final String TAG = "listview";
    private RotateAnimation animation;
    private ImageView arrowImageView;
    private Bitmap bmap;
    private ProgressBar bottomProgressBar;
    private RelativeLayout bottomView;
    private int firstItemIndex;
    private OnGetMoreListener getMoreListener;
    private int headContentHeight;
    private int headContentWidth;
    private LinearLayout headView;
    private boolean isBack;
    private boolean isRecored;
    private boolean isRefreshable;
    private TextView lastUpdatedTextView;
    private LinkedList<View> mListViews;
    private TextView moreTextview;
    private int pageSize = 20;
    private ProgressBar progressBar;
    private OnRefreshListener refreshListener;
    private RotateAnimation reverseAnimation;
    private int startY;
    /* access modifiers changed from: private */
    public int state;
    private TableListAdapter tableListAdapter;
    private TextView tipsTextview;

    public interface OnGetMoreListener {
        void onGetmore();
    }

    public interface OnRefreshListener {
        void onRefresh();
    }

    public CustomListView(Context context) {
        super(context);
        init(context);
    }

    public CustomListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public void setPageSize(int pageSize2) {
        this.pageSize = pageSize2;
    }

    public LinkedList<View> getRowViewList() {
        return this.mListViews;
    }

    private void initBottom(Context context) {
        this.bottomView = new RelativeLayout(context);
        this.bottomView.setLayoutParams(new AbsListView.LayoutParams(-1, Util.dip2px(context, 40.0f)));
        RelativeLayout.LayoutParams rlay = new RelativeLayout.LayoutParams(-2, -2);
        this.moreTextview = new TextView(context);
        this.moreTextview.setId(12321);
        this.moreTextview.setText("更多");
        this.moreTextview.setTextSize(1, 20.0f);
        this.moreTextview.setGravity(17);
        rlay.addRule(13);
        this.bottomView.addView(this.moreTextview, rlay);
        this.bottomView.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == 1 && CustomListView.this.state == 3) {
                    CustomListView.this.state = 5;
                    CustomListView.this.changeBottomViewByState();
                    CustomListView.this.onGetMoreData();
                }
                return true;
            }
        });
        RelativeLayout.LayoutParams rlay2 = new RelativeLayout.LayoutParams(-2, -2);
        this.bottomProgressBar = new ProgressBar(context, null, 16842873);
        this.bottomProgressBar.setIndeterminate(true);
        this.bottomProgressBar.setVisibility(8);
        rlay2.addRule(0, 12321);
        rlay2.addRule(15);
        this.bottomView.addView(this.bottomProgressBar, rlay2);
    }

    private void initHeader(Context context) {
        this.headView = new LinearLayout(context);
        this.headView.setLayoutParams(new AbsListView.LayoutParams(-1, -2));
        RelativeLayout rl = new RelativeLayout(context);
        RelativeLayout.LayoutParams rlay = new RelativeLayout.LayoutParams(-1, -2);
        rl.setPadding(Util.dip2px(context, 30.0f), 0, 0, 0);
        this.headView.addView(rl, rlay);
        FrameLayout flt = new FrameLayout(context);
        RelativeLayout.LayoutParams rlay2 = new RelativeLayout.LayoutParams(-2, -2);
        rlay2.addRule(9);
        rlay2.addRule(15);
        rl.addView(flt, rlay2);
        this.arrowImageView = new ImageView(context);
        this.bmap = BitmapFactory.decodeStream(getClass().getResourceAsStream("arrow.png"));
        this.arrowImageView.setImageBitmap(this.bmap);
        this.arrowImageView.setMinimumWidth(70);
        this.arrowImageView.setMinimumHeight(50);
        flt.addView(this.arrowImageView, new FrameLayout.LayoutParams(-2, -2));
        FrameLayout.LayoutParams fllp = new FrameLayout.LayoutParams(-2, -2);
        this.progressBar = new ProgressBar(context, null, 16842873);
        this.progressBar.setIndeterminate(true);
        this.progressBar.setVisibility(8);
        flt.addView(this.progressBar, fllp);
        LinearLayout textLayout = new LinearLayout(context);
        textLayout.setOrientation(1);
        textLayout.setGravity(1);
        RelativeLayout.LayoutParams rlay3 = new RelativeLayout.LayoutParams(-2, -2);
        rlay3.addRule(14);
        rl.addView(textLayout, rlay3);
        this.tipsTextview = new TextView(context);
        LinearLayout.LayoutParams tipsly = new LinearLayout.LayoutParams(-2, -2);
        this.tipsTextview.setTextColor(-1);
        this.tipsTextview.setText("下拉刷新");
        this.tipsTextview.setTextSize(1, 20.0f);
        textLayout.addView(this.tipsTextview, tipsly);
        this.lastUpdatedTextView = new TextView(context);
        LinearLayout.LayoutParams tipsly2 = new LinearLayout.LayoutParams(-2, -2);
        this.lastUpdatedTextView.setTextColor(Color.parseColor("#b89766"));
        this.lastUpdatedTextView.setText("上次更新");
        this.lastUpdatedTextView.setTextSize(1, 10.0f);
        textLayout.addView(this.lastUpdatedTextView, tipsly2);
        measureView(this.headView);
        this.headContentHeight = this.headView.getMeasuredHeight();
        this.headContentWidth = this.headView.getMeasuredWidth();
        this.headView.setPadding(0, this.headContentHeight * -1, 0, 0);
        this.headView.invalidate();
        Log.v("size", "width:" + this.headContentWidth + " height:" + this.headContentHeight);
        addHeaderView(this.headView, null, false);
    }

    private void init(Context context) {
        setCacheColorHint(Color.parseColor("#00000000"));
        initHeader(context);
        initBottom(context);
        setOnScrollListener(this);
        this.animation = new RotateAnimation(0.0f, -180.0f, 1, 0.5f, 1, 0.5f);
        this.animation.setInterpolator(new LinearInterpolator());
        this.animation.setDuration(250);
        this.animation.setFillAfter(true);
        this.reverseAnimation = new RotateAnimation(-180.0f, 0.0f, 1, 0.5f, 1, 0.5f);
        this.reverseAnimation.setInterpolator(new LinearInterpolator());
        this.reverseAnimation.setDuration(200);
        this.reverseAnimation.setFillAfter(true);
        this.state = 3;
        this.isRefreshable = false;
        this.mListViews = new LinkedList<>();
        this.tableListAdapter = new TableListAdapter(context, this.mListViews);
        setAdapter((ListAdapter) this.tableListAdapter);
        this.lastUpdatedTextView.setText("最近更新:" + new Date().toLocaleString());
    }

    public void setBottomViewVisible(boolean visible) {
        if (visible && getFooterViewsCount() <= 0) {
            addFooterView(this.bottomView, null, false);
        } else if (!visible && getFooterViewsCount() > 0) {
            removeFooterView(this.bottomView);
        }
    }

    public void setArrowImage(int resid) {
        this.arrowImageView.setImageResource(resid);
    }

    public void setHeadBackgrond(int resid) {
        this.headView.setBackgroundResource(resid);
    }

    public void setBottomBackgrond(int resid) {
        this.bottomView.setBackgroundResource(resid);
    }

    public void setHeadBackgrondColor(int Color) {
        this.headView.setBackgroundColor(Color);
    }

    public void setBottomBackgrondColor(int Color) {
        this.bottomView.setBackgroundColor(Color);
    }

    public void setMoreTextColor(int color) {
        this.moreTextview.setTextColor(color);
    }

    public void onScroll(AbsListView arg0, int firstVisiableItem, int arg2, int arg3) {
        this.firstItemIndex = firstVisiableItem;
    }

    public void setUpdateTextColor(int color) {
        this.tipsTextview.setTextColor(color);
    }

    public void setTimeTextColor(int color) {
        this.lastUpdatedTextView.setTextColor(color);
    }

    public void onScrollStateChanged(AbsListView arg0, int arg1) {
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (this.isRefreshable) {
            switch (event.getAction()) {
                case 0:
                    if (this.firstItemIndex == 0 && !this.isRecored) {
                        this.isRecored = true;
                        this.startY = (int) event.getY();
                        Log.v(TAG, "在down时候记录当前位置‘");
                        break;
                    }
                case 1:
                    if (!(this.state == 2 || this.state == 4)) {
                        if (this.state == 1) {
                            this.state = 3;
                            changeHeaderViewByState();
                            Log.v(TAG, "由下拉刷新状态，到done状态");
                        }
                        if (this.state == 0) {
                            this.state = 2;
                            changeHeaderViewByState();
                            onRefresh();
                            Log.v(TAG, "由松开刷新状态，到done状态");
                        }
                    }
                    this.isRecored = false;
                    this.isBack = false;
                    break;
                case 2:
                    int tempY = (int) event.getY();
                    if (!this.isRecored && this.firstItemIndex == 0) {
                        Log.v(TAG, "在move时候记录下位置");
                        this.isRecored = true;
                        this.startY = tempY;
                    }
                    if (!(this.state == 2 || !this.isRecored || this.state == 4)) {
                        if (this.state == 0) {
                            setSelection(0);
                            if ((tempY - this.startY) / 3 < this.headContentHeight && tempY - this.startY > 0) {
                                this.state = 1;
                                changeHeaderViewByState();
                                Log.v(TAG, "由松开刷新状态转变到下拉刷新状态");
                            } else if (tempY - this.startY <= 0) {
                                this.state = 3;
                                changeHeaderViewByState();
                                Log.v(TAG, "由松开刷新状态转变到done状态");
                            }
                        }
                        if (this.state == 1) {
                            setSelection(0);
                            if ((tempY - this.startY) / 3 >= this.headContentHeight) {
                                this.state = 0;
                                this.isBack = true;
                                changeHeaderViewByState();
                                Log.v(TAG, "由done或者下拉刷新状态转变到松开刷新");
                            } else if (tempY - this.startY <= 0) {
                                this.state = 3;
                                changeHeaderViewByState();
                                Log.v(TAG, "由DOne或者下拉刷新状态转变到done状态");
                            }
                        }
                        if (this.state == 3 && tempY - this.startY > 0) {
                            this.state = 1;
                            changeHeaderViewByState();
                        }
                        if (this.state == 1) {
                            this.headView.setPadding(0, (this.headContentHeight * -1) + ((tempY - this.startY) / 3), 0, 0);
                        }
                        if (this.state == 0) {
                            this.headView.setPadding(0, ((tempY - this.startY) / 3) - this.headContentHeight, 0, 0);
                            break;
                        }
                    }
                    break;
            }
        }
        return super.onTouchEvent(event);
    }

    /* access modifiers changed from: private */
    public void changeBottomViewByState() {
        switch (this.state) {
            case 3:
                this.bottomProgressBar.setVisibility(8);
                this.moreTextview.setText("更多");
                Log.v(TAG, "当前状态，更多 done");
                return;
            case 4:
            default:
                return;
            case 5:
                this.bottomProgressBar.setVisibility(0);
                this.moreTextview.setText("正在获取...");
                Log.v(TAG, "当前状态,更多 正在获取...");
                return;
        }
    }

    private void changeHeaderViewByState() {
        switch (this.state) {
            case 0:
                this.arrowImageView.setVisibility(0);
                this.progressBar.setVisibility(8);
                this.tipsTextview.setVisibility(0);
                this.lastUpdatedTextView.setVisibility(0);
                this.arrowImageView.clearAnimation();
                this.arrowImageView.startAnimation(this.animation);
                this.tipsTextview.setText("松开刷新");
                Log.v(TAG, "当前状态，松开刷新");
                return;
            case 1:
                this.progressBar.setVisibility(8);
                this.tipsTextview.setVisibility(0);
                this.lastUpdatedTextView.setVisibility(0);
                this.arrowImageView.clearAnimation();
                this.arrowImageView.setVisibility(0);
                if (this.isBack) {
                    this.isBack = false;
                    this.arrowImageView.clearAnimation();
                    this.arrowImageView.startAnimation(this.reverseAnimation);
                    this.tipsTextview.setText("下拉刷新");
                } else {
                    this.tipsTextview.setText("下拉刷新");
                }
                Log.v(TAG, "当前状态，下拉刷新");
                return;
            case 2:
                this.headView.setPadding(0, 0, 0, 0);
                this.progressBar.setVisibility(0);
                this.arrowImageView.clearAnimation();
                this.arrowImageView.setVisibility(8);
                this.tipsTextview.setText("正在刷新...");
                this.lastUpdatedTextView.setVisibility(0);
                Log.v(TAG, "当前状态,正在刷新...");
                return;
            case 3:
                this.headView.setPadding(0, this.headContentHeight * -1, 0, 0);
                this.progressBar.setVisibility(8);
                this.arrowImageView.clearAnimation();
                this.arrowImageView.setImageBitmap(this.bmap);
                this.tipsTextview.setText("下拉刷新");
                this.lastUpdatedTextView.setVisibility(0);
                Log.v(TAG, "当前状态，done");
                return;
            default:
                return;
        }
    }

    public void setonRefreshListener(OnRefreshListener refreshListener2) {
        this.refreshListener = refreshListener2;
        this.isRefreshable = true;
    }

    public void setonGetMoreListener(OnGetMoreListener getMoreListener2) {
        this.getMoreListener = getMoreListener2;
    }

    public void addAllView(LinkedList<View> all) {
        this.mListViews.addAll(all);
        this.tableListAdapter.notifyDataSetChanged();
    }

    public void addViewToFirst(View v) {
        this.mListViews.addFirst(v);
        this.tableListAdapter.notifyDataSetChanged();
    }

    public void addViewToLast(View v) {
        this.mListViews.addLast(v);
        this.tableListAdapter.notifyDataSetChanged();
    }

    public void addViewToIndex(int index, View v) {
        this.mListViews.add(index, v);
        this.tableListAdapter.notifyDataSetChanged();
    }

    public void removeAllView() {
        this.mListViews.clear();
        setBottomViewVisible(false);
        this.tableListAdapter.notifyDataSetChanged();
    }

    public void removeView(int index) {
        if (this.mListViews.size() > 0) {
            this.mListViews.remove(index);
            this.tableListAdapter.notifyDataSetChanged();
            if (this.mListViews.size() == 0) {
                setBottomViewVisible(false);
            }
        }
    }

    public void removeView(View view) {
        if (this.mListViews.size() > 0) {
            this.mListViews.remove(view);
            this.tableListAdapter.notifyDataSetChanged();
            if (this.mListViews.size() == 0) {
                setBottomViewVisible(false);
            }
        }
    }

    public void retsetStatus() {
        this.state = 3;
        changeBottomViewByState();
        this.tableListAdapter.notifyDataSetChanged();
        setBottomViewVisible(false);
    }

    public void onRefreshComplete() {
        this.state = 3;
        this.lastUpdatedTextView.setText("最近更新:" + new Date().toLocaleString());
        changeHeaderViewByState();
        if (this.headView.getBottom() > 0) {
            invalidateViews();
            setSelection(1);
        }
        this.tableListAdapter.notifyDataSetChanged();
        if (this.mListViews != null && this.mListViews.size() > 0) {
            if (this.mListViews.size() % this.pageSize == 0) {
                setBottomViewVisible(true);
            } else {
                setBottomViewVisible(false);
            }
        }
    }

    public void onGetMoreComplete() {
        invalidateViews();
        this.state = 3;
        changeBottomViewByState();
        if (this.mListViews != null && this.mListViews.size() > 0) {
            setScrollToRow(this.mListViews.size() - this.pageSize);
        }
        this.tableListAdapter.notifyDataSetChanged();
        if (this.mListViews != null && this.mListViews.size() > 0) {
            if (this.mListViews.size() % this.pageSize == 0) {
                setBottomViewVisible(true);
            } else {
                setBottomViewVisible(false);
            }
        }
    }

    public void setScrollToRow(int row) {
        setSelection(row);
    }

    public void freshData() {
        this.state = 2;
        changeHeaderViewByState();
        onRefresh();
    }

    private void onRefresh() {
        if (this.refreshListener != null) {
            this.refreshListener.onRefresh();
        }
    }

    /* access modifiers changed from: private */
    public void onGetMoreData() {
        if (this.getMoreListener != null) {
            this.getMoreListener.onGetmore();
        }
    }

    private void measureView(View child) {
        int childHeightSpec;
        ViewGroup.LayoutParams p = child.getLayoutParams();
        if (p == null) {
            p = new ViewGroup.LayoutParams(-1, -2);
        }
        int childWidthSpec = ViewGroup.getChildMeasureSpec(0, 0, p.width);
        int lpHeight = p.height;
        if (lpHeight > 0) {
            childHeightSpec = View.MeasureSpec.makeMeasureSpec(lpHeight, 1073741824);
        } else {
            childHeightSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        }
        child.measure(childWidthSpec, childHeightSpec);
    }

    public int getListNum() {
        if (this.mListViews != null) {
            return this.mListViews.size();
        }
        return 0;
    }

    private class TableListAdapter extends BaseAdapter {
        private Context mContext;
        private LinkedList<View> views;

        public TableListAdapter(Context context, LinkedList<View> v) {
            this.mContext = context;
            this.views = v;
        }

        public int getCount() {
            return this.views.size();
        }

        public Object getItem(int position) {
            return this.views.get(position).getTag();
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            return this.views.get(position);
        }
    }
}
