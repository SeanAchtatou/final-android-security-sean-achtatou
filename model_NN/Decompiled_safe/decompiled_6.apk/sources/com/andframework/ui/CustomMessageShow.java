package com.andframework.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Process;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.andframework.config.LocalData;
import com.andframework.parse.VersionParse;
import com.andframework.util.Util;

public class CustomMessageShow {
    private static CustomMessageShow inst;
    private ProgressDialog progressDialog;

    private CustomMessageShow() {
    }

    public static CustomMessageShow getInst() {
        if (inst == null) {
            inst = new CustomMessageShow();
        }
        return inst;
    }

    public void showProgressDialog(Context context) {
        if (this.progressDialog != null) {
            this.progressDialog.dismiss();
        }
        this.progressDialog = new ProgressDialog(context);
        this.progressDialog.setMessage("正在加载数据！");
        this.progressDialog.setIndeterminate(true);
        this.progressDialog.setCancelable(true);
        this.progressDialog.show();
    }

    public void cancleProgressDialog() {
        if (this.progressDialog != null) {
            this.progressDialog.dismiss();
        }
    }

    public void showProgressDialog(Context context, String message) {
        if (this.progressDialog != null) {
            this.progressDialog.dismiss();
        }
        this.progressDialog = new ProgressDialog(context);
        this.progressDialog.setMessage(message);
        this.progressDialog.setIndeterminate(true);
        this.progressDialog.setCancelable(true);
        this.progressDialog.show();
    }

    public void showShortToast(Context context, String message) {
        Toast.makeText(context, message, 0).show();
    }

    public void showLongToast(Context context, String message) {
        Toast.makeText(context, message, 1).show();
    }

    public AlertDialog showConfirmDialog(Context context, String message, String title, String left, String middle, String right, int iconid, DialogInterface.OnClickListener l) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (iconid > 0) {
            builder.setIcon(iconid);
        }
        builder.setTitle("提示信息");
        if (title != null) {
            builder.setTitle(title);
        }
        String leftStr = "确认";
        if (left != null) {
            leftStr = left;
        }
        String rightStr = "取消";
        if (right != null) {
            rightStr = right;
        }
        builder.setMessage(message);
        builder.setPositiveButton(leftStr, l);
        if (middle != null && middle.length() > 0) {
            builder.setNeutralButton(middle, l);
        }
        builder.setNegativeButton(rightStr, l);
        AlertDialog dialog = builder.create();
        dialog.show();
        return dialog;
    }

    public void showConfirmDialog(Context context, String message, DialogInterface.OnClickListener l) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("提示信息");
        builder.setMessage(message);
        builder.setPositiveButton("确认", l);
        builder.setNeutralButton("取消", l);
        builder.create().show();
    }

    public void showExitDialog(Context context, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("提示信息");
        builder.setMessage(message);
        builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface d, int i) {
                Process.killProcess(Process.myPid());
            }
        });
        builder.setNeutralButton("取消", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface d, int i) {
            }
        });
        builder.create().show();
    }

    public void showUpdateDialog(Context context, VersionParse versionParse) {
        if (versionParse.versionInfo.updateflag.equals("2")) {
            showShortToast(context, "本版本是最新版本！");
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("发现新版本，是否要升级？");
        View view = buildUpdatePanel(context, versionParse);
        builder.setView(view);
        AlertDialog dialog = builder.create();
        if (versionParse.versionInfo.updateflag.equals("1")) {
            dialog.setCancelable(false);
        }
        view.setTag(dialog);
        dialog.show();
    }

    private View buildUpdatePanel(final Context context, final VersionParse versionParse) {
        final LinearLayout view = new LinearLayout(context);
        view.setOrientation(1);
        TextView nameAndSize = new TextView(context);
        nameAndSize.setText("名称：" + versionParse.versionInfo.appname + " 文件大小：" + versionParse.versionInfo.packsize);
        LinearLayout.LayoutParams lls = new LinearLayout.LayoutParams(-2, -2);
        lls.leftMargin = Util.dip2px(context, 10.0f);
        lls.rightMargin = Util.dip2px(context, 10.0f);
        view.addView(nameAndSize, lls);
        TextView updatetv = new TextView(context);
        updatetv.setText("本次升级：");
        view.addView(updatetv, lls);
        TextView updatetv2 = new TextView(context);
        updatetv2.setText(versionParse.versionInfo.des);
        view.addView(updatetv2, lls);
        if (versionParse.versionInfo.updateflag.equals("1")) {
            LinearLayout butons = new LinearLayout(context);
            butons.setOrientation(0);
            Button update = new Button(context);
            update.setText("强制升级");
            update.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Util.openBrowser(context, versionParse.versionInfo.packurl);
                }
            });
            Button exit = new Button(context);
            exit.setText("退出程序");
            butons.addView(update);
            butons.addView(exit);
            LinearLayout.LayoutParams ll = new LinearLayout.LayoutParams(-2, -2);
            ll.gravity = 17;
            view.addView(butons, ll);
            exit.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Process.killProcess(Process.myPid());
                }
            });
        } else {
            final CheckBox cv = new CheckBox(context);
            cv.setText("不再提醒");
            LinearLayout.LayoutParams ll2 = new LinearLayout.LayoutParams(-2, -2);
            ll2.gravity = 17;
            view.addView(cv, ll2);
            LinearLayout butons2 = new LinearLayout(context);
            butons2.setOrientation(0);
            Button update2 = new Button(context);
            update2.setText("版本升级");
            update2.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Util.openBrowser(context, versionParse.versionInfo.packurl);
                    ((AlertDialog) view.getTag()).cancel();
                }
            });
            Button nexttime = new Button(context);
            nexttime.setText("下次再说");
            nexttime.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (cv.isChecked()) {
                        LocalData.getInst().setLocalValue(LocalData.localDataKey_updatetips, "1");
                    } else {
                        LocalData.getInst().setLocalValue(LocalData.localDataKey_updatetips, "0");
                    }
                    ((AlertDialog) view.getTag()).cancel();
                }
            });
            butons2.addView(update2);
            butons2.addView(nexttime);
            LinearLayout.LayoutParams ll3 = new LinearLayout.LayoutParams(-2, -2);
            ll3.gravity = 17;
            view.addView(butons2, ll3);
        }
        return view;
    }
}
