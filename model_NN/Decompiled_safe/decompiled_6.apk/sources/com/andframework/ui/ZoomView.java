package com.andframework.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import com.jianq.net.JQBasicNetwork;

public class ZoomView extends FrameLayout {
    private Context context;
    private WebView scaleView;

    public ZoomView(Context context2) {
        super(context2);
        this.context = context2;
        initUI();
    }

    public ZoomView(Context context2, AttributeSet attrs) {
        super(context2, attrs);
        this.context = context2;
        initUI();
    }

    public ZoomView(Context context2, AttributeSet attrs, int defStyle) {
        super(context2, attrs, defStyle);
        this.context = context2;
        initUI();
    }

    private void initUI() {
        this.scaleView = new WebView(this.context);
        setBackgroundColor(-1);
        addView(this.scaleView, new LinearLayout.LayoutParams(-1, -1));
    }

    public void showZoomImage(String picurl) {
        if (picurl != null) {
            WebSettings setting = this.scaleView.getSettings();
            setting.setJavaScriptEnabled(true);
            setting.setSupportZoom(true);
            setting.setBuiltInZoomControls(true);
            setting.setDatabaseEnabled(false);
            if (picurl.contains("/mnt/")) {
                picurl = picurl.replace("/mnt/", "file:///");
            }
            this.scaleView.loadDataWithBaseURL(null, "<html><center><img src=\"" + picurl + "\" ></html>", "text/html", JQBasicNetwork.UTF_8, null);
        }
    }

    public void setZoomControlsVisible(boolean bool) {
        this.scaleView.getSettings().setBuiltInZoomControls(bool);
    }
}
