package com.andframework.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import cn.com.jit.pnxclient.constant.PNXConfigConstant;
import com.andframework.config.FrameworkConfig;
import com.andframework.zmfile.ZMFile;
import com.andframework.zmfile.ZMFilePath;
import com.jianq.misc.StringEx;
import com.jianq.net.JQBasicNetwork;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;
import org.w3c.dom.Node;

public class Util {
    public static final int FILELOG = 0;
    public static final int NETLOG = 1;
    public static final int SYSLOG = 2;

    public static synchronized void printLog(String tag, String msg, int type, int logLevel) {
        synchronized (Util.class) {
            if (type == 2) {
                switch (logLevel) {
                    case 2:
                        Log.v(tag, msg);
                        break;
                    case 3:
                        Log.d(tag, msg);
                        break;
                    case 4:
                        Log.i(tag, msg);
                        break;
                    case 5:
                        Log.w(tag, msg);
                        break;
                    case 6:
                        Log.e(tag, msg);
                        break;
                }
            } else if (type == 0) {
                String fileFullPath = String.valueOf(FrameworkConfig.getInst().getLogPath()) + FrameworkConfig.getInst().getLogName();
                try {
                    byte[] data = (String.valueOf(getFormatTime()) + PNXConfigConstant.RESP_SPLIT_3 + tag + "->" + msg + PNXConfigConstant.RESP_SPLIT_2).getBytes("utf-8");
                    if (new File(fileFullPath).exists()) {
                        ZMFile.write(fileFullPath, data, 0, data.length, ZMFile.getSize(fileFullPath));
                    } else {
                        ZMFile.write(fileFullPath, data, 0, data.length, 0);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return;
    }

    public static String getFormatTime() {
        long timestamp = System.currentTimeMillis();
        StringBuffer sb = new StringBuffer();
        Calendar cl = Calendar.getInstance();
        cl.setTime(new Date(timestamp));
        sb.append(cl.get(1));
        sb.append(PNXConfigConstant.RESP_SPLIT_3);
        sb.append(cl.get(2) + 1);
        sb.append(PNXConfigConstant.RESP_SPLIT_3);
        sb.append(cl.get(5));
        sb.append(PNXConfigConstant.RESP_SPLIT_3);
        int tem = cl.get(11);
        if (tem < 10) {
            sb.append("0");
        }
        sb.append(tem);
        sb.append(PNXConfigConstant.RESP_SPLIT_3);
        int tem2 = cl.get(12);
        if (tem2 < 10) {
            sb.append("0");
        }
        sb.append(tem2);
        sb.append(PNXConfigConstant.RESP_SPLIT_3);
        int tem3 = cl.get(13);
        if (tem3 < 10) {
            sb.append("0");
        }
        sb.append(tem3);
        sb.append(PNXConfigConstant.RESP_SPLIT_3);
        int tem4 = cl.get(14);
        if (tem4 < 10) {
            sb.append("0");
        }
        sb.append(tem4);
        return sb.toString();
    }

    public static String getNodeValue(Node node) {
        try {
            return node.getFirstChild().getNodeValue();
        } catch (Exception e) {
            return null;
        }
    }

    public static int dip2px(Context context, float dpValue) {
        return (int) ((dpValue * context.getResources().getDisplayMetrics().density) + 0.5f);
    }

    public static int px2dip(Context context, float pxValue) {
        return (int) ((pxValue / context.getResources().getDisplayMetrics().density) + 0.5f);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static String urlToFileName(String neturl) {
        StringBuffer sb = new StringBuffer();
        String url = new String(neturl);
        int lastindex = 0;
        int len = url.length();
        for (int index = 0; index < len; index++) {
            if (url.charAt(index) == '/') {
                if (index - lastindex > 1 && lastindex != 0) {
                    String split = url.substring(lastindex + 1, index);
                    int mh = split.indexOf(58);
                    if (mh > 0) {
                        split = split.substring(0, mh);
                    }
                    sb.append(split);
                    sb.append(ZMFilePath.FILE_SEPARATOR);
                }
                lastindex = index;
            }
        }
        sb.append(url.substring(lastindex + 1, len));
        return sb.toString().replace('/', '^').replace('?', '^');
    }

    public static boolean emailValidate(String email) {
        return Pattern.compile("\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*").matcher(email).find();
    }

    public static void hideInputMethod(View view, Activity act) {
        ((InputMethodManager) act.getSystemService("input_method")).hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void openBrowser(Context context, String url) {
        context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
    }

    public static int getVersionCode(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException err) {
            Log.e("getVersionCode", err.getMessage(), err);
            return 0;
        }
    }

    public static String getVersionCodeName(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException err) {
            Log.e("getVersionCodeName", err.getMessage(), err);
            return StringEx.Empty;
        }
    }

    public static int getStatusBarHeight(Activity activity) {
        Rect rect = new Rect();
        activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
        return rect.top;
    }

    public static void setFullScreen(Activity activity) {
        activity.requestWindowFeature(1);
        activity.getWindow().setFlags(1024, 1024);
    }

    public static Location getLocation(Activity act, LocationListener locationListener) {
        LocationManager lm = (LocationManager) act.getSystemService("location");
        Location location = null;
        try {
            location = lm.getLastKnownLocation("network");
            if (location != null) {
                Log.i("getLocation", "from NETWORK");
                return location;
            }
        } catch (IllegalArgumentException e) {
            Log.i("getLocation", "from NETWORK IllegalArgumentException");
        } catch (SecurityException e2) {
            Log.i("getLocation", "from NETWORK SecurityException");
        }
        Criteria criteria = new Criteria();
        criteria.setAccuracy(1);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setCostAllowed(true);
        criteria.setPowerRequirement(1);
        String provider = lm.getBestProvider(criteria, true);
        if (provider != null) {
            try {
                location = lm.getLastKnownLocation(provider);
                if (location != null) {
                    Log.i("getLocation", "from Criteria");
                    return location;
                }
            } catch (IllegalArgumentException e3) {
                Log.i("getLocation", "from NETWORK 2 IllegalArgumentException");
            } catch (SecurityException e4) {
                Log.i("getLocation", "from NETWORK 2 SecurityException");
            }
        }
        try {
            location = lm.getLastKnownLocation("gps");
            if (location != null) {
                Log.i("getLocation", "from GPS_PROVIDER");
                return location;
            }
        } catch (IllegalArgumentException e5) {
            Log.i("getLocation", "from NETWORK 3 IllegalArgumentException");
        } catch (SecurityException e6) {
            Log.i("getLocation", "from NETWORK 3 SecurityException");
        }
        if (locationListener != null) {
            lm.requestLocationUpdates("network", 1000, 0.0f, locationListener);
        }
        return location;
    }

    public static void shareContent(Context context, String share, String title) {
        Intent intent = new Intent("android.intent.action.SEND");
        intent.setType(JQBasicNetwork.PLAIN_TEXT_TYPE);
        intent.putExtra("android.intent.extra.SUBJECT", title);
        intent.putExtra("android.intent.extra.TEXT", share);
        context.startActivity(Intent.createChooser(intent, title));
    }
}
