package com.andframework.myinterface;

import android.content.Intent;

public interface ActivityResultCallBack {
    void onActivityResult(int i, int i2, Intent intent);
}
