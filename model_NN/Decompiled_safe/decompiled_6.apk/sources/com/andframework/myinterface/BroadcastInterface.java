package com.andframework.myinterface;

import android.os.Bundle;

public interface BroadcastInterface {
    boolean getBoardcast(String str, Bundle bundle);
}
