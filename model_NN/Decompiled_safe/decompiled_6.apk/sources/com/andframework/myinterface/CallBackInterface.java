package com.andframework.myinterface;

public interface CallBackInterface {
    public static final int DESTROY = 4;
    public static final int PAUSE = 2;
    public static final int RESUME = 1;
    public static final int START = 0;
    public static final int STOP = 3;

    void CallBack(int i, Object obj);
}
