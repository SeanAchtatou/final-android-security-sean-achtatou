package com.andframework.myinterface;

import com.andframework.business.BaseBusi;

public interface UiCallBack {
    void uiCallBack(BaseBusi baseBusi);
}
