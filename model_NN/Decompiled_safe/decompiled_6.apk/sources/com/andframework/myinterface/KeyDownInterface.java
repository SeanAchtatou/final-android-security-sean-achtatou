package com.andframework.myinterface;

import android.view.KeyEvent;

public interface KeyDownInterface {
    public static final int ISNONE = 3;
    public static final int ISNORETURN = 2;
    public static final int ISRETURN = 1;

    int onKeyDown(int i, KeyEvent keyEvent);
}
