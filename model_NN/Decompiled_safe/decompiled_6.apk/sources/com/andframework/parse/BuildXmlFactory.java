package com.andframework.parse;

import android.util.Log;
import android.util.Xml;
import com.jianq.misc.StringEx;
import com.jianq.net.JQBasicNetwork;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlSerializer;

public class BuildXmlFactory {
    public static String buildXml(ArrayList<String> array) {
        if (array.size() == 0) {
            return null;
        }
        try {
            Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
            Element root = doc.createElement(array.get(0));
            doc.appendChild(root);
            for (int i = 1; i < array.size() - 1; i++) {
                doc.createElement(array.get(i));
                doc.createTextNode(array.get(i + 1));
            }
            XmlSerializer serializer = Xml.newSerializer();
            StringWriter writer = new StringWriter();
            serializer.setOutput(writer);
            serializer.startDocument(JQBasicNetwork.UTF_8, 1);
            serializer.startTag(StringEx.Empty, root.getNodeName());
            String text = root.getNodeValue();
            if (text != null) {
                serializer.text(text);
            }
            NamedNodeMap map = root.getAttributes();
            int attrSize = map.getLength();
            for (int i2 = 0; i2 < attrSize; i2++) {
                Node attrNode = map.item(i2);
                serializer.attribute(StringEx.Empty, attrNode.getNodeName(), attrNode.getNodeValue());
            }
            serializer.endDocument();
            return writer.toString();
        } catch (ParserConfigurationException pce) {
            Log.i("buildXml ParserConfigurationException:", pce.getMessage());
            return null;
        } catch (IllegalArgumentException e) {
            Log.i("buildXml IllegalArgumentException:", e.getMessage());
            return null;
        } catch (IllegalStateException e2) {
            Log.i("buildXml IllegalStateException:", e2.getMessage());
            return null;
        } catch (IOException e3) {
            Log.i("buildXml IOException:", e3.getMessage());
            return null;
        }
    }

    public static Document getDom(InputSource in) {
        try {
            return DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(in);
        } catch (SAXException e) {
            Log.i("getDom(InputSource in) SAXException:", e.getMessage());
            return null;
        } catch (IOException e2) {
            Log.i("getDom(InputSource in) IOException:", e2.getMessage());
            return null;
        } catch (ParserConfigurationException e3) {
            Log.i("getDom(InputSource in) ParserConfigurationException:", e3.getMessage());
            return null;
        }
    }

    public static Document getDom(File in) {
        try {
            return DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(in);
        } catch (SAXException e) {
            Log.i("getDom(File in) SAXException:", e.getMessage());
            return null;
        } catch (IOException e2) {
            Log.i("getDom(File in) IOException:", e2.getMessage());
            return null;
        } catch (ParserConfigurationException e3) {
            Log.i("getDom(File in) ParserConfigurationException:", e3.getMessage());
            return null;
        }
    }

    public static Document getDom(InputStream in) {
        DocumentBuilderFactory dbfactory = DocumentBuilderFactory.newInstance();
        try {
            dbfactory.setValidating(false);
            return dbfactory.newDocumentBuilder().parse(in);
        } catch (SAXException e) {
            Log.i("getDom(InputStream in) SAXException:", e.getMessage());
            return null;
        } catch (IOException e2) {
            Log.i("getDom(InputStream in) IOException:", e2.getMessage());
            return null;
        } catch (ParserConfigurationException e3) {
            Log.i("getDom(InputStream in) ParserConfigurationException:", e3.getMessage());
            return null;
        }
    }

    public static Document getDom(String in) {
        try {
            return DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(in);
        } catch (SAXException e) {
            Log.i("getDom(String in) SAXException:", e.getMessage());
            return null;
        } catch (IOException e2) {
            Log.i("getDom(String in) IOException:", e2.getMessage());
            return null;
        } catch (ParserConfigurationException e3) {
            Log.i("getDom(String in) ParserConfigurationException:", e3.getMessage());
            return null;
        }
    }
}
