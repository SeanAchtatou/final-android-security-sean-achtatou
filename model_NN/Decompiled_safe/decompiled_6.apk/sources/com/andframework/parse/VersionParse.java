package com.andframework.parse;

import cn.com.jit.pnxclient.constant.ApplicationConstant;
import com.jianq.misc.StringEx;
import org.json.JSONException;

public class VersionParse extends BaseParse {
    public int code;
    public VersionInfo versionInfo = new VersionInfo();

    public boolean parse(byte[] data) {
        if (!super.parse(data) || this.parseType != 0) {
            return false;
        }
        try {
            if (!this.jsonObject.isNull("code")) {
                this.code = this.jsonObject.getInt("code");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            this.versionInfo = new VersionInfo();
            this.versionInfo.appname = this.jsonObject.getString(ApplicationConstant.APP_NAME);
            this.versionInfo.updateflag = this.jsonObject.getString("updateflag");
            this.versionInfo.packsize = this.jsonObject.getString("packsize");
            this.versionInfo.updatetime = this.jsonObject.getString("updatetime");
            this.versionInfo.des = this.jsonObject.getString("des");
            this.versionInfo.packurl = this.jsonObject.getString("packurl");
            return true;
        } catch (JSONException e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public class VersionInfo {
        public String appname = StringEx.Empty;
        public String des = StringEx.Empty;
        public String packsize = StringEx.Empty;
        public String packurl = StringEx.Empty;
        public String updateflag = "2";
        public String updatetime = StringEx.Empty;

        public VersionInfo() {
        }
    }
}
