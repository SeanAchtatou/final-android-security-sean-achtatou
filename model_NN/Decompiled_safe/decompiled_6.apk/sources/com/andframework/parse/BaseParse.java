package com.andframework.parse;

import cn.com.jit.pnxclient.constant.PNXConfigConstant;
import com.jianq.misc.StringEx;
import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;

public class BaseParse {
    public static final int Binary = 2;
    public static final int JSON = 0;
    public static final int XML = 1;
    public Document doc;
    public String errorCode;
    public int httpResponseCode;
    public JSONObject jsonObject;
    public String message;
    protected int parseType;

    public BaseParse() {
        this.parseType = 0;
        this.parseType = 0;
    }

    public BaseParse(int type) {
        this.parseType = 0;
        this.parseType = type;
    }

    public boolean parse(byte[] data) {
        if (data == null) {
            return false;
        }
        if (this.parseType == 2) {
            return true;
        }
        if (this.parseType == 0) {
            try {
                this.jsonObject = new JSONObject(new String(data, "utf-8").replace(PNXConfigConstant.RESP_SPLIT_2, StringEx.Empty).replace("\r", StringEx.Empty).replace("\n", StringEx.Empty));
                if (!this.jsonObject.isNull("errorcode")) {
                    this.errorCode = this.jsonObject.getString("errorcode");
                }
                if (!this.jsonObject.isNull("message")) {
                    this.message = this.jsonObject.getString("message");
                }
                return true;
            } catch (JSONException e) {
                e.printStackTrace();
                return false;
            } catch (UnsupportedEncodingException e2) {
                e2.printStackTrace();
                return false;
            }
        } else if (this.parseType != 1) {
            return false;
        } else {
            this.doc = BuildXmlFactory.getDom(new ByteArrayInputStream(data));
            return true;
        }
    }
}
