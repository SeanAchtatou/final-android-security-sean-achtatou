package com.andframework.parse;

public class BinaryParse extends BaseParse {
    public byte[] revData;

    public BinaryParse(int type) {
        super(type);
    }

    public boolean parse(byte[] data) {
        if (!super.parse(data)) {
            return false;
        }
        this.revData = data;
        return true;
    }
}
