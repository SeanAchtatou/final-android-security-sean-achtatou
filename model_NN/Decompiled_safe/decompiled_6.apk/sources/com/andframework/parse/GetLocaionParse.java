package com.andframework.parse;

import android.location.Location;
import com.jianq.misc.StringEx;
import org.json.JSONException;
import org.json.JSONObject;

public class GetLocaionParse extends BaseParse {
    private String locStr = StringEx.Empty;
    public Location location;

    public GetLocaionParse(int type) {
        super(type);
    }

    public GetLocaionParse() {
        super(0);
    }

    public String getLocDes() {
        return this.locStr;
    }

    public boolean parse(byte[] data) {
        boolean ret = super.parse(data);
        if (!ret || !ret || this.parseType != 0) {
            return false;
        }
        try {
            this.locStr = this.jsonObject.toString();
            JSONObject locObj = this.jsonObject.getJSONObject("location");
            if (!locObj.has("latitude") || !locObj.has("longitude")) {
                return false;
            }
            this.location = new Location("telephone");
            this.location.setLatitude(locObj.getDouble("latitude"));
            this.location.setLongitude(locObj.getDouble("longitude"));
            this.location.setAccuracy(locObj.has("accuracy") ? (float) locObj.getDouble("accuracy") : 0.0f);
            this.location.setTime(System.currentTimeMillis());
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }
}
